/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.dto.ViewLookupTableDTO;
import com.resolve.persistence.dao.CustomTableDAO;
import com.resolve.persistence.dao.MetaFilterDAO;
import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaFilter;
import com.resolve.persistence.model.MetaTable;
import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.persistence.model.MetaViewLookup;
import com.resolve.persistence.model.MetaxFieldDependency;
import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.vo.RsMetaFilterDTO;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.MetaFieldPropertiesVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.hibernate.vo.MetaViewLookupVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;

public class CustomTableUtil
{
    /**
     * Notes:
     * 
     * MetaFieldProperties.u_ui_type == MetaField.u_type
     * 
     * 
     */
    
    private static final String SPACE = " ";
    private static final String UNDER_SCORE = "_";
    private static final String COLON = ":";
    private static final String PERIOD = ".";
    
    public static final Map<CustomFormUIType, String> rsUITypeToDbTypeMapping = new HashMap<CustomFormUIType, String>();
    static
    {
        rsUITypeToDbTypeMapping.put(CustomFormUIType.TextField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());

        //this will be checked for the length of the string - if length > 4000, then update this to CLOB
        rsUITypeToDbTypeMapping.put(CustomFormUIType.TextArea, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        
        rsUITypeToDbTypeMapping.put(CustomFormUIType.RadioButton,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.NumberTextField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_LONG.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.DecimalTextField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_DOUBLE.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.MultipleCheckBox,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.ComboBox,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.Date, HibernateConstantsEnum.CUSTOMTABLE_TYPE_TIMESTAMP.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.Timestamp, HibernateConstantsEnum.CUSTOMTABLE_TYPE_TIMESTAMP.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.CheckBox,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_BOOLEAN.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.Password,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.Link,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.List,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.DateTime, HibernateConstantsEnum.CUSTOMTABLE_TYPE_TIMESTAMP.getTagName());

        //new mappings - STILL TO DECIDE - TODO
        rsUITypeToDbTypeMapping.put(CustomFormUIType.NameField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.AddressField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.EmailField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.MACAddressField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.IPAddressField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.CIDRAddressField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.PhoneField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.UserPickerField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.TeamPickerField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.MultiSelectComboBoxField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.WysiwygField, HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        
        //DB Type only
        rsUITypeToDbTypeMapping.put(CustomFormUIType.Reference,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.Sequence,  HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        rsUITypeToDbTypeMapping.put(CustomFormUIType.Journal, HibernateConstantsEnum.CUSTOMTABLE_TYPE_CLOB.getTagName());

        
//        rsUITypeToDbTypeMapping.put(CustomFormUIType.ButtonField, WorkflowConstants.CUSTOMTABLE_TYPE_STRING.getTagName());
    }
    
    
    public static ResponseDTO<CustomTableVO> getCustomTables(QueryDTO query, String username) throws Exception
    {
        List<CustomTableVO> vos = new ArrayList<CustomTableVO>();
        int total = 0;

        try
        {
            int limit = query.getLimit();
            int offset = query.getStart();
            
            //filter out the '_fu' tables from the list
            String whereClause = " UName NOT LIKE '%_fu' ";
            
            //get the list of tables excluded for the list
            Set<String> excludeTables = new HashSet<String>(PropertiesUtil.getPropertyList("customtable.table.exclude.list"));
            if (excludeTables.size() > 0)
            {
                whereClause = whereClause  + " AND UName NOT IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(excludeTables)) + ") ";
            }

            //update the whereClause
            query.setWhereClause(whereClause);

            //get the data
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        CustomTable instance = new CustomTable();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        vos.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        CustomTable instance = (CustomTable) o;
                        vos.add(instance.doGetVO());
                    }
                }
            }
            
            //get the total count
            total = ServiceHibernate.getTotalHqlCount(query);
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
            throw t;
        }
        
        ResponseDTO<CustomTableVO> response = new ResponseDTO<CustomTableVO>();
        response.setRecords(vos);
        response.setTotal(total);
        
        return response;
    }
    
    public static CustomTableVO findCustomTableWithFields(String sysId, String name, String username) throws Exception
    {
        CustomTable model = null;
        CustomTableVO result = null;

        try
        {
            model = findCustomTableModelWithFields(sysId, name, username);
            if (model != null)
            {
                result = model.doGetVO();
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }

        return result;
        
    } // findCustomTableWithReferences
    
    public static CustomTableVO saveCustomTable(CustomTableVO customTableVO, String username) throws Exception
    {
        CustomTableVO result = null;
        
        if(customTableVO != null)
        {
            if(UserUtils.isAdminUser(username))
            {
                //presently we are only updating the Display name of the Custom table
                //in the later versions, we may add the changes for fields also
                if(StringUtils.isNotBlank(customTableVO.getSys_id()))
                {
                    try
                    {
                    	//prepare the new VO obj
                      HibernateProxy.setCurrentUser(username);
                    	result = ((CustomTable) HibernateProxy.execute(() -> {

	                        CustomTable ct = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(customTableVO.getSys_id());
	                        if(ct != null)
	                        {
	                            ct.setUDisplayName(customTableVO.getUDisplayName());
	                        }
	                        
	                        //persist
	                        HibernateUtil.getDAOFactory().getCustomTableDAO().persist(ct);
	                        return ct;
                    	})).doGetVO();
                    }
                    catch(Throwable e)
                    {
                        Log.log.error("Failed to persist property: " + e.getMessage(), e);
                        throw new Exception(e);
                    }
                }
            }
            else
            {
                throw new Exception("You need to have 'admin' role for this operation");
            }
        }

        return result;
    }
    
    public static void deleteCustomTableByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {
        Set<String> sysIdsToDelete = new HashSet<String>();

        //prepare the set of ids to delete
        if (deleteAll)
        {
            //get all the sysIds from the table
            sysIdsToDelete.addAll(GeneralHibernateUtil.getSysIdsFor("CustomTable", null, username));
        }
        else if(sysIds != null && sysIds.length > 0)
        {
            sysIdsToDelete.addAll(Arrays.asList(sysIds));
        }

        //delete tables one by one
        for(String sysIdToDelete : sysIdsToDelete)
        {
            try
            {
                Set<String> relatedTables = new DeleteCustomTable(sysIdToDelete, null, username).delete();
                if(relatedTables != null && relatedTables.size() > 0)
                {
                    for(String relatedTable : relatedTables)
                    {
                        new DeleteCustomTable(null, relatedTable, username).delete();
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("error deleting the table with sysId :" + sysIdToDelete, e);
            }
        }
        
        //reinitialize hibernate after all the deletes
        HibernateUtil.reinit();
        
    }
    
    @SuppressWarnings("unchecked")
	public static List<CustomTableVO> getAllCustomTables(String username) throws Exception
    {
        List<CustomTableVO> result = new ArrayList<CustomTableVO>();
        List<CustomTable> list = null;
//        OrderbyProperty orderByProperty = new OrderbyProperty("UName", true);

        try
        {
          HibernateProxy.setCurrentUser(username);
            list = (List<CustomTable>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getCustomTableDAO().find();
            });
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
            throw new Exception(t);
        }

        if (list != null)
        {
            for (CustomTable model : list)
            {
                result.add(model.doGetVO());
            }
        }

        return result;
    }

    
    public static Map<String, Object> getCustomTableRecord(String tableName, String sysId)
    {
       return ServiceHibernate.findById(tableName, sysId);
    }

    public static Map<String, Object> findFirstCustomTableRecord(String tableName, Map<String, Object> example)
    {
        return ServiceHibernate.findFirstCustomTableRecord(tableName, example);
    }
    
    @SuppressWarnings("unused")
    public static CustomTable findCustomTableAndLoadAllReferences(String tableSysId, String tableName, String username)
    {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (CustomTable) HibernateProxy.execute(() -> {
        		CustomTable table = null;
                int x = 0;// temp variable
                
	            if (StringUtils.isNotBlank(tableSysId))
	            {
	                // we have to do this as hibernate does not qry for ids
	                table = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(tableSysId);
	            }
	            else if (StringUtils.isNotEmpty(tableName))
	            {
	                //to make this case-insensitive
	                //String sql = "from CustomTable where LOWER(UName) = '" + tableName.toLowerCase().trim() + "'";
	                String sql = "from CustomTable where LOWER(UName) = :UName";
	                
	                Map<String, Object> queryParams = new HashMap<String, Object>();
	                
	                queryParams.put("UName", tableName.toLowerCase().trim());
	                
	                try
	                {
	                    List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
	                    if (list != null && list.size() > 0)
	                    {
	                        table = (CustomTable) list.get(0);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Error while executing hql:" + sql, e);
	                }
	
	            }
	
	            if (table != null)
	            {
	                // load everything in the memory
	                if (table.getMetaTable() != null)
	                {
	                    table.getMetaTable().getUName();
	
	                    // filters
	                    x = (table.getMetaTable().getMetaFilters() != null) ? table.getMetaTable().getMetaFilters().size() : 0;
	
	                    // table views
	                    x = (table.getMetaTable().getMetaTableViews() != null) ? table.getMetaTable().getMetaTableViews().size() : 0;
	                }
	
	                x = (table.getMetaFields() != null) ? table.getMetaFields().size() : 0;
	
	            }

	            return table;
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return null;

    }
    
    /**
     * 
     * gets a map of sysId-metatableview name that a user has access to for a table 
     * 
     * @param tableName
     * @param username
     * @param rightType
     * @return
     * @throws Exception
     */
    public static Map<String, String> findMetaTableViewForUser(String tableName, String username, RightTypeEnum rightType) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();
        if(rightType == null) 
            rightType = RightTypeEnum.view;
        
        if(StringUtils.isNotBlank(tableName) && StringUtils.isNotBlank(username))
        {
            //get user roles
            Set<String> userRoles = UserUtils.getUserRoles(username);
            
            //prepare the hql 
            StringBuilder hql = new StringBuilder("select b.sys_id, b.UName ");
            hql.append("from MetaTable as a ");
            hql.append("join a.metaTableViews as b ");
            hql.append("join b.metaAccessRights as c ");
            //hql.append("where lower(a.UName) = '").append(tableName.toLowerCase()).append("' and ");
            hql.append("where lower(a.UName) = :UName and ");
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", tableName.toLowerCase());
            
            //role filter 
            String roleFilter = "";
            //qry only if its not an 'admin', else show everything
            if (!userRoles.contains("admin") && !username.equals("resolve.maint"))
            {
                StringBuilder filters = new StringBuilder("(");
                for (String userRole : userRoles)
                {
                    String userRoleSQLSanitizied = "param_" + userRole.trim().replaceAll(SPACE, UNDER_SCORE).replaceAll(COLON, UNDER_SCORE).replaceAll(PERIOD, UNDER_SCORE);
                    //filters.append(" c.UAdminAccess like '%" + userRole + "%' or c.UReadAccess like '%" + userRole + "%' or c.UWriteAccess like '%" + userRole + "%' or");
                    filters.append(" c.UAdminAccess like :" + userRoleSQLSanitizied + " or c.UReadAccess like :" + userRoleSQLSanitizied + " or c.UWriteAccess like :" + userRoleSQLSanitizied + " or");
                    queryParams.put(userRoleSQLSanitizied, "%" + userRole + "%");
                }
                filters.setLength(filters.length() - 2);
                filters.append(") ");  
                roleFilter = filters.toString();
            }
            else
            {
                roleFilter = " 1=1 ";
            }
            hql.append(roleFilter);
            
            //execute the qry and get the data
            try
            {
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
                if(data != null && data.size() > 0)
                {
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        String sysId = (String) record[0];
                        String viewName = (String) record[1];
                        
                        result.put(sysId, viewName);
                        
                    }//end of for loop
                    
                }//end of if
            }
            catch (Exception e)
            {
               Log.log.error("Error while getting doc-tag with sql: " + hql, e);
               throw e;
            }
        }
        
        
        return result;
    }
    
    public static List<MetaTableView> findMetaTableViews(String tableName, String username, RightTypeEnum rightType) throws Exception
    {
        List<MetaTableView> results = new ArrayList<MetaTableView>();
        if (StringUtils.isNotEmpty(tableName))
        {
            Map<String, String> viewsThatUserHasAccessTo = findMetaTableViewForUser(tableName, username, rightType);
         
            //TODO: use the map
//            MetaTable metaTable = new MetaTable();
//            metaTable.setUName(tableName);
            if (viewsThatUserHasAccessTo.size() > 0)
            {
                Set<String> sysIds = viewsThatUserHasAccessTo.keySet();
                try
                {

                  HibernateProxy.setCurrentUser(username);
                	HibernateProxy.execute(() -> {
                    
	                    for(String sysId : sysIds)
	                    {
	                        MetaTableView metaTableView = HibernateUtil.getDAOFactory().getMetaTableViewDAO().findById(sysId);
	                        if(metaTableView != null)
	                        {
	                            // this view and all the child views, the user has access to
	                            metaTableView.getMetaTable().getSys_id();// loading the meta table
	                            results.add(metaTableView);   
	                            
	                         // **NOTE: we are not loading the MetaField as that is not the purpose of getting all the views. We should avoid it as the
	                            // fields will be duplicated in different views and data will be enormous.
	                            //                                    Collection<MetaTableView> childViews = metaTableView.getMetaTableViews();
	                            //                                    if (childViews != null && childViews.size() > 0)
	                            //                                    {
	                            //                                        for (MetaTableView child : childViews)
	                            //                                        {
	                            //                                            child.getMetaTable().getsys_id();
	                            //                                            results.add(child);
	                            //                                        }
	                            //                                    }// end of if
	                        }
	                    }
	                    
                	});
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
            }
            
        }

        return results;

    }
    
    
    public static MetaTableView findMetaTableViewWithReferences(String tableName, String viewName, RightTypeEnum rightType, String username) throws Exception
    {
        MetaTableView viewResult = null;

        if (StringUtils.isNotEmpty(tableName) && StringUtils.isNotEmpty(viewName) && StringUtils.isNotBlank(username))
        {
            Set<String> userRoles = UserUtils.getUserRoles(username);
            
            MetaTable example = new MetaTable();
            example.setUName(tableName);

            Collection<MetaTableViewField> mtvfldsToRemove = new ArrayList<MetaTableViewField>();
            Set<String> uniqueMetaFieldSysIds = new HashSet<String>();
            
            try
            {
              HibernateProxy.setCurrentUser(username);
            	viewResult = (MetaTableView) HibernateProxy.execute(() -> {
            		MetaTableView metaViewResult = null;
            		String viewRoles = null;
            		MetaTable table = HibernateUtil.getDAOFactory().getMetaTableDAO().findFirst(example);

            		if (table != null)
	                {
	                    metaViewResult = lookupMetaTableView(table.getMetaTableViews(), viewName);
	                    if (metaViewResult != null)
	                    {
	                        viewRoles = getMetaTableViewRoles(metaViewResult, rightType);
	                        if(StringUtils.isNotBlank(viewRoles))
	                        {
	                            // check if the user has rights for this view
	                            boolean hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, viewRoles);
	                            if(!hasRights)
	                            {
	                                throw new Exception("User '" + username + "' does not have " + rightType + " rights for view '" + viewName + "'");
	                            }
	                        }
	                        
	                        // Is this needed? commenting it out 
	                        // viewResult.getMetaTable().getSys_id();
	
	                        // load everything
	                        Collection<MetaTableViewField> metaTableViewFields = metaViewResult.getMetaTableViewFields();
	                        
	                        if (metaTableViewFields != null)
	                        {
	                            for (MetaTableViewField viewField : metaTableViewFields)
	                            {
	                                // load it
	                                // Is this needed? commenting it out
	                                // viewField.getMetaField().getMetaFieldProperties().getSys_id();
	                                if (viewField.getMetaFieldTableProperties() != null)
	                                {
	                                    viewField.getMetaFieldTableProperties().getSys_id();
	                                }
	                                
	                                if (viewField.getMetaField() != null )
	                                {
	                                    if (uniqueMetaFieldSysIds.contains(viewField.getMetaField().getSys_id()))
	                                    {
	                                        mtvfldsToRemove.add(viewField);
	                                    }
	                                    else
	                                    {
	                                        uniqueMetaFieldSysIds.add(viewField.getMetaField().getSys_id());
	                                    }
	                                }
	                            }
	                        }
	
	                        // load the parent
	                        MetaTableView parent = metaViewResult.getParentMetaTableView();
	                        if (parent != null)
	                        {
	                            parent.getMetaAccessRights().getSys_id();
	                        }
	
	                    }
	                }
            		
            		return metaViewResult;
            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                throw new Exception(t);
            }
            
            Log.log.debug("" + tableName + ":" + viewName + " has " + uniqueMetaFieldSysIds.size() + " uniqueue fields");
            
            if (viewResult != null && viewResult.getMetaTableViewFields() != null && !mtvfldsToRemove.isEmpty())
            {
                viewResult.getMetaTableViewFields().removeAll(mtvfldsToRemove);
            }
        }

        return viewResult;

    }
    
    @SuppressWarnings("unchecked")
	public static Collection<MetaFieldVO> getColumnsForTable(String tableSysId, String tableModelName, String username) throws Exception
    {
        Collection<MetaFieldVO> result = new ArrayList<MetaFieldVO>();
        Collection<MetaField> metaFields = null;

        if (!(StringUtils.isEmpty(tableModelName) && StringUtils.isEmpty(tableSysId)))
        {
            
            try
            {
              HibernateProxy.setCurrentUser(username);
            	metaFields = (Collection<MetaField>) HibernateProxy.execute(() -> {
            		Collection<MetaField> res = null;
                	CustomTable query = new CustomTable();
                    query.setUModelName(tableModelName);
                    CustomTableDAO dao = HibernateUtil.getDAOFactory().getCustomTableDAO();
                    if (StringUtils.isNotEmpty(tableSysId))
                    {
                        query = dao.findById(tableSysId);
                    }
                    else
                    {
                        query = dao.findFirst(query);
                    }

                    if (query != null)
                    {
                        res = query.getMetaFields();
                        if (res != null)
                        {
                            for (MetaField field : res)
                            {
                                MetaFieldProperties metaFieldProperties = field.getMetaFieldProperties();
                                
                                // This should never happen, MetaField properties can not be null
                                
                                if (metaFieldProperties != null)
                                {
                                    Collection<MetaxFieldDependency> metaxFieldDependencys = metaFieldProperties.getMetaxFieldDependencys();
                                    if (metaxFieldDependencys != null)
                                    {
                                        metaxFieldDependencys.size();
                                    }
                                }
                                else
                                {
                                    Log.log.warn("MetaField " + field.getUDisplayName() + " has its default MetaFieldProperties deleted!!!");
                                }
                            }
                        }
                    }
                    
                    return res;
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                throw new Exception(t);
            }
            
            if (metaFields != null)
            {
                for (MetaField field : metaFields)
                {
                    result.add(field.doGetVO());
                }
            }
        }
        return result;
    }
    
    public static CustomTableVO getCustomTableWithReferences(String tableSysId, String tableName, String username) throws Exception
    {
        CustomTableVO result = null;
        CustomTable customTable = null;

        try
        {

          HibernateProxy.setCurrentUser(username);
        	customTable = (CustomTable) HibernateProxy.execute(() -> {
        		
        		CustomTable res = null;
        		
                if (StringUtils.isNotBlank(tableSysId))
                {
                    // we have to do this as hibernate does not qry for ids
                	res = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(tableSysId);
                }
                else if (StringUtils.isNotEmpty(tableName))
                {
                	res = new CustomTable();
                	res.setUName(tableName);

                    res = HibernateUtil.getDAOFactory().getCustomTableDAO().findFirst(res);
                }
                
                if (res != null)
                {
                    // load the data
                    Collection<MetaField> fields = res.getMetaFields();
                    
                    if (fields != null)
                    {
                        for (MetaField field : fields)
                        {
                            if (field.getMetaFieldProperties() != null && field.getMetaFieldProperties().getMetaxFieldDependencys() != null)
                            {
                                field.getMetaFieldProperties().getMetaxFieldDependencys().size();
                            }
                            else
                            {
                                Log.log.warn("MetaField " + field.getUDisplayName() + " has its default MetaFieldProperties deleted!!!");
                            }
                        }
                    }
                }
                
                return res;
                
                
        	});

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if (customTable != null)
        {
            result = customTable.doGetVO();
        }

        return result;
    }
    
    public static List<MetaFieldPropertiesVO> getReferenceTables(String tableName, String username) throws Exception
    {
        List<MetaFieldPropertiesVO> results = new ArrayList<MetaFieldPropertiesVO>();
        List<MetaFieldProperties> metafields = new ArrayList<MetaFieldProperties>();

        if (StringUtils.isNotEmpty(tableName))
        {
            // SELECT u_name, u_table FROM meta_field_properties WHERE u_ui_type = 'Reference' AND u_reference_table = 'cust_table_1';
            //String hql = "from MetaFieldProperties where UUIType = 'Reference' and UReferenceTable = '" + tableName.trim() + "'";
            String hql = "from MetaFieldProperties where UUIType = 'Reference' and UReferenceTable = :UReferenceTable";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UReferenceTable", tableName.trim());
            
            try
            {
                Set<String> current = new HashSet<String>();

                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
                for(Object o : list)
                {
                    MetaFieldProperties prop = (MetaFieldProperties) o;
                    String refTable = prop.getUTable();
                    if (StringUtils.isNotBlank(refTable) && !current.contains(refTable))
                    {
                        metafields.add(prop);
                        current.add(refTable);
                    }
                }
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
            
            if(metafields.size() > 0)
            {
                for(MetaFieldProperties model : metafields)
                {
                    results.add(model.doGetVO());
                }
            }
        }
        
        return results;
    }
    
    public static ViewLookupTableDTO saveViewLookup(ViewLookupTableDTO viewLookup, String username) throws Exception
    {
        MetaViewLookup lookup = null;
        if(StringUtils.isNotBlank(viewLookup.getId()))
        {
            lookup = getMetaViewLookup(viewLookup.getId());
        }
        
        if(lookup == null)
        {
            lookup = new MetaViewLookup();
        }
        
        lookup.setUAppName(viewLookup.getAppName());
        lookup.setUViewName(viewLookup.getView());
        lookup.setURoles(viewLookup.getRoles());
        lookup.setUOrder(viewLookup.getOrder());

        //save it
        try
        {
            lookup = SaveUtil.saveMetaViewLookup(lookup, username);
            viewLookup.setId(lookup.getSys_id());
        }
        catch(Throwable t)
        {
            Log.log.error("error in updating the meta form lookup", t);
        }
        
        return viewLookup;
        
    }
    
    public static void updateMetaViewLookupOrder(List<String> sysIds, String username)
    {
        if (sysIds != null)
        {   
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
                	MetaViewLookup record = null;
                	int count = 1;
	                for (String sysId : sysIds)
	                {
	                    record = HibernateUtil.getDAOFactory().getMetaViewLookupDAO().findById(sysId);
	                    if (record != null)
	                    {
	                        record.setUOrder(count);
	                        count++;
	                    }
	                }

                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
                
            }
        }
    }
    
    
    public static List<RsMetaFilterDTO> getUserFilters(String tablename, String username) throws Exception
    {
        List<RsMetaFilterDTO> filters = new ArrayList<RsMetaFilterDTO>();
        List<MetaFilter> metaFilters = getAllFilters(tablename, username);
        
        for(MetaFilter metaFilter : metaFilters)
        {
            filters.add(convertMetaFilterToModel(metaFilter));
        }
        
        return filters;
    }
    
    public static RsMetaFilterDTO saveMetaFilter(RsMetaFilterDTO filterDTO, String username) throws Exception
    {       
        
        String metaTableName = filterDTO.getMetaTableName();
        
        //Note: Accessrights for filter is not yet implemented. We may do this in the next version 
        try
        {
        	final RsMetaFilterDTO filterDTOFinal = filterDTO;
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	MetaFilter metaFilter = null;
                MetaTable metaTable = null;

	            metaTable =  findMetaTableByIdOrByName(null, metaTableName, username);
	            if(metaTable == null)
	            {
	                throw new Exception("MetaTable does not exist for filter :" + filterDTOFinal.getName());
	            }
	            
	            if(StringUtils.isNotBlank(filterDTOFinal.getSysId()))
	            {
	                //update
	                metaFilter = HibernateUtil.getDAOFactory().getMetaFilterDAO().findById(filterDTOFinal.getSysId());
	                if(metaFilter == null)
	                {
	                    throw new Exception("Meta Filter with sysId " + filterDTOFinal.getSysId() + " does not exist.");
	                }
	                
	                //check for the duplicate name
	                MetaFilter duplicate = findMetaFilterByIdOrByName(null, filterDTOFinal.getName(), metaTable.getSys_id(), username);
	                if(duplicate != null && !duplicate.getSys_id().equalsIgnoreCase(metaFilter.getSys_id()))
	                {
	                    throw new Exception("'" + filterDTOFinal.getName() + "' already exist. Please try again.");
	                }
	            }
	            else
	            {
	                //insert
	                //check for the duplicate name
	                MetaFilter duplicate = findMetaFilterByIdOrByName(null, filterDTOFinal.getName(), metaTable.getSys_id(), username);
	                if(duplicate != null)
	                {
	                    throw new Exception("'" + filterDTOFinal.getName() + "' already exist. Please try again.");
	                }
	
	                //create a new one
	                metaFilter = new MetaFilter();
	            }
	                
	            metaFilter.setUName(filterDTOFinal.getName());
	            metaFilter.setUValue(filterDTOFinal.getValue());
	            metaFilter.setUIsGlobal(filterDTOFinal.getIsGlobal());
	            metaFilter.setUIsSelf(filterDTOFinal.getIsSelf());
	            metaFilter.setUIsRole(filterDTOFinal.getIsRole());
	            metaFilter.setUUserId(username);
	            
	            //save it immediately
	            HibernateUtil.getDAOFactory().getMetaFilterDAO().persist(metaFilter);
	            
	            metaFilter.setMetaTable(metaTable);
            
            });

            //reload the filter
            filterDTO = getFilter(filterDTO.getName(), filterDTO.getSysId(), username);
            
        }
        catch (Throwable t)
        {
           Log.log.error(t.getMessage(), t);
                     HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return filterDTO;
    }
    
    @SuppressWarnings("unchecked")
	public static List<MetaViewLookupVO> getViewLookupForApp(String appName, String username) throws Exception
    {
        List<MetaViewLookup> models = new ArrayList<MetaViewLookup>();
        List<MetaViewLookupVO> result = new ArrayList<MetaViewLookupVO>();

        if (StringUtils.isNotEmpty(appName))
        {
            MetaViewLookup example = new MetaViewLookup();
            example.setUAppName(appName.trim());

            try
            {
              HibernateProxy.setCurrentUser(username);
                List<MetaViewLookup> list = (List<MetaViewLookup>) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getMetaViewLookupDAO().find(example);
                });
                
                if (list != null)
                {
                    models.addAll(list);
                }

            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }

            if (models.size() > 0)
            {
                Collections.sort(models, new ComparatorMetaViewLookup());
                for(MetaViewLookup model : models)
                {
                    result.add(model.doGetVO());
                }
            }
        }

        return result;
    }
    
    public static boolean isTableExist(String tableName)
    {
//        boolean tableExist = false;
//        String selectSql = "select * from " + tableName + " where 1 = 0";
//        
//        try
//        {
//            ServiceJDBC.executeSQLSelect(selectSql, -1, -1);
//            
//            tableExist = true;
//        }
//        catch(Exception e)
//        {
//            tableExist = false;
//        }
//        
//        return tableExist;
        
        return CustomTableMappingUtil.modelExists(tableName);
    }
    
    //private apis
    private static CustomTable findCustomTableModelWithFields(String tableSysId, String tableName, String username) throws Exception
    {

        if (StringUtils.isNotEmpty(tableName))
        {
            
            try
            {
              HibernateProxy.setCurrentUser(username);
            	return (CustomTable) HibernateProxy.execute(() -> {
            		CustomTable table = null;

	                if (StringUtils.isNotBlank(tableSysId))
	                {
	                    // we have to do this as hibernate does not qry for ids
	                    table = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(tableSysId);
	                }
	                else if (StringUtils.isNotEmpty(tableName))
	                {
	                    //to make this case-insensitive
	                    //String sql = "from CustomTable where LOWER(UName) = '" + tableName.toLowerCase().trim() + "'" ;
	                    String sql = "from CustomTable where LOWER(UName) = :UName" ;
	                    
	                    Map<String, Object> queryParams = new HashMap<String, Object>();
	                    
	                    queryParams.put("UName", tableName.toLowerCase().trim());
	                    
	                    try
	                    {
	                        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
	                        if(list != null && list.size() > 0)
	                        {
	                            table = (CustomTable) list.get(0);
	                        }
	                    }
	                    catch (Exception e)
	                    {
	                        Log.log.error("Error while executing sql:" + sql, e);
	                    }
	                    
	                }
	                
	                if (table != null)
	                {
	                    //load the fields 
	                    if(table.getMetaFields() != null) 
	                        table.getMetaFields().size();
	                }

	                return table;
            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                throw new Exception(t);
            }
        }

        return null;

    }
    
    private static String getMetaTableViewRoles(MetaTableView currentTableView, RightTypeEnum rightType)
    {
        String roles = "";
        MetaAccessRights rights = currentTableView.getMetaAccessRights();

//        if(currentTableView.getParentMetaTableView() == null)
//        {
//            rights = currentTableView.getMetaAccessRights();
//        }
////        else
////        {
//        if(rights == null)
//        {
//            MetaTableView parent = currentTableView.getParentMetaTableView();
//            if(parent != null)
//            {
//                rights = parent.getMetaAccessRights();
//            }
//        }
//        }
        
        if(rights != null)
        {
            roles = rights.getUAdminAccess();
            if (RightTypeEnum.view == rightType)
            {
                roles = roles + "," + rights.getUReadAccess();
            }
            else if (RightTypeEnum.edit == rightType)
            {
                roles = roles + "," + rights.getUWriteAccess();
            }
        }
        
        return roles;
    }


    private static MetaTableView lookupMetaTableView(Collection<MetaTableView> views, String viewname)
    {
        MetaTableView viewResult = null;

        if (views != null && views.size() > 0)
        {
            for (MetaTableView view : views)
            {
                if (view.getUName().equalsIgnoreCase(viewname))
                {
                    viewResult = view;
                    break;
                }
            }
        }

        return viewResult;

    }

    private static MetaViewLookup getMetaViewLookup(String sysId)
    {
        MetaViewLookup result = null;

        if (StringUtils.isNotEmpty(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser("system");
                result = (MetaViewLookup) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getMetaViewLookupDAO().findById(sysId);
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return result;
    }
    
    private static MetaFilter findMetaFilterWithReferences(String filterName, String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (MetaFilter) HibernateProxy.execute(() -> {
        		MetaFilter result = null;
        		MetaFilter query = null;
        		
        		if (StringUtils.isNotEmpty(filterName))
        		{
        			query = new MetaFilter();
        			query.setUName(filterName);
        		}
	
	            MetaFilterDAO daoMetaFormView = HibernateUtil.getDAOFactory().getMetaFilterDAO();
	            if (StringUtils.isNotEmpty(sysId))
	            {
	                result = daoMetaFormView.findById(sysId);
	            }
	            else
	            {
	                result = daoMetaFormView.findFirst(query);
	            }
	
	            if (result != null)
	            {
	                // load references
	                result.getMetaTable().getSys_id();
	            }
	            
	            return result;
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
            throw new Exception(t);
        }
    }
    
    private static RsMetaFilterDTO convertMetaFilterToModel(MetaFilter metaFilter)
    {
        RsMetaFilterDTO filter = null;
        
        if(metaFilter != null)
        {
            filter = new RsMetaFilterDTO();
            filter.setSysId(metaFilter.getSys_id());
            filter.setName(metaFilter.getUName());
            filter.setValue(metaFilter.getUValue());
            filter.setIsGlobal(metaFilter.ugetUIsGlobal());
            filter.setIsSelf(metaFilter.ugetUIsSelf());
            filter.setIsRole(metaFilter.ugetUIsRole());
            filter.setUserId(metaFilter.getUUserId());
            
            filter.setMetaTableSysId(metaFilter.getMetaTable().getSys_id());
            filter.setMetaTableName(metaFilter.getMetaTable().getUName());
        }
        
        return filter;
    }
    
    private static List<MetaFilter> getAllFilters(String tablename, String username)  throws Exception
    {
        List<MetaFilter> list = new ArrayList<MetaFilter>();

        if (StringUtils.isNotEmpty(tablename) && StringUtils.isNotEmpty(username))
        {
            MetaTable example = new MetaTable();
            example.setUName(tablename);

            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
                    MetaTable table = HibernateUtil.getDAOFactory().getMetaTableDAO().findFirst(example);
                    if (table != null)
                    {
                        Collection<MetaFilter> filters = table.getMetaFilters();
                        if (filters != null && filters.size() > 0)
                        {
                            for (MetaFilter filter : filters)
                            {
//                                if (StringUtils.isNotEmpty(filter.getUUserId()) && filter.getUUserId().equalsIgnoreCase(username))
//                                {
                                    filter.getMetaTable().getSys_id();
                                    list.add(filter);
//                                }
                            }
                        }
                    }

            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return list;
    }
    
    private static RsMetaFilterDTO getFilter(String filterName, String sysId, String username) throws Exception
    {
        if(StringUtils.isEmpty(filterName) && StringUtils.isEmpty(sysId))
        {
            throw new RuntimeException("SysId or Name is mandatory to look up filter");
        }

        return convertMetaFilterToModel(findMetaFilterWithReferences(filterName, sysId, username));
    }
    
    private static class ComparatorMetaViewLookup implements Comparator<MetaViewLookup>
    {
        public final int compare(MetaViewLookup a, MetaViewLookup b)
        {
            int aValue = a.getUOrder() != null ? a.getUOrder().intValue() : 0;
            int bValue = b.getUOrder() != null ? b.getUOrder().intValue() : 0;

            return aValue - bValue;
        } // end compare

    } // ComparatorBaseModel

    private static MetaTable findMetaTableByIdOrByName(String sysId, String name, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            return (MetaTable) HibernateProxy.execute(() -> {
            	MetaTable result = null;

	            if (StringUtils.isNotBlank(sysId))
	            {
	                // we have to do this as hibernate does not qry for ids
	                result = HibernateUtil.getDAOFactory().getMetaTableDAO().findById(sysId);
	            }
	            else if (StringUtils.isNotEmpty(name))
	            {
	                //to make this case-insensitive
	                //String sql = "from MetaTable where LOWER(UName) = '" + name.toLowerCase().trim() + "'";
	                String sql = "from MetaTable where LOWER(UName) = :UName";
	                
	                Map<String, Object> queryParams = new HashMap<String, Object>();
	                
	                queryParams.put("UName", name.toLowerCase().trim());
	                
	                try
	                {
	                    List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
	                    if (list != null && list.size() > 0)
	                    {
	                        result = (MetaTable) list.get(0);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Error while executing sql:" + sql, e);
	                }
	            }

	            return result;
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return null;
    }

    private static MetaFilter findMetaFilterByIdOrByName(String sysId, String name, String metaTableSysId, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		MetaFilter result = null;
	
	            if (StringUtils.isNotBlank(sysId))
	            {
	                // we have to do this as hibernate does not qry for ids
	                result = HibernateUtil.getDAOFactory().getMetaFilterDAO().findById(sysId);
	            }
	            else if (StringUtils.isNotEmpty(name))
	            {
	                //to make this case-insensitive
	                //String sql = "from MetaFilter where LOWER(UName) = '" + name.toLowerCase().trim() + "' and metaTable.sys_id = '" + metaTableSysId + "' ";
	                String sql = "from MetaFilter where LOWER(UName) = :UName and metaTable.sys_id = :sys_id ";
	                
	                Map<String, Object> queryParams = new HashMap<String, Object>();
	                
	                queryParams.put("UName", name.toLowerCase().trim());
	                queryParams.put("sys_id", metaTableSysId);
	                
	                try
	                {
	                    List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
	                    if (list != null && list.size() > 0)
	                    {
	                        result = (MetaFilter) list.get(0);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Error while executing sql:" + sql, e);
	                }
	
	            }
	            
	            return result;
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return null;
    }
    
    public static void updateAllCTWithChecksum()
    {
        try {
            UpdateChecksumUtil.updateComponentChecksum("CustomTable", CustomTableUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
            
    public static Integer calculateChecksum(CustomTable table)
    {
        int checksum = 0;
        if (table != null) {
            String schema = table.getUSchemaDefinition();
            if (StringUtils.isNotBlank(schema)) {
                schema = schema.replaceAll(ImpexEnum.REMOVE_SYS_COLUMNS_REGEX.getValue(), "");
                checksum = Objects.hash(schema);
                if (Log.log.isDebugEnabled())
                    Log.log.debug(String.format("CT '%s' schema for checksum: %s ", table.getUName(), schema));
            }
        }
        return checksum;
    }
    
    public static Integer getCustomTableChecksum (String name, String username)
    {
        Integer checksum = null;
        
        try {
            CustomTable table = findCustomTableModelWithFields(null, name, username);
            if (table != null)
                checksum = table.getChecksum();
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return checksum;
    }
}
