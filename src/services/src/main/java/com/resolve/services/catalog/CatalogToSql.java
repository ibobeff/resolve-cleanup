/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.catalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogVO;

/**
 * 
 * util class that converts the UI Tree Catalog struture into records to be inserted in DB
 * 
 * @author jeet.marwah
 *
 */
public class CatalogToSql
{
//    private int count = 1;
    private Catalog catalog = null;
    private List<ResolveCatalogNodeVO> sqlVOs = new ArrayList<ResolveCatalogNodeVO>();
    
    public CatalogToSql(Catalog catalog)
    {
        this.catalog = catalog;
    }

    public List<ResolveCatalogNodeVO> generateSqlVOs()
    {
        //build the catalog hierarchy
        build(catalog, 1);

        return sqlVOs;
    }

    private void build(Catalog catalog, int depth)
    {
        ResolveCatalogNodeVO vo = transformCatalogToVO(catalog);
        
        //always recalculate the values
//        vo.setULft(count++);
//        vo.setUDepth(depth);
            
        if(catalog.getType().equalsIgnoreCase("CatalogReference") && !catalog.isRoot())
        {
            //skip the build of 'children' for reference types
        }
        else
        {
            if (catalog.getChildren() != null)
            {
                for (Catalog child : catalog.getChildren())
                {
                    build(child, depth + 1);
                }
            }
        }

//        vo.setURgt(count++);
        sqlVOs.add(vo);
    }
    
    private ResolveCatalogNodeVO transformCatalogToVO(Catalog catalog)
    {
        ResolveCatalogNodeVO vo = new ResolveCatalogNodeVO();
        vo.setSys_id(catalog.getId());
        vo.setSysCreatedBy(catalog.getSys_created_by());
        vo.setSysUpdatedBy(catalog.getSys_updated_by());
        vo.setSysCreatedOn(new Date(catalog.getSys_created_on()));
        vo.setSysUpdatedOn(new Date(catalog.getSys_updated_on()));
        
        vo.setUName(catalog.getName());
        vo.setUType(catalog.getType());
        vo.setUOperation(catalog.getOperation());
//        vo.setUOrder(catalog.getOrder());
        vo.setURoles(catalog.getRoles());
        vo.setUEditRoles(catalog.getEditRoles());
        vo.setUTitle(catalog.getTitle());
        vo.setUDescription(catalog.getDescription());
        vo.setUImage(catalog.getImage());
        vo.setUImageName(catalog.getImageName());
        vo.setUTooltip(catalog.getTooltip());
        vo.setUWiki(catalog.getWiki());
        vo.setULink(catalog.getLink());
        vo.setUForm(catalog.getForm());
        vo.setUDisplayType(catalog.getDisplayType());
        vo.setUMaxImageWidth(catalog.getMaxImageWidth());
        vo.setUPath(catalog.getPath());
        vo.setUOpenInNewTab(catalog.isOpenInNewTab());
        vo.setUSideWidth(catalog.getSideWidth());
        vo.setUCatalogType(catalog.getCatalogType());
        
        vo.setUWikidocSysID(catalog.getWikidocSysID());
        vo.setUInternalName(catalog.getInternalName());
        vo.setUIcon(catalog.getIcon());
        
        vo.setUNamespace(catalog.getNamespace());
        vo.setUIsRoot(catalog.isRoot());
        vo.setUIsRootRef(catalog.isRootRef());
        
        //tags
        vo.setTags(catalog.getTags());
        
        if(catalog.getType().equalsIgnoreCase("CatalogReference") && !catalog.isRoot())
        {
            //this is reference, so get the refeerence sysId and populate
            ResolveCatalogVO refCat = CatalogUtil.findCatalog(null, vo.getUName(), "admin");
            if(refCat != null)
            {
                vo.setUReferenceCatalogSysId(refCat.getSys_id());
            }
        }
        
        
        
        return vo;
        
    }

}
