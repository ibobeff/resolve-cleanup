/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.dao.EWSAddressDAO;
import com.resolve.persistence.model.EWSAddress;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class EWSGatewayUtil extends GatewayUtil
{
    private static volatile EWSGatewayUtil instance = null;
    public static EWSGatewayUtil getInstance()
    {
        if(instance == null)
        {
            instance = new EWSGatewayUtil();
        }
        return instance;
    }

    @SuppressWarnings("rawtypes")
    public void setEWSAddresses(Map ewsAddresses)
    {
        try
        {
			HibernateProxy.execute(() -> {
				EWSAddressDAO addressDao = HibernateUtil.getDAOFactory().getEWSAddressDAO();

				String queue = deleteByQueue(EWSAddress.class.getSimpleName(), ewsAddresses);

				// update each filter
				for (Object filterMapObj : ewsAddresses.values()) {
					String filterStringMap = (String) filterMapObj;
					Map filterMap = StringUtils.stringToMap(filterStringMap);

					String ewsAddress = (String) filterMap.get("EWSADDRESS");
					String ewsPassword = (String) filterMap.get("EWSPASSWORD");
					String sysModifiedBy = (String) filterMap.get(Constants.SYS_UPDATED_BY);

					// init record entry with values
					EWSAddress row = new EWSAddress();
					row.setUEWSAddress(ewsAddress);
					row.setUEWSPassword(ewsPassword);
					row.setUQueue(queue);
					row.setSysCreatedBy(sysModifiedBy);
					row.setSysUpdatedBy(sysModifiedBy);

					addressDao.save(row);
				}
			});

		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
    } // setEWSAddresses

    @SuppressWarnings("unchecked")
    public List<Map<String, String>> getEWSAddresses(String queueName, boolean isSocialPoster)
    {
        List<Map<String, String>> ewsAddresses = new ArrayList<Map<String,String>>();

        try
        {
        	HibernateProxy.execute(() -> {
                List<EWSAddress> addressList = HibernateUtil.createQuery("select o from EWSAddress o where UQueue='" + queueName + "'").list();

                for (EWSAddress filter : addressList)
                {
                    Map<String, String> addressMap = new HashMap<String, String>();
                    addressMap.put("EWSADDRESS", filter.getUEWSAddress());
                    addressMap.put("EWSPASSWORD", filter.getUEWSPassword());
                    addressMap.put("QUEUE", filter.getUQueue());
                    addressMap.put(Constants.SYS_UPDATED_BY, filter.getSysUpdatedBy());

                    ewsAddresses.add(addressMap);
                }

                //get EWS address from process, forums, team etc.
                if(isSocialPoster)
                {
                    List<Map<String, Object>> addresses = getCustomTableData("select * from social_process where u_is_active=1 and u_receive_from is not null");
                    addresses.addAll(getCustomTableData("select * from social_forum where u_is_active=1 and u_receive_from is not null"));
                    addresses.addAll(getCustomTableData("select * from social_team where u_is_active=1 and u_receive_from is not null"));

                    for(Map<String, Object> address : addresses)
                    {
                        String ewsAddress = (String) address.get("u_receive_from");
                        if(StringUtils.isNotBlank(ewsAddress))
                        {
                            Map<String, String> addressMap = new HashMap<String, String>();
                            addressMap.put("EWSADDRESS", ewsAddress);
                            addressMap.put("EWSPASSWORD", (String) address.get("u_receive_pwd"));
                            addressMap.put("QUEUE", queueName);
                            addressMap.put(Constants.SYS_UPDATED_BY, (String) address.get("sys_updated_by"));
                            ewsAddresses.add(addressMap);
                        }
                    }
                }

        	});
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return ewsAddresses;
    } // getEWSAddresses
}
