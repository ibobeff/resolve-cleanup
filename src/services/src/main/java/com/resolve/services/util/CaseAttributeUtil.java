package com.resolve.services.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.resolve.persistence.model.CaseAttribute;
import com.resolve.persistence.model.CaseValue;
import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.CaseAttributeVO;
import com.resolve.services.hibernate.vo.CaseValueVO;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CaseAttributeUtil {
	private final static int MAX_CASE_ATTRIBUTE_COUNT = 10;
	private static final String ERROR_INCIDENT_NOT_FOUND = "Incident not found";
	private static final String ERROR_UNKNOWN_COLUMN_VALUE = "Unknown column value";
	private static final String ERROR_COLUMN_VALUE_IS_NULL = "Column value is null or empty";
	private static final String ERROR_CASE_ATTRIBUTE_NOT_FOUND = "Case attribute not found";
	private static final String ERROR_GET_CASE_ATTRIBUTE = "Failed to get case attributes";
	private static final String ERROR_GET_CASE_ATTRIBUTES = "Failed to get case attributes";
	private static final String ERROR_COLUMN_NAME_NULL_OR_EMPTY = "Column name is null or empty";
	private static final String ERROR_SAVE_CASE_ATTRIBUTES = "Failed to save SIR case attributes, sir=%s";
	private static final String ERROR_ONE_DEFAULT_VALUE_ALLOWED = "Only one default value is allowed";
	private static final String ERROR_CASE_ATTRIBUTES_COUNT_EXCEEDED = "You exceeded the number of allowed case attributes";

	public static List<CaseAttributeVO> getCaseAttributes(String username) throws Exception {
		List<CaseAttributeVO> caseVOs = new ArrayList<>();

		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CaseAttribute> criteriaQuery = criteriaBuilder.createQuery(CaseAttribute.class);					
				Root<CaseAttribute> from = criteriaQuery.from(CaseAttribute.class);
				criteriaQuery.select(from).orderBy(criteriaBuilder.desc(from.get("sysUpdatedOn")));
				
				List<CaseAttribute> result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();
				for (CaseAttribute ca : result) {
					caseVOs.add(ca.doGetVO());
				}
			});
		} catch (Throwable t) {
			Log.log.error(ERROR_GET_CASE_ATTRIBUTES, t);

			throw t;
		}
		return caseVOs;
	}

	public static CaseAttributeVO saveCaseAttribute(CaseAttributeVO voIn, String username) throws Exception {
		List<CaseAttributeVO> savedCaseAttributes = getCaseAttributes(username);
	
		CaseAttributeVO vo = null;
		try {
        HibernateProxy.setCurrentUser(username);
			vo = ((CaseAttribute) HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CaseAttribute> criteriaQuery = criteriaBuilder.createQuery(CaseAttribute.class);					
				Root<CaseAttribute> from = criteriaQuery.from(CaseAttribute.class);
	
				criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("columnName"), voIn.getColumnName()));
				
				CaseAttribute oldCaseAttribute = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();
				
				if (oldCaseAttribute == null) {
					if (savedCaseAttributes != null && savedCaseAttributes.size() >= MAX_CASE_ATTRIBUTE_COUNT) {
						throw new Exception(ERROR_CASE_ATTRIBUTES_COUNT_EXCEEDED);
					}
					oldCaseAttribute = new CaseAttribute();
				}
				
				oldCaseAttribute = fillCaseAttribute(oldCaseAttribute, voIn);
				
				return HibernateUtil.getDAOFactory().getCaseAttributeDAO().insertOrUpdate(oldCaseAttribute);
			})).doGetVO();

		} catch (Throwable t) {
			Log.log.error(ERROR_SAVE_CASE_ATTRIBUTES, t);
			throw t;
		}
		
		return vo;
	}

	public static boolean deleteCaseAttribute(String columnName, String username) throws Exception {
		
		try {
			if (StringUtils.isBlank(columnName)) {
				throw new IllegalArgumentException(ERROR_COLUMN_NAME_NULL_OR_EMPTY);
			}

			HibernateProxy.execute(() -> {

				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CaseAttribute> criteriaQuery = criteriaBuilder.createQuery(CaseAttribute.class);					
				Root<CaseAttribute> from = criteriaQuery.from(CaseAttribute.class);
				criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("columnName"), columnName));
				
				CaseAttribute oldCase = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();
	
				if (oldCase == null) {
					throw new IllegalArgumentException(ERROR_CASE_ATTRIBUTE_NOT_FOUND);
				}
				
				HibernateUtil.getDAOFactory().getCaseAttributeDAO().delete(oldCase);
			});

			return true;
		} catch (Exception t) {
			Log.log.error(t.getMessage(), t);
			throw t;
		}
	}

	public static CaseAttributeVO getCaseAttribute(String columnName, String username) throws Exception {
		if (StringUtils.isBlank(columnName)) {
			throw new Exception(ERROR_COLUMN_NAME_NULL_OR_EMPTY);
		}

		try {
        HibernateProxy.setCurrentUser(username);
			return (CaseAttributeVO) HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CaseAttribute> criteriaQuery = criteriaBuilder.createQuery(CaseAttribute.class);					
				Root<CaseAttribute> from = criteriaQuery.from(CaseAttribute.class);

				criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("columnName"), columnName));
				
				CaseAttribute oldCaseAttribute = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();
				CaseAttributeVO result = null;
				if (oldCaseAttribute != null) {
					result = oldCaseAttribute.doGetVO();
				}
				
				return result;
			});			
		} catch (Throwable t) {
			Log.log.error(ERROR_GET_CASE_ATTRIBUTE, t);

			throw t;
		}
	}

	public static ResolveSecurityIncidentVO addIcidentAttribute(String incidentId, String columnName, String columnValue, 
																String orgId, String username) throws Exception {
		
		PlaybookUtils.checkUsersSIROrgAccess(orgId, username);
		
		CaseAttributeVO caseAttribute = getCaseAttribute(columnName, username);
	
		if (caseAttribute == null) {
			throw new Exception(ERROR_CASE_ATTRIBUTE_NOT_FOUND);
		}
		
		if (StringUtils.isBlank(columnValue)) {
			throw new Exception(ERROR_COLUMN_VALUE_IS_NULL);
		}
		
		boolean validValue = caseAttribute.getValueRange().stream().anyMatch(p -> columnValue.equalsIgnoreCase(p.getValue()));
		
		if (!validValue) {
			throw new Exception(ERROR_UNKNOWN_COLUMN_VALUE);
		}
		
		ResolveSecurityIncidentVO incident = PlaybookUtils.getSecurityIncident(incidentId, null, username);

		if (incident == null) {
			throw new Exception(ERROR_INCIDENT_NOT_FOUND);
		}

		ResolveSecurityIncidentVO result = incident;
		
		String msgObj = String.format("case attribute %s value %s to SIR %s", columnName, columnValue, incident.getSir());
		
		PlaybookUtils.v2LicenseViolationsAndEnvCheck(msgObj);
		
		boolean columnPresent = incident.getCaseAttributes().keySet().stream().anyMatch(p -> columnName.equals(p));
		
		// update if either columnName is not present or columnValue is different
		if (!columnPresent || !columnValue.equalsIgnoreCase(incident.getCaseAttributes().get(columnName))) {
			incident.getCaseAttributes().put(columnName, columnValue);
			
			try {
         HibernateProxy.setCurrentUser(username);
				result = (ResolveSecurityIncidentVO) HibernateProxy.execute(() -> {

					ResolveSecurityIncident entity = new ResolveSecurityIncident();
					entity.applyVOToModel(incident);
					ResolveSecurityIncident updated = HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().insertOrUpdate(entity);
				
					return updated.doGetVO();

				});
			} catch (Throwable t) {
				Log.log.error(ERROR_SAVE_CASE_ATTRIBUTES, t);

				throw t;
			}
		}
		
		return result;
	}

	private static CaseAttribute fillCaseAttribute(CaseAttribute oldCaseAttribute, CaseAttributeVO vo) throws Exception {
		oldCaseAttribute.setActive(vo.getActive());
		oldCaseAttribute.setColumnName(vo.getColumnName());
		oldCaseAttribute.setDescription(vo.getDescription());
		oldCaseAttribute.setDisplayName(vo.getDisplayName());
		oldCaseAttribute.setShowOnDashboard(vo.getShowOnDashboard());
		oldCaseAttribute.setShowOnIncident(vo.getShowOnIncident());
		oldCaseAttribute.setUseAsFilter(vo.getUseAsFilter());
		
		if (vo.getValueRange() != null) {
			oldCaseAttribute.getValueRange().clear();
			boolean defaultOn = false;
			for (CaseValueVO valueVO : vo.getValueRange()) {
				if (defaultOn && valueVO.getDefaultValue()) {
					throw new Exception(ERROR_ONE_DEFAULT_VALUE_ALLOWED);
				}
				defaultOn = valueVO.getDefaultValue();
				oldCaseAttribute.getValueRange().add(new CaseValue(valueVO.getValue(), valueVO.getDefaultValue()));
			}
		}
		
		return oldCaseAttribute;
	}
}
