/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.constants;

/**
 * this matches with the UI types
 * 
 * @author jeet.marwah
 *
 */

public enum CustomFormUIType
{

    //old field types
    TextField,
    TextArea,
    RadioButton,
    NumberTextField,
    DecimalTextField,
    MultipleCheckBox,
    ComboBox,
    Date,
    Timestamp,
    CheckBox,
    Password,
    Link,
    List,
    DateTime,
    
    //new field types
    NameField,
    AddressField,
    EmailField,
    MACAddressField,
    IPAddressField,
    CIDRAddressField,
    PhoneField,
//    ReadOnlyTextField,
    UserPickerField,
    TeamPickerField,
    MultiSelectComboBoxField,
    WysiwygField,
    
    //DB type only
    Reference,
    Sequence,
    Journal,
    
    //Input type only
    ButtonField,
    FileUploadField,
    ReferenceTableField,
    
    //Spacer for visual spacing, no input or anything like that
    SpacerField,
    
    //Tab Container field for reference and upload
    TabContainerField,
    
    UrlContainerField,
    
    SectionContainerField,
    LabelField
    
}
