/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.vo.UserWorksheetData;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CacheUtils
{
    private static Map<String, UserWorksheetData> WORKSHEET_DATA_CACHE = new ConcurrentHashMap<String, UserWorksheetData>();
    
    //api for the worksheet data cache
    public static UserWorksheetData getWorksheetDataFromCache(String problemId)
    {
        return WORKSHEET_DATA_CACHE.get(problemId);
    }
    
    public static void putWorksheetDataFromCache(String problemId, UserWorksheetData userInfo)
    {
        WORKSHEET_DATA_CACHE.put(problemId, userInfo);
    }
    
    public static boolean hasProblemIdInWorksheetDataCache(String problemId)
    {
        boolean hasProblemId = false;
        
        if(StringUtils.isNotBlank(problemId))
        {
            hasProblemId = WORKSHEET_DATA_CACHE.containsKey(problemId);
        }
        
        return hasProblemId;
    }
    
    public static void removeProblemIdFromWorksheetDataCacahe(String problemId)
    {
        if(StringUtils.isNotEmpty(problemId))
        {
            WORKSHEET_DATA_CACHE.remove(problemId);
        }
    }
    
    public static void cleanupWorksheetDataCache()
    {
        try
        {
            Iterator<String> it = WORKSHEET_DATA_CACHE.keySet().iterator();
            while (it.hasNext())
            {
                String key = it.next();
                UserWorksheetData userInfo = WORKSHEET_DATA_CACHE.get(key);

                long calculatedTime = userInfo.lastQueryTime + 120000;
                long currTime = System.currentTimeMillis();

                if (currTime > calculatedTime)
                {
                    // remove this item
                    WORKSHEET_DATA_CACHE.remove(key);
                }
            }// end of while loop
        }
        catch (Exception e)
        {
            Log.log.info("Worksheet Cache Cleanup Exception. Can be ignored, will try again later.", e);
        }
    }
    
    ///end of worksheet cache
    
    //users cache
    
    //end
        
    //caching the field names of Model that we can use to query 
//    @SuppressWarnings("rawtypes")
//    public static Map<String, FieldMetaData> getFieldNamesForModel(String clazzName)
//    {
//        Map<String, FieldMetaData> result = new HashMap<String, FieldMetaData>();
//        
//        if(StringUtils.isNotBlank(clazzName))
//        {
//            try
//            {
//                //if it exist, return that
//                if(MODEL_FIELDS_CACHE.containsKey(clazzName))
//                {
//                    result.putAll(MODEL_FIELDS_CACHE.get(clazzName));
//                }
//                else //else create it and save it for the next iteration also
//                {
//                    Class clazz = Class.forName(clazzName);
//                    Field[] fields = clazz.getDeclaredFields();
//                    for(Field f : fields)
//                    {
//                        String fieldName = f.getName();
//                        String type = f.getType().getName();
//                        
//                        FieldMetaData metadata = new FieldMetaData();
//                        metadata.setFieldName(fieldName);
//                        metadata.setDatatype(type);
//                        
//                        //map this to QueryFilter.getParameters
//                        if(f.getType().equals(String.class))
//                        {
//                            metadata.setUiType("string");
//                        }
//                        else if(f.getType().equals(Boolean.class))
//                        {
//                            metadata.setUiType("bool");
//                        }
//                        else if(f.getType().equals(Float.class))
//                        {
//                            metadata.setUiType("float");
//                        }
//                        else if(f.getType().equals(Date.class) || f.getType().equals(Timestamp.class) )
//                        {
//                            metadata.setUiType("date");
//                        }
//                        else
//                        {
//                            metadata.setUiType("auto");
//                        }
//                        
//                        result.put(fieldName.toLowerCase(), metadata);
//                    }
//                    
//                    MODEL_FIELDS_CACHE.put(clazzName, result);
//                }
//                
//            }
//            catch (ClassNotFoundException e)
//            {
//                Log.log.error("Class " + clazzName + " not found.");
//            }
//        }
//
//        return result;
//    }
    
    //end 
    

    
    
} 
