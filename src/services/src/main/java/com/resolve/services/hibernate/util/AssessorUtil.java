/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.Query;
import org.hibernate.type.StringType;

import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveAssess;

import com.resolve.persistence.util.HibernateProxy;import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.actiontask.FindAssessor;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;


public class AssessorUtil
{
    
    public static List<ResolveAssessVO> getResolveAssess(QueryDTO query, String username)
    {
        List<ResolveAssessVO> result = new ArrayList<ResolveAssessVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveAssess instance = new ResolveAssess();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ResolveAssess instance = (ResolveAssess) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static Collection<ResolveAssessVO> findResolveAssessByNames(Set<String> assessorNames, String username)
    {
        Collection<ResolveAssessVO> result = new ArrayList<ResolveAssessVO>();
        
        if (assessorNames != null && assessorNames.size() > 0)
        {
            //prepare list of actiontasks based on invoc sysIds
//            String sql = "select sys_id,UName,UDescription,sysUpdatedOn from ResolveAssess where UName IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(assessorNames)) + ")";
            QueryDTO query = new QueryDTO();
            try
            {
                query.setModelName("ResolveAssess");
                query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");
                query.setWhereClause("UName IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(assessorNames)) + ")");
                
                result.addAll(getResolveAssess(query, username));

                //            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql);
                //            for (Object o : list)
                //            {
                //                ResolveAssess instance = (ResolveAssess) o;
                //                result.add(instance.doGetVO());
                //            }
            }
            catch (Exception e)
            {
                Log.log.error("error in sql " + query.getSelectHQL(), e);
            }
        }
        return result;
        
    }
    
    public static ResolveAssessVO findResolveAssessNoCache(String sysId, String name, String username)
    {
        ResolveAssessVO result = null;

        try 
        {
          HibernateProxy.setCurrentUser(username);
            result = (ResolveAssessVO) HibernateProxy.executeNoCache(() -> {
            	return findResolveAssess(sysId, name, username);
            });
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }
    
    public static ResolveAssessVO findResolveAssess(String sysId, String name, String username)
    {
        ResolveAssessVO result = null;

        try
        {
            FindAssessor findAssessor = new FindAssessor(sysId, name, username);
            result = findAssessor.get();
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    public static ResolveAssessVO findResolveAssessWithoutRefs(String sysId, String name, String username)
    {
        ResolveAssessVO result = null;

        try
        {
            FindAssessor findAssessor = new FindAssessor(sysId, name, username);
            result = findAssessor.getWithoutRefs();
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }
    
    
    public static ResolveAssessVO getDefaultResolveAssess()
    {
        String defaultAssessorName = "DEFAULT#resolve";
        
        return findResolveAssess(null, defaultAssessorName, "system");
    }
    
    public static String getAssessorScript(String assessName, String username) throws Exception
    {
        String result = null;

        if (StringUtils.isNotBlank(assessName))
        {
            try
            {
                
                ResolveAssessVO assess = findResolveAssess(null, assessName, username);
                if(assess != null)
                {
                    result = assess.getUScript();
                    if (StringUtils.isEmpty(result))
                    {
                        throw new Exception("FAILED: Missing script content - assessor: " + assessName);
                    }
                }
                else
                {
                    throw new Exception("FAILED: Unable to find assessor: " + assessName);
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    } // getScript
    
    @SuppressWarnings("unchecked")
    public static Set<String> findActiontaskReferencesForAssessors(Set<String> assessorSysIds, String username)   throws Exception
    {
        Set<String> result = new HashSet<String>();
        
        if(assessorSysIds != null && assessorSysIds.size() > 0)
        {
            //String qrySysIds = SQLUtils.prepareQueryString(new ArrayList<String>(assessorSysIds));
            //String sql = "select UFullName from ResolveActionTask where resolveActionInvoc.sys_id in (select sys_id from ResolveActionInvoc where assess.sys_id IN ("+ qrySysIds +"))";
            String sql = "select UFullName from ResolveActionTask where resolveActionInvoc.sys_id in (select sys_id from ResolveActionInvoc where assess.sys_id IN (:qrySysIds))";
            
            Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
            
            queryInParams.put("qrySysIds", new ArrayList<Object>(assessorSysIds));
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectInOnly(sql, queryInParams);
                result.addAll((List<String>) list);
            }
            catch(Exception e)
            {
                Log.log.error("error in sql " + sql, e);
            }
        }
        
        return result;
    }
    
    public static void deleteResolveAssessByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {

        if (deleteAll)
        {
            GeneralHibernateUtil.purgeDBTable("ResolveAssess", username);
            
            //delete all the archives related to this table
            ResolveArchiveUtils.purgeArchive("ResolveAssess", "all", username);
        }
        else
        {
            if (sysIds != null)
            {
                //can be used as its 1 table only
            	ServiceHibernate.deleteDBRow(username, "ResolveAssess", (List<String>) Arrays.asList(sysIds), false);
                
                //delete all the archives related to these sysIds
                String csvSysIds = StringUtils.convertCollectionToString(Arrays.asList(sysIds).iterator(), ",");
                ResolveArchiveUtils.submitPurgeArchiveRequest("ResolveAssess", csvSysIds, username);
            }
        }
    }
    
    public static ResolveAssessVO saveResolveAssess(ResolveAssessVO vo, String username) throws Exception
    {
        if(vo != null && vo.validate())
        {
            ResolveAssess model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findResolveAssessModel(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("Assessor with sysId " + vo.getSys_id() + " does not exist.");
                }
            }
            else
            {
                model = findResolveAssessModel(null, vo.getUName(), username);
                if(model != null)
                {
                    throw new Exception("Assessor with name " + vo.getUName() + " already exist");
                }
                
                model = new ResolveAssess();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveResolveAssess(model, username);
            
            //submit a request to archive it
            ResolveArchiveUtils.submitArchiveRequest("ResolveAssess", "UScript", model.getSys_id(), username);
            
            vo = model.doGetVO();
            
            HibernateUtil.clearDBCacheRegion(DBCacheRegionConstants.ASSESSOR_CACHE_REGION, true, model.getSys_id());
        }
        
        return vo;
    } // saveResolveAssess()
    
    //private apis
    private static ResolveAssess findResolveAssessModel(String sysId, String name, String username)
    {
        ResolveAssess result = null;

        if(StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	result = (ResolveAssess) HibernateProxy.execute(() -> {
    
            		return HibernateUtil.getDAOFactory().getResolveAssessDAO().findById(sysId);
    
            	});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(name))
        {
            //to make this case-insensitive
            //String sql = "select a from ResolveAssess a where LOWER(a.UName) = '" + name.toLowerCase().trim() + "'" ;
            String sql = "select a from ResolveAssess a where LOWER(a.UName) = :UName" ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", name.toLowerCase().trim());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (ResolveAssess) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }
        }
        
        return result;
        
    }
    
    public static void copyAssessor(ResolveActionTaskVO taskVO, ResolveAssessVO assessVO, String username) {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
            
	            ResolveActionInvocVO actionInvocVO = taskVO.getResolveActionInvoc();
	            String taskFullName = taskVO.getUFullName();
	            String newName = taskFullName + System.nanoTime();
	            
	            ResolveAssessVO assess = actionInvocVO.getAssess();
	            
	            if(assess != null) {
	                assess.setSys_id(null);
	                assess.setUName(newName);
	                Log.log.debug("new assess name = " + newName);
	                
	                ResolveAssess model = new ResolveAssess();
	                model.applyVOToModel(assess);
	                HibernateUtil.getDAOFactory().getResolveAssessDAO().persist(model);
	                assess = model.doGetVO();
	                Log.log.debug("new assess id = " + assess.getSys_id());
	                actionInvocVO.setAssess(assess);
	            }
	    
	            ResolveActionInvoc actionInvoc = new ResolveActionInvoc(actionInvocVO);
	            HibernateUtil.getDAOFactory().getResolveActionInvocDAO().update(actionInvoc);
            
        	});
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
    } // copyAssessor
    
    public static void replaceResolveAssess(ResolveAssessVO toDelete, ResolveAssessVO renameTo, String username) {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		 ResolveAssess model = new ResolveAssess();
                 model.applyVOToModel(toDelete);
                 HibernateUtil.getDAOFactory().getResolveAssessDAO().delete(model);
        	});
            
            HibernateProxy.execute(() -> {
            	 ResolveAssess m = new ResolveAssess();
                 m.applyVOToModel(renameTo);
                 HibernateUtil.getDAOFactory().getResolveAssessDAO().persist(m);
            });
            
           
            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
    } // replaceResolveAssess
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static void updateSummaryAndDetailFields()
    {
    	String queryHql = "select sys_id, summaryFormat, detailFormat from ResolveAssess";
    	String updateHql = "update ResolveAssess set summaryRule = :summaryRule, detailRule = :detailRule where sys_id = :sysId";
    	
    	List<Object> assessList = null; 
    	
    	try
    	{
       HibernateProxy.setCurrentUser("admin");
    		assessList = (List<Object>) HibernateProxy.execute(() -> HibernateUtil.createQuery(queryHql).list());
    	}
    	catch(Throwable t)
    	{
    		Log.log.error("Error while getting ResolveAssess list.", t);
             HibernateUtil.rethrowNestedTransaction(t);
    	}
    	
    	if (assessList != null && assessList.size() > 0)
    	{
    		final String TEMPLATE = "[{\"severity\":\"ANY\",\"condition\":\"ANY\",\"display\":\"REPLACE\"}]";
    		boolean update = false;
    		
    		for (Object obj : assessList)
    		{
    			String sysId = (String)((Object[])obj)[0];
    			String summaryFormat = (String)((Object[])obj)[1];
    			String detailFormat = (String)((Object[])obj)[2];
    			
    			if (StringUtils.isBlank(summaryFormat) && StringUtils.isBlank(detailFormat))
    			{
    				continue;
    			}
	    		
    			update = false;
    			
    			if (StringUtils.isNotBlank(summaryFormat))
    			{
    				summaryFormat = TEMPLATE.replace("REPLACE", summaryFormat);
    				update = true;
    			}
    			
    			if (StringUtils.isNotBlank(detailFormat))
    			{
    				detailFormat = TEMPLATE.replace("REPLACE", detailFormat);
    				update = true;
    			}
    			
    			if (update)
    			{
    				try
    				{
    					String summaryFormatFinal = summaryFormat;
    	    			String detailFormatFinal = detailFormat;
          HibernateProxy.setCurrentUser("admin");
    					HibernateProxy.execute(() -> {
	    					Query query = HibernateUtil.createQuery(updateHql);
	    					query.setParameter("sysId", sysId);
	    					query.setParameter("summaryRule", summaryFormatFinal, StringType.INSTANCE);
	    					query.setParameter("detailRule", detailFormatFinal, StringType.INSTANCE);
	    					
	    					query.executeUpdate();
    					});
    				}
    				catch(Throwable t)
    				{
    					Log.log.error("Could not update ResolveAssess sysId: " + sysId, t);
                   HibernateUtil.rethrowNestedTransaction(t);
    				}
    			}
	    		
    		}
    	}
    }

} // AssessorUtil
