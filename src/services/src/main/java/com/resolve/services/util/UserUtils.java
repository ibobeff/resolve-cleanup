/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.hibernate.query.Query;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.resolve.enums.ColorTheme;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UserPreferencesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class UserUtils
{
    private static final String DEFAULT_COLOR_THEME_PROPERTY = "default.color.theme";
    
    static final String OLD_HASH_FUNCTION = "SHA-1";
    static final String HASH_FUNCTION = "SHA-512";
    static final String P_ASSWORD_CHAR = "*";

    public final static String SYSTEM = "system";
    public final static String ADMIN = "admin";
    public final static String RESOLVE_MAINT = "resolve.maint";
    private static ConcurrentHashMap<String,AccessRights> cacheAccessRights = new ConcurrentHashMap<String,AccessRights>();
    //this cached object will only have Sys_Id,URoles, and UActive which are needed for permission validation
    private static ConcurrentHashMap<String,ResolveActionTaskVO> cacheTaskRoles = new ConcurrentHashMap<String,ResolveActionTaskVO>();

    public static String getUserid(String token)
    {
        String result = null;

        if (token != null && !token.equals(""))
        {
            result = token.substring(0, token.indexOf("/"));
        }

        return result;
    } // getUserid

    /**
     * Get Users object.
     * 
     * @param username
     * @return Users object. Null if the username does not exist.
     */
    public static UsersVO getUser(String username)
    {
        return ServiceHibernate.getUser(username);
    } // getUser
    
    public static RolesVO getRole(String roleName)
    {
        return ServiceHibernate.getRole(null, roleName);
    } // getRoles
    
    public static GroupsVO getGroup(String groupName)
    {
        return ServiceHibernate.getGroup(null, groupName);
    } // getRoles

    
    
    public static UsersVO getUserWithRoles(String username)
    {
        return ServiceHibernate.getUserWithRoles(username);
    } // getUser

    public static UsersVO getUserWithPreferences(String username)
    {
        return ServiceHibernate.getUserWithPreferences(username);
    } // getUser

    public static Set<UserPreferencesVO> getUserPreferences(String username, Set<String> preferencesKeys)
    {
        return ServiceHibernate.getUserPreferences(username, preferencesKeys);
    }

    public static UsersVO findUserById(String sys_id)
    {
        return ServiceHibernate.findUserById(sys_id);
    } // getUser

    public static UsersVO findUserWithRolesById(String sys_id)
    {
        return ServiceHibernate.findUserWithRolesById(sys_id);
    } // getUser

    /**
     * 
     * This method returns a Set of roles that a user belongs to.
     * 
     * @param userName
     * @return
     */
    public static Set<String> getUserRoles(String userName)
    {
        return ServiceHibernate.getUserRoles(userName);
    } // getUserRoles

    
    public static ConcurrentHashMap<String, AccessRights> getCacheAccessRights() {
		return cacheAccessRights;
	}

	public static void setCacheAccessRights(ConcurrentHashMap<String, AccessRights> cacheAccessRights) {
		UserUtils.cacheAccessRights = cacheAccessRights;
	}

	/**
     * Returns all the users with matching roles.
     * 
     * @param roles : String represnting roles seperated by comma
     * @param whereClause : String representing SQL 'where' clause.
     * @return List of {@link UsersVO}
     */
    public static List<UsersVO> getUsersWithRole(String roles, String whereClause)
    {
        return ServiceHibernate.getUsersWithRole(roles, whereClause);
    }

    public static boolean hasRole(String user, Set<String> userRoles, String entityRolesStr)
    {
        boolean hasRights = false;

        if (user.equalsIgnoreCase(SYSTEM) || user.equalsIgnoreCase(ADMIN) || user.equalsIgnoreCase(RESOLVE_MAINT))
        {
            hasRights = true;
        }
        else if (userRoles.contains(ADMIN))// if the user has the admin role, then no issues
        {
            hasRights = true;
        }
        else
        {
            if (StringUtils.isNotEmpty(entityRolesStr))
            {
                String[] entityRoles = entityRolesStr.split(",");
                for (String entityRole : entityRoles)
                {
                    if (entityRole.indexOf('&') > -1)
                    {
                        // check for the roles with '&' clause
                        hasRights = hasAllRoles(userRoles, entityRole);
                    }
                    else if (userRoles.contains(entityRole))
                    {
                        hasRights = true;
                    }

                    if (hasRights)
                    {
                        break;
                    }
                }// end of for loop
            }// end of if
        }

        return hasRights;
    }// hasRole

    /**
     * Checks if the user has any of the roles assigned. If yes, then it will
     * return true else false
     * 
     * @param user : String representing user name
     * @param entityRolesStr :  String representing comma seperated roles
     * @return True if the role(s) matches, false otherwise.
     * 
     */
    public static boolean hasRole(String user, String entityRolesStr)
    {
        boolean hasRights = false;

        if (StringUtils.isNotEmpty(user))
        {
            if (user.equalsIgnoreCase(SYSTEM) || user.equalsIgnoreCase(ADMIN) || user.equalsIgnoreCase(RESOLVE_MAINT))
            {
                hasRights = true;
            }
            else
            {
                // get the user roles
                Set<String> userRoles = getUserRoles(user);
                hasRights = hasRole(user, userRoles, entityRolesStr);

            }// end of if
        }

        return hasRights;
    } // hasRole

    public static boolean hasRole(String user, Set<String> roles)
    {
        boolean result = false;

        if (roles != null && roles.size() > 0)
        {
            String entityRolesStr = StringUtils.convertCollectionToString(roles.iterator(), ",");
            result = hasRole(user, entityRolesStr);
        }

        return result;
    } // hasRole

    /**
     * 
     * @param userRoles
     * @param entityRolesStr
     *            - role1,role2,role3&role4,role5
     * @return
     * @throws Throwable
     */
    private static boolean hasRole(Set<String> userRoles, String entityRolesStr)
    {
        boolean hasRights = false;

        if (userRoles != null && userRoles.size() > 0 && StringUtils.isNotEmpty(entityRolesStr))
        {
            String[] entityRoles = entityRolesStr.split(",");
            for (String entityRole : entityRoles)
            {
                //if user has 'admin' as a role, he has admin rights
                if(userRoles.contains(ADMIN))
                {
                    hasRights = true;
                }
                else if (entityRole.indexOf('&') > -1)
                {
                    // check for the roles with '&' clause
                    hasRights = hasAllRoles(userRoles, entityRole);
                }
                else if (userRoles.contains(entityRole))
                {
                    hasRights = true;
                }

                if (hasRights)
                {
                    break;
                }
            }// end of for loop
        }// end of if

        return hasRights;
    } // hasRole

    /**
     * return true only if all the roles are present, else false
     * 
     * @param userRoles
     * @param rolesWithAnd
     *            --> role1&role2&role3
     * @return
     */
    private static boolean hasAllRoles(Set<String> userRoles, String rolesWithAnd)
    {
        boolean hasRights = true;

        if (StringUtils.isNotEmpty(rolesWithAnd) && rolesWithAnd.indexOf('&') > -1 && userRoles != null && userRoles.size() > 0)
        {
            String[] roles = rolesWithAnd.split("&");
            for (String role : roles)
            {
                if (!userRoles.contains(role))
                {
                    hasRights = false;
                    break;
                }
            }// end of for
        }
        else
        {
            hasRights = false;
        }

        return hasRights;

    }// hasAllRoles

    public static String getUserRolesWithAnd(String userID)
    {
        String userRolesInString = "";
        Set<String> userRoleWithAnd = new TreeSet<String>();

        try
        {
            if (!StringUtils.isEmpty(userID))
            {
                Set<String> userRoles = getUserRoles(userID);

                Set<String> rolesWithAnd = getAllRolesWithAnd();

                for (String roleWithAnd : rolesWithAnd)
                {
                    if (hasRole(userRoles, roleWithAnd))
                    {
                        userRoleWithAnd.add(roleWithAnd);
                    }
                }

                if (userRoles != null)
                {
                    for (String userRole : userRoles)
                    {
                        userRolesInString += " " + userRole;
                    }

                    for (String roleWithAndLocal : userRoleWithAnd)
                    {
                        roleWithAndLocal = roleWithAndLocal.replace("&", "_");
                        userRolesInString += " " + roleWithAndLocal;
                    }
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return userRolesInString;
    } // getUserRolesWithAnd

    public static Set<String> getAllRolesWithAnd()
    {
        return ServiceHibernate.getAllRolesWithAnd();
    } // getAllRolesWithAnd

    public static Set<String> getAllRoles()
    {
        return ServiceHibernate.getAllRoles();
    } // getAllRolesWithAnd

    /**
     * Returns all group names on the current system
     * 
     * @return Set containing all group names
     */
    public static Set<String> getAllGroups()
    {
        return ServiceHibernate.getAllGroups();
    } // getAllGroups

    /**
     * This method returns a Set of groups that a user belongs to.
     * 
     * @param userName
     * @return
     */
    public static Set<String> getUserGroups(String userName)
    {
        return ServiceHibernate.getUserGroups(userName);
    } // getUserGroups

    /**
     * This method retuns the User Names of all users belonging to a group.
     * 
     * @param groupName
     * @return
     */
    public static Set<String> getUsersForGroup(String groupName)
    {
        return ServiceHibernate.getUsersForGroup(groupName);
    } // getUsersForGroup

    public static Set<String> getUsersForGroup(String[] groups)
    {
        return ServiceHibernate.getUsersForGroup(groups);
    } // getUsersForGroup

    /**
     * This method returns all roles that make up a group
     * 
     * @param groupName
     * @return
     */
    public static Set<String> getRolesForGroup(String groupName)
    {
        return ServiceHibernate.getRolesForGroup(groupName);
    } // getRolesForGroup

    /**
     * Check if the current user has the required roles
     * 
     * @param task_sid
     *            - sys_id of the ActionTask
     * @throws Exception
     *             - If user does not have permissions, ActionTask is
     *             deactivated or missing
     */
    public static boolean checkTaskPermission(String task_sid, String username)
    {
        return ServiceHibernate.checkTaskPermission(task_sid, username);
    } // checkTaskPermission

    public static boolean checkTaskPermission(ResolveActionTaskVO task, String username)
    {
        boolean result = false;

        if (task != null && StringUtils.isNotEmpty(username))
        {
            String taskname = task.getUName();

            if (task.getUActive() == false)
            {
                Log.log.warn("ActionTask is deactivated. Please activate ActionTask before execution.");
            }
            else if (!hasRole(username, task.getURoles()))
            {
                Log.log.warn("Not permitted to execute ActionTask: " + taskname + ". Missing required permission role.");
            }
            else
            {
                result = true;
            }
        }
        
        return result;
    } // checkTaskPermission

	public static boolean checkTaskPermission(String taskId, Set<String> userRoles)
    {
        //ResolveActionTaskVO task = ServiceHibernate.getResolveActionTask(taskId, null, "system");
        boolean result = false;
        ResolveActionTaskVO task = new ResolveActionTaskVO();
        if(cacheTaskRoles.containsKey(taskId))
        {
        	task = cacheTaskRoles.get(taskId);
        	
        } else {
	        String selectQuery = "select at.UName, at.URoles, at.UActive from ResolveActionTask at where at.sys_id = :SysId";
	        Map<String,Object> params = new HashMap<String, Object>();
	        params.put("SysId",taskId);
	        final List<Object[]> list = new ArrayList<Object[]>();
	        
	        Consumer<HibernateUtil> block = (util) -> {
	        		try {
						@SuppressWarnings("unchecked")
						Query<Object[]> q = HibernateUtil.createQuery(selectQuery);
						q.setParameter("SysId", taskId);
						list.addAll(q.list());
					} catch (Exception e) {
						Log.log.error(e.getMessage(),e);
					}
	        };
	        try {
				HibernateUtil.action(block,"system");
				Object[] obj = list.get(0);
				if(obj != null) {
					task.setUName((String) obj[0]);
					task.setURoles((String) obj[1]);
					task.setUActive((Boolean) obj[2]);
					cacheTaskRoles.put(taskId,task);
				}
			} catch (Throwable e) {
				Log.log.error(e.getMessage(),e);
			}
        }
		result = checkTaskPermission(task, userRoles);
        return result;
    }
    
    public static boolean checkTaskPermission(ResolveActionTaskVO task, Set<String> userRoles)
    {
        boolean result = false;

        if (task != null && userRoles != null && userRoles.size() > 0)
        {
            String taskname = task.getUName();
            
            //if user has 'admin' as a role, he has admin rights
            if(userRoles.contains(ADMIN))
            {
                result = true;
            }
            else if (task.getUActive() == false)
            {
                Log.log.warn("ActionTask is deactivated. Please activate ActionTask before execution.");
            }
            else if (!hasRole(userRoles, task.getURoles()))
            {
                Log.log.warn("Not permitted to execute ActionTask: " + taskname + ". Missing required permission role.");
            }
            else
            {
                result = true;
            }
        }

        // refactored - removed the transaction
        /*
         * try { if (task != null) { HibernateUtil.beginTransaction();
         * 
         * String taskname = task.getUName();
         * 
         * if (task.getUActive() == false) { Log.log.warn(
         * "ActionTask is deactivated. Please activate ActionTask before execution."
         * ); } else if (!UserUtils.hasRole(userRoles, task.getURoles())) {
         * Log.log.warn("Not permitted to execute ActionTask: " + taskname +
         * ". Missing required permission role."); } else { result = true; }
         * 
         * HibernateUtil.commitTransaction(); } else {
         * Log.log.warn("Missing ActionTask to be executed"); } } catch
         * (Throwable e) { Log.log.warn("FAIL: checkTaskPermission. " +
         * e.getMessage()); HibernateUtil.rollbackTransaction();
         * HibernateUtil.rethrowNestedTransaction(e); }
         */
        return result;
    } // checkTaskPermission

    public static boolean isLockedOut(String username)
    {
        return ServiceHibernate.isLockedOut(username);
    } // isLockedOut
    
    public static String passwordIsValid(String newPassword)
    {
        String result = null;
        if(newPassword.length() < 8) {
            result = "password must be at least 8 characters";
        }
        if(!newPassword.matches(".*\\d+.*"))
        {
            result = "password must have at least one number";
        }
        if(newPassword.equals(newPassword.toLowerCase()) || newPassword.equals(newPassword.toUpperCase()))
        {
            result = "password must have at least one lower case and one upper case character";
        }
        return result;
    }
    public static boolean checkPassword(String username, String password)
    {
        return ServiceHibernate.checkPassword(username, password);
    } // checkPassword

    public static boolean checkMaintPassword(String username, String password, File file)
    {
        boolean result = false;

        if (username.equals(Constants.USER_RESOLVE_MAINT))
        {
            String hashPasswd = encryptP_asswd(password);

            // check password from file system
            if (file != null && file.exists())
            {
                try
                {
                    String filePasswd = FileUtils.readFileToString(file, "UTF-8");
                    if (!StringUtils.isEmpty(filePasswd))
                    {
                        filePasswd = filePasswd.trim();
                        if (hashPasswd.equalsIgnoreCase(filePasswd))
                        {
                            result = true;
                        }
                        else
                        {
                            String oldHashPasswd = encryptPasswdOld(password);
                            if (oldHashPasswd.equals(filePasswd))
                            {
                                result = true;
                            }
                        }
                    }
                }
                catch (IOException e)
                {
                    Log.alert(ERR.E60003, e);
                }
            }
            else
            {
                Log.log.error("Missing resolve.maint user file");
            }
        }

        return result;
    } // checkMaintPassword
    
    public static boolean setMaintPassword(String username, String oldPassword, String newPassword, File file)
    {
        boolean result = false;

        if (checkMaintPassword(username, oldPassword, file))
        {
            FileOutputStream fos = null;
            try
            {
                fos = new FileOutputStream(file);
                String hashPasswd = encryptP_asswd(newPassword);
                
                fos.write(hashPasswd.getBytes());
                Log.log.warn("Resolve.maint password updated");
                result = true;
            }
            catch (Exception e)
            {
                Log.log.error("Failed to update Resolve.maint password", e);
            }
            finally
            {
                if (fos != null)
                {
                    try
                    {
                        fos.close();
                    }
                    catch (IOException ioe)
                    {
                        Log.log.error("Failed to close output stream", ioe);
                    }
                }
            }
        }
        else
        {
            Log.log.warn("Old Password provided is not valid");
        }

        return result;
    } // setMaintPassword

    public static void updatePassword(String username, String password) throws Exception
    {
       ServiceHibernate.updatePassword(username, password);
       
    } // updatePassword

    public static String encryptPasswdOld(String x)
    {
        String answer = x;
        
        if (!StringUtils.isEmpty(x) && x.charAt(x.length() - 1) != '=')
        {
            try
            {
                MessageDigest d = MessageDigest.getInstance(OLD_HASH_FUNCTION);
                d.reset();
                d.update(x.getBytes());
                byte[] bytes = d.digest();
                
                answer = new String(Base64.encodeBase64(bytes)); // converted to apache encoding
            }
            catch (Exception e)
            {
                Log.log.warn(e);
            }
        }
        return answer;
    } // encrypt

    public static String encryptP_asswd(String x)
    {
        String answer = x;
        
        if (!StringUtils.isEmpty(x) && x.charAt(x.length() - 1) != '=')
        {
            try
            {
                MessageDigest d = MessageDigest.getInstance(HASH_FUNCTION);
                d.reset();
                d.update(x.getBytes());
                byte[] bytes = d.digest();
                
                answer = new String(Base64.encodeBase64(bytes)); // converted to apache encoding
            }
            catch (Exception e)
            {
                Log.log.warn(e);
            }
        }
        return answer;
    } // encryptPasswd

    /**
     * API to save user
     * 
     * @param user : {@link UsersVO} user vo needs to be saved
     * @param username : String representing user name who is invoking this API.
     * @return Saved {@link UsersVO}
     */
    public static UsersVO saveUser(UsersVO user, String username)
    {
        UsersVO savedUser = null;
        try
        {
            savedUser = ServiceHibernate.saveUser(user, null, username);
        }
        catch(Exception e)
        {
            Log.log.error("Error in saving the user", e);
        }
        
        return savedUser;
    }


    /**
     * Get all user with roles.
     * 
     * @param isLocked
     *            specify is want locked or unlocked.
     * @return
     */
    public static List<UsersVO> getAllUsers(boolean isLocked)
    {
        return ServiceHibernate.getAllUsers(isLocked);
    }

    public static Set<String> getAllUsernames(boolean isLocked)
    {
        return ServiceHibernate.getAllUsernames(isLocked);
    }
    
    public static List<UsersVO> getAllUsers(String whereCaluse)
    {
        return ServiceHibernate.getAllUsers(whereCaluse);
    }
    
    public static List<RolesVO> getAllRoles(String whereCaluse)
    {
        return ServiceHibernate.getAllRoles(whereCaluse);
    }
    
    public static List<GroupsVO> getAllGroups(String whereCaluse)
    {
        return ServiceHibernate.getAllGroups(whereCaluse);
    }

    /**
     * This method returns an unlocked user by searching its email address. If
     * there are multiple people using the same email then it will pick only one
     * of them.
     * 
     * @param email
     *            address of the user.
     * @return
     */
    public static UsersVO getUserByEmail(String email)
    {
        return ServiceHibernate.getUserByEmail(email);
    } // getUserByEmail

    /**
     * This method returns an unlocked user by searching its email address. If
     * there are multiple people using the same email then it will pick only one
     * of them.
     * 
     * @param im
     *            Instant Messanger (XMPP) address of the user.
     * @return
     */
    public static UsersVO getUserByIM(String im)
    {
        return ServiceHibernate.getUserByIM(im);
    } // getUserByIM

    /**
     * 
     * @param wikidocFullName - doc full name
     * @param username - username
     * @param rightType - view, edit, execute, admin --> Same as WikiRightType 
     * @return
     */
    public static boolean checkWikiPermission(String wikidocFullName, String username, String rightType)
    {
        boolean hasRights = false;
        if(StringUtils.isNotBlank(username) && StringUtils.isNotBlank(wikidocFullName) && StringUtils.isNotBlank(rightType))
        {
            Set<String> userRoles = getUserRoles(username);
            hasRights =  checkWikiPermission(wikidocFullName, username, userRoles,  rightType);
        }
        
        return hasRights;
    }
    
    /**
     * return the user UserName
     * @param sys_id
     * @return
     */
    public static String getAssigntoName(String sys_id)
    {
        String result = "";

        UsersVO user =  ServiceHibernate.findUserById(sys_id);
        
        if(user != null)
        {
            result = user.getUUserName();
        }
        
        return result;
    }

    public static void cacheFlush(String wiki) {
    		cacheAccessRights.remove(wiki);
    }
    
    public static void cacheTaskFlush(String taskId) {
		cacheTaskRoles.remove(taskId);
    }
    /**
     * 
     * @param wikidocFullName - doc full name
     * @param username - username
     * @param userRoles - user roles
     * @param rightType rightType - view, edit, execute, admin --> Same as WikiRightType 
     * @return
     */
	public static boolean checkWikiPermission(String wikidocFullName, String username, Set<String> userRoles,  String rightType)
    {
        boolean hasRights = false;
        if(userRoles != null && StringUtils.isNotBlank(wikidocFullName) && StringUtils.isNotBlank(rightType))
        {
        	
        	String selectQuery = "select wd.accessRights from WikiDocument wd where wd.UFullname =:wikiDocFullName";
        	List<AccessRights> list = new ArrayList<AccessRights>();
        	AccessRights accessRights = null;
        	if(cacheAccessRights.containsKey(wikidocFullName)) {
        		accessRights = cacheAccessRights.get(wikidocFullName);
        	}
        	if(accessRights == null) {
	        	Consumer<HibernateUtil> block = (util) -> {
					try {
						@SuppressWarnings("unchecked")
						Query<AccessRights> q = HibernateUtil.createQuery(selectQuery);
						q.setParameter("wikiDocFullName", wikidocFullName);
						list.add(q.list().get(0));
					} catch (Exception e) {
						Log.log.error(e.getMessage(),e);
					}
	        	};
	        	try {
					HibernateUtil.action(block, username);
					accessRights = list.get(0);
					if(accessRights != null) {
						cacheAccessRights.put(wikidocFullName, accessRights);
					}
				} catch (Throwable e) {
					Log.log.error(e.getMessage(),e);
				}
        	}
            if(accessRights != null)
            {	
                AccessRightsVO ar = accessRights.doGetVO();
                if(ar != null)
                {
                    String rights = null;
                    if(rightType.equalsIgnoreCase("view"))
                    {
                        rights = ar.getUReadAccess();
                    }
                    else if(rightType.equalsIgnoreCase("edit"))
                    {
                        rights = ar.getUWriteAccess();
                    }
                    else if(rightType.equalsIgnoreCase("execute"))
                    {
                        rights = ar.getUExecuteAccess();
                    }
                    else if(rightType.equalsIgnoreCase("admin"))
                    {
                        rights = ar.getUAdminAccess();
                    }
                    
                    hasRights = hasRole(username, userRoles, rights);
                }
            }
        }
        
        return hasRights;
    }
    
    public static boolean isOrgEnabled(String username)
    {
        return ServiceHibernate.isUserOrgEnabled(username);
    }
    
    public static String addAdminRole(String csvRoles)
    {
        String result = "";
        Set<String> roles = new HashSet<String>();
        roles.add("admin");
        
        if(StringUtils.isNotBlank(csvRoles))
        {
            roles.addAll(StringUtils.convertStringToList(csvRoles, ","));
        }
        
        result = StringUtils.convertCollectionToString(roles.iterator(), ",");
        
        return result;
    }

    public static String bcryptPasswdHash(String x)
    {
        String answer = x;
        
        if (!StringUtils.isEmpty(x) && !x.startsWith(CryptUtils.prefix128Local))
        {
            String bcryptLogRoundsPropertyVal = PropertiesUtil.getPropertyString("users.password.bcrypt.log_rounds");
            int bcryptLogRounds = (Integer.parseInt(bcryptLogRoundsPropertyVal) >= 4  && Integer.parseInt(bcryptLogRoundsPropertyVal) <= 31) ? Integer.parseInt(bcryptLogRoundsPropertyVal) : 10;
            
            long startnsec = System.nanoTime();
            
            String hash = BCrypt.hashpw(x, BCrypt.gensalt(bcryptLogRounds, new SecureRandom()));
            
            Log.log.debug("Bcrypt password hash with " + bcryptLogRounds + " took " + (System.nanoTime() - startnsec) + " nsec.");
            
            try
            {
                answer = CryptUtils.encrypt(hash);
            }
            catch (Exception e)
            {
                Log.log.error("Failed to encrypt password hash due to " + e.getMessage(), e);
            }
        }
        
        return answer;
    } // bcryptPasswdHash
    
    public static boolean checkBcryptPasswordHash(String plainTextPassword, String savedEncryptedPasswordHash)
    {
        boolean matched = false;
        
        String savedDecryptedPasswordHash = null;
        
        try
        {
            savedDecryptedPasswordHash = CryptUtils.decrypt(savedEncryptedPasswordHash);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to decrypt saved encrypted password hash due to " + e.getMessage(), e);
        }
        
        long startnsec = System.nanoTime();
        
        matched = BCrypt.checkpw(plainTextPassword, savedDecryptedPasswordHash);
        
        Log.log.debug("Checking password with saved bcrypt password hash took " + (System.nanoTime() - startnsec) + " nsec.");
        
        return matched;
    }

    /**
     * Get configured color theme from rsview.properties
     * If the value is not set or wrong return dark theme as default
     * @return {@link ColorTheme}
     */
    public static ColorTheme getDefaultColorTheme() {
	String theme = PropertiesUtil.getPropertyString(DEFAULT_COLOR_THEME_PROPERTY);
	Optional<String> optional = Optional.ofNullable(theme).filter(o -> !o.isEmpty());
	ColorTheme colorTheme;

	if (optional.isPresent()) {
	    try {
		colorTheme = ColorTheme.valueOf(optional.get().toUpperCase());
	    } catch (IllegalArgumentException e) {
		colorTheme = ColorTheme.LIGHT;
	    }
	} else {
	    colorTheme = ColorTheme.LIGHT;
	}

	return colorTheme;
    }
    
    public static boolean isAdminUser(UsersVO user)
    {
        boolean isAdmin = false;

        try
        {
            String csvUserRoles = user.getRoles().replaceAll(" ", ",").replaceAll("_", "&");
            Set<String> userRoles = new HashSet<String>(StringUtils.convertStringToList(csvUserRoles, ","));
            if (userRoles.contains(UserUtils.ADMIN))
            {
                isAdmin = true;
            }
        }
        catch (Throwable t)
        {
            Log.log.error("error validating if the user is admin or not.", t);
        }

        return isAdmin;
    }

    
} // UserUtils