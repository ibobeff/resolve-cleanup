/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.immport;

import java.util.Collection;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskResolveTagRel;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.TagUtil;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.util.Log;


public class ImportActiontaskGraph  extends ImportComponentGraph
{
    public ImportActiontaskGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        Log.log.debug("Importing ActionTask Relationship:" + rel);
        switch(rel.getTargetType())
        {
            case ResolveTag: 
            case TAG:    
                assignTagToActiontask();
                break;
                
            default:
                break;
        }
    }
    
    private void assignTagToActiontask()
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            ResolveActionTask dbTask = ActionTaskUtil.validateUserForActionTaskNoCache(null, rel.getSourceName(), RightTypeEnum.edit, username);
	            if(dbTask != null)
	            {
	                ResolveTag tag = TagUtil.findTag(null, rel.getTargetName(), username);
	                if(tag != null)
	                {
	                    //make sure that the relationship is not duplicated
	                    boolean insert = true;
	                    Collection<ResolveActionTaskResolveTagRel> rels = dbTask.getAtTagRels();
	                    if(rels != null && rels.size() > 0)
	                    {
	                        for(ResolveActionTaskResolveTagRel rel : rels)
	                        {
	                            if(rel.getTag() != null)
	                            {
	                                if(rel.getTag().getUName().equalsIgnoreCase(tag.getUName()))
	                                {
	                                    insert = false;
	                                    break;
	                                }
	                            }
	                        }//end of for loop
	                    }
	                    
	                    if(insert)
	                    {
	                        ResolveActionTaskResolveTagRel rel = new ResolveActionTaskResolveTagRel();
	                        rel.setTask(dbTask);
	                        rel.setTag(tag);
	                        
	                        HibernateUtil.getDAOFactory().getResolveActionTaskResolveTagRelDAO().persist(rel);
	                    }
	                }
	            }
            
            });
        }
        catch(Throwable t)
        {
            Log.log.error("Error in assigning Tag to AT:" + rel, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
    }
    

}
