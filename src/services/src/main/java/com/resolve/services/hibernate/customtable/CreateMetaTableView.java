/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.Collection;

import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaTable;
import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.persistence.model.MetaViewField;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.vo.form.RsCustomTableDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;

public class CreateMetaTableView
{
    private String tableName = null;
    private String username = null;
    
    public CreateMetaTableView(String tableName, String username) throws Throwable
    {
        if(StringUtils.isBlank(tableName) || StringUtils.isBlank(username))
        {
            throw new RuntimeException("Table name and username are mandatory to create meta table view.");
        }
        
        this.tableName = tableName;
        this.username = username;

        //initialize
        init();
        
    }
    
    public void createDefaultView(RsCustomTableDTO uiCustomTable) throws Throwable
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		String url = "/resolve/service/wiki/view/${table}/${view}?sys_id=${sys_id}&view=${view}&table=${table}";
        		if(StringUtils.isNotBlank(uiCustomTable.getUwikiName()) && uiCustomTable.getUwikiName().indexOf('.') > -1)
        		{
        			String[] arr = uiCustomTable.getUwikiName().split("\\.");
        			url = url.replace("${table}", arr[0]);
        			url = url.replace("${view}", arr[1]);
        		}
        		
        		CustomTable customTable = new CustomTable();
        		customTable.setUName(tableName);
	            customTable = HibernateUtil.getDAOFactory().getCustomTableDAO().findFirst(customTable);
	            
	            if(customTable != null)
	            {
	                MetaTable metaTable = customTable.getMetaTable();
	                if(metaTable == null)
	                {
	                    metaTable = new MetaTable();
	                    metaTable.setUName(tableName);
	                    
	                    //persist it
	                    HibernateUtil.getDAOFactory().getMetaTableDAO().persist(metaTable);
	                    
	                    //add it to the table
	                    customTable.setMetaTable(metaTable);
	                }
	                
	                MetaTableView defaultView = getDefaultMetaTableView(metaTable, metaTable.getMetaTableViews(), uiCustomTable, url);
	                
	                //add the fields to the default view
	                addNewFieldsToDefaultMetaTableView(defaultView, customTable.getMetaFields());
	                    
	            }
	            else
	            {
	                throw new Exception(this.tableName + " does not exist. So aborting the creation of meta table view");
	            }
            
        	});
        }
        catch(Throwable t)
        {
            Log.log.error(t.getMessage(), t);
            throw t;
        }
    }
    
    private MetaTableView getDefaultMetaTableView(MetaTable metaTable, Collection<MetaTableView> metaTableViews, RsCustomTableDTO uiCustomTable, String url)
    {
        MetaTableView defaultView = null;
        if(metaTableViews != null)
        {
            for(MetaTableView metaTableView : metaTableViews)
            {
                if(metaTableView.getUName().equalsIgnoreCase(HibernateConstantsEnum.DEFAULT.getTagName()))
                {
                    defaultView = metaTableView;
                    break;
                }
            }
        }
        
        if(defaultView == null)
        {
            defaultView = new MetaTableView();
            defaultView.setUName(HibernateConstantsEnum.DEFAULT.getTagName());
            defaultView.setUDisplayName(StringUtils.isNotBlank(uiCustomTable.getUDisplayName()) ? uiCustomTable.getUDisplayName() : uiCustomTable.getUName());
            defaultView.setUType(HibernateConstantsEnum.META_TABLE_VIEW_SHARED.getTagName());
            defaultView.setUIsGlobal(true);
            defaultView.setUMetaNewLink(url);
            defaultView.setUMetaFormLink(url);
            defaultView.setUTarget(HibernateConstantsEnum.TARGET_APPLICATION_TAB.getTagName());
            defaultView.setUParams("");
            
            //persist it
            HibernateUtil.getDAOFactory().getMetaTableViewDAO().persist(defaultView);

            //add access rights to the meta table
            defaultView.setMetaAccessRights(createAccessRights(defaultView, uiCustomTable));
            
            //add the parent ref
            defaultView.setMetaTable(metaTable);
        }
        
        return defaultView;
    }
    
    
    private void addNewFieldsToDefaultMetaTableView(MetaTableView defaultView, Collection<MetaField> tableFields)
    {
        Collection<MetaField> newFieldsToAdd = new ArrayList<MetaField>();
        
        Collection<MetaTableViewField> metaTableViewFields = defaultView.getMetaTableViewFields();
        if(metaTableViewFields == null)
        {
            metaTableViewFields = new ArrayList<MetaTableViewField>();
            
            //add all the fields
            newFieldsToAdd.addAll(tableFields);
        }
        else
        {
            newFieldsToAdd.addAll(getNewFieldsToAdd(metaTableViewFields, tableFields));
        }
        
        
        if(newFieldsToAdd.size() > 0)
        {
            int order = metaTableViewFields.size() + 1;
            //add the new fields
            for(MetaField newField : newFieldsToAdd)
            {
                MetaViewField metaViewField = new MetaViewField();
                metaViewField.setUId(newField.getUColumn());
                metaViewField.setUHeader(newField.getUDisplayName());
                metaViewField.setUWidth(100);
                metaViewField.setUAlignment("LEFT");
                metaViewField.setUResizeable(true);
                metaViewField.setUToolTip("Tooltip:");
                metaViewField.setUGroupable(false);
                metaViewField.setUFixed(false);
                metaViewField.setUDateTimeFormat("MMM dd yyyy, HH:mm:ss");
                
                metaViewField.setUSortable(true);
                if(newField.getMetaFieldProperties().getUStringMaxLength() != null
                                && newField.getMetaFieldProperties().getUStringMaxLength().intValue() >= 4000)
                {
                    metaViewField.setUSortable(false);
                }
                
                //persist
                HibernateUtil.getDAOFactory().getMetaViewFieldDAO().persist(metaViewField);
                
                //create view field instance
                MetaTableViewField viewField = new MetaTableViewField();
                viewField.setMetaField(newField);
                viewField.setMetaTableView(defaultView);
                viewField.setMetaViewField(metaViewField);
                viewField.setUOrder(order);
                
                //persist
                HibernateUtil.getDAOFactory().getMetaTableViewFieldDAO().persist(viewField);
                
                order++;
                
            }//end of for loop
        }//end of if
    }
    
    private Collection<MetaField> getNewFieldsToAdd(Collection<MetaTableViewField> metaTableViewFields, Collection<MetaField> tableFields)
    {
        Collection<MetaField> newFieldsToAdd = new ArrayList<MetaField>();
        
        if (tableFields != null && !tableFields.isEmpty())
        {
            boolean isNew = true;
            for(MetaField newField : tableFields)
            {
                isNew = true;
                
                //iterate to the existing fields
                for(MetaTableViewField tableViewField : metaTableViewFields)
                {
                    MetaField existingField = tableViewField.getMetaField();
                    if (existingField==null)
                    	continue;
                    if(existingField.getUName().equals(newField.getUName()))
                    {
                        isNew = false;
                        break;
                    }
                }
                
                if(isNew)
                {
                    newFieldsToAdd.add(newField);
                }
            }
        }
        
        return newFieldsToAdd;
    }
    

    private MetaAccessRights createAccessRights(MetaTableView defaultView, RsCustomTableDTO uiCustomTable)
    {
        MetaAccessRights ar = new MetaAccessRights();
        ar.setUAdminAccess(uiCustomTable.getAdminRoles());
        ar.setUReadAccess(uiCustomTable.getViewRoles());
        ar.setUWriteAccess(uiCustomTable.getEditRoles());
        ar.setUResourceType(MetaTableView.RESOURCE_TYPE);
        ar.setUResourceId(defaultView.getSys_id());
        ar.setUResourceName(HibernateConstantsEnum.DEFAULT.getTagName());
        
        //persist it
        HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(ar);
        
        return ar;
    }
    
    
    private void init() throws Throwable
    {
        
        
        

    }
    
    
    
}
