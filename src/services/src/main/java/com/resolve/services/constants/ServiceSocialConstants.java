/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.constants;

public class ServiceSocialConstants
{
 // params keys
    public static final String PARAMS_SYS_ID = "SYS_ID";
    public static final String PARAMS_DISPLAYNAME = "DISPLAYNAME";
    public static final String PARAMS_USERNAME = "USERNAME";
    public static final String PARAMS_TITLE = "TITLE";
    public static final String PARAMS_TITLE_URL = "TITLE_URL";
    public static final String PARAMS_AUTHOR = "AUTHOR";
    public static final String PARAMS_CONTENT = "CONTENT";
    public static final String PARAMS_COMMENT = "COMMENT";
    public static final String PARAMS_EXPIRATION = "EXPIRATION";
    
    public static final String SYS_ID = "sys_id";
    public static final String SYS_CREATED_ON = "sys_created_on";
    public static final String SYS_UPDATED_ON = "sys_updated_on";
    public static final String SYS_CREATED_BY = "sys_created_by";
    public static final String SYS_UPDATED_BY = "sys_updated_by";
    public static final String U_READ_ROLES = "u_read_roles";
    public static final String U_EDIT_ROLES = "u_edit_roles";
    public static final String U_POST_ROLES = "u_post_roles";
    public static final String U_DISPLAY_NAME = "u_display_name";
    
    public static final String TYPE = "type";

}
