/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.query.Query;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.resolve.persistence.dao.MetaFormViewDAO;
import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaControl;
import com.resolve.persistence.model.MetaControlItem;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaFormAction;
import com.resolve.persistence.model.MetaFormTab;
import com.resolve.persistence.model.MetaFormTabField;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.MetaSource;
import com.resolve.persistence.model.MetaxFieldDependency;
import com.resolve.persistence.model.MetaxFormViewPanel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.Operation;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.MetaAccessRightsVO;
import com.resolve.services.hibernate.vo.MetaFieldPropertiesVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.hibernate.vo.ResolveSysScriptVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.DefaultDataDTO;
import com.resolve.services.vo.form.RsButtonAction;
import com.resolve.services.vo.form.RsButtonPanelDTO;
import com.resolve.services.vo.form.RsColumnDTO;
import com.resolve.services.vo.form.RsControls;
import com.resolve.services.vo.form.RsFieldDependency;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsPanelDTO;
import com.resolve.services.vo.form.RsTabDTO;
import com.resolve.services.vo.form.RsUIButton;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;

public class ExtMetaFormLookup
{
    private String id = null;
    private String viewName = null;
//    private String tableName = null;
    private String username = "";
    private RightTypeEnum rightType = null;

    private Set<String> userRoles = null;
    private MetaFormView dbView = null;
    private MetaAccessRights accessRights = null;
    
    //switches
    //If its only form definition, than it will not process sequence or any other process 
    private boolean formDefinitionOnly = false;
    
    //for extjs app
//    private ArrayList<RsUIField> hiddenVariablesX = null;//PHASE OUT THE SYSTEM VALUES
    private RsFormDTO viewx = null;
    
    private CustomTable customTable = null;
    
    private String recordSysId = null;

    public ExtMetaFormLookup(String id, String viewName, String tableName, String userId, RightTypeEnum rightType, String recordSysId) throws Exception
    {
        if(StringUtils.isEmpty(id) && StringUtils.isEmpty(viewName))
        {
            throw new Exception("Id or Viewname is mandatory");
        }
        
        this.id = id;
        this.viewName = viewName;
//        this.tableName = tableName;
        this.username = userId;
        this.rightType = rightType;
        this.recordSysId = recordSysId;

    }// MetaFormLookup

    public boolean isFormDefinitionOnly()
    {
        return formDefinitionOnly;
    }

    public void setFormDefinitionOnly(boolean formDefinitionOnly)
    {
        this.formDefinitionOnly = formDefinitionOnly;
    }

    public List<DefaultDataDTO> getDefaultValues()
    {
        List<DefaultDataDTO> data = new ArrayList<DefaultDataDTO>();
        
        try
        {
            initialize();
            
            Collection<MetaxFormViewPanel> metaxFormViewPanels = dbView.getMetaxFormViewPanels();
            if(metaxFormViewPanels != null)
            {
                for(MetaxFormViewPanel panel : metaxFormViewPanels)
                {
                    Collection<MetaFormTab> metaFormTabs = panel.getMetaFormTabs();
                    if(metaFormTabs != null)
                    {
                        for(MetaFormTab tab : metaFormTabs)
                        {
                            Collection<MetaFormTabField> metaFormTabFields = tab.getMetaFormTabFields();
                            if(metaFormTabFields != null)
                            {
                                for(MetaFormTabField tabField : metaFormTabFields)
                                {
                                    MetaFieldProperties metaFieldFormProperties = tabField.getMetaFieldFormProperties();
                                    if(metaFieldFormProperties == null)
                                    {
                                        //take the default record
                                         if(tabField.getMetaField() != null)
                                         {
                                             metaFieldFormProperties = tabField.getMetaField().getMetaFieldProperties();
                                         }
                                         else if(tabField.getMetaSource() != null)
                                         {
                                             metaFieldFormProperties = tabField.getMetaSource().getMetaFieldProperties();
                                         }
                                    }
                                    
                                    if(metaFieldFormProperties != null)
                                    {
                                        String defaultValue = metaFieldFormProperties.getUDefaultValue();
                                        if (metaFieldFormProperties.getUUIType().equalsIgnoreCase(HibernateConstantsEnum.UI_SEQUENCE.getTagName()) && StringUtils.isNotEmpty(metaFieldFormProperties.getUSequencePrefix()))
                                        {
                                            if(!this.isFormDefinitionOnly())
                                            {
                                                defaultValue = SequenceUtils.getNextSequence(metaFieldFormProperties.getUSequencePrefix());
                                            }
                                        }
                                        data.add(new DefaultDataDTO(metaFieldFormProperties.getUName(), defaultValue));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return data;
        
    }

    
    //for extjs
    public RsFormDTO getMetaFormViewX() throws Exception
    {
        //load the view and validate if the user has rights to it
        initialize();
       
        //transform the model to view
        transformModelToView();
        
        
        return viewx;
    }
    
    public static RsUIField convertMetaFieldToRsUIField(MetaAccessRightsVO accessRightsVO, MetaFieldVO metaFieldVO, MetaFieldPropertiesVO metaFieldFormPropertiesVO, RightTypeEnum rightType, String username, Set<String> userRoles, String recordSysId)
    {
        MetaFieldProperties metaFieldFormProperties = (metaFieldFormPropertiesVO != null ? new MetaFieldProperties(metaFieldFormPropertiesVO) : null);
        MetaField metaField = (metaFieldVO != null ? new MetaField(metaFieldVO) : null);
        MetaAccessRights accessRights = (accessRightsVO != null ? new MetaAccessRights(accessRightsVO) : null);
        
        return convertMetaFieldToRsUIField(accessRights, metaField, metaFieldFormProperties, rightType, username, userRoles, recordSysId, true);
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //private apis
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private void transformModelToView()  throws Exception
    {
        viewx = new RsFormDTO();
        
        viewx.setId(dbView.getSys_id());
        viewx.setName(dbView.getUFormName());
        viewx.setDisplayName(dbView.getUDisplayName());
        viewx.setWindowTitle(dbView.getUWindowTitle());
        
        viewx.setFormName(dbView.getUFormName());
        viewx.setViewName(dbView.getUViewName());
        viewx.setHeaderVisible(dbView.getUIsHeaderVisible());
        viewx.setWikiName(dbView.getUWikiName());
        if(customTable != null)
        {
            viewx.setTableName(customTable.getUName());
            viewx.setTableSysId(customTable.getSys_id());
        }
        viewx.setWizard(dbView.ugetUIsWizard());
        viewx.setTableDisplayName(dbView.getUTableDisplayName());
        viewx.setViewRoles(accessRights.getUReadAccess());
        viewx.setEditRoles(accessRights.getUWriteAccess());
        viewx.setAdminRoles(accessRights.getUAdminAccess());
        viewx.setUsername(username);
        
        //panels
        viewx.setPanels(getFormPanels(dbView.getMetaxFormViewPanels()));            // MetaxFormViewPanel table

        //prepare the button panel
        viewx.setButtonPanel(getButtonPanel(dbView.getMetaControl()));  //MetaControl table
        
        
        
    }
    
    
    private void initialize() throws Exception
    {
        // get User roles
        userRoles = UserUtils.getUserRoles(username);
        if(userRoles == null || userRoles.size() == 0)
        {
            throw new Exception("User " + username + " does not have any roles.");
        } 
        
        //load the view in memory
        loadView();
        
        //validate if the user had rights
        boolean hasAccessRights = hasAccessRights();
        if(!hasAccessRights)
        {
            throw new Exception("User " + username + " does not have access rights for the view " + viewName);
        }
        
    }
    
    private boolean hasAccessRights() throws Exception 
    {
        boolean hasAccessRights = false;
        
        if (dbView != null)
        {
            // check the access rights of the user
            accessRights = dbView.getMetaAccessRights();
            if (accessRights != null)
            {
                String rights = null;// accessRights.getUWriteAccess(); //default as Edit access to the form in the rendered mode
                if (rightType == RightTypeEnum.admin)
                {
                    rights = accessRights.getUAdminAccess();
                }
                else
                // if (rightType == RightTypeEnum.view)
                {
                    rights =        (StringUtils.isNotBlank(accessRights.getUReadAccess()) ? accessRights.getUReadAccess() : "") 
                                    + "," 
                                    + (StringUtils.isNotBlank(accessRights.getUAdminAccess()) ? accessRights.getUAdminAccess() : "");
                }
                
                hasAccessRights = UserUtils.hasRole(username, userRoles, rights);
                if (!hasAccessRights)
                {
                    if (username.equalsIgnoreCase(dbView.getSysCreatedBy()) || username.equalsIgnoreCase(dbView.getSysUpdatedBy()))
                    {
                        hasAccessRights = true;
                    }
                }
            }
        }
        
        return hasAccessRights;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void loadView() throws Exception 
    {
        try
        {

          HibernateProxy.setCurrentUser(this.username);
        	HibernateProxy.execute(() -> {
                if (!userRoles.isEmpty())
                {
                    MetaFormViewDAO daoMetaFormView = HibernateUtil.getDAOFactory().getMetaFormViewDAO();
                    if (StringUtils.isNotEmpty(id))
                    {
                        dbView = daoMetaFormView.findById(id);
                    }
                    
                    if(dbView == null && StringUtils.isNotBlank(viewName))
                    {
                        //String sql = "from MetaFormView where LOWER(UViewName) = '" + viewName.trim().toLowerCase() + "'";
                        String sql = "from MetaFormView where LOWER(UViewName) = :UViewName";
                        Query q = HibernateUtil.createQuery(sql);
                        q.setParameter("UViewName", viewName.trim().toLowerCase());
                        List<MetaFormView> views = q.list();
                        if(views != null && views.size() > 0)
                        {
                            dbView = views.get(0);
                        }
                    }
                    
                    if(dbView != null)
                    {
                        //load the custom table for this form view if any
                        loadCustomTableForView(dbView.getUTableName());
                        
                        //CHECK - make the old form compatible with the new one
                        if(dbView.getMetaFormTabs() != null && dbView.getMetaFormTabs().size() > 0 && 
                           dbView.getMetaxFormViewPanels() != null && !dbView.getMetaxFormViewPanels().isEmpty())
                        {
                            makeOldFormCompatibleWithNewForm();
                        }

                        //load all the references
                        //metacontrol 
                        MetaControl metaControl = dbView.getMetaControl();
                        if(metaControl != null)
                        {
                            Collection<MetaControlItem> metaControlItems = metaControl.getMetaControlItems();
                            if(metaControlItems != null)
                            {
                                for(MetaControlItem metaControlItem : metaControlItems)
                                {
                                    Collection<MetaFormAction> metaFormActions = metaControlItem.getMetaFormActions();
                                    if(metaFormActions != null)
                                        metaFormActions.size();
                                    
                                    Collection<MetaxFieldDependency> metaxFieldDependencys = metaControlItem.getMetaxFieldDependencys();
                                    if(metaxFieldDependencys != null)
                                        metaxFieldDependencys.size();
                                    
                                }
                            }
                        }
                        
                        
                        //metasource
                        Collection<MetaSource> metaSources = dbView.getMetaSources();
                        if(metaSources != null)
                        {
                            for(MetaSource metaSource : metaSources)
                            {
                                MetaFieldProperties metaFieldProperties = metaSource.getMetaFieldProperties();
                                if(metaFieldProperties != null)
                                {
                                    Collection<MetaxFieldDependency> metaxFieldDependencys = metaFieldProperties.getMetaxFieldDependencys();
                                    if(metaxFieldDependencys != null)
                                        metaxFieldDependencys.size();
                                }
                            }
                        }
                        
                        
                        //form panels
                        Collection<MetaxFormViewPanel> metaxFormViewPanels = dbView.getMetaxFormViewPanels();
                        if(metaxFormViewPanels != null)
                        {
                            for(MetaxFormViewPanel metaxFormViewPanel : metaxFormViewPanels)
                            {
                                Collection<MetaFormTab> metaFormTabs = metaxFormViewPanel.getMetaFormTabs();
                                if(metaFormTabs != null)
                                {
                                    for(MetaFormTab metaFormTab : metaFormTabs)
                                    {
                                        Collection<MetaFormTabField> metaFormTabFields = metaFormTab.getMetaFormTabFields();
                                        if(metaFormTabFields != null)
                                        {
                                            for(MetaFormTabField metaFormTabField : metaFormTabFields)
                                            {
                                                MetaField metaField = metaFormTabField.getMetaField();
                                                if(metaField != null)
                                                {
                                                    MetaFieldProperties prop = metaField.getMetaFieldProperties();
                                                    if(prop != null)
                                                    {
                                                        Collection<MetaxFieldDependency> metaxFieldDependencys = prop.getMetaxFieldDependencys();
                                                        if(metaxFieldDependencys != null)
                                                            metaxFieldDependencys.size();
                                                    }
                                                }
                                                
                                                MetaSource metaSource = metaFormTabField.getMetaSource();
                                                if(metaSource != null)
                                                {
                                                    MetaFieldProperties prop = metaSource.getMetaFieldProperties();
                                                    if(prop != null)
                                                    {
                                                        Collection<MetaxFieldDependency> metaxFieldDependencys = prop.getMetaxFieldDependencys();
                                                        if(metaxFieldDependencys != null)
                                                            metaxFieldDependencys.size();
                                                    }
                                                }
                                                
                                                MetaFieldProperties metaFieldFormProperties = metaFormTabField.getMetaFieldFormProperties();
                                                if(metaFieldFormProperties != null)
                                                {
                                                    Collection<MetaxFieldDependency> metaxFieldDependencys = metaFieldFormProperties.getMetaxFieldDependencys();
                                                    if(metaxFieldDependencys != null)
                                                        metaxFieldDependencys.size();
                                                }
                                            }
                                        }
                                        
                                        
                                        Collection<MetaxFieldDependency> metaxFieldDependencys = metaFormTab.getMetaxFieldDependencys();
                                        if(metaxFieldDependencys != null)
                                            metaxFieldDependencys.size();
                                    }
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        throw new Exception("View with name " + viewName + " does not exist." );
                    }
                }
                
                
        	});
        }
        catch (Throwable t)
        {
            Log.log.warn(t.getMessage());
            throw new Exception(t);
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void loadCustomTableForView(String customTableName) throws Exception
    {
        if(StringUtils.isNotBlank(customTableName))
        {
            String sql = "from CustomTable where LOWER(UName) = '" + customTableName.trim().toLowerCase() + "'";
            Query q = HibernateUtil.createQuery(sql);
            List<CustomTable> views = q.list();
            if(CollectionUtils.isNotEmpty(views)) {
                this.customTable = views.get(0);
            } else {
                dbView.setUTableName(null);
            }
            
        }
    }
    
    private void makeOldFormCompatibleWithNewForm()
    {
        Collection<MetaFormTab> tabs = dbView.getMetaFormTabs();
        
        //create panel 
        MetaxFormViewPanel panelx = new MetaxFormViewPanel();
        panelx.setUOrder(1);
        panelx.setMetaFormTabs(dbView.getMetaFormTabs());
        panelx.setUNoOfVerticalColumns(1);
        panelx.setMetaFormView(dbView);
        
        //save the panel
        HibernateUtil.getDAOFactory().getMetaxFormViewPanelDAO().persist(panelx);

        //add the reference of the new panel
        for(MetaFormTab tab : tabs)
        {
            tab.setMetaxFormViewPanel(panelx);
            tab.setMetaFormView(null);
            
            //save it
            HibernateUtil.getDAOFactory().getMetaFormTabDAO().persist(tab);
        }                    

        
        //add the panel to the view
        List<MetaxFormViewPanel> newDbObjs = new ArrayList<MetaxFormViewPanel>();
        newDbObjs.add(panelx);
        
        dbView.setMetaxFormViewPanels(newDbObjs);
        
        //remove the tab from the view
        dbView.setMetaFormTabs(null);
        
        //save the view
        HibernateUtil.getDAOFactory().getMetaFormViewDAO().persist(dbView);
        
        //reload the object again
        dbView = HibernateUtil.getDAOFactory().getMetaFormViewDAO().findById(dbView.getSys_id());
        
    }
    
    // comparator for MetaxFormViewPanel sorting
    private class ComparatorMetaxFormViewPanel implements Comparator<MetaxFormViewPanel>
    {
        public final int compare(MetaxFormViewPanel a, MetaxFormViewPanel b)
        {
            // order by column
            int value = a.getUOrder() - b.getUOrder();
            if (value == 0)
            {
                // order by
                value = a.getUOrder() - b.getUOrder();
            }
            return value;
        } // end compare

    } // ComparatorBaseModel
    
    // comparator for tab sorting
    private class ComparatorMetaFormTab implements Comparator<MetaFormTab>
    {
        public final int compare(MetaFormTab a, MetaFormTab b)
        {
            // order by column
            int value = a.getUOrder() - b.getUOrder();
            return value;
        } // end compare

    } // ComparatorMetaFormTab

    // comparator for action sorting
    private class ComparatorMetaFormAction implements Comparator<MetaFormAction>
    {
        public final int compare(MetaFormAction a, MetaFormAction b)
        {
            return a.getUSequence() - b.getUSequence();
        } // end compare

    } // ComparatorMetaFormAction

    // comparator for action sorting
    private class ComparatorMetaControlItem implements Comparator<MetaControlItem>
    {
        public final int compare(MetaControlItem a, MetaControlItem b)
        {
            return a.getUSequence() - b.getUSequence();
        } // end compare

    } // ComparatorMetaControlItem
    
    // comparator for metafield sorting
    private class ComparatorMetaFormTabField implements Comparator<MetaFormTabField>
    {
        public final int compare(MetaFormTabField a, MetaFormTabField b)
        {
            // order by column
            int value = a.getUColumnNumber() - b.getUColumnNumber();
            if (value == 0)
            {
                // order by
                value = a.getUOrder() - b.getUOrder();
            }
            return value;
        } // end compare

    } // ComparatorBaseModel
    
    private List<RsPanelDTO> getFormPanels(Collection<MetaxFormViewPanel> metaxFormViewPanels)
    {
        List<RsPanelDTO> panels = new ArrayList<RsPanelDTO>();
        
        if(metaxFormViewPanels != null && metaxFormViewPanels.size() > 0)
        {
            //sorting based on order #
            ArrayList<MetaxFormViewPanel> sortedPanelList = new ArrayList<MetaxFormViewPanel>(metaxFormViewPanels);
            Collections.sort(sortedPanelList, new ComparatorMetaxFormViewPanel());
            
            //for each panel
            for(MetaxFormViewPanel dbPanel: sortedPanelList)
            {
                RsPanelDTO panel = new RsPanelDTO();
                panel.setId(dbPanel.getSys_id());
                panel.setXtype(dbPanel.getUXType());
                panel.setDisplayName(dbPanel.getUPanelTitle());
                panel.setOrderNumber(dbPanel.getUOrder());
                panel.setCols(dbPanel.getUNoOfVerticalColumns());
                panel.setUrl(dbPanel.getUUrl());
                
//              panel.setName(name);
//                panel.setMandatory(isMandatory);
//                panel.setEncrypted(isEncrypted);
//              panel.setIsTabPanel(dbPanel.getUIsTabPanel());
                
                //a panel can have 1 or 2 cols - add cols only if url is not set
                if (StringUtils.isEmpty(dbPanel.getUUrl()))
                {
                    List<RsTabDTO> tabs = prepareTabs(dbPanel.getMetaFormTabs());
                    
                    if (metaxFormViewPanels.size() == 1 && 
                        (dbPanel.getMetaFormTabs() == null || dbPanel.getMetaFormTabs().isEmpty()) && 
                        (tabs == null || tabs.isEmpty()))
                    {
                        // Prepare default tab
                        RsTabDTO tab = new RsTabDTO();
                        tab.setOrderNumber(1);
                        tab.setCols(1);
                        List<RsColumnDTO> panelCols = new ArrayList<RsColumnDTO>();
                        panelCols.add(new RsColumnDTO());
                        tab.setColumns(panelCols);
                        
                        if (tabs == null)
                        {
                            tabs = new ArrayList<RsTabDTO>();
                        }
                        
                        tabs.add(tab);
                    }
                    
                    panel.setTabs(tabs);
                }

                panels.add(panel);
            }
        }
        
        return panels;
    }//getFormPanels
    
    private List<RsTabDTO> prepareTabs(Collection<MetaFormTab> metaFormTabs)
    {
        List<RsTabDTO> tabs = new ArrayList<RsTabDTO>();
        
        if(metaFormTabs!= null && metaFormTabs.size() > 0)
        {
            ArrayList<MetaFormTab> sortedTabList = new ArrayList<MetaFormTab>(metaFormTabs);
            Collections.sort(sortedTabList, new ComparatorMetaFormTab());
            
            if (sortedTabList != null && sortedTabList.size() > 0)
            {
                for (MetaFormTab metaFormTab : sortedTabList)
                {
                    //for each tab
                    tabs.add(convertMetaFormTabToRsTabDTO(metaFormTab));
    
                }// end of for loop
            }// end if tab
        }
        
        return tabs;
    }
    
    private RsTabDTO convertMetaFormTabToRsTabDTO(MetaFormTab metaFormTab)
    {
        RsTabDTO tab = new RsTabDTO();
        tab.setId(metaFormTab.getSys_id());
        tab.setName(metaFormTab.getUTabName());
        
        if (metaFormTab.getUOrder() != null)
        {
            tab.setOrderNumber(metaFormTab.getUOrder().intValue());
        }
//        tab.setPosition(metaFormTab.getUPosition());
//        tab.setTabWidth(metaFormTab.getUTabWidth().intValue());
//        tab.setSelectedDefault(metaFormTab.getUIsSelectedDefault() != null ? metaFormTab.getUIsSelectedDefault().booleanValue() : false);
        tab.setCols(metaFormTab.getUNoOfVerticalColumns());
        tab.setUrl(metaFormTab.getUUrl());
        
        //dependencies for the tab
        tab.setDependencies(getMetaxFieldDependency(metaFormTab.getMetaxFieldDependencys()));

        // if URL is set, then there will be no fields
        if (StringUtils.isEmpty(metaFormTab.getUUrl()))
        {
            int cols = metaFormTab.getUNoOfVerticalColumns();
            if(cols <= 1)
            {
                tab.setColumns(get1ColumnForTabPanel(metaFormTab.getMetaFormTabFields()));
            }
            else if(cols == 2)
            {
                tab.setColumns(get2ColumnForTabPanel(metaFormTab.getMetaFormTabFields()));
            }            
            
        }// end of if

        return tab;
    }// convertMetaFormTabToView
    
    private List<RsFieldDependency> getMetaxFieldDependency(Collection<MetaxFieldDependency> dependencies)
    {
        List<RsFieldDependency> uiDependencies = null;
        if(dependencies != null && dependencies.size() > 0)
        {
            uiDependencies = new ArrayList<RsFieldDependency>();
            for(MetaxFieldDependency dep: dependencies)
            {
                RsFieldDependency uidep = new RsFieldDependency();
                uidep.setId(dep.getSys_id());
                uidep.setAction(dep.getUAction());
                uidep.setCondition(dep.getUCondition());
                uidep.setTarget(dep.getUTarget());
                uidep.setValue(dep.getUValue());
                uidep.setActionOptions(dep.getUActionOptions());
                
                uiDependencies.add(uidep);
            }//end of for loop
        }//end of if
        
        return uiDependencies;
    }
    
    private List<RsColumnDTO> get1ColumnForTabPanel(Collection<MetaFormTabField> metaFormTabFields)
    {
        List<RsColumnDTO> panelCols = new ArrayList<RsColumnDTO>();
        
        RsColumnDTO col1 = new RsColumnDTO();
        List<RsUIField> fields1 = new ArrayList<RsUIField>();
        
        if(metaFormTabFields != null && metaFormTabFields.size() > 0)
        {
            ArrayList<MetaFormTabField> sortedTabList = new ArrayList<MetaFormTabField>(metaFormTabFields);
            Collections.sort(sortedTabList, new ComparatorMetaFormTabField());
            
            for (MetaFormTabField metaFormTabField : sortedTabList)
            {
                RsUIField field = null;
                MetaField metaField = metaFormTabField.getMetaField();
                MetaSource metaSource = metaFormTabField.getMetaSource();

                if (metaField != null)
                {
                    // metaField - DB field
                    field = convertMetaFieldToRsUIField(this.accessRights, metaField, metaFormTabField.getMetaFieldFormProperties(), rightType, username, userRoles, this.recordSysId, this.isFormDefinitionOnly());
                }
                else if (metaSource != null)
                {
                    // metasource - INPUT field
                    field = convertMetaSourceToRsUIField(metaSource, rightType, username, userRoles, this.recordSysId);
                }
                
                if(field != null)
                {
                    field.setOrderNumber(metaFormTabField.getUOrder());
                    field.setColNumber(metaFormTabField.getUColumnNumber());//it will always be 1
                    
                    fields1.add(field);
                }
            }// end of for loop
            
            col1.setFields(fields1);
            
            panelCols.add(col1);
        }
        
        return panelCols;
    }
    
    private List<RsColumnDTO> get2ColumnForTabPanel(Collection<MetaFormTabField> metaFormTabFields)
    {
        List<RsColumnDTO> panelCols = new ArrayList<RsColumnDTO>();
        
        RsColumnDTO col1 = new RsColumnDTO();
        RsColumnDTO col2 = new RsColumnDTO();
        
        //the reason to do this way is that we will execute the for loop only once and not twice. that will be better performance 
        List<RsUIField> fields1 = new ArrayList<RsUIField>();
        List<RsUIField> fields2 = new ArrayList<RsUIField>();

        if(metaFormTabFields != null && metaFormTabFields.size() > 0)
        {
            //sort the field positions
            ArrayList<MetaFormTabField> sortedList = new ArrayList<MetaFormTabField>(metaFormTabFields);
            Collections.sort(sortedList, new ComparatorMetaFormTabField());

            for(MetaFormTabField metaFormTabField : sortedList)
            {
                RsUIField field = null;
                int colNo = metaFormTabField.getUColumnNumber();
                MetaField metaField = metaFormTabField.getMetaField();
                MetaSource metaSource = metaFormTabField.getMetaSource();
                
                if (metaField != null)
                {
                    // metaField - DB field
                    field = convertMetaFieldToRsUIField(this.accessRights, metaField, metaFormTabField.getMetaFieldFormProperties(), rightType, username, userRoles, this.recordSysId, isFormDefinitionOnly());
                }
                else if (metaSource != null)
                {
                    // metasource - INPUT field
                    field = convertMetaSourceToRsUIField(metaSource, rightType, username, userRoles, this.recordSysId);
                }
                
                if(field != null)
                {
                    field.setOrderNumber(metaFormTabField.getUOrder());
                    field.setColNumber(colNo);
    
                    if(colNo == 1)
                    {
                        fields1.add(field);
                    }
                    else
                    {
                        fields2.add(field);
                    }
                }
            }//end of for loop
            
            col1.setFields(fields1);
            col2.setFields(fields2);
            
            panelCols.add(col1);
            panelCols.add(col2);
        }        
        
        return panelCols;
    }
    
    private RsUIField convertMetaSourceToRsUIField(MetaSource metaSource, RightTypeEnum rightType, String userId, Set<String> userRoles, String recordSysId)
    {
        RsUIField field = null;
        
        if(metaSource != null)
        {
            field = new RsUIField();
            field.setSourceType(HibernateConstantsEnum.INPUT.name());
            field.setId(metaSource.getSys_id());
            field.setName(metaSource.getUName());
            field.setDisplayName(metaSource.getUDisplayName());
            field.setDbtype(metaSource.getUDbType());
//          field.setDbtable(metaSource.getUTable());
//          field.setDbcolumn(metaSource.getUColumn());
//          field.setColumnModelName(metaSource.getUColumnModelName());
//          field.setRstype(metaSource.getUType());
            
            //set additional values
            setFieldProperties(field, metaSource.getMetaFieldProperties(), null, rightType, userId, userRoles, recordSysId, isFormDefinitionOnly());
        }
        
        return field;
    }
    
    //use the metaFieldFormProperties if available
    private static RsUIField convertMetaFieldToRsUIField(MetaAccessRights accessRights, MetaField metaField, MetaFieldProperties metaFieldFormProperties, 
                    RightTypeEnum rightType, String userId, Set<String> userRoles, String recordSysId, boolean formDefinitionOnly)
    {
        RsUIField field = null;
        
        if(metaField != null)
        {
            /**
             * if the form is in View mode, then all fields in Text/Read Only 
             * 
             * if the form is in Edit mode, then for each field
             * check if the user has Edit rights 
             * If yes, then no issue and render the field
             * If no, then check if the user has Read rights on the field
             *      If yes, then make this field as Text/RO
             *      If no, then make it hidden
             * 
             */            
            
            field = new RsUIField();
            field.setSourceType(HibernateConstantsEnum.DB.name());
            field.setId(metaField.getSys_id());
            field.setName(metaField.getUName());
            field.setDbtable(metaField.getUTable());
            field.setDbcolumn(metaField.getUColumn());
            field.setDbcolumnId(metaField.getSys_id());
            field.setColumnModelName(metaField.getUColumnModelName());
            field.setDisplayName(metaField.getUDisplayName());
//            field.setRstype(metaField.getUType());
            field.setDbtype(metaField.getUDbType());
            
            //if the form properties are not set, then use the default field setting for this form
            if(metaFieldFormProperties == null)
            {
                metaFieldFormProperties = metaField.getMetaFieldProperties();
            }
            else
            {
                //if the display name is available from the form meta field properties record, get it 
                if(StringUtils.isNotBlank(metaFieldFormProperties.getUDisplayName()))
                {
                    field.setDisplayName(metaFieldFormProperties.getUDisplayName());
                }
            }
            
            if(accessRights == null && rightType != RightTypeEnum.admin)
            {
                accessRights = metaField.getMetaAccessRights();
            }
            
            //set additional values
            //** NOte that we are using the form access rights instead of the field now for the DB type
            setFieldProperties(field, metaFieldFormProperties, accessRights, rightType, userId, userRoles, recordSysId, formDefinitionOnly);
        }
        
        return field;
    }
    
    private static void setFieldProperties(RsUIField field, MetaFieldProperties metaFieldProperties, MetaAccessRights metaAccessRights, RightTypeEnum rightType, 
                    String userId, Set<String> userRoles, String recordSysId, boolean formDefinitionOnly)
    {
        if(metaFieldProperties != null)
        {
            //apply the json to the model - if we do this within the transaction, somehow it behaves weird with unexpected results
            Collection<MetaxFieldDependency> mtFldDependencies = metaFieldProperties.getMetaxFieldDependencys();
            metaFieldProperties.setMetaxFieldDependencys(null);
            metaFieldProperties.setMetaxFieldDependencys(mtFldDependencies);
            
            String uitype = metaFieldProperties.getUUIType();
//            String uixtype = metaFieldProperties.getUUIXype();
            
//            if(StringUtils.isNotEmpty(uixtype))
//            {
                //TODO - one of the 2 will be removed 
//                field.setUiXtype(uixtype);
//                field.setXtype(uixtype);
//            }
//            else
//            {
                //lookup and set the value
//                uixtype = rsGxtTypeToUITypeMapping.get(uitype);
                
                //set the value - it will persist in the db 
//                metaFieldProperties.setUUIXype(uixtype);
                
                //for older recs, the xtype may be null, so use the map
//                field.setUiXtype(uixtype);
//                field.setXtype(uixtype);
//            }
            
            //validation of access rights for DB type
            if(field.getSourceType().equals(HibernateConstantsEnum.DB.name()))
            {
                //for the FB, type is admin, so the original value
                if(rightType == RightTypeEnum.admin)
                {
                    field.setUiType(uitype);
                }
                else
                {
                    //this if for the form render now
                    //if the form needs to be VIEWED, then all the fields will be TEXTONLY
//                    if(rightType == RightTypeEnum.view)
//                    {
//                        field.setUiType(WorkflowConstants.UI_READONLYTEXTFIELD.getTagName());
//                    }
//                    else
//                    {
                        /*this is righttype = Edit
                        * check if the user has Edit rights 
                        * If yes, then no issue and render the field
                        * If no, then check if the user has Read rights on the field
                        *      If yes, then make this field as Text/RO
                        *      If no, then make it hidden
                        */
                        boolean hasFieldAccessRights = UserUtils.hasRole(userId, userRoles, metaAccessRights.getUWriteAccess());
                        if(hasFieldAccessRights)
                        {
                            //this means user has EDIT rights on this field, so render as it
                            field.setUiType(uitype);
                        }
                        else
                        {
                            //check if the user has VIEW rights on this field
                            hasFieldAccessRights = UserUtils.hasRole(userId, userRoles, metaAccessRights.getUReadAccess());
                            if(hasFieldAccessRights)
                            {
                                //if the user has VIEW rights than make the field read only text
                                field.setUiType(HibernateConstantsEnum.UI_READONLYTEXTFIELD.getTagName());
                            }
                            else
                            {
                                //make this field as HIDDEN
                                field.setUiType(HibernateConstantsEnum.UI_HIDDEN.getTagName());
                            }
                        }
//                    }                    
                }
            }
            else
            {
                //for INPUT types not validation required
                field.setUiType(uitype);
            }
            
            
            field.setJs(metaFieldProperties.getUJavascript());
            field.setSize(metaFieldProperties.getUSize());
            field.setEncrypted(metaFieldProperties.getUIsCrypt() != null ? metaFieldProperties.getUIsCrypt() : false);
            field.setMandatory(metaFieldProperties.getUIsMandatory() != null ? metaFieldProperties.getUIsMandatory() : false);
            
            // for sequence, generate the # that will be used as default
            // same as FormUtil.java -> UI_SEQUENCE
            if (metaFieldProperties.getUUIType().equalsIgnoreCase(HibernateConstantsEnum.UI_SEQUENCE.getTagName()))
            {
                //if it is not in form builder, then get the value
                if (StringUtils.isEmpty(recordSysId) && rightType != RightTypeEnum.admin)
                {
                    if (StringUtils.isNotEmpty(metaFieldProperties.getUSequencePrefix()))
                    {
                        if (formDefinitionOnly)
                        {
                            field.setDefaultValue(null);
                        }
                        else
                        {
                            // String seq = HibernateUtil.getNextSequenceString(properties.getUSequencePrefix());
                            String seq = SequenceUtils.getNextSequence(metaFieldProperties.getUSequencePrefix());
                            field.setDefaultValue(seq);
                        }
                    }
                }
                else
                {
                    field.setDefaultValue(null);
                }
            }
            else
            {
                field.setDefaultValue(metaFieldProperties.getUDefaultValue());
            }
            
            
            field.setWidth(metaFieldProperties.getUWidth());
            field.setHeight(metaFieldProperties.getUHeight());
            field.setLabelAlign(metaFieldProperties.getULabelAlign());
            field.setBooleanMaxLength(metaFieldProperties.getUBooleanMaxLength());
            field.setStringMinLength(metaFieldProperties.getUStringMinLength());
            field.setStringMaxLength(metaFieldProperties.getUStringMaxLength());
            field.setUiStringMaxLength(metaFieldProperties.getUUIStringMaxLength());
            field.setIsMultiSelect(metaFieldProperties.ugetUSelectIsMultiSelect());
            field.setSelectUserInput(metaFieldProperties.ugetUSelectUserInput());
            field.setSelectMaxDisplay(metaFieldProperties.getUSelectMaxDisplay());
            field.setSelectValues(metaFieldProperties.getUSelectValues());
            field.setChoiceValues(metaFieldProperties.getUChoiceValues());
            field.setCheckboxValues(metaFieldProperties.getUCheckboxValues());
            field.setDateTimeHasCalendar(metaFieldProperties.getUDateTimeHasCalendar());
            field.setIntegerMinValue(metaFieldProperties.getUIntegerMinValue());
            field.setIntegerMaxValue(metaFieldProperties.getUIntegerMaxValue());
            field.setDecimalMinValue(metaFieldProperties.getUDecimalMinValue());
            field.setDecimalMaxValue(metaFieldProperties.getUDecimalMaxValue());
            field.setJournalRows(metaFieldProperties.getUJournalRows());
            field.setJournalColumns(metaFieldProperties.getUJournalColumns());
            field.setJournalMinValue(metaFieldProperties.getUJournalMinValue());
            field.setJournalMaxValue(metaFieldProperties.getUJournalMinValue());
            field.setListRows(metaFieldProperties.getUListRows());
            field.setListColumns(metaFieldProperties.getUListColumns());
            field.setListMaxDisplay(metaFieldProperties.getUListMaxDisplay());
            field.setListValues(metaFieldProperties.getUListValues());
            field.setReferenceTable(metaFieldProperties.getUReferenceTable());
            field.setReferenceDisplayColumn(metaFieldProperties.getUReferenceDisplayColumn());
            field.setReferenceDisplayColumnList(metaFieldProperties.getUReferenceDisplayColumnList());
            field.setReferenceTarget(metaFieldProperties.getUReferenceTarget());
            field.setReferenceParams(metaFieldProperties.getUReferenceParams());
            field.setLinkTarget(metaFieldProperties.getULinkTarget());
            field.setLinkParams(metaFieldProperties.getULinkParams());
            field.setLinkTemplate(metaFieldProperties.getULinkTemplate());
            field.setSequencePrefix(metaFieldProperties.getUSequencePrefix());
            field.setHiddenValue(metaFieldProperties.getUHiddenValue());
            
            field.setGroups(metaFieldProperties.getUGroups());
            field.setUsersOfTeams(metaFieldProperties.getUUsersOfTeams());
            field.setRecurseUsersOfTeam(metaFieldProperties.ugetURecurseUsersOfTeam());
            field.setTeamsOfTeams(metaFieldProperties.getUTeamsOfTeams());
            field.setRecurseTeamsOfTeam(metaFieldProperties.ugetURecurseTeamsOfTeam());
            field.setAllowAssignToMe(metaFieldProperties.ugetUAllowAssignToMe());
            field.setAutoAssignToMe(metaFieldProperties.ugetUAutoAssignToMe());
            
            field.setTeamPickerTeamsOfTeams(metaFieldProperties.getUTeamPickerTeamsOfTeams());
            
            field.setHidden(metaFieldProperties.ugetUIsHidden());
            field.setReadOnly(metaFieldProperties.ugetUIsReadOnly());
            
            field.setTooltip(metaFieldProperties.getUTooltip());
            
            //file upload properties
            field.setFileUploadTableName(metaFieldProperties.getUFileUploadTableName());
            field.setReferenceColumnName(metaFieldProperties.getUFileUploadReferenceColumnName());
            field.setAllowUpload(metaFieldProperties.getUAllowUpload());
            field.setAllowDownload(metaFieldProperties.getUAllowDownload());
            field.setAllowRemove(metaFieldProperties.getUAllowRemove());
            field.setAllowedFileTypes(metaFieldProperties.getUAllowFileTypes());
            
            field.setWidgetColumns(metaFieldProperties.getUWidgetColumns());

            //for grid ref widget
            field.setRefGridReferenceByTable(metaFieldProperties.getURefGridReferenceByTable());
            field.setRefGridSelectedReferenceColumns(metaFieldProperties.getURefGridSelectedReferenceColumns());
            field.setRefGridReferenceColumnName(metaFieldProperties.getURefGridReferenceColumnName());
            field.setAllowReferenceTableAdd(metaFieldProperties.getUAllowReferenceTableAdd());
            field.setAllowReferenceTableRemove(metaFieldProperties.getUAllowReferenceTableRemove());
            field.setAllowReferenceTableView(metaFieldProperties.getUAllowReferenceTableView());
            field.setReferenceTableUrl(metaFieldProperties.getUReferenceTableUrl());
            field.setRefGridViewPopup(metaFieldProperties.getURefGridViewPopup());
            field.setRefGridViewPopupWidth(metaFieldProperties.getURefGridViewPopupWidth());
            field.setRefGridViewPopupHeight(metaFieldProperties.getURefGridViewPopupHeight());
            field.setRefGridViewTitle(metaFieldProperties.getURefGridViewPopupTitle());
            
            // for choice additions
            field.setChoiceTable(metaFieldProperties.getUChoiceTable());
            field.setChoiceDisplayField(metaFieldProperties.getUChoiceDisplayField());
            field.setChoiceValueField(metaFieldProperties.getUChoiceValueField());
            field.setChoiceWhere(metaFieldProperties.getUChoiceWhere());
            field.setChoiceField(metaFieldProperties.getUChoiceField());
            field.setChoiceSql(metaFieldProperties.getUChoiceSql());
            field.setChoiceOptionSelection(metaFieldProperties.getUChoiceOptionSelection());

            // for spacer
            field.setShowHorizontalRule(metaFieldProperties.getUShowHorizontalRule());
            
            //for journal
            field.setAllowJournalEdit(metaFieldProperties.getUAllowJournalEdit());
            
            //for columns
            field.setSectionNumberOfColumns(metaFieldProperties.getUSectionNumberOfColumns());
            field.setSectionType(metaFieldProperties.getUSectionType());
            field.setSectionLayout(metaFieldProperties.getUSectionLayout());
            field.setSectionTitle(metaFieldProperties.getUSectionTitle());
            field.setSectionColumns(evaluateSectionColumns(metaFieldProperties, formDefinitionOnly));
            
            //dependencies for the field
            field.setDependencies(getMetaxFieldDependencies(metaFieldProperties.getMetaxFieldDependencys()));
            
            // for transient fields
            field.setTransientFields(metaFieldProperties.getTransientFieldsJsonString());
        }
    }
    
    private static List<RsFieldDependency> getMetaxFieldDependencies(Collection<MetaxFieldDependency> dependencies)
    {
        List<RsFieldDependency> uiDependencies = null;
        if(dependencies != null && dependencies.size() > 0)
        {
            uiDependencies = new ArrayList<RsFieldDependency>();
            for(MetaxFieldDependency dep: dependencies)
            {
                RsFieldDependency uidep = new RsFieldDependency();
                uidep.setId(dep.getSys_id());
                uidep.setAction(dep.getUAction());
                uidep.setCondition(dep.getUCondition());
                uidep.setTarget(dep.getUTarget());
                uidep.setValue(dep.getUValue());
                uidep.setActionOptions(dep.getUActionOptions());
                
                uiDependencies.add(uidep);
            }//end of for loop
        }//end of if
        
        return uiDependencies;
    }
    
    private RsButtonPanelDTO getButtonPanel(MetaControl metaControl)
    {
        RsButtonPanelDTO panel = null;
        
        if(metaControl != null)
        {
            panel = new RsButtonPanelDTO();
            panel.setId(metaControl.getSys_id());
            panel.setName(metaControl.getUName());
            panel.setDisplayName(metaControl.getUDisplayName());
            
            //control items
            Collection<MetaControlItem> controlItems = metaControl.getMetaControlItems();
            ArrayList<MetaControlItem> sortedList = new ArrayList<MetaControlItem>(controlItems);
            Collections.sort(sortedList, new ComparatorMetaControlItem());
            if (sortedList != null && sortedList.size() > 0)
            {
                panel.setControls(getButtons(sortedList));
                
            }// end of if
            
        }

        return panel;
    }
    
    private List<RsUIButton> getButtons(Collection<MetaControlItem> controlItems)
    {
        List<RsUIButton> buttons = new ArrayList<RsUIButton>();
        
        if(controlItems != null && controlItems.size() > 0)
        {
            for(MetaControlItem controlItem : controlItems)
            {
                buttons.add(getButton(controlItem));
            }
        }

        return buttons;
        
    }//getButtons
    
    private RsUIButton getButton(MetaControlItem controlItem)
    {
        RsUIButton button = null;
        
        if(controlItem != null)
        {
            button = new RsUIButton();
            button.setId(controlItem.getSys_id());
            button.setName(controlItem.getUName());
            button.setDisplayName(controlItem.getUDisplayName());
//            button.setGroup_name(controlItem.getUGroupName());
//            button.setGroup_display_name(controlItem.getUGroupDisplayName());
            button.setType(controlItem.getUType());
//            button.setParam(controlItem.getUParam());
            button.setOrderNumber(controlItem.getUSequence());
            button.setCustomTableDisplay(controlItem.ugetUCustomTableDisplay());
            JsonObject propertiesJSON = new JsonObject();
            if (controlItem.getUPropertiesJSON() != null) 
            {
                propertiesJSON = new JsonParser().parse(controlItem.getUPropertiesJSON()).getAsJsonObject();
            }
            button.setBackgroundColor((propertiesJSON.get("backgroundColor") != null && !propertiesJSON.get("backgroundColor").isJsonNull()) ? propertiesJSON.get("backgroundColor").getAsString() : null);
            button.setFontColor((propertiesJSON.get("fontColor") != null && !propertiesJSON.get("fontColor").isJsonNull()) ? propertiesJSON.get("fontColor").getAsString() : null);
            button.setFont((propertiesJSON.get("font") != null && !propertiesJSON.get("font").isJsonNull()) ? propertiesJSON.get("font").getAsString() : null);
            button.setFontSize((propertiesJSON.get("fontSize") != null && !propertiesJSON.get("fontSize").isJsonNull()) ? propertiesJSON.get("fontSize").getAsString() : null);

            Collection<MetaFormAction> metaFormActions = controlItem.getMetaFormActions();
            ArrayList<MetaFormAction> sortedActionList = new ArrayList<MetaFormAction>(metaFormActions);
            Collections.sort(sortedActionList, new ComparatorMetaFormAction());
            if (sortedActionList != null && sortedActionList.size() > 0)
            {
                List<RsButtonAction> buttonActions = new ArrayList<RsButtonAction>();
                
                for (MetaFormAction action : sortedActionList)
                {
                    buttonActions.add(getButtonAction(action));
                }
                
                button.setActions(buttonActions);
            }// end if if
            
            //dependencies for the button
            button.setDependencies(getMetaxFieldDependency(controlItem.getMetaxFieldDependencys()));

        }
        return button;
    }
    
    private RsButtonAction getButtonAction(MetaFormAction action)
    {
        RsButtonAction buttonAction = null;
        
        if (action != null)
        {
            buttonAction = new RsButtonAction();
            buttonAction.setId(action.getSys_id());
            buttonAction.setOrderNumber(action.getUSequence());
            buttonAction.setCrudAction(action.getUCrudAction());
            buttonAction.setAdditionalParam(action.getUAdditionalParams());
            buttonAction.setOperation(action.getUOperation());
            buttonAction.setRunbook(action.getURunbookName());
            buttonAction.setScript(action.getUScriptName());
            buttonAction.setActionTask(action.getUActionTaskName());
            buttonAction.setOrderNumber(action.getUSequence());
            buttonAction.setRedirectUrl(action.getURedirectUrl());
            buttonAction.setRedirectTarget(action.getURedirectTarget());
        }
        
        return buttonAction;
    }
    
    //the section column can have DB type fields. Set the Default Value for those fields. 
    private static String evaluateSectionColumns(MetaFieldProperties metaFieldProperties, boolean formDefinitionOnly)
    {
        String result = metaFieldProperties.getUSectionColumns();
        
        if(StringUtils.isNotBlank(result))
        {
            String jsonString = result;
            jsonString = jsonString.substring(1);//remove the first '['
            jsonString = jsonString.substring(0, jsonString.length()-1);//remove the last ']'
            
            // parse json to get the columns - Refer RsFormDTO.populateDBFields also for this transformation
            try
            {
                RsControls test = new ObjectMapper().readValue(jsonString, RsControls.class);
                if(test != null)
                {
                    for(RsUIField rf : test.getControls())
                    {
                        if (rf != null)
                        {
                            if (StringUtils.isNotEmpty(rf.getSourceType()) && rf.getSourceType().equalsIgnoreCase(HibernateConstantsEnum.DB.name()))
                            {
                                if (rf.getUiType().equalsIgnoreCase(HibernateConstantsEnum.UI_SEQUENCE.getTagName()) && StringUtils.isNotEmpty(rf.getSequencePrefix()))
                                {
                                    if (!formDefinitionOnly)
                                    {
                                        rf.setDefaultValue(SequenceUtils.getNextSequence(rf.getSequencePrefix()));
                                    }
                                }
                            }
                        }
                    }//end of for loop
                    
                    jsonString = new ObjectMapper().writeValueAsString(test);
                    result = "[" + jsonString + "]";//add back the brackets 
                }
            }
            catch(Throwable t)
            {
                //don't do anything...
                Log.log.error("Error in evaluating the section column:" + result, t);
            }
        }
        
        
        return result;
    }
    
    public Map<String, Object> getFormComponents() throws Exception
    {
        Map<String, Object> result = new HashMap<>();
        Set<String> scripts = new HashSet<>();
        Set<String> wikiNames = new HashSet<>();
        Set<String> tasks = new HashSet<>();
        
        initialize();
        
        if(dbView != null && dbView.getMetaControl() != null && dbView.getMetaControl().getMetaControlItems() != null)
        {
            dbView.getMetaControl().getMetaControlItems().stream().forEach(metaControlItem -> {
                if (metaControlItem.getMetaFormActions() != null)
                {
                    metaControlItem.getMetaFormActions().stream().forEach(metaFormAction -> {
                        if(metaFormAction.getUOperation().equalsIgnoreCase(Operation.SCRIPT.getTagName()))
                        {
                            String scriptName = metaFormAction.getUScriptName();
                            ResolveSysScriptVO scriptVO = ServiceHibernate.findResolveSysScriptByIdOrName(null, scriptName.trim(), username);
                            if (scriptVO != null)
                                scripts.add(scriptName + ImpexEnum.COLON.getValue() + scriptVO.getSys_id());
                        }
                        if(metaFormAction.getUOperation().equalsIgnoreCase(Operation.RUNBOOK.getTagName()))
                        {
                            String wikiName = metaFormAction.getURunbookName();
                            if (StringUtils.isNotBlank(wikiName) && !wikiName.startsWith("$"))
                            {
                                String sysId = WikiUtils.getWikidocSysId(wikiName);
                                if (StringUtils.isNotBlank(sysId))
                                    wikiNames.add(wikiName + ImpexEnum.COLON.getValue() + sysId);
                            }
                        }
                        if(metaFormAction.getUOperation().equalsIgnoreCase(Operation.ACTIONTASK.getTagName()))
                        {
                            String taskName = metaFormAction.getUActionTaskName();
                            String sysId = ActionTaskUtil.getActionIdFromFullname(taskName, username);
                            if (StringUtils.isNotBlank(sysId))
                                tasks.add(taskName + ImpexEnum.COLON.getValue() + sysId);
                        }
                    });
                }
            });
            
            if (StringUtils.isNotBlank(dbView.getUTableName()))
            {
                List<String> customTables = new ArrayList<>();
                customTables.add(dbView.getUTableName() + ImpexEnum.COLON.getValue() + this.customTable.getSys_id());
                result.put("customTable", customTables);
            }
        }
        
        if(CollectionUtils.isNotEmpty(scripts))
        {
            result.put(ImpexEnum.SCRIPT.getValue(), scripts);
        }
        if(CollectionUtils.isNotEmpty(tasks))
        {
            result.put(ImpexEnum.TASK.getValue(), tasks);
        }
        if(CollectionUtils.isNotEmpty(wikiNames))
        {
            result.put(ImpexEnum.RUNBOOK.getValue(), wikiNames);
        }
        
        return result;
    }
    
    public boolean doesFormHasChildren() throws Exception
    {
        boolean result = false;
        
        initialize();
        
        if(dbView != null && dbView.getMetaControl() != null && dbView.getMetaControl().getMetaControlItems() != null)
        {
            if (StringUtils.isNotBlank(dbView.getUTableName()))
            {
                return true;
            }
            result = dbView.getMetaControl().getMetaControlItems().parallelStream().anyMatch(metaControlItem -> {
                boolean innerResult = false;
                
                if (CollectionUtils.isNotEmpty(metaControlItem.getMetaFormActions())) {
                    innerResult = metaControlItem.getMetaFormActions().parallelStream().anyMatch(metaFormAction -> {
                        String operation = metaFormAction.getUOperation();
                        if (operation.equalsIgnoreCase(Operation.SCRIPT.getTagName())
                                        || operation.equalsIgnoreCase(Operation.RUNBOOK.getTagName())
                                        || operation.equalsIgnoreCase(Operation.ACTIONTASK.getTagName())) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
                return innerResult;
            });
        }
        return result;
    }
}
