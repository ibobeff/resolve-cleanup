/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

public class MetricServerDTO
{
    private String serverName;
    private String serverGroup;
    private Integer serverHigh;
    private Integer serverLow;
    private String serverType;
    
    private String guid;
    
    private String regType;
    private String regIpaddress;
    
    
    public String getServerName()
    {
        return serverName;
    }
    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }
    public String getServerGroup()
    {
        return serverGroup;
    }
    public void setServerGroup(String serverGroup)
    {
        this.serverGroup = serverGroup;
    }
    public Integer getServerHigh()
    {
        return serverHigh;
    }
    public void setServerHigh(Integer serverHigh)
    {
        this.serverHigh = serverHigh;
    }
    public Integer getServerLow()
    {
        return serverLow;
    }
    public void setServerLow(Integer serverLow)
    {
        this.serverLow = serverLow;
    }
    public String getServerType()
    {
        return serverType;
    }
    public void setServerType(String serverType)
    {
        this.serverType = serverType;
    }
    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }
    public String getRegType()
    {
        return regType;
    }
    public void setRegType(String regType)
    {
        this.regType = regType;
    }
    public String getRegIpaddress()
    {
        return regIpaddress;
    }
    public void setRegIpaddress(String regIpaddress)
    {
        this.regIpaddress = regIpaddress;
    }
    
    
    
}
