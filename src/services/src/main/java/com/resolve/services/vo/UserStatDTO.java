/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

public class UserStatDTO
{
    private String username;
    private long postContributions;
    private long commentContributions;
    private long commentsReceived;
    private long likesReceivedForPost;
    private long likesReceivedForComment;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public long getCommentsReceived()
    {
        return commentsReceived;
    }

    public void setCommentsReceived(long commentsReceived)
    {
        this.commentsReceived = commentsReceived;
    }

    public long getPostContributions()
    {
        return postContributions;
    }

    public void setPostContributions(long postContributions)
    {
        this.postContributions = postContributions;
    }

    public long getCommentContributions()
    {
        return commentContributions;
    }

    public void setCommentContributions(long commentContributions)
    {
        this.commentContributions = commentContributions;
    }

    public long getLikesReceivedForPost()
    {
        return likesReceivedForPost;
    }

    public void setLikesReceivedForPost(long likesReceivedForPost)
    {
        this.likesReceivedForPost = likesReceivedForPost;
    }

    public long getLikesReceivedForComment()
    {
        return likesReceivedForComment;
    }

    public void setLikesReceivedForComment(long likesReceivedForComment)
    {
        this.likesReceivedForComment = likesReceivedForComment;
    }

    @Override
    public String toString()
    {
        return "UserStatDTO [username=" + username + ", postContributions=" + postContributions + ", commentContributions=" + commentContributions + ", commentsReceived=" + commentsReceived + ", likesReceivedForPost=" + likesReceivedForPost + ", likesReceivedForComment=" + likesReceivedForComment + "]";
    }
}
