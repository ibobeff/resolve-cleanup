/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.FlushMode;
import org.hibernate.query.Query;

import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFilter;
import com.resolve.persistence.model.MetaFormTabField;
import com.resolve.persistence.model.MetaSource;
import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;


public class DeleteCustomTable
{
//    private String tableName = null;
    private String tableSysid = null;
    private String username = null;

    private CustomTable table = null;

    public DeleteCustomTable(String sysId, String tableName,  String username)
    {
//        this.tableName = tableName;
        this.tableSysid = sysId;
        this.username = username;

        if (StringUtils.isEmpty(tableName) && StringUtils.isEmpty(sysId))
        {
            throw new RuntimeException("SysId or Table name is mandatory");
        }

        if (StringUtils.isNotEmpty(tableName))
        {
            table = new CustomTable();
            table.setUName(tableName);
        }
    }
    
    public Set<String> delete()
    {
        Set<String> relatedCustomTables = new HashSet<String>();

        loadCustomTable();

        if (table != null)
        {
            try
            {

                // drop the table first - IT SHOULD NOT BE IN TRANSACTION
                // delete referenced tables for this custom table
                // deleteReferecedTablesForCustomTable(customTable);
                // drop custom table

                // have to put this in try-catch as if there is an error like
                // table does not exist, you still need to clean up the related
                // records.
                
                /* Custom Tables will not be dropped from schema.
                 * It would be customer DBA's responsibility to take any
                 * appropriate actions such as backing up data before 
                 * actually dropping table from schema.
                 * 
                try
                {
                    HibernateUtil.deleteTable(table.getUName());
                }
                catch (Throwable t)
                {
                    Log.log.error("error while deleteing the concrete table " + table.getUName(), t);
                }
                 */
                
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
                
	                FlushMode flushModeB4 = HibernateUtil.getCurrentSession().getHibernateFlushMode();
	                
	                Log.log.debug("Session Flush Mode: " + flushModeB4);
	                                
	                /*
	                 *  Set Session flush mode to MANUAL so when data gets updated/deleted between transaction and 
	                 *  quries are executed inbetween to get same data Hibernate does not tries to flush the session
	                 *  to return updated data from query. 
	                 *  
	                 *  ALWAYS flushes session before performing query
	                 *  COMMIT may flush session at certaiin points
	                 *  
	                 */
	                
	                HibernateUtil.getCurrentSession().setHibernateFlushMode(FlushMode.MANUAL);
	
	                Log.log.debug("Session Flush Mode Set To: " + HibernateUtil.getCurrentSession().getHibernateFlushMode());
	                
	                table = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(table.getSys_id());
	
	                //remove the entries from custom form where this table to be deleted is referenced
	                scanTableToRemoveReferences(table.getUName());
	
	                // delete from CustomTableDefinition
	                removeCustomTableFromSchema(table.getUName());
	
	                // cleanup all meta form data for the custom table.
					try {
						relatedCustomTables.addAll(new DeleteMetaForm().deleteViewsForTable(table.getUName()));
					} catch (Throwable e) {
						String error = "Error deleting Table View :" + table.getUName();
						Log.log.error(error, e);
					}
	                String referenceTables = table.getUReferenceTableNames();
	                if(StringUtils.isNotBlank(referenceTables))
	                {
	                    relatedCustomTables.addAll(StringUtils.convertStringToList(referenceTables, ","));
	                }
	                
	
	                // meta table view
	                Collection<MetaTableView> metaTableViews = table.getMetaTable() != null ? table.getMetaTable().getMetaTableViews() : null;
	                if (metaTableViews != null && metaTableViews.size() > 0)
	                {
	                    for (MetaTableView view : metaTableViews)
	                    {
	                        try
	                        {
	                            DeleteMetaTableView deleteMetaTableView = new DeleteMetaTableView(view, username);
	                            deleteMetaTableView.setDeleteDefaultView(true);
	                            deleteMetaTableView.delete();
	                        }
	                        catch (Throwable t)
	                        {
	                            Log.log.error("error deleting table view for custom table:" + table.getUName(), t);
	                        }
	                    }
	                }
	
	                // delete filters
	                Collection<MetaFilter> filters = table.getMetaTable() != null ? table.getMetaTable().getMetaFilters() : null;
	                if (filters != null && filters.size() > 0)
	                {
	                    for (MetaFilter filter : filters)
	                    {
	                        HibernateUtil.getDAOFactory().getMetaFilterDAO().delete(filter);
	                    }
	                }
	                
	                //now delete the fields for these tables
	                Collection<MetaField> metaFields = table.getMetaFields();
	                if(metaFields != null && metaFields.size() > 0)
	                {
	                    for(MetaField metaField : metaFields)
	                    {
	                        deleteMetaField(metaField);
	                    }
	                }
	
	                // meta table
	                if (table.getMetaTable() != null)
	                {
	                    HibernateUtil.getDAOFactory().getMetaTableDAO().delete(table.getMetaTable());
	                }
	
	                // custom table
	                HibernateUtil.getDAOFactory().getCustomTableDAO().delete(table);
	
	                // Flsush the session since session flush mode was set to manual
	                HibernateUtil.getCurrentSession().flush();
	                
            	});
                
                // sync the table with the hbm file
                CustomTableMappingUtil.syncDBCustomTableSchema();
            }
            catch (Throwable t)
            {
                String error = "Error deleting Table View :" + table.getUName();

                Log.log.error(error, t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return relatedCustomTables;
    }

    private void removeCustomTableFromSchema(String tableName)
    {
        try
        {
            if (CustomTableMappingUtil.hasEntity(tableName))
            {
                CustomTableMappingUtil.removeEntity(tableName);

                // search for the element that has 'entity-name="content_request"' and delete that element..this is for breaking the reference
                CustomTableMappingUtil.removeEntityDependencies(tableName);

                CustomTableMappingUtil.saveMappingFile();
            }
        }
        catch (Throwable t)
        {
            Log.log.info("removeCustomTableFromSchema catched exception: " + t.getMessage());
        }
    }

    private void loadCustomTable()
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	if (table != null)
                {
                    table = HibernateUtil.getDAOFactory().getCustomTableDAO().findFirst(table);
                }
                else
                {
                    table = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(tableSysid);
                }
            });

        }
        catch (Throwable t)
        {
            String error = "Error loading the custom table :" + table.getUName();

            Log.log.error(error, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }
    
    /**
     * this is to  
     * - get the list of tables that have 'Reference' to this table that is getting deleted
     * - delete the fields from those tables and forms and dependency tables - basically cleaning up
     */
    private void scanTableToRemoveReferences(String deleteingTableName)
    {
        //metafield - DB type
        String ui_Reference = getReferenceMetaFieldQuery(deleteingTableName, "Reference");//uitype --> Reference
        removeTableReferences(ui_Reference, "metafield");
        
        //metasource - SOURCE type
        String ui_ReferenceTableField = getReferenceMetaSourceQuery(deleteingTableName, "ReferenceTableField");//uitype --> ReferenceTableField
        removeTableReferences(ui_ReferenceTableField, "metasource");
    }
    
    private String getReferenceMetaFieldQuery(String deleteingTableName, String uitype)
    {
        String hql = "select a from MetaField a, MetaFieldProperties b where b.UReferenceTable = '"+ deleteingTableName +"' and b.UUIType = '"+uitype+"' and b.sys_id = a.metaFieldProperties.sys_id";
        return hql;
    }
    
    private String getReferenceMetaSourceQuery(String deleteingTableName, String uitype)
    {
        String hql = "select a from MetaSource a, MetaFieldProperties b where b.URefGridReferenceByTable = '"+ deleteingTableName +"' and b.UUIType = '"+uitype+"' and b.sys_id = a.metaFieldProperties.sys_id";
        return hql;
    }
    
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void removeTableReferences(String hql, String type)
    {
        try
        {
            Query q = HibernateUtil.createQuery(hql);
            
            if(type.equalsIgnoreCase("metafield"))
            {
                deleteMetaFieldReferences(q.list());    
            }
            else
            {
                deleteMetaSourceReferences(q.list());
            }
            
        }
        catch(Throwable t)
        {
            //only log....
            Log.log.error("Error in cleaning up the references for the deleted table sql :" + hql, t);
        }
    }//end of removeTableReferences    
    
    private void deleteMetaFieldReferences(Collection<MetaField> refFields)
    {
        if (refFields != null && refFields.size() > 0)
        {
            Set<String> refTableSysIds = new HashSet<String>();
            
            for (MetaField refField : refFields)
            {
                //add the table sysId to list so that the schema definition can be updated
                refTableSysIds.add(refField.getCustomTable().getSys_id());
                
                //delete that field
                deleteMetaFieldReference(refField);
            }// end of for
            
            //update the schema for these tables
            cleanupSchemaDefinitionsOfTables(refTableSysIds);
        }
    }
    
    private void deleteMetaSourceReferences(List<MetaSource> refFields)
    {
        if (refFields != null && refFields.size() > 0)
        {
            for (MetaSource refField : refFields)
            {
                deleteMetaSourceReference(refField);
            }// end of for
        }
    }

    private void deleteMetaSourceReference(MetaSource metaSource)
    {
        if (metaSource != null)
        {
            //delete the metaFormTabField rec
            //String hql = "from MetaFormTabField where metaSource = '" + metaSource.getSys_id() + "'";
            String hql = "from MetaFormTabField where metaSource = :metaSourceSysId";
            deleteRefFromMetaFormTables(hql, "metaSourceSysId", metaSource.getSys_id());
            
            // delete props
            DeleteMetaForm.deleteMetaFieldProperties(metaSource.getMetaFieldProperties(), null);

            // delete
            HibernateUtil.getDAOFactory().getMetaSourceDAO().delete(metaSource);
        }// end of if
    }// deleteMetaSource

    
    private void deleteMetaFieldReference(MetaField metaField)
    {
        //ref in the metaformview table
        //String hql = "from MetaFormTabField where metaField = '" + metaField.getSys_id() + "'";
        String hql = "from MetaFormTabField where metaField.sys_id = :metaFieldSysId";
        deleteRefFromMetaFormTables(hql, "metaFieldSysId", metaField.getSys_id());
        
        //ref in the metatableviewfield
        deleteRefFromMetaTableView(metaField);
        
        //metafield rec
        deleteMetaField(metaField);
    }
    
    private void deleteMetaField(MetaField metaField)
    {
        //delete the access rights
        DeleteMetaForm.deleteMetaAccessRights(metaField.getMetaAccessRights());
        
        //delete the properties first
        DeleteMetaForm.deleteMetaFieldProperties(metaField.getMetaFieldProperties(), null);

        //delete the main rec
        HibernateUtil.getDAOFactory().getMetaFieldDAO().delete(metaField);
    }
    
    //MetaFormTabField
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void deleteRefFromMetaFormTables(String hql, String refSysIdParamName, String refSysIdParamValue)
    {
        try
        {
            Query q = HibernateUtil.createQuery(hql);
            List<MetaFormTabField> formTabFields = q.setParameter(refSysIdParamName, refSysIdParamValue).list();
            
            if(formTabFields != null && formTabFields.size() > 0)
            {
                for(MetaFormTabField formTabField : formTabFields)
                {
                    //delete the properties first
                    DeleteMetaForm.deleteMetaFieldProperties(formTabField.getMetaFieldFormProperties(), null);
                    
                    // delete tab field
                    HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().delete(formTabField);

                }//end of for
            }
        }
        catch(Throwable t)
        {
            //only log....
            Log.log.error("Error in cleaning up the ref column sql :" + hql, t);
        }
    }

    //MetaTableViewField
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void deleteRefFromMetaTableView(MetaField metaField)
    {
        //String hql = "from MetaTableViewField where metaField = '" + metaField.getSys_id() + "'";
        String hql = "from MetaTableViewField where metaField.sys_id = :metaFieldSysId";
        
        try
        {
            Query q = HibernateUtil.createQuery(hql);
            List<MetaTableViewField> tableTabFields = q.setParameter("metaFieldSysId", metaField.getSys_id()).list();
            
            if(tableTabFields != null && tableTabFields.size() > 0)
            {
                for(MetaTableViewField tableTabField : tableTabFields)
                {
                    if(tableTabField.getMetaFieldTableProperties() != null)
                    {
                        HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().delete(tableTabField.getMetaFieldTableProperties());
                    }

                    // delete tab field
                    HibernateUtil.getDAOFactory().getMetaTableViewFieldDAO().delete(tableTabField);

                }//end of for
            }
        }
        catch(Throwable t)
        {
            //only log....
            Log.log.error("Error in cleaning up the ref column :" + metaField.getUName(), t);
        }
        
    }

    private void cleanupSchemaDefinitionsOfTables(Set<String> refTableSysIds)
    {
        if(refTableSysIds != null && refTableSysIds.size() > 0)
        {
            //for each one, 
            //- get the schema definition
            //- remove the element 
            //- commit it
            for(String sysId : refTableSysIds)
            {
                CustomTable refTable = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(sysId);
                if(refTable != null)
                {
                    String newSchema = removeRefFieldFrom(refTable.getUSchemaDefinition(), this.table.getUName());
                    refTable.setUSchemaDefinition(newSchema);
                }
            }
        }
    }
    
    private String removeRefFieldFrom(String schemaDefinition, String refTableToBeRemoved)
    {
        String newSchemaDefinition = schemaDefinition;
        
        try
        {
            XDoc doc = new XDoc(schemaDefinition);
            doc.removeElements("many-to-one[@entity-name=\"" + refTableToBeRemoved + "\"]");
            doc.removeResolveNamespaces();
            newSchemaDefinition = doc.toFormattedString();
            newSchemaDefinition = newSchemaDefinition.substring(newSchemaDefinition.indexOf("<class"));
            
        }
        catch(Throwable t)
        {
            //only log....
            Log.log.error("Error in cleaning up the schema definition :" + refTableToBeRemoved, t);
        }
        
        
        return newSchemaDefinition;
    }
}
