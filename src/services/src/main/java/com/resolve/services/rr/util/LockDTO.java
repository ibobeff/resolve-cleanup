package com.resolve.services.rr.util;

import java.util.Map;
import java.util.TreeMap;

import com.resolve.services.hibernate.vo.ResolveEventVO;

public class LockDTO
{
    private ResolveEventVO evt;
    private Map<String,TreeMap<String,String>> locks;
    public LockDTO() {
        this.evt = null;
        this.locks = new TreeMap<String,TreeMap<String,String>>();
    }
    public LockDTO(ResolveEventVO evt, Map<String,TreeMap<String,String>> locks) {
        this.evt = evt;
        this.locks = locks;
    }
    public ResolveEventVO getEvt()
    {
        return evt;
    }
    public void setEvt(ResolveEventVO evt)
    {
        this.evt = evt;
    }
    public Map<String, TreeMap<String, String>> getLocks()
    {
        return locks;
    }
    public void setLocks(TreeMap<String, TreeMap<String, String>> locks)
    {
        this.locks = locks;
    }
    
}
