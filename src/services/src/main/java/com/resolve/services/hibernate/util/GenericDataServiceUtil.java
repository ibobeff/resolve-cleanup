/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class GenericDataServiceUtil
{
    public static String deleteByIds(List<String> ids, Map<String, String> config)
    {
        String modelClass = config.get(HibernateConstantsEnum.MODELCLASS.getTagName());
        if (StringUtils.isEmpty(modelClass))
        {
            throw new RuntimeException("Table or model class name has null value in configuration object: "
                            + config.get(HibernateConstantsEnum.MODELCLASS.getTagName()));
        }

        if (modelClass.contains("resolve_process_request"))
        {
            WorksheetUtil.deleteProcessRequestRows(ids, false, "system");
            return HibernateConstants.STATUS_SUCCESS;
        }
/*        else if (modelClass.contains("resolve_execute_request"))
        {
            CassandraUtil.deleteExecuteRequestRows(ids);
            return HibernateConstants.STATUS_SUCCESS;
        }
*/        
        else if (modelClass.contains("resolve_action_result"))
        {
            WorksheetUtil.deleteTaskResultRows(ids, false, "system");
            return HibernateConstants.STATUS_SUCCESS;
        }
        
        try
        {
        	String tableName = HibernateUtil.class2Table(modelClass);
            Class.forName(HibernateUtil.table2Class(tableName));
            modelClass = HibernateUtil.table2Class(tableName);
        }
        catch (Exception ex)
        {
            modelClass = "com.resolve.persistence.model." + modelClass;
            try
            {
                Class.forName(modelClass);
            }
            catch (Exception ex1)
            {
                throw new RuntimeException("Invalid table or model class name in configuration object: "
                                + config.get(HibernateConstantsEnum.MODELCLASS.getTagName()));
            }
        }

        
        try
        {
			final String modelClassFinal = modelClass;
			HibernateProxy.execute(() -> {
				for (String id : ids) {
					Object obj = Class.forName(modelClassFinal).newInstance();
					PropertyUtils.setProperty(obj, "sys_id", id);
					obj = HibernateUtil.getCurrentSession().get(modelClassFinal, id);
					if (obj != null) {
						HibernateUtil.getCurrentSession().delete(modelClassFinal, obj);
					}
				}

			});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return HibernateConstants.STATUS_SUCCESS;
    }

}
