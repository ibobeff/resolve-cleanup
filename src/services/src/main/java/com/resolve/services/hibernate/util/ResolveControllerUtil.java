/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.ResolveControllers;
import com.resolve.services.hibernate.vo.ResolveControllersVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ResolveControllerUtil
{
    public static List<ResolveControllersVO> getResolveControllers(QueryDTO query, String username)
    {
        List<ResolveControllersVO> result = new ArrayList<ResolveControllersVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveControllers instance = new ResolveControllers();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ResolveControllers instance = (ResolveControllers) o;
                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
  //private apis
    private static ResolveControllersVO convertModelToVO(ResolveControllers model, Map<String, String> mapOfOrganization)
    {
        ResolveControllersVO vo = null;
        
        if(model != null)
        {
            vo = model.doGetVO();
        }
        
        return vo;
    }
}
