/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.jdbc.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WikiUtils
{
    public static List<String> getWikiDocArchiveList(String archiveNamespaces, String archiveDocnames)
    {
        List<String> documentsToArchive = new ArrayList<String>();
        SQLConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Timestamp archiveDate = new Timestamp(GMTDate.getTime());
        
        String whereClause = getWikiDocWhereClause(archiveNamespaces, archiveDocnames);
        String sql = "SELECT sys_id as SYSID, u_fullname as FULLNAME FROM wikidoc where " + whereClause;
        if (StringUtils.isNotBlank(whereClause))
        {
            sql += " AND";
        }
		sql += " u_expire_on is not NULL and u_expire_on < ?";
        
        try
        {
            connection = SQL.getConnection();
            
            statement = connection.prepareStatement(sql);
            statement.setTimestamp(1, archiveDate);
            
            resultSet = statement.executeQuery();
            
            while(resultSet.next())
            {
                String fullName = resultSet.getString("FULLNAME");
                documentsToArchive.add(fullName);
            }//end of while
        }
        catch(Exception e)
        {
            Log.log.error("Error in selecting data for :" + sql, e);
        }
        finally
        {
            try
            {
                if(resultSet!=null)
                {
                    resultSet.close();
                }
                
                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return documentsToArchive;
    }
    
    public static List<String> getWikiDocList(Timestamp recertifyDate, String namespaces, String docnames)
    {
        List<String> recertifyDocuments = new ArrayList<String>();
        SQLConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        String whereClause = getWikiDocWhereClause(namespaces, docnames);
        String sql = "SELECT sys_id as SYSID, u_fullname as FULLNAME FROM wikidoc where "+ whereClause;
        if (StringUtils.isNotBlank(whereClause))
        {
            sql += " AND";
        }
        sql += " sys_updated_on < ?";
        
        try
        {
            connection = SQL.getConnection();
            
            Timestamp gmtRecertifyDate = new Timestamp(GMTDate.getTime(recertifyDate.getTime()));
            statement = connection.prepareStatement(sql);
            statement.setTimestamp(1, gmtRecertifyDate);
            
            resultSet = statement.executeQuery();
            
            while(resultSet.next())
            {
                String fullName = resultSet.getString("FULLNAME");
                //Log.log.debug("Expired Document: " + fullName);
                recertifyDocuments.add(fullName);
            }//end of while
        }
        catch(Exception e)
        {
            Log.log.error("Error in selecting data for :" + sql, e);
        }
        finally
        {
            try
            {
                if(resultSet!=null)
                {
                    resultSet.close();
                }
                
                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return recertifyDocuments;
    }
    
    
    private static String getWikiDocWhereClause(String namespaces, String docnames)
    {
        String whereClause = "";
        boolean useAnd = false;
        if (StringUtils.isNotEmpty(namespaces))
        {
            Log.log.debug("Namespaces: " + namespaces);
            whereClause += " u_namespace IN (";
            boolean firstItem = true;
            String[] splitNamespaces = namespaces.split(",");
            for (String exnamespace : splitNamespaces)
            {
                exnamespace = exnamespace.trim();
                if (firstItem)
                {
                    whereClause += "'" + exnamespace + "'";
                    firstItem = false;
                }
                else
                {
                    whereClause += ",'" + exnamespace + "'";
                }
            }
            whereClause += ")";
            useAnd = true;
        }
        if (StringUtils.isNotEmpty(docnames))
        {
            Log.log.debug("Fullnames is in: " + docnames);
            if (useAnd)
            {
                whereClause += " OR";
            }
            whereClause += " u_fullname IN (";
            boolean firstItem = true;
            String[] splitDocnames = docnames.split(",");
            for (String exDocname : splitDocnames)
            {
                exDocname = exDocname.trim();
                if (firstItem)
                {
                    whereClause += "'" + exDocname + "'";
                    firstItem = false;
                }
                else
                {
                    whereClause += ",'" + exDocname + "'";
                }
            }
            whereClause += ")";
            useAnd = true;
        }
                
        return whereClause;
    }
}
