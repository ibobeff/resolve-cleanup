package com.resolve.services.rr.util;

@SuppressWarnings("serial")
public class RRInterestingException extends Exception
{
    private static final long serialVersionUID = -7605028493237201629L;
	public static final String RULE_EXIST = "RULE_EXIST";
    public static final String SCHEMA_NOT_FOUND = "SCHEMA_NOT_FOUND";
    private String type;
    private Object data;
    public RRInterestingException(String type,String msg) {
        super(msg);
        this.type = type;
    }
    public RRInterestingException(String type,String msg,Object data) {
        super(msg);
        this.type = type;
        this.data = data;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public Object getData()
    {
        return data;
    }
    public void setData(Object data)
    {
        this.data = data;
    }
}
