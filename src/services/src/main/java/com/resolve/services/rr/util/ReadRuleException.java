package com.resolve.services.rr.util;

@SuppressWarnings("serial")
public class ReadRuleException extends Exception
{
    private static final long serialVersionUID = 3668842957627441463L;
	private String id;

    public String getId()
    {
        return id;
    }

    public ReadRuleException(Exception e)
    {
       super(e);
    }

    public ReadRuleException(String id, Exception e)
    {
        super(e);
        this.id = id;
    }
}
