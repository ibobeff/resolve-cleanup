/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.WikiAttachmentUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.dto.AttachmentDTO;

public class SearchWikiAttachments
{
    private static final Map<String, String> mapDTOColsWithModel = new HashMap<String, String>();
    static
    {
        mapDTOColsWithModel.put("filename", "UFilename");
        mapDTOColsWithModel.put("size", "USize");
        mapDTOColsWithModel.put("filesysid", "sys_id");
        mapDTOColsWithModel.put("uploadedby", "sysCreatedBy");
        mapDTOColsWithModel.put("uploadedon", "sysCreatedOn");
    }
    
    
    private QueryDTO query = null; //if this is null, than return all the attachments of the document
    private String docSysId = null;
    private String docFullname = null;
    private String username = null;
    
    private WikiDocument doc = null;
    
    public SearchWikiAttachments(QueryDTO query, String docSysId, String docFullname, String username) throws Exception
    {
        if(StringUtils.isEmpty(docSysId) && StringUtils.isEmpty(docFullname))
        {
            throw new Exception("Doc sysId or name must be there");
        }
        
        if(StringUtils.isEmpty(username))
        {
            throw new Exception("Username is mandatory");
        }
        
        if(query != null)
        {
            this.query = query;
            this.query.setPrefixTableAlias("wa.");
        }
        else
        {
            this.query = new QueryDTO();
        }

        this.docSysId = docSysId;
        this.docFullname = docFullname;
        this.username = username;
    }
    
    public ResponseDTO<AttachmentDTO> getAttachments() throws Exception
    {
        List<AttachmentDTO> result = new ArrayList<AttachmentDTO>();
        
        doc = WikiUtils.getWikiDocumentModel(docSysId, docFullname, username, false);
        if(doc == null)
        {
            if(StringUtils.isNotBlank(docSysId))
            {
                throw new Exception("Document with id " + (docSysId != null ? docSysId : "") + " or name " + 
                                    (docFullname != null ? docFullname : "") + " does not exist.");
            }
            else
            {
                Log.log.warn("No attachements found for Document " + (docFullname != null ? docFullname : "") + " , " + 
                             (docSysId != null ? docSysId : ""));
                ResponseDTO<AttachmentDTO> response = new ResponseDTO<AttachmentDTO>();
                response.setSuccess(false).setMessage("No attachements found for Document " + (docFullname != null ? docFullname : "") + " , " + 
                                                      (docSysId != null ? docSysId : ""));
                return response;
            }
        }
        
        int total = 0;

        String hql = createHql();
        
        //set it in the query object
        this.query.setHql(hql.toString());
        int start = query.getStart();
        int limit = query.getLimit();
        
        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            if (data != null)
            {
                for (Object o : data)
                {
                    Object[] objectArray = (Object[]) o;
                    WikiAttachment wikiAttachment = (WikiAttachment) objectArray[0];
                    WikidocAttachmentRel wikiAttachmentRel = (WikidocAttachmentRel) objectArray[1];
                    
                    result.add(WikiAttachmentUtil.convertWikiAttachmentToDTO(wikiAttachment, wikiAttachmentRel, doc.getUFullname()));
                }
            }//end of if

            total = ServiceHibernate.getTotalHqlCount(query);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for hql:" + hql.toString(), e);
            throw e;
        }
        
        ResponseDTO<AttachmentDTO> response = new ResponseDTO<AttachmentDTO>();
        response.setRecords(result);
        response.setTotal(total); 

        return response;
    }
    
    
    private String createHql()
    {
        //update the fields with correct model values
        List<QueryFilter> filterItems = this.query.getFilterItems();
        if(filterItems != null && filterItems.size() > 0)
        {
            for(QueryFilter filter : filterItems)
            {
                String field = filter.getField();
                String newField = mapDTOColsWithModel.containsKey(field.toLowerCase()) ? mapDTOColsWithModel.get(field.toLowerCase()) : field;
                String type = filter.getType();
                if(newField.equalsIgnoreCase("USize"))
                {
                    type = "int";
                }
                
                filter.setField(newField);
                filter.setType(type);
            }
            
            //update it 
            this.query.setFilterItems(filterItems);
        }
        
        //update the sort fields with correct model values
        List<QuerySort> sortItems = this.query.getSortItems();
        if(sortItems != null && sortItems.size() > 0)
        {
            for(QuerySort sort : sortItems)
            {
                String field = sort.getProperty();
                String newField = mapDTOColsWithModel.containsKey(field.toLowerCase()) ? mapDTOColsWithModel.get(field.toLowerCase()) : field;
                
                sort.setProperty(newField);
            }
            
            this.query.setSortItems(sortItems);
        }
                        
        
        
        StringBuilder hql = new StringBuilder("select wa, rel from WikiAttachment as wa, WikidocAttachmentRel as rel ");
        hql.append(" where wa.sys_id = rel.wikiAttachment and rel.wikidoc = '" + doc.getSys_id()+ "' ");
        
//      filter:[{"field":"fileName","type":"auto","condition":"contains","value":"test"}]
//                      page:1
//                      start:0
//                      limit:50
//                      sort:[{"property":"fileName","direction":"ASC"}]
      
        
      //additional where clause
        String whereClause = this.query.getWhereClause();
        if(StringUtils.isNotBlank(whereClause))
        {
            hql.append(" and ").append(whereClause);
        }
        
        //search filter
        String searchQry = this.query.getFilterWhereClause();
        if (StringUtils.isNotBlank(searchQry))
        {
            hql.append(" and ").append(searchQry);
        }
        
        //sort condition --> [{"property":"name","direction":"DESC"}]
        //comes from the UI...so will have to add the 'wd.' in front of the properties
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        if(StringUtils.isNotBlank(sortBy))
        {
            hql.append(" order by ").append(sortBy.trim());
        }
        
        return hql.toString();
    }
}
