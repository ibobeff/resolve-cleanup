package com.resolve.services;

import java.io.File;
import java.io.IOException;
import java.lang.Thread.State;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsbase.MainBase;
import com.resolve.services.hibernate.util.OrgsUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RRRuleFieldVO;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.rr.util.BaseBulkTask;
import com.resolve.services.rr.util.BulkDBTask;
import com.resolve.services.rr.util.BulkDBTaskDataManipulator;
import com.resolve.services.rr.util.ExportTask;
import com.resolve.services.rr.util.ExportTaskResponseWriter;
import com.resolve.services.rr.util.ImportTask;
import com.resolve.services.rr.util.Locker;
import com.resolve.services.rr.util.ResolutionRoutingUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class ServiceResolutionRouting
{ 
    public static final String NO_SCHEMA_FIELDS_DEFINED_ERR_MSG = "At least one field should be defined for a schema!";
    public static final String RESOLVE_RESERVED_FIELD_ERR_MSG_SUFFIX = " is invalid. Please use different name or use mixed case.";
    public static final String RESOLVE_RESERVED_FIELD_ERR_MSG_MIDDLE = " are reserved keywords in Resolve and can not be used as schema field names. ";
    public static final String RESOLVE_ERR_MSG_SCHEMA_FIELD_NAME = " schema field name ";
    public static final String RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_PREFIX = "Schema field names (case insensitive) must be unique. ";
    public static final String RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_SUFFIX = " is duplicate.";
    public static final String COMMA_SPACE_SEPARATOR = ", ";
    public static final String INSUFFICIENT_RIGHTS_TO_REORDER_SCHEMA = "User has insufficient rights to reorder schema.";
    public static final String ERR_MSG_SCHEMA_NOT_UPDATED_BECAUSE_IN_USE = "Failed to update schema as it is currently in use.";
    public static final String ERR_MSG_SCHEMA_NOT_DELETED_BECAUSE_IN_USE = "Failed to delete schema as it is currently in use.";
    public static final String USER_NOT_MEMBER_OF_SPECIFIED_ORG = "User is not member of specified Org.";
    public static final String USER_NOT_MEMBER_OF_NO_ORG = "User is not member of No Org 'None'.";
    
    //we need thread pool for this in the future
    //export/import should be merged into db task in the future;
    private static Vector<BaseBulkTask> tasks = new Vector<BaseBulkTask>();
    public static Set<String> invalidSchemaFieldNames = new HashSet<String>(); 
    
    static {
        File file = new File(MainBase.getTmpDir() + "/rr_tmp/");
        if(!file.exists())
            file.mkdir();
        
        invalidSchemaFieldNames.add(Constants.HTTP_REQUEST_SYS_ID);
        invalidSchemaFieldNames.add(Constants.HTTP_REQUEST_sys_id);
        invalidSchemaFieldNames.add(Constants.HTTP_REQUEST_PROBLEMID);
        invalidSchemaFieldNames.add(Constants.EXECUTE_ORG_ID);
        invalidSchemaFieldNames.add(Constants.EXECUTE_org_id);
        invalidSchemaFieldNames.add(Constants.EXECUTE_ORG_NAME);
        invalidSchemaFieldNames.add(Constants.EXECUTE_org_name);
        invalidSchemaFieldNames.add(Constants.HTTP_REQUEST_NUMBER);
        invalidSchemaFieldNames.add(Constants.HTTP_REQUEST_REFERENCE);
        invalidSchemaFieldNames.add(Constants.HTTP_REQUEST_ALERTID);
        invalidSchemaFieldNames.add(Constants.HTTP_REQUEST_CORRELATIONID);
    }
    
    public static List<RRRuleVO> listRules(QueryDTO query, String username) throws Exception{
       List<RRRuleVO> rules = ResolutionRoutingUtil.listRules(query, username);
       for(RRRuleVO rule:rules)
           populateWikiFields(rule,username,true);
       return rules;
    }
    private static void populateWikiFields(RRRuleVO vo,String username,boolean useId) {
        WikiDocumentVO wiki,runbook,automation;
        if(!useId) {
            wiki = ServiceWiki.getWikiDoc("",vo.getWiki(),username);
            runbook =ServiceWiki.getWikiDoc("",vo.getRunbook(),username);
            automation = ServiceWiki.getWikiDoc("",vo.getAutomation(),username);
        }else {
            wiki = ServiceWiki.getWikiDoc(vo.getWikiId(),"",username);
            runbook =ServiceWiki.getWikiDoc(vo.getRunbookId(),"",username);
            automation = ServiceWiki.getWikiDoc(vo.getAutomationId(),"",username);
        }
        
        if(wiki!=null) {
            vo.setWikiId(wiki.getId());
            vo.setWiki(wiki.getUFullname());
        }else {
            vo.setWikiId("");
            vo.setWiki("");
        }
        if(runbook!=null) {
            vo.setRunbookId(runbook.getId());
            vo.setRunbook(runbook.getUFullname());       
        }else {
            vo.setRunbookId("");
            vo.setRunbook("");
        }
        if(automation!=null) {
            vo.setAutomationId(automation.getId());
            vo.setAutomation(automation.getUFullname());
        }else {
            vo.setAutomationId("");
            vo.setAutomation("");
        }
    }
    public static void deleteRulesById(List<String> ids, String username) throws Exception {
       ResolutionRoutingUtil.deleteRulesById(ids, username);
    }
    public static void deleteRulesByQuery(QueryDTO query,String username) throws Exception{
       ResolutionRoutingUtil.deleteRuleByQuery(query, username);
    }
    public static RRRuleVO getRule(String id,String username) throws Exception {
        RRRuleVO rule = ResolutionRoutingUtil.getRule(id,username);
        populateWikiFields(rule,username,true);
        return rule;
    }
    public static RRRuleVO saveRule(RRRuleVO vo, String username) throws Exception {
        populateWikiFields(vo,username,false);
        List<RRRuleFieldVO> fields = vo.getRidFields();
        if(fields!=null)
        {
            for(RRRuleFieldVO field:fields)
            {
                field.setValue(StringUtils.isNotBlank(field.getValue()) ? field.getValue().toLowerCase() : field.getValue());
            }
        }
        vo = ResolutionRoutingUtil.saveRule(vo, username);
        RRRuleVO savedRule = getRule(vo.getId(),username);
        return savedRule;
    }

    public static Map<String,Boolean> expandModule(QueryDTO query, String username) throws Exception{
        String[] prefixes = {""};
        if(!query.getFilterItems().isEmpty()) {
            for (QueryFilter item : query.getFilterItems())
            {
                if (!"sysOrg".equals(item.getField()) && !"schema.sysOrg".equals(item.getField()))
                {
                    //QueryFilter item = query.getFilterItems().get(0);
                    item.setValue(StringUtils.isEmpty(item.getValue())?"":item.getValue() + ".");
                    prefixes = StringUtils.isNotBlank(item.getValue()) ? item.getValue().split("\\.") : new String[] {""};
                }
            }
        }
        
        List<String> complete = ResolutionRoutingUtil.listRuleModules(query, username); 
        
        if(StringUtils.isEmpty(prefixes[0]))
            prefixes = new String[0];
        TreeMap<String,Boolean> map = new TreeMap<String,Boolean>();
        for(String module:complete) {
            String[] splits = module.split("\\.");
            if(splits.length <= prefixes.length)
                continue;
            map.put(splits[prefixes.length], prefixes.length+1 >= splits.length);
        }
        return map;
    }
    
    public static List<RRSchemaVO> listSchema(QueryDTO query, String username) throws Exception{
        return ResolutionRoutingUtil.listSchemas(query, username);
    }
    
    public static void deletSchemas(List<String> ids,String username, ConfigSQL configSQL) throws Exception {
        List<String> noAccessIds = new ArrayList<String>();
        
        // Check non-admin role users access to specified Org/No Org 'None'
        
        if (!UserUtils.isAdminUser(username))
        {
            for (String schemaSysId : ids)
            {
                RRSchemaVO vo = getSchema(schemaSysId, username);
                
                if (vo != null)
                {
                    if (StringUtils.isNotBlank(vo.getSysOrg()) && !vo.getSysOrg().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                    {
                        if (!UserUtils.isOrgAccessible(vo.getSysOrg(), username, false, false))
                        {
                            Log.log.info(USER_NOT_MEMBER_OF_SPECIFIED_ORG + " Removing Schema with sys id " + 
                                         schemaSysId + " and org id " + vo.getSysOrg() + " from list of schemas to delete.");
                            noAccessIds.add(schemaSysId);
                        }
                    }
                    else
                    {
                        if (!UserUtils.isNoOrgAccessible(username))
                        {
                            Log.log.info(USER_NOT_MEMBER_OF_NO_ORG + " Removing Schema with sys id " + 
                                         schemaSysId + " and null oeg id from list of schemas to delete.");
                            noAccessIds.add(schemaSysId);
                        }
                    }
                }
                else
                {
                    Log.log.info("Missing schema with sys id " + schemaSysId + ", removing from list of schemas to delete.");
                    noAccessIds.add(schemaSysId);
                }
            }
            
            ids.removeAll(noAccessIds);
        }
        
        HibernateProxy.execute(() -> {
        	ResolutionRoutingUtil.deleteSchemas(ids, username, configSQL);
        });
        
    }
    
    public static Integer getNextSchemaOrder(String username) throws Exception {
        return ResolutionRoutingUtil.getNextSchemaOrder(username);
    }
    
    public static RRSchemaVO getSchema(String id,String username) throws Exception {
       return ResolutionRoutingUtil.getSchema(id, username);
    }
    
    public static RRSchemaVO getSchemaByName(String name,String username) throws Exception{
        return ResolutionRoutingUtil.getSchemaByName(name, username);
    }
    
    public static RRSchemaVO saveSchema(RRSchemaVO vo, String username, boolean reorder, long updatedOnTime, 
    		ConfigSQL configSQL) throws Exception {
       TreeMap<String,Boolean> map = new TreeMap<String,Boolean>();
       Set<String> lcFldNames = new HashSet<String>();
       
       ObjectMapper mapper = new ObjectMapper();
       JsonNode jsonFields = mapper.readTree(vo.getJsonFields());
       
       String next = "";    
       if(jsonFields.size()==0)
           throw new Exception(NO_SCHEMA_FIELDS_DEFINED_ERR_MSG);
       for(int i = 0;i<jsonFields.size();i++) {
           next = jsonFields.get(i).get("name").asText();

           // Validate against set of invalid schema field names
           
           if (invalidSchemaFieldNames.contains(next))
           {
               throw new Exception(StringUtils.setToString(invalidSchemaFieldNames, COMMA_SPACE_SEPARATOR) + RESOLVE_RESERVED_FIELD_ERR_MSG_MIDDLE + 
                                   vo.getName() + RESOLVE_ERR_MSG_SCHEMA_FIELD_NAME + next + RESOLVE_RESERVED_FIELD_ERR_MSG_SUFFIX);
           }
           
           if (map.containsKey(next) || lcFldNames.contains(next.toLowerCase()))
           {
               throw new Exception(RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_PREFIX + vo.getName() + 
                                   RESOLVE_ERR_MSG_SCHEMA_FIELD_NAME + next + RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_SUFFIX);
           }
           map.put(next, true);
           lcFldNames.add(next.toLowerCase());
       }
       
       if (StringUtils.isBlank(vo.getSys_id()) || vo.getSys_id().equals(VO.STRING_DEFAULT))
       {
           // Create new schema
           
           if (!UserUtils.isAdminUser(username))
           {
               vo.setOrder(Integer.MAX_VALUE); // ignore user specified order, only admin can re-order schemes
           }
       }
       
       // Check non-admin role users access to specified Org/No Org 'None'
       
       if (!UserUtils.isAdminUser(username))
       {
           if (StringUtils.isNotBlank(vo.getSysOrg()) && !vo.getSysOrg().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
           {
               if (!UserUtils.isOrgAccessible(vo.getSysOrg(), username, false, false))
               {
                   throw new Exception(USER_NOT_MEMBER_OF_SPECIFIED_ORG + " Failed to save schema.");
               }
           }
           else
           {
               if (!UserUtils.isNoOrgAccessible(username))
               {
                   throw new Exception(USER_NOT_MEMBER_OF_NO_ORG + " Failed to save schema.");
               }
           }
       }
       
       return ResolutionRoutingUtil.saveSchema(vo,username, reorder, updatedOnTime, configSQL);
    }
    
    public static void ReOrderSchemes(String username, ConfigSQL configSQL) throws Exception
    {
        ResolutionRoutingUtil.ReOrderSchemes(username, configSQL);
    }
    
    public static List<String> listRuleModules(QueryDTO query,String username) throws Exception{
        return ResolutionRoutingUtil.listRuleModules(query, username);
    }
    
    public static RRRuleVO lookup(String queryString,String username) throws Exception {
        if(StringUtils.isEmpty(queryString))
            return null;
        QueryDTO query = new QueryDTO();
        query.setModelName("RRSchema");
        query.setSort("[{\"property\":\"order\",\"direction\":\"ASC\"}]");
        String[] pairs = queryString.split("&");
        LinkedHashMap<String,String> params = new LinkedHashMap<String,String>();
        for(String pair:pairs) {
            String[] kv = pair.split("=");
            if(kv.length<2)
                continue;
            String key = kv[0].toLowerCase();
            String value = URLDecoder.decode(kv[1], "UTF-8");
            if (!key.equals(Constants.EXECUTE_org_name)) { // don't lowercase the value if key is resolve.org_name
                value = value.toLowerCase();
            }
            params.put(key, value);
        }
        
        if (params.containsKey(Constants.EXECUTE_org_name)) {
            /*
             * parameter has an org name. So, get the corresponding org id and pass it on as a filter
             */
            String orgName = params.get(Constants.EXECUTE_org_name);
            Orgs org = OrgsUtil.findOrgModelByName(orgName);
            if (org == null) {
                Log.log.warn(String.format("The specified Org, %s, does not exist.", orgName));
                return null;
            }
            QueryFilter filter = new QueryFilter("auto",org.getSys_id(), "sysOrg", "equals");
            query.setFilters(Arrays.asList(filter));
        }
        
        List<RRSchemaVO> schemas = ResolutionRoutingUtil.listSchemas(query, username);
        RRRuleVO ruleVO = null;
        LinkedHashMap<String,String> rid = null;
        for(RRSchemaVO schema:schemas) {
            rid = matchSchema(params,schema);
            if(rid.isEmpty())
                continue;
            if(null!=(ruleVO = lookupRule(rid,schema,username))) 
                break;
        }
        
        // TODO Replace with adding display mode in RRRule entity to indicate DT/Wiki/Catalog etc.
        
        if (ruleVO != null)
        {
            try
            {
            
            	String wikiId = ruleVO.getWikiId();
              HibernateProxy.setCurrentUser("system");
                WikiDocument ruleDoc = (WikiDocument) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(wikiId);
                });
            
                if (ruleDoc != null && StringUtils.isNotBlank(ruleDoc.getUDisplayMode()))
                {
                    String wikiWithDisplayMode = ruleVO.getWiki() + "?UDisplayMode=" + ruleDoc.getUDisplayMode();
                    ruleVO.setWiki(wikiWithDisplayMode);
                }
                
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e);
            } 
        }
        
        return ruleVO;
    }
    
    private static RRRuleVO lookupRule(LinkedHashMap<String,String> rid,RRSchemaVO schema,String username) throws Exception {
        QueryDTO query = new QueryDTO();
        query.setModelName("RRRule");
        query.addFilterItem(new QueryFilter("schema.name",QueryFilter.EQUALS,schema.getName()));
        List<String> ridTokens = new ArrayList<String>();
        for(String key:rid.keySet())
            ridTokens.add(rid.get(key));
        
        query.addFilterItem(new QueryFilter("rid",QueryFilter.EQUALS,StringUtils.join(ridTokens,StringUtils.DELIMITER)));
        query.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE,"true","active",QueryFilter.EQUALS));
        query.setFilterItems(query.getFilterItems().stream()
                                                   .filter(f -> !f.getField().equalsIgnoreCase("sysorg")) // remove sysorg filter from query when model is RRRule.
                                                   .collect(Collectors.toList()));
        query.setLimit(1);
        try
        {
            List<RRRuleVO> list = listRules(query,username);
            if(list.isEmpty())
                return null;
            return list.get(0);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception("Error looking up routing mapping!",e);
        }
    }
    
    private static LinkedHashMap<String,String> matchSchema(LinkedHashMap<String,String> params,RRSchemaVO schema) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        LinkedHashMap<String,String> rid = new LinkedHashMap<String,String>();
        try
        {
            JsonNode jsonFields = mapper.readTree(schema.getJsonFields());
            ArrayList<JsonNode> fields = new ArrayList<JsonNode>();
            for(JsonNode json:jsonFields)
                fields.add(json);
            Collections.sort(fields,new Comparator<JsonNode>() {
                public int compare(JsonNode o1, JsonNode o2)
                {
                    return o1.get("order").asInt()>o2.get("order").asInt()?1:-1;
                }
                
            });
            String[] paramFields = new String[params.size()]; 
            params.keySet().toArray(paramFields);
            
            Pattern p = null;
            String fieldName = null;
            String regex = null;
            String value = null;
            Matcher m = null;
            boolean found=false;
            for(int i = 0;i<fields.size();i++) {
                for(int j = 0;j<paramFields.length;j++) {
                    fieldName = fields.get(i).get("name").asText().toLowerCase();
                    regex = fields.get(i).get("regex").asText();
                    if(!params.containsKey(fieldName)) {
                        found=false;
                        break;
                    }   
                    value = params.get(fieldName).toLowerCase();
                    if(StringUtils.isEmpty(regex)) {
                        found=true;
                        rid.put(fieldName, value);
                        break;
                    }
                    p = Pattern.compile(regex);

                    //all schemas coming in should already match the pattern...so don't need to determine it here... just extract the group
                    m = p.matcher(value);
                    if(m.find()) {
                        found=true;
                        rid.put(fieldName, m.groupCount()>0?m.group(1):value);
                        break;
                    }
                }
                if(!found) {
                    rid.clear();
                    break;
                }   
            }
            return rid;
        }
        catch (JsonProcessingException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception("Error parsing schema fields!",e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception("Error parsing schema fields!",e);
        }
    }
    
    public static Map<String,String> prepareExportTask(List<String> ids,boolean force, String username) throws Exception {
        Map<String,String> map = new TreeMap<String,String>();
        ExportTask exportTask = new ExportTask(ids,username);
        if(!Locker.lock(map,exportTask,force))
            return map;
        tasks.add(exportTask);
        map.put(Locker.EVT_USERNAME,username);
        map.put(BaseBulkTask.TASK_ID, exportTask.getTid());
        return map;
    }
    public static Map<String,String> prepareExportTask(QueryDTO query,boolean force,String username) throws Exception {
        TreeMap<String,String> map = new TreeMap<String,String>();
        ExportTask exportTask = new ExportTask(query,username);
        if(!Locker.lock(map,exportTask,force))
            return map;
        tasks.add(exportTask);
        map.put(Locker.EVT_USERNAME,username);
        map.put(BaseBulkTask.TASK_ID, exportTask.getTid());
        return map;
    }
    private static BaseBulkTask getTask(String tid) {
        for(BaseBulkTask task:tasks) 
            if(task.getTid().equals(tid))
                return task;
        return null;
    }
    public static void startExportTask(String tid,HttpServletRequest request, HttpServletResponse response,final String username) throws Exception {
        ExportTask task = (ExportTask) getTask(tid);
        if(task==null||task.getState()!=State.NEW) 
            throw new Exception("Export task missing!");
        ExportTaskResponseWriter writer = new ExportTaskResponseWriter(request,response,username);
        task.startRealExport(writer);
        task.join();//this pause the main thread so that the writer will respond to the download request instead of the controller
    }
    
    public static JSONObject poll(String tid,String username) throws Exception {
        //ObjectMapper mapper = new ObjectMapper();
        //ObjectNode result = mapper.createObjectNode();
        JSONObject result = new JSONObject();
        //the task will be remove when abort, so client side may still send several polling req, need to do null check
        BaseBulkTask task = getTask(tid);
        if(task==null||task.getState()==State.TERMINATED) {
            result.put("hasGone", Boolean.TRUE);
            /*
             * scenario where we have very small file to upload, bulk poll
             * gets no chance to grab the status.
             */
            result.element("status", new ObjectMapper().writeValueAsString(task.getStatus()));
            if(task!=null)
                Locker.releaseBulkOpLock(task.getTid());
            tasks.remove(task);
            return result;
        }
        Double percent = task.getPercentage(); 
        LinkedList<JSONObject> status = task.getStatus();
        result.put("percent", percent);
        
        if (percent >= 0.0)
        {
            //JSONArray statusArray = StringUtils.stringToJSONArray(new ObjectMapper().writeValueAsString(status));
            
            Log.log.trace("statusArray is " + new ObjectMapper().writeValueAsString(status));

            //Collection statusCollection = JSONArray.toCollection(statusArray);
            
            //if (statusCollection != null && !statusCollection.isEmpty())
            {
                result.element("status", new ObjectMapper().writeValueAsString(status));
            }
        }
        
        if(percent>=1.0) {
            //tasks.remove(task);
            Locker.releaseBulkOpLock(task.getTid());
        }
        
        Log.log.debug("poll result is " + StringUtils.jsonObjectToString(result));
        
        return result;
    }

    public static Map<String,String> beforeImport(String username) throws Exception{
        TreeMap<String,String> map = new TreeMap<String,String>();
        Locker.fetchLock(map);
        return map;
    }
    
    @SuppressWarnings("unchecked")
    public static Map<String,String> prepareImportTask(HttpServletRequest request, final String username) throws Exception {
        TreeMap<String,String> map = new TreeMap<String,String>();
        List<FileItem> fileItems;
        ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
        fileItems = upload.parseRequest(request);
        FileItem item = null;
        boolean override = false;
        boolean force = false;
        for (FileItem fileItem : fileItems)
        {
            if(fileItem.isFormField()) {
                if(fileItem.getFieldName().equals("override"))
                    override = Boolean.valueOf(fileItem.getString());
                if(fileItem.getFieldName().equals("forceImport"))
                    force = Boolean.valueOf(fileItem.getString());
            }
            if (!fileItem.isFormField())
                item = fileItem;
        }
        ImportTask importTask = new ImportTask(item,username,override);
        Locker.lock(map, importTask, force);
        if(!map.isEmpty()) 
            return map;
        tasks.add(importTask);
        importTask.start();
        map.put(Locker.EVT_USERNAME, username);
        map.put(BaseBulkTask.TASK_ID, importTask.getTid());
        return map;
    }
    
    public static void abort(String tid, String username) throws Exception {
        BaseBulkTask task = getTask(tid);
        if(task==null)
            return;
        if(task.getState()==State.TERMINATED) {
            tasks.remove(task);
            return;
        }
        task.stopTask();
        if(task.getState()==State.NEW) 
            task.start();
        task.join();
        tasks.remove(task);
        Locker.releaseBulkOpLock(task.getTid());
    }
    
    public static Map<String,String> toggleActivationByIds(List<String> ids, final boolean isActivated,boolean force,String username) throws Exception {
        TreeMap<String,String> map = new TreeMap<String,String>();
        BulkDBTask dbTask = new BulkDBTask(ids,username,(isActivated?BulkDBTask.ACTIVATION:BulkDBTask.DEACTIVATION),new BulkDBTaskDataManipulator() {
            public void manipulate(RRRuleVO rule, String username) throws Exception
            {
                rule.setActive(isActivated);
                ServiceResolutionRouting.saveRule(rule, username);
            }
        });
        if(!Locker.lock(map, dbTask, force))
            return map;
        tasks.add(dbTask);
        dbTask.start();
        map.put(BaseBulkTask.TASK_ID, dbTask.getTid());
        map.put(Locker.EVT_USERNAME, username);
        return map;
    }
    
    public static Map<String,String> toggleActivationByQuery(QueryDTO query, final boolean isActivated,boolean force,String username) throws Exception {
        TreeMap<String,String> map = new TreeMap<String,String>();
        BulkDBTask dbTask = new BulkDBTask(query,username,(isActivated?BulkDBTask.ACTIVATION:BulkDBTask.DEACTIVATION),new BulkDBTaskDataManipulator() {
            public void manipulate(RRRuleVO rule, String username) throws Exception
            {
                rule.setActive(isActivated);
                ServiceResolutionRouting.saveRule(rule, username);
            }
        });
        if(!Locker.lock(map, dbTask, force))
            return map;
        tasks.add(dbTask);
        dbTask.start();
        map.put(Locker.EVT_USERNAME, username);
        map.put(BaseBulkTask.TASK_ID, dbTask.getTid());
        return map;
    }
}
