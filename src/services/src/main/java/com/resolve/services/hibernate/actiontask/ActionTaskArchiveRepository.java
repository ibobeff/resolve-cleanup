package com.resolve.services.hibernate.actiontask;

import java.util.List;

import org.hibernate.query.Query;

import com.resolve.persistence.model.ActionTaskArchive;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;

public enum ActionTaskArchiveRepository {

	INSTANCE;

	public List<ActionTaskArchive> findAll(String docId, String username) throws Exception {
		
		return (List<ActionTaskArchive>) HibernateProxy.execute(() -> {
			Query query = HibernateUtil.createQuery("FROM ActionTaskArchive AS a WHERE a.UTableId = :docId ORDER BY a.UVersion DESC").setParameter("docId", docId);
			return query.list();
		});

	}

}
