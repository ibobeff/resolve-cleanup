/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.query.Query;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.resolve.persistence.model.ResolveEdge;
import com.resolve.persistence.model.ResolveEdgeProperties;
import com.resolve.persistence.model.ResolveNode;
import com.resolve.persistence.model.ResolveNodeProperties;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.migration.social.NonNeo4jRelationType;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class GraphUtil
{
    private static final String NULL = "null";
    
    public static ResolveNodeVO findNode(String sysId, String compSysId, String compName, NodeType type, String username) throws Exception
    {
        ResolveNodeVO node = null;
        
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        //Assuming that the sysIds are unique
        else if(StringUtils.isNotBlank(compSysId))// && type != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && type != null)
        {
            //good
        }
        else
        {
            throw new Exception("Not sufficient info to get the node info");
        }
        
        ResolveNode model = findResolveNode(sysId, compSysId, compName, type, username);
        if(model != null)
        {
            node = model.doGetVO();
        }
        
        
        return node;
    }
    
    public static ResponseDTO<ResolveNodeVO> findNodes(String compName, NodeType type, int start, int limit, String username) throws Exception
    {
        List<ResolveNodeVO> result = new ArrayList<ResolveNodeVO>();
        
        String hql = "from ResolveNode where 1=1";
        if(StringUtils.isNotBlank(compName))
        {
            hql = hql + " and LOWER(UCompName) like '%" + compName + "%'";
        }
        if(type != null)
        {
            hql = hql + " and UType = '" + type.name() + "'";
        }
        
        hql = hql + " order by UCompName";

        //query it, get the objs and convert to VOs 
        List<? extends Object> nodes = ServiceHibernate.executeHQLSelect(hql, start, limit);
        if(nodes != null)
        {
            for(Object o : nodes)
            {
                result.add(findNode(((ResolveNode) o).getSys_id(), null, null, null, username));
            }
        }
        
        //get the total count
        String countHql = "select COUNT(*) as COUNT " + hql;
        int total = ServiceHibernate.getTotalHqlCount(countHql);
        
        //prepare the response
        ResponseDTO<ResolveNodeVO> response = new ResponseDTO<ResolveNodeVO>();
        response.setRecords(result);
        response.setTotal(total);
        
        
        return response;
    }
    public static ResponseDTO<ResolveNodeVO> findNamespaceNodes(String compName, NodeType type, int start, int limit, String username) throws Exception
    {
        List<ResolveNodeVO> result = new ArrayList<ResolveNodeVO>();
        
        String hql = "from ResolveNode where 1=1";
        if(StringUtils.isNotBlank(compName))
        {
            hql = hql + " and LOWER(UCompName) like '%" + compName + "%'";
        }
        if(type != null)
        {
            hql = hql + " and UType = '" + type.name() + "'";
        }
        
        hql = hql + " order by UCompName";

        //query it, get the objs and convert to VOs 
        List<? extends Object> nodes = ServiceHibernate.executeHQLSelect(hql, start, limit);
        if(nodes != null)
        {
            for(Object o : nodes)
            {
                result.add(findNode(((ResolveNode) o).getSys_id(), null, null, null, username));
            }
        }
        
        //get the total count
        String countHql = "select COUNT(*) as COUNT " + hql;
        int total = ServiceHibernate.getTotalHqlCount(countHql);
        
        //prepare the response
        ResponseDTO<ResolveNodeVO> response = new ResponseDTO<ResolveNodeVO>();
        response.setRecords(result);
        response.setTotal(total);
        
        
        return response;
    }
    
    //create/update a Node 
    public static ResolveNodeVO persistNode(ResolveNodeVO node, String username) throws Exception
    {
        //validate the data
        if(node.validate())
        {
            //validate the uniqueness
            ResolveNode nodeModel = findResolveNode(null, node.getUCompSysId(), node.getUCompName(), node.getUType(), username);
            if(nodeModel != null)
            {
                if(StringUtils.isNotBlank(node.getSys_id()) && !node.getSys_id().equals(VO.STRING_DEFAULT) && !nodeModel.getSys_id().equalsIgnoreCase(node.getSys_id()))
                {
                    throw new Exception("Node with name '" + node.getUCompName() + "', type '"+ node.getUType() +"' already exist.");
                }
            }
            
            //persist
            try
            {
              HibernateProxy.setCurrentUser(username);
            	nodeModel = (ResolveNode) HibernateProxy.execute(() -> {
                
            		ResolveNode nodeModelInner = findResolveNode(node.getSys_id(), node.getUCompSysId(), node.getUCompName(), node.getUType(), username);
	                if(nodeModelInner == null)
	                {
	                    nodeModelInner = new ResolveNode(node);
	                }
	                else
	                {
	                    //delete all the properties first
	                    Collection<ResolveNodeProperties> nodeProperties = nodeModelInner.getProperties();
	                    if(nodeProperties != null && nodeProperties.size() > 0)
	                    {
	                        for(ResolveNodeProperties prop : nodeProperties)
	                        {
	                            HibernateUtil.getDAOFactory().getResolveNodePropertiesDAO().delete(prop);
	                        }
	                        HibernateUtil.getCurrentSession().flush();
	                    }
	                    
	                    //apply the new node values 
	                    nodeModelInner.applyVOToModel(node);
	                }
	                //persist the model
	                HibernateUtil.getDAOFactory().getResolveNodeDAO().persist(nodeModelInner);
	                
	                //Node Properties
	                Collection<ResolveNodeProperties> nodeProperties = nodeModelInner.getProperties();
	                if(nodeProperties != null && nodeProperties.size() > 0)
	                {
	                    for(ResolveNodeProperties prop : nodeProperties)
	                    {
	                        prop.setNode(nodeModelInner);
	
	                        if(StringUtils.isNotEmpty(prop.getSys_id()))
	                        {
	                            HibernateUtil.getDAOFactory().getResolveNodePropertiesDAO().replicate(prop);    
	                        }
	                        else
	                        {
	                            HibernateUtil.getDAOFactory().getResolveNodePropertiesDAO().persist(prop);
	                        }
	                    }//end of for loop
	                }
	                return nodeModelInner;
                
                });
            }
            catch(Exception e)
            {
                Log.log.error(e.getMessage(), e);               
                throw new Exception(e);
            }
            
            //update the data from the DB
            return nodeModel.doGetVO();
        }
        
        return node;
    }
    
    
    //delete the node, with all its edges, properties and edge properties
    public static void deleteNode(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId) && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        ResolveNode nodeModel = findResolveNode(sysId, compSysId, compName, compType, username);
        if(nodeModel != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
	
	                //get the reference of the node
                	ResolveNode nodeModelResult = HibernateUtil.getDAOFactory().getResolveNodeDAO().findById(nodeModel.getSys_id());
	                
	                //delete all the properties first
	                Collection<ResolveNodeProperties> nodeProperties = nodeModelResult.getProperties();
	                if (nodeProperties != null && nodeProperties.size() > 0)
	                {
	                    for (ResolveNodeProperties prop : nodeProperties)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveNodePropertiesDAO().delete(prop);
	                    }
	                    nodeProperties.clear();
	                }
	                
	                //delete the edges
	                Collection<ResolveEdge> edges = findAllEdgesWithSrcAndDest(nodeModelResult.getSys_id(), username);
	                if(edges != null && edges.size() > 0)
	                {
	                    for(ResolveEdge edge : edges)
	                    {
	                        Collection<ResolveEdgeProperties> properties = edge.getProperties();
	                        if(properties != null)
	                        {
	                            for(ResolveEdgeProperties edgeProp : properties)
	                            {
	                                HibernateUtil.getDAOFactory().getResolveEdgePropertiesDAO().delete(edgeProp);
	                            }
	                            properties.clear();
	                        }
	                        
	                        //delete the edge
	                        HibernateUtil.getDAOFactory().getResolveEdgeDAO().delete(edge);
	                        
	                    }//end of for loop
	                }
	
	                //finally, delete the node
	                HibernateUtil.getDAOFactory().getResolveNodeDAO().delete(nodeModelResult);
                
                });
            }
            catch(Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e);
            }
        }
        else
        {
            if (StringUtils.isNotEmpty(compName))
            {
                Log.log.warn("Cannot delete as Node " + compName + " does not exist");
            }
            else
            {
                Log.log.debug("No ResolveNode found for sysId: " + sysId + ", compName: " + compName + ", compSysId: " + compSysId + ", compType: " + compType);  
            }
        }
    }
    
    private static boolean isFollowRelationshipType(ResolveEdge edge, String relType)
    {
        return edge.getURelType().equalsIgnoreCase(relType);
    }
    
    //give me all the nodes that I am following
    public static Collection<ResolveNodeVO> getNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return getNodesFollowedBy(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.FOLLOWER.name());
    }

    public static Collection<ResolveNodeVO> getNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, String relType) throws Exception
    {
    	long startTime = System.currentTimeMillis();
    	long startTimeInstance = startTime;
    	
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId) && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, ""); 
        if(node == null)
        {
            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
        }
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("Time taken to find source node: %d msec", 
        								(System.currentTimeMillis() - startTimeInstance)));
        	startTimeInstance = System.currentTimeMillis();
        }
        
        //get destination (i.e. followed) nodes from all edges where this node is source 
        Collection<ResolveNode> followingNodes = findSrcOrDestNodeFromAllEdges(node.getSys_id(), true, username, relType);
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("Time taken to find %d destination (followed) nodes with node id %s as source and " +
        								"relation type %s: %d msec", followingNodes.size(), node.getSys_id(), relType, 
        								(System.currentTimeMillis() - startTimeInstance)));
        	startTimeInstance = System.currentTimeMillis();
        }
        
        if(CollectionUtils.isNotEmpty(followingNodes))
        {
            //model to VO
            
            followingNodes.parallelStream().forEach(followingNode -> { 
            	try {
	        		ResolveNodeVO rsNodeVO = followingNode.doGetVO();
	        		
	        		if (rsNodeVO != null) {
	        			result.add(rsNodeVO);
	        		}
            	} catch (Throwable t) {
        			Log.log.error(String.format("Error %sin adding node %s following source node %s, relation type %s",
							(StringUtils.isNotBlank(t.getMessage()) ? 
							 "[" + t.getMessage() + "] " : ""), followingNode, node.getSys_id(),
							 relType), t);
            	}
	        });
            
            if (Log.log.isDebugEnabled()) {
            	Log.log.debug(String.format("Time taken to get unique %d destination (followed) node VOs: %d msec",
            								result.size(), (System.currentTimeMillis() - startTimeInstance)));
            	Log.log.debug(String.format("Total Time Taken to get nodes for component id %s with relation type %s: " +
            								"%d msec", node.getSys_id(), relType, (System.currentTimeMillis() - startTime)));
            }
        }
        
        return result;
    }
    
    //give me all the nodes that are following me
    public static Collection<ResolveNodeVO> getNodesFollowingMe(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return getNodesFollowingMe(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
    public static Collection<ResolveNodeVO> getNodesFollowingMe(String sysId, String compSysId, String compName, NodeType compType, String username, String relType) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId))// && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        final Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, username); 
        if(node == null)
        {
            Log.log.info("Currently there are no nodes following" + (StringUtils.isNotBlank(sysId) ? " Node Sys Id = " + sysId : "") +
                         (StringUtils.isNotBlank(compSysId) ? " Component Sys Id = " + compSysId : "") + 
                         (StringUtils.isNotBlank(compName) ? " Component Name = " + compName : "") +
                         (compType != null ? " Component Type = " + compType.name() : "") + 
                         (StringUtils.isNotBlank(username) ? " User Name = " + username : "") + 
                         (StringUtils.isNotBlank(relType) ? " Relation Type = " + relType : ""));
            //throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
            return result;
        }
        
        //get source (i.e. following) nodes from all the edges where this node is destination
        Collection<ResolveNode> followingMeNodes = findSrcOrDestNodeFromAllEdges(node.getSys_id(), false, username, relType);
        
        if(CollectionUtils.isNotEmpty(followingMeNodes))
        {
            //model to VO
        	followingMeNodes.parallelStream().forEach(followingMeNode -> {
        		try {
	        		ResolveNodeVO rsNodeVO = followingMeNode.doGetVO();
	        		
	        		if (rsNodeVO != null) {
	        			result.add(rsNodeVO);
	        		}
        		} catch (Throwable t) {
        			Log.log.error(String.format("Error %sin adding node %s following destination node %s, relation type %s",
        										(StringUtils.isNotBlank(t.getMessage()) ? 
        										 "[" + t.getMessage() + "] " : ""), followingMeNode, node.getSys_id(),
        										 relType), t);
        		}
        	});
        }
        
        return result;
    }
    
//    public static void follow(String compSysId, String compName, NodeType compType, String followSysId, String followCompName, NodeType followType, Map<String, String> edgeProperties, String username) throws Exception
//    {
//       //NOT SURE IF WE WILL NEED THIS - will implement if needed
//    }
    
    public static void follow(String sysId, String compSysId, String compName, NodeType compType, Collection<ResolveNodeVO> followComponents, Map<String, String> edgeProperties, String username) throws Exception
    {
        addEdge(sysId, sysId, compName, compType, followComponents, edgeProperties, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
//    public static void unfollow(String compSysId, String compName, NodeType compType, String followSysId, String followCompName, NodeType followType) throws Exception
//    {
//        throw new Exception("Not Implemented Yet");
//    }
    
    public static void unfollow(String sysId, String compSysId, String compName, NodeType compType, Collection<ResolveNodeVO> unfollowComponents, String username) throws Exception
    {
        removeEdge(sysId, compSysId, compName, compType, unfollowComponents, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
    private static void prepareNotificationForUserFollowingComp(ResolveNode userNode, ResolveNode compNode, boolean follow, String userName)
    {
        String compType = compNode.getUType();
        
        if(follow)
        {
            if(compType.equalsIgnoreCase(NodeType.PROCESS.name()))
            {
                NotificationHelper.getProcessNotification(compNode.getUCompSysId(), userNode.getUCompName(), UserGlobalNotificationContainerType.PROCESS_USER_ADDED, userName, true, true);
            }
            else if(compType.equalsIgnoreCase(NodeType.TEAM.name()))
            {
                NotificationHelper.getTeamNotification(compNode.getUCompSysId(), userNode.getUCompName(), UserGlobalNotificationContainerType.TEAM_USER_ADDED, userName, true, true);
            }
            else if(compType.equalsIgnoreCase(NodeType.FORUM.name()))
            {
                NotificationHelper.getForumNotification(compNode.getUCompSysId(), userNode.getUCompName(), UserGlobalNotificationContainerType.FORUM_USER_ADDED, userName, true, true);
            }
            else if(compType.equalsIgnoreCase(NodeType.USER.name()))
            {
                //submitNotification = NotificationHelper.getUserNotification((User) comp, user, notificationType, user.getUsername(), false, true);
//                                                        getUserFollowNotification(User user, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
                NotificationHelper.getUserFollowNotification(compNode.getUCompSysId(), UserGlobalNotificationContainerType.USER_FOLLOW_ME, userName, true, true);
            }
        }
        else
        {
            if(compType.equalsIgnoreCase(NodeType.PROCESS.name()))
            {
                NotificationHelper.getProcessNotification(compNode.getUCompSysId(), userNode.getUCompName(), UserGlobalNotificationContainerType.PROCESS_USER_REMOVED, userName, true, false);
            }
            else if(compType.equalsIgnoreCase(NodeType.TEAM.name()))
            {
                NotificationHelper.getTeamNotification(compNode.getUCompSysId(), userNode.getUCompName(), UserGlobalNotificationContainerType.TEAM_USER_REMOVED, userName, true, false);
            }
            else if(compType.equalsIgnoreCase(NodeType.FORUM.name()))
            {
                NotificationHelper.getForumNotification(compNode.getUCompSysId(), userNode.getUCompName(), UserGlobalNotificationContainerType.FORUM_USER_REMOVED, userName, true, false);
            }
            else if(compType.equalsIgnoreCase(NodeType.USER.name()))
            {
                //submitNotification = NotificationHelper.getUserNotification((User) comp, user, notificationType, user.getUsername(), false, true);
                NotificationHelper.getUserFollowNotification(compNode.getUCompSysId(), UserGlobalNotificationContainerType.USER_UNFOLLOW_ME, userName, true, false);
            }
        }
    }
    
    public static void addEdge(String srcSysId, String srcCompSysId, String srcCompName, NodeType srcCompType, Collection<ResolveNodeVO> destComponents, Map<String, String> edgeProperties, String username, String relType) throws Exception
    {
        if(StringUtils.isNotBlank(srcSysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(srcCompSysId) && srcCompType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(srcCompName) && srcCompType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Source comp information is mandatory. Either sysId, or name and type.");
        }
        
        if(destComponents == null || destComponents.isEmpty() || relType == null || relType.isEmpty())
        {
            throw new Exception("At least one destnation component and relation type are mandatory.");
        }

        //make it unique
        Set<ResolveNodeVO> uniqueDestComponents = new HashSet<ResolveNodeVO>(destComponents);
        
        //make sure that the src node exist
        ResolveNode srcNode = findResolveNode(srcSysId, srcCompSysId, srcCompName, srcCompType, username); 
        if(srcNode == null)
        {
            throw new Exception("Source Node with id '" + srcCompSysId + "', name '" + srcCompName + "' does not exist");
        }
        
        //for each dest node, add the edge
        for(ResolveNodeVO destNodeVO : uniqueDestComponents)
        {
            ResolveNode destNode = findResolveNode(destNodeVO.getSys_id(), destNodeVO.getUCompSysId(), destNodeVO.getUCompName(), destNodeVO.getUType(), username);
            if(destNode != null)
            {
                try
                {
                    addEdge(srcNode, destNode, edgeProperties, username, relType);
                    
                    if(relType.equals(NonNeo4jRelationType.FOLLOWER))
                    {
                        prepareNotificationForUserFollowingComp (srcNode, destNode, true, username);
                    }
                }
                catch (Exception e)
                {
                    //just a warning...continue to the next one
                    Log.log.warn("Error adding edge from '" + srcNode.getUCompName() + "' to '" + destNode.getUCompName() + "' with relation type " + relType);
                }
            }
            else 
            {
                Log.log.warn("Destination Node with id '" + destNodeVO.getSys_id() + "', name '" + destNodeVO.getUCompName() + "' does not exist");
            }
        }//end of for loop
    }
    
    public static void removeEdge(String srcSysId, String srcCompSysId, String srcCompName, NodeType srcCompType, Collection<ResolveNodeVO> destComponents, String username, String relType) throws Exception
    {
        if(StringUtils.isNotBlank(srcSysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(srcCompSysId) && srcCompType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(srcCompName) && srcCompType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Source comp information is mandatory. Either sysId, or name and type.");
        }
        
        if(destComponents == null || destComponents.isEmpty() || relType == null || relType.isEmpty())
        {
            throw new Exception("At least one destnation component and relation type are mandatory.");
        }
        
        //make it unique
        Set<ResolveNodeVO> uniqueDestComponents = new HashSet<ResolveNodeVO>(destComponents);
        
        //make sure that the src node exist
        ResolveNode srcNode = findResolveNode(srcSysId, srcCompSysId, srcCompName, srcCompType, username); 
        if(srcNode == null)
        {
            throw new Exception("Source Node with" + (srcSysId != null ? " SysId " + srcSysId : "") +
                                (srcCompSysId != null ? " Component Sys Id " + srcCompSysId : "") + 
                                (srcCompName != null ? " Component Name " + srcCompName : "") +
                                (srcCompType != null ? " Component Type " + srcCompType : "") + 
                                " Not Found");
        }
        
        //for each dest node, remove the edge
        for(ResolveNodeVO destNodeVO : uniqueDestComponents)
        {
            ResolveNode destNode = findResolveNode(destNodeVO.getSys_id(), destNodeVO.getUCompSysId(), destNodeVO.getUCompName(), destNodeVO.getUType(), username);
            if(destNode != null)
            {
                try
                {
                    if(relType.equals(NonNeo4jRelationType.FOLLOWER.name()))
                    {
                        prepareNotificationForUserFollowingComp (srcNode, destNode, false, username);
                    }           
                    removeEdge(srcNode, destNode, username, relType);
                }
                catch (Exception e)
                {
                    //just a warning...continue to the next one
                    Log.log.warn("Error when '" + srcNode.getUCompName() + "' unfollowing '" + destNode.getUCompName() + "'");
                }
            }
            else 
            {
                Log.log.warn("Destination Node with id '" + destNodeVO.getSys_id() + "', name '" + destNodeVO.getUCompName() + "' does not exist");
            }
        }//end of for loop
    }
    
    //private apis
    private static ResolveNode findResolveNode(String sysId, String compSysId, String compName, NodeType type, String username) throws Exception
    {
        ResolveNode model = null;
        
        if(StringUtils.isNotEmpty(sysId) && !sysId.equals("UNDEFINED"))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	model = (ResolveNode) HibernateProxy.execute(() -> {
                
	                return HibernateUtil.getDAOFactory().getResolveNodeDAO().findById(sysId);
            	});
            	
                if(model != null && model.getProperties() != null)
                {
                    model.getProperties().size();//loading the properties
                }
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(compName) && type != null)
        {
            
            String normalizedCompName = compName;
            
            if (type == NodeType.USER)
            {
                int pos = compName.indexOf("\\");
                if (pos > 0)
                {
                    normalizedCompName = compName.substring(0, pos) + "\\" + compName.substring(pos + 1, compName.length());
                }
            }
            
            //to make this case-insensitive
            //String sql = "from ResolveNode where LOWER(UCompName) = '" + normalizedCompName.toLowerCase().trim() + "' and LOWER(UType) = '" + type.name().toLowerCase() + "'" ;
            String sql = "from ResolveNode where LOWER(UCompName) = :compName and LOWER(UType) = :typeName" ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("compName", normalizedCompName.toLowerCase().trim());
            queryParams.put("typeName", type.name().toLowerCase());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    if(list.size() != 1)
                    {
                        /*
                         * Node type other than NAMESPACE should always have unique u_comp_name
                         * irrespective of case.
                         * 
                         * Pre 5.0, name spaces were case sensitive i.e. Test and test were considered as
                         * two unique name spaces. From 5.0, name spaces were considered as case insensitive 
                         * and comparison of name space was made case insensitive.
                         * 
                         * To allow old case senstitive name paces to co-mingle with new case insenstitive
                         * name space, if multiple record are returned for name space then exact matched
                         * nme space including case is returned and warning is logged.
                         */
                        
                        if (type != NodeType.NAMESPACE)
                        {
                            throw new Exception("Inconsistent Data Issue. Resolve Administrator needs to fix the data manually. There are more than 1 Nodes with compName " + compName + " (case insensitive) of type " + type + " which should not be the case.");
                        }
                        else // NodeType = NAMESPACE
                        {
                            Log.log.warn("Inconsistent Name Space Data Issue. Resolve Administrator needs to fix the name space data manually. There are more than 1 Name Space Nodes with compName " + compName + " (case insensitive) of type " + type + " which should not be the case.");
                            
                            for (Object obj : list)
                            {
                                ResolveNode rsNode = (ResolveNode)obj;
                                
                                if (rsNode.getUCompName().equals(normalizedCompName.trim()))
                                {
                                    model = rsNode;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {                    
                        model = (ResolveNode) list.get(0);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }
        }
        else if(StringUtils.isNotBlank(compSysId))// && type != null)
        {
            String[] compSysIdParts = StringUtils.split(compSysId, "-");
            
            if(compSysIdParts.length < 1)
            {
                Log.log.warn("Invalid component sys id :" + compSysId);
                return model;
            }
            
          //to make this case-insensitive
            //String sql = "from ResolveNode where UCompSysId = '" + compSysIdParts[0] + "'" ;
            String sql = "from ResolveNode where UCompSysId = :UCompSysId" ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UCompSysId", compSysIdParts[0]);
            
            if(type != null)
            {
                //sql = sql + " and UType = '" + type.name() + "'" ;
            	if (type.name().equals("DOCUMENT"))
            	{
            		sql = sql + " and (UType = :UType1 OR UType = :UType2 OR UType = :UType3) " ;
            		queryParams.put("UType1", type.name());
            		queryParams.put("UType2", NodeType.PLAYBOOK_TEMPLATE.name());
            		queryParams.put("UType3", NodeType.SIR_PLAYBOOK.name());
            	}
            	else
            	{
            		sql = sql + " and UType = :UType";
            		queryParams.put("UType", type.name());
            	}
                
            }
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    if(list.size() != 1)
                    {
                        throw new Exception("There are more than 1 Node with component sysId " + compSysId + (type != null ? " of type " + type.name() : "") + " which should not be the case.");
                    }
                    
                    model = (ResolveNode) list.get(0);
                    
                    //** RECURSIVE ONLY ONCE
                    model = findResolveNode(model.getSys_id(), null, null, null, username);//this will load the properties also for this model
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }
        }
        
        return model;
    }
    
    private static ResolveEdge findResolveEdge(String srcSysId, String destSysId, String username, String relType) throws Exception
    {
        ResolveEdge model = null;
        
        if(StringUtils.isNotBlank(srcSysId) && StringUtils.isNotBlank(destSysId) && StringUtils.isNotBlank(relType))
        {
          //to make this case-insensitive
            /*String sql = "from ResolveEdge where sourceNode = '" + srcSysId.trim() + 
                         "' and destinationNode = '" + destSysId.trim() + "' and URelType = '" + relType + "'";*/
            String sql = "from ResolveEdge where sourceNode.sys_id = :srcSysId and destinationNode.sys_id = :destSysId and URelType = :URelType";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("srcSysId", srcSysId.trim());
            queryParams.put("destSysId", destSysId.trim());
            queryParams.put("URelType", relType);
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    model = (ResolveEdge) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }            
        }
        
        return model;
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveEdge> findAllEdgesWithSrcAndDest(String nodeSysId, String username) throws Exception
    {
        List<ResolveEdge> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            //String sql = "from ResolveEdge where sourceNode = '" + nodeSysId.trim() + "' or destinationNode = '" + nodeSysId.trim() + "'" ;
            String sql = "from ResolveEdge where sourceNode.sys_id = :nodeSysId or destinationNode.sys_id = :nodeSysId" ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("nodeSysId", nodeSysId.trim());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveEdge>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveEdge> findAllEdgesWithSrcOrDest(String nodeSysId, boolean isSrc, String username, String relType) throws Exception
    {
        List<ResolveEdge> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = null ;
            if(isSrc)
            {
                //sql = "from ResolveEdge where sourceNode = '" + nodeSysId.trim() + "'" ;
                sql = "from ResolveEdge where sourceNode.sys_id = :nodeSysId" ;
            }
            else
            {
                //sql = "from ResolveEdge where destinationNode = '" + nodeSysId.trim() + "'" ;
                sql = "from ResolveEdge where destinationNode.sys_id = :nodeSysId" ;
            }
            
            //sql += " and URelType = '" + relType + "'";
            sql += " and URelType = :URelType";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("nodeSysId", nodeSysId.trim());
            queryParams.put("URelType", relType);
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveEdge>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    private static void addEdge(ResolveNode srcNode, ResolveNode destNode, Map<String, String> edgeProperties, String username, String relType) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            ResolveEdge edge = findResolveEdge(srcNode.getSys_id(), destNode.getSys_id(), username, relType);
	            if (edge == null)
	            {
	                edge = new ResolveEdge();
	                edge.setURelType(relType);
	                edge.setSourceNode(srcNode);
	                edge.setDestinationNode(destNode);
	
	                HibernateUtil.getDAOFactory().getResolveEdgeDAO().persist(edge);
	            }
	            else
	            {
	                //delete all the properties first
	                Collection<ResolveEdgeProperties> dbEdgeProperties = edge.getProperties();
	                if (dbEdgeProperties != null && dbEdgeProperties.size() > 0)
	                {
	                    for (ResolveEdgeProperties prop : dbEdgeProperties)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveEdgePropertiesDAO().delete(prop);
	                    }
	                    HibernateUtil.getCurrentSession().flush();
	                }
	            }
	
	            //add the properties
	            if (edgeProperties != null && edgeProperties.size() > 0)
	            {
	                Iterator<String> it = edgeProperties.keySet().iterator();
	                while (it.hasNext())
	                {
	                    String key = it.next();
	                    String value = edgeProperties.get(key);
	
	                    ResolveEdgeProperties prop = new ResolveEdgeProperties();
	                    prop.setUKey(key);
	                    prop.setUValue(value);
	                    prop.setEdge(edge);
	
	                    HibernateUtil.getDAOFactory().getResolveEdgePropertiesDAO().persist(prop);
	                }//end of while loop
	            }

        });

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    }
    
    private static void removeEdge(ResolveNode srcNode, ResolveNode destNode, String username, String relType) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		ResolveEdge edge = findResolveEdge(srcNode.getSys_id(), destNode.getSys_id(), username, relType);
                if (edge != null)
                {
                    //delete all the properties first
                    Collection<ResolveEdgeProperties> dbEdgeProperties = edge.getProperties();
                    if (dbEdgeProperties != null && dbEdgeProperties.size() > 0)
                    {
                        for (ResolveEdgeProperties prop : dbEdgeProperties)
                        {
                            HibernateUtil.getDAOFactory().getResolveEdgePropertiesDAO().delete(prop);
                        }
                        HibernateUtil.getCurrentSession().flush();
                    }
                
                    //delete the edge
                    HibernateUtil.getDAOFactory().getResolveEdgeDAO().delete(edge);
                }
                
        	});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    }
    
    //create Nodes (batch opertaion) 
    public static Collection<ResolveNodeVO> persistNodes(Collection<ResolveNodeVO> nodes, String username, int batchSize) throws Exception
    {
        Collection<ResolveNodeVO> pnodes = new ArrayList<ResolveNodeVO>(batchSize);
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.executeNoCache(() -> {
            	 for(ResolveNodeVO node : nodes)
                 {
                     ResolveNode nodeModel = new ResolveNode(node);
                     ResolveNode pNodeModel = HibernateUtil.getDAOFactory().getResolveNodeDAO().save(nodeModel);
                     
                     Set<ResolveNodeProperties> pnodeProperties = new HashSet<ResolveNodeProperties>();
                     
                     for(ResolveNodeProperties nodeProp : nodeModel.getProperties())
                     {
                         nodeProp.setNode(nodeModel);
                         ResolveNodeProperties pnodePropertiesModel = 
                                         HibernateUtil.getDAOFactory().getResolveNodePropertiesDAO().save(nodeProp);
                         pnodeProperties.add(pnodePropertiesModel);
                     }
                     
                     pNodeModel.setProperties(pnodeProperties);
                     pnodes.add(pNodeModel.doGetVO());
                 }
            }); 

        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        
        return pnodes;
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveEdge> findAllEdgesWithSrcOrDestAndProcessOrTeamNodes(String nodeSysId, boolean isSrc, String username, String relType) throws Exception
    {
        List<ResolveEdge> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = null ;
            if(isSrc)
            {
                sql = "from ResolveEdge where sourceNode.sys_id = :nodeSysId and ( destinationNode.UType = 'PROCESS' or destinationNode.UType = 'TEAM')" ;
            }
            else
            {
                sql = "from ResolveEdge where destinationNode.sys_id = :nodeSysId and ( sourceNode.UType = 'PROCESS' or sourceNode.UType = 'TEAM')" ;
            }
            
            //sql += " and URelType = '" + relType + "'";
            sql += " and URelType = :URelType";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("nodeSysId", nodeSysId.trim());
            queryParams.put("URelType", relType);
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveEdge>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    //give me all proces and team nodes that are following me
    public static Collection<ResolveNodeVO> getProcessAndTeamNodesFollowingMe(String sysId, String username) throws Exception
    {
        if(!StringUtils.isNotBlank(sysId) || !StringUtils.isNotBlank(username))
        {
            throw new Exception("sysId and username is mandatory.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, null, null, null, ""); 
        if(node == null)
        {
            throw new Exception("Node with id '" +sysId + "' does not exist");
        }
        
        //get all the edges where this node is 'src'
        Collection<ResolveEdge> edges = findAllEdgesWithSrcOrDestAndProcessOrTeamNodes(node.getSys_id(), false, username, NonNeo4jRelationType.FOLLOWER.name());
        
        if(edges != null && edges.size() > 0)
        {
            Collection<ResolveNode> followingMeNodes = new HashSet<ResolveNode>();
            try
            {
                for (ResolveEdge edge : edges)
                {
                    if (edge != null )
                    {
                        followingMeNodes.add(edge.getSourceNode());
                    }
                }//end of for loop
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }

            //model to VO
            for(ResolveNode followingMeNode : followingMeNodes)
            {
                result.add(followingMeNode.doGetVO());
            }
        }
        
        return result;
    }
    
    public static ResolveEdge createEdge(String srcSysId, String destSysId, Map<String, String> edgeProperties, String username, String relType, Map<String, ResolveNode> nodesInRelationCache) throws Exception
    {
        if(!StringUtils.isNotBlank(srcSysId) || !StringUtils.isNotBlank(destSysId) || !StringUtils.isNotBlank(relType))
        {
            throw new Exception("Source sysId, destination sysId and relation type are mandatory.");
        }
        
        //make sure that the src node exists
        ResolveNode srcNode = nodesInRelationCache.get(srcSysId);
        
        if(srcNode == null)
        {
            srcNode = findResolveNode(null, srcSysId, null, null, username);
            nodesInRelationCache.put(srcSysId, srcNode);
        }
        
        if(srcNode == null)
        {
            throw new Exception("Source Node for relation of type " + relType + " with component sys id '" + srcSysId + "' does not exist");
        }
        
        //make sure that the destination node exists
        ResolveNode destNode = nodesInRelationCache.get(destSysId);
        
        if(destNode == null)
        {
            destNode = findResolveNode(null, destSysId, null, null, username);
            nodesInRelationCache.put(destSysId, destNode);
        }
        
        if(destNode == null)
        {
            throw new Exception("Destination Node for relation of type " + relType + " with component sys id '" + destSysId + "' does not exist");
        }
        
        ResolveEdge edge = new ResolveEdge();
        edge.setURelType(relType);
        edge.setSourceNode(srcNode);
        edge.setDestinationNode(destNode);
        
        if(edgeProperties != null && !edgeProperties.isEmpty())
        {
            Collection<ResolveEdgeProperties> edgeProps = new ArrayList<ResolveEdgeProperties>();
            
            for(String key: edgeProperties.keySet())
            {
                ResolveEdgeProperties edgeProp = new ResolveEdgeProperties();
                edgeProp.setUKey(key);
                edgeProp.setUValue(edgeProperties.get(key));
                edgeProps.add(edgeProp);
            }
            
            edge.setProperties(edgeProps);
        }
        
        return edge;
    }
    
    public static int persistEdges(Collection<ResolveEdge> edges, String username) throws Exception
    {
        int count = 0;
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            count = (int) HibernateProxy.executeNoCache(() -> {
            	int c = 0;
            	for(ResolveEdge edge : edges)
                {
                    HibernateUtil.getDAOFactory().getResolveEdgeDAO().save(edge);
                    c++;
                }
            	
            	return c;
            });
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        
        return count;
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveEdge> findAllEdgesWithSrcOrDestExceptDocAndAT(String nodeSysId, boolean isSrc, String username, String relType) throws Exception
    {
        List<ResolveEdge> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = null ;
            if(isSrc)
            {
                //sql = "from ResolveEdge where sourceNode = '" + nodeSysId.trim() + "' and destinationNode.UType != 'DOCUMENT' and destinationNode.UType != 'ACTIONTASK'" ;
                sql = "from ResolveEdge where sourceNode.sys_id = :nodeSysId and destinationNode.UType != 'DOCUMENT' and destinationNode.UType != 'ACTIONTASK'" ;
            }
            else
            {
                //sql = "from ResolveEdge where destinationNode = '" + nodeSysId.trim() + "' and sourceNode.UType != 'DOCUMENT' and sourceNode.UType != 'ACTIONTASK'" ;
                sql = "from ResolveEdge where destinationNode.sys_id = :nodeSysId and sourceNode.UType != 'DOCUMENT' and sourceNode.UType != 'ACTIONTASK'" ;
            }
            
            //sql += " and URelType = '" + relType + "'";
            sql += " and URelType = :URelType";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("nodeSysId", nodeSysId.trim());
            queryParams.put("URelType", relType);
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveEdge>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    public static Collection<ResolveNodeVO> getNodesFollowedByExceptDocAndAT(String sysId, String compSysId, String compName, NodeType compType, String username, String relType) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId) && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, ""); 
        if(node == null)
        {
            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
        }
        
        //get all the edges where this node is 'src'
        Collection<ResolveEdge> edges = findAllEdgesWithSrcOrDestExceptDocAndAT(node.getSys_id(), true, username, relType);
        
        if(edges != null && edges.size() > 0)
        {
            Collection<ResolveNode> followingNodes = new HashSet<ResolveNode>();
            try
            {
                for (ResolveEdge edge : edges)
                {
                    followingNodes.add(edge.getDestinationNode());
                }//end of for loop
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }

            //model to VO
            for(ResolveNode followingNode : followingNodes)
            {
                result.add(followingNode.doGetVO());
            }
            
        }
        
        return result;
    }
    
    //give me all the nodes that I am following except Document and ActionTask
    public static Collection<ResolveNodeVO> getNodesFollowedByExceptDocAndAT(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return getNodesFollowedByExceptDocAndAT(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveEdge> findAllEdgesWithSrcOrDestAndProcessNode(String nodeSysId, boolean isSrc, String username, String relType) throws Exception
    {
        List<ResolveEdge> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = null ;
            if(isSrc)
            {
                //sql = "from ResolveEdge where sourceNode = '" + nodeSysId.trim() + "' and destinationNode.UType = 'PROCESS'" ;
                sql = "from ResolveEdge where sourceNode.sys_id = :nodeSysId and destinationNode.UType = 'PROCESS'" ;
            }
            else
            {
                //sql = "from ResolveEdge where destinationNode = '" + nodeSysId.trim() + "' and sourceNode.UType = 'PROCESS'" ;
                sql = "from ResolveEdge where destinationNode.sys_id = :nodeSysId and sourceNode.UType = 'PROCESS'" ;
            }
            
            //sql += " and URelType = '" + relType + "'";
            sql += " and URelType = :URelType";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("nodeSysId", nodeSysId.trim());
            queryParams.put("URelType", relType);
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveEdge>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    //give me all the process nodes that I am following
    public static Collection<ResolveNodeVO> getProcessNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return getProcessNodesFollowedBy(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.FOLLOWER.name());
    }

    public static Collection<ResolveNodeVO> getProcessNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, String relType) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId) && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist provided valid sysId or valid compSysId and compName
        
        if ((StringUtils.isNotBlank(sysId) && (!NULL.equalsIgnoreCase(sysId))) || 
        	(StringUtils.isNotBlank(compSysId) && StringUtils.isNotBlank(compName) &&
        	 (!NULL.equalsIgnoreCase(compSysId)) && (!NULL.equalsIgnoreCase(compName)))) {
	        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, ""); 
	        if(node == null)
	        {
	            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
	        }
	        
	        //get all the edges where this node is 'src'
	        Collection<ResolveEdge> edges = findAllEdgesWithSrcOrDestAndProcessNode(node.getSys_id(), true, username, relType);
	        
	        if(edges != null && edges.size() > 0)
	        {
	            Collection<ResolveNode> followingNodes = new HashSet<ResolveNode>();
	            try
	            {
	                for (ResolveEdge edge : edges)
	                {
	                    followingNodes.add(edge.getDestinationNode());
	                }//end of for loop
	            }
	            catch (Exception e)
	            {
	                Log.log.warn(e.getMessage(), e);
	            }
	
	            //model to VO
	            for(ResolveNode followingNode : followingNodes)
	            {
	                result.add(followingNode.doGetVO());
	            }	            
	        }
        }
        
        return result;
    }
    
    //give me all the process nodes that are following me
    public static Collection<ResolveNodeVO> getProcessNodesFollowingMe(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return getProcessNodesFollowingMe(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
    public static Collection<ResolveNodeVO> getProcessNodesFollowingMe(String sysId, String compSysId, String compName, NodeType compType, String username, String relType) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId))// && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, username); 
        if(node == null)
        {
            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
        }
        
        //get all the edges where this node is 'dest' with 
        Collection<ResolveEdge> edges = findAllEdgesWithSrcOrDestAndProcessNode(node.getSys_id(), false, username, relType);
        if(edges != null && edges.size() > 0)
        {
            Collection<ResolveNode> followingMeNodes = new HashSet<ResolveNode>();
            try
            {
                for (ResolveEdge edge : edges)
                {
                    if (edge != null )
                    {
                        followingMeNodes.add(edge.getSourceNode());
                    }
                }//end of for loop
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage());
            }

            //model to VO
            for(ResolveNode followingMeNode : followingMeNodes)
            {
                result.add(followingMeNode.doGetVO());
            }
            
        }
        
        return result;
    }
    
    //give me all proces nodes that are following me
    public static Collection<ResolveNodeVO> getProcessNodesFollowingMe(String sysId, String username) throws Exception
    {
        return getProcessNodesFollowingMe(sysId, null, null, null, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveEdge> findAllEdgesWithSrcOrDestAndDocumentNode(String nodeSysId, boolean isSrc, String username, String relType, int start, int limit) throws Exception
    {
        List<ResolveEdge> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = null ;
            if(isSrc)
            {
                sql = "from ResolveEdge where sourceNode.sys_id = '" + nodeSysId.trim() + "' and destinationNode.UType = 'DOCUMENT'" ;
            }
            else
            {
                sql = "from ResolveEdge where destinationNode.sys_id = '" + nodeSysId.trim() + "' and sourceNode.UType = 'DOCUMENT'" ;
            }
            
            sql += " and URelType = '" + relType + "'";
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, start, limit);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveEdge>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveNode> findDistinctDocumentNodesFollowedBy(String nodeSysId, String relType, int start, int limit) throws Exception
    {
        List<ResolveNode> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = "select distinct e.destinationNode from ResolveEdge e where e.sourceNode = '" + nodeSysId.trim() + 
                         "' and e.destinationNode.UType = 'DOCUMENT' and e.URelType = '" + relType + "' order by e.destinationNode.UCompName";
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, start, limit);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveNode>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    public static Collection<ResolveNodeVO> getDocumentNodesFollowingMe(String sysId, String compSysId, String compName, NodeType compType, String username, String relType, int start, int limit) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId))// && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, username); 
        if(node == null)
        {
            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
        }
        
        //get all the edges where this node is 'dest' with 
        Collection<ResolveEdge> edges = findAllEdgesWithSrcOrDestAndDocumentNode(node.getSys_id(), false, username, relType, start, limit);
        if(edges != null && edges.size() > 0)
        {
            Collection<ResolveNode> followingMeNodes = new HashSet<ResolveNode>();
            try
            {
                for (ResolveEdge edge : edges)
                {
                    if (edge != null )
                    {
                        followingMeNodes.add(edge.getSourceNode());
                    }
                }//end of for loop
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage());
            }

            //model to VO
            for(ResolveNode followingMeNode : followingMeNodes)
            {
                result.add(followingMeNode.doGetVO());
            }
            
        }
        
        return result;
    }
    
    //give me all the Document nodes that I am following
    public static Collection<ResolveNodeVO> getDocumentNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, int start, int limit) throws Exception
    {
        return getDocumentNodesFollowedBy(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.FOLLOWER.name(), start, limit);
    }

    public static Collection<ResolveNodeVO> getDocumentNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, String relType, int start, int limit) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId) && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, ""); 
        if(node == null)
        {
            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
        }
        
        Collection<ResolveNode> followingNodes = findDistinctDocumentNodesFollowedBy(node.getSys_id(), relType, start, limit);
        
        if (followingNodes != null && !followingNodes.isEmpty())
        {
            //model to VO
            for(ResolveNode followingNode : followingNodes)
            {
                result.add(followingNode.doGetVO());
            }
        }
        
        return result;
    }
    
    private static int countEdgesWithSrcAndDestNodesInRelation(String srcNodeSysId, String destNodeSysId, String username, String relType) throws Exception
    {
        int result = 0;
        
        if(StringUtils.isNotBlank(srcNodeSysId) && StringUtils.isNotBlank(destNodeSysId) && StringUtils.isNotBlank(relType))
        {
            //to make this case-insensitive
            String sql = "select count(*) from ResolveEdge where sourceNode.sys_id = '" + srcNodeSysId + "' and destinationNode.sys_id = '" + destNodeSysId + "' and URelType = '" + relType + "'";
            
            try
            {
                result = GeneralHibernateUtil.getTotalHqlCount(sql);
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    public static boolean isSrcInDirectRelationshipToDest(String srcNodeId, String destNodeId, String username, String relType) throws Exception
    {
        if(StringUtils.isBlank(srcNodeId) || StringUtils.isBlank(destNodeId) || StringUtils.isBlank(relType))
        {
            throw new Exception("Source node id, destination node id and relationship type is necessary.");
        }
        
        //check if the source node exist
        ResolveNode srcNode = findResolveNode(srcNodeId, null, null, null, username); 
        if(srcNode == null)
        {
            throw new Exception("Source Node with id '" + srcNodeId + "' does not exist");
        }
        
        //check if the destination node exist
        ResolveNode destNode = findResolveNode(destNodeId, null, null, null, username); 
        if(destNode == null)
        {
            throw new Exception("Destination Node with id '" + destNodeId + "' does not exist");
        }
        
        //get count of edges where source node is in specified relation with destination node
        int edgeCount = countEdgesWithSrcAndDestNodesInRelation(srcNode.getSys_id(), destNode.getSys_id(), username, relType);
        
        return edgeCount >= 1;
    }
    
    private static long countEdgesWithSrcAndDocumentDestNodesInRelation(String srcNodeSysId, String username, String relType) throws Exception
    {
        long result = 0;
        
        if(StringUtils.isNotBlank(srcNodeSysId) && StringUtils.isNotBlank(relType))
        {
            //to make this case-insensitive
            String sql = "select count(distinct e.destinationNode) from ResolveEdge e where e.sourceNode.sys_id = '" + srcNodeSysId + "' and e.destinationNode.UType = 'DOCUMENT' and e.URelType = '" + relType + "'";
            
            try
            {
                result = GeneralHibernateUtil.getTotalHqlCountLong(sql);
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    public static long countSrcInDirectRelationshipToDocumentDest(String srcNodeId, String username, String relType) throws Exception
    {
        if(StringUtils.isBlank(srcNodeId) || StringUtils.isBlank(relType))
        {
            throw new Exception("Source node id, and relationship type is necessary.");
        }
        
        //check if the source node exist
        ResolveNode srcNode = findResolveNode(null, srcNodeId, null, null, username); 
        if(srcNode == null)
        {
            throw new Exception("Source Node with id '" + srcNodeId + "' does not exist");
        }
        
        //get count of edges where source node is in specified relation with destination node of type Document
        return countEdgesWithSrcAndDocumentDestNodesInRelation(srcNode.getSys_id(), username, relType);
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveEdge> findAllEdgesWithSrcOrDestAndActionTaskNode(String nodeSysId, boolean isSrc, String username, String relType, int start, int limit) throws Exception
    {
        List<ResolveEdge> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = null ;
            if(isSrc)
            {
                sql = "from ResolveEdge where sourceNode = '" + nodeSysId.trim() + "' and destinationNode.UType = 'ACTIONTASK'" ;
            }
            else
            {
                sql = "from ResolveEdge where destinationNode = '" + nodeSysId.trim() + "' and sourceNode.UType = 'ACTIONTASK'" ;
            }
            
            sql += " and URelType = '" + relType + "'";
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, start, limit);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveEdge>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveNode> findDistinctActionTaskNodesFollowedBy(String nodeSysId, String relType, int start, int limit) throws Exception
    {
        List<ResolveNode> result = null;
        
        if(StringUtils.isNotBlank(nodeSysId))
        {
          //to make this case-insensitive
            String sql = "select distinct e.destinationNode from ResolveEdge e where e.sourceNode = '" + nodeSysId.trim() + 
                         "' and e.destinationNode.UType = 'ACTIONTASK' and e.URelType = '" + relType + "' order by e.destinationNode.UCompName";
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, start, limit);
                if(list != null && list.size() > 0)
                {
                    result = (List<ResolveNode>) list;
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    //give me all the Action Task nodes that I am following
    public static Collection<ResolveNodeVO> getActionTaskNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, int start, int limit) throws Exception
    {
        return getActionTaskNodesFollowedBy(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.FOLLOWER.name(), start, limit);
    }

    public static Collection<ResolveNodeVO> getActionTaskNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, String relType, int start, int limit) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId) && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, ""); 
        if(node == null)
        {
            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
        }
        
        Collection<ResolveNode> followingNodes = findDistinctActionTaskNodesFollowedBy(node.getSys_id(), relType, start, limit);
        
        if (followingNodes != null && !followingNodes.isEmpty())
        {
            //model to VO
            for(ResolveNode followingNode : followingNodes)
            {
                result.add(followingNode.doGetVO());
            }
        }
        
        return result;
    }
    
    public static Collection<ResolveNodeVO> getActionTaskNodesFollowingMe(String sysId, String compSysId, String compName, NodeType compType, String username, String relType, int start, int limit) throws Exception
    {
        if(StringUtils.isNotBlank(sysId))
        {
            //good
        }
        else if(StringUtils.isNotBlank(compSysId))// && compType != null)
        {
            //good
        }
        else if(StringUtils.isNotBlank(compName) && compType != null)
        {
            //good
        }
        else
        {
            throw new Exception("Comp information is mandatory. Either sysId, or name and type.");
        }
        
        Collection<ResolveNodeVO> result = new HashSet<ResolveNodeVO>();
        
        //check if the node exist
        ResolveNode node = findResolveNode(sysId, compSysId, compName, compType, username); 
        if(node == null)
        {
            throw new Exception("Node with id '" + compSysId + "', name '" + compName + "' does not exist");
        }
        
        //get all the edges where this node is 'dest' with 
        Collection<ResolveEdge> edges = findAllEdgesWithSrcOrDestAndActionTaskNode(node.getSys_id(), false, username, relType, start, limit);
        if(edges != null && edges.size() > 0)
        {
            Collection<ResolveNode> followingMeNodes = new HashSet<ResolveNode>();
            try
            {
                for (ResolveEdge edge : edges)
                {
                    if (edge != null )
                    {
                        followingMeNodes.add(edge.getSourceNode());
                    }
                }//end of for loop
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage());
            }

            //model to VO
            for(ResolveNode followingMeNode : followingMeNodes)
            {
                result.add(followingMeNode.doGetVO());
            }
            
        }
        
        return result;
    }
    
    private static long countEdgesWithSrcAndActionTaskDestNodesInRelation(String srcNodeSysId, String username, String relType) throws Exception
    {
        long result = 0;
        
        if(StringUtils.isNotBlank(srcNodeSysId) && StringUtils.isNotBlank(relType))
        {
            //to make this case-insensitive
            String sql = "select count(distinct e.destinationNode) from ResolveEdge e where e.sourceNode.sys_id = '" + srcNodeSysId + "' and e.destinationNode.UType = 'ACTIONTASK' and e.URelType = '" + relType + "'";
            
            try
            {
                result = GeneralHibernateUtil.getTotalHqlCountLong(sql);
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    public static long countSrcInDirectRelationshipToActionTaskDest(String srcNodeId, String username, String relType) throws Exception
    {
        if(StringUtils.isBlank(srcNodeId) || StringUtils.isBlank(relType))
        {
            throw new Exception("Source node id, and relationship type is necessary.");
        }
        
        //check if the source node exist
        ResolveNode srcNode = findResolveNode(null, srcNodeId, null, null, username); 
        if(srcNode == null)
        {
            throw new Exception("Source Node with id '" + srcNodeId + "' does not exist");
        }
        
        //get count of edges where source node is in specified relation with destination node of type Document
        return countEdgesWithSrcAndActionTaskDestNodesInRelation(srcNode.getSys_id(), username, relType);
    }
    
    @SuppressWarnings("unchecked")
    private static Collection<ResolveNode> findSrcOrDestNodeFromAllEdges(String nodeSysId, boolean isSrc, String username, 
    																     String relType) throws Exception {
        List<ResolveNode> result = new ArrayList<ResolveNode>();
        
        final Set<ResolveNode> resultSet = new HashSet<ResolveNode>();
        
        if(StringUtils.isNotBlank(nodeSysId)) {
            StringBuilder sqlBldr = new StringBuilder("select ");
            
            if(isSrc) {
                sqlBldr.append("u_dest_sys_id");
            } else {
            	sqlBldr.append("u_src_sys_id");
            }
            
            sqlBldr.append(" from resolve_edge where ");
            
            if(isSrc) {
            	sqlBldr.append("u_src_sys_id");
            } else {
            	sqlBldr.append("u_dest_sys_id");
            }
            
            sqlBldr.append(" = :nodeSysId and u_rel_type = :URelType");
            
            if (Log.log.isDebugEnabled()) {
            	Log.log.debug(String.format("%s type nodes from %s node %s SQL query %s", relType,
            								(isSrc ? "source" : "destination"), nodeSysId, sqlBldr));
            }
            
            try {
            	HibernateProxy.setCurrentUser(username);
            	List<Object> nodeIdObjs = (List<Object>) HibernateProxy.executeHibernateInitLocked(() -> {
            		Query sqlQuery = HibernateUtil.createSQLQuery(sqlBldr.toString());
                    sqlQuery.setParameter("nodeSysId", nodeSysId, StringType.INSTANCE);
                    sqlQuery.setParameter("URelType", relType, StringType.INSTANCE);
                    return sqlQuery.list();
            	});
                
                if (CollectionUtils.isNotEmpty(nodeIdObjs)) {
                	
                	nodeIdObjs.parallelStream().forEach(nodeIdObj -> {
                		
                		if (StringUtils.isNotBlank((String)nodeIdObj)) {
	                		try {
	                           
	                        	HibernateProxy.setCurrentUser(nodeSysId + "-" + (String)nodeIdObj);
	                            ResolveNode rsNode = (ResolveNode) HibernateProxy.executeHibernateInitLocked(() -> {
	                            	return HibernateUtil.getDAOFactory().getResolveNodeDAO()
	                            					 .findById((String)nodeIdObj);
	                            });	                            
	                            
	                            if (rsNode != null && rsNode.doGetVO() != null) {
	                            	resultSet.add(rsNode);
	                            } else {
	                            	Log.log.warn(String.format("Failed to find Resolve Node or failed to get VO for node " +
	                            							   "id %s", (String)nodeIdObj));
	                            }
	                        } catch(Exception e) {
	                            Log.log.warn(e.getMessage(), e);
	                        } catch(Throwable t) {
	                        	Log.log.error(String.format("Error %sin processing results of get %s type nodes from %s " +
	                        								"node %s",
	                        								(StringUtils.isNotBlank(t.getMessage()) ? 
	                        								 "[" + t.getMessage() + "] " : ""),
	                        								relType, (isSrc ? "source" : "destination"), nodeSysId), t);
                                HibernateUtil.rethrowNestedTransaction(t);
                                try {
									throw new Exception(t);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
	                        }
                		}
                	});
                }
            } catch(Throwable t) {
                Log.log.error(String.format("Error %sin getting %s type nodes from %s node %s",
                							(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
                							relType, (isSrc ? "source" : "destination"), nodeSysId), t);
                HibernateUtil.rethrowNestedTransaction(t);
                throw new Exception(t);
            }
        }
        
        if (CollectionUtils.isNotEmpty(resultSet)) {
        	result = resultSet.parallelStream().filter(rn -> rn != null).collect(Collectors.toList());
        }
        
        return result;
    }
}
