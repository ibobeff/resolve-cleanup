/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.neo4j.graphdb.Node;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskResolveTagRel;
import com.resolve.persistence.model.ResolveEdge;
import com.resolve.persistence.model.ResolveNode;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocResolveTagRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceTag;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;
import com.resolve.services.migration.social.ExportProcessGraph;
import com.resolve.services.migration.social.ExportTagGraph;
import com.resolve.services.migration.social.ExportTeamGraph;
import com.resolve.services.migration.social.ExportUserGraph;
import com.resolve.services.migration.social.GraphRelationshipDTO;
import com.resolve.services.migration.social.NonNeo4jRelationType;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

public class MigrateSocialRelationships
{
    private final int FIXED_POOL_THREAD_COUNT = 20;
    //thread pool service
    private ExecutorService service = Executors.newFixedThreadPool(FIXED_POOL_THREAD_COUNT);
    //Map of ResolveNodes in relationship by node sys id
    Map<String, ResolveNode> nodesInRelationCache = new HashMap<String, ResolveNode>();
    
    public static void syncRelationships(Collection<String> pCompSysIds) throws Exception
    {
        MigrateSocialRelationships msr = new MigrateSocialRelationships();
        msr.sync(pCompSysIds);
    }
    
    private ResolveEdge fixRunbookRelationship(GraphRelationshipDTO relationship)
    {
        ResolveEdge edge = null;
        
        if(relationship.getGraphRelationType().equals(NonNeo4jRelationType.FOLLOWER.name()) && 
           relationship.getTargetType().equals(SocialRelationshipTypes.RUNBOOK))
        {
            ResolveNodeVO destNodeVO = null;
            
            try
            {
                destNodeVO = GraphUtil.findNode(null, null, relationship.getTargetName(), NodeType.DOCUMENT, "system");
            }
            catch (Exception e)
            {
                ;
            }
            
            if(destNodeVO != null)
            {
                relationship.setTargetSysId(destNodeVO.getUCompSysId());
                
                try
                {
                    edge = ServiceGraph.createEdge(relationship.getSourceSysId(), relationship.getTargetSysId(), null, "system", relationship.getGraphRelationType(), nodesInRelationCache);
                }
                catch (Exception e)
                {
                    ;
                }
            }
        }
        
        return edge;
    }
    
    private void sync(Collection<String> pCompSysIds) throws Exception
    {
        //tags - this will take care of Documents, Actiontasks
        //this will be updated in their appropriate relationship tables
        migrateTagsRelationships();

        //list of all the relationships to be created
        Set<GraphRelationshipDTO> relationships = new HashSet<GraphRelationshipDTO>();
        
        ////////////////////          USER Followers Only ///////////////////////////
        //Users following Document, AT, NS, RSS, WS, Forum, Process, Team, User 
        relationships.addAll(migrationUserRelationships());
        
        //////////////////////////////////////////////////////////////////////////////////        
        //process, team
        relationships.addAll(migrateProcessRelationships()); //except for the Process-User relationship
        relationships.addAll(migrateTeamRelationships()); //except for Team-User relationship
        
        if(relationships.size() > 0)
        {
//            ObjectMapper objectMapper = new ObjectMapper();
//            objectMapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
//
//            //FOR TESTING
//            try
//            {
//                Log.log.debug(objectMapper.writer().writeValueAsString(relationships));
//            }
//            catch (Exception e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
            Log.log.debug("Total Uniqueue Relationships: " + relationships.size());
            long total = 0;
            long failed = 0;
            
            List<Collection<ResolveEdge>> resolveEdgeBatches = new ArrayList<Collection<ResolveEdge>>();
            Collection<ResolveEdge> currentBatch = null;
            
            for(GraphRelationshipDTO relationship : relationships)
            {
                ResolveEdge edge = null;
                
                try
                {
                    edge = ServiceGraph.createEdge(relationship.getSourceSysId(), relationship.getTargetSysId(), null, "system", relationship.getGraphRelationType(), nodesInRelationCache);
                }
                catch(Exception e)
                {
                    //Fix runbook relationship if broken
                    
                    edge = fixRunbookRelationship(relationship);
                    
                    if(edge == null)
                    {
                        Log.log.warn("Failed to migrate relationship [" + relationship + "] due to " + e.getMessage());
                        failed++;
                    }
                }
                
                if(currentBatch != null && total % HibernateUtil.getJdbcBatchSize() == 0)
                {
                    currentBatch = null;
                }
                
                if(currentBatch == null)
                {
                    resolveEdgeBatches.add(new ArrayList<ResolveEdge>());
                    currentBatch = resolveEdgeBatches.get(resolveEdgeBatches.size() - 1);
                }
                
                if(edge != null)
                {
                    currentBatch.add(edge);
                    total++;
                }
            }
            
            Log.log.warn("Total Failed Unique Relationships count is : " + failed);
            
            //Create task for creating relationships
            Collection<Callable<Object>> createRelationshipTasks = new ArrayList<Callable<Object>>();
            
            for(final Collection<ResolveEdge> resolveEdgeBatch : resolveEdgeBatches)
            {
                //Log.log.trace(relationship);
                
                if(!resolveEdgeBatch.isEmpty())
                {
                    createRelationshipTasks.add(new Callable<Object>()
                        {
                            public Object call() throws Exception
                            {
                                int count = GraphUtil.persistEdges(resolveEdgeBatch, "admin");
                                return new Integer(count);
                            }
                        });
                }
            }//end of for loop
            
            total = 0;
            List<Future<Object>> futures = service.invokeAll(createRelationshipTasks);
            
            if(futures != null && !futures.isEmpty())
            {
                for(Future<Object> future : futures)
                {
                    if(future.isDone())
                    {
                        try
                        {
                            total += ((Integer)future.get()).intValue();
                        }
                        catch (ExecutionException e)
                        {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                }
            }
            
            Log.log.debug("Total Unique Relationships Processed: " + total);
        }        
    }
    
    private List<GraphRelationshipDTO> executeTasks(Collection<Callable<List<GraphRelationshipDTO>>> tasks) throws Exception
    {
        List<GraphRelationshipDTO> grapRelations = new ArrayList<GraphRelationshipDTO>();
        
        List<Future<List<GraphRelationshipDTO>>> futures = service.invokeAll(tasks);
        
        if(futures != null && !futures.isEmpty())
        {
            for(Future<List<GraphRelationshipDTO>> future : futures)
            {
                if(future.isDone())
                {
                    try
                    {
                        grapRelations.addAll(future.get());
                    }
                    catch (ExecutionException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }
        }
        
        return grapRelations;
    }
    
    //tags - this will take care of Documents, Actiontasks
    //this will be updated in their appropriate relationship tables
    private void migrateTagsRelationships()
    {
        Set<Node> neo4jNodes = null;
        List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
        
        try
        {
            neo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.ResolveTag);
            
            Log.log.debug("Total # of ResolveTags : " + neo4jNodes.size());
            
            //Create task for exporting tag graphs
            Collection<Callable<List<GraphRelationshipDTO>>> exportTagGraphTasks = new ArrayList<Callable<List<GraphRelationshipDTO>>>();
            
            for(final Node node : neo4jNodes)
            {
                exportTagGraphTasks.add(new Callable<List<GraphRelationshipDTO>>()
                    {
                        public List<GraphRelationshipDTO> call() throws Exception
                        {
                            Log.log.trace(Thread.currentThread().getName() + " : Exporting Graph for Tag:" + 
                                          node.getProperty(GraphDBManager.SYS_ID) + ", " + 
                                          node.getProperty(GraphDBManager.DISPLAYNAME));
                            return new ExportTagGraph((String)(node.getProperty(GraphDBManager.SYS_ID))).export();
                        }
                    });
                /*
                try
                {
                    Log.log.debug("Processed " + count++ + " Tags");                
                    String sysId = (String) node.getProperty(GraphDBManager.SYS_ID);
                    String name = (String) node.getProperty(GraphDBManager.DISPLAYNAME);
                    
                    relationships.addAll(new ExportTagGraph(sysId).export());
                }
                catch (Exception e)
                {
                    Log.log.error("Error in getting tag info for " + node, e);
                }*/
            }
            
            relationships = executeTasks(exportTagGraphTasks);
        }
        catch (Exception e)
        {
            Log.log.error("Error in Tags relationship migration", e);
        }
        
        Log.log.debug("Total # of ResolveTag Relationships : " + relationships.size());
        
        //consolidate the tags with entities - for each AT and Document, prepare a Set of tags
        if(relationships.size() > 0)
        {
           Map<String, Set<String>> documentTags = new HashMap<String, Set<String>>();
           Map<String, Set<String>> atTags = new HashMap<String, Set<String>>();
                      
            for(GraphRelationshipDTO dto : relationships)
            {
                String targetName = dto.getTargetName();
                String tagName = dto.getSourceName();
                
                Log.log.trace(dto);
                if(SocialRelationshipTypes.DOCUMENT == dto.getTargetType())
                {
                    Set<String> tags = null;
                    if(documentTags.containsKey(targetName))
                    {
                        tags = documentTags.get(targetName);
                    }
                    else
                    {
                        tags = new HashSet<String>();
                    }
                    
                    //prepare the set of tags
                    tags.add(tagName);
                    
                    //add it back to the map
                    documentTags.put(targetName, tags);
                    
                }
                else if(SocialRelationshipTypes.ACTIONTASK == dto.getTargetType())
                {
                    Set<String> tags = null;
                    if(atTags.containsKey(targetName))
                    {
                        tags = atTags.get(targetName);
                    }
                    else
                    {
                        tags = new HashSet<String>();
                    }
                    
                    //prepare the set of tags
                    tags.add(tagName);
                    
                    //add it back to the map
                    atTags.put(targetName, tags);
                }
            }//end of for loop
            
            //update the SQL tables
            updateDocumentAndTagRelationship(documentTags);
            updateActiontaskAndTagRelationship(atTags);
            
        }//end of if 
    }
    
    
    private List<GraphRelationshipDTO> migrateProcessRelationships()
    {
        Set<Node> processesNeo4jNodes = null;
        List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
        try
        {
            processesNeo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.PROCESSES);
            
            Log.log.debug("Total # of Processes : " + processesNeo4jNodes.size());
            
            //Create task for exporting process graphs
            Collection<Callable<List<GraphRelationshipDTO>>> exportProcessGraphTasks = new ArrayList<Callable<List<GraphRelationshipDTO>>>();
            
            for(final Node node : processesNeo4jNodes)
            {
                exportProcessGraphTasks.add(new Callable<List<GraphRelationshipDTO>>()
                    {
                        public List<GraphRelationshipDTO> call() throws Exception
                        {
                            Log.log.trace(Thread.currentThread().getName() + " : Exporting Graph for Process:" + 
                                          node.getProperty(GraphDBManager.SYS_ID) + ", " + 
                                          node.getProperty(GraphDBManager.DISPLAYNAME));
                            return new ExportProcessGraph((String)(node.getProperty(GraphDBManager.SYS_ID))).export();
                        }
                    });
                /*
                Log.log.debug("Processed " + count++ + " Processes");                
                String sysId = (String) node.getProperty(GraphDBManager.SYS_ID);
                String name = (String) node.getProperty(GraphDBManager.DISPLAYNAME);
                
                relationships.addAll(new ExportProcessGraph(sysId).export());*/
            }
            
            relationships = executeTasks(exportProcessGraphTasks);
        }
        catch (Exception e)
        {
            Log.log.error("Error in Process relationship migration", e);
        }
        
        Log.log.debug("Total # of Process Relationships : " + relationships.size());
        
        return relationships;
       
    }
    
    private List<GraphRelationshipDTO> migrateTeamRelationships()
    {
        Set<Node> neo4jNodes = null;
        List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
        try
        {
            neo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.TEAMS);
            
            Log.log.debug("Total # of Teams : " + neo4jNodes.size());
            
            //Create task for exporting process graphs
            Collection<Callable<List<GraphRelationshipDTO>>> exportTeamGraphTasks = new ArrayList<Callable<List<GraphRelationshipDTO>>>();
            
            for(final Node node : neo4jNodes)
            {
                exportTeamGraphTasks.add(new Callable<List<GraphRelationshipDTO>>()
                {
                    public List<GraphRelationshipDTO> call() throws Exception
                    {
                        Log.log.trace(Thread.currentThread().getName() + " : Exporting Graph for Team:" + 
                                      node.getProperty(GraphDBManager.SYS_ID) + ", " + 
                                      node.getProperty(GraphDBManager.DISPLAYNAME));
                        return new ExportTeamGraph((String)(node.getProperty(GraphDBManager.SYS_ID)), false).export();
                    }
                });
            }
            
            relationships = executeTasks(exportTeamGraphTasks);
        }
        catch (Exception e)
        {
            Log.log.error("Error in Team relationship migration", e);
        }
        
        Log.log.debug("Total # of Team Relationships : " + relationships.size());
        
        return relationships;
    }
    
    
//    private List<GraphRelationshipDTO> migrateForumRelationships()
//    {
//        Set<Node> neo4jNodes = null;
//        List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
//        try
//        {
//            neo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.FORUMS);
//            
//            Log.log.debug("Total # of recs : " + neo4jNodes.size());
//            int count = 1;
//            for(Node node : neo4jNodes)
//            {
//                Log.log.debug("Processed " + count++ + " Forums");                
//                String sysId = (String) node.getProperty(GraphDBManager.SYS_ID);
//                String name = (String) node.getProperty(GraphDBManager.DISPLAYNAME);
//                
//                relationships.addAll(new ExportForumGraph(sysId).export());
//            }
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error in Forum relationship migration", e);
//        }
//        
//        return relationships;        
//    }
    
    
    private List<GraphRelationshipDTO> migrationUserRelationships()
    {
        Set<Node> neo4jNodes = null;
        List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
        try
        {
            neo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.USERS);
            
            Log.log.debug("Total # of Users : " + neo4jNodes.size());
            
            //Create task for exporting user graphs
            Collection<Callable<List<GraphRelationshipDTO>>> exportUserGraphTasks = new ArrayList<Callable<List<GraphRelationshipDTO>>>();
            
            for(final Node node : neo4jNodes)
            {
                exportUserGraphTasks.add(new Callable<List<GraphRelationshipDTO>>()
                    {
                        public List<GraphRelationshipDTO> call() throws Exception
                        {
                                        Log.log.trace(Thread.currentThread().getName() + " : Exporting Graph for User:" + 
                                                      node.getProperty(GraphDBManager.SYS_ID) + ", " + 
                                                      node.getProperty(GraphDBManager.DISPLAYNAME));
                                        return new ExportUserGraph((String)(node.getProperty(GraphDBManager.SYS_ID))).export();
                        }
                    });
            }
            
            relationships = executeTasks(exportUserGraphTasks);
        }
        catch (Exception e)
        {
            Log.log.error("Error in User relationship migration", e);
        }
        
        Log.log.debug("Total # of User Relationships : " + relationships.size());
        
        return relationships;
    }
    
    private void updateDocumentAndTagRelationship(Map<String, Set<String>> documentTags)
    {
        Iterator<String> docs = documentTags.keySet().iterator();
        while(docs.hasNext())
        {
            String docname = docs.next();
            Set<String> tags = documentTags.get(docname);
            
            addTagsToDocument(docname, tags);
        }//end of while loop
    }
    
    private void updateActiontaskAndTagRelationship(Map<String, Set<String>> atTags)
    {
        Iterator<String> docs = atTags.keySet().iterator();
        while(docs.hasNext())
        {
            String atname = docs.next();
            Set<String> tags = atTags.get(atname);
            
            addTagsToActiontask(atname, tags);
        }//end of while loop
    }
    
    @SuppressWarnings("rawtypes")
    private void addTagsToDocument(String docname, Set<String> tags)
    {
        //String sql = "from WikiDocument where Lower(UFullname) = '" + docname.toLowerCase() + "'";
        String sql = "from WikiDocument where Lower(UFullname) = :UFullname";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UFullname", docname.toLowerCase());
        
        try
        {
            List recs = ServiceHibernate.executeHQLSelect(sql, queryParams);
            if (recs != null && recs.size() == 1)
            {

              HibernateProxy.setCurrentUser("system");
                HibernateProxy.execute(() -> {
	            	WikiDocument doc = (WikiDocument) recs.get(0);
	
	                doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(doc.getSys_id());
	                
	                //delete the existing tags 
	                Collection<WikidocResolveTagRel> tagRels = doc.getWikidocResolveTagRels();
	                if (tagRels != null && tagRels.size() > 0)
	                {
	                    for (WikidocResolveTagRel rel : tagRels)
	                    {
	                        HibernateUtil.getDAOFactory().getWikidocResolveTagRelDAO().delete(rel);
	                    }
	                    HibernateUtil.getCurrentSession().flush();
	                }
	
	                if (tags.size() > 0)
	                {
	                    for (String tag : tags)
	                    {
	                        ResolveTagVO tagModel = ServiceTag.getTag(null, tag, "system");
	                        if (tagModel != null)
	                        {
	                            WikidocResolveTagRel rel = new WikidocResolveTagRel();
	                            rel.setWikidoc(doc);
	                            rel.setTag(new ResolveTag(tagModel.getSys_id()));
	
	                            HibernateUtil.getDAOFactory().getWikidocResolveTagRelDAO().persist(rel);
	                        }
	                    }//end of for loop
	                }//end of if

                });
            }

        }
        catch (Throwable t)
        {
            Log.log.error("Error adding tags to document:" + docname, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

    }
    
    @SuppressWarnings("rawtypes")
    private void addTagsToActiontask(String atname, Set<String> tags)
    {
        //String sql = "from ResolveActionTask where Lower(UFullName) = '" + atname.toLowerCase() + "'";
        String sql = "from ResolveActionTask where Lower(UFullName) = :UFullName";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("UFullName", atname.toLowerCase());
        
        try
        {
            List recs = ServiceHibernate.executeHQLSelect(sql, queryParams);
            
            if (recs != null && recs.size() == 1)
            {

              HibernateProxy.setCurrentUser("system");
                HibernateProxy.execute(() -> {
                	ResolveActionTask at = (ResolveActionTask) recs.get(0);

	                at = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(at.getSys_id());
	
	                //delete the old tags
	                Collection<ResolveActionTaskResolveTagRel> rels = at.getAtTagRels();
	                if (rels != null && rels.size() > 0)
	                {
	                    for (ResolveActionTaskResolveTagRel rel : rels)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveActionTaskResolveTagRelDAO().delete(rel);
	                    }
	                    //making sure that its deleted
	                    HibernateUtil.getCurrentSession().flush();
	                }
	
	                //add the new tags
	                if (tags.size() > 0)
	                {
	                    for (String tag : tags)
	                    {
	                        ResolveTagVO tagModel = ServiceTag.getTag(null, tag, "system");
	                        if (tagModel != null)
	                        {
	                            ResolveActionTaskResolveTagRel rel = new ResolveActionTaskResolveTagRel();
	                            rel.setTask(at);
	                            rel.setTag(new ResolveTag(tagModel.getSys_id()));
	
	                            HibernateUtil.getDAOFactory().getResolveActionTaskResolveTagRelDAO().persist(rel);
	                        }
	                    }//end of for loop
	                }
                });           
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error adding tags to document:" + atname, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

    }
}
