/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.immport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.resolve.services.ServiceGraph;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.util.Log;

public class ImportProcessGraph extends ImportComponentGraph
{
    public ImportProcessGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        Log.log.debug("Importing Process Relationship:" + rel);
        ResolveNodeVO processNode = ServiceGraph.findNode(null, null, rel.getSourceName(), rel.getSourceType(), username);
        if (processNode != null)
        {
            switch (rel.getTargetType())
            {
                case USER:
                    //*** Note: User will always follow rest of the Entities
                    ResolveNodeVO userNode = ServiceGraph.findNode(null, null, rel.getTargetName(), rel.getTargetType(), username);
                    if (userNode != null)
                    {
                        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
                        followComponents.add(processNode);

                        ServiceGraph.follow(userNode.getSys_id(), null, null, null, followComponents, followerEdgeProps, username);
                    }
                    else
                    {
                        Log.log.debug("User not available for Relationship:" + rel);
                    }
                    break;

                case ACTIONTASK:
                case DOCUMENT:
                case NAMESPACE:
                case FORUM:
                case RSS:
                case TEAM:
                    ResolveNodeVO nodeDst = ServiceGraph.findNode(null, null, rel.getTargetName(), rel.getTargetType(), username);
                    if (nodeDst != null)
                    {
                        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
                        followComponents.add(processNode);

                        ServiceGraph.follow(nodeDst.getSys_id(), null, null, null, followComponents, followerEdgeProps, username);
                    }
                    else
                    {
                        Log.log.debug("Target Node not available for Relationship:" + rel);
                    }
                    break;

                default:
                    break;
            }
        }
        else
        {
            Log.log.debug("Process Node Does not exist for " + rel);
        }
    }

}
