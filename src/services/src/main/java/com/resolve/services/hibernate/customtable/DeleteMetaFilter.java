/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import com.resolve.persistence.model.MetaFilter;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class DeleteMetaFilter
{
    private String username = null;
    private boolean isAdminUser = false;
    private StringBuffer error = new StringBuffer();
    
    public DeleteMetaFilter(String username)
    {
        this.username = username;
        if(StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Username is mandatory to delete the filters");
        }
        
        isAdminUser = ServiceHibernate.isAdminUser(username);
    }

    public String deleteFilters(String sysIds)
    {
        if (StringUtils.isNotBlank(sysIds))
        {
            String[] arr = sysIds.split(",");
            for(String sysId : arr)
            {
                deleteFilter(sysId);
            }
        }

        return error.toString();
    }

    private void deleteFilter(String sysId)
    {
        MetaFilter metaFilter = null;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	metaFilter = (MetaFilter) HibernateProxy.execute(() -> {

	                MetaFilter metaFilterResult = HibernateUtil.getDAOFactory().getMetaFilterDAO().findById(sysId);
	                if(metaFilterResult != null)
	                {
	                    if(isAdminUser)
	                    {
	                        HibernateUtil.getDAOFactory().getMetaFilterDAO().delete(metaFilterResult);
	                    }
	                    else
	                    {
	                        String filterUser = metaFilterResult.getUUserId(); 
	                        if(StringUtils.isNotEmpty(filterUser) && filterUser.trim().equalsIgnoreCase(username.trim()))
	                        {
	                            HibernateUtil.getDAOFactory().getMetaFilterDAO().delete(metaFilterResult);
	                        }
	                        else
	                        {
	                            throw new Exception("User has no rights to delte filter " + metaFilterResult.getUName());
	                        }
	                    }
	                }

	                return metaFilterResult;
            	});
            }
            catch (Throwable t)
            {
                String errorStr = "Error for the filter :" + metaFilter.getUName() + ". " + t.getMessage();
                error.append(errorStr);
                
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

}

