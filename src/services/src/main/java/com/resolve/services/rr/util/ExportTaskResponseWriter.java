package com.resolve.services.rr.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.services.util.ServletUtils;
import com.resolve.util.Log;

public class ExportTaskResponseWriter
{
    private HttpServletResponse response;
    private OutputStream out;
    private OutputStreamWriter ow;
    
    public ExportTaskResponseWriter(HttpServletRequest request,HttpServletResponse response,String username) throws IOException {
        this.response = response;
        this.out = this.response.getOutputStream();
        this.ow = new OutputStreamWriter(this.out);
        ServletUtils.sendHeaders(username + "_rr.csv","text/plain",0, request, response);
    }
    
    public void writeLine(String line) throws IOException {
        ow.write(line);
        ow.write(System.getProperty("line.separator"));
    }
    public void cleanUp() {
        if(this.ow!=null) 
            try
            {
                this.ow.close();
            }
            catch (IOException e1)
            {
                Log.log.error(e1.getMessage(), e1);
            }
        if(this.out!=null) 
            try
            {
                this.out.close();
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
    }
}
