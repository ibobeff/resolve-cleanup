package com.resolve.services.task.util;

import java.util.HashMap;
import java.util.Set;

public abstract class AbstractParserGen
{
    String parseStart;
    String parseEnd;
    String parseType;
    String regex;
    final HashMap<String, Boolean> options;
    final HashMap<String,Integer> variables;
    final Set<String> wsdataVariables;
    final Set<String> flowVariables;
    final Set<String> outputVariables;
    public AbstractParserGen(String regex,String parseStart,String parseEnd, String parseType,HashMap<String,Integer> variables, Set<String> flowVariables,Set<String> wsdataVariables,Set<String> outputVariables, HashMap<String, Boolean> options)
    {
        this.parseStart = parseStart;
        this.parseEnd = parseEnd;
        this.parseType = parseType;
        this.options = options;
        this.variables = variables;
        this.wsdataVariables = wsdataVariables;
        this.flowVariables = flowVariables;
        this.outputVariables = outputVariables;
        this.regex = regex;
    }
    
    abstract String generateCode();
    abstract String getParserTemplate();
}
