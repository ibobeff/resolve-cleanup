/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import com.resolve.util.StringUtils;

public class Geometry
{
    private int x; //x coordinate
    private int y; //x coordinate
    private int width;
    private int height;
    private int relative; //always 1?
    private String as="geometry"; //always "geometry"?

    private int sourcePointX;
    private int sourcePointY;
    private int targetPointX;
    private int targetPointY;
    private int arrayPointX;
    private int arrayPointY;
    
    public Geometry()
    {
        
    }
    
    public Geometry(int x, int y, int width, int height, int relative, String as)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.relative = relative;
        this.as = as;
    }

    public Geometry(int x, int y, int width, int height, int relative, String as, int sourcePointX, int sourcePointY, int targetPointX, int targetPointY)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.relative = relative;
        this.as = as;
        this.sourcePointX = sourcePointX;
        this.sourcePointY = sourcePointY;
        this.targetPointX = targetPointX;
        this.targetPointY = targetPointY;
    }

    public Geometry(int x, int y, int width, int height, int relative, String as, int sourcePointX, int sourcePointY, int targetPointX, int targetPointY, int arrayPointX, int arrayPointY)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.relative = relative;
        this.as = as;
        this.sourcePointX = sourcePointX;
        this.sourcePointY = sourcePointY;
        this.targetPointX = targetPointX;
        this.targetPointY = targetPointY;
        this.arrayPointX = arrayPointX;
        this.arrayPointY = arrayPointY;
    }

    public int getX()
    {
        return x;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return y;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public int getRelative()
    {
        return relative;
    }

    public void setRelative(int relative)
    {
        this.relative = relative;
    }

    public String getAs()
    {
        return as;
    }

    public void setAs(String as)
    {
        this.as = as;
    }


    public int getSourcePointX()
    {
        return sourcePointX;
    }

    public void setSourcePointX(int sourcePointX)
    {
        this.sourcePointX = sourcePointX;
    }

    public int getSourcePointY()
    {
        return sourcePointY;
    }

    public void setSourcePointY(int sourcePointY)
    {
        this.sourcePointY = sourcePointY;
    }

    public int getTargetPointX()
    {
        return targetPointX;
    }

    public void setTargetPointX(int targetPointX)
    {
        this.targetPointX = targetPointX;
    }

    public int getTargetPointY()
    {
        return targetPointY;
    }

    public void setTargetPointY(int targetPointY)
    {
        this.targetPointY = targetPointY;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("        <mxGeometry");
        if(x > 0)
        {
            sb.append(" x=\"").append(x).append("\"");
        }
        if(y > 0)
        {
            sb.append(" y=\"").append(y).append("\"");
        }
        if(width > 0)
        {
            sb.append(" width=\"").append(width).append("\"");
        }
        if(height > 0)
        {
            sb.append(" height=\"").append(height).append("\"");
        }
        if(relative > 0)
        {
            sb.append(" relative=\"").append(relative).append("\"");
        }
        if(StringUtils.isNotBlank(as))
        {
            sb.append(" as=\"").append(as).append("\"");
        }
        if(sourcePointX > 0)
        {
            sb.append(">\n");
            //
            sb.append("         <mxPoint x=\"" + sourcePointX + "\" y=\"" + sourcePointY + "\" as=\"sourcePoint\"/>\n");
            sb.append("         <mxPoint x=\"" + targetPointX + "\" y=\"" + targetPointY + "\" as=\"targetPoint\"/>\n");
            sb.append("         <Array as=\"points\">\n");
            if(arrayPointX > 0 || arrayPointY > 0)
            {
                sb.append("           <mxPoint x=\"" + arrayPointX + "\" y=\""+ arrayPointY + "\"/>\n");
            }
            else
            {
                sb.append("           <mxPoint x=\"" + (sourcePointX / 2) + "\" y=\""+ (targetPointY+10) + "\"/>\n");
            }
            sb.append("         </Array>\n");
            sb.append("        </mxGeometry>\n");
        }
        else
        {
            sb.append("/>\n");
        }
        
        return sb.toString();
    }
}
