/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.vo.social.NodeType;


public class ComponentNotification
{
    @Deprecated
    private RSComponent rsComponent;
    
    private NodeType compType;
    private String componentDisplayName;
    private List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>(); 
    

    @Deprecated
    public void setRSComponent(RSComponent rsComponent)
    {
        this.rsComponent = rsComponent;
    }
    
    @Deprecated
    public RSComponent getRSComponent()
    {
        return rsComponent;
    }
    
    public void setNotifyTypes(List<NotificationConfig> notifyTypes)
    {
        this.notifyTypes = notifyTypes;
    }
    
    public List<NotificationConfig> getNotifyTypes()
    {
        return notifyTypes;
    }
    
    public void addNotifyType(NotificationConfig config)
    {
        this.notifyTypes.add(config);
    }

    public NodeType getCompType()
    {
        return compType;
    }

    public void setCompType(NodeType compType)
    {
        this.compType = compType;
    }

    public String getComponentDisplayName()
    {
        return componentDisplayName;
    }

    public void setComponentDisplayName(String componentDisplayName)
    {
        this.componentDisplayName = componentDisplayName;
    }
    
    
    
}
