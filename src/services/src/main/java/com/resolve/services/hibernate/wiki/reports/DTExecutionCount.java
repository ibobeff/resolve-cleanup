/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;


public class DTExecutionCount
{
    
    private String fullname;
    private Long executioncount;
    private String author;
    
    public DTExecutionCount(String fullname, Long executioncount, String author)
    {
        this.fullname = fullname;
        this.executioncount = executioncount;
        this.author = author;
    }
    
    public String getFullname()
    {
        return fullname;
    }
    
    public String getAuthor()
    {
        return author;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    public long getExecutioncount()
    {
        return executioncount;
    }

    public void setExecutioncount(Long executioncount)
    {
        this.executioncount = executioncount;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + (this.getFullname() == null ? 0 : this.getFullname().hashCode());
        return hash;
    }
    
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof DTExecutionCount)
        {
            DTExecutionCount otherTask = (DTExecutionCount) otherObj;
            
            // if sys_ids are not equal, they are diff
            if (otherTask.getFullname().equals(this.getFullname()))
            {
                isEquals = true;
            }
        }
        return isEquals;

    }
    

}
