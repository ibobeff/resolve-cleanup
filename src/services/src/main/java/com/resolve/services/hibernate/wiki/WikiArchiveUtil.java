/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.springframework.security.access.AccessDeniedException;

import com.google.common.collect.ImmutableMap;
import com.resolve.persistence.model.WikiArchive;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.WikiArchiveVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class WikiArchiveUtil
{
    private final static long noOfAutoRevisionsAllowed = 5;
    
    @SuppressWarnings("unchecked")
	public static List<WikiArchiveVO> getAllWikiRevisionsFor(String fullDocName, String username) throws Exception
    {
        List<WikiArchiveVO> result = new ArrayList<WikiArchiveVO>();

        if (StringUtils.isNotBlank(fullDocName))
        {
            List<? extends Object> list = null;
            String sql = "select  wa  from WikiArchive as wa, WikiDocument as wd " 
                            + " where wa.UTableId = wd.sys_id " 
                            + " and lower(wd.UFullname) = :wikiFullName "
            //                        + " and wa.UTableColumn = '" + WikiDocument.COL_CONTENT + "' "
            //                        + " and wa.UUserArchive = true " 
                            + " order by wa.sysCreatedOn DESC";
            try
            {
//                list = ServiceHibernate.executeHQLSelect(sql);
                
              HibernateProxy.setCurrentUser(username);
                list = (List<? extends Object>) HibernateProxy.execute(() -> {
                	Query query = HibernateUtil.createQuery(sql);
                    query.setParameter("wikiFullName", fullDocName.toLowerCase());
                    return  query.list();
                });
                
                if (list != null && list.size() > 0)
                {
                    for (Object o : list)
                    {
                        WikiArchive wa = (WikiArchive) o;
                        result.add(wa.doGetVO());
                    }
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e);
            }
        }
        return result;
    }
    
    public static WikiArchive getSpecificWikiRevisionModelFor(String docSysId, String fullDocName, Integer revision, String username, String tableColumn) throws Exception
    {
        WikiArchive result = null;
        
        WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, fullDocName, RightTypeEnum.view, "system");
        if(doc != null)
        {
         
            List<? extends Object> list = null;
            /*String sql = "select  wa  from WikiArchive as wa, WikiDocument as wd " 
                            + " where wa.UTableId = wd.sys_id " 
                            + " and wa.UTableId = '" + doc.getSys_id() + "' "
                            + " and wa.UVersion = " + revision + " "
                            + " order by wa.sysCreatedOn DESC";*/
            String sql = "select  wa  from WikiArchive as wa, WikiDocument as wd " 
                            + " where wa.UTableId = wd.sys_id " 
                            + " and wa.UTableId = :docSysId "
                            + " and wa.UVersion = :revision "
                            + " and wa.UTableColumn = :tableColumn "
                            + " order by wa.sysCreatedOn DESC";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("docSysId", doc.getSys_id());
            queryParams.put("revision", revision);
            queryParams.put("tableColumn", tableColumn);
            
            try
            {
                list = ServiceHibernate.executeHQLSelect(sql, queryParams);
                if (list != null && list.size() == 1)
                {
                    result = (WikiArchive)list.get(0);
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e);
            }
        }
        
        return result;
    }
    
	public static List<WikiArchiveVO> getSpecificWikiRevisionsFor(String docSysId, String fullDocName, Integer revision,
			String username) throws Exception {
		List<WikiArchiveVO> result = new ArrayList<WikiArchiveVO>();

		WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, fullDocName, RightTypeEnum.view, "system");
		if (doc != null) {

			List<? extends Object> list = null;
			String sql = "select  wa  from WikiArchive as wa, WikiDocument as wd " + " where wa.UTableId = wd.sys_id "
					+ " and wa.UTableId = :docSysId " + " and wa.UVersion = :revision "
					+ " order by wa.sysCreatedOn DESC";

			Map<String, Object> queryParams = new HashMap<String, Object>();

			queryParams.put("docSysId", doc.getSys_id());
			queryParams.put("revision", revision);

			try {
				list = ServiceHibernate.executeHQLSelect(sql, queryParams);
				if (list != null && list.size() > 0) {
					for (Object o : list) {
						WikiArchive wa = (WikiArchive) o;
						result.add(wa.doGetVO());
					}
				}
			} catch (Throwable e) {
				Log.log.error(e.getMessage(), e);
				throw new Exception(e);
			}
		}

		return result;
	}
	
    public static void rollbackWiki(String docSysId, String fullDocName, Integer rollbackToRevision, String username) throws Exception
    {
        WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, fullDocName, RightTypeEnum.edit, "system");
        if(doc != null)
        {
            List<WikiArchiveVO> revision = getSpecificWikiRevisionsFor(docSysId, fullDocName, rollbackToRevision, username);
            if(revision != null && revision.size() > 0)
            {
                for(WikiArchiveVO vo : revision)
                {
                    String tableColumn = vo.getUTableColumn();
                    if(WikiDocument.COL_CONTENT.equalsIgnoreCase(tableColumn))
                    {
                        doc.setUContent(vo.getUPatch());
                    }
                    else if(WikiDocument.COL_MODEL_PROCESS.equalsIgnoreCase(tableColumn))
                    {
                        doc.setUModelProcess(vo.getUPatch());
                    }
                    else if(WikiDocument.COL_MODEL_EXCEPTION.equalsIgnoreCase(tableColumn))
                    {
                        doc.setUModelException(vo.getUPatch());
                    }
                    else if(WikiDocument.COL_DECISION_TREE.equalsIgnoreCase(tableColumn))
                    {
                        doc.setUDecisionTree(vo.getUPatch());
                    }
                }
                
                //Save the document now
                SaveHelper saveDoc = new SaveHelper(doc, username);
                saveDoc.setComment("Rollback from Revision #" + rollbackToRevision);
                saveDoc.setRollback(true);
                saveDoc.save();
            }
        }
    }
    
    public static void resetWiki(String docSysId, String fullDocName, String username) throws Exception
    {
        WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, fullDocName, RightTypeEnum.edit, "system");
        if(doc != null)
        {
            //delete all the revisions for this document
            deleteWikiArchiveVersions(doc.getSys_id(), null, true);    
            
            //Save the document now
            SaveHelper saveDoc = new SaveHelper(doc, username);
            saveDoc.setComment("Reset revisions");
            saveDoc.setReset(true);
            saveDoc.save();
            
        }
    }
    
    /**
     * 
     * @param wikidocId
     */
    public void cleanupWikiArchive(String wikidocId) throws Exception
    {
        int revAllowed = getNoOfAutoRevisionsAllowed();
        List<Integer> listOfAutoRevisionVersions = getListOfAutoRevisionVersions(wikidocId);
        
        if(listOfAutoRevisionVersions.size() > revAllowed)
        {
            List<Integer> versionToDelete = listOfAutoRevisionVersions.subList(revAllowed, listOfAutoRevisionVersions.size());
            String queryVersion = SQLUtils.prepareQueryInteger(versionToDelete);
            
//            System.out.println(revAllowed);
//            System.out.println(StringUtils.prepareQueryInteger(listOfAutoRevisionVersions));
//            System.out.println(queryVersion);
            
            deleteWikiArchiveVersions(wikidocId, queryVersion, false);
        }
        
    }//cleanupWikiArchive

    private int getNoOfAutoRevisionsAllowed()
    {
        long revAllowed = PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_WIKI_ARCHIVED_ALLOWED);

        //this means that we cannot typecast to int
        if (revAllowed == -1 || revAllowed < Integer.MIN_VALUE || revAllowed > Integer.MAX_VALUE)
        {
            revAllowed = noOfAutoRevisionsAllowed;
        }

        return (int)revAllowed;
    }//getNoOfAutoRevisionsAllowed
    
    @SuppressWarnings("unchecked")
    private List<Integer> getListOfAutoRevisionVersions(String wikidocId)
    {
        List<Integer> listOfAutoRevisionVersions = new ArrayList<Integer>();
        if(StringUtils.isNotBlank(wikidocId))
        {
            
            try
            {
            	String sql = "SELECT distinct UVersion FROM WikiArchive where UTableId = '" + wikidocId.trim() + "' and (UUserArchive is false OR UUserArchive is null) ORDER BY UVersion desc";
                               
                listOfAutoRevisionVersions = (List<Integer>) HibernateProxy.execute(() -> {
                	Query q = HibernateUtil.createQuery(sql);
                	return q.list();
                });

            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        
        }//end of if
        return listOfAutoRevisionVersions;
    }//getListOfAutoRevisionVersions
    
    private static synchronized void deleteWikiArchiveVersions(String wikidocId, String queryVersion, boolean reset) throws Exception
    {
    	final Collection<Integer> queryVerColl = new ArrayList<Integer>();
        String optWhereClause = null;
        if(!reset)
        {
            for (String qv : queryVersion.split(","))
            {
                // NumberFormatException caused by containing whitespace in the value fixed (http://jira/browse/RBA-12841)
                queryVerColl.add(Integer.parseInt(qv.trim()));
            }
            
            //whereClause = whereClause + " and (UUserArchive is false OR UUserArchive is null) and UVersion in (" + queryVersion + ")";
            optWhereClause = " and (UUserArchive is false OR UUserArchive is null) ";
        }
        
		try {
			String whereClause = optWhereClause;
			HibernateProxy.execute(() -> {
				HibernateUtil.deleteQuery("WikiArchive", "UTableId", wikidocId.trim(), whereClause, "UVersion",
						queryVerColl);
			});
		} catch (Throwable e) {
			Log.log.error(
					"error while deleting the WikiArchive with : from WikiArchive where UTableId = " + wikidocId.trim()
							+ (StringUtils.isNotEmpty(optWhereClause)
									? "" + optWhereClause + " and UVersion IN (" + queryVersion + ")"
									: ""),
					e);
			throw new Exception(e);
		}
    }//deleteWikiArchiveVersions

    public static void updateWikiArchives(Collection<WikiDocumentVO> list, String oldFullName, String newFullName, String userName) {
        
        for(WikiDocumentVO wiki:list) {
            updateWikiArchive(wiki, oldFullName, newFullName, userName);
        }
    }
    
    public static void updateWikiArchive(WikiDocumentVO wiki, String oldFullName, String newFullName, String userName) {
        
        QueryDTO query = new QueryDTO();
        List <? extends Object> data = null;
        
        if(StringUtils.isBlank(newFullName))
            return;
        
        String oldName = oldFullName.split("#")[0];
        
        query.setModelName("WikiArchive");
        query.setWhereClause("UTableId = '" + wiki.getId() + "' AND upper(UPatch) like upper('%" + oldName + "%')");

        try
        {
            data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                WikiArchive temp = (WikiArchive)o;
                boolean changed = changeWikiArchiveModel(oldFullName, newFullName, temp, wiki);
                if(!changed)
                    continue;
                
                try
                {
                  HibernateProxy.setCurrentUser(userName);
                    HibernateProxy.execute(() -> HibernateUtil.getDAOFactory().getWikiArchiveDAO().update(temp));
                }
                catch(Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                                      HibernateUtil.rethrowNestedTransaction(e);
                }
            }
        }
    }
    
    protected static boolean changeWikiArchiveModel(String oldFullName, String newFullName, WikiArchive wikiArchive, WikiDocumentVO wiki)
    {
        WikiArchiveVO vo = wikiArchive.doGetVO();
        String content = vo.getUPatch();
        
        if(StringUtils.isBlank(content))
            return false;

        String newContent = content;

        if(newContent.indexOf(oldFullName) != -1)
            newContent = newContent.replace(oldFullName, newFullName);

        if(newContent.compareTo(content) == 0)
            return false;
        Log.log.debug(newContent);
        wikiArchive.setUPatch(newContent);

        return true;
    }

} // WikiArchiveUtil
