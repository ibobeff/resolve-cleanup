package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveNamespace;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.NamespaceVO;
import com.resolve.services.hibernate.vo.ResolveNamespaceVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ResolveNamespaceUtil {
    private static final String ERROR_GETTING_ACCESS_RIGHTS = "Error in getting access rights for ns: %s";
	public static final String RESOURCE_TYPE = "namespace";
	private static final String NAMESPACE_PARENT = "parentNamespace";
	private static final String NAMESPACE_NAME = "UName";
	private static final String WIKI_NAMESPACE = "namespace";
	private static final String NAMESPACE_DELIMITER = ".";
	private static final String NAMESPACE_DELIMITER_ESCAPED = "\\.";
	private static final String ERROR_INPUT_NULL_OR_EMPTY = "Input parameter is null or empty";
	private static final String ERROR_PARENT_NOT_FOUND = "Parent namespace not found";
	private static final String ERROR_SAVING_NAMESPACE = "Failed to save namespace";

	public static List<ResolveNamespaceVO> getAllRootNamespaces() throws Exception {
		List<ResolveNamespaceVO> namespaces = new ArrayList<>();
		try {
			HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();

	            CriteriaQuery<ResolveNamespace> criteriaQuery = criteriaBuilder.createQuery(ResolveNamespace.class);
	            Root<ResolveNamespace> from = criteriaQuery.from(ResolveNamespace.class);

	            criteriaQuery.select(from).where(from.get(NAMESPACE_PARENT).isNull());

	            List<ResolveNamespace> result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();

				for (ResolveNamespace ns : result) {
					namespaces.add(ns.doGetVO());
				}
			});           
			
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
		return namespaces;
	}

	public static Map<ResolveNamespaceVO, Integer> getAllWikiNamespaces() throws Exception {
		Map<ResolveNamespaceVO, Integer> namespaces = new HashMap<>();
		try {

			HibernateProxy.execute(() -> {
	            CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();

	            CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);
	            Root<WikiDocument> from = criteriaQuery.from(WikiDocument.class);
	            Join<WikiDocument, ResolveNamespace> wikis = from.join(WIKI_NAMESPACE);

	            criteriaQuery.multiselect(from.get(WIKI_NAMESPACE), criteriaBuilder.count(wikis))
						.groupBy(from.get(WIKI_NAMESPACE))
						.orderBy(criteriaBuilder.asc(from.get(WIKI_NAMESPACE).get(NAMESPACE_NAME)));

	            List<Object[]> result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();

				for (Object[] obj : result) {
					ResolveNamespace ns = (ResolveNamespace) obj[0];
					Long count = (Long) obj[1]; 
					namespaces.put(ns.doGetVO(), count.intValue());
				}
				
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
		return namespaces;
	}
	
	public static ResolveNamespaceVO getNamespaceByFullName(String name) throws Exception {
		ResolveNamespaceVO resultVO = null;
		
		if (StringUtils.isBlank(name)) {
			return resultVO;
		}
		
		try {
						
			ResolveNamespace result = (ResolveNamespace) HibernateProxy.execute(() -> {
				return getNamespace(name);
			});

			if (result != null) {
				resultVO = result.doGetVO();
			}
			
			return resultVO;
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}

	public static void deleteNamespaceByName(String name) throws Exception {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException(ERROR_INPUT_NULL_OR_EMPTY);
		}

		try {
			HibernateProxy.execute(() -> {
				ResolveNamespace result = getNamespace(name);

				if (result != null) {
					HibernateUtil.getDAOFactory().getResolveNamespaceDAO().delete(result);
				}

			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}

	public static ResolveNamespaceVO saveNamespace(ResolveNamespaceVO vo, String parentName)
			throws Exception {
		if (vo == null || StringUtils.isBlank(vo.getUName())) {
			throw new IllegalArgumentException(ERROR_INPUT_NULL_OR_EMPTY);
		}

		try {
			
			ResolveNamespaceVO savedVo = (ResolveNamespaceVO) HibernateProxy.execute(() -> {
				ResolveNamespace namespace = null;
				ResolveNamespace parent = null;
				
				if (StringUtils.isNotBlank(parentName)) {
					parent = getNamespace(parentName);

					if (parent == null) {
						throw new IllegalArgumentException(ERROR_PARENT_NOT_FOUND);
					}
					
					namespace = parent.getChildNamespaces().stream()
							.filter(n -> vo.getUName().equalsIgnoreCase(n.getUName())).findFirst().orElse(null);
				}
				
				if (namespace == null) {
					namespace = new ResolveNamespace();
				}
				
				namespace.setUName(vo.getUName());

				AccessRights ar = buildAccessRights(vo, parent);
				namespace.setAccessRights(ar);
				namespace.setParentNamespace(parent);

				ResolveNamespace result = HibernateUtil.getDAOFactory().getResolveNamespaceDAO().insertOrUpdate(namespace);

				// update access rights as it's one-way relationship
				ar = result.getAccessRights();
				ar.setUResourceId(result.getSys_id());
				
				ar = HibernateUtil.getDAOFactory().getAccessRightsDAO().insertOrUpdate(ar);
				result.setAccessRights(ar);
				
				return result.doGetVO();
			});
			return savedVo;
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_NAMESPACE, e);
			throw e;
		}
	}

	public static void migrateWikiDocs() throws Exception {
		Map<String, ResolveNamespace> cache = new HashMap<>();
		try {
			HibernateProxy.execute(() -> {
				 QueryDTO queryDTO = new QueryDTO();
		    	    queryDTO.setHql("select sys_id, UNamespace, accessRights from WikiDocument");

			        @SuppressWarnings("unchecked")
					Query<Object[]> query = HibernateUtil.createQuery(queryDTO);
			        List<Object[]> result = query.list();
		    	    
		            for (Object[] wiki : result) {
		            	String wikiSysId = (String) wiki[0];
		            	String wikiUNamespace = (String) wiki[1];
		            	AccessRights wikiAccessRights = (AccessRights) wiki[2];
		            	
		            	if (wikiUNamespace != null) {
		            		ResolveNamespace parentNamespace = null;
		            		String fullName = "";
		            		String[] hierarchy = wikiUNamespace.split(NAMESPACE_DELIMITER_ESCAPED);
		            		
		            		for(String level : hierarchy) {
		            			fullName = fullName.isEmpty() ? fullName + level : fullName + NAMESPACE_DELIMITER + level; 
		            			
		            			ResolveNamespace currentNamespace = null;
		            			if (cache.containsKey(fullName)) {
		            				currentNamespace = cache.get(fullName);
		            			} else {
		            				currentNamespace = getNamespace(fullName);
		            			}
		            			
		            			if (currentNamespace == null) {
		            				// create a new ResolveNamespace
		            				ResolveNamespace namespace = new ResolveNamespace();
		            				namespace.setParentNamespace(parentNamespace);
		            				namespace.setUName(level);

		            				currentNamespace = HibernateUtil.getDAOFactory().getResolveNamespaceDAO().insertOrUpdate(namespace);
		            				
		            				//create new AccessRights and assign a ResolveNamespace to it
		            				AccessRights nsRights = copyAndSaveAccessRights(wikiAccessRights, currentNamespace.getSys_id());
		            				HibernateUtil.getDAOFactory().getAccessRightsDAO().insertOrUpdate(nsRights);

		            				// Update ResolveNamespace reference to AccessRights
		            				currentNamespace.setAccessRights(nsRights);
		            				currentNamespace = HibernateUtil.getDAOFactory().getResolveNamespaceDAO().insertOrUpdate(currentNamespace);
		            			}

		            			if (currentNamespace != null) {
		            				cache.put(fullName, currentNamespace);
		            			}

		            			parentNamespace = currentNamespace;
		            		}
		            		
		            		final ResolveNamespace namespace = parentNamespace; 
		            		
		            		// Update processed WikiDocument with a reference to a new ResolveNamespace
		            		HibernateProxy.execute(() -> {
		            			CriteriaBuilder cb = HibernateUtil.getCurrentSession().getCriteriaBuilder();
		                		CriteriaUpdate<WikiDocument> update = cb.createCriteriaUpdate(WikiDocument.class);
		                		Root<WikiDocument> fromUpdate = update.from(WikiDocument.class);
		    					update.set(fromUpdate.get("namespace"), namespace)
		    							.where(cb.equal(fromUpdate.get("sys_id"), wikiSysId));

		                		return HibernateUtil.getCurrentSession().createQuery(update).executeUpdate();
		            		});
		        			
		            	}
		            }
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}
	
	public static List<NamespaceVO> convertToList(Map<ResolveNamespaceVO, Integer> map) {
		List<NamespaceVO> result = new ArrayList<>();
		for (ResolveNamespaceVO vo : map.keySet()) {
			NamespaceVO ns = new NamespaceVO(vo.getUName());
			ns.setCount(map.get(vo));
			// existing logic uses namespace name instead of sys_id
			ns.setSys_id(vo.getUName());
			result.add(ns);
		}
		
		result.sort((p1, p2) -> p1.getUNamespace().compareToIgnoreCase(p2.getUNamespace()));
		return result;
	}
	
	public static List<AccessRightsVO> getAccessRights(Set<String> namespaces) {
		List<AccessRightsVO> result = new ArrayList<AccessRightsVO>();

		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					ResolveNamespaceVO namespace = getNamespaceByFullName(ns);
					result.add(namespace.getAccessRights());
				} catch (Exception e) {
					Log.log.error(String.format(ERROR_GETTING_ACCESS_RIGHTS, ns), e);
				}
			}
		}

		return result;
	}

	public static Set<String> findAllDocumentsSysIdsForNamespace(String namespace) throws Exception {
		Set<String> result = new HashSet<String>();

		try {
			HibernateProxy.execute(() -> {
			ResolveNamespaceVO nsVO = getNamespaceByFullName(namespace);
				if (nsVO != null) {
					CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
	
					CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);
					Root<WikiDocument> from = criteriaQuery.from(WikiDocument.class);
	
					criteriaQuery.select(from.get("sys_id"))
							.where(criteriaBuilder.equal(from.get("namespace").get("sys_id"), nsVO.getSys_id()));
	
					List<Object[]> data = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();
	
					for (Object obj : data) {
						result.add((String) obj);
					}
				}
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static Set<String> findAllDocumentsNamesForNamespace(String namespace) throws Exception {
		Set<String> result = new HashSet<String>();

		try {
			HibernateProxy.execute(() -> {
				ResolveNamespaceVO nsVO = getNamespaceByFullName(namespace);
				if (nsVO != null) {
					CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();

					CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);
					Root<WikiDocument> from = criteriaQuery.from(WikiDocument.class);

					criteriaQuery.select(from.get("UFullname"))
							.where(criteriaBuilder.equal(from.get("namespace").get("sys_id"), nsVO.getSys_id()));

					List<Object[]> data = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();

					for (Object obj : data) {
						result.add((String) obj);
					}
				}
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	private static ResolveNamespace getNamespace(String name) {
		try {
			String[] hierarchy = name.split(NAMESPACE_DELIMITER_ESCAPED);

			CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();

	        CriteriaQuery<ResolveNamespace> criteriaQuery = criteriaBuilder.createQuery(ResolveNamespace.class);
	        Root<ResolveNamespace> from = criteriaQuery.from(ResolveNamespace.class);

	        criteriaQuery.select(from).where(criteriaBuilder.equal(from.get(NAMESPACE_NAME), hierarchy[0]));

	        ResolveNamespace result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getSingleResult();
	        
	        if (hierarchy.length == 1) {
	        	return result;
	        }

			for (int i = 1; i < hierarchy.length; i++) {
				String nodeName = hierarchy[i];
				result = result.getChildNamespaces().stream().filter(n -> nodeName.equalsIgnoreCase(n.getUName()))
						.findFirst().orElse(null);
			}

			return result;
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	private static AccessRights buildAccessRights(ResolveNamespaceVO vo, ResolveNamespace parent) {
		AccessRights ar = new AccessRights();

		if (vo.getAccessRights() == null) {
			if (parent == null) {
				throw new IllegalArgumentException(ERROR_INPUT_NULL_OR_EMPTY);
			} else {
				ar = parent.getAccessRights();
			}
		} else {
			ar.applyVOToModel(vo.getAccessRights());
		}

		return ar;
	}
	
	private static AccessRights copyAndSaveAccessRights(AccessRights wikiAccessRights, String resourceId) {
		if (wikiAccessRights != null) {
			AccessRights nsRights = new AccessRights();
			nsRights.setUResourceType(RESOURCE_TYPE);
			nsRights.setUReadAccess(wikiAccessRights.getUReadAccess());
			nsRights.setUWriteAccess(wikiAccessRights.getUWriteAccess());
			nsRights.setUAdminAccess(wikiAccessRights.getUAdminAccess());
			nsRights.setUExecuteAccess(wikiAccessRights.getUExecuteAccess());
			nsRights.setUResourceId(resourceId);
			
			return HibernateUtil.getDAOFactory().getAccessRightsDAO().insertOrUpdate(nsRights);
		}
		return null;
	}
}
