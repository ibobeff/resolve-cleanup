/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.immport;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.model.ResolveCatalogNodeWikidocRel;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocResolveTagRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceCatalog;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.catalog.CatalogUtil;
import com.resolve.services.catalog.ResolveCatalogStubCreator;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.TagUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ResolveCatalogVO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.util.Log;

public class ImportDocumentGraph extends ImportComponentGraph
{
    public ImportDocumentGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        Log.log.debug("Importing Document Relationship:" + rel);
        switch (rel.getTargetType())
        {
            case ResolveTag:
            case TAG: 
                assignTagToDocument();
                break;

                //this is for backward compatibility from 5.0
            case CatalogItem:
            case CatalogFolder:
            case CatalogGroup:
            case CatalogReference:
                addDocumentToCatalogReference();
                break;

            default:
                break;
        }
    }
    
    private void assignTagToDocument()
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            WikiDocument doc = WikiUtils.getWikiDocumentModel(null, rel.getSourceName(), username, false);
	            if(doc != null)
	            {
	                ResolveTag tag = TagUtil.findTag(null, rel.getTargetName(), username);
	                if(tag != null)
	                {
	                    //make sure that the relationship is not duplicated
	                    boolean insert = true;
	                    Collection<WikidocResolveTagRel> rels = doc.getWikidocResolveTagRels();
	                    if(rels != null && rels.size() > 0)
	                    {
	                        for(WikidocResolveTagRel rel : rels)
	                        {
	                            if(rel.getTag() != null)
	                            {
	                                if(rel.getTag().getUName().equalsIgnoreCase(tag.getUName()))
	                                {
	                                    insert = false;
	                                    break;
	                                }
	                            }
	                        }//end of for loop
	                    }
	                    
	                    if(insert)
	                    {
	                        WikidocResolveTagRel rel = new WikidocResolveTagRel();
	                        rel.setWikidoc(doc);
	                        rel.setTag(tag);
	                        
	                        HibernateUtil.getDAOFactory().getWikidocResolveTagRelDAO().persist(rel);
	                    }
	                }
	            }
            
            });
        }
        catch(Throwable t)
        {
            Log.log.error("Error in assigning Tag to Doc:" + rel, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
    }
    
    
    private void addDocumentToCatalogReference()
    {
        //example - /Jeet1/Group1/Item1
        String catalogPath = rel.getTargetName();
        String catalogName = catalogPath.split("/")[1]; 
        
        ResolveCatalogVO resolveCatalogVO = CatalogUtil.findCatalog(null, catalogName, username);
        if(resolveCatalogVO == null)
        {
            try
            {
                createCatalogStubs(catalogPath, rel.getTargetObject());
            }
            catch (Exception e)
            {
                Log.log.error("Error creating Doc-Catalog Rel for :" + rel, e);
            }
        }
        else
        {
            //we just cannot insert this record as it can corrupt the existing catalog 
            //so if the Node exist, we will just connect it with the document
            //String nodeSql = "from ResolveCatalogNode where LOWER(UPath) = '" + catalogPath.toLowerCase() + "'";
            String nodeSql = "from ResolveCatalogNode where LOWER(UPath) = :UPath";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UPath", catalogPath.toLowerCase());
            
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                WikiDocument doc = WikiUtils.getWikiDocumentModel(null, rel.getSourceName(), username, false);
	                if(doc != null)
	                {
	                    List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(nodeSql, queryParams);
	                    if (list != null && list.size() == 1)
	                    {
	                        ResolveCatalogNode catalogNode = (ResolveCatalogNode) list.get(0);
	    
	                        //insert only if the record is not present
	                        boolean insert = true;
	                        Collection<ResolveCatalogNodeWikidocRel> catalogWikiRels = catalogNode.getCatalogWikiRels();
	                        if (catalogWikiRels != null && catalogWikiRels.size() > 0)
	                        {
	                            for (ResolveCatalogNodeWikidocRel catalogWikiRel : catalogWikiRels)
	                            {
	                                if(catalogWikiRel.getWikidoc() != null)
	                                {
	                                    if(catalogWikiRel.getWikidoc().getSys_id().equalsIgnoreCase(doc.getSys_id()))
	                                    {
	                                        insert = false;
	                                        break;
	                                    }
	                                    
	                                }
	                            }//end of for loop
	                        }
	    
	                        if(insert)
	                        {
	                            ResolveCatalogNodeWikidocRel rel = new ResolveCatalogNodeWikidocRel();
	                            rel.setCatalog(catalogNode.getCatalog());
	                            rel.setCatalogNode(catalogNode);
	                            rel.setWikidoc(doc);
	                            
	                            HibernateUtil.getDAOFactory().getResolveCatalogNodeWikidocRelDAO().persist(rel);
	                        }
	                        
	                    }
	                }
	                else
	                {
	                    Log.log.debug("Cannot Import Relationship as Document Not available:" + rel);
	                }

                });

            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }
    
    // /Jeet1/Group1/Item1
    private Catalog createCatalogStubs(String path, Catalog refCatalog) throws Exception
    {
        Catalog catalog = null;
        
        //create the stub
        ResolveCatalogStubCreator stub = new ResolveCatalogStubCreator(path, username);
        stub.setReferenceCatalog(refCatalog);
        catalog = stub.createStub();
        
        //create it
        catalog = ServiceCatalog.saveCatalog(catalog, "system");
        
        return catalog;
    }

}
