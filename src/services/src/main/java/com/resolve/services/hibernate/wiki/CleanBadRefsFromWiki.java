package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import com.resolve.services.hibernate.util.UpdateWikiDocRelations;

public class CleanBadRefsFromWiki implements Callable<Integer>
{
    List<String> wikidocSysIdList = new ArrayList<String>();
    
    public CleanBadRefsFromWiki(Set<String> sysIds)
    {
        wikidocSysIdList.addAll(sysIds);
    }

    public Integer call() throws Exception
    {
        int counter = 0;
        for (String wikiSysId : wikidocSysIdList)
        {
            counter++;
            UpdateWikiDocRelations.cleanWikidocBadReferences(wikiSysId);
        }
        
        return counter;
    }
}
