/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.vo;


import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.resolve.services.catalog.Catalog;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.interfaces.ImpexOptionsDTO;

public class SocialImpexVO
{
    //if ALL, then consider all the entities of that component and then you can ignore the rest    
    private Map<String, ImpexOptionsDTO> processes;
    private Map<String, ImpexOptionsDTO> teams;
    private Map<String, ImpexOptionsDTO> forums;
//    private Map<String, ImpexOptionsDTO> rsses;
//    private Map<UserImpexVO, ImpexOptionsDTO> users;
//    private Map<String, ImpexOptionsDTO> tags;
    private Map<String, ImpexOptionsDTO> catalogs;
    private Map<String, ImpexOptionsDTO> documents;
    private Map<String, ImpexOptionsDTO> actiontasks;
    
    //where the export/import will look at
    private String folderLocation;
    
    //return type from social
    private Set<Forum> returnForums = new HashSet<Forum>();
    private Set<Team> returnTeams = new HashSet<Team>();
    private Set<Process> returnProcesses = new HashSet<Process>();
    private Set<User> returnUsers = new HashSet<User>();
    private Set<Document> returnDocument = new HashSet<Document>();
//    private Set<Runbook> returnRunbook = new HashSet<Runbook>();
    private Set<ActionTask> returnActiontask = new HashSet<ActionTask>();
    private Set<Rss> returnRss = new HashSet<Rss>();
//    private Set<ResolveTag> returnTags = new HashSet<ResolveTag>();
    private Set<Catalog> returnCatalogs = new HashSet<Catalog>();
    
    public Set<Document> getReturnDocument()
    {
        return returnDocument;
    }
    public void setReturnDocument(Set<Document> returnDocument)
    {
        this.returnDocument = returnDocument;
    }
    
    public void addReturnDocument(Document document)
    {
        this.returnDocument.add(document);
    }
    
//    public Set<Runbook> getReturnRunbook()
//    {
//        return returnRunbook;
//    }
//    public void setReturnRunbook(Set<Runbook> returnRunbook)
//    {
//        this.returnRunbook = returnRunbook;
//    }
//    
//    public void addReturnRunbook(Runbook runbook)
//    {
//        this.returnRunbook.add(runbook);
//    }
    
    public Set<ActionTask> getReturnActiontask()
    {
        return returnActiontask;
    }
    public void setReturnActiontask(Set<ActionTask> returnActiontask)
    {
        this.returnActiontask = returnActiontask;
    }
    
    public void addReturnActionTask(ActionTask actiontask)
    {
        this.returnActiontask.add(actiontask);
    }
    
    public Set<Rss> getReturnRss()
    {
        return returnRss;
    }

    public void setReturnRss(Set<Rss> returnRss)
    {
        this.returnRss = returnRss;
    }

    public void addReturnRss(Rss rss)
    {
        this.returnRss.add(rss);
    }
//    
//    public Set<ResolveTag> getReturnTags()
//    {
//        return this.returnTags;
//    }
//    
//    public void setReturnTags(Set<ResolveTag> returnTags)
//    {
//        this.returnTags = returnTags;
//    }
//    
//    public void addReturnTag(ResolveTag tag)
//    {
//        this.returnTags.add(tag);
//    }
    
    public Set<Catalog> getReturnCatalogs()
    {
        return this.returnCatalogs;
    }
    
    public void setReturnCatalogs(Set<Catalog> catalogs)
    {
        this.returnCatalogs = catalogs;
    }
    
    public void addReturnCatalog(Catalog catalog)
    {
        this.returnCatalogs.add(catalog);
    }
    
    public Map<String, ImpexOptionsDTO> getForums()
    {
        return forums;
    }
    public void setForums(Map<String, ImpexOptionsDTO> forums)
    {
        this.forums = forums;
    }
    public Map<String, ImpexOptionsDTO> getTeams()
    {
        return teams;
    }
    public void setTeams(Map<String, ImpexOptionsDTO> teams)
    {
        this.teams = teams;
    }
    public Map<String, ImpexOptionsDTO> getProcesses()
    {
        return processes;
    }
    public void setProcesses(Map<String, ImpexOptionsDTO> processes)
    {
        this.processes = processes;
    }
    
//    public Map<String, ImpexOptionsDTO> getRsses()
//    {
//        return rsses;
//    }
//    
//    public void setRsses(Map<String, ImpexOptionsDTO> rsses)
//    {
//        this.rsses = rsses;
//    }
//    
//    public Map<UserImpexVO, ImpexOptionsDTO> getUsers()
//    {
//        return users;
//    }
//    public void setUsers(Map<UserImpexVO, ImpexOptionsDTO> users)
//    {
//        this.users = users;
//    }
//    
//    public void setTags(Map<String, ImpexOptionsDTO> tags)
//    {
//        this.tags = tags;
//    }
//    
//    public Map<String, ImpexOptionsDTO> getTags()
//    {
//        return this.tags;
//    }
    
    public void setCatalogs(Map<String, ImpexOptionsDTO> catalogs)
    {
        this.catalogs = catalogs;
    }

    public Map<String, ImpexOptionsDTO> getCatalogs()
    {
        return this.catalogs;
    }
    
    public String getFolderLocation()
    {
        return folderLocation;
    }
    public void setFolderLocation(String folderLocation)
    {
        this.folderLocation = folderLocation;
    }
    public Set<Forum> getReturnForums()
    {
        return returnForums;
    }
    public void setReturnForums(Set<Forum> returnForums)
    {
        this.returnForums = returnForums;
    }
    
    public void addReturnForum(Forum forum)
    {
        returnForums.add(forum);
    }
    
    public Set<Team> getReturnTeams()
    {
        return returnTeams;
    }
    public void setReturnTeams(Set<Team> returnTeams)
    {
        this.returnTeams = returnTeams;
    }
    
    public void addReturnTeam(Team team)
    {
        this.returnTeams.add(team);
    }
    
    public Set<Process> getReturnProcesses()
    {
        return returnProcesses;
    }
    public void setReturnProcesses(Set<Process> returnProcesses)
    {
        this.returnProcesses = returnProcesses;
    }
    
    public void addReturnProcess(Process process)
    {
        this.returnProcesses.add(process);
    }
    public Map<String, ImpexOptionsDTO> getDocuments()
    {
        return documents;
    }
    public void setDocuments(Map<String, ImpexOptionsDTO> documents)
    {
        this.documents = documents;
    }
    public Map<String, ImpexOptionsDTO> getActiontasks()
    {
        return actiontasks;
    }
    public void setActiontasks(Map<String, ImpexOptionsDTO> actiontasks)
    {
        this.actiontasks = actiontasks;
    }
    
    public Set<User> getReturnUsers()
    {
        return returnUsers;
    }
    public void setReturnUsers(Set<User> returnUsers)
    {
        this.returnUsers = returnUsers;
    }
    
    public void addReturnUser(User user)
    {
        this.returnUsers.add(user);
    }
    
}
