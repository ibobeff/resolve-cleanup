package com.resolve.services.rr.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.util.StringUtils;

public class Locker
{
    public static final String EVT_TYPE = "EVT_TYPE";
    public static final String EVT_USERNAME = "EVT_USERNAME";
    public static final String EVT_TIMESTAMP = "EVT_TIMESTAMP";
    public static final String EVT_RSVIEW_ID = "EVT_RSVIEW_ID";
    public static final String EVT_IS_CURRENT_NODE = "EVT_IS_CURRENT_NODE";
    public static final String RESOLUTION_ROUTING_DB_OP = "RESOLUTION_ROUTING_DB_OP";
    public static final long MINUTE_IN_MILLIS = 60000;
    public static final int TIMEOUT_IN_MINUTE = 5;
    
    private static String generateBulkDBEvtInfo(Map<String, TreeMap<String, String>> locks,String tid,String type,String username,String rsviewId,long millis) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        TreeMap<String,String> newLock = new TreeMap<String,String>();
        newLock.put(Locker.EVT_TYPE, type);
        newLock.put(Locker.EVT_USERNAME, username);
        newLock.put(Locker.EVT_TIMESTAMP, millis + "");
        newLock.put(Locker.EVT_RSVIEW_ID,rsviewId);
        locks.put(tid,newLock);
        return mapper.writeValueAsString(locks);
    }
    public static TreeMap<String, TreeMap<String, String>> parseBulkDBEvtInfo(String strValue) throws JsonProcessingException, IOException {
        String[] values = strValue.split(StringUtils.DELIMITER);
        ObjectMapper mapper = new ObjectMapper();
        TreeMap<String,TreeMap<String,String>> map = null;
        map = mapper.readValue(values[1],new TypeReference<TreeMap<String,TreeMap<String,String>>>(){});
        return map;
    }
    public static LockDTO fetchLock(Map<String,String> map) throws Exception {
        long current = System.currentTimeMillis();
        String rsviewId = MainBase.main.configId.getGuid();
        ResolveEventVO evt = ServiceHibernate.getLatestEventByValue(Locker.RESOLUTION_ROUTING_DB_OP);
        if(evt!=null) {
            Map<String, TreeMap<String, String>> locks= parseBulkDBEvtInfo(evt.getUValue());
            Iterator<Entry<String, TreeMap<String, String>>> it  = locks.entrySet().iterator();
            Entry<String, TreeMap<String, String>> firstLock = null;
            if(!it.hasNext()) {
                releaseBulkOpLock(null);
                return new LockDTO();
            }
            while(it.hasNext()) {
                firstLock = it.next();
                if(!firstLock.getValue().get(Locker.EVT_RSVIEW_ID).equals(rsviewId))
                    continue;
                long stamp = Long.valueOf(firstLock.getValue().get(Locker.EVT_TIMESTAMP));
                String evtUsername = firstLock.getValue().get(Locker.EVT_USERNAME);
                String evtType = firstLock.getValue().get(Locker.EVT_TYPE);
                String evtRsviewId = firstLock.getValue().get(Locker.EVT_RSVIEW_ID);
                if(current - stamp<Locker.TIMEOUT_IN_MINUTE*Locker.MINUTE_IN_MILLIS) {
                    map.put(Locker.EVT_TYPE, evtType);
                    map.put(Locker.EVT_USERNAME, evtUsername);
                    map.put(Locker.EVT_RSVIEW_ID, evtRsviewId);
                    map.put(Locker.EVT_IS_CURRENT_NODE, rsviewId.equals(evtRsviewId)+"");
                    map.put(BaseBulkTask.TASK_ID, firstLock.getKey());
                    return new LockDTO(evt,locks);
                }
                releaseBulkOpLock(firstLock.getKey());
            }
        }
        return new LockDTO();
    }
    //need to pay attention to this... when the server is down due to whatever reason.. the event table may be dirty.. and this rr app won't know it..it may make server reject any bulk req before the lock is timedout
    //u can't simply clean up the event by looking up task with user name store in the event info .. coz user may use another node to do the bulk op at the moment
    public static boolean lock(Map<String,String> map,BaseBulkTask task,boolean force) throws Exception {
        LockDTO lock = fetchLock(map);
        if(force)
            map.clear();
        if(!map.isEmpty())
            return false;
        String rsviewId = MainBase.main.configId.getGuid();
        ResolveEventVO evt;
        if(lock.getEvt()==null)
            evt = new ResolveEventVO();
        else
            evt = lock.getEvt();
        evt.setUValue(Locker.RESOLUTION_ROUTING_DB_OP + StringUtils.DELIMITER + generateBulkDBEvtInfo(lock.getLocks(),task.getTid(),task.getType(),task.getUsername(), rsviewId,System.currentTimeMillis()));
        ServiceHibernate.insertResolveEvent(evt);
        return true;
    }
    
    public static void releaseBulkOpLock(String tid) throws Exception {
        if(StringUtils.isEmpty(tid)) {
            ServiceHibernate.clearAllResolveEventByValue(Locker.RESOLUTION_ROUTING_DB_OP);
            return;
        }
        ResolveEventVO evt = ServiceHibernate.getLatestEventByValue(Locker.RESOLUTION_ROUTING_DB_OP);
        if(evt==null)
            return;
        Map<String, TreeMap<String, String>> locks = parseBulkDBEvtInfo(evt.getUValue());
        locks.remove(tid);
        if(locks.isEmpty()) {
            ServiceHibernate.clearAllResolveEventByValue(Locker.RESOLUTION_ROUTING_DB_OP);
            return;
        }
        ObjectMapper mapper = new ObjectMapper();
        String str = mapper.writeValueAsString(locks);
        evt.setUValue(Locker.RESOLUTION_ROUTING_DB_OP + StringUtils.DELIMITER + str);
        ServiceHibernate.insertResolveEvent(evt);
    }
    
    public static void updateLock(String tid,String username,long millis) throws Exception {
        ResolveEventVO evt = ServiceHibernate.getLatestEventByValue(Locker.RESOLUTION_ROUTING_DB_OP);
        Map<String, TreeMap<String, String>> locks = parseBulkDBEvtInfo(evt.getUValue());
        Map<String,String> currentTaskLock = locks.get(tid);
        String evtUsername = currentTaskLock.get(Locker.EVT_USERNAME);
        String evtType = currentTaskLock.get(Locker.EVT_TYPE);
        String evtRsviewId = currentTaskLock.get(Locker.EVT_RSVIEW_ID);
        evt.setUValue(Locker.RESOLUTION_ROUTING_DB_OP + StringUtils.DELIMITER + generateBulkDBEvtInfo(locks,tid,evtType,evtUsername, evtRsviewId,millis));
        ServiceHibernate.insertResolveEvent(evt);
    }
}
