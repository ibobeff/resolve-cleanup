/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.catalog;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.vo.social.NodeType;
import com.resolve.util.StringUtils;

public class ResolveCatalogStubCreator
{
    private String path = null;
//    private String username = null;
    
    private Catalog rootCatalog = new Catalog();
    
    //optional - we should be able to create a place holder with just the path
    private Catalog referenceCatalog = null;
    
    public ResolveCatalogStubCreator(String path, String username) throws Exception
    {
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(path))
        {
            throw new Exception("Path and username are mandatory");
        }
        
        if(!path.startsWith("/"))
        {
            throw new Exception("Invalid Path. It should start with '/' ");
        }
        
        this.path = path;
//        this.username = username;
    }
    
    public Catalog getReferenceCatalog()
    {
        return referenceCatalog;
    }

    public void setReferenceCatalog(Catalog referenceCatalog)
    {
        this.referenceCatalog = referenceCatalog;
    }

    public Catalog createStub() throws Exception
    {
        create(path, null);
        
        return rootCatalog;
    }
    
    // /Jeet1/Group1/Item1
    private void create(String originalPath, Catalog child)
    {
        String catalogName = originalPath.substring(originalPath.lastIndexOf("/")+1); //Item1
        String parentPath = originalPath.substring(0, originalPath.lastIndexOf("/"));// /Jeet1/Group1
        
        if(parentPath.equals("/") || parentPath.equals(""))
        {
            //this is root
            rootCatalog.setType(NodeType.CatalogReference.name());
            rootCatalog.setRoot(true);
            rootCatalog.setName(catalogName);
            rootCatalog.setPath(originalPath);
            
            if(child != null)
            {
                List<Catalog> children = new ArrayList<Catalog>();
                children.add(child);
                rootCatalog.setChildren(children);
            }
        }
        else
        {
            //create the current catalog object
            Catalog parent = new Catalog();
            parent.setName(catalogName);
            parent.setDisplayType(catalogName);
            parent.setPath(originalPath);            
            if(this.referenceCatalog != null && this.referenceCatalog.getPath().equals(originalPath))
            {
                parent.setType(referenceCatalog.getType());
                parent.setTitle(referenceCatalog.getTitle());
                parent.setInternalName(referenceCatalog.getInternalName());
                parent.setNamespace(referenceCatalog.getNamespace());
                parent.setOpenInNewTab(referenceCatalog.isOpenInNewTab());
            }
            else
            {
                //defaults for a catalog node
                parent.setInternalName(catalogName);
                parent.setType(NodeType.CatalogGroup.name());
            }
            

            if(child != null)
            {
                List<Catalog> children = new ArrayList<Catalog>();
                children.add(child);
                parent.setChildren(children);
            }
            
            //**RECURSIVE
            create(parentPath, parent);
        }
        
    }
    
    

}
