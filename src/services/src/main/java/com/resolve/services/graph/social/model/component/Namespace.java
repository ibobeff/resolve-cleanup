/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

public class Namespace extends RSComponent implements RSPublisher
{
    private static final long serialVersionUID = 4500031328725747561L;

//    public static final String TYPE = "namespace";
    public static final String NAMESPACE_NAME = "namespaceName";
    
    private transient String node_sys_id;
    
    public Namespace() 
    {
        super(NodeType.NAMESPACE);
    }
    
    public Namespace(String name) 
    {
        super(NodeType.NAMESPACE);
        setId(name);
        setName(name);
    }
    
    public Namespace(ResolveNodeVO node) throws Exception
    {
        super(node);
        node_sys_id = node.getSys_id();
    }
    
    public String getNodeSysId()
    {
        return node_sys_id;
    }
}
