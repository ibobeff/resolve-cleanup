/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.social;

import java.util.List;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;

public class ExportDocumentGraph extends ExportComponentGraph
{
    public ExportDocumentGraph(String sysId) throws Exception
    {
        super(sysId, SocialRelationshipTypes.DOCUMENT);
    }

    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode) throws Exception
    {
        //for documents, export the tags 
        exportTagsRelationship(compNode);

        return relationships;
    }

    private void exportTagsRelationship(Node compNode) throws Exception
    {
        //get all the nodes that this process node is refering too
        Iterable<Node> tagsForDocument = findTagsFor(compNode);
        if (tagsForDocument != null)
        {
            for (Node anyNode : tagsForDocument)
            {
                addRelationship(compNode, anyNode, null);
            }//end of for
        }

    }

    public static Iterable<Node> findTagsFor(Node compNode)
    {
        TraversalDescription traversalTeamForMemberTeams = 
                        Traversal.description().breadthFirst()
                        .relationships(SocialRelationshipTypes.ResolveTag)
                        .uniqueness(Uniqueness.NODE_GLOBAL)
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator(new Evaluator()
        {
            public Evaluation evaluate(Path path)
            {
                Node node = path.endNode();
                String type = (String) node.getProperty(GraphDBManager.TYPE);
                //                                    String name = (String) node.getProperty(RSComponent.DISPLAYNAME);
                //                                    Log.log.debug("type : " + type + " name:" + name);

                //if its a TEAM node, than include it, else ignore
                if (type.equalsIgnoreCase("ResolveTag"))
                {
                    return Evaluation.INCLUDE_AND_CONTINUE;
                }
                else
                {
                    return Evaluation.EXCLUDE_AND_CONTINUE;
                }
            }
        });

        return traversalTeamForMemberTeams.traverse(compNode).nodes();
    }

}
