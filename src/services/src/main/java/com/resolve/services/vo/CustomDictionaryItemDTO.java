package com.resolve.services.vo;

public class CustomDictionaryItemDTO {
	private String UShortName;
	private String UFullName;
	private String UDescription;

	public String getUShortName() {
		return UShortName;
	}

	public void setUShortName(String uShortName) {
		UShortName = uShortName;
	}

	public String getUFullName() {
		return UFullName;
	}

	public void setUFullName(String uFullName) {
		UFullName = uFullName;
	}

	public String getUDescription() {
		return UDescription;
	}

	public void setUDescription(String uDescription) {
		UDescription = uDescription;
	}
}
