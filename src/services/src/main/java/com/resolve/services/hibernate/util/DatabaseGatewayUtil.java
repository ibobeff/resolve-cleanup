/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.dao.DatabaseConnectionPoolDAO;
import com.resolve.persistence.model.DatabaseConnectionPool;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class DatabaseGatewayUtil extends GatewayUtil
{
    private static volatile DatabaseGatewayUtil instance = null;
    public static DatabaseGatewayUtil getInstance()
    {
        if(instance == null)
        {
            instance = new DatabaseGatewayUtil();
        }
        return instance;
    }

    @SuppressWarnings("rawtypes")
    public void setDatabasePools(Map pools)
    {
        try
        {
        	HibernateProxy.execute(() -> {
        		 DatabaseConnectionPoolDAO dbDao = HibernateUtil.getDAOFactory().getDatabaseConnectionPoolDAO();

                 String queue = deleteByQueue(DatabaseConnectionPool.class.getSimpleName(), pools);

                 // update each pool
                 for (Object item : pools.entrySet())
                 {
                     Map.Entry entry = (Map.Entry) item;
                     String poolStringMap = (String) entry.getValue();
                     Map poolMap = StringUtils.stringToMap(poolStringMap);

                     String name = (String) poolMap.get("NAME");
                     String driverclass = (String) poolMap.get("DRIVERCLASS");
                     String connecturl = (String) poolMap.get("CONNECTURI");
                     Integer maxconnection = new Integer((String) poolMap.get("MAXCONNECTION"));
                     String username = (String) poolMap.get("USERNAME");
                     String password = (String) poolMap.get("PASSWORD");

                     // init record entry with values
                     DatabaseConnectionPool dbpool = new DatabaseConnectionPool();
                     dbpool.setUQueue(queue);
                     dbpool.setUPoolName(name);
                     DatabaseConnectionPool row = HibernateUtil.getDAOFactory().getDatabaseConnectionPoolDAO().findFirst(dbpool);
                     if (row == null)
                     {
                         row = new DatabaseConnectionPool();
                     }
                     row.setUQueue(queue);
                     row.setUPoolName(name);
                     ;
                     row.setUDriverClass(driverclass);
                     row.setUDBConnectURI(connecturl);
                     row.setUDBMaxConn(maxconnection);
                     row.setUDBUsername(username);
                     row.setUDBPassword(password);
                     dbDao.save(row);
                 }
        	});
           
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setPools

    @SuppressWarnings("unchecked")
    public static List<Map<String, String>> getDatabasePools(String queueName)
    {
        List<Map<String, String>> pools = new ArrayList<Map<String, String>>();
        try
        {

        	HibernateProxy.execute(() -> {
                List<DatabaseConnectionPool> poolList = HibernateUtil.createQuery("select o from DatabaseConnectionPool o where UQueue='" + queueName + "'").list();

                for (DatabaseConnectionPool pool : poolList)
                {
                    Map<String, String> poolMap = new HashMap<String, String>();

                    poolMap.put("NAME", pool.getUPoolName());
                    poolMap.put("DRIVERCLASS", pool.getUDriverClass());
                    poolMap.put("CONNECTURI", pool.getUDBConnectURI());
                    poolMap.put("MAXCONNECTION", String.valueOf(pool.getUDBMaxConn()));
                    poolMap.put("USERNAME", pool.getUDBUsername());
                    poolMap.put("PASSWORD", pool.getUDBPassword());

                    pools.add(poolMap);
                }

        	});
            
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return pools;
    } // getPools

}
