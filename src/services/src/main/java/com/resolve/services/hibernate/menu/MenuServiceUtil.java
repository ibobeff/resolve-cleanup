/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.menu;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.persistence.dao.RolesDAO;
import com.resolve.persistence.dao.SysAppApplicationDAO;
import com.resolve.persistence.dao.SysPerspectiveDAO;
import com.resolve.persistence.dao.SysPerspectiveapplicationsDAO;
import com.resolve.persistence.dao.SysPerspectiverolesDAO;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysAppApplicationroles;
import com.resolve.persistence.model.SysAppModule;
import com.resolve.persistence.model.SysAppModuleroles;
import com.resolve.persistence.model.SysPerspective;
import com.resolve.persistence.model.SysPerspectiveapplications;
import com.resolve.persistence.model.SysPerspectiveroles;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.vo.MenuSetModelVO;
import com.resolve.services.hibernate.util.vo.MenuSetVO;
import com.resolve.services.hibernate.vo.SysAppModuleVO;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.menu.MenuDTO;
import com.resolve.services.vo.menu.MenuItemDTO;
import com.resolve.services.vo.menu.MenuSetModel;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MenuServiceUtil
{
//    private static final String USER_MENU_CACHE = "USER_MENU_CACHE";
    
    public static MenuDTO getMenu(String username) throws Exception
    {
        MenuDTO menu = getMenuFromCache(username);
        if(menu == null)
        {
            Log.log.debug("Querying DB to get the menu");
            menu = getMenuFromDB(username);
            addMenuToCache(username, menu);
        }
        
        return menu;
    }
    
    
    @SuppressWarnings("unchecked")
	public static Map<String, Object> getMenuSets(String userName, boolean forNavigation, String defaultMenuSet)
    {    
        try
        {
          HibernateProxy.setCurrentUser(userName);
        	return (Map<String, Object>) HibernateProxy.execute(() -> {
        		
        		  Map<String, Object> result = new HashMap<String, Object>();
        	        List<MenuSetVO> menuSetList = new ArrayList<MenuSetVO>();
        	        List<SysPerspective> sysPerspective = new ArrayList<SysPerspective>();
        	        List<String> applicationsStrList = new ArrayList<String>();
        	        List<String> rolesStrList = new ArrayList<String>();
        	        
        	        List<Map<String, String>> applications = getMenuApplications();
        	        StringBuffer applicationsStr = null;
        	        StringBuffer rolesStr = null;
        	        boolean isDefaultMenuSetDisabled = false; //return this value too
        		
                List<SysPerspective> list = HibernateUtil.getDAOFactory().getSysPerspectiveDAO().findAll();

                for (SysPerspective sysPersp : list)
                {
                    if (forNavigation)
                    {
                        if (sysPersp.getActive() != null && !sysPersp.getActive())
                        {
                            if (sysPersp.getName().equalsIgnoreCase(defaultMenuSet))
                            {
                                isDefaultMenuSetDisabled = true;
                            }
                            continue;
                        }
                    }
                    List<SysPerspectiveroles> roles = (List<SysPerspectiveroles>) sysPersp.getSysPerspectiveroles();

                    rolesStr = new StringBuffer();
                    for (SysPerspectiveroles role : roles)
                    {
                        rolesStr.append(" " + role.getValue());
                    }

                    List<SysPerspectiveapplications> apps = (List<SysPerspectiveapplications>) sysPersp.getSysPerspectiveapplications();

                    applicationsStr = new StringBuffer();

                    for (SysPerspectiveapplications app : apps)
                    {
                        String key = app.getValue();
                        boolean active = getMenuSectionActivityByName(applications, key);

                        if (active)
                        {
                            String displayName = getMenuSectionDisplayNameByID(applications, key);
                            applicationsStr.append(" " + displayName);
                        }
                    }

                    // check permission
                    boolean hasRole = false;

                    if (roles != null && roles.size() > 0)
                    {
                        Set<String> checkRoles = new HashSet<String>();
                        for (SysPerspectiveroles role : roles)
                        {
                            checkRoles.add(role.getValue());
                        }

                        hasRole = UserUtils.hasRole(userName, checkRoles);
                    }
                    else
                    {
                        // default empty roles is true
                        hasRole = true;
                    }

                    if (hasRole)
                    {
                        sysPerspective.add(sysPersp);
                        applicationsStrList.add(applicationsStr.toString());
                        rolesStrList.add(rolesStr.toString());
                    }
                    
                }//end of for loop
                
                for(int x=0; x < sysPerspective.size(); x++)
                {
                    SysPerspective pers = sysPerspective.get(x);
                    String roles = rolesStrList.get(x);
                    String application = applicationsStrList.get(x);
                    
                    MenuSetVO vo = new MenuSetVO();
                    vo.setSysPerspectiveVO(pers.doGetVO());
                    vo.setApplicationsStr(application);
                    vo.setRolesStr(roles);
                    
                    menuSetList.add(vo);
                }
                
                //put the value
                result.put(HibernateConstants.DEFAULT_MENUSET_DISABLED, isDefaultMenuSetDisabled);
                result.put(HibernateConstants.MENU_SETS, menuSetList);
                
                return result;
                
        	});

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
            return new HashMap<String, Object>();
        }
        
        
    }
    
    public static List<Map<String, String>> getMenuSections(String userName, String selectedMenuSet, String menuSetID)
    {
        List<Map<String, String>> menuDefinitionNameList = new ArrayList<Map<String,String>>();
        
        if (menuSetID == null || menuSetID.equals(""))
        {
            menuSetID = getMenuSetID(selectedMenuSet);
        }
        
        String menuSetIDfinal = menuSetID;        
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
        		
        		List<Map<String, String>> application = getMenuApplications();
                if (StringUtils.isNotBlank(selectedMenuSet) && !selectedMenuSet.equalsIgnoreCase("All"))
                {

                    MenuSetModelVO menuSetModel = getMenuSetByID(menuSetIDfinal);

                    if (menuSetModel != null)
                    {
                        Map<String, String> valueDisplayMap = new HashMap<String, String>();
                        List<Map<String, String>> menuDefList = menuSetModel.getMenuSetApplicationsList();
                        List<Map<String, String>> updatedMenuSectionList = changeSequenceBasedOnMenuDefinition(menuDefList);

                        menuDefList = sortList(updatedMenuSectionList);

                        // check duplicates
                        Set<String> duplicates = new HashSet<String>();

                        for (Map<String, String> menuDef : menuDefList)
                        {
                            String key = menuDef.get("value");

                            String displayName = getMenuSectionDisplayNameByID(application, key);
                            boolean active = getMenuSectionActivityByName(application, key);

                            if (active)
                            {
                                if (checkMenuSectionRole(userName, key) && !duplicates.contains(key))
                                {
                                    valueDisplayMap = new HashMap<String, String>();
                                    valueDisplayMap.put("value", key);
                                    valueDisplayMap.put("displayName", displayName);
                                    valueDisplayMap.put("color", menuDef.get("color"));
                                    valueDisplayMap.put("textColor", menuDef.get("textColor"));
                                    
                                    String seq = menuDef.get("sequence");
                                    if(StringUtils.isNotBlank(seq))
                                    {
                                        try
                                        {
                                            if(seq.indexOf('.') > -1)
                                            {
                                                seq = seq.substring(0, seq.indexOf('.'));
                                            }
                                            
                                            seq = Integer.parseInt(seq) + "";
                                        }
                                        catch(Exception e)
                                        {
                                            seq = "0";
                                        }
                                    }
                                    else
                                    {
                                        seq = "0";
                                    }
                                    valueDisplayMap.put("order", seq);
                                    menuDefinitionNameList.add(valueDisplayMap);

                                    // add duplicate check
                                    duplicates.add(key);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (application != null && !application.isEmpty())
                    {
                        application = sortApplications(application);
                        Map<String, String> valueDisplayMap = new HashMap<String, String>();

                        for (Map<String, String> app : application)
                        {
                            boolean active = Boolean.valueOf(app.get("active")).booleanValue();

                            if (active)
                            {
                                String key = app.get("name");
                                String displayName = app.get("title");

                                if (checkMenuSectionRole(userName, key))
                                {
                                    valueDisplayMap = new HashMap<String, String>();
                                    valueDisplayMap.put("value", key);
                                    valueDisplayMap.put("displayName", displayName);
                                    valueDisplayMap.put("color", app.get("color"));
                                    valueDisplayMap.put("textColor", app.get("textColor"));
                                    String seq = app.get("sequence");
                                    if(StringUtils.isNotBlank(seq))
                                    {
                                        try
                                        {
                                            if(seq.indexOf('.') > -1)
                                            {
                                                seq = seq.substring(0, seq.indexOf('.'));
                                            }
                                            
                                            seq = Integer.parseInt(seq) + "";
                                        }
                                        catch(Exception e)
                                        {
                                            seq = "0";
                                        }
                                    }
                                    else
                                    {
                                        seq = "0";
                                    }
                                    valueDisplayMap.put("order", seq);
                                    menuDefinitionNameList.add(valueDisplayMap);
                                }
                            }
                        }
                    }
                }

        	});
        }
        catch (Throwable t)
        {
            Log.log.warn(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return menuDefinitionNameList;
        
    }
    
    private static List<Map<String, String>> getMenuItems(String userName, String menuSection)
    {
        List<Map<String, String>> menuItems = new ArrayList<Map<String,String>>();

        try
        {
            if (menuSection != null && !menuSection.equals(""))
            {
              HibernateProxy.setCurrentUser(userName);
            	HibernateProxy.execute(() -> {
                    SysAppApplicationDAO sysAppAppDAO = HibernateUtil.getDAOFactory().getSysAppApplicationDAO();
                    SysAppApplication sysAppApp = new SysAppApplication();
                    sysAppApp.setName(menuSection);

                    List<SysAppApplication> sysAppAppList = sysAppAppDAO.find(sysAppApp);

                    if (sysAppAppList != null && !sysAppAppList.isEmpty())
                    {
                        sysAppApp = sysAppAppList.get(0);

                        List<SysAppModule> sysAppModuleList = (List<SysAppModule>) sysAppApp.getSysAppModules();

                        if (sysAppModuleList != null && !sysAppModuleList.isEmpty())
                        {
                            Map<String, String> moduleMap = new HashMap<String, String>();

                            for (SysAppModule module : sysAppModuleList)
                            {
                                String linkType = module.getLinkType();
                                boolean active = (module.getActive() == null) ? false : (module.getActive().booleanValue());

                                // only return the active Modules
                                if ((linkType == null || !linkType.equals("SEPARATOR")) && active)
                                {
                                    boolean hasRole = false;

                                    Collection<SysAppModuleroles> moduleRoles = module.getSysAppModuleroles();
                                    if (moduleRoles != null && moduleRoles.size() > 0)
                                    {
                                        Set<String> roles = new HashSet<String>();
                                        for (SysAppModuleroles role : moduleRoles)
                                        {
                                            roles.add(role.getValue());
                                        }

                                        hasRole = UserUtils.hasRole(userName, roles);
                                    }
                                    else
                                    {
                                        // default empty roles is true
                                        hasRole = true;
                                    }

                                    if (hasRole)
                                    {
                                        moduleMap = new HashMap<String, String>();
                                        moduleMap.put("name", module.getTitle());
                                        moduleMap.put("group", (module.getAppModuleGroup() == null || 
                                        						module.getAppModuleGroup().equals("&nbsp")) ? 
                                        						"." : module.getAppModuleGroup());
                                        moduleMap.put("query", module.getQuery());
                                        moduleMap.put("order", (module.getOrder() == null) ? "1" : module.getOrder().toString());
                                        moduleMap.put("id", module.getSys_id());
                                        menuItems.add(moduleMap);
                                    }
                                }
                            }
                        }
                    }
            	});
            }
            else
            {
                Log.log.warn("MenuSection is empty");
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return sortMenuItems(menuItems);
    } // getMenuItems
    
    @SuppressWarnings("unchecked")
    public static List<SysAppModuleVO> getMenuItems(QueryDTO query, String userName)
    {
        /*
         * This is not a generic method to get all menu items. It's specifically written for Impex operation.
         * Please go through the logic and determine whether it will server the purpose. 
         */
        
        List<SysAppModuleVO> result = new ArrayList<SysAppModuleVO>();
        StringBuilder hql = new StringBuilder("select ");
        
        if(StringUtils.isNotBlank(query.getSelectColumns()))
        {
            StringBuilder updatedCols = new StringBuilder();
            String[] selectedCols = query.getSelectColumns().split(",");
            for(String col : selectedCols)
            {
                updatedCols.append("SAM.").append(col).append(",");
            }
            
            //remove the last comma
            updatedCols.setLength(updatedCols.length()-1);
            
            hql.append(updatedCols).append(",SAA.title");
        }
        else
        {
            hql.append(" SAM.TITLE ");
        }
        
        hql.append(" FROM SysAppModule as SAM, SysAppApplication as SAA where SAM.sysAppApplication = SAA.sys_id");
        
        if (StringUtils.isNotBlank(query.getSort()))
        {
            query.setSort(query.getSort().replaceAll("title", "SAM.title"));
            if (query.getSort().contains("name"))
            {
            	if(query.getSort().toLowerCase().contains("desc"))
            	{
            		query.setSort(query.getSort().replaceAll("name", "SAM.appModuleGroup desc,SAA.title"));
            	}
            	else 
            	{
            		query.setSort(query.getSort().replaceAll("name", "SAM.appModuleGroup asc,SAA.title"));
            	}
            }
            
        }
        
        //search filter
        String searchQry = query.getFilterWhereClause();
        
        searchQry = searchQry.replace("(title)", "(SAM.title)");
        searchQry = searchQry.replace("(name)", "(SAM.name)");

        List<QueryFilter> queryFilterList = query.getFilterItems();
        for (QueryFilter filter : queryFilterList)
        {
            List<QueryParameter> queryParamList = filter.getParameters();
            for (QueryParameter queryParam : queryParamList)
            {
                String value = (String)queryParam.getValue();
                if (value.indexOf('(') != -1)
                {
                    value = value.substring(0, value.indexOf('('));
                    searchQry = searchQry.replaceAll(":" + queryParam.getName(), "'" + value.trim() + "'");
                }
            }
        }
        
        if (StringUtils.isNotBlank(searchQry))
        {
            hql.append(" and ").append(searchQry);
        }
        
        if (StringUtils.isNotBlank(query.getSort()))
        {
            String sort = query.getSort().substring(1, query.getSort().length() - 1);
            try
            {
                Map<String, String> sortMap = new ObjectMapper().readValue(sort, Map.class);
                hql.append (" order by ").append(sortMap.get("property")).append(" ").append(sortMap.get("direction"));
            }
            catch (Exception e)
            {
                Log.log.error("Could not parse sort json query for Menu Item", e);
            }
        }
        
        query.setHql(hql.toString());
        
        int start = query.getStart();
        int limit = query.getLimit();
        
        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
                // Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        SysAppModuleVO instanceVO = new SysAppModuleVO();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value =  record[x];
                            
                            PropertyUtils.setProperty(instanceVO, column, value);
                        }
                        if (columns.length > 1)
                        {
                            instanceVO.setName((StringUtils.isBlank(instanceVO.getAppModuleGroup()) ? 
                            					"" : instanceVO.getAppModuleGroup() + "::") + (String)record[4]);
                        }
                        instanceVO.setId(instanceVO.getSys_id());
                        result.add(instanceVO);
                    }//end of for loop
                }
            }//end of if
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sqk:" + hql.toString(), e);
        }
        
        return result;
    }
    
    
    private static MenuSetModelVO getMenuSetByID(String menuSetID)
    {
        List<Map<String, String>> applicationsList = new ArrayList<Map<String,String>>();
        List<Map<String, String>> rolesList = new ArrayList<Map<String,String>>();
        MenuSetModelVO menuSet = null;
        List<String> appNames = new ArrayList<String>();
        List<String> roleNames = new ArrayList<String>();

        try
        {
            if (StringUtils.isNotBlank(menuSetID))
            {
              HibernateProxy.setCurrentUser("system");
            	menuSet = (MenuSetModelVO) HibernateProxy.execute(() -> {
                    SysPerspectiveDAO sysPerspectiveDAO = HibernateUtil.getDAOFactory().getSysPerspectiveDAO();
                    SysPerspectiverolesDAO sysPerspectiverolesDAO = HibernateUtil.getDAOFactory().getSysPerspectiverolesDAO();
                    SysPerspectiveapplicationsDAO sysPerspectiveapplicationsDAO = HibernateUtil.getDAOFactory().getSysPerspectiveapplicationsDAO();

                    SysPerspective sysPerspective = sysPerspectiveDAO.findById(menuSetID);

                    List<SysPerspectiveroles> roles = (List<SysPerspectiveroles>) sysPerspective.getSysPerspectiveroles();

                    Map<String, String> roleMap = null;

                    List<String> allAvailableRoleNames = getAllAvailableRoleNames();

                    for (SysPerspectiveroles role : roles)
                    {
                        if (allAvailableRoleNames.contains(role.getValue()))
                        {
                            roleMap = new HashMap<String, String>();
                            roleMap.put("sys_id", role.getSys_id());
                            roleMap.put("value", role.getValue());
                            roleMap.put("sequence", role.getSequence().toString());
                            rolesList.add(roleMap);
                            roleNames.add(role.getValue());
                        }
                        else
                        {
                            sysPerspectiverolesDAO.delete(role);
                        }
                    }

                    List<SysPerspectiveapplications> apps = (List<SysPerspectiveapplications>) sysPerspective.getSysPerspectiveapplications();

                    Map<String, String> applicationMap = null;
                    List<Map<String, String>> allApplications = getMenuApplications();
                    List<String> allAvailableAppNames = getAllAvailableAppNames();

                    for (SysPerspectiveapplications app : apps)
                    {
                        if (allAvailableAppNames.contains(app.getValue()))
                        {
                            applicationMap = new HashMap<String, String>();
                            applicationMap.put("sys_id", app.getSys_id());
                            applicationMap.put("value", app.getValue());
                            applicationMap.put("sequence", app.getSequence().toString());
                            applicationMap.put("displayName", getMenuSectionDisplayNameByID(allApplications, app.getValue()));
                            applicationsList.add(applicationMap);
                            appNames.add(app.getValue());
                        }
                        else
                        {
                            sysPerspectiveapplicationsDAO.delete(app);
                        }
                    }

                    MenuSetModelVO result = new MenuSetModelVO(sysPerspective.getName(), "", "", sysPerspective.getSys_id());
                    result.setActive(sysPerspective.getActive());
                    
                    return result;                   
            	});

            }
            else
            {
                menuSet = new MenuSetModelVO();
            }

            menuSet.setMenuSetApplicationsList(sortList(applicationsList));
            menuSet.setMenuSetRolesList(sortList(rolesList));
            menuSet.setAvailableApplicationsList(getAvailableApplications(appNames));
            menuSet.setAvailableRolesList(getAvailableRoles(roleNames));

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return menuSet;

    } // getMenuSetByID
    
    
    @SuppressWarnings("unchecked")
	private static List<Map<String, String>> getMenuApplications()
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (List<Map<String, String>>) HibernateProxy.execute(() -> {
        		
        		 List<SysAppApplication> sysAppApplicationList = new ArrayList<SysAppApplication>();
        	        List<Map<String, String>> appList = new ArrayList<Map<String, String>>();
        	        Map<String, String> appMap = null;
        		
                SysAppApplicationDAO sysAppApplicationDAO = HibernateUtil.getDAOFactory().getSysAppApplicationDAO();
                sysAppApplicationList = sysAppApplicationDAO.findAll();

                for (SysAppApplication sysAppApplication : sysAppApplicationList)
                {
                    appMap = new HashMap<String, String>();
                    appMap.put("name", sysAppApplication.getName());
                    appMap.put("title", sysAppApplication.getTitle());
                    appMap.put("displayName", sysAppApplication.getTitle());
                    appMap.put("active", sysAppApplication.getActive().toString());
                    appMap.put("sequence", (sysAppApplication.getOrder() == null) ? "0.00" : (sysAppApplication.getOrder().toString()));
                    appMap.put("color", sysAppApplication.getColor());
                    appMap.put("textColor", sysAppApplication.getTextColor());
                    appList.add(appMap);
                }
                
                return appList;

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return new ArrayList<Map<String, String>>();

    } // getApplications
    
    @SuppressWarnings("unchecked")
	private static List<Map<String, String>> getRoles()
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
            return (List<Map<String, String>>) HibernateProxy.execute(() -> {
            	List<Roles> roles = new ArrayList<Roles>();
                List<Map<String, String>> rolesList = new ArrayList<Map<String, String>>();
                Map<String, String> roleMap = null;
            	
            	 RolesDAO rolesDAO = HibernateUtil.getDAOFactory().getRolesDAO();
                 roles = rolesDAO.findAll();

                 for (Roles role : roles)
                 {
                     roleMap = new HashMap<String, String>();
                     roleMap.put("name", role.getUName());
                     rolesList.add(roleMap);
                 }
                 
                 return rolesList;

            });

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
            return new ArrayList<Map<String, String>>();
        }

        
    } // getRoles
    private static boolean getMenuSectionActivityByName(List<Map<String, String>> menuSections, String name)
    {
        boolean active = false;

        try
        {
            for (Map<String, String> menuSection : menuSections)
            {
                String nameLocal = menuSection.get("name");

                if (nameLocal != null)
                {
                    if (nameLocal.equals(name))
                    {
                        active = Boolean.valueOf(menuSection.get("active")).booleanValue();
                        break;
                    }
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return active;
    } // getMenuSectionActivityByName
    
    private static String getMenuSectionDisplayNameByID(List<Map<String, String>> menuSections, String key)
    {
        String menuSectionDisplatyName = "";

        try
        {
            for (Map<String, String> menuSection : menuSections)
            {
                String name = menuSection.get("name");

                if (name != null)
                {
                    if (name.equals(key))
                    {
                        menuSectionDisplatyName = menuSection.get("displayName");
                        break;
                    }
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return menuSectionDisplatyName;
    } // getMenuSectionDisplayNameByID
    
    /**
     * This method return menu set id.
     * 
     * @param menuSetName
     * @return
     */
    private static String getMenuSetID(String menuSetName)
    {
        String menuSetID = "";

        try
        {
            SysPerspectiveDAO sysPerspectiveDAO = HibernateUtil.getDAOFactory().getSysPerspectiveDAO();

            SysPerspective sysPerspective = new SysPerspective();
            sysPerspective.setName(menuSetName);

            List<SysPerspective> sysPerspectiveList = sysPerspectiveDAO.find(sysPerspective);

            if (sysPerspectiveList != null && !sysPerspectiveList.isEmpty())
            {
                sysPerspective = sysPerspectiveList.get(0);
                menuSetID = sysPerspective.getSys_id();
            }

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return menuSetID;
    } // getMenuSetID
    
    private static List<Map<String, String>> changeSequenceBasedOnMenuDefinition(List<Map<String, String>> menuSesionList)
    {
        List<Map<String, String>> updatedMenuSectionList = new ArrayList<Map<String,String>>();

        if (menuSesionList != null && !menuSesionList.isEmpty())
        {
            List<Map<String, String>> allMenuSections = getMenuApplications();

            for (Map<String, String> menuSection : menuSesionList)
            {
//                String currentSequence = menuSection.get("sequence");
                String currentDisplayName = menuSection.get("displayName");

                for (Map<String, String> application : allMenuSections)
                {
                    String sequence = application.get("sequence");
                    String displayName = application.get("displayName");

                    if (currentDisplayName != null && displayName != null && currentDisplayName.equals(displayName))
                    {
                        menuSection.put("sequence", sequence);
                        break;
                    }
                }

                updatedMenuSectionList.add(menuSection);
            }
        }

        return updatedMenuSectionList;

    } // changeSequenceBasedOnMenuDefinition

    protected static List<Map<String, String>> sortList(List<Map<String, String>> list)
    {
        if (list != null && !list.isEmpty())
        {
            Collections.sort(list, new Comparator<Map<String, String>>()
            {
                public int compare(Map<String, String> p1, Map<String, String> p2)
                {
                    int sequence1 = new Integer(p1.get("sequence")).intValue();
                    int sequence2 = new Integer(p2.get("sequence")).intValue();
                    return compareInt(sequence1, sequence2);
                }
            });
        }

        return list;
    } // sortList
    
    private static List<Map<String, String>> sortApplications(List<Map<String, String>> applicaitons)
    {
        if (applicaitons != null && !applicaitons.isEmpty())
        {
            Collections.sort(applicaitons, new Comparator<Map<String, String>>()
            {
                public int compare(Map<String, String> p1, Map<String, String> p2)
                {
                    int sequence1 = new BigDecimal(p1.get("sequence")).intValue();
                    int sequence2 = new BigDecimal(p2.get("sequence")).intValue();
                    return compareInt(sequence1, sequence2);
                }
            });
        }

        return applicaitons;
    } // sortApplications
    
    public static int compareInt(int sequence1, int sequence2)
    {
        if(sequence1 == 0 || sequence2 == 0)
        {
            return -1;
        }
        
        if(sequence1 == sequence2)
        {
            return 0;
        }
        else if(sequence1 > sequence2)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    
    private static List<Map<String, String>> getAvailableRoles(List<String> roles)
    {
        List<Map<String, String>> availableRoles = new ArrayList<Map<String,String>>();
        List<Map<String, String>> allRoles = getRoles();

        if (!roles.isEmpty())
        {
            for (Map<String, String> roleMap : allRoles)
            {
                String roleName = roleMap.get("name");

                if (!roles.contains(roleName))
                {
                    availableRoles.add(roleMap);
                }
            }

            return availableRoles;
        }
        else
        {
            return allRoles;
        }
    } // getAvailableRoles
    private static boolean checkMenuSectionRole(String userName, String menuSectionName)
    {
        boolean result = false;

        if (userName != null && menuSectionName != null)
        {
            // special case
            if (menuSectionName.equalsIgnoreCase("sysmaint"))
            {
                if (userName.equalsIgnoreCase("resolve.maint"))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                // get menu section
                SysAppApplication section = new SysAppApplication();
                section.setName(menuSectionName);

                section = HibernateUtil.getDAOFactory().getSysAppApplicationDAO().findFirst(section);
                if (section != null)
                {
                    Collection<SysAppApplicationroles> appRoles = section.getSysAppApplicationroles();
                    if (appRoles != null && appRoles.size() > 0)
                    {
                        Set<String> roles = new HashSet<String>();
                        for (SysAppApplicationroles role : appRoles)
                        {
                            roles.add(role.getValue());
                        }

                        result = UserUtils.hasRole(userName, roles);
                    }
                    else
                    {
                        // default empty roles is true
                        result = true;
                    }
                }
            }
        }
        return result;
    } // checkMenuSectionRole
    
    protected static List<String> getAllAvailableRoleNames()
    {
        List<String> allAvailableRoleNames = new ArrayList<String>();
        List<Map<String, String>> allRoles = getRoles();

        if (!allRoles.isEmpty())
        {
            for (Map<String, String> roleMap : allRoles)
            {
                String roleName = roleMap.get("name");
                allAvailableRoleNames.add(roleName);
            }
        }

        return allAvailableRoleNames;
    } // getAllAvailableRoleNames
    
    /**
     * This method return all available application names.
     * 
     * @return
     */
    private static List<String> getAllAvailableAppNames()
    {
        List<String> allAvailableAppNames = new ArrayList<String>();
        List<Map<String, String>> allApps = getMenuApplications();

        if (!allApps.isEmpty())
        {
            for (Map<String, String> appMap : allApps)
            {
                String appName = appMap.get("name");
                allAvailableAppNames.add(appName);
            }
        }

        return allAvailableAppNames;
    } // getAllAvailableAppNames

    private static List<Map<String, String>> getAvailableApplications(List<String> appNames)
    {
        List<Map<String, String>> availableApps = new ArrayList<Map<String,String>>();
        List<Map<String, String>> allApplications = getMenuApplications();

        if (!appNames.isEmpty())
        {
            for (Map<String, String> appMap : allApplications)
            {
                String appName = appMap.get("name");

                if (!appNames.contains(appName))
                {
                    availableApps.add(appMap);
                }
            }

            return availableApps;
        }
        else
        {
            return allApplications;
        }
    } // getAvailableApplications
    
    
    private static List<Map<String, String>> sortMenuItems(List<Map<String, String>> menuItems)
    {
        if (menuItems != null && !menuItems.isEmpty())
        {
            Collections.sort(menuItems, new Comparator<Map<String, String>>()
            {
                public int compare(Map<String, String> p1, Map<String, String> p2)
                {
                    int sequence1 = new BigDecimal(p1.get("order")).intValue();
                    int sequence2 = new BigDecimal(p2.get("order")).intValue();
                    return compareInt(sequence1, sequence2);
                }
            });
        }

        return menuItems;
    } // sortMenuItems
    
    protected static List<Map<String, String>> getNeedSavedSelectedMenuRoles(List<Map<String, String>> selectedRoles, List<String> nameOfRolesNeedRemovedFromSelection)
    {
        List<Map<String, String>> needSavedRoles = new ArrayList<Map<String,String>>();

        if (nameOfRolesNeedRemovedFromSelection != null && !nameOfRolesNeedRemovedFromSelection.isEmpty())
        {
            for (Map<String, String> role : selectedRoles)
            {
                String roleValueFromSelection = role.get("value");

                if (!nameOfRolesNeedRemovedFromSelection.contains(roleValueFromSelection))
                {
                    needSavedRoles.add(role);
                }
            }

            return needSavedRoles;
        }
        else
        {
            return selectedRoles;
        }
    } // getNeedSavedSelectedMenuRoles

    private static String getModifyURL(String url)
    {
        String result = url;

        if (!url.contains("http://") && !url.contains("https://"))
        {
            if ((url.length() > 3) && (url.substring(0, 3)).equals("www"))
            {
                result = "http://" + url;
            }
            else if (url.startsWith("/"))
            {
                // url = href.substring(0, href.indexOf("/resolve")) + url;
            }
            else if (url.contains(".jsp"))
            {
                result = "/resolve/jsp/" + url;
            }
            else if (!url.contains("/") && !url.contains(".jsp") && url.contains("."))
            {
                result = "/resolve/jsp/rswiki.jsp?wiki=" + url;
            }
        }

        return result;
    }
    
    private static MenuDTO getMenuFromCache(String username)
    {
        MenuDTO menu = null;
        
        if (MenuCache.getInstance().isKeyInCache(username))
        {
            menu = (MenuDTO) MenuCache.getInstance().getCacheObj(username);
        }
        
        return menu;
    }
    
    private static void addMenuToCache(String username, MenuDTO menu)
    {
        if(StringUtils.isNotBlank(username) && menu != null)
        {
            //add it to cache
            MenuCache.getInstance().putCacheObj(username, menu);
        }
    }
    
    public static void invalidateMenuCache()
    {
        MenuCache.getInstance().invalidate();
    }
    
    private static MenuDTO getMenuFromDB(String username)
    {
        MenuDTO menu = new MenuDTO();
        long startTimeInstance = System.currentTimeMillis();
        
        MenuService menuService = new MenuService();
        List<MenuSetModel> menuSets = menuService.getMenuSets(username, true);
        menu.setSets(menuSets);
        
        Log.log.trace("Time taken for getting menu sets:" + (System.currentTimeMillis() - startTimeInstance));
        startTimeInstance = System.currentTimeMillis();


        List<Map<String, String>> menuSections = menuService.getMenuSections(username, "All", "");
        
        Log.log.trace("Time taken for getting menu sections:" + (System.currentTimeMillis() - startTimeInstance));
        startTimeInstance = System.currentTimeMillis();

        for (Map<String, String> section : menuSections)
        {
            MenuItemDTO menuSection = new MenuItemDTO();
            menuSection.fromSection(section);
            for (MenuSetModel menuSet : menuSets)
            {
                if (menuSet.getApplications().contains(menuSection.getText()))
                {
                    menuSection.addSet(menuSet);
                }
            }
            List<Map<String, String>> menuItemsList = getMenuItems(username, section.get("value"));
            Log.log.trace("Time taken for getting menu Items list:" + (System.currentTimeMillis() - startTimeInstance));
            startTimeInstance = System.currentTimeMillis();

            Set<String> views = null;//cache this for all the links
            for (Map<String, String> menuItem : menuItemsList)
            {
                String query = menuItem.get("query");
                if (StringUtils.isNotEmpty(query) && query.contains("${USERID}"))
                {
                    query = query.replace("${USERID}", username);
                }

                if (StringUtils.isNotEmpty(query) && query.contains("${view_lookup:cr}"))
                {
                    try
                    {
                        //will be called only once
                        if (views == null)
                        {
                            Map<String, String> viewsMap = ServiceHibernate.findMetaTableViewForUser("content_request", username, RightTypeEnum.view);
                            views = new HashSet<String>(viewsMap.values());

                            Log.log.trace("Time taken for getting views for CR:" + (System.currentTimeMillis() - startTimeInstance));
                            startTimeInstance = System.currentTimeMillis();
                        }

                        //if the list does not have Default view, get the first one from the list.
                        String crViewName = "Default";
                        if (views.size() > 0 && !views.contains(crViewName))
                        {
                            crViewName = views.iterator().next();
                        }

                        query = query.replace("${view_lookup:cr}", crViewName);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error while getting list of cr views for the user:" + username, e);
                    }    
                }
                
                
                if (StringUtils.isNotEmpty(query))
                {
                    query = getModifyURL(query);
                    menuItem.put("query", query);
                }

                menuItem.put("query", query);
                MenuItemDTO item = new MenuItemDTO();
                item.fromMap(menuItem);
                menuSection.addItem(item);
            }
            menu.addToSections(menuSection);
        }
        
        return menu;        
    }
    
}
