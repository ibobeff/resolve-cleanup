package com.resolve.services.validator.sequence;

import javax.validation.GroupSequence;

@GroupSequence(value = { Level1.class, Level2.class, Level3.class, Level4.class })
public interface ValidationSequence {

}
