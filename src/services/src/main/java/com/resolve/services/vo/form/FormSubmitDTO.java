/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

/**
 * DTO used when a RENDERED CUSTOM FORM is submitted
 * 
 * @author jeet.marwah
 *
 */
public class FormSubmitDTO
{
    // form related details
    private String viewName;
    private String formSysId;
    private String tableName;

    private String userId;

    // represents data in a single row of the main table
    private FormData dbRowData;

    // represents data on the form that are not mapped to table
    private FormData sourceRowData;

    // additional params
    private String timezone;
    private String crudAction;
    private String query;
    
    private String controlItemSysId; // button id that is clicked
    private String controlItemName;//used to simulate the click from the actiontask or ESB
    
    private String currentDocumentName;

    public FormSubmitDTO()
    {
    }

    public String getViewName()
    {
        return viewName;
    }

    public void setViewName(String formName)
    {
        this.viewName = formName;
    }

    public String getFormSysId()
    {
        return formSysId;
    }

    public void setFormSysId(String formSysId)
    {
        this.formSysId = formSysId;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public FormData getDbRowData()
    {
        return dbRowData;
    }

    public void setDbRowData(FormData dbRowData)
    {
        this.dbRowData = dbRowData;
    }

    public FormData getSourceRowData()
    {
        return sourceRowData;
    }

    public void setSourceRowData(FormData sourceRowData)
    {
        this.sourceRowData = sourceRowData;
    }

    public String getTimezone()
    {
        return timezone;
    }

    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    public String getCrudAction()
    {
        return crudAction;
    }

    public void setCrudAction(String crudAction)
    {
        this.crudAction = crudAction;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getControlItemSysId()
    {
        return controlItemSysId;
    }

    public void setControlItemSysId(String controlItemSysId)
    {
        this.controlItemSysId = controlItemSysId;
    }

    public String getControlItemName()
    {
        return controlItemName;
    }

    public void setControlItemName(String controlItemName)
    {
        this.controlItemName = controlItemName;
    }

    public String getCurrentDocumentName()
    {
        return currentDocumentName;
    }

    public void setCurrentDocumentName(String currentDocumentName)
    {
        this.currentDocumentName = currentDocumentName;
    }
    
    

}
