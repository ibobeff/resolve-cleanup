/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

public class Node
{
    public int id;
    public int x;
    public int y;
    public String merge;
    public Edge previousEdge;
    public Edge nextEdge;
    
    public Node(int id, int x, int y, String merge)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.merge = merge;
    }
    
    public Node(int id, int x, int y, String merge, Edge previousEdge, Edge nextEdge)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.merge = merge;
        this.previousEdge = previousEdge;
        this.nextEdge = nextEdge;
    }
}

