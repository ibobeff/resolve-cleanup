/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component.container;

import java.util.Collection;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSContainer;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;


/**
 * Team is only a publisher . It is a CONTAINER
 * Team can belong to another team. there is no limit to the hierarchy
 * 
 * @author jeet.marwah
 *
 */
public class Team extends RSComponent implements RSContainer
{
    private static final long serialVersionUID = 4470537251710679674L;
    
//    public static final String TYPE = "team";

    private Collection<Team> teams = null;
    private Collection<User> users = null;
   
   //for UI processing 
    private User owner;
    private Process parentProcess;
    
    public Team()
    {
        super(NodeType.TEAM);
    }
    
    public Team(String sysId, String name)
    {
        super(NodeType.TEAM);
        setSys_id(sysId);
        setName(name);
    }
    
    public Team(String name)
    {
        super(NodeType.TEAM);
        setName(name);
    }
    
    public Team(ResolveNodeVO node) throws Exception
    {
        super(node);
    }
    
    public void setTeams(Collection<Team> teams)
    {
        this.teams = teams;
    }
    
    public Collection<Team> getTeams()
    {
        return teams;
    }
    
    public void setUsers(Collection<User> users)
    {
        this.users = users;
    }
    
    public Collection<User> getUsers()
    {
        return this.users;
    }
    
    public User getOwner()
    {
        return owner;
    }

    public void setOwner(User owner)
    {
        this.owner = owner;
    }
    
    public Process getParentProcess()
    {
        return parentProcess;
    }

    public void setParentProcess(Process parentProcess)
    {
        this.parentProcess = parentProcess;
    }  
    
}
