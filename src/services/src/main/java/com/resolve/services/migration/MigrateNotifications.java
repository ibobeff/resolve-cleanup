/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.persistence.model.ResolveNode;
import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.SocialNotificationUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;
import com.resolve.services.migration.social.NonNeo4jRelationType;
import com.resolve.services.migration.social.UserNotificationDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * reads data from Neo4j and populates the notification table 
 * 
 * @author jeet.marwah
 *
 */
public class MigrateNotifications
{
    private List<UserNotificationDTO> notificationsFromNeo4j = new ArrayList<UserNotificationDTO>();
    
    public static void migrate()
    {
        MigrateNotifications mn = new MigrateNotifications();
        mn.migrateInternal();
    }
    
    private void migrateInternal()
    {
        //read all the notifications from Neo4j 
        getNotificationsFromNeo4j();
        
        //persist it in Sql DB
        if(notificationsFromNeo4j.size() > 0)
        {
            for(UserNotificationDTO notiRel : notificationsFromNeo4j)
            {
                persistNotification(notiRel);
            }
        }
        
    }
    
    //read the graph Neo4j Db and prepare the data struture
    private void getNotificationsFromNeo4j()
    {
        //get all the User nodes
        try
        {
            Set<Node> neo4jUserNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.USERS);
            if(neo4jUserNodes != null && neo4jUserNodes.size() > 0)
            {
                for(Node userNode : neo4jUserNodes)
                {
                    try
                    {
                        processUserNode(userNode);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error in processing Usernode:" + userNode.getProperty(GraphDBManager.DISPLAYNAME), e);
                    }
                }
            }
        }
        catch(Exception e)
        {
            Log.log.error("Error in migrating notifications", e);
        }
        
    }
    
    private void processUserNode(Node userNode)
    {
        if(userNode.hasProperty(GraphDBManager.TYPE))
        {
            Log.log.debug("Processing Notifications for User:" + userNode.getProperty(GraphDBManager.DISPLAYNAME));
            String username = (String) userNode.getProperty("username");
            if(username.equalsIgnoreCase("resolve.maint"))
                return;
            
            //get the global setting that we have set on the Users node
            String systemDigestEmailVal = userNode.hasProperty("systemDigestEmail") ? ((Boolean) userNode.getProperty("systemDigestEmail")).toString() : "";//UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL
            String systemForwardEmailVal = userNode.hasProperty("systemForwardEmail") ? ((Boolean) userNode.getProperty("systemForwardEmail")).toString() : ""; //UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL
            String inboxDigestEmailVal = userNode.hasProperty("inboxDigestEmail") ? ((Boolean) userNode.getProperty("inboxDigestEmail")).toString() : ""; //UserGlobalNotificationContainerType.USER_DIGEST_EMAIL
            String inboxForwardEmailVal = userNode.hasProperty("inboxForwardEmail") ? ((Boolean) userNode.getProperty("inboxForwardEmail")).toString() : ""; //UserGlobalNotificationContainerType.USER_FORWARD_EMAIL
            String allDigestEmailVal = userNode.hasProperty("allDigestEmail") ? ((Boolean) userNode.getProperty("allDigestEmail")).toString() : ""; //UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL
            String allForwardEmailVal = userNode.hasProperty("allForwardEmail") ? ((Boolean) userNode.getProperty("allForwardEmail")).toString() : ""; //UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL
            
            //prepare the DTOs
            
            if(!StringUtils.isEmpty(systemDigestEmailVal))
            {
                prepareNotificationDTO(username, null, UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL, systemDigestEmailVal);
            }
            
            if(!StringUtils.isEmpty(systemForwardEmailVal))
            {
                prepareNotificationDTO(username, null, UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL, systemForwardEmailVal);
            }
            
            if(!StringUtils.isEmpty(inboxDigestEmailVal))
            {
                prepareNotificationDTO(username, null, UserGlobalNotificationContainerType.USER_DIGEST_EMAIL, inboxDigestEmailVal);
            }
            
            if(!StringUtils.isEmpty(inboxForwardEmailVal))
            {
                prepareNotificationDTO(username, null, UserGlobalNotificationContainerType.USER_FORWARD_EMAIL, inboxForwardEmailVal);
            }
            
            if(!StringUtils.isEmpty(allDigestEmailVal))
            {
                prepareNotificationDTO(username, null, UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL, allDigestEmailVal);
            }
            
            if(!StringUtils.isEmpty(allForwardEmailVal))
            {
                prepareNotificationDTO(username, null, UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL, allForwardEmailVal);
            }
            
            //now for the specific ones
            //get all the rels for the tag
            Set<Relationship> rels = new HashSet<Relationship>();
            
            Iterable<Relationship> outrels = userNode.getRelationships(Direction.OUTGOING);
            
            for(Relationship rel : outrels)
            {
                if(!rel.getType().name().equalsIgnoreCase(NonNeo4jRelationType.FOLLOWER.name()) &&
                   !rel.getType().name().equalsIgnoreCase(NonNeo4jRelationType.MEMBER.name()))
                {
                    Node othernode = rel.getOtherNode(userNode);
                    
                    if(othernode.hasProperty(GraphDBManager.TYPE))
                    {
                        String relName = rel.getType().name(); //this will be one of the UserGlobalNotificationContainerType string
                        try
                        {
                            UserGlobalNotificationContainerType.valueOf(relName);
                            rels.add(rel);
                        }
                        catch(Throwable t)
                        {
                            //this may not be of any type, so ignore it and go to another rel
                            continue;
                        }
                    }
                }
            }
            
            Log.log.debug("RS Component Notifications : " + rels.size());
            
            //Get relationships from User Global Notification Container Type nodes
            Iterable<Relationship> inrels = userNode.getRelationships(Direction.INCOMING);
            
            for(Relationship rel : inrels)
            {
                Node othernode = rel.getOtherNode(userNode);
                
                if(othernode.hasProperty(GraphDBManager.TYPE) && 
                   ((String)othernode.getProperty(GraphDBManager.TYPE)).equalsIgnoreCase(rel.getType().name()))
                {
                    String relName = rel.getType().name(); //this will be one of the UserGlobalNotificationContainerType string
                    try
                    {
                        UserGlobalNotificationContainerType.valueOf(relName);
                        rels.add(rel);
                    }
                    catch(Throwable t)
                    {
                        //this may not be of any type, so ignore it and go to another rel
                        continue;
                    }
                }
            }
            
            Log.log.debug("Total (RS Component + Global) Notifications : " + rels.size());
            
            for(Relationship rel : rels)
            {
                Node othernode = rel.getOtherNode(userNode);
                //get the value set
                String value = rel.hasProperty("NOTIFICATIONVALUE") && ((String) rel.getProperty("NOTIFICATIONVALUE")).equalsIgnoreCase("true") ? "true" : "false";
                    
                //prepare the dtos
                prepareNotificationDTO(username, othernode, UserGlobalNotificationContainerType.valueOf(rel.getType().name()), value);
            }//end of for loop
        }
    }
    
    private void prepareNotificationDTO(String username, Node compNode, UserGlobalNotificationContainerType notificationType, String notificationValue)
    {
        UserNotificationDTO dto = new UserNotificationDTO();
        dto.setUsername(username);
        
        //node
        if(compNode != null)
        {
            String nodeType = null;
            try
            {
                nodeType = (String) compNode.getProperty(GraphDBManager.TYPE);
                NodeType compType = NodeType.valueOf(nodeType);
                
                String compName = (String) compNode.getProperty(GraphDBManager.DISPLAYNAME);
                if(compType == NodeType.USER)
                {
                    compName = (String) compNode.getProperty("username");
                }
                
                dto.setCompName(compName);
                dto.setCompType(compType);
                
            }
            catch(Exception e)
            {
                //don't do anything as the other node can be on Notificaition type which has no value
                Log.log.trace("Error setting component name " + (compNode.hasProperty(GraphDBManager.DISPLAYNAME) ? compNode.getProperty(GraphDBManager.DISPLAYNAME) + " " : "")+ "and type " + nodeType + " for user " + username);
            }
        }
        
        dto.setNotificationType(notificationType);
        dto.setNotificationValue(notificationValue);
        
        this.notificationsFromNeo4j.add(dto);
    }
    
    private void persistNotification(UserNotificationDTO notification)
    {
        if(notification != null && notification.validate())
        {
            ResolveNodeVO nodeVO = null;
            ResolveNode node = null;
            try
            {
                if(StringUtils.isNotEmpty(notification.getCompName()) && notification.getCompType() != null)
                {
                    nodeVO = ServiceGraph.findNode(null, null, notification.getCompName(), notification.getCompType(), "system");
                    if(nodeVO != null)
                    {
                        node = new ResolveNode(nodeVO);
                    }
                    else
                    {
                        /*
                         * Any Neo4j node not found in resolve_node is considered as Global Neo4j Node of types other than RS components.
                         * The type of such Global Neo4j components should match one of UserGlobalNotificationContainerTypes
                         * If not then Log the message throw the exception.   
                         */
                        Log.log.trace("User notification of type " + notification.getNotificationType() + " for user " +  notification.getUsername() + 
                                      " with component node of type " + notification.getCompType() + " and name " + notification.getCompName() + 
                                      " not found, isGlobal " + (notification.getNotificationType().name().equalsIgnoreCase(notification.getCompType().name())));
                    }
                }
                
                //persist it
                try
                {
                    SocialNotificationUtil.persistUserNotification(null, notification.getUsername(), notification.getNotificationType(), null, node, notification.getNotificationValue());
                }
                catch(Exception e)
                {
                    //Ignore "User <username> does not exist" exception in persisting user notification
                    if(!e.getMessage().startsWith("User") || !e.getMessage().endsWith("does not exist"))
                    {
                        throw e;
                    }
                }
            }
            catch (Throwable t)
            {
                Log.log.error("Error persisting notification:" + notification, t);
            }
        }
    }
}
