/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.dao.ResolveRegistrationPropertyDAO;
import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.model.ResolveRegistration;
import com.resolve.persistence.model.ResolveRegistrationProperty;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveRegistrationPropertyVO;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ResolveRegistrationUtil
{
    public static List<ResolveRegistrationVO> getResolveRegistration(QueryDTO query, String username)
    {
        List<ResolveRegistrationVO> result = new ArrayList<ResolveRegistrationVO>();

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if (data != null)
            {
                //grab the map of sysId-organizationname  
//                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                //get the Roles for all the organizations
//                Map<String, String> mapOfAppsAndRoles =  getMapOfAppsAndRoles();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveRegistration instance = new ResolveRegistration();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveRegistration instance = (ResolveRegistration) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }
    
    public static ResolveRegistrationVO findResolveRegistrationById(String sys_id, String username)
    {
        ResolveRegistration model = null;
        ResolveRegistrationVO result = null;

        if (!StringUtils.isEmpty(sys_id))
        {
            try
            { 
                model = findResolveRegistrationByIdPrivate(sys_id, username);
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }

        if (model != null)
        {
            result = model.doGetVO();
        }

        return result;
    }
    

    @SuppressWarnings("unchecked")
	public static List<ResolveRegistrationVO> findResolveRegistrationByStatus(String status)
    {
        List<ResolveRegistrationVO> results = new ArrayList<ResolveRegistrationVO>();
        List<ResolveRegistration> regs = new ArrayList<ResolveRegistration>();

        try
        {
        	regs = (List<ResolveRegistration>) HibernateProxy.execute(() -> {

            if (StringUtils.isNotEmpty(status))
            {
                ResolveRegistration qry = new ResolveRegistration();
                qry.setUStatus(status);

                return HibernateUtil.getDAOFactory().getResolveRegistrationDAO().find(qry);

            }
            else
            {
                // get everything
                return HibernateUtil.getDAOFactory().getResolveRegistrationDAO().findAll();
            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }
        
        if(regs != null)
        {
            for(ResolveRegistration reg : regs)
            {
                results.add(reg.doGetVO());
            }
        }

        return results;

    }
    
    public static ResolveRegistrationVO findResolveRegistrationByGuid(String guid)
    {
        ResolveRegistrationVO result = null;
        ResolveRegistration reg = null;
        try
        {
			reg = (ResolveRegistration) HibernateProxy.execute(() -> {
				if (StringUtils.isNotEmpty(guid)) {
					ResolveRegistration searchedRreg = new ResolveRegistration();
					searchedRreg.setUGuid(guid);

					return HibernateUtil.getDAOFactory().getResolveRegistrationDAO().findFirst(searchedRreg);
				} else {
					return null;
				}
			});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }
        
        if(reg != null)
        {
            result = reg.doGetVO();
        }
        
        return result;
    }
    
    public static void updateRegistrationProperties(String guid, Map<String, String> properties) throws Exception
    {
        try
        {
            if (properties != null)
            {                
                HibernateProxy.execute(() -> {
                	 ResolveRegistration reg = new ResolveRegistration();
                     reg.setUGuid(guid);
                     reg = HibernateUtil.getDAOFactory().getResolveRegistrationDAO().findFirst(reg);
                     if (reg != null)
                     {
                         String reg_sid = reg.getSys_id();
                             
                         // delete existing properties
                         //HibernateUtil.createQuery("delete ResolveRegistrationProperty r where r.URegistration = :regsid").setString("regsid", reg_sid).executeUpdate();
                         for (ResolveRegistrationProperty property : reg.getResolveRegistrationProperties())
                         {
                             HibernateUtil.delete("ResolveRegistrationProperty", property.getSys_id());
                         }
                         
                         // insert new properties
                         ResolveRegistrationPropertyDAO rrpDao = HibernateUtil.getDAOFactory().getResolveRegistrationPropertyDAO();
                         for (Iterator<Map.Entry<String, String>> i = properties.entrySet().iterator(); i.hasNext(); )
                         {
                             Map.Entry<String, String> entry = (Map.Entry<String, String>)i.next();
                             
                             String name = (String)entry.getKey();
                             String value = (String)entry.getValue();
                             
                             String type = "PLAIN";
                             if (name.startsWith(Constants.PROPERTY_ENCRYPT_PREFIX))
                             {
                                 type = "ENCRYPT";
                             }
                                 
                             ResolveRegistrationProperty row = new ResolveRegistrationProperty();
//                           row.setRegistration(HibernateUtil.getDAOFactory().getResolveRegistrationDAO().findById(reg_sid));
                             row.setRegistration(new ResolveRegistration().usetsys_id(reg_sid));
                             row.setUName(name);
                             row.setUType(type);
                             row.setUValue(value);
                             rrpDao.save(row);
                         }
                     }
                });                    
               
            }
            Log.log.debug("completed updateProperties");
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
    } // updateProperties

    public static ResolveRegistrationPropertyVO findResolveRegistrationProperty(String propertyName, String guid)
    {
        ResolveRegistrationPropertyVO result = null;
        ResolveRegistrationProperty prop = null;

        if (StringUtils.isNotBlank(propertyName) && StringUtils.isNotBlank(guid))
        {
            ResolveRegistrationProperty exp = new ResolveRegistrationProperty();
            exp.setRegistration(new ResolveRegistration().usetsys_id(guid));
            exp.setUName(propertyName);

            try
            {
            	prop = (ResolveRegistrationProperty) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getResolveRegistrationPropertyDAO().findFirst(exp);
            	});
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage());
                              HibernateUtil.rethrowNestedTransaction(e);
            }

        }

        if (prop != null)
        {
            result = prop.doGetVO();
        }

        return result;
    }

    public static void deleteResolveRegistrationByIds(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        if (sysIds != null)
        {
            //TODO: prepare the list of sys_ids to delete based on qry
            if (sysIds == null || sysIds.length == 0)
            {
                //prepare the sysIds with the where clause of this obj
                String selectQry = query.getSelectHQL();
                String sql = "select sys_id " + selectQry.substring(selectQry.indexOf("from"));
                System.out.println("ListRegistrationUtil: #111: Delete qry :" + sql);
            }

            //delete one by one
            for (String sysId : sysIds)
            {
                deleteResolveRegistrationById(sysId, username);
            }
           
        }
    }
    
    public static ResolveRegistrationVO saveResolveRegistration(ResolveRegistrationVO vo, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        if (vo != null)
        {
            ResolveRegistration model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findResolveRegistrationByIdPrivate(vo.getSys_id(), username);
            }
            else
            {
                model = new ResolveRegistration();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveResolveRegistration(model, username);
            vo = model.doGetVO();
        }
        
        return vo;
    }
    
    
    //private
    
    private static ResolveRegistration findResolveRegistrationByIdPrivate(String sys_id, String username)
    {
        ResolveRegistration result = null;

        if (StringUtils.isNotBlank(sys_id))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	result = (ResolveRegistration) HibernateProxy.execute(() -> {

            		ResolveRegistration res = HibernateUtil.getDAOFactory().getResolveRegistrationDAO().findById(sys_id);
            		 if(res != null && res.getResolveRegistrationProperties() != null)
                     {
                         res.getResolveRegistrationProperties().size();//load the properties
                     }
            		 
            		 return res;
            	});
            	
               
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return result;
    }

    private static void deleteResolveRegistrationById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	
	            ResolveRegistration model = HibernateUtil.getDAOFactory().getResolveRegistrationDAO().findById(sysId);
	            if (model != null)
	            {
	                Collection<ResolveRegistrationProperty> props = model.getResolveRegistrationProperties();
	                if(props != null)
	                {
	                    for(ResolveRegistrationProperty prop : props)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveRegistrationPropertyDAO().delete(prop);
	                    }
	                }
	                
	                HibernateUtil.getDAOFactory().getResolveRegistrationDAO().delete(model);
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static  Map<String, Object> findRegistrationPropertiesForLogging(String GUID) {
        List<ResolveRegistrationProperty> systemProperties = null;
        List<ResolveRegistrationProperty> envProperties = null;
        ResolveRegistrationPropertyVO resolveVersion = null;
        Map<String, Object> regPropertiesMap = new HashMap<>();
        
        ResolveRegistrationVO resolveReg = findResolveRegistrationByGuid(GUID);
        if (resolveReg != null) { 
            resolveVersion = findResolveRegistrationProperty("Resolve_Version", resolveReg.getSys_id());
            String systemHql = "from ResolveRegistrationProperty where UName like '%.%' AND registration.sys_id = :ID";
            String envHql = "from ResolveRegistrationProperty where UName not like '%.%' AND registration.sys_id = :ID";
            
            try {
                  HibernateProxy.setCurrentUser("system");
            	systemProperties = (List<ResolveRegistrationProperty>) HibernateProxy.execute(() -> {
	                return HibernateUtil.createHQLQuery(systemHql).setParameter("ID", resolveReg.getSys_id()).list();
            	});
            	
            	envProperties = (List<ResolveRegistrationProperty>) HibernateProxy.execute(() -> {
            		return HibernateUtil.createHQLQuery(envHql).setParameter("ID", resolveReg.getSys_id()).list();
                });
            } catch (Throwable t) {
                Log.log.error("Error while retrieving Registration Properties for logging", t);
                                                    HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        
        if (resolveVersion != null) {
            regPropertiesMap.put("VER", resolveVersion);
        }
        if (systemProperties != null) {
            regPropertiesMap.put("SYS", systemProperties);
        }
        if (envProperties != null) {
            regPropertiesMap.put("ENV", envProperties);
        }
        return regPropertiesMap;
    }
    
    public static Map<String, String> getHostInfo(String hostName, String username) {
        Map<String, String> hostInfoMap = new HashMap<>();
        
        ResolveRegistration registration = new ResolveRegistration();
        registration.setUStatus("ACTIVE");
        registration.setUName(hostName);
        
        ResolveRegistration registrationFinal = registration;
        
        try {
              HibernateProxy.setCurrentUser(username);
            registration = (ResolveRegistration) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getResolveRegistrationDAO().findFirst(registrationFinal);
            });
        } catch (Throwable t) {
            Log.log.error(String.format("Error while getting host information for the host name %s", hostName), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
            
        if (registration != null) {
            hostInfoMap.put("OS", registration.getOS());
            hostInfoMap.put("VERSION", registration.getUVersion());
            hostInfoMap.put("NAME", registration.getUName());
            hostInfoMap.put("IP", registration.getUIpaddress().split("/")[1]);
        }
        return hostInfoMap;
    }
}
