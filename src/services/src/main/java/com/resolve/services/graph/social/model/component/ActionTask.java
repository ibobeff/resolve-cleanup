/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;


/**
 * ActionTask is only a publisher  
 * 
 * @author jeet.marwah
 *
 */
public class ActionTask extends RSComponent implements RSPublisher
{
    private static final long serialVersionUID = -849680733435054494L;
    
    public static final String TYPE = "actiontask";
    
    public ActionTask()
    {
        super(NodeType.ACTIONTASK);
    }
    
    public ActionTask(String sysId, String name) 
    {
        super(NodeType.ACTIONTASK);
        setSys_id(sysId);
        setName(name);
    }

    
    public ActionTask(String name)
    {
        super(NodeType.ACTIONTASK);
        setName(name);
    }
    
    public ActionTask(ResolveNodeVO node) throws Exception
    {
        super(node);
    }
    
}
