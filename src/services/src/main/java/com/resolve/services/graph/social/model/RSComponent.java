/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.graph.social.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.services.constants.ServiceSocialConstants;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Component representing the social entity like Process, Team, etc
 * 
 * @author jeet.marwah
 *
 */
public class RSComponent implements Serializable//extends VO implements Comparable<RSComponent>
{
    private static final long serialVersionUID = -2649543819459826422L;

    private String id;
    private String sys_id;
    private String displayName;// Only used for UI 
    private String name; //display name
    private String compName; //most of the time will be same as 'name' but exceptions like User, where this is the 'username' and 'name' is the display name
    private NodeType type;
    private boolean lock = false;
    private boolean pinned = false;
    private boolean markDeleted;
    
    private long sys_created_on;
    private Date sysCreatedOn;
    private long sys_updated_on;
    private Date sysUpdatedOn;
//    private long lastActivityOn; //NOT USED AT THE MOMENT ANYWHERE IN THE CODE
    private String sys_created_by;
    private String sys_updated_by;
    
    private Map<String, String> properties;


    //attributes for UI
    private boolean isDirectFollow;
    private long unReadCount;
    private boolean isCurrentUser;
    private boolean isFollowing = true;
//    private boolean runbook;
//    private boolean decisionTree;
    //this is a flag that simulates soft delete. Do not return this component or use it for traversing if this is true
    //example would be of WikiDocument. If its marked 'deleted', than filter out the Post or this component

    private boolean inboxForwardEmail;
    private boolean inboxDigestEmail;
    private boolean systemForwardEmail;
    private boolean systemDigestEmail;
    private boolean allForwardEmail;
    private boolean allDigestEmail;
    
    //TODO
    protected String roles = "";
    protected String editRoles = "";
    protected String postRoles = "";
    
    protected ComponentNotification componentNotification;
    
    public RSComponent(NodeType type)
    {
        if(type == null)
            throw new RuntimeException("NodeType is mandatory");
        
        this.type = type;
    }
    
    public RSComponent(ResolveNodeVO node) throws Exception
    {
        if(node == null)
            throw new Exception("Node obj is null.");
        
        this.setId(node.getUCompSysId());
        this.setSys_id(node.getUCompSysId());
        this.setName(node.getUCompName());
        this.setDisplayName(node.getDisplayName());
        this.setCompName(node.getUCompName());
        this.setType(node.getUType());
        this.setLock(node.ugetULock());
        this.setPinned(node.ugetUPinned());
        this.setMarkDeleted(node.getUMarkDeleted());
        this.setRoles(node.getUReadRoles());
        this.setEditRoles(node.getUEditRoles());
        this.setPostRoles(node.getUPostRoles());

        this.setSys_created_by(node.getSysCreatedBy());        
        this.setSys_created_on(node.getSysCreatedOn() != null ? node.getSysCreatedOn().getTime() : 0l);
        this.setSys_updated_by(node.getSysUpdatedBy());
        this.setSys_updated_on(node.getSysUpdatedOn() != null ? node.getSysUpdatedOn().getTime() : 0l);
        this.setSysCreatedOn(node.getSysCreatedOn());
        this.setSysUpdatedOn(node.getSysUpdatedOn());
        
        if (node.getSysCreatedOn() == null || node.getSysUpdatedOn() == null)
        {
            Log.log.warn("ResolveNode " + node + " has null sysCreatedOn and/or sysUpdatedOn");
        }
        
        //get the props for this node
        if(node.getProperties() != null && node.getProperties().size() > 0)
        {
            properties = new HashMap<String, String>();
            for(ResolveNodePropertiesVO prop : node.getProperties())
            {
                properties.put(prop.getUKey(), prop.getUValue());
            }
        }
    }
    
    public ResolveNodeVO prepareNode()
    {
        ResolveNodeVO node = new ResolveNodeVO();
        
        node.setUCompSysId(this.getSys_id());
        node.setUCompName(StringUtils.isNotEmpty(this.getName()) ? this.getName() : this.getDisplayName());
        node.setUType(this.getType());
        node.setULock(this.isLock());
        node.setUPinned(this.isPinned());
        node.setUMarkDeleted(this.isMarkDeleted());
        node.setUReadRoles(this.getRoles());
        node.setUEditRoles(this.getEditRoles());
        node.setUPostRoles(getPostRoles());
        
        node.setSysCreatedBy(getSys_created_by());
        node.setSysUpdatedBy(getSys_updated_by());
        node.setSysCreatedOn(this.getSysCreatedOn() != null ? this.getSysCreatedOn() : new Date(this.getSys_created_on()));
        node.setSysUpdatedOn(this.getSysUpdatedOn() != null ? this.getSysUpdatedOn() : new Date(this.getSys_updated_on()));
        
        if(this.getProperties() != null)
        {
            Set<ResolveNodePropertiesVO> nodeProperties = new HashSet<ResolveNodePropertiesVO>();
            Iterator<String> it = getProperties().keySet().iterator();
            while(it.hasNext())
            {
                String key = it.next();
                String value = getProperties().get(key);
                
                ResolveNodePropertiesVO prop = new ResolveNodePropertiesVO();
                prop.setUKey(key);
                prop.setUValue(value);
                
                nodeProperties.add(prop);
            }
            
            node.setProperties(nodeProperties);
        }
        
        return node;
    }
    
    
    public RSComponent(Map<String, Object> params)
    {
        setId((String)params.get(ServiceSocialConstants.SYS_ID));
        setSys_id((String)params.get(ServiceSocialConstants.SYS_ID));
        setName((String)params.get(ServiceSocialConstants.U_DISPLAY_NAME));
        setDisplayName((String)params.get(ServiceSocialConstants.U_DISPLAY_NAME));
        setRoles(params.get(ServiceSocialConstants.U_READ_ROLES)!= null ? ((String) params.get(ServiceSocialConstants.U_READ_ROLES)).replaceAll(",", " ") : null);
        setEditRoles(params.get(ServiceSocialConstants.U_EDIT_ROLES)!= null ? ((String) params.get(ServiceSocialConstants.U_EDIT_ROLES)).replaceAll(",", " ") : null);
        setPostRoles(params.get(ServiceSocialConstants.U_POST_ROLES)!= null ? ((String) params.get(ServiceSocialConstants.U_POST_ROLES)).replaceAll(",", " ") : null);
        setType(NodeType.valueOf((String) params.get(ServiceSocialConstants.TYPE)));

        setSys_updated_by((String)params.get(ServiceSocialConstants.SYS_UPDATED_BY));
        setSys_updated_on(params.get(ServiceSocialConstants.SYS_UPDATED_ON) != null ? ((Long) params.get(ServiceSocialConstants.SYS_UPDATED_ON)).longValue() : 0);
        setSys_created_by((String) params.get(ServiceSocialConstants.SYS_CREATED_BY));
        setSys_created_on(params.get(ServiceSocialConstants.SYS_CREATED_ON) != null ? ((Long) params.get(ServiceSocialConstants.SYS_CREATED_ON)).longValue() : 0);

    }
    
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sysId)
    {
        this.sys_id = sysId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getCompName()
    {
        return compName;
    }

    public void setCompName(String compName)
    {
        this.compName = compName;
    }

    public NodeType getType()
    {
        return type;
    }

    public void setType(NodeType type)
    {
        this.type = type;
    }

    public boolean isLock()
    {
        return lock;
    }

    public void setLock(boolean lock)
    {
        this.lock = lock;
    }

    public boolean isPinned()
    {
        return pinned;
    }

    public void setPinned(boolean pinned)
    {
        this.pinned = pinned;
    }

    public long getSys_created_on()
    {
        if(sys_updated_on == 0l)
        {
            sys_updated_on = System.currentTimeMillis();
        }
        
        return sys_created_on;
    }

    public void setSys_created_on(long sys_created_on)
    {
        this.sys_created_on = sys_created_on;
    }

    public long getSys_updated_on()
    {
        return sys_updated_on;
    }

    public void setSys_updated_on(long sys_updated_on)
    {
        this.sys_updated_on = sys_updated_on;
    }

    public String getSys_created_by()
    {
        return sys_created_by;
    }

    public void setSys_created_by(String sys_created_by)
    {
        this.sys_created_by = sys_created_by;
    }

    public String getSys_updated_by()
    {
        return sys_updated_by;
    }

    public void setSys_updated_by(String sys_updated_by)
    {
        this.sys_updated_by = sys_updated_by;
    }

    public Date getSysCreatedOn()
    {
        return sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    public Date getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    public Map<String, String> getProperties()
    {
        return properties;
    }

    public void setProperties(Map<String, String> properties)
    {
        this.properties = properties;
    }
    
    public boolean isDirectFollow()
    {
        return isDirectFollow;
    }

    public void setDirectFollow(boolean isDirectFollow)
    {
        this.isDirectFollow = isDirectFollow;
    }

    public long getUnReadCount()
    {
        return unReadCount;
    }

    public void setUnReadCount(long unReadCount)
    {
        this.unReadCount = unReadCount;
    }

    public boolean isCurrentUser()
    {
        return isCurrentUser;
    }

    public void setCurrentUser(boolean isCurrentUser)
    {
        this.isCurrentUser = isCurrentUser;
    }

    public boolean isFollowing()
    {
        return isFollowing;
    }

    public void setFollowing(boolean isFollowing)
    {
        this.isFollowing = isFollowing;
    }
    
    public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    public String getPostRoles()
    {
        return postRoles;
    }

    public void setPostRoles(String postRoles)
    {
        this.postRoles = postRoles;
    }

    

    public boolean isInboxForwardEmail()
    {
        return inboxForwardEmail;
    }

    public void setInboxForwardEmail(boolean inboxForwardEmail)
    {
        this.inboxForwardEmail = inboxForwardEmail;
    }

    public boolean isInboxDigestEmail()
    {
        return inboxDigestEmail;
    }

    public void setInboxDigestEmail(boolean inboxDigestEmail)
    {
        this.inboxDigestEmail = inboxDigestEmail;
    }

    public boolean isSystemForwardEmail()
    {
        return systemForwardEmail;
    }

    public void setSystemForwardEmail(boolean systemForwardEmail)
    {
        this.systemForwardEmail = systemForwardEmail;
    }

    public boolean isSystemDigestEmail()
    {
        return systemDigestEmail;
    }

    public void setSystemDigestEmail(boolean systemDigestEmail)
    {
        this.systemDigestEmail = systemDigestEmail;
    }

    public boolean isAllForwardEmail()
    {
        return allForwardEmail;
    }

    public void setAllForwardEmail(boolean allForwardEmail)
    {
        this.allForwardEmail = allForwardEmail;
    }

    public boolean isAllDigestEmail()
    {
        return allDigestEmail;
    }

    public void setAllDigestEmail(boolean allDigestEmail)
    {
        this.allDigestEmail = allDigestEmail;
    }

    public boolean isMarkDeleted()
    {
        return markDeleted;
    }

    public void setMarkDeleted(boolean markDeleted)
    {
        this.markDeleted = markDeleted;
    }
    
    public ComponentNotification getComponentNotification()
    {
        return componentNotification;
    }

    public void setComponentNotification(ComponentNotification componentNotification)
    {
        this.componentNotification = componentNotification;
    }

    public static void sortCollection(Collection<? extends RSComponent> comps)
    {
        if(comps != null && comps.size() > 0)
        {
            Collections.sort((List<? extends RSComponent>)comps, new Comparator<Object>() {

                public int compare(Object obj1, Object obj2)
                {
                    int value = 0;
                    
                    if(obj1!=null && obj2!=null && obj1 instanceof RSComponent && obj2 instanceof RSComponent)
                    {
                        String displayName1 = StringUtils.isNotEmpty(((RSComponent)obj1).getDisplayName()) ? ((RSComponent)obj1).getDisplayName() : "";
                        String displayName2 = StringUtils.isNotEmpty(((RSComponent)obj2).getDisplayName()) ? ((RSComponent)obj2).getDisplayName() : "";
                        
                        value = displayName1.compareTo(displayName2);
                    }
                    
                    return value;
                }
                
            });
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        
        if(obj != null && obj instanceof RSComponent)
        {
            RSComponent that = (RSComponent) obj;
            if(StringUtils.isNotEmpty(this.getId()) && StringUtils.isNotEmpty(that.getId()))
            {
                if(this.getId().equalsIgnoreCase(that.getId()))
                {
                    result = true;
                }
            }
            else if(this.getType() != null && that.getType() != null
                            && StringUtils.isNotEmpty(this.getName()) && StringUtils.isNotEmpty(that.getName())
                            )
            {
                if(this.getType() == that.getType() && this.getName().equalsIgnoreCase(that.getName()) )
                {
                    result = true;
                }
            }
            
        }
        return result;
    }

    @Override
    public int hashCode() 
    {
        int hash = 3;
        
        if(StringUtils.isNotBlank(this.getId()))
        {
            hash = 7 * hash + (StringUtils.isNotEmpty(this.getId()) ? this.getId().hashCode() : 1);
        }
        else
        {
            hash = 7 * hash + (StringUtils.isNotEmpty(this.getName()) ? this.getName().hashCode() : 1);
            hash = 7 * hash + (this.getType() != null ? this.getType().name().hashCode() : 1);
        }
        return hash;
    }
    
    
    public boolean validate() throws Exception
    {
        boolean valid = true;

        if(this.getType() == null)
        {
            Log.log.debug("name: " + getName() + ", sysId:" + getId());
            throw new Exception("Type is mandatory for a node/component");
        }

        if(StringUtils.isEmpty(getName()))
        {
            Log.log.debug("type: " + getType() + ", sysId:" + getId());
            throw new Exception("Name is mandatory for a node/component");
        }

        return valid;
    }
    
    @Override
    public String toString()
    {
        String str = getId() + "||" + getName() + "||" + getCompName() + "||" + getType();
        return str;
    }
    
    public String getUsername()
    {
        return getName();
    }
}

