package com.resolve.services.rb.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.IteratorUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

//import scala.collection.Set;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ABMarkupExtractor
{
    private static String REGEX_SEPERATOR = Character.toString((char) 0x00A7);
    
    JsonNode jsonReqParams;
    JsonNode parserConfig;
    JsonNode markups;
    String sample;
    String parserType;
    String parseStart;
    String parseEnd;
    String regex;
    Boolean generatedFromATB;
    HashMap<String,Integer> variables;
    Set<String> wsdataVariables = new HashSet<String>();
    Set<String> flowVariables = new HashSet<String>();
    Set<String> outputVariables = new HashSet<String>();
    HashMap<String,Boolean> options;
    int curRecursiveEnd;
    

    public ABMarkupExtractor(JsonNode jsonReqParams) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        
        this.jsonReqParams = jsonReqParams;
        if(this.jsonReqParams.findValue("parserConfig").isTextual()) {
            parserConfig = mapper.readTree(this.jsonReqParams.findValue("parserConfig").asText());
        }
        else {
            parserConfig = this.jsonReqParams.findValue("parserConfig");
        }
        markups = parserConfig.findValue("markups");
       if(parserConfig.findValue("generatedFromATB") != null) {
           generatedFromATB = true;
       }else
           generatedFromATB = false;;
        sample = extractSampleOutput();
        
        //catch nulls and prevent object from being instantiated by throwing expception
        if(this.jsonReqParams == null)
        {
            throw new NullPointerException("Variable 'jsonReqParams' was detected as null");
        }
        else if(parserConfig == null)
        {
            throw new NullPointerException("JSON Item 'parserConfig' was detected as null");
        }
        else if(markups == null)
        {
            throw new NullPointerException("JSON Item 'markups' was deteced as null");
        }
        
        parserType = extractType();
        parseStart = extractParseStart();
        parseEnd = extractParseEnd();
        regex = extractRegex();
        variables = extractVariables();
        options = extractOptions();
        
        if(parserType == null)
        {
            throw new NullPointerException("JSON Item 'type' was detected as null");
        }
    }
    
    /******
     * Extracts actiontask variables defined in automation builder UI from Json
     * 
     * @return Map<Group or Column Number,Map<variableDetailName,Value>>
     * Variables: name,wsdata,flow
     */
    public HashMap<Integer,HashMap<String,String>> extractColumnVariables()
    {
        HashMap<Integer,HashMap<String,String>> variablesMap = new HashMap<Integer,HashMap<String,String>>();
        
        Iterator<JsonNode> variableIterator = parserConfig.findValue("tblVariables").getElements();
        while(variableIterator.hasNext())
        {
            JsonNode curVarNode = variableIterator.next();
            String variableName = curVarNode.findValue("name").asText();
            String wsEnabled = curVarNode.findValue("wsdata").asText();
            String flowEnabled = curVarNode.findValue("flow").asText();
            int columnNum = curVarNode.findValue("columnNum").asInt();
            HashMap<String,String> variableOptions = new HashMap<String,String>();
            variableOptions.put("name",variableName);
            variableOptions.put("wsdata",wsEnabled);
            variableOptions.put("flow",flowEnabled);
            variablesMap.put(columnNum,variableOptions);
        }
        
        return variablesMap;
    }
    
    
    /*******
     * @return sample text from json as String or {@link null} if empty
     */
    private String extractSampleOutput()
    {
        String sample = "";
        JsonNode sampleJsonNode = jsonReqParams.findValue("sampleOutput");
        if(sampleJsonNode != null) {
            if(sampleJsonNode.isValueNode() && StringUtils.isNotEmpty(sampleJsonNode.asText())){
                sample = sampleJsonNode.asText();
            }
            else if(StringUtils.isNotEmpty(sampleJsonNode.toString())){
                sample = sampleJsonNode.toString();
            }
        }
        return sample;
    }
    
    
    /***
     * @return Sample text extracted from json
     */
    public String getSample()
    {
        return sample;
    }
    
    
    /****
     * Scans json for key "type" and extracts
     * @return String or {@link null} if not type found
     */
    private String extractType()
    {
        String type = null;
        JsonNode typeJsonNode = jsonReqParams.findValue("parserType");
        if(typeJsonNode != null && StringUtils.isNotEmpty(typeJsonNode.asText()))
        {
            type = typeJsonNode.asText();
        }
        
        return type;
    }
    
    
    /***
     * @return parserType extracted from Json 'table','text'
     */
    public String getType()
    {
        return parserType;
    }

    
    public String extractRegex() throws JsonProcessingException, IOException
    {
        String regex = "(?s)";
        
        if(StringUtils.isNotEmpty(parserType))
        {
            if(StringUtils.equalsIgnoreCase("table", parserType))
            {
                JsonNode separators = parserConfig.findValue("columnSeparator");

                if(separators == null)
                {
                    return ""; 
                }
                
                String seperatorText = separators.asText();
                
                //split by comma ONLY SUPPORT 1 TYPE OF DELIMITER AT A TIME.
                String[] delimiters = seperatorText.split(",");
                
                for(int i = 0 ; i < delimiters.length ; i++)
                {
                    String curDelimiter = delimiters[i];
                    curDelimiter = ABUtils.escapeLiterals(curDelimiter);
                    List<String> tokens = ABUtils.extractWrappedTokens(curDelimiter, REGEX_SEPERATOR);
                    Iterator<String> tokenIter = tokens.iterator();
                    while(tokenIter.hasNext())
                    {
                        String curToken = tokenIter.next();
                        String tokensRegex = ABUtils.convertUIDefinedRegex(curToken);
                        curDelimiter = curDelimiter.replace(REGEX_SEPERATOR+curToken+REGEX_SEPERATOR, tokensRegex);
                    }
                    delimiters[i] = curDelimiter;
                }
                regex = StringUtils.join(delimiters,"|");
            }
            else if(StringUtils.equalsIgnoreCase("text", parserType))
            {
                List<JsonNode> fixedMarkups = fixOverlappingNodes(IteratorUtils.toList(markups.getElements()));
                fixedMarkups = sortMarkups(fixedMarkups);
                List<JsonNode> regexMarkups = addRegexToNodes(fixedMarkups,generatedFromATB);
                Iterator<JsonNode> regexIter = regexMarkups.iterator();
                
                while(regexIter.hasNext())
                {
                    JsonNode curRegexNode = regexIter.next();
                    regex += curRegexNode.findValue("regex").asText();
                }
            }
        }
        
        regex = handleSpecialRegexCases(regex);        
        
        return regex;
    }
    
    private String handleSpecialRegexCases(String regexIn)
    {
        
        if(regexIn.equals("(?s)(.*?)"))
        {
            regexIn = "(?s)(.*)";
        }
        return regexIn;   
    }

    private List<JsonNode> addRegexToNodes(List<JsonNode> markups, Boolean generatedFromATB)
    {
        final String dagger = Character.toString((char) 0x2020);
        final String doubleDagger = Character.toString((char) 0x2021);
        
        Iterator<JsonNode> markupsIter = markups.iterator();
        List<JsonNode> markupsOut = new ArrayList<JsonNode>();
        
        while(markupsIter.hasNext())
        {
            ObjectNode curNode = (ObjectNode)markupsIter.next();
            String curAction = curNode.findValue("action").asText();
            int curFrom = curNode.findValue("from").asInt();
            int curLength = curNode.findValue("length").asInt();
            int curEnd = curFrom + curLength - 1;
            
            if(curAction.equals("literal"))
            {
               String litSample = extractStringFromSample(curNode);
               //Node will not be overlapped so this will not be needed. TODO: refactor
               List<JsonNode> captureNodes = findNodesWithAction("capture",markups);
               List<JsonNode> captureNodesBetween = findNodesBetween(curFrom, curLength, captureNodes);
               Iterator<JsonNode> capIter = captureNodesBetween.iterator();
               String[] capArray = new String[curLength];
               
               while(capIter.hasNext())
               {
                    JsonNode curCap = capIter.next();
                    int curCapFrom = curCap.findValue("from").asInt();
                    int curCapLength = curCap.findValue("length").asInt();
                    int curCapEnd = curCapFrom + curCapLength - 1;
                   
                    
                    
                    int arrayFromMatch = curCapFrom-curFrom;
                    if(arrayFromMatch < 0)
                    {
                        arrayFromMatch = 0;
                    }
                    
                    int frontChop = curFrom - curCapFrom;
                    if(frontChop < 0) 
                    {
                        frontChop = 0;
                    }
                    int endChop = curCapEnd - curEnd;
                    if(endChop < 0)
                    {
                        endChop = 0;
                    }
                    
                    int arrayLengthMatch = curCapLength - (frontChop) - (endChop);
                    int arrayEndMatch = arrayFromMatch + arrayLengthMatch - 1;
                    if( arrayEndMatch > curLength-1)
                    {
                        arrayEndMatch = curLength;
                    }
                    
                    if(curCapFrom >= curFrom)
                    {
                        String indexValue = capArray[arrayFromMatch];
                        String capString = "(";
                        if(indexValue != null && !indexValue.equals(""))
                        {
                            capString += indexValue;
                        }
                        
                        capArray[arrayFromMatch] = capString;
                    }
                    if(curCapEnd <= curEnd)
                    {
                        String indexValue = capArray[arrayEndMatch];
                        String capString = ")";
                        if(indexValue != null && !indexValue.equals(""))
                        {
                            capString = indexValue + capString;
                        }
                        
                        capArray[arrayEndMatch] = capString;
                    }
               }
               
               // build regex
               String regex = "";
               for(int i = 0; i < litSample.length();i++)
               {
                   char curChar = litSample.charAt(i);
                   String capSide = capArray[i];
                   if(capSide != null)
                   {
                       String prelimRegex = curChar+"";
                       if(capSide.contains("(") || capSide.contains(")"))
                       {
                           if(capSide.contains("("))
                           {
                               prelimRegex = dagger + prelimRegex ;
                           }
                           if(capSide.contains(")"))
                           {
                               prelimRegex = prelimRegex+ doubleDagger;
                           }
                           regex += prelimRegex;
                       }
                   }
                   else
                   {
                       regex += curChar;
                   }
               }
               
               regex = ABUtils.escapeLiterals(regex);
               regex = regex.replaceAll(dagger, "(");
               regex = regex.replaceAll(doubleDagger, ")");
               curNode.put("regex",regex);
               markupsOut.add(curNode);
            }
            else if(curAction.equals("type"))
            {
                String regex = ABUtils.convertUIDefinedRegex(curNode.findValue("type").asText());
                
                List<JsonNode> captureNodes = findNodesWithAction("capture",markups);
                List<JsonNode> captureNodesBetween = findNodesBetween(curFrom, curLength, captureNodes);
                Iterator<JsonNode> capIter = captureNodesBetween.iterator();
                while(capIter.hasNext())
                {
                    JsonNode curCap = capIter.next();
                    int curCapFrom = curCap.findValue("from").asInt();
                    int curCapLength = curCap.findValue("length").asInt();
                    int curCapEnd = curCapFrom + curCapLength - 1;
                                       
                    if(curCapFrom >= curFrom)
                    {
                        regex = "(" + regex;
                    }
                    if(curCapEnd <= curEnd)
                    {
                        regex =  regex + ")" ;
                    }
                }
                
                curNode.put("regex", regex);
                markupsOut.add(curNode);
            }
            else if(generatedFromATB && curAction.equals("capture")){
                String regex = ABUtils.convertUIDefinedRegex(curNode.findValue("type").asText());
                curNode.put("regex", "(" + regex + ")");
                markupsOut.add(curNode);
            }   
        }
        
        return markupsOut;
    }
    
    private Boolean hasLiteral(List<JsonNode> list)
    {
        for(int k = 0 ; k < list.size() ; k++)
        {
            if(list.get(k).findValue("action").asText().equals("literal"))
            {
                return true;
            }
        }
        
        return false;
    }
    
    private List<JsonNode> removeNodesWithAction(List<JsonNode> list, String action)
    {
        for(int k = 0 ; k < list.size() ; k++)
        {
            if(list.get(k).findValue("action").asText().equals(action))
            {
                list.remove(k);
                k--;
            }
        }
        
        return list;
    }
    
    /******
     * Iterates through the markups json and returns all "type" markups within a specified region
     * @return List<JsonNodes> "type" markups within the specified range
     */
    private List<JsonNode> findNodesBetween(int startIndex, int length)
    {
        return findNodesBetween(startIndex,length,IteratorUtils.toList(markups.getElements()));
    }
        
    /******
     * Iterates through the markups json and returns all "type" markups within a specified region
     * @return List<JsonNodes> "type" markups within the specified range
     */
    private static List<JsonNode> findNodesBetween(int startIndex, int length, List<JsonNode> listToSearch)
    {
        int endIndex = startIndex + length;
        
        List<JsonNode> types = new ArrayList<JsonNode>();
        Iterator<JsonNode> markupsIter = listToSearch.iterator();
        
        while(markupsIter.hasNext())
        {
            JsonNode curMarkup = markupsIter.next();            
            int curFrom = curMarkup.findValue("from").asInt();  
            int curLength = curMarkup.findValue("length").asInt();
            int curEnd = curFrom + curLength - 1;
            
            if((( curFrom >= startIndex || curEnd >= startIndex ) && curFrom < endIndex) || (curFrom <= startIndex && curEnd > endIndex))
            {
                types.add(curMarkup);
            }
        }
        
        return types;
    }
    
    
    /***
     * @return Characters or Regex representing the ending of parser extraction
     * If empty string is returned then the user has not selected a parseEnd from the interface
     */
    public String getParseEnd()
    {
        return parseEnd;
    }
    
    private HashMap<String, Boolean> extractOptions()
    {
        Boolean repeat = false;
        Boolean trim = false;
        Boolean boundaryStartIncluded = true;
        Boolean boundaryEndIncluded = true;
        HashMap<String,Boolean> options = new HashMap<String,Boolean>();
        
        //alright lets extract some options from the json
        JsonNode jsonOptions = parserConfig.findValue("options");
        if(jsonOptions!=null)
        {
           JsonNode trimNode = jsonOptions.findValue("trim");
           JsonNode repeatNode = jsonOptions.findValue("repeat");
           JsonNode boundaryStartIncludedNode = jsonOptions.findValue("boundaryStartIncluded");
           JsonNode boundaryEndIncludedNode = jsonOptions.findValue("boundaryEndIncluded");
           if(trimNode != null)
           {
               trim = trimNode.asBoolean();
           }
           if(repeatNode != null)
           {
               repeat = repeatNode.asBoolean();
           }
           if(boundaryStartIncludedNode != null) {  
               boundaryStartIncluded = boundaryStartIncludedNode.asBoolean();
           }
           if(boundaryEndIncludedNode != null) {  
               boundaryEndIncluded = boundaryEndIncludedNode.asBoolean();
           }
        }
        
        options.put("trim",trim);
        options.put("repeat", repeat);
        options.put("boundaryStartIncluded",boundaryStartIncluded);
        options.put("boundaryEndIncluded", boundaryEndIncluded);
        return options;
    }
    
    public HashMap<String, Boolean> getOptions()
    {
        return options;
    }
    
    /***
     * @return Characters or Regex representing the ending of parser extraction
     * If empty string isf returned then the user has not selected a parseEnd from the interface
     */
    private String extractParseStart()
    {
        String parseStart = "";
        
        JsonNode beginParseNode = findNodeWithAction("beginParse");
        if(beginParseNode != null)
        { 
            if(generatedFromATB)
            {
                parseStart = beginParseNode.findValue("boundaryInput").asText(); 
            }
            //WILL BE REMOVED WHEN AB IS DEPRECATED.
            else 
            {
                Boolean isFromEndLine = false;          
                if(beginParseNode.findValue("fromTheEndOfLine") != null)
                {
                    isFromEndLine = beginParseNode.findValue("fromTheEndOfLine").asBoolean();
                }
                
                parseStart = extractStringFromSample(beginParseNode);
                parseStart = ABUtils.escapeLiterals(parseStart);
                if(isFromEndLine)
                {
                    parseStart += ".*?(?:\\n|\\r\\n)";                
                }
            }
        }
        return parseStart;
    }
    
    
    /***
     * @return Characters or Regex representing the ending of parser extraction
     * If empty string is returned then the user has not selected a parseEnd from the interface
     */
    private String extractParseEnd()
    {
        String parseEnd = "";
        
        JsonNode endParseNode = findNodeWithAction("endParse");
        if(endParseNode != null)
        {
            if(generatedFromATB)
            {
                parseEnd = endParseNode.findValue("boundaryInput").asText();
            }
            //WILL BE REMOVED WHEN AB IS DEPRECATED.
            else 
            {
                Boolean stopAtStartOfLine = false;
                if(endParseNode.findValue("stopParseAtStartOfLine") != null)
                {
                    stopAtStartOfLine = endParseNode.findValue("stopParseAtStartOfLine").asBoolean();
                }
                
                parseEnd = extractStringFromSample(endParseNode);
                parseEnd = ABUtils.escapeLiterals(parseEnd);
                
                if(stopAtStartOfLine)
                {   
                    parseEnd = "(?:\\n|\\r\\n).*" + parseEnd;                
                }
                else
                {
                    //Additional check for newline at end, if newline we want to add* to make it optional
                    Pattern pattern = Pattern.compile("\\n\\)$");
                    Matcher matcher = pattern.matcher(parseEnd);
                    if(matcher.find())
                    {
                        parseEnd += "*";
                    }
                }
            }
        }
        
        
        return parseEnd;
    }
    
    /***
     * Sort markups by their from value. 
     * If the markups from values are equal.
     *   then the markups are sorted by their from+length ensuring proper ordering
     *   
     * @param markupsIn -  List of markups with from and length values
     * @return List<JsonNode> list of sorted items
     */
    private List<JsonNode> sortMarkups(List<JsonNode> markupsIn)
    {
        int compareValue = 99999999;
        //insertion sort used as data already seems sorted and this is best to prevent slow down  
        Comparator<JsonNode> markupsCompare = new Comparator<JsonNode>()
        {
            public int compare(JsonNode s1,JsonNode s2)
            {
                int value1 = s1.findValue("from").asInt();
                int value2 = s2.findValue("from").asInt();
                // if the froms are the same we have to do a deeper sort, by their from+length
                // this allows from proper ordering of regex grouping (()())
                if(value1 == value2)
                {
                    value1 = value1 - s1.findValue("length").asInt();
                    value2 = value2 - s2.findValue("length").asInt();
                    //Rank based on presidence if both from and lenght are the same
                    if(value1 == value2)
                    {
                        String curAction1 = s1.findValue("action").asText();
                        String curAction2 = s2.findValue("action").asText();
                        value1 = getActionPresidence(curAction1);
                        value2 = getActionPresidence(curAction2);
                    }
                }                
                return value1 - value2;
               
            }
        };
       
        Collections.sort(markupsIn,markupsCompare);
        return markupsIn;
        
    }
    
    public static List<JsonNode> fixOverlappingNodes(List<JsonNode> markupsIn) throws JsonProcessingException, IOException
    {
        //To keep it simple only modify curNode, no lookahead modification
        List<JsonNode> reconstructedNodes = new ArrayList<JsonNode>();
        for(int i = 0 ; i < markupsIn.size();i++)
        {
            JsonNode curNode = markupsIn.get(i);
            
            int curFrom = curNode.findValue("from").asInt();
            int curLength = curNode.findValue("length").asInt();
            int curEnd = curFrom+curLength-1;
            String curAction = curNode.findValue("action").asText();
            
            if(curAction.equals("literal"))
            {
                //literals take prescendence always do not correct
                reconstructedNodes.add(curNode);
            }
            else if(curAction.equals("type"))
            {
                //check for literal overlap and modify type range to compensate
                List<JsonNode> literalList = findNodesWithAction("literal", markupsIn);
                List<JsonNode> overlappingLiterals = findNodesBetween(curFrom, curLength, literalList);
                Iterator<JsonNode> litIter = overlappingLiterals.iterator();
                
                boolean[] isLit = new boolean[curLength];
                for(int j = 0 ; j < isLit.length; j++)
                {
                    isLit[j] = false;
                }
                
                if(litIter.hasNext())
                {
                    while(litIter.hasNext())
                    {
                        JsonNode curLit = litIter.next();
                        int curLitFrom = curLit.findValue("from").asInt();
                        int curLitLength = curLit.findValue("length").asInt();
                        int curLitEnd = curLitFrom+curLitLength-1;
                        
                        int arrayFromMatch = curLitFrom-curFrom;
                        if(arrayFromMatch < 0)
                        {
                            arrayFromMatch = 0;
                        }
                        
                        int frontChop = curFrom - curLitFrom;
                        if(frontChop < 0) 
                        {
                            frontChop = 0;
                        }
                        int endChop = curLitEnd - curEnd;
                        if(endChop < 0)
                        {
                            endChop = 0;
                        }
                        
                        int arrayLengthMatch = curLitLength - (frontChop) - (endChop);
                        int arrayEndMatch = arrayFromMatch + arrayLengthMatch;
                        if( arrayEndMatch > isLit.length-1)
                        {
                            arrayEndMatch = isLit.length;
                        }
                        
                        for(int isLitIndex = arrayFromMatch; isLitIndex < arrayEndMatch; isLitIndex++)
                        {
                            isLit[isLitIndex] = true;
                        }
                    }
                    
                    //construct new type(s) based on isLit array
                    List<JsonNode> splitTypeList = new ArrayList<JsonNode>();
                    boolean previousFlag = isLit[0];
                    int previousStart = 0;
                    for(int j = 0 ; j < isLit.length; j++)
                    {
                        boolean curFlag = isLit[j];
                        if(previousFlag != curFlag)
                        {
                            if(previousFlag == false)
                            {
                                
                                int positionInSample = curFrom+j;
                                int beginOfCurSection = curFrom+previousStart;
                                
                                ObjectNode clonedType = (ObjectNode) ABUtils.cloneJsonNode(curNode);
                                clonedType.put("from", beginOfCurSection);  
                                clonedType.put("length", positionInSample-beginOfCurSection);
                                
                                splitTypeList.add((JsonNode) clonedType);
                            }
                            else if( j == isLit.length-1 && curFlag == false)
                            {
                                previousStart = j;
                                int positionInSample = curFrom+j+1;
                                int beginOfCurSection = curFrom+previousStart;
                                
                                ObjectNode clonedType = (ObjectNode) ABUtils.cloneJsonNode(curNode);
                                clonedType.put("from", beginOfCurSection);
                                
                                if(beginOfCurSection > curEnd)
                                {
                                    clonedType.put("length", positionInSample-curEnd);
                                }
                                else
                                {
                                    clonedType.put("length", positionInSample-beginOfCurSection);
                                }
                                
                                
                                splitTypeList.add((JsonNode) clonedType);
                            }
                            
                            previousStart = j;
                        }
                        else if( j == isLit.length-1 && curFlag == false)
                        { 
                            ObjectNode clonedType = (ObjectNode) ABUtils.cloneJsonNode(curNode);
                            clonedType.put("from", (curFrom+previousStart));  
                            clonedType.put("length", (j-(previousStart-1)));
                            
                            splitTypeList.add((JsonNode) clonedType);
                            previousStart = j;
                        }
                        
                        previousFlag = curFlag;
                    }
                    
                    //remove curNode and add splitTypeList is modifications needed
                    reconstructedNodes.addAll(splitTypeList);
                }
                else
                {
                    reconstructedNodes.add(curNode);
                }
            }
            else if(curAction.equals("capture"))
            {
                //Nothing Needed
                //2014-12-04 -> Capture not allowed to overlap with captures. Also assuming a captures' startindex(from) and endIndex(from+length) cannot exist in the middle of a type
                //However if capture intersects with literal this is completely valid. It might be easier to handle this situation by splitting before parsing.
                
                reconstructedNodes.add(curNode);
            }
            else
            {
                //FailSafe to keep Nodes, just incase..
                reconstructedNodes.add(curNode);
            }
        }
        return reconstructedNodes;
        
    }
   
    
    /*****
     * Quickly gives a priority number to action types
     * Higher the priority the more important.
     * @param action
     * @return
     */
    private int getActionPresidence(String action)
    {
        if(action.equals("capture"))
        {
            return 1;
        }
        else if(action.equals("literal"))
        {
            return 2;
        }
        else if(action.equals("type"))
        {
            return 3;
        }
        return 99;
    
    }
    
    private HashMap<String,Integer> extractVariables()
    {
        HashMap<String,Integer> variableMap = new HashMap<String,Integer>();
        if(StringUtils.equals(parserType,"text"))
        {
            int groupCount = 1;
            List<JsonNode> sortedMarkups = sortMarkups(findNodesWithSpecifiedValue("action", "capture"));
            Iterator<JsonNode> iterator =  sortedMarkups.iterator();
            while(iterator.hasNext())
            {
                JsonNode curNode = iterator.next();
                JsonNode curVariable = curNode.findValue("variable");
                if(curVariable != null && StringUtils.isNotEmpty(curNode.findValue("variable").asText()))
                {
                    variableMap.put(curNode.findValue("variable").asText(), groupCount);
                    if(curNode.findValue("wsdata") != null && curNode.findValue("wsdata").asBoolean())
                    {
                        wsdataVariables.add(curNode.findValue("variable").asText());
                    }
                    if( curNode.findValue("flow") != null && curNode.findValue("flow").asBoolean())
                    {
                        flowVariables.add(curNode.findValue("variable").asText());
                    }
                    if(curNode.findValue("output") != null && curNode.findValue("output").asBoolean())
                    {
                        outputVariables.add(curNode.findValue("variable").asText());
                    }
                    groupCount++;
                }
                
                
            }
        }
        else if(StringUtils.equals(parserType, "table"))
        {
            Iterator<JsonNode> variableIterator = parserConfig.findValue("tblVariables").getElements();
            while(variableIterator.hasNext())
            {
                JsonNode curVarNode = variableIterator.next();
                String variableName = curVarNode.findValue("variable").asText();
                int columnNum = curVarNode.findValue("columnNum").asInt();
                variableMap.put(variableName,columnNum);
                if(curVarNode.findValue("wsdata").asBoolean())
                {
                    wsdataVariables.add(variableName);
                }
                if(curVarNode.findValue("flow").asBoolean())
                {
                    flowVariables.add(variableName);
                }
                if(curVarNode.findValue("output").asBoolean())
                {
                    outputVariables.add(variableName);
                }
            }
        }
        
        return variableMap;
    }
    
    public HashMap<String,Integer> getVariables()
    {
        return variables;
    }

    public Set<String> getWsdataVariables()
    {
        return wsdataVariables;
    }

    public Set<String> getFlowVariables()
    {
        return flowVariables;
    }
    
    public Set<String> getOutputVariables()
    {
        return outputVariables;
    }
    public String getParseStart()
    {
        return parseStart;
    }
    
    /*******
     * Searches through JsonNode markups for an object with key: "action" and value specified by parameter action.
     * 
     * @param action - expected action name
     * @return JsonNode if object found meeting criteria otherwise null returned
     */
    //extracts from markups JsonNode with action = value
    private JsonNode findNodeWithAction(String action)
    {
       JsonNode foundActionNode = null;
       Iterator<JsonNode> iterator =  markups.getElements();
 ITER: while(iterator.hasNext())
       {
           JsonNode curNode = iterator.next();
           if(curNode.findValue("action").asText().equals(action))
           {
               foundActionNode = curNode;
               break ITER;
           }
       }
       
       return foundActionNode;
    }
    
    private static List<JsonNode> findNodesWithAction(String action, List<JsonNode> listIn)
    {
       List<JsonNode> foundActionNodes = new ArrayList<JsonNode>();
       Iterator<JsonNode> iterator =  listIn.iterator();
       while(iterator.hasNext())
       {
           JsonNode curNode = iterator.next();
           if(curNode.findValue("action").asText().equals(action))
           {
               foundActionNodes.add(curNode);
           }
       }
       
       return foundActionNodes;
    }
    
    /*******
     * Searches through JsonNode list for an objects with key: "action" and value specified by parameter action.
     * Removes objects from list and returns
     * 
     * @param action - expected action name
     * @return JsonNode if object found meeting criteria otherwise null returned
     */
    //extracts from markups JsonNode with action = value
    private List<JsonNode> removeNodesWithAction(String action, List<JsonNode> listToRemoveFrom)
    {
       Iterator<JsonNode> iterator =  listToRemoveFrom.iterator();
       while(iterator.hasNext())
       {
           JsonNode curNode = iterator.next();
           if(curNode.findValue("action").asText().equals(action))
           {
               listToRemoveFrom.remove(curNode);
           }
       }
       
       return listToRemoveFrom;
    }
    
    /*******
     * Searches through JsonNode markups for an object with the key and value specified by parameter action.
     * 
     * @param key - json key to pull
     * @param value - json value to pull against
     * @return List<JsonNode> if object found meeting criteria otherwise null returned
     */
    //extracts from markups JsonNode with action = value
    private List<JsonNode> findNodesWithSpecifiedValue(String key, String value)
    {
       List<JsonNode> foundNodes = new ArrayList<JsonNode>();
       Iterator<JsonNode> iterator =  markups.getElements();
       while(iterator.hasNext())
       {
           JsonNode curNode = iterator.next();
           if(curNode.findValue(key).asText().equals(value))
           {
               foundNodes.add(curNode);
           }
       }
       
       return foundNodes;
    }
    
    
    /***
     * Simple substring method of the sample applied to the sample text
     * @param startIndex - index to start extraction
     * @param length - length of string to extract
     * @return Extracted String
     */
    private String extractStringFromSample(int startIndex, int length)
    {
        String extraction = null;
        
        if(startIndex+length-1 > sample.length())
        {
            Log.log.error("Index:"+startIndex+" and Length:"+length+" are greater than sample length");
        }
        else
        {
            extraction = sample.substring(startIndex, startIndex+length);
        }
        
        return extraction;
    }
    
    /**
     * @param markupItem - JsonNode from an item pulled out of the markups Json 
     * @return String of text from sample as marked up on the interface
     */
    private String extractStringFromSample(JsonNode markupItem)
    {
        int startIndex = markupItem.findValue("from").asInt();
        int length = markupItem.findValue("length").asInt();
        return extractStringFromSample(startIndex,length);
    }

    public String getRegex()
    {
        return regex;
    }
    
    /**
    * @return The start index where parsing can begin. beginParse(from+length)
    */
    public int getStartIndex()
    {
        int startIndex = 0;
        JsonNode beginParseNode = findNodeWithAction("beginParse");
        if(beginParseNode != null)
        {
            startIndex = beginParseNode.findValue("from").asInt()+beginParseNode.findValue("length").asInt();
        }
        
        return startIndex;
    }
}
