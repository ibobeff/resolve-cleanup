/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.immport;

import java.util.ArrayList;
import java.util.Collection;

import com.resolve.services.ServiceGraph;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.util.Log;

public class ImportForumGraph extends ImportComponentGraph
{

    public ImportForumGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        Log.log.debug("Importing Forum Relationship:" + rel);
        ResolveNodeVO forumNode = ServiceGraph.findNode(null, null, rel.getSourceName(), rel.getSourceType(), username);
        if (forumNode != null)
        {
            switch (rel.getTargetType())
            {
                case USER:
                    //Note: User will always follow rest of the Entities
                    ResolveNodeVO userNode = ServiceGraph.findNode(null, null, rel.getTargetName(), rel.getTargetType(), username);
                    if (userNode != null)
                    {
                        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
                        followComponents.add(forumNode);

                        ServiceGraph.follow(userNode.getSys_id(), null, null, null, followComponents, followerEdgeProps, username);
                    }
                    break;

                default:
                    break;
            }
        }
    }

}
