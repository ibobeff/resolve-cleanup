/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.vo.StreamDTO;
import com.resolve.services.vo.StreamGroupDTO;

public class AdvanceTree
{
    public static final String ONLY = "Only";
    public static final String STATIC_ID = "__sysId__";
    public static final String NO_DISPLAY_STATIC_ID = "__DND_sysId__";// NO
                                                                      // display
                                                                      // sysid

    public static final String ALL = "All";
    public static final String INBOX = "Inbox";
    public static final String BLOG = "Blog";
    public static final String SENT = "Sent";
    public static final String NOTIFICATION = "Notification";
    public static final String STARRED = "Starred";
    public static final String PROCESS = "Process";
    public static final String TEAMS = "Teams";
    public static final String RUNBOOKS = "Runbooks";
    public static final String DECISIONTREE = "DecisionTree";
    public static final String DOCUMENTS = "Documents";
    public static final String ACTIONTASKS = "Actiontasks";
    public static final String USERS = "Users";
    public static final String FOLLOWS = "Follows";
    public static final String RSS = "RSS";
    public static final String FORUMS = "Forums";
    public static final String NAMESPACES = "Namespaces";
    public static final String DEFAULT = "Default";

    private Collection<Process> processes;
    private Collection<Team> teams;
    private Collection<Forum> forums;
    private Collection<Namespace> namespaces;
    private Collection<User> users;
    private Collection<ActionTask> actiontasks;
    private Collection<Runbook> runbooks;
    private Collection<DecisionTree> decisiontrees;
    private Collection<Document> documents;
    private Collection<Rss> rsss;
    private User owner;

    // collections used for manipulations only
    // private Collection<Process> allProcesses = new HashSet<Process>();
    // private Collection<Team> allTeams = new HashSet<Team>();
    // private Collection<Forum> allForums = new HashSet<Forum>();
    // private Collection<User> allUsers = new HashSet<User>();
    // private Collection<ActionTask> allActiontasks = new
    // HashSet<ActionTask>();
    // private Collection<Runbook> allRunbooks = new HashSet<Runbook>();
    // private Collection<Document> allDocuments = new HashSet<Document>();
    // private Collection<Rss> allRsss = new HashSet<Rss>();

    /**
     * @return the owner
     */
    public User getOwner()
    {
        return owner;
    }

    /**
     * @param owner
     *            the owner to set
     */
    public void setOwner(User owner)
    {
        this.owner = owner;
    }

    public void setProcesses(Collection<Process> processes)
    {
        this.processes = processes;
    }

    public Collection<Process> getProcesses()
    {
        return this.processes;
    }

    public void setTeams(Collection<Team> teams)
    {
        this.teams = teams;
    }

    public Collection<Team> getTeams()
    {
        return this.teams;
    }

    public void setForums(Collection<Forum> forums)
    {
        this.forums = forums;
    }

    public Collection<Forum> getForums()
    {
        return this.forums;
    }
    
    public void setNamespaces(Collection<Namespace> namespaces)
    {
        this.namespaces = namespaces; 
    }

    public Collection<Namespace> getNamespaces()
    {
        return this.namespaces;
    }
    
    public void setUsers(Collection<User> users)
    {
        this.users = users;
    }

    public Collection<User> getUsers()
    {
        return this.users;
    }

    public void setActionTasks(Collection<ActionTask> actiontasks)
    {
        this.actiontasks = actiontasks;
    }

    public Collection<ActionTask> getActionTasks()
    {
        return this.actiontasks;
    }

    public void setRunbooks(Collection<Runbook> runbooks)
    {
        this.runbooks = runbooks;
    }
    
    public Collection<Runbook> getRunbooks()
    {
        return this.runbooks;
    }
    
    public void setDecisionTrees(Collection<DecisionTree> decisiontrees)
    {
        this.decisiontrees = decisiontrees;
    }

    public Collection<DecisionTree> getDecisionTrees()
    {
        return this.decisiontrees;
    }

    public void setDocuments(Collection<Document> documents)
    {
        this.documents = documents;
    }

    public Collection<Document> getDocuments()
    {
        return this.documents;
    }

    public void setRsss(Collection<Rss> rsss)
    {
        this.rsss = rsss;
    }

    public Collection<Rss> getRsss()
    {
        return this.rsss;
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Utility methods
    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Utility methods to transform the format
    // public String convertObjectToJsonString()
    // {
    // preprocessDataStrutures();
    //
    // StringBuffer json = new StringBuffer();
    //
    // json.append("{'text':'.','children': [").append("\n");
    // json.append(createJsonLeaf(ALL, ALL, STATIC_ID, owner.getUsername(), "",
    // "")).append(",").append("\n");
    // json.append(createJsonLeaf(INBOX, INBOX, STATIC_ID, owner.getUsername(),
    // "", "")).append(",").append("\n");
    // json.append(createJsonLeaf(STARRED, STARRED, STATIC_ID,
    // owner.getUsername(), "", "")).append(",").append("\n");
    // json.append(createJsonLeaf(BLOG, BLOG, STATIC_ID, owner.getUsername(),
    // "", "")).append(",").append("\n");
    // json.append(createJsonLeaf(SENT, SENT, STATIC_ID, owner.getUsername(),
    // "", "")).append(",").append("\n");
    // json.append(createJsonLeaf(NOTIFICATION, NOTIFICATION, STATIC_ID,
    // owner.getUsername(), "", "")).append(",").append("\n");
    //
    // json.append(createJsonFolder(PROCESS, PROCESS, STATIC_ID,
    // owner.getUsername(), processes, "", "")).append(",").append("\n");
    //
    // // json.append(createJsonFolder(TEAMS, TEAMS,
    // // STATIC_ID,owner.getUsername(), teams, "",
    // // "")).append(",").append("\n");
    // // json.append(createJsonForTeams(TEAMS, TEAMS, STATIC_ID,
    // // owner.getUsername(), teams)).append(",").append("\n");
    //
    // json.append(createJsonFolder(RUNBOOKS, RUNBOOKS, STATIC_ID,
    // owner.getUsername(), runbooks, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(DOCUMENTS, DOCUMENTS, STATIC_ID,
    // owner.getUsername(), documents, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(ACTIONTASKS, ACTIONTASKS, STATIC_ID,
    // owner.getUsername(), actiontasks, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(USERS, USERS, STATIC_ID,
    // owner.getUsername(), users, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(RSS, RSS, STATIC_ID, owner.getUsername(),
    // rsss, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(FORUMS, FORUMS, STATIC_ID,
    // owner.getUsername(), forums, "", "")).append("\n");
    //
    // // end of json
    // json.append("]}");
    //
    // return json.toString();
    // }// convertObjectToJsonString

    public List<Object> getAdvancedCpList()
    {

        List<Object> streams = new ArrayList<Object>();

        List<StreamDTO> items = new ArrayList<StreamDTO>();
        streams.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.ALL, AdvanceTree.ALL));
        streams.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.INBOX, AdvanceTree.INBOX));
        streams.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.STARRED, AdvanceTree.STARRED));
        streams.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.BLOG, AdvanceTree.BLOG));
        streams.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.SENT, AdvanceTree.SENT));
        streams.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.NOTIFICATION, AdvanceTree.NOTIFICATION));

        if (processes != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.PROCESS).setStreamsFromCollection(processes, true));
        if (teams != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.TEAMS).setStreamsFromCollection(teams, true));
        if (documents != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.DOCUMENTS).setStreamsFromCollection(documents, true));
        if (runbooks != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.RUNBOOKS).setStreamsFromCollection(runbooks, true));
        if (decisiontrees != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.DECISIONTREE).setStreamsFromCollection(decisiontrees, true));
        if (actiontasks != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.ACTIONTASKS).setStreamsFromCollection(actiontasks, true));
        if (users != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.USERS).setStreamsFromCollection(users, true));
        if (rsss != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.RSS).setStreamsFromCollection(rsss, true));
        if (forums != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.FORUMS).setStreamsFromCollection(forums, false));
        if (namespaces != null) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.NAMESPACES).setStreamsFromCollection(namespaces, false));

        return streams;
    }

    public List<Object> getHomeCpList()
    {

        List<Object> streams = new ArrayList<Object>();

        List<StreamDTO> items = new ArrayList<StreamDTO>();
        items.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.ALL, AdvanceTree.ALL));
        items.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.INBOX, AdvanceTree.INBOX));
        items.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.STARRED, AdvanceTree.STARRED));
        items.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.BLOG, AdvanceTree.BLOG));
        items.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.SENT, AdvanceTree.SENT));
        items.add(new StreamDTO(AdvanceTree.STATIC_ID, AdvanceTree.NOTIFICATION, AdvanceTree.NOTIFICATION));
        streams.add(new StreamGroupDTO().setDisplayName(AdvanceTree.DEFAULT).setRecords(items));

        if (processes != null && processes.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.PROCESS).setStreamsFromCollection(processes, false));
        if (teams != null && teams.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.TEAMS).setStreamsFromCollection(teams, false));
        if (documents != null && documents.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.DOCUMENTS).setStreamsFromCollection(documents, false));
        if (runbooks != null && runbooks.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.RUNBOOKS).setStreamsFromCollection(runbooks, false));
        if (decisiontrees != null && decisiontrees.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.DECISIONTREE).setStreamsFromCollection(decisiontrees, false));
        if (actiontasks != null && actiontasks.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.ACTIONTASKS).setStreamsFromCollection(actiontasks, false));
        if (users != null && users.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.USERS).setStreamsFromCollection(users, false));
        if (rsss != null && rsss.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.RSS).setStreamsFromCollection(rsss, false));
        if (forums != null && forums.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.FORUMS).setStreamsFromCollection(forums, false));
        if (namespaces != null && namespaces.size() > 0) streams.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.NAMESPACES).setStreamsFromCollection(namespaces, false));

        return streams;
    }

    // public String getHomeCPJsonString()
    // {
    // // collectAllObjects();
    // preprocessDataStrutures();
    //
    // StringBuffer json = new StringBuffer();
    //
    // json.append("[").append("\n");
    // json.append(createEntityJson(ALL, owner.getUsername(), STATIC_ID, ALL,
    // DEFAULT)).append(",").append("\n");
    // json.append(createEntityJson(INBOX, owner.getUsername(), STATIC_ID,
    // INBOX, DEFAULT)).append(",").append("\n");
    // json.append(createEntityJson(STARRED, owner.getUsername(), STATIC_ID,
    // STARRED, DEFAULT)).append(",").append("\n");
    // json.append(createEntityJson(BLOG, owner.getUsername(), STATIC_ID, BLOG,
    // DEFAULT)).append(",").append("\n");
    // json.append(createEntityJson(SENT, owner.getUsername(), STATIC_ID, SENT,
    // DEFAULT)).append(",").append("\n");
    // json.append(createEntityJson(NOTIFICATION, owner.getUsername(),
    // STATIC_ID, NOTIFICATION, DEFAULT)).append("\n");
    //
    // StringBuffer tempStr = null;
    // if (processes.size() > 0)
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(PROCESS, PROCESS, STATIC_ID,
    // owner.getUsername(), processes));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // if (teams.size() > 0)
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(TEAMS, TEAMS, STATIC_ID,
    // owner.getUsername(), teams));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // if (documents.size() > 0)
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(DOCUMENTS, DOCUMENTS, STATIC_ID,
    // owner.getUsername(), documents));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // if (runbooks.size() > 0)
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(RUNBOOKS, RUNBOOKS, STATIC_ID,
    // owner.getUsername(), runbooks));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // if (actiontasks.size() > 0)
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(ACTIONTASKS, ACTIONTASKS, STATIC_ID,
    // owner.getUsername(), actiontasks));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // if (users != null && users.size() > 0)
    // ;
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(USERS, USERS, STATIC_ID,
    // owner.getUsername(), users));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // if (rsss.size() > 0)
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(RSS, RSS, STATIC_ID,
    // owner.getUsername(), rsss));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // if (forums.size() > 0)
    // {
    // tempStr = new StringBuffer();
    // tempStr.append(createCPGroupJson(FORUMS, FORUMS, STATIC_ID,
    // owner.getUsername(), forums));
    // if (tempStr.length() > 0)
    // {
    // json.append(",").append(tempStr).append("\n");
    // }
    // }
    //
    // // end of json
    // json.append("]");
    //
    // return json.toString();
    // }// getHomeCPJsonString

    // public static String createCPGroupJson(String folderName, String
    // compType, String sysid, String username, Collection<? extends
    // RSComponent> children)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // if (children != null && children.size() > 0)
    // {
    // Iterator<? extends RSComponent> it = children.iterator();
    // while (it.hasNext())
    // {
    // RSComponent comp = it.next();
    //
    // json.append(createEntityJson(comp.getDisplayName(), username,
    // comp.getSys_id(), compType, folderName));
    //
    // if (it.hasNext())
    // {
    // json.append(",");
    // }
    // }// end of while
    //
    // }// end of if
    //
    // return json.toString();
    //
    // }// createHomeCPFolderJson

    // private static String createEntityJson(String displayName, String user,
    // String sysid, String comptype, String grouptitle)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // json.append("{").append("\n");
    // json.append("   displayName: '").append(SocialUtil.encodePost(displayName)).append("',").append("\n");
    // json.append("   user: '").append(user).append("',").append("\n");
    // json.append("   sysid: '").append(sysid).append("',").append("\n");
    // json.append("   comptype: '").append(comptype).append("',").append("\n");
    // json.append("   leaf: '").append(true).append("',").append("\n");
    // json.append("   grouptitle: '").append(grouptitle).append("'").append("\n");
    // json.append("}").append("\n");
    //
    // return json.toString();
    // }// createHomeCPEntity

    // public String getComponentsListInJson()
    // {
    // StringBuffer json = new StringBuffer();
    //
    //
    // json.append("[").append("\n");
    //
    // json.append(createJsonString(STATIC_ID, INBOX,
    // INBOX)).append(",").append("\n");
    // json.append(createJsonString(STATIC_ID, BLOG,
    // BLOG)).append(",").append("\n");
    // json.append(createJsonString(STATIC_ID, SENT,
    // SENT)).append(",").append("\n");
    // json.append(createJsonString(STATIC_ID, NOTIFICATION,
    // NOTIFICATION)).append(",").append("\n");
    // json.append(createJsonString(STATIC_ID, STARRED,
    // STARRED)).append(",").append("\n");
    // // json.append(createJsonString(STATIC_ID, INBOX,
    // PROCESS)).append(",").append("\n");
    // // json.append(createJsonString(STATIC_ID, INBOX,
    // TEAMS)).append(",").append("\n");
    // // json.append(createJsonString(STATIC_ID, INBOX,
    // RUNBOOKS)).append(",").append("\n");
    // // json.append(createJsonString(STATIC_ID, INBOX,
    // ACTIONTASKS)).append(",").append("\n");
    // // json.append(createJsonString(STATIC_ID, INBOX,
    // USERS)).append(",").append("\n");
    // // json.append(createJsonString(STATIC_ID, INBOX,
    // RSS)).append(",").append("\n");
    // json.append(createComponentsListForCBInJson(forums,
    // FORUMS)).append("").append("\n");
    //
    //
    // //end of json
    // json.append("]");
    //
    // return json.toString();
    // }//getComponentsListInJson

    // private String createJsonLeaf(String nodeType, String compType, String
    // sysid, String username, String parentCompType, String parentSysId)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // json.append("{").append("\n");
    // //
    // json.append("   text: '").append(SocialUtil.encodePost(nodeType)).append("',").append("\n");
    // json.append("   comptype: '").append(compType).append("',").append("\n");
    // json.append("   sysid: '").append(sysid).append("',").append("\n");
    // json.append("   user: '").append(username).append("',").append("\n");
    // json.append("   parent_comptype: '").append(parentCompType).append("',").append("\n");
    // json.append("   parent_sysid: '").append(parentSysId).append("',").append("\n");
    // json.append("   leaf: true, ").append("\n");
    // json.append("   iconCls: 'noicons'").append("\n");
    //
    // json.append("}").append("\n");
    //
    // return json.toString();
    // }// createJsonLeaf

    // private String createJsonFolder(String folderName, String compType,
    // String sysid, String username, Collection<? extends RSComponent>
    // children, String parentCompType, String parentSysId)
    // {
    //
    // StringBuffer json = new StringBuffer();
    //
    // json.append("{").append("\n");
    // //
    // json.append("   text: '").append(SocialUtil.encodePost(folderName)).append("',").append("\n");
    // json.append("   comptype: '").append(compType).append("',").append("\n");
    // json.append("   sysid: '").append(sysid).append("',").append("\n");
    // json.append("   user: '").append(username).append("',").append("\n");
    // // json.append("   expanded: true, ").append("\n");
    // json.append("   iconCls: 'task-folder'").append(",").append("\n");
    // json.append("   children: [").append("\n");
    // if (children != null && children.size() > 0)
    // {
    // if (folderName.equals(PROCESS))
    // {
    // json.append(createJsonForProcesses(children, username));
    // }
    // else if (folderName.equals(TEAMS))
    // {
    // // json.append(createJsonForTeams(children, username,
    // // parentCompType, parentSysId));
    // }
    // else if (folderName.equals(FORUMS))
    // {
    // // json.append(createJsonForForums(children, username,
    // // parentCompType, parentSysId));
    // }
    // else
    // {
    // Iterator<? extends RSComponent> it = children.iterator();
    // while (it.hasNext())
    // {
    // RSComponent comp = it.next();
    // json.append(createJsonLeaf(comp.getDisplayName(), folderName,
    // comp.getSys_id(), username, parentCompType, parentSysId));
    //
    // if (it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }// end of while
    // }
    //
    // }// end of if
    // json.append("   ]").append("\n");// close the children array
    // json.append("}").append("\n");
    //
    // return json.toString();
    //
    // }// createJsonFolder

    // private String createJsonForTeams(String folderName, String compType,
    // String sysid, String username, Collection<? extends RSComponent>
    // teamsLocal)
    // {
    // StringBuffer json = new StringBuffer();
    // json.append("{").append("\n");
    // json.append("   displayName: '").append(SocialUtil.encodePost(folderName)).append("',").append("\n");
    // json.append("   comptype: '").append(compType).append("',").append("\n");
    // json.append("   sysid: '").append(sysid).append("',").append("\n");
    // json.append("   user: '").append(username).append("',").append("\n");
    // // json.append("   expanded: true, ").append("\n");
    // json.append("   iconCls: 'task-folder'").append(",").append("\n");
    // json.append("   children: [").append("\n");
    //
    // if (teamsLocal != null && teamsLocal.size() > 0)
    // {
    // Iterator<? extends RSComponent> it = teamsLocal.iterator();
    // while (it.hasNext())
    // {
    // Team team = (Team) it.next();
    //
    // json.append(createJsonForTeam(team, username, TEAMS, team.getSys_id()));
    //
    // if (it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }// end of while
    // }// end of if
    //
    // json.append("   ]").append("\n");// close the children array
    // json.append("}").append("\n");
    //
    // return json.toString();
    //
    // }

    // private String createJsonForTeams(Collection<? extends RSComponent>
    // teamsLocal, String username, String parentCompType, String parentSysId)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // if (teamsLocal != null && teamsLocal.size() > 0)
    // {
    // Iterator<? extends RSComponent> it = teamsLocal.iterator();
    // while (it.hasNext())
    // {
    // Team team = (Team) it.next();
    //
    // // json.append(createJsonForTeam(team, username, TEAMS,
    // // team.getSys_id()));//**MAY BE A BUG ** REVIEW THIS AGAIN
    // json.append(createJsonForTeam(team, username, parentCompType,
    // parentSysId));// **MAY
    // // BE
    // // A
    // // BUG
    // // **
    // // REVIEW
    // // THIS
    // // AGAIN
    //
    // if (it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }// end of while
    // }// end of if
    //
    // return json.toString();
    // }

    // private String createJsonForTeam(Team team, String username, String
    // parentCompType, String parentSysId)
    // {
    // StringBuffer json = new StringBuffer();
    // String displayName = SocialUtil.encodePost(team.getDisplayName());
    //
    // Collection<Team> childTeams = team.getTeams();
    // Collection<User> childUsers = team.getUsers();
    //
    // // sort
    // RSComponent.sortCollection(childTeams);
    // RSComponent.sortCollection(childUsers);
    //
    // json.append("{").append("\n");
    // json.append("   displayName: '").append(displayName).append("',").append("\n");
    // json.append("   comptype: '").append(TEAMS).append("',").append("\n");
    // json.append("   sysid: '").append(team.getSys_id()).append("',").append("\n");
    // json.append("   parent_comptype: '").append(parentCompType).append("',").append("\n");
    // json.append("   parent_sysid: '").append(parentSysId).append("',").append("\n");
    // json.append("   user: '").append(username).append("',").append("\n");
    // // json.append("   expanded: true, ").append("\n");
    // json.append("   iconCls: 'task-folder'").append(",").append("\n");
    // json.append("   children: [").append("\n");
    //
    // // add the ONLY for this container
    // json.append(createJsonLeaf(displayName + "(" + ONLY + ")", TEAMS,
    // team.getSys_id(), owner.getUsername(), "", "")).append(",").append("\n");
    //
    // // if(childTeams != null && childTeams.size() > 0)
    // // {
    // // Iterator<Team> it = childTeams.iterator();
    // // while(it.hasNext())
    // // {
    // // Team childTeam = it.next();
    // //
    // // if(StringUtils.isNotEmpty(parentCompType) &&
    // // parentCompType.equalsIgnoreCase(TEAMS))
    // // {
    // // json.append(createJsonForTeam(childTeam, username, TEAMS,
    // // childTeam.getSys_id()));//*** RECURSIVE
    // // }
    // // else
    // // {
    // // json.append(createJsonForTeam(childTeam, username, parentCompType,
    // // parentSysId));//*** RECURSIVE
    // // }
    // //
    // // if(it.hasNext())
    // // {
    // // json.append(",").append("\n");
    // // }
    // // }//end of while
    // //
    // // json.append(",").append("\n");
    // // }//end of if
    // if (childUsers != null && childUsers.size() > 0)
    // {
    // Iterator<User> it = childUsers.iterator();
    // while (it.hasNext())
    // {
    // User user = it.next();
    //
    // json.append(createJsonLeaf(user.getDisplayName(), USERS,
    // user.getSys_id(), username, parentCompType, parentSysId));
    //
    // if (it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }// end of while
    // }// end of childUsers
    //
    // json.append("   ]").append("\n");// close the children array
    // json.append("}").append("\n");
    //
    // return json.toString();
    // }// createJsonForTeam

    // private String createJsonForProcesses(Collection<? extends RSComponent>
    // processesLocal, String username)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // if (processesLocal != null && processesLocal.size() > 0)
    // {
    // Iterator<? extends RSComponent> it = processesLocal.iterator();
    // while (it.hasNext())
    // {
    // Process process = (Process) it.next();
    //
    // json.append(createJsonForProcess(process, username));
    //
    // if (it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }// end of while
    // }// end of if
    //
    // return json.toString();
    //
    // }// createJsonForProcesses

    // private String createJsonForForums(Collection<? extends RSComponent>
    // forumsLocal, String username, String parentCompType, String parentSysId)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // if (forumsLocal != null && forumsLocal.size() > 0)
    // {
    // Iterator<? extends RSComponent> it = forumsLocal.iterator();
    // while (it.hasNext())
    // {
    // Forum forum = (Forum) it.next();
    //
    // json.append(createJsonForForum(forum, username, parentCompType,
    // parentSysId));
    //
    // if (it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }// end of while
    // }// end of if
    //
    // return json.toString();
    //
    // }// createJsonForForums
    //
    // private String createJsonForProcess(Process process, String username)
    // {
    // StringBuffer json = new StringBuffer();
    // // String displayName = SocialUtil.encodePost(process.getDisplayName());
    //
    // Collection<Team> childTeams = process.getTeams();
    // Collection<User> childUsers = process.getUsers();
    // Collection<Runbook> childRunbooks = process.getRunbooks();
    // Collection<Document> childDocuments = process.getDocuments();
    // Collection<ActionTask> childActiontasks = process.getActiontasks();
    // Collection<Rss> childRss = process.getRss();
    // Collection<Forum> childForums = process.getForums();
    //
    // // sort
    // RSComponent.sortCollection(childTeams);
    // RSComponent.sortCollection(childUsers);
    // RSComponent.sortCollection(childRunbooks);
    // RSComponent.sortCollection(childDocuments);
    // RSComponent.sortCollection(childActiontasks);
    // RSComponent.sortCollection(childRss);
    // RSComponent.sortCollection(childForums);
    //
    // json.append("{").append("\n");
    // //
    // json.append("   displayName: '").append(displayName).append("',").append("\n");
    // json.append("   comptype: '").append(PROCESS).append("',").append("\n");
    // json.append("   sysid: '").append(process.getSys_id()).append("',").append("\n");
    // json.append("   user: '").append(username).append("',").append("\n");
    // // json.append("   expanded: true, ").append("\n");
    // json.append("   iconCls: 'task-folder'").append(",").append("\n");
    // json.append("   children: [").append("\n");
    //
    // // add the ONLY for this container
    // // json.append(createJsonLeaf(displayName + "(" + ONLY + ")", PROCESS,
    // // process.getSys_id(), owner.getUsername(), "",
    // // "")).append(",").append("\n");
    //
    // json.append(createJsonFolder(TEAMS, TEAMS, NO_DISPLAY_STATIC_ID,
    // owner.getUsername(), childTeams, PROCESS,
    // process.getSys_id())).append(",").append("\n");
    // json.append(createJsonFolder(RUNBOOKS, RUNBOOKS, NO_DISPLAY_STATIC_ID,
    // owner.getUsername(), childRunbooks, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(DOCUMENTS, DOCUMENTS, NO_DISPLAY_STATIC_ID,
    // owner.getUsername(), childDocuments, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(ACTIONTASKS, ACTIONTASKS,
    // NO_DISPLAY_STATIC_ID, owner.getUsername(), childActiontasks, "",
    // "")).append(",").append("\n");
    // json.append(createJsonFolder(USERS, USERS, NO_DISPLAY_STATIC_ID,
    // owner.getUsername(), childUsers, PROCESS,
    // process.getSys_id())).append(",").append("\n");
    // json.append(createJsonFolder(RSS, RSS, NO_DISPLAY_STATIC_ID,
    // owner.getUsername(), childRss, "", "")).append(",").append("\n");
    // json.append(createJsonFolder(FORUMS, FORUMS, NO_DISPLAY_STATIC_ID,
    // owner.getUsername(), childForums, "", "")).append("\n");
    //
    // json.append("   ]").append("\n");// close the children array
    // json.append("}").append("\n");
    //
    // return json.toString();
    // }// createJsonForProcess
    //
    // private String createJsonForForum(Forum forum, String username, String
    // parentCompType, String parentSysId)
    // {
    // StringBuffer json = new StringBuffer();
    // String displayName = SocialUtil.encodePost(forum.getDisplayName());
    // if (StringUtils.isEmpty(parentCompType))
    // {
    // parentCompType = FORUMS;
    // parentSysId = forum.getSys_id();
    // }
    //
    // Collection<User> childUsers = forum.getUsers();
    //
    // // sort
    // RSComponent.sortCollection(childUsers);
    //
    // json.append("{").append("\n");
    // json.append("   displayName: '").append(displayName).append("',").append("\n");
    // json.append("   comptype: '").append(FORUMS).append("',").append("\n");
    // json.append("   sysid: '").append(forum.getSys_id()).append("',").append("\n");
    // json.append("   user: '").append(username).append("',").append("\n");
    // // json.append("   expanded: true, ").append("\n");
    // json.append("   iconCls: 'task-folder'").append(",").append("\n");
    // json.append("   children: [").append("\n");
    //
    // // add the ONLY for this container
    // // json.append(createJsonLeaf(displayName + "-" + ONLY, PROCESS,
    // // forum.getSys_id(), owner.getUsername(), "",
    // // "")).append(",").append("\n");
    //
    // if (childUsers != null && childUsers.size() > 0)
    // {
    // Iterator<? extends RSComponent> it = childUsers.iterator();
    // while (it.hasNext())
    // {
    // User user = (User) it.next();
    //
    // json.append(createJsonLeaf(user.getDisplayName(), USERS,
    // user.getSys_id(), username, parentCompType, parentSysId));
    //
    // if (it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }// end of while
    // }// end of if
    //
    // json.append("   ]").append("\n");// close the children array
    // json.append("}").append("\n");
    //
    // return json.toString();
    // }// createJsonForProcess

    // //for Home Tab - Control Panel
    // private void collectAllObjects()
    // {
    // //process
    // if(processes != null && processes.size() > 0)
    // {
    // Iterator<Process> it = processes.iterator();
    // while(it.hasNext())
    // {
    // Process comp = it.next();
    // collectAllObjectsOfProcess(comp);
    // }//end of while
    // }//end of if
    //
    // addTeamsToContainer(teams);
    // addRunbooksToContainer(runbooks);
    // addDocumentsToContainer(documents);
    // addActionTaskToContainer(actiontasks);
    // //addUsersToContainer(users);
    // addRssToContainer(rsss);
    // addForumsToContainer(forums);
    //
    // }//collectAllObjecs();

    // private void collectAllObjectsOfProcess(Process process)
    // {
    // if(process != null)
    // {
    // allProcesses.add(process);
    //
    // addTeamsToContainer(process.getTeams());
    // addRunbooksToContainer(process.getRunbooks());
    // addDocumentsToContainer(process.getDocuments());
    // addActionTaskToContainer(process.getActiontasks());
    // addUsersToContainer(process.getUsers());
    // addRssToContainer(process.getRss());
    // addForumsToContainer(process.getForums());
    // }
    //
    // }//collectAllObjectsOfProcess

    // private void addTeamsToContainer(Collection<Team> comps)
    // {
    // if(comps != null && comps.size() > 0)
    // {
    // for(Team team : comps)
    // {
    // addTeamToContainer(team);
    // }
    // }
    // }//addTeamsToContainer

    // private void addTeamToContainer(Team team)
    // {
    // if(team != null)
    // {
    // allTeams.add(team);
    //
    // Collection<Team> childTeams = team.getTeams();
    // if(childTeams != null)
    // {
    // for(Team childTeam : childTeams)
    // {
    // addTeamToContainer(childTeam);//***RECURSIVE
    // }
    // }
    //
    // //add the users
    // addUsersToContainer(team.getUsers());
    // }
    // }//addTeamToContainer

    // private void addRunbooksToContainer(Collection<Runbook> comps)
    // {
    // if(comps != null && comps.size() > 0)
    // {
    // allRunbooks.addAll(comps);
    // }
    // }//addRunbooksToContainer
    //
    // private void addDocumentsToContainer(Collection<Document> comps)
    // {
    // if(comps != null && comps.size() > 0)
    // {
    // allDocuments.addAll(comps);
    // }
    // }//addRunbooksToContainer
    //
    // private void addActionTaskToContainer(Collection<ActionTask> comps)
    // {
    // if(comps != null && comps.size() > 0)
    // {
    // allActiontasks.addAll(comps);
    // }
    // }//addActionTaskToContainer
    //
    // private void addUsersToContainer(Collection<User> users)
    // {
    // if(users != null && users.size() > 0)
    // {
    // allUsers.addAll(users);
    // }
    // }//addUsersToContainer
    //
    // private void addRssToContainer(Collection<Rss> comps)
    // {
    // if(comps != null && comps.size() > 0)
    // {
    // allRsss.addAll(comps);
    // }
    // }//addRssToContainer
    //
    // private void addForumsToContainer(Collection<Forum> forums)
    // {
    // if(forums != null && forums.size() > 0)
    // {
    // allForums.addAll(forums);
    // }
    // }//addForumsToContainer

    private void preprocessDataStrutures()
    {
        sortAllCollections();
    }

    private void sortAllCollections()
    {
        RSComponent.sortCollection(processes);
        RSComponent.sortCollection(teams);
        RSComponent.sortCollection(forums);
        RSComponent.sortCollection(namespaces);
        RSComponent.sortCollection(users);
        RSComponent.sortCollection(actiontasks);
        RSComponent.sortCollection(runbooks);
        RSComponent.sortCollection(decisiontrees);
        RSComponent.sortCollection(documents);
        RSComponent.sortCollection(rsss);
    }

    // private String createComponentsListForCBInJson(Collection<? extends
    // RSComponent> children, String compType)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // if(children != null && children.size() > 0)
    // {
    // if(compType.equals(PROCESS))
    // {
    // // json.append(createJsonForProcesses(children, compType));
    // }
    // else if(compType.equals(TEAMS))
    // {
    // // json.append(createJsonForTeams(children, compType));
    // }
    // else
    // {
    // Iterator<? extends RSComponent> it = children.iterator();
    // String typeInfo = "(" + compType + ")";
    // while(it.hasNext())
    // {
    // RSComponent comp = it.next();
    // json.append(createJsonString(comp.getSys_id(), comp.getDisplayName() +
    // typeInfo, compType));
    //
    // if(it.hasNext())
    // {
    // json.append(",").append("\n");
    // }
    // }//end of while
    // }
    //
    // }//end of if
    //
    // return json.toString();
    // }//createJsonString
    //
    //
    // private String createJsonString(String sysid, String displayName, String
    // compType)
    // {
    // StringBuffer json = new StringBuffer();
    //
    // json.append("{");
    // json.append("   sysid: '").append(sysid).append("', ");
    // json.append("   displayName: '").append(displayName).append("', ");
    // json.append("   compType: '").append(compType).append("' ");
    // json.append("}");
    //
    // return json.toString();
    // }//createJsonString
    //

}
