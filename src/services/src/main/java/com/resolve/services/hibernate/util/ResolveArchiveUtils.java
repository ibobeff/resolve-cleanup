/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.CacheMode;
import org.hibernate.query.Query;
import org.owasp.esapi.ESAPI;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.dao.ResolveArchiveDAO;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.ResolveArchive;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveAssessRel;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolveParserRel;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.model.ResolvePreprocessRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.hibernate.vo.ResolveArchiveVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class ResolveArchiveUtils
{
    public static void submitArchiveRequest(String modelClassName, String attributeName, String sysId, String username)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put(HibernateConstants.ARCHIVE_CLASS_NAME, modelClassName);
        params.put(HibernateConstants.ARCHIVE_ATTRIBUTE_NAME, attributeName);
        params.put(HibernateConstants.ARCHIVE_SYS_ID, sysId);
        params.put(HibernateConstants.USERNAME, username);
        
        MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MAction.archive", params);
    }
    
    public static void submitPurgeArchiveRequest(String modelClassName, String sysId, String username)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put(HibernateConstants.ARCHIVE_CLASS_NAME, modelClassName);
        params.put(HibernateConstants.ARCHIVE_SYS_ID, sysId);
        params.put(HibernateConstants.USERNAME, username);
        
        MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MAction.purgeArchive", params);
    }
    
    @SuppressWarnings("rawtypes")
    public static void archive(String modelClassName, String attributeName, String sysId, String username) throws Exception
    {
        Class modelClass = Class.forName("com.resolve.persistence.model." + modelClassName);
        
        // full name - com.persistance....WikiDocument
        String objectType = modelClass.getName();

        // get the table name
        String tableName = HibernateUtil.class2Table(objectType);

        // get the column name
        String columnName = HibernateUtil.getColumnName(objectType, attributeName);
        
        // get the version from the archive table for this table and column and sys_id
        ResolveArchive example = new ResolveArchive();
        example.setUTableName(tableName);
        example.setUTableColumn(columnName);
        example.setUTableId(sysId);

        example = findLatestVersion(example);
        
        int version = example != null ? example.getUVersion() + 1 : 1;
        BaseModel<VO> data = GeneralHibernateUtil.findModelById(objectType, sysId);
        if(data != null)
        {
            String value = (String) PropertyUtils.getProperty(data, attributeName);
            
            //update the references also
            updateReferences(modelClassName, sysId, data, username);

            //create archive object
            ResolveArchive archive = new ResolveArchive();
            archive.setUTableName(tableName);
            archive.setUTableColumn(columnName);
            archive.setUTableId(sysId);
            archive.setUPatch(value);
            archive.setUVersion(version);
            archive.setSysCreatedBy(username);
            archive.setSysCreatedOn(GMTDate.getDate());
            
            //save the archive
            SaveUtil.saveResolveArchive(archive, username);
            
        }
    }
    
    @SuppressWarnings("rawtypes")
    public static void purgeArchive(String modelClassName, String sysIds, String username) throws Exception
    {
        if(StringUtils.isEmpty(modelClassName) || StringUtils.isEmpty(sysIds))
        {
            throw new Exception("modelclassname and sysIds to be deleted are mandatory");
        }
        
        Class modelClass = Class.forName("com.resolve.persistence.model." + modelClassName);
        
        // full name - com.persistance....WikiDocument
        String objectType = modelClass.getName();

        // get the table name
        String tableName = HibernateUtil.class2Table(objectType);
        
        try
        {
            

          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	
            	
            	Collection<String> tableIds = null;
                String inParamName = null;
            	String sql = "";
                if(sysIds.equalsIgnoreCase("all"))
                {
                    sql = "UTableName = '" + tableName + "'";
                }
                else
                {
                    String[] sysIdArray = sysIds.split(",");
                    
                    tableIds = new ArrayList<String>();
                    
                    for (String sysId : sysIdArray)
                    {
                        tableIds.add(sysId);
                    }
                    
                    inParamName = "UTableId";
                }
            	
            	HibernateUtil.deleteQuery("ResolveArchive", "UTableName", tableName, inParamName, tableIds );    
            });
                        

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        //purge the references table 
        purgeReferences(modelClassName, sysIds, username);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<ResolveArchiveVO> findArchivesFor(String modelClassName, String sysId, int start, int limit, String username) throws Exception
    {
		ResponseDTO<ResolveArchiveVO> result = new ResponseDTO<ResolveArchiveVO>();

		List<ResolveArchiveVO> list = new ArrayList<ResolveArchiveVO>();

		try {
			ESAPI.validator().getValidInput("Invalid archive model class name", modelClassName, "ResolveText", 255,
					false);

			// Class modelClass = Class.forName("com.resolve.persistence.model." +
			// modelClassName);
			// white list by checking if table name for class name exists
			String tableName = HibernateUtil.class2Table("com.resolve.persistence.model." + modelClassName);

			if (StringUtils.isBlank(tableName)) {
				throw new Exception("Invalid archive model class name " + modelClassName);
			}

        HibernateProxy.setCurrentUser(username);
			HibernateProxy.executeNoCache(() -> {
				List<ResolveArchive> rows = null;
				String hql = "";
				String countQry = "";
				int total = 0;
				try {
					hql = "from ResolveArchive where UTableName = '" + tableName + "' and UTableId = '" + sysId
							+ "' order by UVersion";
					countQry = "select COUNT(*) as COUNT " + hql;

					// get the data
					Query query = HibernateUtil.createQuery(hql);
					if (start > -1 && limit > -1) {
						query.setMaxResults(limit);
						query.setFirstResult(start);
					}
					rows = query.list();

					// for the count
					query = HibernateUtil.createQuery(countQry);
					List<Object> totalCount = query.list();
					if (totalCount.size() > 0) {
						total = Integer.valueOf(totalCount.get(0).toString());
					}

				} catch (Exception e) {
					Log.log.error("Error in executing HQL: " + hql, e);
					throw e;
				}
				
				if (rows != null) {
					for (ResolveArchive model : rows) {
						list.add(model.doGetVO());
					}

					result.setRecords(list);
					result.setTotal(total);
				}
			});

		} catch (Exception e) {
			throw e;
		}

		return result;

	}
    
    
    ///////////////////*/**************************************************
    
    ///private apis
    /**
     * Returns the latest record from the archive table. Used basically for the
     * latest version #
     * 
     * @param example
     *            - must set values for tablename, column name, tableId to get
     *            the correct results
     * @return
     */
    private static ResolveArchive findLatestVersion(ResolveArchive example)
    {
        ResolveArchive obj = new ResolveArchive();
        obj.setUVersion(0);

        // order by UVersion desc
        OrderbyProperty orderByVersion = new OrderbyProperty("UVersion", false);
        List<OrderbyProperty> orderByList = new ArrayList<OrderbyProperty>();
        orderByList.add(orderByVersion);

        try
        {
			obj = (ResolveArchive) HibernateProxy.execute(() -> {

				ResolveArchiveDAO dao = HibernateUtil.getDAOFactory().getResolveArchiveDAO();
				List<ResolveArchive> list = dao.find(example, orderByList);
				if (list != null && list.size() > 0) {
					return list.get(0);
				} else {
					return null;
				}

			});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return obj;
    }
    
    /**
     * 
     * @param modelClassName - ResolveAssessRel, 
     * @param sysId
     * @param data
     * @param username
     */
    private static void updateReferences(String modelClassName, String sysId, BaseModel<VO> data, String username)
    {
        try
        {
            if ((modelClassName.equalsIgnoreCase("ResolveAssess") 
                 || modelClassName.equalsIgnoreCase("ResolvePreprocess") 
                 || modelClassName.equalsIgnoreCase("ResolveParser"))
                && StringUtils.isNotEmpty(sysId))
            {
                Set<String> refNames = new HashSet<String>();

                String value = (String) PropertyUtils.getProperty(data, "UScript");
                if (StringUtils.isNotBlank(value))
                {
                    if (modelClassName.equalsIgnoreCase("ResolveAssess"))
                    {
                        //delete the current recs
                        //String sql = "assessor = '" + sysId + "'";
                        //ServiceHibernate.deleteQuery("ResolveAssessRel", sql, "system");
                        ServiceHibernate.deleteQuery("ResolveAssessRel", "assessor", sysId, "system");

                        //get the new ones and save it
                        refNames.addAll(ParseUtil.parseAssessorNamesFromScript(value));
                        saveResolveAssessRel(sysId, refNames, username);
                    }
                    else if (modelClassName.equalsIgnoreCase("ResolvePreprocess"))
                    {
                        //delete the current recs
                        //String sql = "preprocess = '" + sysId + "'";
                        //ServiceHibernate.deleteQuery("ResolvePreprocessRel", sql, "system");
                        ServiceHibernate.deleteQuery("ResolvePreprocessRel", "preprocess", sysId, "system");

                        //get the new ones and save it
                        refNames.addAll(ParseUtil.parsePreprocessorNamesFromScript(value));
                        saveResolvePreprocessRel(sysId, refNames, username);
                    }
                    else if (modelClassName.equalsIgnoreCase("ResolveParser"))
                    {
                        //delete the current recs
                        String sql = "parser = '" + sysId + "'";
                        //ServiceHibernate.deleteQuery("ResolveParserRel", sql, "system");
                        ServiceHibernate.deleteQuery("ResolveParserRel", "parser", sysId, "system");

                        //get the new ones and save it
                        refNames.addAll(ParseUtil.parseParserNamesFromScript(value));
                        saveResolveParserRel(sysId, refNames, username);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("error in creating references for " + modelClassName + " with sysId " + sysId, e);
        }
    }
    
    private static void purgeReferences(String modelClassName, String sysIds, String username)
    {
        String modelName = null;
        String sql = "";
        String queryString = null;
        String inParamName = null;
        Collection<String> ids = null;
        
        if(!sysIds.equalsIgnoreCase("all"))
        {
            String[] sysIdArray = sysIds.split(",");
            queryString = SQLUtils.prepareQueryString(sysIdArray);
        }

        //prepare the modelname and sql where clause
        if(modelClassName.equalsIgnoreCase("ResolveAssess"))
        {
            modelName = "ResolveAssessRel";
            if(queryString != null)
            {
                sql = "assessor IN (" + queryString + ")";
                inParamName = "assessor";
                ids = SQLUtils.convertQueryStringToSet(queryString);
            }            
        }
        else if(modelClassName.equalsIgnoreCase("ResolvePreprocess"))
        {
            modelName = "ResolvePreprocessRel";
            if(queryString != null)
            {
                sql = "preprocess IN (" + queryString + ")";
                inParamName = "preprocess";
                ids = SQLUtils.convertQueryStringToSet(queryString);
            }
        }
        else if(modelClassName.equalsIgnoreCase("ResolveParser"))
        {
            modelName = "ResolveParserRel";
            if(queryString != null)
            {
                sql = "parser IN (" + queryString + ")";
                inParamName = "parser";
                ids = SQLUtils.convertQueryStringToSet(queryString);
            }
        }
        
        //execute only if the modelname is not null
        if(modelName != null)
        {
        	String modelNameFinal = modelName;
        	String inParamNameFinal = inParamName;
        	Collection<String> idsFinal = ids;
        	
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		 HibernateUtil.deleteQuery(modelNameFinal, inParamNameFinal, idsFinal);   
            	});
                        

            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
    }
    
    
    
    private static void saveResolveAssessRel(String sysId, Set<String> refNames, String username) throws Exception
    {
        try
        {
            //insert new ones
            for(String refName : refNames)
            {
                ResolveAssess assess = new ResolveAssess();
                assess.setSys_id(sysId);
                
                ResolveAssessRel ref = new ResolveAssessRel();
                ref.setAssessor(assess);
                ref.setURefAssessor(refName);
                
                //persist it
                SaveUtil.saveResolveAssessRel(ref, "system");
            }
            
        }
        catch (Exception e)
        {
            throw e;
        }
    }
    
    private static void saveResolvePreprocessRel(String sysId, Set<String> refNames, String username) throws Exception
    {
        try
        {
            //insert new ones
            for(String refName : refNames)
            {
                ResolvePreprocess preprocess = new ResolvePreprocess();
                preprocess.setSys_id(sysId);
                
                ResolvePreprocessRel ref = new ResolvePreprocessRel();
                ref.setPreprocess(preprocess);
                ref.setURefPreprocess(refName);
                
                //persist it
                SaveUtil.saveResolvePreprocessRel(ref, "system");
            }
            
        }
        catch (Exception e)
        {
            throw e;
        }
    }
        
    private static void saveResolveParserRel(String sysId, Set<String> refNames, String username) throws Exception
    {
        try
        {
            //insert new ones
            for(String refName : refNames)
            {
                ResolveParser parser = new ResolveParser();
                parser.setSys_id(sysId);
                
                ResolveParserRel ref = new ResolveParserRel();
                ref.setParser(parser);
                ref.setURefParser(refName);
                
                //persist it
                SaveUtil.saveResolveParserRel(ref, "system");
            }
            
        }
        catch (Exception e)
        {
            throw e;
        }
    }
}
