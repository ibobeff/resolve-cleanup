/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.jdbc.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.DateTime;
import org.owasp.esapi.ESAPI;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.services.util.DateManipulator;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.BeanUtil;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultValidator;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;

/**
 * the idea of this class is to avoid to write the jdbc code everywhere
 * 
 * @author jeet.marwah
 *
 */
public class GenericJDBCHandler
{
    //static Set<String> tableList = new HashSet<String>();
    @Deprecated
    public static ConcurrentHashMap<String, String> tableMap = new ConcurrentHashMap<String, String>();
    
    @SuppressWarnings("rawtypes")
    private Class type;
    
    private String tableName = null;
    private Map<String, String> attributeToColumnMap = null;
    private Map<String, String> columnToAttributeMap = null;
    private String username = null;
    
    private List<Field> fields = null;

    
    public GenericJDBCHandler()
    {
    }
    
    /**
     * 
     * @param type - java class that you would like to represent your data. It shoulb be a VO/Bean object with getter-setters. Proper getter-setters should be there else this api will fail
     * @param tableName - actual database table name
     * @param attColMap - attribute - column mapping
     * @param username - 
     */
    @SuppressWarnings("rawtypes")
    public GenericJDBCHandler(Class type, String tableName, Map<String, String> attColMap, String username)
    {
        if(type == null || tableName == null && attColMap == null)
        {
            throw new RuntimeException("All parameters are mandatory");
        }
        
        this.type = type;
        this.tableName = tableName;
        this.attributeToColumnMap = attColMap;
        this.username = StringUtils.isNotEmpty(username) ? username : "system";

        
        this.fields = filterFields(BeanUtil.getAllFields(null, type));

    }
    
    /**
     * generic select using jdbc. Pass the sql and the columns you want to select and you can expect list of the objects populated with the data
     * 
     * eg.
     * Map<String, String> attColMap = MappingAttributeToColumnsUtil.getMappingForResolveEvent();
     * List t = new GenericJDBCHandler(ResolveEvent.class, "resolve_event", attColMap, username).selectObjects("select * from resolve_event", null);
     * 
     * @param sql - jdbc SQL, eg, select u_name, u_sys_Id from resolve_event where ....
     * @param selectColumns - list of columns in the select qry, eg. u_name, u_sys_id. If this is null, it will consider all the columns that are provided in the attributeToColumnMap
     * @return
     */
    public List<? extends Object> selectObjects(String sql, List<String> selectColumns)
    {
        List<Object> result = new ArrayList<Object>();
        
        if(StringUtils.isNotBlank(sql))
        {
            result.addAll(proceedToSelect(sql, selectColumns));
        }

        return result;
    }
    
    public static List<Map<String, Object>> executeSQLSelect(String sql, int start, int limit, boolean forUI) throws Exception
    {
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

        SQLConnection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        ResultSetMetaData rsmd = null;

        boolean getAllRecords = (start == -1) || limit == -1;// get all recs if
                                                             // any of them is
                                                             // not set
        int recStart = start;
        int recEnd = recStart + limit - 1;

        try
        {
            connection = SQL.getConnection();
            statement = connection.prepareStatement(
            						ESAPI.validator().getValidInput("Execute SQL Select Statement", 
            														SQLUtils.getSafeSQL(sql),
            														ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
            														ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
            														false, false));

            rs = statement.executeQuery();
            rsmd = rs.getMetaData();

            int numberOfColumns = rsmd.getColumnCount();
            String[] dbColumnNames = new String[numberOfColumns];
            for (int count = 0; count < numberOfColumns; count++)
            {
                // columns start with 1 , so count+1
                dbColumnNames[count] = rsmd.getColumnLabel(count + 1);
            }

            int recCount = 0;

            // while loop to get the data
            while (rs.next())
            {
                if (getAllRecords)
                {
                    Map<String, Object> row = getRowData(rs, dbColumnNames, forUI);
                    data.add(row);
                }
                else
                {
                    if (recCount >= recStart && recCount <= recEnd)
                    {
                        Map<String, Object> row = getRowData(rs, dbColumnNames, forUI);
                        data.add(row);
                    }
                    else if (recCount > recEnd)
                    {
                        break;
                    }
                }

                recCount++;

            }// end of while loop
        }
        catch (Throwable t)
        {
            Log.log.error("Error in fetching data for :" + sql, t);
            throw new Exception(t.getMessage());
        }
        finally
        {

            try
            {
                if (rs != null)
                {
                    rs.close();
                }

                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return data;
    }
    
    public static List<Map<String, Object>> executeSQLSelect(String sql, int start, int limit, boolean forUI, Map<Integer, Object> queryParams) throws Exception
    {
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

        SQLConnection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        ResultSetMetaData rsmd = null;

        boolean getAllRecords = (start == -1) || limit == -1;// get all recs if
                                                             // any of them is
                                                             // not set
        int recStart = start;
        int recEnd = recStart + limit - 1;

        try
        {
            connection = SQL.getConnection();
            statement = connection.prepareStatement(SQLUtils.getSafeSQL(sql));
            
            if (queryParams != null && !queryParams.isEmpty())
            {
                for (Integer idx : queryParams.keySet())
                {
                    statement.setObject(idx.intValue(), queryParams.get(idx));
                }
            }
            
            rs = statement.executeQuery();
            rsmd = rs.getMetaData();

            int numberOfColumns = rsmd.getColumnCount();
            String[] dbColumnNames = new String[numberOfColumns];
            for (int count = 0; count < numberOfColumns; count++)
            {
                // columns start with 1 , so count+1
                dbColumnNames[count] = rsmd.getColumnLabel(count + 1);
            }

            int recCount = 0;

            // while loop to get the data
            while (rs.next())
            {
                if (getAllRecords)
                {
                    Map<String, Object> row = getRowData(rs, dbColumnNames, forUI);
                    data.add(row);
                }
                else
                {
                    if (recCount >= recStart && recCount <= recEnd)
                    {
                        Map<String, Object> row = getRowData(rs, dbColumnNames, forUI);
                        data.add(row);
                    }
                    else if (recCount > recEnd)
                    {
                        break;
                    }
                }

                recCount++;

            }// end of while loop
        }
        catch (Throwable t)
        {
            Log.log.error("Error in fetching data for :" + sql, t);
            throw new Exception(t.getMessage());
        }
        finally
        {

            try
            {
                if (rs != null)
                {
                    rs.close();
                }

                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return data;
    }
    
    public static Map<String, String> getSQLMetaData(String tableName) throws Exception
    {
        Map<String, String> metaData = new HashMap<String, String>();

        if (StringUtils.isNotBlank(tableName))
        {
            SQLConnection connection = null;
            ResultSet rs = null;
//            Statement statement = null;
            PreparedStatement statement = null;
            ResultSetMetaData rsmd = null;
            String sql = "select * from " + tableName;
            try
            {
                connection = SQL.getConnection();
                statement = connection.prepareStatement(HibernateUtil.getValidQuery(sql));
                rs = statement.executeQuery();
                rsmd = rs.getMetaData();
                metaData.putAll(getMetaData(rsmd));
            }
            catch (Throwable t)
            {
                Log.log.error("Error in fetching data for :" + sql, t);
                throw new RuntimeException(t.getMessage());
            }
            finally
            {

                try
                {
                    if (rs != null)
                    {
                        rs.close();
                    }

                    if (statement != null)
                    {
                        statement.close();
                    }

                    if (connection != null)
                    {
                        connection.close();
                    }
                }
                catch (Throwable t)
                {
                }
            }
        }
        return metaData;
    }
    
    public static void executeSQLUpdate(String sql, String username) throws Exception
    {
        SQLConnection connection = null;
        PreparedStatement statement = null;

        try
        {
            connection = SQL.getConnection();

            Log.log.debug("Executing Update SQL :\n" + sql);
            connection.getConnection().setAutoCommit(false);

            statement = connection.prepareStatement(ESAPI.validator().getValidInput(
            														"Execute SQL Update Statement", 
            														SQLUtils.getSafeSQL(sql),
            														ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
            														ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH, 
            														false, false));
            
            statement.executeUpdate();
            connection.commit();
        }
        catch (Throwable t)
        {
            connection.getConnection().rollback();

            Log.log.error("Error in fetching data for :" + sql, t);
            throw new RuntimeException(t.getMessage());
        }
        finally
        {

            try
            {
                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }
    }
    
    private static Map<String, String> getMetaData(ResultSetMetaData rsmd) throws Exception
    {
        Map<String, String> metaData = new HashMap<String, String>();
        
        if(rsmd != null)
        {
            int numberOfColumns = rsmd.getColumnCount();
            for (int count = 1; count <= numberOfColumns; count++)
            {
                String colname = rsmd.getColumnName(count);
                String type = rsmd.getColumnTypeName(count);
    
                metaData.put(colname, type);
                // System.out.println(colname + " : " + type);
            }
        }
        
        return metaData;
    }

    
    /**
     * List of records to be inserted. Note that the list should have the objects of the same type that you have defined in the constructor
     * 
     * example:
     *      ResolveEvent data = new ResolveEvent();
            data.setUValue(msg);
            
            List<ResolveEvent> list = new ArrayList<ResolveEvent>();
            list.add(data);
            
            Map<String, String> attColMap = MappingAttributeToColumnsUtil.getMappingForResolveEvent();
            
            new GenericJDBCHandler(ResolveEvent.class, "resolve_event", attColMap, username).insertObjects(list);
     * 
     * 
     * @param list
     */
    public void insertObjects(List<? extends Object> list)
    {
        if(list != null && list.size() > 0)
        {
            Object test = list.get(0);
            if(type.isInstance(test))
            {
                proceedToInsert(list);
            }
        }
    }
    
    
    /**
     * List of records to be inserted. Note that the list should have the objects of the same type that you have defined in the constructor
     * 
     * example:
     *      ResolveEvent data = new ResolveEvent();
            data.setsys_id("8a9482e53b86d184013b86d2acc40066");
            data.setUValue("added by Jeet1");
            
            List<ResolveEvent> list = new ArrayList<ResolveEvent>();
            list.add(data);
            
            Map<String, String> attColMap = MappingAttributeToColumnsUtil.getMappingForResolveEvent();
            
            new GenericJDBCHandler(ResolveEvent.class, "resolve_event", attColMap, username).insertObjects(list);

     * 
     * 
     * @param list
     */
    public void updateObjects(List<? extends Object> list)
    {
        if(list != null && list.size() > 0)
        {
            Object test = list.get(0);
            if(type.isInstance(test))
            {
                proceedToUpdate(list);
            }
        }
        
    }
    

    ////////////////////////////////////////////////////////////////////////////////////////
    //private apis
    ////////////////////////////////////////////////////////////////////////////////////////
    private void proceedToUpdate(List<? extends Object> list)
    {
        PreparedStatement preparedStatement = null;
        SQLConnection connection = null;
        String sql = createUpdateQuery();
        Log.log.trace("Executing SQL :\n" + sql);
        
        List<Object> insertObjs = new ArrayList<Object>();
        
        try
        {
            connection = SQL.getConnection();
            preparedStatement = connection.prepareStatement(sql);

            connection.getConnection().setAutoCommit(false);
            
            for(Object o : list)
            {
                int i = 1;
                try
                {
                    String sysId = null; 
                    for (Field field : fields)
                    {
                        String attributeName = field.getName();
                        Object value = null;
                        try
                        {
                            if(attributeName.equalsIgnoreCase("sysCreatedBy") || attributeName.equalsIgnoreCase("sysCreatedOn"))
                            {
                                continue;//do not update the create fields
                            }
                            else if(attributeName.equalsIgnoreCase("sysUpdatedBy"))
                            {
                                value = username;
                            }
                            else if(attributeName.equalsIgnoreCase("sysUpdatedOn"))
                            {
                                value = new Timestamp(GMTDate.getDate(System.currentTimeMillis()).getTime());
                            }
                            else if(attributeName.equalsIgnoreCase("sysModCount"))
                            {
                                value = new Integer(0);
                            }
                            else
                            {
                                value = getValue(attributeName, o);
                            }
                            
                            if(attributeName.equalsIgnoreCase("sys_id"))
                            {
                                sysId = (value != null) ? (String) value : null;
                            }
                            else
                            {
                                preparedStatement.setObject(i++, value);
                            }
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Error in inserting data for :" + sql, e);
                        }
                    }//end of for
                    
                    //for the sysId 
                    if(StringUtils.isNotBlank(sysId))
                    {
                        //last to add 
                        preparedStatement.setObject(i++, sysId);
                    }
                    else
                    {
                        //do an insert
                        insertObjs.add(o);
                        continue;
                    }
                    
                    preparedStatement.addBatch();
                }
                catch (SecurityException e)
                {
                    Log.log.error("Error in inserting data for :" + sql, e);
                }
            }//end of for loop
            
            preparedStatement.executeBatch();
            connection.getConnection().commit();
        }
        catch (SQLException e)
        {
            Log.log.error("Error in inserting data for :" + sql, e);
            try
            {
                connection.getConnection().rollback();
            }
            catch (SQLException x)
            {
                
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in inserting data for :" + sql, e);
            try
            {
                connection.getConnection().rollback();
            }
            catch (SQLException y)
            {
                
            }
        }
        finally
        {
            try
            {
                if (preparedStatement != null)
                {
                    preparedStatement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }
        
        //if there are any objects for insert
        insertObjects(insertObjs);
        
    }

        
    private void proceedToInsert(List<? extends Object> list)
    {
        PreparedStatement preparedStatement = null;
        SQLConnection connection = null;
        String sql = createInsertQuery();
        Log.log.trace("Executing SQL :\n" + sql);
        
        try
        {
            connection = SQL.getConnection();
            preparedStatement = connection.prepareStatement(sql);

            connection.getConnection().setAutoCommit(false);
            
            for(Object o : list)
            {
                int i = 1;
                try
                {
                    for (Field field : fields)
                    {
                        String attributeName = field.getName();
                        Object value = null;
                        try
                        {
                            if(attributeName.equalsIgnoreCase("sys_id"))
                            {
                                value = SysId.getSysId();//for insert, create a new sysId
                            }
                            else if(attributeName.equalsIgnoreCase("sysCreatedBy") || attributeName.equalsIgnoreCase("sysUpdatedBy"))
                            {
                                value = username;
                            }
                            else if(attributeName.equalsIgnoreCase("sysCreatedOn") || attributeName.equalsIgnoreCase("sysUpdatedOn"))
                            {
                                value = new Timestamp(GMTDate.getDate(System.currentTimeMillis()).getTime());
                            }
                            else if(attributeName.equalsIgnoreCase("sysModCount"))
                            {
                                value = new Integer(0);
                            }
                            else
                            {
                                value = getValue(attributeName, o);
                            }
                            
                            preparedStatement.setObject(i++, value);
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Error in inserting data for :" + sql, e);
                        }
                    }
                    
                    preparedStatement.addBatch();
                }
                catch (SecurityException e)
                {
                    Log.log.error("Error in inserting data for :" + sql, e);
                }
            }//end of for loop
            
            preparedStatement.executeBatch();
            connection.getConnection().commit();
        }
        catch (SQLException e)
        {
            Log.log.error("Error in inserting data for :" + sql, e);
            try
            {
                connection.getConnection().rollback();
            }
            catch (SQLException x)
            {
                
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in inserting data for :" + sql, e);
            try
            {
                connection.getConnection().rollback();
            }
            catch (SQLException y)
            {
                
            }
        }
        finally
        {
            try
            {
                if (preparedStatement != null)
                {
                    preparedStatement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }
    }
    
    private List<? extends Object> proceedToSelect(String sql, List<String> selectColumns)
    {
        List<Object> result = new ArrayList<Object>();
        SQLConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        
        //create the map of colname-attribute
        createColToAttributeMap();
        
        //if its '*' than all the cols that are defined in the map
        boolean selectAllColumns = selectColumns != null && selectColumns.size() > 0 ? false : true;
        if(selectAllColumns)
        {
            selectColumns = getColumnsFromMap(this.attributeToColumnMap);
        }
        
        try
        {
            connection = SQL.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            
            while(resultSet.next())
            {
                Object instance = type.newInstance();
                
                for(String col : selectColumns)
                {
                    Object value = resultSet.getObject(col);
                    value = getMassagedData(value, false);

                    String attributeName = this.columnToAttributeMap.get(col);

                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(attributeName, type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }//end of for 
                
                result.add(instance);
            }//end of while
        }
        catch(Exception e)
        {
            Log.log.error("Error in selecting data for :" + sql, e);
        }
        finally
        {
            try
            {
                if(resultSet!=null)
                {
                    resultSet.close();
                }
                
                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return result;
    }
    
    public static Object getMassagedData(Object rawdata, boolean forUI)
    {
        Object massagedData = null;

        if (rawdata != null)
        {
            if (rawdata instanceof Timestamp)
            {
                if(forUI)
                {
                    Timestamp ts = (Timestamp) rawdata;
                    String dStr = DateManipulator.getDateInUTCFormat(ts);
                    DateTime dt = DateManipulator.parseISODate(dStr);
                    massagedData = dt.toDate();
                    // massagedData = DateManipulator.getDateInUTCFormat((Timestamp) rawdata); //THIS IS STRING...
                }
                else
                {
                    massagedData = (Timestamp) rawdata;
                }
                
            }
            else if (rawdata instanceof oracle.sql.TIMESTAMP)
            {
                oracle.sql.TIMESTAMP ts = (oracle.sql.TIMESTAMP)rawdata;
                
                try
                {
                    if(forUI)
                    {
                        massagedData = new Date(ts.dateValue().getTime());
                    }
                    else
                    {
                        massagedData = ts.dateValue();
                    }
                }
                catch (SQLException e)
                {
                    massagedData = null;
                }
            }
            else if(rawdata instanceof BigDecimal)
            {
                massagedData = ((BigDecimal)rawdata).doubleValue();
            }
            else if (rawdata instanceof oracle.sql.CLOB || rawdata instanceof java.sql.Clob)
            {
                massagedData = getStringFromClob(rawdata);
            }
            else if (rawdata instanceof oracle.sql.BLOB || rawdata instanceof java.sql.Blob)
            {
                // NOTE : To get the file , use the QueryCustomForm.downloadFile api
                massagedData = "";
            }
            else if (rawdata instanceof byte[])
            {
                // NOTE : To get the file , use the QueryCustomForm.downloadFile api
                massagedData = "";
            } 
            else
            {
                massagedData = rawdata;
            }
        }
        
        return massagedData;
    }
    
    protected String createInsertQuery()
    {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ");
        sql.append(tableName);
        sql.append("(");
        sql.append(getColumns(false));
        sql.append(")");
        sql.append(" VALUES (");
        sql.append(getColumns(true));
        sql.append(")");
        
        return sql.toString();
    }
    
    protected String createUpdateQuery()
    {
        StringBuilder sql = new StringBuilder();
        sql.append(" update ").append(tableName);
        sql.append(" set ");
        sql.append(getUpdateColumns());
        sql.append(" where sys_id = ?");
        
        return sql.toString();
    }

    //TODO
    protected String createDeleteQuery()
    {
        String sql = "";
        
        return sql;
    }
    
    private String getUpdateColumns()
    {
        StringBuilder sb = new StringBuilder();

        for (Field f : fields)
        {
            String attributeName = f.getName();
            String colName = this.attributeToColumnMap.get(attributeName);

            if(StringUtils.isNotBlank(colName))
            {
                if(colName.equalsIgnoreCase("sys_id") || attributeName.equalsIgnoreCase("sysCreatedBy") || attributeName.equalsIgnoreCase("sysCreatedOn"))
                {
                    continue;
                }
                
                sb.append(colName).append(" = ?").append(",");
            }
        }//end of for 
        
        String str = sb.substring(0, sb.length() - 1);
        
        
        return str;
    }


    protected String getColumns(boolean usePlaceHolders)
    {
        StringBuilder sb = new StringBuilder();

        boolean first = true;
        
        /* Iterate the column-names */
        for (Field f : fields)
        {
            String attributeName = f.getName();
            String colName = this.attributeToColumnMap.get(attributeName);

            if(StringUtils.isNotBlank(colName))
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    sb.append(", ");
                }
    
                if (usePlaceHolders)
                {
                    sb.append("?");
                }
                else
                {
                    sb.append(colName);
                }
            }
        }

        return sb.toString();

    }
    
    private List<Field> filterFields(List<Field> fields)
    {
        List<Field> result = new ArrayList<Field>();
        
        for (Field f : fields)
        {
            String attributeName = f.getName();
            String colName = this.attributeToColumnMap.get(attributeName);

            if(StringUtils.isNotBlank(colName))
            {
                result.add(f);
            }
        }
        
        return result;
    }
    
    private Object getValue(String attributeName, Object o) throws Exception
    {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(attributeName, type);
        Method method = propertyDescriptor.getReadMethod();
        Object value = method.invoke(o);
        
        return value;
    }
    
    private void createColToAttributeMap()
    {
        this.columnToAttributeMap = new HashMap<String, String>();
        Iterator<String> it = attributeToColumnMap.keySet().iterator();
        
        while(it.hasNext())
        {
            String attribute = it.next();
            String colName = attributeToColumnMap.get(attribute);
            
            this.columnToAttributeMap.put(colName, attribute);
        }
    }
    
    private List<String> getColumnsFromMap(Map<String, String> attColMap)
    {
        return new ArrayList<String>(this.columnToAttributeMap.keySet());
    }
    
    public static Map<String, Object> getRowData(ResultSet rs, String[] dbColumnNames, boolean forUI) throws Exception
    {
        Map<String, Object> row = new HashMap<String, Object>();

        // add the data to the row
        for (int count = 0; count < dbColumnNames.length; count++)
        {
            String column = dbColumnNames[count];
            Object rawdata = rs.getObject(column);
//            String clazz = rawdata != null ? rawdata.getClass().toString() : "";

            Object massagedData = getMassagedData(rawdata, forUI);

            row.put(column.toLowerCase(), massagedData);
        }// end of for loop

        return row;
    }
    
    public static int getTotalCountSafe(String tableName,String selectColumns,String whereClause
    		, ConfigSQL configSql) throws Exception
    {
        SQLConnection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        int totalCount = -1;
        String type = configSql.getDbtype();
        type = type.trim();
        
        if (StringUtils.isNotEmpty(tableName))
        {
            String countSql = null;
            if (StringUtils.isBlank(selectColumns)) {
                selectColumns = "* ";
            }
            if(StringUtils.isBlank(whereClause)) {
                whereClause = "";
            }else {
                whereClause = " where " + whereClause;
            }
            if(type.equalsIgnoreCase("mysql")) {
                countSql = "SELECT COUNT(*) as TOTALROWCOUNT FROM (SELECT " + selectColumns + "FROM " + tableName + whereClause + ") AS RESOLVECOUNT";
            }else {
                countSql = "SELECT COUNT(*) as TOTALROWCOUNT FROM (SELECT " + selectColumns + "FROM " + tableName + whereClause + ")";
            }
            try
            {
                connection = SQL.getConnection();
                statement = connection.prepareStatement(
                							ESAPI.validator().getValidInput(
                													"Get Total Count Safe", 
                													SQLUtils.getSafeSQL(countSql), 
                													ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
                													ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
                													false, false));
                
                rs = statement.executeQuery();
    
                if (rs.next())
                {
                    totalCount = rs.getInt("TOTALROWCOUNT");
                }// end of while loop
            }
            catch (Throwable t)
            {
                Log.log.error("Error in fetching data for :" + countSql, t);
                throw new Exception(t.getMessage());
            }
            finally
            {
                try
                {
                    if (rs != null)
                    {
                        rs.close();
                    }
    
                    if (statement != null)
                    {
                        statement.close();
                    }
    
                    if (connection != null)
                    {
                        connection.close();
                    }
                }
                catch (Throwable t)
                {
                }
            }
        }
        
        return totalCount;
    
    }
    
    @Deprecated
    public static int getTotalCount(String countSql) throws Exception
    {
        throw new Exception("Method getTotalCount(String countSql) has been replaced by getTotalCountSafe(tableName) method because of Veracode scan fix");
//        SQLConnection connection = null;
//        ResultSet rs = null;
//        Statement statement = null;
//        int totalCount = -1;
//
//        try
//        {
//            connection = SQL.getConnection();
//            statement = connection.createStatement();
//            rs = statement.executeQuery(countSql.trim());
//
//            if (rs.next())
//            {
//                totalCount = rs.getInt("COUNT");
//            }// end of while loop
//        }
//        catch (Throwable t)
//        {
//            Log.log.error("Error in fetching data for :" + countSql, t);
//            throw new RuntimeException(t.getMessage());
//        }
//        finally
//        {
//            try
//            {
//                if (rs != null)
//                {
//                    rs.close();
//                }
//
//                if (statement != null)
//                {
//                    statement.close();
//                }
//
//                if (connection != null)
//                {
//                    connection.close();
//                }
//            }
//            catch (Throwable t)
//            {
//            }
//        }
//
//        return totalCount;
    }
    
    //similar to HibernateUtil.java. This is duplicated so that the rsmgmt, who does not have persistence, can use this jdbc 
    private static String getStringFromClob(Object obj)
    {
        if (obj==null)
            return null;
        
        // With new Oracle driver, we can safely typecast oracle.sql.CLOB to java.sql.Clob
//        if (!(obj instanceof Clob))
//        {
//            throw new RuntimeException("Object is not a Clob: " + obj);
//        }
        
        Clob c = (Clob)obj;
        String s = null;
        try
        {
            s = StringUtils.readAll(c.getCharacterStream());
//            s=c.getSubString(1L, (int)c.length());//**This was throwing exception --> java.lang.RuntimeException: java.lang.UnsupportedOperationException: Blob may not be manipulated from creating session
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
        return s;
    }
    
    @Deprecated
    private static String dbType = "MYSQL";

    @Deprecated
    public static void setDBType(String t)
    {
        dbType = t;
    }
    
    @Deprecated
    public /*synchronized*/ static boolean hasTable(String table)
    {
        if (StringUtils.isEmpty(table))
        {
            return false;
        }
        else
        {
            /*if (tableMap/*List/.isEmpty())
            {
                updateTableMap/*List/();
            }*/
            
            return /*tableMap/*List*.contains(table/*.toUpperCase())*/true;
        }
    }
    
    @Deprecated
    public /*synchronized*/ static void updateTableMap/*List*/()
    {
        String sql = null;
        /*
        if(dbType == null || !dbType.equalsIgnoreCase("Oracle"))
        {
             sql = " SELECT table_name FROM user_tables  where 1 != ?";
        }
        else
        {
            sql = " SELECT TABLE_NAME FROM information_schema.TABLES where 1 != ?";
        }*/
        
        //List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

        SQLConnection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        //ResultSetMetaData rsmd = null;

        try
        {
            connection = SQL.getConnection();
            
            if(dbType == null || !dbType.equalsIgnoreCase("Oracle"))
            {
                sql = "SELECT table_name FROM user_tables";
                statement = connection.prepareStatement(SQLUtils.getSafeSQL("SELECT table_name FROM user_tables"));
            }
            else
            {
                sql = "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'resolve'";
                statement = connection.prepareStatement(SQLUtils.getSafeSQL("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'resolve'"));
            }
            
            rs = statement.executeQuery();
            
            //tableMap/*List*/ = new /*HashSet*/ConcurrentHashMap<String, String>();
            tableMap.clear();
            
            // while loop to get the data
            while (rs.next())
            {
                String table = rs.getString(1)/*.toUpperCase()*/; 
                tableMap/*List*/./*add*/put(table, table);
            }// end of while loop
        }
        catch (Throwable t)
        {
            Log.log.error("Error in fetching data for :" + sql, t);
            throw new RuntimeException(t.getMessage());
        }
        finally
        {

            try
            {
                if (rs != null)
                {
                    rs.close();
                }

                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
                Log.log.error(t,t);
            }
        }
    }

    @Deprecated
    public void refreshTableMap/*List*/(Map<String, String> params)
    {
        updateTableMap/*List*/();
    }
}
