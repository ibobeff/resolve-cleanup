package com.resolve.services.rr.util;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonNode;

import com.resolve.services.vo.QueryDTO;

import net.sf.json.JSONObject;

public class BaseBulkTask extends Thread
{
    protected String username;
    protected Cursor cursor;
    protected boolean shouldStop;
    protected BulkTaskStatusRecorder recorder;
    protected String type;
    private UUID tid;
    public static final String TASK_ID = "TASK_ID";
    public static final String ACTIVATION = "ACTIVATION";
    public static final String DEACTIVATION = "DEACTIVATION";
    public static final String EXPORT = "EXPORT";
    public static final String IMPORT = "IMPORT";
    
    public BaseBulkTask() {
        this.tid = UUID.randomUUID();
    }
   
    protected void init(List<String> ids,String username,String type) {
        this.cursor = new IdCursor(ids,username);
        this.username = username;
        this.recorder = new BulkTaskStatusRecorder();
        this.shouldStop = false;
        this.type = type;
    }
    protected void init(QueryDTO query,String username, String type) throws Exception {
        this.cursor = new QueryCursor(query, username);
        this.username = username;
        this.recorder = new BulkTaskStatusRecorder();
        this.shouldStop = false;
        this.type = type;
    }
    protected void init(String username, String type) {
        this.username = username;
        this.type = type;
        this.recorder = new BulkTaskStatusRecorder();
        this.shouldStop = false;
    }
    public LinkedList<JSONObject> getStatus() throws Exception {
        return this.recorder.getMessages();
    }
    public void stopTask() {
        this.shouldStop = true;
    }
    public boolean isStoping() {
        return this.shouldStop;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public double getPercentage() {
        return this.cursor.getPercentage();
    }
    public String getTid() {
        return this.tid.toString();
    }

    public String getUsername()
    {
        return username;
    }
    
}
