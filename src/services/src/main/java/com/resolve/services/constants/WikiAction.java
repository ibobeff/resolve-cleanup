/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.constants;

/**
 * Enum that defines the ACTIONS that can be taken on a wiki document
 * 
 * @author jeet.marwah
 *
 */
public enum WikiAction
{
	//just to view the toolbar page of the namespace, if not there then System.Toolbar page
	toolbar,
	
	//Navigate Mode
	listdoc, //list of docs in the current namespace - DONE, DONE
	listns, //list of all the namespaces 
	listrelated, //list of related docs having the same tag
	listrecent, //list the recent docs that were modified.	
	
	//View Mode
	view,//view/refresh a wiki doc - http://localhost:8080/resolve/service/wiki/view/Test/test1 - DONE, DONE
	viewsrc,//opens the wiki doc code in READ ONLY mode - http://localhost:8080/resolve/service/wiki/viewsrc/Test/test1 - DONE, DONE
	viewtoc, 
	navigate,//this is for the applet
	viewattach, //list the attachments of the wiki doc - http://localhost:8080/resolve/service/wiki/viewattach/Test/test1 - DONE, DONE
	attach,//attachment to a doc - http://localhost:8080/xwiki/bin/attach/Test/test1 - DONE, DONE
	deleteattach, //this is to delete an attachment - DONE, DONE
	info, //info like page last updated, outgoing links, incoming links, document tags, dependencies of the doc
	lookup, //used for lookup a regular exp - resolve_wiki_lookup

	//EDIT
	edit,//edit a wiki doc - http://localhost:8080/xwiki/bin/edit/Test/test1 - DONE, DONE
	save, //DONE, DONE
	saveandexit,//DONE, DONE
	cancel, //this is for when the doc is in EDIT mode and user clicks on Cancel
	preview,
	
	//Automate Mode
	execute, //execute a runbook
	editmodel,//edit in the AUTOMATION mode - http://localhost:8080/xwiki/bin/view/Test/test1?xpage=model
	editabort, //edit the exception model
	editfinal,//edit the finally model 
	savemodel,//save MODEL - from the MXGRAPH 
	saveabort,//save ABORT - from the MXGRAPH
	savefinal,//save FINAL - from the MXGRAPH
	savedtree,//save Decision Tree - from the MXGRAPH
	editmodelc,//edit in the AUTOMATION mode - http://localhost:8080/xwiki/bin/view/Test/test1?xpage=model -  XML code - DONE, DONE
	editexcpc, //edit the exception model XML code - DONE, DONE
	editfinalc,//edit the finally model  XML code - DONE, DONE
	savemodelc,//DONE, DONE
	saveexcpc,//DONE, DONE
	savefinalc,//DONE, DONE
	savedtreec,//DONE, DONE
//	semodelc, //save and exit for model code - Maybe we will not need this
	//seexcpc,//save and exit for exception code
	//sefinalc,//save and exit for final code
	viewmodel, //view the model -- http://localhost:8080/resolve/service/wiki/viewmodel/Test/test1
	viewmodelc, //view the XML model of the wiki doc in READ ONLY mode - http://localhost:8080/xwiki/bin/view/Test/test1?xpage=model-def - DONE, DONE
	viewexcpc, //view the exception model code in XML form - DONE, DONE
	viewfinalc, //view the finally block model code in XML form - DONE, DONE
	
	editdtree, //edit the decision tree

	//File Mode
	lock,
	unlock,
	delete, //deletes the doc - http://localhost:8080/xwiki/bin/delete/Test/test1
	move,//move or rename the doc - http://localhost:8080/xwiki/bin/view/System/Rename?srcweb=Test&srcpage=test1
	copy, //copy a doc - http://localhost:8080/xwiki/bin/view/System/Copy?srcweb=Test&srcpage=test1

	//TODO: still to decide
	viewmonitor, //view the monitor on the doc - http://localhost:8080/xwiki/bin/view/System/Monitor?action=VIEW
	setmonitor, //set monitor - http://localhost:8080/xwiki/bin/view/System/Monitor?action=SET&web=Test&page=test1
	rmmonitor, // remove a monitor - http://localhost:8080/xwiki/bin/view/System/Monitor?action=REMOVE&web=Test&page=test1
	
	revision,//view the doc history - http://localhost:8080/xwiki/bin/view/Test/test1?xpage=history
	//viewrev, //view the selected revision - may not need it as the 'view' with 'rev' parameter will do the same
	rollback,//action that rollbacks an older version of the doc
	reset, //reset the version of the doc - http://localhost:8080/xwiki/bin/reset/Test/test1
	diffrev, //difference in the revisions - DONE

	
	download, //to download an attachment - DONE, DONE
	downloadrev, //this is to download a rev of the document

	
	qsearch,//quick search, DONE, DONE
	asearch,//advance search
	
	viewhelp, //view documentation - http://localhost:8080/xwiki/bin/view/Doc/
	viewwiki, //view wiki syntax - http://localhost:8080/xwiki/bin/view/Doc/WikiSyntax
	
	impex//this is for import-export module
	
	
	;
	
	public static WikiAction getWikiAction(String action)
	{
		if(action == null || action.trim().length() == 0)
		{
			return null;
		}
		
		if(action.equals("toolbar"))
			return WikiAction.toolbar;
		else if(action.equals("listdoc"))
			return WikiAction.listdoc;
		else if(action.equals("listns"))
			return WikiAction.listns;
		else if(action.equals("listrelated"))
			return WikiAction.listrelated;
		else if(action.equals("listrecent"))
			return WikiAction.listrecent;
		else if(action.equals("view"))
			return WikiAction.view;
		else if(action.equals("viewsrc"))
			return WikiAction.viewsrc;
		else if(action.equals("viewtoc"))
			return WikiAction.viewtoc;
		else if(action.equals("navigate"))
			return WikiAction.navigate;
		else if(action.equals("viewattach"))
			return WikiAction.viewattach;
		else if(action.equals("attach"))
			return WikiAction.attach;
		else if(action.equals("deleteattach"))
			return WikiAction.deleteattach;
		else if(action.equals("info"))
			return WikiAction.info;
		else if(action.equals("edit"))
			return WikiAction.edit;
		else if(action.equals("save"))
			return WikiAction.save;
		else if(action.equals("saveandexit"))
			return WikiAction.saveandexit;
		else if(action.equals("cancel"))
			return WikiAction.cancel;
		else if(action.equals("preview"))
			return WikiAction.preview;
		else if(action.equals("execute"))
			return WikiAction.execute;
		else if(action.equals("editmodel"))
			return WikiAction.editmodel;
		else if(action.equals("editabort"))
			return WikiAction.editabort;
		else if(action.equals("editfinal"))
			return WikiAction.editfinal;
		else if(action.equals("savemodel"))
			return WikiAction.savemodel;
		else if(action.equals("saveabort"))
			return WikiAction.saveabort;
		else if(action.equals("savefinal"))
			return WikiAction.savefinal;
		else if(action.equals("savedtree"))
			return WikiAction.savedtree;
		else if(action.equals("editmodelc"))
			return WikiAction.editmodelc;
		else if(action.equals("editexcpc"))
			return WikiAction.editexcpc;
		else if(action.equals("editfinalc"))
			return WikiAction.editfinalc;
		else if(action.equals("savemodelc"))
			return WikiAction.savemodelc;
		else if(action.equals("saveexcpc"))
			return WikiAction.saveexcpc;
		else if(action.equals("savefinalc"))
			return WikiAction.savefinalc;
		else if(action.equals("savedtreec"))
			return WikiAction.savedtreec;
		else if(action.equals("viewmodelc"))
			return WikiAction.viewmodelc;
		else if(action.equals("viewexcpc"))
			return WikiAction.viewexcpc;
		else if(action.equals("viewfinalc"))
			return WikiAction.viewfinalc;
		else if(action.equals("editdtree"))
			return WikiAction.editdtree;
		else if(action.equals("lock"))
			return WikiAction.lock;
		else if(action.equals("unlock"))
			return WikiAction.unlock;
		else if(action.equals("delete"))
			return WikiAction.delete;
		else if(action.equals("move"))
			return WikiAction.move;
		else if(action.equals("copy"))
			return WikiAction.copy;
		else if(action.equals("viewmonitor"))
			return WikiAction.viewmonitor;
		else if(action.equals("setmonitor"))
			return WikiAction.setmonitor;
		else if(action.equals("rmmonitor"))
			return WikiAction.rmmonitor;
		else if(action.equals("revision"))
			return WikiAction.revision;
		else if(action.equals("rollback"))
			return WikiAction.rollback;
		else if(action.equals("reset"))
			return WikiAction.reset;
		else if(action.equals("diffrev"))
			return WikiAction.diffrev;
		else if(action.equals("download"))
			return WikiAction.download;
		else if(action.equals("downloadrev"))
			return WikiAction.downloadrev;
		else if(action.equals("qsearch"))
			return WikiAction.qsearch;
		else if(action.equals("asearch"))
			return WikiAction.asearch;
		else if(action.equals("viewhelp"))
			return WikiAction.viewhelp;
		else if(action.equals("viewwiki"))
			return WikiAction.viewwiki;
		else if(action.equals("impex"))
			return WikiAction.impex;
		else 
			return WikiAction.view;
			
	}
}
