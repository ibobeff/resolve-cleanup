/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.sir.util;

import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.persistence.model.SIRActivityMetaData;
import com.resolve.persistence.model.SIRActivityRuntimeData;
import com.resolve.persistence.model.SIRPhaseMetaData;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.PlaybookActivityUtils;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.util.GMTDate;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import static com.resolve.services.hibernate.vo.OrgsVO.*;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.*;
import static com.resolve.util.Log.*;
import static com.resolve.util.StringUtils.*;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;

/**
 * SIR Activity Utilities.
 * 
 * @author hemant.phanasgaonkar
 *
 */
public class ActivityUtils {
    
    private static final String UNIQUE_ACTIVITY_NAME_ADDENDUM_PREFIX = " (";
    private static final String UNIQUE_ACTIVITY_NAME_ADDENDUM_SUFFIX = ")";
    private static final String PERCENT = "%";
    private static final String SIR_ACTIVITY_METADATA_ACTIVITY_NAME_PROIPERTY = "name";
    
    private static final String CREATE_SIR_ACTGIVITY_METADATA_LOG = 
    															"createSIRActivityMetaData(%s, %b, %s, %b, %s, %s, %s)";
    private static final String FOUND_OR_NOTFOUND_MATCHING_SIR_ACTIVITY_METADATA_LOG =
    													"%s matching SIR Activity Meta Data %sbased on isRequired = " +
    													"%b, Ref Wiki Full Name = %s, SLA = %s, Org = %s, and Name = %s.";
    private static final String LEGACY_ACTIVITY_NAME_DUPLICATE_COUNT_LOG = 
    								"Legacy Activity Name Duplicate Count For Legacy Activity Name: %s and %s Org is %d.";
    private static final String UNIQUE_ACTIVITY_NAME_LOG = "Unique Activity Name: %s.";
    private static final String CREATED_NEW_SIR_ACTIVITY_METADATA_LOG = "Created New SIR Activity Meta Data: %s.";
    private static final String FOUND_EXISTING_ACTIVITY_MATCHING_SPECIFIED_DATA_LOG = 
    												"Found existing activity named %s with alternate activity id %s" +
    												" matching activity meta data for specified legacy activity named %s" + 
    												" and alternate activity id %s.";
    private static final String UPSERTED_SIR_ACTIVITY_RUNTIMEDATA_LOG = "Upserted Activity Runtime Data: %s.";
    private static final String FAILED_TO_FIND_META_PHASE_ID_LOG = "Failed to find phase meta data for meta phase id %s.";
    private static final String FAILED_TO_FIND_META_ACTIVITY_ID_LOG = 
    														"Failed to find activity meta data for meta activity id %s.";
    
    public static SIRActivityMetaData createSIRActivityMetaData(String legacyActivityName, String description, 
                                                                boolean isRequired, String refWikiFullName,
                                                                String refWikiId, boolean isTemplateActivity,
                                                                Duration sla, String altActiviyId, String orgId, 
                                                                String username) throws Exception {
        
        log.debug(String.format(CREATE_SIR_ACTGIVITY_METADATA_LOG, legacyActivityName, isRequired, refWikiFullName,
        						isTemplateActivity, sla, (isNotBlank(altActiviyId) ? altActiviyId : ""), 
        						(isBlank(orgId) ? NONE_ORG_NAME : orgId)));
        
        if (isBlank(refWikiFullName) && isBlank(refWikiId)) {
            throw new Exception("Missing refernced wiki full name as well as wiki id." + 
                                " Referenced wiki full name or wiki id is required.");
        }
        
        WikiDocument refWikiDoc = WikiUtils.getWikiDocumentModel(refWikiId, refWikiFullName, username, false);
        
        if (refWikiDoc == null) {
            throw new Exception("Missing refernced wiki document.");
        }
        
        // Check user's access to specified Org including None
        
        if (!UserUtils.isOrgAccessible2(orgId, username)) {
            throw new Exception("User does not have access to " + 
                                (isNotBlank(orgId) ? "specified" : NONE_ORG_NAME) + " Org.");
        }
        
        SIRActivityMetaData sirActivityMetaData = findSIRActivityMetaData(isRequired, refWikiDoc.getSys_id(), 
                                                                          sla, orgId, legacyActivityName, username, false);
        
        log.debug(String.format(FOUND_OR_NOTFOUND_MATCHING_SIR_ACTIVITY_METADATA_LOG, 
        						(sirActivityMetaData != null ? "Found" : "No"),
        						(sirActivityMetaData != null ? ": " + sirActivityMetaData + " " : ""),
        						isRequired, refWikiDoc.getUFullname(), sla,
        						(isBlank(orgId) ? OrgsVO.NONE_ORG_NAME : orgId),
        						legacyActivityName));
        
        if (sirActivityMetaData == null) {
            if (isBlank(legacyActivityName)) {
                throw new Exception("Missing activity name required to create new activity meta data.");
            }
            
            // Check if activity with similar sounding name exists
            
            sirActivityMetaData = findSIRActivityMetaData(isRequired, refWikiDoc.getSys_id(), 
                    									  sla, orgId, legacyActivityName, username, true);
            
            if (sirActivityMetaData == null) {
	            // Check for uniqueness of activity name per Org
	            
	            int activityNameDupCount = getLegacyActivityNameDupCount(legacyActivityName, orgId, username);
	            
	            log.debug(String.format(LEGACY_ACTIVITY_NAME_DUPLICATE_COUNT_LOG, legacyActivityName,
	                      (isNotBlank(orgId) ? "specified" : NONE_ORG_NAME), activityNameDupCount));
	            
	            String uniqueActivityName = legacyActivityName;
	            
	            if (activityNameDupCount >= 1) {
	                uniqueActivityName = legacyActivityName + UNIQUE_ACTIVITY_NAME_ADDENDUM_PREFIX + 
	                                     activityNameDupCount + UNIQUE_ACTIVITY_NAME_ADDENDUM_SUFFIX;
	            }
	            
	            log.debug(String.format(UNIQUE_ACTIVITY_NAME_LOG, uniqueActivityName));
	            
	            sirActivityMetaData = createSIRActivityMetaData(uniqueActivityName, description, isRequired, 
	                                                            refWikiDoc.getSys_id(), isTemplateActivity,
	                                                            legacyActivityName, sla, altActiviyId, orgId,
	                                                            username);
	            
	            log.debug(String.format(CREATED_NEW_SIR_ACTIVITY_METADATA_LOG, sirActivityMetaData));
            } else {
            	if (isNotBlank(legacyActivityName) && !sirActivityMetaData.getName().equals(legacyActivityName)) {            	
                    log.info(String.format(FOUND_EXISTING_ACTIVITY_MATCHING_SPECIFIED_DATA_LOG, sirActivityMetaData.getName(), 
                             			   sirActivityMetaData.getAltActivityId(), legacyActivityName, altActiviyId));
                }
            }
        } else {
            if (isNotBlank(legacyActivityName) && !sirActivityMetaData.getName().equals(legacyActivityName)) {            	
                log.info(String.format(FOUND_EXISTING_ACTIVITY_MATCHING_SPECIFIED_DATA_LOG, sirActivityMetaData.getName(), 
                         			   sirActivityMetaData.getAltActivityId(), legacyActivityName, altActiviyId));
            }
        }
        
        return sirActivityMetaData;
    }
    
    private static SIRActivityMetaData setupSIRActivityMetaData(String activityName, String description, 
                                                                boolean isRequired, String refWikiSysId, 
                                                                boolean isTemplateActivity, 
                                                                String legacyActivityName, 
                                                                Duration sla, String altActivityId, 
                                                                String orgId) {
        SIRActivityMetaData sirActivityMetaData = setupSIRActivityMetaData(isRequired, refWikiSysId, sla, orgId, activityName);
        
        if (sirActivityMetaData != null) {
            sirActivityMetaData.setName(activityName);
            sirActivityMetaData.setDescription(description);
            sirActivityMetaData.setIsTemplateActivity(Boolean.valueOf(isTemplateActivity));
            sirActivityMetaData.setLegacyName(legacyActivityName);
            sirActivityMetaData.setAltActivityId(altActivityId);
        }
        
        return sirActivityMetaData;
    }
    
    private static SIRActivityMetaData setupSIRActivityMetaData(boolean isRequired, String refWikiSysId, 
                                                                Duration sla, String orgId, String activityName) {
        
        SIRActivityMetaData sirActivityMetaData = new SIRActivityMetaData();
        
        sirActivityMetaData.setIsRequired(Boolean.valueOf(isRequired));
        sirActivityMetaData.setRefWikiSysId(refWikiSysId);
        
        if (sla != null && sla.toNanos() > 0) {
            sirActivityMetaData.setSLA(Duration.ofNanos(sla.toNanos()));
        } else {
            sirActivityMetaData.setSLA(SIRActivityMetaData.DEFAULT_SLA_DURATION);
        }
        
        if (isNotBlank(orgId)) {
            sirActivityMetaData.setSysOrg(orgId);
        }
        
        sirActivityMetaData.setName(activityName);
        
        return sirActivityMetaData;
    }
    
    public static SIRActivityMetaData findSIRActivityMetaData(boolean isRequired, String refWikiSysId, 
                                                              Duration sla, String orgId, String activityName,
                                                              String username, boolean activityNameLikeQuery) {
        
        SIRActivityMetaData sirActivityMetaData = null;
        
        String likeActivityName = activityNameLikeQuery ? 
        						  activityName + UNIQUE_ACTIVITY_NAME_ADDENDUM_PREFIX + PERCENT + 
        						  UNIQUE_ACTIVITY_NAME_ADDENDUM_SUFFIX :
        						  activityName;
        
        SIRActivityMetaData example = setupSIRActivityMetaData(isRequired, refWikiSysId, sla, orgId, likeActivityName);
                
        try {
              HibernateProxy.setCurrentUser(username);
        	sirActivityMetaData = (SIRActivityMetaData) HibernateProxy.execute(() -> {
            
	            if (activityNameLikeQuery) {
	            	List<String> likeQueryProperties = new ArrayList<String>();
	            	likeQueryProperties.add(SIR_ACTIVITY_METADATA_ACTIVITY_NAME_PROIPERTY);
	            	OrderbyProperty orderBySysCreatedOn = new OrderbyProperty("sysCreatedOn", true);
	                List<OrderbyProperty> orderByList = new ArrayList<OrderbyProperty>();
	                orderByList.add(orderBySysCreatedOn);
	            	return HibernateUtil.getDAOFactory().getSIRActivityMetaDataDAO().
							  			  findFirstLike(example, orderByList, likeQueryProperties, "isTemplateActivity");
	            } else {
	            	return HibernateUtil.getDAOFactory().getSIRActivityMetaDataDAO().
	            						  findFirst(example, "isTemplateActivity");
	            }
        	});
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return sirActivityMetaData;
    }
    
    public static SIRActivityMetaData createSIRActivityMetaData(String acitivtyName, String description, 
                                                                   boolean isRequired, String refWikiSysId, 
                                                                   boolean isTemplateActivity, 
                                                                   String legacyActivityName,
                                                                   Duration sla, String altActivityId, 
                                                                   String orgId, String username) {
        
        SIRActivityMetaData sirActivityMetaData = setupSIRActivityMetaData(acitivtyName, description, isRequired, 
                                                                           refWikiSysId, isTemplateActivity, 
                                                                           legacyActivityName, sla, altActivityId,
                                                                           orgId);
        SIRActivityMetaData newSIRActivityMetaData = null;
        
        try {
              HibernateProxy.setCurrentUser(username);
        	newSIRActivityMetaData = (SIRActivityMetaData) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getSIRActivityMetaDataDAO().insert(sirActivityMetaData);
	            /*
	             * Persist activity meta data record immediately irrespective of success/failure of transactions 
	             * enclosing this transaction (if any). This avoids duplicate activity meta data 
	             * i.e. Org + same activity legacy name or Org + reference wiki id + is Required + SLA 
	             * getting created when new activity legacy name is repeated in more than one activity in
	             * SIR template.    
	             */
            });
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        if (newSIRActivityMetaData != null && isBlank(newSIRActivityMetaData.getAltActivityId())) {
            newSIRActivityMetaData.setAltActivityId(newSIRActivityMetaData.getSys_id());
        }
        
        return newSIRActivityMetaData;
    }
    
    @SuppressWarnings("unchecked")
	protected static int getLegacyActivityNameDupCount(String legacyActivityName, String orgId, String username) {
        
        List<SIRActivityMetaData> sirActivitiesMetaData = null;
        
        SIRActivityMetaData example = new SIRActivityMetaData();
        
        example.setLegacyName(legacyActivityName);
        if (isNotBlank(orgId)) {
            example.setSysOrg(orgId);
        }
        
        // Do not remove "SLA" for some reasons Apache BeanUtil.describe returns property name for "sLA" as "SLA"
        
        try {
           
              HibernateProxy.setCurrentUser(username);
            sirActivitiesMetaData =
            		(List<SIRActivityMetaData>) HibernateProxy.execute(() -> {
            			return HibernateUtil.getDAOFactory().getSIRActivityMetaDataDAO().find(example, "isRequired", 
                                "isTemplateActivity",
                                "sLA", "SLA");
            		});
                            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return sirActivitiesMetaData != null ? sirActivitiesMetaData.size() : 0;
    }
    
    public static void upsertSIRActivitiesRuntimeData(ResolveSecurityIncident incident, String username) throws Exception {
        JSONArray activityList = PlaybookActivityUtils.getPbActivities(incident.getSys_id(), incident.getPlaybook(), 
                                                                       incident.getPlaybookVersion(), incident.getSysOrg(),
                                                                       username);
        
        if (activityList != null && activityList.size() > 0) {
            
            try {
                  HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		int currActivityIndex = 0;
                    String currMetaPhaseId = null;
	                for (int i=0; i<activityList.size(); i++) {
	                    JSONObject jsonActivity = (JSONObject)activityList.get(i);
	                    
	                    if (jsonActivity.containsKey(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
	                        StringUtils.isNotBlank(jsonActivity.getString(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
	                        jsonActivity.containsKey(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
	                        StringUtils.isNotBlank(jsonActivity.getString(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))
	                    {
	                        if (!jsonActivity.getString(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY).
	                             equals(currMetaPhaseId))
	                        {
	                            currActivityIndex = 0;
	                            currMetaPhaseId = jsonActivity.getString(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY);
	                        }
	                        
	                        currActivityIndex++;
	                        
	                        SIRActivityRuntimeData dbSIRActivityRuntimeData = 
	                                        findSIRActivityRuntimeData(
	                                                        jsonActivity.getString(
	                                                                        PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY),
	                                                         null,
	                                                         jsonActivity.getString(
	                                                                         PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY),
	                                                         Integer.valueOf(currActivityIndex),
	                                                         incident.getSys_id(),
	                                                         incident.getSysOrg(),
	                                                         username);
	                        
	                        boolean upserted = false;
	                        
	                        if (dbSIRActivityRuntimeData != null) {
	                            if (!dbSIRActivityRuntimeData.getPosition().equals(Integer.valueOf(currActivityIndex))) {
	                                upserted = true;
	                                dbSIRActivityRuntimeData.setPosition(Integer.valueOf(currActivityIndex));
	                                
	                                dbSIRActivityRuntimeData.setSysUpdatedBy(username);
	                                dbSIRActivityRuntimeData.setSysUpdatedOn(GMTDate.getDate());
	                                dbSIRActivityRuntimeData.setSysModCount(
	                                                Integer.valueOf(dbSIRActivityRuntimeData.getSysModCount().intValue() + 1));
	                            }
	                        } else {
	                            upserted = true;
	                            dbSIRActivityRuntimeData = 
	                                            setupSIRActivityRuntimeData(
	                                                    incident,
	                                                    jsonActivity.getString(
	                                                        PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY),
	                                                    jsonActivity.getString(
	                                                        PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY),
	                                                    Integer.valueOf(currActivityIndex));
	                            
	                            dbSIRActivityRuntimeData.setStatus(PLAYBOOK_ACTIVITY_RUNTIME_STATUS_DEFAULT_VALUE);
	                        }
	                        
	                        if (upserted) {
	                            SIRActivityRuntimeData upsertedSIRActivityRuntimeData = 
	                                        HibernateUtil.getDAOFactory().getSIRActivityRuntimeDataDAO().insertOrUpdate(
	                                                        dbSIRActivityRuntimeData);
	                            
	                            log.debug(String.format(UPSERTED_SIR_ACTIVITY_RUNTIMEDATA_LOG, upsertedSIRActivityRuntimeData));
	                        }
	                    }
	                }
	                });
                
            } catch (Throwable t) {
                log.error(t.getMessage(), t);
                                                    HibernateUtil.rethrowNestedTransaction(t);
                
            }
        }
    }
    
    private static SIRActivityRuntimeData setupSIRActivityRuntimeData(ResolveSecurityIncident incident,
                                                                      String sirPhaseMetaDataId, 
                                                                      String sirActivityMetaDataId, 
                                                                      Integer activityIndex) throws Exception
    {
        SIRPhaseMetaData sPMD = HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO().findById(sirPhaseMetaDataId);
        
        if (sPMD == null) {
            log.error(String.format(FAILED_TO_FIND_META_PHASE_ID_LOG, sirPhaseMetaDataId));
            throw new Exception("Failed to find phase meta data by meta phase id.");
        }
        
        SIRActivityMetaData sAMD = HibernateUtil.getDAOFactory().getSIRActivityMetaDataDAO().
                                   findById(sirActivityMetaDataId);
        
        if (sAMD == null) {
            log.error(String.format(FAILED_TO_FIND_META_ACTIVITY_ID_LOG, sirActivityMetaDataId));
            throw new Exception("Failed to find activity meta data by meta activity id.");
        }
        
        return setupSIRActivityRuntimeData(sPMD, sAMD, incident, activityIndex, incident.getSysOrg());
    }

    public static SIRActivityRuntimeData setupSIRActivityRuntimeData(SIRPhaseMetaData eSPMD, 
                                                                      SIRActivityMetaData eSAMD, 
                                                                      ResolveSecurityIncident eRSI,
                                                                      Integer sirActivityIndex,
                                                                      String orgId) {
        SIRActivityRuntimeData sirActivityRuntimeData = new SIRActivityRuntimeData();

        if (eSPMD != null) {
            sirActivityRuntimeData.setPhaseMetaData(eSPMD);
        }
        
        if (eSAMD != null) {
            sirActivityRuntimeData.setActivityMetaData(eSAMD);
        }
        
        sirActivityRuntimeData.setReferencedRSI(eRSI);
        
        if (sirActivityIndex != null && sirActivityIndex.intValue() > NON_NEGATIVE_INTEGER_DEFAULT) {
            sirActivityRuntimeData.setPosition(sirActivityIndex);
        }

        if (isNotBlank(orgId)) {
            sirActivityRuntimeData.setSysOrg(orgId);
        }

        return sirActivityRuntimeData;
    }
    
    public static SIRActivityRuntimeData findSIRActivityRuntimeData(String sirPhaseMetaDataId, 
                                                                    Integer sirPhaseIndex, 
                                                                    String sirActivityMetaDataId, 
                                                                    Integer sirActivityIndex, 
                                                                    String incidentId, String orgId, 
                                                                    String username)
    {
        SIRActivityRuntimeData sirActivityRuntimeData = null;
        
        SIRPhaseMetaData eSPMD = null;
        
        if (isNotBlank(sirPhaseMetaDataId)) {
            eSPMD = new SIRPhaseMetaData();
            eSPMD.setSysOrg(orgId);
            eSPMD.setSys_id(sirPhaseMetaDataId);
        }
        
       
        SIRActivityMetaData eSAMD = null;
        
        if (isNotBlank(sirActivityMetaDataId)) {
            eSAMD = new SIRActivityMetaData();
            eSAMD.setSysOrg(orgId);
            eSAMD.setSys_id(sirActivityMetaDataId);
        }
                
        ResolveSecurityIncident eRSI = new ResolveSecurityIncident();
        eRSI.setSysOrg(orgId);
        eRSI.setSys_id(incidentId);
        
        SIRActivityRuntimeData example = setupSIRActivityRuntimeData(eSPMD, eSAMD, eRSI, sirActivityIndex, orgId);
        
        try {
              HibernateProxy.setCurrentUser(username);
			sirActivityRuntimeData = (SIRActivityRuntimeData) HibernateProxy.execute(() -> {

				if (sirActivityIndex != null && sirActivityIndex.intValue() > NON_NEGATIVE_INTEGER_DEFAULT) {
					return HibernateUtil.getDAOFactory().getSIRActivityRuntimeDataDAO().findFirst(example);
				} else {
					return HibernateUtil.getDAOFactory().getSIRActivityRuntimeDataDAO().findFirst(example, "position");
				}
			});

        	if (sirActivityRuntimeData != null) {
        		// Load lazyily initialized objects
        		Hibernate.initialize(sirActivityRuntimeData.getPhaseMetaData());
        		Hibernate.initialize(sirActivityRuntimeData.getActivityMetaData());
        		Hibernate.initialize(sirActivityRuntimeData.getReferencedRSI());
        		Hibernate.initialize(sirActivityRuntimeData.getAssignedUser());
        	}
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return sirActivityRuntimeData;
    }
    
    @SuppressWarnings("unchecked")
	public static int getRuntimeDataActivityCount(String incidentId, String sirPhaseMetaDataId, 
                                                  Integer sirPhaseIndex, String orgId, String username) {
        
        List<SIRActivityRuntimeData> sirRuntimeDataPhaseActivities = null;
        
        ResolveSecurityIncident referencedRSI = new ResolveSecurityIncident();
        
        referencedRSI.setSys_id(incidentId);
        referencedRSI.setPrimary(null);
        referencedRSI.setSirStarted(null);
        
        if (isNotBlank(orgId)) {
            referencedRSI.setSysOrg(orgId);
        }
        
        SIRPhaseMetaData eSPMD = null;
        
        if (isNotBlank(sirPhaseMetaDataId)) {
            eSPMD = new SIRPhaseMetaData();
            
            if (isNotBlank(orgId)) {
                eSPMD.setSysOrg(orgId);
            }
            
            eSPMD.setSys_id(sirPhaseMetaDataId);
        }
        
        SIRActivityRuntimeData example = new SIRActivityRuntimeData();
        
        example.setPhaseMetaData(eSPMD);
        example.setReferencedRSI(referencedRSI);
        
        if (isNotBlank(orgId)) {
            example.setSysOrg(orgId);
        }
        
        try {

              HibernateProxy.setCurrentUser(username);
            sirRuntimeDataPhaseActivities = (List<SIRActivityRuntimeData>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getSIRActivityRuntimeDataDAO().find(example, "assignee",
                        "status", "position");
            });
                            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return sirRuntimeDataPhaseActivities != null ? sirRuntimeDataPhaseActivities.size() : 0;
    }
}
