/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.resolve.services.interfaces.DTO;

public class WikiPageInfoDTO extends DTO
{
    private static final long serialVersionUID = -2105692600544118910L;
    
    private String fullName;
    private String summary;
    private Integer version;
    private Date expireOn;
    private Date requestSubmissionOn;
    private Date lastReviewedOn;
    private String lastReviewedBy;    

    //stats
    private Integer viewed;
    private Integer edited;
    private Integer executed;

    //rating
    private double rating;

    //tags
    private Set<String> tags = new HashSet<String>();
    
    //catalog items
    private Set<String> catalogItems = new HashSet<String>();
    
    //Reference links - Document full names
    private Set<String> outgoingRefLinks = new HashSet<String>();
    private Set<String> incomingRefLinks = new HashSet<String>();
    
    //Runbook Dependencies
    //In Content
    private Set<String> atRefInContent = new HashSet<String>(); //actiontask reference in content
    
    //In Main Model
    private Set<String> atRefInMainModel = new HashSet<String>();// actiontask referenced in Main Model
    private Set<String> wikiMainModel = new HashSet<String>(); // wiki referenced in Main Model
    
    //In Abort Model
    private Set<String> atRefInAbortModel = new HashSet<String>();// actiontask referenced in Abort Model
    private Set<String> wikiAbortModel = new HashSet<String>(); // wiki referenced in Abort Model
    
    //In Decision tree
    private Set<String> wikiDecisionTreeModel = new HashSet<String>(); //wiki referenced in Decision Tree model

    public WikiPageInfoDTO() {}
    
    
    
    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public Integer getViewed()
    {
        return viewed;
    }

    public void setViewed(Integer viewed)
    {
        this.viewed = viewed;
    }

    public Integer getEdited()
    {
        return edited;
    }

    public void setEdited(Integer edited)
    {
        this.edited = edited;
    }

    public Integer getExecuted()
    {
        return executed;
    }
    public void setExecuted(Integer executed)
    {
        this.executed = executed;
    }
    
    public Date getExpireOn()
    {
        return expireOn;
    }

    public void setExpireOn(Date expireOn)
    {
        this.expireOn = expireOn;
    }

    public Date getRequestSubmissionOn()
    {
        return requestSubmissionOn;
    }

    public void setRequestSubmissionOn(Date requestSubmissionOn)
    {
        this.requestSubmissionOn = requestSubmissionOn;
    }

    public Date getLastReviewedOn()
    {
        return lastReviewedOn;
    }

    public void setLastReviewedOn(Date lastReviewedOn)
    {
        this.lastReviewedOn = lastReviewedOn;
    }

    public String getLastReviewedBy()
    {
        return lastReviewedBy;
    }

    public void setLastReviewedBy(String lastReviewedBy)
    {
        this.lastReviewedBy = lastReviewedBy;
    }

    public double getRating()
    {
        return rating;
    }

    public void setRating(double rating)
    {
        this.rating = rating;
    }

    public Set<String> getTags()
    {
        return tags;
    }

    public void setTags(Set<String> tags)
    {
        this.tags = tags;
    }
    
    public Set<String> getCatalogItems()
    {
        return catalogItems;
    }

    public void setCatalogItems(Set<String> catalogItems)
    {
        this.catalogItems = catalogItems;
    }

    public Set<String> getOutgoingRefLinks()
    {
        return outgoingRefLinks;
    }

    public void setOutgoingRefLinks(Set<String> outgoingRefLinks)
    {
        this.outgoingRefLinks = outgoingRefLinks;
    }

    public Set<String> getIncomingRefLinks()
    {
        return incomingRefLinks;
    }

    public void setIncomingRefLinks(Set<String> incomingRefLinks)
    {
        this.incomingRefLinks = incomingRefLinks;
    }

    public Set<String> getAtRefInContent()
    {
        return atRefInContent;
    }

    public void setAtRefInContent(Set<String> atRefInContent)
    {
        this.atRefInContent = atRefInContent;
    }

    public Set<String> getAtRefInMainModel()
    {
        return atRefInMainModel;
    }

    public void setAtRefInMainModel(Set<String> atRefInMainModel)
    {
        this.atRefInMainModel = atRefInMainModel;
    }

    public Set<String> getWikiMainModel()
    {
        return wikiMainModel;
    }

    public void setWikiMainModel(Set<String> wikiMainModel)
    {
        this.wikiMainModel = wikiMainModel;
    }

    public Set<String> getAtRefInAbortModel()
    {
        return atRefInAbortModel;
    }

    public void setAtRefInAbortModel(Set<String> atRefInAbortModel)
    {
        this.atRefInAbortModel = atRefInAbortModel;
    }

    public Set<String> getWikiAbortModel()
    {
        return wikiAbortModel;
    }

    public void setWikiAbortModel(Set<String> wikiAbortModel)
    {
        this.wikiAbortModel = wikiAbortModel;
    }

    public Set<String> getWikiDecisionTreeModel()
    {
        return wikiDecisionTreeModel;
    }

    public void setWikiDecisionTreeModel(Set<String> wikiDecisionTreeModel)
    {
        this.wikiDecisionTreeModel = wikiDecisionTreeModel;
    }
    
    
    

}
