/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.util.ArrayList;
import java.util.List;

import com.resolve.util.StringUtils;

public class DTReportDetailDTO
{
    private String dtFullName;
    private int count = 0;
    private double totalTimeInSecs;
    private DTReportStatusType status = null;// Possible values ==> COMPLETED, ABORTED
    private List<DTPathInfoDTO> paths = new ArrayList<DTPathInfoDTO>();
    
    public DTReportDetailDTO()
    {
        
    }

    public DTReportDetailDTO(List<DTRecord> records)
    {
        if(records != null)
        {
            DTRecord record0 = records.get(0);
            this.dtFullName = StringUtils.isNotEmpty(record0.getDtRootDoc()) ? record0.getDtRootDoc() : record0.getNodeId();
            this.count = record0.getTotalCount();
            this.status = record0.getStatus();
            
            long totalTimeInMS = 0;
            for(DTRecord record : records)
            {
                //filter out the '_Decision' dt docs
                if(record.getNodeId().endsWith("_Decision"))
                {
                    continue;
                }
                
                DTPathInfoDTO dto = new DTPathInfoDTO();
                dto.setDtFullName(record.getNodeId());
                dto.setTotalTimeInSecs(record.getTotalTime() / 1000d);
                paths.add(dto);
                
                totalTimeInMS += record.getTotalTime();
            }
            
            setTotalTimeInSecs(totalTimeInMS / 1000d);
        }
    }
    
    public String getDtFullName()
    {
        return dtFullName;
    }

    public void setDtFullName(String dtFullName)
    {
        this.dtFullName = dtFullName;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public DTReportStatusType getStatus()
    {
        return status;
    }

    public void setStatus(DTReportStatusType status)
    {
        this.status = status;
    }

    public List<DTPathInfoDTO> getPaths()
    {
        return paths;
    }

    public void setPaths(List<DTPathInfoDTO> paths)
    {
        this.paths = paths;
    }

    public double getTotalTimeInSecs()
    {
        return totalTimeInSecs;
    }

    public void setTotalTimeInSecs(double totalTimeInSecs)
    {
        this.totalTimeInSecs = totalTimeInSecs;
    }
    
}
