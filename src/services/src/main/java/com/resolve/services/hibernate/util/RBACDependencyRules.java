/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.HashMap;
import java.util.Map;

import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class RBACDependencyRules
{
    private Map<String, JSONObject> rulesMap = new HashMap<String, JSONObject>();
    
    public Map<String, JSONObject> getRules() throws Exception
    {
//        populateRule("sirDashboard", null);
//        populateRule("sirDashboardCharts", "{view: ['sirDashboard.view']}");
//        populateRule("sirDashboardInvestigationList", "{view: ['sirDashboard.view'], create: ['sirDashboard.view', 'sirDashboardInvestigationList.view']}");
//        populateRule("sirDashboardPlaybook", "{modify: ['sirDashboardInvestigationList.view']}");
//        populateRule("sirDashboardTitle", "{modify: ['sirDashboardInvestigationList.view']}");
//        populateRule("sirDashboardType", "{modify: ['sirDashboardInvestigationList.view']}");
//        populateRule("sirDashboardSeverity", "{modify: ['sirDashboardInvestigationList.view']}");
//        populateRule("sirDashboardStatus", "{modify: ['sirDashboardInvestigationList.view']}");
//        populateRule("sirDashboardDueDate", "{modify: ['sirDashboardInvestigationList.view']}");
//        populateRule("sirDashboardOwner", "{modify: ['sirDashboardInvestigationList.view']}");

//        populateRule("sirInvestigationViewer", null);
//        populateRule("sirInvestigationViewerDataCompromised", "{modify: ['sirInvestigationViewer.view']}");
//        populateRule("sirInvestigationViewerStatus", "{modify: ['sirInvestigationViewer.view']}");
//        populateRule("sirInvestigationViewerTeam", "{modify: ['sirInvestigationViewer.view']}");
        populateRule("sirInvestigationViewerActivities", "{create: ['sirInvestigationViewer.view'], modify: ['sirInvestigationViewer.view'], execute: ['sirInvestigationViewer.view']}");
        populateRule("sirInvestigationViewerNotes", "{create: ['sirInvestigationViewer.view']}");
        populateRule("sirInvestigationViewerArtifacts", "{create: ['sirInvestigationViewer.view'], delete: ['sirInvestigationViewer.view']}");
        populateRule("sirInvestigationViewerAttachments", "{create: ['sirInvestigationViewer.view'], modify: ['sirInvestigationViewer.view'], delete: ['sirInvestigationViewer.view']}");

//        populateRule("sirPlaybookTemplates", null);
//        populateRule("sirPlaybookTemplatesTemplateBuilder", "{view: ['sirPlaybookTemplates.view'], create: ['sirPlaybookTemplates.view', "
//                        + "'sirPlaybookTemplatesTemplateBuilder.view', 'sirPlaybookTemplatesTemplateBuilder.modify'], modify: ['sirPlaybookTemplates.view', 'sirPlaybookTemplatesTemplateBuilder.view']}");
        
        return rulesMap; 
    }
    
    private void populateRule(String key, String jsonRules) throws Exception
    {
        JSONObject roles = null;
        if (StringUtils.isNotBlank(jsonRules))
        {
            roles = StringUtils.stringToJSONObject(jsonRules);
        }
        rulesMap.put(key, roles);
    }
}
