/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;


public class Worksheet extends RSComponent implements RSPublisher
{
    private static final long serialVersionUID = 9042930222781326657L;
    
    public Worksheet()
    {
        super(NodeType.WORKSHEET);
       
    }
    
    /**
     * @param sysId --- the WorkSheet sys_id
     * @param name --- that is the WorkSheet PRB_NUMBER
     */
    public Worksheet(String sysId, String name) 
    {
        super(NodeType.WORKSHEET);
        setId(sysId);
        setName(name);
    }
    
    public Worksheet(ResolveNodeVO node) throws Exception
    {
        super(node);
    }
}