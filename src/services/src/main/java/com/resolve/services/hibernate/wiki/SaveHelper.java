/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.model.ResolveCatalogNodeWikidocRel;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.model.WikidocResolveTagRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionBuilder;
import com.resolve.services.ServiceTag;
import com.resolve.services.ServiceWiki;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiDocStatsCounterName;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.RBACUtils;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocAttachmentRelVO;
import com.resolve.services.rb.util.ResolutionBuilderUtils;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.GMTDate;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * 
 * Requirement:
 * ===========
When I rename a document that is part of a decision tree, it looks like it never rebuilds the link on the root page.

On the intranet I renamed ProdMan.Pricing to ProductMangement.Pricing and the decision tree model is updated, 
but the section in the doc still tries to call the old version of the page in the decision section. 
I had to go into that decision tree and save it again to have the wiki updated with the correct link.

 * @author jeet.marwah
 *
 */
public class SaveHelper
{
	public static final String FAILED_TO_SAVE_OBJECT_PREFIX = "Failed to save/add/delete/update ";
	public static final String FAILED_TO_SAVE_OBJECT_SUFFIX = " as license has expired.";	
	public static final String EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER = "object";
	public static final String SPACE = " ";
	public static final String FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX = 
														" as license is not entilted for use in High Availability (HA) setup.";
	public static final String FAILED_TO_SAVE_OBJECT_USER_COUNT_VIOLATED_SUFFIX = 
															" as number of named users has exceeded entitled number of users.";
	public static final String DIRECTLY_LINKED_TO_ITSELF = "directly linked to itself";
	
    private WikiDocumentVO docVO = null;
    private String username = null;

    private Set<String> userRoles = null;
    private boolean isAdminUser = false;
    private WikiDocument uiDoc = null;//doc reference that is mapped with the UI data, dirty/changed data
    private WikiDocument dbDoc = null;//current state of the document in the table

    //internal flags
    private boolean updateContent = true;
    private boolean updateMain = true;
    private boolean updateException = true;
    private boolean updateDTree = true;
    private boolean updateAccess = true;

    //switches
    private boolean isImportExportOperation = false;
    private boolean archive = true;
    private boolean reviewed = false;
    private boolean userShouldFollow = true;
    private boolean sendNotifications = true;
    private boolean overwrite = false; 
    private boolean rollback = false;
    private boolean reset = false;

    private String comment = null;
    
    @SuppressWarnings("unused")
	private boolean updateDTreeOptions = true;
    
    //for notification
    private List<SubmitNotification> submitNotifications = new ArrayList<SubmitNotification>();
    
    //list of tags
    private Set<String> tagsList = new HashSet<String>();
    
    //list of catalog paths assigned to this doc
    private Set<String> catalogItemPaths = new HashSet<String>();
    
    //list of attachments
    private Collection<WikidocAttachmentRelVO> attachmentsToAdd = null;
    
    //attachment models to be added to doc
    private Collection<WikiAttachment> attachments = null;
    
    public SaveHelper(WikiDocumentVO docVO, String username) throws Exception
    {
        if (docVO == null || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Doc and username are madatory for this operation");
        }
        
        if (!isImportExportOperation())
        {
            //validate the wiki name first
        	final WikiDocumentValidatorService validatorService = new WikiDocumentValidatorService();
        	try {
            	validatorService.validate(docVO);
            } catch(RuntimeException re) {
            	throw new Exception(docVO.getUFullname() + " is not a valid document. Aborting saving of Document.", re);
            }
        }
        
		// make sure there is no soft lock on the doc
		WikiDocLock lock = DocUtils.getWikiDocLockNonBlocking(docVO.getUFullname());
		if (lock != null && !lock.getUsername().equals(username)) {
			// DocUtils.requestSoftLock(username, docVO.getUFullname());
			throw new Exception("Save Failed : User has no lock for this document");
		}

        this.docVO = docVO;
        this.username = username;

        uiDoc = new WikiDocument(docVO);
        attachmentsToAdd = docVO.getWikidocAttachmentRels();
        
        init();
    }
    
    public SaveHelper(WikiDocument doc, String username) throws Exception
    {
        if (doc == null || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Doc and username are madatory for this operation");
        }
        
        this.uiDoc = doc;
        this.username = username;
            
        //for validation
        docVO = doc.doGetVO();
        final WikiDocumentValidatorService validatorService = new WikiDocumentValidatorService();
    	try {
        	validatorService.validate(docVO);
        } catch(RuntimeException re) {
        	throw new Exception(docVO.getUFullname() + " is not a valid document. Aborting saving of Document.", re);
        }
        
        //prepare the list of attachments
        prepareAttachmentList(doc);
        
        init();
    }
    
    private void prepareAttachmentList(WikiDocument doc)
    {
        Collection<WikidocAttachmentRel> attachRels = doc.getWikidocAttachmentRels();
        if(attachRels != null)
        {
            this.attachments = new ArrayList<WikiAttachment>();
            for(WikidocAttachmentRel rel : attachRels)
            {
                this.attachments.add(rel.getWikiAttachment());
            }
        }
    }

    public SaveHelper(WikiDocument doc, boolean isImport, String username) throws Exception
    {
        setImportExportOperation(isImport);
        if (doc == null || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Doc and username are madatory for this operation");
        }
        
        this.uiDoc = doc;
        this.docVO = doc.doGetVO();

        this.username = username;
        
        //prepare the list of attachments
        prepareAttachmentList(doc);
        
        init();
    }
    
    private void preSaveDTValidation() throws Exception {
    	if (StringUtils.isNotBlank(docVO.getUDecisionTree()) &&
    		docVO.getUDecisionTree().contains(String.format("<Document label=\"%s\"", docVO.getUFullname()))) {
    		String errMsg = String.format("%s decision tree. Decision Tree %s is %s and " +
					   					  "cannot be saved in its current form.", 
					   					  FAILED_TO_SAVE_OBJECT_PREFIX, docVO.getUFullname(), 
					   					  DIRECTLY_LINKED_TO_ITSELF);
    		
    		if (Log.log.isDebugEnabled()) {
    			Log.log.debug(String.format("Pre Save DT Validation Failed. Error Message: [%s]", errMsg));
    		}
    		
    		throw new Exception (errMsg);
    	}
    }
    
    public WikiDocumentVO save() throws Exception
    {
    	// If DT validate that it is not linking directly to itself
    	
    	preSaveDTValidation();
    	// Check for expired V2 license in NON-PROD environments/instance types
    	
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndExpiredAndNonProd() &&
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10024.getMessage();
    		
    		if (docVO != null && StringUtils.isNotBlank(docVO.getUDisplayMode())) {
    			msg = msg.replaceFirst(EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
    								   docVO.getUDisplayMode() + SPACE + "\"" + docVO.getUFullname() + "\"");
    		}
    		
    		Log.alert(ERR.E10024.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    	
    	// Check for User Count violated V2 license in NON-PROD environments/instances
    	
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndNonProd() &&
    		MainBase.main.getLicenseService().getIsUserCountViolated() &&
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10031.getMessage();
    		
    		if (docVO != null && StringUtils.isNotBlank(docVO.getUDisplayMode())) {
    			msg = msg.replaceFirst(EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
    								   docVO.getUDisplayMode() + SPACE + "\"" + docVO.getUFullname() + "\"");
    		}
    		
    		Log.alert(ERR.E10031.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    	
    	// Check for HA violated V2 license in NON-PROD environments/instances
    	
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndNonProd() &&
    		MainBase.main.getLicenseService().getIsHAViolated() &&
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10030.getMessage();
    		
    		if (docVO != null && StringUtils.isNotBlank(docVO.getUDisplayMode())) {
    			msg = msg.replaceFirst(EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
    								   docVO.getUDisplayMode() + SPACE + "\"" + docVO.getUFullname() + "\"");
    		}
    		
    		Log.alert(ERR.E10030.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    	
        saveWikiDocumentModel();
        
        //index the doc
        ServiceWiki.indexWikiDocument(uiDoc.getSys_id(), username);

        docVO = ServiceWiki.getWikiDocWithGraphRelatedData(uiDoc.getSys_id(), null, username);
        
        return docVO;
    }

    public boolean isImportExportOperation()
    {
        return isImportExportOperation;
    }

    public void setImportExportOperation(boolean isImportExportOperation)
    {
        this.isImportExportOperation = isImportExportOperation;
        
        //if its an Impex operation, than set the users follow to false
        setUserShouldFollow(!isImportExportOperation);
        
        //if its impex, than disable archive 
        setArchive(!isImportExportOperation);        
    }

    public boolean isArchive()
    {
        return archive;
    }

    public void setArchive(boolean archive)
    {
        this.archive = archive;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public boolean isReviewed()
    {
        return reviewed;
    }

    public void setReviewed(boolean reviewed)
    {
        this.reviewed = reviewed;
    }

    public boolean isUserShouldFollow()
    {
        return userShouldFollow;
    }

    public void setUserShouldFollow(boolean userShouldFollow)
    {
        this.userShouldFollow = userShouldFollow;
    }

    public boolean isSendNotifications()
    {
        return sendNotifications;
    }

    public void setSendNotifications(boolean sendNotifications)
    {
        this.sendNotifications = sendNotifications;
    }

    public Collection<WikiAttachment> getAttachments()
    {
        return attachments;
    }

    public void setAttachments(Collection<WikiAttachment> attachments)
    {
        this.attachments = attachments;
    }
    
    public boolean isOverwrite()
    {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite)
    {
        this.overwrite = overwrite;
    }
    
    public boolean isRollback()
    {
        return rollback;
    }

    public void setRollback(boolean rollback)
    {
        this.rollback = rollback;
    }
    
    public boolean isReset()
    {
        return reset;
    }

    public void setReset(boolean reset)
    {
        this.reset = reset;
    }

    //private apis
    private void saveWikiDocumentModel() throws Exception
    {
        String sysId = uiDoc.getSys_id();
        
        boolean sirTemplate = StringUtils.isNotBlank(uiDoc.getUDisplayMode()) && 
                              uiDoc.getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK) &&
                              uiDoc.ugetUIsTemplate().booleanValue();
        
        //update
        if (StringUtils.isNotBlank(sysId))
        {
            if (sirTemplate)
            {
                if (!UserUtils.getUserfunctionPermission(username, 
                		RBACUtils.RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER_CHANGE, null))
                {
                    throw new Exception(String.format("User %s is denied access to modify this security template.", 
                    								  username));
                }
            }
            update();
        }
        else
        {
            if (sirTemplate)
            {
                if (!UserUtils.getUserfunctionPermission(username, 
                		RBACUtils.RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER_CHANGE, null))
                {
                    throw new Exception(String.format("User %s is denied access to create new security template.", 
                    								  username));
                }
            }
            //insert
            insert();
        }
        
        docVO = ServiceWiki.getWikiDoc(uiDoc.getSys_id(), null, username);
        WikiUtils.updateWikiChecksum(docVO, username);
        
        //send jms msgs
        sendMessagesToESB();
    }

    private void insert() throws Exception
    {
        String name = uiDoc.getUName();
        String namespace = uiDoc.getUNamespace();
        
        // A check is needed in case Wiki save controller API is not getting called from UI.
        if (StringUtils.isBlank(name) || StringUtils.isBlank(namespace))
        {
            throw new Exception("Document name and namespace is mandatory.");
        }
        
        String fullName = namespace + "." + name;

        //make sure that the doc does not exist with the same name
        boolean docExist = WikiUtils.isWikidocExist(fullName, false);
        if (docExist)
        {
            throw new Exception("Document with name '" + fullName + "' already exist.");
        }
        
        // Check for bad data (missing rb_general fk in doc) and if found delete it
        
        List<RBGeneralVO> rbGeneralVOs = ResolutionBuilderUtils.findRBGeneralByUniqueKey(uiDoc.getUNamespace(), uiDoc.getUName(), username);
        
        if(rbGeneralVOs != null && !rbGeneralVOs.isEmpty())
        {
            ServiceResolutionBuilder.deleteGeneral(rbGeneralVOs.iterator().next().getSys_id(), false, username);
        }
        
        //check if the document namespace exist. If yes, than use that one to avoid NS duplication
        updateNamespace();
        

        uiDoc.setUVersion(1);

        //update the db
        upsert();
        
        //social operations for wiki
        socialOperations(true);

    }

    private void update() throws Exception
    {
        String sysId = uiDoc.getSys_id();
        dbDoc = WikiUtils.getWikiDocumentModel(sysId, null, username, false, true);
        if(dbDoc == null)
        {
            throw new Exception("Document with sysId " + sysId + " does not exist.");
        }
        
        // Ignore locked flag if it's an import operation
        if(dbDoc.ugetUIsLocked() && !isImportExportOperation)
        {
            throw new Exception("Document " + dbDoc.getUFullname() + " is Locked. Please Unlock it before saving.");
        }

        if(dbDoc.ugetUIsDeleted())
        {
            throw new Exception("Document " + dbDoc.getUFullname() + " is marked Deleted. Please Undelete it before saving.");
        }

        
        //check if the user has edit rights for this document
        if(dbDoc.getAccessRights() != null)
        {
        	if(dbDoc.getAccessRights().doGetVO().equals(uiDoc.getAccessRights().doGetVO()))
        	{
        		updateAccess = false;
        	}
            String editRoles = dbDoc.getAccessRights().getUWriteAccess() + ","+  dbDoc.getAccessRights().getUAdminAccess();
            boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(username, userRoles, editRoles);
            if(!userCanEdit)
            {
                throw new Exception("User does not have Edit rights for the document " + dbDoc.getUFullname());
            }
        }
        
        //validate the document if its locked or deleted or hidden, etc
        boolean createdNewDocument = validateBeforeSave();
        if(!createdNewDocument)
        {
          //set the flags 
          setFlagsForChanges();

          //increment the version
          if(isReset())
              uiDoc.setUVersion(1);
          else
              uiDoc.setUVersion(dbDoc.getUVersion() + 1);
          
          
          //update the sys created for doc and access rights
          uiDoc.setSysCreatedBy(dbDoc.getSysCreatedBy());
          uiDoc.setSysCreatedOn(dbDoc.getSysCreatedOn());
          uiDoc.getAccessRights().setSysCreatedBy(dbDoc.getAccessRights().getSysCreatedBy());
          uiDoc.getAccessRights().setSysCreatedOn(dbDoc.getAccessRights().getSysCreatedOn());
          
          //update the other flags not set through UI
          
          uiDoc.setUHasActiveModel(dbDoc.getUHasActiveModel());
         boolean hasResolutionBuilder = (dbDoc.getUHasResolutionBuilder() != null && 
                                          dbDoc.getUHasResolutionBuilder().booleanValue()); /*|| 
                                         (uiDoc.getUModelProcess() != null && 
                                          uiDoc.getUModelProcess().contains("<Start"));*///RBA-12998: Commneted this as all automations are marked builder beasuse of this
          uiDoc.setUHasResolutionBuilder(Boolean.valueOf(hasResolutionBuilder));
          
          //update the summary since it gets overwritten with title if null
          /*
          if (!isImportExportOperation())
          {
              uiDoc.setUSummary(dbDoc.getUSummary());
          }
          */
          
          uiDoc.usetPbActivities(dbDoc.ugetPbActivities());
          
          //update the db
          upsert();
          
          //social operations for wiki
          socialOperations(false);
        }
        
    }
    
    /*
     * Commented removeDTSections because this is no longer needed.
     * It should get called only during wiki 'Save As...' operation, for which we alread have an API in CopyHelper. 
     */
//    private String removeDTSections(String content)
//    {
//        StringBuilder sb = new StringBuilder();
//        int fromIndx = 0;
//        int sectionStartIndx = -1;
//        
//        if (StringUtils.isBlank(content))
//        {
//            return sb.toString();
//        }
//        
//        String lowerCaseContent = content.toLowerCase();
//        
//        do
//        {
//            sectionStartIndx = lowerCaseContent.indexOf(DT_SECTION_PREFIX, fromIndx);
//            
//            if (sectionStartIndx != -1)
//            {
//                String dtSectionSuffix = DT_SECTION_SUFFIX;
//                
//                int endIndx = lowerCaseContent.indexOf(dtSectionSuffix, sectionStartIndx + DT_SECTION_PREFIX.length());
//                
//                if (endIndx <= (sectionStartIndx + DT_SECTION_PREFIX.length() - 1))
//                {
//                    dtSectionSuffix = DT_SECTION_SUFFIX_ALTERNATE;
//                    endIndx = lowerCaseContent.indexOf(dtSectionSuffix, sectionStartIndx + DT_SECTION_PREFIX.length());
//                    
//                    if (endIndx <= (sectionStartIndx + DT_SECTION_PREFIX.length() - 1))
//                    {
//                        break;
//                    }
//                }
//                
//                String docFullName = uiDoc.getUFullname().endsWith(DT_VIRTUAL_DOC_SUFFIX) ? 
//                                     StringUtils.substringBefore(uiDoc.getUFullname(), DT_VIRTUAL_DOC_SUFFIX) : uiDoc.getUFullname();
//                
//                sb.append(content.substring(fromIndx, sectionStartIndx));
//                                     
//                if (lowerCaseContent.substring(sectionStartIndx + DT_SECTION_PREFIX.length()).startsWith(docFullName.toLowerCase()))
//                {
//                    sb.append(content.substring(sectionStartIndx, endIndx + dtSectionSuffix.length()));
//                }
//                
//                fromIndx = endIndx + dtSectionSuffix.length();
//            }
//        } while (sectionStartIndx != -1 && fromIndx < lowerCaseContent.length());
//        
//        if (fromIndx < lowerCaseContent.length())
//        {
//            sb.append(content.substring(fromIndx));
//        }
//                
//        return sb.toString();
//    }
    
    //common operations for insert and update
    private void upsert() throws Exception
    {
        String displayType = uiDoc.getUDisplayMode();
    	
        //set the organization sysId for the document
        UsersVO user = ServiceHibernate.getUser(username);
        OrganizationVO userOrg = user.getBelongsToOrganization();
        if(userOrg !=null)
        {
            uiDoc.setSysOrg(userOrg.getSys_id());
        }
        
        //set the flag for runbook
        if (StringUtils.isBlank(uiDoc.getUModelProcess()) || !uiDoc.getUModelProcess().contains("<Start"))
        {
            uiDoc.setUHasActiveModel(false);
            Log.log.trace("saveWikiDocument the Model xml is blank");
        }
        else
        {
            uiDoc.setUModelProcess(uiDoc.getUModelProcess().replace("\r", ""));
            uiDoc.setUHasActiveModel(true);
        }
        
        // set the flag for decision tree
        if (StringUtils.isBlank(uiDoc.getUDecisionTree()) || !uiDoc.getUDecisionTree().contains("<Start"))
        {
            uiDoc.setUIsRoot(false);
            Log.log.trace("saveWikiDocument the DecisionTree xml is blank");
        }
        else
        {
            uiDoc.setUDecisionTree(uiDoc.getUDecisionTree().replace("\r", ""));
            uiDoc.setUIsRoot(true);
        }
        
        //if the reviewed flag is set
        if(isReviewed())
        {
            uiDoc.setULastReviewedOn(GMTDate.getDate());
            uiDoc.setUReqestSubmissionOn(null);
            uiDoc.setUIsRequestSubmission(false);
        }
        
        //update the summary
        if (StringUtils.isBlank(uiDoc.getUSummary()))
        {
            if(StringUtils.isNotBlank(uiDoc.getUTitle()))
            {
                uiDoc.setUSummary(uiDoc.getUTitle());
            }
            else
            {
                uiDoc.setUSummary(uiDoc.getUFullname());
                uiDoc.setUTitle(uiDoc.getUFullname());
            }                 
        }        
        
        
        //Tags - create nodes and relationship in social
        String csvTags = uiDoc.getUTag();
        if (StringUtils.isNotBlank(csvTags))
        {
            tagsList.addAll(StringUtils.convertStringToList(csvTags, ","));
        }
        
        //catalog item paths
        String csvCatalogPaths = uiDoc.getUCatalog();
        if(StringUtils.isNotBlank(csvCatalogPaths))
        {
            catalogItemPaths.addAll(StringUtils.convertStringToList(csvCatalogPaths, ","));
        }
        
        //massage the roles based on the user roles
        massageAccessRightsBasedOnUserRoles();
        
        //update the access rights
        EvaluateAccessRights.updateAccessRightsFor(uiDoc, username);
        
        //remove the chars that get into the content when copied from other applications like Word
        String massagedContent = massageWikiContent(uiDoc.getUContent());
        uiDoc.setUContent(massagedContent);
        
        // remove DT sections which do not belong to current document (if any) if NOT import/export operation
        // String msgContentNoDTSections = isImportExportOperation ? massagedContent : removeDTSections(massagedContent);
        // uiDoc.setUContent(msgContentNoDTSections);
        
        //save it
        uiDoc = SaveUtil.saveWikiDocument(uiDoc, username);
        
        //update tags
        updateTagsForThisDocument();
        
        //update Catalog paths for this document
        updateCatalogPathsForThisDocument();
        
        //create/update the social node
        WikiUtils.updateSocialGraphDB(uiDoc, username);
        
        //add Attachments
        if(!isReset()) 
            addAttachments();

        if(archive)
        {
            //archive
            new ArchiveWiki(uiDoc, comment, username).archive();
        }
        
        if(isImportExportOperation)
        {
            //this is for the import export operation
        }
        else
        {
            //DT - DO NOT execute the decision tree if its saving from the IMPEX
            if(uiDoc.ugetUIsRoot())
            {
                DecisionTreeHelper.applyDecisionTreeXML(uiDoc, username);
            }
        }
        
    }
    
    private void socialOperations(boolean insertOperation) throws Exception
    {
        if(insertOperation)
        {
            //add this user to follow the document
            addUserToFollowThisDocument();
            
            if (!isImportExportOperation)
            {
                submitNotifications.add(NotificationHelper.getDocumentNotification(uiDoc, UserGlobalNotificationContainerType.DOCUMENT_CREATE, username, false, true));
            }

        }
        else
        {
            //update
            if (!isImportExportOperation)
            {
                submitNotifications.add(NotificationHelper.getDocumentNotification(uiDoc, UserGlobalNotificationContainerType.DOCUMENT_UPDATE, username, false, true));
            }
        }
    }
    
//    private void createTagsInSocial() throws Exception
//    {
//        Set<String> updatedTags = new HashSet<String>();
//        
//        for(String tagName : tagsList)
//        {
//            try
//            {
//                tagName = tagName.replaceAll(HibernateConstants.REGEX_TAG_NAME, "_");
//                ResolveTag t = ServiceTag.createTag(tagName, username);
//                if(t != null)
//                {
//                    updatedTags.add(t.getName());
//                }
//            }
//            catch (Exception e)
//            {
//                if(e.getMessage().contains("Tag already exists"))
//                {
//                    updatedTags.add(tagName);
//                }
//                //no issue if there is an exception 
//                Log.log.debug("Tag " + tagName + " already exist");
//            }
//        }
//        
//        //update the tag list
//        tagsList.clear();
//        tagsList.addAll(updatedTags);
//        
//    }
    
    private void updateTagsForThisDocument() throws Exception
    {
        try
        {
//            ServiceTag.updateTagsForComponent(this.uiDoc.getSys_id(), tagsList);
            
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(this.uiDoc.getSys_id());
	            if(doc != null)
	            {
	                //delete the existing tags 
	                Collection<WikidocResolveTagRel> tagRels = doc.getWikidocResolveTagRels();
	                if(tagRels != null && tagRels.size() > 0)
	                {
	                    for(WikidocResolveTagRel rel : tagRels)
	                    {
	                        HibernateUtil.getDAOFactory().getWikidocResolveTagRelDAO().delete(rel);
	                    }
	                    HibernateUtil.getCurrentSession().flush();
	                }
	                
	                if(this.tagsList.size() > 0)
	                {
	                    for(String tag : tagsList)
	                    {
	                        ResolveTagVO tagModel = ServiceTag.getTag(null, tag, username);
	                        
	                        if (tagModel == null)
	                        {
	                        	ResolveTag resolveTag = new ResolveTag();
	                        	resolveTag.setUName(tag);
	                        	HibernateUtil.getDAOFactory().getResolveTagDAO().persist(resolveTag);
	                        	tagModel = resolveTag.doGetVO();
	                        }
	                        
	                        WikidocResolveTagRel rel = new WikidocResolveTagRel();
	                        rel.setWikidoc(doc);
	                        rel.setTag(new ResolveTag(tagModel.getSys_id()));
	                        
	                        HibernateUtil.getDAOFactory().getWikidocResolveTagRelDAO().persist(rel);
	                    }//end of for loop
	                }//end of if
	            }//end of if
            });
        }
        catch (Exception e)
        {
            Log.log.error("error while assigning tags to document " + this.uiDoc.getUFullname(), e);
            throw e;
        }
    }
    
    private void updateCatalogPathsForThisDocument() throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(this.uiDoc.getSys_id());
	            if(doc != null)
	            {
	                //delete the existing catalog list
	                Collection<ResolveCatalogNodeWikidocRel> catalogNodeWikidocRels = doc.getCatalogNodeWikidocRels();
	                if(catalogNodeWikidocRels != null && catalogNodeWikidocRels.size() > 0)
	                {
	                    for(ResolveCatalogNodeWikidocRel catalogNodeWikidocRel : catalogNodeWikidocRels)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveCatalogNodeWikidocRelDAO().delete(catalogNodeWikidocRel);
	                    }
	                    
	                    HibernateUtil.getCurrentSession().flush();
	                }
	                
	                //add the new ones
	                if(this.catalogItemPaths.size() > 0)
	                {
	                    //String whereClause = "UPath IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(catalogItemPaths)) + ")";
	                    Set<String> sysIdsOfCatalogNodes = GeneralHibernateUtil.getSysIdsFor("ResolveCatalogNode", "UPath" , "system", catalogItemPaths);
	                    for(String sysIdsOfCatalogNode : sysIdsOfCatalogNodes)
	                    {
	                        ResolveCatalogNode node = HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().findById(sysIdsOfCatalogNode);
	                        if(node != null)
	                        {
	                            if(StringUtils.isNotBlank(node.getUDisplayType()))
	                            {
	                                if(node.getUDisplayType().equalsIgnoreCase(Catalog.DISPLAY_TYPE_WIKI))
	                                {
	                                    //only 1 document can be assigned to this node at a Time
	                                    Collection<ResolveCatalogNodeWikidocRel> catalogDocRels = node.getCatalogWikiRels();
	                                    if(catalogDocRels != null && catalogDocRels.size() > 0)
	                                    {
	                                        //delete all the previous rels and add the new one
	                                        for(ResolveCatalogNodeWikidocRel rel : catalogDocRels)
	                                        {
	                                            HibernateUtil.getDAOFactory().getResolveCatalogNodeWikidocRelDAO().delete(rel);
	                                        }
	                                        HibernateUtil.getCurrentSession().flush();
	                                    }
	                                }
	                                
	                                ResolveCatalogNodeWikidocRel rel = new ResolveCatalogNodeWikidocRel();
	                                rel.setCatalogNode(node);
	                                rel.setCatalog(node.getCatalog());
	                                rel.setWikidoc(doc);
	
	                                HibernateUtil.getDAOFactory().getResolveCatalogNodeWikidocRelDAO().persist(rel);
	                            }
	                        }
	                    }//end of for
	                }
	                
	                
	            }//end of if
            
            });
        }
        catch (Exception e)
        {
            Log.log.error("error while assigning catalog paths to document " + this.uiDoc.getUFullname(), e);
            
            throw e;
        }
    }
    
//    private void updateCatalogItemsForThisDocument() throws Exception
//    {
//        try
//        {
//            Document doc = SocialCompConversionUtil.convertWikiDocumentToDocument(uiDoc, null);
//            ServiceCatalog.updateCatalogItemPathsToDocument(doc, catalogItemPaths, username);
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error while assigning catalog paths to document " + this.uiDoc.getUFullname(), e);
//        }
//    }
    
    private void addUserToFollowThisDocument()
    {
        //if its NOT a copy and if its NOT Impex operation, than follow the document
        //if (!isComingFromCopy && !isImportExportOperation)
        if(isUserShouldFollow())
        {
            ResolveNodeVO docNode = new ResolveNodeVO();
            docNode.setUCompName(uiDoc.getUFullname());
            docNode.setUCompSysId(uiDoc.getSys_id());
            docNode.setUType(NodeType.DOCUMENT);
            
            Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
            followComponents.add(docNode);
            
            try
            {
                //user following this Document
                ServiceGraph.follow(null, null, username, NodeType.USER, followComponents, null, username);
            }
            catch (Exception e)
            {
                Log.log.error("Error following document :" + uiDoc.getUFullname() + " by User:" + username, e);
            }
        }
    }
    
    private void sendMessagesToESB() throws Exception
    {
        //send ESB for social notifications
        if(sendNotifications)
        {
            if(submitNotifications.size() > 0)
            {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put(ConstantValues.SOCIAL_NOTIFICATION_LIST, submitNotifications);
                
                SocialUtil.submitESBForUpdateSocialForWiki(params);
            }
        }
        Boolean updateDependencies = updateMain || updateException || updateDTree;
        //clear the cache on other nodes if any of these are updated
        if (updateDependencies || updateAccess)
        {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("WIKI", uiDoc.getUFullname());
            params.put("FLUSHDEPENDENCIES", updateDependencies.toString());
            params.put("FLUSHACCESSRIGHTS", Boolean.toString(updateAccess));
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.cacheFlush", params);
        }
        //update the wiki relationship tables and stats
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ConstantValues.WIKI_DOCUMENT_SYS_ID, uiDoc.getSys_id());
        params.put(ConstantValues.WIKIDOC_STATS_COUNTER_NAME_KEY, WikiDocStatsCounterName.EDITED);
        params.put(ConstantValues.WIKI_DOCUMENT_CONTENT, updateContent);
        params.put(ConstantValues.WIKI_DOCUMENT_MODEL, updateMain);
        params.put(ConstantValues.WIKI_DOCUMENT_EXCEPTION, updateException);
        params.put(ConstantValues.WIKI_DOCUMENT_DT, updateDTree);
        params.put(ConstantValues.USERNAME, username);

        WikiUtils.sendESBForUpdatingWikiRelations(params);
    }
    
    private void setFlagsForChanges()
    {
        String tempStr1 = uiDoc.getUContent() != null ? uiDoc.getUContent().trim() : "";
        String tempStr2 = dbDoc.getUContent() != null ? dbDoc.getUContent().trim() : "";
        if (tempStr1.equals(tempStr2))
        {
            updateContent = false;
        }

        tempStr1 = uiDoc.getUModelProcess() != null ? uiDoc.getUModelProcess().trim() : "";
        tempStr2 = dbDoc.getUModelProcess() != null ? dbDoc.getUModelProcess().trim() : "";
        if (tempStr1.equals(tempStr2))
        {
            updateMain = false;
        }

        tempStr1 = uiDoc.getUModelException() != null ? uiDoc.getUModelException().trim() : "";
        tempStr2 = dbDoc.getUModelException() != null ? dbDoc.getUModelException().trim() : "";
        if (tempStr1.equals(tempStr2))
        {
            updateException = false;
        }

//        tempStr1 = uiDoc.getUModelFinal() != null ? uiDoc.getUModelFinal().trim() : "";
//        tempStr2 = dbDoc.getUModelFinal() != null ? dbDoc.getUModelFinal().trim() : "";
//        if (tempStr1.equals(tempStr2))
//        {
//            updateFinal = false;
//        }

        tempStr1 = uiDoc.getUDecisionTree() != null ? uiDoc.getUDecisionTree().trim() : "";
        tempStr2 = dbDoc.getUDecisionTree() != null ? dbDoc.getUDecisionTree().trim() : "";
        if (tempStr1.equals(tempStr2))
        {
            updateDTree = false;
        }
        
        tempStr1 = uiDoc.getUDTOptions() != null ? uiDoc.getUDTOptions().trim() : "";
        tempStr2 = dbDoc.getUDTOptions() != null ? dbDoc.getUDTOptions().trim() : "";
        if (tempStr1.equals(tempStr2))
        {
            updateDTreeOptions = false;
        }
    }
    
    private void addAttachments() throws Exception
    {
        if(attachmentsToAdd != null || attachments != null)
        {
            AttachmentHelper attachment = new AttachmentHelper(uiDoc.getSys_id(), uiDoc.getUFullname(), attachmentsToAdd, attachments, username);
            attachment.setImpex(isImportExportOperation);
            if(isRollback())
                attachment.attach(true);
            else
                attachment.attach();
        }
    }
    

    private void init() throws Exception 
    {
        //if the content is null, set it to empty string. 
        if(StringUtils.isBlank(this.uiDoc.getUContent()))
        {
            this.uiDoc.setUContent("");
        }
        
        userRoles = UserUtils.getUserRoles(username);
        
        //flag to indicate if the user is admin
        isAdminUser = UserUtils.isAdminUser(username);
    }
    
    private boolean validateBeforeSave() throws Exception
    {
        boolean createdNewDocument = false;

        if(this.dbDoc != null)
        {
            //if the document is mark deleted OR if the overwrite flag is true, delete the existing document
            if(dbDoc.ugetUIsDeleted() || isOverwrite())
            {
                //delete the document 
                Log.log.trace("Purging document " + dbDoc.getUFullname() + " before saving.");
                DeleteHelper.deleteWikiDocument(null, dbDoc.getUFullname(), username);
                
                //reset
                dbDoc = null;
                uiDoc.setSys_id(null);
                if(uiDoc.getAccessRights() != null)
                {
                    uiDoc.getAccessRights().setSys_id(null);
                }
                    
                
                //insert the new one
                insert();
                
                //the control should not continue after doing an insert operation
                createdNewDocument = true;
//                return;
            }
                
//            if(dbDoc != null && dbDoc.ugetUIsLocked())
//                throw new Exception("Cannot update document " + dbDoc.getUFullname() + " as it is marked locked");
        }
        
        return createdNewDocument;

    }
    
    private void massageAccessRightsBasedOnUserRoles()
    {
        //'Admin' access rights can be updated only if the user has 'admin' role. So if the user is NOT AN ADMIN, make sure to use the current DB exist right
        if(!isAdminUser && this.dbDoc != null)
        {
            AccessRights uiAccessRights = this.uiDoc.getAccessRights();
            AccessRights dbAccessRights = this.dbDoc.getAccessRights();
            
            uiAccessRights.setUAdminAccess(dbAccessRights.getUAdminAccess());
        }
    }
    
    private String massageWikiContent(String content)
    {
        String result = content;
        
        if (StringUtils.isNotBlank(result))
        {
            //http://www.branah.com/ascii-converter
            //change the quotes from Word(8220) to a regular text code (34)
            boolean hasWordQuotes = content.indexOf(8220) > -1 || content.indexOf(8221) > -1;
            if (hasWordQuotes)
            {
                result = result.replace((char) 8220, (char) 34);
                result = result.replace((char) 8221, (char) 34);
            }
        }
        return result;
    }
    
    private void updateNamespace()
    {
        String docFullName = this.uiDoc.getUFullname().trim();
        String[] nameArr = docFullName.split("\\.");
        String namespace = nameArr[0];
        String name = nameArr[1];
        //String hql = "select DISTINCT UNamespace from WikiDocument where Lower(UNamespace) = '" + namespace.toLowerCase() + "'";
        String hql = "select DISTINCT UNamespace from WikiDocument where Lower(UNamespace) = :UNamespace";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UNamespace", namespace.trim().toLowerCase());
        
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
            if(list != null && list.size() >= 1)
            {
                if (list.size() == 1)
                {
                    namespace = (String) list.get(0);
                    docFullName = namespace + "." + name;
                }
                else
                {
                    // If multiple records then choose the namespace with lowest Levenshtein distance
                    
                    int distBetweenNS = Integer.MAX_VALUE;
                    int minDistBetweenNSIndx = -1;
                    
                    for (int i = 0; i < list.size() && distBetweenNS != 0; i++)
                    {
                        if (StringUtils.getLevenshteinDistance(namespace, (String) list.get(i)) < distBetweenNS)
                        {
                            minDistBetweenNSIndx = i;
                            distBetweenNS = StringUtils.getLevenshteinDistance(namespace, (String) list.get(minDistBetweenNSIndx));
                        }
                    }
                    
                    namespace = (String) list.get(minDistBetweenNSIndx);
                    docFullName = namespace + "." + name;
                }
            }
            
            uiDoc.setUFullname(docFullName);
            uiDoc.setUNamespace(namespace);
        }
        catch (Exception e)
        {
           Log.log.error("Error while executing sql:" + hql, e);
        }
        
        
    }

}
