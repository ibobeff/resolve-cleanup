/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.rb.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.vo.Operation;
import com.resolve.services.vo.form.CRUDAction;
import com.resolve.services.vo.form.DataSource;
import com.resolve.services.vo.form.RsButtonAction;
import com.resolve.services.vo.form.RsButtonPanelDTO;
import com.resolve.services.vo.form.RsColumnDTO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsFormProperties;
import com.resolve.services.vo.form.RsPanelDTO;
import com.resolve.services.vo.form.RsTabDTO;
import com.resolve.services.vo.form.RsUIButton;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;

public class CustomForm
{
    private RBGeneralVO entity = null;
    private String username = null;
    private String problemId = "ACTIVE";
    private WikiDocumentVO wiki = null;
    
    public CustomForm(RBGeneralVO entity, String username) throws Exception
    {
        if(entity == null)
        {
            throw new Exception("RBGeneralVO obj is mandatory");
        }
        
        if(StringUtils.isEmpty(entity.getWikiId()))
        {
            throw new Exception("Wiki SysId is not present. It is mandatory");
        }
        
        //wiki parameters were moved to DB
        wiki = ServiceWiki.getWikiDocWithGraphRelatedData(entity.getWikiId(), null, username);
        if(wiki == null)
        {
            throw new Exception("Wiki with sysId " + entity.getWikiId() + " does not exist.");
        }
        
        this.entity = entity;
        this.entity.setWikiParameters(wiki.getUWikiParameters());
        this.username = username;
        if(entity.getNewWorksheet() != null && entity.getNewWorksheet())
        {
            problemId = "NEW";
        }
    }
    
    public RsFormDTO create() throws Exception
    {
        String formName = synthesizeFormName(entity);
        RsFormDTO formDTO = null;
        
        try
        {
            formDTO = ServiceHibernate.getMetaFormView(null, formName, null, username, RightTypeEnum.view, null, false);
        }
        catch(Exception e)
        {
            //most likely not found so ignore
        }
        
        // If it is corrupted form (i.e. no tab) perpare again
        
        if(formDTO != null && formDTO.getPanels().get(0).getTabs() != null &&
           !formDTO.getPanels().get(0).getTabs().isEmpty())
        {
            //Temporary fix for RS-22616
            if(formDTO.getPanels().get(0).getTabs().get(0).getColumns() != null 
                            && formDTO.getPanels().get(0).getTabs().get(0).getColumns().size() == 0)
            {
                RsColumnDTO column = new RsColumnDTO();
                formDTO.getPanels().get(0).getTabs().get(0).getColumns().add(column);
            }
            formDTO.getPanels().get(0).getTabs().get(0).getColumns().get(0).setFields(getFields());
            
            formDTO.getButtonPanel().setControls(getControls());
            
            formDTO.setAdminRoles(wiki.getAccessRights().getUAdminAccess());
            formDTO.setViewRoles(wiki.getAccessRights().getUReadAccess());
            formDTO.setEditRoles(wiki.getAccessRights().getUWriteAccess());
            formDTO = ServiceHibernate.saveMetaFormView(formDTO, username);
        }
        else
        {
            //create a custom form too
            formDTO = prepareForm(formName);
            //persist
            formDTO = ServiceHibernate.saveMetaFormView(formDTO, username);
        }
        
        return formDTO;
    }
    
    private RsFormDTO prepareForm(String formName) throws Exception
    {
        RsFormDTO form = new RsFormDTO();
        form.setFormName(formName);
        form.setViewName(form.getFormName());
        //form.setDisplayName(form.getFormName()); //seem like we do not need this
        form.setWizard(false);
        form.setAdminRoles(wiki.getAccessRights().getUAdminAccess());
        form.setViewRoles(wiki.getAccessRights().getUReadAccess());
        form.setEditRoles(wiki.getAccessRights().getUWriteAccess());
        
        //panels
        form.setPanels(getPanels());
        
        //button panel
        form.setButtonPanel(getButtonPanel());
        
        //properties
        form.setProperties(new RsFormProperties());
        
        return form;
        
    }
    
    private List<RsPanelDTO> getPanels() throws Exception
    {
        List<RsPanelDTO> panels = new ArrayList<RsPanelDTO>();
        
        //their is only 1 panel
        RsPanelDTO panel = new RsPanelDTO();
        panel.setTabs(getTabs());
        
        panels.add(panel);       
        
        return panels;
    }
    
    private List<RsTabDTO> getTabs() throws Exception
    {
        List<RsTabDTO> tabs = new ArrayList<RsTabDTO>();
        
        //only 1 tab for this form
        RsTabDTO tab = new RsTabDTO();
        tab.setCols(1);
        tab.setColumns(getColumns());
        
        tabs.add(tab);
        
        return tabs;
    }
    
    private List<RsColumnDTO> getColumns() throws Exception
    {
        List<RsColumnDTO> columns = new ArrayList<RsColumnDTO>();
        
        //only 1 column for this form
        RsColumnDTO column = new RsColumnDTO();
        column.setFields(getFields());
        
        columns.add(column);
        
        return columns;
    }
    
    private List<RsUIField> getFields() throws Exception
    {
        List<RsUIField> fields = new ArrayList<RsUIField>();
        
        if(StringUtils.isNotBlank(entity.getWikiParameters()) && !"UNDEFINED".equalsIgnoreCase(entity.getWikiParameters()))
        {
            WikiParameter[] wikiParameters = new ObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(entity.getWikiParameters(), WikiParameter[].class);
            for(WikiParameter wikiParameter : wikiParameters)
            {
                RsUIField field = null;
                String displayName = StringUtils.isBlank(wikiParameter.getDisplayName()) ? wikiParameter.getName() : wikiParameter.getDisplayName();
                String defaultValue = "";
                if(wikiParameter.getSource().equals("CONSTANT"))
                    defaultValue = wikiParameter.getSourceName();
                else if(wikiParameter.getSource().equals("WSDATA"))
                    defaultValue = "$WSDATA{" + wikiParameter.getSourceName() + "}";
                else if(wikiParameter.getSource().equals("PROPERTY"))
                    defaultValue = "$PROPERTY{" + wikiParameter.getSourceName() + "}";
                else
                    defaultValue = "";
                if(wikiParameter.getHidden()) {
                    field = createHiddenField(wikiParameter.getName(),defaultValue);
                }else if("string".equalsIgnoreCase(wikiParameter.getType()))
                {
                    field = createField(wikiParameter.getName(), displayName, "textfield", HibernateConstantsEnum.UI_TEXTFIELD.getTagName(), wikiParameter.getOrder(), defaultValue,wikiParameter.getFieldTip());
                }
                else if("int".equalsIgnoreCase(wikiParameter.getType()))
                {
                    field = createField(wikiParameter.getName(), displayName, "numberfield", HibernateConstantsEnum.UI_NUMBERTEXTFIELD.getTagName(), wikiParameter.getOrder(), defaultValue,wikiParameter.getFieldTip());
                }
                else if("date".equalsIgnoreCase(wikiParameter.getType()))
                {
                    field = createField(wikiParameter.getName(), displayName, "datefield", HibernateConstantsEnum.UI_DATE.getTagName(), wikiParameter.getOrder(), defaultValue,wikiParameter.getFieldTip());
                }
                else if("decimal".equalsIgnoreCase(wikiParameter.getType()))
                {
                    field = createField(wikiParameter.getName(), displayName, "numberfield", HibernateConstantsEnum.UI_DECIMALTEXTFIELD.getTagName(), wikiParameter.getOrder(), defaultValue,wikiParameter.getFieldTip());
                }
                else if("encrypted".equalsIgnoreCase(wikiParameter.getType()))
                {
                    field = createField(wikiParameter.getName(), displayName, "textfield", HibernateConstantsEnum.UI_PASSWORD.getTagName(), wikiParameter.getOrder(), defaultValue,wikiParameter.getFieldTip());
                }

                fields.add(field);
            }//end of for loop
            Collections.sort(fields, new Comparator<RsUIField>() {

                public int compare(RsUIField f1, RsUIField f2)
                {
                    return f1.getOrderNumber()>f2.getOrderNumber()?1:-1;
                }
                
            });
            //add the Hidden fields
//            fields.add(createHiddenField("PROBLEMID", problemId));
        }
        /* If Tab is empty add hidden field PROBLEMID so Tab will not get deleted while saving */
        if (fields.isEmpty())
        {
            //add the Hidden fields
            fields.add(createHiddenField("PROBLEMID", problemId));
        }
        
        return fields;
    }
    
    private static RsUIField createField(String name, String displayName, String xType, String uiType, Integer order,String defaultValue,String fieldTip)
    {
        RsUIField field = new RsUIField();
        field.setXtype(xType);
        field.setName(name);
        field.setDisplayName(displayName);
        field.setOrderNumber(order==null ? 0 : order);
        field.setSourceType(DataSource.INPUT.name());
        field.setUiType(uiType);
        field.setLabelAlign("left");
        field.setStringMinLength(1);
        field.setStringMaxLength(100);
        field.setUiStringMaxLength(100);
        field.setReadOnly(false);
        field.setHidden(false);
        field.setMandatory(false);
        field.setEncrypted(false);
        field.setDefaultValue(defaultValue);
        field.setTooltip(fieldTip);
        
        return field;
    }
    
    private RsUIField createHiddenField(String name, String value)
    {
        RsUIField field = new RsUIField();
        field.setXtype("hiddenfield");
        field.setName(name);
        field.setHiddenValue(value);
        field.setSourceType(DataSource.INPUT.name());
        field.setUiType(HibernateConstantsEnum.UI_HIDDEN.getTagName());
        field.setReadOnly(false);
        field.setHidden(false);
        field.setMandatory(false);
        field.setEncrypted(false);
        
        return field;
    }
    
    private  RsButtonPanelDTO getButtonPanel() throws Exception
    {
        RsButtonPanelDTO buttonPanel = new  RsButtonPanelDTO();
        buttonPanel.setControls(getControls());
        
        return buttonPanel;
    }
    
    private List<RsUIButton> getControls() throws Exception
    {
        List<RsUIButton> controls = new ArrayList<RsUIButton>();

        //only 1 button
        RsUIButton button = new RsUIButton();
        button.setName("CUSTOM");
        button.setDisplayName("Execute");
        button.setType("button");
        
        //set the actions on the button
        button.setActions(getActions());
        
        //add it to the list
        controls.add(button);
        
        return controls;
    }
    
    private List<RsButtonAction> getActions() throws Exception
    {
        List<RsButtonAction> actions = new ArrayList<RsButtonAction>();
        
        RsButtonAction action = new RsButtonAction();
        action.setCrudAction(CRUDAction.SUBMIT.name());
        action.setOperation(Operation.RUNBOOK.name());
        action.setOrderNumber(1);
        action.setAdditionalParam("PROBLEMID|=|" + problemId);
        action.setRunbook(ResolutionBuilderUtils.getWikiName(entity.getNamespace(), entity.getName()));
        
        //add to the list
        actions.add(action);
        
        return actions;
    }
    
    public static String synthesizeFormName(RBGeneralVO general) throws Exception
    {
        String result = null;
        if(general == null || StringUtils.isBlank(general.getNamespace()) || StringUtils.isBlank(general.getName()))
        {
             throw new Exception("Invalid Automation Builder.");
        }
        else
        {
            //TODO, need to find a API that validates the name format, disallowed char etc.
            //wikis support space, - but not custom form, hence this conversion to _
            result = "ABF_".concat(general.getNamespace().toUpperCase().replaceAll("[ -]", "_")).concat("_").concat(general.getName().toUpperCase().replaceAll("[ -]", "_"));
        }
        return result;
    }
}
