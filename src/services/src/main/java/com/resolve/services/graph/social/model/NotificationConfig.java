/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model;

import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;

public class NotificationConfig
{
    private UserGlobalNotificationContainerType type;
    private String displayName;
    private String value;
    
    public void setType(UserGlobalNotificationContainerType type)
    {
        this.type = type;
    }
    
    public UserGlobalNotificationContainerType getType()
    {
        return type;
    }
    
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    public String getDisplayName()
    {
        return this.displayName;
    }
    
    public void setValue(String value)
    {
        this.value = value;
    }
    
    public String getValue()
    {
        return value;
    }
}
