/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.Query;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.model.Organization;
import com.resolve.persistence.model.ResolveAppControllerRel;
import com.resolve.persistence.model.ResolveAppOrganizationRel;
import com.resolve.persistence.model.ResolveAppRoleRel;
import com.resolve.persistence.model.ResolveApps;
import com.resolve.persistence.model.ResolveControllers;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.util.CacheUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.ResolveAppsVO;
import com.resolve.services.hibernate.vo.ResolveControllersVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AppsUtil
{
    private static final String CONTROLLER_CACHE_KEY = "CONTROLLER_CACHE_KEY";

    public static List<ResolveAppsVO> getResolveApps(QueryDTO query, String username)
    {
        List<ResolveAppsVO> result = new ArrayList<ResolveAppsVO>();

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if (data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                //get the Roles for all the organizations
                Map<String, String> mapOfAppsAndRoles =  getMapOfAppsAndRoles();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveApps instance = new ResolveApps();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(convertModelToVO(instance, mapOfOrganization, mapOfAppsAndRoles));
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveApps instance = (ResolveApps) o;
                        result.add(convertModelToVO(instance, mapOfOrganization, mapOfAppsAndRoles));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    public static ResolveAppsVO findResolveAppsById(String sys_id, String username)
    {
        ResolveApps model = null;
        ResolveAppsVO result = null;

        if (!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findResolveAppsModelByIdOrName(sys_id, null, true, username);
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }

        if (model != null)
        {
            result = convertModelToVO(model, null, null);
        }

        return result;
    }

    public static void deleteResolveAppsByIds(String[] sysIds, QueryDTO query, String username, boolean purge) throws Exception
    {
        if (sysIds != null)
        {
            //TODO: prepare the list of sys_ids to delete based on qry
            if (sysIds == null || sysIds.length == 0)
            {
                //prepare the sysIds with the where clause of this obj
                String selectQry = query.getSelectHQL();
                String sql = "select sys_id " + selectQry.substring(selectQry.indexOf("from"));
                System.out.println("AppsUtil: #128: Delete qry :" + sql);
            }

            //delete one by one
            for (String sysId : sysIds)
            {
                deleteResolveAppsById(sysId, username, purge);
            }

            //update the cache
            cacheControllerAndRoles(true);
        }
    }

    public static ResolveAppsVO saveResolveApps(ResolveAppsVO vo, String username) throws Exception
    {
        if (vo != null)
        {
            ResolveApps model = null;
            if (StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                model = findResolveAppsModelByIdOrName(vo.getSys_id(), null, true, username);
            }
            else
            {
                model = new ResolveApps();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveResolveApps(model, username);

            //assign controllers, roles, orgs to apps
            assignArtifactsToApp(vo, model, username);

            //get a fresh copy
            vo = findResolveAppsById(model.getSys_id(), username);

            //update the cache
            cacheControllerAndRoles(true);

        }

        return vo;
    }

    public static void initControllerProperties(String fileName) throws Exception
    {
    	boolean insertIsDone = false;//checkIfRecordsExistInControllerTable();

        //if there are no recs yet in the tables than only read it from the properties file and add it
        if (!insertIsDone)
        {
        	Map<String, ControllerProperty> data = new HashMap<String, ControllerProperty>();
            java.util.Properties properties = FileUtils.readFileAndConvertToProperties(fileName);
            if (properties != null)
            {
                Enumeration<Object> enu = properties.keys();
                while (enu.hasMoreElements())
                {
                    String key = (String) enu.nextElement(); // eg. app.sysadmin.controllers<list>
                    String value = properties.getProperty(key);//eg. Configad,Configldap

                    convertPropertiesToMap(data, key, value);
                }
            }
            //process the map
            
            processControllerProperties(data);
        	
        }

        //prepare the cache for controller and roles
        cacheControllerAndRoles(false);

        //testing
        //            System.out.println("admin user: " + doesUserHaveRightsForController("configad", "admin"));
        //            System.out.println("user user: " + doesUserHaveRightsForController("configad", "user"));
    }

    /**
     * API to check whether the user, specified by username, has rights for the controller.
     * 
     * @param controllerName - /sysproperties/getAllProperties
     * @param username
     * @return True if user has rights, false otherwise
     */
    public static boolean doesUserHaveRightsForController(String controllerName, String username)
    {
        //if there are no rights defined for a controller, that means user HAS the rights to access it.
        boolean hasRights = false;

        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(controllerName))
        {
            try
            {
                //flag to indicate if the user is admin
                boolean isAdminUser = UserUtils.isAdminUser(username);
                if(isAdminUser)
                {
                    hasRights = true;
                }
                else
                {
                
                    //user info
                    UsersVO user = UserUtils.getUserNoReferences(username);
                    if(user != null)
                    {
                        String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;
        
                        //get the cache obj
                        ControllerProperty controllerProperty = getControllerProperty(controllerName, userOrg);
        
                        if (controllerProperty != null)
                        {
                            //controller roles
                            Set<String> controllerRoles = controllerProperty.controllerRoles;//getControllerRoles(controllerName, userOrg);
                            //if the controller has no rights associated with it, than anybody can use it and this is public controller
                            if (controllerRoles.isEmpty())
                            {
                                hasRights = true;
                            }
                            else
                            {
                                //test if user has roles to access this controller
                                Set<String> userRoles = UserUtils.getUserRoles(username);
                                if(userRoles == null || userRoles.size() == 0)
                                {
                                    throw new Exception("User " + username + " has no roles assigned. Please check with the administrator.");
                                }
                                String controllerRolesCSV = StringUtils.listToString(new ArrayList<String>(controllerRoles), ",");
        
                                if (StringUtils.isNotBlank(userOrg))
                                {
                                    //check if the userOrg is in a list of orgs 
                                    Set<String> orgs = controllerProperty.orgs;
                                    if (orgs.isEmpty())
                                    {
                                        //continue and check for roles now
                                        hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, controllerRolesCSV);
                                    }
                                    else
                                    {
                                        //if user org is listed than check for roles
                                        if (orgs.contains(userOrg))
                                        {
                                            //continue and check for roles now
                                            hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, controllerRolesCSV);
                                        }
                                    }
                                }
                                else
                                {
                                    hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, controllerRolesCSV);
                                }
                            }
                        }
                        else
                        {
                            hasRights = true;
                        }
                    }
                    else
                    {
                        Log.log.error("Error: User with " + username + " does not exist.");  
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("error in finding the rights for the controller " + controllerName + " for user " + username, e);
                hasRights = true;
            }
        }

        return hasRights;
    }

    //private apis
    private static ResolveAppsVO convertModelToVO(ResolveApps model, Map<String, String> mapOfOrganization, Map<String, String> mapOfAppsAndRoles)
    {
        ResolveAppsVO vo = null;

        if (model != null)
        {
            if (mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }

            vo = model.doGetVO();
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }

            //set the comma seperated Roles
            if(mapOfAppsAndRoles != null)
            {
                vo.setRoles(mapOfAppsAndRoles.get(vo.getSys_id()));
            }
        }

        return vo;
    }

    private static ResolveApps findResolveAppsModelByIdOrName(String sys_id, String appName, boolean loadReferencesAlso, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            return (ResolveApps) HibernateProxy.execute(() -> {
            	ResolveApps result = null;

	            if (StringUtils.isNotBlank(sys_id))
	            {
	                result = HibernateUtil.getDAOFactory().getResolveAppsDAO().findById(sys_id);
	            }
	            else if (StringUtils.isNotBlank(appName))
	            {
	                ResolveApps app = new ResolveApps();
	                app.setUAppName(appName);
	                result = HibernateUtil.getDAOFactory().getResolveAppsDAO().findFirst(app);
	            }
	
	            if (loadReferencesAlso)
	            {
	                if (result != null)
	                {
	                    result.getResolveAppControllerRels().size();
	                    result.getResolveAppRoleRels().size();
	                }
	            }
	            
	            return result;
            });

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return null;
    }

    private static void deleteResolveAppsById(String sysId, String username, boolean purge) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		//we will not use the isDeleted for now.
        		boolean purgeInternal = true;

	            ResolveApps model = HibernateUtil.getDAOFactory().getResolveAppsDAO().findById(sysId);
	            if (model != null)
	            {
	
	                if (purgeInternal)
	                {
	                    //delete the roles relationships
	                    Collection<ResolveAppRoleRel> roleRels = model.getResolveAppRoleRels();
	                    if (roleRels != null && roleRels.size() > 0)
	                    {
	                        for (ResolveAppRoleRel roleRel : roleRels)
	                        {
	                            HibernateUtil.getDAOFactory().getResolveAppRoleRelDAO().delete(roleRel);
	                        }
	                    }
	
	                    //delete the controllers relationships
	                    Collection<ResolveAppControllerRel> controllerRels = model.getResolveAppControllerRels();
	                    if (controllerRels != null && controllerRels.size() > 0)
	                    {
	                        for (ResolveAppControllerRel controllerRel : controllerRels)
	                        {
	                            HibernateUtil.getDAOFactory().getResolveAppControllerRelDAO().delete(controllerRel);
	                        }
	                    }
	
	                    //delete the main rec
	                    HibernateUtil.getDAOFactory().getResolveAppsDAO().delete(model);
	                }
	                else
	                {
	                    //mark as deleted
	                    Collection<ResolveAppRoleRel> roleRels = model.getResolveAppRoleRels();
	                    if (roleRels != null && roleRels.size() > 0)
	                    {
	                        for (ResolveAppRoleRel roleRel : roleRels)
	                        {
	                            roleRel.setSysIsDeleted(true);
	                        }
	                    }
	
	                    //delete the controllers relationships
	                    Collection<ResolveAppControllerRel> controllerRels = model.getResolveAppControllerRels();
	                    if (controllerRels != null && controllerRels.size() > 0)
	                    {
	                        for (ResolveAppControllerRel controllerRel : controllerRels)
	                        {
	                            controllerRel.setSysIsDeleted(true);
	                        }
	                    }
	
	                    model.setSysIsDeleted(true);
	                }
	
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);

            throw new Exception(e);
        }
    }

    /**
     * 
     * @param key -   eg. app.sysadmin.controllers<list>
     * @param value - eg. Configad,Configldap                              ##AjaxConfigActiveDirectory, AjaxConfigLDAP
     */
    private static void convertPropertiesToMap(Map<String, ControllerProperty> map, String key, String value)
    {
        if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value) && key.startsWith("app"))
        {
            //app.sysadmin.controllers
            String completeStr = key;
            if (completeStr.indexOf("<") > -1)
            {
                completeStr = completeStr.substring(0, completeStr.indexOf("<"));
            }

            String[] allStr = completeStr.split("\\.");
            String appRefName = allStr[1]; //sysadmin
            String type = allStr[2];    //controllers

            //prepare the data object
            ControllerProperty data = null;
            if (map.containsKey(appRefName))
            {
                data = map.get(appRefName);
            }
            else
            {
                data = new ControllerProperty();
            }

            //populate the object
            if (type.equalsIgnoreCase("controllers"))
            {
                data.controllers = value;
            }
            else if (type.equalsIgnoreCase("roles"))
            {
                data.roles = value;
            }
            else if (type.equalsIgnoreCase("description"))
            {
                data.description = value;
            }
            else if (type.equalsIgnoreCase("name"))
            {
                data.name = value;
            }

            //add it back to map
            map.put(appRefName, data);

            //            ResolveApps app = findResolveAppsModelById(null, appRefName, false, "system");
            //            if (app == null)
            //            {
            //                app = new ResolveApps();
            //                app.setUAppName(appRefName);
            //                if (type.equalsIgnoreCase("description"))
            //                {
            //                    app.setUDescription(value);
            //                }
            //                app = SaveUtil.saveResolveApps(app, "system");
            //            }//end of if
            //            
            //            if (type.equalsIgnoreCase("controllers"))
            //            {
            //                Set<String> controllers = new HashSet<String>(StringUtils.stringToList(value, ","));
            //                for (String controller : controllers)
            //                {
            //                    assignControllerToApp(app.getSys_id(), controller);
            //                }
            //            }
            //            else if (type.equalsIgnoreCase("roles"))
            //            {
            //                Set<String> roles = new HashSet<String>(StringUtils.stringToList(value, ","));
            //                for (String role : roles)
            //                {
            //                    assignRoleToApp(app.getSys_id(), role);
            //                }
            //            }
        }
    }

    private static void processControllerProperties(Map<String, ControllerProperty> map)
    {
        Iterator<String> it = map.keySet().iterator();
        while (it.hasNext())
        {
            String key = it.next();
            ControllerProperty value = map.get(key);

            processControllerProperty(key, value);
        }
    }

    private static void processControllerProperty(String refName, ControllerProperty property)
    {
        if (StringUtils.isNotBlank(property.name))
        {
            ResolveApps app = findResolveAppsModelByIdOrName(null, property.name, false, "system");
            if (app == null)
            {
            	try
            	{
	                app = new ResolveApps();
	                app.setUAppName(property.name);
	                app.setUDescription(property.description);
	
	                app = SaveUtil.saveResolveApps(app, "system");
	
	                //controllers
	                if (StringUtils.isNotBlank(property.controllers))
	                {
	                    Set<String> controllers = new HashSet<String>(StringUtils.stringToList(property.controllers, ","));
	                    for (String controller : controllers)
	                    {
	                        assignControllerToApp(app.getSys_id(), null, controller);
	                    }
	                }
	                //roles
	                if (StringUtils.isNotBlank(property.roles))
	                {
	                    Set<String> roles = new HashSet<String>(StringUtils.stringToList(property.roles, ","));
	                    for (String role : roles)
	                    {
	                        assignRoleToApp(app.getSys_id(), null, role);
	                    }
	                }
            	}
            	catch(Exception e)
            	{
            		Log.log.error("Error: Issue while processing controller property: " + property.name , e);
            	}
            	
            }//end of if
        }
        else
        {
            Log.log.error("name property for " + refName + " is missing. This is a mandatory field. So presently ignoring this and moving ahead.");
        }

    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void assignRoleToApp(String sysId, String roleSysId, String roleStr)
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            ResolveApps app = HibernateUtil.getDAOFactory().getResolveAppsDAO().findById(sysId);
	            if (app != null)
	            {
	                Roles role = null;
	                
	                if(StringUtils.isNotBlank(roleSysId))
	                {
	                    role = HibernateUtil.getDAOFactory().getRolesDAO().findById(roleSysId);
	                }
	                else if(StringUtils.isNotBlank(roleStr))
	                {
	                    role = new Roles();
	                    role.setUName(roleStr);
	                    
	                    role = HibernateUtil.getDAOFactory().getRolesDAO().findFirst(role);
	                }
	                
	                if (role != null)
	                {
	                    //check if this relationship exist, if no, than make one
	                    String hql = "from ResolveAppRoleRel where resolveApp = :appSysId and roles = :roleSysId";
	                    Query query = HibernateUtil.createQuery(hql);
	                    List<ResolveAppRoleRel> result = query.
	                                                     setParameter("appSysId", app.getSys_id()).
	                                                     setParameter("roleSysId", role.getSys_id()).
	                                                     list();
	                    if (result == null || result.size() == 0)
	                    {
	                        ResolveAppRoleRel rel = new ResolveAppRoleRel();
	                        rel.setResolveApp(app);
	                        rel.setRoles(role);
	
	                        HibernateUtil.getDAOFactory().getResolveAppRoleRelDAO().persist(rel);
	                    }
	                }
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to assign role " + roleStr + " to app " + sysId, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void assignControllerToApp(String sysId, String controllerSysId, String controllerStr)
    {
        String hql = null;

        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        if (StringUtils.isNotBlank(controllerSysId))
        {
            //hql = "from ResolveControllers where sys_id = '" + controllerSysId.trim() + "'";
            hql = "from ResolveControllers where sys_id = :sys_id";
            queryParams.put("sys_id", controllerSysId.trim());
        }
        else if (StringUtils.isNotBlank(controllerStr))
        {
            //hql = "from ResolveControllers where LOWER(UControllerName) = '" + controllerStr.trim().toLowerCase() + "'";
            hql = "from ResolveControllers where LOWER(UControllerName) = :UControllerName";
            queryParams.put("UControllerName", controllerStr.trim().toLowerCase());
        }

        if (StringUtils.isNotBlank(hql))
        {
            try
            {

                ResolveControllers controller = null;
                List<? extends Object> controllers = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
                if (controllers == null || controllers.size() == 0)
                {
                    controller = createController(controllerStr);
                }
                else
                {
                    controller = (ResolveControllers) controllers.get(0);
                }

                if (controller != null)
                {
                    try
                    {
                    	ResolveControllers finalController = controller;
                      HibernateProxy.setCurrentUser("system");
                    	HibernateProxy.execute(() -> {

	                        ResolveApps app = HibernateUtil.getDAOFactory().getResolveAppsDAO().findById(sysId);
	                        if (app != null)
	                        {
	                            //check if this relationship exist, if no, than make one
	                            String hqlCheck = "from ResolveAppControllerRel where resolveApp.sys_id = :appSysId and resolveController.sys_id = :controllerSysId";
	                            Query query = HibernateUtil.createQuery(hqlCheck);
	                            List<ResolveAppControllerRel> result = query.
	                                                                   setParameter("appSysId", app.getSys_id()).
	                                                                   setParameter("controllerSysId", finalController.getSys_id()).
	                                                                   list();
	                            if (result == null || result.size() == 0)
	                            {
	                                ResolveAppControllerRel rel = new ResolveAppControllerRel();
	                                rel.setResolveApp(app);
	                                rel.setResolveController(finalController);
	
	                                HibernateUtil.getDAOFactory().getResolveAppControllerRelDAO().persist(rel);
	                            }
	                        }

                    	});
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to assign controller " + controllerStr + " to app " + sysId, e);
                                              HibernateUtil.rethrowNestedTransaction(e);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Failed to assign role " + controllerStr + " to app " + sysId, e);
            }
        }

    }

	private static ResolveControllers createController(String controllerStr) {
		ResolveControllers controller = null;
		try {
        HibernateProxy.setCurrentUser("system");
			controller = (ResolveControllers) HibernateProxy.execute(() -> {
				ResolveControllers searchedController = new ResolveControllers();
				searchedController.setUControllerName(controllerStr);

				return HibernateUtil.getDAOFactory().getResolveControllersDAO().persist(searchedController);

			});
		} catch (Exception e) {
			Log.log.error("Failed to create controller " + controller, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return controller;

	}

    private static Map<String, ControllerProperty> cacheControllerAndRoles(boolean invalidateClusterCache)
    {
        Map<String, ControllerProperty> mapControllerRoles = prepareMapForController();

        //cache it
        if(mapControllerRoles != null)
        {
            CacheUtil.putCacheObj(CONTROLLER_CACHE_KEY, mapControllerRoles);
            
            if (invalidateClusterCache) // True only for local save/delete, False otherwise
            {
                Log.log.info("Sending message to other rsviews in cluster to invalidate " + CONTROLLER_CACHE_KEY + " cache from local object cache..."); 
                //Invalidate RSView Controller and Roles Cache
                Map<String, String> msgProperties = new HashMap<String, String>();
                msgProperties.put(Constants.MSG_SOURCE_ID, MainBase.main.configId.getGuid());
                MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "MAction.invalidateControllerAndRolesCache", msgProperties);
            }
        }
        
        return mapControllerRoles;
    }

    
    @SuppressWarnings("unchecked")
    private static ControllerProperty getControllerProperty(String controllerName, String userOrg )
    {
        ControllerProperty result = null;

        Log.log.trace("Get controller property for " + controllerName); 
                        
        if (StringUtils.isNotEmpty(controllerName))
        {
            Map<String, ControllerProperty> mapControllerRoles = null;
            Map<String, ControllerProperty> map  = null;
            //HACK - There were scenarios where the cache does not have this key and that behaviour is not consistent at all. In that case, here we are making 1 attempt to read and 
            //populate the data again in the cache. Once the cache bug is fixed, we can remove this reloading of the roles again
            if(!CacheUtil.isKeyInCache(CONTROLLER_CACHE_KEY))
            {
                Log.log.trace(CONTROLLER_CACHE_KEY + 
                              " not found in local object cache, reloading..."); 
                mapControllerRoles = cacheControllerAndRoles(false);
            }
            
            //get the controller property
            if (CacheUtil.isKeyInCache(CONTROLLER_CACHE_KEY))
            {
                map = (Map<String, ControllerProperty>) CacheUtil.getCacheObj(CONTROLLER_CACHE_KEY);
            }
            else
            {
                Log.log.warn("CACHE FOR APP RIGHTS IS NOT WORKING. AS A WORKAROUND, PLEASE RESTART THE RSVIEW AND LET THE ADMINISTRATOR KNOW.");
                map = mapControllerRoles;
            }
            

            if (map != null && map.containsKey(controllerName))
            {
                result = map.get(controllerName);
            }
        
            if (result == null)
            {
                
                //try to remove the last value and find it furthur
                //eg make '/sysproperties/getAllProperties' to '/sysproperties' and find the roles
                int lastIndexOfPosition = controllerName.lastIndexOf("/");
                if (StringUtils.isNotBlank(controllerName) && lastIndexOfPosition > 1) //first '/' will always be there, so start from 1
                {
                    String newControllerName = controllerName.substring(0, lastIndexOfPosition);

                    //RECURSIVE
                    result = getControllerProperty(newControllerName, userOrg);
                }
            }
        }

        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("Controller property for " + controllerName + " is " + 
                          (result == null ? "Null" : result.controllerRoles));
        }
        
        return result;
    }

    private static Map<String, ControllerProperty> prepareMapForController()
    {
        
        Map<String, ControllerProperty> map = new HashMap<String, ControllerProperty>();

        try
        {

          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
        		List<ResolveControllers> list = null;
                list = HibernateUtil.getDAOFactory().getResolveControllersDAO().findAll();
                if (list != null && list.size() > 0)
                {
                    for (ResolveControllers controller : list)
                    {
                        if (!controller.ugetSysIsDeleted())
                        {
                            ControllerProperty controllerProperty = new ControllerProperty();
                            controllerProperty.name = controller.getUControllerName();
                            controllerProperty.controllerRoles = new HashSet<String>();
                            controllerProperty.orgs = new HashSet<String>();

                            //apps rel
                            Collection<ResolveAppControllerRel> appsRels = controller.getResolveAppControllerRels();
                            if (appsRels != null)
                            {
                                for (ResolveAppControllerRel rel : appsRels)
                                {
                                    if (!rel.ugetSysIsDeleted())
                                    {
                                        ResolveApps app = rel.getResolveApp();
                                        
                                        if (app == null)
                                        {
                                            Log.log.warn("" + controllerProperty.name + 
                                                         " controller's Controller Resolve App Relation is missing Resolve Application.");
                                        }
                                        
                                        if (app != null && !app.ugetSysIsDeleted())
                                        {
                                            //orgs
                                            Collection<ResolveAppOrganizationRel> orgsRels = app.getResolveAppOrgRel();
                                            if (orgsRels != null)
                                            {
                                                for (ResolveAppOrganizationRel orgRel : orgsRels)
                                                {
                                                    if (!orgRel.ugetSysIsDeleted())
                                                    {
                                                        //add the org sysid to list
                                                        controllerProperty.orgs.add(orgRel.getOrganization().getSys_id());
                                                    }
                                                }
                                            }//end of if

                                            //roles
                                            Collection<ResolveAppRoleRel> rolesRels = app.getResolveAppRoleRels();
                                            if (rolesRels != null)
                                            {
                                                for (ResolveAppRoleRel roleRel : rolesRels)
                                                {
                                                    if (!roleRel.ugetSysIsDeleted())
                                                    {
                                                        //add the role string to list
                                                        Roles roles = roleRel.getRoles();
                                                        
                                                        if (roles != null && StringUtils.isNotBlank(roles.getUName()))
                                                        {
                                                            controllerProperty.controllerRoles.add(roles.getUName());
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }//end of if
                                }//end of for loop
                            }//end of if

                            //add it to the map
                            map.put(controller.getUControllerName(), controllerProperty);

                        }//end of if
                    }
                }

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Failed to find out the recs if exist or not: ", e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return map;
    }
    
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static Map<String, String> getMapOfAppsAndRoles()
    {
        Map<String, String> mapOfAppsAndRoles = new HashMap<String, String>();
        String hql = "from ResolveApps where sysIsDeleted is null OR sysIsDeleted = false";
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                Query query = HibernateUtil.createQuery(hql);
                List<ResolveApps> list = query.list();
                
                if(list != null && list.size() > 0)
                {
                    for(ResolveApps app : list)
                    {
                        Collection<ResolveAppRoleRel> rolesRel = app.getResolveAppRoleRels();
                        if(rolesRel != null)
                        {
                            StringBuilder roles = new StringBuilder();

                            for (ResolveAppRoleRel roleRel : rolesRel)
                            {
                                Roles role = roleRel.getRoles();
                                if (role != null)
                                {
                                    roles.append(role.getUName()).append(",");
                                }
                            }//end of for loop
                            
                            String rolesStr = null;
                            if(roles.indexOf(",") > -1)
                            {
                                rolesStr = roles.substring(0, roles.lastIndexOf(","));
                            }
                            else
                            {
                                rolesStr = roles.toString();
                            }
                            mapOfAppsAndRoles.put(app.getSys_id(), rolesStr);
                            
                        }//end of if
                    }//end of for                
                }//end of if
        	});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return mapOfAppsAndRoles;
    }
    
    private static void assignArtifactsToApp(ResolveAppsVO vo, ResolveApps model, String username)
    {
        if(vo != null && model != null)
        {
            //controllers
            List<ResolveControllersVO> appControllers = vo.getAppControllers();
            
            //roles
            List<RolesVO> appRoles = vo.getAppRoles();
            
            //orgs
            List<OrganizationVO> appOrgs = vo.getAppOrgs();

            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                ResolveApps modelDb = HibernateUtil.getDAOFactory().getResolveAppsDAO().findById(model.getSys_id());
	                if (modelDb != null)
	                {
	
	                    //update all the rel recs isDeleted to false simulating delete
	                    //roles
	                    Collection<ResolveAppRoleRel> rolesRelsDB = modelDb.getResolveAppRoleRels();
	                    for (ResolveAppRoleRel rolesRelDB : rolesRelsDB)
	                    {
	//                        rolesRelDB.setSysIsDeleted(true);
	                        HibernateUtil.getDAOFactory().getResolveAppRoleRelDAO().delete(rolesRelDB);
	                    }
	
	                    //controllers
	                    Collection<ResolveAppControllerRel> controllerRelsDB = modelDb.getResolveAppControllerRels();
	                    for (ResolveAppControllerRel controllerRelDB : controllerRelsDB)
	                    {
	//                        controllerRelDB.setSysIsDeleted(true);
	                        HibernateUtil.getDAOFactory().getResolveAppControllerRelDAO().delete(controllerRelDB);
	                    }
	
	                    //orgs
	                    Collection<ResolveAppOrganizationRel> resolveAppOrgRels = modelDb.getResolveAppOrgRel();
	                    for (ResolveAppOrganizationRel rel : resolveAppOrgRels)
	                    {
	//                        rel.setSysIsDeleted(true);
	                        HibernateUtil.getDAOFactory().getResolveAppOrganizationRelDAO().delete(rel);
	                    }
	
	                    //now go through what the user selected from the UI.
	                    //roles
	                    if (appRoles != null)
	                    {
	                        for (RolesVO rolesVO : appRoles)
	                        {
	//                            ResolveAppRoleRel rel = findRoleAssignedToApp(rolesRelsDB, rolesVO.getSys_id());
	//                            if (rel != null)
	//                            {
	//                                rel.setSysIsDeleted(false);
	//                            }
	//                            else
	//                            {
	                                ResolveAppRoleRel rel = new ResolveAppRoleRel();
	                                rel.setResolveApp(modelDb);
	                                rel.setRoles(new Roles(rolesVO));
	//                            }
	
	                            //persist it
	                            HibernateUtil.getDAOFactory().getResolveAppRoleRelDAO().persist(rel);
	                        }
	                    }
	                    if (appControllers != null)
	                    {
	                        //controllers
	                        for (ResolveControllersVO controllerVO : appControllers)
	                        {
	//                            ResolveAppControllerRel rel = findControllerAssignedToApp(controllerRelsDB, controllerVO.getSys_id());
	//                            if (rel != null)
	//                            {
	//                                rel.setSysIsDeleted(false);
	//                            }
	//                            else
	//                            {
	                                ResolveAppControllerRel rel = new ResolveAppControllerRel();
	                                rel.setResolveApp(modelDb);
	                                rel.setResolveController(new ResolveControllers(controllerVO));
	//                            }
	
	                            //persist it
	                            HibernateUtil.getDAOFactory().getResolveAppControllerRelDAO().persist(rel);
	                        }
	                    }
	                    //orgs
	                    
	                    
	                    if (appOrgs != null)
	                    {
	                        for (OrganizationVO orgVO : appOrgs)
	                        {
	//                            ResolveAppOrganizationRel rel = findOrgAssignedToApp(resolveAppOrgRels, orgVO.getSys_id());
	//                            if (rel != null)
	//                            {
	//                                rel.setSysIsDeleted(false);
	//                            }
	//                            else
	//                            {
	                                ResolveAppOrganizationRel rel = new ResolveAppOrganizationRel();
	                                rel.setResolveApp(modelDb);
	                                rel.setOrganization(new Organization(orgVO));
	//                            }
	
	                            //persist it
	                            HibernateUtil.getDAOFactory().getResolveAppOrganizationRelDAO().persist(rel);
	                        }
	                    }
	
	                }//end of if

                });
            }
            catch (Exception e)
            {
                Log.log.error("Failed to assign controller to app: " + e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

    }
    
//    private static ResolveAppControllerRel findControllerAssignedToApp(Collection<ResolveAppControllerRel> controllerRelsDB, String controllerSysId)
//    {
//        ResolveAppControllerRel result = null;
//        
//        if(controllerRelsDB != null && StringUtils.isNotBlank(controllerSysId))
//        {
//            for(ResolveAppControllerRel rel : controllerRelsDB)
//            {
//                if(rel.getResolveController().getSys_id().equalsIgnoreCase(controllerSysId))
//                {
//                    result = rel;
//                    break;
//                }
//            }
//        }
//        
//        return result;
//    }
    
//    private static ResolveAppRoleRel findRoleAssignedToApp(Collection<ResolveAppRoleRel> rolesRelsDB, String roleSysId)
//    {
//        ResolveAppRoleRel result = null;
//        
//        if(rolesRelsDB != null && StringUtils.isNotBlank(roleSysId))
//        {
//            for(ResolveAppRoleRel rel : rolesRelsDB)
//            {
//                if(rel.getRoles().getSys_id().equalsIgnoreCase(roleSysId))
//                {
//                    result = rel;
//                    break;
//                }
//            }
//        }
//        
//        return result;
//    }
    
//    private static ResolveAppOrganizationRel findOrgAssignedToApp(Collection<ResolveAppOrganizationRel> orgsRelsDB, String orgSysId)
//    {
//        ResolveAppOrganizationRel result = null;
//        
//        if(orgsRelsDB != null && StringUtils.isNotBlank(orgSysId))
//        {
//            for(ResolveAppOrganizationRel rel : orgsRelsDB)
//            {
//                if(rel.getOrganization().getSys_id().equalsIgnoreCase(orgSysId))
//                {
//                    result = rel;
//                    break;
//                }
//            }
//        }
//        
//        return result;
//    }

    public static void invalidateControllerAndRolesCache()
    {
        CacheUtil.invalidateCache(CONTROLLER_CACHE_KEY);
    }
    
    public static class ControllerProperty
    {
        public String controllers;
        public String roles;
        public String description;
        public String name;

        //additional properties used for cacheing
        public Set<String> controllerRoles;
        public Set<String> orgs; //sysIds of orgs that this controller can be used 
    }

}
