/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.dao.ResolveTagDAO;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocResolveTagRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.WikiDocumentDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TagUtil
{
    public static ResponseDTO<ResolveTagVO> listTags(QueryDTO query, String username) throws Exception
    {
        ResponseDTO<ResolveTagVO> response = new ResponseDTO<ResolveTagVO>();
        //making sure that query obj is not null, if null, it will pull all the recs
        if(query == null)
        {
            query = new QueryDTO();
        }
        query.setModelName("ResolveTag");
        
        int start = query.getStart();
        int limit = query.getLimit();
        int total  = 0;
        List<ResolveTagVO> vos = new ArrayList<ResolveTagVO>();
        
        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
//                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveTag instance = new ResolveTag();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        vos.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveTag instance = (ResolveTag) o;
                        vos.add(instance.doGetVO());
                    }
                }
            }//end of if

            //get the total count
            total = ServiceHibernate.getTotalHqlCount(query);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting list of tags", e);
            throw e;
        }
        
        //collect the data
        response.setRecords(vos);
        response.setTotal(total);
        
        return response;
    }
    
    public static ResolveTagVO getTag(String tagSysId, String tagName, String username) throws Exception
    {
        ResolveTagVO vo = null;

        ResolveTag model = findTag(tagSysId, tagName, username);
        if(model != null)
        {
            vo = model.doGetVO();
        }

        return vo;
    }
    
    public static ResolveTagVO createTag(String tagName, String username) throws Exception
    {
        ResolveTagVO result = null;
        
        if(StringUtils.isNotBlank(tagName))
        {
            ResolveTag model = findTag(null, tagName, username);
            if(model != null)
            {
                result = model.doGetVO();
            }
            else
            {
                //if there is no tag with this name, create one
                ResolveTagVO tag = new ResolveTagVO();
                tag.setUName(tagName);
                
                result = saveTag(tag, username);
            }
        }

        return result;
    }
    
    
    public static ResolveTagVO saveTag(ResolveTagVO tag, String username) throws Exception
    {
        if(tag == null)
        {
            throw new Exception("Tag object cannot be null");
        }
         
        ResolveTag model = null;
        String sysId = (tag.getSys_id() == null || tag.getSys_id().equals(VO.STRING_DEFAULT)) ? "" : tag.getSys_id();
        String tagName = tag.getUName();
        if(StringUtils.isEmpty(tagName))
        {
            throw new Exception("Tag name cannot be empty and is mandatory.");
        }
        
        ResolveTagVO testTag = getTag(null, tagName, username);
        if(testTag != null)
        {
            if(StringUtils.isEmpty(sysId) && testTag.getUName().equalsIgnoreCase(tagName))
            {
                Log.log.warn("Tag with name '" + tagName + "' already exist.");
                return testTag;
                //throw new Exception("Tag with name '" + tagName + "' already exist.");
            }
            else if(StringUtils.isNotEmpty(sysId) 
                            && testTag.getUName().equalsIgnoreCase(tagName) 
                            && !testTag.getSys_id().equalsIgnoreCase(sysId))
            {
                throw new Exception("Tag with name '" + tagName + "' already exist with sys_id " + 
                                    testTag.getSys_id() + " and is different from expected sys id " + sysId+ " (case ignored).");
            }
        }
        
        try
        {
        	ResolveTagVO tagFinal = tag;
          HibernateProxy.setCurrentUser(username);
        	model = (ResolveTag) HibernateProxy.execute(()-> {
        		ResolveTag modelFinal;
	            ResolveTagDAO tagDao = HibernateUtil.getDAOFactory().getResolveTagDAO();
	            
	            if(StringUtils.isNotBlank(sysId))
	            {
	            	modelFinal = tagDao.findById(sysId);
	                if(modelFinal == null)
	                {
	                    //for the ones coming in from graph
	                	modelFinal = new ResolveTag(tagFinal);
	                	modelFinal.setSys_id(null); //this will not be null coming in from graph...so creating new sysId for it...
	                }
	                else
	                {
	                	modelFinal.applyVOToModel(tagFinal);
	                }
	            }
	            else
	            {
	            	modelFinal = new ResolveTag(tagFinal);
	            }
	            
	            //persist it
	            modelFinal.setChecksum(Objects.hash(modelFinal.getUName()));
	            tagDao.persist(modelFinal);
	            return modelFinal;
            
            });
            
            //TODO: Still need to decide if we want to do this save it in ES - index the tag
          
            
            //get the updated tag
            tag = getTag(model.getSys_id(), null, username);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);            
            throw new Exception(e);
        }
        
        return tag;
    }
    
    public static void deleteResolveTagsByIds(List<String> tagSysIds, String username)  throws Exception
    {
        if(tagSysIds != null && tagSysIds.size() > 0)
        {
            //to make sure they are unique
            //Set<String> sysIds = new HashSet<String>(tagSysIds);
            //String queryStr = SQLUtils.prepareQueryString(new ArrayList<String>(sysIds));
            //String whereClause = "sys_id IN (" + queryStr + ")";
            //String whereClauseForTagRel = "tag IN (" + queryStr + ")";
    
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
	                
	                //ServiceHibernate.deleteQuery("ResolveTag", whereClause, username);
	                ServiceHibernate.deleteQuery("ResolveTag", "sys_id", tagSysIds, username);
	                
	                //ServiceHibernate.deleteQuery("ResolveCatalogNodeTagRel", whereClauseForTagRel, username);
	                ServiceHibernate.deleteQuery("ResolveCatalogNodeTagRel", "tag", tagSysIds, username);
	                
	                //ServiceHibernate.deleteQuery("WikidocResolveTagRel", whereClauseForTagRel, username);
	                ServiceHibernate.deleteQuery("WikidocResolveTagRel", "tag", tagSysIds, username);
	                
	                //ServiceHibernate.deleteQuery("ResolveActionTaskResolveTagRel", whereClauseForTagRel, username);
	                ServiceHibernate.deleteQuery("ResolveActionTaskResolveTagRel", "tag", tagSysIds, username);
                
            	});
            }
            catch(Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e);
            }
        }
        
    }
    
    
    public static Set<WikiDocumentDTO> findDocumentsWithTags(Collection<String> tags, String username) throws Exception
    {
        Set<WikiDocumentDTO> result = new HashSet<WikiDocumentDTO>();
        
        if(tags != null && tags.size() > 0)
        {
            for(String tag : tags)
            {
                result.addAll(findDocumentWithTag(tag, username));
            }//end of for loop
        }
        
        return result;
    }
    
    public static ResolveTagVO rename(String from, String to, String username) throws Exception
    {
        ResolveTagVO tag = null;
        if(StringUtils.isNotEmpty(from) && StringUtils.isNotEmpty(to))
        {
            tag = getTag(null, from, username);
            if(tag != null)
            {
                tag.setUName(to);
                tag = saveTag(tag, username);
            }
            else
            {
                throw new Exception("Tag " + from + "does not exist.");
            }
        }
        
        return tag;
    }
    
    //private apis
    private static Set<WikiDocumentDTO> findDocumentWithTag(String tag, String username) throws Exception
    {
        Set<WikiDocumentDTO> result = new HashSet<WikiDocumentDTO>();
        
        if(StringUtils.isNotBlank(tag))
        {
            try
            {
                ResolveTag model = findTag(null, tag, username);
                if(model != null)
                {
                    Collection<WikidocResolveTagRel> wikiTagRels = model.getWikiTagRels();
                    if(wikiTagRels != null && wikiTagRels.size() > 0)
                    {
                        for(WikidocResolveTagRel rel : wikiTagRels)
                        {
                            WikiDocument doc = rel.getWikidoc();
                            if(doc != null && doc.ugetUIsActive() && !doc.ugetUIsHidden() && !doc.ugetUIsLocked() && !doc.ugetUIsDeleted())
                            {
                                result.add(WikiUtils.prepareWikiDTO(doc));
                            }//end of if
                        }
                    }
                }
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        
        
        
        return result;
    }

    public static ResolveTag findTag(String sysId, String name, String username) throws Exception
    {
        ResolveTag model = null;
        
        if(StringUtils.isNotEmpty(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	model = (ResolveTag) HibernateProxy.execute(() -> {
                
            		return HibernateUtil.getDAOFactory().getResolveTagDAO().findById(sysId);
                
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(name))
        {
            //to make this case-insensitive
            //String sql = "from ResolveTag where LOWER(UName) = '" + name.toLowerCase().trim() + "' " ;
            String sql = "from ResolveTag where LOWER(UName) = :UName " ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", name.toLowerCase().trim());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    model = (ResolveTag) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }
            
        }
        return model;
    }
        
    /**
     * TODO:
     * gets list of docs based on the 'and' and 'or' clauses of tags
     * 
     * @param tagNames - tag1&tag2,tag4,tag5&tag6
     * @param username
     * @return
     * @throws Exception
     */
    public static List<WikiDocumentVO> findListOfDocumentsHavingTags(Set<String> tagNames, String docNamespace, String username) throws Exception
    {
        List<WikiDocumentVO> result = new ArrayList<WikiDocumentVO>();
        
        if(tagNames != null && tagNames.size() > 0)
        {
            for(String tagName : tagNames)
            {
                if(StringUtils.isNotBlank(tagName))
                {
                    if(tagName.indexOf('&') > -1)
                    {
                        result.addAll(findListOfDocumentsHavingTagsWithAndClause(tagName, docNamespace, username));
                    }
                    else
                    {
                        result.addAll(findListOfDocumentsHavingTag(tagName, docNamespace, username));
                    }
                }
            }
        }
        
        
        return result;
    }
//    
//    //private apis
    private static List<WikiDocumentVO> findListOfDocumentsHavingTag(String tagName, String docNamespace, String username) throws Exception
    {
        List<WikiDocumentVO> result = new ArrayList<WikiDocumentVO>();
        
        StringBuilder hql = new StringBuilder("select wd from WikiDocument wd, WikidocResolveTagRel rel, ResolveTag t");
        hql.append(" where wd.sys_id = rel.wikidoc");
        hql.append(" and rel.tag = t.sys_id");
        //hql.append(" and lower(t.UName) = '" + tagName.toLowerCase() + "'");
        hql.append(" and lower(t.UName) = :UName");
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UName", tagName.toLowerCase());
        
        if (StringUtils.isNotBlank(docNamespace))
        {
            //hql.append(" and wd.UNamespace = '" + docNamespace + "'");
            hql.append(" and wd.UNamespace = :UNamespace");
            queryParams.put("UNamespace", docNamespace);
        }
        
        try
        {
        	Log.log.trace("Executing tag hql: " + hql);
            List<? extends Object> docs = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
            
            for (Object doc : docs)
            {
            	Log.log.trace("Adding Document: " + ((WikiDocument) doc).getUFullname());
                result.add(((WikiDocument) doc).doGetVO());
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    }
    
    private static List<WikiDocumentVO> findListOfDocumentsHavingTagsWithAndClause(String tags, String docNamespace, String username) throws Exception
    {
        List<WikiDocumentVO> result = new ArrayList<WikiDocumentVO>();
        
        String[] tagsAry = tags.split("&");
        
        StringBuilder hql = new StringBuilder("select wd.id from WikiDocument wd, WikidocResolveTagRel rel, ResolveTag t");
        hql.append(" where wd.sys_id = rel.wikidoc");
        hql.append(" and rel.tag = t.sys_id");
        //hql.append(" and lower(t.UName) in (");
        hql.append(" and lower(t.UName) in (:UName)");
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
        
        queryInParams.put("UName", new ArrayList<Object>(Arrays.asList(tagsAry)));
        
        if (StringUtils.isNotBlank(docNamespace))
        {
            //hql.append(" and wd.UNamespace = '" + docNamespace + "'");
            hql.append(" and wd.UNamespace = :UNamespace");
            queryParams.put("UNamespace", docNamespace);
        }
        hql.append(" group by wd.id having count(wd.id) = " + tagsAry.length);
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	        	Log.log.trace("Executing tag hql: " + hql);
	            List<? extends Object> docIds = GeneralHibernateUtil.executeHQLSelectIn(hql.toString(), queryInParams, queryParams);
	            
	            for (Object docId : docIds)
	            {
	            	WikiDocument doc = StoreUtility.findById((String) docId);
	            	Log.log.trace("Adding Document: " + doc.getUFullname());
	                result.add(doc.doGetVO());
	            }
            
        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        
        return result;
    }
    
    public static Integer getTagChecksum(String tagName, String username) {
        Integer checksum = null;
        ResolveTag tag = null;
        
        try {
            tag = findTag(null, tagName, username);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        if (tag != null) {
            checksum = tag.getChecksum();
        }
        return checksum;
    }
    
    public static Integer calculateChecksum(ResolveTag tag)
    {
        Integer checksum = 0;
        if (tag != null) {
            checksum = Objects.hash(tag.getUName());
        }
        return checksum;
    }
    
    public static void updateAllTagsForChecksum() {
        try {
            UpdateChecksumUtil.updateComponentChecksum("ResolveTag", TagUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
}
