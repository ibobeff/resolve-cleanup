package com.resolve.services.rb.util;

import java.util.List;

import com.resolve.persistence.model.RBGeneral;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.RBTaskVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class AutomationGenerationProcess extends Thread
{
    private String username;
    private String id;
    private ABProcessConst stage;
    private String message;
    
    private Boolean success;
    
    public AutomationGenerationProcess(String id, String username) {
        this.username = username;
        this.id = id;
        this.success = true;
    }
    
    public JSONObject getStatus() {
        JSONObject status = new JSONObject();
        //ObjectNode status = new ObjectMapper().createObjectNode();
        status.put("stage", this.stage.toString());
        status.put("success", this.success);
        status.put("message", this.message);
        return status;
    }
    
    private void cleanUp(){
        int count = 0;
        while(count<=5) {
            count++;
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        try
        {
            ResolutionBuilderUtils.pollABGeneration(this.username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
   
    @Override
    public void run()
    {
        
        if (StringUtils.isNotBlank(id))
        {
            this.stage = ABProcessConst.AB_GEN_GET_GENERAL;
            RBGeneral general = ResolutionBuilderUtils.getGeneralEntity(id, username);
            if (general == null)
            {
//                throw new Exception("Invalid Resolution builder Id: " + id);
                this.message = "Invalid Resolution builder Id: " + id;
                this.success = false;
                this.cleanUp();
                return;
            }
            
            this.stage = ABProcessConst.AB_GEN_GET_WIKI;
            // find the wiki document by the resolution builder id
            WikiDocumentVO wikiDocumentVO = ServiceWiki.getWikiDoc(general.getWikiId(), null, username);
            if (wikiDocumentVO == null)
            {
                // brand new generate
                // build wiki with parameters
            }
            else
            {
                // add parameters to the wiki
                // wikiDocumentVO.setUWikiParameters(createWikiParameters(general));
                
                if (general != null)
                {
                    this.stage = ABProcessConst.AB_GEN_GENERATE_FORM;
                    CustomForm form;
                    try
                    {
                        form = new CustomForm(general.doGetVO(), username);
                        RsFormDTO formDTO = form.create();
                        String formId = formDTO.getId();
                        String formName = formDTO.getName();
                        Log.log.debug("Form created with name : " + formName + " and formId : " + formId);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to create custom form. " + e.getMessage(), e);
                        this.success = false;
                        this.message = "Failed to create custom form. " + e.getMessage();
                        this.cleanUp();
                        return;
                    }
                    
                    /*
                    StringBuilder formSection = new StringBuilder();
                    formSection.append("{section:type=form}\n");
                    formSection.append("{form:name=").append(formName).append("}\n");
                    formSection.append("{section}\n");
                    wikiDocumentVO.setUContent(formSection.toString());
                    */
                }
                
                this.stage = ABProcessConst.AB_GEN_GENERATE_TASK;
                // create action tasks
                List<RBTaskVO> taskVOs = ResolutionBuilderUtils.findTasksByGeneralId(id, username);
                for (RBTaskVO taskVO : taskVOs)
                {
                    try
                    {
                        if(taskVO.getRefRunbookParams() == null || 
                           taskVO.getRefRunbookParams().trim().equalsIgnoreCase(VO.STRING_DEFAULT) ||
                           StringUtils.isBlank(taskVO.getRefRunbookParams().trim()))
                        {
                            ResolutionBuilderUtils.generateTask(general.doGetVO(), taskVO, username);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                        this.message = "Error generating task " + taskVO.getName() + " : " + e.getMessage();
                        this.success = false;
                        this.cleanUp();
                        return;
                    }
                }
                this.stage = ABProcessConst.AB_GEN_GENERATE_XML;
                ModelBuilder modelBuilder = new ModelBuilder();
                String modelProcess = modelBuilder.buildModelXml(general.getSys_id(), username);
                Log.log.debug("Model XML: " + modelProcess);
                this.stage = ABProcessConst.AB_GEN_UPDATE_WIKI;
                // redo
                wikiDocumentVO.setUModelProcess(modelProcess);
                try
                {
                    ServiceWiki.saveAndCommitWikiDoc(wikiDocumentVO, null, username, true);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    this.message = "Error saving and committing wikidoc " + wikiDocumentVO.getUFullname() + " with generated model: " + e.getMessage();
                    this.success = false;
                    this.cleanUp();
                    return;
                }

                // set the generated flag
                this.stage = ABProcessConst.AB_GEN_UPDATE_AB;
                try {
                    general.setGenerated(Boolean.TRUE);
                    HibernateProxy.execute(() -> {
                    	HibernateUtil.saveObj("RBGeneral", general);
                    });
                    
                }catch(Exception e) {
                    Log.log.error(e);
                    this.message = "Error in updating generated status in RBGeneral record for " + 
                                   general.getNamespace() + "." + general.getName() + " : " + e.getMessage();
                    this.success = false;
                    this.cleanUp();
                                                          HibernateUtil.rethrowNestedTransaction(e);
                    return;
                }
            }
        }
        this.stage = ABProcessConst.DONE;
        this.success = true;
        this.cleanUp();
    }
    
}
