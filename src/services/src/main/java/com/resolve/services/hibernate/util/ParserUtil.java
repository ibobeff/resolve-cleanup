/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.dao.ResolveParserDAO;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolveParserTemplate;
import com.resolve.persistence.util.DBCacheRegionConstants;import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.actiontask.FindParser;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveParserTemplateVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class ParserUtil
{
    @SuppressWarnings("unchecked")
	public static List<ResolveParserVO> getAllParserOfMethod(String methodType)
    {
        List<ResolveParserVO> result = new ArrayList<ResolveParserVO>();
        List<ResolveParser> list = new ArrayList<ResolveParser>();
        
        if(StringUtils.isNotEmpty(methodType))
        {
            ResolveParser resolveParser = new ResolveParser();
            resolveParser.setUMethod(methodType);
            
            try
            {
            	list = (List<ResolveParser>) HibernateProxy.execute(() -> {
                	ResolveParserDAO rpDao = HibernateUtil.getDAOFactory().getResolveParserDAO();
                    return rpDao.find(resolveParser);
                });               
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        if( list != null)
        {
            for(ResolveParser p : list)
            {
                result.add(p.doGetVO());
            }
        }
        
        return result;
    }
    
    public static List<ResolveParserVO> getResolveParser(QueryDTO query, String username)
    {
        List<ResolveParserVO> result = new ArrayList<ResolveParserVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveParser instance = new ResolveParser();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ResolveParser instance = (ResolveParser) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
	public static List<ResolveParserTemplateVO> getTemplatesForParser(String parserSysId, String parserName, String type)
    {
        List<ResolveParserTemplateVO> result = new ArrayList<ResolveParserTemplateVO>();
        Collection<ResolveParserTemplate> templates  = null;
        try
        {          

           templates = (Collection<ResolveParserTemplate>) HibernateProxy.execute(() -> {
        	   
        	   ResolveParser parser = new ResolveParser();
               parser.setUName(parserName);
            	
                if (StringUtils.isNotEmpty(parserSysId))
                {
                    return HibernateUtil.getDAOFactory().getResolveParserDAO().findById(parserSysId);
                }
                else if (StringUtils.isNotEmpty(parserName))
                {
                    return HibernateUtil.getDAOFactory().getResolveParserDAO().findFirst(parser);
                }  
                
                if (parser != null)
                {
                    return parser.getResolveParserTemplates();
                }
                
                return null;

            });          

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if(templates != null)
        {
            for(ResolveParserTemplate temp : templates )
            {
                if(StringUtils.isNotEmpty(type) && type.trim().equalsIgnoreCase(temp.getUType()))
                {
                    result.add(temp.doGetVO());
                }
                else if(StringUtils.isEmpty(type))
                {
                    //incase type is NULL, return all of them
                    result.add(temp.doGetVO());
                }
            }
        }
        
        return result;
        
    }
    
    public static ResolveParserVO saveResolveParser(ResolveParserVO vo, String username) throws Exception
    {
        if(vo != null && vo.validate())
        {
            ResolveParser model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findResolveParserModel(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("Parser with sysId " + vo.getSys_id() + " does not exist.");
                }
            }
            else
            {
                model = findResolveParserModel(null, vo.getUName(), username);
                if(model != null)
                {
                    throw new Exception("Parser with name " + vo.getUName() + " already exist");
                }
                
                model = new ResolveParser();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveResolveParser(model, username);
            
            //submit a request to archive it
            ResolveArchiveUtils.submitArchiveRequest("ResolveParser", "UScript", model.getSys_id(), username);
            
            vo = model.doGetVO();            

            HibernateUtil.clearDBCacheRegion(DBCacheRegionConstants.PARSER_CACHE_REGION, true, model.getSys_id());
        }
        
        return vo;
    }
    
    @SuppressWarnings("unchecked")
    public static Set<String> findActiontaskReferencesForParser(Set<String> parserSysIds, String username)   throws Exception
    {
        Set<String> result = new HashSet<String>();
        
        if(parserSysIds != null && parserSysIds.size() > 0)
        {
            //String qrySysIds = SQLUtils.prepareQueryString(new ArrayList<String>(parserSysIds));
            //String sql = "select UFullName from ResolveActionTask where resolveActionInvoc.sys_id in (select sys_id from ResolveActionInvoc where parser.sys_id IN ("+ qrySysIds +"))";
            String sql = "select UFullName from ResolveActionTask where resolveActionInvoc.sys_id in (select sys_id from ResolveActionInvoc where parser.sys_id IN (:qrySysIds))";
            
            Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
            
            queryInParams.put("qrySysIds", new ArrayList<Object>(parserSysIds));
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectInOnly(sql, queryInParams);
                result.addAll((List<String>) list);
            }
            catch(Exception e)
            {
                Log.log.error("error in sql " + sql, e);
            }
        }
        
        return result;
    }
    
    public static void deleteResolveParserByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {
        Set<String> sysIdsToDelete = new HashSet<String>();
        
        //prepare the set of ids to delete
        if (deleteAll)
        {
            //get all the sysIds from the table
            sysIdsToDelete.addAll(GeneralHibernateUtil.getSysIdsFor("ResolveParser", null, username));
        }
        else if(sysIds != null && sysIds.length > 0)
        {
            sysIdsToDelete.addAll(Arrays.asList(sysIds));
        }
        
        //delete tables one by one
        for(String sysIdToDelete : sysIdsToDelete)
        {
            try
            {
                deleteParserById(sysIdToDelete, username);
            }
            catch (Exception e)
            {
                Log.log.error("error deleting the parser with sysId :" + sysIdToDelete, e);
            }
        }
        
        //delete all the archives related to these sysIds
        String csvSysIds = StringUtils.convertCollectionToString(Arrays.asList(sysIds).iterator(), ",");
        ResolveArchiveUtils.submitPurgeArchiveRequest("ResolveParser", csvSysIds, username);
        
    }
    
    private static void deleteParserById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
            
	            ResolveParser parser = HibernateUtil.getDAOFactory().getResolveParserDAO().findById(sysId);
	            if(parser != null)
	            {
	                Collection<ResolveParserTemplate> resolveParserTemplates = parser.getResolveParserTemplates();
	                if(resolveParserTemplates != null)
	                {
	                    for(ResolveParserTemplate temp : resolveParserTemplates)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveParserTemplateDAO().delete(temp);
	                    }
	                }
	                                
	                HibernateUtil.getDAOFactory().getResolveParserDAO().delete(parser);
	            }
	            
        	});
        }
        catch (Throwable t)
        {
            String error = "Error deleting parser :" + sysId;
            Log.log.error(error, t);
            throw new Exception(t);
        }
    }
    
    public static String getParserScript(String name, String username) throws Exception
    {
        String result = null;

        if (StringUtils.isNotBlank(name))
        {
            try
            {
                
                ResolveParserVO parser = findResolveParser(null, name, username);
                if(parser != null)
                {
                    result = parser.getUScript();
                    if (StringUtils.isEmpty(result))
                    {
                        throw new Exception("FAILED: Missing script content - parser: " + name);
                    }
                }
                else
                {
                    throw new Exception("FAILED: Unable to find parser: " + name);
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    } // getParserScript
    
    //The No-cache version for parser
    public static ResolveParserVO findResolveParserNoCache(String sysId, String name, String username)
    {
        ResolveParserVO result = null;
        
        try 
        {
          HibernateProxy.setCurrentUser(username);
            result = (ResolveParserVO) HibernateProxy.executeNoCache(() -> {
            	return findResolveParser(sysId, name, username);
            }); 
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
        
    } // findResolveParser
    
    
    public static ResolveParserVO findResolveParser(String sysId, String name, String username)
    {
        ResolveParserVO result = null;

        try
        {
            FindParser findParser = new FindParser(sysId, name, username);
            result = findParser.get();
            
//            parser = findResolveParserModel(sysId, name, username);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
        
    } // findResolveParser

    public static ResolveParserVO findResolveParserWithoutRefs(String sysId, String name, String username)
    {
        ResolveParserVO result = null;

        try
        {
            FindParser findParser = new FindParser(sysId, name, username);
            result = findParser.getWithoutRefs();
            
//            parser = findResolveParserModel(sysId, name, username);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
        
    } // findResolveParser
    
    
    public static Collection<ResolveParserVO> findResolveParserByNames(Set<String> assessorNames, String username)
    {
        Collection<ResolveParserVO> result = new ArrayList<ResolveParserVO>();
        
        if (assessorNames != null && assessorNames.size() > 0)
        {
            //prepare list of actiontasks based on invoc sysIds
//            String sql = "select sys_id,UName,UDescription,sysUpdatedOn from ResolveParser where UName IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(assessorNames)) + ")";
            QueryDTO query = new QueryDTO();
            try
            {
                
                query.setModelName("ResolveParser");
                query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");
                query.setWhereClause("UName IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(assessorNames)) + ")");
                
                result.addAll(getResolveParser(query, username));

                //            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql);
                //            for (Object o : list)
                //            {
                //                ResolveAssess instance = (ResolveAssess) o;
                //                result.add(instance.doGetVO());
                //            }
            }
            catch (Exception e)
            {
                Log.log.error("error in sql " + query.getSelectHQL(), e);
            }
        }
        return result;
        
    }
    
    private static ResolveParser findResolveParserModel(String sysId, String name, String username)
    {
        ResolveParser result = null;

        if(StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	result = (ResolveParser) HibernateProxy.execute(() -> {
    
            		return HibernateUtil.getDAOFactory().getResolveParserDAO().findById(sysId);
    
            	});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(name))
        {
          //to make this case-insensitive
            //String sql = "from ResolveParser where LOWER(UName) = '" + name.toLowerCase().trim() + "'" ;
            String sql = "from ResolveParser where LOWER(UName) = :UName" ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", name.toLowerCase().trim());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (ResolveParser) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }
        }
        
        return result;
    }
    
    public static void copyParser(ResolveActionTaskVO taskVO, ResolveParserVO assessVO, String username) {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        	
	            ResolveActionInvocVO actionInvocVO = taskVO.getResolveActionInvoc();
	            String taskFullName = taskVO.getUFullName();
	            String newName = taskFullName + System.nanoTime();
	            
	            ResolveParserVO assess = actionInvocVO.getParser();
	            
	            if(assess != null) {
	                assess.setSys_id(null);
	                assess.setUName(newName);
	                Log.log.debug("new assess name = " + newName);
	                
	                ResolveParser model = new ResolveParser();
	                model.applyVOToModel(assess);
	                HibernateUtil.getDAOFactory().getResolveParserDAO().persist(model);
	                assess = model.doGetVO();
	                Log.log.debug("new assess id = " + assess.getSys_id());
	                actionInvocVO.setParser(assess);
	            }
	    
	            ResolveActionInvoc actionInvoc = new ResolveActionInvoc(actionInvocVO);
	            HibernateUtil.getDAOFactory().getResolveActionInvocDAO().update(actionInvoc);
            
        	});
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
    } // copyParser()
    
    public static void replaceResolveParser(ResolveParserVO toDelete, ResolveParserVO renameTo, String username) {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
                
                ResolveParser model = new ResolveParser();
                model.applyVOToModel(toDelete);
                HibernateUtil.getDAOFactory().getResolveParserDAO().delete(model);
                
            });
            
            HibernateProxy.execute(() -> {
            	ResolveParser m = new ResolveParser();
                m.applyVOToModel(renameTo);
                HibernateUtil.getDAOFactory().getResolveParserDAO().persist(m);
            });
            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
    } // replaceResolveParser()
    
} // ParserUtil
