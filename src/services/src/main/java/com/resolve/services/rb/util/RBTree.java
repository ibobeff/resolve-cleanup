/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import java.util.LinkedList;
import java.util.List;

public class RBTree
{
    private Object data;
    
    //Map<Integer, Object> children = new LinkedHashMap<Integer, Object>();
    List<Object> children = new LinkedList<Object>();

    public RBTree(Object data)
    {
        super();
        this.data = data;
    }

    public Object getData()
    {
        return data;
    }
    public void setData(Object data)
    {
        this.data = data;
    }

    public List<Object> getChildren()
    {
        return children;
    }

    public void setChildren(List<Object> children)
    {
        this.children = children;
    }
    public RBTree addChildren(Object child)
    {
        children.add(child);
        return this;
    }
}
