package com.resolve.services.util;

import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_DESCRIPTION_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_INCIDENT_ID_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_TEMPLATE_ACTIVITY_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_RUNTIME_STATUS_DEFAULT_VALUE;
import static com.resolve.services.hibernate.vo.WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_POSITION_KEY;
import static com.resolve.util.Log.log;
import static java.util.stream.Collectors.toSet;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.type.StringType;
import org.joda.time.DateTime;

import com.resolve.dto.SIRBulkUpdatePayloadDTO;
import com.resolve.exception.LimitExceededException;
import com.resolve.persistence.dao.WikiAttachmentContentDAO;
import com.resolve.persistence.dao.WikiAttachmentDAO;
import com.resolve.persistence.model.CustomDataFormTab;
import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.persistence.model.SIRActivityMetaData;
import com.resolve.persistence.model.SIRActivityRuntimeData;
import com.resolve.persistence.model.SIRConfig;
import com.resolve.persistence.model.SIRPhaseMetaData;
import com.resolve.persistence.model.SIRPhaseRuntimeData;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.model.WikiArchive;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.model.sir.MeanTimeFilterContainer;
import com.resolve.rsview.model.sir.SIRMeanTimeReportGroupType;
import com.resolve.rsview.model.sir.SIRMeanTimeReportType;
import com.resolve.rsview.model.sir.SIRSeverityFilter;
import com.resolve.search.SearchException;
import com.resolve.search.model.PBActivity;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBAuditLog;
import com.resolve.search.model.PBNotes;
import com.resolve.search.model.SecurityIncident;
import com.resolve.search.model.Worksheet;
import com.resolve.search.playbook.PlaybookIndexAPI;
import com.resolve.search.playbook.PlaybookSearchAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.archive.ArchiveSIRData;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.OrgsUtil;
import com.resolve.services.hibernate.util.PlaybookActivityUtils;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.RBACUtils;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiAttachmentUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ActivityVO;
import com.resolve.services.hibernate.vo.CustomDataFormTabVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.hibernate.vo.SIRConfigVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiArchiveVO;
import com.resolve.services.hibernate.vo.WikiAttachmentContentVO;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.DecisionTreeHelper;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.hibernate.wiki.WikiArchiveUtil;
import com.resolve.services.interfaces.VO;
import com.resolve.services.sir.util.ActivityUtils;
import com.resolve.services.sir.util.PhaseUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.QuerySort.SortOrder;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.SecurityIncidentLightDTO;
import com.resolve.util.BeanUtil;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.ERR;
import com.resolve.util.GMTDate;
import com.resolve.util.Guid;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.attachment.WikiFileUpload;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class PlaybookUtils
{
	public static final String ERROR_CLOSED_INCIDENT = "Closed incident cannot be modified. Reopen it first";
	public static final String DENIED_VIEW_ACCESS_SECURITY_INCIDENTS = "User %s does not have view permission on Security Incident(s).";
	public static final String COMPLETE_REQUIRED_ACTIVITIES = "Found a required task/activity that still needs to be completed:";
	public static final String ACTIVITY_STATUS_FIELD_PREFIX = "Activity.";
	public static final String RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER = "is denied access to";
	public static final String RBAC_DOES_NOT_HAVE_ACCESS_TO_MESSAGE_MARKER = "does not have access to";
	public static final String INCIDENT_ID = "incidentId";
	public static final String SIR_STATUS_CLOSED = "Closed";
	public static final String NOTE_TITLE_CANNOT_BE_BLANK_MSG = "Note title cannot be blank";
	public static final String USER_DOES_NOT_ERROR_MSG_PREFIX = "User %s does not";
	public static final String BLANK_EXT_REF_ID_ERR_MSG = 
											"External reference id cannot be blank for non Resolve or blank source system.";
	public static final String COULD_NOT_FIND_ERR_MSG_PREFIX = "Could not find";
	public static final String NOT_SUPPORTED_FOR = "not supperted for";
		
	private static final String EMPTY_SEARCH_CRITERIA = "Empty search criteria";
	private static final String NO_ORG_FILTER = "No organization filter provided";	
	private static final String NO_ACCESS_TO_ORG = "User has no access to provided organization";
	private static final String NULL_VALUE_CONDITION = "NOT _exists_:sys_org";
	private static final String OR_SEARCH_CONDITION = " OR ";
	private static final String SECURITY_INCIDENT_FT_BOOST_COLUMNS = "title^6,description^5,pbartifacts.name^4,pbartifacts.value^4,"
			+ "pbartifacts.source^4,pbartifacts.sourceValue^4,pbnotes.note^3,pbnotes.source^3,pbnotes.sourceValue^3,automation_results.summary^2,"
			+ "automation_results.detail^2,automation_results.esbaddr^2,automation_results.completion^2,automation_results.condition^2,"
			+ "automation_results.severity^2,_all";
    private static final String FILTER_NONE = "all";
    private static final String SPACE = " ";
    private static final String UNDER_SCORE = "_";
    private static final String COLON = ":";
    private static final String PERIOD = ".";
	private static final String COMMA = ", ";
    private static final String SIR_MODEL_NAME = "ResolveSecurityIncident";
    private static final String SIR_ACTIVITY_RUNTIMEDATA_MODEL_NAME = "SIRActivityRuntimeData";
    private static final String SIR_ACTIVITY_RUNTIMEDATA_MODEL_REFERENCED_RSI_PROPERTY_NAME = "referencedRSI";
    private static final String SIR_ACTIVITY_RUNTIMEDATA_MODEL_HQL_QUERY_ALIAS = "sard";
	private static final String BASE_MODEL_SYS_UPDATED_ON_PROPERTY_NAME = "sysUpdatedOn";
	private static final String MANDATORY_ACTIVITIES_NOT_IN_STATUSES_BY_SIR_IDS_HQL_QUERY = "SELECT DISTINCT sard from SIRActivityRuntimeData as sard "
                    + "WHERE sard.referencedRSI.sys_id in :ids "
                    + "AND sard.status not in :statuses "
                    + "AND sard.activityMetaData.isRequired = true";
	private static final String NOTE_INTO_SIR = "note into SIR";
	
	private static Map<String, String> INCIDENTS_INDEX_FILEDS_MAP = new HashMap<>();
	private static final List<String> ACTIVITY_COMPLETED_STATUSES = Arrays.asList("Complete", "Complete With Errors");
	private static final String USER_DOES_NOT_BELONG_TO_SIRS_ORG_ERR_MSG = USER_DOES_NOT_ERROR_MSG_PREFIX + 
			  															   " belong to SIR's Org.";
	private static final String USER_DOES_NOT_BELONG_TO_NO_OR_NONE_ORG_ERR_MSG = USER_DOES_NOT_ERROR_MSG_PREFIX + 
																				 " have access to No/None Org.";
	private static final String COULD_NOT_FIND_MASTER_SIR_ERR_MSG = COULD_NOT_FIND_ERR_MSG_PREFIX + 
																	" master SIR for sys_id: %s";
	private static final String PROPERTY_NOT_SUPPORTED_FOR_UPDATE_SIR_ERR_MSG = 
																	"Property %s " + NOT_SUPPORTED_FOR + " update in SIR. ";
	private static final String ACTIVITIES_IN_SIR = "[%s] activities in SIR %s";
	private static final String SIR_TITLED = "SIR titled \"%s\"";
	private static final String SIRS_PROPERTY = "property \"%s\" of %d SIRs";
	
    static {
    	Field[] fields = SecurityIncident.class.getDeclaredFields();

        for (Field field : fields) {
            if (field.isAnnotationPresent(JsonProperty.class)) {
                String annotationValue = field.getAnnotation(JsonProperty.class).value();
                INCIDENTS_INDEX_FILEDS_MAP.put(field.getName(), annotationValue);
            }
        }
    }
	
    @Deprecated
	public static void indexActivity(PBActivity  activity, String username) throws Exception
	{
		if (activity != null)
		{
			String worksheetId = getSecurityIncident(activity.getIncidentId(), null, username).getProblemId();
			if (StringUtils.isBlank(activity.getSysId()))
			{
				activity.setSysId(Guid.getGuid(true));
				activity.setSysCreatedBy(username);
				activity.setWorksheetId(worksheetId);
				activity = new PBActivity(activity);
			}
			else
			{
				long now = DateUtils.GetUTCDateLong();
				activity.setSysUpdatedBy(username);
				activity.setSysUpdatedDt(DateUtils.getDate(now));
		        activity.setSysUpdatedOn(now);
			}
			activity.setModified(true);
			PlaybookIndexAPI.indexPBActivity(activity, username);
		}
	}
	
	public static ResponseDTO<PBActivity> getActivities(QueryDTO queryDTO, String username) throws SearchException
	{
		return PlaybookSearchAPI.searchPBActivity(queryDTO, username);
	}
	
	public static ResponseDTO<PBActivity> getActivity(String activityId, String incidentId, String username) throws SearchException
	{
		return PlaybookSearchAPI.searchPBActivity(activityId, incidentId, username);
	}
	
	/**
	 * Index (save) / update a note
	 * <p>
     * Here's a sample PBNotes json String:
     * <pre><code>
     * {
       "note": "Test Note", // String representing a note.
       "incidentId": "8a9482f05b937a3d015b94b96cfd0005" // Id of an incident against which the note is taken.
     * } </code></pre>
     * 
     * Every note will have it's own sysId. If sysId field is not empty, an existing note will be updated. A new note will be created otherwise.
	 * @param note : Json string representing PBNotes.
	 * @param username : Username of a user who is invoking this API.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static PBNotes indexNote(PBNotes  note, String username) throws Exception
	{
		if (note != null)
		{
		    String auditMessage = null;
		    ResolveSecurityIncidentVO vo = getSecurityIncident(note.getIncidentId(), null, username);

    		if (vo != null && "Closed".equalsIgnoreCase(vo.getStatus())) {
        		throw new Exception(ERROR_CLOSED_INCIDENT);
            }

			String worksheetId = vo.getProblemId();
			if (StringUtils.isBlank(note.getSysId()))
			{
			    note.setSysId(Guid.getGuid(true));
				note.setSysCreatedBy(username);
				note.setPostedBy(username);
				note.setWorksheetId(worksheetId);
				note = new PBNotes(note);
				auditMessage = String.format("User '%s' added a new note '%s'", username, note.getNote());
			}
			note.setModified(true);
			String noteId = PlaybookIndexAPI.indexPBNotes(note, username);
			
			if (noteId != null)
			{
				note.setSysId(noteId);
				note.setId(noteId);
			}
			
			note.setAuditMessage(auditMessage.toString());
		}
		return note;
	}
	
	public static ResponseDTO <PBNotes> deleteNotes(String[] ids, String sir, String username)
	{
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	public static PBNotes sanitizeNote(String jsonNote) throws Exception
	{
		Map noteMap = new ObjectMapper().readValue(jsonNote, Map.class);
		String incidentId = noteMap.get("incidentId").toString();
		
		Object obj = noteMap.get("note");
		String note = noteMap.get("note").toString();
		
		if (obj instanceof Map)
		{
			note = new ObjectMapper().writeValueAsString(obj);
		}
		
		PBNotes pbNote = new PBNotes();
		pbNote.setNote(note);
		pbNote.setIncidentId(incidentId);
		pbNote.setSir(noteMap.containsKey("sir") ? noteMap.get("sir").toString() : null);
		if (StringUtils.isNotBlank((String)noteMap.get("category")))
		{
			pbNote.setCategory(noteMap.get("category").toString());
		}
		if (StringUtils.isNotBlank((String)noteMap.get("id")))
		{
			pbNote.setId(noteMap.get("id").toString()); 
		}
		
		// Set source / source value only if it's not null and not blank. SIRINC-16 
        pbNote.setSource("");
        pbNote.setSourceValue("");
        
        Object object = noteMap.get("source");
        if (object != null)
        {
            if (StringUtils.isNotBlank(object.toString()))
            {
                pbNote.setSource(object.toString());
            }
        }
        
        object = noteMap.get("sourceValue");
        if (object != null)
        {
            String sourceValue = object.toString();
            if (StringUtils.isNotBlank(sourceValue))
            {
                pbNote.setSourceValue(sourceValue.length() > 255 ? sourceValue.substring(0, 255) : sourceValue);
            }
        }
        
        String sourceAndValue = (StringUtils.isNotBlank(pbNote.getSource())? pbNote.getSource() + " " : "None") + 
                        (StringUtils.isBlank(pbNote.getSource()) ? "" : (StringUtils.isNotBlank(pbNote.getSourceValue())? pbNote.getSourceValue().replace(".", " ") : "None"));
        pbNote.setSourceAndValue(sourceAndValue);
        
        boolean titlePresent = false;
        object = noteMap.get("title");
        if (object != null)
        {
            String title = object.toString();
            if (StringUtils.isNotBlank(title))
            {
                titlePresent = true;
                pbNote.setTitle(title.length() > 255 ? title.substring(0, 255) : title);
            }
        }
        
        if (!titlePresent)
        {
            throw new Exception(NOTE_TITLE_CANNOT_BE_BLANK_MSG);
        }
		
		return pbNote;
	}
	
	public static ResponseDTO<PBNotes> searchPBNotes(QueryDTO queryDTO, String username) throws SearchException
	{
		if (queryDTO != null && queryDTO.getFilters().size() == 0)
		{
			// return an empty array if there's no filter set on the QueryDTO.
			ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
			List<PBNotes> list = new ArrayList<PBNotes>();
			result.setRecords(list).setTotal(0l);
			return result;
		}
		
		return PlaybookSearchAPI.searchPBNotes(queryDTO, username);
	}
	
	/**
	 * Fetch all the notes taken against an incident.
	 * 
	 * @param incidentId
	 * @param username
	 * @return a Json String representing {@link ResponseDTO}. Records field of ReponseDTO will contain an array of PBNotes object. Here's a sample response with one note:
	 * <pre>
	 * <code>
	 * {
       "success": true,
       "message": null,
       "data": null,
       "records": [
          {
             "sysId": "6d15a269090e254d357b6897d6353d4f",
             "sysOrg": null,
             "sysUpdatedBy": "admin",
             "sysUpdatedOn": 1492852879672,
             "sysUpdatedDt": 1492852879672,
             "sysCreatedBy": "admin",
             "sysCreatedOn": 1492852617936,
             "sysCreatedDt": 1492852617936,
             "worksheetId": "82d67f04586141a3945c49683742a8d9",
             "incidentId": "8a9482f05b937a3d015b94b96cfd0005",
             "note": "Test Note - 1",
             "activityName": null,
             "category": "",
             "postedBy": "admin",
             "componentId": null,
             "activityId": null,
             "id": "6d15a269090e254d357b6897d6353d4f"
          }
       ],
       "total": 1
    }
	 * </code></pre>
	 * @throws SearchException
	 * @throws LimitExceededException 
	 */
	public static ResponseDTO<PBNotes> searchPBNotesByIncidentId(String incidentId, String username, int maxLimit) throws SearchException, LimitExceededException
    {
		return PlaybookSearchAPI.searchPBNotesByIncidentId(incidentId, username, maxLimit);
    }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void saveArtifactToWSDATA(String problemId, String propName, String propValue) throws SearchException
    {
		String username = "system";
		Object obj = WorksheetUtil.getWorksheetData(problemId, propName, username);
		Set valSet = null; 
		if (obj!=null)
		{
			valSet = (HashSet)obj;
		}
		else
		{
			valSet = new HashSet();
		}
			
		valSet.add(propValue);		
		WorksheetUtil.setWorksheetWSDATAProperty(problemId, propName, valSet, username);
    }
	
	@SuppressWarnings({ "rawtypes" })
	public static void deleteArtifactFromWSDATA(String problemId, String propName, String propValue) throws SearchException
    {
		String username = "system";
		Object obj = WorksheetUtil.getWorksheetData(problemId, propName, username);
		Set valSet = null; 
		if (obj!=null)
		{
			valSet = (HashSet)obj;
		}
		
		if (valSet != null)
		{
    		valSet.remove(propValue);
    		
    		if (valSet.isEmpty())
            {
    		    WorksheetUtil.deleteWorksheetWSDATA(problemId, new String[] {propName}, username);
            }
            else
            {
                WorksheetUtil.setWorksheetWSDATAProperty(problemId, propName, valSet, username);
            }
		}
    }
	
	public static ResponseDTO<PBNotes> searchPBNote(String category, String activityName, String sir, String username) throws Exception
    {
	    // Check users access to SIR's Org
	    getSecurityIncident(sir, null, username);
		return PlaybookSearchAPI.searchPBNote(category, activityName, sir, username);
    }
	
	/**
	 * Add a new artifact name (key) - value pair to an incident represented by SIR.
     
	 * @param pbArtifactMap : A {@link Map} representing an artifact information. Here's an example:
	 * <pre><code> [
           "sir"="SIR-000000001",
           "name"="Test Name",
           "value"="Test Value",
           "description"="Test Desc."
           ]
        </code></pre>
        In this case, this Map has 4 different key-value pairs which represents an artifact:
        1. sir : representing a unique SIR id which belongs to an incident.
        2. name : String representing key of an artifact.
        3. value : String representing value of an artifact.
        3. description : String representing a description.
	 * @param append : This params is no longer used and will be ignored.
	 * @param username : String representing username of a user who is invoking this API.
	 * @return
	 *     Newly created PBArtifact object in json format.
	 * @throws Exception
	 *     <pre>An Exception will be thrown under following conditions:
	 *     1. If an incident is not found.
	 *     2. If worksheet id is not found.
	 *     3. During append operation, if name is not found.
	 *     </pre>
	 */
	public static PBArtifacts saveArtifact(Map<String, Object> pbArtifactMap, boolean append, String username) throws Exception
	{
	    String sir = pbArtifactMap.get("sir").toString();
	    
		ResolveSecurityIncidentVO incidentVo = getSecurityIncident(null, sir, username);
		
		if (incidentVo == null)
		{
			throw new Exception("Could not find incident for SIR: " + sir);
		}

        if ("Closed".equalsIgnoreCase(incidentVo.getStatus())) {
    		throw new Exception(ERROR_CLOSED_INCIDENT);
        }
	    
        String msgObj = String.format("artifact named %s to SIR %s", 
				  					  pbArtifactMap.get(Constants.PB_ARTIFACT_NAME).toString(), sir);
        
        v2LicenseViolationsAndEnvCheck(msgObj);
        
		// Check if the name has a json string and treate it properly
        pbArtifactMap.put(Constants.PB_ARTIFACT_NAME, pbArtifactMap.get(Constants.PB_ARTIFACT_NAME).toString());
        
		// Check if the value has a json string and treate it properly
		Object obj = pbArtifactMap.get(Constants.PB_ARTIFACT_VALUE);
        String artifactString = pbArtifactMap.get(Constants.PB_ARTIFACT_VALUE).toString();
        if (obj instanceof Map)
        {
        	artifactString = new ObjectMapper().writeValueAsString(pbArtifactMap.get(Constants.PB_ARTIFACT_VALUE));
        }
        pbArtifactMap.put(Constants.PB_ARTIFACT_VALUE, artifactString);
        
        // Check if the desctiption has a json string and treate it properly
        obj = pbArtifactMap.get(Constants.PB_DESCRIPTION);
        artifactString = pbArtifactMap.get(Constants.PB_DESCRIPTION).toString();
        if (obj instanceof Map)
        {
        	artifactString = new ObjectMapper().writeValueAsString(pbArtifactMap.get(Constants.PB_DESCRIPTION));
        }
        pbArtifactMap.put(Constants.PB_DESCRIPTION, artifactString);
        
		String auditMessage = null;
		
		String worksheetId = incidentVo.getProblemId();
		if (StringUtils.isBlank(worksheetId))
		{
			throw new SearchException("WorksheetId could not be found for sir: " + sir);
		}
        String name = pbArtifactMap.get("name") == null ? "" : pbArtifactMap.get("name").toString().trim();
        String type = pbArtifactMap.get("type") == null ? "" : pbArtifactMap.get("type").toString().trim();
        String value = pbArtifactMap.get("value") == null ? "" : pbArtifactMap.get("value").toString().trim();
        value = value.length() > 255 ? value.substring(0, 255) : value;
        String description = pbArtifactMap.get("description") == null ? "" : pbArtifactMap.get("description").toString().trim();
        description = description.length() > 4000 ? description.substring(0, 4000) : description;
        
        if (StringUtils.isBlank(name) || StringUtils.isBlank(type) || StringUtils.isBlank(value))
        {
            throw new Exception("Artifact Type or Key or Value cannot be blank.");
        }
        
        PBArtifacts artifact = null;
        ResponseDTO<PBArtifacts> result = PlaybookSearchAPI.searchPBArtifact(sir, name, value, username);
        if (result != null && result.getRecords() != null && result.getRecords().size() > 0)
        {
            artifact = result.getRecords().get(0);
            artifact.setSysUpdatedBy(username);
            Date now = new Date();
            artifact.setSysUpdatedOn(now.getTime());
            artifact.setSysUpdatedDt(now);
        }
        else
        {
            artifact = new PBArtifacts(Guid.getGuid(true), "", username);
            artifact.setSir(sir);
            artifact.setWorksheetId(worksheetId);
            artifact.setIncidentId(incidentVo.getSys_id());
        }
        
        artifact.setName(name);
        artifact.setValue(value);
		artifact.setType(type);
        artifact.setDescription(description);
        artifact.setModified(true);

        // Set source / source value only if it's not null and not blank. SIRINC-18
        artifact.setSource("");
        artifact.setSourceValue("");
        
        Object object = pbArtifactMap.get("source");
        if (object != null && StringUtils.isNotBlank(object.toString()))
        {
            artifact.setSource(object.toString().trim());
        }
        
        object = pbArtifactMap.get("sourceValue");
        if (object != null && StringUtils.isNotBlank(object.toString()))
        {
            String sourceValue = object.toString().trim();
            artifact.setSourceValue(sourceValue.length() > 255 ? sourceValue.substring(0, 255) : sourceValue);
        }
        
        /*
         * If both, source and sourceValue, are blank, sourceAndValue will be "NULL".
         * If only sourceValue is blank, sourceAndValue will be "<SOURCE>NULL".
         * If both, source and sourceValue, are non blank, sourceAndValue will be "<SOURCE><SOURCEVALUE>".
         */
        String sourceAndValue = (StringUtils.isNotBlank(artifact.getSource())? artifact.getSource() + " " : "None") + 
                        (StringUtils.isBlank(artifact.getSource()) ? "" : (StringUtils.isNotBlank(artifact.getSourceValue())? artifact.getSourceValue().replace("." ,  " ") : "None"));
        artifact.setSourceAndValue(sourceAndValue);
        
        PlaybookIndexAPI.indexPBArtifacts(artifact, username);
        
        saveArtifactToWSDATA(worksheetId, name, value);
        
        touchSIR(username, incidentVo.getSys_id());
        
        auditMessage = String.format("User '%s' added a new artifact. Name = '%s' and Value = '%s'", username, name, value);
        artifact.setAuditMessage(auditMessage);
		 
		return artifact;
	}
	
	public static PBArtifacts indexArtifact(Map<String, String> artifactMap, String workshetId, boolean append, String username) throws Exception
	{
		PBArtifacts artifact = null;
		
		String sir = artifactMap.get("sir");
		String name = artifactMap.get("name");
		String value = artifactMap.get("value");
		String description = artifactMap.get("description");
		
		artifact = new PBArtifacts(Guid.getGuid(true), "", username);
        artifact.setName(name);
        artifact.setSir(sir);
        artifact.setWorksheetId(workshetId);
        artifact.setDescription(description);
		
		if (artifact != null)
        {
            if (append)
            {
                String newValue = artifact.getValue() + "," + value;
                artifact.setValue(newValue);
            }
            else
            {
                artifact.setValue(value);
            }
            artifact.setDescription(description);
            
            PlaybookIndexAPI.indexPBArtifacts(artifact, username);
        }
		
		artifact.setAuditMessage(String.format("User '%s' added a new artifact with Name: '%s' and Value: '%s'", username, name, value));
		return artifact;
	}
	
	public static void deleteArtifacts(List<Map<String, String>> artifactList, String incidentId, String username) throws Exception
	{
		ResolveSecurityIncidentVO incident = getSecurityIncident(incidentId, null, username);
		List<String> ids = new ArrayList<String>();
		if (incident != null)
		{
	        if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ARTIFACT_DELETE, null))
	        {
	            throw new Exception(String.format("User %s does not have access to delete artifact(s) for SIR %s.", username, incident.getSir()));
	        }
	        
	        if ("Closed".equalsIgnoreCase(incident.getStatus())) {
	    		throw new Exception(ERROR_CLOSED_INCIDENT);
	        }

	        checkUsersSIROrgAccess(incident, username);
	        
	        String msgObj = String.format("[%s] artifacts from SIR %s", 
  				  						  StringUtils.collectionToString(artifactList
  				  								  						 .parallelStream()
  				  								  						 .map(m -> m.get("name"))
  				  								  						 .collect(Collectors.toList()), ", "),
  				  						  incident.getSir());
	        
	        v2LicenseViolationsAndEnvCheck(msgObj);
	        
			String problemId = incident.getProblemId();
			
		    if (artifactList != null)
		    {
		        for (Map<String, String> artifact : artifactList)
		        {
		            ids.add(artifact.get("id"));
		            deleteArtifactFromWSDATA(problemId, artifact.get("name"), artifact.get("value"));
		        }
		    }
		}
		
		if (ids.size()>0)
		{
			PlaybookIndexAPI.deleteArtifacts(ids, username);
			ArchiveSIRData.deleteData(ids, "artifact");
		}
	}
	
	public static ResponseDTO<ResolveSecurityIncidentVO> getIncidentsFromArtifact(QueryDTO queryDTO, String username) throws Exception
	{
		List<ResolveSecurityIncidentVO> incidents = new ArrayList<ResolveSecurityIncidentVO>();
		
		ResponseDTO<PBArtifacts> responseDTO = PlaybookSearchAPI.getIncidentsFromArtifact(queryDTO, username);
		
		ResponseDTO<ResolveSecurityIncidentVO> incidentResponseVO = new ResponseDTO<ResolveSecurityIncidentVO>();
		Set<String> ids = new HashSet<String>();
		
		if (responseDTO.getRecords().size() > 0)
		{
		    for (PBArtifacts artifact : responseDTO.getRecords())
            {
		        ids.add(artifact.getSir());
            } 
		    
			for (String sir : ids)
			{
			    ResolveSecurityIncidentVO incident = null;
			    
			    try
			    {
			        incident = getSecurityIncident(null, sir, username);
			        
    				if (incident != null && VO.STRING_DEFAULT.equalsIgnoreCase(incident.getMasterSir()))
    				{
    					incidents.add(incident);
    				}
			    }
			    catch (Exception e)
			    {
			        Log.log.debug("SIR " + sir + " omitted from list of SIRs for artifact due to " + e.getLocalizedMessage());
			    }
			}
		}
		
		incidentResponseVO.setRecords(incidents);
		incidentResponseVO.setTotal(ids.size());
		incidentResponseVO.setSuccess(true);
		
		return incidentResponseVO;
	}
	
	public static ResponseDTO<PBArtifacts> searchPBArtifacts(QueryDTO queryDTO, String username) throws SearchException
    {
		if (queryDTO != null && queryDTO.getFilters().size() == 0)
		{
			// return an empty array if there's no filter set on the QueryDTO.
			ResponseDTO<PBArtifacts> result = new ResponseDTO<PBArtifacts>();
			List<PBArtifacts> list = new ArrayList<PBArtifacts>();
			result.setRecords(list);
			return result;
		}
		
		return PlaybookSearchAPI.searchPBArtifacts(queryDTO, username);
    }
	
	@SuppressWarnings("unchecked")
    public static List<Map<String, Object>> searchPBArtifactsBySir(String sir, String username, int maxLimit) throws Exception
    {
	    ResponseDTO<PBArtifacts> response = PlaybookSearchAPI.searchPBArtifactsBySir(sir, username, maxLimit);
	    List<PBArtifacts> list = response.getRecords();
	    Map<String, Object> artifactMap = new HashMap<String, Object>();
	    List<Map<String, Object>> artifactMapList = new ArrayList<Map<String, Object>>();
	    
	    for (PBArtifacts artifact : list)
	    {
	        ResponseDTO<PBArtifacts> result = null;
	        QueryDTO queryDTO = new QueryDTO();
	        
	        try
	        {
	            queryDTO.addFilterItem(new QueryFilter("auto ", artifact.getName(), "name", QueryFilter.EQUALS, true));
	            queryDTO.addFilterItem(new QueryFilter("auto ", artifact.getValue(), "value", QueryFilter.EQUALS, true));
	            queryDTO.addFilterItem(new QueryFilter("auto ", sir, "sir", QueryFilter.NOT_EQUALS, true));
	            
	            result = PlaybookSearchAPI.searchPBArtifacts(queryDTO, username);
	            List<PBArtifacts> artifactList = result.getRecords();
	            List<String> sirList = new ArrayList<String>();
	            if (artifactList != null && artifactList.size() > 0)
	            {
	                for (PBArtifacts artifactInfo : artifactList)
	                {
	                    sirList.add(artifactInfo.getSir());
	                }
	            }
	            artifactMap = new ObjectMapper().convertValue(artifact, Map.class);
	            artifactMap.put("sirList", sirList);
	            artifactMapList.add(artifactMap); 
	        }
	        catch (Exception e)
	        {
	            Log.log.error(e.getMessage(), e);
	            throw new SearchException(e.getMessage(), e);
	        }
	    }
	    
		return artifactMapList;
		
//		String worksheetId = getSecurityIncident(null, sir, username).getProblemId();
//		if (StringUtils.isBlank(worksheetId))
//		{
//			throw new SearchException("WorksheetId could not be found for user: " + username);
//		}
//		
//		Object obj = WorksheetSearchAPI.getWorksheetData(worksheetId, sir, username);
//		
//		if (obj != null)
//		{
//			try
//			{
//				artifacts = new ObjectMapper().readValue((String)obj, HashMap.class);
//			}
//			catch (IOException e)
//			{
//				Log.log.error("Could not deserialize value of incidentId: " + sir, e);
//			}
//		}
    }
	
	/**
	 * Fetch all the artifacts logged against an incident.
	 * 
	 * @param sir : String representing sir id of an incident.
	 * @param username : String representing a username of a user who is invoking this API.
	 * @return
	 *     Returns {@link List} of {@link PBArtifacts} object. Here's an example:
	 *     <pre>
	 *     <code>
	 *     [PBArtifacts[
         "sysId"=null,
         "sysOrg"=null,
         "sysUpdatedBy"="admin",
         "sysUpdatedOn"=1492855910817,
         "sysUpdatedDt"=1492855910817,
         "sysCreatedBy"="admin",
         "sysCreatedOn"=1492855910817,
         "sysCreatedDt"=1492855910817,
         "worksheetId"="82d67f04586141a3945c49683742a8d9",
         "incidentId"="8a9482f05b937a3d015b94b96cfd0005",
         "name"="Name 2",
         "value"="Value 2",
         "description"="Test Desc. 2",
         "sir"="SIR-000000007",
         "id"=null
      ],
      PBArtifacts[
         "sysId"=null,
         "sysOrg"=null,
         "sysUpdatedBy"="admin",
         "sysUpdatedOn"=1492855892325,
         "sysUpdatedDt"=1492855892325,
         "sysCreatedBy"="admin",
         "sysCreatedOn"=1492855892325,
         "sysCreatedDt"=1492855892325,
         "worksheetId"="82d67f04586141a3945c49683742a8d9",
         "incidentId"="8a9482f05b937a3d015b94b96cfd0005",
         "name"="Name 1",
         "value"="Value 1",
         "description"="Test Desc. 1",
         "sir"="SIR-000000007",
         "id"=null
       ]]
       </code>
       </pre>
	 * @throws Exception
	 */
//	public static List<PBArtifacts> getArtifactList(String sir, String username) throws Exception
//	{
//		List<PBArtifacts> list = new ArrayList<PBArtifacts>();
//		
//		Map<String, Map<String, String>> artifactMap = searchPBArtifactsBySir(sir, username);
//		if (artifactMap != null && artifactMap.size() > 0)
//		{
//			Set<String> keys = artifactMap.keySet();
//			for (String key : keys)
//			{
//				Map<String, String> valueMap = artifactMap.get(key);
//				valueMap.put("name", key);
//				
//				String valueMapJson = new ObjectMapper().writeValueAsString(valueMap);
//				
//				PBArtifacts artifact = new ObjectMapper().readValue(valueMapJson, PBArtifacts.class);
//				list.add(artifact);
//			}
//		}
//		
//		return list;
//	}
	
	public static ResponseDTO<PBArtifacts> searchPBArtifact(String sir, String name, String username) throws Exception
    {
		ResponseDTO<PBArtifacts> result = null;
		
		// Check users access to SIR's Org
		getSecurityIncident(sir, name, username);
		
		result = PlaybookSearchAPI.searchPBArtifact(sir, name, username);
		
//		if (artifactsMap != null && artifactsMap.size() > 0)
//		{
//			valueMap = artifactsMap.get(name);
//			if (valueMap != null)
//			{
//				valueMap.put("name", name);
//				String valueMapJson = new ObjectMapper().writeValueAsString(valueMap);
//				
//				result = new ObjectMapper().readValue(valueMapJson, PBArtifacts.class);
//			}
//		}
		
		return result;
    }
	
	public static void indexAttachment(PBAttachments  attachment, String username) throws Exception
	{
	    attachment.setModified(true);
		PlaybookIndexAPI.indexPBAttachment(attachment, username);
	}
	
	public static ResponseDTO<PBAttachments> searchPBAttachments(QueryDTO queryDTO, String username) throws SearchException
    {
		if (queryDTO != null && queryDTO.getFilters().size() == 0)
		{
			// return an empty array if there's no filter set on the QueryDTO.
			ResponseDTO<PBAttachments> result = new ResponseDTO<PBAttachments>();
			List<PBAttachments> list = new ArrayList<PBAttachments>();
			result.setRecords(list);
			return result;
		}
		
		return PlaybookSearchAPI.searchPBAttachments(queryDTO, username);
    }
	
	/**
	 * Fetch all attachments associated with an incident.
	 * 
	 * @param incident : String representing an incident id of an incident whoes attachments are needed to be fetched.
	 * @param username : String representing username of a user who is invoking this API.
	 * @return 
	 *     {@link ResponseDTO} with records field holding an array of {@link PBAttachments} object.
	 * @throws SearchException
	 * @throws LimitExceededException 
	 */
	public static ResponseDTO<PBAttachments> searchPBAttachmentsByIncident(String incident, String username, Integer maxLimit) throws SearchException, LimitExceededException
    {
		return PlaybookSearchAPI.searchPBAttachmentsByIncident(incident, username, maxLimit);
    }
	
	public static ResponseDTO<PBAttachments> searchPBAttachmentsByName(String sir, String name, String username) throws Exception
    {
	    // Check users access to SIR's Org
	    getSecurityIncident(sir, name, username);
	    
		return PlaybookSearchAPI.searchPBAttachmentsByName(sir, name, username);
    }
	
	public static void indexAuditLog(PBAuditLog auditLog, String username) throws Exception
	{
		if (auditLog != null)
		{
			auditLog.setSysId(Guid.getGuid(true));
			auditLog.setSysCreatedBy(username);
			auditLog = new PBAuditLog(auditLog);
			auditLog.setModified(true);
			
			PlaybookIndexAPI.indexPBAuditLog(auditLog, username);
		}
	}
	
	public static ResponseDTO<PBAuditLog> searchPBAuditLogs(QueryDTO queryDTO, String username) throws SearchException
    {
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ACTIVITY_TIMELINE_VIEW, null))
        {
            throw new SearchException(String.format("User %s does not have access to view timeline.", username));
        }
	    
		if (queryDTO != null && queryDTO.getFilters().size() == 0)
		{
			// return an empty array if there's no filter set on the QueryDTO.
			ResponseDTO<PBAuditLog> result = new ResponseDTO<PBAuditLog>();
			List<PBAuditLog> list = new ArrayList<PBAuditLog>();
			result.setRecords(list);
			return result;
		}
		
		return PlaybookSearchAPI.searchPBAuditLogs(queryDTO, username);
    }
	
	public static ResponseDTO<PBAuditLog> searchPBAuditLogsBySIR(String sir, String username, int maxLimit) throws SearchException, LimitExceededException
    {
		return PlaybookSearchAPI.searchPBAuditLogsBySIR(sir, username, maxLimit);
    }
	
	public static boolean hasMapCaseAttributesFilter(QueryDTO query) {
		return query.getFilters().stream().anyMatch(queryFilter -> "map(caseAttributes)".equals(queryFilter.getField()));
	}
	
	@SuppressWarnings("unchecked")
    public static List<ResolveSecurityIncidentVO> listSecurityIncidents(QueryDTO query, String username) throws Exception
	{
		List<ResolveSecurityIncidentVO> result = new ArrayList<ResolveSecurityIncidentVO>();
        int limit = query.getLimit();
        int offset = query.getStart();
        
        /*
         * If filters contains filter with "Activity." prefix
         * for field then set model to SIRActivityRuntimeData 
         * once and re-write field name without "Activity." prefix.           
         */
         
        if (query.getFilters() != null && !query.getFilters().isEmpty())
        {
            for (QueryFilter qryFltr : query.getFilters())
            {
                if (qryFltr.getField().startsWith(ACTIVITY_STATUS_FIELD_PREFIX))
                {
                    if (StringUtils.isBlank(query.getModelName()))
                    {
                        query.setModelName(SIR_ACTIVITY_RUNTIMEDATA_MODEL_NAME);
                        query.setHqlDistinct(true);
                        query.setPrefixTableAlias(SIR_ACTIVITY_RUNTIMEDATA_MODEL_HQL_QUERY_ALIAS);
                        query.setSelectColumns(SIR_ACTIVITY_RUNTIMEDATA_MODEL_REFERENCED_RSI_PROPERTY_NAME + 
                                               PERIOD + Constants.HTTP_REQUEST_sys_id);
                    }
                    
                    qryFltr.setField(StringUtils.substringAfter(qryFltr.getField(), ACTIVITY_STATUS_FIELD_PREFIX));
                }
            }
        }
         
        if (StringUtils.isBlank(query.getModelName()))
        {
            query.setModelName(SIR_MODEL_NAME);
        }
        else if (query.getModelName().equals(SIR_ACTIVITY_RUNTIMEDATA_MODEL_NAME))
        {
        	// transform ResolveSecurityIncident.masterSir filter to SIRActivityRuntimeData.referencedRSI.masterSir
        	
        	for (QueryFilter qryFltr : query.getFilters())
            {
        		if (qryFltr.getField().equals(ResolveSecurityIncident.MASTER_SIR_FIELD_LABLE))
        		{
        			qryFltr.setField(SIR_ACTIVITY_RUNTIMEDATA_MODEL_REFERENCED_RSI_PROPERTY_NAME + PERIOD + 
        							 qryFltr.getField());
        			break;
        		}
            }
        }
        
        String sortPropertyPrefix = SIR_ACTIVITY_RUNTIMEDATA_MODEL_NAME.equals(query.getModelName()) ?
                                    SIR_ACTIVITY_RUNTIMEDATA_MODEL_REFERENCED_RSI_PROPERTY_NAME + PERIOD : "";
        
        List<QuerySort> originalSortList = new ArrayList<QuerySort>();
        
        if (StringUtils.isBlank(query.getSort()))
        {
            query.setSort("[{\"property\":\"" + sortPropertyPrefix + "sysUpdatedOn\",\"direction\":\"DESC\"}]");
            originalSortList.add(new QuerySort("sysUpdatedOn", SortOrder.DESC));
            
            if (SIR_ACTIVITY_RUNTIMEDATA_MODEL_NAME.equals(query.getModelName()))
            {
                query.setSelectColumns(query.getSelectColumns() + COMMA + 
                                       sortPropertyPrefix + BASE_MODEL_SYS_UPDATED_ON_PROPERTY_NAME);
            }
        }
        else if (StringUtils.isNotBlank(query.getSort()) && (StringUtils.isNotBlank(sortPropertyPrefix)))
        {
            // Sort properties should include the "referencedRSI" prefix if not present if already specified
            
            StringBuilder sb = new StringBuilder();
            
            for (QuerySort qrySort : query.getSortItems())
            {
                if (!qrySort.getProperty().startsWith(sortPropertyPrefix))
                {
                    originalSortList.add(new QuerySort(qrySort.getProperty(), qrySort.getDirection()));
                    qrySort.setProperty(sortPropertyPrefix + qrySort.getProperty());
                    sb.append(COMMA).append(qrySort.getProperty());
                }
            }
            
            if (sb.length() > 0)
            {
                query.setSelectColumns(query.getSelectColumns() + sb.toString());
            }
        }
        
        List<? extends Object> data = null;
        
        if (query.getModelName().equals(SIR_ACTIVITY_RUNTIMEDATA_MODEL_NAME))
        {
            List<Object[]> results = (List<Object[]>) GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            
            if (results != null && results.size() > 0)
            {
                List<String> rsiIds = results.stream().map(objArray -> (String)objArray[0]).collect(Collectors.toList());
                
                String rsiIdsCs = StringUtils.listToString(rsiIds, COMMA);
                
                QueryDTO rsiQueryDTO = new QueryDTO();
                
                rsiQueryDTO.setModelName(SIR_MODEL_NAME);
                rsiQueryDTO.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, rsiIdsCs, 
                                          Constants.HTTP_REQUEST_sys_id, QueryFilter.EQUALS));
                rsiQueryDTO.setSortItems(originalSortList);
                
                data = GeneralHibernateUtil.executeHQLSelectModel(rsiQueryDTO, -1, -1, true);                
            }
        }
        else
        {
        	// If filters contains map(caseAttributes) then set distinct
        	
        	boolean hasMapCaseAttributesFilter = hasMapCaseAttributesFilter(query);
        	
        	if (hasMapCaseAttributesFilter) {
        		query.setHqlDistinctMap(true);
        	}
        			
            data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            
            // Reset for get total count query which uses same filters
            
            if (hasMapCaseAttributesFilter) {
            	query.setHqlDistinctMap(false);
            }
        }
        
        if (data != null && data.size() > 0)
        {
            Set<String> orgsSysIdsUserHasAccessTo = null;
            UsersVO userVO = UserUtils.getUser(username);
            
            if (userVO != null && userVO.getUserOrgs() != null && !userVO.getUserOrgs().isEmpty())
            {
                Set<OrgsVO> orgsUserHasAccessTo = new HashSet<OrgsVO>();
                
                for (OrgsVO orgsVO : userVO.getUserOrgs())
                {
                    orgsUserHasAccessTo.addAll(UserUtils.getAllOrgsInHierarchy(orgsVO.getSys_id(), orgsVO.getUName()));
                }
                
                if (!orgsUserHasAccessTo.isEmpty())
                {
                    orgsSysIdsUserHasAccessTo = new HashSet<String>(orgsUserHasAccessTo.size());
                    
                    for (OrgsVO orgsVO : orgsUserHasAccessTo)
                    {
                        orgsSysIdsUserHasAccessTo.add(orgsVO.getSys_id());
                    }
                }
            }
            
            boolean hasNoOrgAccess = UserUtils.isNoOrgAccessible(username);
            
        	for (Object obj : data)
        	{
        		ResolveSecurityIncident resolveSecurityIncident = (ResolveSecurityIncident) obj;
        		ResolveSecurityIncidentVO incidentVO = resolveSecurityIncident.doGetVO();
        		
        		if (incidentVO != null)
        		{
        		    boolean add = false;
        		    
        		    if (StringUtils.isNotBlank(incidentVO.getSysOrg()))
        		    {
        		        add = orgsSysIdsUserHasAccessTo.contains(incidentVO.getSysOrg());
        		    }
        		    else
        		    {
        		        add = hasNoOrgAccess;
        		    }
        		    
        		    if (add && !result.stream().anyMatch(t -> t.getSys_id().equals(incidentVO.getSys_id())))
        		    {
    		    		result.add(incidentVO);
        		    }
        		}
        	}
        }
        
        return result;
	}
	
	private static void updatePlaybookSIRRefCounts(String oldPlaybook, String newPlaybook, Integer oldPlaybookVersion, String username)
	{
	    if (StringUtils.isBlank(oldPlaybook) && oldPlaybookVersion == null)
	    {
	        if (StringUtils.isNotBlank(newPlaybook))
	        {
	            WikiDocument templateDocModel = WikiUtils.getWikiDocumentModel(null, newPlaybook, username, false);
	            WikiDocumentVO templateDoc = templateDocModel.doGetVO();
	            HibernateUtil.getCurrentSession().evict(templateDocModel);
	            
	            if (templateDoc != null)
	            {
	                templateDoc.setUSIRRefCount(templateDoc.getUSIRRefCount().longValue() + 1l);
	                
	                try
	                {
                   HibernateProxy.setCurrentUser(username);
	                    HibernateProxy.execute(() -> {
	                    
		                    WikiDocument templateWikiDoc = new WikiDocument();
		                    templateWikiDoc.applyVOToModel(templateDoc);
		                    HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(templateWikiDoc);
		                    	                    
		                    // Update corresponding WikiArchive record
	                        
	                        WikiArchive tmpWikiArchiveModel = WikiArchiveUtil.getSpecificWikiRevisionModelFor(templateWikiDoc.getSys_id(), templateWikiDoc.getUFullname(), templateWikiDoc.getUVersion(), "admin", "CONTENT");
	                        
	                        if (tmpWikiArchiveModel != null)
	                        {
	                            WikiArchiveVO tmpWikiArchiveVO = tmpWikiArchiveModel.doGetVO();
	                            
	                            HibernateUtil.getCurrentSession().evict(tmpWikiArchiveModel);
	                            
	                            tmpWikiArchiveVO.setUSIRRefCount(Long.valueOf(tmpWikiArchiveVO.getUSIRRefCount().longValue() + 1l));
	                            WikiArchive templateWikiArchive = new WikiArchive();
	                            templateWikiArchive.applyVOToModel(tmpWikiArchiveVO);
	                            HibernateUtil.getDAOFactory().getWikiArchiveDAO().update(templateWikiArchive);
	                        }
                        
	                    });
	                }
	                catch(Throwable e)
	                {
	                    Log.log.warn("Failed to add 1 to SIR reference count of the playbook template " + 
	                                 (templateDoc != null ? templateDoc.getUFullname() : "null") + 
	                                 ". " + e.getMessage(), e);
                                        HibernateUtil.rethrowNestedTransaction(e);

	                }
	            }
	        }
	    }
	    else
	    {
            // Get latest version of oldPlaybook
            
            List<Object> objs = WikiUtils.getWikiContentList(oldPlaybook, null);
            Integer latestWikiDocVer = null;
            Object oldObj = null;
            Object newObj = null;
            
            if (objs != null && objs.size() == 1)
            {
                Object[] objArray = (Object[]) objs.get(0);
                
                String oldObjSysId = (String) objArray[0];
                latestWikiDocVer = (Integer) objArray[2];
                
                if (latestWikiDocVer.equals(oldPlaybookVersion))
                {
                    WikiDocument oldTemplateDocModel = WikiUtils.getWikiDocumentModel(null, oldPlaybook, "system", false);
                    oldObj = oldTemplateDocModel.doGetVO();
                    HibernateUtil.getCurrentSession().evict(oldTemplateDocModel);
                }
                else
                {
                    // Get specific version of oldPlaybook (from wiki archive)
                    
                    WikiArchive wikiArchive = null;
                    
                    try
                    {
                        wikiArchive = WikiArchiveUtil.getSpecificWikiRevisionModelFor(oldObjSysId, oldPlaybook, oldPlaybookVersion, "admin", "CONTENT");
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                    
                    if (wikiArchive != null)
                    {
                        oldObj = wikiArchive.doGetVO();
                        HibernateUtil.getCurrentSession().evict(wikiArchive);
                    }
                    else
                    {
                        Log.log.warn("Failed to get latest version of old playbook template " + oldPlaybook); 
                    }	                     
                }
            }
            else
            {
                Log.log.warn("Failed to get latest version of old playbook template " + oldPlaybook);
            }
            
            if (StringUtils.isNotBlank(newPlaybook))
            {
                
                WikiDocument newTemplateDocModel = null;
                                
                if (!newPlaybook.equals(oldPlaybook))
                {
                    newTemplateDocModel = WikiUtils.getWikiDocumentModel(null, newPlaybook, "system", false);
                    newObj = newTemplateDocModel.doGetVO();
                }
                else
                {
                    if (latestWikiDocVer != null && 
                        (latestWikiDocVer.equals(oldPlaybookVersion) || latestWikiDocVer.intValue() < oldPlaybookVersion.intValue()))
                    {
                        oldObj = null;
                        newObj = null;
                    }
                    else
                    {
                        newTemplateDocModel = WikiUtils.getWikiDocumentModel(null, newPlaybook, "system", false);
                        newObj = newTemplateDocModel.doGetVO();
                    }
                }
                
                if (newObj != null && newTemplateDocModel != null)
                {
                    HibernateUtil.getCurrentSession().evict(newTemplateDocModel);
                }
            }
            
            try
            {
                if (oldObj != null || newObj != null)
                {
                	Object oldObjFinal = oldObj;
                    Object newObjFinal = newObj;
                  HibernateProxy.setCurrentUser(username);
                    HibernateProxy.execute(() -> {
                    
	                    if (oldObjFinal != null)
	                    {
	                        if ( oldObjFinal instanceof WikiDocumentVO )
	                        {
	                            if (((WikiDocumentVO)oldObjFinal).getUSIRRefCount().longValue() >= 1l)
	                            {
	                                ((WikiDocumentVO)oldObjFinal).setUSIRRefCount(Long.valueOf(((WikiDocumentVO)oldObjFinal).getUSIRRefCount().longValue() - 1l));
	                                WikiDocument templateWikiDoc = new WikiDocument();
	                                templateWikiDoc.applyVOToModel((WikiDocumentVO) oldObjFinal);
	                                HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(templateWikiDoc);
	                                
	                                // Update corresponding WikiArchive record
	                                
	                                WikiArchive tmpWikiArchiveModel = WikiArchiveUtil.getSpecificWikiRevisionModelFor(((WikiDocumentVO)oldObjFinal).getSys_id(), ((WikiDocumentVO)oldObjFinal).getUFullname(), ((WikiDocumentVO)oldObjFinal).getUVersion(), "admin", "CONTENT");
	                                
	                                if (tmpWikiArchiveModel != null)
	                                {
	                                    WikiArchiveVO tmpWikiArchiveVO = tmpWikiArchiveModel.doGetVO();
	                                    
	                                    HibernateUtil.getCurrentSession().evict(tmpWikiArchiveModel);
	                                    
	                                    if (tmpWikiArchiveVO.getUSIRRefCount().longValue() >= 1l)
	                                    {
	                                        tmpWikiArchiveVO.setUSIRRefCount(Long.valueOf(tmpWikiArchiveVO.getUSIRRefCount().longValue() - 1l));
	                                        WikiArchive templateWikiArchive = new WikiArchive();
	                                        templateWikiArchive.applyVOToModel(tmpWikiArchiveVO);
	                                        HibernateUtil.getDAOFactory().getWikiArchiveDAO().update(templateWikiArchive);
	                                    }
	                                }
	                            }
	                        }
	                        else if ( oldObjFinal instanceof WikiArchiveVO)
	                        {
	                            WikiArchiveVO templateWikiArchiveVO = (WikiArchiveVO)oldObjFinal;
	                            
	                            if (templateWikiArchiveVO.getUSIRRefCount().longValue() >= 1l)
	                            {
	                                templateWikiArchiveVO.setUSIRRefCount(Long.valueOf(templateWikiArchiveVO.getUSIRRefCount().longValue() - 1l));
	                                WikiArchive templateWikiArchive = new WikiArchive();
	                                templateWikiArchive.applyVOToModel(templateWikiArchiveVO);
	                                HibernateUtil.getDAOFactory().getWikiArchiveDAO().update(templateWikiArchive);
	                            }
	                        }
	                    }
	                    
	                    if (newObjFinal != null)
	                    {
	                        WikiDocumentVO templateDoc = (WikiDocumentVO)newObjFinal;
	                        
	                        templateDoc.setUSIRRefCount(templateDoc.getUSIRRefCount().longValue() + 1l);
	                        
	                        WikiDocument templateWikiDoc = new WikiDocument();
	                        templateWikiDoc.applyVOToModel(templateDoc);
	                        HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(templateWikiDoc);
	                                                
	                        // Update corresponding WikiArchive record
	                        
	                        WikiArchive tmpWikiArchiveModel = WikiArchiveUtil.getSpecificWikiRevisionModelFor(templateWikiDoc.getSys_id(), templateWikiDoc.getUFullname(), templateWikiDoc.getUVersion(), "admin", "CONTENT");
	                        
	                        if (tmpWikiArchiveModel != null)
	                        {
	                            WikiArchiveVO tmpWikiArchiveVO = tmpWikiArchiveModel.doGetVO();
	                            
	                            HibernateUtil.getCurrentSession().evict(tmpWikiArchiveModel);
	                            
	                            tmpWikiArchiveVO.setUSIRRefCount(Long.valueOf(tmpWikiArchiveVO.getUSIRRefCount().longValue() + 1l));
	                            WikiArchive templateWikiArchive = new WikiArchive();
	                            templateWikiArchive.applyVOToModel(tmpWikiArchiveVO);
	                            HibernateUtil.getDAOFactory().getWikiArchiveDAO().update(templateWikiArchive);
	                        }    	                                
	                    }
                    
                    });
                }
            }
            catch(Throwable e)
            {
                Log.log.warn("Failed to update SIR reference count of the old and new playbook templates " + 
                             oldPlaybook + "(" + oldPlaybookVersion.intValue() + "), " + newPlaybook +
                             ". " + e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
               
            }
	    }
	}
	
	/**
	 * API to Save/Update resolve security incident
	 * 
	 * Here's a sample input JSON string representing ResolveSecurityIncident:
	 * <pre><code>
	 * {
	   "title": "Phishing Investigation", // Title of an incident.
	   "investigationType": "Phishing",   // phishing, malware, etc...
	   "playbook": "Phishing.Template",   // playbook associated with this incident.
	   "severity": "Critical",            // Severity of this incident. Critical, High, Medium, Low etc...
	   "owner": "admin",                  // User who is an owner of this incident.
	   "sourceSystem": "Resolve",         // Source system from where the incident generated (Resolve or Splunk or Arcsight, etc...)
	   "externalReferenceId": "",         // If the sourceSystem is not Resolve, this field indicates the assigned ID from the external system.
	   "status": "Open",                  // Status of the incident in Resolve. (Open, In Progress, Closed, etc...)
	   "problemId": ""                    // problem id (worksheet id) of an assigned worksheet.
	 * }
	 * </code></pre>
	 * 
	 * If the sourceSystem is Resolve, externalReferenceId is ignored.
	 * If problemId field is empty, new worksheet will be created. Not otherwise.
	 * During save operation, incident will be searched for sourceSystem, externalReferenceId and problemId. If an incident is found, it will be updated. New incident will be created otherwise.
	 * If user assigned as an owner does not belong to the security group(s) specified in the system property, app.security.groups, he will not be assigned as an owner.
	 * 
	 * @param model : {@link ResolveSecurityIncident}
	 * @param username : Username of a person who is invoking this API.
	 * @return {@link ResolveSecurityIncidentVO}
	 * @throws Exception
	 */
	public static ResolveSecurityIncidentVO saveSecurityIncident(ResolveSecurityIncident model, String username) throws Exception
	{
		ResolveSecurityIncident incident = null;
		String auditMessage = "";
		String description = null;

		if (StringUtils.isNotBlank(model.getSysOrg()))
        {
            if (!UserUtils.isOrgAccessible(model.getSysOrg(), username, false, false))
            {
                throw new Exception(String.format(USER_DOES_NOT_BELONG_TO_SIRS_ORG_ERR_MSG, username));
            }
        }
        else
        {
            if (!UserUtils.isNoOrgAccessible(username))
            {
                throw new Exception(String.format(USER_DOES_NOT_BELONG_TO_NO_OR_NONE_ORG_ERR_MSG, username));
            }
        }
		
		if (StringUtils.isBlank(username))
		{
		    username = "admin";
			//throw new Exception("Username cannot be blank.");
		}
		
		String msgObj = String.format(SIR_TITLED, model.getTitle());
		
		v2LicenseViolationsAndEnvCheck(msgObj);
		
		if (model != null)
		{
			String validation = validate(model, username);
			if (StringUtils.isBlank(validation))
			{
				String sourceSystem = StringUtils.isBlank(model.getSourceSystem()) || model.getSourceSystem().equals("UNDEFINED")? "" : model.getSourceSystem();
				String externalRefId = StringUtils.isBlank(model.getExternalReferenceId()) || model.getExternalReferenceId().equals("UNDEFINED") ? "" : model.getExternalReferenceId();
				if ((StringUtils.isNotBlank(sourceSystem)  && !sourceSystem.equalsIgnoreCase("resolve")) && (StringUtils.isBlank(externalRefId)))
				{
					Log.log.error(BLANK_EXT_REF_ID_ERR_MSG);
					throw new Exception(BLANK_EXT_REF_ID_ERR_MSG);
				}
				
				ResolveSecurityIncidentVO localVo = null;
				if (StringUtils.isBlank(sourceSystem) || !sourceSystem.equalsIgnoreCase("resolve"))
				{
					localVo = findResolveSecurityIncident(sourceSystem, externalRefId, model.getProblemId(), username);
				}
				else
				{
					if (StringUtils.isNotBlank(model.getSys_id()) && !model.getSys_id().equals("UNDEFINED"))
					{
						localVo = getSecurityIncident(model.getSys_id(), null, username);
					}
				}
	
				try
				{
					incident = new ResolveSecurityIncident();
					
					if (localVo == null) // NEW SIR - CREATE A NEW ONE
					{
					    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_CASECREATION_CREATE, null))
					    {
					        throw new Exception(String.format("User %s is denied access to create a new investigation.", username));
					    }
						incident = model;
						incident.setSir(getPaddedSequence(HibernateUtil.getNextSequence(WorksheetUtils.SIRID, 1).toString()));
						
						// Playbook can be null for SIR created from RR
						
						if (StringUtils.isNotBlank(model.getPlaybook()))
						{
						    incident.setPlaybookVersion(getTemplateVersion(model.getPlaybook(), username));
						}
						
						long slaSeconds = 0;
						Object slaObject = getTemplateDescAndSla(incident.getPlaybook(), incident.getPlaybookVersion(), true).get("SLA");
						if (slaObject != null)
						{
						    slaSeconds = (Long)slaObject;
						}
						incident.setSla(slaSeconds);
						
						if (slaSeconds > 0)
						{
							long currMilliSec = new Date().getTime();
							
							if (slaSeconds < (Long.MAX_VALUE / 1000))
							{
								if ((slaSeconds * 1000) < (Long.MAX_VALUE - currMilliSec))
								{
									incident.setDueBy(new Date(currMilliSec + (slaSeconds * 1000)));
								}
								else
								{
									incident.setDueBy(new Date(Long.MAX_VALUE));
								}
							}
							else
							{
								incident.setDueBy(new Date(Long.MAX_VALUE));
							}
						}
						
						// RBA-14408: Design Playbook Object Model
						// set explicitly the status to "Open" only if it's not provided in the model
						if (model.getStatus() == null) {
							incident.setStatus("Open");
						}
						
						if(!model.getOwner().isEmpty()) {
						    incident.setAssignmentDate(new Date());
						}
						
						// check for problem ID being passed to use it (UI RR use case)
						if (model.getProblemId() != null) {
							Worksheet enforcedWorksheet = WorksheetUtil.findWorksheetById(model.getProblemId(), username);
							if (enforcedWorksheet == null) {
								Log.log.error(String.format("Failed to save security incident: Cannot find the passed Worksheet with ID '%s'.", model.getProblemId()));
								return null;
							}
							enforcedWorksheet.setSirId(incident.getSir());
							enforcedWorksheet.setSummary("Security Investigation Response");
							WorksheetUtil.updateWorksheet(enforcedWorksheet);
							incident.setProblemId(enforcedWorksheet.getId());
						} else {
							Map<String, String> params = new HashMap<String, String>();
							params.put(WorksheetUtils.SIRID, incident.getSir());
							params.put(WorksheetUtils.WORKSHEET_SUMMARY, "Security Investigation Response");
							
							if (StringUtils.isNotBlank(incident.getSysOrg()))
							{
							    params.put(Constants.EXECUTE_ORG_ID, incident.getSysOrg());
							}
							
							Worksheet newlyCreatedWorksheet = null;
							
							try
							{
							    newlyCreatedWorksheet = WorksheetUtil.createNewWorksheet(params, username);
							}
							catch (Throwable t)
							{
							    throw new Exception(t.getMessage());
							}
							
    						incident.setProblemId(newlyCreatedWorksheet.getSysId());
						}
						HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().persist(incident);
						
						if (StringUtils.isNotBlank(model.getPlaybook()))
						{
							String usernameFinal = username;
        HibernateProxy.setCurrentUser(username);
							HibernateProxy.execute(() -> {
								updatePlaybookSIRRefCounts(null, model.getPlaybook(), null, usernameFinal);
							});
						    
						}
					}
					else // EXISTING SIR - UPDATE THE EXISTING ONE
					{
                        if (localVo.getStatus().equalsIgnoreCase(model.getStatus()) && "Closed".equalsIgnoreCase(localVo.getStatus())) {
                    		throw new Exception(ERROR_CLOSED_INCIDENT);
                        }
						
						if (StringUtils.isNotBlank(model.getExternalStatus()))
					    {
					        if (StringUtils.isBlank(localVo.getExternalStatus()))
					        {
					            auditMessage += String.format("Incident '%s' received external status of '%s' ", localVo.getSir(), model.getExternalStatus());
					        }
					        else
					        {
					            auditMessage += String.format("Incident '%s' externalStatus changed from '%s' to '%s' ", localVo.getSir(), localVo.getExternalStatus(), model.getExternalStatus());
					        }
					        localVo.setExternalStatus(model.getExternalStatus());
					    }
					    
					    if (StringUtils.isNotBlank(model.getSeverity()) &&
					    	(StringUtils.isBlank(localVo.getSeverity()) ||
					    	 !localVo.getSeverity().equals(model.getSeverity())))
					    {
					        if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_SEVERITY_CHANGE, null))
					        {
    					        if (StringUtils.isBlank(localVo.getSeverity()))
                                {
                                    auditMessage += String.format("Incident '%s' received Severity of '%s' ", localVo.getSir(), model.getSeverity());
                                }
                                else
                                {
                                    if (!localVo.getSeverity().equals(model.getSeverity()))
                                        auditMessage += String.format("Incident '%s' Severity changed from '%s' to '%s' ", localVo.getSir(), localVo.getSeverity(), model.getSeverity());
                                }
    					        localVo.setSeverity(model.getSeverity());
					        }
					        else
					        {
					        	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify incident severity.", username);
					            throw new Exception(warnMsg);
					        }
					    }
					    
					    if (StringUtils.isNotBlank(model.getTitle()) &&
					    	(StringUtils.isBlank(localVo.getTitle()) ||
					    	 !localVo.getTitle().equals(model.getTitle())))
					    {
					    	if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_TITLE_CHANGE, null))
					        {
						        if (StringUtils.isBlank(localVo.getTitle()))
	                            {
	                                auditMessage += String.format("Incident '%s' received Title of '%s' ", localVo.getSir(), model.getTitle());
	                            }
	                            else
	                            {
	                                if (!localVo.getTitle().equals(model.getTitle()))
	                                    auditMessage += String.format("Incident '%s' Title changed from '%s' to '%s' ", localVo.getSir(), localVo.getTitle(), model.getTitle());
	                            }
						        localVo.setTitle(model.getTitle());
					        }
					    	else
					    	{
					    		String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify incident title.", username);
					    		throw new Exception(warnMsg);
					    	}
					    }
					    
					    if (StringUtils.isNotBlank(model.getOwner()) &&
					    	(StringUtils.isBlank(localVo.getOwner()) ||
					    	 !localVo.getOwner().equals(model.getOwner())))
					    {
					        if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_OWNER_CHANGE, null))
					        {
    					        if (StringUtils.isBlank(localVo.getOwner()))
                                {
    					            localVo.setAssignmentDate(new Date());
                                    auditMessage += String.format("Incident '%s' received new Owner '%s' ", localVo.getSir(), model.getOwner());
                                }
                                else
                                {
                                    if (!localVo.getOwner().equals(model.getOwner())) 
                                    {
                                        localVo.setAssignmentDate(new Date());
                                        auditMessage += String.format("Incident '%s' Owner changed from '%s' to '%s' ", localVo.getSir(), localVo.getOwner(), model.getOwner());
                                    }
                                }
    					        
    					        localVo.setOwner(model.getOwner());
					        }
					        else
					        {
					        	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify incident owner.", username);
					        	throw new Exception(warnMsg);
					        }
					    }
					    
				    	Set<String> modelMembers = StringUtils.stringToSet(model.getMembers(), ",");
				    	Set<String> dbMembers = StringUtils.stringToSet(localVo.getMembers(), ",");
				    	
					    if ((modelMembers.size() != dbMembers.size()) || !modelMembers.containsAll(dbMembers))
					    {
					    	if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_TEAM_CHANGE, null))
						    {						    
						    	if (StringUtils.isBlank(localVo.getMembers()))
						    	{
						    		auditMessage += String.format("Incident '%s' Following members are added: '%s' ", localVo.getSir(), model.getMembers());
						    	}
						    	else
						    	{
						    		Set<String> newMembers = new HashSet<String>(modelMembers);
						    		newMembers.addAll(StringUtils.stringToList(localVo.getMembers(), ","));
						    		
						    		String removedMembers = "";
						    		for (String existingMember : dbMembers)
						    		{
						    			if (!modelMembers.contains(existingMember))
						    			{
						    				removedMembers += existingMember + ", ";
						    				newMembers.remove(existingMember);
						    			}
						    			else
						    			{
						    				newMembers.remove(existingMember);
						    			}
						    		}
						    		
						    		if (StringUtils.isNotBlank(removedMembers))
						    		{
						    			removedMembers = removedMembers.substring(0, removedMembers.length()-2);
						    		}
						    		
						    		if (StringUtils.isNotBlank(removedMembers) && newMembers.size() > 0)
						    		{
						    			String addedMembers = StringUtils.substringBetween(newMembers.toString(), "[", "]");
						    			auditMessage += String.format("Incident '%s' These members are added: '%s' and these are removed: %s", localVo.getSir(), addedMembers, removedMembers);
						    		}
						    		else
						    		{
						    			if (StringUtils.isNotBlank(removedMembers))
						    			{
						    				auditMessage += String.format("Incident '%s' These members are removed: '%s'", localVo.getSir(), removedMembers);
						    			}
						    			else if (newMembers.size() > 0)
						    			{
						    				String addedMembers = StringUtils.substringBetween(newMembers.toString(), "[", "]");
						    				auditMessage += String.format("Incident '%s' These members are added: '%s'", localVo.getSir(), addedMembers);
						    			}
						    		}
						    	}
						    }
					    	else
					    	{
					    		String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " change team members.", username);
					        	throw new Exception(warnMsg);
					    	}
					    	
					    	localVo.setMembers(model.getMembers());
					    }

						if (StringUtils.isNotBlank(model.getInvestigationType()) &&
							(StringUtils.isBlank(localVo.getInvestigationType()) ||
							 !localVo.getInvestigationType().equals(model.getInvestigationType())))
						{
						    if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_TYPE_CHANGE, null))
						    {
    						    if (StringUtils.isBlank(localVo.getInvestigationType()))
                                {
                                    auditMessage += String.format("Incident '%s' received new Investigation type of '%s' ", localVo.getSir(), model.getInvestigationType());
                                }
                                else
                                {
                                    if (!localVo.getInvestigationType().equals(model.getInvestigationType()))
                                        auditMessage += String.format("Incident '%s' Investigation type changed from '%s' to '%s' ", localVo.getSir(), localVo.getInvestigationType(), model.getInvestigationType());
                                }
    						    localVo.setInvestigationType(model.getInvestigationType());
						    }
						    else
						    {
						    	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify investigation type.", username);
					        	throw new Exception(warnMsg);
						    }
						}
						
						if (StringUtils.isNotBlank(model.getDataCompromised()) &&
							(StringUtils.isBlank(localVo.getDataCompromised()) ||
							 !localVo.getDataCompromised().equals(model.getDataCompromised())))
						{
						    if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_DATACOMPROMISED_CHANGE, null))
						    {
    						    if (StringUtils.isBlank(localVo.getDataCompromised()))
                                {
                                    auditMessage += String.format("Incident '%s' received new data compromised indicator of '%s' ", localVo.getSir(), model.getDataCompromised());
                                }
                                else
                                {
                                    if (!localVo.getDataCompromised().equals(model.getDataCompromised()))
                                        auditMessage += String.format("Incident '%s' data compromised indicator changed from '%s' to '%s' ", localVo.getSir(), localVo.getDataCompromised(), model.getDataCompromised());
                                }
    						    localVo.setDataCompromised(model.getDataCompromised());
						    }
						    else
						    {
						    	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify data compromised.", username);
					        	throw new Exception(warnMsg);
						    }
						}
						
						if (model.getDueBy() != null &&
							(localVo.getDueBy() == null || localVo.getDueBy().getTime() != model.getDueBy().getTime()))
                        {
						    if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_DUEDATE_CHANGE, null))
						    {
                                if (localVo.getDueBy() == null)
                                {
                                    String dueDate = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(model.getDueBy());
                                    auditMessage += String.format("Incident '%s' received new Due Date of '%s' ", localVo.getSir(), dueDate);
                                }
                                else
                                {
                                    if (localVo.getDueBy().getTime() != model.getDueBy().getTime())
                                    {
                                        String dueDate1 = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(localVo.getDueBy());
                                        String dueDate2 = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(model.getDueBy());
                                        auditMessage += String.format("Incident '%s' Due Date changed from '%s' to '%s' ", localVo.getSir(), dueDate1, dueDate2);
                                    }
                                }
                                localVo.setDueBy(model.getDueBy());
						    }
						    else
						    {
						    	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify investigation duedate.", username);
					        	throw new Exception(warnMsg);
						    }
                        }
						
						localVo.setSysModCount(Integer.valueOf(localVo.getSysModCount().intValue() + 1));
						
						/*
						 *  Update associated playbook template and/or version as long as 
						 *  status of associated SIR in DB or model is Open.
						 */
						
						boolean updatePlaybookSIRRefCounts = false;
						String oldPlaybook = null;
						Integer oldPlaybookVersion = null;
						
						if ((localVo.getStatus() != null && localVo.getStatus().equals("Open")) ||
							(model.getStatus() != null && localVo.getStatus().equals("Open")))
	                    {
						    boolean updatedPlaybook = false;
						    oldPlaybook = localVo.getPlaybook();
						    
						    if (!model.getPlaybook().equals(oldPlaybook))
						    {
							    if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_PLAYBOOK_CHANGE, null))
							    {
    						        localVo.setPlaybook(model.getPlaybook());
    						        updatedPlaybook = true;
							    }
							    else
							    {
							    	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify investigation playbook.", username);
						        	throw new Exception(warnMsg);
							    }
						    }
						    
						    oldPlaybookVersion = localVo.getPlaybookVersion();
						    boolean updatedPlaybookVersion = false;
						    
						    if (updatedPlaybook || 
						        (oldPlaybookVersion != null && StringUtils.isNotBlank(model.getPlaybook()) && 
						         !oldPlaybookVersion.equals(getTemplateVersion(model.getPlaybook(), username))))
						    {
						    	if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_PLAYBOOK_CHANGE, null))
							    {
							        localVo.setPlaybookVersion(getTemplateVersion(model.getPlaybook(), username));
							        updatedPlaybookVersion = true;
							    }
						    	else
						    	{
						    		String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify investigation playbook version.", username);
						        	throw new Exception(warnMsg);
						    	}
						    }
						    
						    // Increment SIRRefcount of template wikidoc
						    // only if SIR has no custom SIR specific activit(y/ies).
						    
						    if ((updatedPlaybook || updatedPlaybookVersion) && 
						        !PlaybookActivityUtils.hasSIRActivities(localVo.getSys_id()))
						    {
						        updatePlaybookSIRRefCounts = true;
						    }
	                    }
						
						if (StringUtils.isNotBlank(model.getStatus()) &&
							(StringUtils.isBlank(localVo.getStatus()) ||
							 !localVo.getStatus().equals(model.getStatus())))
	                    {
						    if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_STATUS_CHANGE, null))
						    {
    	                        if (model.getStatus().equals("Closed"))
    	                        {
    	                        	checkAllRequiredActivitiesCompleted(model.getSys_id(), model.getSir());
    	                        	
    	                        	localVo.setSirStarted(false);
    	                            localVo.setClosedBy(username);
    	                            localVo.setClosedOn(new Date());
    	                            auditMessage += String.format("Incident '%s' status changed to Closed ", localVo.getSir());
    	                        }
    	                        else
    	                        {
      	                            if (!localVo.getStatus().equals(model.getStatus()))
    	                                auditMessage += String.format("Incident status '%s' changed to '%s' ", localVo.getSir(), model.getStatus());
    	                            localVo.setClosedBy(null);
    	                            localVo.setClosedOn(null);
    	                        }
    	                        
    	                        /*
    	                         *  When playbook goes into "In Progress" state, increment the 
    	                         *  refcount of wiki's referenced by activities.
    	                         *  When playbook goes into "Closed" state, decrement the 
    	                         *  refcount of wiki's referenced by activities.
    	                         */
    	                        
    	                        if (model.getStatus().equals("In Progress") && !localVo.getStatus().equals("In Progress"))
    	                        {
    	                        	localVo.setSirStarted(true);
    	                            PlaybookActivityUtils.addPlaybookActivitySIRRefCounts(localVo.getSys_id(), 
    	                            													  localVo.getPlaybook(), 
    	                            													  localVo.getPlaybookVersion(), 
    	                            													  Integer.parseInt("+1"),
    	                            													  localVo.getSysOrg(),
    	                            													  username);
    	                        }
    	                        else if (model.getStatus().equals("Closed") && !localVo.getStatus().equals("Closed"))
    	                        {
    	                            PlaybookActivityUtils.addPlaybookActivitySIRRefCounts(localVo.getSys_id(), 
    	                            													  localVo.getPlaybook(), 
    	                            													  localVo.getPlaybookVersion(), 
    	                            													  Integer.parseInt("-1"),
    	                            													  localVo.getSysOrg(),
    	                            													  username);
    	                        }
    	                        
    	                        if (StringUtils.isNotBlank(model.getStatus()))
    	                        {
    	                            localVo.setStatus(model.getStatus());
    	                        }
						    }
						    else
						    {
						    	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify investigation status.", username);
					        	throw new Exception(warnMsg);
						    }
	                    }
						
						// RBA-14934 : Save All Data from a Gateway to an Investigation Record in SIR case
						if (StringUtils.isNotBlank(model.getRequestData()))
						{
						    localVo.setRequestData(model.getRequestData());
						}
						
						// Identify risk score update if any
						
						if (localVo.getRiskScore().compareTo(model.getRiskScore()) != 0)
						{
						    if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_RISKSCORE_CHANGE, ""))
						    {
                                auditMessage += String.format(
                                                    "User '%s' changed incident '%s' risk score from '%s' to '%s'.",
                                                    username,
                                                    localVo.getSir(), 
                                                    (localVo.getRiskScore() != 
                                                     ResolveSecurityIncident.RISK_SCORE_NOT_SET ?
                                                     localVo.getRiskScore().toString() : 
                                                     ResolveSecurityIncident.RISK_SCORE_NOT_SET_TEXT), 
                                                    (model.getRiskScore() != 
                                                     ResolveSecurityIncident.RISK_SCORE_NOT_SET ?
                                                     model.getRiskScore().toString() : 
                                                     ResolveSecurityIncident.RISK_SCORE_NOT_SET_TEXT));                                
                                localVo.setRiskScore(model.getRiskScore());
						    }
						    else
                            {
						    	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify investigation risk score.", username);
					        	throw new Exception(warnMsg);
                            }
						}
						
						if (StringUtils.isNotBlank(model.getClassification()) &&
							(StringUtils.isBlank(localVo.getClassification()) ||
							 !model.getClassification().equalsIgnoreCase(localVo.getClassification())))
                        {
                            if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_CLASSIFICATION_CHANGE, ""))
                            {
                                if (StringUtils.isBlank(localVo.getClassification()))
                                {
                                    auditMessage += String.format("Incident '%s' received new classification of '%s' ", localVo.getSir(), model.getClassification());
                                }
                                else
                                {
                                    if (!model.getClassification().equalsIgnoreCase(localVo.getClassification()))
                                    {
                                        auditMessage += String.format("Incident '%s' classification updated from %s to '%s' ", localVo.getSir(), localVo.getClassification(), model.getClassification());
                                    }
                                }
                                
                                localVo.setClassification(model.getClassification());
                            }
                            else
                            {
                            	String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify investigation classification.", username);
					        	throw new Exception(warnMsg);
                            }
                        }
						
						if (StringUtils.isNotBlank(model.getPriority()) &&
                            (StringUtils.isBlank(localVo.getPriority()) ||
                             !localVo.getPriority().equals(model.getPriority())))
                        {
                            if (UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_PRIORITY_CHANGE, null))
                            {
                                if (StringUtils.isBlank(localVo.getPriority()))
                                {
                                    auditMessage += String.format("Incident '%s' received Priority of '%s' ", localVo.getSir(), model.getPriority());
                                }
                                else
                                {
                                    if (!localVo.getPriority().equals(model.getPriority()))
                                        auditMessage += String.format("Incident '%s' Priority changed from '%s' to '%s' ", localVo.getSir(), localVo.getPriority(), model.getPriority());
                                }
                                localVo.setPriority(model.getPriority());
                            }
                            else
                            {
                                String warnMsg = String.format("User %s " + RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER + " modify incident priority.", username);
                                throw new Exception(warnMsg);
                            }
                        }
						
						description = localVo.getDescription();

						incident.applyVOToModel(localVo);
						
						if (StringUtils.isNotBlank(localVo.getMasterSysId()) && !VO.STRING_DEFAULT.equals(localVo.getMasterSysId()))
						{
							ResolveSecurityIncident master = new ResolveSecurityIncident();
							master.setSys_id(localVo.getMasterSysId());
							master.setSir(localVo.getMasterSir());
							master.addDuplicate(incident);
						}
						
						incident.setSysCreatedBy(localVo.getSysCreatedBy());
						incident.setSysCreatedOn(GMTDate.getDate(localVo.getSysCreatedOn()));
						
						ResolveSecurityIncident incidentFinal = incident; 
       HibernateProxy.setCurrentUser(username);
						incident = (ResolveSecurityIncident) HibernateProxy.execute(() ->
							HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().update(incidentFinal));

					}
					
					/*
			         * Create (Phases/Activities) / Update (Activities Only) RuntimeData (for Resolve 6.2 i.e. SIR 2.0 and above only)
			         */
			        
			        PhaseUtils.createSIRPhasesRuntimeData(incident, username);
			        ActivityUtils.upsertSIRActivitiesRuntimeData(incident, username);
				}
				catch(Exception e)
				{
					if (e.getLocalizedMessage().contains(RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER) ||
						e.getLocalizedMessage().contains(ERROR_CLOSED_INCIDENT))
					{
						Log.log.warn("Failed to save security incident: " + e.getLocalizedMessage());
					}
					else
					{
						Log.log.error("Failed to save security incident: " + e.getMessage(), e);
					}
					
		            throw e;
				}
			}
			else
			{
				Log.log.error("Following fields are needed to create an SIR: " + validation);
				throw new Exception("Following fields are needed to create an SIR: " + validation);
			}
		}
		
		ResolveSecurityIncidentVO incidntVO = incident != null ? incident.doGetVO() : null;
		if (incidntVO != null)
		{
		    if (StringUtils.isNotBlank(auditMessage))
		    {
		        auditMessage = String.format("User '%s', %s", username, auditMessage);
		        incidntVO.setAuditMessage(auditMessage);
		    }
		    
		    incidntVO.setDescription(description);
		}
		
		return incidntVO;
	}
	
	private static String validate(ResolveSecurityIncident model, String username) throws Exception
	{
		String result = null;

		// Set owner to default owner if the system property app.security.defaultowner is set
		if (StringUtils.isBlank(model.getOwner()) && 
		    StringUtils.isNotBlank(PropertiesUtil.getPropertyString(Constants.SECURITY_DEFAULT_OWNER)))
		{
		    model.setOwner(PropertiesUtil.getPropertyString(Constants.SECURITY_DEFAULT_OWNER));
		}
		
		if (StringUtils.isNotBlank(model.getOwner()))
		{
			if (validateMembers(null, model.getOwner(), username).size() != 1)
			{
				model.setOwner(null);
				Log.log.warn("Failed to validate specified owner " + model.getOwner() + ", resetting owner!!!");
			}
		}
		
		if (StringUtils.isNotBlank(model.getMembers()))
		{
			List<String> memberList = Arrays.asList(model.getMembers().split(","));
			List<String> validatedUsers = validateMembers(memberList, null, username);
			model.setMembers(StringUtils.substringBetween(validatedUsers.toString(), "[", "]").trim());
		}
		else
		{
			// if members is blank, set it to a blank (empty) string
		    model.setMembers("");
		}
		
		/*
         * Add owner of an incident as a member.
         */
        if (StringUtils.isNotBlank(model.getOwner()))
        {
            if (StringUtils.isNotBlank(model.getMembers()) && !Pattern.compile("\\b" + model.getOwner() + "\\b").matcher(model.getMembers()).find())
            {
                model.setMembers(model.getOwner() + ", " + model.getMembers());
            }
            else if (StringUtils.isBlank(model.getMembers()))
            {
                model.setMembers(model.getOwner());
            }
        }
		
		if (!UserUtils.isAdminUser(username))
        {
    		if (StringUtils.isNotBlank(model.getSysOrg()) && !model.getSysOrg().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
    		{
    		    if (!UserUtils.isOrgAccessible(model.getSysOrg(), username, false, false))
    		    {
    		        throw new Exception("User " + username + " does not have access to Org specified in SIR.");
    		    }
    		}
    		else
    		{
    		    if (!UserUtils.isNoOrgAccessible(username))
                {
                    throw new Exception("User " + username + " does not have permission to specified No Org 'None'.");
                }
    		}
        }
		
		return result;
	}
	
	private static List<String> validateMembers(List<String> members, String member, String username) throws Exception
	{
		List<String> validatedMembers = new ArrayList<String>();
		if ((members != null && members.size() == 0) && StringUtils.isBlank(member))
		{
			return validatedMembers;
		}
		
		String groupNames = PropertiesUtil.getPropertyString(Constants.SECURITY_GROUPS);
		{
			if (StringUtils.isBlank(groupNames))
			throw new Exception("No security group defined. Cannot validate member(s).");
		}
		
		List<Map<String, String>> userMapLIst = UserUtils.getGroupUserNames(groupNames, username);
		List<String> userList = new ArrayList<String>();
		
		for (Map<String, String> userMap : userMapLIst)
		{
			if( !userList.contains(userMap.get("userId").trim()))
			{
				userList.add(userMap.get("userId")); 
			}
		}
	
		if (members != null && members.size() > 0)
		{
			for (String mem : members)
			{
				if (userList.contains(mem.trim()))
				{
					validatedMembers.add(mem.trim());
				}
				else
				{
					Log.log.error("User '" + mem + "' does not belong to a security group.");
				}
			}
		}
		else
		{
			if (userList.contains(member))
			{
				validatedMembers.add(member);
			}
			else
			{
				Log.log.error("User '" + member + "' does not belong to a security group.");
			}
		}
		
		return validatedMembers;
	}
	
	public static String getPaddedSequence(String toPad)
	{
		int width = 9;
		String prefix = PropertiesUtil.getPropertyString(Constants.SECURITY_SIR_PREFIX);
		if (StringUtils.isBlank(prefix))
		{
		    prefix = "SIR-";
		}
		else
		{
		    prefix += "-";
		}
		return prefix + new String(new char[width - toPad.length()]).replace('\0', '0') + toPad;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ResolveSecurityIncidentVO findResolveSecurityIncident(String sourceSystem, String externalRefId, String problemId, String username) throws Exception
	{
		ResolveSecurityIncidentVO vo = null;
		List<? extends Object> data = null;
		
		if (StringUtils.isBlank(externalRefId) && (StringUtils.isBlank(sourceSystem) ) && StringUtils.isBlank(problemId))
		{
			return vo;
		}
		
		try
		{
    HibernateProxy.setCurrentUser(username);
			data = (List<? extends Object>) HibernateProxy.execute(() -> {
				String hql = "from ResolveSecurityIncident where ";
				
				if (StringUtils.isBlank(externalRefId))
				{
				    hql += "(externalReferenceId is null OR externalReferenceId = '') AND ";
				}
				else
				{
				    hql += "externalReferenceId = :externalRefId AND ";
				}
				
				if (StringUtils.isBlank(sourceSystem))
		        {
		            hql += "(sourceSystem is null OR sourceSystem = '') AND ";
		        }
		        else
		        {
		            hql += "sourceSystem = :sourceSystem AND ";
		        }
				
				if (StringUtils.isBlank(problemId))
		        {
		            hql += "(problemId is null OR problemId = '') AND ";
		        }
		        else
		        {
		            hql += "problemId = :problemId AND ";
		        }
				
				hql = hql.substring(0, hql.length() - 4);
		
		    
	    		Query hibernateQuery = HibernateUtil.createQuery(hql);
	    		
	    		if (StringUtils.isNotBlank(externalRefId))
	    		{
	    		    hibernateQuery.setParameter("externalRefId", externalRefId);
	    		}
	    		if (StringUtils.isNotBlank(sourceSystem))
	            {
	                hibernateQuery.setParameter("sourceSystem", sourceSystem);
	            }
	    		if (StringUtils.isNotBlank(problemId))
	            {
	                hibernateQuery.setParameter("problemId", problemId);
	            }
	    		
	    		return hibernateQuery.list();
    		
			});
		}
		catch(Throwable t)
		{
		    Log.log.error("Could not get security incident", t );
          HibernateUtil.rethrowNestedTransaction(t);
		}
		
		if (data != null && data.size() > 0)
		{
			if (data.size() == 1)
			{
				ResolveSecurityIncident incident = (ResolveSecurityIncident)data.get(0);
				vo = incident.doGetVO();
			}
			else
			{
				throw new Exception("Two incidents with same source :" + sourceSystem + " and externalRefId: " + externalRefId);
			}
		}
		
		return vo;
	}
	
	public static ResolveSecurityIncidentVO getSecurityIncident(String incidentId, String sir, String username) throws Exception
	{
		/*
		 * User's view access to incident i.e whether user has access to all
		 * SIRs or is restricted to SIRs owned by or is member of team, 
		 * should be based on legacy sirInvestigationListViewMode "all" permission 
		 * or new sir.dashboard.incidentList.viewAll permission.
		 * 
		 * Check users Org and RBAC permissions after getting the incident.
		 */
//	    if (!UserUtils.getUserfunctionPermission(username, Constants.INVESTIGATION_VIEW, Constants.VIEW_ACCESS))
//	    {
//	    	throw new Exception(String.format(DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username));
//	    }
	    
		ResolveSecurityIncidentVO securityIncidentVO = null;
		if (StringUtils.isNotBlank(incidentId))
		{
			securityIncidentVO = getSecurityIncidentById(incidentId, username);
		}
		else if (StringUtils.isNotBlank(sir))
		{
			securityIncidentVO = getResolveSecurityIncidentBySIR(sir, username);
			if (securityIncidentVO != null)
			{
				securityIncidentVO.setSir(sir);
			}
		}
				                
		if (securityIncidentVO != null)
		{
		    checkUsersSIROrgAccess(securityIncidentVO, username);
		    checkUsersViewAllOrOwnerTeamMemberAccess(securityIncidentVO, username);
		    
		    Map<String, Object> templateInfoMap = getTemplateDescAndSla(securityIncidentVO.getPlaybook(), securityIncidentVO.getPlaybookVersion(), false);
		    if (templateInfoMap.get("DESCRIPTION") != null)
		    {
		        securityIncidentVO.setDescription(templateInfoMap.get("DESCRIPTION").toString());
		    }
		}
		
		return securityIncidentVO;
	}

	/*
	 * Note: Following API returns SIR info for only those incident ids user has permissions for based on Org or RBAC.
	 */
public static List<ResolveSecurityIncidentVO> getSecurityIncidents(Collection<String> incidentIds, 
																	   String username, String orgId) throws Exception {
		return getSecurityIncidents("sys_id", incidentIds, username, orgId);
















































































	}

	public static ResolveSecurityIncidentVO getDetachedSecurityIncident(String incidentId, String username) throws Exception
    {
		/*
		 * User's view access to incident i.e whether user has access to all
		 * SIRs or is restricted to SIRs owned by or is member of team, 
		 * should be based on legacy sirInvestigationListViewMode "all" permission 
		 * or new sir.dashboard.incidentList.viewAll permission.
		 * 
		 * Check users Org and RBAC permissions after getting the incident.
		 */
//        if (!UserUtils.getUserfunctionPermission(username, Constants.INVESTIGATION_VIEW, Constants.VIEW_ACCESS))
//        {
//            throw new Exception(String.format("User %s does not have view permission on Security Incidents.", username));
//        }
        
        ResolveSecurityIncidentVO securityIncidentVO = null;
        if (StringUtils.isNotBlank(incidentId))
        {
            try
            {                    
              HibernateProxy.setCurrentUser(username);
            	securityIncidentVO = (ResolveSecurityIncidentVO) HibernateProxy.execute(() -> {
            		
            		ResolveSecurityIncidentVO resultVO = null;
                	if(StringUtils.isNotBlank(incidentId))
                    {
                        ResolveSecurityIncident securityIncident = HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(incidentId);
                        if (securityIncident != null)
                        {
                        	resultVO = securityIncident.doGetVO();
                            HibernateUtil.getCurrentSession().evict(securityIncident);
                        }
                    }
                	
                	return resultVO;
                });
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
                                
        if (securityIncidentVO != null)
        {
            checkUsersSIROrgAccess(securityIncidentVO, username);
            checkUsersViewAllOrOwnerTeamMemberAccess(securityIncidentVO, username);
            
            Map<String, Object> templateInfoMap = getTemplateDescAndSla(securityIncidentVO.getPlaybook(), securityIncidentVO.getPlaybookVersion(), false);
            if (templateInfoMap.get("DESCRIPTION") != null)
            {
                securityIncidentVO.setDescription(templateInfoMap.get("DESCRIPTION").toString());
            }
        }
        
        return securityIncidentVO;
    }
	
	private static void checkUsersViewAllOrOwnerTeamMemberAccess(ResolveSecurityIncidentVO securityIncidentVO, String username) throws Exception {
		
		if (securityIncidentVO != null) {
			if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL, "")) {
				Set<String> ownerAndTeamMembers = new HashSet<String>();
				
				ownerAndTeamMembers.add(securityIncidentVO.getOwner());
				
				if (StringUtils.isNotBlank(securityIncidentVO.getMembers())) {
					List<String> teamMembers = StringUtils.stringToList(securityIncidentVO.getMembers(), ",");
					ownerAndTeamMembers.addAll(teamMembers);
				}
				
				if (!ownerAndTeamMembers.contains(username)) {
					throw new Exception(String.format(DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username));
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static ResolveSecurityIncidentVO getResolveSecurityIncidentBySIR(String sir, String username) throws Exception
	{
		ResolveSecurityIncidentVO securityIncidentVO = null;
		
		/*
		 * User's view access to incident i.e whether user has access to all
		 * SIRs or is restricted to SIRs owned by or is member of team, 
		 * should be based on legacy sirInvestigationListViewMode "all" permission 
		 * or new sir.dashboard.incidentList.viewAll permission.
		 * 
		 * Check users Org and RBAC permissions after getting the incident.
		 */
//		if (!UserUtils.getUserfunctionPermission(username, Constants.INVESTIGATION_VIEW, Constants.VIEW_ACCESS))
//        {
//			throw new Exception(String.format(DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username));
//        }
		
		String hql = "from ResolveSecurityIncident where LOWER(sir) = :sir";
		List list = null;
		try
		{
    HibernateProxy.setCurrentUser(username);
			list = (List) HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(hql);
				query.setParameter("sir", sir.toLowerCase());
				
				return  query.list();
			});
			
		}
		catch(Exception e)
		{
			Log.log.error("Error in executing HQL: " + hql, e);
       HibernateUtil.rethrowNestedTransaction(e);
		}
		
		if (list != null && list.size() > 0)
		{
			ResolveSecurityIncident securityIncident = (ResolveSecurityIncident)list.get(0);
			securityIncidentVO = securityIncident.doGetVO();
			checkUsersSIROrgAccess(securityIncidentVO, username);
			checkUsersViewAllOrOwnerTeamMemberAccess(securityIncidentVO, username);
		}
		
		return securityIncidentVO;
	} 
	
	@SuppressWarnings("rawtypes")
    public static ResolveSecurityIncidentVO closeIncident(String incidentId, String sir, String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_STATUS_CHANGE, null))
        {
            throw new Exception(String.format("User %s is denied access to modify status for SIR %s.", username, incidentId));
        }
	    
	    ResolveSecurityIncidentVO incidentVo = getSecurityIncident(incidentId, null, username);
        
        if (incidentVo == null)
        {
            throw new Exception("Could not find incident for SIR with sys_id: " + incidentId);
        }
        
        checkUsersSIROrgAccess(incidentVo, username);
        
		Date utcNow = new Date(DateUtils.GetUTCDateLong());
		String hql = "update ResolveSecurityIncident set closedOn = :now, closedBy = :closedBy, status = :status ";
		
		if (StringUtils.isNotBlank(incidentId))
		{
			hql += "where sys_id = :sysId";
		}
		else if (StringUtils.isNotBlank(sir))
		{
			hql += "where LOWER(sir) = :sir";
		}
		else
		{
			throw new Exception ("Either incidentId or SIR is needed to close the incident.");
		}
		
		checkAllRequiredActivitiesCompleted(incidentId, incidentVo.getSir());
		
		try
		{
			String finalHql = hql;
    HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(finalHql);
				query.setParameter("now", utcNow);
				query.setParameter("closedBy", username);
				query.setParameter("status", "Closed");
				if (StringUtils.isNotBlank(incidentId))
				{
					query.setParameter("sysId", incidentId);
				}
				else if (StringUtils.isNotBlank(sir))
				{
					query.setParameter("sir", sir.toLowerCase());
				}
				
				query.executeUpdate();
			
			});
		}
		catch(Exception e)
		{
			Log.log.error("Error in executing HQL: " + hql, e);
       HibernateUtil.rethrowNestedTransaction(e);
		}
		
		return getSecurityIncident(incidentId, sir, username);
	}
	
//	public static ResolveSecurityIncidentVO setSITemplate(String sysId, String template, String username) throws Exception
//	{
//		ResolveSecurityIncidentVO securityIncident = getSecurityIncident(sysId, null, username);
//		
//		if (securityIncident != null)
//		{
//			securityIncident.setPlaybook(template);
//			
//			WikiDocumentVO docVO = WikiUtils.getWikiDoc(null, template, username);
//			if (docVO != null)
//			{
//				securityIncident.setPlaybookVersion(docVO.getUVersion());
//			}
//			else
//			{
//				throw new Exception("");
//			}
//			
//			saveSecurityIncident(securityIncident, username);
//		}
//		
//		return securityIncident;
//	}
	
	/**
	 * Upload an attachment to an incident.
	 
	 * @param request : 
	 * Here's a sample payload of an attachment as seen from chrome's debug window:
     * <pre><code>
     * ------WebKitFormBoundaryo5vAtrQBAuKwePwd
    Content-Disposition: form-data; name="name"
    
    o_1beaj9ss69i81k6j1mdg11en1onla.lic
    ------WebKitFormBoundaryo5vAtrQBAuKwePwd
    Content-Disposition: form-data; name="recordId"
    
    8a9482f05b937a3d015b94b96cfd0005
    ------WebKitFormBoundaryo5vAtrQBAuKwePwd
    Content-Disposition: form-data; name="docFullName"
    
    8a9482f05b937a3d015b94b96cfd0005
    ------WebKitFormBoundaryo5vAtrQBAuKwePwd
    Content-Disposition: form-data; name="file"; filename="license.lic"
    Content-Type: application/octet-stream
    
    
    ------WebKitFormBoundaryo5vAtrQBAuKwePwd--
    </code>
    </pre>
    Please note that docFullName, which contains the incident id, is used to identify an incident to which the attachment to be uploaded.
	 * @param username : String representing username of a user who is invoking this API.
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static PBAttachments uploadAttachment(HttpServletRequest request, String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_UPLOAD, null))
        {
            throw new Exception(String.format("User %s does not have access to upload an attachment of this incident.", username));
        }
	    
		// Check that we have a file upload request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart)
        {
            //if not an upload request, then return
            throw new Exception("Request object does not have any multipart content to upload.");
        }
        
        // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();
        
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        
        //create the attachment object for the document
        WikiAttachmentVO wikiAttachment =  new WikiAttachmentVO();
        wikiAttachment.setULocation("");
        wikiAttachment.setUType("DB");//DEFAULT
        wikiAttachment.setSysCreatedBy(username);
        
        // Parse the request
        List<FileItem> items = upload.parseRequest(request);
        String incidentId = null;
        Boolean isMalicious = null;
        String description = null;
        String overriddenFilename = null;
        String source = null;
        String sourceValue = null;
        for (FileItem item : items)
        {
        	if (item.isFormField())
            {
        		String name = item.getFieldName();
                String value = new String(item.get());
                
                /*
                 * Beacuse of some limitations, UI sends incident id in
                 * docFullName.
                 */
                if (name.equals("docFullName"))
                {
                	incidentId = value;
                }
                else if (name.equalsIgnoreCase("malicious"))
                {
                    isMalicious = Boolean.valueOf(value);
                }
                else if (name.equalsIgnoreCase("description"))
                {
                    description = value;
                } 
                else if (name.equalsIgnoreCase("overriddenFilename"))
                {
                    overriddenFilename = value;
                }
                else if (name.equalsIgnoreCase("source"))
                {
                    source = StringUtils.isBlank(value) ? "" : value;
                }
                else if (name.equalsIgnoreCase("sourceValue"))
                {
                    sourceValue = StringUtils.isBlank(value) ? "" : value.length() > 255 ? value.substring(0, 255) : value;
                }
            }
            else
            {
                WikiFileUpload.processUploadedFile(item, wikiAttachment, overriddenFilename);
            }
        }
        
        PBAttachments pbAttachment = null;
        if (StringUtils.isNotBlank(incidentId))
        {
    		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);

    		if (incidentVO != null && "Closed".equalsIgnoreCase(incidentVO.getStatus())) {
	    		throw new Exception(ERROR_CLOSED_INCIDENT);
	        }
        	
	        wikiAttachment = persistIncidentAttachment(wikiAttachment, incidentId, username);
	        
	        pbAttachment = populateAttachmentIndex(wikiAttachment, incidentId, username, isMalicious, description, source, sourceValue);
	        
	        indexAttachment(pbAttachment, username);
        }
        else
        {
        	throw new Exception("Incident Id is needed to upload an attachment.");
        }

        touchSIR(username, incidentId);

        return pbAttachment;
	}
	
	public static void downloadAttachment (HttpServletRequest request, HttpServletResponse response, String attachId, boolean malicious, String username) throws Exception
	{
	    if (malicious)
	    {
	        if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOADMALICIOUS, null))
	        {
	            throw new Exception(String.format("User %s does not have access to download malicious attachment of this incident.", username));
	        }
	    }
	    else if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOAD, null))
        {
            throw new Exception(String.format("User %s does not have access to downloan an attachment of this incident.", username));
        }
	    
        WikiAttachment attachment = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	attachment = (WikiAttachment) HibernateProxy.execute(() -> {
            
        		WikiAttachment attachmentResult = HibernateUtil.getDAOFactory().getWikiAttachmentDAO().findById(attachId);
	            WikiAttachmentUtil.populateAttachmentContent(attachmentResult);
	            
	            return attachmentResult;
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Could not download the attachment.", e);
            throw e;
        }
        
        if(attachment != null)
        {
            Log.log.info("User '" + username + "' downloaded attachment '" + attachment.getUFilename() + "'");
            Log.auth.info("User '" + username + "' downloaded attachment '" + attachment.getUFilename() + "'");
            
            response.setDateHeader("Last-Modified", attachment.getSysCreatedOn().getTime());
            
            ServletUtils.sendHeaders(attachment.getUFilename(), attachment.ugetWikiAttachmentContent().getUContent().length, 
                                     Arrays.copyOf(attachment.ugetWikiAttachmentContent().getUContent(), 
                                                   attachment.ugetWikiAttachmentContent().getUContent().length > 128 ? 
                                                   128 : attachment.ugetWikiAttachmentContent().getUContent().length), 
                                     request, response);
            response.getOutputStream().write(attachment.ugetWikiAttachmentContent().getUContent());
        }
    }
	
	public static void deleteAttachments(List<Map<String, String>> attachmentMapList, String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DELETE, null))
        {
            throw new Exception(String.format("User %s does not have access to delete an attachment.", username));
        }
	    
	    String msgObj = String.format("[%s] attachments from SIR", 
				  					  StringUtils.collectionToString(attachmentMapList
				  							  						 .parallelStream()
				  							  						 .map(m -> m.get("name"))
				  							  						 .collect(Collectors.toList())), ", ");
	    
	    v2LicenseViolationsAndEnvCheck(msgObj);
	    
		List<String> attachmentIdList = new ArrayList<String>();
		List<String> attachmentContentIds = new ArrayList<String>();
		
		for (Map<String, String> attachment : attachmentMapList)
		{
			attachmentIdList.add(attachment.get("id"));
		}
		
		if (attachmentIdList.size() > 0)
		{
			try
	        {
           HibernateProxy.setCurrentUser(username);
	            HibernateProxy.execute(() -> {
	            
		            for (String attachId : attachmentIdList)
	                {
	                    WikiAttachment model = HibernateUtil.getDAOFactory().getWikiAttachmentDAO().findById(attachId);
	                    
						if (model != null) {
							WikiAttachmentUtil.populateAttachmentContent(model);
							attachmentContentIds.add(model.ugetWikiAttachmentContent().getSys_id());
	
							HibernateUtil.delete("WikiAttachmentContent", getAttachmentContentIds(attachmentIdList, username));
			                HibernateUtil.delete("WikiAttachment", attachmentIdList);
						}
	                }
	            
	            });
	        }
			catch (Throwable e)
	        {
	            Log.log.error(e.getMessage(), e);
	            throw new Exception(e);
	        }

			PlaybookIndexAPI.deleteAttachments(attachmentIdList, username);
			ArchiveSIRData.deleteData(attachmentIdList, "attachment");
		}
	}
	
	private static List<String> getAttachmentContentIds(List<String> attachmentIds, String username) throws Exception
	{
        List<String> attachmentContentIds = new ArrayList<String>();
        
        try
        {
           
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		for (String attachId : attachmentIds)
                {
                    WikiAttachment model = HibernateUtil.getDAOFactory().getWikiAttachmentDAO().findById(attachId);
                    
                    if (model != null)
                    {
                        attachmentContentIds.add(model.ugetWikiAttachmentContent().getSys_id());
                    }
                }
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error while reading attachment content.", e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return attachmentContentIds;
    }
	
	private static WikiAttachmentVO persistIncidentAttachment(WikiAttachmentVO wikiAttachment, String incidentId, String username) throws Exception
	{   
		WikiAttachment attachment = null;
		try
    	{
			WikiAttachmentVO wikiAttachmentFinal = wikiAttachment;
       HibernateProxy.setCurrentUser(username);
			attachment = (WikiAttachment) HibernateProxy.execute(() -> {
	            //This is an attachment for an incident. Persist WikiAttachment along with WikiAttachmentContent.
	        	WikiAttachmentDAO daoWikiAttachment = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
	            WikiAttachmentContentDAO daoWikiAttachmentContent = HibernateUtil.getDAOFactory().getWikiAttachmentContentDAO();
	            
	            WikiAttachmentContentVO wacVO = wikiAttachmentFinal.ugetWikiAttachmentContent();
	            wacVO.setWikiAttachment(wikiAttachmentFinal);
	            
	            WikiAttachment searchedAttachment = new WikiAttachment(wikiAttachmentFinal);
	            searchedAttachment = daoWikiAttachment.persist(searchedAttachment);
	            
	            WikiAttachmentContent content = searchedAttachment.ugetWikiAttachmentContent();
	            content.setWikiAttachment(searchedAttachment);
	            
	            
	            daoWikiAttachmentContent.persist(content);
	            
	            return searchedAttachment;
			});
    	}
    	catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception("Could not save incident attachment for incidentId: " + incidentId);
        }
        finally
        {
            //to avoid memory leaks and release memory
            attachment.usetWikiAttachmentContent(null);
            wikiAttachment = attachment.doGetVO();
        }
		
		return attachment.doGetVO();
	}
	
	public static List<Map<String, String>> getSecurityGroupMembers(String username) throws Exception
	{
		List<Map<String, String>> userNameMap = null;
		String groupNames = PropertiesUtil.getPropertyString(Constants.SECURITY_GROUPS);
		if (StringUtils.isNotBlank(groupNames))
		{
			userNameMap = UserUtils.getGroupUserNames(groupNames, username);
		}
		
		return userNameMap;
	}
	
	@SuppressWarnings("unchecked")
	public static ActivityVO updateActivity(Map<String, Object> activityMap, String username) throws Exception
	{
		PBActivity activity = null;
		boolean nameChanged = false;
		boolean metaPhaseChanged = false;
		boolean metaActivityChanged = false;
		String auditMessage = null;
		StringBuilder auditMessageBldr = new StringBuilder();
		
		String incidentId = (String)activityMap.get("incidentId");
		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
		
        if ("Closed".equalsIgnoreCase(incidentVO.getStatus())) {
    		throw new Exception(ERROR_CLOSED_INCIDENT);
        }

        String msgObj = String.format(ACTIVITIES_IN_SIR,
				  					  (String)activityMap.get("activityName"),
				  					  incidentVO.getSir());
        
        v2LicenseViolationsAndEnvCheck(msgObj);
        
		JSONArray pactivityArray = PlaybookActivityUtils.getPbActivities(incidentId, incidentVO.getPlaybook(), 
																		 incidentVO.getPlaybookVersion(), 
																		 incidentVO.getSysOrg(), username);
		
		List<Map<String, String>> activityMapList =  new ObjectMapper().readValue(pactivityArray.toString(), List.class);
		// Keep track of old meta phase id<->new meta phase id and old meta activity id<->new meta activity id for runtime updates
		Map<String, String> newToOldMetaPhaseIds = new HashMap<String, String>();
		Map<String, String> newToOldMetaActivityIds = new HashMap<String, String>();
		String matchedOldMetaPhaseId = null;
		String matchedOldMetaActivityId = null; 
		String currMetaPhaseId = null;
		int currActivityIndex = 0;
		 
		for (Map<String, String> dbActivityMap : activityMapList)
		{
		    // Handle 6.2 meta phase id<->name and meta activity id<->ref wiki id + required + sla mismatch
		    
		    if (activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
		        StringUtils.isNotBlank((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
		        activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
		        StringUtils.isNotBlank((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))
		    {
		    	if (dbActivityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
			        StringUtils.isNotBlank((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
			        !((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)).equals(currMetaPhaseId))
		    	{
		    		currActivityIndex = 0;
		    		currMetaPhaseId = (String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY);
		    	}
		        
		    	currActivityIndex++;
		    	
		        if (dbActivityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
		            StringUtils.isNotBlank((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
		            dbActivityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
		            StringUtils.isNotBlank((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)) &&
		            ((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)).
		            equals((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
		            ((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)).
		            equals((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)) &&
		            currActivityIndex == 
		            ((Integer)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_POSITION_KEY)).intValue())
		        {
		            matchedOldMetaPhaseId = (String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY);
		            matchedOldMetaActivityId = (String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY);
		            
    		        // meta phase id<->name mismatch
    		        SIRPhaseMetaData sirPhaseMetaData = 
                                    PhaseUtils.createSIRPhaseMetaData((String)activityMap.
                                                                      get(PLAYBOOK_ACTIVITY_MAP_PHASE_KEY), 
                                                                      incidentVO.getSysOrg(), username);
                
    		        if (sirPhaseMetaData == null)
    		        {
    		            throw new Exception("Failed to create SIR phase meta data for phase in updated activity.");
    		        }
                
    		        if (!((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)).
    		             equals(sirPhaseMetaData.getSys_id()))
    		        {
    		            newToOldMetaPhaseIds.put(sirPhaseMetaData.getSys_id(),
    		                                     (String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY));
    		            
    		            dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY, sirPhaseMetaData.getSys_id());
    		            activityMap.put(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY, sirPhaseMetaData.getSys_id());
    		            
    		            metaPhaseChanged = true;
    		        }
    		        
    		        // meta activity id<->ref wiki id + required + sla mismatch
    		        
    		        Duration sla = SIRActivityMetaData.DEFAULT_SLA_DURATION;
                    
                    if (activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY) &&
                        (Integer)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY) != null)
                    {
                        try
                        {
                            Long slaDays = Long.valueOf(
                                                (Integer)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY));
                            
                            if ((Long.MAX_VALUE -  TimeUnit.NANOSECONDS.convert(slaDays, TimeUnit.DAYS)) > 0)
                            {
                                sla = sla.plusDays(slaDays);
                            }
                            else
                            {
                                sla = sla.plusDays(Long.MAX_VALUE / TimeUnit.NANOSECONDS.convert(1, TimeUnit.DAYS));
                            }
                        }
                        catch (NumberFormatException nfe)
                        {
                            Log.log.warn("Error " + nfe.getLocalizedMessage() + 
                                         " occurred while converting specified SLA " +
                                         PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY + " value " + 
                                         activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY).toString() +
                                         " to " + Long.class.getName() + ".", nfe);
                        }
                    }
                    
                    if (sla.toNanos() < Long.MAX_VALUE && 
                        activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY) &&
                        (Integer)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY) != null)
                    {
                        try
                        {
                            Long slaHours = Long.valueOf(
                                                (Integer)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY));
                            
                            if ((Long.MAX_VALUE - sla.toNanos()) > TimeUnit.NANOSECONDS.convert(slaHours, TimeUnit.HOURS))
                            {
                                sla = sla.plusHours(slaHours);
                            }
                            else
                            {
                                sla = sla.plusHours((Long.MAX_VALUE - sla.toNanos()) / 
                                                    TimeUnit.NANOSECONDS.convert(1, TimeUnit.HOURS));
                            }
                        }
                        catch (NumberFormatException nfe)
                        {
                            Log.log.warn("Error " + nfe.getLocalizedMessage() + 
                                         " occurred while converting specified SLA " +
                                         PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY + " value " + 
                                         activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY).toString() +
                                         " to " + Long.class.getName() + ".", nfe);
                        }
                    }
                    
                    if (sla.toNanos() < Long.MAX_VALUE && 
                        activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY) &&
                        (Integer)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY) != null)
                    {
                        try
                        {
                            Long slaMinutes = 
                                        Long.valueOf(
                                            (Integer)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY));
                            
                            if ((Long.MAX_VALUE - sla.toNanos()) > 
                                TimeUnit.NANOSECONDS.convert(slaMinutes, TimeUnit.MINUTES))
                            {
                                sla = sla.plusMinutes(slaMinutes);
                            }
                            else
                            {
                                sla = sla.plusMinutes((Long.MAX_VALUE - sla.toNanos()) / 
                                                      TimeUnit.NANOSECONDS.convert(1, TimeUnit.MINUTES));
                            }                                
                        }
                        catch (NumberFormatException nfe)
                        {
                            Log.log.warn("Error " + nfe.getLocalizedMessage() + 
                                         " occurred while converting specified SLA " +
                                         PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY + " value " + sla.toString() +
                                         " to " + Long.class.getName() + ".", nfe);
                        }
                    }
                    
                    /*
                     * DO NOT REMOVE : SLA granularity is upto minutes only.
                     * SLA is persisted in DB with default granularity which 
                     * is nano seconds. Code below normalizes SLA to minutes
                     * granularity.
                     */
                    
                    if (!sla.isZero())
                    {
                        sla = Duration.ofMinutes(sla.toMinutes());
                    }
                    
                    Log.log.debug("Updated Activity SLA based on days, hours and minutes: " + sla);
                    
                    Long longDate = (Long)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY);
                    
                    if (longDate != null)
                    {
                        if (!sla.isZero())
                        {
                            /*
                             * Currently removing association of Activity SLA and Activity Due By
                             * untill product decides if they are related or independent.
                             * 
                             * If they are related then any change in Due By is change in 
                             * activity SLA which being part of composite key will result
                             * into new Activity with SLA = Due By (timestamp) - 
                             * SIR creation time.
                             * 
                             * If they are not related then Activity Due By needs to be 
                             * persisted as part of Activity runtime data.
                             */
                            Duration dueBySLA = SIRActivityMetaData.DEFAULT_SLA_DURATION;
                            
                            // Activity creation time = Incident creation time
                            long activityCreateTimeMillis = incidentVO.getSysCreatedOn().getTime();
                            
                            if (longDate - activityCreateTimeMillis > 0)
                            {
                                if (TimeUnit.NANOSECONDS.convert((longDate.longValue() - activityCreateTimeMillis), TimeUnit.MILLISECONDS) < 
                                    Long.MAX_VALUE)
                                {
                                    dueBySLA = dueBySLA.plusMillis((longDate.longValue() - activityCreateTimeMillis));
                                }
                                else
                                {
                                    dueBySLA = dueBySLA.plusNanos(Long.MAX_VALUE);
                                }
                            }
                            
                            /*
                             * DO NOT REMOVE : SLA granularity is upto minutes only.
                             * SLA is persisted in DB with default granularity which 
                             * is nano seconds. Code below normalizes SLA to minutes
                             * granularity.
                             */
                            
                            if (!dueBySLA.isZero())
                            {
                                dueBySLA = Duration.ofMinutes(dueBySLA.toMinutes());
                            }
                            
                            Log.log.debug("Updated Activity SLA based on dueDate : " + dueBySLA);
                        }
                        else
                        {
                            Log.log.warn("Activity SLA is " + sla + " but " + PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY + 
                                         " is set to " + activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY) + "ms."  +
                                         " Resetting " + PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY + " to 0 ms.");
                            activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY, 
                                            Long.toString(SIRActivityMetaData.DEFAULT_SLA_DURATION.toMillis(), 10));
                        }
                        
                        Log.log.debug("Updated Activity SLA based on dueDate : " + sla);
                    }
                    
                    SIRActivityMetaData sirActivityMetaData = 
                        ActivityUtils.createSIRActivityMetaData(
                            (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
                            (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_DESCRIPTION_KEY), 
                            Boolean.valueOf(
                                        (Boolean)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY)).
                                        booleanValue(), 
                            (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY), null, 
                            Boolean.valueOf(
                                        (Boolean)activityMap.get(PLAYBOOK_ACTIVITY_MAP_TEMPLATE_ACTIVITY_KEY)).
                                        booleanValue(), 
                            sla, (String)activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY),
                            incidentVO.getSysOrg(), username);
                    
                    if (sirActivityMetaData == null)
                    {
                        throw new Exception("Failed to create SIR activity meta data for updated activity.");
                    }
                    
                    if (!((String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)).
                         equals(sirActivityMetaData.getSys_id()))
                    {
                        newToOldMetaActivityIds.put(
                                        sirActivityMetaData.getSys_id(),
                                        (String)dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY));
                        
                        dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY, Long.toString(sla.toDays()));
                        activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY, Long.toString(sla.toDays()));
                        
                        dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY, 
                                          Long.toString(sla.toHours() - 
                                                        (TimeUnit.HOURS.convert(sla.toDays(), TimeUnit.DAYS))));
                        activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY, 
                                        Long.toString(sla.toHours() - 
                                                      (TimeUnit.HOURS.convert(sla.toDays(), TimeUnit.DAYS))));
                        
                        dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY, 
                                          Long.toString(sla.toMinutes() - 
                                                        (TimeUnit.MINUTES.convert(sla.toHours(), TimeUnit.HOURS))));
                        activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY, 
                                        Long.toString(sla.toMinutes() - 
                                                      (TimeUnit.MINUTES.convert(sla.toHours(), TimeUnit.HOURS))));
                        
                        Log.log.debug("updateActivity Updated SLA: " + sla + ", Activity Map Values: " +
                                        activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY) + "D" + 
                                        activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY) + "H" + 
                                        activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY) + "M");
                        
                        dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY, sirActivityMetaData.getName());
                        activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY, sirActivityMetaData.getName());
                        
                        dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY,
                                          sirActivityMetaData.getAltActivityId());
                        activityMap.put(PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY,
                                        sirActivityMetaData.getAltActivityId());
                        
                        dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY,
                                          sirActivityMetaData.getSys_id());
                        activityMap.put(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY,
                                        sirActivityMetaData.getSys_id());
                        
                        metaActivityChanged = true;
                    }
                    
                    break;
		        }
		    }
		    else
		    {
    		    String dbActivityId = dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY);
    		    
    		    if (StringUtils.isNotBlank(dbActivityId) && 
    		        dbActivityId.equalsIgnoreCase((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY)) &&
    		        !dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY).equals(
    		                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)))
    		    {
    				auditMessage = String.format("User '%s' changed name of a custom activity from '%s' to '%s'",
    				                             username, 
    				                             dbActivityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY),
                                                 activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY));
    		        nameChanged = true;
                    dbActivityMap.put(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY, 
                                      (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY));
    		    }
		    }
		}
		
		if (nameChanged || metaPhaseChanged || metaActivityChanged)
		{
		    JSONArray updatedJArray = new JSONArray();
		    updatedJArray.addAll(activityMapList);
		    PlaybookActivityUtils.savePbActivities(incidentId, incidentVO.getPlaybook(), 
		                                           incidentVO.getPlaybookVersion(), updatedJArray, username);
		}
		
		ActivityVO activityVO = new ActivityVO();
		
		if (activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
		    StringUtils.isNotBlank((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
		    activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
		    StringUtils.isNotBlank((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))
		{
		    // For 6.2 and above new Activities get runtime data from DB
		    
		    SIRPhaseMetaData newSIRPhaseMetaData = null;
		    
		    Integer activityIndex = currActivityIndex > 0 ? Integer.valueOf(currActivityIndex) : null;
		    
		    SIRActivityRuntimeData oldDBSIRActivityRuntimeData = 
                            ActivityUtils.findSIRActivityRuntimeData(matchedOldMetaPhaseId, null, 
                                                                     matchedOldMetaActivityId, activityIndex, 
                                                                     incidentId, incidentVO.getSysOrg(), 
                                                                     username);
		    
		    if (oldDBSIRActivityRuntimeData == null)
		    {
		        Log.log.error("Failed to find SIR Activity Runtime data with old phase meta data id " +
		                      matchedOldMetaPhaseId + ", activity meta data id " + 
		                      matchedOldMetaActivityId + ", and incident id " + incidentId + 
		                      " for updated activity.");
		        throw new Exception ("Failed to find old SIR Activity Runtime data for updated activity.");
		    }
		    
		    Log.log.debug("Old DB SIR Activity Runtime Data: " + oldDBSIRActivityRuntimeData);
					    
		    if (metaPhaseChanged)
		    {
		        // Update Meta Phase in Phase runtime record in DB
		        
		        SIRPhaseRuntimeData dbSIRPhaseRuntimeData = 
		            PhaseUtils.findSIRPhaseRuntimeData(
		                newToOldMetaPhaseIds.get((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)), 
		                                         null, incidentId, incidentVO.getSysOrg(), username);
		        
		        if (dbSIRPhaseRuntimeData == null)
		        {
		            Log.log.error("Failed to find existing SIR Phase Runtime data with old phase meta data id " + 
		                          newToOldMetaPhaseIds.get(
		                                          (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) +
		                          ", and incident id " + incidentId + " to update.");
		            throw new Exception ("Failed to find existing SIR Phase Runtime data to update.");
		        }
		        
		        newSIRPhaseMetaData = 
		                        HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO().findById(
		                                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY));
		        
		        if (newSIRPhaseMetaData == null)
		        {
		            Log.log.error("Failed to find SIR Phase meta data for new phase meta data id " + 
	                              (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) +
	                              ".");
	                throw new Exception ("Failed to find SIR Phase meta data for new phase meta data id.");
		        }
		        
		        dbSIRPhaseRuntimeData.setPhaseMetaData(newSIRPhaseMetaData);
		        
		        dbSIRPhaseRuntimeData.setSysUpdatedBy(username);
		        dbSIRPhaseRuntimeData.setSysUpdatedOn(GMTDate.getDate());
		        dbSIRPhaseRuntimeData.setSysModCount(
		                        Integer.valueOf(dbSIRPhaseRuntimeData.getSysModCount().intValue() + 1));
		        
		        try 
		        {
            HibernateProxy.setCurrentUser(username);
		            HibernateProxy.execute(()-> {
		            
			            SIRPhaseRuntimeData updatedSIRPhaseRuntimeData = 
			                            HibernateUtil.getDAOFactory().getSIRPhaseRuntimeDataDAO().
			                            update(dbSIRPhaseRuntimeData);
			        
			            Log.log.debug("Updated SIR Phase Runtime Data: " + updatedSIRPhaseRuntimeData);
		        
		            });
		        }
		        catch (Throwable t)
		        {
		            log.error("Error " + t.getLocalizedMessage() + " updating SIR Phase Runtime Data: " + dbSIRPhaseRuntimeData, t);		           
                          HibernateUtil.rethrowNestedTransaction(t);
		        }
		    }
		    
		    SIRActivityRuntimeData effectiveSIRActivityRuntimeData = null;
		    
		    if (metaActivityChanged)
		    {
		        // Update Meta Phase (if modified) and Meta Activity in Activity runtime record in DB
		        String oldSIRPhaseMetaDataId = 
		                    metaPhaseChanged ? 
		                        newToOldMetaPhaseIds.get(
		                                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) :
		                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY);
		                                        
		        SIRActivityRuntimeData dbSIRActivityRuntimeData = 
		                        ActivityUtils.findSIRActivityRuntimeData(
		                            oldSIRPhaseMetaDataId, 
		                            null, 
		                            newToOldMetaActivityIds.get(
		                                    (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)), 
		                            null, incidentId, incidentVO.getSysOrg(), username);
		        
		        if (dbSIRActivityRuntimeData == null)
                {
                    Log.log.error("Failed to find existing SIR Activity Runtime data with old phase meta data id " +
                                  oldSIRPhaseMetaDataId + 
                                  ", old activity meta data id " + 
                                  newToOldMetaActivityIds.get(
                                              (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)) +
                                  ", and incident id " + incidentId + " to update.");
                    throw new Exception ("Failed to find existing SIR Activity Runtime data to update.");
                }
		        
		        if (metaPhaseChanged)
		        {
		            dbSIRActivityRuntimeData.setPhaseMetaData(newSIRPhaseMetaData);
		        }
		        
		        SIRActivityMetaData newSIRActivityMetaData = null;
		        
		        try 
                {
                  HibernateProxy.setCurrentUser(username);
		        	newSIRActivityMetaData = (SIRActivityMetaData) HibernateProxy.execute(() -> 
		        		HibernateUtil.getDAOFactory().getSIRActivityMetaDataDAO().findById(
                                                    (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)));
                    
                    Log.log.debug("New SIR Activity Runtime Data: " + newSIRActivityMetaData);
                    
                    
                }
                catch (Throwable t)
                {
                    log.error("Error " + t.getLocalizedMessage() + " in getting new SIR Activity Meta Data for Id: " + 
                              (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY), t);                    
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
		        
		        if (StringUtils.isNotBlank(
		                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY)))
		        {
		            dbSIRActivityRuntimeData.setAssignee(
		                            (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY));
		            
		            boolean updateAssignedUser = false;
		            
		            if (dbSIRActivityRuntimeData.getAssignedUser() != null)
		            {
		                if (!dbSIRActivityRuntimeData.getAssignedUser().getUUserName().
		                    equals(dbSIRActivityRuntimeData.getAssignee()))
		                {
		                    updateAssignedUser = true;
		                }
		            }
		            else
		            {
		                updateAssignedUser = true;
		            }
		            
		            if (updateAssignedUser)
		            {
		                Users assignedUser = UserUtils.findUser(null, dbSIRActivityRuntimeData.getAssignee());
		                
		                if (assignedUser == null)
		                {
		                    Log.log.error("Failed to find user info for user " + 
		                                  dbSIRActivityRuntimeData.getAssignee() +
		                                  " assigned to updated activity.");
		                    throw new Exception("Failed to find user info for user assigned to updated activity");
		                }
		                
		                dbSIRActivityRuntimeData.setAssignedUser(assignedUser);
		            }
		        }
		        else
		        {
		            // Updated activity is unassigned
		            dbSIRActivityRuntimeData.setAssignee(null);
		            dbSIRActivityRuntimeData.setAssignedUser(null);
		        }
		        
		        if (StringUtils.isNotBlank(
                                (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY)))
                {
		            dbSIRActivityRuntimeData.setStatus(
		                            (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY));
                }
		        
		        dbSIRActivityRuntimeData.setActivityMetaData(newSIRActivityMetaData);
		        
		        dbSIRActivityRuntimeData.setSysUpdatedBy(username);
		        dbSIRActivityRuntimeData.setSysUpdatedOn(GMTDate.getDate());
		        dbSIRActivityRuntimeData.setSysModCount(
                                Integer.valueOf(dbSIRActivityRuntimeData.getSysModCount().intValue() + 1));
		        
		        try 
                {
                  HibernateProxy.setCurrentUser(username);
		        	effectiveSIRActivityRuntimeData = (SIRActivityRuntimeData) HibernateProxy.execute(() ->                    
                                    HibernateUtil.getDAOFactory().getSIRActivityRuntimeDataDAO().
                                    update(dbSIRActivityRuntimeData));
		        
                    Log.log.debug("Updated and Effective SIR Activity Runtime Data: " + effectiveSIRActivityRuntimeData);
                    
                    
                }
		        catch (Throwable t)
		        {
                    log.error("Error " + t.getLocalizedMessage() + " updating SIR Activity Runtime Data: " + dbSIRActivityRuntimeData, t);
                                HibernateUtil.rethrowNestedTransaction(t);
                   
                }
		    }
		    
		    if (effectiveSIRActivityRuntimeData == null)
		    {		        
		        SIRActivityRuntimeData dbSIRActivityRuntimeData = new SIRActivityRuntimeData(oldDBSIRActivityRuntimeData.doGetVO());
		        
		        Log.log.debug("SIR Activity Runtime Data B4 modifications: " + dbSIRActivityRuntimeData);
		        
		        if (StringUtils.isNotBlank(
                                (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY)))
                {
                    dbSIRActivityRuntimeData.setAssignee(
                                    (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY));
                    
                    boolean updateAssignedUser = false;
                    
                    if (dbSIRActivityRuntimeData.getAssignedUser() != null)
                    {
                        if (!dbSIRActivityRuntimeData.getAssignedUser().getUUserName().
                            equals(dbSIRActivityRuntimeData.getAssignee()))
                        {
                            updateAssignedUser = true;
                        }
                    }
                    else
                    {
                        // No previous assigned user to assigned user change is also an update of assigned user
                        updateAssignedUser = true;
                    }
                    
                    if (updateAssignedUser)
                    {
                        Users assignedUser = UserUtils.findUser(null, dbSIRActivityRuntimeData.getAssignee());
                        
                        if (assignedUser == null)
                        {
                            Log.log.error("Failed to find user info for user " + 
                                          dbSIRActivityRuntimeData.getAssignee() +
                                          " assigned to updated activity.");
                            throw new Exception("Failed to find user info for user assigned to updated activity");
                        }
                        
                        dbSIRActivityRuntimeData.setAssignedUser(assignedUser);
                    }
                }
                else
                {
                    // Updated activity is unassigned
                    dbSIRActivityRuntimeData.setAssignee(null);
                    dbSIRActivityRuntimeData.setAssignedUser(null);
                }
                
                if (StringUtils.isNotBlank(
                                (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY)))
                {
                    dbSIRActivityRuntimeData.setStatus(
                                    (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY));
                }
                
                dbSIRActivityRuntimeData.setSysUpdatedBy(username);
                dbSIRActivityRuntimeData.setSysUpdatedOn(GMTDate.getDate());
                dbSIRActivityRuntimeData.setSysModCount(
                                Integer.valueOf(dbSIRActivityRuntimeData.getSysModCount().intValue() + 1));
                
                try 
                {
                  HibernateProxy.setCurrentUser(username);
                	effectiveSIRActivityRuntimeData = (SIRActivityRuntimeData) HibernateProxy.execute(() ->
                                    HibernateUtil.getDAOFactory().getSIRActivityRuntimeDataDAO().
                                    update(dbSIRActivityRuntimeData));
                
                    Log.log.debug("Updated and Effective SIR Activity Runtime Data: " + effectiveSIRActivityRuntimeData);
                    
                }
                catch (Throwable t)
                {
                    log.error("Error " + t.getLocalizedMessage() + " updating SIR Activity Runtime Data: " + dbSIRActivityRuntimeData, t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                    
                }
		    }
		    
		    Log.log.debug("Updated DB SIR Activity Runtime Data: " + effectiveSIRActivityRuntimeData);
		    
		    boolean canChangeAssignee = UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_TASK_ASSIGNEE_CHANGE, null);
		    boolean assigneeChanged = false;
		    if (StringUtils.isNotBlank(effectiveSIRActivityRuntimeData.getAssignee()))
            {
                if (StringUtils.isBlank(oldDBSIRActivityRuntimeData.getAssignee()))
                {
                    assigneeChanged = true;
                    if (canChangeAssignee)
                    {
                        auditMessageBldr.append(String.format("User '%s' changed the assignee of activity named '%s' to '%s'.", 
                                                              username, 
                                                              effectiveSIRActivityRuntimeData.getActivityMetaData().getName(), 
                                                              effectiveSIRActivityRuntimeData.getAssignee()));
                    }
                }
                else
                {
                    if (!oldDBSIRActivityRuntimeData.getAssignee().equals(
                                    effectiveSIRActivityRuntimeData.getAssignee()))
                    {
                        assigneeChanged = true;
                        if (canChangeAssignee)
                        {
                            auditMessageBldr.append(String.format("User '%s' changed the assignee for activity named '%s' from '%s' to '%s'.", 
                                                                  username, 
                                                                  effectiveSIRActivityRuntimeData.getActivityMetaData().getName(), 
                                                                  oldDBSIRActivityRuntimeData.getAssignee(), 
                                                                  effectiveSIRActivityRuntimeData.getAssignee()));
                        }
                    }
                }
                
                if (assigneeChanged)
                {
                    if (canChangeAssignee)
                    {
                        activityVO.setAssignee(effectiveSIRActivityRuntimeData.getAssignee());
                    }
                    else
                    {
                        throw new Exception(String.format("Permission Error: User %s does not have permission to change assignee", username));
                    }
                }
            }
		    
		    if (StringUtils.isNotBlank(effectiveSIRActivityRuntimeData.getStatus()))
            {
		        if (StringUtils.isBlank(oldDBSIRActivityRuntimeData.getStatus()))
                {
                    auditMessageBldr.append(String.format((auditMessageBldr.length() > 0 ? " " : "") + 
                                                          "User '%s' changed the status of activity named '%s' to '%s'.", 
                                                          username, 
                                                          effectiveSIRActivityRuntimeData.getActivityMetaData().getName(), 
                                                          effectiveSIRActivityRuntimeData.getStatus()));
                }
                else
                {
                    if (!oldDBSIRActivityRuntimeData.getStatus().equals(
                                    effectiveSIRActivityRuntimeData.getStatus()))
                    {
                        auditMessageBldr.append(String.format((auditMessageBldr.length() > 0 ? " " : "") +
                                                              "User '%s' changed the status for activity named '%s' from '%s' to '%s'.", 
                                                              username, 
                                                              effectiveSIRActivityRuntimeData.getActivityMetaData().getName(), 
                                                              oldDBSIRActivityRuntimeData.getStatus(), 
                                                              effectiveSIRActivityRuntimeData.getStatus()));
                    }
                }
            }
		    
		    if (effectiveSIRActivityRuntimeData.getActivityMetaData().getSLA().toMinutes() !=
		        oldDBSIRActivityRuntimeData.getActivityMetaData().getSLA().toMinutes())
		    {
		        if (oldDBSIRActivityRuntimeData.getActivityMetaData().getSLA().isZero())
		        {
		            auditMessageBldr.append(String.format((auditMessageBldr.length() > 0 ? " " : "") +
		                                                  "User '%s' changed the due by duration of activity named '%s' to '%s'.", 
		                                                  username, 
		                                                  effectiveSIRActivityRuntimeData.getActivityMetaData().getName(), 
		                                                  effectiveSIRActivityRuntimeData.getActivityMetaData().getSLA().toString()));
		        }
		        else
		        {
		            auditMessageBldr.append(String.format((auditMessageBldr.length() > 0 ? " " : "") +
		                                                  "User '%s' changed the due by duration of activity named '%s' from '%s' to '%s'.", 
		                                                  username, 
		                                                  effectiveSIRActivityRuntimeData.getActivityMetaData().getName(),
		                                                  oldDBSIRActivityRuntimeData.getActivityMetaData().getSLA(),
		                                                  effectiveSIRActivityRuntimeData.getActivityMetaData().getSLA()));
		        }
		    }
		    
		    activityVO.setActivityName(effectiveSIRActivityRuntimeData.getActivityMetaData().getName());
            activityVO.setAltActivityId(effectiveSIRActivityRuntimeData.getActivityMetaData().getAltActivityId());
            
            activityVO.setDescription(effectiveSIRActivityRuntimeData.getActivityMetaData().getDescription());
            
            if (!effectiveSIRActivityRuntimeData.getActivityMetaData().getSLA().isZero())
            {
                long dueDateMillis = effectiveSIRActivityRuntimeData.getSysCreatedOn().getTime();
                
                if ((Long.MAX_VALUE - dueDateMillis) > 
                    effectiveSIRActivityRuntimeData.getActivityMetaData().getSLA().toMillis())
                {
                    dueDateMillis += effectiveSIRActivityRuntimeData.getActivityMetaData().getSLA().toMillis();
                }
                else
                {
                    dueDateMillis = Long.MAX_VALUE; 
                }
                
                activityVO.setDueDate(GMTDate.getDate(dueDateMillis));
            }
            
            activityVO.setId(effectiveSIRActivityRuntimeData.getActivityMetaData().getSys_id());
            activityVO.setIncidentId(incidentId);
            activityVO.setPhase(effectiveSIRActivityRuntimeData.getPhaseMetaData().getName());
            activityVO.setStatus(effectiveSIRActivityRuntimeData.getStatus());
            activityVO.setWikiName(effectiveSIRActivityRuntimeData.getReferencedRSI().getPlaybook());
            activityVO.setWorksheetId(effectiveSIRActivityRuntimeData.getReferencedRSI().getProblemId());
            activityVO.setAuditMessage(auditMessageBldr.toString());
            activityVO.setPosition(activityIndex);
		}
		else
		{
		    // Get Activity runtime from ES
    		String activityId = (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY);
    		ResponseDTO<PBActivity> activities = PlaybookSearchAPI.searchPBActivity(activityId, incidentId, username);
    		
    		if (activities != null && activities.getRecords() != null && activities.getRecords().size() > 0)
    		{
    			// update
    			activity = activities.getRecords().get(0);
    			long now = DateUtils.GetUTCDateLong();
    			activity.setSysUpdatedOn(now);
    			activity.setSysUpdatedDt(DateUtils.getDate(now));
    		}
    		else
    		{
    			// insert
    			activity = new PBActivity(activityId, null, username);
    			activity.setIncidentId(incidentId);
    			startIncident(activity.getIncidentId(), username);
    			activity.setAltActivityId(activityId);
    		}
    		
    		boolean canChangeAssignee = UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_TASK_ASSIGNEE_CHANGE, null);
    		boolean assigneeChange = false;
    		if (StringUtils.isNotBlank(
    		            (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY)))
    		{
    		    if (StringUtils.isBlank(activity.getAssignee()))
    		    {
    		        assigneeChange = true;
    		        if (canChangeAssignee)
    		        {
        		        auditMessage = String.format("User '%s' changed the assignee of activity named '%s' to '%s'", 
        		                                     username, 
        		                                     activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
        		                                     activityMap.get(
        		                                                 PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY));
    		        }
    		    }
    		    else
    		    {
    		        if (!activity.getAssignee().equals(
    		                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY)))
    		        {
    		            assigneeChange = true;
    		            if (canChangeAssignee)
    		            {
        		            auditMessage = 
        		                        String.format(
        		                            "User '%s' changed the assignee for activity named '%s' from '%s' to '%s'", 
        		                            username, 
        		                            activityMap.get("activityName"), 
        		                            activity.getAssignee(), 
        		                            activityMap.get("assignee"));
    		            }
    		        }
    		    }
    		}
    		
    		if (assigneeChange)
    		{
    		    if (canChangeAssignee)
    		    {
    		        activity.setAssignee((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY));
    		    }
    		    else
    		    {
    		        throw new Exception(String.format("Permission Error: User %s does not have permission to change assignee", username));
    		    }
    		}
    		Long longDate = (Long)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY);
    		
    		if (longDate != null)
    		{
        		if (activity.getDueDate() == null)
        		{
        		    auditMessage = String.format("User '%s' added a due date to activity named '%s' to '%s'", 
        		                                 username, 
        		                                 activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
        		                                 new Date(longDate));
        		}
        		else
        		{
        		    if (activity.getDueDate().getTime() != longDate)
        		    {
        		        auditMessage = String.format(
        		                        "User '%s' changed a due date of activity named '%s' from '%s' to '%s'", 
        		                        username, 
        		                        activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
        		                        activity.getDueDate(), new Date(longDate));
        		    }
        		}
    		}
    		
    		if(longDate != null)
    		{
    			activity.setDueDate(new Date(longDate));
    		}
    		
    		boolean canChangeStatus = UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_TASK_STATUS_CHANGE, null);
    		boolean statusChanged = false;
    		if (activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY) != null)
    		{
    		    if (StringUtils.isBlank(activity.getStatus()))
    		    {
    		        statusChanged = true;
    		        if (canChangeStatus)
    		        {
        		        auditMessage = String.format("User '%s' added a new status for activity named '%s' to '%s'", 
        		                                     username, 
        		                                     activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
        		                                     activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY));
    		        }
    		    }
    		    else
    		    {
    		        if (!activity.getStatus().equals(
    		                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY)))
    	            {
    		            statusChanged = true;
    		            if (canChangeStatus)
    		            {
        	                auditMessage = String.format(
        	                                "User '%s' changed the status of activity named '%s' from '%s' to '%s'", 
        	                                username, 
        	                                activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
        	                                activity.getStatus(), 
        	                                activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY));
    		            }
    	            }
    		    }    		
    		}
    		
    		if (statusChanged)
    		{
    		    if (canChangeStatus)
    		    {
    		        activity.setStatus((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY));
    		    }
    		    else
    		    {
    		        throw new Exception(String.format("Permission Error: User %s does not have access to change status", username));
    		    }
    		}
    		activity.setWorksheetId(incidentVO.getProblemId());
    		activity.setModified(true);
    		
    		PlaybookIndexAPI.indexPBActivity(activity, username);
    		
    		activityVO.setActivityName((String)activityMap.get("activyName"));
    		activityVO.setAltActivityId(activityId);
    		activityVO.setAssignee(activity.getAssignee());
    		activityVO.setDescription((String)activityMap.get("description"));
    		activityVO.setDueDate(activity.getDueDate());
    		activityVO.setId(activityId);
    		activityVO.setIncidentId(incidentId);
    		activityVO.setPhase((String)activityMap.get("phase"));
    		activityVO.setStatus(activity.getStatus());
    		activityVO.setWikiName((String)activityMap.get("wikiName"));
    		activityVO.setWorksheetId(activity.getWorksheetId());
    		activityVO.setAuditMessage(auditMessage);
		}
		
		return activityVO;
	}
	
	public static List<ActivityVO> saveActivities(String incidentId, JSONArray activitiesJson, String username) throws Exception
	{
		List<ActivityVO> activityList = null;
		
		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
		if (incidentVO != null)
		{
	        if ("Closed".equalsIgnoreCase(incidentVO.getStatus())) {
	    		throw new Exception(ERROR_CLOSED_INCIDENT);
	        }

			PlaybookActivityUtils.savePbActivities(incidentId, incidentVO.getPlaybook(), incidentVO.getPlaybookVersion(), activitiesJson, username);
			
			/*
             *  Update associated playbook template version as long as 
             *  status of associated SIR is NOT In Progress or Closed.
             */
            
            if (incidentVO.getStatus() != null && !incidentVO.getStatus().equals("Closed") &&
                !incidentVO.getStatus().equals("In Progress"))
            {
    			try
                {
                  HibernateProxy.setCurrentUser(username);
                    HibernateProxy.execute(() -> {
                    
	                    // Update playbook template version
	                    
	                    incidentVO.setPlaybookVersion(getTemplateVersion(incidentVO.getPlaybook(), username));
	                    
	                    ResolveSecurityIncident updatedSIR = new ResolveSecurityIncident();
	                    updatedSIR.setSysCreatedBy(incidentVO.getSysCreatedBy());
	                    updatedSIR.setSysCreatedOn(incidentVO.getSysCreatedOn());
	                    updatedSIR.setSysOrg(incidentVO.getSysOrg());
	                    
	                    updatedSIR.applyVOToModel(incidentVO);
	                    
	                    HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().update(updatedSIR);
                    
                    });
                }
                catch (Throwable t)
                {
                    Log.log.info("Failed to update version of playbok template associated with SIR. SIR = " + incidentVO.getSir() +
                                 ", Playbook Template = " + incidentVO.getPlaybook() + ", Version = " + incidentVO.getPlaybookVersion());                   
                    throw t;
                }
            }
			
    		touchSIR(username, incidentId);
			activityList = getAllPbActivities(incidentId, incidentVO.getPlaybook(), 
			                                  incidentVO.getPlaybookVersion(), incidentVO.getSysOrg(), username);
		}
		
		return activityList;
	}
	
	public static List<ActivityVO> addActivity(String incidentId, String activityJson, String username)
			throws Exception {
		List<ActivityVO> activityVOList = new ArrayList<ActivityVO>();

		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
		ResolveSecurityIncidentVO updatedIncidentVOResult = incidentVO;

		try {
        HibernateProxy.setCurrentUser(username);
			updatedIncidentVOResult = (ResolveSecurityIncidentVO) HibernateProxy.execute(() -> {
				ResolveSecurityIncidentVO updatedIncidentVO = incidentVO;

				if (incidentVO != null) {
					if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_TASK_CREATE,
							null)) {
						throw new Exception(String.format("User %s is denied access to create activity for SIR %s.",
								username, incidentVO.getSir()));
					}

					if ("Closed".equalsIgnoreCase(incidentVO.getStatus())) {
						throw new Exception(ERROR_CLOSED_INCIDENT);
					}

					checkUsersSIROrgAccess(incidentVO, username);

					String msgObj = String.format("activity to SIR %s", incidentVO.getSir());

					v2LicenseViolationsAndEnvCheck(msgObj);

					boolean updatePlaybookSIRRefCounts = false;

					if (!PlaybookActivityUtils.hasSIRActivities(incidentVO.getSys_id())) {
						updatePlaybookSIRRefCounts = true;
					}

					JSONObject savedActivityJson = PlaybookActivityUtils.addPbActivity(incidentId,
							incidentVO.getPlaybook(), incidentVO.getPlaybookVersion(), activityJson,
							incidentVO.getSysOrg(), username);

					updatedIncidentVO = getDetachedSecurityIncident(incidentId, username);

					ResolveSecurityIncident updatedSIR = new ResolveSecurityIncident();

					updatedIncidentVO.setSysUpdatedBy(username);
					updatedIncidentVO.setSysUpdatedOn(new GregorianCalendar(TimeZone.getTimeZone("GMT")).getTime());
					updatedIncidentVO.setSysModCount(Integer.valueOf(incidentVO.getSysModCount().intValue() + 1));

					updatedSIR.applyVOToModel(updatedIncidentVO);

					HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().update(updatedSIR);

					// Increment SIRRefcount of template wikidoc
					// only if SIR has no custom SIR specific activity.

					if (updatePlaybookSIRRefCounts) {
						updatePlaybookSIRRefCounts(incidentVO.getPlaybook(), null,
								updatedIncidentVO.getPlaybookVersion(), username);
					}

					/*
					 * If activity JSON has new meta phase id and meta activity id then create phase
					 * and activity runtime data instead of adding document into ES pbActivities
					 * index
					 */

					if (!(savedActivityJson.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)
							&& StringUtils.isNotBlank(
									savedActivityJson.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY))
							&& savedActivityJson.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)
							&& StringUtils.isNotBlank(savedActivityJson
									.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))) {
						updateActivityIndex(savedActivityJson.toString(), updatedIncidentVO, username);
					}
				}

				return updatedIncidentVO;
			});
		} catch (Throwable t) {
			if (StringUtils.isNotBlank(t.getLocalizedMessage())
					&& t.getLocalizedMessage()
							.contains(PlaybookActivityUtils.DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG)
					|| (t.getCause() != null && StringUtils.isNotBlank(t.getCause().getLocalizedMessage())
							&& t.getCause().getLocalizedMessage().contains(
									PlaybookActivityUtils.DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG))) {
				Log.log.warn("Failed to add custom playbook activity to SIR. " + t.getLocalizedMessage() + " SIR = "
						+ updatedIncidentVOResult.getSir() + ", Activity JSON = " + activityJson + ". "
						+ t.getLocalizedMessage());
			} else {
				Log.log.error("Failed to add custom playbook activity to SIR. " + t.getLocalizedMessage() + " SIR = "
						+ updatedIncidentVOResult.getSir() + ", Activity JSON = " + activityJson, t);
			}

			throw t;
		}

		touchSIR(username, incidentId);
		activityVOList = getAllPbActivities(incidentId, updatedIncidentVOResult.getPlaybook(),
				updatedIncidentVOResult.getPlaybookVersion(), updatedIncidentVOResult.getSysOrg(), username);

		return activityVOList;
	}
	
	public static List<ActivityVO> getAllPbActivities (String incidentId, String playbook, Integer version, 
	                                                   String orgId, String username) throws Exception
	{
		List<ActivityVO> activityVOList = new ArrayList<ActivityVO>();
		JSONArray activityList = PlaybookActivityUtils.getPbActivities(incidentId, playbook, version, orgId, username);
		
		if (activityList != null && activityList.size() > 0)
		{
		    int currPhaseIndex = 0;
		    int currActivityIndex = 0;
		    String currMetaPhaseId = null;
		    
			for (int i=0; i<activityList.size(); i++)
			{
				JSONObject jsonActivity = (JSONObject)activityList.get(i);
				
				if ((jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY) &&
				     StringUtils.isBlank(jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY))) ||
				    !jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY))
				{
				    jsonActivity.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY, Boolean.TRUE);
				}
				
				/*
				 * legacy activity : pre 6.2 i.e. with no phase and activity meta data
				 * new activity : 6.2 onwards i.e. with phase and activity meta data created 
				 *                at the time of (template/custom) activity generation.
				 *                  
				 * Get legacy activity runtime data such as status, assignee from ES,
				 * once only if DB runtime data is missing. 
				 * 
				 * In 6.2 activity SLA java.util.Duration is part of activity metadata as it is  
                 * not editable and forms one of constituents of unique composite key on activity 
                 * meta data.
                 * 
				 * For new acitvities no documents will be creted into pbactivities index in ES 
				 * and any update to activity will update the runtime data in DB.
				 */
				
				/*
				 * Identify Phase and Activity within Phase Index, Note all activity JSONs will 
				 * have the new phase and activity meta data keys or none i.e. no mix old and new 
				 * activities.
				 */
				
				if (jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
				    StringUtils.isNotBlank(jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
				    jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
				    StringUtils.isNotBlank(jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))
				{
				    if (!jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY).
				         equals(currMetaPhaseId))
				    {
				        currPhaseIndex++;
				        currActivityIndex = 0;
				        currMetaPhaseId = jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY);
				    }
				    
				    currActivityIndex++;
				}
				
				String updatedActivity = getActivityRuntimeInfo(jsonActivity.toString(), currPhaseIndex, currActivityIndex, 
				                                                incidentId, orgId, username);
				ActivityVO vo = populateActivityVO(updatedActivity, currActivityIndex, username);
				activityVOList.add(vo);
			}
		}
		
		return activityVOList;
	}
	
	public static List<ActivityVO> listActivitiesByIncidentId(String incidentId, String username) throws Exception
	{
		List<ActivityVO> activityList = null;
		
		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
		if (incidentVO != null)
		{
			activityList = getAllPbActivities(incidentId, incidentVO.getPlaybook(), 
			                                  incidentVO.getPlaybookVersion(), incidentVO.getSysOrg(), 
			                                  username);
		}
		
		return activityList;
	}
	
	@SuppressWarnings("unchecked")
	private static String getActivityRuntimeInfo(String activityJson, int phaseIndex, int activityIndex, 
	                                             String incidentId, String orgId, String username) throws Exception
	{
		String activityJsonResult = "";
		if (StringUtils.isNotBlank(activityJson) && StringUtils.isNotBlank(incidentId))
		{
			Map<String, Object> activityMap = new ObjectMapper().readValue(activityJson, HashMap.class);
			
			activityMap.put(PLAYBOOK_ACTIVITY_MAP_INCIDENT_ID_KEY, incidentId);
			String activityId = (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY);
			
			if(StringUtils.isNotBlank(activityId))
			{
			    if (activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
			        StringUtils.isNotBlank((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
			        activityMap.containsKey(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
			        StringUtils.isNotBlank((String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))
			    {
			        SIRActivityRuntimeData sirActivityRuntimeData = 
			                        ActivityUtils.findSIRActivityRuntimeData(
			                                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY),
			                                        Integer.valueOf(phaseIndex),
			                                        (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY),
			                                        Integer.valueOf(activityIndex), incidentId,
			                                        orgId, username);
			        
			        if (sirActivityRuntimeData == null)
			        {
			            Log.log.error("Failed to get SIR activity runtime data for meta phase id " + 
			                          (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) + 
			                          " and meta activity id " + 
			                          (String)activityMap.get(PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) + 
			                          " at activity index " + activityIndex + ".");
			            throw new Exception("Failed to get SIR activity runtime data.");
			        }
			        
			        activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY,
			                        sirActivityRuntimeData.getStatus());
			        
			        if (StringUtils.isNotBlank(sirActivityRuntimeData.getAssignee()))
			        {
			            activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY,
			                            sirActivityRuntimeData.getAssignee());
			        }
			        
			        if (!sirActivityRuntimeData.getActivityMetaData().getSLA().isZero())
			        {
			            long dueDateMillis = sirActivityRuntimeData.getSysCreatedOn().getTime();
			            
			            if ((Long.MAX_VALUE - dueDateMillis) > 
			                sirActivityRuntimeData.getActivityMetaData().getSLA().toMillis())
			            {
			                dueDateMillis += sirActivityRuntimeData.getActivityMetaData().getSLA().toMillis();
			            }
			            else
			            {
			                dueDateMillis = Long.MAX_VALUE; 
			            }
			            
			            activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY,
			                            Long.valueOf(dueDateMillis));
			        }
			    }
			    else
			    {
    				ResponseDTO<PBActivity> result = getActivity(activityId, incidentId, username);
    				
    				if (result != null && result.getRecords() != null && result.getRecords().size() > 0)
    				{
    					PBActivity activity = result.getRecords().get(0);
    					if (activity != null)
    					{
    						activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY, 
    						                activity.getStatus());
    						activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY, 
    						                activity.getAssignee());
    						if (activity.getDueDate() != null)
    						{
    							activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY, 
    							                activity.getDueDate().getTime());
    						}
    					}
    				}
    				else
    				{
    					ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
    					PBActivity activity = new PBActivity(activityId, null, username);
    					activity.setAssignee(incidentVO.getOwner());
    					activity.setStatus(PLAYBOOK_ACTIVITY_RUNTIME_STATUS_DEFAULT_VALUE);
    					activity.setAltActivityId(activityId);
    					activity.setWorksheetId(incidentVO.getProblemId());
    					activity.setIncidentId(incidentId);
    					Long dueDate = getDueDate(activityMap);
    					if (dueDate != 0)
    					{
    						activity.setDueDate(new Date(dueDate));
    					}
    					activity.setModified(true);
    					PlaybookIndexAPI.indexPBActivity(activity, username);
    					
    					activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY, activity.getStatus());
    					activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY, 
    					                activity.getAssignee());
    					if (activity.getDueDate() != null)
    					{
    						activityMap.put(PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY, 
    						                activity.getDueDate().getTime());
    					}
    				}
			    }
			}
			
			activityJsonResult = new ObjectMapper().writeValueAsString(activityMap);
		}
		else
		{
			throw new Exception("activity json cannot be empty while reading activity runtime info.");
		}
		
		return activityJsonResult;
	}
	
	private static PBAttachments populateAttachmentIndex(WikiAttachmentVO wikiAttachment, String incidentId, String username, Boolean isMalicious, String description, String source, String sourceValue) throws Exception
	{
		PBAttachments attachment = new PBAttachments(wikiAttachment.getSys_id(), null, username, isMalicious, description);
		
		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
		if (incidentVO == null)
		{
		    throw new Exception(String.format("No incident found with the given incident id: %s",incidentId));
		}
		
		attachment.setName(wikiAttachment.getUFilename());
		attachment.setSize(wikiAttachment.getUSize());
		attachment.setIncidentId(incidentId);
		attachment.setWorksheetId(incidentVO.getProblemId());
		attachment.setSource(source);
		attachment.setSourceValue(sourceValue);
		String sourceAndValue = (StringUtils.isNotBlank(source)? source + " " : "None") + 
		                (StringUtils.isBlank(source) ? "" : (StringUtils.isNotBlank(sourceValue)? sourceValue.replace(".", " ") : "None"));
		attachment.setSourceAndValue(sourceAndValue);
		
		String auditMessage = String.format("User '%s' added a new attachment '%s'", username, attachment.getName());
		
		StringBuilder sb = new StringBuilder(auditMessage);
		
		if (isMalicious != null)
		{
		    sb.append(" with is malicious set to ").append(isMalicious.booleanValue());
		}
		
		if (StringUtils.isNotBlank(description))
		{
		    if (isMalicious != null)
		    {
		        sb.append(" and ");
		    }
		    else
		    {
		        sb.append(" with ");
		    }
		    
		    sb.append(" description set to ").append(description);
		}
		
		attachment.setAuditMessage(auditMessage);
		
		return attachment;
	}
	
	private static Integer getTemplateVersion(String templateName, String username) throws Exception
	{
		WikiDocumentVO wikiVO = WikiUtils.getWikiDoc(null, templateName, username);
		if (wikiVO == null)
		{
			throw new Exception("Could not find a security incident template: " + templateName);
		}
		
		return wikiVO.getUVersion();
	}
	
	@SuppressWarnings("rawtypes")
    private static ResolveSecurityIncidentVO startIncident(String incidentId, String username) throws Exception
	{
		String hql = "update ResolveSecurityIncident set sirStarted = :sirStarted where sys_id = :sysId";
		
		if (StringUtils.isBlank(incidentId))
		{
			throw new Exception ("Either incidentId or SIR is needed to close the incident.");
		}
		
		try
		{
    HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(hql);
	
				query.setParameter("sirStarted", Boolean.TRUE);
				query.setParameter("sysId", incidentId);
				
				query.executeUpdate();
			});
			
		}
		catch(Exception e)
		{
			Log.log.error("Error in executing HQL: " + hql, e);
            HibernateUtil.rethrowNestedTransaction(e);
		}
		
		return getSecurityIncident(incidentId, null, username);
	}
	
	@SuppressWarnings("unchecked")
    private static Map<String, Object> getTemplateDescAndSla(String templateName, Integer version, boolean isNew) throws Exception
	{
	    // this Map will contain desc and SLA from template.
	    Map<String, Object> templateInfoMap = new HashMap<String, Object>();
		
		if (StringUtils.isNotBlank(templateName))
		{
			String content = null;
			List<Object> objList = WikiUtils.getWikiContentList(templateName, version);
			if (objList != null)
			{
				for (Object obj : objList)
	    		{
	    			Object[] objArray = (Object[])obj;
	    			content = (String)objArray[1];
	    		}
			}
			
			if (StringUtils.isNotBlank(content))
			{
				String[] sections = StringUtils.substringsBetween(content, "{section:", "{section}");
				
				if (sections.length > 0)
				{
				    String existingDesc = null;
				    
					for (String section : sections)
					{
						if (section.contains("wysiwyg") || section.contains("image"))
						{
							//description = description + "{section:" + section + "{section}";
						    if (templateInfoMap.containsKey("DESCRIPTION"))
						    {
						        existingDesc = (String)templateInfoMap.remove("DESCRIPTION");
						    }
						    
						    templateInfoMap.put("DESCRIPTION", 
						                        (StringUtils.isNotBlank(existingDesc) ? existingDesc : "") + 
						                        "{section:" + section + "{section}");
						}
						
						// get SLA seconds only if it's a new incident
						if (isNew && section.contains("sla|title"))
                        {
						    String slaJson = "{" + StringUtils.substringBetween(section, "{", "}") + "}";
						    Map<String, String> slaMap = new ObjectMapper().readValue(slaJson, Map.class);
						    
						    Long days = Long.valueOf(slaMap.get("days"));
						    Long hours = Long.valueOf(slaMap.get("hours"));
						    Long minutes = Long.valueOf(slaMap.get("minutes"));
						    long seconds = TimeUnit.DAYS.toSeconds(days) + TimeUnit.HOURS.toSeconds(hours) + 
						                   TimeUnit.MINUTES.toSeconds(minutes);
						    
                            templateInfoMap.put("SLA", seconds);
                        }
					}
				}
			}
		}
		
		return templateInfoMap;
	}
	
	@Deprecated
	@SuppressWarnings({"unchecked" })
	private static void updateActivityIndex(String activityJson, ResolveSecurityIncidentVO incidentVO, String username) throws Exception
	{
		Map <String, Object> activityMap = new ObjectMapper().readValue(activityJson, Map.class);
		
		if (activityMap != null)
		{
			String activityId = (String)activityMap.get("altActivityId");
			
			ResponseDTO<PBActivity> activityResDTO = PlaybookSearchAPI.searchPBActivity(activityId, incidentVO.getSys_id(), username);
			PBActivity pbActivity = null;
			
			if (activityResDTO != null && activityResDTO.getRecords() != null && activityResDTO.getRecords().size() > 0)
			{
				pbActivity = activityResDTO.getRecords().get(0);
			}
			else
			{
				pbActivity = new PBActivity(activityId, null, username);
			}
			
			pbActivity.setAssignee((String)activityMap.get("assignee"));
			pbActivity.setStatus((String)activityMap.get("status"));
			Long longDate = getDueDate(activityMap);
			if (longDate != 0)
			{
				pbActivity.setDueDate(new Date(longDate));
			}
			pbActivity.setWorksheetId(incidentVO.getProblemId());
			pbActivity.setIncidentId(incidentVO.getSys_id());
			pbActivity.setAltActivityId(activityId);
			pbActivity.setActivityId(activityId);
			pbActivity.setModified(true);
			
			PlaybookIndexAPI.indexPBActivity(pbActivity, username);
		}
	}
	
	@SuppressWarnings("unchecked")
	private static ActivityVO populateActivityVO (String activityJson, int activityIndex, String username) throws Exception
	{
		ActivityVO vo = new ActivityVO();
		
		if (StringUtils.isNotBlank(activityJson))
		{
			Map<String, Object> activityMap = new ObjectMapper().readValue(activityJson,  Map.class);
			
			if (activityMap != null && activityMap.size() > 0)
			{
				Set<String> keys = activityMap.keySet();
				
				for (String key : keys)
				{
					if (activityMap.get(key) != null)
					{
						BeanUtil.setProperty(vo, key, activityMap.get(key));
					}
				}
			}
		}
		
		vo.setId(vo.getAltActivityId());
		vo.setPosition(Integer.valueOf(activityIndex));
		return vo;
	}
	
	private static Long getDueDate(Map<String, Object> activityMap) throws Exception
	{
		Long days = 0L;
		Long hours = 0L;
		Long minutes = 0L;
		
		Object sla = activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY);
		if (sla != null && StringUtils.isNotBlank(sla.toString()))
		{
		    try
		    {
		        days = Long.parseLong(sla.toString(), 10);
		    }
		    catch (NumberFormatException nfe)
		    {
		        Log.log.warn("Error " + nfe.getLocalizedMessage() + " occurred while converting specified SLA " +
		                     WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY + " value " + sla.toString() +
		                     " to " + Long.class.getName() + ".", nfe);
		    }
		}
		
		sla = activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY);
		if (sla != null && StringUtils.isNotBlank(sla.toString()))
		{
		    try
            {
                hours = Long.parseLong(sla.toString(), 10);
            }
            catch (NumberFormatException nfe)
            {
                Log.log.warn("Error " + nfe.getLocalizedMessage() + " occurred while converting specified SLA " +
                             WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY + " value " + sla.toString() +
                             " to " + Long.class.getName() + ".", nfe);
            }
		}
		
		sla = activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY);
		if (sla != null && StringUtils.isNotBlank(sla.toString()))
		{
		    try
            {
                minutes = Long.parseLong(sla.toString(), 10);
            }
            catch (NumberFormatException nfe)
            {
                Log.log.warn("Error " + nfe.getLocalizedMessage() + " occurred while converting specified SLA " +
                             WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY + " value " + sla.toString() +
                             " to " + Long.class.getName() + ".", nfe);
            }
		}
		
		Long totalTime = 0L;
		if (days != 0)
		{
			totalTime = TimeUnit.MILLISECONDS.convert(days, TimeUnit.DAYS);
		}
		
		if (hours != 0 && totalTime < Long.MAX_VALUE)
		{
		    if (Long.MAX_VALUE - totalTime >= TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS))
		    {
		        totalTime += TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS);
		    }
		    else
		    {
		        totalTime = Long.MAX_VALUE;
		    }
		}
		
		if (minutes != 0 && totalTime < Long.MAX_VALUE)
		{
		    if (Long.MAX_VALUE - totalTime >= TimeUnit.MILLISECONDS.convert(minutes, TimeUnit.MINUTES))
            {
		        totalTime += TimeUnit.MILLISECONDS.convert(minutes, TimeUnit.MINUTES);
            }
		    else
            {
                totalTime = Long.MAX_VALUE;
            }
		}
		
		if (totalTime != 0 && totalTime < Long.MAX_VALUE)
		{
		    long currTimeInMillis = DateUtils.GetUTCDateLong();
		    
		    if (Long.MAX_VALUE - totalTime >= currTimeInMillis)
		    {
		        totalTime += currTimeInMillis;
		    }
		    else
		    {
		        totalTime = Long.MAX_VALUE;
		    }
		}
		
		return totalTime;
	}
	
	private static String getOrgHierarchy(String orgId, String separator, String username) throws Exception {
	    String orgHierarchy = "";
	    boolean hasNoOrgAcess = false;
	    Collection<String> orgIdList = new ArrayList<String>();
	    
	    if (StringUtils.isNotBlank(orgId))
	    {
	        if (!orgId.equalsIgnoreCase(VO.STRING_DEFAULT) &&
	            !orgId.equalsIgnoreCase(Constants.NIL_STRING))
	        {
        	    Set<OrgsVO> hierarchyOrgVOs = UserUtils.getAllOrgsInHierarchy(orgId, null);
                if (hierarchyOrgVOs != null && !hierarchyOrgVOs.isEmpty())
                {
                    for (OrgsVO hierarchyOrgVO : hierarchyOrgVOs)
                    {
                        if (hierarchyOrgVO.getUHasNoOrgAccess().booleanValue())
                        {
                            hasNoOrgAcess = true;
                        }
                        orgIdList.add(hierarchyOrgVO.getSys_id());
                    }
                }
	        }
            else
            {
                if (UserUtils.isNoOrgAccessible(username))
                {
	                orgHierarchy = "nil";
                }
            }
	        
            if (!orgIdList.isEmpty())
            {
                orgHierarchy = StringUtils.collectionToString(orgIdList, separator);
                if (StringUtils.isNotBlank(orgHierarchy) && hasNoOrgAcess)
                {
                    orgHierarchy += (separator + " nil ");
                }
            } else if (orgId != null && (orgId.trim().equalsIgnoreCase(Constants.NIL_STRING) ||
                                         orgId.trim().equalsIgnoreCase(VO.STRING_DEFAULT))) {
                if (hasNoOrgAcess)
                {
                    orgHierarchy = Constants.NIL_STRING;
                }
            }
	    }
	    else
	    {
	        if (UserUtils.isNoOrgAccessible(username))
	        {
	            orgHierarchy = "nil";
	        }
	    }
	    
	    if (StringUtils.isBlank(orgHierarchy))
	    {
	        throw new Exception("User does not have access to any Orgs in the system.");
	    }
	    
	    return orgHierarchy;
	}

	public static String buildUserOnlyVisibilityCondition(String username) {
	    boolean sirViewAllPermissions = UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL, null);
	    if (sirViewAllPermissions == false) { // admin user should see all SIRs
	       return String.format("(u_owner = '%s' OR u_members like '%s' OR u_members like '%s,%%' OR u_members like ' %s,%%' OR u_members like '%%,%s' OR u_members like '%%, %s' OR u_members like '%%,%s,%%' OR u_members like '%%, %s,%%')",
                           username, username, username, username, username, username, username, username);
	    }
	    return "";
	}
	
	private static String buildOrgWhereClauseHierarchy(String orgId, String username)  {
	       String orgHierarchy = "";
	       String noOrgFilter = "sys_org is NULL OR sys_org = ''";
	        boolean hasNoOrgAcess = false;
	        
	        if (StringUtils.isNotBlank(orgId))
	        {
	            if (!orgId.equalsIgnoreCase(VO.STRING_DEFAULT) &&
	                !orgId.equalsIgnoreCase(Constants.NIL_STRING))
	            {
        	        Set<OrgsVO> hierarchyOrgVOs = UserUtils.getAllOrgsInHierarchy(orgId, null);
        	        Iterator<OrgsVO> it = hierarchyOrgVOs.iterator();
        	        while (it.hasNext()) {
        	            OrgsVO hierarchyOrgVO = it.next();
        	            orgHierarchy += (orgHierarchy.isEmpty()? "": " OR") + " sys_org = '" + hierarchyOrgVO.getSys_id() + "'";
                        if (hierarchyOrgVO.getUHasNoOrgAccess().booleanValue())
                        {
                            hasNoOrgAcess = true;
                        }
        	        }
        	        if (!orgHierarchy.isEmpty()) {
        	            if (hasNoOrgAcess) {
        	                orgHierarchy += " OR " + noOrgFilter;
        	            }
        	            orgHierarchy = " AND (" + orgHierarchy + ")";
        	        } else if (orgId != null && orgId.trim().equals("nil")) {
        	            orgHierarchy = " AND ("+noOrgFilter+")";
        	        }
	            }
	            else
	            {
	                if (UserUtils.isNoOrgAccessible(username))
	                {
	                    orgHierarchy = " AND (" + noOrgFilter + ")";
	                }
	            }
	        }
	        else
	        {
	            if (UserUtils.isNoOrgAccessible(username))
	            {
	                orgHierarchy = " AND (" + noOrgFilter + ")";
	            }
	        }
	        
	        return orgHierarchy;
	} 
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<Map<String, Object>> getSIRCountReport(String username, Integer scopeUnits, Integer slaAtRiskUnits, TemporalUnit tmprlUnit, String orgId) throws Exception
	{
	    ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
	    Map<String, Object> countsMap = new HashMap<String, Object>();
	    
	    if (StringUtils.isNotBlank(orgId))
	    {
	        if (!orgId.equalsIgnoreCase(VO.STRING_DEFAULT) &&
	            !orgId.equalsIgnoreCase(Constants.NIL_STRING))
	        {
	            if (!UserUtils.isOrgAccessible(orgId, username, false, false))
	            {
	                throw new Exception ("User does not have permission to access selected Org.");
	            }
	        }
	        else
	        {
	            if (!UserUtils.isNoOrgAccessible(username))
	            {
	                throw new Exception ("User does not have permission to access No Org 'None'.");
	            }
	        }
	    }
	    else
	    {
	        if (!UserUtils.isNoOrgAccessible(username))
            {
                throw new Exception ("User does not have permission to access No Org 'None'.");
            }
	    }
	    
	    String orgWhereCondidtion = buildOrgWhereClauseHierarchy(orgId, username);
	    
	    if (StringUtils.isBlank(orgWhereCondidtion))
	    {
	        throw new Exception ("User does not have access to any Org in the system.");
	    }
	    
	    QueryDTO countByStateQuery = new QueryDTO();

	    countByStateQuery.setHql("select status, count(sys_id) from ResolveSecurityIncident where sysIsDeleted is false and (masterSir is null OR masterSir = '') and sysCreatedOn > :sysCreatedAfter " + orgWhereCondidtion + " group by status");
	    
	    QueryDTO countPastDueQuery = new QueryDTO();

	    countPastDueQuery.setHql("select count(sys_id) from ResolveSecurityIncident where sysIsDeleted is false and (masterSir is null OR masterSir = '') and sysCreatedOn > :sysCreatedAfter and dueBy is not null and dueBy < :currDate " + orgWhereCondidtion);
	    
	    QueryDTO countSLAAtRiskQuery = new QueryDTO();
	    
//	    countSLAAtRiskQuery.setHql("select count(sys_id) from ResolveSecurityIncident where sysIsDeleted is false and sysCreatedOn > :sysCreatedAfter and dueBy is not null and ( status = 'Open' or status = 'In Progress' ) and dueBy > :currDate and dueBy < :slaAtRiskDeadline " + orgWhereCondidtion + visibilityWhereCondition);
//	    countSLAAtRiskQuery.setHql("select count(sys_id) from ResolveSecurityIncident where sysIsDeleted is false and sysCreatedOn > :sysCreatedAfter and dueBy is not null and status in ('Open','In Progress') and dueBy > :currDate and dueBy < :slaAtRiskDeadline " + orgWhereCondidtion + visibilityWhereCondition);
	    countSLAAtRiskQuery.setHql("select count(sys_id) from ResolveSecurityIncident where sysIsDeleted is false and (masterSir is null OR masterSir = '') and sysCreatedOn > :sysCreatedAfter and dueBy is not null and lower(status) not in ('closed','complete') and dueBy > :currDate and dueBy < :slaAtRiskDeadline " + orgWhereCondidtion);
	    
	    Map<String, Object> countByStateQueryParams = new HashMap<String, Object>();
	    
	    Date currDate = new Date();
	    
	    Instant currInst = currDate.toInstant();
	    
	    countByStateQueryParams.put("sysCreatedAfter", new Date(currInst.minus(scopeUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));
	    	    
	    countByStateQuery.setParams(countByStateQueryParams);
	    
	    Map<String, Object> countPastDueQueryParams = new HashMap<String, Object>(countByStateQueryParams);
	    
	    countPastDueQueryParams.put("currDate", currDate);
	    
	    countPastDueQuery.setParams(countPastDueQueryParams);
	    
	    Map<String, Object> countSLAAtRiskQueryParams = new HashMap<String, Object>(countPastDueQueryParams);
	    
	    countSLAAtRiskQueryParams.put("slaAtRiskDeadline", new Date(currInst.plus(slaAtRiskUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));
	    
	    countSLAAtRiskQuery.setParams(countSLAAtRiskQueryParams);
	    
	    try
	    {
	     
       HibernateProxy.setCurrentUser("system");
	    	HibernateProxy.execute(() -> {
	    		   Query query = HibernateUtil.createQuery(countByStateQuery);
	   	        
	   	        List<Object[]> data = query.list();
	   	        
	   	        if(data != null)
	               {
	                   for (Object[] aRow : data)
	                   {
	                       countsMap.put((String)aRow[0], (Long)aRow[1]);
	                   }
	               }
	   	        
	   	        Query pastDueQuery = HibernateUtil.createQuery(countPastDueQuery);
	   	        
	   	        List<Object[]> pastDueData = pastDueQuery.list();
	   	        
	   	        if(pastDueData != null && !pastDueData.isEmpty())
	               {
	                   countsMap.put("Past Due", pastDueData.get(0));
	               }
	   	        
	   	        Query slaAtRiskQuery = HibernateUtil.createQuery(countSLAAtRiskQuery);
	               
	               List<Object[]> slaAtRiskData = slaAtRiskQuery.list();
	               
	               if(slaAtRiskData != null && !slaAtRiskData.isEmpty())
	               {
	                   countsMap.put("SLA at Risk", slaAtRiskData.get(0));
	               }
	               
	   	        result.setData(countsMap);
	   	        result.setSuccess(true);
	   	        
	    	});
	    }
	    catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting data for hql:" + countByStateQuery.getHql(), e);
            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
	            
        return result;
	}
	
	private static void buildFilterQuery(String filter, String filterType, StringBuilder filterSb, Map<String, String> filterByKeyVals)
	{
	    if (StringUtils.isNotBlank(filter) && !filter.equalsIgnoreCase(FILTER_NONE))
        {
            String[] filters = filter.split(",");
            
            if (filters.length > 0)
            {
                if (StringUtils.isNotBlank(filterSb.toString()))
                {
                    filterSb.append(" and ");
                }
                
                for (int i = 0;i < filters.length; i++)
                {
                    if (i == 0)
                    {
                        filterSb.append("(");
                    }
                    else
                    {
                        filterSb.append("or");
                    }
                    // ReplaceAll will treat "." as regex for any char and will cause issue when two filter names have the same length
                    String key = filters[i].trim();
                    if (key.equals("nil")) {
                        filterSb.append(" ").append(filterType).append(" = '' OR ").append(filterType).append(" is NULL ");
                    } else {
                        key = key.replace(SPACE, UNDER_SCORE).replace(COLON, UNDER_SCORE).replace(PERIOD, UNDER_SCORE);
                        filterSb.append(" ").append(filterType).append(" = :param_").append(key).append(" ");
                        filterByKeyVals.put("param_" + key, filters[i].trim());
                    }
                }
                
                filterSb.append(") ");
            }
        }
	}
	
	private static void setQueryParams(Map<String, String> keyValues, Map<String, Object> queryParams)
	{
	    if (keyValues != null && !keyValues.isEmpty() && queryParams != null)
	    {
	        for (String key : keyValues.keySet())
            {
	            queryParams.put(key, keyValues.get(key));
            }
	    }
	}
	
    /**
     * The function builds filters for a column containing comma separated list of values
     * (i.e., the team members), assuming that the input is a not empty and not "All" value.
     * @param teamFilter   Comma separated list of values that possible values for the column.
     *                     For example "admin, resolve, annonymous"
     * @return hql string filters
     */
	private static String buildteamFilterQuery(String values, String colName) {
	    // Input example "admin, resolve, annonymous"
        String filters = "(";
	    String[] users = values.split(",");
	    for (int i=0; i<users.length; i++) {
	        String user = users[i].trim();
	        filters += String.format(" %s LIKE '%%%%, %s,%%%%' OR %s LIKE '%%%%,%s,%%%%' OR %s LIKE '%%%%%s,%%%%'  OR %s LIKE '%%%%, %s%%%%'  OR %s LIKE '%%%%,%s%%%%'  OR %s='%s' ", 
	          colName, user, colName, user, colName, user, colName, user, colName, user, colName, user) + ((i<users.length-1)? " OR ": "");
	    }
        return filters+")";
    }

    /**
     * 
     * @param numberOfCols       Evenly divided buckets over scope, i.e. 10 1-day buckets on 10 days.
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     * @param typeFilter         Query filters by type, i.e. All|Malware|Phishing    
     * @param severityFilter     Query filters by severity, i.e. Critical|High|Medium|Low                 
     * @param ownerFilter        Query filters by owner, i.e. admin|resolve_user              
     * @param teamFilter         Query filters by team member, i.e. admin|resolve_user
     * @param priorityFilter     Query filters by priority, i.e. low|medium|high              
     * @param statusFilter       Query filters by status, i.e. Open|In Progress|Closed    
     * @param orgId 
     * @return
     * @throws Exception 
     */

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static ResponseDTO<Map<Object, Object>> getSIRCountOverTimeReport(Integer numberOfCols, Integer scopeUnits,
			String unitType, String typeFilter, String severityFilter, String ownerFilter, String teamFilter,
			String priorityFilter, String statusFilter, String orgId, String username, ConfigSQL configSQL)
			throws Exception {
        /**
         * 
         * Helper class to dynamically generate buckets with equal interval as hql 'when case' statements. Example:
         *  WHEN 1493833450  <= UNIX_TIMESTAMP(sys_created_on) AND  UNIX_TIMESTAMP(sys_created_on) < 1494870250 THEN '8'
         *  WHEN 1494870250  <= UNIX_TIMESTAMP(sys_created_on) AND  UNIX_TIMESTAMP(sys_created_on) < 1495907050 THEN '9'
         *
         */
        class BucketsFactory {
            private long curBucket;
            private long nBuckets;
            private long bucketSize;     // in seconds
            private long scope;
            private String unit;
            private int bucketIncrement;
            private TemporalUnit tmprlUnit;
            BucketsFactory(Integer nCols, Integer scopeUnits, String unitType) {
                nBuckets = nCols;
                curBucket = 0;
                unit = unitType.trim().toUpperCase();
                bucketIncrement = scopeUnits/nCols;
                switch(unit) {
                    case "DAYS":
                        tmprlUnit = ChronoUnit.DAYS;
                        bucketSize = bucketIncrement * 86400;
                        break;
                    case "HOURS":
                        bucketSize = bucketIncrement * 3600;
                        tmprlUnit = ChronoUnit.HOURS;
                        break;
                        
                    case "MINUTES":
                        bucketSize = bucketIncrement * 60;
                        tmprlUnit = ChronoUnit.MINUTES;
                        break;
                    default:
                        bucketSize = bucketIncrement * 86400;
                        tmprlUnit = ChronoUnit.DAYS;
                        unit = "DAYS";
                        break;
                }
                scope = new Date().getTime()/1000;  // current time in seconds
                scope -= bucketSize*nBuckets;
            }
            
            private String generateLabel(char sortingChar) {
                String label = "";
                long from = (nBuckets-curBucket) * bucketIncrement;
                switch(unit) {
                    case "DAYS":
                        // sortingChar is a place holder to trick SQL sorting correctly.
                        label = String.format("%c@%d Days - %d Days", sortingChar, from, from+bucketIncrement);
                        break;
                    case "HOURS":
                        label = String.format("%c@%d Hours - %d Hours", sortingChar, from, from+bucketIncrement);
                        break;
                    case "MINUTES":
                        label = String.format("%c@%d Minutes - %d Minutes", sortingChar, from, from+bucketIncrement);
                        break;
                    default:
                        Log.log.error("Unsupported unit ["+unit+"]. Supported units are DAYS, HOURS or MINUTES");
                        break;
                }
                return label;
            }
            
            private long[] generateNextBucketInterval() {
                long [] bucket = {-1, -1};
                bucket[0] = scope + bucketSize * curBucket++;
                bucket[1] = bucket[0] + bucketSize;
                return bucket;
            }
            
            private String getUnixTimeStamp(String timeStampcolumn, ConfigSQL configSQL) {
                String dbType = configSQL.getDbtype().toUpperCase();
                if (dbType.equals("MYSQL")) {
                    // Number of seconds since 01/01/1970
                    return String.format("UNIX_TIMESTAMP(%s)", timeStampcolumn);
                } else {
                    // ORACLE doesn't support UNIX_TIMESTAMP so need to do differently.
                    // The number of seconds since 01/01/1970 is the same as the number of days
                    // since 01/01/1970 mutliplying by number of seconds in a day (24*60*60 = 86400)
                    return String.format("(cast(%s as date) - TO_DATE('01-01-1970', 'DD-MM-YYYY'))*86400", timeStampcolumn);
                }
            }
            
            private String generateNextBucketWhenSql(char sortingChar, ConfigSQL configSQL) {
               long [] bucketInterval = generateNextBucketInterval();
               String unixTimeStamp = getUnixTimeStamp("sys_created_on", configSQL);
               return String.format("WHEN %d <= %s AND %s < %d THEN '%s'",
                               bucketInterval[0], unixTimeStamp, unixTimeStamp, bucketInterval[1], generateLabel(sortingChar)); 
            }
            
            private boolean hasNext() {
                return (curBucket <= nBuckets-1);
            }

            public String generateCaseWhenSqlStmt(final ConfigSQL configSQL) {
                String stmt = "";
                char sortingChar = 'A';
                while(hasNext()) {
                    stmt += generateNextBucketWhenSql(sortingChar++, configSQL) + " ";;
                }
                return stmt;
            }
            public TemporalUnit getTmprlUnit()
            {
                return tmprlUnit;
            }
        };

        BucketsFactory buckets = new BucketsFactory(numberOfCols, scopeUnits, unitType);
        String whenCaseSqlStmt = buckets.generateCaseWhenSqlStmt(configSQL);
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object, Object>>();
        List<Map<Object, Object>> records = new ArrayList<Map<Object, Object>>();
        String teamFilterStr = "";
        if (!teamFilter.trim().toUpperCase().equals("ALL")) {
            teamFilterStr = buildteamFilterQuery(teamFilter, "u_members") + " and ";
        }
        QueryDTO countNewSirOverTimeQuery = new QueryDTO();
        String sqlQueryPrefix = "SELECT CASE " + whenCaseSqlStmt 
                        + " END as buckets, COUNT(sys_created_on) as value from resolve_security_incident where " 
                        + teamFilterStr + " sys_is_deleted != 'Y' and sys_created_on >= :sysCreatedAfter and (u_master_sir is null OR u_master_sir = '') ";
        String sqlQuerySuffix = "GROUP BY case " + whenCaseSqlStmt + " END ORDER BY buckets DESC";

        
        StringBuilder filterSb = new StringBuilder();
        
        Map<String, String> filterByTypes = new HashMap<String, String>();
        buildFilterQuery(typeFilter, "u_investigation_type", filterSb, filterByTypes);
        
        Map<String, String> filterBySeverities = new HashMap<String, String>();
        buildFilterQuery(severityFilter, "u_severity", filterSb, filterBySeverities);
        
        Map<String, String> filterByOwners = new HashMap<String, String>();
        buildFilterQuery(ownerFilter, "u_owner", filterSb, filterByOwners);
        
        Map<String, String> filterByPriority = new HashMap<String, String>();
        buildFilterQuery(priorityFilter, "u_priority", filterSb, filterByPriority);
        
        Map<String, String> filterByStatuses = new HashMap<String, String>();
        buildFilterQuery(statusFilter, "u_status", filterSb, filterByStatuses);

        Map<String, String> filterByOrgs = new HashMap<String, String>();
        String orgFilters = getOrgHierarchy(orgId, ",", username);
        buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);
        
        String countByQuerySql = sqlQueryPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + sqlQuerySuffix;
        countNewSirOverTimeQuery.setSqlQuery(countByQuerySql);
        countNewSirOverTimeQuery.setUseSql(true);

        Map<String, Object> countNewSirOverTimeQueryParams = new HashMap<String, Object>();

        setQueryParams(filterByTypes, countNewSirOverTimeQueryParams);
        setQueryParams(filterBySeverities, countNewSirOverTimeQueryParams);
        setQueryParams(filterByOwners, countNewSirOverTimeQueryParams);
        setQueryParams(filterByPriority, countNewSirOverTimeQueryParams);
        setQueryParams(filterByStatuses, countNewSirOverTimeQueryParams);
        setQueryParams(filterByOrgs, countNewSirOverTimeQueryParams);
        Date currDate = new Date();
        Instant currInst = currDate.toInstant();
        TemporalUnit tmprlUnit = buckets.getTmprlUnit();
        countNewSirOverTimeQueryParams.put("sysCreatedAfter", new Date(currInst.minus(scopeUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));
        countNewSirOverTimeQuery.setParams(countNewSirOverTimeQueryParams);
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                Query query = HibernateUtil.createSQLQuery(countNewSirOverTimeQuery.getSelectSQLNonParameterizedSort());
                for (String key : countNewSirOverTimeQueryParams.keySet())
                {
                    query.setParameter(key, countNewSirOverTimeQueryParams.get(key));
                }
                List<Object[]> data = query.list();
                String key;
                if(data != null)
                {
                    HashMap<Object, Object> record = new LinkedHashMap<Object, Object>();
                    for (Object[] aRow : data)
                    {
                        String dbType = configSQL.getDbtype().toUpperCase();
                        if (aRow[0] != null) {
                            if (dbType.equals("MYSQL")) {
                                key = ((String)aRow[0]).split("@")[1];
                                record.put(key, (BigInteger)aRow[1]);
                            } else {
                                if (numberOfCols == 1) {
                                    // Oracle has issue with 1 column query so this is a quick fix for 6.1
                                    record.put("0 Days - " + scopeUnits + " " + unitType, (BigDecimal)aRow[1]);
                                } else {
                                    key = ((String)aRow[0]).split("@")[1];
                                    record.put(key, (BigDecimal)aRow[1]);
                                }
                            }
                        }
                    }
                    records.add(record);
                    result.setTotal(data.size());
                }
                result.setRecords(records);
                result.setSuccess(true);

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting data for hql:" + countNewSirOverTimeQuery.getHql(), e);

            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ResponseDTO<Map<String, Object>> getSIRCountByColumnReport(String col, Integer scopeUnits,
			TemporalUnit tmprlUnit, String typeFilter, String severityFilter, String ownerFilter, String teamFilter,
			String priorityFilter, String statusFilter, String orgId, String username, ConfigSQL configSQL) throws Exception
    {
	    ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
	    List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        QueryDTO countByTypeQuery = new QueryDTO();
        String teamFilterStr = "";
        if (!teamFilter.trim().toUpperCase().equals("ALL")) {
            teamFilterStr = buildteamFilterQuery(teamFilter, "u_members") + " and ";
        }

        String hqlQueryPrefix = String.format("select %s, count(sys_id) from resolve_security_incident where %s %s is not null and sys_is_deleted != 'Y' and sys_created_on > :sysCreatedAfter and (u_master_sir is null OR u_master_sir = '') ",
                        col, teamFilterStr ,col);
        String hqlQuerySuffix = String.format("group by %s order by count(sys_id) desc", col);
        
        StringBuilder filterSb = new StringBuilder();
        
        Map<String, String> filterByTypes = new HashMap<String, String>();
        
        buildFilterQuery(typeFilter, "u_investigation_type", filterSb, filterByTypes);
        
        Map<String, String> filterBySeverities = new HashMap<String, String>();
        
        buildFilterQuery(severityFilter, "u_severity", filterSb, filterBySeverities);
        
        Map<String, String> filterByOwners = new HashMap<String, String>();
        
        buildFilterQuery(ownerFilter, "u_owner", filterSb, filterByOwners);

        Map<String, String> filterByStatuses = new HashMap<String, String>();
        
        buildFilterQuery(statusFilter, "u_status", filterSb, filterByStatuses);
        
        Map<String, String> filterByPriority = new HashMap<String, String>();
        buildFilterQuery(priorityFilter, "u_priority", filterSb, filterByPriority);

        Map<String, String> filterByOrgs = new HashMap<String, String>();
        String orgFilters = getOrgHierarchy(orgId, ",", username);
        buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);

        String countByQueryHql = hqlQueryPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + hqlQuerySuffix;
        
        countByTypeQuery.setSqlQuery( countByQueryHql );// setHql(countByQueryHql);
        countByTypeQuery.setUseSql(true);
        
        Map<String, Object> countByTypeQueryParams = new HashMap<String, Object>();
        
        Date currDate = new Date();
        
        Instant currInst = currDate.toInstant();
        
        countByTypeQueryParams.put("sysCreatedAfter", new Date(currInst.minus(scopeUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));
        
        setQueryParams(filterByTypes, countByTypeQueryParams);
        setQueryParams(filterBySeverities, countByTypeQueryParams);
        setQueryParams(filterByOwners, countByTypeQueryParams);
        setQueryParams(filterByStatuses, countByTypeQueryParams);
        setQueryParams(filterByPriority, countByTypeQueryParams);
        setQueryParams(filterByOrgs, countByTypeQueryParams);
        
        countByTypeQuery.setParams(countByTypeQueryParams);
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                Query query = HibernateUtil.createSQLQuery(countByTypeQuery.getSelectSQLNonParameterizedSort());

                for (String key : countByTypeQueryParams.keySet())
                {
                    query.setParameter(key, countByTypeQueryParams.get(key));
                }

                List<Object[]> data = query.list();
                
                if(data != null)
                {
                    for (Object[] aRow : data)
                    {
                        HashMap<String, Object> record = new HashMap<String, Object>();
                        String dbType = configSQL.getDbtype().toUpperCase();
                        if (dbType.equals("MYSQL")) {
                            record.put((String)aRow[0], (BigInteger)aRow[1]);
                        } else {
                            record.put((String)aRow[0], (BigDecimal)aRow[1]);
                        }
                        
                        records.add(record);
                    }
                    result.setTotal(data.size());
                }
                
                result.setRecords(records);
                result.setSuccess(true);
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting data for hql:" + countByTypeQuery.getHql(), e);
            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<Map<String, Object>> getSIRCountBySeverityReport(Integer scopeUnits, TemporalUnit tmprlUnit, String uppercaseDBType, String typeFilter, 
                                                                               String severityFilter, String ownerFilter, String teamFilter, String statusFilter, String priorityFilter,
                                                                               String orgId, String username) throws Exception
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        
        QueryDTO countBySeverityQuery = new QueryDTO();

        String teamFilterStr = "";
        if (!teamFilter.trim().toUpperCase().equals("ALL")) {
            teamFilterStr = buildteamFilterQuery(teamFilter, "u_members") + " and ";
        }
        //countBySeverityQuery.setHql("select severity, count(sys_id) from ResolveSecurityIncident where sysCreatedOn > :sysCreatedAfter group by severity");
        
        String sqlQueryPrefix = "select        t.u_severity, t.severity_indx, count(t.sys_id) " +
                                "from          (select      u_severity, " +
                                "                           case    u_severity " +
                                "                                   when 'Critical' then 1 " +
                                "                                   when 'High' then 2 " + 
                                "                                   when 'Medium' then 3 " +
                                "                                   when 'Low' then 4 " +
                                "                                   else 100 " +
                                "                           end as severity_indx, " +
                                "                           sys_id " +
                                "               from        resolve_security_incident " +
                                "               where   " + teamFilterStr + "    u_severity is not null " +
                                "               and         sys_is_deleted != 'Y' " +
                                "               and         sys_created_on >= :sysCreatedAfter " +
                                "               and         (u_master_sir is null OR u_master_sir = '') ";
        
        String sqlQuerySuffix = "                                                              ) t " +
                                "group by       t.u_severity, t.severity_indx " +
                                "order by       t.severity_indx asc";
        
        StringBuilder filterSb = new StringBuilder();
        
        Map<String, String> filterByTypes = new HashMap<String, String>();
        
        buildFilterQuery(typeFilter, "u_investigation_type", filterSb, filterByTypes);
        
        Map<String, String> filterBySeverities = new HashMap<String, String>();
        
        buildFilterQuery(severityFilter, "u_severity", filterSb, filterBySeverities);
        
        Map<String, String> filterByOwners = new HashMap<String, String>();
        
        buildFilterQuery(ownerFilter, "u_owner", filterSb, filterByOwners);
        
        Map<String, String> filterByStatuses = new HashMap<String, String>();
        
        buildFilterQuery(statusFilter, "u_status", filterSb, filterByStatuses);
        
        Map<String, String> filterByPriority = new HashMap<String, String>();
        buildFilterQuery(priorityFilter, "u_priority", filterSb, filterByPriority);

        Map<String, String> filterByOrgs = new HashMap<String, String>();
        String orgFilters = getOrgHierarchy(orgId, ",", username);
        buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);
        
        String countBySeveritySql = sqlQueryPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + sqlQuerySuffix;
        
        countBySeverityQuery.setSqlQuery(countBySeveritySql);
        countBySeverityQuery.setUseSql(true);
        
        Map<String, Object> countBySeverityQueryParams = new HashMap<String, Object>();
        
        Date currDate = new Date();
        
        Instant currInst = currDate.toInstant();
        
        countBySeverityQueryParams.put("sysCreatedAfter", new Date(currInst.minus(scopeUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));
        
        setQueryParams(filterByTypes, countBySeverityQueryParams);
        setQueryParams(filterBySeverities, countBySeverityQueryParams);
        setQueryParams(filterByOwners, countBySeverityQueryParams);
        setQueryParams(filterByStatuses, countBySeverityQueryParams);
        setQueryParams(filterByPriority, countBySeverityQueryParams);
        setQueryParams(filterByOrgs, countBySeverityQueryParams);
        
        countBySeverityQuery.setParams(countBySeverityQueryParams);
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
        		   //Query query = HibernateUtil.createQuery(countBySeverityQuery);
                
                Query query = HibernateUtil.createSQLQuery(countBySeverityQuery.getSelectSQLNonParameterizedSort());
                
                for (String key : countBySeverityQueryParams.keySet())
                {
                    query.setParameter(key, countBySeverityQueryParams.get(key));
                }
                
                List<Object[]> data = query.list();
                
                if(data != null)
                {
                    for (Object[] aRow : data)
                    {
                        HashMap<String, Object> record = new HashMap<String, Object>();
                        
                        //record.put((String)aRow[0], (Long)aRow[1]);
                        
                        if (uppercaseDBType.equals("MYSQL"))
                        {
                            record.put((String)aRow[0], (BigInteger)aRow[2]);
                        }
                        else
                        {
                            record.put((String)aRow[0], (BigDecimal)aRow[2]);
                        }
                        
                        records.add(record);
                    }
                    result.setTotal(data.size());
                }
                
                result.setRecords(records);
                result.setSuccess(true);
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting data for sql:" + countBySeverityQuery.getSelectSQLNonParameterizedSort(), e);
            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<Map<String, Object>> getSIRCountBySLAReport(Integer scopeUnits, TemporalUnit tmprlUnit, String uppercaseDBType, String typeFilter, 
                    String severityFilter, String ownerFilter, String teamFilter, String priorityFilter, String statusFilter, Integer nSecondsBAtRisk, String orgId, String username) throws Exception
	{
	    ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
	    String orderedStatus[] = {"Good","At Risk","Past Due", "No SLA"};
	    String dateDiffSQL = null;
	    if (uppercaseDBType.equals("MYSQL"))
	    {
	        dateDiffSQL = "timestampdiff(SECOND, sys_created_on , now())";
	    }
	    else
	    {
	        dateDiffSQL = "(EXTRACT(DAY FROM (CURRENT_TIMESTAMP - sys_created_on))*86400)";
	    }
	    List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        
        QueryDTO countBySeverityQuery = new QueryDTO();

        String teamFilterStr = "";
        if (!teamFilter.trim().toUpperCase().equals("ALL")) {
            teamFilterStr = buildteamFilterQuery(teamFilter, "u_members") + " and ";
        }
        
        String sqlQueryPrefix = "select t.status, count(t.sys_id) " +
                                "from ( select sys_id, " +
                                "                           case            " +
                                "                                   when (u_sla = 0 OR u_sla IS NULL) then 'No SLA'" +
                                "                                   when (u_sla - " + dateDiffSQL + " <= 0) then 'Past Due'" + 
                                "                                   when (u_sla - " + dateDiffSQL + " <= " + nSecondsBAtRisk + ") then 'At Risk'" + 
                                "                                   when (u_sla - " + dateDiffSQL + " > " + nSecondsBAtRisk + ") then 'Good'" + 
                                "                                   else 'OK' " +
                                "                           end as status " +
                                "               from        resolve_security_incident " +
                                "               where   " + teamFilterStr + "    u_severity is not null " +
                                "               and         sys_is_deleted != 'Y' " +
                                "               and         sys_created_on >= :sysCreatedAfter " +
                                "               and         (u_master_sir is null OR u_master_sir = '') ";
        
        String sqlQuerySuffix = "                                                              ) t " +
                                "group by       t.status ";
        
        StringBuilder filterSb = new StringBuilder();
        
        Map<String, String> filterByTypes = new HashMap<String, String>();
        
        buildFilterQuery(typeFilter, "u_investigation_type", filterSb, filterByTypes);
        
        Map<String, String> filterBySeverities = new HashMap<String, String>();
        
        buildFilterQuery(severityFilter, "u_severity", filterSb, filterBySeverities);
        
        Map<String, String> filterByOwners = new HashMap<String, String>();
        
        buildFilterQuery(ownerFilter, "u_owner", filterSb, filterByOwners);
        
        Map<String, String> filterByPriority = new HashMap<String, String>();
        buildFilterQuery(priorityFilter, "u_priority", filterSb, filterByPriority);
        
        Map<String, String> filterByStatuses = new HashMap<String, String>();
        
        buildFilterQuery(statusFilter, "u_status", filterSb, filterByStatuses);

        Map<String, String> filterByOrgs = new HashMap<String, String>();
        String orgFilters = getOrgHierarchy(orgId, ",", username);
        buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);
        
        String countBySeveritySql = sqlQueryPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + sqlQuerySuffix;
        
        countBySeverityQuery.setSqlQuery(countBySeveritySql);
        countBySeverityQuery.setUseSql(true);
        
        Map<String, Object> countBySeverityQueryParams = new HashMap<String, Object>();
        
        Date currDate = new Date();
        
        Instant currInst = currDate.toInstant();
        
        countBySeverityQueryParams.put("sysCreatedAfter", new Date(currInst.minus(scopeUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));
        
        setQueryParams(filterByTypes, countBySeverityQueryParams);
        setQueryParams(filterBySeverities, countBySeverityQueryParams);
        setQueryParams(filterByOwners, countBySeverityQueryParams);
        setQueryParams(filterByPriority, countBySeverityQueryParams);
        setQueryParams(filterByStatuses, countBySeverityQueryParams);
        setQueryParams(filterByOrgs, countBySeverityQueryParams);
        
        countBySeverityQuery.setParams(countBySeverityQueryParams);
        
        try
        {
         
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
        		   Query query = HibernateUtil.createSQLQuery(countBySeverityQuery.getSelectSQLNonParameterizedSort());
                   
                   for (String key : countBySeverityQueryParams.keySet())
                   {
                       query.setParameter(key, countBySeverityQueryParams.get(key));
                   }
                   
                   List<Object[]> data = query.list();
                   if(data != null)
                   {
                       HashMap<String, Object> record = new HashMap<String, Object>();
                       for (Object[] aRow : data)
                       {
                           if (uppercaseDBType.equals("MYSQL"))
                           {
                               record.put((String)aRow[0], (BigInteger)aRow[1]);
                           }
                           else
                           {
                               record.put((String)aRow[0], (BigDecimal)aRow[1]);
                           }
                       }
                       Map<String, Object> linkedMap = new LinkedHashMap<String, Object>();
                       for (String status : orderedStatus)
                       {
                           if (record.get(status) != null)
                           {
                               linkedMap.put(status, record.get(status));
                           }
                       }
                       records.add(linkedMap);
                       result.setTotal(data.size());
                   }
                   
                   result.setRecords(records);
                   result.setSuccess(true);
                   
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting report data for SIR count by SLA. sql:" + countBySeverityQuery.getSelectSQLNonParameterizedSort(), e);

            
            result.setSuccess(false).setMessage("Error in getting report data for SIR count by SLA. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
	    
	    return result;
	}
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<Map<BigDecimal, Map<String, BigDecimal>>> getMeanTimeSIRReport(
            SIRMeanTimeReportType mtReportType, Integer lastBucketLowerBoundary, 
            MeanTimeFilterContainer filterContainer, ConfigSQL configSQL) throws Exception
    {
        SIRMeanTimeReportGroupType visualizationGroupType = SIRMeanTimeReportGroupType.valueOf(filterContainer.getVisualizedFilter());
        QueryDTO meanTimeAvgQuery = new QueryDTO();
        QueryDTO meanTimeAvgPastThresholdQuery = new QueryDTO();
        
        Map<String, String> filterByTypes = new HashMap<String, String>();
        Map<String, String> filterBySeverities = new HashMap<String, String>();
        Map<String, String> filterByOwners = new HashMap<String, String>();
        Map<String, String> filterByOrgs = new HashMap<String, String>();
        Map<String, String> filterByStatus = new HashMap<String, String>();
        Map<String, String> filterByPriority = new HashMap<String, String>();
        
        StringBuilder filterSb = new StringBuilder();
        buildFilterQuery(filterContainer.getTypeFilter(), "u_investigation_type", filterSb, filterByTypes);
        buildFilterQuery(filterContainer.getSeverityFilter(), "u_severity", filterSb, filterBySeverities);
        buildFilterQuery(filterContainer.getOwnerFilter(), "u_owner", filterSb, filterByOwners);
        buildFilterQuery(filterContainer.getStatusFilter(), "u_status", filterSb, filterByStatus);
        buildFilterQuery(filterContainer.getPriorityFilter(), "u_priority", filterSb, filterByPriority);
        
        String orgFilters = getOrgHierarchy(filterContainer.getOrgId(), ",", filterContainer.getUsername());
        buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);

        String uppercaseDBType = configSQL.getDbtype().toUpperCase();

        if (uppercaseDBType.equals("MYSQL"))
        {            
            String meanTimeAvgSql   =  "select      t.age,  " + visualizationGroupType.getColumnName() + ", AVG(UNIX_TIMESTAMP("+ mtReportType.getColumnName() +") - UNIX_TIMESTAMP(sys_created_on))" + 
                                            "from        ( " +
                                            "             select      FLOOR((UNIX_TIMESTAMP(" + mtReportType.getColumnName() + ") - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) as age, " + visualizationGroupType.getColumnName() + "," + mtReportType.getColumnName()  + ", sys_created_on" +
                                            "             from        resolve_security_incident " +
                                            "             where       " + mtReportType.getColumnName() + " is not null " +
                                            "             and         sys_is_deleted != 'Y' " +
                                            (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") +
                                            "             and         (u_master_sir is null OR u_master_sir = '') " +
                                            "             and         FLOOR((UNIX_TIMESTAMP(" + mtReportType.getColumnName() + ") - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) < :lastBucketLowerBoundary " +
                                            "             ) t " +
                                            "where t.sys_created_on > :dateBeforeTwoYears " +
                                            "group by     t.age " + "," + visualizationGroupType.getColumnName() +
                                            "order by     t.age asc";
            
            
            meanTimeAvgQuery.setSqlQuery(meanTimeAvgSql);
            
            String meanTimeAvgPastThresholdSql  = "select      " + visualizationGroupType.getColumnName() + ", AVG(UNIX_TIMESTAMP("+ mtReportType.getColumnName() +") - UNIX_TIMESTAMP(sys_created_on))" + 
                                                  "from        ( " +
                                                  "             select      " + visualizationGroupType.getColumnName() + "," + mtReportType.getColumnName()  + ", sys_created_on " +
                                                  "             from        resolve_security_incident " +
                                                  "             where       " + mtReportType.getColumnName() + " is not null " +
                                                  "             and         sys_is_deleted != 'Y' " +
                                                  "             and         (u_master_sir is null OR u_master_sir = '') " +
                                                  (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") +
                                                  "             and         FLOOR((UNIX_TIMESTAMP(" + mtReportType.getColumnName() + ") - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) >= :lastBucketLowerBoundary " +
                                                  "             ) t " +
                                                  "where t.sys_created_on > :dateBeforeTwoYears " +
                                                  "group by     " + visualizationGroupType.getColumnName();
            
                        
            meanTimeAvgPastThresholdQuery.setSqlQuery(meanTimeAvgPastThresholdSql);
        }
        else
        {            
            String meanTimeAvgSql  = "select     t.age, " + visualizationGroupType.getColumnName() +", " + "avg((cast("+ mtReportType.getColumnName() + " as date) -  cast(sys_created_on as date))*86400)"  +
                                          "from       ( " +
                                          "            select      FLOOR(((cast("+ mtReportType.getColumnName() + " as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) as age, " + visualizationGroupType.getColumnName() + "," + mtReportType.getColumnName()  + ", sys_created_on" +
                                          "            from        resolve_security_incident " +
                                          "            where       "+ mtReportType.getColumnName() + " is not null " +
                                          "            and         sys_is_deleted != 'Y' " +
                                          "            and         (u_master_sir is null OR u_master_sir = '') " +
                                          (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") +
                                          "            and         FLOOR(((cast("+ mtReportType.getColumnName() + " as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) < :lastBucketLowerBoundary " +
                                          "           ) t " + 
                                          "where t.sys_created_on > :dateBeforeTwoYears " +
                                          "group by   t.age," +  visualizationGroupType.getColumnName() +
                                          "order by   t.age asc";
            
            meanTimeAvgQuery.setSqlQuery(meanTimeAvgSql);
            
            String meanTimeAvgPastThresholdSql  = "select      " + visualizationGroupType.getColumnName() + ", avg((cast("+ mtReportType.getColumnName() +" as date) -  cast(sys_created_on as date))*86400)" + 
                                                  "from        ( " +
                                                  "             select      " + visualizationGroupType.getColumnName() + "," + mtReportType.getColumnName() + ", sys_created_on" +
                                                  "             from        resolve_security_incident " +
                                                  "             where       "+ mtReportType.getColumnName() +" is not null " +
                                                  "             and         sys_is_deleted != 'Y' " +
                                                  "             and         (u_master_sir is null OR u_master_sir = '') " +
                                                  (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") +
                                                  "             and         FLOOR(((cast("+ mtReportType.getColumnName() +" as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) >= :lastBucketLowerBoundary " +
                                                  "            ) t " + 
                                                  "where t.sys_created_on > :dateBeforeTwoYears " +
                                                  "group by     " + visualizationGroupType.getColumnName();
                        
            meanTimeAvgPastThresholdQuery.setSqlQuery(meanTimeAvgPastThresholdSql);
        }
        
        meanTimeAvgQuery.setUseSql(true);
        meanTimeAvgPastThresholdQuery.setUseSql(true);
        
        Map<String, Object> countByAgeQueryParams = new HashMap<String, Object>();
        
        long secsInTmprlUnit = 60;
        
        countByAgeQueryParams.put("secsInTmprlUnit", new Double(secsInTmprlUnit));
        
        Date now = new Date();
        countByAgeQueryParams.put("dateBeforeTwoYears", new DateTime(now).minusYears(2).toDate());
        
        if (uppercaseDBType.equals("MYSQL"))
        {
            countByAgeQueryParams.put("lastBucketLowerBoundary", lastBucketLowerBoundary);
        }
        else
        {
            countByAgeQueryParams.put("lastBucketLowerBoundary", new Double(lastBucketLowerBoundary.doubleValue()));
        }
        
        setQueryParams(filterByTypes, countByAgeQueryParams);
        setQueryParams(filterBySeverities, countByAgeQueryParams);
        setQueryParams(filterByOwners, countByAgeQueryParams);
        setQueryParams(filterByOrgs, countByAgeQueryParams);
        setQueryParams(filterByStatus, countByAgeQueryParams);
        setQueryParams(filterByPriority, countByAgeQueryParams);
        
        meanTimeAvgQuery.setParams(countByAgeQueryParams);
        meanTimeAvgPastThresholdQuery.setParams(countByAgeQueryParams);
        
        ResponseDTO<Map<BigDecimal, Map<String, BigDecimal>>> result = new ResponseDTO<Map<BigDecimal, Map<String, BigDecimal>>>();
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
        		   Query query = HibernateUtil.createSQLQuery(meanTimeAvgQuery.getSelectSQLNonParameterizedSort());
                   
                   for (String key : countByAgeQueryParams.keySet())
                   {
                       query.setParameter(key, countByAgeQueryParams.get(key));
                   }
                               
                   List<Object[]> data = query.list();
                   int total = 0;
                   
                   Map<BigDecimal, Map<String, BigDecimal>> bucketsData = new HashMap<BigDecimal, Map<String, BigDecimal>>();
                   if(data != null)
                   {
                       for (Object[] aRow : data)
                       {
                           Map<String, BigDecimal> bucketRecords;
                           
                           BigDecimal bucket;
                           if (uppercaseDBType.equals("MYSQL"))
                           {
                               bucket = new BigDecimal((BigInteger)aRow[0]);
                           }
                           else
                           {
                               bucket = (BigDecimal)aRow[0];
                           }
                           
                           if(bucketsData.containsKey(bucket)) {
                               bucketRecords = bucketsData.get(bucket);
                           } else {
                               bucketRecords = new HashMap<String, BigDecimal>();
                           }
                           
                           bucketRecords.put((String)aRow[1], (BigDecimal)aRow[2]);
                           bucketsData.put(bucket, bucketRecords);
                       }
                       total = data.size();
                   }
                   
                   Query pastThresholdQuery = HibernateUtil.createSQLQuery(meanTimeAvgPastThresholdQuery.getSelectSQL());
                   
                   for (String key : countByAgeQueryParams.keySet())
                   {
                       pastThresholdQuery.setParameter(key, countByAgeQueryParams.get(key));
                   }
                   
                   List<Object[]> pastThresholdData = pastThresholdQuery.list();
                 
                   
                   if(pastThresholdData != null && !pastThresholdData.isEmpty())
                   {
                       Map<String, BigDecimal> pastThreasholdBucketRecords = new HashMap<String, BigDecimal>();
                       for(Object[] pastThreasholdBucketRecord : pastThresholdData) {
                           pastThreasholdBucketRecords.put((String) pastThreasholdBucketRecord[0], (BigDecimal)pastThreasholdBucketRecord[1]);
                       }
                       bucketsData.put(new BigDecimal(lastBucketLowerBoundary.intValue() + 1), pastThreasholdBucketRecords);
                       total++;
                   }
                   
                   
                   result.setRecords(Arrays.asList(bucketsData));
                   result.setTotal(total);
                   
                   result.setSuccess(true);
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting data for hql:" + meanTimeAvgQuery.getSelectSQLNonParameterizedSort(), e);

            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
	
    public static List<String> getMTBucketKeys(MeanTimeFilterContainer filterContainer) throws Exception{
        
        switch(SIRMeanTimeReportGroupType.valueOf(filterContainer.getVisualizedFilter())) {
            case OWNER: return PlaybookUtils.getSecurityGroupMembers(filterContainer.getUsername())
                            .stream()
                            .map(p -> p.get("userId"))
                            .filter(p -> (filterContainer.getOwnerFilter().isEmpty() || filterContainer.getOwnerFilter().contains(p)))
                            .collect(Collectors.toList());
            
            case SEVERITY: return SIRSeverityFilter.getSeverityNames()
                            .stream()
                            .filter(p -> (filterContainer.getSeverityFilter().isEmpty() || filterContainer.getSeverityFilter().contains(p)))
                            .collect(Collectors.toList());
            
            case TYPE: return getSIRTypes()
                            .stream()
                            .filter(p -> (filterContainer.getTypeFilter().isEmpty() || filterContainer.getTypeFilter().contains(p)))
                            .collect(Collectors.toList());
            
            default: throw new IllegalArgumentException(filterContainer.getVisualizedFilter() +" type is not supported!");
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private static List<String> getSIRTypes() throws Exception
    {
    	List<String> result = new ArrayList<>();
        QueryDTO typesInScopeQuery = new QueryDTO();

        typesInScopeQuery.setHql("select investigationType from ResolveSecurityIncident where investigationType is not null and sysIsDeleted is false group by investigationType");

        Map<String, Object> typesInScopeQueryParams = new HashMap<String, Object>();

        typesInScopeQuery.setParams(typesInScopeQueryParams);

        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (List<String>) HibernateProxy.execute(() -> {
        		Query query = HibernateUtil.createQuery(typesInScopeQuery);
                return query.list();
        	});
            
        }
        catch(Exception e)
        {
            Log.log.error(e,e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<Map<BigDecimal, Object>> getSIRCountByAgeReport(Integer thresholdUnits, String uppercaseDBType, String typeFilter, 
                                                                              String severityFilter, String ownerFilter, String teamFilter, String statusFilter, 
                                                                              String priorityFilter, String orgId, String username) throws Exception
    {
	    ResponseDTO<Map<BigDecimal, Object>> result = new ResponseDTO<Map<BigDecimal, Object>>();
        List<Map<BigDecimal, Object>> records = new ArrayList<Map<BigDecimal, Object>>();
        
        QueryDTO countByAgeQuery = new QueryDTO();
        
        QueryDTO countPastThresholdQuery = new QueryDTO();
        
        Map<String, String> filterByTypes = new HashMap<String, String>();
        Map<String, String> filterBySeverities = new HashMap<String, String>();
        Map<String, String> filterByOwners = new HashMap<String, String>();
        Map<String, String> filterByStatuses = new HashMap<String, String>();
        Map<String, String> filterByPriority = new HashMap<String, String>();
        Map<String, String> filterByOrgs = new HashMap<String, String>();
        String orgFilters = getOrgHierarchy(orgId, ",", username);
        if (uppercaseDBType.equals("MYSQL"))
        {            
            String countByAgeSqlPrefix   =  "select      t.age, count(t.sys_id) " +
                                            "from        ( " +
                                            "             select      FLOOR((UNIX_TIMESTAMP(u_closed_on) - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) as age, sys_id " +
                                            "             from        resolve_security_incident " +
                                            "             where       u_closed_on is not null " +
                                            "             and         sys_is_deleted != 'Y' " +
                                            "             and         (u_master_sir is null OR u_master_sir = '') ";
            
            String countByAgeSqlSuffix1 =   "             and         FLOOR((UNIX_TIMESTAMP(u_closed_on) - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) < :thresholdUnits " + 
                                            "             UNION " +
                                            "             select      FLOOR((UNIX_TIMESTAMP(:currentTime) - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) as age, sys_id " +
                                            "             from        resolve_security_incident " +
                                            "             where       u_closed_on is null " +
                                            "             and         sys_is_deleted != 'Y' ";
            
            String countByAgeSqlSuffix2 =   "             and         FLOOR((UNIX_TIMESTAMP(:currentTime) - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) < :thresholdUnits " +
                                            "             ) t " +
                                            "group by     t.age " + 
                                            "order by     t.age asc";
            
            StringBuilder filterSb = new StringBuilder();
            
            buildFilterQuery(typeFilter, "u_investigation_type", filterSb, filterByTypes);
            
            buildFilterQuery(severityFilter, "u_severity", filterSb, filterBySeverities);
            
            buildFilterQuery(ownerFilter, "u_owner", filterSb, filterByOwners);
            
            buildFilterQuery(statusFilter, "u_status", filterSb, filterByStatuses);
            
            buildFilterQuery(priorityFilter, "u_priority", filterSb, filterByPriority);

            buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);

            String countByAgeSql = countByAgeSqlPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countByAgeSqlSuffix1 +
                                   (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countByAgeSqlSuffix2;            
            
            countByAgeQuery.setSqlQuery(countByAgeSql);
            
            String countPastThresholdSqlPrefix  = "select      count(t.sys_id) " +
                                                  "from        ( " +
                                                  "             select      sys_id " +
                                                  "             from        resolve_security_incident " +
                                                  "             where       u_closed_on is not null " +
                                                  "             and         sys_is_deleted != 'Y' ";
            
            String countPastThresholdSqlSuffix1 = "             and         FLOOR((UNIX_TIMESTAMP(u_closed_on) - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) >= :thresholdUnits " + 
                                                  "             UNION " +
                                                  "             select      sys_id " +
                                                  "             from        resolve_security_incident " +
                                                  "             where       u_closed_on is null " +
                                                  "             and         sys_is_deleted != 'Y' ";
            
            String countPastThresholdSqlSuffix2 = "             and         FLOOR((UNIX_TIMESTAMP(:currentTime) - UNIX_TIMESTAMP(sys_created_on)) / :secsInTmprlUnit) >= :thresholdUnits " +
                                                  "             ) t ";
                        
            String countPastThresholdQuerySql = countPastThresholdSqlPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countPastThresholdSqlSuffix1 +
                                                (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countPastThresholdSqlSuffix2;
                        
            countPastThresholdQuery.setSqlQuery(countPastThresholdQuerySql);
        }
        else
        {            
            String countByAgeSqlPrefix  = "select     t.age, count(sys_id) " +
                                          "from       ( " +
                                          "            select      FLOOR(((cast(u_closed_on as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) as age, sys_id " +
                                          "            from        resolve_security_incident " +
                                          "            where       u_closed_on is not null " +
                                          "            and         sys_is_deleted != 'Y' ";
            
            String countByAgeSqlSuffix1 = "            and         FLOOR(((cast(u_closed_on as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) < :thresholdUnits " +
                                          "            UNION " +
                                          "            select      FLOOR(((cast(:currentTime as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) as age, sys_id " +
                                          "            from        resolve_security_incident " +
                                          "            where       u_closed_on is null " +
                                          "            and         sys_is_deleted != 'Y' ";
            
            String countByAgeSqlSuffix2 = "            and         FLOOR(((cast(:currentTime as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) < :thresholdUnits " +
                                          "           ) t " + 
                                          "group by   t.age " +
                                          "order by   t.age asc ";
            
            StringBuilder filterSb = new StringBuilder();
            
            buildFilterQuery(typeFilter, "u_investigation_type", filterSb, filterByTypes);
            
            buildFilterQuery(severityFilter, "u_severity", filterSb, filterBySeverities);
            
            buildFilterQuery(ownerFilter, "u_owner", filterSb, filterByOwners);
            
            buildFilterQuery(statusFilter, "u_status", filterSb, filterByStatuses);
            
            buildFilterQuery(priorityFilter, "u_priority", filterSb, filterByPriority);

            buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);

            String countByAgeSql = countByAgeSqlPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countByAgeSqlSuffix1 +
                                   (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countByAgeSqlSuffix2;
            
            countByAgeQuery.setSqlQuery(countByAgeSql);
            
            String countPastThresholdSqlPrefix  = "select      count(t.sys_id) " +
                                                  "from        ( " +
                                                  "             select      sys_id " +
                                                  "             from        resolve_security_incident " +
                                                  "             where       u_closed_on is not null " +
                                                  "             and         sys_is_deleted != 'Y' ";
            
            String countPastThresholdSqlSuffix1 = "             and         FLOOR(((cast(u_closed_on as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) >= :thresholdUnits " +
                                                  "             UNION " +
                                                  "             select      sys_id " +
                                                  "             from        resolve_security_incident " +
                                                  "             where       u_closed_on is null " +
                                                  "             and         sys_is_deleted != 'Y' ";
            
            String countPastThresholdSqlSuffix2 = "             and         FLOOR(((cast(:currentTime as date) - cast(sys_created_on as date))*86400) / :secsInTmprlUnit) >= :thresholdUnits " +
                                                  "            ) t ";
            
            String countPastThresholdQuerySql = countPastThresholdSqlPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countPastThresholdSqlSuffix1 +
                                                (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "") + countPastThresholdSqlSuffix2;
                        
            countPastThresholdQuery.setSqlQuery(countPastThresholdQuerySql);
        }
        
        countByAgeQuery.setUseSql(true);
        countPastThresholdQuery.setUseSql(true);
        
        Map<String, Object> countByAgeQueryParams = new HashMap<String, Object>();
        
        long secsInTmprlUnit = 60;
        
        countByAgeQueryParams.put("secsInTmprlUnit", new Double(secsInTmprlUnit));
        
        countByAgeQueryParams.put("currentTime", new Date());
        
        if (uppercaseDBType.equals("MYSQL"))
        {
            countByAgeQueryParams.put("thresholdUnits", thresholdUnits);
        }
        else
        {
            countByAgeQueryParams.put("thresholdUnits", new Double(thresholdUnits.doubleValue()));
        }
        
        setQueryParams(filterByTypes, countByAgeQueryParams);
        setQueryParams(filterBySeverities, countByAgeQueryParams);
        setQueryParams(filterByOwners, countByAgeQueryParams);
        setQueryParams(filterByStatuses, countByAgeQueryParams);
        setQueryParams(filterByPriority, countByAgeQueryParams);
        setQueryParams(filterByOrgs, countByAgeQueryParams);
        
        countByAgeQuery.setParams(countByAgeQueryParams);
        countPastThresholdQuery.setParams(countByAgeQueryParams);
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
        		   Query query = HibernateUtil.createSQLQuery(countByAgeQuery.getSelectSQLNonParameterizedSort());
                   
                   for (String key : countByAgeQueryParams.keySet())
                   {
                       query.setParameter(key, countByAgeQueryParams.get(key));
                   }
                               
                   List<Object[]> data = query.list();
                   
                   int total = 0;
                   
                   if(data != null)
                   {
                       for (Object[] aRow : data)
                       {
                           HashMap<BigDecimal, Object> record = new HashMap<BigDecimal, Object>();
                           
                           if (uppercaseDBType.equals("MYSQL"))
                           {
                               record.put(new BigDecimal((BigInteger)aRow[0]), (BigInteger)aRow[1]);
                           }
                           else
                           {
                               record.put((BigDecimal)aRow[0], (BigDecimal)aRow[1]);
                           }
                           
                           records.add(record);
                       }
                       total = data.size();
                   }
                   
                   Query pastThresholdQuery = HibernateUtil.createSQLQuery(countPastThresholdQuery.getSelectSQL());
                   
                   for (String key : countByAgeQueryParams.keySet())
                   {
                       pastThresholdQuery.setParameter(key, countByAgeQueryParams.get(key));
                   }
                   
                   List<Object[]> pastThresholdData = pastThresholdQuery.list();
                   
                   if(pastThresholdData != null && !pastThresholdData.isEmpty())
                   {
                       HashMap<BigDecimal, Object> record = new HashMap<BigDecimal, Object>();
                       
                       Object obj = pastThresholdData.get(0);
                       
                       if (uppercaseDBType.equals("MYSQL"))
                       {
                           record.put(new BigDecimal(thresholdUnits.intValue() + 1), (BigInteger)obj);
                       }
                       else
                       {
                           record.put(new BigDecimal(thresholdUnits.intValue() + 1), (BigDecimal)obj);
                       }
                       records.add(record);
                       total++;
                   }
                   
                   result.setRecords(records);
                   result.setTotal(total);
                   
                   result.setSuccess(true);
                   
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting data for hql:" + countByAgeQuery.getSelectSQLNonParameterizedSort(), e);
            
            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
	
	public static void resetDTData(String problemId, String dtFullName, String username)
	{
		if (StringUtils.isNotBlank(problemId) && StringUtils.isNotBlank(dtFullName))
		{
			DecisionTreeHelper.resetDTData(problemId, dtFullName, username); 
		}
		else
		{
			Log.log.error("ERROR: To reset DT data from an incident, DT full name and problemId is needed.");
		}
	}
	
	public static ResponseDTO<PBNotes> searchPBWikiNotesByIncidentId(String incidentId, String username, int maxLimit) throws SearchException, LimitExceededException
    {
        return PlaybookSearchAPI.searchPBWikiNotesByIncidentId(incidentId, username, maxLimit);
    }
	
	public static ResponseDTO<PBNotes> searchPBWikiNotesByIncidentAndActivityId(String incidentId, String activityId, String username) throws SearchException
    {
        return PlaybookSearchAPI.searchPBWikiNotesByIncidentAndActivityId(incidentId, activityId, username);
    }
	
	public static ResponseDTO<PBNotes> searchPBWikiNoteByActivityAndComponentId(String activityId, String componentId, String username) throws SearchException
    {
        return PlaybookSearchAPI.searchPBWikiNotesByActivityAndComponentId(activityId, componentId, username);
    }
	
	public static Map<String, Object> getAutomationFilter(String templateName, Integer  version)
	{
		Map<String, Object> automationFilterMap = new HashMap<String, Object>();
		List<String> taskList = new ArrayList<String>();
		
		if (StringUtils.isNotBlank(templateName))
		{
			String content = null;
			List<Object> objList = WikiUtils.getWikiContentList(templateName, version);
			if (objList != null)
			{
				for (Object obj : objList)
	    		{
	    			Object[] objArray = (Object[])obj;
	    			content = (String)objArray[1];
	    		}
			}
			
			if (StringUtils.isNotBlank(content))
			{
				String[] sections = StringUtils.substringsBetween(content, "{result2:", "{result2}");
				
				if (sections != null && sections.length > 0)
				{
					for (String section : sections)
					{
						String[] sectionArray = section.split("\n");
						
						for (String line : sectionArray)
						{
							if (StringUtils.isNotBlank(line))
							{
								if (line.contains("selectAll"))
								{
									// result2 macro definition
									line = line.replace("}", "");
									String[] macroDefinitionArray = line.split("\\|");
									
									for (String macroDefinition : macroDefinitionArray)
									{
										String[] defItems = macroDefinition.split("=");
										if (defItems.length == 2)
										{
											automationFilterMap.put(defItems[0], defItems[1]);
										}
									}
								}
								else if (line.contains("namespace="))
								{
									taskList.add(line);
								}
							}
						}
						
						automationFilterMap.put("actionTasks", taskList);
					}
				}
			}
		}
		
		return automationFilterMap;
	}
	
	public static void auditAction(String description, String incidentId, String remoteAddress, String username) 
	{
		try
		{
		    PBAuditLog auditLog = new PBAuditLog();
		    auditLog.setUserName(username);
	        auditLog.setIpAddress(remoteAddress);
	        auditLog.setIncidentId(incidentId);
	        auditLog.setDescription(description);
	        indexAuditLog(auditLog, username);
		}
		catch (Exception exp)
        {
        	throw new RuntimeException(exp);
        }
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<Map<String, String>> getSIRTypesInScope(Integer scopeUnits, TemporalUnit tmprlUnit)
    {
	    ResponseDTO<Map<String, String>> result = new ResponseDTO<Map<String, String>>();
        List<Map<String, String>> records = new ArrayList<Map<String, String>>();
        
        QueryDTO typesInScopeQuery = new QueryDTO();
        
        typesInScopeQuery.setHql("select investigationType from ResolveSecurityIncident where investigationType is not null and sysIsDeleted is false and sysCreatedOn > :sysCreatedAfter group by investigationType");
        
        Map<String, Object> typesInScopeQueryParams = new HashMap<String, Object>();
        
        Date currDate = new Date();
        
        Instant currInst = currDate.toInstant();
        
        typesInScopeQueryParams.put("sysCreatedAfter", new Date(currInst.minus(scopeUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));
                
        typesInScopeQuery.setParams(typesInScopeQueryParams);
        
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	Query query = HibernateUtil.createQuery(typesInScopeQuery);
                
                List<String> data = query.list();
                
                if(data != null)
                {
                    for (String aRow : data)
                    {
                        HashMap<String, String> record = new HashMap<String, String>();
                        record.put("type", aRow);
                        records.add(record);
                    }
                    result.setTotal(data.size());
                }
                
                result.setRecords(records);
                result.setSuccess(true);
                
            });
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting data for hql:" + typesInScopeQuery.getHql(), e);
            
            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
	
	public static void updateAttachments(List<Map<String, Object>> attachmentMapList, String incidentId, String username) throws Exception
    {   
		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
		
		if (incidentVO != null && "Closed".equalsIgnoreCase(incidentVO.getStatus())) {
    		throw new Exception(ERROR_CLOSED_INCIDENT);
        }

        Map<String, Map<String, Object>> attachmentIdMap = new HashMap<String, Map<String, Object>>();
        
        for (Map<String, Object> attachment : attachmentMapList)
        {
            attachmentIdMap.put((String)attachment.get("id"), attachment);
        }
        
        ResponseDTO<PBAttachments> respDTO = PlaybookUtils.searchPBAttachmentsByIncident(incidentId, username, null);
        
        if (respDTO != null && !respDTO.getRecords().isEmpty())
        {
            for (PBAttachments pbAttchmt : respDTO.getRecords())
            {
                if (attachmentIdMap.containsKey(pbAttchmt.getSysId()))
                {
                    Map<String, Object> updatedAttchmt = attachmentIdMap.get(pbAttchmt.getSysId());
                    
                    if (updatedAttchmt.containsKey("malicious"))
                    {
                        boolean malicious = Boolean.valueOf(updatedAttchmt.get("malicious").toString());
                        if (pbAttchmt.isMalicious() && !malicious)
                        {
                            if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_UNSETMALICIOUS, ""))
                            {
                                throw new Exception(String.format("User %s does not have access to unset malicious flag on the attachment(s).", username));
                            }
                        }
                        pbAttchmt.setMalicious((Boolean)updatedAttchmt.get("malicious"));
                    }
                    
                    if (updatedAttchmt.containsKey("description"))
                    {
                        pbAttchmt.setDescription((String)updatedAttchmt.get("description"));
                    }
                    
                    indexAttachment(pbAttchmt, username);
                }
            }
        }
    }
	
	public static WikiDocumentVO createNewTemplate(String fullname) throws Exception
	{
	    WikiDocumentVO wikiVO = null;
	    
	    if (StringUtils.isNotBlank(fullname))
	    {
	        if (WikiUtils.isWikidocExist(fullname, false))
	        {
	            return wikiVO;
	        }
	        else
	        {
	            wikiVO = new WikiDocumentVO();
                String[] nameArray = fullname.split("\\.");
                if (nameArray.length == 2)
                {
                    wikiVO.setUName(nameArray[1].trim());
                    wikiVO.setUNamespace(nameArray[0].trim());
                    wikiVO.setUFullname(fullname);
                }
	        }
	    }
	    
	    return wikiVO;
	}
	
	public static Map<String, Object> createSIRAndUpdateWSReferenceDetails(Map<String, Object> sirParams) throws Exception 
    {
        Map<String, Object> response = new HashMap<String, Object>();
        ResolveSecurityIncident resolveSIR = new ResolveSecurityIncident();
        resolveSIR.setTitle((String) sirParams.get(Constants.EXECUTE_SIR_TITLE));
        resolveSIR.setType((String) sirParams.get(Constants.EXECUTE_SIR_TYPE));
        resolveSIR.setInvestigationType((String) sirParams.get(Constants.EXECUTE_SIR_INVESTIGATION_TYPE));
        resolveSIR.setPlaybook((String) sirParams.get(Constants.EXECUTE_SIR_PLAYBOOK));
        resolveSIR.setSeverity((String) sirParams.get(Constants.EXECUTE_SIR_SEVERITY));
        resolveSIR.setOwner((String) sirParams.get(Constants.EXECUTE_SIR_OWNER));
        
        // Set external reference id as either REFERENCE, ALERTID or CORRELATIONID in the order
        
        String extRefId = null;
        
        if (sirParams.containsKey(Constants.EXECUTE_REFERENCE) && StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_REFERENCE)))
        {
            extRefId = (String)sirParams.get(Constants.EXECUTE_REFERENCE);
        }
        
        if (StringUtils.isBlank(extRefId) && sirParams.containsKey(Constants.EXECUTE_ALERTID) &&
            StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_ALERTID)))
        {
            extRefId = (String)sirParams.get(Constants.EXECUTE_ALERTID);
        }
        
        if (StringUtils.isBlank(extRefId) && sirParams.containsKey(Constants.EXECUTE_CORRELATIONID) &&
            StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_CORRELATIONID)))
        {
            extRefId = (String)sirParams.get(Constants.EXECUTE_CORRELATIONID);
        }
        
        if (StringUtils.isNotBlank(extRefId))
        {
            resolveSIR.setExternalReferenceId(extRefId);
        }
        
        resolveSIR.setSourceSystem((String) sirParams.get(Constants.EXECUTE_SIR_SOURCE_SYSTEM));
        // check for problem ID being passed to use it (UI RR use case)
        if (sirParams.containsKey(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR)) {
            resolveSIR.setProblemId((String) sirParams.get(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR));
        }
        // RBA-14934 : Save All Data from a Gateway to an Investigation Record in SIR case
        resolveSIR.setRequestData((String) sirParams.get(Constants.EXECUTE_SIR_INITIAL_REQUEST_DATA));
        OrgsVO orgsVO = null;
        if (sirParams.containsKey(Constants.EXECUTE_ORG_NAME) && 
            StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_ORG_NAME)))
        {
            // Get Org Id for Org Name and set it as sysOrg in resolveSIR
            
            orgsVO = UserUtils.getOrg(null, (String)sirParams.get(Constants.EXECUTE_ORG_NAME));
            
            if (orgsVO != null)
            {
                if (!UserUtils.isAdminUser(resolveSIR.getOwner()))
                {
                    if (UserUtils.isOrgAccessible(orgsVO.getSys_id(), resolveSIR.getOwner(), false, false))
                    {
                        resolveSIR.setSysOrg(orgsVO.getSys_id());
                    }
                    else
                    {
                        throw new Exception("User does not have permission to specified Org.");
                    }
                }
                else
                {
                    resolveSIR.setSysOrg(orgsVO.getSys_id());
                }
            }
            else
            {
                throw new Exception ("Invalid Org Name " + (String)sirParams.get(Constants.EXECUTE_ORG_NAME) +
                                     " specified in parameters passed for creating SIR.");
            }
        }
        else
        {
            if (!UserUtils.isAdminUser(resolveSIR.getOwner()) && !UserUtils.isNoOrgAccessible(resolveSIR.getOwner()))
            {
                throw new Exception("User does not have permission to specified No Org 'None'.");
            }
        }
        
        // upsert SIR
        ResolveSecurityIncidentVO resolveSIRVO = PlaybookUtils.saveSecurityIncident(resolveSIR, resolveSIR.getOwner());
        // returning back all necessary details
        response.put(Constants.EXECUTE_PROBLEMID_ENFORCED_BY_SIR, resolveSIRVO.getProblemId());
        response.put(Constants.EXECUTE_SIR_ID, resolveSIRVO.getSir());
        response.put(Constants.EXECUTE_SIR_PLAYBOOK, resolveSIRVO.getPlaybook());
        // Updating WS reference, alertid and correlationid
        Map<String, Object> referenceDetails = new HashMap<String, Object>();
        referenceDetails.put("u_reference", sirParams.get(Constants.EXECUTE_REFERENCE));
        referenceDetails.put("u_alert_id", sirParams.get(Constants.EXECUTE_ALERTID));
        referenceDetails.put("u_correlation_id", sirParams.get(Constants.EXECUTE_CORRELATIONID));
        if (orgsVO == null) {
            referenceDetails.put("sysOrg", null); // set the sysOrg as null
        } else {
            referenceDetails.put("sysOrg", orgsVO.getSys_id());
        }
        ServiceWorksheet.updateWorksheet(resolveSIRVO.getProblemId(), referenceDetails, resolveSIR.getOwner());
        return response;
    }
	
	/**
     * API to be invoked from update/upgrade script ONLY
     */
    @SuppressWarnings("rawtypes")
    public static void addDefaultSecGroupsToSysProperty()
    {
        String selectHQL = "select UValue from Properties where UName = 'app.security.groups'";
        String updateHQL = "update Properties set UValue = :value where UName = 'app.security.groups'" ;
        String secGroups = "Security Admin Group,Security Auditor Group,Security Designer Group,Security User Group";
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	boolean modified = false;
            
	            Query hqlQuery = HibernateUtil.createQuery(selectHQL);
	            Object obj = hqlQuery.uniqueResult();
	            
	            String value = obj == null ? "" : obj.toString();
	            
	            if (StringUtils.isBlank(value))
	            {
	                modified = true;
	                value = secGroups;
	            }
	            else
	            {
	                List<String> groupList = new ArrayList<String>();
	                groupList.addAll(Arrays.asList(value.replaceAll(" *, *", ",").split(",")));
	                List<String> defaultGroups = Arrays.asList(secGroups.split(","));
	                
	                
	                for (String defaultGroup : defaultGroups)
	                {
	                    if (!groupList.contains(defaultGroup))
	                    {
	                        modified = true;
	                        groupList.add(defaultGroup);
	                    }
	                }
	                
	                value = StringUtils.substringBetween(groupList.toString(), "[", "]");
	            }
	            
	            if (modified)
	            {
	                HibernateUtil.createQuery(updateHQL).setParameter("value", value, StringType.INSTANCE).executeUpdate();
	                Log.log.info("app.security.groups got updated to: " + value);
	            }
	            else
	            {
	                Log.log.info("app.security.groups is uptodate.");
	            }
            
            });
        }
        catch(Throwable t)
        {
            Log.log.error("Error: " + t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<Map<String, Object>> getSIRCountByWorkloadReport (
                    Integer scopeUnits,
                    TemporalUnit tmprlUnit,
                    String uppercaseDBType,
                    String typeFilter, 
                    String severityFilter,
                    String ownerFilter,
                    String teamFilter,
                    String statusFilter,
                    String priorityFilter,
                    String orgId,
                    String username) throws Exception
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
//        String aliasTbl1 = "";
//        String aliasTbl2 = "";
//        String aliasTbl3 = "";
//        if (uppercaseDBType.equals("MYSQL"))
//        {
//            aliasTbl1 = " AS tbl1";
//            aliasTbl2 = " AS tbl2";
//            aliasTbl3 = " AS tbl3";
//        }
        List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        QueryDTO countByWorkloadQuery = new QueryDTO();
        String teamFilterStr = "";

        if (!teamFilter.trim().toUpperCase().equals("ALL")) {
            teamFilterStr = buildteamFilterQuery(teamFilter, "u_members") + " AND ";
        }
        String cond = teamFilterStr + " sys_is_deleted != 'Y' AND sys_created_on >= :sysCreatedAfter and (u_master_sir is null OR u_master_sir = '') ";
        String sqlQueryPrefix ="SELECT u_status from resolve_security_incident WHERE " + cond;

        StringBuilder filterSb = new StringBuilder();

        Map<String, String> filterByTypes = new HashMap<String, String>();
        buildFilterQuery(typeFilter, "u_investigation_type", filterSb, filterByTypes);

        Map<String, String> filterBySeverities = new HashMap<String, String>();
        buildFilterQuery(severityFilter, "u_severity", filterSb, filterBySeverities);

        Map<String, String> filterByOwners = new HashMap<String, String>();
        buildFilterQuery(ownerFilter, "u_owner", filterSb, filterByOwners);

        Map<String, String> filterByStatuses = new HashMap<String, String>();
        buildFilterQuery(statusFilter, "u_status", filterSb, filterByStatuses);
        
        Map<String, String> filterByPriority = new HashMap<String, String>();
        buildFilterQuery(priorityFilter, "u_priority", filterSb, filterByPriority);

        Map<String, String> filterByOrgs = new HashMap<String, String>();
        String orgFilters = getOrgHierarchy(orgId, ",", username);
        buildFilterQuery(orgFilters, "sys_org", filterSb, filterByOrgs);

        String countByWorkloadSql = sqlQueryPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? " and " + filterSb.toString() : "");
        countByWorkloadQuery.setSqlQuery(countByWorkloadSql);
 
        countByWorkloadQuery.setUseSql(true);

        Map<String, Object> countByWorkloadQueryParams = new HashMap<String, Object>();

        Date currDate = new Date();

        Instant currInst = currDate.toInstant();

        countByWorkloadQueryParams.put("sysCreatedAfter", new Date(currInst.minus(scopeUnits.longValue(), tmprlUnit).getLong(ChronoField.INSTANT_SECONDS)*1000));

        setQueryParams(filterByTypes, countByWorkloadQueryParams);
        setQueryParams(filterBySeverities, countByWorkloadQueryParams);
        setQueryParams(filterByOwners, countByWorkloadQueryParams);
        setQueryParams(filterByStatuses, countByWorkloadQueryParams);
        setQueryParams(filterByPriority, countByWorkloadQueryParams);
        setQueryParams(filterByOrgs, countByWorkloadQueryParams);

        countByWorkloadQuery.setParams(countByWorkloadQueryParams);

        try
        {

          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                Query query = HibernateUtil.createSQLQuery(countByWorkloadQuery.getSelectSQLNonParameterizedSort());
                for (String key : countByWorkloadQueryParams.keySet())
                {
                    query.setParameter(key, countByWorkloadQueryParams.get(key));
                }
                List<String> data = query.list();
                if(data != null)
                {
                    Map<String, Object> linkedMap = new LinkedHashMap<String, Object>();
                    Iterator<String> it = data.iterator();
                    int closed = 0;
                    int newSir = data.size();
                    while (it.hasNext()) {
                        String o = it.next();
                        if (o.trim().compareToIgnoreCase("closed") == 0) {
                            closed++;
                        }
                    }
                    
                    linkedMap.put("New", newSir);
                    linkedMap.put("Closed", closed);
                    linkedMap.put("Nonclosed", newSir-closed);
                    records.add(linkedMap);
                    result.setTotal(data.size());
                }

                result.setRecords(records);
                result.setSuccess(true);
                
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting report data for SIR count by workload. sql:" + countByWorkloadQuery.getSelectSQLNonParameterizedSort(), e);

            
            result.setSuccess(false).setMessage("Error in getting report data for SIR count by workload. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
    
    protected static void checkUsersSIROrgAccess(String orgId, String username) throws Exception {
		if (StringUtils.isNotBlank(orgId)) {
			if (!UserUtils.isOrgAccessible(orgId, username, false, false)) {
				throw new Exception(String.format(USER_DOES_NOT_BELONG_TO_SIRS_ORG_ERR_MSG, username));
			}
		} else {
			if (!UserUtils.isNoOrgAccessible(username)) {
				throw new Exception(String.format(USER_DOES_NOT_BELONG_TO_NO_OR_NONE_ORG_ERR_MSG, username));
			}
		}
    }
    
	private static void checkUsersSIROrgAccess(VO vo, String username) throws Exception {
		checkUsersSIROrgAccess(vo.getSysOrg(), username);
	}
    
    /**
     * A generic API to retrieve list of associated SIR models from a specified model.
     * As an example, if sourceId is an Activity, and the model is Notes, this API will return all the
     * notes associated with that activity. If the sourceId is an Activity and model is Artifact, this API
     * will return a list of artifacts associated with that Activity.
     * 
     * <br>
     * @param sourceId : String representing the Id of a source which created the model.
     * @param model : String representing the model whose list is needed. Possible values are notes, artifacts, attachments.
     * 
     * @return String. A list of model in JSON format. null if the model name is not predefined.
     */
    
    public static String getCompList(String sourceId, String model, String username) throws Exception
    {
        String result = null;
        
        // both the arguments are needed.
        if (StringUtils.isBlank(sourceId) || StringUtils.isBlank(model))
        {
            Log.log.error("To get a list, sourceId and model is needed.");
            return result;
        }
        
        QueryDTO query = new QueryDTO();
        List<QueryFilter> filters = new ArrayList<QueryFilter>();
        filters.add(new QueryFilter("auto", sourceId, "sourceId", "equals"));
        query.setFilters(filters);
        
        switch (model.toLowerCase())
        {
            case "notes" :
            {
                result = new ObjectMapper().writeValueAsString(searchPBNotes(query, username));
                break;
            }
            case "artifacts" :
            {
                result = new ObjectMapper().writeValueAsString(searchPBArtifacts(query, username));
                break;
            }
            case "attachments" :
            {
                result = new ObjectMapper().writeValueAsString(searchPBAttachments(query, username));
                break;
            }
            default :
            {
                Log.log.error("The model, " + model + " does not belong to a specifed list of either notes, artifacts or attachments");
            }
        }
        
        return result;
    }
    
    public static SIRConfigVO saveSIRConfig(SIRConfigVO sirConfigVO, String username) throws Exception
    {
        SIRConfig savedSIRConfig = null;
        SIRConfigVO savedSIRConfigVO = null;
        
        if (sirConfigVO != null)
        {
            if (StringUtils.isNotBlank(sirConfigVO.getSysOrg()))
            {
                if (!UserUtils.isOrgAccessible(sirConfigVO.getSysOrg(), username, false, false))
                {
                    throw new Exception("User " + username + " does not belong to SIR Config's Org.");
                }
            }
            else
            {
                if (!UserUtils.isNoOrgAccessible(username))
                {
                    throw new Exception("User " + username + " does not have access to No/None Org.");
                }
            }
            
            // find SIRConfig 
            
            SIRConfigVO dbSIRConfigVO = findSIRConfig(sirConfigVO.getSys_id(), sirConfigVO.getSysOrg(), username, true);
            
            Set<CustomDataFormTabVO> newCustomDataFormTabVOs = sirConfigVO.getCustomDataFormTabs();
            Map<String, CustomDataFormTabVO> newCustomDataFormTabNameToVOMap = new HashMap<String, CustomDataFormTabVO>();
            
            if (newCustomDataFormTabVOs != null && !newCustomDataFormTabVOs.isEmpty())
            {
                for (CustomDataFormTabVO newCustomDataFormTabVO : newCustomDataFormTabVOs)
                {
                    newCustomDataFormTabNameToVOMap.put(newCustomDataFormTabVO.getUName(), newCustomDataFormTabVO);
                }
            }
            
            sirConfigVO.setCustomDataFormTabs(null);
            
            if (dbSIRConfigVO == null)
            {
                sirConfigVO.setSysCreatedBy(username);
                sirConfigVO.setSysCreatedOn(GMTDate.getDate());
                sirConfigVO.setSysUpdatedBy(null);
                sirConfigVO.setSysUpdatedOn(null);
                sirConfigVO.setSysIsDeleted(Boolean.FALSE);
                sirConfigVO.setSysModCount(VO.NON_NEGATIVE_INTEGER_DEFAULT);
            }
            else
            {
                sirConfigVO.setSys_id(dbSIRConfigVO.getSys_id());
                sirConfigVO.setSysCreatedBy(dbSIRConfigVO.getSysCreatedBy());
                sirConfigVO.setSysCreatedOn(GMTDate.getDate(dbSIRConfigVO.getSysCreatedOn()));
                sirConfigVO.setSysUpdatedBy(username);
                sirConfigVO.setSysUpdatedOn(GMTDate.getDate());
                sirConfigVO.setSysIsDeleted(dbSIRConfigVO.getSysIsDeleted());
                sirConfigVO.setSysModCount(Integer.valueOf(dbSIRConfigVO.getSysModCount().intValue() + 1));
                sirConfigVO.setCustomDataFormTabs(dbSIRConfigVO.getCustomDataFormTabs());
            }
            
            SIRConfig sirConfig = new SIRConfig(sirConfigVO);
            
            try
            {
              HibernateProxy.setCurrentUser(username);
            	savedSIRConfig = (SIRConfig) HibernateProxy.execute(() ->
                	HibernateUtil.getDAOFactory().getSIRConfigDAO().persist(sirConfig));                
            }
            catch(Throwable t)
            {                
                Log.log.error("Error " + t.getLocalizedMessage() + 
                              " occurred while saving SIRConfig [" + sirConfigVO + "].", t );
                              HibernateUtil.rethrowNestedTransaction(t);
            }
                        
            if (savedSIRConfig != null && StringUtils.isNotBlank(savedSIRConfig.getSys_id()))
            {
                savedSIRConfigVO = savedSIRConfig.doGetVO();
            }
            
            Map<String, CustomDataFormTabVO> customDataFormTabVOsToDelete = new HashMap<String, CustomDataFormTabVO>();
            
            if (savedSIRConfigVO.getCustomDataFormTabs() != null && !savedSIRConfigVO.getCustomDataFormTabs().isEmpty())
            {
                for (CustomDataFormTabVO customDataFormTabVO : savedSIRConfigVO.getCustomDataFormTabs())
                {
                    customDataFormTabVOsToDelete.put(customDataFormTabVO.getSys_id(), customDataFormTabVO);
                    
                    // Check if any of the name (case sensitive) matches with existing one and if so set the sys_id so as to update
                    
                    if (newCustomDataFormTabNameToVOMap.containsKey(customDataFormTabVO.getUName()))
                    {
                        CustomDataFormTabVO newCusomDataFormTabVO = newCustomDataFormTabNameToVOMap.get(customDataFormTabVO.getUName());
                        newCusomDataFormTabVO.setSys_id(customDataFormTabVO.getSys_id());
                    }
                }
            }
            
            Map<String, CustomDataFormTabVO> customDataFormTabVOsToUpdate = new HashMap<String, CustomDataFormTabVO>();
            Set<CustomDataFormTabVO> customDataFormTabVOsToAdd = new HashSet<CustomDataFormTabVO>();
            
            // Update new CustomDataFormTabVOs with SIRConfig
            
            if (newCustomDataFormTabVOs != null && !newCustomDataFormTabVOs.isEmpty())
            {
                for (CustomDataFormTabVO newCustomDataFormTabVO : newCustomDataFormTabVOs)
                {
                    newCustomDataFormTabVO.setSysOrg(savedSIRConfigVO.getSysOrg());
                    newCustomDataFormTabVO.setSirConfig(savedSIRConfigVO);
                    
                    if (StringUtils.isBlank(newCustomDataFormTabVO.getSys_id()))
                    {
                        // New
                        customDataFormTabVOsToAdd.add(newCustomDataFormTabVO);
                    }
                    else
                    {
                        // Existing
                        if (customDataFormTabVOsToDelete.containsKey(newCustomDataFormTabVO.getSys_id()))
                        {
                            newCustomDataFormTabVO.setSysCreatedBy(customDataFormTabVOsToDelete.get(newCustomDataFormTabVO.getSys_id()).getSysCreatedBy());
                            newCustomDataFormTabVO.setSysCreatedOn(customDataFormTabVOsToDelete.get(newCustomDataFormTabVO.getSys_id()).getSysCreatedOn());
                            newCustomDataFormTabVO.setSysModCount(Integer.valueOf(customDataFormTabVOsToDelete.get(newCustomDataFormTabVO.getSys_id()).getSysModCount().intValue()));
                            customDataFormTabVOsToUpdate.put(newCustomDataFormTabVO.getSys_id(), newCustomDataFormTabVO);
                            customDataFormTabVOsToDelete.remove(newCustomDataFormTabVO.getSys_id());
                        }
                        else
                        {
                            customDataFormTabVOsToAdd.add(newCustomDataFormTabVO);
                        }
                    }
                }
            }
            
            if (!customDataFormTabVOsToDelete.isEmpty() || !customDataFormTabVOsToUpdate.isEmpty() ||
                !customDataFormTabVOsToAdd.isEmpty())
            {
                try
                {
                  HibernateProxy.setCurrentUser(username);
                    HibernateProxy.execute(() -> {
                    
	                    // Process Deletes
	                    
	                    if (!customDataFormTabVOsToDelete.isEmpty())
	                    {
	                        for (CustomDataFormTabVO customDataFormTabVOToDelete : customDataFormTabVOsToDelete.values())
	                        {
	                            CustomDataFormTab customDataFormTabToDelete = new CustomDataFormTab(customDataFormTabVOToDelete);
	                            HibernateUtil.getDAOFactory().getCustomDataFormTabDAO().delete(customDataFormTabToDelete);
	                        }
	                    }
	                    
	                    // Process Updates
	                    
	                    if (!customDataFormTabVOsToUpdate.isEmpty())
	                    {
	                        for (CustomDataFormTabVO customDataFormTabVOToUpdate : customDataFormTabVOsToUpdate.values())
	                        {
	                            CustomDataFormTab customDataFormTabToUpdate = new CustomDataFormTab(customDataFormTabVOToUpdate);
	                            
	                            customDataFormTabToUpdate.setSysUpdatedBy(username);
	                            customDataFormTabToUpdate.setSysUpdatedOn(GMTDate.getDate());
	                            customDataFormTabToUpdate.setSysModCount(Integer.valueOf(customDataFormTabToUpdate.getSysModCount().intValue() + 1));
	                            
	                            HibernateUtil.getDAOFactory().getCustomDataFormTabDAO().update(customDataFormTabToUpdate);
	                        }
	                    }
	                    
	                    // Process Adds
	                    
	                    if (!customDataFormTabVOsToAdd.isEmpty())
	                    {
	                        for (CustomDataFormTabVO customDataFormTabVOToAdd : customDataFormTabVOsToAdd)
	                        {
	                            customDataFormTabVOToAdd.setSysCreatedBy(username);
	                            customDataFormTabVOToAdd.setSysCreatedOn(GMTDate.getDate());
	                            customDataFormTabVOToAdd.setSysUpdatedBy(null);
	                            customDataFormTabVOToAdd.setSysUpdatedOn(null);
	                            customDataFormTabVOToAdd.setSysIsDeleted(Boolean.FALSE);
	                            customDataFormTabVOToAdd.setSysModCount(VO.NON_NEGATIVE_INTEGER_DEFAULT);
	                            
	                            CustomDataFormTab customDataFormTabToAdd = new CustomDataFormTab(customDataFormTabVOToAdd);
	                            HibernateUtil.getDAOFactory().getCustomDataFormTabDAO().persist(customDataFormTabToAdd);
	                        }
	                    }
                    
                    });
                }
                catch(Throwable t)
                {                    
                    sirConfigVO.setCustomDataFormTabs(newCustomDataFormTabVOs);
                    Log.log.error("Error " + t.getLocalizedMessage() + 
                                  " occurred while saving SIRConfig [" + sirConfigVO + "] CustomDataFormTabs.", t );
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
                
                try
                {
                    // Get updated SIRConfig
                    
                    savedSIRConfigVO = findSIRConfig(savedSIRConfigVO.getSys_id(), savedSIRConfigVO.getSysOrg(), username, false);
                }
                catch(Throwable t)
                {
                    HibernateUtil.rethrowNestedTransaction(t);
                    sirConfigVO.setCustomDataFormTabs(newCustomDataFormTabVOs);
                    Log.log.error("Error " + t.getLocalizedMessage() + 
                                  " occurred while getting updated SIRConfig after saving CustomDataFormTabs for SIRConfig [" + sirConfigVO + "].", t );
                }
                
                if (savedSIRConfig != null && StringUtils.isNotBlank(savedSIRConfig.getSys_id()) &&
                    savedSIRConfigVO == null)
                {
                    savedSIRConfigVO = savedSIRConfig.doGetVO();
                }
            }
        }
        
        return savedSIRConfigVO;
    }
    
    @SuppressWarnings("unchecked")
    public static SIRConfigVO findSIRConfig(String sirConfigSysId, String orgId, String username, boolean lock) throws Exception
    {
        SIRConfig sirConfig = null;
        
        if (StringUtils.isNotBlank(sirConfigSysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	sirConfig = (SIRConfig) HibernateProxy.execute(() -> {
                
	                if (lock)
	                {
	                    return HibernateUtil.getDAOFactory().getSIRConfigDAO().findById(sirConfigSysId, lock);
	                }
	                else
	                {
	                    return HibernateUtil.getDAOFactory().getSIRConfigDAO().findById(sirConfigSysId);
	                }
                
            	});
            }
            catch(Throwable t)
            {
                Log.log.error("Error " + t.getLocalizedMessage() + 
                              " occurred while getting SIRConfig for sys_id " + sirConfigSysId + ".", t );
                throw t;
            }
        }
        else
        {
            try
            {
                QueryDTO sirConfigQueryDTO = new QueryDTO();
                
                sirConfigQueryDTO.setModelName("SIRConfig");
                
                QueryFilter sirQueryOrgFilter = StringUtils.isNotBlank(orgId) ? 
                                                new QueryFilter("string", orgId, "sysOrg", QueryFilter.EQUALS, true) :
                                                new QueryFilter("string", "NULL", "sysOrg", QueryFilter.EQUALS, true);
                                                
                sirConfigQueryDTO.addFilterItem(sirQueryOrgFilter);
                
                sirConfigQueryDTO.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, Boolean.FALSE.toString(),
                                                                "sysIsDeleted", QueryFilter.EQUALS));
                SIRConfig sirConfigFinal = sirConfig;
                
              HibernateProxy.setCurrentUser(username);
                sirConfig = (SIRConfig) HibernateProxy.execute(() -> {
                	Query<SIRConfig> sirConfigQuery = HibernateUtil.createQuery(sirConfigQueryDTO);
                    
                    List<SIRConfig> sirConfigs = sirConfigQuery.list();
                    
                    if (sirConfigs != null && sirConfigs.size() > 0)
                    {
                        return sirConfigs.get(0);                    
                    }
                    
                    return sirConfigFinal;
                });
            }
            catch(Throwable t)
            {
                Log.log.error("Error " + t.getLocalizedMessage() + 
                              " occurred while getting SIRConfig for " + 
                              (StringUtils.isNotBlank(orgId) ? " Org sys_id " + orgId + "." : "None Org."), t );
                throw t;
            }
        }
        
        if (sirConfig != null && !sirConfig.getSysIsDeleted().booleanValue())
        {
            return sirConfig.doGetVO(true);
        }
        else
        {
            return null;
        }
    }

	public static PBNotes saveNote(String noteJson, String username) throws Exception {
		PBNotes pbNote = sanitizeNote(noteJson);

		ResolveSecurityIncidentVO incidentVO = getSecurityIncident(pbNote.getIncidentId(), null, username);
		
		v2LicenseViolationsAndEnvCheck(NOTE_INTO_SIR);
		
		if (incidentVO != null && "Closed".equalsIgnoreCase(incidentVO.getStatus())) {
    		throw new Exception(ERROR_CLOSED_INCIDENT);
        }

		pbNote = indexNote(pbNote, username);
		touchSIR(username, pbNote.getIncidentId());
		return pbNote;
	}
	
	public static ResponseDTO<SecurityIncident> searchSecurityIncidents(String text, String username)
			throws Exception {
        String filterCondition = getOrgsFilterCondition(username);

        QueryDTO dto = new QueryDTO();
		dto.addFilterItem(new QueryFilter("auto", text, SECURITY_INCIDENT_FT_BOOST_COLUMNS, QueryFilter.FULLTEXT, true));
		dto.addFilterItem(new QueryFilter("auto", filterCondition, "sys_org", QueryFilter.CONTAINS, true));
		
		return PlaybookSearchAPI.searchSecurityIncident(dto, username);
	}

	public static ResponseDTO<SecurityIncident> searchSecurityIncidents(QueryDTO dto, String username)
			throws Exception {
		ResponseDTO<SecurityIncident> response = new ResponseDTO<SecurityIncident>();
        if (dto.getFilterItems() != null && !dto.getFilterItems().isEmpty()) {
        	boolean hasOrgFilter = false;

            for (QueryFilter qryFltr : dto.getFilterItems()) {
    			if (INCIDENTS_INDEX_FILEDS_MAP.containsKey(qryFltr.getField())) {
    				qryFltr.setField(INCIDENTS_INDEX_FILEDS_MAP.get(qryFltr.getField()));
    			}

                if ("sys_org".equalsIgnoreCase(qryFltr.getField())) {
                	hasOrgFilter = true;
                	String orgValue = qryFltr.getValue();
                	
                	if (orgValue == null || orgValue.isEmpty() || "nil".equalsIgnoreCase(orgValue)) {
                		if (UserUtils.isNoOrgAccessible(username)) {
                			qryFltr.setValue(NULL_VALUE_CONDITION);
                			qryFltr.setCondition(QueryFilter.FULLTEXT);
                		} else {
                	        return response.setSuccess(false).setMessage(NO_ACCESS_TO_ORG);
                		}
                	} else {
                		Collection<String> orgIdList = UserUtils.getUserOrgHierarchyIdList(username);
                		if (orgIdList.contains(orgValue)) {
                			qryFltr.setValue(orgValue);
                			qryFltr.setCondition(QueryFilter.EQUALS);
                		} else {
                	        return response.setSuccess(false).setMessage(NO_ACCESS_TO_ORG);
                		}
                	}
                }
            }

            if (!hasOrgFilter) {
            	return response.setSuccess(false).setMessage(NO_ORG_FILTER);
            }

    		for (QuerySort sort: dto.getSortItems()) {
    			String sortField = sort.getProperty();
				Field field = SecurityIncident.class.getDeclaredField(sortField);
    			
    			if (INCIDENTS_INDEX_FILEDS_MAP.containsKey(sortField)) {
    				sortField = INCIDENTS_INDEX_FILEDS_MAP.get(sort.getProperty());
    			}

				if (String.class.getName().equals(field.getType().getName())) {
					sortField = String.join(".", sortField, "keyword");
				}

				sort.setProperty(sortField);
    		}

    		if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL, "")) {
    			dto.addFilterItem(new QueryFilter("auto", username, "members,owner", QueryFilter.CONTAINS, true));
    		}

    		return PlaybookSearchAPI.searchSecurityIncident(dto, username);
        } 

       	return response.setSuccess(false).setMessage(EMPTY_SEARCH_CRITERIA);
	}

    public static String getSIRId(String username, String problemId) {

      String result = null;

      /*
       * This API returns only the SIR Id based on problem id.
 	   * To access SIR user will still have to be either owner 
 	   * or team member or SIR should be duplicate and user
 	   * has access to master SIR.
       */
//      if (!UserUtils.getUserfunctionPermission(username, Constants.INVESTIGATION_VIEW, Constants.VIEW_ACCESS)) {
//          return result;
//      }

      if (StringUtils.isNotBlank(problemId)) {
          try {
              
                HibernateProxy.setCurrentUser(username);
        	  ResolveSecurityIncident sirResult = (ResolveSecurityIncident) HibernateProxy.execute(() -> {
        		  CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
                  CriteriaQuery<ResolveSecurityIncident> criteriaQuery = criteriaBuilder
                          .createQuery(ResolveSecurityIncident.class);
                  Root<ResolveSecurityIncident> from = criteriaQuery.from(ResolveSecurityIncident.class);
                  criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("problemId"), problemId));

                  return HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();
        	  });
        	  
              if (sirResult != null) {
                result = sirResult.getSys_id();
              }
          } catch (Throwable e) {
              Log.log.warn(e.getMessage(), e);
                                                HibernateUtil.rethrowNestedTransaction(e);
          }
      }

      return result;
  }

	private static String getOrgsFilterCondition(String username) throws Exception {
		Collection<String> orgIdList = UserUtils.getUserOrgHierarchyIdList(username);
        String filterCondition = UserUtils.isNoOrgAccessible(username) ? NULL_VALUE_CONDITION : "";

        if (!orgIdList.isEmpty()) {
        	String orgCondition = orgIdList.stream().map(x -> x).collect(Collectors.joining(OR_SEARCH_CONDITION));
        	if (StringUtils.isNotEmpty(filterCondition)) {
        		filterCondition = String.join(OR_SEARCH_CONDITION, orgCondition, filterCondition);
        	}
        }

        return filterCondition;
	}

	public static void touchSIR(String username, String incidentId) throws Exception {
		String sir = incidentId;
		try {
			ResolveSecurityIncidentVO incidentVO = getSecurityIncident(incidentId, null, username);
			sir = incidentVO.getSir();

        HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {

				ResolveSecurityIncident updatedSIR = new ResolveSecurityIncident();
	
				incidentVO.setSysUpdatedBy(username);
				incidentVO.setSysUpdatedOn(new GregorianCalendar(TimeZone.getTimeZone("GMT")).getTime());
	
				updatedSIR.applyVOToModel(incidentVO);
	
				HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().update(updatedSIR);

			});
		} catch (Throwable t) {
			Log.log.error(String.format("Failed to update SIR %s", sir), t);			

			throw t;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResponseDTO<Map<String, Object>> getSIRActivityCountByStatusReport(String orgId, 
    		String username, ConfigSQL configSQL) throws Exception
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        QueryDTO countByStatusQuery = new QueryDTO();

        if (StringUtils.isNotBlank(orgId))
	    {
	        if (!orgId.equalsIgnoreCase(VO.STRING_DEFAULT) &&
	            !orgId.equalsIgnoreCase(Constants.NIL_STRING))
	        {
	            if (!UserUtils.isOrgAccessible(orgId, username, false, false))
	            {
	                throw new Exception ("User does not have permission to access selected Org.");
	            }
	        }
	        else
	        {
	            if (!UserUtils.isNoOrgAccessible(username))
	            {
	                throw new Exception ("User does not have permission to access 'None' Org.");
	            }
	        }
	    }
	    else
	    {
	        if (!UserUtils.isNoOrgAccessible(username))
            {
                throw new Exception ("User does not have permission to access No Org 'None'.");
            }
	    }
        
        // Check RBAC SIR Incident (Investigation Viewer).Playbook.Task.Status View permissions
        
        if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_TASK_STATUS_CHANGE, ""))
        {
        	return result;
        }
        
        String sqlQueryPrefix = "select sard.u_status, count(sard.sys_id) from sir_activity_runtimedata sard, " +
        						"resolve_security_incident rsi where sard.sys_is_deleted != 'Y' and " +
        						"rsi.sys_is_deleted != 'Y' and " +
        						"sard.u_resolve_security_incident_id = rsi.sys_id and " +
        						"rsi.u_master_sir is null ";
        String sqlQuerySuffix = "group by sard.u_status order by count(sard.sys_id) desc";
        
        StringBuilder filterSb = new StringBuilder();
        
        Map<String, String> filterByAssignees = new HashMap<String, String>();
        
        buildFilterQuery(username, "sard.u_assignee", filterSb, filterByAssignees);

        Map<String, String> filterByOrgs = new HashMap<String, String>();
        String orgFilters = getOrgHierarchy(orgId, ",", username);
        buildFilterQuery(orgFilters, "sard.sys_org", filterSb, filterByOrgs);

        String countByQuerySql = sqlQueryPrefix + (StringUtils.isNotBlank(filterSb.toString()) ? "and " + filterSb.toString() : "") + sqlQuerySuffix;
        
        countByStatusQuery.setSqlQuery(countByQuerySql);
        countByStatusQuery.setUseSql(true);
        
        Map<String, Object> countByStatusQueryParams = new HashMap<String, Object>();
        
        setQueryParams(filterByAssignees, countByStatusQueryParams);
        setQueryParams(filterByOrgs, countByStatusQueryParams);
        
        countByStatusQuery.setParams(countByStatusQueryParams);
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                Query query = HibernateUtil.createSQLQuery(countByStatusQuery.getSelectSQLNonParameterizedSort());

                for (String key : countByStatusQueryParams.keySet())
                {
                    query.setParameter(key, countByStatusQueryParams.get(key));
                }

                List<Object[]> data = query.list();
                
                if(data != null)
                {
                    for (Object[] aRow : data)
                    {
                        HashMap<String, Object> record = new HashMap<String, Object>();
                        String dbType = configSQL.getDbtype().toUpperCase();
                        if (dbType.equals("MYSQL")) {
                            record.put((String)aRow[0], (BigInteger)aRow[1]);
                        } else {
                            record.put((String)aRow[0], (BigDecimal)aRow[1]);
                        }
                        
                        records.add(record);
                    }
                    result.setTotal(data.size());
                }
                
                result.setRecords(records);
                result.setSuccess(true);
                
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getLocalizedMessage() + " in getting data for sql:" + countByStatusQuery.getSqlQuery(), e);
            
            
            result.setSuccess(false).setMessage("Error in getting data. Please check the logs.");
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }

	public static List<SecurityIncidentLightDTO> searchAssociatedSir(String sir, String username) throws Exception {
		if (StringUtils.isBlank(sir)) {
    		throw new Exception("Empty incident provided");
		}

		ResolveSecurityIncidentVO originalVO = getSecurityIncident(null, sir, username);
		
		ResponseDTO<PBArtifacts> response = PlaybookSearchAPI.searchPBArtifactsBySir(sir, username, null);
		List<PBArtifacts> list = response.getRecords();

		ResponseDTO<PBArtifacts> artifactResponse = new ResponseDTO<>();
		List<SecurityIncidentLightDTO> incidents = new ArrayList<>();
		Set<String> incidentIds = new HashSet<>();

		for (PBArtifacts artifact : list) {
			artifactResponse = PlaybookSearchAPI.searchAllArtifacts(artifact.getName(), artifact.getValue(), username);

			if (artifactResponse.getRecords() != null) {
				for (PBArtifacts a : artifactResponse.getRecords()) {
					// Skip artifacts that belong to original incident
					if (StringUtils.isNotEmpty(a.getIncidentId()) && !sir.equalsIgnoreCase(a.getSir())) {
						ResolveSecurityIncidentVO incidentVO = getSecurityIncidentById(a.getIncidentId(), username);

						// Check that found incident org is in the same root as original incident,
						// skip duplicate incidents and make sure incident is put into the response only once.
						if (incidentVO != null 
								&& VO.STRING_DEFAULT.equalsIgnoreCase(incidentVO.getMasterSir())
								&& hasNoOrgOrSameRoot(incidentVO.getSysOrg(), originalVO.getSysOrg())
								&& !incidentIds.contains(incidentVO.getSys_id())) {
							incidentIds.add(incidentVO.getSys_id());
							incidents.add(convertSecurityIncidentVOToDTO(incidentVO));
						}
					}
				}
			}
		}

		return incidents;
	}

    public static ResolveSecurityIncidentVO markAsDuplicateIncident(String masterId, String duplicateId, String username, HttpServletRequest request) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_MARKINCIDENT_ASDUPLICATE, null))
        {
            throw new Exception(String.format("User %s is denied access to mark case as duplicate of another master case.", username));
        }
	    
	    ResolveSecurityIncidentVO incidentVo = getSecurityIncident(masterId, null, username);
        
        if (incidentVo == null)
        {
            throw new Exception("Could not find incident for SIR with sys_id: " + masterId);
        }

		if (StringUtils.isNotEmpty(masterId))
		{
			if (masterId.equalsIgnoreCase(duplicateId))
			{
				throw new Exception("Master and duplicate SIR are the same: " + incidentVo.getSir());
			}
		}
		
        checkUsersSIROrgAccess(incidentVo, username);
        
        incidentVo = getSecurityIncident(duplicateId, null, username);
        
        if (incidentVo == null)
        {
            throw new Exception("Could not find incident for SIR with sys_id: " + duplicateId);
        }
        
        checkUsersSIROrgAccess(incidentVo, username);        
        
        
        
		try
        {

          HibernateProxy.setCurrentUser(username);
			return (ResolveSecurityIncidentVO) HibernateProxy.execute(() -> {
				final ResolveSecurityIncident master;
		        ResolveSecurityIncident duplicate = null;
				
	        	master = HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(masterId);
	        	duplicate = HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(duplicateId);
	        
	        	if (master.getDuplicates().contains(duplicate))
	        	{
	        		throw new Exception("Master already has incident as its duplicate. Master: " + master.getSir() + ", Duplicate: " + duplicate.getSir());
	        	}
	        	
	        	if (duplicate.isDuplicate())
	        	{
	        		throw new Exception("Incident is already a duplicate and not allowed marked as duplicate of another master. Master: " + master.getSir() + ", Duplicate: " + duplicate.getSir());
	        	}

	        	if (master.isDuplicate())
	        	{
	        		throw new Exception("Master incident is a duplicate itself and can't add an incident as duplicate. Master: " + master.getSir() + ", Duplicate: " + duplicate.getSir());
	        	}
	        	
	        	if (hasNoOrgOrSameRoot(master.getSysOrg(), duplicate.getSysOrg()))
	        	{
	            	final ResolveSecurityIncident finalMaster =  master;
	        		Collection<ResolveSecurityIncident> childDups = new ArrayList<ResolveSecurityIncident>(duplicate.getDuplicates());
	        		childDups.parallelStream().forEach(p -> { finalMaster.addDuplicate(p); String msg = String.format("Incident %s is marked as a duplicate of Incident %s .", p.getSir(), finalMaster.getSir()); auditAction(msg, finalMaster.getSys_id(), request.getRemoteAddr(), username); auditAction(msg, p.getSys_id(), request.getRemoteAddr(), username);} );
	                master.addDuplicate(duplicate);
	        	}
	        	else
	        	{
	        		throw new Exception("Master and duplicate do not have common root Org. Master: " + master.getSir() + ", Duplicate: " + duplicate.getSir());
	        	}
	        	

	    		ResolveSecurityIncidentVO masterIncidentVO = master.doGetVO();
	    		
	    		masterIncidentVO.setAuditMessage("Incident " + duplicate.getSir() + " marked as duplicate of (current) master incident " + master.getSir() + ".");

	    		return masterIncidentVO;
	        	
			});


        }
		catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
		
	}

    public static ResolveSecurityIncidentVO removeDuplicateIncident(String masterId, String duplicateId, String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_MARKINCIDENT_ASDUPLICATE, null))
        {
            throw new Exception(String.format("User %s is denied access to remove case as duplicate of master case.", username));
        }
	    
	    ResolveSecurityIncidentVO incidentVo = getSecurityIncident(masterId, null, username);
        
        if (incidentVo == null)
        {
            throw new Exception("Could not find incident for SIR with sys_id: " + masterId);
        }
        
        checkUsersSIROrgAccess(incidentVo, username);
        
        incidentVo = getSecurityIncident(duplicateId, null, username);
        
        if (incidentVo == null)
        {
            throw new Exception("Could not find incident for SIR with sys_id: " + duplicateId);
        }
        
        checkUsersSIROrgAccess(incidentVo, username);  
		try
        {

          HibernateProxy.setCurrentUser(username);
			return (ResolveSecurityIncidentVO) HibernateProxy.execute(() -> {
				ResolveSecurityIncident master = null;
			    ResolveSecurityIncident duplicate = null;
				master = HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(masterId);
	        	duplicate = HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(duplicateId);
	            
	        	if (hasNoOrgOrSameRoot(master.getSysOrg(), duplicate.getSysOrg()))
	        	{
	        		master.removeDuplicate(duplicate);
	        	}
	        	else
	        	{
	        		throw new Exception("Master and duplicate don't have common Org. Master: " + masterId + ", Duplicate: " + duplicateId);
	        	}
	        	
	        	ResolveSecurityIncidentVO masterIncidentVO = master.doGetVO();
	    		
	    		masterIncidentVO.setAuditMessage("Case " + duplicate.getSir() + " removed as duplicate of (current) master case " + master.getSir() + ".");
	    		
	    		return masterIncidentVO;
			});
        	

        }
		catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
		
		
	}

    public static boolean hasNoOrgOrSameRoot(String masterSysOrg, String duplicateSysOrg) throws Exception
    {
        boolean result = false;

        if (StringUtils.isNotBlank(masterSysOrg) && StringUtils.isNotBlank(duplicateSysOrg))
        {
            if (masterSysOrg.equalsIgnoreCase(duplicateSysOrg))
            {
                result = true;
            }
            else
            {
                result = OrgsUtil.hasCommonRoot(masterSysOrg, duplicateSysOrg);
            }
        }
        else if (StringUtils.isBlank(masterSysOrg))
        {
        	if (StringUtils.isBlank(duplicateSysOrg))
        	{
        		result = true;
        	}
        	else
        	{
        		OrgsVO orgsVO = UserUtils.getOrg(duplicateSysOrg, null);
                if (orgsVO != null)
                {
                    if (orgsVO.getUHasNoOrgAccess().booleanValue())
                    {
                    	result = true;
                    }
                }
        	}
        }

        return result;
    }

	public static String buildRiskScoreCondition(QueryDTO queryDTO, String userVisibilityCondition) {
		
		String riskScoreAndUserVisibilityCondition = userVisibilityCondition;
		
		if (queryDTO != null && queryDTO.getFilters() != null && !queryDTO.getFilters().isEmpty()) {
			List<QueryFilter> queryFilters = queryDTO.getFilters();
			
			List<QueryFilter> riskScoreFilters = 
								queryFilters.stream().
								filter(queryFilter -> 
									   queryFilter.getField().equals(ResolveSecurityIncident.RISK_SCORE_FIELD_LABLE)).
								collect(Collectors.toList());
			
			// Compose Risk Score where clause from individual Risk Score filters
			
			if (riskScoreFilters != null && !riskScoreFilters.isEmpty()) {
				StringBuilder riskScoreWhereClauseSb = new StringBuilder();
				
				riskScoreFilters.forEach(riskScoreFilter -> {
					
					if (riskScoreWhereClauseSb.length() > 0) {
						riskScoreWhereClauseSb.append(" ").append(QueryDTO.OR_OPERATOR).append(" ");
					}
					
					String riskScoreFilterWhereClause = riskScoreFilter.getWhereClause("tableData");
					
					riskScoreFilterWhereClause = riskScoreFilterWhereClause.replace(
														":" + riskScoreFilter.getField() + "_0", 
														Integer.toString(
															   Integer.parseInt(riskScoreFilter.getValue(), 10), 10));
					
					riskScoreWhereClauseSb.append(riskScoreFilterWhereClause);
				});
				
				riskScoreAndUserVisibilityCondition = " (" + 
													  (StringUtils.isNotBlank(userVisibilityCondition) ?
													   "(" + userVisibilityCondition + ") and (" + 
													   riskScoreWhereClauseSb.toString() + ")" :
													   riskScoreWhereClauseSb.toString()) + ") ";
				
				// Remove the Risk Score filters from queryDTO
				
				riskScoreFilters.forEach(riskScoreFilter->queryDTO.getFilters().remove(riskScoreFilter));
			}
		}
		
		return riskScoreAndUserVisibilityCondition;			
	}

    public static ResponseDTO<ResolveSecurityIncidentVO> listIncidentDuplicates(QueryDTO dto, String masterId, 
    																			String username) throws Exception {
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();

		List<ResolveSecurityIncidentVO> incidentList = new ArrayList<ResolveSecurityIncidentVO>();
		
        int limit = dto.getLimit();
        int offset = dto.getStart();
        dto.setModelName(SIR_MODEL_NAME);
        
        /*
         * List of master SIR duplicates incidents(if any) is accessible if user has access to master SIR.
         * Please note it is just the listing only and not details of duplicate SIRs.
         * Access to duplicate SIR (if any) details is still controlled by user's access to duplicate SIR's Org 
         */
	    ResolveSecurityIncidentVO incidentVo = getSecurityIncident(masterId, null, username);
        
        if (incidentVo == null) {
            throw new Exception("Could not find Master SIR incident, sys_id: " + masterId);
        }
        
        checkUsersSIROrgAccess(incidentVo, username);

        if (StringUtils.isBlank(dto.getSort())) {
        	ArrayList<QuerySort> sorts = new ArrayList<QuerySort>();
        	sorts.add(new QuerySort("sysCreatedOn", SortOrder.DESC));
        	dto.setSortItems(sorts);
        }
        
        dto.addFilterItem(new QueryFilter("auto", masterId, "masterSir.id", QueryFilter.EQUALS));
        dto.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, "false|null", "sysIsDeleted", QueryFilter.EQUALS));        
        
		List<? extends Object> data = null;
        data = GeneralHibernateUtil.executeHQLSelectModel(dto, offset, limit, true);
        if (data != null && data.size() > 0) {
        	for (Object obj : data) {
        		ResolveSecurityIncident resolveSecurityIncident = (ResolveSecurityIncident) obj;
        		ResolveSecurityIncidentVO incidentVO = resolveSecurityIncident.doGetVO();
        		
        		if (incidentVO != null) {
        			incidentList.add(incidentVO);
        		}
        	}
        }

		int total = ServiceHibernate.getTotalHqlCount(dto);
		result.setSuccess(true).setRecords(incidentList).setTotal(total);
		return result;
	}

    public static List<ResolveSecurityIncidentVO> listIncidentDuplicates(String masterId, String username) throws Exception
	{
		List<ResolveSecurityIncidentVO> result = new ArrayList<ResolveSecurityIncidentVO>();
		
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL, null))
        {
			throw new Exception(String.format(DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username));
        }
	    
	    ResolveSecurityIncidentVO incidentVo = getSecurityIncident(masterId, null, username);
        
        if (incidentVo == null)
        {
            throw new Exception("Could not find incident for SIR with sys_id: " + masterId);
        }
        
        checkUsersSIROrgAccess(incidentVo, username);
        
        ResolveSecurityIncident master = null;
        
		try
        {
          HibernateProxy.setCurrentUser(username);
			master = (ResolveSecurityIncident) HibernateProxy.execute(() -> {

				return HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(masterId);
				
			});
            
        	if (master==null)
        	{
        		throw new Exception("Failed to retrive master SIR: " + masterId);
        	}
        	
        
        	Collection<ResolveSecurityIncident> dups = master.getDuplicates();
        	
        	for (ResolveSecurityIncident i : dups)
        	{
        		ResolveSecurityIncidentVO vo = i.doGetVO();
        		result.add(vo);
        	}

        }
		catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
		
		return result;
	}

    @SuppressWarnings({ "deprecation", "unchecked" })
	public static void checkAllRequiredActivitiesCompleted(String  incidentId, String sir) throws Exception
    {
		List<SIRActivityRuntimeData> acts = null;
		try {
			acts = (List<SIRActivityRuntimeData>) HibernateProxy.execute(() -> {

				return (List<SIRActivityRuntimeData>) HibernateProxy.execute(() -> {
					return HibernateUtil.getCurrentSession().createCriteria(SIRActivityRuntimeData.class)
							.add(Restrictions.eq("referencedRSI.id", incidentId))
							.setFetchMode("activityMetaData", FetchMode.EAGER).list();
				});
			});
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
			throw e;
		}
    	
    	Optional<SIRActivityRuntimeData> opt = acts.stream().filter(p -> p.getActivityMetaData().getIsRequired() && !"Complete".equalsIgnoreCase(p.getStatus()) && !"Complete With Errors".equalsIgnoreCase(p.getStatus())).findFirst();
    	if (opt.isPresent())
    	{
    		SIRActivityRuntimeData sard = opt.get();
			throw new Exception(String.format("%s %s. Incident %s can not be closed.", COMPLETE_REQUIRED_ACTIVITIES, sard.getActivityMetaData().getName(), sir));
    	}
    }
	
    public static Pair<List<ResolveSecurityIncidentVO>, String> updateSecurityIncidents(SIRBulkUpdatePayloadDTO payload, String username, HttpServletRequest request) throws Exception
    {
        String errorMessage = "";

        List<String> ids = payload.getSysIds();
        if (ids == null || ids.isEmpty())
        {
            throw new Exception("No incidents provided to update.");
        }

        if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_CASECREATION_CREATE, null))
        {
            throw new Exception(String.format("User %s is denied access to perform SIR bulk update.", username));
        }
        
        String msgObj = String.format(SIRS_PROPERTY, payload.getPropertyName().getName(), payload.getSysIds().size());
        
        v2LicenseViolationsAndEnvCheck(msgObj);
        
        List<ResolveSecurityIncident> incidents = getSecurityIncidentsByIds(ids, username);
        Set<String> organizationIds = incidents.stream().map(ResolveSecurityIncident::getSysOrg).collect(toSet());

        String newPropertyValue = payload.getValue();

        List<String> orgsNotAllowed = new ArrayList<>();

        for (String organizationId : organizationIds)
        {
            Optional<String> res = checkUserOrgNotAllowedToAccessSir(organizationId, username);
            if (res.isPresent())
            {
                orgsNotAllowed.add(organizationId);
                errorMessage += res.get();
            }
        }

        incidents = incidents.stream().filter(i -> !orgsNotAllowed.contains(i.getSysOrg())).collect(Collectors.toList());
        List<ResolveSecurityIncident> incidentsToUpdate = new ArrayList<>();

        switch (payload.getPropertyName())
        {
            case SEVERITY:
            {
                if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_SEVERITY_CHANGE, null))
                {
                    String warnMsg = String.format("User %s %s modify incident severity.", username, RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER);
                    throw new Exception(warnMsg);
                }

                for (ResolveSecurityIncident incident : incidents)
                {
                    String auditMessage = "";
                    ResolveSecurityIncidentVO incidentVo = incident.doGetVO();
                    if (!newPropertyValue.equals(incidentVo.getSeverity()))
                    {
                        auditMessage += StringUtils.isBlank(incidentVo.getSeverity()) ? String.format("Incident '%s' received Severity of '%s' ", incidentVo.getSir(), newPropertyValue) : String.format("Incident '%s' Severity changed from '%s' to '%s' ", incidentVo.getSir(), incidentVo.getSeverity(), newPropertyValue);
                        incidentVo.setSeverity(newPropertyValue);
                    }
                    incidentVo.setAuditMessage(auditMessage);
            		auditAction(auditMessage, incidentVo.getSys_id(), request.getRemoteAddr(), username);
                    incident.applyVOToModel(incidentVo);
                    incidentsToUpdate.add(incident);
                }
            }
                ;
                break;
            case OWNER:
            {
                if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_OWNER_CHANGE, null))
                {
                    String warnMsg = String.format("User %s %s modify incident owner.", username, RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER);
                    throw new Exception(warnMsg);
                }

                for (ResolveSecurityIncident incident : incidents)
                {
                    String auditMessage = "";
                    ResolveSecurityIncidentVO incidentVo = incident.doGetVO();
                    if (!newPropertyValue.equals(incidentVo.getOwner()))
                    {
				    	Set<String> members = StringUtils.stringToSet(incidentVo.getMembers(), ",");

                        if (StringUtils.isBlank(incidentVo.getOwner()))
                        {
                            incidentVo.setAssignmentDate(new Date());
                            auditMessage += String.format("Incident '%s' received new Owner '%s' ", incidentVo.getSir(), newPropertyValue);
                        }
                        else
                        {
                            incidentVo.setAssignmentDate(new Date());
                            auditMessage += String.format("Incident '%s' Owner changed from '%s' to '%s' ", incidentVo.getSir(), incidentVo.getOwner(), newPropertyValue);
                        }
                        members.add(newPropertyValue);
                        incidentVo.setMembers(StringUtils.setToString(members, ","));
                        incidentVo.setOwner(newPropertyValue);
                    }
                    incidentVo.setAuditMessage(auditMessage);
            		auditAction(auditMessage, incidentVo.getSys_id(), request.getRemoteAddr(), username);
                    incident.applyVOToModel(incidentVo);
                    incidentsToUpdate.add(incident);
                }
            }
                ;
                break;
            case STATUS:
            {
                if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_STATUS_CHANGE, null))
                {
                    String warnMsg = String.format("User %s %s modify investigation status.", username, RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER);
                    throw new Exception(warnMsg);
                }

                Map<String, String> notCompleted = null;
                if (SIR_STATUS_CLOSED.equals(newPropertyValue))
                { 
                    notCompleted = getSirsWithNotCompletedMandatoryActivities(ids, username);
                }

                for (ResolveSecurityIncident incident : incidents)
                {
                    String auditMessage = "";
                    ResolveSecurityIncidentVO incidentVo = incident.doGetVO();
                    String oldStatus = incidentVo.getStatus();

                    if (!newPropertyValue.equals(oldStatus))
                    {
                       if (SIR_STATUS_CLOSED.equals(newPropertyValue))
                        {
                            if(notCompleted.containsKey(incidentVo.getId())) {
                                errorMessage += String.format(
                                                "Sir %s can't be closed. All required tasks/activities need to be completed first: %s. ",
                                                incidentVo.getSir(), notCompleted.get(incidentVo.getSys_id()));
                                continue;
                            } else {
                                incidentVo.setSirStarted(false);
                                incidentVo.setClosedBy(username);
                                incidentVo.setClosedOn(new Date());
                                PlaybookActivityUtils.addPlaybookActivitySIRRefCounts(incidentVo.getSys_id(), 
                                													  incidentVo.getPlaybook(), 
                                													  incidentVo.getPlaybookVersion(), -1,
                                													  incidentVo.getSysOrg(),
                                													  username);
                                auditMessage += String.format("Incident '%s' status changed to Closed ", incidentVo.getSir());
                            }
                        }
                        else
                        {
                            auditMessage += StringUtils.isBlank(oldStatus) ? 
                            				String.format("Incident '%s' received Status of '%s' ", incidentVo.getSir(), 
                            							  newPropertyValue) : 
                            				String.format("Incident '%s' Status changed from '%s' to '%s' ", incidentVo.getSir(), oldStatus, newPropertyValue);
                            incidentVo.setClosedBy(null);
                            incidentVo.setClosedOn(null);
                        }
                        if (newPropertyValue.equals("In Progress"))
                        {
                            incidentVo.setSirStarted(true);
                            PlaybookActivityUtils.addPlaybookActivitySIRRefCounts(incidentVo.getSys_id(), 
                            													  incidentVo.getPlaybook(), 
                            													  incidentVo.getPlaybookVersion(), 1,
                            													  incidentVo.getSysOrg(),
                            													  username);
                        }
                        incidentVo.setStatus(newPropertyValue);
                    }
                    incidentVo.setAuditMessage(auditMessage);
            		auditAction(auditMessage, incidentVo.getSys_id(), request.getRemoteAddr(), username);
                    incident.applyVOToModel(incidentVo);
                    incidentsToUpdate.add(incident);
                }
            }
                ;
                break;
            case MASTER_ID:
            {
                if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENT_MARKINCIDENT_ASDUPLICATE, null))
                {
                    throw new Exception(String.format("User %s is denied access to mark case as duplicate of another master case.", username));
                }

                ResolveSecurityIncident masterIncident = null;
                try
                {
                  HibernateProxy.setCurrentUser(username);
                	masterIncident = (ResolveSecurityIncident) HibernateProxy.execute( ()->
                		HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(newPropertyValue)
                    );
                }
                catch (Exception e)
                {
                    throw e;
                }

                if (masterIncident == null)
                {
                    throw new Exception(String.format(COULD_NOT_FIND_MASTER_SIR_ERR_MSG, newPropertyValue));
                }

                checkUsersSIROrgAccess(masterIncident.doGetVO(), username);

                for (ResolveSecurityIncident incident : incidents)
                {
                    ResolveSecurityIncidentVO incidentVo = incident.doGetVO();

                    if (newPropertyValue.equals(incident.getSys_id()))
                    {
                        errorMessage += String.format("Cannot mark incident %s as duplicate of itself. ", incident.getSir());
                        continue;
                    }

                    if (masterIncident.getDuplicates().contains(incident))
                    {
                        errorMessage += String.format("Master %s already has incident %s as its duplicate. ", masterIncident.getSir(), incident.getSir());
                        continue;
                    }

                    if (incident.isDuplicate())
                    {
                        errorMessage += String.format("Incident %s is already a duplicate. Not allowed to mark as duplicate of a different master. ", incident.getSir());
                        continue;
                    }

                    if (hasNoOrgOrSameRoot(masterIncident.getSysOrg(), incidentVo.getSysOrg()))
                    {
                    	final ResolveSecurityIncident finalMaster =  masterIncident;
                		Collection<ResolveSecurityIncident> childDups = new ArrayList<ResolveSecurityIncident>(incident.getDuplicates());
                		
                		childDups.parallelStream().forEach(p -> { finalMaster.addDuplicate(p); String msg = String.format("Incident %s is marked as a duplicate of Incident %s .", p.getSir(), finalMaster.getSir()); auditAction(msg, finalMaster.getSys_id(), request.getRemoteAddr(), username); auditAction(msg, p.getSys_id(), request.getRemoteAddr(), username);} );
                		
                        masterIncident.addDuplicate(incident);
                        incidentVo = incident.doGetVO();
                        incidentsToUpdate.addAll(childDups);                
                    }
                    else
                    {
                        errorMessage += String.format("Master %s and duplicate %s do not have common root Org. ", masterIncident.getSir(), incident.getSir());
                        continue;
                    }
                    
                    String auditMsg = String.format("Incident %s marked as duplicate of (current) master incident %s .", incident.getSir(), masterIncident.getSir());
                    incidentVo.setAuditMessage(auditMsg);
                    auditAction(auditMsg, masterIncident.getSys_id(), request.getRemoteAddr(), username); 
                    auditAction(auditMsg, incident.getSys_id(), request.getRemoteAddr(), username);
                    incident.applyVOToModel(incidentVo);
                    incidentsToUpdate.add(incident);
                }
                incidentsToUpdate.add(masterIncident);

            }
                ;
                break;
            default:
                throw new Exception(String.format(PROPERTY_NOT_SUPPORTED_FOR_UPDATE_SIR_ERR_MSG, payload.getPropertyName()));
        }
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(()->
            	HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().update(incidentsToUpdate)
            );

            return Pair.of(incidentsToUpdate.stream().map(i -> i.doGetVO()).collect(Collectors.toList()), errorMessage.trim());
        }
        catch (Exception e)
        {
            if (StringUtils.isNotBlank(e.getMessage()) && 
            	e.getMessage().contains(RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER) || 
            	e.getMessage().contains(ERROR_CLOSED_INCIDENT) ||
            	e.getMessage().startsWith(COULD_NOT_FIND_ERR_MSG_PREFIX) ||
            	e.getMessage().contains(NOT_SUPPORTED_FOR))
            {
                Log.log.warn("Failed to save security incident: " + e.getMessage());
            }
            else
            {
                Log.log.error("Failed to save security incident: " + e.getMessage(), e);
            }
            
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
	private static List<ResolveSecurityIncident> getSecurityIncidentsByIds(List<String> ids, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (List<ResolveSecurityIncident>) HibernateProxy.execute(() -> {
        		CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();

                CriteriaQuery<ResolveSecurityIncident> criteriaQuery = criteriaBuilder.createQuery(ResolveSecurityIncident.class);
                Root<ResolveSecurityIncident> from = criteriaQuery.from(ResolveSecurityIncident.class);

                criteriaQuery.select(from).where(from.get("sys_id").in(ids));

                List<ResolveSecurityIncident> result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();

                return result;
        	});
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private static Optional<String> checkUserOrgNotAllowedToAccessSir(String organizationId, String username)
    {
        if (StringUtils.isNotBlank(organizationId))
        {
            if (!UserUtils.isOrgAccessible(organizationId, username, false, false))
            {
                Optional.of(String.format("User %s does not belong to Organization %s. ", username, organizationId));
            }
        }
        else
        {
            if (!UserUtils.isNoOrgAccessible(username))
            {
                Optional.of(String.format("User %s does not have access to No/None Org. ", username));
            }
        }
        return Optional.empty();
    }
    
    private static ResolveSecurityIncidentVO getSecurityIncidentById(String incidentId, String username) {
    	ResolveSecurityIncidentVO vo = null;
		try {


        HibernateProxy.setCurrentUser(username);
			ResolveSecurityIncident securityIncident = (ResolveSecurityIncident) HibernateProxy.execute(() -> {
				return HibernateUtil.getDAOFactory().getResolveSecurityIncidentDAO().findById(incidentId);
			});

			if (securityIncident != null) {
				vo = securityIncident.doGetVO();
			}

		} catch (Throwable e) {
            Log.log.warn(e.getMessage(), e);
                                      HibernateUtil.rethrowNestedTransaction(e);
        }
		return vo;
    }

    @SuppressWarnings("unchecked")
	private static Map<String, String> getSirsWithNotCompletedMandatoryActivities(List<String>  ids, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            return (Map<String, String>) HibernateProxy.execute(() -> {
            	@SuppressWarnings("unchecked")
                List<SIRActivityRuntimeData> queryResult = HibernateUtil.getCurrentSession()
                                .createQuery(MANDATORY_ACTIVITIES_NOT_IN_STATUSES_BY_SIR_IDS_HQL_QUERY)
                .setParameterList("ids", ids)
                .setParameter("statuses", ACTIVITY_COMPLETED_STATUSES)
                .getResultList();
                Map<String, String> result = queryResult
                                .stream()
                                .collect(
                                    Collectors.toMap(
                                        sard -> sard.getReferencedRSI().getSys_id(),
                                        sard -> sard.getActivityMetaData().getName(),
                                        (s,a)-> a));
                
                return result;
                
            });           
            
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private static SecurityIncidentLightDTO convertSecurityIncidentVOToDTO(ResolveSecurityIncidentVO vo) {
    	SecurityIncidentLightDTO dto = new SecurityIncidentLightDTO();
    	dto.setSys_id(vo.getSys_id());
    	dto.setInvestigationType(vo.getInvestigationType());
    	dto.setSeverity(vo.getSeverity());
    	dto.setSir(vo.getSir());
    	dto.setStatus(vo.getStatus());
    	dto.setSysOrg(vo.getSysOrg());
    	dto.setTitle(vo.getTitle());
    	
    	return dto;
    }
    
    public static final void v2LicenseViolationsAndEnvCheck(String msgSubstitution) throws Exception {
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndNonProd() &&
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = null;
    		
    		if (MainBase.main.getLicenseService().getCurrentLicense().isExpired()) {
    			msg = ERR.E10024.getMessage();
        		
        		if (StringUtils.isNotBlank(msgSubstitution)) {
        			msg = msg.replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, msgSubstitution);
        		}
        		
        		Log.alert(ERR.E10024.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
    			throw new Exception(msg);
    		}
    		
    		if (MainBase.main.getLicenseService().getIsHAViolated()) {
    			msg = ERR.E10030.getMessage();
        		
        		if (StringUtils.isNotBlank(msgSubstitution)) {
        			msg = msg.replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, msgSubstitution);
        		}
        		
        		Log.alert(ERR.E10030.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
    			throw new Exception(msg);
    		}
    		
    		if (MainBase.main.getLicenseService().getIsUserCountViolated()) {
    			msg = ERR.E10031.getMessage();
        		
        		if (StringUtils.isNotBlank(msgSubstitution)) {
        			msg = msg.replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, msgSubstitution);
        		}
        		
        		Log.alert(ERR.E10031.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
    			throw new Exception(msg);
    		}
    	}
    }
    
    public static final void expiredV2LicenseAndEnvCheck(String msgSubstitution) throws Exception {
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndExpiredAndNonProd() &&
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10024.getMessage();
    		
    		if (StringUtils.isNotBlank(msgSubstitution)) {
    			msg = msg.replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, msgSubstitution);
    		}
    		
    		Log.alert(ERR.E10024.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    }
    
    public static final void haViolatedV2LicenseAndEnvCheck(String msgSubstitution) throws Exception {
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndNonProd() &&
    		MainBase.main.getLicenseService().getIsHAViolated() &&	
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10030.getMessage();
    		
    		if (StringUtils.isNotBlank(msgSubstitution)) {
    			msg = msg.replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, msgSubstitution);
    		}
    		
    		Log.alert(ERR.E10030.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    }
    
    public static final void userCountViolatedV2LicenseAndEnvCheck(String msgSubstitution) throws Exception {
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndNonProd() &&
    		MainBase.main.getLicenseService().getIsUserCountViolated() &&	
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10031.getMessage();
    		
    		if (StringUtils.isNotBlank(msgSubstitution)) {
    			msg = msg.replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, msgSubstitution);
    		}
    		
    		Log.alert(ERR.E10031.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    }
    
    public static List<ResolveSecurityIncidentVO> getSecurityIncidents(String keyFieldName, Collection<String> keys, 
    																   String username, String orgId) throws Exception {
    	if (StringUtils.isBlank(keyFieldName) || CollectionUtils.isEmpty(keys)) {
    		throw new IllegalArgumentException("Passed KeyFieldName is blank or list of keys passed is null or empty");
    	}
    	
		/*
		 * User's view access to incident i.e whether user has access to all
		 * SIRs or is restricted to SIRs owned by or is member of team, 
		 * should be based on legacy sirInvestigationListViewMode "all" permission 
		 * or new sir.dashboard.incidentList.viewAll permission.
		 */
		
		final List<ResolveSecurityIncidentVO> securityIncidentVOs = new CopyOnWriteArrayList<ResolveSecurityIncidentVO>();

		try {
			final List<ResolveSecurityIncident> sirs = new ArrayList<>();
			
			HibernateUtil.actionNoCacheHibernateInitLocked(hibernateUtil -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<ResolveSecurityIncident> criteriaQuery = criteriaBuilder
						.createQuery(ResolveSecurityIncident.class);
				Root<ResolveSecurityIncident> from = criteriaQuery.from(ResolveSecurityIncident.class);
				Predicate pred = criteriaBuilder.and(from.get(keyFieldName).in(keys), from.get("sysOrg").isNull());
				
				if (orgId != null && !"".equals(orgId) && !"nil".equalsIgnoreCase(orgId)) {
	    	        Set<OrgsVO> childOrgs = UserUtils.getAllOrgsInHierarchy(orgId, username);
	    	        List<String> orgs = childOrgs.stream().map(OrgsVO::getSys_id).collect(Collectors.toList());
					pred = criteriaBuilder.and(from.get(keyFieldName).in(keys),	from.get("sysOrg").in(orgs));
				}
				
				/*
				 * Check if user has permission to view all SIRs or only those 
				 * user is owner or team member of.
				 */
				
				Predicate finalPred = pred;
				
				if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL, "")) {
					List<Predicate> ownerTeamMemberPredicates = new ArrayList<Predicate>();
					
					// SIR owner = username predicate
					ownerTeamMemberPredicates.add(criteriaBuilder.equal(from.get("owner"), username));
					
					// Member of team predicates
					
					// in the middle and with or without prefixed with space i.e. member like %, username,% or member like %,username,%
					ownerTeamMemberPredicates.add(criteriaBuilder.like(from.<String>get("member"), "%, " + username + ",%"));
					ownerTeamMemberPredicates.add(criteriaBuilder.like(from.<String>get("member"), "%," + username + ",%"));
					
					// first i.e. member like username,%
					ownerTeamMemberPredicates.add(criteriaBuilder.like(from.<String>get("member"), username + ",%"));
					
					// last with or without prefixed with space i.e member like %, username or member like %,username
					ownerTeamMemberPredicates.add(criteriaBuilder.like(from.<String>get("member"), "%, " + username));
					ownerTeamMemberPredicates.add(criteriaBuilder.like(from.<String>get("member"), "%," + username));
					
					// single  i.e member = username
					ownerTeamMemberPredicates.add(criteriaBuilder.equal(from.get("member"), username));
					
					Predicate ownerOrTeamMemberPredicate = criteriaBuilder.or(ownerTeamMemberPredicates.toArray(new Predicate[0]));
					
					finalPred = criteriaBuilder.and(pred, ownerOrTeamMemberPredicate);
				}
				
				criteriaQuery.select(from).where(finalPred);
	
				sirs.addAll(HibernateUtil.getCurrentSession().createQuery(criteriaQuery).list());
			}, username);
			
			if (CollectionUtils.isNotEmpty(sirs)) {
				sirs.parallelStream().forEach(sir -> {
					if (sir != null) {
						try {
							ResolveSecurityIncidentVO sirVO = sir.doGetVO();
							securityIncidentVOs.add(sirVO);
						} catch (Throwable t) {
							Log.log.warn(String.format("Error %sin converting %s to VO", 
													   (StringUtils.isNotBlank(t.getMessage()) ? 
														"[" + t.getMessage() + "] " : ""), sir.getSir()));
						}
					}
				});
			}
		} catch (Throwable t) {
			Log.log.warn(String.format("Error %swhile getting security incidents by key field name %s for %d keys by user " +
									   "%s%s", 
					   				   (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
					   				   keyFieldName, keys.size(), username, 
									   (StringUtils.isNotBlank(orgId) ? 
										" under " + (OrgsUtil.findOrgById(orgId)).getUName() : "")));
			throw new Exception(t);
		}

		return securityIncidentVOs;
	}
    
    public static Map<String, Pair<Set<String>, Set<String>>> getSIRAndPrimarySIRIdsForWorksheetIds(
    																String keyFieldName, Collection<String> keys, 
    																String username, 
    																String orgId) throws Exception {
    	final Map<String, Pair<Set<String>, Set<String>>> wsIdToSIRAndPSIRIds = new ConcurrentHashMap<>();

    	List<ResolveSecurityIncidentVO> sirVOs = getSecurityIncidents(keyFieldName, keys, username, orgId);

    	if (CollectionUtils.isNotEmpty(sirVOs)) {
    		sirVOs.parallelStream().forEach(sirVO -> {
    			if (sirVO != null && StringUtils.isNotEmpty(sirVO.getProblemId()) && 
    				StringUtils.isNotEmpty(sirVO.getSys_id())) {
    				Pair<Set<String>, Set<String>> sirAndPSIRIncidentIds = wsIdToSIRAndPSIRIds.get(sirVO.getProblemId());

    				if (sirAndPSIRIncidentIds == null) {
    					Set<String> sirIds = new ConcurrentSkipListSet<String>();
    					Set<String> pSIRIds = new ConcurrentSkipListSet<String>();
    					sirAndPSIRIncidentIds = Pair.of(sirIds, pSIRIds);
    					wsIdToSIRAndPSIRIds.put(sirVO.getProblemId(), sirAndPSIRIncidentIds); 
    				}

    				sirAndPSIRIncidentIds.getLeft().add(sirVO.getSys_id());
    				
    				if (MapUtils.isNotEmpty(sirVO.getDuplicates())) {
    					sirAndPSIRIncidentIds.getRight().add(sirVO.getSys_id());
    				}
    			}
    		});
    	}

    	return wsIdToSIRAndPSIRIds;
    }
        
    public static final List<String> identifySIRsUpdatedAfterExpiry(long expiryTime, 
    																List<String> sirWorksheetIds,
    																String username) throws Exception {
    	final List<String> sirUpdatedAfterExpiryWsIds = new CopyOnWriteArrayList<String>();
    	
    	try {
    		final List<ResolveSecurityIncident> updatedSIRs = new ArrayList<>();
    		
    		HibernateUtil.actionNoCacheHibernateInitLocked(hibernateUtil -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<ResolveSecurityIncident> criteriaQuery = criteriaBuilder
																	   .createQuery(ResolveSecurityIncident.class);
				Root<ResolveSecurityIncident> from = criteriaQuery.from(ResolveSecurityIncident.class);
				
				Predicate pred = criteriaBuilder.and(from.get("problemId").in(sirWorksheetIds),
													 criteriaBuilder.greaterThan(from.get("sysUpdatedOn").as(Date.class), 
															 					 new Date(expiryTime)));
				
				criteriaQuery.select(from).where(pred);
	
				updatedSIRs.addAll(HibernateUtil.getCurrentSession().createQuery(criteriaQuery).list());;
				
				if (CollectionUtils.isNotEmpty(updatedSIRs)) {
					updatedSIRs.parallelStream().forEach(updatedSIR -> {
						if (updatedSIR != null && StringUtils.isNotBlank(updatedSIR.getProblemId())) {
							sirUpdatedAfterExpiryWsIds.add(updatedSIR.getProblemId());
						}
					});
				}
    		}, username);
    		
    		if (CollectionUtils.isNotEmpty(updatedSIRs)) {
				updatedSIRs.parallelStream().forEach(updatedSIR -> {
					if (updatedSIR != null && StringUtils.isNotBlank(updatedSIR.getProblemId())) {
						sirUpdatedAfterExpiryWsIds.add(updatedSIR.getProblemId());
					}
				});
			}
		} catch (Throwable t) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			
			Log.log.warn(String.format("Error %sin identifying SIRs updated after %s (%d ms) for %d worksheet ids by user %s", 
									   (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
									   sdf.format(new Date(expiryTime)), expiryTime, sirWorksheetIds.size(), username), t);
			throw new Exception(t);
		}
    	
    	return sirUpdatedAfterExpiryWsIds;    	
    }
}
