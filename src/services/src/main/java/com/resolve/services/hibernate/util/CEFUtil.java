package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.criterion.Restrictions;

import com.resolve.persistence.model.ArtifactConfiguration;
import com.resolve.persistence.model.ArtifactType;
import com.resolve.persistence.model.CEFDictionaryItem;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ArtifactConfigurationVO;
import com.resolve.services.hibernate.vo.CEFDictionaryItemVO;
import com.resolve.util.Log;

public class CEFUtil {
	protected static final String U_SHORT_NAME = "UShortName";

	public static List<CEFDictionaryItemVO> getAllCEFDictionary() {
		List<CEFDictionaryItemVO> cefVOs = new ArrayList<CEFDictionaryItemVO>();
		try {
			HibernateProxy.execute(() -> {
				List<CEFDictionaryItem> result = HibernateUtil.getDAOFactory().getCEFDictionaryDAO().findAll();
				for (CEFDictionaryItem cef : result) {
					cefVOs.add(convertToCEFDictionaryVO(cef));
				}
			});		
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return cefVOs;
	}

	public static CEFDictionaryItemVO getCEFDictionaryByShortName(String shortName) {
		
		try {
			return (CEFDictionaryItemVO) HibernateProxy.execute(() -> {
				CEFDictionaryItemVO cefVO = null;
				CEFDictionaryItem result = (CEFDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CEFDictionaryItem.class)
						.add(Restrictions.eq(U_SHORT_NAME, shortName).ignoreCase()).uniqueResult();
				if (result != null) {
					cefVO =	convertToCEFDictionaryVO(result);
				}
				return cefVO;
			});

		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
			return null;
		}
	}

	public static Set<ArtifactConfigurationVO> getCEFItemArtifactConfigurations(String shortName) {
		Set<ArtifactConfigurationVO> configVOs = new HashSet<>();
		try {
			HibernateProxy.execute(() -> {
				CEFDictionaryItem result = (CEFDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CEFDictionaryItem.class)
						.add(Restrictions.eq(U_SHORT_NAME, shortName).ignoreCase()).uniqueResult();
				if (result != null) {
					ArtifactType artifactType = result.getArtifactType();
					if (artifactType != null) {
						Set<ArtifactConfiguration> configs = artifactType.getArtifactConfigurations();
						if (configs != null) {
							for (ArtifactConfiguration config : configs) {
								configVOs.add(config.doGetVO());
							}
						}
					}
				}
			});		
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return configVOs;
	}
	
	protected static CEFDictionaryItemVO convertToCEFDictionaryVO(CEFDictionaryItem cef) {
		CEFDictionaryItemVO cefVO = new CEFDictionaryItemVO();
		cefVO.setUShortName(cef.getUShortName());
		cefVO.setUFullName(cef.getUFullName());
		cefVO.setUDataType(cef.getUDataType());
		cefVO.setULength(cef.getULength());
		cefVO.setUDescription(cef.getUDescription());
		cefVO.setSys_id(cef.getSys_id());
		cefVO.setSysCreatedBy(cef.getSysCreatedBy());
		cefVO.setSysCreatedOn(cef.getSysCreatedOn());
		cefVO.setSysIsDeleted(cef.getSysIsDeleted());
		cefVO.setSysModCount(cef.getSysModCount());
		cefVO.setSysOrg(cef.getSysOrg());
		cefVO.setSysUpdatedBy(cef.getSysUpdatedBy());
		cefVO.setSysUpdatedOn(cef.getSysUpdatedOn());
		return cefVO;
	}
}
