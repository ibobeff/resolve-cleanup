/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.sir;

public enum NISTSIRPhasesEnum {
    PREPARATION("Preparation", "Preparation"),
    DETECTTIONANDANALYSIS("Detection & Analysis", "Detection and Analysis"),
    CONTAINMENTERADICATIONANDRECOVERY("Containiment Eradication & Recovery", "Containment, Eradication, and Recovery"),
    POSTINCIDENTACTIVITY("Post-Incident Activity", "Post-Incident Activity");
    
    public static final String NIST_STANDARDS = "NIST";
    private static final String COLON = ":";
    private static final String PERIOD = ".";
        
    private final String phaseName;
    private final String uIPhaseName;
    private final String persistenceId;
    
    NISTSIRPhasesEnum(String phaseName, String uIPhaseName) {
        this.phaseName = phaseName;
        this.uIPhaseName = uIPhaseName;
        
        String tmpPersistenceId = NIST_STANDARDS + COLON + phaseName;
        
        if (tmpPersistenceId.length() > 32) {
            tmpPersistenceId = tmpPersistenceId.substring(0, 28) + PERIOD + PERIOD + PERIOD;
        }
        
        this.persistenceId = tmpPersistenceId;
    }
    
    public String getPhaseName() {
        return phaseName;
    }
    
    public String getUIPhaseName() {
        return uIPhaseName;
    }
    
    public String getPersistenceId() {
        return persistenceId;
    }
}
