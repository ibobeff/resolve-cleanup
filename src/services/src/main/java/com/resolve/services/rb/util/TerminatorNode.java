/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

public abstract class TerminatorNode extends Common
{
    public static final int width = 30;
    private static final int height = 30;

    private Params params;
    
    public TerminatorNode(int id, String label, int x, int y, String inputs, String outputs, String merge)
    {
        super(id, label, "", x, y, width, height, merge);
        params = new Params(inputs, outputs);
    }

    public Params getParams()
    {
        return params;
    }

    public void setParams(Params params)
    {
        this.params = params;
    }
}
