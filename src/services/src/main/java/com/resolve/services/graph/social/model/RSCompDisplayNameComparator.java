/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model;

import java.util.Comparator;

import com.resolve.util.StringUtils;

@Deprecated
public class RSCompDisplayNameComparator implements Comparator<Object>
{

    public int compare(Object obj1, Object obj2)
    {
        int value = 0;
        
        if(obj1!=null && obj2!=null && obj1 instanceof RSComponent && obj2 instanceof RSComponent)
        {
            String displayName1 = StringUtils.isNotEmpty(((RSComponent)obj1).getDisplayName()) ? ((RSComponent)obj1).getDisplayName() : "";
            String displayName2 = StringUtils.isNotEmpty(((RSComponent)obj2).getDisplayName()) ? ((RSComponent)obj2).getDisplayName() : "";
            
            value = displayName1.compareTo(displayName2);
        }
        
        return value;
    }

}
