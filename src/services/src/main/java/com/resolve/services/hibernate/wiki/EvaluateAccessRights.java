/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikiDocumentMetaFormRel;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.StoreUtility;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.util.StringUtils;

public class EvaluateAccessRights
{
    public static void updateAccessRightsFor(WikiDocument doc, String username) throws Exception
    {
        if(doc == null)
        {
            throw new Exception("Document object cannot be null");
        }
        
        //make sure that the name of the document is valid
        String docFullName = doc.getUFullname();
//        WikiUtils.validateWikiDocName(docFullName);

        //get the namespace
        String namespace = docFullName.split("\\.")[0];

        //get list of wiki properties
        List<PropertiesVO> lProps = PropertiesUtil.getPropertiesLikeName(ConstantValues.PROP_WIKI_RIGHTS);

        //evaluate the ar
        AccessRights ar = evaluateAccessRights(doc.getAccessRights(), lProps, namespace, doc.ugetUIsDefaultRole());
        
        //set it back to document
        doc.setAccessRights(ar);
        
    }
    
    public static void updateAccessRightsFor(ResolveActionTask task, String username) throws Exception
    {
        if(task == null)
        {
            throw new Exception("Actiontask cannot be null");
        }
        
        //make sure that the name of the document is valid
        String atFullName = task.getUFullName();

        //get the namespace - ssh#rsqa
        String namespace = atFullName.split("#")[1];

        //get list of wiki properties
        List<PropertiesVO> lProps = PropertiesUtil.getPropertiesLikeName(ConstantValues.PROP_ACTIONTASK_RIGHTS);

        //evaluate the ar
        boolean defaultRole = task.ugetUIsDefaultRole();//true;
//        if(StringUtils.isNotEmpty(task.getSys_id()))
//        {
//            defaultRole = false;
//        }
        AccessRights ar = evaluateAccessRights(task.getAccessRights(), lProps, namespace, defaultRole);
        
        //set it to the at
        task.setAccessRights(ar);
    }
    
    public static void updateAccessRightsFor(WikiDocumentMetaFormRel model, String username) throws Exception
    {
        if(model == null)
        {
            throw new Exception("Template Model cannot be null");
        }
        
        //evaluate the ar
        boolean defaultRole = model.ugetUIsDefaultRole();
        
        AccessRights ar = evaluateAccessRights(model.getAccessRights(), defaultRole);
        
        //set it to the template
        model.setAccessRights(ar);
    }
    
    public static AccessRights evaluateAccessRights(AccessRights ar, List<PropertiesVO> lProps, String namespace, boolean defaultRole)
    {
        if(ar == null)
        {
            //for insert, it may be null
            ar = new AccessRights();
        }
        
        //local variables
        String readRoles = "";
        String writeRoles = "";
        String adminRoles = "";
        String executeRoles = "";

        //prepare the role sets - admin must be there
        Set<String> wikiDocReadAccess = new TreeSet<String>();
        wikiDocReadAccess.add("admin");
        Set<String> wikiDocWriteAccess = new TreeSet<String>();
        wikiDocWriteAccess.add("admin");
        Set<String> wikiDocAdminAccess = new TreeSet<String>();
        wikiDocAdminAccess.add("admin");
        Set<String> wikiDocExecuteAccess = new TreeSet<String>();
        wikiDocExecuteAccess.add("admin");
        
        if(defaultRole)
        {
            
            //get the default roles that are conf and just use that
            wikiDocReadAccess.addAll(StoreUtility.getRolesFor(RightTypeEnum.view, lProps, namespace));
            wikiDocWriteAccess.addAll(StoreUtility.getRolesFor(RightTypeEnum.edit, lProps, namespace));
            wikiDocAdminAccess.addAll(StoreUtility.getRolesFor(RightTypeEnum.admin, lProps, namespace));
            wikiDocExecuteAccess.addAll(StoreUtility.getRolesFor(RightTypeEnum.execute, lProps, namespace));
        }
        else
        {
            readRoles = StringUtils.isNotBlank(ar.getUReadAccess()) ? ar.getUReadAccess().trim() : "";
            writeRoles = StringUtils.isNotBlank(ar.getUWriteAccess()) ? ar.getUWriteAccess().trim() : "";
            adminRoles = StringUtils.isNotBlank(ar.getUAdminAccess()) ? ar.getUAdminAccess().trim() : "";
            executeRoles = StringUtils.isNotBlank(ar.getUExecuteAccess()) ? ar.getUExecuteAccess().trim() : "";
            
            wikiDocReadAccess.addAll(StringUtils.convertStringToList(readRoles, ","));
            wikiDocWriteAccess.addAll(StringUtils.convertStringToList(writeRoles, ","));
            wikiDocAdminAccess.addAll(StringUtils.convertStringToList(adminRoles, ","));
            wikiDocExecuteAccess.addAll(StringUtils.convertStringToList(executeRoles, ","));
        }

        //prepare the roles in string 
        readRoles = StringUtils.convertCollectionToString(wikiDocReadAccess.iterator(), ",");
        writeRoles = StringUtils.convertCollectionToString(wikiDocWriteAccess.iterator(), ",");
        adminRoles = StringUtils.convertCollectionToString(wikiDocAdminAccess.iterator(), ",");
        executeRoles = StringUtils.convertCollectionToString(wikiDocExecuteAccess.iterator(), ",");
        
        //update the ar object
        ar.setUReadAccess(readRoles);
        ar.setUWriteAccess(writeRoles);
        ar.setUAdminAccess(adminRoles);
        ar.setUExecuteAccess(executeRoles);
        
        return ar;
        
    }
    
    private static AccessRights evaluateAccessRights(AccessRights ar,  boolean defaultRole)
    {
        if(ar == null)
        {
            //for insert, it may be null
            ar = new AccessRights();
        }
        
        //local variables
        String readRoles = "";
        String writeRoles = "";
        String adminRoles = "";
        String executeRoles = "";

        //prepare the role sets - admin must be there
        Set<String> wikiDocReadAccess = new TreeSet<String>();
        wikiDocReadAccess.add("admin");
        Set<String> wikiDocWriteAccess = new TreeSet<String>();
        wikiDocWriteAccess.add("admin");
        Set<String> wikiDocAdminAccess = new TreeSet<String>();
        wikiDocAdminAccess.add("admin");
        Set<String> wikiDocExecuteAccess = new TreeSet<String>();
        wikiDocExecuteAccess.add("admin");
        
        if(defaultRole)
        {
            //get the default roles that are conf and just use that
            wikiDocReadAccess.addAll(PropertiesUtil.getPropertyList("wikitemplate.rights.read"));
            wikiDocWriteAccess.addAll(PropertiesUtil.getPropertyList("wikitemplate.rights.edit"));
            wikiDocAdminAccess.addAll(PropertiesUtil.getPropertyList("wikitemplate.rights.admin"));
//            wikiDocExecuteAccess.addAll(StoreUtility.getRolesFor(RightTypeEnum.execute, lProps, namespace));
        }
        else
        {
            readRoles = StringUtils.isNotBlank(ar.getUReadAccess()) ? ar.getUReadAccess().trim() : "";
            writeRoles = StringUtils.isNotBlank(ar.getUWriteAccess()) ? ar.getUWriteAccess().trim() : "";
            adminRoles = StringUtils.isNotBlank(ar.getUAdminAccess()) ? ar.getUAdminAccess().trim() : "";
            executeRoles = StringUtils.isNotBlank(ar.getUExecuteAccess()) ? ar.getUExecuteAccess().trim() : "";
            
            wikiDocReadAccess.addAll(StringUtils.convertStringToList(readRoles, ","));
            wikiDocWriteAccess.addAll(StringUtils.convertStringToList(writeRoles, ","));
            wikiDocAdminAccess.addAll(StringUtils.convertStringToList(adminRoles, ","));
            wikiDocExecuteAccess.addAll(StringUtils.convertStringToList(executeRoles, ","));
        }

        //prepare the roles in string 
        readRoles = StringUtils.convertCollectionToString(wikiDocReadAccess.iterator(), ",");
        writeRoles = StringUtils.convertCollectionToString(wikiDocWriteAccess.iterator(), ",");
        adminRoles = StringUtils.convertCollectionToString(wikiDocAdminAccess.iterator(), ",");
        executeRoles = StringUtils.convertCollectionToString(wikiDocExecuteAccess.iterator(), ",");
        
        //update the ar object
        ar.setUReadAccess(readRoles);
        ar.setUWriteAccess(writeRoles);
        ar.setUAdminAccess(adminRoles);
        ar.setUExecuteAccess(executeRoles);
        
        return ar;
    }
    

}
