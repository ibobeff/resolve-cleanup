/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.Map;

import org.hibernate.query.Query;

import com.resolve.persistence.dao.SSHConnectionPoolDAO;
import com.resolve.persistence.model.SSHPool;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SSHUtil
{
    @SuppressWarnings({ "unused", "rawtypes" })
    public static void setMSSHPools(Map<String, String> pools)
    {
        try
        {
        	HibernateProxy.execute(() -> {
        		 boolean initRemove = false;

                 SSHConnectionPoolDAO sshDAO = HibernateUtil.getDAOFactory().getSSHConnectionPoolDAO();

                 // update each filter
                 for (Object mapEntry : pools.entrySet())
                 {
                     Map.Entry entry = (Map.Entry) mapEntry;
                     String id = (String) entry.getKey();
                     String filterStringMap = (String) entry.getValue();
                     Map filterMap = StringUtils.stringToMap(filterStringMap);

                     // init values
                     String queue = (String) filterMap.get("QUEUE");
                     if (StringUtils.isEmpty(queue))
                     {
                         throw new Exception("Missing QUEUE name");
                     }

                     // remove all entries for queue
                     if (initRemove == false)
                     {
                         initRemove = true;

                         Query query = HibernateUtil.createQuery("DELETE FROM SSHPool WHERE UQueue = :UQueue");
                         query.setParameter("UQueue", queue);
                         query.executeUpdate();
                         HibernateUtil.getCurrentSession().flush();
                     }

                     //Boolean active = new Boolean((String) filterMap.get("ACTIVE"));
                     String subnetMask = (String) filterMap.get("SUBNETMASK");
                     Integer maxConn = new Integer((String) filterMap.get("MAXCONNECTION"));
                     Integer timeout = new Integer((String) filterMap.get("TIMEOUT"));

                     // init record entry with values
                     SSHPool exp = new SSHPool();
                     exp.setUQueue(queue);
                     SSHPool row = HibernateUtil.getDAOFactory().getSSHConnectionPoolDAO().findFirst(exp);
                     if (row == null)
                     {
                         row = new SSHPool();
                     }

                     row.setUQueue(queue);
                     row.setUSubnetMask(subnetMask);
                     //row.setUActive(active);
                     row.setUMaxConn(maxConn);
                     row.setUTimeout(timeout);

                     sshDAO.save(row);
                 }
        	});
           
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setFilters
}
