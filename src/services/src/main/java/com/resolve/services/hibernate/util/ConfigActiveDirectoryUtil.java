/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.model.ConfigActiveDirectory;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ConfigActiveDirectoryUtil
{
    public static List<ConfigActiveDirectoryVO> getConfigActiveDirectory(QueryDTO query, String username)
    {
        List<ConfigActiveDirectoryVO> result = new ArrayList<ConfigActiveDirectoryVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ConfigActiveDirectory instance = new ConfigActiveDirectory();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(convertModelToVO(instance));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ConfigActiveDirectory instance = (ConfigActiveDirectory) o;
                        result.add(convertModelToVO(instance));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static ConfigActiveDirectoryVO findConfigActiveDirectoryById(String sys_id, String username)
    {
        ConfigActiveDirectory model = null;
        ConfigActiveDirectoryVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findConfigActiveDirectoryModelById(sys_id);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = convertModelToVO(model);
        }
        
        return result;
    }
    
    public static ConfigActiveDirectoryVO saveConfigActiveDirectory(ConfigActiveDirectoryVO vo, String username)
    {
        if(vo != null)
        {
            ConfigActiveDirectory model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                model = findConfigActiveDirectoryModelById(vo.getSys_id());
            }
            else
            {
                model = new ConfigActiveDirectory();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveConfigActiveDirectory(model, username);
            
            //invalidate the cache by sending the JMS to the gateway
            sendJmsMessageToInvalidate(model);
            
            if(model.getUSsl())
            {
                sendMessageToImportSSLCertificate(model);
            }

            vo = convertModelToVO(model);
        }
        
        return vo;
    }
    
    public static ConfigActiveDirectoryVO findConfigActiveDirectoryByDomainName(String domainName)
    {
        ConfigActiveDirectoryVO result = null;
        
        if(StringUtils.isNotBlank(domainName))
        {
            ConfigActiveDirectory model = findConfigActiveDirectoryModelByDomainName(domainName);
            if(model != null)
            {
                result = convertModelToVO(model);
            }
        }
        
        return result;
    }
    
    public static void deleteConfigActiveDirectoryByIds(String[] sysIds, String username) throws Exception
    {
        if(sysIds != null)
        {
            for(String sysId : sysIds)
            {
                deleteConfigActiveDirectoryById(sysId, username);
            }
        }
    }
    
    private static void deleteConfigActiveDirectoryById(String sysId, String username) throws Exception
    {
        ConfigActiveDirectory model = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
            model = (ConfigActiveDirectory) HibernateProxy.execute(() -> {
            
            	ConfigActiveDirectory modelResult = HibernateUtil.getDAOFactory().getConfigActiveDirectoryDAO().findById(sysId);
	            if(modelResult != null)
	            {
	                HibernateUtil.getDAOFactory().getConfigActiveDirectoryDAO().delete(modelResult);
	            }
	            
	            return modelResult;
            });
            
            //invalidate the cache by sending the JMS to the gateway
            sendJmsMessageToInvalidate(model);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            
            throw new Exception(e); 
        }
    }
    
    
    //private apis
    private static ConfigActiveDirectory findConfigActiveDirectoryModelByDomainName(String domainName)
    {
        ConfigActiveDirectory result = null;
        if(StringUtils.isNotBlank(domainName))
        {
            ConfigActiveDirectory example = new ConfigActiveDirectory();
            example.setUDomain(domainName);
            
            try
            {
            	result = (ConfigActiveDirectory) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getConfigActiveDirectoryDAO().findFirst(example);
            	});
            }
            catch (Throwable e)
            {
                e.printStackTrace();
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static ConfigActiveDirectory findConfigActiveDirectoryModelById(String sys_id)
    {
        ConfigActiveDirectory result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
				result = (ConfigActiveDirectory) HibernateProxy.execute(() -> {
					return HibernateUtil.getDAOFactory().getConfigActiveDirectoryDAO().findById(sys_id);
				});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static ConfigActiveDirectoryVO convertModelToVO(ConfigActiveDirectory model)
    {
        ConfigActiveDirectoryVO vo = null;
        
        if(model != null)
        {
            vo = model.doGetVO();
            if(vo.getBelongsToOrganization() != null)
            {
                vo.setSysOrganizationName(vo.getBelongsToOrganization().getUOrganizationName());
            }
            else
            {
                vo.setSysOrganizationName("");
            }            
        }
        
        return vo;
    }
    
    private static void sendJmsMessageToInvalidate(ConfigActiveDirectory model)
    {
      //invalidate the cache by sending the JMS to the gateway
        String gateway = model.getUGateway();
        if(StringUtils.isNotBlank(gateway))
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.AUTH_DOMAIN, model.getUDomain());
            
            //jms message
            MainBase.getESB().sendMessage(gateway, "MAuth.invalidateDomain", params);
        }            
        
      //this is invalidate the RSVIEWS
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.AUTH_DOMAIN, model.doGetVO()); 
        
        MainBase.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.invalidateAD", params);
        
    }

    private static void sendMessageToImportSSLCertificate(ConfigActiveDirectory model)
    {
        //inform rsremote to import certificate from the AD server.
        String gateway = model.getUGateway();
        if(StringUtils.isNotBlank(gateway))
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.AUTH_LDAP_HOST, model.getUIpAddress());
            params.put(Constants.AUTH_LDAP_PORT, "" + model.getUPort());
            
            //message
            MainBase.getESB().sendMessage(gateway, "MAuth.importSSLCertificate", params);
        }  
    }
}
