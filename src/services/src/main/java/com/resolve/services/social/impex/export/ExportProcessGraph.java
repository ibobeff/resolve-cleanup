/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.export;

import java.util.Collection;
import java.util.List;

import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

public class ExportProcessGraph extends ExportComponentGraph
{
    public ExportProcessGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
        super(sysId, options, NodeType.PROCESS);
    }

    @Override
    protected List<GraphRelationshipDTO> exportRelationships(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception
    {
        Log.log.debug("Exporting Process:" + compNode.getUCompName() + "[" + compNode.getSys_id() + "]");
        //get all the nodes that this process node is refering too
        Collection<ResolveNodeVO> nodesThatThisProcessContains = ServiceGraph.getMemberNodes(compNode.getSys_id(), null, null, null, "admin");
        if (nodesThatThisProcessContains != null)
        {
            for (ResolveNodeVO nodeFollowingMe : nodesThatThisProcessContains)
            {
                prepareReturnNode(compNode, nodeFollowingMe, socialImpex);
            }//end of for loop
        }

        //for Users
        Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(compNode.getSys_id(), null, null, null, "admin");
        if (nodesFollowingMe != null)
        {
            for (ResolveNodeVO nodeFollowingMe : nodesFollowingMe)
            {
                NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                //if this a USER node, add to the list
                if (nodeFollowingMeType == NodeType.USER)
                {
                    prepareReturnNode(compNode, nodeFollowingMe, socialImpex);
                }
            }
        }

        return relationships;
    }

    private void prepareReturnNode(ResolveNodeVO compNode, ResolveNodeVO anyNode, SocialImpexVO socialImpex) throws Exception
    {
        NodeType type = anyNode.getUType();

        if (type == NodeType.FORUM)
        {
            if (options != null && options.getProcessForums())
            {
                addRelationship(compNode, anyNode);
                socialImpex.addReturnForum(new RSComponent(anyNode));
            }
        }
        else if (type == NodeType.TEAM)
        {
            if (options != null && options.getProcessTeams())
            {
                addRelationship(compNode, anyNode);
                socialImpex.addReturnTeam(new RSComponent(anyNode));
            }
        }
        else if (type == NodeType.DOCUMENT)
        {
            if (options != null && options.getProcessWiki())
            {
                addRelationship(compNode, anyNode);
                socialImpex.addReturnDocument(new RSComponent(anyNode));
            }
        }
        else if (type == NodeType.ACTIONTASK)
        {
            if (options != null && options.getProcessATs())
            {
                addRelationship(compNode, anyNode);
                socialImpex.addReturnActionTask(new RSComponent(anyNode));
            }
        }
        else if (type == NodeType.RSS)
        {
            if (options != null && options.getProcessRSS())
            {
                addRelationship(compNode, anyNode);
                socialImpex.addReturnRss(new RSComponent(anyNode));
            }
        }
        else if (type == NodeType.USER)
        {
            if (options != null && options.getProcessUsers())
            {
                addRelationship(compNode, anyNode);
                socialImpex.addReturnUser(new RSComponent(anyNode));
            }
        }

    }

}
