/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.ResolveNode;
import com.resolve.persistence.model.SocialUserNotification;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.NotificationTypeEnum;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.social.follow.ProcessFollow;
import com.resolve.services.social.follow.TeamFollow;
import com.resolve.services.social.notification.NotificationRuleEnum;
import com.resolve.services.social.notification.SpecificNotificationUtil;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialNotificationUtil
{
//    //reads the social_notifications.properties and updates the tables
//    public static void initSocialNotifications(String filename) throws Exception
//    {
//        new InitSocialNotifications(filename).init();
//    }
    
    public static List<ComponentNotification> getGlobalNotificationsForUser(User user) throws Exception
    {
        List<ComponentNotification> result = new ArrayList<ComponentNotification>();
        if (user != null)
        {
            Map<UserGlobalNotificationContainerType, Boolean> userCurrentSettings = getCurrentGlobalConfForUser(user);
            
            result.add(getGlobalConfForType(NodeType.PROCESS, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.TEAM, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.FORUM, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.USER, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.ACTIONTASK, userCurrentSettings));
            //        result.add(getGlobalConfForType(SocialObjectType.NAMESPACE, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.DOCUMENT, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.RSS, userCurrentSettings));
        }
        return result;
    }
    
    public static void updateGlobalNotificationsForUser(User user, Set<String> selectedNotificationTypes) throws Exception
    {
        if(user != null)
        {
            ResolveNodeVO userNode = ServiceGraph.findNode(null, user.getSys_id(), user.getCompName(), NodeType.USER, user.getCompName());
            if(userNode != null)
            {
                Iterator<UserGlobalNotificationContainerType> it = UserGlobalNotificationContainerType.allOfGlobalNotification.iterator();
                while(it.hasNext())
                {
                    UserGlobalNotificationContainerType type = it.next();
                  
                    //skip the EMAIL and DIGEST type notifications - they are handled separately
                    if(type.name().contains(ENUM_TO_SEARCH_TYPE.DIGEST_EMAIL.name())
                                    || type.name().contains(ENUM_TO_SEARCH_TYPE.FORWARD_EMAIL.name()))
                    {
                        continue;
                    }
                    
                    String typeName = type.name();
                    boolean value =  selectedNotificationTypes.contains(typeName) ? true : false;
                    
                    //update the value
                    updateGlobalNotificationTypeForUser(user, type, value);
                
                }//end of while loop

            }
        }

    }
    
    public static void updateSpecificNotificationTypeFor(String streamId, User user, UserGlobalNotificationContainerType type, boolean value) throws Exception
    {
        if(StringUtils.isNotEmpty(streamId))
        {
            //for the Inbox and System stream, the setting is in the user component node
            if(streamId.equalsIgnoreCase("inbox") || streamId.equalsIgnoreCase("system") || streamId.equalsIgnoreCase("all"))
            {
                //this being a Global notification, it will NOT have any Node associated with it..
                //in Neo4j version, this was a part of User node but in this, its in the UserNotification table
                updateGlobalNotificationTypeForUser(user, type, value);
                
                
//                //get the user node using the lookup 
//                RSComponent userComp = ServiceSocial.getComponentById(user.getSys_id(), user.getName());
//                if(type == UserGlobalNotificationContainerType.USER_FORWARD_EMAIL)
//                {
//                    userComp.setInboxForwardEmail(value);
//                }
//                else if(type == UserGlobalNotificationContainerType.USER_DIGEST_EMAIL)
//                {
//                    userComp.setInboxDigestEmail(value);
//                }
//                else if(type == UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL)
//                {
//                    userComp.setSystemForwardEmail(value);
//                }
//                else if(type == UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL)
//                {
//                    userComp.setSystemDigestEmail(value);
//                }
//                else if(type == UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL)
//                {
//                    userComp.setAllForwardEmail(value);
//                }
//                else if(type == UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL)
//                {
//                    userComp.setAllDigestEmail(value);
//                }
//                
                //update the node
                //TODO: Update the User node with the properties
//                ServiceSocial.insertOrUpdateRSComponent(userComp);
                
            }
            else
            {
                new SpecificNotificationUtil(streamId, user).updateType(type, value);
            }
            
        }
    }

    public static ComponentNotification getSpecificNotificationFor(String streamId, User user) throws Exception
    {
        ComponentNotification result = null;

        if (StringUtils.isNotEmpty(streamId))
        {
            //for the Inbox and System stream, the setting is in the user component node
            if (streamId.equalsIgnoreCase("inbox") || streamId.equalsIgnoreCase("system") || streamId.equalsIgnoreCase("all"))
            {
                //get the list of global notifications that are conf for this User
                
                ResolveNodeVO userNode = ServiceGraph.findNode(null, user.getSys_id(), user.getCompName(), NodeType.USER, "system");
                if(userNode == null)
                {
                    throw new Exception("User Node not present ");
                }

                //get the global notifications for this user
                Map<UserGlobalNotificationContainerType, String> userNotificationsAndValue = getUserNotificationsForNode(user.getSys_id(), null, null, null, user.getCompName());
                
                //get the user node using the lookup 
//                RSComponent userComp = ServiceSocial.getComponentById(user.getSys_id(), user.getCompName());
                List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>();

                EnumSet<NotificationTypeEnum> enumSetNotificationType = findNotificationTypesForType(NodeType.USER, false);
                for (NotificationTypeEnum typeEnum : enumSetNotificationType)
                {
                    NotificationConfig config = new NotificationConfig();
                    config.setType(typeEnum.getType());
                    config.setDisplayName(typeEnum.getDisplayName());
                    
                    config.setValue(userNotificationsAndValue.get(typeEnum.getType()) != null && userNotificationsAndValue.get(typeEnum.getType()).equalsIgnoreCase("true") ? "true" : "false");//userComp.isInboxForwardEmail() + "");
                    
//                    if (typeEnum.getType() == UserGlobalNotificationContainerType.USER_FORWARD_EMAIL)
//                    {
//                        config.setValue(userNotificationsAndValue.get(typeEnum.getType()) != null && userNotificationsAndValue.get(typeEnum.getType()).equalsIgnoreCase("true") ? "true" : "false");//userComp.isInboxForwardEmail() + "");
//                    }
//                    else if (typeEnum.getType() == UserGlobalNotificationContainerType.USER_DIGEST_EMAIL)
//                    {
//                        config.setValue(userComp.isInboxDigestEmail() + "");
//                    }
//                    else if (typeEnum.getType() == UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL)
//                    {
//                        config.setValue(userComp.isSystemForwardEmail() + "");
//                    }
//                    else if (typeEnum.getType() == UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL)
//                    {
//                        config.setValue(userComp.isSystemDigestEmail() + "");
//                    }
//                    else if (typeEnum.getType() == UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL)
//                    {
//                        config.setValue(userComp.isAllForwardEmail() + "");
//                    }
//                    else if (typeEnum.getType() == UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL)
//                    {
//                        config.setValue(userComp.isAllDigestEmail() + "");
//                    }

                    //add to the list
                    notifyTypes.add(config);
                }

                //prepare the result
                result = new ComponentNotification();
                result.setCompType(NodeType.USER);
                result.setNotifyTypes(notifyTypes);
                result.setComponentDisplayName(userNode.getUCompName());
            }
            else
            {
                result = new SpecificNotificationUtil(streamId, user).getSpecificNotifications();
            }

        }

        return result;

    }
    
    
    public static EnumSet<NotificationTypeEnum> findNotificationTypesForType(NodeType compType, boolean forNotificationType)
    {
        EnumSet<NotificationTypeEnum> enumSet = null;
        
        switch (compType)
        {
            case PROCESS:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetProcess;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetProcessEmail;
                }
                break;

            case TEAM:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetTeam;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetTeamEmail;
                }
                break;

            case FORUM:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetForum;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetForumEmail;
                }
                break;

            case USER:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetUser;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetUserEmail;
                }
                break;

            case WORKSHEET:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetWorksheet;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetWorksheetEmail;
                }
                break;

            case NAMESPACE:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetNamespace;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetNamespaceEmail;
                }
                break;

            case RUNBOOK:
//                if (forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetRunbook;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetRunbookEmail;
//                }
                break;

            case DOCUMENT:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetDocument;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetDocumentEmail;
                }
                break;

            case DECISIONTREE:
                break;

            case RSS:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetRss;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetRssEmail;
                }
                break;

            case ACTIONTASK:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetActionTask;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetActionTaskEmail;
                }
                break;
            
            case PLAYBOOK_TEMPLATE:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetPlaybookTemplate;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetPlaybookTemplateEmail;
                }
                break;
                
            default:
                break;

        }
        
        return enumSet;
    }
    
    public static List<RSComponent> findUsersRegisteredToReceiveThisNotificationForComp(String compSysId, UserGlobalNotificationContainerType notificationType) throws Exception
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        //get the list of user Ids that have global relationship of this notification type set as 'True'
        Set<String> userWithGlobalNotification = userWithGlobalNotification(notificationType);
        
        //if this is a CREATE/PURGE notfication type, than any user who have registered for that will receive the notification
        if(notificationType.name().contains("CREATE") || notificationType.name().contains("PURGE"))
        {
            if(userWithGlobalNotification != null && userWithGlobalNotification.size() > 0)
            {
                for(String userSysId : userWithGlobalNotification)
                {
                    ResolveNodeVO userNode = ServiceGraph.findNode(null, userSysId, null, NodeType.USER, "system");
                    result.add(new User(userNode));
                }
            }            
        }
        
        if (notificationType.name().startsWith("NAMESPACE"))
        {
            Set<String> usersWithNamespaceNotification = usersWithSpecificNotification(compSysId, notificationType);
            
            if(usersWithNamespaceNotification != null && usersWithNamespaceNotification.size() > 0)
            {
                for(String userSysId : usersWithNamespaceNotification)
                {
                    ResolveNodeVO userNode = ServiceGraph.findNode(null, userSysId, null, NodeType.USER, "system");
                    User user = new User(userNode);
                    
                    if(!result.contains(user))
                    {
                        result.add(user);
                    }
                }
            }
        }
        
        List<User> usersFollowingThisComp = null;
        
        try
        {
        //get the list of users that are following this component
            usersFollowingThisComp = ServiceSocial.getAllUsersFollowingThisStream(compSysId, "system");
        }
        catch (Exception e)
        {
            if (!notificationType.name().startsWith("NAMESPACE"))
            {
                throw e;
            }
        }
        
        if(usersFollowingThisComp != null && !usersFollowingThisComp.isEmpty())
        {
            for(User user : usersFollowingThisComp)
            {
                if (result.contains(user))
                {
                    continue;
                }
                
                if(userWithGlobalNotification.contains(user.getSys_id()))
                {
                    result.add(user);
                }
                else
                {
                    //Process component specific User notification if set to true and User is not in result
                    
                    SpecificNotificationUtil specNotifUtil = new SpecificNotificationUtil(compSysId, user);
                    
                    ComponentNotification compNotif = specNotifUtil.getSpecificNotifications();
                    
                    if(compNotif.getNotifyTypes() != null && !compNotif.getNotifyTypes().isEmpty())
                    {
                        for(NotificationConfig notifCfg : compNotif.getNotifyTypes())
                        {
                            if(notifCfg.getType() == notificationType && notifCfg.getValue().equals(SpecificNotificationUtil.SELECT_TRUE))
                            {
                                result.add(user);
                            }
                        }
                    }
                }
            }
        }

        return result;
    }
    
    public static Set<User> findUsersBelongingToTeamAndItsChildTeams(String teamId, boolean child)
    {
        Set<User> users = new HashSet<User>();
        
        if(StringUtils.isNotBlank(teamId))
        {
            Collection<ResolveNodeVO> nodes = null;
            try
            {
                // Users
                nodes = ServiceGraph.getNodesFollowingMe(null, teamId, null, NodeType.TEAM, "admin");
            }
            catch (Exception e)
            {
                Log.log.error("ERROR getting users followed by process. SysId: " + teamId, e);
            }
            
            if (nodes != null && nodes.size() > 0)
            {
                for (ResolveNodeVO node : nodes)
                {
                    if (node.getUType().equals(NodeType.USER))
                    {
                        try
                        {
                            users.add(new User(node));
                        }
                        catch (Exception e)
                        {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                }
            }
            
            if (child)
            {
                try
                {
                    nodes = ServiceGraph.getNodesFollowedBy(null, teamId, null, NodeType.TEAM, "admin");
                }
                catch (Exception e)
                {
                    Log.log.error("ERROR getting nodes of process. SysId: " + teamId, e);
                }
                
                if (nodes != null && nodes.size() > 0)
                {
                    for (ResolveNodeVO node : nodes)
                    {
                        if (node.getUType().equals(NodeType.TEAM))
                        {
                            //**RECURSIVE
                            findUsersBelongingToTeamAndItsChildTeams(node.getUCompSysId(), true);
                        }
                    }
                }
            }
        }

        return users;
    }
    
    public static void bulkInsertOrUpdateDocumentGraphNodes(Collection<Document> documents, String username) throws Exception
    {
        if(documents != null)
        {
            for(Document doc : documents)
            {
                try
                {
                    ServiceGraph.persistNode(doc.prepareNode(), username);
                }
                catch(Exception e)
                {
                    Log.log.error("Error in persisting the doc node:" + doc, e);
                }
            }
        }
        
    }
    
    public static Set<String> getComponentIdsBelongingToContainer(RSComponent targetComponent) throws Exception
    {
        Set<String> result = new HashSet<String>();

        Set<RSComponent> comps = null;
        if (targetComponent.getType() == NodeType.PROCESS)
        {
            ProcessFollow process = new ProcessFollow(targetComponent.getSys_id(), null, "system");
            process.setRecurse(true);

            comps = process.getStreams();
        }
        else if (targetComponent.getType() == NodeType.TEAM)
        {

            TeamFollow team = new TeamFollow(targetComponent.getSys_id(), null, "system");
            team.setRecurse(true);

            comps = team.getStreams();
        }

        if (comps != null)
        {
            for (RSComponent comp : comps)
            {
                result.add(comp.getSys_id());
            }
        }
        
        return result;
    }
    
    
    @SuppressWarnings("rawtypes")
    public static Map<UserGlobalNotificationContainerType, String> getUserNotificationsForNode(String userSysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        Map<UserGlobalNotificationContainerType, String> result = new HashMap<UserGlobalNotificationContainerType, String>();
        
        if(StringUtils.isNotEmpty(userSysId))
        {
            try
            {

              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		
            		ResolveNodeVO compNode = null;
                    if(StringUtils.isNotEmpty(compSysId) || StringUtils.isNotEmpty(compName))
                    {
                        compNode = ServiceGraph.findNode(null, compSysId, compName, compType, username);
                    }
            		
                    Users user = HibernateUtil.getDAOFactory().getUsersDAO().findById(userSysId);
                    if(user != null)
                    {
                        //give me all the User notifications conf for this node 
                        //String hql = "from SocialUserNotification where user = '" + user.getSys_id() + "'";
                        String hql = "from SocialUserNotification where user.sys_id = :userSys_id";
                        
                        Map<String, Object> queryParams = new HashMap<String, Object>();
                        
                        queryParams.put("userSys_id", user.getSys_id());
                        
                        if(compNode != null)
                        {
                            //hql = hql + " and node = '" + compNode.getSys_id() + "'"; //should give you specific notifications 
                            hql = hql + " and node.sys_id = :compNodeSys_id"; //should give you specific notifications 
                            queryParams.put("compNodeSys_id", compNode.getSys_id());
                        }
                        else
                        {
                            hql = hql + " and node is null"; //this will give u all the GLobal Notifications
                        }
                        
                        
                        List records = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
                        for(Object o : records)
                        {
                            SocialUserNotification userNotf = (SocialUserNotification) o;
                            
                            UserGlobalNotificationContainerType notification = UserGlobalNotificationContainerType.valueOf(userNotf.getUNotification());
                            String value = userNotf.getUNotificationValue();
                            
                            result.put(notification, value);
                        }//end of for loop   
                    }
            	});
            }
            catch(Exception e)
            {
                Log.log.error("Error in getUserNotificationsForNode for user:" + userSysId + ", comp:" + compName + "," + compSysId + "," + compType, e);
                throw e;
            }
        }
        
        return result;
    }

    public static Set<User> getUsersForForwardEmail(RSComponent component, String username) throws Exception
    {
        return getUsersForDigestEmailOrForwardEmail(component, ENUM_TO_SEARCH_TYPE.FORWARD_EMAIL, username);
    }
    
    public static Set<User> getUsersForDigestEmail(RSComponent component, String username) throws Exception
    {
        return getUsersForDigestEmailOrForwardEmail(component, ENUM_TO_SEARCH_TYPE.DIGEST_EMAIL, username);
    }
    
    //private apis
    private static ComponentNotification getGlobalConfForType(NodeType compType, Map<UserGlobalNotificationContainerType, Boolean> userCurrentSettings)
    {
        ComponentNotification compNotify = new ComponentNotification();
        compNotify.setCompType(compType);
        compNotify.setNotifyTypes(getCompNotificationType(compType, userCurrentSettings, true));
        
        switch(compType)
        {
            case PROCESS:
                compNotify.setComponentDisplayName("Process");
                break;

            case TEAM:
                compNotify.setComponentDisplayName("Team");
                break;

            case FORUM:
                compNotify.setComponentDisplayName("Forum");
                break;


            case USER:
                compNotify.setComponentDisplayName("User");
                break;


            case WORKSHEET:
                compNotify.setComponentDisplayName("Worksheet");
                break;

            case NAMESPACE:
                compNotify.setComponentDisplayName("Namespace");
                break;

            case RUNBOOK:
                compNotify.setComponentDisplayName("Runbook");
                break;


            case DOCUMENT:
                compNotify.setComponentDisplayName("Document");
                break;

            case DECISIONTREE:
                compNotify.setComponentDisplayName("Decision Tree");
                break;

            case RSS:
                compNotify.setComponentDisplayName("Rss");
                break;

            case ACTIONTASK:
                compNotify.setComponentDisplayName("Actiontask");
                break;
                
            default:
                break;
                
        }
        
        return compNotify;
    }
    
    private static List<NotificationConfig> getCompNotificationType(NodeType compType, Map<UserGlobalNotificationContainerType, Boolean> userCurrentSettings, boolean forNotificationType)
    {
        List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>();

        try
        {
            EnumSet<NotificationTypeEnum> enumSet = findNotificationTypesForType(compType, forNotificationType);
            if (enumSet != null)
            {
                NotificationConfig config = null;

                for (NotificationTypeEnum typeEnum : enumSet)
                {
                    UserGlobalNotificationContainerType typeOfNotification = typeEnum.getType();
                    boolean value = userCurrentSettings.containsKey(typeOfNotification) ? userCurrentSettings.get(typeOfNotification) : false;
                    
                    config = new NotificationConfig();
                    config.setType(typeOfNotification);
                    config.setDisplayName(typeEnum.getDisplayName());
                    config.setValue(value+"");
                    notifyTypes.add(config);
                }
            }

        }
        catch (Throwable t)
        {
            Log.log.info("getCompNotificationType catch exception: " + t, t);
        }

        return notifyTypes;
    }
    
    private static Map<UserGlobalNotificationContainerType, Boolean> getCurrentGlobalConfForUser(User user) throws Exception
    {
        Map<UserGlobalNotificationContainerType, Boolean> result = new HashMap<UserGlobalNotificationContainerType, Boolean>();
        
        //get the list of global notifications
        Set<String> globalNotificationTypes = new HashSet<String>();
        Iterator<UserGlobalNotificationContainerType> it = UserGlobalNotificationContainerType.allOfGlobalNotification.iterator();
        while(it.hasNext())
        {
            UserGlobalNotificationContainerType t = it.next();
            globalNotificationTypes.add(t.name());
        }
        
        //get all the notifications for this user
        Map<UserGlobalNotificationContainerType, String> userNotifications = getUserNotificationsForNode(user.getSys_id(), null, null, null, user.getCompName());
        Iterator<UserGlobalNotificationContainerType> itNot = userNotifications.keySet().iterator();
        while(itNot.hasNext())
        {
            boolean value = false;
            UserGlobalNotificationContainerType notification = itNot.next();
            if (globalNotificationTypes.contains(notification.name()))
            {
                //this is a global notification name...get this one
                String valueStr = userNotifications.get(notification);
                if (StringUtils.isNotBlank(valueStr) && valueStr.equalsIgnoreCase("true"))
                {
                    value = true;
                }

                result.put(notification, value);
            }
            
        }//end of while loop
        
        return result;
    }
    
//    private static Map<UserGlobalNotificationContainerType, Boolean> getCurrentGlobalConfForUser(User user) throws Exception
//    {
//        Map<UserGlobalNotificationContainerType, Boolean> result = new HashMap<UserGlobalNotificationContainerType, Boolean>();
//        
//        //get the list of global notifications
//        Set<String> globalNotificationTypes = new HashSet<String>();
//        Iterator<UserGlobalNotificationContainerType> it = UserGlobalNotificationContainerType.allOfGlobalNotification.iterator();
//        while(it.hasNext())
//        {
//            UserGlobalNotificationContainerType t = it.next();
//            globalNotificationTypes.add(t.name());
//        }
//        
//        //get all the notifications for this user
//        Map<SocialNotificationsVO, String> userNotifications = getUserNotificationsForNode(user.getSys_id(), null, null, null, user.getCompName());
//        Iterator<SocialNotificationsVO> itNot = userNotifications.keySet().iterator();
//        while(itNot.hasNext())
//        {
//            boolean value = false;
//            SocialNotificationsVO notification = itNot.next();
//            if (globalNotificationTypes.contains(notification.getUName()))
//            {
//                //this is a global notification name...get this one
//                String valueStr = userNotifications.get(notification);
//                if (StringUtils.isNotBlank(valueStr) && valueStr.equalsIgnoreCase("true"))
//                {
//                    value = true;
//                }
//
//                result.put(UserGlobalNotificationContainerType.valueOf(notification.getUName()), value);
//            }
//            
//        }//end of while loop
//        
//        return result;
//    }
    
    /**
     * this api is used to update/set a specific Global type for a user. 
     * 
     * For eg. if the user wants to get email for all the Post for all the Processes it is following, than the values will be
     * type =  "PROCESS_FORWARD_EMAIL"  <== mapped to UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL
     * value = true
     * 
     * @param user
     * @param selectedType
     * @param value
     * @throws Exception
     */
    private static void updateGlobalNotificationTypeForUser(User user, UserGlobalNotificationContainerType type, boolean value) throws Exception
    {
        if(user != null)
        {
            ResolveNodeVO userNode = ServiceGraph.findNode(null, user.getSys_id(), null, NodeType.USER, user.getCompName());
            if(userNode != null)
            {
//                SocialNotifications notification = findSocialNotification(null, type, user.getCompName());
//                if(notification != null)
//                {
                    persistUserNotification(userNode.getUCompSysId(), userNode.getUCompName(), type, null, null, Boolean.toString(value));
//                }
            }
            
        }
    }
    
    /**
     * 
     * @param userSysId
     * @param username
     * @param notification
     * @param rule  - NOT CONSIDERING THE RULE ATM AS NOT SURE IF WE WILL NEED IT
     * @param node
     * @param notificationValue
     * @throws Exception
     */
    
    @SuppressWarnings("rawtypes")
    public static void persistUserNotification(String userSysId, String username, UserGlobalNotificationContainerType notification, NotificationRuleEnum rule, ResolveNode node, String notificationValue) throws Exception
    {
        if(StringUtils.isEmpty(userSysId) && StringUtils.isEmpty(username))
        {
            throw new Exception("UserId or Username is mandatory");
        }
        
        if(notification == null)
        {
            throw new Exception("Notification is mandatory");
        }
        
        Users user = UserUtils.findUser(userSysId, username);
        if(user == null)
        {
            Log.log.warn("Ignoring peristance of " + notification + " for user " + (userSysId != null ? userSysId + "," : "") + username + " as it does not exist");
            throw new Exception("User " + (userSysId != null ? userSysId + "," : "") + username + " does not exist");
        }
        
        SocialUserNotification userNotification = null;
        try
        {
          HibernateProxy.setCurrentUser(user.getUUserName());
        	userNotification = (SocialUserNotification) HibernateProxy.execute(() -> {
        		SocialUserNotification userNotificationResult = null;
	            //String hql = "from SocialUserNotification where user = '" + user.getSys_id() + "' and UNotification = '" + notification.name() + "' ";
	            String hql = "from SocialUserNotification where user.sys_id= :userSys_id and UNotification = :UNotification ";
	            
	            Map<String, Object> queryParams = new HashMap<String, Object>();
	            
	            queryParams.put("userSys_id", user.getSys_id());
	            queryParams.put("UNotification", notification.name());
	            
	            //NOT CONSIDERING THE RULE ATM AS NOT SURE IF WE WILL NEED IT
	//            if(rule != null)
	//            {
	//                hql = hql + " and UNotificationRule = '" + rule.name() + "' ";
	//            }
	//            else
	//            {
	//                hql = hql + " and UNotificationRule is null ";
	//            }
	            
	            if(node != null)
	            {
	                //hql = hql + " and node = '" + node.getSys_id() + "' ";
	                hql = hql + " and node.sys_id = :nodeSys_id ";
	                queryParams.put("nodeSys_id", node.getSys_id());
	            }
	            else
	            {
	                hql = hql + " and node is null ";
	            }
	            
	            List list = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
	            if(list != null && list.size() > 0)
	            {
	                userNotificationResult = (SocialUserNotification) list.get(0);
	            }
	            else
	            {
	                userNotificationResult = new SocialUserNotification();
	                userNotificationResult.setUser(user);
	                userNotificationResult.setUNotification(notification.name());
	            }
	            
	            
	            if(node != null)
	                userNotificationResult.setNode(node);
	            
	//            if(rule != null)
	//                userNotification.setUNotificationRule(rule.name());
	            
	            userNotificationResult.setUNotificationValue(notificationValue);
	            
	            HibernateUtil.getDAOFactory().getSocialUserNotificationDAO().persist(userNotificationResult);
	            
	            return userNotificationResult;
            
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error in persistUserNotification:", e);
            throw e;
        }
    }
    
    @SuppressWarnings("rawtypes")
    private static Set<String> userWithGlobalNotification(UserGlobalNotificationContainerType notificationType) throws Exception
    {
        Set<String> userSysIds = new HashSet<String>();
        
        if(notificationType != null)
        {
            /*String hql = "from SocialUserNotification where UNotification = '" + notificationType.name() + "' " +
                         " and node is null and user is not null";*/
            String hql = "from SocialUserNotification where UNotification = :UNotification " +
                            " and node is null and user is not null";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UNotification", notificationType.name());
            
            try
            {                
              HibernateProxy.setCurrentUser("system");
                List list = (List) HibernateProxy.execute(() -> GeneralHibernateUtil.executeHQLSelect(hql, queryParams));
                if(list != null && list.size() > 0)
                {
                    for(Object o : list)
                    {
                        SocialUserNotification userNotf = (SocialUserNotification) o;
                        String val = userNotf.getUNotificationValue();
                        
                        if(StringUtils.isNotBlank(val) && val.trim().equalsIgnoreCase("true"))
                        {
                            if (userNotf.getUser() != null)
                            {
                                userSysIds.add(userNotf.getUser().getSys_id());
                            }
                            else
                            {
                                Log.log.info("No user is registered to receive " + notificationType.name() + " notification." );
                            }
                        }
                    }//end of for loop
                }//end of if
               
            }
            catch(Exception e)
            {
                Log.log.error("Error in getting users interested in receiving global notification " + notificationType, e);
                throw e;
            }
        }
        
        return userSysIds;
    }

    private static Set<User> getUsersForDigestEmailOrForwardEmail(RSComponent component, ENUM_TO_SEARCH_TYPE searchString, String username) throws Exception
    {
        //get the user notification type for this comp
        UserGlobalNotificationContainerType emailType = getEmailTypeForComp(component.getType(), searchString);
        
        List<RSComponent> rsComps = findUsersRegisteredToReceiveThisNotificationForComp(component.getSys_id(), emailType);
        Set<User> users = new HashSet<User>();
        
        for(RSComponent rsComp : rsComps)
        {
            users.add((User)rsComp);
            
        }
        
        return users;
    }
    
    private static UserGlobalNotificationContainerType getEmailTypeForComp(NodeType compType, ENUM_TO_SEARCH_TYPE emailOrDigestType)
    {
        UserGlobalNotificationContainerType result = null;
        
        EnumSet<NotificationTypeEnum> enumSet = findNotificationTypesForType(compType, false);
        if (enumSet != null)
        {
            for (NotificationTypeEnum typeEnum : enumSet)
            {
                if(typeEnum.name().contains(emailOrDigestType.name()))
                {
                    result = typeEnum.getType();
                    break;
                }
            }//end of for
        }
        
        return result;
    }
    
    @SuppressWarnings("rawtypes")
    private static Set<String> usersWithSpecificNotification(String nodeSysId, UserGlobalNotificationContainerType notificationType) throws Exception
    {
        Set<String> userSysIds = new HashSet<String>();
        
        if(notificationType != null)
        {
            /*String hql = "from SocialUserNotification where UNotification = '" + notificationType.name() + "' " +
                         " and node.sys_id = '" + nodeSysId + "'";*/
            String hql = "from SocialUserNotification where UNotification = :UNotification " +
                            " and node.sys_id = :sys_id";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UNotification", notificationType.name());
            queryParams.put("sys_id", nodeSysId);
            
            try
            {   
              HibernateProxy.setCurrentUser("system");
                List list = (List) HibernateProxy.execute(() -> GeneralHibernateUtil.executeHQLSelect(hql, queryParams));
                if(list != null && list.size() > 0)
                {
                    for(Object o : list)
                    {
                        SocialUserNotification userNotf = (SocialUserNotification) o;
                        String val = userNotf.getUNotificationValue();
                        
                        if(StringUtils.isNotBlank(val) && val.trim().equalsIgnoreCase("true"))
                        {
                            userSysIds.add(userNotf.getUser().getSys_id());
                        }
                    }//end of for loop
                }//end of if
                
            }
            catch(Exception e)
            {
                Log.log.error("Error in persistUserNotification:", e);                
                throw e;
            }
            
        }
        
        return userSysIds;
    }
}

//just string used to search the type of the digest or email
enum ENUM_TO_SEARCH_TYPE {
  DIGEST_EMAIL,
  FORWARD_EMAIL;    
}
