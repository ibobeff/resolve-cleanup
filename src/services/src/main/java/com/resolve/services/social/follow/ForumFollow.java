/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.follow;

import java.util.Collection;
import java.util.Set;

import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

/**
 * get the Users that belong to a Forum
 * 
 * @author jeet.marwah
 *
 */
public class ForumFollow extends ContainerFollow
{

    public ForumFollow(String forumSysId, String forumName, String username) throws Exception
    {
        super(ServiceGraph.findNode(null, forumSysId, forumName, NodeType.FORUM, username), username);
    }

    @Override
    public Set<RSComponent> getStreams() throws Exception
    {
        Collection<ResolveNodeVO> nodesThatThisForumContains = ServiceGraph.getNodesFollowingMe(rootNode.getSys_id(), null, null, null, username);
        if (nodesThatThisForumContains != null)
        {
            for (ResolveNodeVO node : nodesThatThisForumContains)
            {
                NodeType type = node.getUType();
                if (type == NodeType.USER)
                {
                    streams.addAll(convertNodeToRsComponent(node, isDirectFollow()));
                }
            }
        }

        return streams;
    }

}
