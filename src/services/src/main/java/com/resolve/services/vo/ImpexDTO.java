/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.List;

/**
 * 
 * This will be deleted
 * 
 * this class is used to help in convert the json to object and vice versa. If we use List directly, the conversion fails and type casting does not work either
 * 
 * @author jeet.marwah
 *
 */
@Deprecated
public class ImpexDTO
{

    private List<GraphRelationshipDTO> relationships = null;

    public List<GraphRelationshipDTO> getRelationships()
    {
        return relationships;
    }

    public void setRelationships(List<GraphRelationshipDTO> relationships)
    {
        this.relationships = relationships;
    }

}
