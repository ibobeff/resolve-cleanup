/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.Collection;
//import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;

import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.EntityUtil;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceCustomTable;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * 
 * The purpose of this class is to delete the data in a Custom Table. It will also delete related data from the reference tables like the File Upload table.
 * 
 * @author jeet.marwah
 *
 */
public class DeleteCustomTableRecords
{
    private Collection<String> sysIds = null;
    private String tableName = null;
    private String username = null;
    
    private CustomTableVO customTable = null;

    public DeleteCustomTableRecords(String tableName, Collection<String> sysIds, String username) throws Exception 
    {
        if(StringUtils.isBlank(tableName) || sysIds == null || sysIds.size() ==  0 ||  StringUtils.isBlank(username))
        {
            throw new Exception("Table name,List of SysIds and Username to delete are mandatory");
        }
        
        this.sysIds = sysIds;
        this.tableName = tableName;
        this.username = username;
    }
    
    public Map<String, String> delete() throws Exception
    {
        customTable = ServiceCustomTable.getCustomTableWithReferences(null, tableName, "system");
        if(customTable == null)
        {
            throw new Exception("Table " + this.tableName + " does not exist");
        }
        
        Map<String, String> result = ServiceHibernate.deleteDBRow(username, this.customTable.getUModelName(), new ArrayList<String>(sysIds), true);
        
        //delete the ref table records if any
        deleteRefTableRecords(sysIds);
        
        return result;
    }
    
    private void deleteRefTableRecords(Collection<String> sysIds) throws Exception
    {
        String refTableNames = this.customTable.getUReferenceTableNames();
        if(StringUtils.isNotBlank(refTableNames))
        {
            try
            {
                Set<String> refTables = new HashSet<String>(StringUtils.convertStringToList(refTableNames, ","));
                
                    //String refColumnName = ExtSaveMetaFormView.prepareRefColumnNameBasedOnTableName(this.customTable.getUModelName());
                    if ((StringUtils.isNotEmpty(HibernateUtil.class2Table(this.customTable.getUModelName()))) &&
                        (StringUtils.isNotEmpty(HibernateUtil.table2Class(HibernateUtil.class2Table(this.customTable.getUModelName())))))
                    {
                        String refColumnName = "u_" + 
                                               (this.customTable.getUModelName().length() > 15 ? 
                                                CustomTableMappingUtil.table2Class(CustomTableMappingUtil.class2Table(this.customTable.getUModelName())).toLowerCase().substring(0, 15) : 
                                                    CustomTableMappingUtil.table2Class(CustomTableMappingUtil.class2Table(this.customTable.getUModelName())).toLowerCase()) + "_sys_id";
                        
                        Log.log.debug("deleteRefTableRecords() reference column name = [" + refColumnName + "]");
                        
                        if (StringUtils.isNotEmpty(refColumnName))
                        {
                            for(String refTable : refTables)
                            {
                                List<String> refSysIdsToDelete = new ArrayList<String>();
                                
                                //for each master rec, delete the child recs if any
                                for(String refSysId : sysIds)
                                {
                                    // for all sys_ids find reference record sys_id in reference table under consideration using foreign key
                                    
                                    if ((StringUtils.isNotEmpty(HibernateUtil.class2Table(refTable))) &&
                                        (StringUtils.isNotEmpty(HibernateUtil.table2Class(HibernateUtil.class2Table(refTable)))))
                                    {
                                        String tblName = HibernateUtil.class2Table(refTable);
                                        String colName = HibernateUtil.getColumnName(refTable, refColumnName);
                                        
//                                        Map<String, Object> restrictions = new HashMap<String, Object>();
//                                        
//                                        restrictions.put(HibernateUtil.getPropertyName(tblName, colName), refSysId);
                                        
                                        List<Map<String,Object>> refRecs = EntityUtil.findAll(HibernateUtil.table2Class(tblName), /*restrictions);*/ 
                                                                                              (Criterion)Restrictions.sqlRestriction(HibernateUtil.getPropertyName(tblName, colName) + " = ?", refSysId, StringType.INSTANCE));
                                                        
                                        
                                        if (refRecs != null && !refRecs.isEmpty())
                                        {
                                            for (Map<String, Object> refRec : refRecs)
                                            {
                                                if (refRec.containsKey("sys_id"))
                                                {
                                                    if (StringUtils.isNotEmpty((String)refRec.get("sys_id")))
                                                    {
                                                        refSysIdsToDelete.add((String)refRec.get("sys_id"));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }//end of for loop
                                
                                ServiceHibernate.deleteDBRow(username, refTable, refSysIdsToDelete, true);
                            }
                        }
                    }
            }
            catch (Exception e)
            {
                Log.log.error("Error in deleting the records of " + refTableNames, e);
            }
        }
    }
}
