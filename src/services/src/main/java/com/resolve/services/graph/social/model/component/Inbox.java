/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.vo.social.NodeType;

/**
 *   
 * 
 * @author jeet.marwah
 *
 */
public class Inbox extends RSComponent
{
    private static final long serialVersionUID = -5681445145333513363L;
    
//    public static final String TYPE = "inbox";

    public Inbox()
    {
        super(NodeType.INBOX);
    }
}
