/******************************************************************************
 * (C) Copyright 2019
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.GatewayFilter;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.GatewayUtil;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.util.Log;

/**
 * Utility class to update a checksum of any Resolve model.
 * 
 * @author Mangesh Shimpi
 *
 */
public class UpdateChecksumUtil {
    
    private static final String SYSID = "sysId";
    private static final String CHECKSUM = "checksum";
    private static final String UPDATE_CHECKSUM_SQL_TEMPLATE = "update %s set checksum = :" + CHECKSUM + " where sys_id = :" + SYSID;
    private static final String FROM_MODEL_SQL_TEMPLATE = "from %s where (checksum is null OR checksum = 0) order by sysCreatedOn";
    private static final String ERROR_READING_MODEL_FOR_CHECKSUM_UPDATE_TEMPLATE = "Error: Could not read %s for checksum update.";
    private static final String ERROR_UPDATING_MODEL_WITH_CHECKSUM_TEMPLATE = "Error: Could not update %s checksums in batch operation";
    
    /**
     * API to be invoked from update/upgrade script only!
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void updateComponentChecksum(String model, Class caller) throws Exception
    {
        String UPDATE_CHECKSUM_SQL = String.format(UPDATE_CHECKSUM_SQL_TEMPLATE, model);
        String FROM_MODEL_SQL = String.format(FROM_MODEL_SQL_TEMPLATE, model);
        String ERROR_READING_MODEL_FOR_CHECKSUM_UPDATE = String.format(ERROR_READING_MODEL_FOR_CHECKSUM_UPDATE_TEMPLATE, model);
        String ERROR_UPDATING_MODEL_WITH_CHECKSUM = String.format(ERROR_UPDATING_MODEL_WITH_CHECKSUM_TEMPLATE, model);
        
        int returnedRecords = -1;
        final List<Object> modelList = new ArrayList<>();
        
        do {
            try
            {
                HibernateUtil.action(hibernateUtil -> {
                    try {
                        modelList.addAll(HibernateUtil.createQuery(FROM_MODEL_SQL).setMaxResults(100).setFirstResult(0).list());
                    } catch(Throwable t) {
                        Log.log.error(t.getMessage(), t);
                    }
                }, "admin");
            }
            catch(Throwable t)
            {
                Log.log.error(ERROR_READING_MODEL_FOR_CHECKSUM_UPDATE, t);
            }
            
            if (CollectionUtils.isNotEmpty(modelList))
            {
                try {
                    // action API is called within a transaction.
                    HibernateUtil.action(hibernateUtil -> {
                        try {
                            // batch update all returned records.
                            Query query = HibernateUtil.createQuery(UPDATE_CHECKSUM_SQL);
                            modelList.stream().forEach(localModel -> {
                                int checksum = 0;
                                try {
                                    /*
                                     * Invoke calculateChecksum API of a caller.
                                     * Caller knows better about how to calculate a checksum for a give component.
                                     * So, the resposibility is deligated.
                                     */
                                    Object object = null;
                                    String modelName = localModel.getClass().getSimpleName();
                                    if (localModel instanceof GatewayFilter) {
                                        GatewayFilter filter = (GatewayFilter)localModel;
                                        object = filter.doGetVO(false).hashCode();
                                    } else if (modelName.equals(ImpexEnum.SSHPool.getValue()) || 
                                                    modelName.equals(ImpexEnum.DatabaseConnectionPool.getValue()) ||
                                                    modelName.equals(ImpexEnum.EmailAddress.getValue()) ||
                                                    modelName.equals(ImpexEnum.EWSAddress.getValue()) ||
                                                    modelName.equals(ImpexEnum.RemedyxForm.getValue()) ||
                                                    modelName.equals(ImpexEnum.XMPPAddress.getValue()) ||
                                                    modelName.equals(ImpexEnum.MSGGatewayFilter.getValue()) ||
                                                    modelName.equals(ImpexEnum.PushGatewayFilter.getValue()) ||
                                                    modelName.equals(ImpexEnum.PullGatewayFilter.getValue())) {
                                        object = GatewayUtil.calculateChecksumForUpdate(localModel, modelName);
                                    } else {
                                        object = MethodUtils.invokeExactStaticMethod(caller, "calculateChecksum", localModel);
                                    }
                                    
                                    if (object != null && object instanceof Integer) {
                                        checksum = (Integer)object;
                                    }
                                    query.setParameter(SYSID, BeanUtils.getSimpleProperty(localModel, "sys_id"))
                                        .setParameter(CHECKSUM, checksum)
                                        .executeUpdate();
                                }
                                catch (Exception e) {
                                    Log.log.error(e.getMessage(), e);
                                }
                            });
                        } catch (Throwable t) {
                            Log.log.error(t.getMessage(), t);
                        }
                    }, "admin");
                }
                catch(Throwable t)
                {
                    Log.log.error(ERROR_UPDATING_MODEL_WITH_CHECKSUM, t);
                }
            }
            returnedRecords = modelList.size();
            modelList.clear();
            // stop after returned records are less than 100. 
        } while (returnedRecords == 100);
    }
}
