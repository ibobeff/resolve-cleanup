/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util.vo;

public class RsMetaFilterDTO
{

    private String sysId;
    private String name;
    private String value;
    private Boolean isGlobal;
    private Boolean isSelf;
    private Boolean isRole;
    private String userId;
    
    private String metaTableSysId;
    private String metaTableName;
    
    //roles for a form - CSVs
    private String viewRoles;
    private String editRoles;
    private String adminRoles;

    public RsMetaFilterDTO() {}

    public String getSysId()
    {
        return sysId;
    }

    public void setSysId(String sysId)
    {
        this.sysId = sysId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public Boolean getIsGlobal()
    {
        return isGlobal;
    }

    public void setIsGlobal(Boolean isGlobal)
    {
        this.isGlobal = isGlobal;
    }

    public Boolean getIsSelf()
    {
        return isSelf;
    }

    public void setIsSelf(Boolean isSelf)
    {
        this.isSelf = isSelf;
    }

    public Boolean getIsRole()
    {
        return isRole;
    }

    public void setIsRole(Boolean isRole)
    {
        this.isRole = isRole;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getMetaTableSysId()
    {
        return metaTableSysId;
    }

    public void setMetaTableSysId(String metaTableSysId)
    {
        this.metaTableSysId = metaTableSysId;
    }

    public String getMetaTableName()
    {
        return metaTableName;
    }

    public void setMetaTableName(String metaTableName)
    {
        this.metaTableName = metaTableName;
    }

    public String getViewRoles()
    {
        return viewRoles;
    }

    public void setViewRoles(String viewRoles)
    {
        this.viewRoles = viewRoles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    public String getAdminRoles()
    {
        return adminRoles;
    }

    public void setAdminRoles(String adminRoles)
    {
        this.adminRoles = adminRoles;
    }
    
    
    
    
    
}
