package com.resolve.services.rr.util;

import java.util.List;

import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;

public class BulkDBTask extends BaseBulkTask
{
    private BulkDBTaskDataManipulator manipulator;
    public BulkDBTask(QueryDTO query,String username,String type,BulkDBTaskDataManipulator manipulator) throws Exception{
        this.manipulator = manipulator;
        this.init(query,username,type);
    }
    public BulkDBTask(List<String> ids,String username,String type,BulkDBTaskDataManipulator manipulator)
    {
        this.manipulator = manipulator;
        this.init(ids,username,type);
    }
    @Override
    public void run()
    {
        long start = System.currentTimeMillis();
        long current = -1;
        try
        {
            while(this.cursor.hasNext()&&!this.isStoping()) {
                current = System.currentTimeMillis();
                if((current-start)/Locker.MINUTE_IN_MILLIS>Locker.TIMEOUT_IN_MINUTE) {
                    start = current;
                    Locker.updateLock(this.getTid(),this.username, current);
                }
                RRRuleVO rule = null;
                rule = this.cursor.next();
                try
                {
//                    Thread.sleep(1000);
                    this.manipulator.manipulate(rule,this.username);
                }
                catch (ReadRuleException e)
                {
                    Log.log.error(e);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        //this takes out the thread from the map
        try
        {
            ServiceResolutionRouting.poll(this.getTid(),this.username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } 
}
