/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.export;

import java.util.Collection;
import java.util.List;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskResolveTagRel;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

public class ExportActiontaskGraph extends ExportComponentGraph
{
    public ExportActiontaskGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
       super(sysId, options, NodeType.ACTIONTASK);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
            
	            ResolveActionTask task = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(compNode.getUCompSysId()); 
	            if(task != null)
	            {
	                Collection<ResolveActionTaskResolveTagRel> tagRels = task.getAtTagRels();
	                if(tagRels != null && tagRels.size() > 0)
	                {
	                    for(ResolveActionTaskResolveTagRel rel : tagRels)
	                    {
	                        ResolveTag tag = rel.getTag();
	                        if(tag != null)
	                        {
	                            GraphRelationshipDTO relation = new GraphRelationshipDTO();
	                            relation.setSourceName(task.getUFullName());
	                            relation.setSourceType(NodeType.ACTIONTASK);
	                            relation.setTargetName(tag.getUName());
	                            relation.setTargetType(NodeType.TAG);
	                            
	                            relationships.add(relation);
	                        }
	                    }
	                }
	            }            
        	});
        }
        catch(Throwable t)
        {
            Log.log.error("Error in exporting Document:" + compNode.getUCompName(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        
//        //get all the tags for this actiontask node
//        Iterable<Node> tagsForActiontask = TraversalUtil.findTagsFor(compNode);
//        if(tagsForActiontask != null)
//        {
//            for(Node anyNode : tagsForActiontask)
//            {
//                addRelationship(compNode, anyNode);
//            }//end of for
//        }
        
        
        return relationships;
    }
}
