/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.LRUMap;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.index.query.QueryBuilder;
import org.hibernate.query.Query;
import org.hibernate.type.StringType;

import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.dao.ResolveActionTaskDAO;
import com.resolve.persistence.dao.UsersDAO;
import com.resolve.persistence.model.ArchiveActionResult;
import com.resolve.persistence.model.ArchiveExecuteRequest;
import com.resolve.persistence.model.ArchiveExecuteResult;
import com.resolve.persistence.model.ArchiveWorksheet;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveSession;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.ExecutionSummary;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.worksheet.WorksheetIndexAPI;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.ServiceArchive;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.vo.ActionTaskInfoVO;
import com.resolve.services.hibernate.vo.ArchiveActionResultVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Guid;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.constants.HibernateConstantsEnum;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class WorksheetUtil
{
    @SuppressWarnings({ "unchecked" })
    private static Map<String, Map<String, Object>> resultMacroCache =  Collections.synchronizedMap(new LRUMap(7000));
    @SuppressWarnings({ "unchecked" })
    private static Map<String, Map<String, Object>> detailMacroCache =  Collections.synchronizedMap(new LRUMap(7000));
    private static Map<String, Map<String, Object>> resultCountCache = new ConcurrentHashMap<String, Map<String, Object>>(); 
    private static Map<String, Map<String, Object>> openCountCache = new ConcurrentHashMap<String, Map<String, Object>>();
    private static String LAST_UPDATE = "Last_Update";
    private static int UPDATE_INTERVAL = 5000;
    private static final String COUNT_VALUE = "Count_Value";
    private static final String RESPONSEDTO_VALUE = "ResponseDTO_Value";
    private static final String BASEMODEL_SYS_ORG = "sysOrg";
    private static final String BASEMODEL_SYS_UPDATED_ON = "sysUpdatedOn";
    private static final String SYSTEM_USER = "system";
    private static long CLEANUP_INTERVAL = 1000*60*30; 
    private static int detailCacheThreshold = 0; //500*1024;
    private static Map<String, String> userNameToDisplayName = new ConcurrentHashMap<String, String>();
    public static boolean enableDetailCache = false;
    public static int detailCacheSize = 0;

    static 
    {
        startCleanupThread();
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked", "unused" })
    public static Worksheet initWorksheet(String problemId, String reference, String alertId, String correlationId, String username, String processId, boolean isEvent, Map params) throws Exception
    {
        Worksheet worksheet = null;
        String worksheetId = problemId;
        boolean newWorksheet = false;

        if(Log.log.isDebugEnabled()) {
            Log.log.debug("initWorksheet");
        }

        // check if Event and PROCESSID is specified AND no EVENT_REFERENCE
        if (isEvent && StringUtils.isNotBlank(processId) && StringUtils.isEmpty((String) params.get(Constants.EVENT_REFERENCE)))
        {
            worksheetId = getOldestWorksheetByProcessId(processId);

            if (worksheetId == null)
            {
                Log.log.warn("Invalid PROCESSID value for event: " + processId, new Exception());
            }
        }
        else if (StringUtils.isEmpty(problemId) || problemId.equalsIgnoreCase(Constants.PROBLEMID_ACTIVE) || problemId.equalsIgnoreCase(Constants.PROBLEMID_LOOKUP))
        {
            // lookup references first
            if (problemId == null || problemId.equalsIgnoreCase(Constants.PROBLEMID_LOOKUP))
            {
                worksheetId = findWorksheetIdByColumnValues(params);
                
            }

            // get active worksheet
            if (worksheetId == null || problemId.equalsIgnoreCase(Constants.PROBLEMID_ACTIVE))
            {
                boolean isArchiveWS = isActiveArchiveWorksheet(username, (String)params.get(Constants.EXECUTE_ORG_ID), (String)params.get(Constants.EXECUTE_ORG_NAME));
                if (isArchiveWS)
                {
                    worksheetId = null;
                }
                else
                {
                    worksheetId = getActiveWorksheet(username, (String)params.get(Constants.EXECUTE_ORG_ID), (String)params.get(Constants.EXECUTE_ORG_NAME));
                }

                if (worksheetId != null)
                {
                    if(!isWorksheetExist(worksheetId, null, null, null, null, null, null, username))
                    {
                        worksheetId = null;
                    }
                }
            }

            if (StringUtils.isBlank(worksheetId))
            {
                try
                {
                    worksheet = createNewWorksheet(params, username);
                }
                catch(Throwable t)
                {
                    throw new Exception(t.getMessage());
                }
                
                newWorksheet = true;
            }
        }
        else if (problemId.equalsIgnoreCase(Constants.PROBLEMID_NEW) || problemId.equals("-1"))
        {
            try
            {
                worksheet = createNewWorksheet(params, username);
            }
            catch(Throwable t)
            {
            	Log.log.error("Exception Creating Worksheet", t);
                throw new Exception (t.getMessage());
            }
            
            newWorksheet = true;
        }

        // init reference and alertid
        if (newWorksheet == false)
        {
            worksheet = findWorksheetById(worksheetId, username);
            
            if (worksheet == null)
            {
                String errMsg = "Unable to find worksheet with specified worksheet/problem id " + worksheetId;
                Log.log.warn(errMsg);
                throw new Exception(errMsg);
            }
            
            reference = worksheet.getReference();
            alertId = worksheet.getAlertId();
            
            // Fix for JIRA RBA-10193 - Blank summary in worksheet use case
            if (StringUtils.isBlank(worksheet.getSummary()) || StringUtils.isBlank(worksheet.getAssignedTo()))
            {
                /*
                 * During execution, if assignedTo or summary of Worksheet is blank,
                 * populate it appropriately.
                 */
                if (StringUtils.isBlank(worksheet.getAssignedTo()))
                {
                    UsersVO assignedTo = ServiceHibernate.getUser(username);
                    if(assignedTo != null)
                    {
                        worksheet.setAssignedTo(assignedTo.getUUserName());
                        worksheet.setAssignedToName(assignedTo.getUDisplayName());
                    }
                }

                if (StringUtils.isBlank(worksheet.getSummary()))
                {
                    String summary = null;
                    if (StringUtils.isNotBlank((String) params.get(WorksheetUtils.WIKI)))
                    {
                        summary = "Runbook: " + params.get(WorksheetUtils.WIKI);
                    }
                    else
                    {
                        summary = "ActionTask Execution";
                    }
                    worksheet.setSummary(summary);
                }

                WorksheetIndexAPI.indexWorksheet(worksheet, username);
            }
        }

        if (reference == null)
        {
            reference = "";
        }
        if (alertId == null)
        {
            alertId = "";
        }

        // init params
        params.put(Constants.EXECUTE_PROBLEMID, worksheetId);
        params.put(Constants.EXECUTE_REFERENCE, reference);
        params.put(Constants.EXECUTE_ALERTID, alertId);
        params.put(Constants.EXECUTE_CORRELATIONID, worksheet.getCorrelationId());

        return worksheet;
    } // initWorksheet

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Worksheet createNewWorksheet(final Map params, final String username)
    {
        Worksheet worksheet = null;

        String problemNumber = null;
        if (params != null && !params.containsKey(WorksheetUtils.NUMBER))
        {
            problemNumber = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PRB);
        }

        String problemId = SearchUtils.getNewRowKey();
        worksheet = new Worksheet(SearchUtils.getNewRowKey(), null, username);
        worksheet.setSysId(problemId);

        // assigned_to
        String assignedTo = (String) params.get(WorksheetUtils.ASSIGNED_TO);
        if(StringUtils.isBlank(assignedTo))
        {
            assignedTo = username;
        }

        if (StringUtils.isNotBlank(assignedTo) && !assignedTo.equalsIgnoreCase("system"))
        {
            if (!userNameToDisplayName.containsKey(assignedTo))
            {
                Users/*VO*/ user = UserUtils./*get*/findUser("", assignedTo);
    
                if(user != null)
                {
                    userNameToDisplayName.putIfAbsent(user.getUUserName(), user.buildUDisplayName());
//                    worksheet.setAssignedTo(user.getUUserName());
//                    worksheet.setAssignedToName(user.getUDisplayName());
                }
            }
            
            if (userNameToDisplayName.containsKey(assignedTo))
            {
                worksheet.setAssignedTo(assignedTo);
                worksheet.setAssignedToName(userNameToDisplayName.get(assignedTo));
            }
        }

        if (params != null)
        {
            
            if (params.containsKey(Constants.EXECUTE_INCIDENT_ID)){
                worksheet.setIncidentId((String) params.get(Constants.EXECUTE_INCIDENT_ID));
            }
            
            // number
            String numValue = null;
            if (params.containsKey(WorksheetUtils.NUMBER))
            {
                numValue = (String) params.get(WorksheetUtils.NUMBER);
            }
            else
            {
                numValue = problemNumber;
            }
            worksheet.setNumber(numValue);
            params.put(Constants.EXECUTE_PROBLEM_NUMBER, numValue);

            // reference
            String refValue = null;
            if (params.containsKey(WorksheetUtils.REFERENCE))
            {
                refValue = (String) params.get(WorksheetUtils.REFERENCE);
            }
            else if (params.containsKey(WorksheetUtils.SERIAL))
            {
                refValue = (String) params.get(WorksheetUtils.SERIAL);
            }
            else
            {
                refValue = "";
            }
            worksheet.setReference(refValue);

            // identifier
            String alertValue = null;
            if (params.containsKey(WorksheetUtils.ALERTID))
            {
                alertValue = (String) params.get(WorksheetUtils.ALERTID);
            }
            else if (params.containsKey(WorksheetUtils.IDENTIFIER))
            {
                alertValue = (String) params.get(WorksheetUtils.IDENTIFIER);
            }
            else
            {
                alertValue = "";
            }
            worksheet.setAlertId(alertValue);

            // correlation
            String correlationValue = null;
            if (params.containsKey(WorksheetUtils.CORRELATIONID))
            {
                correlationValue = (String) params.get(WorksheetUtils.CORRELATIONID);
            }
            else
            {
                correlationValue = "";
            }
            worksheet.setCorrelationId(correlationValue);
            
            // sirid
            String sirId = params.containsKey(WorksheetUtils.SIRID) ? (String)params.get(WorksheetUtils.SIRID) : "";
            worksheet.setSirId(sirId);

            // summary
            String summary = "";
            if (!StringUtils.isEmpty((String) params.get(WorksheetUtils.WORKSHEET_SUMMARY)))
            {
                summary = (String)params.get(WorksheetUtils.WORKSHEET_SUMMARY);
            }
            else if (!StringUtils.isEmpty((String) params.get(WorksheetUtils.WIKI)))
            {
                summary += "Runbook: " + params.get(WorksheetUtils.WIKI);
            }
            else
            {
                summary += "ActionTask Execution";
            }
            worksheet.setSummary(summary);

            // description
            String description = "";
            if (!StringUtils.isEmpty((String) params.get(WorksheetUtils.WORKSHEET_DESCRIPTION)))
            {
                description = (String)params.get(WorksheetUtils.WORKSHEET_DESCRIPTION);
            }
            worksheet.setDescription(description);

            // work notes
            worksheet.setWorkNotes("");
            
            // Org
            
            String orgIdInt = (String)params.get(Constants.EXECUTE_ORG_ID);
            
            if (StringUtils.isBlank(orgIdInt) && StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_NAME)))
            {
                if (!((String)params.get(Constants.EXECUTE_ORG_NAME)).equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                {
                    // Identify org id based on org name
                    
                    OrgsVO orgsVO = UserUtils.getOrg(null, (String)params.get(Constants.EXECUTE_ORG_NAME));
                    
                    if (orgsVO != null)
                    {
                        orgIdInt = orgsVO.getSys_id();
                    }
                }
            }
            
            if (!UserUtils.isAdminUser(username))
            {
                if (StringUtils.isNotBlank(orgIdInt))
                {
                    if (UserUtils.isOrgAccessible(orgIdInt, username, false, false))
                    {
                        worksheet.setSysOrg(orgIdInt);
                        
                        if (!params.containsKey(Constants.EXECUTE_ORG_ID))
                        {
                            params.put(Constants.EXECUTE_ORG_ID, orgIdInt);
                        }
                    }
                    else
                    {
                        Log.log.warn("User " + username + " does not have permission to create worksheet for specified Org with id " + 
                                     orgIdInt);
                        throw new RuntimeException("ERROR/FAILURE: User " + username + " does not have permission to create worksheet for specified Org.");
                    }
                }
                else
                {
                    if (!UserUtils.isNoOrgAccessible(username))
                    {
                        Log.log.warn("User " + username + " does not have permission to create worksheet for No Org 'None'.");
                        throw new RuntimeException("ERROR/FAILURE: User " + username + " does not have permission to create worksheet for No Org 'None'.");
                    }
                }
            }
            else
            {
                if (StringUtils.isNotBlank(orgIdInt))
                {
                    worksheet.setSysOrg(orgIdInt);
                    
                    if (!params.containsKey(Constants.EXECUTE_ORG_ID))
                    {
                        params.put(Constants.EXECUTE_ORG_ID, orgIdInt);
                    }
                }
            }
            
            /*
            Set<String> aggrOrgIdsInHierarchy = new HashSet<String>();
            boolean checkOrgAccess = false;
            
            if (!username.equalsIgnoreCase("admin") && !username.equalsIgnoreCase("system"))
            {
                /*
                 * Restrict creation of org worksheet by non admin/system users to 
                 * orgs in hierarchy only
                 *
                
                checkOrgAccess = true;
                
                UsersVO usersVO = UserUtils.getUserNoCache(username);
                
                if (usersVO != null && usersVO.getUserOrgs() != null && !usersVO.getUserOrgs().isEmpty())
                {
                    Set<OrgsVO> userOrgVOs = usersVO.getUserOrgs();
                    
                    for (OrgsVO userOrgsVO : userOrgVOs)
                    {
                        Set<OrgsVO> orgsInHierarchy = UserUtils.getAllOrgsInHierarchy(userOrgsVO.getSys_id(), userOrgsVO.getUName());
                        
                        if (orgsInHierarchy != null && !orgsInHierarchy.isEmpty())
                        {
                            for (OrgsVO orgInHierarchyVO : orgsInHierarchy)
                            {
                                aggrOrgIdsInHierarchy.add(orgInHierarchyVO.getSys_id());
                            }
                        }
                    }
                }
            }
            
            if (StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_ID)))
            {
                if (checkOrgAccess)
                {
                    if (!aggrOrgIdsInHierarchy.contains((String)params.get(Constants.EXECUTE_ORG_ID)))
                    {
                        Log.log.warn("User " + username + " does not have permission to create worksheet for specified org with id " + 
                                     (String)params.get(Constants.EXECUTE_ORG_ID));
                        throw new RuntimeException("ERROR/FAILURE: User " + username + " does not have permission to create worksheet for specified org");
                    }
                }
                
                worksheet.setSysOrg((String)params.get(Constants.EXECUTE_ORG_ID));
            }
            
            if (StringUtils.isBlank((String)params.get(Constants.EXECUTE_ORG_ID)) &&
                StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_NAME)))
            {
                // Identify org id based on org name
                
                OrgsVO orgsVO = UserUtils.getOrg(null, (String)params.get(Constants.EXECUTE_ORG_NAME));
                
                if (orgsVO != null)
                {
                    if (checkOrgAccess)
                    {
                        if (!aggrOrgIdsInHierarchy.contains(orgsVO.getSys_id()))
                        {
                            Log.log.warn("User " + username + " does not have permission to create worksheet for specified org named " + 
                                         (String)params.get(Constants.EXECUTE_ORG_NAME));
                            throw new RuntimeException("ERROR/FAILURE: User " + username + " does not have permission to create worksheet for specified org");
                        }
                    }
                    
                    worksheet.setSysOrg(orgsVO.getSys_id());
                    params.put(Constants.EXECUTE_ORG_ID, orgsVO.getSys_id());
                }
            }*/
        }
        worksheet.setCondition("");
        worksheet.setSeverity("");

        worksheet.setWorksheet("");

        WorksheetIndexAPI.indexWorksheet(worksheet, username);

        return worksheet;
    } // createNewWorksheet
    
    public static void createNewExecutionSummary(Map<String, String> rbcParams, String username)
    {
    	String rbc = rbcParams.get(LicenseEnum.RBC.getKeygenName());
        String problemId = rbcParams.get(Constants.WORKSHEET_PROBLEMID);
        String reference = rbcParams.get(Constants.EXECUTE_REFERENCE);
        String wikiName = rbcParams.get(Constants.WIKI);
        String sysOrg = rbcParams.get(Constants.EXECUTE_ORG_ID);
        String rbcSource = rbcParams.get(LicenseEnum.RBC_SOURCE.getKeygenName());
        
        rbcParams.remove(LicenseEnum.RBC.getKeygenName());
        rbcParams.remove(Constants.WORKSHEET_PROBLEMID);
        rbcParams.remove(Constants.EXECUTE_REFERENCE);
        rbcParams.remove(Constants.WIKI);
        rbcParams.remove(Constants.EXECUTE_ORG_ID);
        rbcParams.remove(LicenseEnum.RBC_SOURCE.getKeygenName());
        
        String runbookParams = rbcParams.toString();
        
        ExecutionSummary es = new ExecutionSummary(Guid.getGuid(true), sysOrg);
        
        es.setRbc(rbc);
        es.setWorksheetId(problemId);
        es.setRunbookParams(runbookParams);
        es.setSysCreatedBy(username);
        es.setSysUpdatedBy(username);
        es.setReference(reference);
        if (StringUtils.isBlank(wikiName))
        {
            wikiName = "";
        }
        es.setRunbook(wikiName);
        String hash = bcryptExecutionSummaryHash(rbc + wikiName.toLowerCase() + runbookParams);
        es.setHash(hash);
        if (StringUtils.isNotBlank(rbcSource)) {
        	es.setRbcSource(rbcSource);
        }
        
        WorksheetIndexAPI.indexExecutionSummary(es, false, username);
    }

    public static String getOldestWorksheetByProcessId(String processId)
    {
        String result = null;

        //get the process requests by ascending order of updated date then the first one if the oldest
        QueryDTO queryDTO = new QueryDTO(0, 10);
        queryDTO.addFilterItem(new QueryFilter("sysId", QueryFilter.EQUALS, processId)).addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));

        try
        {
            List<ProcessRequest> processRequests = WorksheetSearchAPI.searchProcessRequests(queryDTO, "system").getRecords();
            if(processRequests.size() > 0)
            {
                result = processRequests.get(0).getProblem();
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public static String findWorksheetByColumnValues(Map<String, String> params)
    {
        return findWorksheetIdByColumnValues(params);
    }
    
    public static String findWorksheetIdByColumnValues(Map<String, String> params)
    {
        String result = null;
        try
        {
            String number = params.get(WorksheetUtils.NUMBER) != null ? params.get(WorksheetUtils.NUMBER).trim() : null;
            String reference = params.get(WorksheetUtils.REFERENCE) != null ? params.get(WorksheetUtils.REFERENCE).trim() : null;
            String alertId = params.get(WorksheetUtils.ALERTID) != null ? params.get(WorksheetUtils.ALERTID).trim() : null;
            String correlationId = params.get(WorksheetUtils.CORRELATIONID) != null ? params.get(WorksheetUtils.CORRELATIONID).trim() : null;
            String orgId = params.get(Constants.EXECUTE_ORG_ID) != null ? params.get(Constants.EXECUTE_ORG_ID).trim() : null;
            String orgName = params.get(Constants.EXECUTE_ORG_NAME) != null ? params.get(Constants.EXECUTE_ORG_NAME).trim() : null;
            String username = params.get(Constants.EXECUTE_USERID);
            if (StringUtils.isBlank(username)) { // if params doesn't contain USERID, check if it has USERNAME
                username = params.get(Constants.HTTP_REQUEST_USERNAME);
            }
            if (StringUtils.isBlank(username)) { // still no username, maybe we could set it as "system"
                username = "system";
            }
            result = findWorksheetSysId(null, number, reference, alertId, correlationId, orgId, orgName, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }// findWorksheetIdByColumnValues
    
    public static String getWorksheetIdByProcessId(String processId)
    {
        String result = null;
        try
        {
            ProcessRequest processRequest = WorksheetSearchAPI.findProcessRequestsById(processId, "system").getData();
            if(processRequest != null)
            {
                result = processRequest.getProblem();
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }// getWorksheetIDByProcessID

    public static Set<String> getWorksheetProcessids(String problemId)
    {
        Set<String> result = new HashSet<String>();

        QueryDTO queryDTO = new QueryDTO();
        queryDTO.addFilterItem(new QueryFilter("problem", QueryFilter.EQUALS, problemId));
        try
        {
            List<ProcessRequest> processRequests = WorksheetSearchAPI.searchProcessRequests(queryDTO, "system").getRecords();
            for(ProcessRequest processRequest : processRequests)
            {
                result.add(processRequest.getSysId());
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }// getWorksheetProcessids

    public static void flushWorksheetDebug(ScriptDebug debug)
    {

        if (!debug.isDebug() || debug.isBufferEmpty())
            return;

        String str = debug.flush().toString();

        try
        {
            Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(debug.getProblemId(), "system").getData();
            if(worksheet != null)
            {
                String debugString = worksheet.getDebug();
                if(StringUtils.isBlank(debugString))
                {
                    debugString = str;
                }
                else
                {
                    debugString = debugString.concat(str);
                }
                worksheet.setDebug(debugString);

                //index for latest debug
                WorksheetIndexAPI.indexWorksheet(worksheet, "system");
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }

    }

    public static List<ExecuteState> getEventRowsFor(boolean event_end, String processid, String problemid, String eventid)
    {
        List<ExecuteState> rows = null;

        if(StringUtils.isNotEmpty(problemid) && StringUtils.isNotEmpty(processid) && StringUtils.isNotEmpty(eventid))
        {
            try
            {
                // try with eventid / targetaddr first
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processid))
                    .addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemid))
                    .addFilterItem(new QueryFilter("targetAddress", QueryFilter.EQUALS, eventid));
                /*
                IndexQuery<String, String> query = CassandraUtil.getResolveKeyspace().prepareQuery(CassandraUtil.getExecuteStateCF()).searchWithIndex().setRowLimit(10).autoPaginateRows(true)
                    .addExpression().whereColumn("u_process_id").equals().value(processid)
                    .addExpression().whereColumn("u_problem_id").equals().value(problemid)
                    .addExpression().whereColumn("u_targetaddr").equals().value(eventid);

                rows = CassandraUtil.searchByIndex(query);
                */
                rows = WorksheetSearchAPI.searchExecuteStates(queryDTO, "system").getRecords();
                if (rows == null || rows.size() == 0)
                {
                    // try without eventid
                    queryDTO = new QueryDTO();
                    queryDTO.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processid))
                        .addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemid));
                    /*
                    query = CassandraUtil.getResolveKeyspace().prepareQuery(CassandraUtil.getExecuteStateCF()).searchWithIndex().setRowLimit(10).autoPaginateRows(true)
                        .addExpression().whereColumn("u_process_id").equals().value(processid)
                        .addExpression().whereColumn("u_problem_id").equals().value(problemid);
                    rows = CassandraUtil.searchByIndex(query);
                    */
                    rows = WorksheetSearchAPI.searchExecuteStates(queryDTO, "system").getRecords();
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error getting rows for processid: " + processid + " && problemid:" + problemid, e);
            }
        }

        return rows;
    }

    public static List<ExecuteState> getEventRowsByReference(boolean event_end, String event_reference, String problemid, String eventid)
    {
        List<ExecuteState> rows = null;

        if(StringUtils.isNotEmpty(event_reference) && StringUtils.isNotEmpty(problemid))
        {

            try
            {
                /*
                IndexQuery<String, String> query = CassandraUtil.getResolveKeyspace().prepareQuery(CassandraUtil.getExecuteStateCF()).searchWithIndex().setRowLimit(10000).autoPaginateRows(true)
                    .addExpression().whereColumn("u_reference").equals().value(event_reference+eventid)
                    .addExpression().whereColumn("u_problem_id").equals().value(problemid);
                */
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.addFilterItem(new QueryFilter("reference", QueryFilter.EQUALS, event_reference+eventid))
                    .addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemid));

                // append eventid (for in memory EVENT_END == false)
                if (!event_end)
                {
                    // NOTE: Using targetAddr for eventid
                    //query.addExpression().whereColumn("u_targetaddr").equals().value(eventid);
                    queryDTO.addFilterItem(new QueryFilter("targetAddress", QueryFilter.EQUALS, eventid));
                }

                //rows = CassandraUtil.searchByIndex(query);
                rows = WorksheetSearchAPI.searchExecuteStates(queryDTO, "system").getRecords();
            }
            catch (Exception e)
            {
                Log.log.error("Error getting rows for event_reference: " + event_reference + " && problemid:" + problemid, e);
            }
        }

        return rows;
    }

    public static String getProcessNumberByProcessId(String processid)
    {
        String result = null;
        try
        {
            ProcessRequest processRequest = WorksheetSearchAPI.findProcessRequestsById(processid, "system").getData();
            if(processRequest != null)
            {
                result = processRequest.getNumber();
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public static String getProblemNumberByProblemId(String problemid)
    {
        String result = null;
        try
        {
            Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(problemid, "system").getData();
            if(worksheet != null)
            {
                result = worksheet.getNumber();
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    @SuppressWarnings({ "unused" })
    public static String createExecuteProcessRequestCAS(String wiki, String newProcessNumber, String problemId, String processId, Map<String, Object> params, String user, boolean isEvent) throws Exception
    {
        boolean existingProcessid = false;

        String rowKey = StringUtils.isEmpty(processId) ? SearchUtils.getNewRowKey() : processId;

        try
        {
            // check if the process request has already been created
            if (StringUtils.isBlank(processId))
            {
                ProcessRequest processRequest = new ProcessRequest(rowKey, null, user);
                
                if (!(params.containsKey(Constants.WORKSHEET_NUMBER) && 
                      StringUtils.isNotBlank((String)params.get(Constants.WORKSHEET_NUMBER))))
                {
                    Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(problemId, user).getData();
                
                    params.put(Constants.WORKSHEET_NUMBER, worksheet.getNumber());
                    
                }
                
//                if(worksheet != null)
                if (params.containsKey(Constants.WORKSHEET_NUMBER) && 
                    StringUtils.isNotBlank((String)params.get(Constants.WORKSHEET_NUMBER)))
                {
                    processRequest.setNumber(newProcessNumber);
                    processRequest.setProblem(problemId);
//                    processRequest.setProblemNumber(worksheet.getNumber());
                    processRequest.setProblemNumber((String)params.get(Constants.WORKSHEET_NUMBER));
                    processRequest.setWiki(wiki);
                    processRequest.setStatus(Constants.ASSESS_STATUS_OPEN);
                    processRequest.setTimeout((String) params.get(Constants.PROCESS_TIMEOUT));
                    WorksheetIndexAPI.indexProcessRequest(processRequest, user);
                }
            }

        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return rowKey;
    } // createExecuteProcessRequestCAS

    public static boolean isThisWorksheetCurrent(String problemId)
    {
        boolean result = false;

        if (StringUtils.isNotBlank(problemId))
        {
            try
            {
                result = isWorksheetExist(problemId, null, null, null, null, null, null, "system");
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    static class ATIVOHelper implements Comparable<ATIVOHelper>
    {
        long time;
        ActionTaskInfoVO ati;

        public ATIVOHelper(long t, ActionTaskInfoVO a)
        {
            time = t;
            ati = a;
        }

        public int compareTo(ATIVOHelper o)
        {
            if (this==o)
                return 0;

            return (int) (this.time - o.time);
        }
    }

    public static Set<String> getAllActionTaskSysIdsForProblemId(String problemId)
    {
        Set<String> atNames = new HashSet<String>();

        if(StringUtils.isNotEmpty(problemId))
        {
            try
            {
                // init problem
                List<TaskResult> taskResults = WorksheetSearchAPI.findTaskResultByWorksheetId(problemId, null, "system").getRecords();
                if(taskResults != null )
                {
                    for(TaskResult taskResult : taskResults)
                    {
                        atNames.add(taskResult.getTaskId());
                    }
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return atNames;

    }//getAllActionTaskForProblemSys_id

    public static List<TaskResult> findByWorksheetTaskAndNodeId(String problemId, String taskId, String nodeId, boolean isAscending)
    {
        List<TaskResult> result = new ArrayList<TaskResult>();
        QueryDTO queryDTO = new QueryDTO();
        if(isAscending)
        {
            queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));
        }
        else
        {
            queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.DESC));
        }

        if(StringUtils.isNotBlank(problemId))
        {
            queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemId));
        }
        if(StringUtils.isNotBlank(taskId))
        {
            queryDTO.addFilterItem(new QueryFilter("taskId", QueryFilter.EQUALS, taskId));
        }
        if(StringUtils.isNotBlank(nodeId))
        {
            queryDTO.addFilterItem(new QueryFilter("nodeId", QueryFilter.EQUALS, taskId));
        }

        try
        {
            ResponseDTO<TaskResult> responseDTO = WorksheetSearchAPI.searchTaskResults(queryDTO, "system");
            if(responseDTO != null && responseDTO.getRecords() != null && responseDTO.getRecords().size() > 0)
            {
                for(TaskResult actionResult : responseDTO.getRecords())
                {
                    result.add(actionResult);
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public List<TaskResult> findByWorksheetAndTaskId(String problemId, List<String> actionTaskIds, boolean isAscending) throws SearchException
    {
        List<TaskResult> result = new ArrayList<TaskResult>();
        QueryDTO queryDTO = new QueryDTO();
        if(isAscending)
        {
            queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));
        }
        else
        {
            queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.DESC));
        }

        if(StringUtils.isNotBlank(problemId))
        {
            queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemId));
        }

        result = WorksheetSearchAPI.findTaskResultByWorksheetId(problemId, actionTaskIds, "system").getRecords();

        return result;
    }

    public static String getActionResultDetail(String problem_id, String task_id, String node_id, String separator, Integer max, Integer offset)
    {
        String value = "";

        try
        {
            Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(problem_id, "system").getData();
            if (worksheet != null) //Cassandra
            {
                //Get data from rssearch
                List<TaskResult> actionResults = findByWorksheetTaskAndNodeId(problem_id, task_id, node_id, false);
                if (actionResults.size() == 1)
                {
                    value = actionResults.iterator().next().getDetail();
                }
                else if (actionResults.size() > 1)
                {
                    StringBuilder values = new StringBuilder();
                    int count=0;
                    int displayed=0;
                    for(TaskResult actionResult : actionResults)
                    {
                        if (offset == null || count >= offset)
                        {
                            values.append(actionResult.getDetail());
                            values.append(separator);
                            displayed++;
                        }
                        count++;
                        if (max != null && displayed == max)
                        {
                            break;
                        }
                    }
                    if(StringUtils.isNotEmpty(separator) && values.length() > separator.length())
                    {
                        values.setLength(values.length() - separator.length());
                    }
                    value = values.toString();
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return value;
    }

    public static boolean isProcessCompleted(String problemId, String wiki)
    {
        boolean result = true;
        final long INIT_REFRESH_DELAY = 10000;

        try
        {
            if(StringUtils.isNotBlank(problemId))
            {
                List<ProcessRequest> processRequests = null;

                if (StringUtils.isBlank(wiki))
                {
                        processRequests = WorksheetSearchAPI.findProcessRequestByWorksheetId(problemId, "system").getRecords();
                }
                else
                {
                    //number 10 is taken from the old API.
                    QueryDTO queryDTO = new QueryDTO(0,  10);
                    queryDTO.addFilterItem(new QueryFilter("problem", QueryFilter.EQUALS, problemId))
                        .addFilterItem(new QueryFilter("wiki", QueryFilter.EQUALS, wiki.trim()));
                    processRequests = WorksheetSearchAPI.searchProcessRequests(queryDTO, "system").getRecords();
                }
                if(!processRequests.isEmpty())
                {
                    for(ProcessRequest processRequest : processRequests)
                    {
                        if(!Constants.ASSESS_STATUS_COMPLETED.equalsIgnoreCase(processRequest.getStatus()) && !Constants.ASSESS_STATUS_ABORTED.equalsIgnoreCase(processRequest.getStatus()))
                        {
                            result = false;
                            break;
                        }
                    }
                }
                else
                {
                    Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(problemId, "system").getData();
                    if(worksheet != null)
                    {
                        if ((System.currentTimeMillis() - worksheet.getSysUpdatedOn()) < INIT_REFRESH_DELAY)
                        {
                            result = false;
                        }
                    }
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }
    
    public static Pair<String, Integer> getProcessStatusAndATExecCountForRunbook(String processID, 
    																			 boolean ignoreESInMemCache)  {
        Pair<String, Integer> procStatusAndATExecCount = Pair.of(null,  VO.NON_NEGATIVE_INTEGER_DEFAULT);
        
        try {
            ProcessRequest processRequest = WorksheetSearchAPI.findProcessRequestsById(processID, "system", 
            																		   ignoreESInMemCache).getData();
            if(processRequest != null) {
                String status = processRequest.getStatus();
                Integer atExecCount = processRequest.getuATExecCount();
                procStatusAndATExecCount = Pair.of(status, atExecCount);
                if (Log.log.isDebugEnabled()) {
                	Log.log.debug(String.format("Found process Id: %s, Process State: %d", 
                								procStatusAndATExecCount.getLeft(), procStatusAndATExecCount.getRight()));
                }
            }
        } catch (SearchException e) {
            Log.log.error(e.getMessage(), e);
        }
        return procStatusAndATExecCount;
    }//getProcessStatus
    
    public static Pair<String, Integer> getProcessStatusAndATExecCountForRunbook(String processID)
    {
    	return WorksheetUtil.getProcessStatusAndATExecCountForRunbook(processID, false);
    }//getProcessStatus
    
    public static String getProcessStatusForRunbook(String processID, boolean ignoreESInMemCache) {
        return getProcessStatusAndATExecCountForRunbook(processID, ignoreESInMemCache).getLeft();
    }//getProcessStatus
    
    public static String getProcessStatusForRunbook(String processID)
    {
        return getProcessStatusAndATExecCountForRunbook(processID).getLeft();
    }//getProcessStatus


    public static String getSummaryResultForRunbook(String processID)
    {
        JSONObject output = new JSONObject();

        try
        {
            ResponseDTO<ProcessRequest> processRequestResponse;
            processRequestResponse = WorksheetSearchAPI.findProcessRequestsById(processID, "system");
            if(processRequestResponse != null && processRequestResponse.getData() != null)
            {
                ProcessRequest processRequest = processRequestResponse.getData();
                String problemNum = processRequest.getProblemNumber();
                String wikiName = processRequest.getWiki();

                QueryDTO queryDTO = new QueryDTO(0, 10);
                queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, processRequest.getProblem()));
                ResponseDTO<TaskResult> actionResults = WorksheetSearchAPI.searchTaskResults(queryDTO, "system");

                if(actionResults != null && actionResults.getRecords() != null &&  actionResults.getRecords().size() > 0)
                {
                    output.accumulate("worksheetNumber", problemNum);
                    output.accumulate("runbookName", wikiName);

                    JSONArray tasks = new JSONArray();

                    for (TaskResult resultRow : actionResults.getRecords())
                    {
                        JSONObject taskItem = new JSONObject();

                        taskItem.accumulate("taskFullName", resultRow.getTaskFullName());
                        taskItem.accumulate("completion", resultRow.getCompletion());
                        taskItem.accumulate("condition", resultRow.getCondition());
                        taskItem.accumulate("severity", resultRow.getSeverity());
                        taskItem.accumulate("processRequest", resultRow.getProcessNumber());
                        taskItem.accumulate("taskRequest", resultRow.getExecuteRequestNumber());

                        tasks.add(taskItem);
                    }

                    output.accumulate("taskResults", tasks);
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            output.accumulate("error", e.getMessage());
        }

        if(Log.log.isDebugEnabled()) {
            Log.log.debug(output.toString());
        }

        return ExecuteUtil.filterOutput(output.toString());

    } // getSummaryResultForRunbook


    public static String getDetailResultForRunbook(String processID)
    {
        JSONObject output = new JSONObject();

        try
        {
            ResponseDTO<ProcessRequest> processRequestResponse = WorksheetSearchAPI.findProcessRequestsById(processID, "system");
            if(processRequestResponse != null && processRequestResponse.getData() != null)
            {
                ProcessRequest processRequest = processRequestResponse.getData();
                String problemNum = processRequest.getProblemNumber();
                String wikiName = processRequest.getWiki();

                QueryDTO queryDTO = new QueryDTO(0, 10);
                queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, processRequest.getProblem()));
                ResponseDTO<TaskResult> actionResults = WorksheetSearchAPI.searchTaskResults(queryDTO, "system");

                if(actionResults != null && actionResults.getRecords() != null &&  actionResults.getRecords().size() > 0)
                {
                    output.accumulate("worksheetNumber", problemNum);
                    output.accumulate("runbookName", wikiName);

                    JSONArray tasks = new JSONArray();

                    for (TaskResult resultRow : actionResults.getRecords())
                    {
                        JSONObject taskItem = new JSONObject();

                        String detail = resultRow.getDetail();
                        taskItem.accumulate("taskFullName", resultRow.getTaskFullName());
                        taskItem.accumulate("detail", detail==null?"":detail);

                        tasks.add(taskItem);
                    }

                    output.accumulate("taskResults", tasks);
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            output.accumulate("error", e.getMessage());
        }
        if(Log.log.isDebugEnabled()) {
            Log.log.debug(output.toString());
        }

        return ExecuteUtil.filterOutput(output.toString());

    } // getDetailResultForRunbook

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static String getWSDATAForRunbook(String problemid, String username)
    {
        JSONObject output = new JSONObject();

        try
        {
            Map<String, Object> wsData = WorksheetSearchAPI.getWorksheetData(problemid, Collections.<String> emptyList(), username);

            if(wsData != null && wsData.size() != 0)
            {
                Set<String> keys = wsData.keySet();
                for(Iterator<String> it = keys.iterator(); it.hasNext();) {
                    String key = it.next();
                    Object values = wsData.get(key);

                    if(values instanceof String) {
                        output.accumulate(key, values);
                    }

                    else if(values instanceof Integer) {
                        output.accumulate(key, values);
                    }

                    else if(values instanceof ArrayList) {
                        JSONArray list = new JSONArray();


                        for (Object value : (ArrayList)values)
                        {
                            list.add(value);
                        }

                        output.accumulate(key, list);
                    }

                    else if(values instanceof Map) {
                        JSONObject map = new JSONObject();

                        Set<String> mapKeys = ((Map<String, String>) values).keySet();
                        for(Iterator<String> keyMap = mapKeys.iterator(); keyMap.hasNext();) {
                            String mapKey = keyMap.next();
                            String mapValue = (String)((Map<String, String>) values).get(mapKey);

                            map.accumulate(mapKey, mapValue);
                        }

                        output.accumulate(key, map);
                    }

                    else
                        ;
                } // end of for(it).
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            output.accumulate("Error", e.getMessage());
        }

        if(Log.log.isDebugEnabled()) {
            Log.log.debug(output.toString());
        }

        return ExecuteUtil.filterOutput(output.toString());

    } // getWSDATAForRunbook

    public static String getExecuteStatusForActionTask(String executeId)
    {
        throw new RuntimeException("This is obsolete method, change the calling code");
    }//getExecuteStatusForActionTask

    public static String getContentForTaskDetail(String taskresultid)
    {
        String content = "";

        if (StringUtils.isNotBlank(taskresultid))
        {
            try
            {
                ResponseDTO<TaskResult> responseDTO = WorksheetSearchAPI.findTaskResultById(taskresultid, "system");
                if (responseDTO != null && responseDTO.getData() != null)
                {
                    TaskResult taskResult = responseDTO.getData();
                    
                    if (StringUtils.isNotBlank(taskResult.getDetail()))
                    {
                        content =  responseDTO.getData().getDetail();
                    }
                    else
                    {
                        content = "No details provided by task \"" + taskResult.getTaskFullName() + "\"";
                    }
                }
            }
            catch (Throwable e)
            {
                Log.log.error("Error retrieving info for taskId: " + taskresultid, e);
            }
        }

        return content;
    }

    public static String getRawContentForTaskDetail(String executeResultId)
    {
        String content = "";

        if (StringUtils.isNotBlank(executeResultId))
        {
            try
            {
                TaskResult taskResult = WorksheetSearchAPI.findTaskResultByExecuteResultId(executeResultId, "system");

                if(taskResult != null)
                {
                    content = taskResult.getRaw();
                }
            }
            catch (Throwable e)
            {
                Log.log.error("Error retrieving info for executeResultId: " + executeResultId, e);
            }
        }

        return content;
    }

    public static String getSummaryResultForActionTask(String executeId)
    {
        StringBuffer output = new StringBuffer();
        try
        {
            TaskResult taskResult = WorksheetSearchAPI.findTaskResultByExecuteRequestId(executeId, "system");
            if(taskResult != null)
            {
                List<TaskResult> results = WorksheetSearchAPI.findTaskResultByWorksheetId(taskResult.getProblemId(), null, "system").getRecords();

                output.append("========================================================").append("\n");
                output.append("Worksheet Number: ").append(taskResult.getProblemNumber()).append("\n");
    //            output.append(" Action Task Name: ").append(request.getTask().getUFullName()).append("\n");
                output.append("========================================================").append("\n");
                if(results != null && results.size() > 0)
                {
                    for(TaskResult result : results)
                    {
                        output.append("Action Task Name: ").append(result.getTaskFullName()).append("\n");
    //                    output.append(" Summary: ").append(result.getUSummary()).append("\n");
                        output.append("Completion: ").append(result.getCompletion()).append("\n");
                        output.append("Condition: ").append(result.getCondition()).append("\n");
                        output.append("Severity: ").append(result.getSeverity()).append("\n");
                        output.append("Process Request: ").append(result.getProblemNumber()).append("\n");
                        output.append("Task Request: ").append(result.getProblemNumber()).append("\n");
                        output.append("--------------------------------------------------------").append("\n");
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to retrive execute status for executeId : " + executeId, e);
        }

        return output.toString();
    }//getSummaryResultForActionTask

    public static String getDetailResultForActionTask(String executeId)
    {
        StringBuffer output = new StringBuffer("");

        try
        {
            TaskResult taskResult = WorksheetSearchAPI.findTaskResultByExecuteRequestId(executeId, "system");
            if(taskResult != null)
            {
                List<TaskResult> results = WorksheetSearchAPI.findTaskResultByWorksheetId(taskResult.getProblemId(), null, "system").getRecords();

                output.append("========================================================").append("\n");
                output.append(" Worksheet Number: ").append(taskResult.getProblemNumber()).append("\n");
    //            output.append(" Action Task Name: ").append(request.getTask().getUFullName()).append("\n");
                output.append("========================================================").append("\n");
                if(results != null && results.size() > 0)
                {
                    for(TaskResult result : results)
                    {
                        output.append("Action Task Name: ").append(result.getTaskFullName()).append("\n");
                        String detail = (result.getDetail() == null ? "" : result.getDetail());
                        output.append("Detail: ").append(detail).append("\n");
                        output.append("--------------------------------------------------------").append("\n");
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to retrive execute status for executeId : " + executeId, e);
        }

        return output.toString();
    }//getDetailResultForActionTask
    
    @Deprecated
    public static boolean isWorksheetExist(String sys_id, String number, String reference, String alertId, String correlationId)
    {
        return isWorksheetExist(sys_id, number, reference, alertId, correlationId, null, null, "system");
    }
    
    public static boolean isWorksheetExist(String sys_id, String number, String reference, String alertId, String correlationId, String orgId, String orgName, String username)
    {
        boolean exist = false;

        try
        {
            String sysId = findWorksheetSysId(sys_id, number, reference, alertId, correlationId, orgId, orgName, username);
            exist = StringUtils.isNotBlank(sysId);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return exist;
    }//isWorksheetExist

    /*
     *  HP TO DO
     *
     * Allowing access to worksheet from public static APIs without providing username is severe security issue.
     * 
     * Need to blacklist/whitelist resolve packages which can be used from Groovy.
     * 
     * Groovy support whitelisting of packages allowed to be used in the the context. Need to check
     * if it can blacklist packages which can be used in groovy script in context.
     */
    
    @Deprecated
    public static Worksheet findWorksheetById(String sysId)
    {
        return findWorksheetById(sysId, "system");
    }
    
    public static Worksheet findWorksheetById(String sysId, String username)
    {
        Worksheet result = null;
        try
        {
            ResponseDTO<Worksheet> resp = WorksheetSearchAPI.findWorksheetById(sysId, username);
            
            if (resp != null)
            {
                result = resp.getData();
            }
            
            // Check if user has access to worksheet
            if (result != null && !UserUtils.isAdminUser(username))
            {
                if (StringUtils.isNotBlank(result.getSysOrg()))
                {
                    if (!UserUtils.isOrgAccessible(result.getSysOrg(), username, false, false))
                    {
                        result = null;
                        throw new SearchException("User does not belongs to Org of worksheet with id " + sysId + ".");
                    }
                }
                else
                {
                    if (!UserUtils.isNoOrgAccessible(username))
                    {
                        result = null;
                        throw new SearchException("User does not belongs to No Org 'None' of worksheet with id " + sysId + ".");
                    }
                }
            }
        }
        catch (SearchException e)
        {
            //Log.log.trace(e.getMessage(), e);
        }
        return result;
    }
    
    @Deprecated
    public static Worksheet findWorksheet(String sys_id, String number, String reference, String alertId, String correlationId) throws Exception
    {
        return findWorksheet(sys_id, number, reference, alertId, correlationId, null, null, "system");
    }
    
    public static Worksheet findWorksheet(String sys_id, String number, String reference, String alertId, String correlationId, String orgId, String orgName, String username) throws Exception
    {
        Worksheet result = null;

        try
        {
            if(StringUtils.isNotBlank(sys_id))
            {
                result = WorksheetSearchAPI.findWorksheetById(sys_id, "system").getData();
            }
            else
            {
                QueryDTO queryDTO = new QueryDTO(0,  1);
                if(StringUtils.isNotBlank(number))
                {
                    queryDTO.addFilterItem(new QueryFilter("number", QueryFilter.EQUALS, number));
                }
                else if(StringUtils.isNotBlank(reference))
                {
                    queryDTO.addFilterItem(new QueryFilter("reference", QueryFilter.EQUALS, reference));
                }
                else if(StringUtils.isNotBlank(alertId))
                {
                    queryDTO.addFilterItem(new QueryFilter("alertId", QueryFilter.EQUALS, alertId));
                }
                else if(StringUtils.isNotBlank(correlationId))
                {
                    queryDTO.addFilterItem(new QueryFilter("correlationId", QueryFilter.EQUALS, correlationId));
                }
                else if(StringUtils.isNotBlank(orgId))
                {
                    queryDTO.addFilterItem(new QueryFilter("sysOrg", QueryFilter.EQUALS, orgId));
                }
                else if(StringUtils.isNotBlank(orgName) && StringUtils.isBlank(orgId))
                {
                    OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                    
                    if (orgsVO != null)
                    {
                        queryDTO.addFilterItem(new QueryFilter("sysOrg", QueryFilter.EQUALS, orgsVO.getSys_id()));
                    }
                }
                
                if(queryDTO.getFilterItems().size() > 0)
                {
                    List<Worksheet> worksheets = WorksheetSearchAPI.searchWorksheets(queryDTO, username).getRecords();
                    if(worksheets != null && worksheets.size() > 0)
                    {
                        if (worksheets.size() == 1)
                        {
                            result = worksheets.get(0);
                        }
                        else
                        {
                            throw new Exception("Found multiple worksheets for Worksheet Number = " +
                                                (StringUtils.isNotBlank(number) ? number : "") + ", Referene = " +
                                                (StringUtils.isNotBlank(reference) ? reference : "") + ", Alert Id = " +
                                                (StringUtils.isNotBlank(alertId) ? alertId : "") + ", Correlation Id = " +
                                                (StringUtils.isNotBlank(correlationId) ? correlationId : "") + ", and Org Id = " +
                                                (StringUtils.isNotBlank(orgId) ? orgId : ""));
                        }
                    }
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }//findWorksheet
    
    @Deprecated
    public static String findWorksheetSysId(String sys_id, String number, String reference, String alertId, String correlationId) throws Exception
    {
        return findWorksheetSysId(sys_id, number, reference, alertId, correlationId, null, null, "system");
    }
    
    public static String findWorksheetSysId(String sys_id, String number, String reference, String alertId, String correlationId, String orgId, String orgName, String username) throws Exception
    {
        String result = null;
        Worksheet worksheet = findWorksheet(sys_id, number, reference, alertId, correlationId, orgId, orgName, username);
        if(worksheet != null)
        {
            result = worksheet.getSysId();
        }
        return result;
    }//findWorksheetSysId
    
    /**
     * Get name value pairs of worksheet u_reference column and u_alert_id column if values are not null
     *
     * @param problemid
     *            worksheet row key (sysId)
     * @return a Java map with column names and values as entries
     * @throws SearchException
     */
    public static Map<String, String> getWorksheetReferences(String problemid) throws SearchException
    {
        Map<String, String> result = new HashMap<String, String>();

        Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(problemid, "system").getData();
        if(worksheet != null)
        {
            if(StringUtils.isNotBlank(worksheet.getReference()))
            {
                result.put(WorksheetUtils.REFERENCE, worksheet.getReference());
            }
            if(StringUtils.isNotBlank(worksheet.getAlertId()))
            {
                result.put(WorksheetUtils.ALERTID, worksheet.getAlertId());
            }
        }
        return result;
    } // getWorksheetReferences

    public static List<Map<String, Object>> getWorksheetMapsByKeys(List<String> worksheetIds, String[] colNames)
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        try
        {
            List<Worksheet> worksheets = WorksheetSearchAPI.findWorksheetByIds(worksheetIds, "system").getRecords();
            if(worksheets != null && worksheets.size() > 0)
            {
                for(Worksheet worksheet : worksheets)
                {
                    result.add(worksheet.asMap(colNames));
                }
            }
        }
        catch (Exception ex)
        {
            Log.log.error(ex, ex);
            throw new RuntimeException(ex);
        }

        return result;
    }


    public static Map<String, Object> getWorksheetMapByKey(String worksheetId, String[] columns)
    {
        Map<String, Object> result = new HashMap<String, Object>();

        try
        {
            Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(worksheetId, "system").getData();
            if(worksheet != null)
            {
                result = worksheet.asMap(columns);
            }
        }
        catch (Exception ex)
        {
            Log.log.error(ex, ex);
            throw new RuntimeException(ex);
        }

        return result;
    }

    @Deprecated
    public static String getActiveWorksheet(String userid)
    {
        return getActiveWorksheet(userid, null, null);
    } // getActiveWorksheet
    
    private static Pair<Boolean, String> sessionWorksheetExists(ResolveSession rs, String userid) {
    	String effectiveWorksheetId = (rs.ugetUIsArchive().booleanValue() && rs.getArchiveWorksheet() != null) ? 
                					  rs.getArchiveWorksheet().getSys_id() : rs.getUWorksheetId() ;

        Worksheet activeWorksheet = null;

        if (StringUtils.isNotBlank(effectiveWorksheetId)) {
        	if (rs.ugetUIsArchive().booleanValue()) {
        		activeWorksheet = ServiceWorksheet.getArchiveWorksheet(effectiveWorksheetId, 
	 															userid);
        	} else {
        		ResponseDTO<Worksheet> worksheetRespDTO = null;
        		
        		try {
					worksheetRespDTO = WorksheetSearchAPI.findWorksheetById(effectiveWorksheetId, userid);
				} catch (SearchException e) {
					//Log ResolveSession not getting active worksheet.
					
					Log.log.debug(String.format("Error [%s] in finding active worksheet with id %s in ES for Resolve " +
												"Session belonging to User %s with Org %s", e.getMessage(),
												effectiveWorksheetId, userid, rs.getSysOrg()));
				}

        		if (worksheetRespDTO != null && worksheetRespDTO.isSuccess()) {
        			activeWorksheet = worksheetRespDTO.getData();
        		}
        	}
        }

        if (Log.log.isTraceEnabled()) {
        	Log.log.trace(String.format("Resolve Session Id(%s) Effective Worksheet Id(%s)", rs.getSys_id(), 
        								effectiveWorksheetId));
        }
        
    	return activeWorksheet != null ? Pair.of(Boolean.TRUE, activeWorksheet.getNumber()) : Pair.of(Boolean.FALSE, null);
    }
    
    public static String getActiveWorksheet(String userid, String orgId, String orgName)
    {
        String result = null;

        if (StringUtils.isNotBlank(userid))
        {
            try
            {
              HibernateProxy.setCurrentUser("system");
            	result = (String) HibernateProxy.execute(() -> {
                    ResolveSession query = new ResolveSession();
                    
                    query.setUActiveType(userid);
                    
                    String orgIdInt = orgId;
                    
                    if (StringUtils.isNotBlank(orgId) || StringUtils.isNotBlank(orgName))
                    {
                        if (StringUtils.isBlank(orgId))
                        {
                            // Identify org id based on org name
                            
                            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                            
                            if (orgsVO != null)
                            {
                                orgIdInt = orgsVO.getSys_id();
                            }
                        }
                        
                        if (StringUtils.isNotBlank(orgIdInt))
                        {
                            query.setSysOrg(orgIdInt);
                        }
                    }
                    
                    ResolveSession session = null;
                    
                    List<OrderbyProperty> orderByList = new ArrayList<OrderbyProperty>();
                    orderByList.add(new OrderbyProperty("sysUpdatedOn", false));
                    
                    List<ResolveSession> resolveSessions = HibernateUtil.getDAOFactory().getResolveSessionDAO().find(query, 
                    																								 orderByList);
                    
                    // If OrgIdInt is blank then filter out Resolve Sessions belonging to other Orgs
                    
                    List<ResolveSession> filteredResolveSessions = resolveSessions;
                    
                    if (CollectionUtils.isNotEmpty(resolveSessions) && StringUtils.isBlank(orgIdInt)) {
                    	filteredResolveSessions = resolveSessions.parallelStream()
    		 					 				  .filter(rs -> StringUtils.isBlank(rs.getSysOrg()))
    		 					 				  .collect(Collectors.toList());
                    }
                    
                    if (CollectionUtils.isNotEmpty(filteredResolveSessions))
                    {
                        for (ResolveSession rs : filteredResolveSessions)
                        {
                        	if (sessionWorksheetExists(rs, userid).getLeft().booleanValue())
                            {
                                session = rs;
                                break;
                            }  
                        }
                    }
                    
                    if (session == null) {
                        /*
                         * There no resolve session for this user.
                         * So, create one, create a new worksheet, make it active and assigned it to this user.
                         */
                        return checkAndCreateNewWorksheet(userid, null, orgId, orgName);
                    } else {
                    	return (session.ugetUIsArchive().booleanValue() && session.getArchiveWorksheet() != null) ? 
          					  	 session.getArchiveWorksheet().getSys_id() : session.getUWorksheetId() ;
                    }

                    
            	});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                result = null;
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        return result;
    } // getActiveWorksheet
    
    public static Collection<String> getAllActiveWorksheetIds(String userid)
    {
        Collection<String> allActiveWorkSheetIds = new ArrayList<>();
        
        if (StringUtils.isNotBlank(userid))
        {
            try
            {               
                HibernateProxy.execute( () -> {
                	 ResolveSession query = new ResolveSession();
                     
                     query.setUActiveType(userid);
                     
                     List<ResolveSession> resolveSessions = HibernateUtil.getDAOFactory().getResolveSessionDAO().find(query);
                     
                     if (resolveSessions != null && !resolveSessions.isEmpty())
                     {           
                         
                         for (ResolveSession rs : resolveSessions)
                         {
                             if (rs.ugetUIsArchive())
                             {
                                 if (rs.getArchiveWorksheet() != null)
                                 {
                                     allActiveWorkSheetIds.add(rs.getArchiveWorksheet().getSys_id());
                                 }
                             }
                             else
                             {
                                 allActiveWorkSheetIds.add(rs.getUWorksheetId());
                             }
                         }
                     }
                });

               
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return allActiveWorkSheetIds;
    }
    
    /**
     * Check whether user has active worksheet. If not create one and set it active.
     *
     * @param userName
     * @param problemId
     * @param orgId
     * @param orgName
     * @return
     * @throws Exception
     */
    private static String checkAndCreateNewWorksheet(String userName, String problemId, String orgId, String orgName) throws Exception
    {
        if(Log.log.isDebugEnabled()) {
            Log.log.debug("In checkAndCreateNewWorksheet(" + userName + ", " + (StringUtils.isNotBlank(problemId) ? problemId : "null") + 
                            ", " + (StringUtils.isNotBlank(orgId) ? orgId : "null") + ", " + (StringUtils.isNotBlank(orgName)? orgName : "null"));
        }
        if (StringUtils.isBlank(problemId))
        {
            problemId = SearchUtils.getNewRowKey();
        }

        String orgIdInt = orgId;
        
        if (StringUtils.isBlank(orgIdInt) && StringUtils.isNotBlank(orgName))
        {
            // Identify org id based on org name
            
            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
            
            if (orgsVO != null)
            {
                orgIdInt = orgsVO.getSys_id();
            }
        }
        
        if (!UserUtils.isAdminUser(userName))
        {
            if (StringUtils.isNotBlank(orgIdInt))
            {
                if (!UserUtils.isOrgAccessible(orgIdInt, userName, false, false))
                {
                    throw new Exception("User does not have permission to create new worksheet for specified Org."); 
                }
            }
            else
            {
                if (!UserUtils.isNoOrgAccessible(userName))
                {
                    throw new Exception("User does not have permission to create new worksheet for specified No Org ('None').");
                }
            }
        }
        
        Worksheet worksheetModel = new Worksheet(problemId, null, userName);
        worksheetModel.setAssignedTo(userName);
        worksheetModel.setNumber(SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PRB));

        if (StringUtils.isNotBlank(orgIdInt))
        {
            worksheetModel.setSysOrg(orgIdInt); 
        }
        
        String id = null;
        Worksheet worksheet = saveWorksheet(worksheetModel, userName);
        if(worksheet != null)
        {
            id = worksheet.getSysId();
        }
        setActiveWorksheet(userName, id, orgId, orgName);

        if(Log.log.isDebugEnabled()) {
            Log.log.debug("Out checkAndCreateNewWorksheet(" + userName + ", " + (StringUtils.isNotBlank(problemId) ? problemId : "null") + 
                            ", " + (StringUtils.isNotBlank(orgId) ? orgId : "null") + ", " + (StringUtils.isNotBlank(orgName) ? orgName : "null") +
                            ", " + (worksheet != null ? worksheet.getSysId() : "null") + ", " + (worksheet != null ? worksheet.getNumber() : "null"));
        }
        
        return problemId;
    }

    @Deprecated
    public static String setActiveWorksheet(String userid, String problemid) throws Exception
    {
        return setActiveWorksheet(userid, problemid, null, null)/*result*/;
    } // setActiveWorksheet

    public static String setActiveWorksheet(String userid, String problemid, String orgId, String orgName) throws Exception
    {
        String result = null;

        if (StringUtils.isNotBlank(problemid))
        {
            ResolveSession query = new ResolveSession();
            query.setUActiveType(userid);
            
            String orgIdInt = orgId;
            
            if (StringUtils.isNotBlank(orgId) || StringUtils.isNotBlank(orgName))
            {
                if (StringUtils.isBlank(orgId))
                {
                    // Identify org id based on org name
                    
                    OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                    
                    if (orgsVO != null)
                    {
                        orgIdInt = orgsVO.getSys_id();
                    }
                }
                
                if (StringUtils.isNotBlank(orgIdInt))
                {
                    query.setSysOrg(orgIdInt);
                }
            }
            
            // get handle of worksheet and archive worksheet
            Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(problemid, userid).getData();
            
            if (worksheet == null) {
            	String errMsg = String.format("Unable to find Worksheet with problem id %s", problemid);
            	Log.log.error(errMsg);
            	throw new Exception(errMsg);
            }
            
            try
            {
                

            	final String oId = orgIdInt;
				HibernateProxy.execute(() -> {
					// Check worksheet access for Non admin role user
	                
	                if (!UserUtils.isAdminUser(userid))
	                {
	                    // Case # 1 : Specified Org but Worksheet Org is blank
	                    
	                    if (StringUtils.isNotBlank(query.getSysOrg()) &&
	                        StringUtils.isBlank(worksheet.getSysOrg()))
	                    {
	                        if (UserUtils.isOrgAccessible(query.getSysOrg(), userid, false, false))
	                        {
	                            // Next check if specified Org or its childerens have access to None Org
	                            
	                        	
	                            if (!UserUtils.isNoOrgAccessibleByOrg(oId))
	                            {
	                                Log.log.debug("Worksheet Org is None and Org " + query.getSysOrg() + 
	                                              " specified for setting worksheet active does not have access to No/None Org.");
	                                throw new Exception("Specified Org for settign worksheet active does not have acess to No/None Org.");
	                            }
	                        }
	                        else
	                        {
	                            Log.log.debug("Worksheet Org is None and user " + userid + " does not have access to Org " + query.getSysOrg() + 
	                                          " specified for setting worksheet active.");
	                            throw new Exception("User does not have access to Org specified for settign worksheet active.");
	                        }
	                    }
	                    
	                    // Case # 2 Org not specified and Worksheet has Org
	                    
	                    if (StringUtils.isBlank(query.getSysOrg()) &&
	                        StringUtils.isNotBlank(worksheet.getSysOrg()))
	                    {
	                        // Check if user has access to Worksheet Org
	                        
	                        if (UserUtils.isOrgAccessible(worksheet.getSysOrg(), userid, false, false))
	                        {
	                            query.setSysOrg(worksheet.getSysOrg());
	                        }
	                        else
	                        {
	                            Log.log.debug("User " + userid + " did not specify any Org to set workshet active. User " + userid + 
	                                          " does not have access to worksheet's Org " + worksheet.getSysOrg() + ".");
	                            throw new Exception("User did not specify any Org to set workshet active. User does not have access to worksheet's Org.");
	                        }
	                    }
	                    
	                    // Case # 3 Org specified and Worksheet has Org
	                    
	                    if (StringUtils.isNotBlank(query.getSysOrg()) &&
	                        StringUtils.isNotBlank(worksheet.getSysOrg()))
	                    {
	                        if (!query.getSysOrg().equalsIgnoreCase(worksheet.getSysOrg()))
	                        {
	                            // If specified Org and worksheet Org do not match
	                            
	                            boolean noMatchFound = true;
	                            
	                            List<OrgsVO> wsParentOrgVOsInHierarchy = UserUtils.getParentOrgsInHierarchy(worksheet.getSysOrg(), null);
	                            
	                            if (wsParentOrgVOsInHierarchy != null && !wsParentOrgVOsInHierarchy.isEmpty())
	                            {
	                                for (OrgsVO wsParentOrgVOInHierarchy : wsParentOrgVOsInHierarchy)
	                                {
	                                    if (wsParentOrgVOInHierarchy.getSys_id().equalsIgnoreCase(query.getSysOrg()))
	                                    {
	                                        noMatchFound = false;
	                                        break;
	                                    }
	                                }
	                            }
	                            
	                            if (noMatchFound)
	                            {
	                                Log.log.debug("Specified Org " + query.getSysOrg() +
	                                              " does not match worksheet's Org " + worksheet.getSysOrg() + 
	                                              " or any parents/grand parents of worksheet's Org in Org hierarchy.");
	                                throw new Exception("Specified Org does not match worksheet's Org or any parents/grand parents of worksheet's Org in Org hierarchy.");
	                            }
	                        }
	                    }
	                }
	                
	                // prepare the session object
	                
	                List<String> nullProps = new ArrayList<String>();
	                
	                if (StringUtils.isBlank(query.getSysOrg())) {
	                	nullProps.add(BASEMODEL_SYS_ORG);
	                }
	                
	                List<OrderbyProperty> orderByList = new ArrayList<OrderbyProperty>();
	                orderByList.add(new OrderbyProperty(BASEMODEL_SYS_UPDATED_ON, false));
	                
	                List<ResolveSession> sessions = HibernateUtil.getDAOFactory().getResolveSessionDAO()
	               		 		   				    .findNullProps(query, 1, 0, orderByList, nullProps);
	                
	                ResolveSession session = CollectionUtils.isNotEmpty(sessions) ? sessions.get(0) : null;
	                
	                if (session == null)
	                {
	                    session = new ResolveSession();
	                    session.setUActiveType(userid);
	                    
	                    if (StringUtils.isNotBlank(query.getSysOrg()))
	                    {
	                        session.setSysOrg(oId);
	                    }
	                }
	                else
	                {
	                    session.setSysModCount(Integer.valueOf(session.getSysModCount().intValue() + 1));
	                }

	                String pId = problemid;
					// this cassandra id
	                if (problemid.length() > 40)
	                {
	                    Log.log.warn("PROBLEMID Value too large, Max 40 Characters: " + problemid);
	                    pId  = problemid.substring(0, 40);
	                }
	                
	                if (StringUtils.isBlank(session.getUWorksheetId()) ||
	                    (StringUtils.isNotBlank(session.getUWorksheetId()) && !pId.equals(session.getUWorksheetId())))
	                {
	                    session.setUWorksheetId(pId);
	                }
	                
	                if (StringUtils.isBlank(session.getWorksheetNum()) ||
	                    (StringUtils.isNotBlank(session.getWorksheetNum()) && 
	                     !worksheet.getNumber().equals(session.getWorksheetNum()))) {
	                	session.setWorksheetNum(worksheet.getNumber());
	                }
	                
	                // save it
	                SaveUtil.saveResolveSession(session, userid);
				});
                
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                throw new Exception(e.getMessage(), e);
            }
        }

        return result;
    } // setActiveWorksheet
    
    @Deprecated
    public static boolean isActiveArchiveWorksheet(String userid)
    {
        boolean isArchive = false;

        try
        {
           isArchive = (boolean) HibernateProxy.execute( () -> {
        	   ResolveSession query = new ResolveSession();
               query.setUActiveType(userid);
               ResolveSession session = HibernateUtil.getDAOFactory().getResolveSessionDAO().findFirst(query);
               if (session != null)
               {
                   return session.getArchiveWorksheet() != null;                   
               } else  {
            	   return false;
               }
           });
           

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage());
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return isArchive;
    }
    
    public static boolean isActiveArchiveWorksheet(String userid, String orgId, String orgName)
    {
        boolean isArchive = false;

        try
        {
        	isArchive = (boolean) HibernateProxy.execute(() -> {
            	ResolveSession query = new ResolveSession();
                query.setUActiveType(userid);
                
                String orgIdInt = orgId;
                
                if (StringUtils.isNotBlank(orgId) || StringUtils.isNotBlank(orgName))
                {
                    if (StringUtils.isBlank(orgId))
                    {
                        // Identify org id based on org name
                        
                        OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                        
                        if (orgsVO != null)
                        {
                            orgIdInt = orgsVO.getSys_id();
                        }
                    }
                    
                    if (StringUtils.isNotBlank(orgIdInt))
                    {
                        query.setSysOrg(orgIdInt);
                    }
                }
                
                //ResolveSession session = HibernateUtil.getDAOFactory().getResolveSessionDAO().findFirst(query);
                
                ResolveSession session = null;
                
                if (StringUtils.isNotBlank(orgIdInt))
                {
                    session = HibernateUtil.getDAOFactory().getResolveSessionDAO().findFirst(query);
                }
                else
                {
                    List<ResolveSession> resolveSessions = HibernateUtil.getDAOFactory().getResolveSessionDAO().find(query);
                    
                    if (resolveSessions != null && !resolveSessions.isEmpty())
                    {
                        for (ResolveSession rs : resolveSessions)
                        {
                            if (StringUtils.isBlank(rs.getSysOrg()))
                            {
                                session = rs;
                                break;
                            }
                        }
                    }
                }
                
                if (session != null)
                {
                    if (session.getArchiveWorksheet() != null)
                    {
                        return true;
                    }
                }
                
                return false;
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage());
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return isArchive;
    }
    
    public static boolean isThisWorksheetInArchive(String problemId)
    {
        boolean isArchived = false;

        if (StringUtils.isNotBlank(problemId))
        {
            try
            {
            	isArchived = (boolean) HibernateProxy.execute( () -> {
                	 ArchiveWorksheet archiveWorksheet = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO().findById(problemId);
                     return archiveWorksheet != null;
                     
                });
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return isArchived;
    }

    public static List<String> getActionTaskids(String[] actionNamespaceArr, String[] actionNameArr, String problemId, boolean isArchive)
    {
        List<String> actionTaskids = new ArrayList<String>();

        if(actionNameArr != null && actionNameArr.length > 0)
        {
            for(int count=0; count<actionNameArr.length; count++)
            {
                String actionTaskNamespace = actionNamespaceArr[count];
                String actionTaskName = actionNameArr[count];
                if(StringUtils.isNotEmpty(actionTaskName))
                {
                    //create the example
                    ResolveActionTask task = new ResolveActionTask();
                    if (actionTaskNamespace != null && actionTaskNamespace.trim().length() > 0)
                    {
                        task.setUNamespace(actionTaskNamespace.trim());
                    }
                    if (actionTaskName != null && actionTaskName.trim().length() > 0)
                    {
                        task.setUName(actionTaskName.trim());
                    }

                    try
                    {                        
                        ResolveActionTask t = task;
                        task = (ResolveActionTask) HibernateProxy.execute(() -> {
                        	 ResolveActionTaskDAO ratDAO = HibernateUtil.getDAOFactory().getResolveActionTaskDAO();
                             return ratDAO.findFirst(t);
                        });

                        // init task
                       
                        if (task != null)
                        {
                            actionTaskids.add(task.getSys_id());
                        }
                    }
                    catch (Throwable e)
                    {
                        Log.log.warn(e.getMessage());
                        Log.log.trace(e.getMessage(), e);
                                              HibernateUtil.rethrowNestedTransaction(e);
                    }
                }//end of if

            }//end of for loop
        }
        else
        {
            if (isArchive)
            {
                actionTaskids.addAll(ServiceArchive.getAllArchiveTaskSysIdsForProblemId(problemId));
            }
            else
            {
                actionTaskids.addAll(getAllActionTaskSysIdsForProblemId(problemId));
            }
        }

        return actionTaskids;
    }//getActionTaskids

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static String getActionResultDetailFromArchive(String problem_id, String task_id, String node_id, String separator, Integer max, Integer offset)
    {
        String value = "";

        if(StringUtils.isNotBlank(problem_id))
        {            
            try
            {
                HibernateProxy.execute( () -> {
                	String sql = " ";
                	ArchiveWorksheet archiveWorksheet = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO().findById(problem_id);
                    if (archiveWorksheet != null)
                    {
                        String tablename = "ArchiveActionResult";
                        List<String> lRAR = null;
                        if (StringUtils.isEmpty(node_id))
                        {
                            sql = "SELECT r.actionResultLob.UDetail FROM "+tablename+" r WHERE r.problem = :problem_id AND r.task = :task_id order by r.UTimestamp desc";

                            Query q = HibernateUtil.createQuery(sql);
                            q.setParameter("problem_id", problem_id, StringType.INSTANCE);
                            q.setParameter("task_id", task_id, StringType.INSTANCE);
                            if (max != null && max > 0)
                            {
                                q.setMaxResults(max);
                            }
                            if (offset != null && offset >=0)
                            {
                                q.setFirstResult(offset);
                            }
                            lRAR = q.list();
                        }
                        else
                        {
                            sql = "SELECT r.actionResultLob.UDetail FROM "+tablename+" r WHERE r.problem = :problem_id AND r.task = :task_id AND r.executeRequest.UNodeId = :node_id order by r.UTimestamp desc";

                            Query q = HibernateUtil.createQuery(sql);
                            q.setParameter("problem_id", problem_id, StringType.INSTANCE);
                            q.setParameter("task_id", task_id, StringType.INSTANCE);
                            q.setParameter("node_id", node_id, StringType.INSTANCE);
                            if (max != null && max > 0)
                            {
                                q.setMaxResults(max);
                            }
                            if (offset != null && offset >=0)
                            {
                                q.setFirstResult(offset);
                            }
                            lRAR = q.list();
                        }
 
                        if (lRAR != null)
                        {
                            if (lRAR.size() == 1)
                            {
                                return lRAR.get(0);
                            }
                            else if (lRAR.size() > 1)
                            {
                            	String result = value;
                                for (int i=0; i < lRAR.size(); i++)
                                {
                                	result += lRAR.get(i);
                                    if (i+1 < lRAR.size())
                                    {
                                    	result += separator;
                                    }
                                }
                                
                                return result;
                            }
                        } else {
                        	return "";
                        } 
                    }
					return "";
                });
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        return value;
    }

    @SuppressWarnings({ "unchecked" })
    public static boolean isProcessCompletedForTask(String problemId, String[] actionNamespaceArr, 
    		String[] actionNameArr, ConfigSQL configSQL) {
        boolean result = true;

        if(StringUtils.isNotBlank(problemId))
        {
            //get the list of actiontask ids - problemid and isArchive are not required 
            List<String> actionTaskids = ServiceHibernate.getActionTaskids(actionNamespaceArr, actionNameArr, null, false);
            String dbType = configSQL.getDbtype().toUpperCase();
            String actiontaskids = SQLUtils.prepareQueryString(actionTaskids, dbType);
            String sql = "select UStatus FROM ArchiveExecuteRequest WHERE problem = '" + problemId + "' and task IN (" + actiontaskids + ")";

            List<String> list = null;

            try
            {
            	list = (List<String>) HibernateProxy.execute(() -> {
                	return   HibernateUtil.createQuery(sql).list();
                });
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }

            if(list != null && list.size() > 0)
            {
                for(String status : list)
                {
                    if (!status.equalsIgnoreCase("COMPLETED") && !status.equalsIgnoreCase("ABORTED"))
                    {
                        result = false;
                        break;
                    }
                }
            }
        }//end of if
        return result;
    }

    public static String getArchiveContentForTaskDetail(String executeid, String taskresultid)
    {
        String content = "";

        if (!StringUtils.isEmpty(executeid) || !StringUtils.isEmpty(taskresultid))
        {
            try
            {

            	content = (String) HibernateProxy.execute(() -> {
                	
                	if(StringUtils.isEmpty(taskresultid))
                    {
                        ArchiveExecuteRequest executeRequest = HibernateUtil.getDAOFactory().getArchiveExecuteRequestDAO().findById(executeid);
                        if (executeRequest != null)
                        {
                            ArchiveActionResult actionResult = executeRequest.ugetArchiveActionResult();
                            if (actionResult != null)
                            {
                                return  (actionResult.getActionResultLob() == null ? "" : actionResult.getActionResultLob().getUDetail());
                            }
                        }
                    }
                    else
                    {
                        ArchiveActionResult actionResult = HibernateUtil.getDAOFactory().getArchiveActionResultDAO().findById(taskresultid);
                        if (actionResult != null)
                        {
                            return  (actionResult.getActionResultLob() == null ? "" : actionResult.getActionResultLob().getUDetail());
                        }
                    }
					return "";
                });
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage());
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return content;
    }

    public static String getRawArchiveContentForTaskDetail(String executeid, String executeresultid)
    {
        String content = "";

        if (!StringUtils.isEmpty(executeid))
        {
            try
            {
            	content = (String) HibernateProxy.execute( () -> {
            		
            		String result = "";
                    if(executeresultid == null || executeresultid.equals(""))
                    {
                        ArchiveExecuteRequest executeRequest = HibernateUtil.getDAOFactory().getArchiveExecuteRequestDAO().findById(executeid);
                        if (executeRequest != null)
                        {
                            ArchiveExecuteResult executeResult = executeRequest.ugetArchiveExecuteResult();
                            if (executeResult != null)
                            {
                            	result = executeResult.getURaw();
                            }
                        }
                    }
                    else
                    {
                        ArchiveExecuteResult executeResult = HibernateUtil.getDAOFactory().getArchiveExecuteResultDAO().findById(executeresultid);
                        if (executeResult != null)
                        {
                        	result = executeResult.getURaw();
                        }
                    }
                    return result;
                });
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage());
                content = "Error while getting raw archive content of a task result.";
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return content;
    }

    public static Map<String, Object> getWorksheetWSDATA(String problemid, List<String> keyList, String username)
    {
        Map<String, Object> result = new HashMap<String, Object>();

        try
        {
            result = WorksheetSearchAPI.getWorksheetData(problemid, Collections.<String> emptyList(), username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    public static WSDataMap getWorksheetWSDATA(String problemId)
    {
        WSDataMap result = null;

        try
        {
            Map<String, Object> wsData = WorksheetSearchAPI.getWorksheetData(problemId, Collections.<String> emptyList(), "system");
            result = new WSDataMapImpl(problemId, wsData);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    public static Object getWorksheetData(final String sysId, final String propertyName, final String username,
    									  boolean ignoreESinMemCache) {
        Object result = null;
        try {
            result = WorksheetSearchAPI.getWorksheetData(sysId, propertyName, username, ignoreESinMemCache);
        } catch (SearchException e) {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }
    
    public static Object getWorksheetData(final String sysId, final String propertyName, final String username)
    {
    	return getWorksheetData(sysId, propertyName, username, false);
    }

    public static void setWorksheetWSDATAProperty(String problemid, String propertyName, Object propertyValue, String username)
    {
        try
        {
            WorksheetIndexAPI.indexWorksheetData(problemid, propertyName, propertyValue, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void deleteWorksheetWSDATA(String problemId, String[] propertyNames, String username)
    {
        try
        {
            WorksheetIndexAPI.deleteWorksheetData(problemId, Arrays.asList(propertyNames), username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void saveWorksheetWSDATA(String problemid, WSDataMap wsData, String username)
    {
        Worksheet worksheet = findWorksheetById(problemid, username);
        if(worksheet == null)
        {
            Log.log.info("saveWorksheetWSDATA: Worksheet not found, id: " + problemid);
        }
        else
        {
            if(wsData != null)
            {
                Map<String, Object> data = ((WSDataMapImpl)wsData).getDataMap();
                WorksheetIndexAPI.indexWorksheetData(problemid, data, username);
            }
        }
    }

    public static void updateWorksheetWSDATA(String problemId, Map<String, Object> map, String username)
    {
        if (StringUtils.isNotBlank(problemId))
        {
            WSDataMap wsDataMap = getWorksheetWSDATA(problemId);
            if (wsDataMap==null)
            {
                wsDataMap=new WSDataMapImpl(problemId, new HashMap<String,Object>());
            }
            WorksheetIndexAPI.indexWorksheetData(problemId, map, username);
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean isActiveWorksheet(String wsId) {
    	boolean wsIsActive = false;
    	
    	try {
            
    		HibernateProxy.setCurrentUser(SYSTEM_USER);
			List<Object> wsCounts = (List<Object>) HibernateProxy.executeHibernateInitLocked(() -> {
				Query sqlQuery = HibernateUtil.createSQLQuery("select count(sys_id) " + 
															  "from resolve_session " + 
															  "where u_problem_sys_id = :problemId " + 
															  "or u_archive_problem = :archivedProblemId");

				sqlQuery.setParameter("problemId", wsId, StringType.INSTANCE);
				sqlQuery.setParameter("archivedProblemId", wsId, StringType.INSTANCE);

				return sqlQuery.list();
			});
            
            int count = 0;
            
            if (CollectionUtils.isNotEmpty(wsCounts)) {
            	if (wsCounts.get(0) instanceof BigInteger) {
            		count = ((BigInteger)wsCounts.get(0)).intValue();
            	} else if (wsCounts.get(0) instanceof BigDecimal) {
            		count = ((BigDecimal)wsCounts.get(0)).intValue();
            	}
            }
            
            wsIsActive = count > 0;
        } catch(Throwable t) {
            Log.log.error(String.format("Error %sin identifying if worksheet with id %s is active(ES/Archived)", 
            							(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""), wsId), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new RuntimeException(t);
        }
    	
    	return wsIsActive;
    }
    
    public static Collection<String> deleteWorksheetRows(List<String> worksheetIds, boolean isSync, String username)
    {
        Collection<String> deletedWorksheetIds = Collections.emptySet();
        
        if (CollectionUtils.isEmpty(worksheetIds)) {
        	return deletedWorksheetIds;
        }
        
        try
        {
        	// Filter out active worksheets as they cannot be deleted/removed
        	
        	long startTime = System.currentTimeMillis();
        	List<String> nonActiveWorksheetIds = worksheetIds.parallelStream()
        										 .filter(wsId -> !isActiveWorksheet(wsId))
        										 .collect(Collectors.toList());
        	
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug(String.format("Time taken to filter out %d active worksheet ids from given %d worksheet " +
        									"ids: %d msec", 
        									(worksheetIds.size() - nonActiveWorksheetIds.size()),
        									worksheetIds.size(), (System.currentTimeMillis() - startTime)));
        	}
        	
        	if (CollectionUtils.isNotEmpty(nonActiveWorksheetIds)) {
	            deletedWorksheetIds = WorksheetIndexAPI.deleteWorksheets(nonActiveWorksheetIds, isSync, username, true);
	            
	            //delete the graph nodes for deleted worksheets
	            if (isSync && CollectionUtils.isNotEmpty(deletedWorksheetIds))
	            {
	            	deletedWorksheetIds.parallelStream().forEach(deletedWorksheetId -> {
	            		try {
							ServiceGraph.deleteNode(null, deletedWorksheetId, null, NodeType.WORKSHEET, username);
						} catch (Exception e) {
							Log.log.error(String.format("Error %sin deleting social worksheet node with sysId %s",
														(StringUtils.isNotEmpty(e.getMessage()) ? 
														 "[" + e.getMessage() + "] " : ""), 
														deletedWorksheetId), e);
						}
	            	});
	            }
        	}
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return deletedWorksheetIds;
    }

    public static void deleteProcessRequestRows(List<String> processRequestIds, boolean isSync, String username)
    {
        try
        {
            WorksheetIndexAPI.deleteProcessRequests(processRequestIds, isSync, username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void deleteAllProcessRequestRows(String status, String username)
    {
    	QueryDTO queryDTO = new QueryDTO();
    	if (StringUtils.isNotBlank(status) && !status.equals("ALL"))
    	{
    		queryDTO.addFilterItem(new QueryFilter("status", QueryFilter.EQUALS, status.toLowerCase()));
    	}
    	QueryBuilder queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
        try
        {
        	WorksheetIndexAPI.deleteAllProcessRequestRows(queryBuilder, status, username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void deleteTaskResultRows(List<String> taskResultIds, boolean isSync, String username)
    {
        try
        {
            WorksheetIndexAPI.deleteTaskResults(taskResultIds, isSync, username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void deleteExecuteStateRows(List<String> executeStateIds)
    {
        try
        {
            WorksheetIndexAPI.deleteExecuteStates(executeStateIds, "system");
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void deleteExecuteStateRows(List<String> executeStateIds, String username)
    {
        try
        {
            WorksheetIndexAPI.deleteExecuteStates(executeStateIds, username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static String initExecuteProcess(Map<String, Object> params, String problemid, String processid, String userid, String reference, String alertid, String queryString) throws Exception
    {

        try
        {
          HibernateProxy.setCurrentUser(userid);
        	return (String) HibernateProxy.execute(() -> {
        		
        		String queryStringFinal = queryString;
        		String problemIdFinal = problemid;
        		String processidFinal = processid;

                String userQueryString = (String) params.get(Constants.HTTP_REQUEST_QUERYSTRING);
                String procNum = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PROC);

                String wiki = (String)params.get(Constants.HTTP_REQUEST_WIKI);
                String problemNumber = "";
        		
                // create new worksheet and set active
                if (StringUtils.isBlank(problemIdFinal)
                                || problemIdFinal.equalsIgnoreCase(Constants.PROBLEMID_NEW)
                                || problemIdFinal.equalsIgnoreCase(Constants.PROBLEMID_ACTIVE)
                                || problemIdFinal.equalsIgnoreCase(Constants.PROBLEMID_LOOKUP))
                {
                    String event_eventid = (String) params.get(Constants.EVENT_EVENTID);
                    String event_reference = (String) params.get(Constants.EVENT_REFERENCE);
                    String processNotEvent = (String) params.get(Constants.EXECUTE_PROCESS_NOTEVENT);

                    // init isEvent
                    boolean isEvent = false;
                    boolean notEvent = StringUtils.isNotEmpty(processNotEvent) && processNotEvent.equalsIgnoreCase("true");
                    if (notEvent == false && StringUtils.isNotEmpty(event_eventid) && (StringUtils.isNotEmpty(event_reference) || StringUtils.isNotEmpty(processidFinal)))
                    {
                        isEvent = true;
                    }

                    // create worksheet
                    
                     Worksheet worksheet = WorksheetUtil.initWorksheet(problemIdFinal, reference, alertid, null, userid, processidFinal, isEvent, params);
                    
                     problemIdFinal = worksheet.getSysId();
                    problemNumber = worksheet.getNumber();

                    // set active worksheet
                    WorksheetUtils.setActiveWorksheet(userid, problemIdFinal, 
                                                      (String)params.get(Constants.EXECUTE_ORG_ID), 
                                                      (String)params.get(Constants.EXECUTE_ORG_NAME));

                    // set params
                    params.put(Constants.EXECUTE_PROBLEMID, problemIdFinal);

                    // replace NEW in query string
                    if (!StringUtils.isEmpty(queryStringFinal))
                    {
                    	queryStringFinal = queryStringFinal.replaceAll("NEW", problemIdFinal);
                    }

                    // replace NEW in query string
                    if (!StringUtils.isEmpty(userQueryString))
                    {
                        userQueryString = userQueryString.replaceAll("NEW", problemIdFinal);
                    }
                }

                // create new processid if value is set to NEW
                if (processidFinal != null && (processidFinal.equalsIgnoreCase("CREATE") || processidFinal.equalsIgnoreCase("NEW")))
                {
                    String rowKey = SearchUtils.getNewRowKey(); // processid;
                    ProcessRequest processRequest = new ProcessRequest(rowKey, null, userid);
                    processRequest.setNumber(procNum);
                    processRequest.setProblem(problemIdFinal);
                    processRequest.setProblemNumber(problemNumber);
                    processRequest.setStatus("OPEN");
                    processRequest.setWiki(wiki);
                    //##################################
                    //TODO, need to find this out
                    //processRequest.setTimeout(UTimeout);
                    //processRequest.setDuration(UDuration);

                    WorksheetIndexAPI.indexProcessRequest(processRequest, userid);

                    processidFinal=rowKey;

                    // set params
                    params.put(Constants.EXECUTE_PROCESSID, processidFinal);

                    // replace CREATE in query string
                    if (!StringUtils.isEmpty(queryStringFinal))
                    {
                    	queryStringFinal = queryStringFinal.replaceAll("CREATE", processidFinal);
                    }

                    // replace NEW in query string
                    if (!StringUtils.isEmpty(userQueryString))
                    {
                        userQueryString = userQueryString.replaceAll("CREATE", problemIdFinal);
                    }
                }
                
                //update the value with the new one
                if (!StringUtils.isEmpty(queryStringFinal))
                {
                    params.put(Constants.HTTP_REQUEST_QUERY, queryStringFinal);
                }

                // update the new value
                if (!StringUtils.isEmpty(userQueryString))
                {
                    params.put(Constants.HTTP_REQUEST_QUERYSTRING, userQueryString);
                }
                
                return queryStringFinal;
                
        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
    } // initExecuteProcess

    public static String createNewActionResult(Map<String, Object> valueMap) {
    	return createNewActionResult(valueMap, true);
	}

    /**
     * Insert a new Action Result row. Not only action result column values are reuqired, also additional values for worksheet result list column family.
     *
     * @param valueMap a Java map for all column name/value pairs
     *
     * @return row key of new row
     */
    public static String createNewActionResult(Map<String, Object> valueMap, boolean updateSir)
    {
        String sysCreatedBy = (String)valueMap.get("sys_created_by");
        String sysUpdatedBy = (String)valueMap.get("sys_updated_by");
        if (sysCreatedBy==null)
        {
            sysCreatedBy = HibernateUtil.getCurrentUsername();
        }
        if (sysUpdatedBy==null)
        {
            sysUpdatedBy = sysCreatedBy;
        }

        TaskResult taskResult = new TaskResult();
        Long timestamp = (Long)valueMap.get("u_timestamp");
        
        taskResult.setSysId(Guid.getGuid());
        taskResult.setSysOrg(null);
        taskResult.setSysCreatedBy(sysCreatedBy);
        taskResult.setSysCreatedOn(timestamp);
        taskResult.setSysUpdatedBy(sysUpdatedBy);
        taskResult.setSysUpdatedOn(timestamp);

        String problemId = (String)valueMap.get("u_problem");
        taskResult.setProblemId(problemId == null ? "" : problemId);
        taskResult.setProblemNumber((String)valueMap.get("u_problem_num"));

        String processId = (String)valueMap.get("u_process");
        taskResult.setProcessId(processId == null ? "" : processId);
        taskResult.setProcessNumber((String)valueMap.get("u_process_number"));

        taskResult.setDetail((String)valueMap.get("u_detail"));
        taskResult.setTaskId((String)valueMap.get("u_actiontask"));
        taskResult.setExecuteRequestId((String)valueMap.get("u_execute_request_id"));
        taskResult.setExecuteRequestNumber((String)valueMap.get("u_execute_request_number"));

        taskResult.setExecuteResultId((String)valueMap.get("u_execute_result_id"));
        if ((String)valueMap.get("u_execute_result_command")!=null)
        {
            taskResult.setExecuteResultCommand((String)valueMap.get("u_execute_result_command"));
        }
        if ((String)valueMap.get("u_execute_result_returncode")!=null)
        {
            taskResult.setExecuteResultReturncode(Integer.valueOf((String)valueMap.get("u_execute_result_returncode")));
        }

        taskResult.setTaskId((String)valueMap.get("u_actiontask"));
        taskResult.setTaskName((String)valueMap.get("u_actiontask_name"));
        //taskResult.setTaskFullName((String)valueMap.get("u_actiontask_full_name"));
        if ((String)valueMap.get("u_task_namespace")!=null)
        {
            taskResult.setTaskNamespace((String)valueMap.get("u_task_namespace"));
        }
        if ((String)valueMap.get("u_task_summary")!=null)
        {
            taskResult.setTaskSummary((String)valueMap.get("u_task_summary"));
        }
        if (valueMap.get("u_task_hidden")!=null && valueMap.get("u_task_hidden") instanceof String)
        {
            taskResult.setHidden(Boolean.parseBoolean((String)valueMap.get("u_task_hidden")));
        }
        if ((String)valueMap.get("u_task_tags")!=null)
        {
            String tags = (String)valueMap.get("u_task_tags");
            taskResult.setTaskTags(SearchUtils.parseTags(tags));
        }
        if ((String)valueMap.get("u_task_roles")!=null)
        {
            String taskRoles = (String)valueMap.get("u_task_roles");
            taskResult.setTaskRoles(SearchUtils.parseRoles(taskRoles));
        }

        taskResult.setSummary((String)valueMap.get("u_summary"));
        taskResult.setDetail((String)valueMap.get("u_detail"));
        taskResult.setRaw((String)valueMap.get("u_raw"));

        taskResult.setAddress((String)valueMap.get("u_address"));
        taskResult.setEsbaddr((String)valueMap.get("u_esbaddr"));
        taskResult.setCompletion((String)valueMap.get("u_completion"));
        taskResult.setCondition((String)valueMap.get("u_condition"));
        taskResult.setSeverity((String)valueMap.get("u_severity"));
        int duration = (Integer)valueMap.get("u_duration");
        taskResult.setDuration(duration);
        taskResult.setTimestamp(timestamp);
        taskResult.setWiki((String)valueMap.get("u_wiki"));
        taskResult.setNodeId((String)valueMap.get("u_node_id"));

        String actionResultGUID = (String)valueMap.get("u_target_guid");
        if (actionResultGUID==null)
        {
            actionResultGUID="";
        }
        taskResult.setTargetGUID(actionResultGUID);
        taskResult.setActivityId((String)valueMap.get("activityId"));
        if (valueMap.get("u_task_fullname") != null)
            taskResult.setTaskFullName((String)valueMap.get("u_task_fullname"));
        else if (valueMap.get("u_actiontask_full_name") != null)
            taskResult.setTaskFullName((String)valueMap.get("u_actiontask_full_name"));
        WorksheetIndexAPI.indexTaskResult(taskResult, sysCreatedBy, updateSir);

        return taskResult.getSysId();
    }

    @Deprecated
    public static void updateWorksheet(String problemId, Map<String, Object> columns)
    {
        updateWorksheet(problemId, columns, "system");
    }
    
    public static void updateWorksheet(String problemId, Map<String, Object> columns, String username)
    {
        if (columns != null && columns.size() > 0)
        {
            Worksheet worksheet = findWorksheetById(problemId, username);
            if(worksheet != null)
            {
                for(String key : columns.keySet())
                {
                    String value = (String) columns.get(key);
                    if ("sys_created_on".equals(key))
                    {
                        throw new RuntimeException("Worksheet column 'sysCreatedOn' was not modifiable");
                    }
                    else if ("sys_created_by".equals(key)) worksheet.setSysCreatedBy(value);
                    else if ("sys_updated_by".equals(key)) worksheet.setSysUpdatedBy(value);
                    else if(key.equals("u_assigned_to")) worksheet.setAssignedTo((String) columns.get(key));
                    else if(key.equals("u_assigned_to_name")) worksheet.setAssignedToName((String) columns.get(key));
                    else if ("u_condition".equals(key) || "condition".equalsIgnoreCase(key))
                    {
                        worksheet.setCondition((String) columns.get(key));
                    }
                    else if ("u_correlation_id".equals(key) || "correlation_id".equalsIgnoreCase(key) || "correlationid".equalsIgnoreCase(key))
                    {
                        worksheet.setCorrelationId((String) columns.get(key));
                    }
                    else if(key.equals("u_debug"))
                    {
                        //this field is not updated through this because the debug fields
                        //stores actual debug information but this u_debug is a boolean value of "true" or "false"
                        //worksheet.setDebug((String) columns.get(key));
                    }
                    else if ("u_description".equals(key) || "description".equalsIgnoreCase(key))
                    {
                        worksheet.setDescription((String) columns.get(key));
                    }
                    else if(key.equals("u_number") || "number".equalsIgnoreCase(key))
                    {
                        throw new RuntimeException("Worksheet column 'u_number' was not modifiable");
                    }
                    else if ("u_alert_id".equals(key) || "alert_id".equalsIgnoreCase(key) || "alertid".equalsIgnoreCase(key))
                    {
                        worksheet.setAlertId((String) columns.get(key));
                    }
                    else if ("u_reference".equals(key) || "reference".equalsIgnoreCase(key))
                    {
                        worksheet.setReference((String) columns.get(key));
                    }
                    else if ("u_severity".equals(key) || "severity".equalsIgnoreCase(key))
                    {
                        worksheet.setSeverity((String) columns.get(key));
                    }
                    else if ("u_summary".equals(key) || "summary".equalsIgnoreCase(key))
                    {
                        worksheet.setSummary((String) columns.get(key));
                    }
                    else if(key.equals("u_work_notes"))
                    {
                        worksheet.setWorkNotes((String) columns.get(key));
                    }
                    else if(key.equals("u_worksheet"))
                    {
                        worksheet.setWorksheet((String) columns.get(key));
                    }
                    else if(key.equals("u_sir_id"))
                    {
                        worksheet.setSirId((String) columns.get(key));
                    }
                    else if(key.equals("sysOrg"))
                    {
                        worksheet.setSysOrg(columns.get(key) == null ? null : (String)columns.get(key));
                    }
                }
                worksheet.setSysUpdatedOn(DateUtils.GetUTCDateLong());
                worksheet.setModified(true);
                WorksheetIndexAPI.indexWorksheet(worksheet, username);
            }
        }
    }

    public static void updatProcessRequest(ProcessRequest processRequest, String username)
    {
        WorksheetIndexAPI.indexProcessRequest(processRequest, username);
    }

    public static List<Object> getNodeByProblemId(String problemId, String status)
    {
        List<Object> result = new ArrayList<Object>();

        try
        {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemId));
            ResponseDTO<TaskResult> actionResults = WorksheetSearchAPI.searchTaskResults(queryDTO, "system");

            //Now prepare the result
            if(actionResults != null && actionResults.getRecords() != null && actionResults.getRecords().size() > 0)
            {
                for(TaskResult actionResult : actionResults.getRecords())
                {
                    String execReqId = actionResult.getExecuteRequestId();
                    String[] resultItem = new String[3];

                    String nodeId = execReqId.substring(0, execReqId.indexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER));
                    String nodeLabel = execReqId.substring(execReqId.lastIndexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER) + Constants.EXECUTE_REQUEST_NODEID_DELIMITER.length(), execReqId.length());

                    //Node Id
                    resultItem[0]=nodeId;
                    //Node Label
                    resultItem[1]=nodeLabel;
                    if(status.equalsIgnoreCase(Constants.STATUS_SEVERITY))
                    {
                        //Severity
                        resultItem[2]=actionResult.getSeverity();
                    }
                    else
                    {
                        //Condition
                        resultItem[2]=actionResult.getCondition();
                    }
                    result.add(resultItem);
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public static ProcessRequest findProcessRequestById(String sysId)
    {
        ProcessRequest result = null;
        try
        {
            result = WorksheetSearchAPI.findProcessRequestsById(sysId, "system").getData();
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<ExecuteState> getEventRowsByProcessId(String processId)
    {
        List<ExecuteState> executeStates = null;

        if(StringUtils.isNotEmpty(processId))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processId));
                executeStates = WorksheetSearchAPI.searchExecuteStates(queryDTO, "system").getRecords();
            }
            catch (Exception e)
            {
                Log.log.error("Error getting rows for processId: " + processId, e);
            }
        }

        return executeStates;
    }

    public static List<ExecuteState> findExecuteStates(String sysId, String processId, String problemId, String targetAddress, String reference)
    {
        List<ExecuteState> result = new ArrayList<ExecuteState>();
        try
        {
            if(StringUtils.isNotBlank(sysId))
            {
                 result.add(WorksheetSearchAPI.findExecuteStateById(sysId, "system").getData());
            }
            else
            {
                QueryDTO queryDTO = new QueryDTO();
                if(StringUtils.isNotBlank(processId))
                {
                    queryDTO.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processId));
                }
                if(StringUtils.isNotBlank(problemId))
                {
                    queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemId));
                }
                if(StringUtils.isNotBlank(targetAddress))
                {
                    queryDTO.addFilterItem(new QueryFilter("targetAddress", QueryFilter.EQUALS, targetAddress));
                }
                if(StringUtils.isNotBlank(reference))
                {
                    queryDTO.addFilterItem(new QueryFilter("reference", QueryFilter.EQUALS, reference));
                }
                result = WorksheetSearchAPI.searchExecuteStates(queryDTO, "system").getRecords();
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    } 

    public static void saveExecuteState(ExecuteState executeState)
    {
        WorksheetIndexAPI.indexExecuteState(executeState, "system");
    }

    public static ExecuteState findExecuteStateById(String sysId)
    {
        ExecuteState result = null;
        try
        {
            result = WorksheetSearchAPI.findExecuteStateById(sysId, "system").getData();
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public static ExecuteState findExecuteStateByProcessId(String processId)
    {
        ExecuteState result = null;
        try
        {
            QueryDTO queryDTO = new QueryDTO(0, 1);
            queryDTO.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processId));
            List<ExecuteState> executeStates = WorksheetSearchAPI.searchExecuteStates(queryDTO, "system").getRecords();
            if(executeStates != null && executeStates.size() > 0)
            {
                result = executeStates.get(0);
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }


    public static void updateWorksheet(Worksheet worksheet)
    {
        WorksheetIndexAPI.indexWorksheet(worksheet, "system");
    }
    
    @SuppressWarnings("rawtypes")
    public static XDoc getWorksheetXML(String problem_id, Map params)
    {
        XDoc result = null;
        try
        {
            if (problem_id != null)
            {
                // init workXML
                Worksheet cl = ServiceWorksheet.findWorksheetById(problem_id, "system");
                
                if (cl != null )
                {
                    String workXML = null;
                    if (cl.getWorksheet() != null)
                    {
                        workXML = cl.getWorksheet();
                    }
    
                    if (!StringUtils.isEmpty(workXML))
                    {
                        result = new XDoc(workXML);
                    }
                    else
                    {
                        result = new XDoc();
                        result.addRoot("WORKSHEET");
                    }
                }
                else
                {
                    throw new Exception("Failed to find worksheet with sys_id: " + problem_id);
                }
            }
            else
            {
                throw new Exception("Unable to retrieve worksheet with sys_id: " + problem_id);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Unable to get worksheet. " + e.getMessage(), e);
        }
        return result;
    } // getWorkXMLCAS

    public static XDoc getWorksheetXMLObject(String problemid) throws Exception
    {
        XDoc result = null;

        Worksheet worksheet = WorksheetSearchAPI.findWorksheetById(problemid, "system").getData();

        if (worksheet != null)
        {
            XDoc workXML = null;

            String worksheetText = worksheet.getWorksheet();
            if (StringUtils.isNotBlank(worksheetText))
            {
                workXML = new XDoc(worksheetText.trim());
            }
            else
            {
                workXML = new XDoc();
                workXML.addRoot("WORKSHEET");
            }

            result = workXML;
        }
        else
        {
            Log.log.error("Invalid problemid: " + problemid);
        }

        return result;
    }

    public static List<Map<String, Object>> findTaskResultByWorksheet(String sysId, String number, String[] actionTasks, String username) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();

        try
        {
            QueryDTO queryDTO = new QueryDTO();
            if(StringUtils.isNotBlank(sysId))
            {
                queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, sysId));
            }
            else if(StringUtils.isNotBlank(number))
            {
                queryDTO.addFilterItem(new QueryFilter("problemNumber", QueryFilter.EQUALS, number));
            }
            else
            {
                throw new Exception("Either sysId or number must be provided");
            }

            if(actionTasks != null && actionTasks.length > 0)
            {
                String taskNames = StringUtils.arrayToString(actionTasks, ",");
                queryDTO.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, taskNames, "taskFullName", QueryFilter.EQUALS));
            }

            List<TaskResult> actionResults = WorksheetSearchAPI.searchTaskResults(queryDTO, username).getRecords();
            for(TaskResult taskResult : actionResults)
            {
                result.add(taskResult.asMap());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to get task result for worksheet. " + e.getMessage());
            throw e;
        }
        return result;
    }

    public static ArchiveWorksheet getArchiveWorksheet(String id, String username)
    {
        ArchiveWorksheet result = null;

        try
        {

            if (StringUtils.isNotBlank(id))
            {
               	result = (ArchiveWorksheet) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getArchiveWorksheetDAO().findById(id);
            	});
            }

        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }

    //UI sends the field in ES way, need
    private static Map<String, String> archiveWorksheetMapper = new HashMap<String, String>();
    static
    {
        archiveWorksheetMapper.put("alertId","UAlertId");
        archiveWorksheetMapper.put("assignedTo","UAssignedTo");
		archiveWorksheetMapper.put("assignedToName","UAssignedToName");
        archiveWorksheetMapper.put("condition","UCondition");
        archiveWorksheetMapper.put("correlationId","UCorrelationId");
        archiveWorksheetMapper.put("number","UNumber");
        archiveWorksheetMapper.put("reference","UReference");
        archiveWorksheetMapper.put("severity","USeverity");
        archiveWorksheetMapper.put("summary","USummary");
    }

    public static List<Worksheet> listArchiveWorksheets(QueryDTO query, String username)
    {
        List<Worksheet> result = new ArrayList<Worksheet>();

        //set it in the query object
        query.setModelName("ArchiveWorksheet");

        //map columns, this is due to using ES model and going after Hib model
        if(query.getFilterItems() != null && query.getFilterItems().size() > 0)
        {
            for(QueryFilter filter : query.getFilterItems())
            {
                if(archiveWorksheetMapper.containsKey(filter.getField()))
                {
                    filter.setField(archiveWorksheetMapper.get(filter.getField()));
                }
            }
        }

        if(query.getSortItems() != null && query.getSortItems().size() > 0)
        {
            for(QuerySort sorter : query.getSortItems())
            {
                if(archiveWorksheetMapper.containsKey(sorter.getProperty()))
                {
                    sorter.setProperty(archiveWorksheetMapper.get(sorter.getProperty()));
                }
            }
        }

        query.setHql(query.getSelectHQL());

        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, query.getStart(), query.getLimit(), true);
            if(data != null)
            {
                for (Object o : data)
                {
                    ArchiveWorksheet instance = (ArchiveWorksheet) o;
                    instance.setUDebug(""); //we do not want to send "debug" in this API
                    result.add(convert(instance));
                }
            }//end of if
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + query.getHql(), e);
        }

        return result;
    }

    public static Worksheet convert(ArchiveWorksheet instance)
    {
        Worksheet result = null;
        if(instance != null)
        {
            result = new Worksheet();
            result.setId(instance.getSys_id());
            result.setSysId(instance.getSys_id());
            result.setSysCreatedBy(instance.getSysCreatedBy());
            if(instance.getSysCreatedOn() != null)
            {
                result.setSysCreatedOn(instance.getSysCreatedOn().getTime());
            }
            result.setSysUpdatedBy(instance.getSysUpdatedBy());
            if(instance.getSysUpdatedOn() != null)
            {
                result.setSysUpdatedOn(instance.getSysUpdatedOn().getTime());
            }

            result.setAlertId(instance.getUAlertId());
            result.setAssignedTo(instance.getUAssignedTo());
            result.setAssignedToName((StringUtils.isBlank(instance.getUAssignedToName()) ? instance.getUAssignedTo() : instance.getUAssignedToName()));
            result.setCondition(instance.getUCondition());
            result.setCorrelationId(instance.getUCorrelationId());
            result.setDebug(instance.getUDebug());
            result.setDescription(instance.getUDescription());
            result.setNumber(instance.getUNumber());
            result.setReference(instance.getUReference());
            result.setSeverity(instance.getUSeverity());
            result.setSummary(instance.getUSummary());
            result.setWorkNotes(instance.getUWorkNotes());
            result.setWorksheet(instance.getUWorksheet());
            result.setSysOrg(instance.getSysOrg());
            result.setSirId(instance.getSirId());
            result.setWorknotesHtml("");
            result.setWorksheetXml("");
            result.setModified(true);
        }
        return result;
    }

    public static ArchiveActionResult getArchiveTaskResult(String id, String username)
    {
        ArchiveActionResult result = null;

        try
        {

            if (StringUtils.isNotBlank(id))
            {
            	result = (ArchiveActionResult) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getArchiveActionResultDAO().findById(id);
            	});
            }

        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;

    }

    //UI sends the field in ES way, need
    private static Map<String, String> archiveTaskResultMapper = new HashMap<String, String>();
    static
    {
        archiveTaskResultMapper.put("address","UAddress");
        archiveTaskResultMapper.put("completion","UCompletion");
        archiveTaskResultMapper.put("condition","UCondition");
        archiveTaskResultMapper.put("duration","UDuration");
        archiveTaskResultMapper.put("esbaddr","UEsbAddr");
        archiveTaskResultMapper.put("severity","USeverity");
        archiveTaskResultMapper.put("timestamp","UTimestamp");
        archiveTaskResultMapper.put("targetGUID","UTargetGUID");
        archiveTaskResultMapper.put("hidden","UHidden");
        archiveTaskResultMapper.put("taskName","task.UName");
        archiveTaskResultMapper.put("problemId","problem.sys_id");
        archiveTaskResultMapper.put("processNumber","process.UNumber");
        archiveTaskResultMapper.put("summary","actionResultLob.USummary");
        archiveTaskResultMapper.put("detail","actionResultLob.UDetail");
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<TaskResult> listArchiveWorksheetResultsByWorksheetId(String worksheetId, QueryDTO queryDTO, Boolean hidden, List<String> taskFullNames, List<String> taskNames, String username)
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<TaskResult>();
        List<TaskResult> records = new ArrayList<TaskResult>();

        //set it in the query object
        queryDTO.setModelName("ArchiveActionResult");
        //map columns, this is due to using ES model and going after Hib model
        //List<QueryFilter> filters = new ArrayList<QueryFilter>();
        //StringBuilder whereClause = new StringBuilder(" tableData.problem.sys_id='" + worksheetId + "' ");
        queryDTO.addFilterItem(new QueryFilter("auto", worksheetId, "problemId", QueryFilter.EQUALS));
      //in archived worksheet we don't need the Advanced View feature..so we don't need to care about the UHidden field
//        if(hidden)
//        {
//            queryDTO.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, "false", "hidden", QueryFilter.EQUALS));
//        }

        if(queryDTO.getFilterItems() != null && queryDTO.getFilterItems().size() > 0)
        {
            for(QueryFilter filter : queryDTO.getFilterItems())
            {
                if(archiveTaskResultMapper.containsKey(filter.getField()))
                {
                    filter.setField(archiveTaskResultMapper.get(filter.getField()));
                }
            }
        }

        if(queryDTO.getSortItems() != null && queryDTO.getSortItems().size() > 0)
        {
            for(QuerySort sort : queryDTO.getSortItems())
            {
                if(archiveTaskResultMapper.containsKey(sort.getProperty()))
                {
                    sort.setProperty(archiveTaskResultMapper.get(sort.getProperty()));
                }
            }
        }

        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	            //execute the qry
	            Query query = HibernateUtil.createQuery(queryDTO); // GeneralHibernateUtil.executeHQLSelectModel(query, query.getStart(), query.getLimit(), true);
	            Query countQuery = HibernateUtil.createCountQuery(queryDTO);
	            if(taskFullNames != null && taskFullNames.size() > 0)
	            {
	                query.setParameterList("taskFullNames", taskFullNames);
	                countQuery.setParameterList("taskFullNames", taskFullNames);
	            }
	            if(taskNames != null && taskNames.size() > 0)
	            {
	                query.setParameterList("taskNames", taskNames);
	                countQuery.setParameterList("taskNames", taskNames);
	            }
	
	            List<? extends Object> data = query.list();
	
	            if(data != null)
	            {
	                for (Object o : data)
	                {
	                    ArchiveActionResult instance = (ArchiveActionResult) o;
	                    records.add(convert(instance));
	                }
	            }//end of if
	            result.setRecords(records);
	
	            int total = ServiceHibernate.getTotalHqlCount(countQuery);
	            result.setTotal(total).setSuccess(true);
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + queryDTO.getHql(), e);

            result.setSuccess(false);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }

    public static TaskResult convert(ArchiveActionResult vo)
    {
        TaskResult result = null;

        if(vo != null)
        {
            result = new TaskResult();

            ArchiveActionResultVO instance = vo.doGetVO();

            result.setId(instance.getSys_id());
            result.setSysId(instance.getSys_id());
            result.setSysCreatedBy(instance.getSysCreatedBy());
            if(instance.getSysCreatedOn() != null)
            {
                result.setSysCreatedOn(instance.getSysCreatedOn().getTime());
            }
            result.setSysUpdatedBy(instance.getSysUpdatedBy());
            if(instance.getSysUpdatedOn() != null)
            {
                result.setSysUpdatedOn(instance.getSysUpdatedOn().getTime());
            }

            result.setAddress(instance.getUAddress());
            result.setCompletion(instance.getUCompletion());
            result.setCondition(instance.getUCondition());
            result.setDuration(instance.getUDuration());
            result.setEsbaddr(instance.getUEsbAddr());
            result.setSeverity(instance.getUSeverity());
            result.setTimestamp(instance.getUTimestamp());
            if(instance.getExecuteRequest() != null)
            {
                result.setExecuteRequestId(instance.getExecuteRequest().getSys_id());
                result.setExecuteRequestNumber(instance.getExecuteRequest().getUNumber());
            }
            if(instance.getExecuteResult() != null)
            {
                result.setExecuteResultId(instance.getExecuteResult().getSys_id());
                result.setExecuteResultCommand(instance.getExecuteResult().getUCommand());
                result.setExecuteResultReturncode(instance.getExecuteResult().getUReturncode());
            }
            if(instance.getProblem() != null)
            {
                result.setProblemId(instance.getProblem().getSys_id());
                result.setProblemNumber(instance.getProblem().getUNumber());
            }
            result.setProcessId(instance.getUProcessId());
            result.setProcessNumber(instance.getUProcessNumber());
            if(instance.getProcess() != null)
            {
                //borrowing wiki from process
                result.setWiki(instance.getProcess().getUWiki());
            }
            result.setTargetGUID(instance.getUTargetGUID());
            if(instance.getTask() != null)
            {
                result.setTaskId(instance.getTask().getSys_id());
                result.setTaskName(instance.getTask().getUName());
                result.setTaskNamespace(instance.getTask().getUNamespace());
                result.setTaskFullName(instance.getTask().getUFullName());
                result.setTaskSummary(instance.getTask().getUSummary());
                if(instance.getTask().getTags() != null)
                {
                    result.setTaskTags(new TreeSet<String>(instance.getTask().getTags()));
                }
            }

            if(instance.getActionResultLob() != null)
            {
                result.setDetail(instance.getActionResultLob().getUDetail());
                result.setSummary(instance.getActionResultLob().getUSummary());
                result.setRaw(instance.getActionResultLob().getURaw());
            }
            result.setNodeId(instance.getUNodeId());
            result.setHidden(Boolean.TRUE.equals(instance.getUHidden()));
        }
        return result;
    }

    private static class ProcessCounts
    {
        @SuppressWarnings("unused")
        public long open = 0;
        @SuppressWarnings("unused")
        public long waiting = 0;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResponseDTO serveResultMacro(String id, String wiki, QueryDTO query, Boolean hidden, String username) throws SearchException
    {
        ResponseDTO<Worksheet> findWorksheetResp = WorksheetSearchAPI.findWorksheetById(id, username);
        
        if (!findWorksheetResp.isSuccess())
        {
            throw new SearchException(findWorksheetResp.getMessage());
        }
        
        String macroType = (String)query.getParams().get(Constants.MACRO_TYPE);
        String activityId = (String)query.getParams().get(Constants.ACTIVITY_ID);
        
        if (StringUtils.isEmpty(macroType) || !(Constants.MACRO_TYPE_RESULTS.equalsIgnoreCase(macroType) || Constants.MACRO_TYPE_RESULTS2.equalsIgnoreCase(macroType) || Constants.MACRO_TYPE_DETAILS.equalsIgnoreCase(macroType)) )
        {
            String err = "Must pass 'macrotype' parameter. Missing or received invalid wiki macro type: " + macroType;
            Exception e = new Exception(err);
            e.setStackTrace(Thread.currentThread().getStackTrace());
            Log.log.warn(err,e);
            macroType = Constants.MACRO_TYPE_DETAILS;
        } 
            
        String macroIncludedFields = null; 
        String macroExcludedFields = null;
        boolean isResults = false;
        boolean isDetails = false;
        if (Constants.MACRO_TYPE_RESULTS.equalsIgnoreCase(macroType))
        {
            macroIncludedFields = Constants.MACRO_RESULTS_INCLUDED_FIELDS;
            macroExcludedFields = Constants.MACRO_RESULTS_EXCLUDED_FIELDS;
            isResults = true;
        }
        else if (Constants.MACRO_TYPE_RESULTS2.equalsIgnoreCase(macroType))
        {
            macroIncludedFields = Constants.MACRO_RESULTS2_INCLUDED_FIELDS;
            macroExcludedFields = Constants.MACRO_RESULTS2_EXCLUDED_FIELDS;
            isResults = true;
        }
        else if (Constants.MACRO_TYPE_DETAILS.equalsIgnoreCase(macroType))
        {
            macroIncludedFields = Constants.MACRO_DETAIL_INCLUDED_FIELDS;
            macroExcludedFields = Constants.MACRO_DETAIL_EXCLUDED_FIELDS;
            isDetails = true;
        }
        
        query.setSelectColumns(macroIncludedFields);
        query.setExcludeColumns(macroExcludedFields);
        
        ResponseDTO result = null;
        if (StringUtils.isNotBlank(id))
        {
            query.addFilterItem(new QueryFilter("auto", id, "problemId", QueryFilter.EQUALS));
            if(hidden)
            {
                query.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, "false", "hidden", QueryFilter.EQUALS));
            }
            StringBuilder archiveWhereClause = new StringBuilder(" tableData.problem.sys_id='" + id + "' and ( 1=1 ");
            
            List<String> taskFullNames = null;
            List<String> taskNames = null;
            
            StringBuffer fullNameKey = new StringBuffer();
            StringBuffer taskNameKey = new StringBuffer();
            for(QueryFilter filter : query.getFilterItems())
            {
                if(StringUtils.isNotBlank(filter.getValue()))
                {
                    if("taskFullName".equalsIgnoreCase(filter.getField()) && StringUtils.isNotBlank(filter.getValue()))
                    {
                        filter.setValue(filter.getValue() + ",abort#resolve");
                        taskFullNames = StringUtils.stringToList(filter.getValue(), ",");
                        //archiveWhereClause.append(" or LOWER(tableData.task.UFullName) IN ("+ SQLUtils.prepareQueryString(values) +") ");
                        archiveWhereClause.append(" or LOWER(tableData.task.UFullName) IN (:taskFullNames) ");
                        fullNameKey.append(filter.getValue());
                    }
                    else if("taskName".equalsIgnoreCase(filter.getField()) && StringUtils.isNotBlank(filter.getValue()))
                    {
                        filter.setValue(filter.getValue() + ",abort");
                        taskNames = StringUtils.stringToList(filter.getValue(), ",");
                        //archiveWhereClause.append(" or LOWER(tableData.task.UName) IN ("+ SQLUtils.prepareQueryString(values) +") ");
                        archiveWhereClause.append(" or LOWER(tableData.task.UName) IN (:taskNames) ");
                        taskNameKey.append(filter.getValue());
                    }
                }
            }
            archiveWhereClause.append(" )");
            
//            String cacheKey = id + macroType + archiveWhereClause + "taskFullName: " + fullNameKey.toString() + "taskName: " + taskNameKey.toString() + wiki; 
            String cacheKey = id + macroType + archiveWhereClause + "taskFullName: " + fullNameKey.toString() + "taskName: " + taskNameKey.toString(); 
            query.setOrderBy(Constants.MACRO_TYPE);
            long wsTaskCount = 0;
            
            Map<String, Object> lastOpenCount = openCountCache.get(id);
            long openValue = lastOpenCount==null ? -1 : (Long)lastOpenCount.get(COUNT_VALUE); 

            long openUpdateInterval = lastOpenCount==null ? -1 : (System.currentTimeMillis()-(Long)lastOpenCount.get(LAST_UPDATE));
            if (lastOpenCount==null || (openValue!=0 && (openUpdateInterval > UPDATE_INTERVAL)))
            {
                long latestCount = WorksheetSearchAPI.getOpenProcessRequestsCount(id, wiki, username);
                Map<String, Object> newCount = new HashMap<String, Object>();
                newCount.put(COUNT_VALUE, latestCount);
                newCount.put(LAST_UPDATE, System.currentTimeMillis());
                openCountCache.put(id, newCount);
                lastOpenCount = newCount;
            }
            
            long open = (Long)lastOpenCount.get(COUNT_VALUE);   //lastOpenWorksheetSearchAPI.getOpenProcessRequestsCount(id, wiki, username);
            
            if (isResults)
            {
                Map<String, Object> cacheObj = resultMacroCache.get(cacheKey);
                if (cacheObj!=null)
                {
                    ResponseDTO cacheResult = (ResponseDTO)cacheObj.get(RESPONSEDTO_VALUE);
                    long cacheTaskCount = (Long)cacheObj.get(COUNT_VALUE);
                    
                    Map<String, Object> lastResultCount = resultCountCache.get(id);
                    long interval = lastResultCount==null ? 0 : System.currentTimeMillis()-(Long)lastResultCount.get(LAST_UPDATE);
                    if (lastResultCount==null || (interval > UPDATE_INTERVAL )) //&& (open!=0 || (open==0 && interval < UPDATE_INTERVAL * 5))))
                    {
                        Map<String, Object> newCount = new HashMap<String, Object>();
                        long latest = WorksheetSearchAPI.getWorksheetResultCount(id, username);
                        newCount.put(COUNT_VALUE, latest);
                        newCount.put(LAST_UPDATE, System.currentTimeMillis());
                        resultCountCache.put(id, newCount);
                        lastResultCount = newCount;
                    }
                
                    wsTaskCount =  (Long)lastResultCount.get(COUNT_VALUE);
                    if (wsTaskCount == cacheTaskCount)
                    {
                        if(StringUtils.isNotEmpty(wiki))
                        {
                            return filterResult(cacheResult, wiki, activityId, username);
                        } 
                        else
                        {  
                            return cacheResult;
                        }
                    }
                }
            }
            else if (isDetails && enableDetailCache)
            {
                Map<String, Object> cacheObj = detailMacroCache.get(cacheKey);
                if (cacheObj!=null)
                {
                    ResponseDTO cacheResult = (ResponseDTO)cacheObj.get(RESPONSEDTO_VALUE);
                    long cacheTaskCount = (Long)cacheObj.get(COUNT_VALUE);
                    
                    Map<String, Object> lastResultCount = resultCountCache.get(id);
                    long interval = lastResultCount==null ? 0 : System.currentTimeMillis()-(Long)lastResultCount.get(LAST_UPDATE);
                    if (lastResultCount==null || interval > UPDATE_INTERVAL )   //&& (open!=0 || (open==0 && interval < UPDATE_INTERVAL * 5))))
                    {
                        Map<String, Object> newCount = new HashMap<String, Object>();
                        long latest = WorksheetSearchAPI.getWorksheetResultCount(id, username);
                        newCount.put(COUNT_VALUE, latest);
                        newCount.put(LAST_UPDATE, System.currentTimeMillis());
                        resultCountCache.put(id, newCount);
                        lastResultCount = newCount;
                    }
                    wsTaskCount =  (Long)lastResultCount.get(COUNT_VALUE);
                    if (wsTaskCount == cacheTaskCount)
                    {
                        if(StringUtils.isNotEmpty(wiki))
                        {
                            return filterResult(cacheResult, wiki, activityId, username);
                        }
                        else
                        {
                            return cacheResult;
                        }
                    }
                }
            }
            
            long waiting = 0; //WorksheetSearchAPI.getWaitProcessRequestsCount(id, wiki, username);
            
            ProcessCounts counts = new ProcessCounts();
            result = WorksheetSearchAPI.serveResultMacroByFilters(query, username);
            String checkArchive = PropertiesUtil.getPropertyString("resultmacro.check.archive");
            if((result.getRecords() == null || result.getRecords().size() == 0) && "true".equalsIgnoreCase(checkArchive))
            {
                //get data from archive
                query.setFilterItems(new ArrayList<QueryFilter>());
                query.setWhereClause(archiveWhereClause.toString());
                query.setOrderBy("");
                result = listArchiveWorksheetResultsByWorksheetId(id, query, hidden, taskFullNames, taskNames, username);
            }
            else
            {
                //get count for incomplete processes.
                counts.open = open; //WorksheetSearchAPI.getOpenProcessRequestsCount(id, wiki, username);
                counts.waiting = waiting; //WorksheetSearchAPI.getWaitProcessRequestsCount(id, wiki, username);
            }
            
            result.setData(counts);
            
            if (result!=null && result.getTotal()>0)     
            {
                if(isResults)
                {
                    Map<String, Object> resMap = new HashMap<String, Object>();
                    resMap.put(RESPONSEDTO_VALUE, result);
                    resMap.put(COUNT_VALUE, wsTaskCount);
                    resultMacroCache.put(cacheKey, resMap);
//                    resultMacroCache.put(cacheKey, result);
                }
                else if (isDetails && enableDetailCache)
                {
                    List<TaskResult> ltr =  result.getRecords();
                    int size = 0;
                    for (TaskResult tr : ltr)
                    {
                        String detail = tr.getDetail();
                         size += (detail==null ? 0 : detail.length());
                    }

                    if (size > detailCacheThreshold)
                    {
                        Map<String, Object> resMap = new HashMap<String, Object>();
                        resMap.put(RESPONSEDTO_VALUE, result);
                        resMap.put(COUNT_VALUE, wsTaskCount);
                        detailMacroCache.put(cacheKey, resMap);
                    }
                }
            }
        }
        
        if(StringUtils.isNotEmpty(wiki))
            result = filterResult(result, wiki, activityId, username);

        return result;
    }
    
    // Filter the result set with specific wiki name
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static ResponseDTO filterResult(ResponseDTO result, String targetWiki, String activityId, String username) {

        result = copyTaskResultDTO(result);
        
        List<TaskResult> records = result.getRecords();
        
        // 1. Record all the process ids of the records with their wiki names
        Map<String, String> processIds = new HashMap<String, String>();
        
        for(TaskResult record: records) {
            String wikiName = record.getWiki();
            String processId = record.getProcessId();
            
            String names = processIds.get(processId);
            if(StringUtils.isNotEmpty(names) )
            {    
                if (!includes(names, wikiName))
                {
                    processIds.put(processId, names + "|" + wikiName);
                }
            }
            else
            {
                processIds.put(processId, wikiName);
            }
        }
        
        // 2. Remove the process ids with the specific wiki name as a subrunbook name and the process ids that do not include the name specified
        for(TaskResult record: records) {
            String wikiName = record.getWiki();
            String processId = record.getProcessId();

            String names = processIds.get(processId);
            if(targetWiki.equalsIgnoreCase(wikiName)) {
                if(processIds.containsKey(processId)) {
                    if(StringUtils.isNotBlank(names) && names.contains("|") && !includes(names, wikiName))
                        processIds.remove(processId);
                }
            }
            
            else if(names != null && !includes(names, targetWiki) && processIds.containsKey(processId))
                processIds.remove(processId);
        }
        
        // 3. Remove the records that does not belong to any of the process ids or does not belong to the executed wiki or includd particular activity id
        for(int i=0; i<records.size(); i++) {
            TaskResult record = records.get(i);
            
            if(StringUtils.isNotEmpty(activityId)) {
                String actId = record.getActivityId();
                if(!activityId.equals(actId)) {
                    records.remove(i);
                    i--;
                    continue;
                }
            }
            
            String processId = record.getProcessId();

            if(processIds.size() != 0 && processIds.containsKey(processId)) {
                String executeWikiName = record.getExecuteRequestId();
                if(StringUtils.isNotBlank(executeWikiName)) {
                    if (!executeWikiName.contains("::")) {
                        /*
                         * looks like this user does not have permission to execute either RB or one of the task(s).
                         * In this case only, executeRequestId will be a 32 bit GUID and will not have any '::'.
                         */
                        continue;
                    }
                    String execute = executeWikiName.split("::")[0];
                    if(targetWiki.equalsIgnoreCase(execute))
                        continue;
                    
                    else records.remove(i);
                }
                
                else
                    records.remove(i);
            }
            
            else
                records.remove(i);
            
            i--;
        }

        int total = (new Long(result.getTotal())).intValue();
        if(total != records.size())
            result.setTotal((new Long(records.size()).longValue()));
        
        return result;
    }

    private static Comparator<TaskResult> comp = new Comparator<TaskResult>()
    {
       public int compare(TaskResult t1, TaskResult t2)
       {
           long d =  t1.getSysCreatedOn() - t2.getSysCreatedOn() ;
           if (d>0)
           {
               return 1;
           }
           else if (d<0)
           {
               return -1;
           }
           else
           {
               return 0;
           }
       }
    };
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static ResponseDTO copyTaskResultDTO(ResponseDTO src)
    {
        ResponseDTO cp = new ResponseDTO();
        cp.setData(src.getData());
        cp.setMessage(src.getMessage());
        
        List records = src.getRecords();
        List cpRecords = new ArrayList(records);
        Collections.sort((List<TaskResult>)cpRecords, comp);
        cp.setRecords(cpRecords);
        cp.setSuccess(src.isSuccess());
        cp.setTotal(src.getTotal());
        return cp;
    }
    
    /*
     * Code from WorkSheetService.java is added here so that RSControl could have access to it.
     * WorkSheetService.java from RSView project is removed.
     */

    public static String getAssigntoName(String assigntoID)
    {
        String assigntoName = null;

        try
        {
            if (assigntoID != null && !assigntoID.equals(""))
            {
            	assigntoName =  (String) HibernateProxy.execute(() -> {
                	UsersDAO usersDAO = HibernateUtil.getDAOFactory().getUsersDAO();
                    Users users = usersDAO.findById(assigntoID);

                    if (users != null)
                    {
                        return users.getUUserName();
                    } else {
                    	return false;
                    }
                });
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return assigntoName;
    } //getAssigntoName

    /**
     *
     * @param id
     * @param userid
     * @param timezone
     * @return
     */
    @Deprecated
    public static Worksheet getModelByIDCAS(String id, String userid, String timezone)
    {
        return getModelByIDCAS(id, userid, timezone, null, null);

    } //getModelByIDCAS
    
    /**
    *
    * @param id
    * @param userid
    * @param timezone
    * @param orgId
    * @param orgName
    * @return
    */
   public static Worksheet getModelByIDCAS(String id, String userid, String timezone, String orgId, String orgName)
   {
       Worksheet model = new Worksheet();
       String idLocal = null;

       try
       {
           if (id != null && !id.equals(""))
           {
               if (id.equals(HibernateConstantsEnum.WS_ACTIVE.getTagName()))
               {
                   idLocal = WorksheetUtils.getActiveWorksheet(userid, orgId, orgName);

                   if (idLocal != null)
                   {
                       id = idLocal;
                   }
                   else
                   {
                       return model;
                   }
               }
               else if (id.equals(HibernateConstantsEnum.WS_NEGATIVE_ONE.getTagName()) || id.equals(HibernateConstantsEnum.WS_CREATE.getTagName()))
               {
                   return model;
               }

               Worksheet ws = WorksheetUtil.findWorksheetById(id, userid);
               if (ws == null)
               {
                   return model;
               }
               else
               {
                   model = ws;
               }
           }
       }
       catch (Throwable t)
       {
           Log.log.error(t.getMessage(), t);
           throw new RuntimeException(t);
       }

       return model;

   } //getModelByIDCAS
   
   @Deprecated
    public static Worksheet getModelByID(String id, String userid, String timezone)
    {
        return getModelByIDCAS(id, userid, timezone, null, null);
    } //getModelByID
    
    public static Worksheet getModelByID(String id, String userid, String timezone, String orgId, String orgName)
    {
        return getModelByIDCAS(id, userid, timezone, orgId, orgName);
    } //getModelByID
    
    /**
     *
     * @param worknotes
     * @param timezone
     * @return
     */
    protected static String buildWorkNotesHTML(String worknotes, String timezone)
    {
        StringBuffer htmlSB = new StringBuffer();

        if (worknotes != null && !worknotes.equals(""))
        {
            try
            {
                JSONArray worknotesArray = JSONArray.fromObject(worknotes);

                if (worknotesArray != null)
                {
                    for (int i = 0; i < worknotesArray.size(); i++)
                    {
                        JSONObject wn = worknotesArray.getJSONObject(i);

                        if (wn != null)
                        {
                            htmlSB.append(convertJSONToString(wn, timezone));
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
        }

        return htmlSB.toString();
    } //buildWorkNotesHTML

    /**
     *
     * @param worknotes
     * @param timezone
     * @return
     */
    protected static String convertJSONToString(JSONObject worknotes, String timezone)
    {
        String html = "";
        String createDate = "";
        String userid = "";
        String detail = "";

        if (worknotes != null)
        {
            Long createDateInLong = (Long) worknotes.get(HibernateConstantsEnum.WS_GENERAL_WN_CREATE_DATE.getTagName());

            if (createDateInLong != null)
            {
                long createDateInlong = createDateInLong.longValue();
                // Timestamp ts = new Timestamp(createDateInlong);
                // createDate = DateUtils.convertGMTToLocalTimeString(ts,
                // timezone, true);
                createDate = DateUtils.convertTimeInMillisToLocalString(createDateInlong, timezone);
                userid = (String) worknotes.get(HibernateConstantsEnum.WS_GENERAL_WN_USER_ID.getTagName());
                detail = (String) worknotes.get(HibernateConstantsEnum.WS_GENERAL_WN_DETAIL.getTagName());

                html = "<tr><td colspan=\"2\"><table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td colspan=\"2\"><hr></td></tr><tr style=\"font-weight:bold\">"
                                + "<td class=\"tdwrap\"><strong style=\"font-weight: bold; font-size: 13px\"; >"
                                + createDate
                                + " - <a style=\"color: blue;\">"
                                + userid
                                + "</a></strong></td></tr><tr style=\"\">"
                                + "<td colspan=\"2\"><span>"
                                + detail
                                + "</span></td></tr></tbody></table></td></tr>";
            }
        }

        return html;
    } //convertJSONToString

    public static Worksheet saveWorksheet(Worksheet worksheet, String userid) throws Exception
    {
        Worksheet result = null;
        
        if (!UserUtils.isAdminUser(userid))
        {
            if (StringUtils.isNotBlank(worksheet.getSysOrg()))
            {
                if (!UserUtils.isOrgAccessible(worksheet.getSysOrg(), userid, false, false))
                {
                    throw new Exception("User does not have permission to create new worksheet or update existing worksheet for specified Org."); 
                }
            }
            else
            {
                if (!UserUtils.isNoOrgAccessible(userid))
                {
                    throw new Exception("User does not have permission to create new worksheet or update existing worksheet for specified No Org ('None').");
                }
            }
        }
        
        String problemNumber = worksheet.getNumber();
        //UI is sending the number, this check is for other entry points where number don't come with the worksheet object.
        if(StringUtils.isBlank(problemNumber))
        {
            problemNumber = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PRB);
        }

        if(worksheet != null)
        {
            UsersVO assignedTo = null;
            if (StringUtils.isNotBlank(worksheet.getAssignedTo()))
            {
                //assignedTo is actually the login id (username) of a user.
                assignedTo = ServiceHibernate.getUser(null, worksheet.getAssignedTo());
            }
            else
            {
                //it is a brand new worksheet
                if(StringUtils.isBlank(worksheet.getSysId()))
                {
                    assignedTo = ServiceHibernate.getUser(userid);
                }
            }

            String rowKey = (StringUtils.isBlank(worksheet.getSysId()) ? SearchUtils.getNewRowKey() : worksheet.getSysId());
            String problemid = rowKey;
            Worksheet tmpWorksheet = new Worksheet(rowKey, null, userid);
            tmpWorksheet.setNumber(problemNumber);
            if (StringUtils.isNotBlank(worksheet.getSysId()))
            {
                Worksheet oldWorksheet = WorksheetUtil.findWorksheetById(problemid, userid);
                if (oldWorksheet != null)
                {
                    //Worksheet exists
                    tmpWorksheet = oldWorksheet;
                    tmpWorksheet.setSysUpdatedOn(DateUtils.GetUTCDateLong());
                    tmpWorksheet.setSysUpdatedBy(userid);
                }
            }
            tmpWorksheet.setAlertId(worksheet.getAlertId()==null?"":worksheet.getAlertId());
            tmpWorksheet.setReference(worksheet.getReference()==null?"":worksheet.getReference());
            tmpWorksheet.setCorrelationId(worksheet.getCorrelationId()==null?"":worksheet.getCorrelationId());
            if(assignedTo != null)
            {
                tmpWorksheet.setAssignedTo(assignedTo.getUUserName());
                tmpWorksheet.setAssignedToName(assignedTo.getUDisplayName());
            }
            else
            {
                tmpWorksheet.setAssignedTo(null);
                tmpWorksheet.setAssignedToName(null);
            }

            tmpWorksheet.setCondition(worksheet.getCondition()==null?"":worksheet.getCondition());
            tmpWorksheet.setSeverity(worksheet.getSeverity()==null?"":worksheet.getSeverity());

            if (StringUtils.isNotEmpty(worksheet.getSysCreatedBy()))
            {
                tmpWorksheet.setSysCreatedBy(worksheet.getSysCreatedBy());
            }
            
            if (StringUtils.isNotBlank(worksheet.getSysOrg()))
            {
                tmpWorksheet.setSysOrg(worksheet.getSysOrg());
            }

            if (StringUtils.isNotEmpty(worksheet.getCondition()))
            {
                tmpWorksheet.setCondition(worksheet.getCondition());
            }

            if (StringUtils.isNotEmpty(worksheet.getSeverity()))
            {
                tmpWorksheet.setSeverity(worksheet.getSeverity());
            }

            tmpWorksheet.setSummary(worksheet.getSummary());
            tmpWorksheet.setDescription(worksheet.getDescription());

            String worknotesString = buildWorkNotes(worksheet.getWorkNotes(), tmpWorksheet.getWorkNotes(), userid);
            if (StringUtils.isNotEmpty(worknotesString))
            {
                tmpWorksheet.setWorkNotes(worknotesString);
            }
            if (StringUtils.isNotEmpty(worksheet.getWorksheetXml()))
            {
                tmpWorksheet.setWorksheetXml(worksheet.getWorksheetXml());
            }
            tmpWorksheet.setModified(worksheet.getModified());
            
            result = tmpWorksheet;
            WorksheetIndexAPI.indexWorksheet(tmpWorksheet, userid);
        }

        return result;
    }


     /**
      *
      * @param sysId
      * @param newWorknotes
      * @param userid
      * @return
      */
     private static String buildWorkNotes(String newWorknotes, String oldWorknotes, String userid)
     {
         String worknotesString = oldWorknotes;
         JSONArray sourceArray = new JSONArray();

         if (StringUtils.isNotBlank(newWorknotes))
         {
             String currentWorkNotesString = oldWorknotes;

             if (StringUtils.isNotBlank(currentWorkNotesString))
             {
                 sourceArray = JSONArray.fromObject(currentWorkNotesString);
             }

             JSONObject worknotesJSONObjtect = new JSONObject();
             worknotesJSONObjtect.put(HibernateConstantsEnum.WS_GENERAL_WN_CREATE_DATE.getTagName(), System.currentTimeMillis());
             worknotesJSONObjtect.put(HibernateConstantsEnum.WS_GENERAL_WN_USER_ID.getTagName(), userid);
             worknotesJSONObjtect.put(HibernateConstantsEnum.WS_GENERAL_WN_DETAIL.getTagName(), newWorknotes);
             sourceArray.add(worknotesJSONObjtect);

             worknotesString = sourceArray.toString();
         }

         return worknotesString;
     } //buildWorkNotes

     /**
     *
     * @param worksheetID
     * @param userid
     * @param timezone
     * @return
     */
    public static List<TaskResult> getActionResultsByWorksheetId(String worksheetID, String userid, String timezone, String orgId, String orgName)
    {
        List<TaskResult> results = new ArrayList<TaskResult>();
        String idLocal = null;

        try
        {
            if (StringUtils.isNotBlank(worksheetID))
            {
                if (worksheetID.equals(HibernateConstantsEnum.WS_ACTIVE.getTagName()))
                {
                    idLocal = WorksheetUtils.getActiveWorksheet(userid, orgId, orgName);

                    if (idLocal != null)
                    {
                        worksheetID = idLocal;
                    }
                    else
                    {
                        return results;
                    }
                }
                else if (worksheetID.equals(HibernateConstantsEnum.WS_NEGATIVE_ONE.getTagName()) || worksheetID.equals(HibernateConstantsEnum.WS_CREATE.getTagName()))
                {
                    return results;
                }

                List<TaskResult> actionResults = WorksheetSearchAPI.findTaskResultByWorksheetId(worksheetID, null, userid).getRecords();

                ProcessRequest process = null;

                for (TaskResult result : actionResults)
                {
                    if(StringUtils.isNotBlank(result.getProcessId()) && (process == null || process.getSysId() == null || !process.getSysId().equals(result.getProcessId())))
                    {
                        process = WorksheetSearchAPI.findProcessRequestsById(result.getProcessId(), userid).getData();
                    }
                    if(process != null)
                    {
                        result.setProcessNumber(process.getNumber());
                    }
                    results.add(result);
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return results;
    } //getActionResultsByWorksheetID

     /**
      *
      * @param timestamp
      * @param timezone
      * @return
      */
     protected static String getTimestampString(Long timestamp, String timezone)
     {
         String timestampString = "";

         if (timestamp != null && timestamp.longValue() != 0)
         {
             Timestamp ts = new Timestamp(timestamp.longValue());
             Timestamp tsGMT = DateUtils.convertLocalToGMTTime(ts, timezone, true);
             timestampString = DateUtils.convertGMTToLocalTimeInMilliSecondsString(tsGMT, timezone, true);
         }

         return timestampString;
     } //getTimestampString

     /**
      *
      * @param id
      * @return
      */
     protected static String getActionTaskNameByID(String id)
     {
         String actionTaskName = "";

         try
         {
             
             ResolveActionTask actionTask = (ResolveActionTask) HibernateProxy.execute(() -> {
            	 ResolveActionTaskDAO actionTaskDAO = HibernateUtil.getDAOFactory().getResolveActionTaskDAO();

                 if (id != null && !id.equals(""))
                 {
                     return actionTaskDAO.findById(id);                     
                 } else { 
                	 return null;
                 }
                 
             });
             
             if (actionTask != null)
             {
                 actionTaskName = actionTask.getUName();
             }

             

         }
         catch (Throwable t)
         {
             Log.log.error(t.getMessage(), t);
                        HibernateUtil.rethrowNestedTransaction(t);
         }

         return actionTaskName;
     } //getActionTaskNameByID

     /**
      *
      * @param matchInt
      * @return
      */
     protected static String getMatchOperation(int matchInt)
     {
         String matchOperation = " = ";

         switch (matchInt)
         {
             case 1:
                 matchOperation = " = ";
                 break;
             case 2:
                 matchOperation = " != ";
                 break;
             case 3:
                 matchOperation = " like ";
                 break;
         }

         return matchOperation;
     } //getMatchOperation

     /**
     *
     * @param userid
     * @param modelid
     * @param orgId (optional)
     * @param orgName (optional)
     * @return
     * @throws Exception
     */
    public static String activeModel(String userid, String modelid, String orgId, String orgName) throws Exception
    {
        WorksheetUtils.setActiveWorksheet(userid, modelid, orgId, orgName);
        return "SUCCESS";
    } //activeModel
    
     /**
      *
      * @param userid
      * @param modelid
      * @return
      * @throws Exception
      */
    @Deprecated
     public static String activeModel(String userid, String modelid) throws Exception
     {
         WorksheetUtils.setActiveWorksheet(userid, modelid, null, null);
         return "SUCCESS";
     } //activeModel

     public static String getRunbookResult(String problemId, String processId, String username, boolean isDetail, boolean isWSDATA, int timeout, String orgId, String orgName) {

         int retry = 1;
         String status = null;
         JSONObject json = new JSONObject();

         Worksheet worksheet = null;
         List<TaskResult> taskResults = null;

         while(retry <= timeout) {
             if(StringUtils.isNotEmpty(problemId))
                 taskResults = WorksheetUtil.getActionResultsByWorksheetId(problemId, username, null, orgId, orgName);

             if(StringUtils.isNotEmpty(processId))
                 status = ServiceWorksheet.getProcessStatusForRunbook(processId);

             if(Log.log.isDebugEnabled()) {
                 Log.log.debug("retry = " + retry);
                 Log.log.debug("status = " + status);
             }

             if(status != null && status.equals(Constants.EXECUTE_STATUS_OPEN)) {
                 try {
                     Thread.sleep(1000);
                 } catch(Exception e) {}

                 retry ++;
                 continue;
             }

             if(status != null && status.equals(Constants.EXECUTE_STATUS_COMPLETED)) {
                 break;
             }

             else if(status != null && status.equals(Constants.EXECUTE_STATUS_WAITING)) {
                 break;
             }

             else if(status != null && status.equals(Constants.EXECUTE_STATUS_ABORTED)) {

                 if(taskResults != null && taskResults.size() != 0) {
                     int starts = 0;

                     for(TaskResult taskResult:taskResults) {
                         String task = taskResult.getTaskFullName();

                         if(task.contains("start#resolve"))
                             starts ++;
                         else if(task.contains("abort#resolve"))
                             starts --;
                         else if(task.contains("end#resolve"))
                             starts --;
                     }

                     if(starts != 0) {
                         try {
                             Thread.sleep(1000);
                         } catch(Exception e) {}

                         retry ++;
                         continue;
                     }

                     else
                         break;
                 }
             } // ABORT

             // Delay some time for ES to index the worksheet. Otherwise, no detailed result is available even the runbook has completed execution
             try {
                 Thread.sleep(1000);
             } catch(Exception e) {}

             retry ++;
         } // while()

         worksheet = ServiceWorksheet.findWorksheetById(problemId, "system");

         if(worksheet == null) {
             json.accumulate("message", "Worksheet information is not available at this moment. Please try again later.");
             return json.toString();
         }

         String condition = worksheet.getCondition();
         String severity = worksheet.getSeverity();
         String number = worksheet.getNumber();
         String summary = worksheet.getSummary();

         try {
             json.accumulate("worksheetId", problemId);
             json.accumulate("worksheetNumber", number);
             json.accumulate("summary", summary);

             json.accumulate("condition", condition);
             json.accumulate("severity", severity);

             if(StringUtils.isNotEmpty(status))
                 json.accumulate("status", status);

             if(isWSDATA) {
                 String wsData = ServiceWorksheet.getWSDATAForRunbook(problemId, username);

                 if(StringUtils.isNotEmpty(wsData))
                     json.accumulate("WSDATA", wsData);
             }

             if(taskResults != null && taskResults.size() > 0) {
                 JSONArray tasks = new JSONArray();

                 for(Iterator<TaskResult> it = taskResults.iterator(); it.hasNext();) {
                     TaskResult task = it.next();

                     String name = task.getTaskFullName();
                     if(Log.log.isDebugEnabled()) {
                         Log.log.debug("task name = " + name);
                     }

                     JSONObject taskItem = new JSONObject();
                     taskItem.accumulate("name", task.getTaskFullName());
                     taskItem.accumulate("id", task.getId());
                     taskItem.accumulate("completion", task.getCompletion());
                     taskItem.accumulate("condition", task.getCondition());
                     taskItem.accumulate("severity", task.getSeverity());
                     taskItem.accumulate("summary", task.getSummary());

                     if(isDetail)
                         taskItem.accumulate("detail", task.getDetail());

                     tasks.add(taskItem);
                 }

                 json.accumulate("taskResults", tasks);
             }

             else {
                 json.accumulate("taskResults", "Action Task result is not available at this moment. Please try again later.");
             }

         } catch(Exception e) {
             Log.log.error(e.getMessage(), e);
         }

         return json.toString();

     } // getRunbookResult()

     public static String getActionTaskResult(String problemId, String processId, String actionTaskName, String username, int timeout, String orgId, String orgName) {

         int retry = 1;
         String status = null;
         JSONObject json = new JSONObject();
         List<TaskResult> taskResults = null;

         while(retry <= timeout) {
             if(StringUtils.isNotEmpty(processId))
                 status = ServiceWorksheet.getProcessStatusForRunbook(processId);

             if(Log.log.isDebugEnabled()) {
                 Log.log.debug("retry = " + retry);
                 Log.log.debug("status = " + status);
             }

             if(status != null && status.equals("OPEN")) {
                 try {
                     Thread.sleep(1000);
                 } catch(Exception e) {}

                 retry ++;
                 continue;
             }

             taskResults = WorksheetUtil.getActionResultsByWorksheetId(problemId, username, null, orgId, orgName);

             if(taskResults != null && taskResults.size() != 0) {
                 int starts = 0;

                 for(TaskResult taskResult:taskResults) {
                     String task = taskResult.getTaskFullName();

                     if(task.contains("start#resolve"))
                         starts ++;
                     else if(task.contains("abort#resolve"))
                         starts --;
                     else if(task.contains("end#resolve"))
                         starts --;
                 }

                 if(starts != 0) {
                     try {
                         Thread.sleep(1000);
                     } catch(Exception e) {}

                     retry ++;
                     continue;
                 }

                 else
                     break;
             }

             // Delay some time for ES to index the worksheet. Otherwise, no detailed result is available even the runbook has completed execution
             try {
                 Thread.sleep(1000);
             } catch(Exception e) {}

             retry ++;
         }

         if(taskResults == null || taskResults.size() == 0) {
             json.accumulate("message", "Action Task result is not available at this moment. Please try again later.");
             return json.toString();
         }

         try {
             for(Iterator<TaskResult> it = taskResults.iterator(); it.hasNext();) {
                 TaskResult task = it.next();
                 String taskName = task.getTaskFullName();
                 if(!taskName.equalsIgnoreCase(actionTaskName))
                     continue;

                 json.accumulate("worksheetId", problemId);

                 json.accumulate("name", task.getTaskFullName());
                 json.accumulate("id", task.getId());
                 json.accumulate("completion", task.getCompletion());
                 json.accumulate("condition", task.getCondition());
                 json.accumulate("severity", task.getSeverity());
                 json.accumulate("summary", task.getSummary());
                 json.accumulate("detail", task.getDetail());

                 break;
             }
         } catch(Exception e) {
             Log.log.error(e.getMessage(), e);
         }

         if(json.size() == 0) {
             json.accumulate("worksheetId", problemId);
             json.accumulate("warning", "The specific action task is not found. Please try again.");
         }

         return json.toString();

     } // getActionTaskResult()
     @SuppressWarnings({ "unchecked" })
    public static void initDetailCache(boolean enable, int size, int threshold)
     {
         enableDetailCache = enable;
         detailCacheSize = size;
         detailCacheThreshold = threshold;
         if (enableDetailCache)
         {
             detailMacroCache =  Collections.synchronizedMap(new LRUMap(detailCacheSize));
         }
     }
       
     private static void startCleanupThread()
     {
         Thread cleanup = new Thread(new Runnable()
         {
             public void run()
             {
                 while(true)
                 {
                     long now = System.currentTimeMillis(); // minutes before processid is removed.
                     for (String id : resultCountCache.keySet() )
                     {
                         Map<String,Object> countObj = resultCountCache.get(id);
                         long interval = now - (long) countObj.get(LAST_UPDATE);
                         if (interval>CLEANUP_INTERVAL)
                         {
                             resultCountCache.remove(id);
                         }
                     }
                     
                     for (String id : openCountCache.keySet() )
                     {
                         Map<String,Object> countObj = openCountCache.get(id);
                         long interval = now - (long) countObj.get(LAST_UPDATE);
                         if (interval>CLEANUP_INTERVAL)
                         {
                             openCountCache.remove(id);
                         }
                     }
                     
                     try
                     {
                         Thread.sleep(60000); // Wake up every 60 seconds to clean up
                     }
                     catch (InterruptedException ex)
                     {
                         Log.log.error("Cleanup thread is interrupted unexpectedly.", ex);
                         break;
                     }
                 }
             }

         });
         cleanup.start();
     } // startCleanupThread

     public static boolean includes(String names, String target) {
         
         if(StringUtils.isBlank(names))
             return false;
         
         String[] nameList = names.split("\\|");
         
         for(int i=0; i<nameList.length; i++) {
             if(nameList[i].equals(target))
                 return true;
         }
         
         return false;
     }
     
     @SuppressWarnings("unchecked")
	public static Map<String, Pair<String, String>> getMapOfActiveWorksheets(String userid) throws Exception
     {
    	 long startTime = System.currentTimeMillis();
    	 long startTimeInstance = startTime;
         
         final ConcurrentMap<String, Pair<String, String>> mapOfActiveWorksheets = 
        		 												new ConcurrentHashMap<String, Pair<String, String>>();
         
         if (StringUtils.isBlank(userid)) {
        	 return mapOfActiveWorksheets;
         }
         
         Map<String, String> accessibleOrgIdNames = UserUtils.getUserOrgHierarchyIdNameMap(userid);
         if (Log.log.isDebugEnabled()) {
	         Log.log.debug(String.format("Time taken for getting %d Accessible Org Ids to Org Names map for user %s: " +
						 				 "%d msec", accessibleOrgIdNames.size(), userid,
						 				 (System.currentTimeMillis() - startTimeInstance)));
	         startTimeInstance = System.currentTimeMillis();
         }
         
         boolean isNoneOrgAccessible = UserUtils.isNoOrgAccessible(userid);
         if (Log.log.isDebugEnabled()) {
	         Log.log.debug(String.format("Time taken for %s user's access to None Org: %d msec",
	 					 				 userid, (System.currentTimeMillis() - startTimeInstance)));
	         startTimeInstance = System.currentTimeMillis();
         }
         
         if (isNoneOrgAccessible) {
        	 accessibleOrgIdNames.put(OrgsVO.NONE_ORG_NAME, OrgsVO.NONE_ORG_NAME);
         }
         
         accessibleOrgIdNames.keySet().parallelStream().forEach(orgId -> {
        	 long fStartTime = System.currentTimeMillis();
        	 long fStartTimeInstance = fStartTime;
        	 List<ResolveSession> resolveSessions = null;
        	 
             try {
            	 HibernateProxy.setCurrentUser(userid);
            	 resolveSessions =  (List<ResolveSession>) HibernateProxy.executeHibernateInitLocked(() -> {
                     ResolveSession query = new ResolveSession();
                     
                     query.setUActiveType(userid);
                     
                     List<String> nullProps = new ArrayList<String>();
                     
                     if (!OrgsVO.NONE_ORG_NAME.equalsIgnoreCase(orgId)) {
                    	 query.setSysOrg(orgId);
                     } else {
                    	 nullProps.add(BASEMODEL_SYS_ORG);
                     }
                     
                     List<OrderbyProperty> orderByList = new ArrayList<OrderbyProperty>();
                     orderByList.add(new OrderbyProperty(BASEMODEL_SYS_UPDATED_ON, false));
                     
                     return HibernateUtil.getDAOFactory().getResolveSessionDAO()
                    		 		   .findNullProps(query, 1, 0, orderByList, nullProps);
                     
                      
            	 });
                 
                 if (Log.log.isDebugEnabled()) {
	                 Log.log.debug(String.format("Time taken for getting %d Resolve Sessions for user %s and %s Org: %d " +
	                		 					 "msec", (resolveSessions != null ? resolveSessions.size() : 0), userid, 
	                		 					 orgId, (System.currentTimeMillis() - fStartTimeInstance)));
	                 fStartTimeInstance = System.currentTimeMillis();
                 }
             } catch (Throwable t) {
            	 Log.log.error(String.format("Error %sin getting map of active worksheets for user %s", 
            			 					 (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
            			 					 userid), t);
                 HibernateUtil.rethrowNestedTransaction(t);
                 try {
					throw new Exception(t);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
             }
             
             if (CollectionUtils.isNotEmpty(resolveSessions)) {
            	 ResolveSession rs = resolveSessions.get(0);
            	 String effectiveWorksheetId = (rs.ugetUIsArchive().booleanValue() && 
							 					rs.getArchiveWorksheet() != null) ? 
							 					rs.getArchiveWorksheet().getSys_id() : rs.getUWorksheetId();
				
				if (StringUtils.isNotEmpty(effectiveWorksheetId) && StringUtils.isNotEmpty(rs.getWorksheetNum())) {
					mapOfActiveWorksheets.putIfAbsent(accessibleOrgIdNames.get(orgId), 
                     		 					  	  Pair.of(effectiveWorksheetId, rs.getWorksheetNum()));
				} else {
					Log.log.info(String.format("Active worksheet id and problem number is null in latest Resolve Session " +
											   "for Org %s", accessibleOrgIdNames.get(orgId)));
				}
             }
             
             if (Log.log.isDebugEnabled()) {
	             Log.log.debug(String.format("Time taken to find active worksheet for %s Org" + 
	 					 					 " out of total 1 Org Resolve Sessions: %d msec", 
	 					 					 accessibleOrgIdNames.get(orgId), 
	 					 					 (System.currentTimeMillis() - fStartTime)));
             }
         });
         
         if (Log.log.isDebugEnabled()) {
        	 Log.log.debug(String.format("Time taken to find active worksheets for %d Orgs: %d msec",
					   				 	 mapOfActiveWorksheets.size(),(System.currentTimeMillis() - startTime)));
         }
         
         return mapOfActiveWorksheets;
     }
     
    public static String bcryptExecutionSummaryHash(String x) {
    	String answer = x;
         
//        long startTime = 0;
//        if (PerformanceDebug.debugRSControl()) {
//        	startTime = System.currentTimeMillis();
//     	}
//     	
//        if (!StringUtils.isEmpty(x) && !x.startsWith(CryptUtils.prefix128Local)) {
//             long startnsec = System.nanoTime();
//             
//             String hash = BCrypt.hashpw(x, BCrypt.gensalt(1, new SecureRandom()));
//             
//             if (Log.log.isTraceEnabled()) {
//            	 Log.log.trace(String.format("Bcrypt execution summary hash with 4 rounds took %d nsec", 
//            			 					  (System.nanoTime() - startnsec)));
//             }
//             
//             try {
//                 answer = CryptUtils.encrypt(hash);
//             } catch (Exception e) {
//                 Log.log.error(String.format("Failed to encrypt execution summary hash due to %s", e.getMessage()), e);
//             }
//        }
//        
//        if (PerformanceDebug.debugRSControl())
//    	{
//    		Log.log.info(String.format("bcryptExecutionSummaryHash of string of %d size took total %d msec", 
//    								   x.length(), (System.currentTimeMillis() - startTime)));
//    	}
        
        return answer;
    } // bcryptExecutionSummaryHash
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private static Collection<Pair<String, String>> distinctUserOrgsWithActiveWS() {
    	final List<Pair<String, String>> result = new ArrayList<Pair<String, String>>();
    	
		try {
			HibernateProxy.setCurrentUser(SYSTEM_USER);
			HibernateProxy.executeHibernateInitLocked(() -> {
				Query sqlQuery = HibernateUtil.createSQLQuery("select distinct u_active_type, sys_org " + 
															  "from resolve_session " + 
															  "where u_active_type is not null");

				List<Object[]> userOrgs = sqlQuery.list();

				if (CollectionUtils.isNotEmpty(userOrgs)) {
					userOrgs.parallelStream().forEach(userOrg -> {
						result.add(Pair.of((String) userOrg[0],
								StringUtils.isNotBlank((String) userOrg[1]) ? (String) userOrg[1] : null));
					});
				}
			});           
           
        } catch(Throwable t) {
            Log.log.error(String.format("Error %sin getting distinct users active worksheets per Org", 
            							(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new RuntimeException(t);
        }
    	return result;
    }
    
    @SuppressWarnings("unchecked")
	private static List<ResolveSession> getResolveSessionsForUserOrg(Pair<String, String> userOrg) {
    	List<ResolveSession> resolveSessions = null;
    	
    	try {
    		HibernateProxy.setCurrentUser(userOrg.getLeft());
    		resolveSessions = (List<ResolveSession>) HibernateProxy.executeHibernateInitLocked(() -> {
    			ResolveSession query = new ResolveSession();
                
                query.setUActiveType(userOrg.getLeft());
                
                List<String> nullProps = new ArrayList<String>();
                
                if (StringUtils.isNotBlank(userOrg.getRight())) {
                	query.setSysOrg(userOrg.getRight());
                } else {
                	nullProps.add(BASEMODEL_SYS_ORG);
                }
                
                List<OrderbyProperty> orderByList = new ArrayList<OrderbyProperty>();
                orderByList.add(new OrderbyProperty(BASEMODEL_SYS_UPDATED_ON, false));
                
                return HibernateUtil.getDAOFactory().getResolveSessionDAO()
               		 		   	  .findNullProps(query, orderByList, nullProps);
    		});

        } catch (Throwable t) {
       	 	Log.log.error(String.format("Error %sin getting Resolve Session for user %s%s", 
       	 								(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
       	 								userOrg.getLeft(), 
       	 								(StringUtils.isNotBlank(userOrg.getRight()) ? " Org " + userOrg.getRight() : "")), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new RuntimeException(t);
        }
    	
    	return resolveSessions;
    }
    
    public static int cleanupResolveSessions() {
    	Map<Pair<String, String>, Integer> cleanedUpCountByUserOrg = new HashMap<Pair<String, String>, Integer>();
    	
    	// Get set of users + Org (including None) with 1 or more active WS (in ES or archived)
    	Collection<Pair<String, String>> userOrgs = distinctUserOrgsWithActiveWS();
    	
    	if (CollectionUtils.isNotEmpty(userOrgs)) {
    		userOrgs.parallelStream().forEach(userOrg -> {
    			if (userOrg != null && StringUtils.isNotBlank(userOrg.getLeft())) {
	    			List<ResolveSession> resolveSessions = getResolveSessionsForUserOrg(userOrg);
	    			
	    			if (CollectionUtils.isNotEmpty(resolveSessions)) {
	    				final StringBuilder foundWSNumStrBldr = new StringBuilder();
	    				ResolveSession latestRSWithExistingWS = resolveSessions.stream()
	    														.filter(rs -> {
	    															Pair<Boolean, String> existsAndWSNum = 
	    																					sessionWorksheetExists
	    																						(rs, rs.getUActiveType());
	    															
	    															if (existsAndWSNum.getLeft().booleanValue()) {
	    																foundWSNumStrBldr.append(existsAndWSNum.getRight());
	    																return true;
	    															} else {
	    																return false;
	    															}
	    														})
	    														.findFirst()
	    														.orElse(null);
	    				if (latestRSWithExistingWS != null && foundWSNumStrBldr.length() > 0) {
	    					resolveSessions.remove(latestRSWithExistingWS);
	    					
	    					if (!foundWSNumStrBldr.toString().equals(latestRSWithExistingWS.getWorksheetNum())) {
	    						// Update WS number of latest Resolve Session with existing Worksheet found in DB
	    						latestRSWithExistingWS.setWorksheetNum(foundWSNumStrBldr.toString());
	    						latestRSWithExistingWS.setSysModCount(Integer.valueOf(latestRSWithExistingWS.getSysModCount()
	    																			  .intValue() + 1));
	    						SaveUtil.saveResolveSession(latestRSWithExistingWS, latestRSWithExistingWS.getUActiveType());
	    					}
	    				}
	    				
	    				if (CollectionUtils.isNotEmpty(resolveSessions)) {
	    					cleanedUpCountByUserOrg.put(userOrg, Integer.valueOf(resolveSessions.size()));
	    					
							// Remove remaining Resolve Sessions from DB
							
	    					resolveSessions.parallelStream().forEach(rs -> {
	    						try {            
	    							HibernateProxy.setCurrentUser(rs.getSys_id());
	    				            HibernateProxy.executeHibernateInitLocked(() -> {
	    				            	HibernateUtil.getDAOFactory().getResolveSessionDAO().delete(rs);
	    				            });
	    				        } catch (Throwable t) {
	    				       	 	Log.log.error(String.format("Error %sin deleting Resolve Session with sys id %s " +
	    				       	 								"from resolve_session table", 
	    				       	 								(StringUtils.isNotBlank(t.getMessage()) ? 
	    				       	 								 "[" + t.getMessage() + "] " : ""), rs.getSys_id()), t);
                                    HibernateUtil.rethrowNestedTransaction(t);
                                    throw new RuntimeException(t);
	    				        }
	    					});
						}
	    			}
    			}
    		});
    	}
    	
    	int cleanedUpCount = 0;
    	
    	if (CollectionUtils.isNotEmpty(cleanedUpCountByUserOrg.values())) {
    		for(Integer userOrgCleanedUpCount : cleanedUpCountByUserOrg.values()) {
    			cleanedUpCount += userOrgCleanedUpCount.intValue();
    		};
    	}
    	
    	return cleanedUpCount;
    }
} // WorksheetUtil
