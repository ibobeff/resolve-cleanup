/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.message;

import java.io.Serializable;

import com.resolve.dto.SocialComponentType;

public class SubmitNotification implements Serializable
{
    private static final long serialVersionUID = -248546171387178774L;
    
    private UserGlobalNotificationContainerType eventType;
    private SocialComponentType compType;
    private String compSysId;
    private String compDisplayName;
    private String compReadRoles;
    private String compEditRoles;
    private String username;
    private String subject;
    private String content;
    
    private String timeZone;
    private boolean timeZoneOffset = Post.TIMEZONE_OFFSET_DEFAULT;
    
    public UserGlobalNotificationContainerType getEventType()
    {
        return eventType;
    }
    public void setEventType(UserGlobalNotificationContainerType eventType)
    {
        this.eventType = eventType;
    }
    public String getCompSysId()
    {
        return compSysId;
    }
    public void setCompSysId(String compSysId)
    {
        this.compSysId = compSysId;
    }
    
    public SocialComponentType getCompType()
    {
        return compType;
    }
    public void setCompType(SocialComponentType compType)
    {
        this.compType = compType;
    }
    public String getCompDisplayName()
    {
        return compDisplayName;
    }
    public void setCompDisplayName(String compDisplayName)
    {
        this.compDisplayName = compDisplayName;
    }
    public String getCompReadRoles()
    {
        return compReadRoles;
    }
    public void setCompReadRoles(String compReadRoles)
    {
        this.compReadRoles = compReadRoles;
    }
    
    public String getCompEditRoles()
    {
        return compEditRoles;
    }
    public void setCompEditRoles(String compEditRoles)
    {
        this.compEditRoles = compEditRoles;
    }
    public String getUsername()
    {
        return username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }
    public String getSubject()
    {
        return subject;
    }
    public void setSubject(String subject)
    {
        this.subject = subject;
    }
    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }
    public String getTimeZone()
    {
        return timeZone;
    }
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }
    public boolean isTimeZoneOffset()
    {
        return timeZoneOffset;
    }
    public void setTimeZoneOffset(boolean timeZoneOffset)
    {
        this.timeZoneOffset = timeZoneOffset;
    }
    
    @Override
    public String toString()
    {
        return "SubmitNotification [eventType=" + eventType + ", compType=" + compType + ", compSysId=" + compSysId + ", compDisplayName=" + compDisplayName + ", compReadRoles=" + compReadRoles + ", compEditRoles=" + compEditRoles + ", username=" + username + ", subject=" + subject + ", content=" + content + ", timeZone=" + timeZone + ", timeZoneOffset=" + timeZoneOffset + "]";
    }
}
