/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.Map;

public interface WSDataMap
{
    public Object get(String key);
    
    public void put(String key, Object value);
    
    public void put(String key, Object value, long interval);
    
    public void putAll(Map<String, Object> map);

}
