/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.export;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.ServiceGraph;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.StringUtils;

public abstract class ExportComponentGraph
{
    protected String sysId = null;
    protected NodeType type = null;
    protected ImpexOptionsDTO options = null;//optional

    protected List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
    
    public ExportComponentGraph(String sysId, ImpexOptionsDTO options, NodeType type) throws Exception
    {
        if(StringUtils.isEmpty(sysId) || type == null)
        {
            throw new Exception("sysId and type are mandatory.");
        }
        
        this.sysId = sysId;
        this.options = options;
        this.type = type;
    }
    
    public List<GraphRelationshipDTO> export(SocialImpexVO socialImpex) throws Exception
    {
        ResolveNodeVO compNode = validate();
        return exportRelationships(compNode, socialImpex);
    }
    
    protected abstract List<GraphRelationshipDTO> exportRelationships(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception;
    protected ResolveNodeVO validate() throws Exception
    {
        ResolveNodeVO compNode = ServiceGraph.findNode(null, sysId, null, type, "system");
        if(compNode == null)
        {
            throw new Exception(type + " with sysId " + sysId + " does not exist");
        }
        
        String sourceName = compNode.getUCompName();
        NodeType sourceType = compNode.getUType(); 
        if (sourceType != type)
        {
        	// if type is DOCUMENT, sourceType should either be SIR_PLAYBOOK or PLAYBOOK_TEMPLATE cause both are wikidocs.
        	if (type.name().equals("DOCUMENT") && !(sourceType.name().equals(NodeType.SIR_PLAYBOOK.name()) || sourceType.name().equals(NodeType.PLAYBOOK_TEMPLATE.name())))
        	{
        		throw new Exception("This node is not of a " + type + " but of " + sourceType + " with name " + sourceName);
        	}
        }
        
        return compNode;
    }
    
    protected void addRelationship(ResolveNodeVO compNode, ResolveNodeVO anyOtherNode) throws Exception
    {
        GraphRelationshipDTO relation = new GraphRelationshipDTO();
        relation.setSourceName(compNode.getUCompName());
        relation.setSourceType(compNode.getUType());
        relation.setTargetName(anyOtherNode.getUCompName());
        relation.setTargetType(anyOtherNode.getUType());
        
        relationships.add(relation);
    }

}
