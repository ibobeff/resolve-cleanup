/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceCache;
import com.resolve.services.ServiceWiki;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

/**
 * this is the server side implementation of executing the Runbook and
 * ActionTask
 * 
 * @author jeet.marwah
 * 
 */
public class ExecuteRunbook
{
    
    private static final int DEFAULT_TIMEOUT = 10;

    /**
     * This is to submit a request to abort a runbook
     * 
     * @param params
     * @return
     */
    public static String abortProcess(Map<String, String> params)
    {
        String result = "";
        
        try
        {
            if(validateAbort(params))
            {
                MainBase.esb.sendMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.abortProcess", params);
                
                String processId = params.get(Constants.EXECUTE_PROCESSID);
                String problemId = params.get(Constants.EXECUTE_PROBLEMID);
                if (StringUtils.isNotEmpty(processId))
                {
                    result = "Successfully submitted request to Abort Runbook - processid: " + processId;
                }
                else if (StringUtils.isNotEmpty(problemId))
                {
                    result = "Successfully submitted request to Abort Runbook - problemid: " + problemId;
                }
                else
                {
                    result = "FAILED: missing PROCESSID or PROBLEMID";
                }
            }
        }
        catch(Exception e)
        {
            result = "Error while submitting request to abort a runbook.";
        }
        
        return result;
    }//abortProcess
    
    
    /**
     * This is SYNCHRONOUS API for executing a RUNBOOK or an ACTIONTASK. So it will wait to fetch the results
     * 
     * Returns the output of the execution
     * 
     * @param params
     * @return
     */
    public static String execute(Map<String, Object> params)
    {
        String result = "";
        JSONObject output = new JSONObject();
        
        try
        {
            if(validate(params))
            {
                validateOrg(params);
                
                params.put(Constants.EXECUTE_PROCESSID, "CREATE");
                
                result = executeRunbook(params);
                
                if (result != null && !result.toUpperCase().contains("ERROR") && !result.toUpperCase().contains("FAILURE"))
                {                               
                    String problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
                
                    //cleanup the cache for Result and Detail macro
                    ServiceCache.removeProblemIdFromWorksheetDataCacahe(problemId);
                }
                else
                {
                    throw new Exception(result);
                }
            }
            
            /*
            else
            {
                String wikiDoc = params.get(Constants.EXECUTE_WIKI);
                if(StringUtils.isNotEmpty(wikiDoc))
                {
                    result.append(String.format("Either %s runbook does not exist, or you do not have rights to execute it", wikiDoc));
                }
                else
                {
                    result.append(String.format("No Runbook to execute."));
                }
            }*/
        }
        catch(Exception e)
        {
            output.accumulate("status", false);
            output.accumulate("error", e.getMessage());
            result = output.toString();
        }
        
        return result;
        
    } //execute
    
    public static String WebserviceStart(Map<String, Object> params)
    {
        // set EVENT_TYPE
        params.put(Constants.ESB_PARAM_EVENTTYPE, Constants.GATEWAY_EVENT_TYPE_WEBSERVICE);
        
        String result = submitExecuteRequestForRunbookByDispatcher(params);

        if (result != null && (result.toUpperCase().contains("ERROR") || result.toUpperCase().contains("FAILURE")))
        {
            throw new RuntimeException(result);
        }
        
        //cleanup the cache for Result and Detail macro
        String problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
        ServiceCache.removeProblemIdFromWorksheetDataCacahe(problemId);
        
        return result;
    } // execute
    
    
    /**
     * This is a ASYNCHRONOUS API
     * 
     * this is generic api that exeutes the Runbook or an Actiontask based on
     * the params. Returns the processId.
     * 
     * @param params
     * @return
     */
    public static String start(Map<String, Object> params)
    {
        StringBuffer result = new StringBuffer();
        String problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
        String wiki = (String)params.get(Constants.EXECUTE_WIKI);
        
        try
        {
            problemId = HttpUtil.sanitizeValue(problemId, "ResolveUUID" , 32);
            wiki = HttpUtil.sanitizeValue(wiki, "ResolveText" , 500);
            if (validate(params))
            {
                validateOrg(params);
                
                // set EVENT_TYPE
                params.put(Constants.ESB_PARAM_EVENTTYPE, Constants.GATEWAY_EVENT_TYPE_RSVIEW); 
                String processID = submitExecuteRequestForRunbook(params);
                
                if (processID != null && (processID.toUpperCase().contains("ERROR") || processID.toUpperCase().contains("FAILURE")))
                {
                    throw new Exception(processID);
                }
                
                processID = HttpUtil.sanitizeValue(processID, "ResolveUUID" , 32);

                //cleanup the cache for Result and Detail macro
                problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
                problemId = HttpUtil.sanitizeValue(problemId, "ResolveUUID" , 32);

                ServiceCache.removeProblemIdFromWorksheetDataCacahe(problemId);
                
                result.append("runbook:" + wiki).append("\n");
                result.append("worksheetId:" + problemId).append("\n");
                result.append("processId:" + processID).append("\n");
            }
            else
            {
                result.append(" Access Rights Error: Current user does not have execute rights to Wiki: " + wiki);
            }
        }
        catch(Exception e)
        {
//            result.append(EXECUTE_SUCCESS).append("=false&").append(ERROR).append("=").append(e.getMessage());
            Log.log.error("FAILURE:Unable to submit execute request", e);
            result.append("FAILURE:Unable to submit execute request. " + e.getMessage());
        }
        
        return result.toString();
    } // execute

    public static long getProcessTimeoutInSecs(String strProcessTimeout)
    {
        long timout = DEFAULT_TIMEOUT * 60;//in secs
        try
        {
            if(!StringUtils.isEmpty(strProcessTimeout))
            {
                timout = Long.parseLong(strProcessTimeout) * 60;
            }
        }
        catch(Exception e)
        {
            //nothing to do
        }
        
        return timout;
    }//getProcessTimeout
    

    
    /**
     * this API is to initialize the execute process
     * 
     * @param params
     * @param problemid
     * @param processid
     * @param userid
     * @param reference
     * @param queryString
     * @return
     * @throws Exception
     */
    public static String initExecuteProcess(Map<String, Object> params, String problemid, String processid, String userid, String reference, String alertid, String queryString) throws Exception
    {
        queryString = ServiceWorksheet.initExecuteProcess(params, problemid, processid, userid, reference, alertid, queryString);

        //cleanup the cache
        problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);
        ServiceCache.removeProblemIdFromWorksheetDataCacahe(problemid);
        
        return queryString;
    } // initExecuteProcess
    
    @SuppressWarnings("unused")
    private static String submitExecuteRequestForRunbook(Map<String, Object> params)
    {
        String result = null;
        String action = (String) params.get(Constants.HTTP_REQUEST_ACTION);
        String redirect = (String) params.get(Constants.HTTP_REQUEST_REDIRECT);
        String debug = (String) params.get(Constants.HTTP_REQUEST_DEBUG);
        String token = (String) params.get(Constants.HTTP_REQUEST_TOKEN);

        // parameters
        String problemid                = (String) params.get(Constants.EXECUTE_PROBLEMID);                  // required
        String userid                   = (String) params.get(Constants.EXECUTE_USERID);                     // required
        String wiki                     = (String) params.get(Constants.EXECUTE_WIKI);                       // required
        String processid                = (String) params.get(Constants.EXECUTE_PROCESSID);                  // optional
        String reference                = (String) params.get(Constants.EXECUTE_REFERENCE);                  // optional
        String alertid                  = (String) params.get(Constants.EXECUTE_ALERTID);                    // optional
        String guid                     = (String) params.get(Constants.EXECUTE_GUID);                       // optional
        
        // start parameters
        String processTimeoutStr        = "10";
        if(params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
            processTimeoutStr = (String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT); // optional
        } else if (params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
            processTimeoutStr = "" + params.get(Constants.EXECUTE_PROCESS_TIMEOUT);
        }
        String processLoop              = (String) params.get(Constants.EXECUTE_PROCESS_LOOP);               // optional
        String processDebug             = (String) params.get(Constants.EXECUTE_PROCESS_DEBUG);              // optional
        String processConcurrentLimit   = (String) params.get(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT);   // optional
        
        
        String queryString =  (String) params.get(Constants.HTTP_REQUEST_QUERY);
        String userQueryString = (String) params.get(Constants.HTTP_REQUEST_QUERYSTRING);
        
        // init
        boolean sendRequest = false;

        // FIX - validate user permissions
        try
        {
            // set params and redirect querystring
            initExecuteProcess(params, problemid, processid, userid, reference, alertid, queryString);
            result = (String) params.get(Constants.EXECUTE_PROCESSID);

            // redirect to wiki with no parameter checks
            if (!StringUtils.isEmpty(wiki))
            {
                sendRequest = true;
            }

            // send request
            if (sendRequest)
            {
                // runbook
                if (StringUtils.isEmpty(action) || action.equalsIgnoreCase("EXECUTEPROCESS") || action.equalsIgnoreCase("PROCESS"))
                {
                    Log.log.trace("ExecuteProcess params: "+params);
                    MainBase.esb.sendMessage(Constants.ESB_NAME_RSCONTROL, "MAction.executeProcess", params);

                    // increase the execute counter for this runbook
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            result = e.getMessage();
        }

        return result;
    }//submitExecuteRequestForRunbook
   
    @SuppressWarnings("unused")
    private static String submitExecuteRequestForRunbookByDispatcher(Map<String, Object> params)
    {
        String result = null;
        String action = (String) params.get(Constants.HTTP_REQUEST_ACTION);
        String redirect = (String) params.get(Constants.HTTP_REQUEST_REDIRECT);
        String debug = (String) params.get(Constants.HTTP_REQUEST_DEBUG);
        String token = (String) params.get(Constants.HTTP_REQUEST_TOKEN);

        // parameters
        String problemid                = (String) params.get(Constants.EXECUTE_PROBLEMID);                  // required
        String userid                   = (String) params.get(Constants.EXECUTE_USERID);                     // required
        String wiki                     = (String) params.get(Constants.EXECUTE_WIKI);                       // required
        String processid                = (String) params.get(Constants.EXECUTE_PROCESSID);                  // optional
        String reference                = (String) params.get(Constants.EXECUTE_REFERENCE);                  // optional
        String alertid                  = (String) params.get(Constants.EXECUTE_ALERTID);                    // optional
        String guid                     = (String) params.get(Constants.EXECUTE_GUID);                       // optional
        
        // start parameters
        String processTimeoutStr        = "10";
        if(params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
            processTimeoutStr = (String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT); // optional
        } else if (params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
            processTimeoutStr = "" + params.get(Constants.EXECUTE_PROCESS_TIMEOUT);
        }
        String processLoop              = (String) params.get(Constants.EXECUTE_PROCESS_LOOP);               // optional
        String processDebug             = (String) params.get(Constants.EXECUTE_PROCESS_DEBUG);              // optional
        String processConcurrentLimit   = (String) params.get(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT);   // optional
        
        String queryString =  (String) params.get(Constants.HTTP_REQUEST_QUERY);
        String userQueryString = (String) params.get(Constants.HTTP_REQUEST_QUERYSTRING);
        
        // init
        boolean sendRequest = false;

        // FIX - validate user permissions
        try
        {
            // set params and redirect querystring
            initExecuteProcess(params, problemid, processid, userid, reference, alertid, queryString);
            result = (String) params.get(Constants.EXECUTE_PROCESSID);

            // redirect to wiki with no parameter checks
            if (!StringUtils.isEmpty(wiki))
            {
                sendRequest = true;
            }

            // send request
            if (sendRequest)
            {
                // runbook
                if (StringUtils.isEmpty(action) || action.equalsIgnoreCase("EXECUTEPROCESS") || action.equalsIgnoreCase("PROCESS"))
                {
                    MessageDispatcher.sendMessage(params);
                    
                    // increase the execute counter for this runbook
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            result = e.getMessage();
        }

        return result;
    }//submitExecuteRequestForRunbookByDispatcher
    
    
    @SuppressWarnings("static-access")
    private static String executeRunbook(Map<String, Object> params)
    {
        String state = null;
        int maxWaitTime = 1;

        long processTimeoutInSecs = getProcessTimeoutInSecs((String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT)); 
        //System.out.println("Will wait for (secs):" + processTimeoutInSecs);

        // set EVENT_TYPE
        params.put(Constants.ESB_PARAM_EVENTTYPE, Constants.GATEWAY_EVENT_TYPE_RSVIEW);
        
        //submit the request to execute
        String processId = submitExecuteRequestForRunbook(params);
        
        if (processId == null || processId.length() != 32 || processId.contains(" "))
        {
            Log.log.error("FAILURE:Unable to submit execute request. " + processId);
            return "FAILURE:Unable to submit execute request. " + processId;
        }
        
        String problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
        
        //wait till you get the results as this SYNCHRONOUS
        while (StringUtils.isEmpty(state) || "OPEN".equals(state))
        {
            // Bail out after timeout and if runbook is still not finished or aborted.
            if (maxWaitTime++ > processTimeoutInSecs)
            {
                throw new RuntimeException("Timed out getting status for processID :" + processId);
            }
            
            //System.out.println("Counting secs :" + maxWaitTime);
            try
            {
                // wait for runbook to finish
                // Sleep for one second to
                Thread.currentThread().sleep(1000); 
            }
            catch (Exception ex)
            {
                Log.log.info("Exception is thrown when waiting for runbook to finish", ex);
                throw new RuntimeException(ex);
            }
            
            state = ServiceWorksheet.getProcessStatusForRunbook(processId);
        }//end of while loop
        
        params.put("Status", state);
        
        //if the DETAIL flag is set, then prepare the DETAIL result, else return the SUMMARY
        boolean isDetail = StringUtils.isEmpty((String) params.get(Constants.DETAIL_OUTPUT_FLAG)) ? false : true ;
        boolean isWSDATA = StringUtils.isEmpty((String) params.get(Constants.WSDATA_FLAG)) ? false : true ;
        String actionTaskDetailOutput = (String) params.get(Constants.ACTIONTASK_DETAIL_OUTPUT);
        String username = (String) params.get(Constants.EXECUTE_USERID);
        
        int timeout = 100;
        try {
            String timedout = (String)params.get(Constants.EXECUTE_TIMEOUT);
            timeout = ((new Integer(timedout))).intValue();
        } catch(Exception e) {}

        String result = "";
        
        if(StringUtils.isEmpty(actionTaskDetailOutput))
            result = WorksheetUtil.getRunbookResult(problemId, processId, username, isDetail, isWSDATA, timeout, (String)params.get(Constants.EXECUTE_ORG_ID), (String)params.get(Constants.EXECUTE_ORG_NAME));

        else
        {
            // syntax = task#namespace(wiki::nodeId) ==> Similar to DetailMacro.java
            // result = ExecuteUtil.getActionTaskDetail(problemId, actionTaskDetailOutput);
            result = WorksheetUtil.getActionTaskResult(problemId, processId, actionTaskDetailOutput, username, timeout, (String)params.get(Constants.EXECUTE_ORG_ID), (String)params.get(Constants.EXECUTE_ORG_NAME));
        }
        
        return result;
        
    } // executeRunbook
    
    private static boolean validate(Map<String, Object> params) throws Exception
    {
        boolean validate = false;
        
        //validate the user that it has access rights to execute this runbook
        String user = (String) params.get(Constants.EXECUTE_USERID);
        String docFullName =  (String) params.get(Constants.EXECUTE_WIKI);
        if(StringUtils.isNotEmpty(docFullName))
        {
            if(WikiUtils.isWikidocExist(docFullName, false))
            {
                validate = ServiceWiki.hasRightsToDocument(docFullName, user, RightTypeEnum.execute);//Wiki.getInstance().getWikiRightService().checkAccess(EXECUTE_ACTION, docFullName, user);
            }
            else
            {
                throw new Exception(String.format("The Wiki document %s doesn't exist.", docFullName));
            }
        }
        else
        {
            throw new Exception("Must provide a Wiki document.");
        }
        
        return validate;
    }
    
    private static void validateOrg(Map<String, Object> params) throws Exception
    {
        String orgName = (String) params.get(Constants.EXECUTE_ORG_NAME); // optional
        String orgId = (String) params.get(Constants.EXECUTE_ORG_ID); // optional
        
        if ((StringUtils.isNotBlank(orgName) && orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME)) ||
            (StringUtils.isNotBlank(orgId) && orgId.equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            params.remove(Constants.EXECUTE_ORG_NAME);
            params.remove(Constants.EXECUTE_ORG_ID);
            
            orgName = (String) params.get(Constants.EXECUTE_ORG_NAME); // optional
            orgId = (String) params.get(Constants.EXECUTE_ORG_ID); // optional
        }
            
        if (StringUtils.isNotBlank(orgName) && StringUtils.isBlank(orgId))
        {
            // Identify org id based on org name
            
            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
            
            if (orgsVO != null)
            {
                params.put(Constants.EXECUTE_ORG_ID, orgsVO.getSys_id());
                orgId = (String) params.get(Constants.EXECUTE_ORG_ID);
            }
        }
        
        if ((StringUtils.isNotBlank(orgName) && StringUtils.isBlank(orgId)) ||
            StringUtils.isNotBlank(orgId))
        {
            // Validate Org Id either found from name or specified directly
            
            if (StringUtils.isBlank(orgId) || (OrgsUtil.findOrgById(orgId) == null))
            {
                throw new Exception ("Invalid " + (StringUtils.isNotBlank(orgName) ? 
                                     "Org Name " + orgName : 
                                     "Org Id " + orgId) + " specified in parameters passed for execution.");
            }
        }
    }//validateOrg
    
    private static boolean validateAbort(Map<String, String> params) throws Exception
    {
        boolean validate = true;
        String processId = params.get(Constants.EXECUTE_PROCESSID);
        String problemId = params.get(Constants.EXECUTE_PROBLEMID);
        
        if(StringUtils.isEmpty(processId) && StringUtils.isEmpty(problemId))
        {
            throw new Exception("FAILED: PROCESSID or PROBLEMDID parameter is required to abort a runbook");
        }
        
        return validate;
    }//validate
    
}// RSExecute
