/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.services;

import java.util.List;

import com.resolve.services.hibernate.vo.RBConditionVO;
import com.resolve.services.hibernate.vo.RBConnectionVO;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.RBIfVO;
import com.resolve.services.hibernate.vo.RBLoopVO;
import com.resolve.services.hibernate.vo.RBTaskVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.rb.util.ABProcessConst;
import com.resolve.services.rb.util.ResolutionBuilderUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class ServiceResolutionBuilder
{
    public static List<RBGeneralVO> listGeneral(QueryDTO query, String username)
    {
        return ResolutionBuilderUtils.listGeneral(query, username);
    }

    public static RBGeneralVO getGeneral(String sysId, String username)
    {
        return ResolutionBuilderUtils.getGeneral(sysId, username);
    }

    public static RBGeneralVO saveGeneral(RBGeneralVO vo, String username) throws Exception
    {
        if (vo != null && StringUtils.isBlank(vo.getSys_id()))
        {
            return ResolutionBuilderUtils.createGeneral(vo, true, username);
        }
        else
        {
            return ResolutionBuilderUtils.updateGeneral(vo, username, true);
        }
    }

    public static boolean deleteGeneral(String id, String username) throws Exception
    {
        return deleteGeneral(id, true, username);
    }

    public static boolean deleteGeneral(String id, boolean deleteWiki, String username) throws Exception
    {
        return ResolutionBuilderUtils.deleteGeneral(id, deleteWiki, username);
    }

    public static RBGeneralVO rename(String resolutionBuilderId, String newNamespace, String newName, String username) throws Exception
    {
        return ResolutionBuilderUtils.rename(resolutionBuilderId, newNamespace, newName, username);
    }
    
    public static RBLoopVO getLoop(String sysId, String username)
    {
        return ResolutionBuilderUtils.getLoop(sysId, username);
    }

    public static RBLoopVO saveLoop(RBLoopVO vo, String username) throws Exception
    {
        if (StringUtils.isBlank(vo.getSys_id()))
        {
            return ResolutionBuilderUtils.createLoop(vo, username);
        }
        else
        {
            return ResolutionBuilderUtils.updateLoop(vo, username);
        }
    }

    public static boolean deleteLoop(String id, String username)
    {
        return ResolutionBuilderUtils.deleteLoop(id, username);
    }

    public static RBConnectionVO getConnection(String sysId, String username)
    {
        return ResolutionBuilderUtils.getConnection(sysId, username);
    }

    public static RBConnectionVO saveConnection(RBConnectionVO vo, String username) throws Exception
    {
        if (StringUtils.isBlank(vo.getSys_id()))
        {
            return ResolutionBuilderUtils.createConnection(vo, username);
        }
        else
        {
            return ResolutionBuilderUtils.updateConnection(vo, username);
        }
    }

    public static boolean deleteConnection(String id, String username) throws Exception
    {
        return ResolutionBuilderUtils.deleteConnection(id, username);
    }

    public static RBIfVO getIf(String sysId, String username)
    {
        return ResolutionBuilderUtils.getIf(sysId, username);
    }

    public static RBIfVO saveLoop(RBIfVO vo, String username) throws Exception
    {
        if (StringUtils.isBlank(vo.getSys_id()))
        {
            return ResolutionBuilderUtils.createIf(vo, username);
        }
        else
        {
            return ResolutionBuilderUtils.updateIf(vo, username);
        }
    }

    public static boolean deleteIf(String id, String username) throws Exception
    {
        return ResolutionBuilderUtils.deleteIf(id, username);
    }

    public static RBConditionVO getCondition(String sysId, String username)
    {
        return ResolutionBuilderUtils.getCondition(sysId, username);
    }

    public static RBConditionVO saveCondition(RBConditionVO vo, String username) throws Exception
    {
        if (StringUtils.isBlank(vo.getSys_id()))
        {
            return ResolutionBuilderUtils.createCondition(vo, username);
        }
        else
        {
            return ResolutionBuilderUtils.updateCondition(vo, username);
        }
    }

    public static boolean deleteCondition(String id, String username)
    {
        return ResolutionBuilderUtils.deleteCondition(id, username);
    }

    public static RBTaskVO getTask(String sysId, String username)
    {
        return ResolutionBuilderUtils.getTask(sysId, username);
    }

    public static RBTaskVO saveTask(RBTaskVO vo, String username) throws Exception
    {
        if (StringUtils.isBlank(vo.getSys_id()))
        {
            return ResolutionBuilderUtils.createTask(vo, username);
        }
        else
        {
            return ResolutionBuilderUtils.updateTask(vo, username);
        }
    }

    public static boolean deleteTask(String id, String username)
    {
        return ResolutionBuilderUtils.deleteTask(id, username);
    }

    public static Object getRBTree(String id, String username)
    {
        return ResolutionBuilderUtils.getRBTree(id, username);
    }
    
    public static JSONObject pollABGeneration(String username) throws Exception {
        return ResolutionBuilderUtils.pollABGeneration(username);
    }
    
    public static ABProcessConst generate(String id, String username) throws Exception
    {
        return ResolutionBuilderUtils.generate(id, username);
    }
    
    public static JSONObject pollParserCodeTest(String username) throws Exception {
        return ResolutionBuilderUtils.pollParserTest(username);
    }

    public static ABProcessConst testCode(String groovyScript, String sampleData, String username) throws Exception
    {
        return ResolutionBuilderUtils.testCode(groovyScript, sampleData, username);
    }

    public static List<ResolveActionTaskVO> listActionTask(String wikiName, String username) throws Exception
    {
        return ResolutionBuilderUtils.listActionTask(wikiName, username);
    }
    
    public static String getParserAutoGenCode(String jsonProperty) {
        return ResolutionBuilderUtils.generateParserCode(jsonProperty);
    }
}
