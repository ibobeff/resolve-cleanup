/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.interfaces;

import java.util.Map;


public class SocialDTO extends DTO implements Comparable<SocialDTO>
{
    private static final long serialVersionUID = 3353874868845204417L;
    
    private String u_display_name;
    private String u_owner;
    private String u_user_display_name;
    private Boolean u_is_private;
    private String u_description;
    private String u_module;
    // private Boolean u_is_active; Using u_is_locked flag instead.
    private String u_read_roles;
    private String u_edit_roles;
    private String u_post_roles;
    private String u_im;
    private String u_im_pwd;
    private String u_im_display_name;
    private String u_email;
    private String u_send_to;
    private String u_receive_from;
    private String u_receive_pwd;
    private String u_email_display_name;
    
    private String compType;
    
    private Boolean u_editable;
    private Boolean u_is_locked;

    public SocialDTO() {}
    public SocialDTO(Map<String, Object> data)
    {
        super(data);
    }
    
    public String getU_display_name()
    {
        return u_display_name;
    }
    public void setU_display_name(String u_display_name)
    {
        this.u_display_name = u_display_name;
    }
    public String getU_owner()
    {
        return u_owner;
    }
    public void setU_owner(String u_owner)
    {
        this.u_owner = u_owner;
    }
    public String getU_user_display_name()
    {
        return u_user_display_name;
    }
    public void setU_user_display_name(String u_user_display_name)
    {
        this.u_user_display_name = u_user_display_name;
    }
    public String getU_description()
    {
        return u_description;
    }
    public void setU_description(String u_description)
    {
        this.u_description = u_description;
    }
    public String getU_module()
    {
        return u_module;
    }
    public void setU_module(String u_module)
    {
        this.u_module = u_module;
    }
    
//    public Boolean getU_is_active()
//    {
//        return u_is_active;
//    }
//    public void setU_is_active(Boolean u_is_active)
//    {
//        this.u_is_active = u_is_active;
//    }
    
    public String getU_read_roles()
    {
        return u_read_roles;
    }
    public void setU_read_roles(String u_read_roles)
    {
        this.u_read_roles = u_read_roles;
    }
    public String getU_edit_roles()
    {
        return u_edit_roles;
    }
    public void setU_edit_roles(String u_edit_roles)
    {
        this.u_edit_roles = u_edit_roles;
    }
    public String getU_im()
    {
        return u_im;
    }
    public void setU_im(String u_im)
    {
        this.u_im = u_im;
    }
    public String getU_im_pwd()
    {
        return u_im_pwd;
    }
    public void setU_im_pwd(String u_im_pwd)
    {
        this.u_im_pwd = u_im_pwd;
    }
    public String getU_im_display_name()
    {
        return u_im_display_name;
    }
    public void setU_im_display_name(String u_im_display_name)
    {
        this.u_im_display_name = u_im_display_name;
    }
    public String getU_email()
    {
        return u_email;
    }
    public void setU_email(String u_email)
    {
        this.u_email = u_email;
    }
    public String getU_send_to()
    {
        return u_send_to;
    }
    public void setU_send_to(String u_send_to)
    {
        this.u_send_to = u_send_to;
    }
    public String getU_receive_from()
    {
        return u_receive_from;
    }
    public void setU_receive_from(String u_receive_from)
    {
        this.u_receive_from = u_receive_from;
    }

    public String getCompType()
    {
        return compType;
    }
    public void setCompType(String compType)
    {
        this.compType = compType;
    }
    
    public String getU_receive_pwd()
    {
        return u_receive_pwd;
    }
    public void setU_receive_pwd(String u_receive_pwd)
    {
        this.u_receive_pwd = u_receive_pwd;
    }
    public String getU_email_display_name()
    {
        return u_email_display_name;
    }
    public void setU_email_display_name(String u_email_display_name)
    {
        this.u_email_display_name = u_email_display_name;
    }
    public Boolean getU_editable()
    {
        return u_editable;
    }
    public void setU_editable(Boolean u_editable)
    {
        this.u_editable = u_editable;
    }
    public Boolean getU_is_private()
    {
        return u_is_private;
    }
    public void setU_is_private(Boolean u_is_private)
    {
        this.u_is_private = u_is_private;
    }
    public String getU_post_roles()
    {
        return u_post_roles;
    }
    public void setU_post_roles(String u_post_roles)
    {
        this.u_post_roles = u_post_roles;
    }
    public Boolean getU_is_locked()
    {
        return u_is_locked;
    }
    public void setU_is_locked(Boolean u_is_locked)
    {
        this.u_is_locked = u_is_locked;
    }
    
    @Override
    public int hashCode()
    {
        int hashcode = 1;
        hashcode = 31 * hashcode + ((u_display_name == null) ? 0 : u_display_name.hashCode());
        return hashcode;
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = true;

        if (this == obj)
        {
            result = true;
        }

        if (obj == null)
        {
            result = false;
        }

        if (getClass() != obj.getClass())
        {
            result = false;
        }

        SocialDTO socialDTO = (SocialDTO) obj;

        if (u_display_name == null)
        {
            if (socialDTO.getU_display_name() != null)
            {
                result = false;
            }
        }
        else if (!u_display_name.equals(socialDTO.getU_display_name()))
        {
            result = false;
        }

        return result;
    }
    
    public int compareTo(SocialDTO o)
    {
        return getU_display_name().compareTo(o.getU_display_name());
    }
}
