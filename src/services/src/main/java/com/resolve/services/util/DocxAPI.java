package com.resolve.services.util;

import com.resolve.util.Log;

public class DocxAPI
{
    
    public DocxAPI( ) {
        
    }
    
    /** 
     * Allows the user to convert a docx file to wiki markup, extract all embedded images, and attach them to a given wiki ID 
     */
    
    public static String convertDocxFileToWiki(
                    String docxSourcePath, 
                    String wikiID,
                    String wikiName, 
                    String userName
                    ) throws Exception
    {
        
        DocxUtil d = new DocxUtil();
        return d.convertDocxToWiki(docxSourcePath, wikiID, wikiName, userName);
        
    }
    
    /**
     * Allows the users to convert a docx document into a Resolve Systems IMPEX module.
     *
     * @param sourcePath : the absolute path to the docx source file.
     * @param nameSpace - the name of the nameSpace of the IMPEX runbook. (e.g. nameSpace.RunbookName)
     * @param runbookName - the name of the runbook of the IMPEX runbook. (e.g. nameSpace.RunbookName)
     * @param outputPath - the absolute path to the destination impex zip file. (File does not need to exist)
     * @return True if successful, false otherwise
     */
    
    public static boolean convertDocxFileToImpex(
                    String sourcePath, 
                    String nameSpace, 
                    String runbookName, 
                    String outputPath
                    ) {
       try {
           DocxUtil d = new DocxUtil();
        return d.exportDocxToImpex(
                        sourcePath, 
                        nameSpace, 
                        runbookName, 
                        outputPath
                        );
       } catch (Exception e) {
           System.out.println("****** CONVERSION ERROR ********");
           System.out.println("An error occured while converting the document at: " + sourcePath);
           e.printStackTrace();
           Log.log.error(e.getMessage(), e);
           
       } 
       
       return false;
       
    }
    
    /**
     * Allows the users to convert a directory of doc files into Resolve Systems IMPEX modules. NOTE: the
     * IMPEX runbook names will be whatever the filename is on the source path.
     *
     * @param sourcePath - the absolute path to the docx source file.
     * @param nameSpace - the name of the nameSpace of the IMPEX runbook. (e.g. nameSpace.RunbookName)
     * @param outputPath - the absolute path to the destination impex zip file. (File does not need to exist)
     * @return True if successful, false otherwise
     */
    
    public static boolean convertDocxDirectoryToImpex(
                    String sourcePath, 
                    String nameSpace,
                    String outputPath
                    ) {
        try {
            DocxUtil d = new DocxUtil();
            return d.exportDirectoryToImpex(
                        sourcePath, 
                        nameSpace, 
                        outputPath
                        );
        } catch (Exception e) {
            System.out.println("****** CONVERSION ERROR ********");
            System.out.println("An error occured while converting the document at: " + sourcePath);
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
        } 
        
        return false;
    }
}
