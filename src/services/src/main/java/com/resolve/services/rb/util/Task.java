/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import com.resolve.util.StringUtils;

public class Task extends Common implements Comparable<Common>
{
    private String name;
    private String module;
    private String fullName;
    private String tooltip = "undefined";
    private String detail="";
    private String href;
    private boolean isRunbookRef = false;
    
    private static final int height = 40;
    public static final int width = 100;
    
    private Cell cell;
    private Params params;
    private String inputs;
    
    public Task(int id, String name, String module, int x, int y, String description, String inputs, String outputs, String merge, boolean isRunbookRef)
    {
        super(id, name, description, x, y, width, height, merge);
        
        this.name = name;
        this.module = module;
        this.fullName = name + "#" + module;
        this.href = fullName;
        this.isRunbookRef = isRunbookRef;
        
        Geometry geometry = new Geometry(x, y, width, height, 0, "geometry");
        cell = new Cell(getStyle(), 1, -1, -1, -1, 1, geometry);
        this.inputs = inputs;
        params = new Params(inputs, outputs);
    }
    
    public String getMerge()
    {
        return merge;
    }

    public void setMerge(String merge)
    {
        this.merge = merge;
    }

    public int getHeight()
    {
        return height;
    }

    @Override
    String getStyle()
    {
        String style = isRunbookRef ? 
                       "labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#ADD8E6;gradientColor=white" : 
                       "rounded;labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#D3D3D3;gradientColor=white;verticalLabelPosition=middle;verticalAlign=middle";
        return style;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        String startTag = isRunbookRef ? "    <Subprocess " : "    <Task ";
        sb.append(startTag);
        sb.append("label=\"").append(getNodeLabel()).append("\"")
        .append(" description=\"").append(getDescription()).append("\"");
        if(!isRunbookRef)
        {
            sb.append(" tooltip=\"").append(tooltip).append("\"")
            .append(" href=\"").append(href).append("\"")
            .append(" detail=\"").append(detail).append("\"");
            
            if(StringUtils.isNotBlank(merge))
            {
                sb.append(" merge=\"merge = ").append(merge).append("\"");
            }
        }
        sb.append(" id=\"").append(getId()).append("\">\n");
//        sb.append("      <name>").append(fullName).append("</name>\n");
        sb.append(cell.toString()).append("\n");
//        again.. this is temporary... we will have an runbook vo...
        if(isRunbookRef){
            sb.append("      <params>\n");
            sb.append("        <inputs><![CDATA[").append(StringUtils.isEmpty(this.inputs)?"{}":this.inputs).append("]]>").append("</inputs>");
            sb.append("     </params>\n");    
        }else
        sb.append(params.toString()).append("\n");
        String endTag = isRunbookRef ? "    </Subprocess>" : "    </Task>";
        sb.append(endTag);
        return sb.toString();
    }

    public int compareTo(Common o)
    {
        return this.getId().compareTo(o.getId());
    }
}
