/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

public class RsFormProperties
{
    private Integer labelWidth;
    private String labelAlign;
    
    

    public String getLabelAlign()
    {
        return labelAlign;
    }

    public void setLabelAlign(String labelAlign)
    {
        this.labelAlign = labelAlign;
    }

    public Integer getLabelWidth()
    {
        return labelWidth;
    }

    public void setLabelWidth(Integer labelWidth)
    {
        this.labelWidth = labelWidth;
    }
    
    
    

}
