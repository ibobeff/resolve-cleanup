/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.social;

import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;
import com.resolve.util.Log;


public class ExportUserGraph extends ExportComponentGraph
{
    public ExportUserGraph(String sysId) throws Exception
    {
       super(sysId, SocialRelationshipTypes.USER);
    }
    
    private void buildRelationship(Node srcNode, Node destNode, RelationshipType relationshipType) throws Exception
    {
        try
        {
            if(destNode.hasProperty(GraphDBManager.TYPE))
            {
    //            String otherSysId = (String) othernode.getProperty(GraphDBManager.SYS_ID);
    //            String otherName = (String) othernode.getProperty(GraphDBManager.DISPLAYNAME);
    //            String type = (String) othernode.getProperty(GraphDBManager.TYPE);
                
                addRelationship(srcNode, destNode, relationshipType != null ? relationshipType.name() : null);
            }
            else
            {
                Log.log.error("Failed to add " + (relationshipType != null ? relationshipType.name() : "null") + 
                              " relationship between source Node (" + 
                              (srcNode.getProperty(GraphDBManager.SYS_ID) != null ? srcNode.getProperty(GraphDBManager.SYS_ID) : "null" ) + ", " + 
                              (srcNode.getProperty(GraphDBManager.DISPLAYNAME) != null ? srcNode.getProperty(GraphDBManager.DISPLAYNAME) : "null") + ", " + 
                              (srcNode.getProperty(GraphDBManager.TYPE) != null ? srcNode.getProperty(GraphDBManager.TYPE) : "null") + 
                              ") and destination Node(" +
                              (destNode.getProperty(GraphDBManager.SYS_ID) != null ? destNode.getProperty(GraphDBManager.SYS_ID) : "null") + ", " + 
                              (destNode.getProperty(GraphDBManager.DISPLAYNAME) != null ? destNode.getProperty(GraphDBManager.DISPLAYNAME) : "null") + 
                              ") because destination node is missing " + GraphDBManager.TYPE + " property");
            }
        }
        catch(Exception e)
        {
            Log.log.error("Failed to add " + (relationshipType != null ? relationshipType.name() : "null") +
                          " relationship between source Node (" + 
                          (srcNode.getProperty(GraphDBManager.SYS_ID) != null ? srcNode.getProperty(GraphDBManager.SYS_ID) : "null" ) + ", " + 
                          (srcNode.getProperty(GraphDBManager.DISPLAYNAME) != null ? srcNode.getProperty(GraphDBManager.DISPLAYNAME) : "null") + ", " + 
                          (srcNode.getProperty(GraphDBManager.TYPE) != null ? srcNode.getProperty(GraphDBManager.TYPE) : "null") + 
                          ") and destination Node(" +
                          (destNode.getProperty(GraphDBManager.SYS_ID) != null ? destNode.getProperty(GraphDBManager.SYS_ID) : "null")  + ", " +
                          (destNode.getProperty(GraphDBManager.DISPLAYNAME) != null ? destNode.getProperty(GraphDBManager.DISPLAYNAME) : "null") + ", " +
                          (destNode.getProperty(GraphDBManager.TYPE) != null ? destNode.getProperty(GraphDBManager.TYPE) : "null") + ")", e);
            throw e;
        }
    }
        
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode) throws Exception
    {
        //get all the outgoing rels for the user 
        Iterable<Relationship> relso = compNode.getRelationships(Direction.OUTGOING);
        
        Log.log.debug("Exporting outgoing relationships for User " + compNode.getProperty(GraphDBManager.DISPLAYNAME) + "...");
        
        Node srcNode, destNode;
        int relProcessedCount = 0;
        
        for(Relationship rel : relso)
        {
            srcNode = compNode;
            destNode = rel.getOtherNode(srcNode);
            
            buildRelationship(srcNode, destNode, rel.getType());
            relProcessedCount++;
        }
        
        Log.log.debug("Exported " + relProcessedCount + " outgoing relationships for User " + 
                      compNode.getProperty(GraphDBManager.DISPLAYNAME)/* + ", exporting incoming relationships..."*/);
        /*
        //get all the incoming rels for the user
        
        Iterable<Relationship> relsi = compNode.getRelationships(Direction.INCOMING);
        
        relProcessedCount = 0;
        for(Relationship rel : relsi)
        {
            srcNode = rel.getOtherNode(compNode);
            destNode = compNode;
            
            try
            {
                buildRelationship(srcNode, destNode, rel.getType());
                relProcessedCount++;
            }
            catch(Exception e)
            {
                Log.log.error("Error in exporting incoming User relationship for User " + compNode.getProperty(GraphDBManager.DISPLAYNAME) , e);
            }
        }
        
        Log.log.debug("Exported " + relProcessedCount + " incoming relationships for User " + 
                        compNode.getProperty(GraphDBManager.DISPLAYNAME));
        
        Log.log.debug("Exported total " + relationships.size() + " relationships for User " + 
                        compNode.getProperty(GraphDBManager.DISPLAYNAME));
        */
        return relationships;
    }
}
