package com.resolve.services.task.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.resolve.services.rb.util.ABXmlNodeExtractor;
import com.resolve.services.rb.util.XPathVariable;
import com.resolve.util.StringUtils;

public class XPathCodeGen
{
    private ABXmlNodeExtractor extractor;
    public XPathCodeGen(JsonNode jsonReqParams) throws JsonParseException, JsonMappingException, IOException
    {
        if(jsonReqParams.findValue("parserConfig").isTextual()) {
            this.extractor = ABXmlNodeExtractor.getInstance(jsonReqParams.get("parserConfig").asText());
        }
        else {          
            this.extractor = ABXmlNodeExtractor.getInstance(jsonReqParams.get("parserConfig").toString());
        }              
        this.buildParserTmpl();
    }
    private String parserTmpl;
    private void buildParserTmpl() {
        List<String> list = new ArrayList<String>();
        list.add("import org.dom4j.Document");
        list.add("import org.dom4j.DocumentHelper");
        list.add("import org.dom4j.Element");
        list.add("import org.dom4j.Node");
        list.add("//Remove XML declaration");
        list.add("RAW = RAW.replaceAll('\\\\<\\\\?xml(.+?)\\\\?\\\\>', '')");
        list.add("Document document = DocumentHelper.parseText('<resolveroot>' + RAW + '</resolveroot>')");
        list.add("Element root = null");
        list.add("int count = 0");
        list.add("List augmentedList = document.selectNodes('/resolveroot/node()')");
        list.add("augmentedList.each{");
        list.add("  Node node->");
        list.add("  if(node.nodeType==1){");
        list.add("      root = node");
        list.add("      count++");
        list.add("  }");
        list.add("}");
        list.add("if(count>1)");
        list.add("  throw new Exception('Multiple xml section!')");
        list.add("List list = []");
        list.add("DATA = [:]");
        list.add("%1s");
        list.add("DATA");
        this.parserTmpl = StringUtils.join(list, "\n");
    }
    
    public String generateParserCode() {
        List<XPathVariable> variables = this.extractor.getVariables();
        List<String> list = new ArrayList<String>();
        for(int i = 0;i<variables.size();i++) {
            XPathVariable current = variables.get(i);
            list.add(String.format("list = root.selectNodes(\"\"\"%1s\"\"\")", current.getXpath()));
            list.add("if(null!=list&&!list.isEmpty()){");
            list.add(String.format("    DATA[\"%1s\"] = list.get(0).getStringValue()", current.getVariable()));
            if(current.getFlow())
                list.add(String.format("    FLOWS[\"%1s\"] = DATA[\"%1s\"]", current.getVariable(),current.getVariable()));
            if(current.getWsdata()) {
                list.add(String.format("    WSDATA[\"%1s\"] = DATA[\"%1s\"]", current.getVariable(),current.getVariable()));
            }
            if(current.getOutput()) {
                list.add(String.format("    OUTPUTS[\"%1s\"] = DATA[\"%1s\"]", current.getVariable(),current.getVariable()));
            }
            list.add("}");
                
        }
        return String.format(this.parserTmpl,StringUtils.join(list, "\n"));
    }
}
