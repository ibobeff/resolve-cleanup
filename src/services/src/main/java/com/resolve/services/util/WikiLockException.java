package com.resolve.services.util;

public class WikiLockException extends RuntimeException
{

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    String username;
    public WikiLockException(String username,String message)
    {
        super(message);
        this.username = username;
        // TODO Auto-generated constructor stub
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
