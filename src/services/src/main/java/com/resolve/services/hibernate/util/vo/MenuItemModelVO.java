/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util.vo;

import java.util.List;
import java.util.Map;

public class MenuItemModelVO
{
    private String order;
    private String name;
    private String menuDefGroup;
    private Boolean active;
    private String query;
    private String sys_ID;
    private String parentID;
    private List<Map<String, String>> menuItemRoles;
    private List<Map<String, String>> availableRolesList;
    private MenuDefinitionModelVO menuDefinition;
    
    public MenuItemModelVO()
    {
    }
    
    public MenuItemModelVO(String name, String group, boolean active, String query, String ID)
    {
        this.setName(name);
        this.setMenuDefGroup(group);
        this.setActive(active);
        this.setQuery(query);
        this.setSys_ID(ID);
    }
    
    public String getOrder()
    {
        return order;
    }
    public void setOrder(String order)
    {
        this.order = order;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getMenuDefGroup()
    {
        return menuDefGroup;
    }
    public void setMenuDefGroup(String menuDefGroup)
    {
        this.menuDefGroup = menuDefGroup;
    }
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    public String getQuery()
    {
        return query;
    }
    public void setQuery(String query)
    {
        this.query = query;
    }
    public String getSys_ID()
    {
        return sys_ID;
    }
    public void setSys_ID(String sys_ID)
    {
        this.sys_ID = sys_ID;
    }
    public String getParentID()
    {
        return parentID;
    }
    public void setParentID(String parentID)
    {
        this.parentID = parentID;
    }
    public List<Map<String, String>> getMenuItemRoles()
    {
        return menuItemRoles;
    }
    public void setMenuItemRoles(List<Map<String, String>> menuItemRoles)
    {
        this.menuItemRoles = menuItemRoles;
    }
    public List<Map<String, String>> getAvailableRolesList()
    {
        return availableRolesList;
    }
    public void setAvailableRolesList(List<Map<String, String>> availableRolesList)
    {
        this.availableRolesList = availableRolesList;
    }
    public MenuDefinitionModelVO getMenuDefinition()
    {
        return menuDefinition;
    }
    public void setMenuDefinition(MenuDefinitionModelVO menuDefinition)
    {
        this.menuDefinition = menuDefinition;
    }
    
}
