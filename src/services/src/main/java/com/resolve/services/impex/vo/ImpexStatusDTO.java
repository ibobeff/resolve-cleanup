package com.resolve.services.impex.vo;

import com.resolve.util.StringUtils;

public class ImpexStatusDTO
{
    String percent;
    String module;
    String operation;
    String wiki;
    boolean finished;
    String status;
    String url;
    String stage;
    String stageSuccess;
    String impexLog;
    
    public String getPercent()
    {
        return percent;
    }
    public void setPercent(String percent)
    {
        this.percent = percent;
    }
    public String getModule()
    {
        return module;
    }
    public void setModule(String module)
    {
        this.module = module;
    }
    public String getOperation()
    {
        return operation;
    }
    public void setOperation(String operation)
    {
        this.operation = operation;
    }
    public String getWiki()
    {
        return wiki;
    }
    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }
    public boolean getFinished()
    {
        return finished;
    }
    public void setFinished(boolean finished)
    {
        this.finished = finished;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }
    public String getUrl()
    {
        return url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }
    public String getStage() {
    	return stage;
    }
    public void setStage(String stage) {
    	this.stage = stage;
    }
    public String getStageSuccess() {
    	return stageSuccess;
    }
    public void setStageSuccess(String stageSuccess) {
    	this.stageSuccess = stageSuccess;
    }
    public String getImpexLog()
    {
        return impexLog;
    }
    public void setImpexLog(String impexLog)
    {
        this.impexLog = impexLog;
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder("Impex Status [");
    	
    	sb.append("finished=").append(finished).append(", percent=").append(StringUtils.isNotBlank(percent) ? percent : "")
    	.append(", module=").append(module).append(", operation=").append(operation).append(", status=")
    	.append(StringUtils.isNotBlank(status) ? status : "").append(", wiki=")
    	.append(StringUtils.isNotBlank(wiki) ? wiki : "").append(", url=")
    	.append(StringUtils.isNotBlank(url) ? url : "").append(", status=")
    	.append(StringUtils.isNotBlank(status) ? status : "").append(", stage=")
    	.append(StringUtils.isNotBlank(stage) ? stage : "").append(", stageSuccess=")
    	.append(StringUtils.isNotBlank(stageSuccess) ? stageSuccess : "");
    	
    	return sb.toString();
    }
}
