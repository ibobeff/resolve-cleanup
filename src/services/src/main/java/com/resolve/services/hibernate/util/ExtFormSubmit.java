/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.math.BigDecimal;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceCustomTable;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.services.constants.Operation;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.customtable.DeleteCustomTableRecords;
import com.resolve.services.hibernate.customtable.ExtMetaFormLookup;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.MetaFieldPropertiesVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveSysScriptVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.DateManipulator;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.VariableUtil;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.services.vo.WikiUser;
import com.resolve.services.vo.form.FormData;
import com.resolve.services.vo.form.FormSubmitDTO;
import com.resolve.services.vo.form.RsButtonAction;
import com.resolve.services.vo.form.RsButtonPanelDTO;
import com.resolve.services.vo.form.RsColumnDTO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsPanelDTO;
import com.resolve.services.vo.form.RsTabDTO;
import com.resolve.services.vo.form.RsUIButton;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

public class ExtFormSubmit
{
    private FormSubmitDTO formSubmitDTO = null;
    private String userId = null;

    private WikiUser wikiUser = null;
    private RsFormDTO form = null;

    private HashMap<String, String> result = new HashMap<String, String>();
    private Map<String, Object> DBRowData = new HashMap<String, Object>();
    private Map<String, Object> SourceRowData = new HashMap<String, Object>();
    private Map<String, String> ALL_FORM_PARAMS = new HashMap<String, String>();

    private String modelClass = "";
    private boolean isCustomTable = false;
    private Map<String, Object> oldCopyOfRowData = new HashMap<String, Object>();
    private Map<String, Object> defaultValuesOfFormForDBType = new HashMap<String, Object>();
    private Map<String, Object> persistedRowData = null;

    private Set<String> propertyNamesFromUIForUpdate = new HashSet<String>();
    private Map<String, String> sequenceNumbers = new HashMap<String, String>();

    private CustomTableVO customTable = null;

    public ExtFormSubmit(FormSubmitDTO formSubmitDTO, String userId) throws Exception
    {
        if (formSubmitDTO == null)
        {
            throw new RuntimeException("Form View information not available.");
        }

        if (StringUtils.isEmpty(userId))
        {
            throw new RuntimeException("UserId not provided");
        }

        this.formSubmitDTO = formSubmitDTO;
        this.userId = userId;

        init();
    }

    private void init() throws Exception
    {
        // get the form details
        //change the rights from ADMIN to VIEW so that any user who has View rights, can execute the form which is required for the Wiki Template 
        this.form = ServiceHibernate.getMetaFormView(this.formSubmitDTO.getFormSysId(), this.formSubmitDTO.getViewName(), this.formSubmitDTO.getTableName(), this.userId, RightTypeEnum.view, null, false);
        if (form == null)
        {
            throw new RuntimeException("Form does not exist : " + this.formSubmitDTO.getViewName() + " - sysId :" + this.formSubmitDTO.getFormSysId());
        }

        modelClass = this.form.getTableName();//this.formSubmitDTO.getTableName();// params.get(AdminConstants.MODELCLASS.getTagName());
        isCustomTable = StoreUtility.isCustomTable(modelClass);

        //load the custom table
        if (StringUtils.isNotBlank(modelClass))
        {
            customTable = ServiceCustomTable.getCustomTableWithReferences(null, modelClass, "system");
        }

        //prepare the base model from UI - all data comes in as a String type, we have to convert it in apprpriate types
        if (this.formSubmitDTO.getDbRowData() != null && this.formSubmitDTO.getDbRowData().getData() != null)
        {
            this.DBRowData.putAll(this.formSubmitDTO.getDbRowData().getData());
        }

        if (this.formSubmitDTO.getSourceRowData() != null && this.formSubmitDTO.getSourceRowData().getData() != null)
        {
            this.SourceRowData.putAll(this.formSubmitDTO.getSourceRowData().getData());
        }

        ALL_FORM_PARAMS.put(Constants.EXECUTE_USERID, userId);
        if (SourceRowData != null)
        {
            ALL_FORM_PARAMS.putAll(transformMapObjectToString(SourceRowData));
        }
        if (DBRowData != null)
        {
            ALL_FORM_PARAMS.putAll(transformMapObjectToString(DBRowData));
        }
       
        //get all the fields populated from the custom table which will be later on overridden by form default values
        //and later on the values from the UI
        this.defaultValuesOfFormForDBType.putAll(getDefaultValuesOfCustomTable());
        
        //make sure that all the fields on the form are in DBRowData. If not, add it with the defaults.
        //for now, its only the checkbox 
        Set<RsUIField> fieldsOnForm = prepareSetOfFieldsOnTheForm();
        for (RsUIField field : fieldsOnForm)
        {
            String name = field.getName();
            String dbType = field.getDbtype();
            
            //for the auto assign flag, when the Form is Submitted from UI, make sure that the user is populated who is submitting the request
            boolean autoAssignToMe = field.getAutoAssignToMe();
            if(this.DBRowData.containsKey(name) && autoAssignToMe)
            {
                String autoAssignValue = (String) this.DBRowData.get(name);
                if(StringUtils.isBlank(autoAssignValue) )
                {
                    autoAssignValue = userId;
                }
                this.DBRowData.put(name, autoAssignValue);   
            }
            
            if (StringUtils.isNotBlank(dbType))
            {
                //for checkbox, the value does not come in from the UI. So act as if its FALSE and populate the map coming from the UI
                if (!this.DBRowData.containsKey(name))
                {
                    //if the value is not coming from UI, than is will always be false
                    if (dbType.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BOOLEAN.getTagName()))
                    {
                        this.DBRowData.put(name, "false");
                    }
                }
            }
        } //end of for loop    
        
        //populate the list of properties coming from UI
        propertyNamesFromUIForUpdate.addAll(DBRowData.keySet());
        
        /////////////////////////
        
        

        try
        {
            //            String testTAblename = this.formSubmitDTO.getTableName();
            //            timeZone = this.formSubmitDTO.getTimezone();// params.get(AdminConstants.TIMEZONE.getTagName());
            wikiUser = UserUtils.getUserRolesAndGroups(userId);

            try
            {
                // get the default values from the conf and then populate it with the ones from the FORM
                // process the fields
                processHiddenFields();
                processFields();
            }
            catch (Throwable t)
            {
                Log.log.error("Issue with encrypting the fields", t);
                throw new RuntimeException("Issue with encrypting the fields", t);
            }
        }
        catch (Throwable t)
        {
            Log.log.error("error in getting reference object:" + t.getMessage(), t);
        }
    }

    public Map<String, String> executeFormProcessSequence() throws Exception
    {
        RsUIButton controlItemClicked = getButtonClicked();

        // proceed if you get the control button
        if (controlItemClicked != null)
        {
            List<RsButtonAction> actions = controlItemClicked.getActions();
            if (actions != null && actions.size() > 0)
            {
                for (RsButtonAction action : actions)
                {
                    String operation = action.getOperation();
                    if (operation.equalsIgnoreCase(Operation.DB.getTagName()))
                    {
                        executeDB(action);
                    }
                    else if (operation.equalsIgnoreCase(Operation.RUNBOOK.getTagName()))
                    {
                        executeRunbook(action);
                    }
                    else if (operation.equalsIgnoreCase(Operation.ACTIONTASK.getTagName()))
                    {
                        executeActionTask(action);
                    }
                    else if (operation.equalsIgnoreCase(Operation.MAPPING.getTagName()))
                    {
                        executeMapping(action);
                    }
                    else if (operation.equalsIgnoreCase(Operation.REDIRECT.getTagName()))
                    {
                        executeRedirect(action);
                    }
                    else if (operation.equalsIgnoreCase(Operation.SCRIPT.getTagName()))
                    {
                        executeSystemScripts(action);
                    }
                    else if (operation.equalsIgnoreCase(Operation.EVENT.getTagName()))
                    {
                        executeEvent(action);
                    }
                    else if (operation.equalsIgnoreCase(Operation.CLOSE.getTagName()))
                    {
                        result.put(HibernateConstantsEnum.EXECUTE_CLOSE_ACTION.getTagName(), "CLOSE");
                    }
                }// end of for loop
            }// end of if

        }

        return result;
    }

    public Map<String, Object> getDBRowData()
    {
        return DBRowData;
    }

    private Map<String, Object> getDefaultValuesOfCustomTable()
    {
        HashMap<String, Object> defaultValues = new HashMap<String, Object>();

        if (isCustomTable)
        {
            Collection<MetaFieldVO> fields = this.customTable.getMetaFields();
            for (MetaFieldVO field : fields)
            {
                String dbtype = field.getUDbType();
                MetaFieldPropertiesVO metaFldPropsVO = field.getMetaFieldProperties();
                
                String defaultValue = null;
                
                if (metaFldPropsVO != null)
                {
                    defaultValue = metaFldPropsVO.getUDefaultValue();
                }
                
                Object obj = getObjectDefaultValue(dbtype, defaultValue);

                defaultValues.put(field.getUName(), obj);
            }// end of for loop
        }// end of if

        return defaultValues;

    }// getDefaultValues

    private Object getObjectDefaultValue(String dbtype, String defaultValue)
    {
        Object object = null;

        if (StringUtils.isNotEmpty(defaultValue))
        {
            try
            {
                if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName()))
                {
                    object = defaultValue;
                }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_INTEGER.getTagName()))
                {
                    object = new Integer(defaultValue);
                }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_LONG.getTagName()))
                {
                    object = new Long(defaultValue);
                }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_FLOAT.getTagName()))
                {
                    object = new Float(defaultValue);
                }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_DOUBLE.getTagName()))
                {
                    object = new Double(defaultValue);
                }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BOOLEAN.getTagName()))
                {
                    object = new Boolean(defaultValue);
                }
                // else if(dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BYTE.getTagName()))
                // {
                // object = 0;
                // }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_TIMESTAMP.getTagName()))
                {
                    if (defaultValue.equalsIgnoreCase("NOW"))
                    {
                        object = GMTDate.getDate();
                    }
                    else
                    {
                        object = null;
                    }
                }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_CLOB.getTagName()) || dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BLOB.getTagName()))
                {
                    object = defaultValue;
                }
                else if (dbtype.equals(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BIGDECIMAL.getTagName()))
                {
                    object = new BigDecimal(defaultValue);
                }
                else
                {
                    object = defaultValue;
                }
            }
            catch (Throwable t)
            {
                object = null;
                Log.log.info("Exception getObjectInitValue -->" + t, t);
            }
        }// end of if

        return object;
    }// getObjectDefaultValue

    private void processHiddenFields()
    {
        List<RsUIField> fields = this.form.getSystemFields();
        if (fields != null && fields.size() > 0)
        {
            for (RsUIField field : fields)
            {
                massageData(field);
            }// end of for loop
        }

    }// processHiddenFields

    private void massageData(RsUIField field)
    {
        String name = field.getColumnModelName();
        String value = (String) SourceRowData.get(name);

        // remove the values that start with '$' and set it to empty strings
        if (StringUtils.isNotEmpty(value) && value.startsWith("$"))
        {
            value = "";
            SourceRowData.put(name, value);
        }

    }// massageINPUTData

    private void processFields() throws Throwable
    {
        List<RsPanelDTO> panels = form.getPanels();

        if (panels != null && panels.size() > 0)
        {
            for (RsPanelDTO panel : panels)
            {
                // if(panel.getIsTabPanel())
                // {
                // each panel can have more than 1 tab
                Collection<RsTabDTO> tabs = panel.getTabs();
                if (tabs != null)
                {
                    for (RsTabDTO tab : tabs)
                    {
                        // each tab can have 1 or 2 cols
                        processColumns(tab.getColumns());

                    }// end of for
                }// end of if
                 // }
                 // else
                 // {
                 // processColumns(panel.getColumns());
                 // }

            }// end of for loop

        }// end of if
    }// encryptFields();

    private void processColumns(List<RsColumnDTO> columns) throws Throwable
    {
        for (RsColumnDTO column : columns)
        {
            List<RsUIField> fields = column.getFields();
            if (fields != null && fields.size() > 0)
            {
	            for (RsUIField field : fields)
	            {
	                if (CustomFormUIType.SectionContainerField.name().equalsIgnoreCase(field.getUiType()))
	                {
	                    String jsonString = field.getSectionColumns();
	                    // parse json to get the columns
	                    JSONArray cols = StringUtils.stringToJSONArray(jsonString);
	                    // iterate through the fields in the column an convert
	                    // them to base models and add them to fields list
	                    for (int i = 0; i < cols.size(); i++)
	                    {
	                        JSONObject col = cols.getJSONObject(i);
	                        JSONArray columnFields = (JSONArray) col.get(HibernateConstants.META_FIELD_PROP_SECTION_COLUMN_CONTROLS);
	                        for (int j = 0; j < columnFields.size(); j++)
	                        {
	                            JsonConfig jsonConfig = new JsonConfig();
	                            jsonConfig.setRootClass(RsUIField.class);
	                            RsUIField rf = (RsUIField) JSONSerializer.toJava(columnFields.getJSONObject(j), jsonConfig);
	                            if (rf != null)
	                            {
	                                MetaFieldVO metaField = findMetaFieldFor(rf);
	                                if (metaField != null)//this is for DB field
	                                {
	                                    RsUIField dbField = ExtMetaFormLookup.convertMetaFieldToRsUIField(null, metaField, null, RightTypeEnum.admin, "admin", null, null);
	                                    populateUIPropToDbField(rf, dbField);
	                                    processField(dbField);
	                                }
	                                else
	                                {
	                                    //this is for INPUT types
	                                    processField(rf);
	                                }
	                            }
	                        }
	                    }
	                }
	                else
	                {
	                    processField(field);
	                }
	            }
            }
        }// end of for
    }

    private MetaFieldVO findMetaFieldFor(RsUIField uifield)
    {
        MetaFieldVO result = null;

        if (this.customTable != null)
        {
            Collection<MetaFieldVO> fields = this.customTable.getMetaFields();
            for (MetaFieldVO field : fields)
            {
                if (uifield.getName().equalsIgnoreCase(field.getUName()))
                {
                    result = field;
                    break;
                }
            }
        }//end of if

        return result;
    }

    private void populateUIPropToDbField(RsUIField uifield, RsUIField dbField)
    {
        //apply the UI props to the dbField obj
        dbField.setOrderNumber(uifield.getOrderNumber());
        dbField.setDisplayName(uifield.getDisplayName());
        dbField.setHidden(uifield.isHidden());
        dbField.setReadOnly(uifield.isReadOnly());
        dbField.setStringMinLength(uifield.getStringMinLength());
        dbField.setStringMaxLength(uifield.getStringMaxLength());
        dbField.setMandatory(uifield.isMandatory());
        dbField.setTooltip(uifield.getTooltip());
        dbField.setEncrypted(uifield.isEncrypted());
        dbField.setUiStringMaxLength(uifield.getUiStringMaxLength());
        dbField.setHeight(uifield.getHeight());
        dbField.setWidth(uifield.getWidth());

        //dependecies - TODO - keep an eye on this one as different dependencies may be applied for different forms for the same field
        dbField.setDependencies(uifield.getDependencies());
    }

    private void processField(RsUIField field) throws Throwable
    {
        String source = field.getSourceType();
        //        String uiType = field.getUiType();

        // convert the strings to clob if the datatype is
        // clob
        if (source.equals(HibernateConstantsEnum.DB.getTagName()))
        {
            updateDBFieldWithCorrectDatatype(field);
        }// end of if

        // if crypt or if the UI type is Password
        if (field.isEncrypted() || field.getUiType().equals(HibernateConstantsEnum.UI_PASSWORD.getTagName())) //should come from UI
        {
            if (source.equals(HibernateConstantsEnum.DB.getTagName()))
            {
                encrypt(field, HibernateConstantsEnum.DB);
            }
            else if (source.equals(HibernateConstantsEnum.INPUT.getTagName()))
            {
                encrypt(field, HibernateConstantsEnum.INPUT);
            }
        }
    }

    private void updateDBFieldWithCorrectDatatype(RsUIField field) throws Throwable
    {
        String columnModelName = field.getColumnModelName();
        String defaultValue = evaluateDefaultValue(field);

        String source = field.getSourceType();
        String type = field.getUiType();//getRstype();
        String dbType = field.getDbtype();
        
        //update the default value in the hashmap
        this.defaultValuesOfFormForDBType.put(columnModelName, getObjectDefaultValue(dbType, defaultValue));

        String value = (String) DBRowData.get(columnModelName);
        Object newValue = null;

        if (StringUtils.isNotBlank(source) && StringUtils.isNotBlank(value) && StringUtils.isNotBlank(type) && source.equals(HibernateConstantsEnum.DB.getTagName()))
        {
            if (type.equalsIgnoreCase(HibernateConstantsEnum.REFERENCE_TYPE.getTagName()))
            {
                // for reference type
                String sys_ID = (String) DBRowData.get(columnModelName);
                String tableName = field.getReferenceTable();
                String modelName = ServiceHibernate.table2Class(tableName);

                if (StringUtils.isNotEmpty(modelName) && modelName.startsWith("com.resolve.persistence.model"))
                {
                    // this is for System tables
                    newValue = Class.forName(modelName).newInstance();
                    PropertyUtils.setProperty(newValue, HibernateConstants.SYS_ID, sys_ID);
                }
                else
                {
                    try
                    {
                        newValue = ServiceHibernate.findById(tableName, sys_ID);
                    }
                    catch (Throwable t)
                    {
                        Log.log.error("error in getting reference object:" + t.getMessage(), t);
                    }
                }

            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_CLOB.getTagName()))
            {
                // for CLOB
                newValue = value;
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_TIMESTAMP.getTagName()))
            {
                // for Date
                newValue = DateManipulator.parseISODate(value).toDate();
                newValue = GMTDate.getDate((Date) newValue);
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_INTEGER.getTagName()))
            {
                // if integer
                try
                {
                    newValue = Integer.parseInt(value);
                }
                catch (Exception e)
                {
                    newValue = null;
                }
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_LONG.getTagName()))
            {
                // if long
                try
                {
                    newValue = Long.parseLong(value);
                }
                catch (Exception e)
                {
                    newValue = null;
                }
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_FLOAT.getTagName()))
            {
                // if float
                try
                {
                    newValue = Float.parseFloat(value);
                }
                catch (Exception e)
                {
                    newValue = null;
                }
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_DOUBLE.getTagName()))
            {
                // if double
                try
                {
                    newValue = Double.parseDouble(value);
                }
                catch (Exception e)
                {
                    newValue = null;
                }
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BOOLEAN.getTagName()))
            {
                // if boolean
                try
                {
                    newValue = Boolean.parseBoolean(value);
                }
                catch (Exception e)
                {
                    newValue = false;
                }
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BYTE.getTagName()))
            {
                // if byte
                // NOT IMPLEMENTED FOR CUSTOM TABLE
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BLOB.getTagName()))
            {
                // if blob
                // NOT IMPLEMENTED FOR CUSTOM TABLE
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BIGDECIMAL.getTagName()))
            {
                // if big decimal
                try
                {
                    newValue = new BigDecimal(value);
                }
                catch (Exception e)
                {
                    newValue = null;
                }
            }
            else
            {
                // its a string
                // no need to do anything as everything coming from UI is already a string
                newValue = value;
            }
        }// end of if
        else
        {
            if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BOOLEAN.getTagName()))
            {
                newValue = false;
            }
            //if sequence is null, then get the next one as these request can come from the api and not from the UI
            else if (CustomFormUIType.Sequence.name().equalsIgnoreCase(type))
            {
                newValue = getSequenceNumber(field.getSequencePrefix());
            }
            else if (dbType.equalsIgnoreCase(HibernateConstantsEnum.CUSTOMTABLE_TYPE_CLOB.getTagName()))
            {
                // for CLOB
                newValue = ServiceHibernate.getClobFromString("");
            }
        }

        // set the new value only if the UI has that value
        if (propertyNamesFromUIForUpdate.contains(field.getColumnModelName()))
        {
            DBRowData.put(field.getColumnModelName(), newValue);
        }
    }

    //there can be more than 1 seq controls on a form
    private String getSequenceNumber(String preFix)
    {
        String seqNum = null;

        if (sequenceNumbers.containsKey(preFix))
        {
            seqNum = sequenceNumbers.get(preFix);
        }
        else
        {
            seqNum = SequenceUtils.getNextSequence(preFix);
            sequenceNumbers.put(preFix, seqNum);
        }

        return seqNum;
    }

    private void encrypt(RsUIField field, HibernateConstantsEnum source) throws Exception
    {
        String name = field.getName();
        String value = null;

        switch (source)
        {
            case DB:
                value = (String) DBRowData.get(name);
                if (StringUtils.isNotEmpty(value) && !value.startsWith("ENC"))
                {
                    value = CryptUtils.encrypt(value);
                    DBRowData.put(name, value);
                }
                break;

            case INPUT:
            default:
                value = (String) SourceRowData.get(name);
                if (StringUtils.isNotEmpty(value) && !value.startsWith("ENC"))
                {
                    value = CryptUtils.encrypt(value);
                    SourceRowData.put(name, value);
                }
                break;
        }// end of switch

    }// encrypt

    private String evaluateDefaultValue(RsUIField field)
    {
        String defaultValue = field.getDefaultValue();
        String type = field.getUiType();
        String fieldName = field.getName();

        if (CustomFormUIType.UserPickerField.name().equalsIgnoreCase(type))
        {
            if (field.getAutoAssignToMe())
            {
                defaultValue = this.userId;
            }
            else
            {
                String teams = field.getTeamsOfTeams();
                if (StringUtils.isNotEmpty(teams))
                {
                    String team = null;
                    //cccc|aaaa
                    if (teams.indexOf('|') > -1)
                    {
                        String[] arr = teams.split("\\|");
                        team = arr[0].trim();
                    }
                    else
                    {
                        team = teams;
                    }

                    //get the owner of the team and set it as default value
                    Map<String, Object> record = SocialUtil.findTeamByIdOrName(null, team);
                    if (record != null)
                    {
                        defaultValue = (String) record.get(HibernateConstants.U_OWNER);
                    }
                }
            }

        }
        else if (CustomFormUIType.TeamPickerField.name().equalsIgnoreCase(type))
        {
            String teams = field.getTeamPickerTeamsOfTeams();
            if (StringUtils.isNotEmpty(teams))
            {
                //bbbb|cccc|aaaa
                if (teams.indexOf('|') > -1)
                {
                    String[] arr = teams.split("\\|");
                    defaultValue = arr[0].trim();
                }
                else
                {
                    defaultValue = teams;
                }
            }
        }
        else if (CustomFormUIType.Sequence.name().equalsIgnoreCase(type))
        {
            //sequence # should be increased only if there is no value coming in for this field. 
            //so check if the map coming in with data has the value. If no, than get the defaultValue for the sequence
            String uiValue = (String) this.DBRowData.get(fieldName);
            if(StringUtils.isNotBlank(uiValue))
            {
                defaultValue = uiValue;
            }
            else
            {
                defaultValue = getSequenceNumber(field.getSequencePrefix());    
            }
        }

        return defaultValue;

    }

    private RsUIButton getButtonClicked()
    {
        RsButtonPanelDTO metaControl = form.getButtonPanel();
        RsUIButton controlItemClicked = null;

        String controlItemId = this.formSubmitDTO.getControlItemSysId();// params.get(HibernateConstantsEnum.CONTROL_ITEM_SYS_ID.getTagName());
        String controlItemName = this.formSubmitDTO.getControlItemName();//coming from ESB or actiontask

        if (metaControl != null && (StringUtils.isNotEmpty(controlItemId) || StringUtils.isNotEmpty(controlItemName)))
        {
            List<RsUIButton> items = metaControl.getControls();
            for (RsUIButton item : items)
            {
                if (StringUtils.isNotEmpty(controlItemId) && item.getId().equals(controlItemId))
                {
                    controlItemClicked = item;
                    break;
                }
                else if (StringUtils.isNotEmpty(controlItemName) && item.getDisplayName().trim().equalsIgnoreCase(controlItemName.trim()))
                {
                    controlItemClicked = item;
                    break;
                }
            }// end of for loop

            if (controlItemClicked != null)
            {
                System.out.println("control clicked : " + controlItemClicked.getDisplayName());
            }
            else
            {
                System.out.println("control item is null and did not match. Please verify. Value was:" + controlItemId + " : " + controlItemName);
                Log.log.error("control item is null and did not match. Please verify. Value was:" + controlItemId + " : " + controlItemName);
            }
        }

        return controlItemClicked;
    }

    private void executeDB(RsButtonAction action)
    {
        if (StringUtils.isNotEmpty(this.modelClass))
        {
            String crudAction = action.getCrudAction();// this.formSubmitDTO.getCrudAction();//params.get(HibernateConstantsEnum.EXECUTE_CRUD_ACTION.getTagName());

            // prepare the object that needs to be persisted - make sure it is outside the transaction and has its own transaction
            persistedRowData = getPersistedObject();

            if (StringUtils.isNotEmpty(crudAction))
            {
                try
                {
                    if (crudAction.equals(HibernateConstantsEnum.DELETE.getTagName()))
                    {
                        deleteDBRow();
                    }
                    else
                    {
                        // its insert or update
                        updateDBRow();
                    }
                }
                catch (Throwable t)
                {
                    Log.log.error("error in update of DB:" + t.getMessage(), t);
                }
            }
        }
    }// executeDB

    @SuppressWarnings({ "unchecked" })
    private void executeRunbook(RsButtonAction action)
    {   
        String additionalParamStr = action.getAdditionalParam();
        Map<String, String> additionalParams = StringUtils.stringToMap(additionalParamStr);

        // params for runbook
        Map<String, Object> runbookParams = new HashMap<String, Object>();
        runbookParams.putAll(additionalParams);//put this first so that it can get updated from the UI
        runbookParams.putAll(ALL_FORM_PARAMS);
        runbookParams.put(Constants.GROOVY_BINDING_OLD_PARAMS, this.oldCopyOfRowData);

        // NOTE : THese values are coming from UI now, the button when clicked will execute a javascript that will set these hidden values in the form and it will be submitted to the server
        // so NO MANIPULATION OF THE DATA should be done at this point
        // convert string to hashmap for additional params
        // if(StringUtils.isNotEmpty(action.getAdditionalParam()))
        // {
        // Map<String, String> additionalParams = StringUtils.stringToMap(action.getAdditionalParam(), StringUtils.VALUESEPARATOR, StringUtils.FIELDSEPARATOR);
        // runbookParams.putAll(additionalParams);
        // }

        // String runbookToExecute = action.getRunbook();
        // if(StringUtils.isNotEmpty(runbookToExecute))
        // {
        // runbookParams.put(SharedConstants.EXECUTE_RUNBOOK, runbookToExecute);
        // }
        String runbookToExecuteFromDef = action.getRunbook();
        if( "${CURRENT_WIKI}".equalsIgnoreCase(runbookToExecuteFromDef) && StringUtils.isNotBlank(formSubmitDTO.getCurrentDocumentName())) {
            runbookToExecuteFromDef = formSubmitDTO.getCurrentDocumentName();
        }
        String runbookToExecuteFromParams = (String) runbookParams.get(Constants.EXECUTE_WIKI);
        String runbookToExecute = null;

        if (StringUtils.isNotEmpty(runbookToExecuteFromParams))
        {
            runbookToExecute = runbookToExecuteFromParams;
        }
        else
        {
            runbookToExecute = runbookToExecuteFromDef;
        }

        // add to runbookParams
        runbookParams.put(Constants.EXECUTE_WIKI, runbookToExecute);//same as Constants.HTTP_REQUEST_WIKI
        runbookParams.put(HibernateConstants.EXECUTE_RUNBOOK, runbookToExecute);

        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("Executing Runbook :" + runbookToExecute);
            Log.log.trace("Params for Runbook :" + runbookParams);
        }

        try
        {
            //check if the user has Access rights to execute this runbook
            WikiDocumentVO runbook = ServiceWiki.getWikiDoc(null, runbookToExecute, this.userId);
            if (runbook != null)
            {
                String executeRoles = runbook.getUExecuteRoles();
                boolean hasRights = com.resolve.services.util.UserUtils.hasRole(userId, wikiUser.getRoles(), executeRoles);
                if (hasRights)
                {
                    if (Log.log.isTraceEnabled())
                    {
                        Log.log.trace("Executing Runbook :" + runbookToExecute);
                        Log.log.trace("Params for Runbook :" + runbookParams);
                    }

                    //from wiki.submitRequestToExecuteRunbook
                    runbookParams.put(Constants.HTTP_REQUEST_ACTION, "EXECUTEPROCESS");

                    //execute the runbook
                    try
                    {
                        String resultStr = ExecuteRunbook.start(runbookParams);

                        if (StringUtils.isNotBlank(resultStr))
                        {
                            if (resultStr.toUpperCase().contains("ERROR") || resultStr.toUpperCase().contains("FAILURE"))
                            {
                                throw new Exception ("Failed to execute runbook " + runbookToExecute + ". " + resultStr);
                            }
                        }
                        
                        // ADD THE PROBLEMID TO ALL_FORM_PARAMS
                        // problemdid, processid
                        if (this.ALL_FORM_PARAMS.get(Constants.HTTP_REQUEST_PROBLEMID) != null)
                        {
                            this.ALL_FORM_PARAMS.put(Constants.HTTP_REQUEST_PROBLEMID, (String) runbookParams.get(Constants.HTTP_REQUEST_PROBLEMID));
                            result.put(Constants.HTTP_REQUEST_PROBLEMID, (String) runbookParams.get(Constants.HTTP_REQUEST_PROBLEMID));
                        }

                        if (this.ALL_FORM_PARAMS.get(Constants.EXECUTE_PROCESSID) != null)
                        {
                            this.ALL_FORM_PARAMS.put(Constants.EXECUTE_PROCESSID, (String) runbookParams.get(Constants.EXECUTE_PROCESSID));
                            result.put(Constants.EXECUTE_PROCESSID, (String) runbookParams.get(Constants.EXECUTE_PROCESSID));
                        }

                        result.put(Operation.RUNBOOK.getTagName(), "Runbook executed successfully");
                    }
                    catch (Throwable t)
                    {
                        Log.log.error("Error in executing Runbook:", t);
                        result.put(Operation.RUNBOOK.getTagName(), "Runbook Execution Failed:" + runbookToExecute);
                    }
                }
                else
                {
                    result.put(Operation.RUNBOOK.getTagName(), "User has no rights to execute runbook :" + runbookToExecute);
                }
            }
            else
            {
                result.put(Operation.RUNBOOK.getTagName(), runbookToExecute + " Runbook does not exist.");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in execting Runbook:" + runbookToExecute, e);
            result.put(Operation.RUNBOOK.getTagName(), "Runbook Execution Failed: " + runbookToExecute);
        }

    }// executeRunbook

    @SuppressWarnings({ "unchecked", "unused" })
    private void executeActionTask(RsButtonAction action)
    {
        String actiontaskName = action.getActionTask();

        if (StringUtils.isNotEmpty(actiontaskName))
        {
            try
            {
                String additionalParamStr = action.getAdditionalParam();
                Map<String, String> additionalParams = StringUtils.stringToMap(additionalParamStr);

                ResolveActionTaskVO at = ServiceHibernate.getResolveActionTask(null, actiontaskName, userId);
                if (at != null)
                {
                    String executeRoles = at.getURoles();
                    boolean hasRights = com.resolve.services.util.UserUtils.hasRole(userId, wikiUser.getRoles(), executeRoles);

                    if (hasRights)
                    {
                        //params for actiontask
                        Map<String, Object> actiontaskParams = new HashMap<String, Object>();
                        actiontaskParams.putAll(additionalParams);
                        actiontaskParams.putAll(ALL_FORM_PARAMS);
                        actiontaskParams.put(Constants.EXECUTE_USERID, userId);
                        actiontaskParams.put(Constants.EXECUTE_ACTIONNAME, actiontaskName);
                        actiontaskParams.put(Constants.HTTP_REQUEST_ACTION, "EXECUTETASK");
                        actiontaskParams.put(Constants.GROOVY_BINDING_OLD_PARAMS, this.oldCopyOfRowData);

                        if (Log.log.isTraceEnabled())
                        {
                            Log.log.trace("Executing Actiontask :" + actiontaskName);
                            Log.log.trace("Params for Actiontask :" + actiontaskParams);
                        }

                        //execute the actiontask
                        try
                        {
                            String resultStr = ExecuteActionTask.start(actiontaskParams);
                            result.put(Operation.ACTIONTASK.getTagName(), "Actiontask executed successfully");
                            result.put(Constants.EXECUTE_PROBLEMID, (String) actiontaskParams.get(Constants.EXECUTE_PROBLEMID));
                        }
                        catch (Throwable t)
                        {
                            Log.log.error("Error in execting Actiontask:", t);
                            result.put(Operation.ACTIONTASK.getTagName(), "Actiontask Execution Failed:" + t.getMessage());
                        }
                    }
                    else
                    {
                        result.put(Operation.ACTIONTASK.getTagName(), "User has no rights to execute Actiontask " + actiontaskName);
                    }
                }
                else
                {
                    result.put(Operation.ACTIONTASK.getTagName(), actiontaskName + " Actiontask does not exist.");
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error in execting Actiontask:", e);
                result.put(Operation.ACTIONTASK.getTagName(), "Actiontask Execution Failed:" + e.getMessage());
            }
        }
        else
        {
            result.put(Operation.ACTIONTASK.getTagName(), "Actiontask name is null.");
        }
    }
    
    @SuppressWarnings("unchecked")
    private void executeMapping(RsButtonAction action)
    {
        // update the values 'to' to the 'from'
        // PROBLEMID|=|u_problemid|&|EVENTID|=|u_eventId123
        String params = action.getAdditionalParam();
        if (StringUtils.isNotBlank(params))
        {
            Map<String, String> additionalParams = StringUtils.stringToMap(params);
            Iterator<String> it = additionalParams.keySet().iterator();
            while (it.hasNext())
            {
                String to = it.next();
                String from = additionalParams.get(to);

                // check if the value is in DB , if yes, then update it
                Object fromValue = this.ALL_FORM_PARAMS.get(from);
                if (this.DBRowData.containsKey(to))
                {
                    this.DBRowData.put(to, fromValue);
                }

                if (this.ALL_FORM_PARAMS.containsKey(to))
                {
                    this.ALL_FORM_PARAMS.put(to, (String) fromValue);
                }
            }// end of while

            result.put(Operation.MAPPING.getTagName(), "Mapping done successfully.");
        }
    }

    private void executeRedirect(RsButtonAction action) throws Exception
    {
        String url = action.getRedirectUrl();

        // replace the url parameters
        if (StringUtils.isNotEmpty(url))
        {
            if (url.indexOf(HibernateConstants.VIEW_LOOKUP) > -1)
            {
                //test/view/customtable.jsp?main=table&view=${view_lookup:CR}&table=CR
                String searchStr = "view=";
                String lookupString = url.substring(url.indexOf(searchStr) + searchStr.length());
                lookupString = lookupString.substring(0, lookupString.indexOf('}') + 1);

                String value = StoreUtility.evaluateViewLookup(lookupString, this.userId);

                url = url.replace(lookupString, value);
            }

            //do this last else the values may be replaced by an empty string
            url = VariableUtil.replaceUrlParams(url, ALL_FORM_PARAMS);
        }
        else
        {
            url = "";
        }

        result.put(Operation.REDIRECT.getTagName(), url);
        result.put(Operation.REDIRECT.getTagName() + "_TARGET", action.getRedirectTarget());
    }

    // groovy scripts - similar to executeSystemScript.java
    @SuppressWarnings("unchecked")
    private void executeSystemScripts(RsButtonAction action)
    {
        // get the script from resolve_sys_script table
        String scriptName = action.getScript();
        if (StringUtils.isNotEmpty(scriptName))
        {
            ResolveSysScriptVO script = ServiceHibernate.findResolveSysScriptByIdOrName(null, scriptName, this.userId);

            if (script != null)
            {
                String additionalParamStr = action.getAdditionalParam();
                Map<String, String> additionalParams = StringUtils.stringToMap(additionalParamStr);

                // params for runbook
                Map<String, Object> scriptParams = new HashMap<String, Object>();
                scriptParams.putAll(additionalParams);
                scriptParams.putAll(ALL_FORM_PARAMS);

                // create a map for templates - this may be used only for the Templates
                Map<String, Object> templateMap = createParamsForWikiTemplate();

                // add it to the script map
                scriptParams.put(HibernateConstants.WIKI_TEMPLATE_VALUES_KEY, templateMap);
                scriptParams.put(HibernateConstants.USERNAME, this.userId);

                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace("Executing System Script :" + scriptName);
                    Log.log.trace("Params for Script :" + scriptParams);
                }
                // its a groovy script,
                // executeSystemScript(script.getUScript(), scriptParams);
                executeSystemScript(script.getUName(), scriptParams);
            }// end of if
            else
            {
                result.put(Operation.SCRIPT.getTagName(), "No Script to execute");
            }
        }// end of if

    }// executeSystemScripts
    
    @SuppressWarnings("unchecked")
    private void executeEvent(RsButtonAction action)
    {
        String additionalParamStr = action.getAdditionalParam();
        Map<String, String> additionalParams = StringUtils.stringToMap(additionalParamStr);

        // MAPFROM should already been replaced by the client side. Check if the map from field is empty.
        Map<String, String> eventParams = new HashMap<String, String>();
        eventParams.putAll(additionalParams);
        eventParams.putAll(ALL_FORM_PARAMS);

        String runbookToExecute = action.getRunbook();
        if (StringUtils.isNotEmpty(runbookToExecute) && !eventParams.containsKey(Constants.EXECUTE_WIKI))
        {
            eventParams.put(Constants.EXECUTE_WIKI, runbookToExecute);
            eventParams.put(HibernateConstants.EXECUTE_RUNBOOK, runbookToExecute);
        }

        try
        {
            MainBase.getESB().sendEvent(eventParams);
            result.put(Operation.EVENT.getTagName(), "Event send successfully");
        }
        catch (Throwable t)
        {
            Log.log.error("Error in sending event:", t);
            result.put(Operation.EVENT.getTagName(), "Event Failed:" + t.getMessage());
        }

    }// executeEvent

    private Map<String, Object> getPersistedObject()
    {
        String id = (String) DBRowData.get(HibernateConstants.SYS_ID);
        Map<String, Object> persistedRowData = null;

        // update the sysUpdatedBy and sysUpdatedOn
        DBRowData.put(HibernateConstants.SYS_UPDATED_BY, userId);
        //long millis = GMTDate.calendar.getTimeInMillis();
        DBRowData.put(HibernateConstants.SYS_UPDATED_ON, GMTDate.getDate());

        if (StringUtils.isEmpty(id))
        {
            DBRowData.put(HibernateConstants.SYS_CREATED_BY, userId);
            DBRowData.put(HibernateConstants.SYS_CREATED_ON, GMTDate.getDate());

            persistedRowData = new HashMap<String, Object>(this.defaultValuesOfFormForDBType);// getDefaultValues();
        }
        else
        {
            try
            {
                persistedRowData = ServiceHibernate.findById(modelClass, id);
                if (persistedRowData != null)
                {
                    this.oldCopyOfRowData = new HashMap<String, Object>(persistedRowData);
                }
                else
                {
                    this.oldCopyOfRowData = new HashMap<String, Object>();
                    persistedRowData = new HashMap<String, Object>();
                }

            }
            catch (Throwable t)
            {
                Log.log.error("error in fetching the persisted data object:" + t.getMessage(), t);
            }
        }

        // populate the persisted oject with what we have from the UI
        persistedRowData.putAll(DBRowData);

        return persistedRowData;
    }// getPersistedObject

    private void deleteDBRow()
    {
        String id = (String) DBRowData.get(HibernateConstants.SYS_ID);
        List<String> listId = createList(id);

        Map<String, String> deleteResult;
        try
        {
            deleteResult = new DeleteCustomTableRecords(modelClass, listId, userId).delete();
            
            this.result.putAll(deleteResult);
        }
        catch (Exception e)
        {
            Log.log.error("error in deleting DB Row", e);
        }

    }// deleteDBRow()

    private List<String> createList(String val)
    {
        List<String> list = new ArrayList<String>();
        list.add(val);

        return list;
    }// createList

    private void updateDBRow()
    {
        Map<String, String> updateResult = ServiceHibernate.updateDBRow(userId, modelClass, persistedRowData, isCustomTable);
        result.putAll(updateResult);

        String id = (String) persistedRowData.get(HibernateConstants.SYS_ID);
        DBRowData.put(HibernateConstants.SYS_ID, id);
        ALL_FORM_PARAMS.putAll(transformMapObjectToString((DBRowData)));

    }// updateDBRow();
    
 // TODO: Review and Testing of this API is pending
    private String executeSystemScript(String scriptName, Map<String, Object> params)
    {
        String resultStr = "";

        String problemId = null;
        if (StringUtils.isNotEmpty(userId))
        {
            problemId = WorksheetUtils.getActiveWorksheet(userId, (String)params.get(Constants.EXECUTE_ORG_ID), (String)params.get(Constants.EXECUTE_ORG_NAME));
        }
        Log.log.info("ProblemId in executeSystemScript :" + problemId);

        // check if the params has PROBLEMID, if no, then add this one
        if (!params.containsKey(Constants.GROOVY_BINDING_PROBLEMID))
        {
            params.put(Constants.GROOVY_BINDING_PROBLEMID, problemId);
        }

        // TODO: VERIFY THIS IF THIS SHOULD BE DONE
        params.put(Constants.EXECUTE_USERID, this.userId);
        params.put(Constants.GROOVY_BINDING_PARAMS, params);
        params.put(Constants.GROOVY_BINDING_INPUTS, params);

        //add old copy of the DB object
        params.put(Constants.GROOVY_BINDING_OLD_PARAMS, this.oldCopyOfRowData);
        try
        {
            resultStr = ServiceHibernate.executeScript(scriptName, params);
            result.put(Operation.SCRIPT.getTagName(), "Script executed successfully.");
            result.put("SCRIPT_RESULT", resultStr);
        }
        catch (Throwable t)
        {
            Log.log.error("Error in executeSystemScript :", t);
            resultStr = t.getMessage();// just put the actual exception content and return it.
            result.put(HibernateConstants.ERROR_EXCEPTION_KEY, "BR Script Failed:" + resultStr);
        }

        return resultStr;
    }
    
    private Map<String, Object> createParamsForWikiTemplate()
    {
        Map<String, Object> templateMap = new HashMap<String, Object>();
        if (ALL_FORM_PARAMS != null && ALL_FORM_PARAMS.size() > 0)
        {
            Iterator<String> it = ALL_FORM_PARAMS.keySet().iterator();
            while (it.hasNext())
            {
                String key = it.next();
                String value = ALL_FORM_PARAMS.get(key);

                String newKey = "[" + key + "]";

                templateMap.put(newKey, value);
            }
        }

        return templateMap;
    }
    
    private static HashMap<String, String> transformMapObjectToString(Map<String, Object> data)
    {
        HashMap<String, String> result = new HashMap<String, String>();

        if (data != null)
        {
            Iterator<String> it = data.keySet().iterator();
            while (it.hasNext())
            {
                String key = it.next();
                Object objValue = data.get(key);
                if (objValue != null)
                {
                    //check for datatypes and then convert to string
                    //Clob
                    String value = objValue.toString();
                    if(objValue instanceof Clob)
                    {
                       value = HibernateUtil.getStringFromClob(objValue);
                    }
                    
                    result.put(key, value);
                }
            }// while loop
        }// end of if
        return result;
    }// convertBaseModelToHashMap
    
    private Set<RsUIField> prepareSetOfFieldsOnTheForm()
    {
        Set<RsUIField> fieldsOnForm = new HashSet<RsUIField>();

        List<RsPanelDTO> panels = this.form.getPanels();
        for (RsPanelDTO panel : panels)
        {
            List<RsTabDTO> tabs = panel.getTabs();
            for (RsTabDTO tab : tabs)
            {
                List<RsColumnDTO> columns = tab.getColumns();
                for (RsColumnDTO column : columns)
                {
                    if (column.getFields() != null)
                    {
                        List<RsUIField> fields = column.getFields();
                        for (RsUIField field : fields)
                        {
                            fieldsOnForm.add(field);
                        }
                    }
                }
            }
        }

        return fieldsOnForm;
    }


}
