package com.resolve.services.exception;

/**
 * Used when client tries to save not a valid color theme defined in
 * {@link ColorTheme}
 * 
 * @author IvanPetrov
 *
 */
public class ColorThemeException extends Exception {

    private static final long serialVersionUID = -9103525340405888599L;

}
