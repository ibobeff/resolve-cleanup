package com.resolve.services.vo;

import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;

/**
 * Lightweight version of {@link ResolveSecurityIncidentVO} used to provide
 * reduced data set in cases when full incident cannot be provided for security reasons.
 */
public class SecurityIncidentLightDTO {
    private String sys_id;
    private String sysOrg;

	private String investigationType;
	private String severity;
	private String sir;
	private String status;
	private String title;

	public String getSys_id() {
		return sys_id;
	}

	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}

	public String getSysOrg() {
		return sysOrg;
	}

	public void setSysOrg(String sysOrg) {
		this.sysOrg = sysOrg;
	}

	public String getInvestigationType() {
		return investigationType;
	}

	public void setInvestigationType(String investigationType) {
		this.investigationType = investigationType;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getSir() {
		return sir;
	}

	public void setSir(String sir) {
		this.sir = sir;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
