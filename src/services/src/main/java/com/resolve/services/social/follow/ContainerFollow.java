/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.follow;

import java.util.HashSet;
import java.util.Set;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.StringUtils;

/**
 * 
 * Class for containers like Process, Team and Forum
 * 
 * @author jeet.marwah
 *
 */

public abstract class ContainerFollow
{
    protected ResolveNodeVO rootNode = null; 
    protected String username = null;

    //switches
    private boolean recurse = true;//be default, get all the child teams also
    private boolean ignoreUsers = false; // if its true, then the result will filter out Users and return only Teams
    private boolean directFollow = true;
    
    public ContainerFollow(ResolveNodeVO rootNode, String username) throws Exception
    {
        if(rootNode == null || StringUtils.isEmpty(username))
        {
            throw new Exception("Root Node and Username are mandatory.");
        }
        
        this.rootNode = rootNode;
        this.username = username;
    }
    
    protected Set<RSComponent> streams = new HashSet<RSComponent>();
    protected Set<String> compSysIds = new HashSet<String>();
    
    public abstract Set<RSComponent> getStreams() throws Exception;

    public boolean isRecurse()
    {
        return recurse;
    }

    public void setRecurse(boolean recurse)
    {
        this.recurse = recurse;
    }
    
    public boolean isIgnoreUsers()
    {
        return ignoreUsers;
    }

    public void setIgnoreUsers(boolean ignoreUsers)
    {
        this.ignoreUsers = ignoreUsers;
    }

    public boolean isDirectFollow()
    {
        return directFollow;
    }

    public void setDirectFollow(boolean directFollow)
    {
        this.directFollow = directFollow;
    }

    protected Set<RSComponent> convertNodeToRsComponent(ResolveNodeVO node, boolean directFollow) throws Exception
    {
        Set<RSComponent> result = new HashSet<RSComponent>();
        NodeType type = node.getUType();
        boolean markedDeleted = node.ugetUMarkDeleted();

        if (!markedDeleted)
        {
            //if the type is Document, we have to add Runbook and DT as separate listing for the UI
            if (type == NodeType.DOCUMENT)
            {
                Document doc = new Document(node);
                doc.setDirectFollow(directFollow);

                //add it to the list
                result.add(doc);

                //if the document is runbook
                if (doc.isRunbook())
                {
                    try
                    {
                        Runbook runbook = new Runbook(node);
                        runbook.setDirectFollow(directFollow);
                        result.add(runbook);
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }
                }

                //if the doc is DT
                if (doc.isDecisionTree())
                {
                    try
                    {
                        DecisionTree dt = new DecisionTree(node);
                        dt.setDirectFollow(directFollow);
                        result.add(dt);
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }
                }
            }
            else
            {
                RSComponent comp = new RSComponent(node);

                //set the flag
                comp.setDirectFollow(directFollow);

                //add it to the list
                result.add(comp);
            }
            
            compSysIds.add(node.getSys_id());
        }

        return result;
    }
}
