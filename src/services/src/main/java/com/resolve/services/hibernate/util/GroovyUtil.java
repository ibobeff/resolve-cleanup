/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import groovy.lang.Binding;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.resolve.groovy.script.GroovyScript;
import com.resolve.persistence.util.EntityUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ResultCode;
import com.resolve.util.StringUtils;

public class GroovyUtil {
	private static final String IMPORT_PEFIX = "import ";
	private static final String IMPORT_WILD_CARD_SUFFIX = ".*";
	private static final String PERIOD = ".";
	
    public static Map<String, Object> executeGroovy(String script, String scriptName, Object[] args, Binding binding, 
    												ResultCode rc, boolean cacheable, boolean isException) {        
        Object outputResult = null;
        Map<String, Object> result = new HashMap<String, Object>();
        
        if (StringUtils.isBlank(script)) {
        	return result;
        }
        
        boolean hasDB = GroovyScript.checkScriptUseDB(script, cacheable);
        
        /*
         * Groovy can import classes in two ways just like Java
         * 
         * Specific class with fully qualified name
         * 
         * import my.package.class
         * 
         *  or all classes in packages using wild card "*"
         *  
         * import my.package.* 
         * 
         * Bind DB connection to groovy execution thread context 
         * if and only if groovy script contains import of one or more 
         * EntityUtil, HibernateUtil, and HibernateProxy
         * classes and one or more is used in script with marker class simple name 
         * (i.e. without package prefix) followed by period "." to invoke
         * one of their static methods.
         * 
         * Please note even if some comment(s) in groovy script results in 
         * false positive and DB connection does get bound to groovy 
         * execution thread context, there is relatively low chance of 
         * connection not getting closed or returned back to pool
         * only if thread executing groovy script terminates.
         */
        if (!hasDB) {
        	hasDB = (script.contains(IMPORT_PEFIX + EntityUtil.class.getName()) || 
        			 script.contains(IMPORT_PEFIX + HibernateUtil.class.getName()) || 
                	 script.contains(IMPORT_PEFIX + HibernateProxy.class.getName()) ||
                	 script.contains(IMPORT_PEFIX + EntityUtil.class.getPackage().getName() + IMPORT_WILD_CARD_SUFFIX)) &&
                	(script.contains(EntityUtil.class.getSimpleName() + PERIOD) || 
                	 script.contains(HibernateUtil.class.getSimpleName() + PERIOD) ||
                	 script.contains(HibernateProxy.class.getSimpleName() + PERIOD));
        }
        
        try {
        	// add RC to binding
            binding.setVariable(Constants.GROOVY_BINDING_RC, rc);
            
            // init transaction
            if (hasDB) {
            	/*
            	 * Opening connection in try resource block guarantees its closure or 
            	 * return back to pool if connection is obtained from pool, 
            	 * irrespective of normal or abnormal execution
            	 * of invoked groovy script.
            	 * 
            	 * Not closing connection or returning it back to pool will
            	 * cause connection leak if groovy script uses one of the 
            	 * EntityUtil, HibenrateUtil or HibernateProxy classes
            	 * and ends abnormally i.e. throws exception
            	 */
            	try (Connection conn = HibernateUtil.getConnection()) {
            		outputResult = HibernateProxy.execute(() -> {           		
            			binding.setVariable(Constants.GROOVY_BINDING_DB, conn);
                    
            			// execute script
            			return GroovyScript.execute(script, scriptName, cacheable, binding, args);
            		});
            	}
            } else {
            	outputResult = GroovyScript.execute(script, scriptName, cacheable, binding, args);
            }
        } catch (Throwable t) {
            Log.log.warn(String.format("Error %soccurred in executing groovy script %s", 
            						   (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
            						   scriptName));
            rc.isException = true;
            outputResult = Log.getStrackTrace(t);
        }
        
        //prepare the return
        result.put(HibernateConstants.GROOVY_OUTPUT_RESULT, outputResult);
        result.put(HibernateConstants.GROOVY_EXECEPTION, rc.isException);
        
        return result;
    }
}
