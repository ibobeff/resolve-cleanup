/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import com.resolve.services.util.WorksheetUtils;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This is a command prompt utility that executes a Runbook, ActionTask, or
 * Event
 * 
 * eg. of arguments set in the Eclipse:
 * 
 * -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -rb
 * "Doc3.Runbook Unable to Process Automation Request" -params
 * name1=val1&amp;name2=val2 -m SYNC -u admin -p resolve -protocol https -ip
 * 127.0.0.1 -port 8443 -rb "Doc3.Runbook Unable to Process Automation Request"
 * -params name1=val1&amp;name2=val2 -m SYNC -o DETAIL -u admin -p resolve -protocol
 * http -ip 127.0.0.1 -port 8080 -rb "Runbook.SampleRunbook" -s SYNC -f
 * C:/Jeet/development/params.properties -u admin -p resolve -protocol http -ip
 * 127.0.0.1 -port 8080 -at "Ping#test" -m SYNC -f
 * C:/Jeet/development/params.properties
 * 
 * -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -rb
 * "Runbook.SampleRunbook" -o DETAIL -verbose TRUE -f
 * C:/Jeet/development/params.properties -u admin -p resolve -protocol http -ip
 * 127.0.0.1 -port 8080 -rb "Runbook.SampleRunbook" -o DETAIL -verbose TRUE -f
 * C:/Jeet/development/params.properties -atd
 * "Ping#test(Runbook.SampleRunbook::4)" -u admin -p resolve -protocol http -ip
 * 127.0.0.1 -port 8080 -rb "Runbook.SampleRunbook" -o DETAIL -verbose TRUE -f
 * C:/Jeet/development/params.properties -atd "Ping#test" -c
 * C:/Jeet/development/connection.properties -rb "Runbook.SampleRunbook" -o
 * DETAIL -verbose TRUE -f C:/Jeet/development/params.properties -atd
 * "Ping#test"
 * 
 * //for ORacle DB, use this runbook -u admin -p resolve -protocol http -ip
 * 127.0.0.1 -port 8080 -rb "Test.test1" -m SYNC -d Y -f
 * C:/Jeet/development/params.properties -atd comment#resolve(Test.tes1::4)
 * 
 * //to abort a runbook -u admin -p resolve -protocol http -ip 127.0.0.1 -port
 * 8080 -rb "Runbook.SampleRunbook" -abort Y -pid
 * d372090fc6112272000fd8b94ed07c50
 * 
 * Sample of 'params.properties' file ==================================
 * ipaddress=172.16.5.205 ACTION=EXECUTEPROCESS
 * REDIRECT=/resolve/service/wiki/view/Test/test1
 * QUERYSTRING=PROBLEMID=NEW&amp;_username_=admin&amp;_password_=****
 * 
 * Sample of 'connection.properties' file. The 'key' strings are
 * CASE-INSENSITIVE ================================== username=admin
 * password1=**** ip=127.0.0.1 port=8080
 * 
 * 
 * 
 * URL to execute the Runbook from the Browser directly and forward it to
 * another runbook
 * http://127.0.0.1:8080/resolve/service/execute?_username_=admin
 * &amp;_password_=****
 * &amp;WIKI=Runbook.SampleRunbook&amp;PROBLEMID=NEW&amp;REDIRECT=%2Fresolve
 * %2Fservice%2Fwiki
 * %2Fview%2FTest%2Ftest1&amp;PROCESSID=CREATE&amp;ipaddress=172.16.5.205
 * &amp;QUERYSTRING=PROBLEMID%3DNEW%
 * 26_username_%3Dadmin%26_password_%3D****&amp;ACTION=EXECUTEPROCESS&amp;USERID=admi
 * n
 * 
 * Command line arguments to send an Event:
 * 
 * Persistent Event: -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080
 * -event true -eventid "MyEvent" -reference "MyReference" -rb
 * "Runbook.SampleRunbook" -wid "8a9482f13834602e013834686dc00006" -start true
 * -params name1=val1&amp;name2=val2 -u admin -p resolve -protocol http -ip
 * 127.0.0.1 -port 8080 -event true -eventid "MyEvent" -reference "MyReference"
 * -rb "Runbook.SampleRunbook" -wid "LOOKUP" -number "PRB1234-1" -start true
 * -params name1=val1&amp;name2=val2 -u admin -p resolve -protocol http -ip
 * 127.0.0.1 -port 8080 -event true -eventid "MyEvent" -reference "MyReference"
 * -rb "Runbook.SampleRunbook" -wid "LOOKUP" -alertid "MyAlert" -start true
 * -params name1=val1&amp;name2=val2 -u admin -p resolve -protocol http -ip
 * 127.0.0.1 -port 8080 -event true -eventid "MyEvent" -reference "MyReference"
 * -rb "Runbook.SampleRunbook" -wid "LOOKUP" -prbref "MyWorksheetRef" -start
 * true -params name1=val1&amp;name2=val2
 * 
 * Inmemory Event: -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080
 * -event true -eventid "MyEvent" -reference "MyReference" -rb
 * "Runbook.SampleRunbook" -params name1=val1&amp;name2=val2
 * 
 * -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -event true -f
 * C:/temp/params.properties
 * 
 * Note: params.properties can be structured as follows with every valid
 * combinations from above: eventid=MyEvent reference=MyReference
 * rb=Runbook.SampleRunbook wid="8a9482f13834602e013834686dc00006" start=true
 * params="name1=val1&amp;name2=val2"
 * 
 * Note: -event true doesn't really have any value than indicating this is
 * event, currently false doesn't mean it's not an event user can put "yes" as
 * well.
 * 
 * URL to send an event from the browser
 * http://127.0.0.1:8080/resolve/service/event
 * /execute?_username_=admin&amp;_password_
 * =****&amp;EVENT_EVENTID=MyEvent&amp;REFERENCE=MyReference
 * &amp;PROCESSID=MyProcessId&amp;EVENT_START=true&amp;WIKI=Runbook.SampleRunbook
 * 
 * @author jeet.marwah
 * 
 */
public class RSClientExecute
{
    private static final String CMD_USERNAME = "-u";
    private static final String CMD_P_ASSWORD = "-p";
    private static final String PROTOCOL = "-protocol";
    private static final String IP = "-ip";
    private static final String ACTIONTASK = "-at";
    private static final String RUNBOOK = "-rb";
    private static final String ABORT_RUNBOOK = "-abort";

    private static final String EVENT = "-event";
    private static final String EVENT_ID = "-eventid";
    private static final String EVENT_REFERENCE = "-reference";
    private static final String EVENT_END = "-eventend";

    private static final String CONNECTION_PROPERTIES = "-c";
    private static final String PORT = "-port";
    private static final String PROPERTIES_FILE = "-f";
    private static final String PROPERTIES_PARAMS = "-params";
    private static final String IS_SYNC = "-m";
    private static final String IS_DETAIL = "-o";
    private static final String ACTIONTASK_DETAIL_AS_OUTPUT = "-atd";
    private static final String PROCESSID = "-pid";

    private static final String PROBLEMID = "-wid";
    private static final String WORKSHEET_REFERENCE = "-prbref";
    private static final String WORKSHEET_NUMBER = "-number";
    private static final String WORKSHEET_ALERTID = "-alertid";

    private static final String VERBOSE = "-v";

    private static final String URI_PATH_RUNBOOK_ABORT = "/resolve/service/runbook/abort?";
    private static final String URI_PATH_RUNBOOK_START = "/resolve/service/runbook/start?";
    private static final String URI_PATH_RUNBOOK_EXECUTE = "/resolve/service/runbook/execute?";
    private static final String URI_PATH_ACTIONTASK_START = "/resolve/service/actiontask/start?";
    private static final String URI_PATH_ACTIONTASK_EXECUTE = "/resolve/service/actiontask/execute?";
    private static final String URI_PATH_EVENT_EXECUTE = "/resolve/service/event/execute?";

    private static final int DEFAULT_TIMEOUT = 10;// in mins
    private static final int CLIENT_BUFFER_TIMEOUT_IN_SECS = 10;// in secs
    private final static String LOOKUP = "LOOKUP";

    private final static String USERNAME = "_username_";
    private final static String P_ASSWORD = "_password_";
    private final static String DETAIL_OUTPUT_FLAG = "DETAIL_OUTPUT_FLAG";
    private final static String ACTIONTASK_DETAIL_OUTPUT = "ACTIONTASK_DETAIL_OUTPUT";

    // constants used in connection.properties file
    private final static String CONN_USERNAME = "username";
    private final static String CONN_P_ASSWORD = "password";
    private final static String CONN_IP = "ip";
    private final static String CONN_PORT = "port";
    private final static String HTTP_PROTOCOL = "http";
    private final static String HTTPS_PROTOCOL = "https";

    private static boolean isVerbose = false;

    private static final Map<String, String> eventKeyMap = new HashMap<String, String>();

    static
    {
        eventKeyMap.put(EVENT_ID, Constants.EVENT_EVENTID);
        eventKeyMap.put(EVENT_REFERENCE, Constants.EVENT_REFERENCE);
        eventKeyMap.put(PROCESSID, Constants.EVENT_EVENTID);
        eventKeyMap.put(RUNBOOK, Constants.HTTP_REQUEST_WIKI);
        eventKeyMap.put(PROBLEMID, Constants.EXECUTE_PROBLEMID);
        eventKeyMap.put(WORKSHEET_ALERTID, WorksheetUtils.ALERTID);
        eventKeyMap.put(WORKSHEET_NUMBER, WorksheetUtils.NUMBER);
        eventKeyMap.put(WORKSHEET_REFERENCE, WorksheetUtils.REFERENCE);
    }
	
	/*
    public static void mainY(String[] args)
    {
        Log.init();
        try
        {
            List<String> listOfArgs = new ArrayList<String>();
            listOfArgs.add("-u");
            listOfArgs.add("admin");
            listOfArgs.add("-p");
            listOfArgs.add("resolve");
            listOfArgs.add("-protocol");
            listOfArgs.add("http");
            listOfArgs.add("-ip");
            listOfArgs.add("127.0.0.1");
            listOfArgs.add("-port");
            listOfArgs.add("8888");
            listOfArgs.add("-event");
            listOfArgs.add("true");

            listOfArgs.add("-eventid");
            listOfArgs.add("MyEvent");
            listOfArgs.add("-reference");
            listOfArgs.add("MyReference");
            listOfArgs.add("-rb");
            listOfArgs.add("bipul.TestEvent");
            listOfArgs.add("-wid");
            listOfArgs.add("LOOKUP");
            listOfArgs.add("-number");
            listOfArgs.add("PRB990-5");
            listOfArgs.add("-start");
            listOfArgs.add("true");
            listOfArgs.add("-params");
            listOfArgs.add("name1=val1&name2=val2");

            // listOfArgs.add("-f");
            // listOfArgs.add("c:/temp/params.properties");

            commandLine(listOfArgs);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }

    }// main
	*/

    public static void main(String[] args)
    {
        Log.init();
        try
        {
            if (args == null || args.length < 5)
            {
                showHelp();
            }
            else
            {
                List<String> listOfArgs = convertArrayToList(args);
                isVerbose = isVerbose(listOfArgs);

                if (isVerbose)
                {
                    System.out.println("Following are the parameters:");
                    System.out.println(Arrays.toString(args));
                }
                commandLine(listOfArgs);
            }
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }

    }// main

    /**
     * This is SYNCHRONOUS API for executing an ACTIONTASK. So it will wait to
     * fetch the results
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @return
     * @throws Exception
     */
    public static String executeActionTask(String protocol, String ipaddress, long portNumber, String username, String password, Map<String, String> params) throws Exception
    {
        StringBuffer url = new StringBuffer(generateUrl(protocol, ipaddress, portNumber, URI_PATH_ACTIONTASK_EXECUTE, username, password, params));
        int processTimeoutInSecs = getProcessTimeoutInSecs(params.get(Constants.EXECUTE_PROCESS_TIMEOUT));
        // client should wait a little more then the server
        processTimeoutInSecs += CLIENT_BUFFER_TIMEOUT_IN_SECS;

        return send(url.toString(), processTimeoutInSecs);

    }// executeActionTask

    /**
     * ExecuteActionTask with the specified ActionTask name
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param actionTask
     * @param params
     * @return
     * @throws Exception
     */
    public static String executeActionTask(String protocol, String ipaddress, long portNumber, String username, String password, String actionTask, Map<String, String> params) throws Exception
    {
        params.put(Constants.EXECUTE_ACTIONNAME, actionTask);
        return executeActionTask(protocol, ipaddress, portNumber, username, password, params);
    } // executeActionTask

    /**
     * ExecuteActionTask and return the "detail" of the ActionTask result
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @return
     * @throws Exception
     */
    public static String executeActionTask(String protocol, String ipaddress, long portNumber, String username, String password, String actionTask, Map<String, String> params, boolean detail) throws Exception
    {
        if (detail)
        {
            params.put(DETAIL_OUTPUT_FLAG, "Y");
        }
        return executeActionTask(protocol, ipaddress, portNumber, username, password, actionTask, params);
    } // executeActionTask

    public static String executeActionTask(String protocol, String ipaddress, long portNumber, String username, String password, String actionTask, Map<String, String> params, String actionTaskDefinition) throws Exception
    {
        if (actionTaskDefinition != null && !actionTaskDefinition.equals(""))
        {
            params.put(ACTIONTASK_DETAIL_OUTPUT, actionTaskDefinition);
        }
        return executeActionTask(protocol, ipaddress, portNumber, username, password, actionTask, params);
    } // executeActionTask

    /**
     * This is a ASYNCHRONOUS API which will submit a request to execute
     * ActionTask
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @return
     * @throws Exception
     */
    public static String startActionTask(String protocol, String ipaddress, long portNumber, String username, String password, Map<String, String> params) throws Exception
    {
        StringBuffer url = new StringBuffer(generateUrl(protocol, ipaddress, portNumber, URI_PATH_ACTIONTASK_START, username, password, params));
        int processTimeoutInSecs = getProcessTimeoutInSecs(params.get(Constants.EXECUTE_PROCESS_TIMEOUT));
        // client should wait a little more then the server
        processTimeoutInSecs += CLIENT_BUFFER_TIMEOUT_IN_SECS;

        return send(url.toString(), processTimeoutInSecs);
    }// startActionTask

    /**
     * Execute the specified ActionTask but dont wait for the result.
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param actionTask
     * @param params
     * @throws Exception
     */
    public static void startActionTask(String protocol, String ipaddress, long portNumber, String username, String password, String actionTask, Map<String, String> params) throws Exception
    {
        params.put(Constants.EXECUTE_ACTIONNAME, actionTask);
        startActionTask(protocol, ipaddress, portNumber, username, password, params);
    }// startActionTask

    /**
     * submit a request to abort a runbook
     * 
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @throws Exception
     */
    public static String abortRunbook(String protocol, String ipaddress, long portNumber, String username, String password, Map<String, String> params) throws Exception
    {
        StringBuffer url = new StringBuffer(generateUrl(protocol, ipaddress, portNumber, URI_PATH_RUNBOOK_ABORT, username, password, params));
        int processTimeoutInSecs = getProcessTimeoutInSecs(params.get(Constants.EXECUTE_PROCESS_TIMEOUT));
        // client should wait a little more then the server
        processTimeoutInSecs += CLIENT_BUFFER_TIMEOUT_IN_SECS;

        return send(url.toString(), processTimeoutInSecs);
    }// abortRunbook

    public static String abortRunbook(String protocol, String ipaddress, long portNumber, String username, String password, String wiki, String processid) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.HTTP_REQUEST_WIKI, wiki);
        params.put(Constants.EXECUTE_PROCESSID, processid);
        return abortRunbook(protocol, ipaddress, portNumber, username, password, params);
    }// abortRunbook

    /**
     * This is SYNCHRONOUS API for executing a RUNBOOK . So it will wait to
     * fetch the results
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @return
     * @throws Exception
     */
    public static String executeRunbook(String protocol, String ipaddress, long portNumber, String username, String password, Map<String, String> params) throws Exception
    {
        // set PROBLEMID to NEW if not defined
        if (!params.containsKey(Constants.EXECUTE_PROBLEMID))
        {
            params.put(Constants.EXECUTE_PROBLEMID, "NEW");
        }

        StringBuffer url = new StringBuffer(generateUrl(protocol, ipaddress, portNumber, URI_PATH_RUNBOOK_EXECUTE, username, password, params));
        int processTimeoutInSecs = getProcessTimeoutInSecs(params.get(Constants.EXECUTE_PROCESS_TIMEOUT));
        // client should wait a little more then the server
        processTimeoutInSecs += CLIENT_BUFFER_TIMEOUT_IN_SECS;

        return send(url.toString(), processTimeoutInSecs);

    }// executeRunbook

    public static String executeRunbook(String protocol, String ipaddress, long portNumber, String username, String password, String wiki, Map<String, String> params) throws Exception
    {
        params.put(Constants.HTTP_REQUEST_WIKI, wiki);
        return executeRunbook(protocol, ipaddress, portNumber, username, password, params);
    } // executeRunbook

    /**
     * ExecuteRunbook with the results from "detail"
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @param detail
     * @return
     * @throws Exception
     */
    public static String executeRunbook(String protocol, String ipaddress, long portNumber, String username, String password, String wiki, Map<String, String> params, boolean detail) throws Exception
    {
        if (detail)
        {
            params.put(DETAIL_OUTPUT_FLAG, "Y");
        }
        return executeRunbook(protocol, ipaddress, portNumber, username, password, wiki, params);
    }// executeRunbook

    /**
     * ExecuteRunbook with the results from "detail" for a specific ActionTask
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @param actionTaskDefinition
     * @return
     * @throws Exception
     */
    public static String executeRunbook(String protocol, String ipaddress, long portNumber, String username, String password, String wiki, Map<String, String> params, String actionTaskDefinition) throws Exception
    {
        if (actionTaskDefinition != null && !actionTaskDefinition.equals(""))
        {
            params.put(ACTIONTASK_DETAIL_OUTPUT, actionTaskDefinition);
        }
        return executeRunbook(protocol, ipaddress, portNumber, username, password, wiki, params);
    }// executeRunbook

    /**
     * This is a ASYNCHRONOUS API which will submit a request to execute RUNBOOK
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     * @return
     * @throws Exception
     */
    public static String startRunbook(String protocol, String ipaddress, long portNumber, String username, String password, Map<String, String> params) throws Exception
    {
        // set PROBLEMID to NEW if not defined
        if (!params.containsKey(Constants.EXECUTE_PROBLEMID))
        {
            params.put(Constants.EXECUTE_PROBLEMID, "NEW");
        }

        StringBuffer url = new StringBuffer(generateUrl(protocol, ipaddress, portNumber, URI_PATH_RUNBOOK_START, username, password, params));
        int processTimeoutInSecs = getProcessTimeoutInSecs(params.get(Constants.EXECUTE_PROCESS_TIMEOUT));
        // client should wait a little more then the server
        processTimeoutInSecs += CLIENT_BUFFER_TIMEOUT_IN_SECS;

        return send(url.toString(), processTimeoutInSecs);
    }// startRunbook

    public static String startRunbook(String protocol, String ipaddress, long portNumber, String username, String password, String wiki, Map<String, String> params) throws Exception
    {
        // runbook wiki
        params.put(Constants.HTTP_REQUEST_WIKI, wiki);

        return startRunbook(protocol, ipaddress, portNumber, username, password, params);
    }// startRunbook

    public static String commandLine(List<String> listOfArgs) throws Exception
    {
        String result = null;

        if (validate(listOfArgs))
        {
            boolean isSync = isSynchronous(listOfArgs);
            String protocol = getProtocol(listOfArgs);
            String ipaddress = getIpAdddress(listOfArgs);
            long portNumber = getPortNumber(protocol, listOfArgs);
            String username = getUsername(listOfArgs);
            String password = getPassword(listOfArgs);
            boolean showDetail = showDetail(listOfArgs);
            String actionTaskDetailOutput = getActionTaskDetailOutput(listOfArgs);
            boolean isRunbookAbort = isRunbookAbort(listOfArgs);
            boolean isSendEvent = isSendEvent(listOfArgs);

            Map<String, String> params = getParams(listOfArgs);
            params.put(Constants.EXECUTE_USERID, username);

            // if showDetail is true and isSync is false, then throw exception.
            // If its DETAIL, then it has to be SYNC
            if (showDetail && !isSync)
            {
                throw new Exception("When using -o(Detail) option, the execution should be SYNC (which is by default).");
            }

            // if abort command
            if (isSendEvent)
            {
                result = sendEvent(protocol, ipaddress, portNumber, username, password, params);
            }
            else if (isRunbookAbort)
            {
                String processId = getProcessId(listOfArgs);
                params.put(Constants.EXECUTE_PROCESSID, processId);

                if (!params.containsKey(Constants.HTTP_REQUEST_WIKI))
                {
                    throw new Exception("The option -rb is mandatory for abort");
                }

                // submit the command
                result = abortRunbook(protocol, ipaddress, portNumber, username, password, params);
            }
            else
            {
                // set worksheet
                String problemId = getProblemId(listOfArgs);
                params.put(Constants.EXECUTE_PROBLEMID, problemId);

                if (params.containsKey(Constants.HTTP_REQUEST_WIKI))
                {

                    params.put(Constants.EXECUTE_PROCESSID, "CREATE");
                    if (isSync)
                    {
                        // output type detail
                        if (showDetail)
                        {
                            params.put(DETAIL_OUTPUT_FLAG, "Y");
                        }

                        // detail output for specific actiontask
                        if (actionTaskDetailOutput != null && !actionTaskDetailOutput.equals(""))
                        {
                            params.put(ACTIONTASK_DETAIL_OUTPUT, actionTaskDetailOutput);
                        }
                        result = executeRunbook(protocol, ipaddress, portNumber, username, password, params);
                    }
                    else
                    {
                        result = startRunbook(protocol, ipaddress, portNumber, username, password, params);
                    }

                }
                else
                {
                    if (isSync)
                    {
                        // output type detail
                        if (showDetail)
                        {
                            params.put(DETAIL_OUTPUT_FLAG, "Y");
                        }
                        // detail output for specific actiontask
                        if (actionTaskDetailOutput != null && !actionTaskDetailOutput.equals(""))
                        {
                            params.put(ACTIONTASK_DETAIL_OUTPUT, actionTaskDetailOutput);
                        }
                        result = executeActionTask(protocol, ipaddress, portNumber, username, password, params);
                    }
                    else
                    {
                        result = startActionTask(protocol, ipaddress, portNumber, username, password, params);
                    }
                }

            }// end of if -else
        }
        return result;
    }// commandLine

    /**
     * 
     * egs of URI
     * http://127.0.0.1:8080/resolve/service/runbook/execute?_username_
     * =admin&amp;_password_
     * =resolve&amp;WIKI=Test.test1&amp;PROBLEMID=NEW&amp;DETAIL_OUTPUT_FLAG
     * =Y&amp;ACTIONTASK_DETAIL_OUTPUT
     * =comment%23resolve%28Test.tes1%3A%3A4%29&amp;PROCESSID
     * =CREATE&amp;ipaddress=172.16.5.205&amp;USERID=admin
     * http://127.0.0.1:8080/resolve/
     * service/execute?_username_=admin&amp;_password_=
     * ****&amp;ACTION=EXECUTEPROCESS&amp;
     * WIKI=Runbook.SampleRunbook&amp;PROBLEMID=NEW&amp;PROCESSID
     * =CREATE&amp;ipaddress=172.16.5.205
     * http://127.0.0.1:8080/resolve/service/runbook
     * /start?_username_=admin&amp;_password_
     * =resolve&amp;ACTION=EXECUTEPROCESS&amp;WIKI=Runbook
     * .SampleRunbook&amp;PROBLEMID=NEW&amp;PROCESSID=CREATE&amp;ipaddress=172.16.5.205
     * 
     * 
     * URL to execute the Runbook from the Browser directly and forward it to
     * another runbook
     * http://127.0.0.1:8080/resolve/service/execute?_username_=admin
     * &amp;_password_=
     * ****&amp;WIKI=Runbook.SampleRunbook&amp;PROBLEMID=NEW&amp;REDIRECT=%2Fresolve
     * %2Fservice
     * %2Fwiki%2Fview%2FTest%2Ftest1&amp;PROCESSID=CREATE&amp;ipaddress=172.16.5
     * .205&amp;QUERYSTRING=PROBLEMID%3DNEW%
     * 26_username_%3Dadmin%26_password_%3D****&amp;ACTION=EXECUTEPROCESS&amp;USERID=admi
     * n
     * 
     * @param uri
     * @param timeout
     * @return
     * @throws Exception
     */
    private static String send(String uri, int timeout) throws Exception
    {
        StringBuilder response = new StringBuilder();
        try
        {
            // URL url = new URL(uri + "&_logout_=true");
            URL url = new URL(uri);
            Map<String,String> owasp = getOwaspToken(url,timeout);
            HttpURLConnection conn = null;
            if (isVerbose)
            {
                System.out.println("Opening Connection: " + uri);
                System.out.println("  timeout(secs): " + timeout);
            }

            if (url.getProtocol().equals(HTTPS_PROTOCOL))
            {
                // Sometime SSL communication may need the actual host name
                // (FQDN), need to see if the following is required.
                // String actualServerName = SecurityUtil.getServerName(uri);
                String actualServerName = url.getHost();
                URL sslActualUrl = new URL(url.getProtocol() + "://" + actualServerName + ":" + url.getPort() + url.getFile());
                conn = (HttpsURLConnection)sslActualUrl.openConnection();
            }
            else
            {
            	conn = (HttpURLConnection)url.openConnection();
            }
            // 0 is timout to
            // infinity...for testing
            // only
            // conn.addRequestProperty("Content-Length",
            // Integer.toString(uri.length()));

            // only send a message if there is a message to send
            // BufferedWriter writer = new BufferedWriter(new
            // OutputStreamWriter(conn.getOutputStream(), "UTF8"));
            // writer.flush();
            // writer.close();
        	String line = "";
            conn.setDoOutput(true);
            conn.setConnectTimeout(timeout * 1000);
            conn.addRequestProperty("X-Requested-With", "XMLHttpRequest");
            for(String key : owasp.keySet()) 
            {
            	conn.addRequestProperty(key, owasp.get(key));
            }
            if (isVerbose)
            {
                System.out.println("Getting response");
            }

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream())))
            {
                while ((line = br.readLine()) != null)
                {
                    response.append(line).append("\n");
                }   	
            }

            if (isVerbose)
            {
                System.out.println("HTTPConnect Response Successful. Here is the output:");
            }
            System.out.println(response.toString());
        }
        catch (Exception e)
        {
            System.out.println("HTTPConnect send fail: " + e.getMessage());
            throw e;
        }
        return response.toString();
    }// send a message to a uri, get a response
    
    private static Map<String,String> getOwaspToken(URL url,int timeout) throws IOException
    {
    	URL owaspUrl = new URL(url.getProtocol()+"://"+url.getHost()+":"+url.getPort()+"/resolve/JavaScriptServlet");
    	HttpURLConnection conn = null;
    	if("https".equals(owaspUrl.getProtocol().toLowerCase()))
    	{
    		conn = (HttpsURLConnection)owaspUrl.openConnection();
    	}
    	else
    	{
    		conn = (HttpURLConnection)owaspUrl.openConnection();
    	}
        conn.setRequestMethod("POST");
        conn.addRequestProperty("X-Requested-With", "XMLHttpRequest");
        conn.addRequestProperty("FETCH-CSRF-TOKEN", "1");
        conn.setDoOutput(true);
        conn.setConnectTimeout(timeout * 1000);
        String line = "";
        Map<String,String> owaspTokens = new HashMap<String,String>();
        owaspTokens.put("Cookie",conn.getHeaderField("Set-Cookie"));
        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream())))
        {
            while ((line = br.readLine()) != null)
            {
                if(line.contains("OWASP_CSRFTOKEN"))
                {
                	owaspTokens.put("OWASP_CSRFTOKEN",line.split(":")[1]);
                }
            }
        }
        return owaspTokens;
    }
    /**
     * make sure that there is no null
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @return
     */
    private static String generateUrl(String protocol, String ipaddress, long portNumber, String URI_PATH, String username, String password, Map<String, String> params) throws Exception
    {
        // StringBuffer url = new StringBuffer("http://");
        StringBuffer url = new StringBuffer(protocol);
        url.append("://");

        // http://127.0.0.1/
        url.append(ipaddress);

        // http://127.0.0.1:8080 - This is Optional
        url.append(":").append(portNumber);

        // http://127.0.0.1:8080/resolve/service/execute?
        url.append(URI_PATH);

        // username
        url.append(USERNAME).append("=").append(username);

        // password
        url.append("&").append(P_ASSWORD).append("=").append(password);

        // params
        url.append(prepareQueryStringFromMap(params));

        return url.toString();
    }// generateUrl

    private static String prepareQueryStringFromMap(Map<String, String> params) throws Exception
    {
        StringBuffer queryString = new StringBuffer();

        Iterator<String> it = params.keySet().iterator();
        while (it.hasNext())
        {
            String key = it.next();
            String value = params.get(key);
            if (value != null && value.trim().length() > 0)
            {
                queryString.append("&").append(key).append("=").append(URLEncoder.encode(value, "UTF8"));
            }
        }

        return queryString.toString();
    }// prepareQueryStringFromMap

    private static void display(String[] args)
    {
        int count = 0;
        for (String str : args)
        {
            System.out.println(count + "==>" + str);
            count++;
        }
    }// display

    /**
     * 
     * To abort, here is the example java -jar rsexec.jar -u username -p
     * password -rb "Test.test1" -abort Y -pid d372090fc6112272000fd8b94ed07c50
     * 
     * Usage: java -jar rsexec.jar -u @lt;username&gt; -p @lt;password&gt; -ip @lt;IPADDRESS&gt;
     * -port @lt;PORT&gt; @lt;-at|rb&gt; @lt;actiontask|runbook&gt; -params
     * @lt;name1=val1&amp;name2=val2&amp;...&gt; -s @lt;SYNC | ASYNC&gt;
     * 
     * where options include:
     * 
     * MANDATORY: --------- -u username -p password -ip IP address -at
     * ActionTask -rb Runbook -abort Y | N, Abort Runbook that is executing with
     * a process Id. -pid and -rb is mandatory if this option is used.
     * 
     * OPTIONAL: --------- -f name of the properties file -params params as a
     * string (name1=val1&amp;name2=val2&amp;...) -port port (eg. 8080, default is 80)
     * -s SYNC | ASYNC , indicates command to be SYNCHRONOUSLY or
     * ASYNCHRONOUSLY. SYNC by default. -o SUMMARY | DETAIL , for output.
     * SUMMARY is default -atd actiontask detail output in HTML format - syntax
     * ==&gt;[task#namespace(wiki::nodeId) OR task#namespace] -pid ProcessId,
     * madatory for -abort option -wid NEW | ACTIVE | LOOKUP, Worksheet ID.
     * LOOKUP will use other params fields to determine the worksheet id -v TRUE
     * | FALSE, verbose to show details. FALSE by default.
     * 
     * example : --------- java -jar rsexec.jar -u admin -p resolve -ip
     * 127.0.0.1 -port 8080 -rb
     * \"Doc3.Process Automation Request\" -params "name1=val1&amp;name2=val2" -m
     * SYNC java -jar rsexec.jar -u admin -p resolve -ip 127.0.0.1 -port 8080
     * -rb \"Doc3.Process Automation Request\" -f params.properties java -jar
     * rsexec.jar -u admin -p resolve -ip 127.0.0.1 -port 8080 -rb
     * \"Doc3.Process Automation Request\" -f params.properties -m SYNC -o
     * DETAIL java -jar rsexec.jar -u admin -p resolve -ip 127.0.0.1 -port 8080
     * -rb \"Doc3.Process Automation Request\" -abort Y -pid
     * d372090fc6112272000fd8b94ed07c50
     * 
     * 
     * Final URL that gets generated:
     * http://127.0.0.1:8080/resolve/service/wsexecute
     * ?_username_=admin&amp;_password_
     * =resolve&amp;WIKI=Runbook.WebHome&amp;ACTION=EXECUTEPROCESS&amp;name1=val1&amp;name2=val2
     * 
     * @param args
     */

    private static void showHelp()
    {
        System.out.println("Usage: execute.(bat|sh) -u <username> -p <password> -protocol http -ip <IPADDRESS> -port <PORT> <-at|rb> <actiontask|runbook> -params <name1=val1&name2=val2&...> -s <SYNC | ASYNC> ");
        System.out.println("");
        System.out.println("where options include:");
        System.out.println("");
        System.out.println(" MANDATORY:");
        System.out.println(" ---------");
        System.out.println("    -u            username");
        System.out.println("    -p            password");
        System.out.println("    -ip           IP address");
        System.out.println("    -at           ActionTask");
        System.out.println("    -rb           Runbook");
        System.out.println("    -event        true, indicates that it is a event execution.");
        System.out.println("    -abort        Y | N, Abort Runbook that is executing with a process Id. -pid and -rb is mandatory if this option is used. ");
        System.out.println("");
        System.out.println(" OPTIONAL:");
        System.out.println(" ---------");
        System.out.println("    -protocol   http or https protocol to be used, if not provided http is used ");
        System.out.println("    -f          name of the params.properties file ");
        System.out.println("    -c          name of the connection.properties file ");
        System.out.println("    -params     params as a string (name1=val1&name2=val2&...)");
        System.out.println("    -port       port (eg. 8080 or 8443 (ssl), default is 80 or 443(for ssl)");
        System.out.println("    -m          SYNC | ASYNC , indicates command to be SYNCHRONOUSLY or ASYNCHRONOUSLY. SYNC by default.");
        System.out.println("    -o          SUMMARY | DETAIL , for output. SUMMARY is default");
        System.out.println("    -atd        actiontask detail output in HTML format - syntax ==>[task#namespace(wiki::nodeId)]");
        System.out.println("    -pid        Process ID, madatory for -abort option");
        System.out.println("    -wid        NEW | ACTIVE | LOOKUP, Worksheet ID. LOOKUP will use other params fields to determine the worksheet id");
        System.out.println("    -v          TRUE | FALSE, verbose to show details. FALSE by default.");
        System.out.println("    -eventid    Event ID");
        System.out.println("    -reference  Event Reference");
        System.out.println("    -start      TRUE | FALSE, indicates if event will start.");
        System.out.println("");
        System.out.println(" example : ");
        System.out.println(" ---------");
        System.out.println(" execute.(bat|sh) -u admin -p resolve -ip 127.0.0.1 -port 8080 -rb \"Doc3.Process Automation Request\" -params name1=val1&name2=val2 -m SYNC ");
        System.out.println(" execute.(bat|sh) -u admin -p resolve -ip 127.0.0.1 -port 8080 -rb \"Doc3.Process Automation Request\" -f params.properties ");
        System.out.println(" execute.(bat|sh) -u admin -p resolve -ip 127.0.0.1 -port 8080 -rb \"Doc3.Process Automation Request\" -f params.properties -m SYNC -o DETAIL ");
        System.out.println(" execute.(bat|sh) -u admin -p resolve -ip 127.0.0.1 -port 8080 -rb \"Doc3.Process Automation Request\" -abort Y -pid d372090fc6112272000fd8b94ed07c50 ");
        System.out.println(" execute.(bat|sh) -c C:/development/connection.properties -rb \"Runbook.SampleRunbook\" -o DETAIL -verbose TRUE -f C:/development/params.properties -atd \"Ping#test\" ");
        System.out.println(" ");
        System.out.println(" Persistent event");
        System.out.println(" ---------");
        System.out.println(" -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -event true -eventid \"MyEvent\" -reference \"MyReference\" -rb \"Runbook.SampleRunbook\" -wid \"8a9482f13834602e013834686dc00006\" -start true -params \"name1=val1&name2=val2\"");
        System.out.println(" -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -event true -eventid \"MyEvent\" -reference \"MyReference\" -rb \"Runbook.SampleRunbook\" -wid \"LOOKUP\" -number \"PRB1234-1\" -start true -params \"name1=val1&name2=val2\"");
        System.out.println(" -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -event true -eventid \"MyEvent\" -reference \"MyReference\" -rb \"Runbook.SampleRunbook\" -wid \"LOOKUP\" -alertid \"MyAlert\" -start true -params \"name1=val1&name2=val2\"");
        System.out.println(" -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -event true -eventid \"MyEvent\" -reference \"MyReference\" -rb \"Runbook.SampleRunbook\" -wid \"LOOKUP\" -prbref \"MyWorksheetRef\" -start true -params \"name1=val1&name2=val2\"");
        System.out.println(" ");
        System.out.println(" Inmemory event");
        System.out.println(" ---------");
        System.out.println(" -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -event true -eventid \"MyEvent\" -reference \"MyReference\" -rb \"Runbook.SampleRunbook\" -params \"name1=val1&name2=val2\"");
        System.out.println(" -u admin -p resolve -protocol http -ip 127.0.0.1 -port 8080 -event true -f C:/temp/params.properties");
        System.out.println(" ");
        System.out.println(" Note: params.properties can be structured as follows with every valid combinations from above:");
        System.out.println(" -eventid=MyEvent");
        System.out.println(" -reference=MyReference");
        System.out.println(" -rb=Runbook.SampleRunbook");
        System.out.println(" -wid=LOOKUP");
        System.out.println(" -number=PRB1234-1");
        System.out.println(" -start=true");
        System.out.println(" -params=name1=val1&name2=val2");
        System.out.println(" ");
        System.out.println(" Note: -event true indicates this is a send event call.");

        System.out.println(" ");
    }// showHelp

    private static List<String> convertArrayToList(String[] args)
    {
        List<String> list = new ArrayList<String>();
        for (String str : args)
        {
            list.add(str);
        }

        return list;
    }// convertArrayToList

    private static boolean validate(List<String> listOfArgs) throws Exception
    {
        boolean validated = true;

        // read the connection file if there is any
        readConnectionPropertiesFile(listOfArgs);

        // validation for mandatory values
        if (!listOfArgs.contains(CMD_USERNAME) || !listOfArgs.contains(CMD_P_ASSWORD) || !listOfArgs.contains(IP))
        {
            System.out.println("One of the mandatory parameter is missing. Please review the following help:");
            showHelp();
            validated = false;
        }

        return validated;
    }// validate

    private static boolean isSynchronous(List<String> listOfArgs)
    {
        boolean isSync = true;// DEFAULT TO SYNC

        int index = listOfArgs.indexOf(IS_SYNC);
        if (index > -1)
        {
            String sync = listOfArgs.get(index + 1);
            isSync = (sync != null && sync.trim().equalsIgnoreCase("SYNC")) ? true : false;
        }

        return isSync;

    }// isSynchronous

    private static String getProtocol(List<String> listOfArgs) throws Exception
    {
        String result = "http";
        int index = listOfArgs.indexOf(PROTOCOL);
        if (index > -1)
        {
            String protocol = listOfArgs.get(index + 1);
            if (StringUtils.isNotEmpty(protocol))
            {
                result = protocol.toLowerCase();
            }
        }
        return result;
    }// getProtocol

    private static String getIpAdddress(List<String> listOfArgs) throws Exception
    {
        String errorMsg = "IP address(-ip) is mandatory. Please provide the ip address.";

        int index = listOfArgs.indexOf(IP);
        if (index < 0)
        {
            throw new Exception(errorMsg);
        }

        String ip = listOfArgs.get(index + 1);
        if (ip == null || ip.trim().length() == 0)
        {
            throw new Exception(errorMsg);
        }

        return ip;

    }// getIpAdddress

    private static long getPortNumber(String protocol, List<String> listOfArgs) throws Exception
    {
        String errorMsg = "Require valid HTTP Port number (-port) e.g. 8080";
        long port = 80;

        int index = listOfArgs.indexOf(PORT);
        if (index < 0)
        {
            if (protocol.equalsIgnoreCase("https"))
            {
                port = 443;
            }
        }
        else
        {
            String portStr = listOfArgs.get(index + 1);
            try
            {
                port = Long.parseLong(portStr);
            }
            catch (Exception e)
            {
                throw new Exception(errorMsg, e);
            }

        }

        return port;

    }// getPortNumber

    private static String getUsername(List<String> listOfArgs) throws Exception
    {
        String errorMsg = "Username(-u) is mandatory. Please provide the username.";

        int index = listOfArgs.indexOf(CMD_USERNAME);
        if (index < 0)
        {
            throw new Exception(errorMsg);
        }

        String username = listOfArgs.get(index + 1);
        if (username == null || username.trim().length() == 0)
        {
            throw new Exception(errorMsg);
        }

        return username;
    }// getUsername

    private static String getPassword(List<String> listOfArgs) throws Exception
    {
        String errorMsg = "Password(-p) is mandatory. Please provide the Password.";

        int index = listOfArgs.indexOf(CMD_P_ASSWORD);
        if (index < 0)
        {
            throw new Exception(errorMsg);
        }

        String password = listOfArgs.get(index + 1);
        if (password == null || password.trim().length() == 0)
        {
            throw new Exception(errorMsg);
        }

        return password;
    }// getUsername

    private static String getActionTaskDetailOutput(List<String> listOfArgs) throws Exception
    {
        String actionTaskDetailOutput = null;

        int index = listOfArgs.indexOf(ACTIONTASK_DETAIL_AS_OUTPUT);
        if (index > -1)
        {
            actionTaskDetailOutput = listOfArgs.get(index + 1);
            if (actionTaskDetailOutput != null && actionTaskDetailOutput.trim().length() > 0)
            {
                if (actionTaskDetailOutput.indexOf("#") < 0)
                {
                    throw new Exception(actionTaskDetailOutput + " is not a valid value. Please check the syntax for option [" + ACTIONTASK_DETAIL_AS_OUTPUT + "].");
                }
                else if (actionTaskDetailOutput.indexOf("(") > 0 && actionTaskDetailOutput.indexOf("::") < 0)
                {
                    throw new Exception(actionTaskDetailOutput + " is not a valid value. String inside the () is not valid as it should contain '::'.");
                }
            }
            else
            {
                throw new Exception("The option [" + ACTIONTASK_DETAIL_AS_OUTPUT + "] does not have a value set.");
            }
        }

        return actionTaskDetailOutput;

    }// getActionTaskDetailOutput

    private static String getProcessId(List<String> listOfArgs) throws Exception
    {
        String processId = null;

        int index = listOfArgs.indexOf(PROCESSID);
        if (index > -1)
        {
            processId = listOfArgs.get(index + 1);
            if (processId == null || processId.trim().length() == 0)
            {
                throw new Exception("The option [" + PROCESSID + "] does not have a value set. For ABORT, it is mandatory.");
            }
        }

        return processId;

    }// getProcessId

    private static String getProblemId(List<String> listOfArgs) throws Exception
    {
        String problemId = "NEW";

        int index = listOfArgs.indexOf(PROBLEMID);
        if (index > -1)
        {
            problemId = listOfArgs.get(index + 1);
            if (problemId == null || problemId.trim().length() == 0)
            {
                throw new Exception("The option [" + PROBLEMID + "] does not have a value set.");
            }
        }

        return problemId;
    }// getProblemId

    private static boolean showDetail(List<String> listOfArgs)
    {
        boolean showDetail = false;

        int index = listOfArgs.indexOf(IS_DETAIL);
        if (index > -1)
        {
            String detailFlag = listOfArgs.get(index + 1);
            if (detailFlag != null && detailFlag.trim().equalsIgnoreCase("DETAIL"))
            {
                showDetail = true;
            }
        }

        return showDetail;
    }// showDetail

    private static boolean isRunbookAbort(List<String> listOfArgs)
    {
        boolean isRunbookAbort = false;

        int index = listOfArgs.indexOf(ABORT_RUNBOOK);
        if (index > -1)
        {
            String detailFlag = listOfArgs.get(index + 1);
            if (detailFlag != null && detailFlag.trim().equalsIgnoreCase("Y"))
            {
                isRunbookAbort = true;
            }
        }

        return isRunbookAbort;
    }// isRunbookAbort

    private static Map<String, String> getParams(List<String> listOfArgs) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();

        // check if it is AT, Runbook, or Event
        if (isSendEvent(listOfArgs)) // this must be the first comparison.
        {
            if (listOfArgs.indexOf(EVENT_ID) >= 0)
            {
                params.put(Constants.EVENT_EVENTID, listOfArgs.get(listOfArgs.indexOf(EVENT_ID) + 1));
            }
            if (listOfArgs.indexOf(EVENT_REFERENCE) >= 0)
            {
                params.put(Constants.EVENT_REFERENCE, listOfArgs.get(listOfArgs.indexOf(EVENT_REFERENCE) + 1));
            }
            if (listOfArgs.indexOf(EVENT_END) >= 0)
            {
                params.put(Constants.EVENT_END, listOfArgs.get(listOfArgs.indexOf(EVENT_END) + 1));
            }
            if (listOfArgs.indexOf(PROCESSID) >= 0)
            {
                params.put(Constants.EXECUTE_PROCESSID, listOfArgs.get(listOfArgs.indexOf(PROCESSID) + 1));
            }
            if (listOfArgs.indexOf(RUNBOOK) >= 0)
            {
                params.put(Constants.HTTP_REQUEST_WIKI, listOfArgs.get(listOfArgs.indexOf(RUNBOOK) + 1));
            }
            // Worksheet relates
            if (listOfArgs.indexOf(PROBLEMID) >= 0)
            {
                params.put(Constants.EXECUTE_PROBLEMID, listOfArgs.get(listOfArgs.indexOf(PROBLEMID) + 1));
            }
            if (listOfArgs.indexOf(WORKSHEET_ALERTID) >= 0)
            {
                params.put(WorksheetUtils.ALERTID, listOfArgs.get(listOfArgs.indexOf(WORKSHEET_ALERTID) + 1));
            }
            if (listOfArgs.indexOf(WORKSHEET_NUMBER) >= 0)
            {
                params.put(WorksheetUtils.NUMBER, listOfArgs.get(listOfArgs.indexOf(WORKSHEET_NUMBER) + 1));
            }
            if (listOfArgs.indexOf(WORKSHEET_REFERENCE) >= 0)
            {
                params.put(WorksheetUtils.REFERENCE, listOfArgs.get(listOfArgs.indexOf(WORKSHEET_REFERENCE) + 1));
            }
        }
        else if (isExecuteRunbook(listOfArgs))
        {
            int index = listOfArgs.indexOf(RUNBOOK);
            String runbookName = listOfArgs.get(index + 1);
            if (runbookName == null || runbookName.trim().length() == 0)
            {
                throw new Exception("Please provide the name of the Runbook that needs to get executed.");
            }

            params.put(Constants.HTTP_REQUEST_WIKI, runbookName);
        }
        else
        {
            int index = listOfArgs.indexOf(ACTIONTASK);
            String atName = listOfArgs.get(index + 1);
            if (atName == null || atName.trim().length() == 0)
            {
                throw new Exception("Please provide the name of the ActionTask that needs to get executed.");
            }

            params.put(Constants.EXECUTE_ACTIONNAME, atName);
        }

        // read the params
        if (listOfArgs.contains(PROPERTIES_FILE) || listOfArgs.contains(PROPERTIES_PARAMS))
        {
            params.putAll(getProperties(listOfArgs));
        }

        return params;

    }// getParams

    private static boolean isExecuteRunbook(List<String> listOfArgs) throws Exception
    {
        boolean executeRunbook = false;

        // runbook or actiontask
        if (listOfArgs.contains(RUNBOOK))
        {
            executeRunbook = true;
        }
        else if (listOfArgs.contains(ACTIONTASK))
        {
            executeRunbook = false;
        }
        else
        {
            throw new Exception("One of the properties (-at OR -rb) should be specified.");
        }

        return executeRunbook;
    }// isExecuteRunbook

    private static boolean isSendEvent(List<String> listOfArgs) throws Exception
    {
        boolean sendEvent = false;

        // event
        if (listOfArgs.contains(EVENT))
        {
            sendEvent = true;
        }

        return sendEvent;
    }// isExecuteRunbook

    private static Map<String, String> getProperties(List<String> listOfArgs) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();

        if (listOfArgs.contains(PROPERTIES_FILE))
        {
            int index = listOfArgs.indexOf(PROPERTIES_FILE);
            String fileName = listOfArgs.get(index + 1);
            if (fileName == null || fileName.trim().length() == 0)
            {
                throw new Exception("Please provide the file name for the property " + PROPERTIES_FILE);
            }

            params.putAll(getPropertiesFromFile(fileName));
        }
        else if (listOfArgs.contains(PROPERTIES_PARAMS))
        {
            int index = listOfArgs.indexOf(PROPERTIES_PARAMS);
            String queryParams = listOfArgs.get(index + 1);

            params.putAll(convertQueryParamsToMap(queryParams));
        }

        return params;
    }// getProperties

    private static Map<String, String> getPropertiesFromFile(String fileName) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();

        Properties properties = getProperties(fileName);
        for (Iterator i = properties.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            params.put(key, value);
        }

        return params;
    }// getPropertiesFromFile

    /**
     * 
     * 
     * @param queryParams
     *            --&gt; name1=val1&amp;name2=val2&amp;...
     * @return
     * @throws Exception
     */
    private static Map<String, String> convertQueryParamsToMap(String queryParams) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();

        String[] arrNameValue = queryParams.split("\\&");
        for (String nameValue : arrNameValue)
        {
            String[] arr = nameValue.split("\\=");
            String key = arr[0];
            String value = arr[1];

            params.put(key, value);
        }

        return params;
    }// convertQueryParamsToMap

    private static Properties getProperties(String filename) throws Exception
    {
        Properties properties = new Properties();

        FileInputStream fis = null;
        try
        {
            File file = FileUtils.getFile(filename);
            if (file != null && file.exists())
            {
                fis = new FileInputStream(file);

                properties.load(fis);
            }
        }
        catch (Exception e)
        {
            throw new Exception("Properties file " + filename + " does not exist or is corrupt.", e);
        }
        finally
        {
            if (fis != null)
            {
                try
                {
                    fis.close();
                }
                catch (Exception e)
                {
                    // do nothing
                }
            }
        }

        return properties;
    }// getProperties

    private static int getProcessTimeoutInSecs(String strProcessTimeout)
    {
        int timeout = DEFAULT_TIMEOUT * 60;// in secs
        try
        {
            if (strProcessTimeout != null && strProcessTimeout.trim().length() > 0)
            {
                timeout = Integer.parseInt(strProcessTimeout) * 60;
            }
        }
        catch (Exception e)
        {
            // nothing to do
        }

        return timeout;
    }// getProcessTimeout

    private static boolean isVerbose(List<String> listOfArgs)
    {
        boolean isVerbose = false;

        int index = listOfArgs.indexOf(VERBOSE);
        if (index > -1)
        {
            String verbose = listOfArgs.get(index + 1);
            isVerbose = (verbose != null && verbose.trim().equalsIgnoreCase("TRUE")) ? true : false;
        }

        return isVerbose;

    }// isVerbose

    private static void readConnectionPropertiesFile(List<String> listOfArgs) throws Exception
    {
        int index = listOfArgs.indexOf(CONNECTION_PROPERTIES);
        if (index > -1)
        {
            String connectionPropFileName = listOfArgs.get(index + 1);
            Map<String, String> params = getPropertiesFromFile(connectionPropFileName);
            if (params != null && params.size() > 0)
            {
                Iterator<String> it = params.keySet().iterator();
                while (it.hasNext())
                {
                    String key = it.next();
                    String value = params.get(key) != null ? params.get(key).trim() : "";

                    if (key.equalsIgnoreCase(CONN_USERNAME))
                    {
                        listOfArgs.add(CMD_USERNAME);
                        listOfArgs.add(value);
                    }
                    else if (key.equalsIgnoreCase(CONN_P_ASSWORD))
                    {
                        listOfArgs.add(CMD_P_ASSWORD);
                        listOfArgs.add(value);
                    }
                    else if (key.equalsIgnoreCase(CONN_IP))
                    {
                        listOfArgs.add(IP);
                        listOfArgs.add(value);
                    }
                    else if (key.equalsIgnoreCase(CONN_PORT))
                    {
                        listOfArgs.add(PORT);
                        listOfArgs.add(value);
                    }
                }
            }
        }
    }// readConnectionPropertiesFile

    /**
     * This method sends an event to be processed.
     * 
     * @param protocol
     * @param ipaddress
     * @param portNumber
     * @param username
     * @param password
     * @param params
     *            is a {@link Map} with possibly following keys and their
     *            values: "EVENT_EVENTID", "WIKI", "REFERENCE", "PROCESSID" etc.
     * @return
     * @throws Exception
     */
    public static String sendEvent(String protocol, String ipaddress, long portNumber, String username, String password, Map<String, String> params) throws Exception
    {
        // params need to be massaged as it may have the raw key (like eventid
        // instead of "EVENT_EVENTID") when read from a file.
        params = cleanupEventParams(params);

        StringBuffer url = new StringBuffer(generateUrl(protocol, ipaddress, portNumber, URI_PATH_EVENT_EXECUTE, username, password, params));
        int processTimeoutInSecs = getProcessTimeoutInSecs(params.get(Constants.EXECUTE_PROCESS_TIMEOUT));
        // client should wait a little more than the server
        processTimeoutInSecs += CLIENT_BUFFER_TIMEOUT_IN_SECS;

        return send(url.toString(), processTimeoutInSecs);
    }

    private static Map<String, String> cleanupEventParams(Map<String, String> params) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();
        for (String rawKey : params.keySet())
        {
            if (eventKeyMap.containsKey(rawKey))
            {
                result.put(eventKeyMap.get(rawKey), params.get(rawKey));
            }
            else
            {
                if (!rawKey.equalsIgnoreCase(PROPERTIES_PARAMS))
                {
                    result.put(rawKey, params.get(rawKey)); // otherwise simply
                                                            // put it in.
                }
            }
        }

        if (params.containsKey(PROPERTIES_PARAMS))
        {
            String queryParams = params.get(PROPERTIES_PARAMS);
            result.putAll(convertQueryParamsToMap(queryParams));
        }

        return result;
    }
}// RSExecute
