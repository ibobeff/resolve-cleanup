/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.util.List;


public class RsButtonPanelDTO extends RsUIComponent
{
    private List<RsUIButton> controls;

    public List<RsUIButton> getControls()
    {
        return controls;
    }

    public void setControls(List<RsUIButton> controls)
    {
        this.controls = controls;
    }
    
    
    
}
