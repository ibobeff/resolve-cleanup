/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.jdbc.util;

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.owasp.esapi.ESAPI;

import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultValidator;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;

public class JdbcCrudUtil
{
   /**
    *
    * @param username - user
    * @param tableName - tableName
    * @param records - list of recs..some may be insert , some may be update
    * @return
    * @throws Exception
    */
    public static List<Map<String, Object>> save(String username, String tableName,
    		List<Map<String, Object>> records, String dbType) throws Exception
    {
        List<Map<String, Object>> result = null;

        if(StringUtils.isNotBlank(tableName) && records != null)
        {
            records = convertKeysToSmallerCase(records);

            List<Map<String, Object>> insertRecords = new ArrayList<>();
            List<Map<String, Object>> updateRecords = new ArrayList<>();

            for(Map<String, Object> record : records)
            {
                if(record.containsKey("sys_id"))
                {
                    String sysId = (String) record.get("sys_id");
                    if(StringUtils.isBlank(sysId))
                    {
                        records.remove("sys_id");

                        //its insert
                        insertRecords.add(record);
                    }
                    else
                    {
                        //its update
                        updateRecords.add(record);
                    }
                }
                else
                {
                    //its insert
                    insertRecords.add(record);
                }

            }

            if(insertRecords.size() > 0)
            {
                insertRecords = executeInsert(username, tableName, insertRecords, dbType);
            }

            if(updateRecords.size() > 0)
            {
                updateRecords = executeUpdate(username, tableName, updateRecords, dbType);
            }

            result = new ArrayList<>();
            result.addAll(insertRecords);
            result.addAll(updateRecords);
        }

        return result;
    }

    private static List<Map<String, Object>> executeSelect(String username, String tableName, List<String> sysIds) 
    		throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<>();

        SQLConnection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        ResultSetMetaData rsmd = null;

        String sql = null;
                
        try
        {
            sql = SQLUtils.getSafeSQL("select * from " + 
            						  ESAPI.validator().getValidInput("Execute Select Statement", tableName, 
            								  						  ResolveDefaultValidator.HQL_SQL_ENTITY_VALIDATOR, 
            								  						  ResolveDefaultValidator.MAX_HQL_SQL_ENTITY_LENGTH, 
            								  						  false, false) + 
            						  " where sys_id IN (" + 
            						  ESAPI.validator().getValidInput("Execute Select Statement SysIds", 
            								  						  SQLUtils.prepareQueryString(sysIds), 
            								  						ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
            								  						ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
            								  						false, false) + ")");
        	sanitizeTableName(tableName);
            connection = SQL.getConnection();
            statement = connection.prepareStatement(ESAPI.validator().getValidInput(
            												"Execute Select Statement SysIds", sql, 
            												ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
            												(2 * ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH),
            												false, false));
            
            rs = statement.executeQuery();
            rsmd = rs.getMetaData();

            int numberOfColumns = rsmd.getColumnCount();
            String[] dbColumnNames = new String[numberOfColumns];
            for (int count = 0; count < numberOfColumns; count++)
            {
                // columns start with 1 , so count+1
                dbColumnNames[count] = rsmd.getColumnLabel(count + 1);
            }

            // while loop to get the data
            while (rs.next())
            {
                Map<String, Object> row = getRowData(rs, dbColumnNames, false);
                result.add(row);
            }// end of while loop
        }
        catch (Throwable t)
        {
        	String errMsg = String.format("Error %sin fetching data for executeSelect(%s,%s,[%s]), SQL = [%s] ",
										  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""), 
										  username, tableName, StringUtils.listToString(sysIds, ", "),
										  (StringUtils.isNotEmpty(sql) ? sql : "Unsafe SQL"));
            Log.log.error(errMsg, t);
            throw new Exception(errMsg, t);
        }
        finally
        {

            try
            {
                if (rs != null)
                {
                	try {
                		rs.close();
                	} catch (Exception e) {
                		// Ignore exception in closing resultset
                	}
                }

                if (statement != null)
                {
                	try {
                		statement.close();
                	} catch (Exception e) {
                		// Ignore exception in closing statement
                	}
                }

                if (connection != null)
                {
                	try {
                		connection.close();
                	} catch (Exception e) {
                		Log.log.warn(String.format("Error %sin closing connection obtained for executeSelect(%s,%s,[%s])", 
                								   (StringUtils.isNotBlank(e.getMessage()) ? 
                									"[" + e.getMessage() + "] " : ""), username, tableName, 
                								   StringUtils.listToString(sysIds, ", ")));
                	}
                }
            }
            catch (Throwable t)
            {
            }
        }

        return result;
    }

    private static List<Map<String, Object>> executeUpdate(String username, String tableName, 
    		List<Map<String, Object>> records, String dbType) throws Exception
    {
        SQLConnection connection = null;
        PreparedStatement preparedStatement = null;
        List<String> sysIds = new ArrayList<String>();

        //cols list
        Set<String> cols = new HashSet<String>(records.get(0).keySet());
        cols.remove("sys_id");

        //prepare the update qry
        String sql = createUpdateQuery(tableName, cols);
        Log.log.trace("Executing SQL :\n" + sql);

        try
        {
        	sanitizeTableName(tableName);
            Map<String, String> columnDataTypes = getTableColumnsAndDatatype(tableName, dbType);

            connection = SQL.getConnection();
            connection.getConnection().setAutoCommit(false);
            preparedStatement = connection.prepareStatement(
            									ESAPI.validator().getValidInput(
            														"Execute Update Statement", SQLUtils.getSafeSQL(sql),
            														ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
            														ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
            														false, false));

            for(Map<String, Object> record : records)
            {
                int i = 1;
                String sysId = (String) record.get("sys_id");
                sysIds.add(sysId);

                try
                {
                    for (String col : cols)
                    {
                        Object value = record.get(col);
                        String colDataType = columnDataTypes.get(col);
                        
                        try
                        {
                            value = convertValueToCorrectDatatype(value, colDataType);
                            preparedStatement.setObject(i++, value);
                        }
                        catch (Exception e)
                        {
                        	String errMsg = String.format("Error %sin setting value in prepared statement %s for column %s",
        												  (StringUtils.isNotBlank(e.getMessage()) ? 
        												   "[" + e.getMessage() + "] " : ""), sql, col);
                        	
                            Log.log.warn(errMsg);
                            throw e;
                        }
                    }

                    //last one will be sys_id
                    preparedStatement.setObject(i++, sysId);
                    preparedStatement.addBatch();

                }
                catch (Exception e)
                {
                	String errMsg = String.format("Error %sin preparing batch of update statements %s for %d records",
							  					  (StringUtils.isNotBlank(e.getMessage()) ? 
							  					   "[" + e.getMessage() + "] " : ""), sql, records.size());
                    Log.log.warn(errMsg);
                    throw e;
                }

            }//end of for loop

            preparedStatement.executeBatch();
            connection.getConnection().commit();

            //get the recs
            records = executeSelect(username, tableName, sysIds);
        }
        catch (Throwable t)
        {
            connection.getConnection().rollback();
            String errMsg = String.format("Error %sin updating for executeUpdate(%s,%s,%d,%s), SQL = [%s]",
					  					  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""), 
					  					  username, tableName, records.size(), dbType, sql);
            Log.log.error(errMsg, t);
            throw new Exception(t.getMessage(), t);
        }
        finally
        {
            try
            {
                if (preparedStatement != null)
                {
                	try {
                		preparedStatement.close();
                	} catch (Exception e) {
                		// Ignore errors in closing prepared statement
                	}
                }

                if (connection != null)
                {
                	try {
                		connection.close();
                	} catch (Exception e) {
                		Log.log.warn(String.format("Error %sin closing connection obtained for executeUpdate(%s,%s,%d,%s)", 
												   (StringUtils.isNotBlank(e.getMessage()) ? 
												    "[" + e.getMessage() + "] " : ""), username, tableName, records.size(), 
												   dbType));
                	}
                }
            }
            catch (Throwable t)
            {
            }
        }

        return records;
    }
    
    public static void sanitizeTableName(String tableName) throws Exception
    {
    	Pattern pattern = Pattern.compile("([;'\\s]|-{2,})");
    	Matcher m = pattern.matcher(tableName);
    	if (m.find())
    	{
    		throw new Exception("Invalid table name");
    	}
    }

    private static List<Map<String, Object>> executeInsert(String username, String tableName, 
    		List<Map<String, Object>> records, String dbType) throws Exception
    {
        SQLConnection connection = null;
        PreparedStatement preparedStatement = null;
        List<String> sysIds = new ArrayList<String>();

        //cols list
        Set<String> cols = new HashSet<String>(records.get(0).keySet());
        cols.add("sys_id");

        //prepare the insert qry
        String sql = createInsertQuery(tableName, cols);
        Log.log.trace("Executing SQL :\n" + sql);


        try
        {
        	sanitizeTableName(tableName);
            Map<String, String> columnDataTypes = getTableColumnsAndDatatype(tableName, dbType);

            connection = SQL.getConnection();
            connection.getConnection().setAutoCommit(false);
            preparedStatement = connection.prepareStatement(
					ESAPI.validator().getValidInput(
							"Execute Insert Statement", SQLUtils.getSafeSQL(sql),
							ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
							ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH, 
							false, false));

            for(Map<String, Object> record : records)
            {
                int i = 1;
                String sysId = SysId.generate(record);
                sysIds.add(sysId);
                record.put("sys_id", sysId);

                try
                {
                    for (String col : cols)
                    {
                        Object value = record.get(col);
                        String colDataType = columnDataTypes.get(col);

                        try
                        {
                            value = convertValueToCorrectDatatype(value, colDataType);
                            preparedStatement.setObject(i++, value);
                        }
                        catch (Exception e)
                        {
                        	String errMsg = String.format("Error %sin setting value in prepared statement %s for column %s",
									  					  (StringUtils.isNotBlank(e.getMessage()) ? 
									  					   "[" + e.getMessage() + "] " : ""), sql, col);
                        	Log.log.warn(errMsg);
                            throw e;
                        }
                    }//end of for loop

                    preparedStatement.addBatch();
                }
                catch (Exception e)
                {
                	String errMsg = String.format("Error %sin preparing batch of insert statements %s for %d records",
		  					  					  (StringUtils.isNotBlank(e.getMessage()) ? 
		  					  					   "[" + e.getMessage() + "] " : ""), sql, records.size());
                	Log.log.warn(errMsg);
                    throw e;
                }

            }//end of for loop

            //execute batch
            preparedStatement.executeBatch();
            connection.commit();

            //get the recs
            records = executeSelect(username, tableName, sysIds);

        }
        catch (Throwable t)
        {
            connection.getConnection().rollback();
            String errMsg = String.format("Error %sin inserting for executeInsert(%s,%s,%d,%s), SQL = [%s]",
					  					  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""), 
					  					  username, tableName, records.size(), dbType, sql);
            Log.log.error(errMsg, t);
            throw new Exception(t.getMessage(), t);
        }
        finally
        {

            try
            {
                if (preparedStatement != null)
                {
                    preparedStatement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return records;

    }

    private static String createInsertQuery(String tableName, Set<String> cols)
    {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ");
        sql.append(tableName);
        sql.append("(");
        sql.append(getColumns(cols, false));
        sql.append(")");
        sql.append(" VALUES (");
        sql.append(getColumns(cols, true));
        sql.append(")");

        return sql.toString();
    }

    private static String createUpdateQuery(String tableName, Set<String> cols)
    {
    	//String modelName = /*HibernateUtil*/GenericJDBCHandler./*table2Class*/tableMap.get(tableName);
    	
        StringBuilder sql = new StringBuilder();
        sql.append(" update ").append(/*HibernateUtil*GenericJDBCHandler./*class2Table*tableMap.get(modelName)*/tableName);
        sql.append(" set ");
        sql.append(getUpdateColumns(cols));
        sql.append(" where sys_id = ?");

        return sql.toString();
    }

    private static String getUpdateColumns(Set<String> cols)
    {
        StringBuilder sb = new StringBuilder();

        for (String colName : cols)
        {
            if(StringUtils.isNotBlank(colName))
            {
                if(colName.equalsIgnoreCase("sys_id"))
                {
                    continue;
                }

                sb.append(colName).append(" = ?").append(",");
            }
        }//end of for

        String str = sb.substring(0, sb.length() - 1);
        return str;
    }

    private static String getColumns(Set<String> cols, boolean usePlaceHolders)
    {
        StringBuilder sb = new StringBuilder();

        boolean first = true;

        /* Iterate the column-names */
        for (String colName: cols)
        {
            if(StringUtils.isNotBlank(colName))
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    sb.append(", ");
                }

                if (usePlaceHolders)
                {
                    sb.append("?");
                }
                else
                {
                    sb.append(colName);
                }
            }
        }

        return sb.toString();

    }

    private static Map<String, Object> getRowData(ResultSet rs, String[] dbColumnNames, boolean forUI) throws Exception
    {
        Map<String, Object> row = new HashMap<String, Object>();

        // add the data to the row
        for (int count = 0; count < dbColumnNames.length; count++)
        {
            String column = dbColumnNames[count];
            Object rawdata = rs.getObject(column);
            Object data = null;

            if (rawdata != null)
            {
                String className = rawdata.getClass().getName().toLowerCase();
                if (className.contains("clob"))
                {
                    Clob c = (Clob) rawdata;
                    long l = c.length();
                    data = c.getSubString(1, (int) l);
                }
                else if (className.contains("blob"))
                {
                    data = null;//NOT supported for now
                }
                else
                {
                    data = rawdata.toString();//sending all the data as a string else will have to specific to oracle or db
                }
            }
            row.put(column.toLowerCase(), data);
        }// end of for loop

        return row;
    }

    private static Map<String, String> getTableColumnsAndDatatype(String tableName, String dbType) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();

        SQLConnection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        ResultSetMetaData rsmd = null;

        String sql = SQLUtils.getSafeSQL("select * from " + 
        		ESAPI.validator().getValidInput("Get Table Columns And Data Types",
        				tableName, ResolveDefaultValidator.HQL_SQL_ENTITY_VALIDATOR, 4096, false, false));

        try
        {
        	sanitizeTableName(tableName);
            connection = SQL.getConnection();
            statement = connection.prepareStatement(sql);
            rs = statement.executeQuery();
            rsmd = rs.getMetaData();

            int numberOfColumns = rsmd.getColumnCount();
            for (int count = 0; count < numberOfColumns; count++)
            {
                // columns start with 1 , so count+1
                String columnName = rsmd.getColumnName(count + 1).toLowerCase();
                String dataType = rsmd.getColumnClassName(count + 1);

                result.put(columnName, dataType);
            }

        }
        catch (Throwable t)
        {
            Log.log.error("Error in fetching data for :" + sql, t);
            throw new Exception(t.getMessage());
        }
        finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }

                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return result;
    }

    /**
     *
     * @param obj
     * @param dataType - className , eg, java.math.BigDecimal, oracle.sql.TIMESTAMP, etc
     * @return
     */
    private static Object convertValueToCorrectDatatype(Object obj, String dataType)
    {
        Object result = null;

        if(obj != null)
        {
            dataType = dataType.toLowerCase();
            if(dataType.contains("timestamp"))
            {
                Long timeInLong = new Long(obj.toString());//from UI, time is in long
                result = new Timestamp(timeInLong.longValue());
            }
            else if(dataType.contains("boolean"))
            {
                result = new Boolean(obj.toString());
            }
            else if(dataType.contains("long"))
            {
                result = new Long(obj.toString());
            }
            else if(dataType.contains("double"))
            {
                result = new Double(obj.toString());
            }
            else if(dataType.contains("bigdecimal"))
            {
                result = new BigDecimal(obj.toString());
            }
            else if(dataType.contains("blob"))
            {
                //not supported yet
            }
            else
            {
                //by default, not changing the type
                result = obj;
            }
        }


        return result;
    }

    private static List<Map<String, Object>> convertKeysToSmallerCase(List<Map<String, Object>> data)
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        if(data != null)
        {
            for(Map<String, Object> record : data)
            {
                Map<String, Object> newRecord = new HashMap<String, Object>();
                Iterator<String> keys = record.keySet().iterator();
                while(keys.hasNext())
                {
                    String key = keys.next();
                    Object value = record.get(key);

                    newRecord.put(key.toLowerCase(), value);
                }

                result.add(newRecord);
            }
        }

        return result;
    }
}
