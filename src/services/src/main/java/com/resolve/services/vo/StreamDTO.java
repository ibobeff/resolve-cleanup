/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;

public class StreamDTO //extends RSComponent
{
    private String id;
    private String sysId;
    private String displayName;// Only used for UI 

    private String comptype = "";
    private boolean systemStream = false;
    private boolean isLeaf = true;

    private String parentSysId = "";
    private String parentCompType = "";

    private List<Object> records = new ArrayList<Object>();

    public StreamDTO(String sysId, String displayName, String compType)
    {
        if (sysId.equalsIgnoreCase(AdvanceTree.STATIC_ID))
        {
            sysId = displayName;
            this.setSystemStream(true);
        }
        this.setId(sysId);
        this.setSysId(sysId);
        this.setDisplayName(displayName);
        this.setComptype(compType);
    }

    public StreamDTO(RSComponent comp, boolean canBeFolder)
    {
        this.setId(comp.getSys_id());
        this.setSysId(comp.getSys_id());
        this.setDisplayName(comp.getDisplayName());

        String name = comp.getClass().getSimpleName();
        if (name.equalsIgnoreCase("Process")) this.processComponentProcess(comp, canBeFolder);
        if (name.equalsIgnoreCase("Team")) this.processComponentTeam(comp, canBeFolder);// this.setComptype(AdvanceTree.TEAMS);
        if (name.equalsIgnoreCase("Document")) this.setComptype(AdvanceTree.DOCUMENTS);
        if (name.equalsIgnoreCase("Runbook")) this.setComptype(AdvanceTree.RUNBOOKS);
        if (name.equalsIgnoreCase("ActionTask")) this.setComptype(AdvanceTree.ACTIONTASKS);
        if (name.equalsIgnoreCase("User")) this.setComptype(AdvanceTree.USERS);
        if (name.equalsIgnoreCase("Rss")) this.setComptype(AdvanceTree.RSS);
        if (name.equalsIgnoreCase("Forum")) this.processComponentForum(comp, canBeFolder);// this.setComptype(AdvanceTree.FORUMS);
        if (name.equalsIgnoreCase(AdvanceTree.DEFAULT)) this.setComptype(AdvanceTree.DEFAULT);
    }

    public StreamDTO(RSComponent comp, boolean canBeFolder, String parentSysId, String parentCompType)
    {
        this.setId(comp.getSys_id());
        this.setSysId(comp.getSys_id());
        this.setDisplayName(comp.getDisplayName());

        this.parentSysId = parentSysId;
        this.parentCompType = parentCompType;
        String name = comp.getClass().getSimpleName();
        if (name.equalsIgnoreCase("Process")) this.processComponentProcess(comp, canBeFolder);
        if (name.equalsIgnoreCase("Team")) this.processComponentTeam(comp, canBeFolder);// this.setComptype(AdvanceTree.TEAMS);
        if (name.equalsIgnoreCase("Document")) this.setComptype(AdvanceTree.DOCUMENTS);
        if (name.equalsIgnoreCase("Runbook")) this.setComptype(AdvanceTree.RUNBOOKS);
        if (name.equalsIgnoreCase("ActionTask")) this.setComptype(AdvanceTree.ACTIONTASKS);
        if (name.equalsIgnoreCase("User")) this.setComptype(AdvanceTree.USERS);
        if (name.equalsIgnoreCase("Rss")) this.setComptype(AdvanceTree.RSS);
        if (name.equalsIgnoreCase("Forum")) this.processComponentForum(comp, canBeFolder);// this.setComptype(AdvanceTree.FORUMS);
        if (name.equalsIgnoreCase(AdvanceTree.DEFAULT)) this.setComptype(AdvanceTree.DEFAULT);

        // Set parentCompType properly
        if (parentCompType.equalsIgnoreCase("Process")) this.parentCompType = AdvanceTree.PROCESS;
        if (parentCompType.equalsIgnoreCase("Team")) this.parentCompType = AdvanceTree.TEAMS;
        if (parentCompType.equalsIgnoreCase("Document")) this.parentCompType = AdvanceTree.DOCUMENTS;
        if (parentCompType.equalsIgnoreCase("Runbook")) this.parentCompType = AdvanceTree.RUNBOOKS;
        if (parentCompType.equalsIgnoreCase("ActionTask")) this.parentCompType = AdvanceTree.ACTIONTASKS;
        if (parentCompType.equalsIgnoreCase("User")) this.parentCompType = AdvanceTree.USERS;
        if (parentCompType.equalsIgnoreCase("Rss")) this.parentCompType = AdvanceTree.RSS;
        if (parentCompType.equalsIgnoreCase("Forum")) this.parentCompType = AdvanceTree.FORUMS;
        if (parentCompType.equalsIgnoreCase(AdvanceTree.DEFAULT)) this.parentCompType = AdvanceTree.DEFAULT;
    }

    public String getComptype()
    {
        return comptype;
    }

    public void setComptype(String comptype)
    {
        this.comptype = comptype;
    }

    public boolean isSystemStream()
    {
        return systemStream;
    }

    public void setSystemStream(boolean systemStream)
    {
        this.systemStream = systemStream;
    }

    public boolean isLeaf()
    {
        return isLeaf;
    }

    public void setLeaf(boolean isLeaf)
    {
        this.isLeaf = isLeaf;
    }

    public List<Object> getRecords()
    {
        return records;
    }

    public void setRecords(List<Object> records)
    {
        this.records = records;
    }

    public void processComponentProcess(RSComponent comp, boolean canBeFolder)
    {
        this.setComptype(AdvanceTree.PROCESS);
        if (canBeFolder) this.isLeaf = false;
        com.resolve.services.graph.social.model.component.container.Process p = ((com.resolve.services.graph.social.model.component.container.Process) comp);

        Collection<Team> childTeams = p.getTeams();
        Collection<User> childUsers = p.getUsers();
        Collection<Runbook> childRunbooks = p.getRunbooks();
        Collection<Document> childDocuments = p.getDocuments();
        Collection<ActionTask> childActiontasks = p.getActiontasks();
        Collection<Rss> childRss = p.getRss();
        Collection<Forum> childForums = p.getForums();

        // sort
        RSComponent.sortCollection(childTeams);
        RSComponent.sortCollection(childUsers);
        RSComponent.sortCollection(childRunbooks);
        RSComponent.sortCollection(childDocuments);
        RSComponent.sortCollection(childActiontasks);
        RSComponent.sortCollection(childRss);
        RSComponent.sortCollection(childForums);

        this.records.add(new StreamDTO(comp.getSys_id(), comp.getDisplayName() + " (" + AdvanceTree.ONLY + ")", AdvanceTree.PROCESS));
        if (childTeams != null) this.records.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.TEAMS).setStreamsFromCollection(childTeams, canBeFolder, comp));
        if (childRunbooks != null) this.records.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.RUNBOOKS).setStreamsFromCollection(childRunbooks, canBeFolder, comp));
        if (childDocuments != null) this.records.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.DOCUMENTS).setStreamsFromCollection(childDocuments, canBeFolder, comp));
        if (childActiontasks != null) this.records.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.ACTIONTASKS).setStreamsFromCollection(childActiontasks, canBeFolder, comp));
        if (childUsers != null) this.records.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.USERS).setStreamsFromCollection(childUsers, canBeFolder, comp));
        if (childRss != null) this.records.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.RSS).setStreamsFromCollection(childRss, canBeFolder, comp));
        if (childForums != null) this.records.add(new StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.FORUMS).setStreamsFromCollection(childForums, canBeFolder, comp));
    }

    public void processComponentTeam(RSComponent comp, boolean canBeFolder)
    {
        this.setComptype(AdvanceTree.TEAMS);
        if (canBeFolder) this.isLeaf = false;
        Team t = ((Team) comp);

        Collection<Team> childTeams = t.getTeams();
        Collection<User> childUsers = t.getUsers();

        // sort
        RSComponent.sortCollection(childTeams);
        RSComponent.sortCollection(childUsers);

        this.records.add(new StreamDTO(comp.getSys_id(), comp.getDisplayName() + " (" + AdvanceTree.ONLY + ")", AdvanceTree.TEAMS));
        if (childTeams != null && childTeams.size() > 0)
        {
            for (Team childTeam : childTeams)
            {
                this.records.add(new StreamDTO(childTeam, true, comp.getSys_id(), comp.getClass().getSimpleName()));
            }
            // this.records.add(new
            // StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.TEAMS).setStreamsFromCollection(childTeams,
            // canBeFolder, comp));
        }
        if (childUsers != null && childUsers.size() > 0)
        {
            for (User childUser : childUsers)
            {
                this.records.add(new StreamDTO(childUser, false, comp.getSys_id(), comp.getClass().getSimpleName()));
            }
            // this.records.add(new
            // StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.USERS).setStreamsFromCollection(childUsers,
            // canBeFolder,comp));
        }
    }

    public void processComponentForum(RSComponent comp, boolean canBeFolder)
    {
        this.setComptype(AdvanceTree.FORUMS);
        if (canBeFolder) this.isLeaf = false;
        Forum f = ((Forum) comp);

        Collection<User> childUsers = f.getUsers();

        // sort
        RSComponent.sortCollection(childUsers);

        // this.records.add(new StreamDTO(comp.getSys_id(),
        // comp.getDisplayName() + " (" + AdvanceTree.ONLY + ")",
        // AdvanceTree.FORUMS));
        if (childUsers != null && childUsers.size() > 0)
        {
            for (User childUser : childUsers)
            {
                this.records.add(new StreamDTO(childUser, false, comp.getSys_id(), comp.getClass().getSimpleName()));
            }
            // this.records.add(new
            // StreamGroupDTO().setExpanded(false).setDisplayName(AdvanceTree.USERS).setStreamsFromCollection(childUsers,
            // canBeFolder,comp));
        }
    }

    public String getParentSysId()
    {
        return parentSysId;
    }

    public void setParentSysId(String parentSysId)
    {
        this.parentSysId = parentSysId;
    }

    public String getParentCompType()
    {
        return parentCompType;
    }

    public void setParentCompType(String parentCompType)
    {
        this.parentCompType = parentCompType;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getSysId()
    {
        return sysId;
    }

    public void setSysId(String sysId)
    {
        this.sysId = sysId;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    

}
