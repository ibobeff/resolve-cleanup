/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.dao.XMPPAddressDAO;
import com.resolve.persistence.model.XMPPAddress;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class XMPPGatewayUtil extends GatewayUtil
{
    private static volatile XMPPGatewayUtil instance = null;
    public static XMPPGatewayUtil getInstance()
    {
        if(instance == null)
        {
            instance = new XMPPGatewayUtil();
        }
        return instance;
    }

    @SuppressWarnings("rawtypes")
    public void setXMPPAddresses(Map xmppAddresses)
    {
       try
        {
    	   HibernateProxy.execute(() -> {
    		   XMPPAddressDAO filterDao = HibernateUtil.getDAOFactory().getXMPPAddressDAO();

               String queue = deleteByQueue(XMPPAddress.class.getSimpleName(), xmppAddresses);

               // update each filter
               for (Object filterMapObj : xmppAddresses.values())
               {
                   String filterStringMap = (String) filterMapObj;
                   Map filterMap = StringUtils.stringToMap(filterStringMap);

                   String xmppAddress = (String) filterMap.get("XMPPADDRESS");
                   String xmppPassword = (String) filterMap.get("XMPPPASSWORD");
                   String sysModifiedBy = (String) filterMap.get(Constants.SYS_UPDATED_BY);

                   // init record entry with values
                   XMPPAddress row = new XMPPAddress();
                   row.setUXMPPAddress(xmppAddress);
                   row.setUXMPPPassword(xmppPassword);
                   row.setUQueue(queue);
                   row.setSysCreatedBy(sysModifiedBy);
                   row.setSysUpdatedBy(sysModifiedBy);

                   filterDao.save(row);
               }
    	   });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setXMPPAddresses

    @SuppressWarnings("unchecked")
    public List<Map<String, String>> getXMPPAddresses(String queueName, boolean isSocialPoster)
    {
        List<Map<String, String>> xmppAddresses = new ArrayList<Map<String,String>>();

        try
        {
            HibernateProxy.execute(() -> {
            	
            	List<XMPPAddress> addressList = HibernateUtil.createQuery("select o from XMPPAddress o where UQueue='" + queueName + "'").list();

                for (XMPPAddress address : addressList)
                {
                    Map<String, String> filterMap = new HashMap<String, String>();
                    filterMap.put("XMPPADDRESS", address.getUXMPPAddress());
                    filterMap.put("XMPPPASSWORD", address.getUXMPPPassword());
                    filterMap.put("QUEUE", address.getUQueue());
                    filterMap.put(Constants.SYS_UPDATED_BY, address.getSysUpdatedBy());

                    xmppAddresses.add(filterMap);
                }

                //get XMPP address from process, forums, team etc.
                if(isSocialPoster)
                {
                    //found out that there is no active flag for these anymore and by default they are active. but in the future
                    //they may add it. the day they do that hopefully someone will remember this to turn on. 
                    //List<Map<String, Object>> addresses = getCustomTableData("select * from social_process where u_is_active=1 and u_im is not null");
                    //addresses.addAll(getCustomTableData("select * from social_forum where u_is_active=1 and u_im is not null"));
                    //addresses.addAll(getCustomTableData("select * from social_team where u_is_active=1 and u_im is not null"));

                    List<Map<String, Object>> addresses = getCustomTableData("select * from social_process where u_im is not null");
                    addresses.addAll(getCustomTableData("select * from social_forum where u_im is not null"));
                    addresses.addAll(getCustomTableData("select * from social_team where u_im is not null"));

                    for(Map<String, Object> address : addresses)
                    {
                        String imAddress = (String) address.get("u_im");
                        if(StringUtils.isNotBlank(imAddress))
                        {
                            Map<String, String> addressMap = new HashMap<String, String>();

                            addressMap.put("XMPPADDRESS", imAddress);
                            String pwd = (String) address.get("u_im_pwd");
                            addressMap.put("XMPPPASSWORD", pwd);
                            addressMap.put("QUEUE", queueName);
                            addressMap.put(Constants.SYS_UPDATED_BY, (String) address.get("sys_updated_by"));
                            xmppAddresses.add(addressMap);
                        }
                    }
                }
            });

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return xmppAddresses;
    } // getXMPPAddresses
}
