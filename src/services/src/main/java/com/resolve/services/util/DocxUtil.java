package com.resolve.services.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.FilenameUtils;
import org.docx4j.Docx4J;
import org.docx4j.convert.out.HTMLSettings;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.resolve.util.Compress;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.SysId;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.WikiAttachmentContentVO;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;

public class DocxUtil
{

    private String filesSuffix = "_files";

    public DocxUtil()
    {

    }

    /**
     * Returns a WordprocessingMLPackage object from the given file.
     *
     * @param sourceFile
     *            - A file object that points to a .docx file
     *
     * @return WordprocessingMLPackage object
     */
    private WordprocessingMLPackage openDocxFile(File sourceFile)
    {
        WordprocessingMLPackage wordMLPackage;
        wordMLPackage = null;
        try
        {
            wordMLPackage = WordprocessingMLPackage.load(sourceFile);
        }
        catch (Docx4JException e)
        {
            Log.log.error(e.getMessage(), e);
            System.out.println("An error occurred while trying to import the docx file.");
            e.printStackTrace();
        }
        return wordMLPackage;
    }

    /**
     * Returns a String object of HTML data converted over from the .docx file
     * associated with the WordprocessingMLPackage
     *
     * @param w
     *            an instance of the WordprocessingMLPackage object associated
     *            with the .docx file.
     * @param docxSourceFilePath
     *            The absolute path to the .docx file
     *
     * @return The HTML data
     */
    private String getHTML(String docxSourceFilePath, WordprocessingMLPackage w) throws JAXBException, Docx4JException, Exception
    {
        String html = "\nAn error occured while importing the HTML";

        HTMLSettings htmlSettings = Docx4J.createHTMLSettings();
        htmlSettings.setImageDirPath(docxSourceFilePath + filesSuffix);
        htmlSettings.setImageTargetUri(docxSourceFilePath.substring(docxSourceFilePath.lastIndexOf("/") + 1) + filesSuffix);
        htmlSettings.setImageIncludeUUID(false);
        // System.out.println("htmlSettings:" + );
        htmlSettings.setWmlPackage(w);
        ByteArrayOutputStream os;

        ClassLoader tccl = Thread.currentThread().getContextClassLoader();

        try
        {
            Object o = new Object();
            ClassLoader cl = o.getClass().getClassLoader();
            Thread.currentThread().setContextClassLoader(cl);
            os = new ByteArrayOutputStream();
            Docx4J.toHTML(htmlSettings, os, Docx4J.FLAG_EXPORT_PREFER_XSL);
            html = os.toString();

            // System.out.println(os.toByteArray());
            // Sanitize the HTML by removing unsupported unicode characters
            char[] charArray = html.toCharArray();
            for (int i = 0; i < charArray.length; i++)
            {
                // System.out.println(charArray[i]);
                charArray[i] = sanitizeHTMLCharacter(charArray[i]);
            }

            html = new String(charArray);
            // System.out.println(html);
            os.close();

        }
        catch (IOException ioe)
        {
            System.out.println("An IOExecption has occured: " + ioe.getMessage());
            html = "An IOExecption has occured: " + ioe.getMessage();
            Log.log.error(ioe.getMessage(), ioe);
            // System.out.println(ioe.getMessage());
        }

        return html;
    }

    /**
     * Sanitizes your HTML by removing any unsupported unicode characters. As of
     * Resolve 5.2, our MySQL DB will throw a Generic JDBC Exception if Unicode
     * charactes above 127 are used.
     *
     * @param c
     *            - the character to sanitize
     *
     * @return char sanitized char value
     */
    private char sanitizeHTMLCharacter(char c)
    {
        // Remove any character above 128. The MySQL DB can't handle them when
        // importing IMPEX modules
        int code = ((Character) c).hashCode();
        if (code > 127)
        {
            return ' ';
        }

        return c;
    }

    /**
     * Converts <img> tags in the given _html String to the {image} macro.
     *
     * @param _html
     *            the HTML data to update
     * @param fileName
     *            the docx filename
     *
     * @return the updated HTML data
     */
    private String updateHTMLImageTagsToMacros(String _html, String fileName)
    {
        String html = _html;
        String newHTML = "";
        HashMap<String, String> changeMap = new HashMap<String, String>();

        // System.out.println("Pre HTML: " + _html);

        // parse through html and replace all <img> tags with {image} macros

        String imgRegex = "<img [^>]*>";
        String srcRegex = "src=\"[^\"]*\"";
        String borderRegex = "border=\"[0-9]*\"";
        String heightRegex = "height=\"[0-9]*\"";
        String widthRegex = "width=\"[0-9]*\"";
        String imageSrcHeader = "src=\"?([^\"]*)\"?";
        String heightHeader = "height=\"";
        String widthHeader = "width=\"";
        String borderHeader = "border=\"";

        Pattern imgPattern = Pattern.compile(imgRegex);
        Pattern srcPattern = Pattern.compile(srcRegex);
        Pattern heightPattern = Pattern.compile(heightRegex);
        Pattern widthPattern = Pattern.compile(widthRegex);
        Pattern borderPattern = Pattern.compile(borderRegex);

        Matcher imgMatcher;
        Matcher imgSRCMatcher;
        Matcher imgWidthMatcher;
        Matcher imgHeightMatcher;
        Matcher imgBorderMatcher;

        String imgTagString;
        String imageFileString;
        String imageWidthString = "";
        String imageHeightString = "";
        String imageBorderString = "";
        String imageMacroString;

        // loop through all <img> tags and replace them with {image} macros
        imgMatcher = imgPattern.matcher(html);

        // System.out.println("Finding image tags");

        // System.out.println("SEARCHING FOR IMAGE TAGS...");
        while (imgMatcher.find())
        {
            // Image found!

            // get src filename String. Removes the preceding directory from the
            // filename path
            // System.out.println("FILE NAME: " + fileName);
            imgTagString = imgMatcher.group();
            //System.out.println("-------");
            //System.out.println("IMAGE TAG FOUND: " + imgTagString);
            //System.out.println(imgTagString);
            //System.out.println("-------");

            imgSRCMatcher = srcPattern.matcher(imgTagString);
            boolean isSrcFound = imgSRCMatcher.find();
            
            // ignore if no src is found
            if(isSrcFound) {

                String pathName = "src=\"" + fileName + filesSuffix + "\\";
                // System.out.println("IMAGE PATH NAME: " + pathName);
    
                String imgsrc = imgSRCMatcher.group();
                // System.out.println("IMAGE SRC: " + imgsrc);
    
                int pathNameLength = pathName.length();
                // System.out.println("PATH NAME LENGTH: " + pathNameLength);
    
                int imgSRCLength = imgsrc.length();
                // System.out.println("IMAGE SRC LENGTH: " + imgSRCLength);
    
                // String imgsrcfile = pathName.substring(pathNameLength,
                // imgSRCLength - 1);
                String imgsrcfile = imgsrc.substring(imgsrc.indexOf("/")+1, imgSRCLength - 1);
                // System.out.println("IMAGE SRC FILE NAME: " + imgsrcfile);
    
                imageFileString = imgsrcfile;
                // System.out.println("IMAGE FILE NAME: " + imageFileString);
    
                // get the height
                imgHeightMatcher = heightPattern.matcher(imgTagString);
                imageHeightString = "";
                if (imgHeightMatcher.find())
                {
                    imageHeightString = imgHeightMatcher.group();
                    imageHeightString = imageHeightString.substring(heightHeader.length(), imageHeightString.length() - 1);
                }
                // System.out.println("IMAGE HEIGHT: " + imageHeightString);
    
                // get the width
                imgWidthMatcher = widthPattern.matcher(imgTagString);
                imageWidthString = "";
                if (imgWidthMatcher.find())
                {
                    imageWidthString = imgWidthMatcher.group();
                    imageWidthString = imageWidthString.substring(widthHeader.length(), imageWidthString.length() - 1);
                }
                // System.out.println("IMAGE WIDTH: " + imageWidthString);
    
                // get the border
                imgBorderMatcher = borderPattern.matcher(imgTagString);
                imageBorderString = "";
                if (imgBorderMatcher.find())
                {
                    imageBorderString = imgBorderMatcher.group();
                    imageBorderString = imageBorderString.substring(borderHeader.length(), imageBorderString.length() - 1);
                }
                // System.out.println("IMAGE BORDER: " + imageBorderString);
    
                // assemble them into a {image} macro
                imageMacroString = "{pre}{image:" + imageFileString;
    
                if (!imageHeightString.isEmpty())
                {
                    imageMacroString += "|height=" + imageHeightString;
                }
    
                if (!imageWidthString.isEmpty())
                {
                    imageMacroString += "|width=" + imageWidthString;
                }
                if (!imageBorderString.isEmpty())
                {
                    imageMacroString += "|border=" + imageBorderString;
                }
    
                imageMacroString += "}{pre}";
                // System.out.println("IMAGE MACRO STRING: " + imageMacroString);
    
                changeMap.put(imgTagString, imageMacroString);
            }
        }

        imgMatcher.reset();

        Pattern imgTagPattern;
        newHTML = html;

        // System.out.println("REPLACING <IMG> TAGS...");
        // replace all <img> tags with {image} macros in the changeMap
        for (Map.Entry<String, String> entry : changeMap.entrySet())
        {
            imgTagPattern = Pattern.compile(Pattern.quote(entry.getKey()));
            imgMatcher = imgTagPattern.matcher(newHTML);
            imgMatcher.find();
            // System.out.println("imgMatcher found: " + imgMatcher.group());
            // System.out.println("replacing with: " + entry.getValue());
            newHTML = imgMatcher.replaceAll(entry.getValue());
        }

        // System.out.println("Post HTML: " + newHTML);

        return newHTML;
    }

    /**
     * Returns the data to put into a IMPEX module's package.xml file
     *
     * @param nameSpace
     *            the namespace of the IMPEX module
     * @param packageName
     *            the package name of the IMPEX module
     * @param runbookNames
     *            a list of runbooks to get 'stuffed' into this IMPEX module
     * @param author
     *            the author to be associated with each runbook
     * @param date
     *            the 'created by' date for this IMPEX module and runbooks. Must
     *            be in the format "yyyy-MM-dd HH:mm:ss"
     *
     * @return the package XML data
     */
    private static String getPackageXMLBulkFile(String nameSpace, String packageName, ArrayList<String> runbookNames, String author, String date)
    {
        // Returns a string of XML content for the package.xml file, for bulk
        // conversions

        StringBuilder xml = new StringBuilder();
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        xml.append("\n");
        xml.append("<package>\n");
        xml.append("<infos>\n");
        xml.append("<name>" + packageName + "</name>\n");
        xml.append("<module>" + packageName + "</module>\n");

        xml.append("<description>wiki module exported on " + date + " </description>\n");
        xml.append("<licence>proprietry</licence>\n");
        xml.append("<author>" + author + "</author>\n");
        xml.append("<version>3.x</version>;\n");
        xml.append("<backupPack>false</backupPack>\n");
        xml.append("</infos>\n");
        xml.append("<files>\n");
        for (String runbookName : runbookNames)
        {
            xml.append("<file defaultAction=\"0\" language=\"\">" + nameSpace + "." + runbookName + "</file>\n");
        }
        xml.append("</files>\n");
        xml.append("</package>\n");

        return xml.toString();

    }

    /**
     * Returns the data to put into a IMPEX module's package.xml file
     *
     * @param nameSpace
     *            the namespace of the IMPEX module
     * @param packageName
     *            the package name of the IMPEX module
     * @param runbookNames
     *            a list of runbooks to get 'stuffed' into this IMPEX module
     * @param author
     *            the author to be associated with each runbook
     * @param date
     *            the 'created by' date for this IMPEX module and runbooks. Must
     *            be in the format "yyyy-MM-dd HH:mm:ss"
     *
     * @return the package XML data
     */
    private String getPackageXMLSingleFile(String nameSpace, String runbookName, String author, String date)
    {
        // Returns a string of XML content for the package.xml file, for SINGLE
        // conversion only

        StringBuilder xml = new StringBuilder();
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        xml.append("\n");
        xml.append("<package>\n");
        xml.append("<infos>\n");
        xml.append("<name>" + nameSpace + "</name>\n");
        xml.append("<module>" + nameSpace + "</module>\n");

        xml.append("<description>wiki module exported on " + date + " </description>\n");
        xml.append("<licence>proprietry</licence>\n");
        xml.append("<author>" + author + "</author>\n");
        xml.append("<version>3.x</version>;\n");
        xml.append("<backupPack>false</backupPack>\n");
        xml.append("</infos>\n");
        xml.append("<files>\n");
        xml.append("<file defaultAction=\"0\" language=\"\">" + nameSpace + "." + runbookName + "</file>\n");
        xml.append("</files>\n");
        xml.append("</package>\n");

        return xml.toString();

    }

    /**
     * Gets the relationships JSON data for the IMPEX module
     *
     * 
     * @return the JSON data
     */
    private String getRelationshipJSON()
    {
        // Returns a string of XML content for the relationships JSON file
        StringBuilder relationshipXML = new StringBuilder();
        relationshipXML.append("{\n");
        relationshipXML.append("  \"relationships\" : [ ]\n");
        relationshipXML.append("}\n");
        return relationshipXML.toString();
    }

    /**
     * Gets the wiki content for a given runbook
     *
     * @param nameSpace
     *            the namespace of the IMPEX module
     * @param runbookName
     *            the runbook name
     * @param authorName
     *            a name of the author of the runbook
     * @param sysID
     *            the author to be associated with each runbook
     * @param html
     *            the HTML which will be added to the view of the runbook
     * @param images
     *            a list of the images linked in the HTML
     * @param dateLong
     *            the 'created by' date for this IMPEX module and runbooks. Must
     *            be in the format "yyyy-MM-dd HH:mm:ss"
     * @return the wiki content
     */
    private String getWikiContent(String nameSpace, String runbookName, String authorName, String sysID, String html, File[] images, String dateLong)
    {
        // Returns a string of XML content for the wiki content file per
        // runbook.
        StringBuilder wikiContent = new StringBuilder();

        wikiContent.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        wikiContent.append("\n");
        wikiContent.append("<xwikidoc>\n");
        wikiContent.append("<web>" + nameSpace + "</web>\n");
        wikiContent.append("<sys_id>" + sysID + "</sys_id>\n");
        wikiContent.append("<name>" + runbookName + "</name>\n");
        wikiContent.append("<summary>" + nameSpace + "." + runbookName + "</summary>\n");
        wikiContent.append("<title>" + nameSpace + "." + runbookName + "</title>\n");
        wikiContent.append("<language></language>\n");
        wikiContent.append("<defaultLanguage></defaultLanguage>\n");
        wikiContent.append("<translation></translation>\n");
        wikiContent.append("<parent></parent>\n");
        wikiContent.append("<creator></creator>\n");
        wikiContent.append("<author>" + authorName + "</author>\n");
        wikiContent.append("<contentAuthor></contentAuthor>\n");
        wikiContent.append("<creationDate>1420875307000</creationDate>\n");
        wikiContent.append("<contentUpdateDate>1420875791000</contentUpdateDate>\n");
        wikiContent.append("<tags></tags>\n");
        wikiContent.append("<template></template>\n");

        // iterate through all files and 'attach' images here
        if (images != null && images.length > 0)
        {
            for (File image : images)
            {
                wikiContent.append("<attachment>\n");
                wikiContent.append("<filename>" + image.getName() + "</filename>\n");
                wikiContent.append("<filesize>" + image.length() + "</filesize>\n");
                wikiContent.append("<author>" + authorName + "</author>\n");
                wikiContent.append("<date>" + dateLong + "</date>\n");
                wikiContent.append("<version></version>\n");
                wikiContent.append("<comment></comment>\n");
                wikiContent.append("<is_global>false</is_global>\n");
                wikiContent.append("</attachment>\n");
            }
        }

        wikiContent.append("<model><![CDATA[]]></model>\n");
        wikiContent.append("<abort><![CDATA[]]></abort>\n");
        wikiContent.append("<final><![CDATA[]]></final>\n");
        wikiContent.append("<content><![CDATA[");
        wikiContent.append("{section:type=wysiwyg}\n");
        wikiContent.append(html + "\n");
        wikiContent.append("{section}\n");
        wikiContent.append("]]></content>\n");
        wikiContent.append("<defaultRole>true</defaultRole>\n");
        wikiContent.append("<readRole>admin,resolve_dev,resolve_process,resolve_user</readRole>\n");
        wikiContent.append("<writeRole>admin,resolve_dev,resolve_process,resolve_user</writeRole>\n");
        wikiContent.append("<adminRole>admin,resolve_dev,resolve_process,resolve_user</adminRole>\n");
        wikiContent.append("<executeRole>admin,resolve_dev,resolve_process,resolve_user</executeRole>\n");
        wikiContent.append("<decisionTree></decisionTree>\n");
        wikiContent.append("<weight>0.0</weight>\n");
        wikiContent.append("<displayMode></displayMode>\n");
        wikiContent.append("<catalogId></catalogId>\n");
        wikiContent.append("<wikiParameters> []</wikiParameters>\n");
        wikiContent.append("<isRequestSubmission>false</isRequestSubmission>\n");
        wikiContent.append("<reqestSubmissionOn></reqestSubmissionOn>\n");
        wikiContent.append("<lastReviewedOn></lastReviewedOn>\n");
        wikiContent.append("<lastReviewedBy></lastReviewedBy>\n");
        wikiContent.append("<expireOn></expireOn>\n");
        wikiContent.append("<dtAbortTime>0</dtAbortTime>\n");
        wikiContent.append("</xwikidoc>\n");

        return wikiContent.toString();
    }

    /**
     * Gets the IMPEX module's XML
     *
     * @param actionType
     *            the action type of the module. Can be either
     *            'INSERT_OR_UPDATE' or DELETE
     * @param exportFileName
     *            the name of the zip file you're exporting to
     * @param packageName
     *            the package name of the IMPEX
     * @param authorName
     *            the author to be associated with this IMPEX module
     * @param date
     *            the 'created by' date for this IMPEX module and runbooks. Must
     *            be in the format "yyyy-MM-dd HH:mm:ss"
     * @param moduleSysID
     *            A SysID for this specific module. It must be different from
     *            the SysID of the runbooks
     * 
     * @return the IMPEX module content
     */
    private String getImpexModuleXML(String actionType, String exportFileName, String packageName, String authorName, String date, String moduleSysID)
    {
        // Returns a string of XML content for the impex module xml file
        StringBuilder s = new StringBuilder();
        s.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        s.append("\n");
        s.append("<record_update table=\"resolve_impex_module\">\n");
        s.append("  <resolve_impex_module action=\"" + actionType + "\">\n");
        s.append("    <u_description></u_description>\n");
        s.append("    <u_forward_to_document></u_forward_to_document>\n");
        s.append("    <u_name>" + packageName + "</u_name>\n");
        s.append("    <u_script_name></u_script_name>\n");
        s.append("    <u_version>5.0.1</u_version>\n");
        s.append("    <u_zip_file_name>" + exportFileName + "</u_zip_file_name>\n");
        s.append("    <sys_mod_count>0</sys_mod_count>\n");
        s.append("    <sys_updated_by>" + authorName + "</sys_updated_by>\n");
        s.append("    <sys_updated_on>" + date + "</sys_updated_on>\n");
        s.append("    <sys_id>" + moduleSysID + "</sys_id>\n");
        s.append("  </resolve_impex_module>\n");
        s.append("</record_update>\n");

        return s.toString();
    }

    /**
     * Gets the IMPEX wiki XML
     *
     * @param actionType
     *            the action type of the module. Can be either
     *            'INSERT_OR_UPDATE' or DELETE
     * @param exportFileName
     *            the name of the zip file you're exporting to
     * @param packageName
     *            the package name of the IMPEX
     * @param authorName
     *            the author to be associated with this IMPEX module
     * @param date
     *            the 'created by' date for this IMPEX module and runbooks. Must
     *            be in the format "yyyy-MM-dd HH:mm:ss"
     * @param moduleSysID
     *            A SysID for this specific module. It must be different from
     *            the SysID of the runbooks
     * @param wikiSysID
     *            A SysID for this specific Wiki. It must be different from the
     *            SysID of the runbooks
     * @return the IMPEX module content
     */
    private String getImpexWikiXML(String actionType, String nameSpace, String runbookName, String authorName, String date, String moduleSysID, String wikiSysID)
    {
        // Returns a String of XML content for the impex wiki xml file
        StringBuilder s = new StringBuilder();
        s.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        s.append("\n");
        s.append("<record_update table=\"resolve_impex_wiki\">\n");
        s.append("  <resolve_impex_wiki action=\"" + actionType + "\">\n");
        s.append("    <u_description>{\"ufullname\":\"" + nameSpace + "." + runbookName + "\",\"usummary\":\"" + nameSpace + "." + runbookName + "\"} </u_description>\n");
        s.append("    <u_options>{\"atProperties\":true,\"atPost\":false,\"excludeRefAssessor\":false,\"excludeRefParser\":false,\"excludeRefPreprocessor\":false,\"atNsProperties\":true,\"excludeNsRefAssessor\":false,\"excludeNsRefParser\":false,\"excludeNsRefPreprocessor\":false,\"atNsPosts\":false,\"wikiOverride\":true,\"wikiSubRB\":true,\"wikiTags\":true,\"wikiSubDT\":false,\"wikiPost\":false,\"wikiForms\":false,\"wikiRefATs\":true,\"wikiCatalogs\":false,\"wikiDeleteIfSameSysId\":false,\"wikiDeleteIfSameName\":false,\"startDate\":null,\"endDate\":null,\"wikiNsRefATs\":true,\"wikiNsSubRB\":true,\"wikiNsTags\":false,\"wikiNsSubDT\":false,\"wikiNsOverride\":true,\"wikiNsPost\":false,\"wikiNsForms\":true,\"wikiNsDeleteIfSameSysId\":false,\"wikiNsDeleteIfSameName\":false,\"mSetMenuSection\":false,\"mSetMenuItem\":false,\"mSecMenuItem\":false,\"processWiki\":true,\"processUsers\":true,\"processTeams\":true,\"processForums\":true,\"processRSS\":true,\"processATs\":true,\"processPosts\":false,\"teamUsers\":true,\"teamTeams\":false,\"teamPosts\":false,\"forumUsers\":true,\"forumPosts\":false,\"rssUsers\":true,\"userRoleRel\":false,\"userGroupRel\":false,\"userInsert\":false,\"roleUsers\":false,\"roleGroups\":false,\"groupUsers\":false,\"groupRoleRel\":false,\"formTables\":false,\"formRBs\":true,\"formSS\":true,\"tableForms\":true,\"tableData\":false,\"catalogWikis\":false,\"catalogTags\":true,\"catalogRefCatalogs\":true,\"wsPosts\":false,\"templateForm\":true,\"templateWiki\":true}</u_options>\n");
        s.append("    <u_type>wiki</u_type>\n");
        s.append("    <u_value>" + nameSpace + "." + runbookName + "</u_value>\n");
        s.append("    <u_module>" + moduleSysID + "</u_module>\n");
        s.append("    <sys_created_by>" + authorName + "</sys_created_by>\n");
        s.append("    <sys_created_on>" + date + "</sys_created_on>\n");
        s.append("    <sys_mod_count>0</sys_mod_count>\n");
        s.append("    <sys_updated_by>" + authorName + "</sys_updated_by>\n");
        s.append("    <sys_updated_on>" + date + "</sys_updated_on>\n");
        s.append("    <sys_id>" + wikiSysID + "</sys_id>\n");
        s.append("  </resolve_impex_wiki>\n");
        s.append("</record_update>\n");

        return s.toString();
    }

    /**
     * Creates the IMPEX template for single file conversions
     *
     * @param runbookSysID
     *            The sysID for the runbook
     * @param moduleSysID
     *            The sysID for the IMPEX module
     * @param outputFileLocationPath
     *            the absolute path to the location of the destination zip file
     * @param inputFilesLocationDirectory
     *            the absolute path the to "_files" directory generated by the
     *            conversion process
     * @param nameSpace
     *            the namespace of the runbook
     * @param runbookName
     *            the name of the runbook
     * @param wpmlp
     *            an instance of the WordprocessingMLPackage associated with the
     *            .docx file
     * @param html
     *            the HTML data derived from the docx4j conversion
     * @return a File that points to final the zip file
     */
    private File createImpexTemplate(String runbookSysID, String moduleSysID, String outputFileLocationPath, File inputFilesLocationDirectory, String nameSpace, String runbookName, WordprocessingMLPackage wpmlp, String html) throws FileNotFoundException, IOException
    {
        // Creates a standard directory and file template of the IMPEX module

        File rootDirectory = null;
        String exportDirectoryName = "";
        File installDirectory;
        File uninstallDirectory;
        File updateDirectory;
        File wikiDirectory;
        File socialDirectory;
        File nameSpaceDirectory;
        File wikiPackageContentDirectory;
        File wikiPackageXMLFile;

        File installImpexModuleFile;
        File installImpexWikiFile;

        File uninstallImpexModuleFile;
        File uninstallImpexWikiFile;

        File relationshipJSONFile;
        File wikiNameSpaceContentFile;
        FileWriter fw;

        // create the date
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date exportDate = new Date();
        String date = dateFormat.format(exportDate);

        // create root directory
        File outputFileLocation = new File(outputFileLocationPath);
        exportDirectoryName = "/" + nameSpace + "." + runbookName;

        rootDirectory = new File(outputFileLocation.getParent() + exportDirectoryName + "/");
        rootDirectory.mkdir();

        // create social directory
        socialDirectory = new File(rootDirectory.getCanonicalPath() + "/social");
        socialDirectory.mkdir();

        // create relationships json
        relationshipJSONFile = new File(socialDirectory.getAbsoluteFile() + "/graph_relationships.json");
        relationshipJSONFile.createNewFile();
        String relationshipJSON = getRelationshipJSON();

        fw = new FileWriter(relationshipJSONFile);
        fw.write(relationshipJSON);
        fw.close();
        fw = null;

        // create update directory
        updateDirectory = new File(rootDirectory.getCanonicalPath() + "/update");
        updateDirectory.mkdir();

        // create install directory
        installDirectory = new File(updateDirectory.getCanonicalPath() + "/install");
        installDirectory.mkdir();

        // create resolve impex module
        installImpexModuleFile = new File(installDirectory.getCanonicalPath() + "/" + "resolve_impex_module_" + moduleSysID + ".xml");
        installImpexModuleFile.createNewFile();
        String installImpexModuleXML = getImpexModuleXML("INSERT_OR_UPDATE", outputFileLocation.getName(), runbookName, "admin", date, moduleSysID);

        fw = new FileWriter(installImpexModuleFile);
        fw.write(installImpexModuleXML);
        fw.close();
        fw = null;

        // create resolve impex wiki
        installImpexWikiFile = new File(installDirectory.getCanonicalPath() + "/" + "resolve_impex_wiki_" + runbookSysID + ".xml");
        installImpexWikiFile.createNewFile();
        String installImpexWikiXML = getImpexWikiXML("INSERT_OR_UPDATE", nameSpace, runbookName, "admin", date, moduleSysID, runbookSysID);

        fw = new FileWriter(installImpexWikiFile);
        fw.write(installImpexWikiXML);
        fw.close();
        fw = null;

        // create uninstall directory
        uninstallDirectory = new File(updateDirectory.getCanonicalPath() + "/uninstall");
        uninstallDirectory.mkdir();

        // create resolve impex module
        uninstallImpexModuleFile = new File(uninstallDirectory.getCanonicalPath() + "/" + "resolve_impex_module_" + moduleSysID + ".xml");
        uninstallImpexModuleFile.createNewFile();
        String uninstallImpexModuleXML = getImpexModuleXML("DELETE", outputFileLocation.getName(), runbookName, "admin", date, moduleSysID);

        fw = new FileWriter(uninstallImpexModuleFile);
        fw.write(uninstallImpexModuleXML);
        fw.close();
        fw = null;

        // create resolve impex wiki
        uninstallImpexWikiFile = new File(uninstallDirectory.getCanonicalPath() + "/" + "resolve_impex_wiki_" + runbookSysID + ".xml");
        uninstallImpexWikiFile.createNewFile();
        String uninstallImpexWikiXML = getImpexWikiXML("DELETE", nameSpace, runbookName, "admin", date, moduleSysID, runbookSysID);

        fw = new FileWriter(uninstallImpexWikiFile);
        fw.write(uninstallImpexWikiXML);
        fw.close();
        fw = null;

        // create wiki directory
        wikiDirectory = new File(rootDirectory.getCanonicalPath() + "/wiki");
        wikiDirectory.mkdir();

        // create namespace directory
        nameSpaceDirectory = new File(wikiDirectory.getCanonicalPath() + "/" + nameSpace);
        nameSpaceDirectory.mkdir();

        // create package xml
        String packageXML = getPackageXMLSingleFile(nameSpace, runbookName, "admin", date);
        wikiPackageXMLFile = new File(wikiDirectory.getCanonicalPath() + "/package.xml");
        wikiPackageXMLFile.createNewFile();
        fw = new FileWriter(wikiPackageXMLFile);
        fw.write(packageXML);
        fw.close();
        fw = null;

        // create package content folder
        wikiPackageContentDirectory = new File(wikiDirectory.getCanonicalPath() + "/" + nameSpace + "/");
        wikiPackageContentDirectory.createNewFile();

        // copy image files from _files dir to the impex folder
        File[] inputFiles = inputFilesLocationDirectory.listFiles();
        String wikiContent = getWikiContent(nameSpace, runbookName, "admin", runbookSysID, html, inputFiles, Long.toString(exportDate.getTime()));

        // create wiki runbook file
        wikiNameSpaceContentFile = new File(wikiPackageContentDirectory.getCanonicalPath() + "/" + runbookName);
        fw = new FileWriter(wikiNameSpaceContentFile);
        fw.write(wikiContent);
        fw.close();
        fw = null;
        System.gc();

        if (inputFiles != null)
        {
            for (File asset : inputFiles)
            {
                String directoryPath = wikiPackageContentDirectory.getCanonicalPath();
                String assetName = runbookName + "#" + asset.getName();
                File fileCopy = new File(directoryPath + "/" + assetName);
                FileUtils.copyFile(asset, fileCopy);
            }
        }
        else
        {
            System.out.println("No pictures were found to export.");
        }

        // Remove the _files directory when you're done.
        System.gc();
        inputFilesLocationDirectory.delete();

        // wpmlp.getExternalResources()
        return rootDirectory;

    }

    /**
     * Checks to see if the runbook name contains any illegal characters
     *
     * @param runbookName
     *            the name of the runbook
     * @return a boolean value if there was an error detected
     */
    private boolean checkIfRunbookError(String runbookName)
    {
        // checks to see if the runbook name has any invalid characters in it

        // System.out.println("Checking for valid runbookName: " + runbookName);
        String specialCharacterCheckRegex = "[^a-zA-Z0-9\\-\\_ ]";
        Pattern p = Pattern.compile(specialCharacterCheckRegex);
        Matcher m = p.matcher(runbookName);
        boolean illegalCharsFound = m.find();
        if (illegalCharsFound)
        {
            System.out.println("ERROR: A runbook naming error has occured. Illegal characters were found.");
            return true;
        }
        else
        {
            // System.out.println("Valid runbook name.");
            return false;
        }
    }

    /**
     * Sanitizes a runbook name by replacing illegal characters with '_'
     *
     * @param runbookName
     *            the name of the runbook
     * @return a sanitized runbook name
     */
    private String sanitizeRunbookName(String runbookName)
    {
        // if a runbook contains any illegal characters, replace it with "_"

        System.out.println("Checking the runbook name for illegal characters: " + runbookName);
        String specialCharacterCheckRegex = "[^a-zA-Z0-9\\-\\_ ]";
        Pattern p = Pattern.compile(specialCharacterCheckRegex);
        Matcher m = p.matcher(runbookName);
        boolean illegalCharsFound = m.find();
        if (illegalCharsFound)
        {
            System.out.println("Sanitizing runbook name. Only the following valid characters are permitted in runbook names: a-z, A-Z, 0-9, -, and _");
            String newRunbookName = m.replaceAll("_");
            System.out.println("New runbook name: " + newRunbookName);
            newRunbookName = newRunbookName.trim();
            return newRunbookName;
        }
        else
        {
            return runbookName;
        }
    }

    /**
     * Checks for invalid characters in a namespace
     *
     * @param nameSpace
     *            the name of the name space
     * @return a sanitized name space value
     */
    private boolean isNameSpaceValid(String nameSpace)
    {
        // checks if a namespace contains any invalid characters

        String specialCharacterCheckRegex = "[^a-zA-Z0-9\\-\\_& ]";
        Pattern p = Pattern.compile(specialCharacterCheckRegex);
        Matcher m = p.matcher(nameSpace);
        boolean illegalCharsFound = m.find();
        if (illegalCharsFound)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Checks for invalid characters in a filename
     *
     * @param nameSpace
     *            the name of the name space
     * @return a sanitized name space value
     */
    private boolean isFileNameValid(String filename)
    {
        // checks if a filename contains any invalid characters
        // System.out.println("Checking filename for illegal characters: " +
        // filename);

        // remove extension
        String fileNameFinal = filename;

        // Remove the extension.
        int extensionIndex = fileNameFinal.lastIndexOf(".");
        if (extensionIndex >= 0)
        {
            fileNameFinal = fileNameFinal.substring(0, extensionIndex);
        }

        // System.out.println("filename without extension: " + fileNameFinal);

        String specialCharacterCheckRegex = "[^a-zA-Z0-9\\-\\_]";
        Pattern p = Pattern.compile(specialCharacterCheckRegex);
        Matcher m = p.matcher(fileNameFinal);

        boolean illegalCharsFound = m.find();
        // boolean illegalCharsFound = false;

        if (illegalCharsFound)
        {
            System.out.println("Illegal characters found in filename: " + filename);
            return false;
        }
        else
        {
            if (filename.charAt(filename.length() - 1) == ' ')
            {
                System.out.println("The file name cannot end with a ' ': " + filename);
                return false;
            }
            else
            {
                // System.out.println("Destination file name is valid: " +
                // filename);
                return true;
            }
        }
    }

    /**
     * Returns a {@link File} object that points to the zip file location
     * 
     * @param impexOutputFilePath
     *            the path to the zip file
     * @param nameSpace
     *            the namespace associated with this docx conversion. Only
     *            necessary if the impexOuputPath is a directory.
     * @param runbookName
     *            the runbook name associated with this docx conversion. Only
     *            necessary if the impexOuputPath is a directory.
     * @return A {@link File} object that points to the zip file location
     * @throws IOException
     *             occurs if there is a problem creating the zip file
     * @throws Exception
     */
    private File getZipFile(String impexOutputFilePath, String nameSpace, String runbookName) throws IOException, Exception
    {
        File impexOutputFilePathFile = null;

        impexOutputFilePathFile = new File(impexOutputFilePath);
        // System.out.println("Testing file name: " +
        // impexOutputFilePathFile.getName());
        try
        {
            if (impexOutputFilePathFile.isDirectory())
            {
                impexOutputFilePathFile = new File(impexOutputFilePathFile.getCanonicalPath() + "/" + nameSpace + "_" + runbookName + ".zip");
            }
            else if (!isFileNameValid(impexOutputFilePathFile.getName()))
            {
                throw new Exception("The destination file name included invalid characters. IMPEX import file names can only contain the following characters: a-z, A-Z, 0-9, -, _, and ' '");
            }
            else if (!FileUtils.getFileExtension(impexOutputFilePathFile.getCanonicalPath()).equalsIgnoreCase(".zip"))
            {
                // System.out.println("CAN'T FIND ZIP. ADDING 'zip'");
                impexOutputFilePathFile = new File(impexOutputFilePath + ".zip");
                System.out.println(impexOutputFilePathFile.getCanonicalPath());
            }
        }
        catch (StringIndexOutOfBoundsException ex)
        {

            // Catches a bug in linux if no .zip extension is given in the file
            // path
            impexOutputFilePathFile = new File(impexOutputFilePath + ".zip");
            System.out.println(impexOutputFilePathFile.getCanonicalPath());
        }

        return impexOutputFilePathFile;
    }

    private File getZipFilePackageName(String impexOutputFilePath, String packageName) throws IOException, Exception
    {
        File impexOutputFilePathFile = null;

        impexOutputFilePathFile = new File(impexOutputFilePath);
        // System.out.println("Testing file name: " +
        // impexOutputFilePathFile.getName());
        if (impexOutputFilePathFile.isDirectory())
        {
            impexOutputFilePathFile = new File(impexOutputFilePathFile.getCanonicalPath() + "/" + packageName + ".zip");
        }
        else if (!isFileNameValid(impexOutputFilePathFile.getName()))
        {
            throw new Exception("The destination file name included invalid characters. IMPEX import file names can only contain the following characters: a-z, A-Z, 0-9, -, _, and ' '");
        }
        else if (!FileUtils.getFileExtension(impexOutputFilePathFile.getCanonicalPath()).equalsIgnoreCase(".zip"))
        {
            // System.out.println("CAN'T FIND ZIP. ADDING 'zip'");
            impexOutputFilePathFile = new File(impexOutputFilePath + ".zip");
            System.out.println(impexOutputFilePathFile.getCanonicalPath());
        }

        return impexOutputFilePathFile;
    }

    /**
     * Exports a docx file to an IMPEX file
     *
     * @param docxSourceFilePath
     *            the absolute path of the .docx source file
     * @param nameSpace
     *            the name of the name space
     * @param runbookName
     *            the name of the runbook
     * @param impexOutputFilePath
     *            the absolute path to the destination zip file.
     * @return a sanitized name space value
     */
    public boolean exportDocxToImpex(String docxSourceFilePath, String nameSpace, String runbookName, String impexOutputFilePath) throws Exception
    {
        System.out.println("Staring conversion of " + docxSourceFilePath);

        // ------ INIT

        Boolean filePathCheck = true;
        File inputFile;
        String sysIdRunbook = SysId.getSysId();
        String sysIdModule = SysId.getSysId();

        // System.out.println("GETTING INPUTS...");
        // verify the inputs
        if (docxSourceFilePath == null)
        {
            filePathCheck = false;
            throw new Exception("No docx source filepath given.");
        }
        else if (docxSourceFilePath.equals(""))
        {
            filePathCheck = false;
            throw new Exception("An empty docx source filepath is not acceptable.");
        } else if (docxSourceFilePath.matches(".*[&\\*~\\^].*"))
        {
            filePathCheck = false;
            throw new Exception("The docx file cannot contain the following characters: & * ~ ^");
        }

        if (nameSpace == null)
        {
            filePathCheck = false;
            throw new Exception("No namespace given.");
        }
        else if (nameSpace.equals(""))
        {
            filePathCheck = false;
            throw new Exception("An empty namespace is not acceptable.");
        }
        else if (!isNameSpaceValid(nameSpace))
        {
            filePathCheck = false;
            throw new Exception("The given namespace includes illegal characters.");
        }

        if (runbookName == null)
        {
            filePathCheck = false;
            throw new Exception("No runbook name given.");
        }
        else if (runbookName.equals(""))
        {
            filePathCheck = false;
            throw new Exception("An empty runbook name is not acceptable.");
        }
        else if (checkIfRunbookError(runbookName))
        {
            filePathCheck = false;
            throw new Exception("A runbookName can only contain the following characters: a-z, A-Z, 0-9, -, and _");
        }

        if (impexOutputFilePath == null)
        {
            filePathCheck = false;
            throw new Exception("No impex output filepath given.");
        }
        else if (impexOutputFilePath.equals(""))
        {
            filePathCheck = false;
            throw new Exception("An empty impex output filepath is not acceptable.");
        }

        if (filePathCheck)
        {

            WordprocessingMLPackage w;
            String html;

            docxSourceFilePath = docxSourceFilePath.trim();
            String fileExtension = FilenameUtils.getExtension(docxSourceFilePath);
            if (fileExtension.equalsIgnoreCase("docx"))
            {
                inputFile = new File(docxSourceFilePath);
                w = openDocxFile(inputFile);
                String filePath;

                // Check which OS they're using

                String OS = System.getProperty("os.name").toLowerCase();
                Boolean isLinux = false;
                if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0)
                {
                    isLinux = true;
                }

                if (isLinux)
                {
                    // On linux, use relative path
                    // System.out.println("FILE PATH ORIGINAL: " +
                    // inputFile.getCanonicalPath());
                    filePath = inputFile.getName();
                }
                else
                {
                    // On windows, or any other OS use absolute path
                    filePath = inputFile.getCanonicalPath();
                }

                // System.out.println("FILE PATH: " + filePath);
                String fileName = inputFile.getName();

                // System.out.println("GETTING HTML FROM INPUT FILE...");

                html = getHTML(docxSourceFilePath, w);

                // System.out.println("UPDATING IMAGE TAGS...");
                String newHTML = updateHTMLImageTagsToMacros(html, filePath);

                // System.out.println("TAGS UPDATED");
                File impexDirectory = null;
                File inputFilesLocationDirectory = null;

                try
                {
                    inputFilesLocationDirectory = new File(docxSourceFilePath + filesSuffix + "/");
                    impexDirectory = createImpexTemplate(sysIdRunbook, sysIdModule, impexOutputFilePath, inputFilesLocationDirectory, nameSpace, runbookName, w, newHTML);
                    // System.out.println("Impex directory: " + impexDirectory);

                    // Check impexOutputFilePath for proper format.
                    // If a directory is given, the zip filename will be the
                    // namespace_runbookname.zip
                    File impexOutputFilePathFile = getZipFile(impexOutputFilePath, nameSpace, runbookName);

                    // zip up the impex directory
                    Compress c = new Compress();
                    c.zip(impexDirectory, impexOutputFilePathFile);

                    System.out.println("\n****************************\nCONVERSION PROCESS COMPLETE:");
                    System.out.println("Your IMPEX .zip file is at : " + impexOutputFilePath + "\n********************");

                    return true;

                }
                catch (IOException ioe)
                {
                    Log.log.error(ioe.getMessage(), ioe);
                    System.out.println("An error occurred while saving the file: " + ioe.getMessage());
                    ioe.printStackTrace();
                }
                catch (Exception e)
                {

                    Log.log.error(e.getMessage(), e);
                    System.out.println("An error occurred while saving the file: " + e.getMessage());
                    e.printStackTrace();
                    throw e;

                }
                finally
                {

                    // delete impex folder
                    FileUtils.deleteDirectory(impexDirectory);

                    // delete _files folder
                    FileUtils.deleteDirectory(inputFilesLocationDirectory);

                }
            }
            else
            {
                System.out.println("The source file is not a docx file.");
            }

        }
        else
        {
            return false;
        }
        return false;

    }

    /**
     * Exports a directory of docx files to an IMPEX file
     *
     * @param docxDirectoryFilePath
     *            the absolute path of the .docx source file
     * @param nameSpace
     *            the name of the name space
     * @param impexOutputFilePath
     *            the absolute path to the destination zip file.
     * @return a sanitized name space value
     */
    public boolean exportDirectoryToImpex(String docxDirectoryFilePath, String nameSpace, String impexOutputFilePath) throws Exception
    {
        // System.out.println("Starting conversion of all .docx files in " +
        // docxDirectoryFilePath);

        // ------ INIT

        Boolean filePathCheck = true;
        File inputFile;
        FileWriter fw;
        String packageName = nameSpace + "_directory_conversion";

        // Get date
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date exportDate = new Date();
        String date = dateFormat.format(exportDate);

        // The impex module needs its own Sys Id
        String impexModuleSysID = SysId.getSysId();

        // verify the inputs
        // check docx directory
        if (docxDirectoryFilePath == null)
        {
            filePathCheck = false;
            throw new Exception("No docx source filepath given.");
        }
        else if (docxDirectoryFilePath.equals(""))
        {
            filePathCheck = false;
            throw new Exception("An empty docx source filepath is not acceptable.");
        }

        // Check namespace
        if (nameSpace == null)
        {
            filePathCheck = false;
            throw new Exception("No namespace given.");
        }
        else if (nameSpace.equals(""))
        {
            filePathCheck = false;
            throw new Exception("An empty namespace is not acceptable.");
        }
        else if (!isNameSpaceValid(nameSpace))
        {
            filePathCheck = false;
            throw new Exception("The given namespace includes illegal characters.");
        }

        // Check impex output path
        if (impexOutputFilePath == null)
        {
            filePathCheck = false;
            throw new Exception("No impex output file given.");
        }
        else if (impexOutputFilePath.equals(""))
        {
            filePathCheck = false;
            throw new Exception("An empty impex output file is not acceptable.");
        }

        // If all is well...
        if (filePathCheck)
        {

            // get list of files in the directory
            System.out.println("docx directory: " + docxDirectoryFilePath);
            String docxDirectoryFilePathTrimmed = docxDirectoryFilePath.trim();
            System.out.println("docx directory trimmed: " + docxDirectoryFilePathTrimmed);
            File inputLocationDirectory = new File(docxDirectoryFilePathTrimmed);
            File[] docxFiles = inputLocationDirectory.listFiles();
            if (docxFiles == null)
            {
                throw new Exception("ERROR: No docx files were found in this directory.");
            }
            else
            {
                // System.out.println("DOCX FILES: " + docxFiles);

                File impexOutputFile = new File(impexOutputFilePath);
                // impexOutputFileDirectory.mkdir();

                String impexZipFilePath = packageName + ".zip";

                String outputFileParentDirectory = impexOutputFile.getParent();
                System.out.println("Output file parent: " + outputFileParentDirectory);
                File tempIMPEXZipFolder = new File(outputFileParentDirectory + "/" + packageName);
                tempIMPEXZipFolder.mkdir();

                WordprocessingMLPackage w;
                String html = "<html></html>";

                // ------ SOCIAL

                // create social folder
                File socialFolder = new File(tempIMPEXZipFolder.getCanonicalPath() + "/social");
                socialFolder.mkdir();
                // create social file
                File relationshipsFile = new File(socialFolder.getCanonicalPath() + "/graph_relationships.json");
                String relationshipsJSON = getRelationshipJSON();
                fw = new FileWriter(relationshipsFile);
                fw.write(relationshipsJSON);
                fw.close();
                fw = null;
                // System.out.println("BUILT SOCIAL FOLDER...");

                // ------ UPDATE

                // create update folder
                File updateFolder = new File(tempIMPEXZipFolder.getCanonicalPath() + "/update");
                updateFolder.mkdir();
                // create install folder
                File installFolder = new File(updateFolder.getCanonicalPath() + "/install");
                installFolder.mkdir();
                // create uninstall folder
                File uninstallFolder = new File(updateFolder.getCanonicalPath() + "/uninstall");
                uninstallFolder.mkdir();
                // System.out.println("BUILT UPDATE FOLDER...");

                // create impex install module file
                File impexInstallModuleFile = new File(installFolder.getCanonicalPath() + "/resolve_impex_module_" + impexModuleSysID + ".xml");
                String impexInstallModuleXML = getImpexModuleXML("INSERT_OR_UPDATE", impexZipFilePath, packageName, "admin", date, impexModuleSysID);
                fw = new FileWriter(impexInstallModuleFile);
                fw.write(impexInstallModuleXML);
                fw.close();
                fw = null;

                // create impex uninstall module file
                File impexUninstallModuleFile = new File(uninstallFolder.getCanonicalPath() + "/resolve_impex_module_" + impexModuleSysID + ".xml");
                String uninstallImpexModuleXML = getImpexModuleXML("DELETE", impexZipFilePath, packageName, "admin", date, impexModuleSysID);
                fw = new FileWriter(impexUninstallModuleFile);
                fw.write(uninstallImpexModuleXML);
                fw.close();
                fw = null;

                // System.out.println("CREATED INSTALL AND UNINSTALL FILES...");

                ArrayList<String> runbookNameList = new ArrayList<String>();
                ArrayList<File> docxFilesDirectoriesToDelete = new ArrayList<File>();

                // ------ WIKI
                File wikiFolder = new File(tempIMPEXZipFolder.getCanonicalPath() + "/wiki");
                wikiFolder.mkdir();
                // System.out.println("BUILT WIKI FOLDER...");

                // create namespace folder
                File nameSpaceFolder = new File(wikiFolder.getCanonicalPath() + "/" + nameSpace);
                nameSpaceFolder.mkdir();
                // System.out.println("BUILT NAMESPACE FOLDER...");
                System.out.println("Namespace folder: " + nameSpaceFolder.getCanonicalPath());

                // Keeps a list of .docx file that pass and fail the conversion
                // process

                // System.out.println("CONVERTING FILES1: " +
                // docxFiles.toString());
                ArrayList<String[]> conversionOutcomes = new ArrayList<String[]>();
                ArrayList<String> finalRunbookNameList = new ArrayList<String>();

                // System.out.println("CONVERTING FILES2: " +
                // docxFiles.toString());
                if (docxFiles != null)
                {
                    if (docxFiles.length > 0)
                    {
                        for (File docXFile : docxFiles)
                        {

                            // ignore hidden files
                            if (!docXFile.isHidden())
                            {

                                // The outcome array is used to describe whether
                                // the
                                // conversion was
                                // successful or not.

                                String[] outcome = { docXFile.getName(), "CONVERTING" };
                                conversionOutcomes.add(outcome);

                                String ext = FilenameUtils.getExtension(docXFile.getCanonicalPath());
                                // check if docx file
                                if (ext.equalsIgnoreCase("docx"))
                                {

                                    try
                                    {
                                        System.out.println("\n-------------\nConverting file: " + docXFile.getName() + "\n-------------");

                                        // make a sys ID
                                        String runbookSysID = SysId.getSysId();

                                        // runbook name (without the extension)
                                        String runbookName = docXFile.getName().substring(0, docXFile.getName().length() - 5);
                                        // System.out.println("Runbook name: " +
                                        // runbookName);
                                        String runbookNameSanitized = sanitizeRunbookName(runbookName);
                                        // System.out.println("Runbook name
                                        // (Sanitized): "
                                        // + runbookNameSanitized);
                                        runbookNameList.add(runbookNameSanitized);

                                        // ------ UPDATE IMPEX

                                        // UPDATE INSTALL

                                        // create
                                        // update/install/resolve_impex_wiki_[sysID].xml
                                        File updateInstallImpex = new File(installFolder.getCanonicalPath() + "/resolve_impex_wiki_" + runbookSysID + ".xml");
                                        String installImpexWiki = getImpexWikiXML("INSERT_OR_UPDATE", nameSpace, runbookNameSanitized, "admin", date, impexModuleSysID, runbookSysID);
                                        fw = new FileWriter(updateInstallImpex);
                                        fw.write(installImpexWiki);
                                        fw.close();
                                        fw = null;

                                        // UPDATE UNINSTALL

                                        // create update/uninstall/impex wiki
                                        File updateUninstallImpex = new File(uninstallFolder.getCanonicalPath() + "/resolve_impex_wiki_" + runbookSysID + ".xml");
                                        String uninstallImpexWiki = getImpexWikiXML("DELETE", nameSpace, runbookNameSanitized, "admin", date, impexModuleSysID, runbookSysID);
                                        fw = new FileWriter(updateUninstallImpex);
                                        fw.write(uninstallImpexWiki);
                                        fw.close();
                                        fw = null;

                                        // ------ WIKI

                                        // create wiki namespace content
                                        // System.out.println("The docx file is:
                                        // "
                                        // +
                                        // f);

                                        w = openDocxFile(docXFile);
                                        // System.out.println("Word process");
                                        html = getHTML(docXFile.getCanonicalPath(), w);

                                        File inputFilesLocationDirectory = new File(docXFile.getCanonicalPath() + filesSuffix + "/");
                                        String filePath = inputFilesLocationDirectory.getCanonicalPath();

                                        System.out.println("CHECKING OS...");

                                        // OS Check
                                        String OS = System.getProperty("os.name").toLowerCase();
                                        String docxFilePath = "";
                                        Boolean isLinux = false;
                                        if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0)
                                        {
                                            isLinux = true;
                                        }

                                        if (isLinux)
                                        {
                                            // On linux, use relative path
                                            // System.out.println("FILE PATH
                                            // ORIGINAL: "
                                            // + docXFile.getCanonicalPath());
                                            docxFilePath = docXFile.getName();
                                        }
                                        else
                                        {
                                            // On windows, or any other OS use
                                            // absolute path
                                            docxFilePath = docXFile.getCanonicalPath();
                                        }

                                        System.out.println("DOCXFILEPATH..." + docxFilePath);

                                        String newHTML = updateHTMLImageTagsToMacros(html, docxFilePath);

                                        // copy image files from _files dir to
                                        // the
                                        // impex folder

                                        docxFilesDirectoriesToDelete.add(inputFilesLocationDirectory);
                                        File[] inputFiles = inputFilesLocationDirectory.listFiles();
                                        String wikiContent = getWikiContent(nameSpace, runbookNameSanitized, "admin", runbookSysID, newHTML, inputFiles, Long.toString(exportDate.getTime()));

                                        // create wiki runbook file
                                        File wikiNameSpaceContentFile = new File(nameSpaceFolder.getCanonicalPath() + "/" + runbookNameSanitized);
                                        fw = new FileWriter(wikiNameSpaceContentFile);
                                        fw.write(wikiContent);
                                        fw.close();
                                        fw = null;
                                        System.gc();

                                        // copy .docx _files to the wiki
                                        // nameSpaceFolder
                                        if (inputFiles != null)
                                        {
                                            for (File asset : inputFiles)
                                            {
                                                File fileCopy = new File(nameSpaceFolder.getCanonicalPath() + "/" + runbookNameSanitized + "#" + asset.getName());
                                                FileUtils.copyFile(asset, fileCopy);
                                            }
                                        }
                                        else
                                        {
                                            // System.out.println("_files
                                            // directory was not found.");
                                        }

                                        conversionOutcomes.get(conversionOutcomes.size() - 1)[1] = "SUCCESS: The .docx file was converted and added to the impex module.";
                                        finalRunbookNameList.add(runbookNameSanitized);
                                    }
                                    catch (Exception de)
                                    {
                                        Log.log.error(de.getMessage(), de);
                                        conversionOutcomes.get(conversionOutcomes.size() - 1)[1] = "*** FAILED ***: An error occurred while converting this docx file.\n" + de.getLocalizedMessage();
                                    }

                                }
                                else
                                {
                                    conversionOutcomes.get(conversionOutcomes.size() - 1)[1] = "Not a .docx file.";
                                }
                            }

                        }
                    }

                    // create package xml
                    String packageXML = getPackageXMLBulkFile(nameSpace, packageName, runbookNameList, "admin", date);
                    File packageFile = new File(wikiFolder.getCanonicalPath() + "/package.xml");
                    fw = new FileWriter(packageFile);
                    fw.write(packageXML);
                    fw.close();
                    fw = null;

                    // ------ ZIP IT UP

                    Compress c = new Compress();
                    File zipFile = getZipFilePackageName(impexOutputFilePath, packageName);

                    c.zip(tempIMPEXZipFolder, zipFile);
                    c = null;

                    // ------ CLEAN IT ALL UP

                    // delete impex folder
                    FileUtils.deleteDirectory(tempIMPEXZipFolder);

                    // delete _files folder
                    for (File filesDirectory : docxFilesDirectoriesToDelete)
                    {
                        FileUtils.deleteDirectory(filesDirectory);
                    }

                    // ------ DONE

                    System.out.println("\n****************************\nCONVERSION PROCESS COMPLETE:\n****************************\n");
                    System.out.println("Conversion results:\n-------------------------------------");

                    for (int i = 0; i < conversionOutcomes.size(); i++)
                    {
                        if (!conversionOutcomes.get(i)[1].equalsIgnoreCase("Not a .docx file."))
                        {
                            System.out.println("FILE: " + conversionOutcomes.get(i)[0]);
                            System.out.println("STATUS: " + conversionOutcomes.get(i)[1]);
                            System.out.println("......");
                        }
                    }

                    System.out.println("\n--- List of runbook names: ---\n");
                    for (String rbn : finalRunbookNameList)
                    {
                        System.out.println(nameSpace + "." + rbn);
                    }

                    System.out.println("\n********************\nYour IMPEX .zip file is at : " + zipFile.getCanonicalPath() + "\n********************");

                    return true;
                }
            }

        }

        return false;
    }

    /**
     * Converts a docx file content to HTML, extracts its embedded images, and
     * returns the converted HTML content
     */
    public String convertDocxToWiki(String docxSourcePath, String wikiID, String wikiName, String username) throws Exception
    {

        // Get the file
        File docXFile = new File(docxSourcePath);
        WordprocessingMLPackage wpmlp = openDocxFile(docXFile);

        // convert the docx content and save images to docxSourcePath_images
        String html = getHTML(docXFile.getCanonicalPath(), wpmlp);

        html = updateHTMLImageTagsToMacros(html, docXFile.getCanonicalPath());
        /*
         * Docx4j generates html, with some #if - then syntax, out of docx files.
         * It dies when Velocity renderer tries to render it cause the #if syntax generated by Docx4j
         * is syntactically incorrect for the renderer. It's a temporary fix. Will try to find proper
         * fix in 6.2
         */
        html = html.replace("#if", "").replace(">then<", "><");
        html = "{pre}" + html + "{pre}";
        
        // attach images to wiki
        attachImagesToWiki(docxSourcePath, wikiID, wikiName, username);

        return html;
    }

    private String attachImagesToWiki(String docxSourcePath, String wikiID, String wikiName, String userName)
    {
        //System.out.println("Attaching images to wiki...");
        File docXImageFolder = new File(docxSourcePath + filesSuffix + "/");

        File[] imgFiles = docXImageFolder.listFiles();
        FileInputStream fis;
        InputStream uploadedStream;
        try
        {
            if(imgFiles != null) {
                for (File asset : imgFiles)
                {
    
                    // String fileName =
                    // item.getName();//C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
                    // fileName = fileName.substring(fileName.lastIndexOf("\\") +
                    // 1);
                    // String contentType = item.getContentType();//text/plain
                    // boolean isInMemory = item.isInMemory();
    
                    // System.out.println("Adding image: " + asset.getName());
                    long sizeInBytes = asset.length();
    
                    fis = new FileInputStream(asset);
                    uploadedStream = fis;
                    byte[] data = new byte[(int) sizeInBytes];
                    uploadedStream.read(data);
    
                    WikiAttachmentContentVO wac = new WikiAttachmentContentVO();
                    wac.setUContent(data);
    
                    WikiAttachmentVO wikiAttachment = new WikiAttachmentVO();
                    wikiAttachment.setUFilename(asset.getName());
                    wikiAttachment.setUSize(data.length);
                    wikiAttachment.usetWikiAttachmentContent(wac);
    
                    //System.out.println("Adding image: " + wikiID + ", " + wikiName + ", " + wikiAttachment + ", " + userName);
                    ServiceWiki.addAttachmentToWiki(wikiID, wikiName, wikiAttachment, userName);
    
                    fis.close();
    
                }
            }

            // Clean it up
            FileUtils.deleteDirectory(docXImageFolder);

        }
        catch (FileNotFoundException e)
        {
            //System.out.println();
            Log.log.error(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return "";
    }

}
