/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.xml.resolver.apps.resolver;
import org.apache.commons.collections.CollectionUtils;

import com.resolve.persistence.model.ResolveCron;
import com.resolve.persistence.util.HibernateMethod;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CronUtil
{
    public static List<ResolveCronVO> getResolveCronJobs(QueryDTO query, String username)
    {
        List<ResolveCronVO> result = new ArrayList<ResolveCronVO>();

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if (data != null)
            {
                //grab the map of sysId-organizationname  
//                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                //get the Roles for all the organizations
//                Map<String, String> mapOfAppsAndRoles =  getMapOfAppsAndRoles();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveCron instance = new ResolveCron();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveCron instance = (ResolveCron) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }
    
    
    @SuppressWarnings("unchecked")
	public static List<ResolveCronVO> findAllActiveCronJobs(boolean onlyActiveJobs)
    {
        List<ResolveCronVO> result = new ArrayList<ResolveCronVO>();
        List<ResolveCron> cronTasks = null;
        try
        {
            ResolveCron example = new ResolveCron();
            example.setUActive(onlyActiveJobs);

			cronTasks = (List<ResolveCron>) HibernateProxy.execute(() -> {

				if (onlyActiveJobs) {
					return HibernateUtil.getDAOFactory().getResolveCronDAO().find(example);
				} else {
					return HibernateUtil.getDAOFactory().getResolveCronDAO().findAll();
				}

			});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        if(cronTasks != null)
        {
            for (ResolveCron dbTask : cronTasks)
            {
                // check if the task is active
                if (dbTask.ugetUActive() == true)
                {
                    result.add(dbTask.doGetVO());
                }
            }
        }

        
        return result;
        
    }
    
    public static ResolveCronVO findCronJob(String sysId, String name, String username)
    {
        ResolveCronVO result = null;
        ResolveCron cronTask = null;

        try
        {
            cronTask = findCronJobModelById(sysId, name, username);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        if (cronTask != null)
        {
            result = cronTask.doGetVO();
        }

        return result;
    }

    public static void deleteCronByName(String name, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        if(StringUtils.isNotBlank(name))
        {
            
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		ResolveCron cronTask = new ResolveCron();
            		cronTask.setUName(name);
	                
	                cronTask = HibernateUtil.getDAOFactory().getResolveCronDAO().findFirst(cronTask);
	                if(cronTask != null)
	                {
	                    HibernateUtil.getDAOFactory().getResolveCronDAO().delete(cronTask);
	                }
                
            	});
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
    }
    
    public static void deleteCronJobsById(String[] sysIds, QueryDTO query, String username)
    {
        if(sysIds != null)
        {
            for(String sysId : sysIds)
            {
                deleteCronJobById(sysId, username);
            }
        }
    }
    
    
    public static ResolveCronVO saveResolveCron(ResolveCronVO vo, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        if(vo != null && vo.validate())
        {
            ResolveCron model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findCronJobModelById(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("Cron with sysId " + vo.getSys_id() + " does not exist.");
                }
            }
            else
            {
                model = findCronJobModelById(null, vo.getUName(), username);
                if(model != null)
                {
                    throw new Exception("Cron with name " + vo.getUName() + " already exist");
                }
                
                model = new ResolveCron();
            }
            model.applyVOToModel(vo);
            model.setChecksum(calculateChecksum(model));
            model = SaveUtil.saveResolveCron(model, username);
            vo = model.doGetVO();
            
        }
        
        return vo;
    }
    
    public static void activateDeactivateCronJob(String[] ids, boolean activateJob, String username)
    {
        if(ids != null)
        {
            for(String sysId : ids)
            {
                activateDeactivateCronJob(sysId, activateJob, username);
            }
            
        }
    }
    
   
   
    @SuppressWarnings("unchecked")
    private static ResolveCron findCronJobModelById(String sysId, String name, String username)
    {   	   	
		try {
        HibernateProxy.setCurrentUser(username);
			return (ResolveCron) HibernateProxy.execute(() -> {
				ResolveCron result = null;
				try
			    {	
					
			        if (StringUtils.isNotBlank(sysId))
			        {
			            result  = HibernateUtil.getDAOFactory().getResolveCronDAO().findById(sysId);
			        }
			        else if (StringUtils.isNotBlank(name))
			        {
			            String sql = "from ResolveCron where LOWER(UName) = '" + name.trim().toLowerCase() + "'";
			            
			            List<ResolveCron> list = HibernateUtil.createQuery(sql).list();
			            
			            if (list != null && list.size() > 0)
			            {
			                result = list.get(0);
			            }
			        }	            

			    }
			    catch (Throwable e)
			    {
			        Log.log.warn(e.getMessage(), e);
			    }
				
				return result;
				    		
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
    }
    

    private static void deleteCronJobById(String sysId, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        if(StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
                
	                ResolveCron cronTask = HibernateUtil.getDAOFactory().getResolveCronDAO().findById(sysId);
	                if(cronTask != null)
	                {
	                    HibernateUtil.getDAOFactory().getResolveCronDAO().delete(cronTask);
	                }
                
            	});
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

    }
    
    private static void activateDeactivateCronJob(String sysId, boolean activateJob, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        if(StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
                	 ResolveCron cronTask = HibernateUtil.getDAOFactory().getResolveCronDAO().findById(sysId);
	                if(cronTask != null)
	                {
	                    cronTask.setUActive(activateJob);
	                    HibernateUtil.getDAOFactory().getResolveCronDAO().persist(cronTask);
	                }
                });
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }
    
    public static Map<String, Object> getSchedulerComponents(String sysId, String username) {
        Map<String, Object> result = new HashMap<>();
        List<ResolveCron> list = readScheduler(sysId, username);
        if (CollectionUtils.isNotEmpty(list)) {
            String runbook = list.get(0).getURunbook();
            if (StringUtils.isNotBlank(runbook) && !runbook.contains("#")) {
                result.put("runbook", Arrays.asList(String.format("%s:%s",runbook, WikiUtils.getWikidocSysId(runbook))));
            }
        }
        return result;
    }
    
    @SuppressWarnings("static-access")
    public static List<ResolveCron> readScheduler(String sysId, String username) {
        List<ResolveCron> list = new ArrayList<>();
        if (StringUtils.isNotBlank(sysId)) {
            try {
                HibernateUtil.action(hibernateUtil -> {
                    ResolveCron cronTask = new ResolveCron();
                    cronTask.setSys_id(sysId);
                    cronTask = hibernateUtil.getDAOFactory().getResolveCronDAO().findFirst(cronTask);
                    list.add(cronTask);
                }, username);
            }
            catch (Throwable e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        return list;
    }
    
    public static boolean doesSchedulerHasWiki(String sysId, String username) {
        return !getSchedulerComponents(sysId, username).isEmpty();
    }
    
    public static Integer getCronChecksum(String name, String username) {
        Integer checksum = null;
        
        ResolveCron cronTask = findCronJobModelById(null, name, username);
        if (cronTask != null) {
            checksum = cronTask.getChecksum();
        }
        
        return checksum;
    }
    
    public static Integer calculateChecksum(ResolveCron cron)
    {
        Integer checksum = 0;
        if (cron != null) {
            checksum = Objects.hash(cron.getUName(), cron.getUModule(), cron.getURunbook(), cron.getUParams(), cron.getUStartTime(), cron.getUEndTime(), cron.getUExpression());
        }
        return checksum;
    }
    
    public static void updateAllCronsForChecksum() {
        try {
            UpdateChecksumUtil.updateComponentChecksum("ResolveCron", CronUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
}
