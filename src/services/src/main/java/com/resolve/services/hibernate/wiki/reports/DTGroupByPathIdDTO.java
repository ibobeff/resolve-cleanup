/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.util.ArrayList;
import java.util.List;

public class DTGroupByPathIdDTO
{
    private String pathId;
    private List<DTRecord> records = new ArrayList<DTRecord>();
    
    public DTGroupByPathIdDTO() {}
    
    public String getPathId()
    {
        return pathId;
    }
    public void setPathId(String pathId)
    {
        this.pathId = pathId;
    }
    public List<DTRecord> getRecords()
    {
        return records;
    }
    public void setRecords(List<DTRecord> records)
    {
        this.records = records;
    }

}
