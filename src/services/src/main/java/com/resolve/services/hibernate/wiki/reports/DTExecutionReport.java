/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import com.resolve.services.jdbc.util.GenericJDBCHandler;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

/**
 * api for the DT Executed Reports
 * 
 * @author jeet.marwah
 *
 */
public class DTExecutionReport
{
    @SuppressWarnings("unused")
    private String username;

    private boolean top = true; // for the Top Executed report set this to true, for Bottom Executed reports set this to false
    private String namespaces = null; //comman separted values of namespaces
    private String authors = null; //comma seperated values of authors
    private int noOfRecs = -1; //top 5 recs or bottom 5 recs

    //range of how old the documents should be, default to first 90 days
    private long startDate = -1;
    private long endDate = -1;

    //pagination
    //    private int page = 1;
    //    private int start = 0;
    //    private int limit = 50;

    private Map<String, Object> data = new HashMap<String, Object>();

    public DTExecutionReport(QueryDTO query, String username)
    {
        this.username = username;

        evaluateQueryDTO(query);
    }

    public ResponseDTO<Map<String, Object>> getData() throws Exception
    {
        ResponseDTO<Map<String, Object>> response = new ResponseDTO<Map<String, Object>>();
        String sql = getSql();

//        List<Map<String, Object>> reportData = new ArrayList<Map<String, Object>>();
        List<DTExecutionCount> reportData = new ArrayList<DTExecutionCount>();
        Set<String> namespaces = new HashSet<String>();
        Set<String> authors = new HashSet<String>();

        try
        {
            List<Map<String, Object>> data = GenericJDBCHandler.executeSQLSelect(sql, -1, -1, false);
            if (data != null)
            {
                //we have to do this as date functions are different for Mysql and ORacle
                data = filterDataBasedOnDate(data);

                Map<String, Long> fullNameExecutionCountMap = new HashMap<String, Long>();
                Map<String, String> executedByMap = new HashMap<String, String>();
                for (Map<String, Object> record : data)
                {
                    String fullname = (String) record.get("fullname");
                    String namespace = (String) record.get("namespace");
                    String author = (String) record.get("author");

                    Number executioncount = null;
                    try
                    {
                        executioncount = (Double) record.get("executioncount"); //for Oracle
                    }
                    catch (Throwable t)
                    {
                        executioncount = (Long) record.get("executioncount"); //for MySql
                    }

                    //aggregate the count
                    if (fullNameExecutionCountMap.containsKey(fullname))
                    {
                        fullNameExecutionCountMap.put(fullname, fullNameExecutionCountMap.get(fullname).longValue() + executioncount.longValue());
                    }
                    else
                    {
                        fullNameExecutionCountMap.put(fullname, executioncount.longValue());
                    }
                    
                    //Add executed author for each dt name.
                    if (!executedByMap.containsKey(fullname))
                    {
                        executedByMap.put(fullname,author);
                    }
                    
                    //namespace
                    namespaces.add(namespace);

                    //authors
                    authors.add(author);
                }//end of for loop

                //prepare the output
                response.setTotal(fullNameExecutionCountMap.size());
//                int count = 0;
                
                //prepare a set for all the recs
                List<DTExecutionCount> allRecs = new ArrayList<DTExecutionCount>();
                Iterator<String> it = fullNameExecutionCountMap.keySet().iterator();
                while (it.hasNext())
                {
                    String fullname = it.next();
                    Long executioncount = fullNameExecutionCountMap.get(fullname);
                    String author = executedByMap.get(fullname);
                    allRecs.add(new DTExecutionCount(fullname, executioncount, author));
                    
//                    count++;
//                    if (count >= this.noOfRecs)
//                    {
//                        break;
//                    }
                }
                
                //sort it based on top or bottom
                if(top)
                    Collections.sort(allRecs, new DescComparator());
                else 
                    Collections.sort(allRecs, new AscComparator());
                
                //get the top # of recs
                if(noOfRecs == -1 || allRecs.size() < noOfRecs)
                    reportData = allRecs;
                else
                    reportData = allRecs.subList(0, noOfRecs);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in execting sql: " + sql, e);
            throw e;
        }

        //prepare the data for the UI
        data.put("data", reportData);
        data.put("Namespace", namespaces);
        data.put("Author", authors);
        response.setData(data);

        return response;
    }

    /**
        SELECT * FROM (
         SELECT w.u_fullname as fullname,  w.u_namespace as namespace, w.sys_created_by as author, COUNT(metric.u_dt_root_doc) as executioncount
         FROM wikidoc w
              LEFT OUTER JOIN tmetric_lookup metric ON LOWER(w.u_fullname) = LOWER(metric.u_dt_root_doc) 
         WHERE w.u_is_root = 'Y' 
         and  LOWER(w.u_namespace) IN ('dt', 'rsqa')
         and  LOWER(w.sys_created_by) IN ('admin')
         group by w.u_fullname, w.u_namespace, w.sys_created_by 
        )  t1 
        ORDER BY executioncount desc,
         fullName
         
      * @return
     */
    private String getSql()
    {
        StringBuilder sql = new StringBuilder("SELECT * FROM ");
        sql.append("(");//.append("\n");
        sql.append(" SELECT w.u_fullname as fullname,  w.u_namespace as namespace, w.sys_created_by as author, metric.sys_created_on, COUNT(metric.u_dt_root_doc) as executioncount");//.append("\n");
        sql.append(" FROM wikidoc w");//.append("\n");
        sql.append("      LEFT OUTER JOIN tmetric_lookup metric ON LOWER(w.u_fullname) = LOWER(metric.u_dt_root_doc) ");//.append("\n");
        sql.append(" WHERE w.u_is_root = 'Y' ");//.append("\n");

        //        sql.append(filterTimeRange());
        sql.append(filterNamespaces());
        sql.append(filterAuthors());

        sql.append(" group by w.u_fullname, w.u_namespace, w.sys_created_by, metric.sys_created_on ");//.append("\n");
        sql.append(")  t1 ");//.append("\n")"
        sql.append(" WHERE executioncount > 0 ");
        sql.append("ORDER BY executioncount").append(this.top ? " desc," : ",");//.append("\n");
        sql.append(" fullName");//.append("\n");

        Log.log.debug("DTExecutionReport Sql:" + sql.toString());

        return sql.toString();
    }

    private void evaluateQueryDTO(QueryDTO query)
    {
        /**
            filter:[{"field":"sysCreatedOn","type":"date","condition":"between","startValue":"2014-12-10T10:27:31-08:00","endValue":"2014-12-12T10:27:31-08:00"},{"field":"top","type":"boolean","condition":"equals","value":true}]
            page:1
            start:0
            limit:50
            group:[{"property":"unamespace","direction":"ASC"}]
            sort:[{"property":"unamespace","direction":"ASC"},{"property":"sysUpdatedOn","direction":"DESC"}]         
            
        */

        if (query != null)
        {
            List<QueryFilter> filters = query.getFilterItems();
            if (filters != null && filters.size() > 0)
            {
                for (QueryFilter filter : filters)
                {
                    setFilter(filter);
                }
            }

            //            this.page = query.getPage();
            //            this.start = query.getStart();
            //            this.limit = query.getLimit();

        }//end of if
    }

    private void setFilter(QueryFilter filter)
    {
        if (filter.isValid())
        {
            String field = filter.getField();
            if (field.equalsIgnoreCase("namespaces"))
            {
                this.namespaces = filter.getValue();
            }
            else if (field.equalsIgnoreCase("authors") || field.equalsIgnoreCase("sysCreatedBy"))
            {
                this.authors = filter.getValue();
            }
            else if (field.equalsIgnoreCase("top") && StringUtils.isNotEmpty(filter.getValue()) && filter.getValue().equalsIgnoreCase("false"))
            {
                this.top = false;
            }
            else if (field.equalsIgnoreCase("sysCreatedOn"))
            {
                String startValue = filter.getStartValue();
                DateTime localDate = DateUtils.parseISODate(startValue);
                this.startDate = DateUtils.convertLocalToGMT(localDate.toDate()).getTime();

                String endValue = filter.getEndValue();
                localDate = DateUtils.parseISODate(endValue);
                this.endDate = DateUtils.convertLocalToGMT(localDate.toDate()).getTime();

            }
            else if (field.equalsIgnoreCase("noOfRecs"))
            {
                this.noOfRecs = StringUtils.isNotEmpty(filter.getValue()) ? new Integer(filter.getValue()) : -1;
            }
        }
    }

    private List<Map<String, Object>> filterDataBasedOnDate(List<Map<String, Object>> data)
    {
        List<Map<String, Object>> newData = new ArrayList<Map<String, Object>>();

        for (Map<String, Object> record : data)
        {
            Date createdDate = null;
            
            try
            {
                createdDate = (Date) record.get("sys_created_on"); //for Oracle
            }
            catch(Throwable t)
            {
                createdDate = new Date(((Timestamp) record.get("sys_created_on")).getTime()); //for MySql
            }
            if (createdDate != null)
            {
                //*** NOTE: only the date value is stored in the table somehow...and if we just use that, the valid records gets filtered too
                //so getting the day from the DB and manipulating it for that day
                //just use for the comparision only
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                String d = format.format(createdDate) + "-00:00"; //GMT format since data is recorded in GMT
                DateTime date1 = DateTime.parse(d, ISODateTimeFormat.dateTimeParser());
                long date = date1.getMillis();

                if (date >= this.startDate && date <= this.endDate)
                {
                    newData.add(record);
                }
            }
            else
            {
                newData.add(record);
            }
        }

        return newData;
    }

    //    private String filterTimeRange()
    //    {
    //        StringBuilder whereClause = new StringBuilder("");
    //
    //        if (this.startDate > -1 && this.endDate > -1)
    //        {
    //            whereClause.append(" and metric.ts >= ").append(this.startDate).append(" and metric.ts <= ").append(this.endDate).append(" \n");
    //        }
    //
    //        return whereClause.toString();
    //    }

    private String filterNamespaces()
    {
        StringBuilder whereClause = new StringBuilder("");

        if (StringUtils.isNotBlank(namespaces))
        {
            //to make it case-insensitive
            namespaces = namespaces.toLowerCase().trim();

            Set<String> ns = new HashSet<String>(StringUtils.convertStringToList(namespaces, ","));
            whereClause.append(" and  LOWER(w.u_namespace) IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(ns)) + ")");//.append("\n");
        }

        return whereClause.toString();
    }

    private String filterAuthors()
    {
        StringBuilder whereClause = new StringBuilder("");

        if (StringUtils.isNotBlank(authors))
        {
            //to make it case-insensitive
            authors = authors.toLowerCase().trim();

            Set<String> ns = new HashSet<String>(StringUtils.convertStringToList(authors, ","));
            whereClause.append(" and  LOWER(w.sys_created_by) IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(ns)) + ")");//.append("\n");
        }

        return whereClause.toString();
    }

}

class AscComparator implements Comparator<DTExecutionCount> {

    public int compare(DTExecutionCount o1, DTExecutionCount o2) {
        return (o1.getExecutioncount() - o2.getExecutioncount() >= 0 ? 1 : -1);
    }

}

class DescComparator implements Comparator<DTExecutionCount> {

    public int compare(DTExecutionCount o1, DTExecutionCount o2) {
        return (o2.getExecutioncount() - o1.getExecutioncount() >= 0 ? 1 : -1);
    }

}
