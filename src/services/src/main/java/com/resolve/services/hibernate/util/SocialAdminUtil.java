/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.query.Query;
import org.hibernate.type.TimestampType;

import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.SocialPostResponse;
import com.resolve.search.model.SocialTarget;
import com.resolve.search.social.SocialIndexAPI;
import com.resolve.services.GatewayUtil;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.ServiceWiki;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.migration.social.NonNeo4jRelationType;
import com.resolve.services.social.follow.TeamFollow;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.CryptUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;


public class SocialAdminUtil
{
    private static List<UsersVO> users;
    private static String usersWhereClause;
    private static final Map<String, String> followerEdgeProps = null;
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Social Process CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * A generic method to get the list of social component, either Processes, Teams, Forums or RSSs.
     * @param query
     *      - populate modelName property to get the appropriate component list.
     *      - For Process: social_process, for Teams: social_team and so on.
     * @param userName
     *      - User name of a user who is invoking this API.
     * @return
     *      - A map containing list of component.
     */
    public static Map<String, Object> getSocialComponents(QueryDTO query, String userName)
    {
        //        query.getSelectHQL()
        //        query.getFilterWhereClause()
        Map<String, Object> result = new HashMap<String, Object>();

        Set<String> userRoles = UserUtils.getUserRoles(userName);

        int limit = query.getLimit();
        int offset = query.getStart();

        StringBuilder whereClause = new StringBuilder();
        int count = 0;
        if (userRoles.size() > 0)
        {
            if ("rss_subscription".equalsIgnoreCase(query.getModelName()))
            {
                whereClause.append("( u_rss_owner = '" + userName + "' OR ");
            }
            else
            {
                whereClause.append("( u_owner = '" + userName + "' OR ");
            }
        }
        for (String role : userRoles)
        {
            if (count < userRoles.size() - 1)
            {
                whereClause.append(" u_read_roles LIKE '%").append(role.trim()).append("%' OR u_edit_roles LIKE '%").append(role.trim()).append("%' OR u_post_roles LIKE '%").append(role.trim()).append("%' OR");
            }
            else
            {
                whereClause.append(" u_read_roles LIKE '%").append(role.trim()).append("%' OR u_edit_roles LIKE '%").append(role.trim()).append("%' OR u_post_roles LIKE '%").append(role.trim()).append("%'");
            }
            count++;
        }//end of for

        if (userRoles.size() > 0)
        {
            whereClause.append(" ) ");
        }

        if (StringUtils.isNotBlank(query.getWhereClause()))
        {
            query.setWhereClause(query.getWhereClause().replaceAll("where", ""));
            whereClause.append(" AND ").append(query.getWhereClause());
        }

        query.setWhereClause(whereClause.toString());

        try
        {
            List<Map<String, Object>> data = GeneralHibernateUtil.executeHQLSelect(query, offset, limit, true);
            if (data != null)
            {
                List<SocialDTO> resultList = new ArrayList<SocialDTO>();
                for (Map<String, Object> row : data)
                {
                    if (query.getModelName().contains("process"))
                    {
                        resultList.add(new SocialProcessDTO(row));
                    }
                    else if (query.getModelName().contains("forum"))
                    {
                        resultList.add(new SocialForumDTO(row));
                    }
                    else if (query.getModelName().contains("team"))
                    {
                        resultList.add(new SocialTeamDTO(row));
                    }
                    else if (query.getModelName().contains("rss"))
                    {
                        resultList.add(new SocialRssDTO(row));
                    }
                }

                for (SocialDTO dto : resultList)
                {
                    dto.setU_editable(false);
                    
                    if(StringUtils.isNotBlank(dto.getU_edit_roles()))
                    {
                        String[] editRoles = dto.getU_edit_roles().split(",");
                        List<String> compEditRoles = Arrays.asList(editRoles);
                        for (String userRole : userRoles)
                        {
                            if (compEditRoles.contains(userRole))
                            {
                                dto.setU_editable(true);
                                break;
                            }
                        }
                    }
                    
                    boolean isOwner = StringUtils.isNotBlank(dto.getU_owner()) ? dto.getU_owner().equalsIgnoreCase(userName) : false;
                    boolean isRSSOwner = (dto instanceof SocialRssDTO && StringUtils.isNotBlank(((SocialRssDTO) dto).getU_rss_owner())) ? ((SocialRssDTO) dto).getU_rss_owner().equalsIgnoreCase(userName) : false;
                    String owner = dto.getU_owner();
                    if (query.getModelName().contains("rss"))
                    {
                        owner =((SocialRssDTO) dto) .getU_rss_owner();
                    }
                    dto.setU_user_display_name(getUserDisplayName(owner));
                    if (dto.getU_editable() || isOwner || isRSSOwner)
                    {
                        dto.setU_editable(true);
                    }
                }

                result.put("DATA", resultList);
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    static boolean checkUserAccessRights(SocialDTO dto, String userName)
    {
        boolean result = false;

        Set<String> userRoles = UserUtils.getUserRoles(userName);
        Set<String> compRoles = new HashSet<String>();
        if (dto.getU_edit_roles() != null)
        {
            compRoles.addAll(Arrays.asList(dto.getU_edit_roles().split(",")));
        }
        if (dto.getU_read_roles() != null)
        {
            compRoles.addAll(Arrays.asList(dto.getU_read_roles().split(",")));
        }

        for (String userRole : userRoles)
        {
            if (compRoles.contains(userRole))
            {
                result = true;
                break;
            }
        }

        return result;
    }

    private static boolean checkIsEditable(SocialDTO dto, String userName) 
    {
        Set<String> userRoles = UserUtils.getUserRoles(userName);

        boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(userName, userRoles,  dto.getU_edit_roles());
        if(!userCanEdit)
        {
            if (dto instanceof SocialRssDTO)
            {
                if(userName.equalsIgnoreCase(((SocialRssDTO) dto).getU_rss_owner()))
                {
                    userCanEdit = true;
                }
            }
            else
            {
                if(userName.equalsIgnoreCase(dto.getU_owner()))
                {
                    userCanEdit = true;
                }
            }
        }

        //set the value to this obj
        dto.setU_editable(userCanEdit);
        
        return userCanEdit;
    }

    public static SocialProcessDTO getProcess(String sysId, String userName)
    {
        SocialProcessDTO result = (SocialProcessDTO) getSocialDTO(AdvanceTree.PROCESS, sysId, null, userName);
        if (result == null)
        {
            throw new RuntimeException("No Process with sys_id '" + sysId + "' exist.");
        }

        checkIsEditable(result, userName);

        return result;
    }

    public static SocialProcessDTO getProcessByName(String processName, String userName)
    {
        SocialProcessDTO result = (SocialProcessDTO) getSocialDTO(AdvanceTree.PROCESS, null, processName, userName);
        if (result == null)
        {
            throw new RuntimeException("No Process with name '" + processName + "' exist.");
        }

        checkIsEditable(result, userName);

        return result;
    }

    public static SocialDTO getdefaultAccessRights(String comp)
    {
        SocialDTO dto = new SocialDTO();
        if (comp.equalsIgnoreCase("process"))
        {
            dto.setU_edit_roles(PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_RIGHTS_EDIT));
            dto.setU_read_roles(PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_READ_ROLES));
            dto.setU_post_roles(PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_READ_POST));
        }
        else if (comp.equalsIgnoreCase("teams"))
        {
            dto.setU_edit_roles(PropertiesUtil.getPropertyString(HibernateConstants.TEAMS_RIGHTS_EDIT));
            dto.setU_read_roles(PropertiesUtil.getPropertyString(HibernateConstants.TEAMS_RIGHTS_ACCESS));
            dto.setU_post_roles(PropertiesUtil.getPropertyString(HibernateConstants.TEAMS_RIGHTS_POST));
        }
        else if (comp.equalsIgnoreCase("forums"))
        {
            dto.setU_edit_roles(PropertiesUtil.getPropertyString(HibernateConstants.FORUMS_RIGHTS_EDIT));
            dto.setU_read_roles(PropertiesUtil.getPropertyString(HibernateConstants.FORUMS_RIGHTS_READ));
            dto.setU_post_roles(PropertiesUtil.getPropertyString(HibernateConstants.FORUMS_RIGHTS_POST));
        }
        else if (comp.equalsIgnoreCase("rss"))
        {
            dto.setU_edit_roles(PropertiesUtil.getPropertyString(HibernateConstants.RSS_RIGHTS_EDIT));
            dto.setU_read_roles(PropertiesUtil.getPropertyString(HibernateConstants.RSS_RIGHTS_READ));
            dto.setU_post_roles(PropertiesUtil.getPropertyString(HibernateConstants.RSS_RIGHTS_POST));
        }

        return dto;
    }

    public static Set<SocialDTO> getProcessComponents(String processCompSysId, String userName)
    {
        Set<SocialDTO> resultList = new TreeSet<SocialDTO>();
        Collection<ResolveNodeVO> nodes = null;
        try
        {
            nodes = ServiceGraph.getMemberNodes(null, processCompSysId, null, NodeType.PROCESS, userName);
        }
        catch (Exception e)
        {
            Log.log.error("ERROR getting members of process component SysId: " + processCompSysId, e);
        }
        
        if (nodes != null && nodes.size() > 0)
        {
            for (ResolveNodeVO node : nodes)
            {
                if (node.getUType().name().equalsIgnoreCase("team"))
                {
                    SocialTeamDTO teamDto = getTeam(node.getUCompSysId(), userName);
                    teamDto.setCompType("Team");
                    resultList.add(teamDto);
                    continue;
                }
                if (node.getUType().name().equalsIgnoreCase("forum"))
                {
                    SocialForumDTO forumDto = getForum(node.getUCompSysId(), userName);
                    forumDto.setCompType("Forum");
                    resultList.add(forumDto);
                    continue;
                }
                
                if (node.getUType().name().equalsIgnoreCase("rss"))
                {
                    SocialRssDTO rssDto = getRss(node.getUCompSysId(), userName);
                    rssDto.setCompType("RSS");
                    resultList.add(rssDto);
                    continue;
                }
                
                if (node.getUType().name().equalsIgnoreCase("actiontask"))
                {
                    ResolveActionTaskVO actionTaskVO = ServiceHibernate.getResolveActionTask(node.getUCompSysId(), null, userName);
                    if (actionTaskVO != null)
                    {
                        SocialDTO socialDto = new SocialDTO();
                        socialDto.setSys_id(actionTaskVO.getSys_id());
                        socialDto.setCompType("Action Tasks");
                        socialDto.setU_display_name(actionTaskVO.getUFullName());
                        socialDto.setU_description(actionTaskVO.getUDescription());
                        resultList.add(socialDto);
                    }
                    continue;
                }
                
                if (node.getUType().name().equalsIgnoreCase("document"))
                {
                    WikiDocumentVO docVO = WikiUtils.getWikiDoc(node.getUCompSysId(), null, "system");
                    if (docVO != null)
                    {
                        SocialDTO socialDto = new SocialDTO();
                        socialDto.setCompType("Document");
                        socialDto.setSys_id(docVO.getSys_id());
                        socialDto.setU_display_name(docVO.getUFullname());
                        socialDto.setU_description(docVO.getUSummary());
                        resultList.add(socialDto);
                        if (docVO.getUHasActiveModel())
                        {
                            socialDto.setCompType("Runbook");
                        }
                        else
                        {
                            socialDto.setCompType("Document");
                        }
                    }
                    continue;
                }
                if (node.getUType().name().equalsIgnoreCase("namespace"))
                {
                    SocialDTO socialDto = new SocialDTO();
                    socialDto.setSys_id(node.getUCompSysId());
                    socialDto.setCompType("Namespace");
                    socialDto.setU_display_name(node.getUCompName());
                    resultList.add(socialDto);
                    continue;
                }
                
                if (node.getUType().name().equalsIgnoreCase("user"))
                {
                    UsersVO userVO = UserUtils.getUserById(node.getUCompSysId());
                    if (userVO != null)
                    {
                        SocialDTO socialDto = new SocialDTO();
                        socialDto.setSys_id(userVO.getSys_id());
                        socialDto.setCompType("User");
                        socialDto.setU_display_name(userVO.getUUserName());
                        String firstName = StringUtils.isBlank(userVO.getUFirstName()) ? "" : userVO.getUFirstName();
                        String lastName = StringUtils.isBlank(userVO.getULastName()) ? "" : userVO.getULastName();
                        socialDto.setU_description(firstName + " " + lastName);
                        resultList.add(socialDto);
                    }
                }
            }
        }
        
        return resultList;
    }

    public static void deleteProcesses(String[] processSysIds, String username) throws Exception
    {
        if (processSysIds != null && StringUtils.isNotBlank(username))
        {
            UsersVO user = ServiceHibernate.getUser(username);
            for (String sysId : processSysIds)
            {
                // Send notification
                NotificationHelper.getProcessNotification(sysId, UserGlobalNotificationContainerType.PROCESS_PURGED, username, true, false);
                
                deleteSocialCompNode(sysId, NodeType.PROCESS, user);
            }
        }
    }

    public static void deleteTeam(String[] teamSysIds, String username) throws Exception
    {
        if (teamSysIds != null && StringUtils.isNotBlank(username))
        {
            UsersVO user = ServiceHibernate.getUser(username);
            for (String sysId : teamSysIds)
            {
                // Send notification
                NotificationHelper.getTeamNotification(sysId, UserGlobalNotificationContainerType.TEAM_PURGED, username, true, false);
                
                deleteSocialCompNode(sysId, NodeType.TEAM, user);
            }
        }
    }

    public static void deleteForum(String[] forumSysIds, String username) throws Exception
    {
        if (forumSysIds != null && StringUtils.isNotBlank(username))
        {
            UsersVO user = ServiceHibernate.getUser(username);
            for (String sysId : forumSysIds)
            {
                // Send notification
                NotificationHelper.getForumNotification(sysId, UserGlobalNotificationContainerType.FORUM_PURGED, username, true, false);
                
                deleteSocialCompNode(sysId, NodeType.FORUM, user);
            }
        }
    }

    public static SocialProcessDTO saveProcess(SocialProcessDTO process, String userName, Boolean validate) throws Exception
    {
        if (process != null)
        {
            validateName(process.getU_display_name());
            UsersVO user = ServiceHibernate.getUser(userName);
            String orgSysId = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

            boolean isInsert = StringUtils.isBlank(process.getSys_id()) ? true : false;
            Date now = GMTDate.getDate();
            if (isInsert)
            {
                process.setSys_created_by(userName);
                process.setSys_created_on(now);
                process.setSys_updated_by(userName);
                process.setSys_updated_on(now);
            }
            else
            {
                process.setSys_created_on(GMTDate.getDate(process.getSys_created_on()));
                process.setSys_updated_by(userName);
                process.setSys_updated_on(now);
                
                if (StringUtils.isBlank(process.getU_post_roles()))
                {
                    /*
                     * For social components, post roles should never be bkank.
                     * If they are, populate the default post one.
                     */
                    SocialDTO dto = ServiceHibernate.getDefaultAccessRights("process");
                    process.setU_post_roles(dto.getU_post_roles());
                }
            }
            String name = process.getU_display_name();
            if (StringUtils.isBlank(name))
            {
                throw new RuntimeException("Process name cannot be empty and is mandatory.");
            }
            
            Map<String, Object> whereClauseParams = new HashMap<String, Object>();
            
            //String whereClause = "LOWER(u_display_name) = '" + name.toLowerCase() + "'";
            String whereClause = "LOWER(u_display_name) = :u_display_name";
            whereClauseParams.put("u_display_name", name.toLowerCase());
            
            if (StringUtils.isNotBlank(orgSysId))
            {
                //whereClause += " and ( sys_org is null OR sys_org = '" + orgSysId + "')";
                whereClause += " and ( sys_org is null OR sys_org = :sys_org)";
                whereClauseParams.put("sys_org", orgSysId);
            }

            List<Map<String, Object>> qryResult = ServiceHibernate.queryCustomTable(SocialProcessDTO.TABLE_NAME, whereClause, whereClauseParams);
            Map<String, Object> dbRec = null;

            //if its a create - than check for uniqueness
            if (qryResult != null && qryResult.size() > 0)
            {
                dbRec = qryResult.get(0);
                if (isInsert)
                {
                    throw new RuntimeException("Process name already exist. This has to be unique.");
                }
                else
                {
                    //this is update..make sure that the sys_id of both are same , than its the same rec else name already exist
                    String dbSysId = (String) dbRec.get("sys_id");
                    if (!process.getSys_id().equalsIgnoreCase(dbSysId))
                    {
                        throw new RuntimeException("Process name already exist. This has to be unique.");
                    }
                }
            }
            else
            {
                if (StringUtils.isNotBlank(process.getSys_id()))
                {
                    whereClause = "sys_id = '" + process.getSys_id() + "'";
                    qryResult = ServiceHibernate.queryCustomTable(SocialProcessDTO.TABLE_NAME, whereClause, null);
                    dbRec = qryResult.get(0);
                }
            }

            //pwds
            String imPwd = process.getU_im_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                process.setU_im_pwd(CryptUtils.encrypt(imPwd));
            }

            //receive pwd
            String receivePwd = process.getU_receive_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                process.setU_receive_pwd(CryptUtils.encrypt(receivePwd));
            }

            //roles manipulation
            String readRoles = evaluateRoles(process.getU_read_roles(), PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_READ_ROLES));
            process.setU_read_roles(readRoles);

            String editRoles = evaluateRoles(process.getU_edit_roles(), PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_RIGHTS_EDIT));
            process.setU_edit_roles(editRoles);

            //populate the sys_org and prepare a map 
            process.setSys_org(orgSysId);
            Map<String, Object> record = process.convertDTOToMap();
            
            SocialProcessDTO oldProcess = new SocialProcessDTO();
            if (StringUtils.isNotBlank(process.getSys_id()))
            {
                try
                {
                    oldProcess = getProcess(process.getSys_id(), userName);
                }
                catch(RuntimeException re)
                {
                    // Needn't do anything here.
                }
            }
            checkComponentForChange(oldProcess, process);
            
            //persist it
            record.put("u_display_name", ((String)record.get("u_display_name")).trim());
            
            ResolveNodeVO processNodeVO = null;
            try
            {   
                class TupleFinal {
                	Map<String, Object> record;
                	SocialProcessDTO process;
            		ResolveNodeVO processNodeVO;
            		
            	}            	
            	
                Map<String, Object> recordFinal = record;
              HibernateProxy.setCurrentUser(userName);
            	TupleFinal tupleResult = (TupleFinal) HibernateProxy.execute(() -> {                	
                	TupleFinal tuple = new TupleFinal();
                	tuple.record = GeneralHibernateUtil.insertOrUpdateCustomTable(userName, SocialProcessDTO.TABLE_NAME, recordFinal);
                	tuple.process = new SocialProcessDTO(tuple.record);
                	tuple.processNodeVO = createOrUpdateSocialCompNode(tuple.process, NodeType.PROCESS, userName);
                	return tuple;
                });
            	record = tupleResult.record;
            	process = tupleResult.process;
            	processNodeVO = tupleResult.processNodeVO;
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
               
                throw new Exception("Error while Inserting / Updating " + process.getU_display_name());
            }
            
            if (isInsert)
            {
                // Add current user to the process
                addUserToSocialComp(processNodeVO, NodeType.PROCESS, userName);
                NotificationHelper.getProcessNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.PROCESS_CREATE, userName, true, true);
            }
            else
            {
                NotificationHelper.getProcessNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.PROCESS_UPDATE, userName, true, true);
            }
            
            
            checkIsEditable(process, userName);

            process.setSys_created_on(GMTDate.getLocalServerDate(process.getSys_created_on()));
            process.setSys_updated_on(GMTDate.getLocalServerDate(process.getSys_updated_on()));

            //Send notification
//            NotificationHelper.getProcessNotification(socialProcess.getId(), UserGlobalNotificationContainerType.PROCESS_UPDATE, userName, true, true);

        }
        UsersVO user = ServiceHibernate.getUser(process.getU_owner());
        if (user != null)
        {
            process.setU_user_display_name(ServiceHibernate.getUser(process.getU_owner()).getUDisplayName());
        }
        
        return process;
    }
    
    private static ResolveNodeVO createOrUpdateSocialCompNode(SocialDTO socialDTO, NodeType nodeType, String userName) throws Exception
    {        
        ResolveNodeVO atNode = null;
        try {
        	atNode = ServiceGraph.findNode(null, socialDTO.getSys_id(), null, nodeType, userName);
        } catch (Exception e) {
            Log.log.warn("Node not found for " + socialDTO.getU_display_name());
		}

        if(atNode == null)
        {
            atNode = new ResolveNodeVO();
            atNode.setUCompSysId(socialDTO.getSys_id());
            atNode.setUType(nodeType);
            atNode.setUMarkDeleted(false);
            atNode.setUPinned(false);
        }
        
        atNode.setULock(socialDTO.getU_is_locked());
        atNode.setUCompName(socialDTO.getU_display_name());
        atNode.setUPostRoles(socialDTO.getU_post_roles());
        atNode.setUEditRoles(socialDTO.getU_edit_roles());
        
        try
        {
            //persist the node
            ServiceGraph.persistNode(atNode, userName);
        }
        catch (Exception e)
        {
            Log.log.error("error while creating actiontask in graph " + socialDTO.getU_display_name(), e);
            throw e;
        }
        
        return atNode;
    }
    
    private static void addUserToSocialComp(ResolveNodeVO docNode, NodeType nodeType, String userName) throws Exception
    {
        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
        followComponents.add(docNode);
        
        try
        {
            //user following this Actiontask
            User socialUser = SocialCompConversionUtil.getSocialUser(userName);
            List<String> ids = new ArrayList<String>();
            ids.add(docNode.getUCompSysId());
            SocialUtil.setFollowStreams(socialUser, ids, true);
            
            ResolveNodeVO srcCompNode = ServiceGraph.findNode(null, socialUser.getSys_id(), null, null, userName);
            List<ResolveNodeVO> destComponentNodes = new ArrayList<ResolveNodeVO>();
            destComponentNodes.add(docNode);
            ServiceGraph.addRelation(srcCompNode.getSys_id(), srcCompNode.getUCompSysId(), srcCompNode.getUCompName(), srcCompNode.getUType(),
                            destComponentNodes, followerEdgeProps, userName, NonNeo4jRelationType.MEMBER.name());
            //ServiceGraph.follow(null, null, userName, NodeType.USER, followComponents, null, userName);
        }
        catch (Exception e)
        {
            String errorMessage = "Error following " + nodeType.name() + ": " +docNode.getUCompName() + " by User:" + userName;
            Log.log.error(errorMessage, e);
            throw new Exception(errorMessage);
        }
    }

//    public static Process createSocialProcessAndAddUser(SocialProcessDTO process, UsersVO user) throws Exception
//    {
//        Process socialProcess = SocialCompConversionUtil.convertSocialProcessDTOToProcess(process, null);
//        User socialUser = SocialCompConversionUtil.convertUserToSocialUser(user, null);
//
//        //Create new Process. 
//        ServiceSocial.insertOrUpdateRSComponent(socialProcess);
//
//        //if user does not exist in the graph, than only create it.
//        RSComponent userComp = ServiceSocial.getComponentById(socialUser.getId(), "system");
//        if(userComp == null)
//        {
//            ServiceSocial.insertOrUpdateRSComponent(socialUser);
//        }
//
//        //Add current user to this process
//        if (checkUserAccessRights(process, user.getUUserName()))
//        {
//            ServiceSocial.addUserToProcess(socialUser, socialProcess);
//        }
//
//        return socialProcess;
//    }

    public static SocialTeamDTO saveTeam(SocialTeamDTO team, String userName, Boolean validate) throws Exception
    {
        if (team != null)
        {
            validateName(team.getU_display_name());
            UsersVO user = ServiceHibernate.getUser(userName);
            String orgSysId = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

            boolean isInsert = StringUtils.isBlank(team.getSys_id()) ? true : false;
            Date now = GMTDate.getDate();
            if (isInsert)
            {
                team.setSys_created_by(userName);
                team.setSys_created_on(now);
                team.setSys_updated_by(userName);
                team.setSys_updated_on(now);
            }
            else
            {
                team.setSys_created_on(GMTDate.getDate(team.getSys_created_on()));
                team.setSys_updated_by(userName);
                team.setSys_updated_on(now);
                
                if (StringUtils.isBlank(team.getU_post_roles()))
                {
                    /*
                     * For social components, post roles should never be bkank.
                     * If they are, populate the default post one.
                     */
                    SocialDTO dto = ServiceHibernate.getDefaultAccessRights("teams");
                    team.setU_post_roles(dto.getU_post_roles());
                }
            }
            String name = team.getU_display_name();
            if (StringUtils.isBlank(name))
            {
                throw new RuntimeException("Team name cannot be empty and is mandatory.");
            }
            
            Map<String, Object> whereClauseParams = new HashMap<String, Object>();
            
            //String whereClause = "LOWER(u_display_name) = '" + name.toLowerCase() + "'";
            String whereClause = "LOWER(u_display_name) = :u_display_name";
            whereClauseParams.put("u_display_name", name.toLowerCase());
            
            if (StringUtils.isNotBlank(orgSysId))
            {
                //whereClause += " and ( sys_org is null OR sys_org = '" + orgSysId + "')";
                whereClause += " and ( sys_org is null OR sys_org = :sys_org)";
                whereClauseParams.put("sys_org", orgSysId);
            }

            List<Map<String, Object>> qryResult = ServiceHibernate.queryCustomTable(SocialTeamDTO.TABLE_NAME, whereClause, whereClauseParams);
            Map<String, Object> dbRec = null;

            //if its a create - than check for uniqueness
            if (qryResult != null && qryResult.size() > 0)
            {
                dbRec = qryResult.get(0);
                if (isInsert)
                {
                    throw new RuntimeException("Team name already exist. This has to be unique.");
                }
                else
                {
                    //this is update..make sure that the sys_id of both are same , than its the same rec else name already exist
                    String dbSysId = (String) dbRec.get("sys_id");
                    if (!team.getSys_id().equalsIgnoreCase(dbSysId))
                    {
                        throw new RuntimeException("Team name already exist. This has to be unique.");
                    }
                }
            }
            else
            {
                if (StringUtils.isNotBlank(team.getSys_id()))
                {
                    whereClause = "sys_id = '" + team.getSys_id() + "'";
                    qryResult = ServiceHibernate.queryCustomTable(SocialTeamDTO.TABLE_NAME, whereClause, null);
                    dbRec = qryResult.get(0);
                }
            }

            //pwds
            String imPwd = team.getU_im_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                team.setU_im_pwd(CryptUtils.encrypt(imPwd));
            }

            //receive pwd
            String receivePwd = team.getU_receive_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                team.setU_receive_pwd(CryptUtils.encrypt(receivePwd));
            }

            //roles manipulation
            String readRoles = evaluateRoles(team.getU_read_roles(), PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_READ_ROLES));
            team.setU_read_roles(readRoles);

            String editRoles = evaluateRoles(team.getU_edit_roles(), PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_RIGHTS_EDIT));
            team.setU_edit_roles(editRoles);

            //populate the sys_org and prepare a map 
            team.setSys_org(orgSysId);
            Map<String, Object> record = team.convertDTOToMap();

            SocialTeamDTO oldTeam = new SocialTeamDTO();
            if (StringUtils.isNotBlank(team.getSys_id()))
            {
                try
                {
                    oldTeam = getTeam(team.getSys_id(), userName);
                }
                catch(RuntimeException re)
                {
                    // Needn't do anything here.
                }
            }
            checkComponentForChange(oldTeam, team);
            
            //persist it
            record.put("u_display_name", ((String)record.get("u_display_name")).trim());
            
            ResolveNodeVO teamNodeVO = null;
            try
            {
            	class TupleFinal {
            		SocialTeamDTO team;
            		ResolveNodeVO teamNodeVO;
            		
            	}            	
            	SocialTeamDTO teamFinal = team;
              HibernateProxy.setCurrentUser(userName);
            	TupleFinal tupleResult = (TupleFinal) HibernateProxy.execute(() -> {                	
                	TupleFinal tuple = new TupleFinal();
                	tuple.team = new SocialTeamDTO(GeneralHibernateUtil.insertOrUpdateCustomTable(userName, SocialTeamDTO.TABLE_NAME, record));
                	tuple.teamNodeVO = createOrUpdateSocialCompNode(teamFinal, NodeType.TEAM, userName);
                	return tuple;
                });
            	team = tupleResult.team;
            	teamNodeVO = tupleResult.teamNodeVO;
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);                
                throw new Exception("Error while Inserting / Updating " + team.getU_display_name());
            }
            
            if (isInsert)
            {
                // Add current user to the process
                addUserToSocialComp(teamNodeVO, NodeType.TEAM, userName);
                NotificationHelper.getTeamNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.TEAM_CREATE, userName, true, true);
            }
            else
            {
                NotificationHelper.getTeamNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.TEAM_UPDATE, userName, true, true);
            }
            
            
            team = new SocialTeamDTO(record);
            checkIsEditable(team, userName);

            team.setSys_created_on(GMTDate.getLocalServerDate(team.getSys_created_on()));
            team.setSys_updated_on(GMTDate.getLocalServerDate(team.getSys_updated_on()));

            //Send notification
//            NotificationHelper.getTeamNotification(socialTeam.getId(), UserGlobalNotificationContainerType.TEAM_UPDATE, userName, true, true);

        }

        GroupsVO groupVO = UserUtils.getGroup(null, team.getU_display_name(), userName);
        if (groupVO != null)
        {
            if (groupVO.getUIsLinkToTeam())
            {
                ServiceHibernate.addRolesToGroup(groupVO.getSys_id(), groupVO.getUName(), Arrays.asList(team.getU_read_roles().split(",")), true, userName);
            }
        }
        
        UsersVO user = ServiceHibernate.getUser(team.getU_owner());
        if (user != null)
        {
            team.setU_user_display_name(ServiceHibernate.getUser(team.getU_owner()).getUDisplayName());
        }
        
        return team;
    }

//    public static Team createSocialTeamAndAddUser(SocialTeamDTO team, UsersVO user) throws Exception
//    {
//        Team socialTeam = SocialCompConversionUtil.convertSocialTeamDTOToTeam(team, null);
//        User socialUser = SocialCompConversionUtil.convertUserToSocialUser(user, null);
//
//        //Create new Process. 
//        ServiceSocial.insertOrUpdateRSComponent(socialTeam);
//
//        //Add current user to this process
//        if (checkUserAccessRights(team, user.getUUserName()))
//        {
//            ServiceSocial.addUserToTeam(socialUser, socialTeam);
//        }
//
//        //        List<String> ids = new ArrayList<String>();
//        //        ids.add(socialTeam.getId());
//        //        ServiceSocial.setFollowStreams(socialUser, ids, true);
//
//        return socialTeam;
//    }

    public static SocialForumDTO saveForum(SocialForumDTO forum, String userName, Boolean validate) throws Exception
    {
        if (forum != null)
        {
            validateName(forum.getU_display_name());
            UsersVO user = ServiceHibernate.getUser(userName);
            String orgSysId = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

            boolean isInsert = StringUtils.isBlank(forum.getSys_id()) ? true : false;
            Date now = GMTDate.getDate();
            if (isInsert)
            {
                forum.setSys_created_by(userName);
                forum.setSys_created_on(now);
                forum.setSys_updated_by(userName);
                forum.setSys_updated_on(now);
            }
            else
            {
                forum.setSys_created_on(GMTDate.getDate(forum.getSys_created_on()));
                forum.setSys_updated_by(userName);
                forum.setSys_updated_on(now);
                
                if (StringUtils.isBlank(forum.getU_post_roles()))
                {
                    /*
                     * For social components, post roles should never be bkank.
                     * If they are, populate the default post one.
                     */
                    SocialDTO dto = ServiceHibernate.getDefaultAccessRights("forums");
                    forum.setU_post_roles(dto.getU_post_roles());
                }
            }
            String name = forum.getU_display_name();
            if (StringUtils.isBlank(name))
            {
                throw new RuntimeException("Forum name cannot be empty and is mandatory.");
            }
            
            Map<String, Object> whereClauseParams = new HashMap<String, Object>();
            
            //String whereClause = "LOWER(u_display_name) = '" + name.toLowerCase() + "'";
            String whereClause = "LOWER(u_display_name) = :u_display_name";
            whereClauseParams.put("u_display_name", name.toLowerCase());
            
            if (StringUtils.isNotBlank(orgSysId))
            {
                //whereClause += " and ( sys_org is null OR sys_org = '" + orgSysId + "')";
                whereClause += " and ( sys_org is null OR sys_org = :sys_org)";
                whereClauseParams.put("sys_org", orgSysId);
            }

            List<Map<String, Object>> qryResult = ServiceHibernate.queryCustomTable(SocialForumDTO.TABLE_NAME, whereClause, whereClauseParams);
            Map<String, Object> dbRec = null;

            //if its a create - than check for uniqueness
            if (qryResult != null && qryResult.size() > 0)
            {
                dbRec = qryResult.get(0);
                if (isInsert)
                {
                    throw new RuntimeException("Forum name already exist. This has to be unique.");
                }
                else
                {
                    //this is update..make sure that the sys_id of both are same , than its the same rec else name already exist
                    String dbSysId = (String) dbRec.get("sys_id");
                    if (!forum.getSys_id().equalsIgnoreCase(dbSysId))
                    {
                        throw new RuntimeException("Forum name already exist. This has to be unique.");
                    }
                }
            }
            else
            {
                if (StringUtils.isNotBlank(forum.getSys_id()))
                {
                    whereClause = "sys_id = '" + forum.getSys_id() + "'";
                    qryResult = ServiceHibernate.queryCustomTable(SocialForumDTO.TABLE_NAME, whereClause, null);
                    dbRec = qryResult.get(0);
                }
            }

            //pwds
            String imPwd = forum.getU_im_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                forum.setU_im_pwd(CryptUtils.encrypt(imPwd));
            }

            //receive pwd
            String receivePwd = forum.getU_receive_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                forum.setU_receive_pwd(CryptUtils.encrypt(receivePwd));
            }

            //roles manipulation
            String readRoles = evaluateRoles(forum.getU_read_roles(), PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_READ_ROLES));
            forum.setU_read_roles(readRoles);

            String editRoles = evaluateRoles(forum.getU_edit_roles(), PropertiesUtil.getPropertyString(HibernateConstants.PROCESS_RIGHTS_EDIT));
            forum.setU_edit_roles(editRoles);

            //populate the sys_org and prepare a map 
            forum.setSys_org(orgSysId);
            Map<String, Object> record = forum.convertDTOToMap();

            SocialForumDTO oldForum = new SocialForumDTO();
            if (StringUtils.isNotBlank(forum.getSys_id()))
            {
                try
                {
                    oldForum = getForum(forum.getSys_id(), userName);
                }
                catch(RuntimeException re)
                {
                    // Needn't do anything here.
                }
            }
            checkComponentForChange(oldForum, forum);
            
            //persist it
            record.put("u_display_name", ((String)record.get("u_display_name")).trim());

            ResolveNodeVO forumNodeVO = null;
            try
            {               
                class TupleFinal {
                	Map<String, Object> record;
                	SocialForumDTO forum;
                	ResolveNodeVO forumNodeVO;
            		
            	}            	
                Map<String, Object> recordFinal = record;
              HibernateProxy.setCurrentUser(userName);
            	TupleFinal tupleResult = (TupleFinal) HibernateProxy.execute(() -> {                	
                	TupleFinal tuple = new TupleFinal();
                	tuple.record = GeneralHibernateUtil.insertOrUpdateCustomTable(userName, SocialForumDTO.TABLE_NAME, recordFinal);
                	tuple.forum = new SocialForumDTO(tuple.record);
                	tuple.forumNodeVO = createOrUpdateSocialCompNode(tuple.forum , NodeType.RSS, userName);
                	return tuple;
                });
            	record = tupleResult.record;
            	forum = tupleResult.forum;
            	forumNodeVO = tupleResult.forumNodeVO;
            
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);

                HibernateUtil.rethrowNestedTransaction(t);
                throw new Exception("Error while Inserting / Updating " + forum.getU_display_name());
            }
            
            if (isInsert)
            {
                // Add current user to the process
                addUserToSocialComp(forumNodeVO, NodeType.FORUM, userName);
                NotificationHelper.getForumNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.FORUM_CREATE, userName, true, true);
            }
            else
            {
                NotificationHelper.getForumNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.FORUM_UPDATE, userName, true, true);
            }
            
            checkIsEditable(forum, userName);

            forum.setSys_created_on(GMTDate.getLocalServerDate(forum.getSys_created_on()));
            forum.setSys_updated_on(GMTDate.getLocalServerDate(forum.getSys_updated_on()));
        }
        
        UsersVO user = ServiceHibernate.getUser(forum.getU_owner());
        if (user != null)
        {
            forum.setU_user_display_name(ServiceHibernate.getUser(forum.getU_owner()).getUDisplayName());
        }
        
        return forum;
    }
    
    public static void validateName(String compName) throws Exception
    {
        if (!compName.matches("^[a-zA-Z0-9 ]*$"))
        {
            throw new Exception("Name should contain only alphanumeric charactors and spaces");
        }
    }

//    public static Forum createSocialForumAndAddUser(SocialForumDTO forum, UsersVO user) throws Exception
//    {
//        Forum socialForum = SocialCompConversionUtil.convertSocialForumDTOToForum(forum, null);
//        User socialUser = SocialCompConversionUtil.convertUserToSocialUser(user, null);
//
//        //Create new Process. 
//        ServiceSocial.insertOrUpdateRSComponent(socialForum);
//
//        //Add current user to this Forum
//        if (checkUserAccessRights(forum, user.getUUserName()))
//        {
//            ServiceSocial.addUserToForum(socialUser, socialForum);
//        }
//
//        return socialForum;
//    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Social Team CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static SocialTeamDTO getTeam(String sysId, String userName)
    {
        SocialTeamDTO result = (SocialTeamDTO) getSocialDTO(AdvanceTree.TEAMS, sysId, null, userName);
        if (result == null)
        {
            Log.log.error("No Team with sys_id '" + sysId + "' exist.");
            return result;
        }
        GroupsVO groupVO = UserUtils.getGroup(null, result.getU_display_name(), userName);
        if (groupVO != null)
        {
            if (groupVO.getUIsLinkToTeam())
            {
                result.setHasGroup(true);
            }
        }

        checkIsEditable(result, userName);

        return result;
    }

    public static Set<SocialDTO> getContainerComponents(String compSysId, String compType, String userName) throws Exception
    {
        if (compType.equalsIgnoreCase("process"))
        {
            return getProcessComponents(compSysId, userName);
        }
        else if (compType.equalsIgnoreCase("teams"))
        {
            return getTeamComponents(compSysId, userName);
        }
        else if (compType.equalsIgnoreCase("forums"))
        {
            return getForumComponents(compSysId, userName);
        }
        else
            return new TreeSet<SocialDTO>();
    }

    public static Set<SocialDTO> getTeamComponents(String teamCompSysId, String userName) throws Exception
    {
        
        Set<SocialDTO> teamList = new TreeSet<SocialDTO>();
        Collection<ResolveNodeVO> nodes = null;
        try
        {
            nodes = ServiceGraph.getMemberNodes(null, teamCompSysId, null, NodeType.TEAM, userName);
        }
        catch (Exception e)
        {
            Log.log.error("ERROR getting members of team component SysId: " + teamCompSysId, e);
        }
        
        if (nodes != null && nodes.size() > 0)
        {
            for (ResolveNodeVO node : nodes)
            {
                if (node.getUType().name().equalsIgnoreCase("team"))
                {
                    SocialTeamDTO teamDto = getTeam(node.getUCompSysId(), userName);
                    teamDto.setCompType("Team");
                    teamList.add(teamDto);
                    continue;
                }
                
                if (node.getUType().name().equalsIgnoreCase("user"))
                {
                    UsersVO userVO = UserUtils.getUserById(node.getUCompSysId());
                    if (userVO != null)
                    {
                        SocialDTO socialDto = new SocialDTO();
                        socialDto.setSys_id(userVO.getSys_id());
                        socialDto.setCompType("User");
                        socialDto.setU_display_name(userVO.getUUserName());
                        String firstName = StringUtils.isBlank(userVO.getUFirstName()) ? "" : userVO.getUFirstName();
                        String lastName = StringUtils.isBlank(userVO.getULastName()) ? "" : userVO.getULastName();
                        socialDto.setU_description(firstName + " " + lastName);
                        teamList.add(socialDto);
                    }
                }
            }
        }
        
        return teamList;
    }

    public static String removeContainerComponents(String containerType, String containerId, List<SocialDTO> components, String userName) throws Exception
    {
        String warning = null;
        if (containerType != null)
        {
            List<String> warnCompList = new ArrayList<String>();
            
            if (containerType.equalsIgnoreCase("process"))
            {
                if (components != null)
                {
                    ResolveNodeVO parentProcessNode = ServiceGraph.findNode(null, containerId, null, NodeType.PROCESS, userName);
                    if (parentProcessNode == null)
                    {
                        throw new Exception("Node for the Process SysId: " + containerId + " could not be found.");
                    }
                    for (SocialDTO dto : components)
                    {
                        if (dto.getCompType().equals("Team"))
                        {
                            String message = removeMember(parentProcessNode, dto, NodeType.TEAM, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getTeamNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_TEAM_REMOVED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().equals("Forum"))
                        {
                            String message = removeMember(parentProcessNode, dto, NodeType.FORUM, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getForumNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.FORUM_REMOVED_FROM_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompName(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_FORUM_REMOVED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().equals("RSS"))
                        {
                            String message = removeMember(parentProcessNode, dto, NodeType.RSS, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getRssNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.RSS_REMOVED_FROM_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_RSS_REMOVED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().equals("Document") || dto.getCompType().contains("Runbook") || dto.getCompType().contains("DecissionTree"))
                        {
                            String message = removeMember(parentProcessNode, dto, NodeType.DOCUMENT, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getDocumentNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.DOCUMENT_REMOVED_FROM_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_DOCUMENT_REMOVED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().contains("ActionTasks"))
                        {
                            String message = removeMember(parentProcessNode, dto, NodeType.ACTIONTASK, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getActionTaskNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.ACTIONTASK_REMOVED_FROM_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_REMOVED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().equals("User"))
                        {
                            if (dto.getU_display_name().equals("admin"))
                            {
                                warning = "Warning: Admin user cannot be removed from Process.";
                                Log.log.warn(warning);
                                continue;
                            }
                            String message = removeMember(parentProcessNode, dto, NodeType.USER, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getUserNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.USER_REMOVED_FROM_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_USER_REMOVED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().equals("Namespace"))
                        {
                            String message = removeMember(parentProcessNode, dto, NodeType.NAMESPACE, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                //NotificationHelper.getActionTaskNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.NAMESPACE_REMOVED_FROM_PROCESS, userName, true, true);
                                //NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_NAMESPACE_REMOVED, userName, true, true);
                            }
                        }
                    }
                }
            }
            else if (containerType.equalsIgnoreCase("teams"))
            {
                if (components != null)
                {
                    //Team parentTeam = SocialCompConversionUtil.createTeam(containerId);
                    ResolveNodeVO parentTeamNode = ServiceGraph.findNode(null, containerId, null, NodeType.TEAM, userName);
                    if (parentTeamNode == null)
                    {
                        throw new Exception("Node for the Team SysId: " + containerId + " could not be found.");
                    }
                    
                    for (SocialDTO dto : components)
                    {
                        if (dto.getCompType().equals("Team"))
                        {
                            String message = removeMember(parentTeamNode, dto, NodeType.TEAM, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getTeamNotification(dto.getSys_id(), parentTeamNode.getUCompName(), UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_TEAM, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().equals("User"))
                        {
                            if (dto.getU_display_name().equals("admin"))
                            {
                                warning = "Warning: Admin user cannot be removed from Team.";
                                Log.log.warn(warning);
                                continue;
                            }
                            String message = removeMember(parentTeamNode, dto, NodeType.USER, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getUserNotification(dto.getSys_id(), parentTeamNode.getUCompName(), UserGlobalNotificationContainerType.USER_REMOVED_FROM_TEAM, userName, true, true);
                                NotificationHelper.getTeamNotification(parentTeamNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.TEAM_USER_REMOVED, userName, true, true);
                            }

                            GroupsVO groupVO = UserUtils.getGroup(null, parentTeamNode.getUCompName(), userName);
                            if (groupVO != null)
                            {
                                if (groupVO.getUIsLinkToTeam())
                                {
                                    List<String> groupList = new ArrayList<String>();
                                    groupList.add(parentTeamNode.getUCompName());
                                    ServiceHibernate.removeUserFromGroups(dto.getSys_id(), null, groupList, userName);
                                }
                            }
                        }
                    }
                }
            }
            else if (containerType.equalsIgnoreCase("forums"))
            {
                if (components != null)
                {
                    //Forum parentForum = SocialCompConversionUtil.createForum(containerId);
                    ResolveNodeVO parentForumNode = ServiceGraph.findNode(null, containerId, null, NodeType.FORUM, userName);
                    for (SocialDTO dto : components)
                    {
                        if (dto.getCompType().equals("User"))
                        {
                            if (dto.getU_display_name().equals("admin"))
                            {
                                warning = "Warning: Admin user cannot be removed from Forum.";
                                Log.log.warn(warning);
                                continue;
                            }
                            String message = removeMember(parentForumNode, dto, NodeType.USER, userName);
                            if (message != null)
                            {
                                warnCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getUserNotification(dto.getSys_id(), parentForumNode.getUCompName(), UserGlobalNotificationContainerType.USER_REMOVED_FROM_FORUM, userName, true, true);
                                NotificationHelper.getForumNotification(parentForumNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.FORUM_USER_REMOVED, userName, true, true);
                            }
                        }
                    }
                }
            }
            
            if (warnCompList.size() > 0)
            {
                Log.log.error("Resolve does not contain node for these components: " + warnCompList.toString() + ".");
                // throw new Exception("Error while adding " + errorCompList + "(s) to this process. Please contact System Admin.");
                throw new Exception("Resolve does not contain node for these components: " + warnCompList.toString());
            }
        }
        
        return warning;
    }
    
    private static String removeMember(ResolveNodeVO destComponentNode, SocialDTO dto, NodeType srcCompNodeType, String userName) throws Exception
    {
        String errorMessage = null;
        Collection<ResolveNodeVO> destComponentNodes = new ArrayList<ResolveNodeVO>();
        ResolveNodeVO srcComponentNode = ServiceGraph.findNode(null, dto.getSys_id(), null, srcCompNodeType, userName);
        if (srcComponentNode != null)
        {
            destComponentNodes.add(destComponentNode);
            ServiceGraph.removeRelation(srcComponentNode.getSys_id(), srcComponentNode.getUCompSysId(), 
                                        srcComponentNode.getUCompName(), srcComponentNode.getUType(), 
                                        destComponentNodes, userName, NonNeo4jRelationType.MEMBER.name());
        }
        else
        {
            errorMessage = "Member node does not exist for the " + srcCompNodeType.name() + ": " + dto.getSys_id();
        }
        
        return errorMessage;
    }
    
    public static void unfollowCompByUser(ResolveNodeVO unfollowNode, String userSysId, String userName) throws Exception
    {
        ResolveNodeVO userNodeVO = ServiceGraph.findNode(null, userSysId, null, NodeType.USER, userName);
        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
        if (userNodeVO == null)
        {
            Log.log.error("Node does not exist for the User: " + userSysId);
        }
        else
        {
            followComponents.add(unfollowNode);
            ServiceGraph.unfollow(null, null, userNodeVO.getUCompName(), NodeType.USER, followComponents, userName);
        }
    }
    
    public static void followCompByUser(ResolveNodeVO unfollowNode, String userSysId, String userName) throws Exception
    {
        ResolveNodeVO userNodeVO = ServiceGraph.findNode(null, userSysId, null, NodeType.USER, userName);
        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
        if (userNodeVO == null)
        {
            Log.log.error("Node does not exist for the User: " + userSysId);
        }
        else
        {
            followComponents.add(unfollowNode);
            ServiceGraph.follow(null, null, userNodeVO.getUCompName(), NodeType.USER, followComponents, followerEdgeProps, userName);
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO getListOfComponentsToBeAdded(QueryDTO query, String parentName, String parentId, String userName)
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            //            query.setModelName(socialTableName);
            if (query.getTableName().equalsIgnoreCase("resolveactiontask"))
            {
                query.setSelectColumns("sys_id,UName,UNamespace,USummary,sysUpdatedOn");
                String whereClause = query.getWhereClause();
                if (StringUtils.isNotBlank(whereClause))
                {
                    whereClause = " where (LOWER(UName) like '" + whereClause.toLowerCase() + "%' " +
                    		" OR LOWER(UNamespace) like '" + whereClause.toLowerCase() + "%'" +
                    	    " OR LOWER(USummary) like '" + whereClause.toLowerCase() + "%') ";
                    query.setWhereClause(whereClause);
                }

                List<String> actionTaskIds = getAddedCompIdList("ActionTask", parentName, parentId, userName, null);
                if (actionTaskIds.size() != 0)
                {
                    whereClause = appendNotInCaluse(whereClause, actionTaskIds, false);
                }
                query.setWhereClause(whereClause);

                Map<String, Object> resultMap = ServiceHibernate.getAllActionTasks(query);
                //            int total = ServiceHibernate.getTotalHqlCount(query);
                List<ResolveActionTaskVO> data = (List<ResolveActionTaskVO>) resultMap.get("ACTION_TASK_VO_LIST");
                Integer total = (Integer) resultMap.get("TOTAL_COUNT");
                result.setSuccess(true).setRecords(data);
                result.setTotal(total);
            }
            else if (query.getTableName().equalsIgnoreCase("Users"))
            {
                //                query.setPage(1);
                usersWhereClause = query.getWhereClause();
                int start = query.getStart();
                if (start == 0 || !usersWhereClause.equalsIgnoreCase(query.getWhereClause()))
                {
                    usersWhereClause = query.getWhereClause();
                    String whereClause = query.getWhereClause();

                    if (StringUtils.isNotBlank(whereClause))
                    {
                        whereClause = whereClause.toLowerCase();
                        whereClause = " AND (LOWER(u.UUserName) like '" + whereClause + "%' OR LOWER(u.UFirstName) like '" + whereClause + "%' OR LOWER(u.ULastName) like '" + whereClause + "%' )";
                    }

                    List<String> userIds = getAddedCompIdList("User", parentName, parentId, userName, null);
                    if (userIds.size() != 0)
                    {
                        whereClause = appendNotInCaluse(whereClause, userIds, true);
                    }

                    StringBuilder readRoleBuffer = new StringBuilder();
                    String[] readRolesArray = new String[0];
                    if (StringUtils.isNotBlank(parentId))
                    {
                        if (parentName.equals("process"))
                        {
                            SocialProcessDTO processDTO = getProcess(parentId, userName);
                            String processRoles = processDTO.getU_read_roles() + "," +  processDTO.getU_edit_roles() + "," + processDTO.getU_post_roles();
                            readRolesArray = processRoles.split(",");
                        }
                        else if (parentName.equals("teams"))
                        {
                            SocialTeamDTO teamDTO = getTeam(parentId, userName);
                            String teamRoles = teamDTO.getU_read_roles() + "," +  teamDTO.getU_edit_roles() + "," + teamDTO.getU_post_roles();
                            readRolesArray = teamRoles.split(",");
                        }
                        else if (parentName.equals("forums"))
                        {
                            SocialForumDTO forumDTO = getForum(parentId, userName);
                            String forumRoles = forumDTO.getU_read_roles() + "," +  forumDTO.getU_edit_roles() + "," + forumDTO.getU_post_roles();
                            readRolesArray = forumRoles.split(",");
                        }
                        else if (parentName.equals("rss"))
                        {
                            SocialRssDTO rssDTO = getRss(parentId, userName);
                            String rssRoles = rssDTO.getU_read_roles() + "," +  rssDTO.getU_edit_roles() + "," + rssDTO.getU_post_roles();
                            readRolesArray = rssRoles.split(",");
                        }
                    }
                    else
                    {
                        SocialDTO dto = ServiceHibernate.getDefaultAccessRights(parentName);
                        if (dto != null && StringUtils.isNotBlank(dto.getU_read_roles()))
                        {
                            readRolesArray = (dto.getU_read_roles() + "," + dto.getU_edit_roles() + "," + dto.getU_post_roles()).split(",");
                        }
                    }
                    readRoleBuffer.append("(");
                    for (String role : readRolesArray)
                    {
                        readRoleBuffer.append("'").append(role.trim()).append("',");
                    }
                    readRoleBuffer.setCharAt(readRoleBuffer.length() - 1, ')');

                    users = ServiceHibernate.getUsersWithRole(readRoleBuffer.toString(), whereClause);
                }

                List<UsersVO> pagedList = new ArrayList<UsersVO>();
                if (users != null)
                {
                    int pageSize = query.getLimit();
                    for (int i = 0; i < pageSize; i++)
                    {
                        try
                        {
                            pagedList.add(users.get(start++));
                        }
                        catch (IndexOutOfBoundsException iobe)
                        {
                            break;
                        }
                    }
                }

                result.setSuccess(true).setRecords(pagedList);
                result.setTotal(users.size());
            }
            else if (query.getTableName().equalsIgnoreCase("WikiDocument"))
            {
                String whereClause = query.getWhereClause();
                if (StringUtils.isNotBlank(whereClause))
                {
                    whereClause = " (LOWER(wd.UName) like '" + whereClause.toLowerCase() + "%' " +
                                    " OR LOWER(wd.UNamespace) like '" + whereClause.toLowerCase() + "%'" +
                                    " OR LOWER(wd.USummary) like '" + whereClause.toLowerCase() + "%') ";
                    query.setWhereClause(whereClause);
                            
                            
//                    query.setFilter("[{\"field\":\"UName\",\"condition\":\"contains\",\"value\":\"" + whereClause + "\"}," +
//                    		"{\"field\":\"UNamespace\",\"condition\":\"contains\",\"value\":\"" + whereClause + "\"}," +
//                    		"{\"field\":\"USummary\",\"condition\":\"contains\",\"value\":\"" + whereClause + "\"} ]");
//                    query.setWhereClause(null);
//                    whereClause = null;
                }

                List<String> userIds = getAddedCompIdList("Document", parentName, parentId, userName, null);
                if (userIds.size() != 0)
                {
                    whereClause = appendWikiNotInClause(whereClause, userIds);
                    query.setWhereClause(whereClause);
                }

                List<WikiDocumentVO> resultMap = ServiceWiki.getWikiDocuments(query, NodeType.DOCUMENT, userName);
                int count = ServiceHibernate.getTotalHqlCount(query);
                result.setSuccess(true).setRecords(resultMap);
                result.setTotal(count);
            }
            else if (query.getTableName().equalsIgnoreCase("Runbook"))
            {
                query.setTableName("WikiDocument");
                query.setModelName("WikiDocument");
                String whereClause = query.getWhereClause();
                if (StringUtils.isNotBlank(whereClause))
                {
                    whereClause = " (LOWER(wd.UName) like '" + whereClause.toLowerCase() + "%' " +
                                    " OR LOWER(wd.UNamespace) like '" + whereClause.toLowerCase() + "%'" +
                                    " OR LOWER(wd.USummary) like '" + whereClause.toLowerCase() + "%') ";
                    query.setWhereClause(whereClause);
                    
//                    query.setFilter("[{\"field\":\"UName\",\"condition\":\"contains\",\"value\":\"" + whereClause + "\"}]");
//                    query.setWhereClause(null);
//                    whereClause = null;
                }

                //                if (StringUtils.isBlank(whereClause))
                //                {
                //                    whereClause = whereClause + " (UHasActiveModel = true) " ;
                //                }
                //                else
                //                {
                //                    whereClause = whereClause + " and (UHasActiveModel = true) " ;
                //                }
                query.setWhereClause(whereClause);

                List<String> userIds = getAddedCompIdList("Runbook", parentName, parentId, userName, null);
                if (userIds.size() != 0)
                {
                    whereClause = appendWikiNotInClause(whereClause, userIds);
                    query.setWhereClause(whereClause);
                }

                List<WikiDocumentVO> resultMap = ServiceWiki.getWikiDocuments(query, NodeType.RUNBOOK, userName);
                result.setSuccess(true).setRecords(resultMap);
                int count = ServiceHibernate.getTotalHqlCount(query);
                result.setTotal(count);
            }
            else if (query.getTableName().contains("team") || query.getTableName().contains("forum") || query.getTableName().contains("rss"))
            {
                query.setSort("[{\"property\":\"u_display_name\",\"direction\":\"ASC\"}]");
                String whereClause = query.getWhereClause();
                if (StringUtils.isNotBlank(whereClause))
                {
                    whereClause = whereClause.toLowerCase();
                    whereClause = " ( LOWER(u_display_name) like '" + whereClause + "%' OR LOWER(u_description) like '" + whereClause + "%' )";
                    query.setWhereClause(whereClause);
                }

                List<String> idList = null;
                if (query.getTableName().contains("team"))
                {
                    idList = getAddedCompIdList("Team", parentName, parentId, userName, null);
                }
                else if (query.getTableName().contains("forum"))
                {
                    idList = getAddedCompIdList("Forum", parentName, parentId, userName, null);
                }
                else if (query.getTableName().contains("rss"))
                {
                    idList = getAddedCompIdList("Rss", parentName, parentId, userName, null);
                }
                if (idList != null && idList.size() > 0)
                {
                    whereClause = appendNotInCaluse(whereClause, idList, false);
                    query.setWhereClause(whereClause);
                }

                Map<String, Object> data = ServiceHibernate.getSocialComponents(query, userName);

                int total = ServiceHibernate.getTotalHqlCount(query);
                if (query.getModelName().contains("process"))
                {
                    List<SocialRssDTO> list = (List<SocialRssDTO>) data.get("DATA");
                    result.setSuccess(true).setRecords(list);
                }
                else if (query.getModelName().contains("forum"))
                {
                    List<SocialForumDTO> list = (List<SocialForumDTO>) data.get("DATA");
                    result.setSuccess(true).setRecords(list);
                }
                else if (query.getModelName().contains("team"))
                {
                    List<SocialTeamDTO> list = (List<SocialTeamDTO>) data.get("DATA");
                    result.setSuccess(true).setRecords(list);
                }
                else if (query.getModelName().contains("rss"))
                {
                    List<SocialRssDTO> list = (List<SocialRssDTO>) data.get("DATA");
                    result.setSuccess(true).setRecords(list);
                }
                result.setTotal(total);
            }
            else if (query.getTableName().equalsIgnoreCase("namespace"))
            {
                // [{"field":"uname","type":"auto","condition":"contains","value":"WHERE_CLAUSE"}]
                StringBuilder filter = new StringBuilder();
                filter.append("[{\"field\":\"type\",\"condition\":\"equals\",\"value\":\"namespace\"}");
                User user = SocialCompConversionUtil.getSocialUser(userName);
                String whereClause = query.getWhereClause();
                if (StringUtils.isNotBlank(whereClause))
                {
                    filter.append(", {\"field\":\"name\",\"condition\":\"contains\",\"value\":\"" + whereClause + "\"}");
                }
                filter.append("]");
                query.setFilter(filter.toString());
                query.setOperator("AND");
                query.setModelName(null);
                
                result = ServiceSocial.getAllStreams(user, query);
                result.setSuccess(true);

            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Components", e);
            result.setSuccess(false).setMessage("Error retrieving Components " + e.getMessage());
        }

        return result;
    }

    static String appendNotInCaluse(String whereClause, List<String> idList, boolean isString)
    {
        StringBuilder notIn = new StringBuilder();
        for (String s : idList)
        {
            notIn.append("'").append(s).append("',");
        }

        if (notIn.length() > 0)
        {
            notIn.setCharAt(notIn.length() - 1, ' ');

            if (isString)
            {
                whereClause = whereClause + " and (u.sys_id not in (" + notIn.toString() + ") ) ";
            }
            else
            {
                if (StringUtils.isNotBlank(whereClause))
                {
                    whereClause = whereClause + " and (sys_id not in (" + notIn.toString() + ") ) ";
                }
                else
                {
                    whereClause = " where sys_id not in (" + notIn.toString() + ") ";
                }
            }
        }

        return whereClause;
    }

    static String appendWikiNotInClause(String whereClause, List<String> idList)
    {

        StringBuilder notIn = new StringBuilder();
        for (String s : idList)
        {
            notIn.append("'").append(s).append("',");
        }

        if (notIn.length() > 0)
        {
            notIn.setCharAt(notIn.length() - 1, ' ');
            if (StringUtils.isBlank(whereClause))
            {
                whereClause = " sys_id not in ( " + notIn.toString() + " ) ";
            }
            else
            {
                whereClause = whereClause + "and sys_id not in ( " + notIn.toString() + " ) ";
            }
        }
        return whereClause;
    }

    public static List<String> getAddedCompIdList(String comp, String parentName, String parentId, String userName, Set<String> processedIds) throws Exception 
    {
        //234
        List<String> ids = new ArrayList<String>();
        
        if (processedIds == null)
        {
            processedIds = new HashSet<String>();
        }
        
        if (StringUtils.isBlank(parentId)) return ids;
        NodeType compNodeType = null;
        
        if (parentName.equalsIgnoreCase("process"))
        {
            compNodeType = NodeType.PROCESS;
        }
        else if (parentName.equalsIgnoreCase("teams"))
        {
            compNodeType = NodeType.TEAM;
            ids.add(parentId);
        }
        else if (parentName.equalsIgnoreCase("forums"))
        {
            compNodeType = NodeType.FORUM;
        }
        
        Collection<ResolveNodeVO> nodes = null;
        try
        {
            nodes = ServiceGraph.getMemberNodes(null, parentId, null, compNodeType, userName);
        }
        catch (Exception e)
        {
            Log.log.error("ERROR getting members of " + compNodeType.name() + " with component SysId: " + parentId, e);
        }
        
        if (nodes != null && nodes.size() > 0)
        {
            for (ResolveNodeVO node : nodes)
            {
                if ((node.getUType().name().equalsIgnoreCase(comp) || 
                     compNodeType.name().equals(NodeType.TEAM.name())) && 
                    !processedIds.contains(node.getUCompSysId()))
                {
                    //Add to list of processed nodes to avoid recursion (if one exists)
                    processedIds.add(node.getUCompSysId());
                    if (!ids.contains(node.getUCompSysId()))
                    {
                        if (node.getUType().name().equalsIgnoreCase(comp))
                        {
                            ids.add(node.getUCompSysId());
                        }
                        else if (compNodeType.name().equals(NodeType.TEAM.name()))
                        {
                            //Recurse Team Tree
                            ids.addAll(getAddedCompIdList(comp, parentName, node.getUCompSysId(), userName, processedIds));
                        }
                    }
                }
            }
        }
        
        return ids;
    }
    
    public static List<String> addComponentToContainer(String containerType, String containerId, List<SocialDTO> components, String userName, boolean addUsersToGroup, boolean validate) throws Exception
    {
        List<String> errorCompList = new ArrayList<String>();
        
        if (containerType != null)
        {
            if (containerType.equalsIgnoreCase("process"))
            {
                if (components != null)
                {
                    ResolveNodeVO parentProcessNode = ServiceGraph.findNode(null, containerId, null, NodeType.PROCESS, userName);
                    if (parentProcessNode == null)
                    {
                        // This condition should never arise.
                        throw new Exception("Node for the Process SysId: " + containerId + " could not be found.");
                    }
                    
                    for (SocialDTO dto : components)
                    {
                        if (dto.getCompType().contains("team"))
                        {
                            String message = addMember(parentProcessNode, dto, NodeType.TEAM, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getTeamNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.TEAM_ADDED_TO_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_TEAM_ADDED, userName, true, true);
                            }

                        }
                        else if (dto.getCompType().contains("forum"))
                        {
                            String message = addMember(parentProcessNode, dto, NodeType.FORUM, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getForumNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.FORUM_ADDED_TO_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_FORUM_ADDED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().contains("rss"))
                        {
                            String message = addMember(parentProcessNode, dto, NodeType.RSS, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getRssNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.RSS_ADDED_TO_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_RSS_ADDED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().contains("Document") || dto.getCompType().contains("Runbook") || dto.getCompType().contains("DecissionTree"))
                        {
                            String message = addMember(parentProcessNode, dto, NodeType.DOCUMENT, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getDocumentNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.DOCUMENT_ADDED_TO_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_DOCUMENT_ADDED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().contains("ActionTask"))
                        {
                            String message = addMember(parentProcessNode, dto, NodeType.ACTIONTASK, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getActionTaskNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.ACTIONTASK_ADDED_TO_PROCESS, userName, true, true);
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_ADDED, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().contains("User"))
                        {
                            String message = addMember(parentProcessNode, dto, NodeType.USER, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getProcessNotification(parentProcessNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.PROCESS_USER_ADDED, userName, true, true);
                                NotificationHelper.getUserNotification(dto.getSys_id(), parentProcessNode.getUCompName(), UserGlobalNotificationContainerType.USER_ADDED_TO_PROCESS, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().contains("namespace"))
                        {
                            String message = addMember(parentProcessNode, dto, NodeType.NAMESPACE, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                //ServiceSocial.addNameSpaceToProcess(namespace, parentProcessNode);
                                //NotificationHelper.getProcessNotification(parentProcess, namespace, UserGlobalNotificationContainerType., username, doSubmit, isAsync)
                            }
                        }
                    }

                    if (errorCompList.size() > 0)
                    {
                        Log.log.warn("Resolve does not contain node for " + errorCompList.size() + " of the " + components.size() + " components selected for adding into process, components not added to process are " + errorCompList.toString() + ".");
                        // throw new Exception("Error while adding " + errorCompList + "(s) to this process. Please contact System Admin.");
                        //throw new Exception("Resolve does not contain node for these components: " + errorCompList.toString());
                        return errorCompList;
                    }
                }
            }
            else if (containerType.equalsIgnoreCase("teams"))
            {
                ResolveNodeVO parentTeamNode = ServiceGraph.findNode(null, containerId, null, NodeType.TEAM, userName);
                
                if (parentTeamNode == null)
                {
                    throw new Exception("Node for the Team SysId: " + containerId + " could not be found.");
                }
                
                List<String> userNames = new ArrayList<String>();
                if (components != null)
                {
                    for (SocialDTO dto : components)
                    {
                        if (dto.getCompType().contains("team"))
                        {
                            String message = addMember(parentTeamNode, dto, NodeType.TEAM, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getTeamNotification(dto.getSys_id(), parentTeamNode.getUCompName(), UserGlobalNotificationContainerType.TEAM_ADDED_TO_TEAM, userName, true, true);
                            }
                        }
                        else if (dto.getCompType().contains("User"))
                        {
                            if (addUsersToGroup)
                            {
                                /*
                                 *  From UI, there wasn't any apropriate field available to store userName, so I used u_display_name
                                 *  Populate userNames list here and outside the loop, call addUsersToGroup
                                 */

                                userNames.add(dto.getU_display_name());
                            }
                            
                            String message = addMember(parentTeamNode, dto, NodeType.USER, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getUserNotification(dto.getSys_id(), parentTeamNode.getUCompName(), UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM, userName, true, true);
                                NotificationHelper.getTeamNotification(parentTeamNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.TEAM_USER_ADDED, userName, true, true);
                            }
                        }
                    }
                }

                if (errorCompList.size() > 0)
                {
                    Log.log.warn("Resolve does not contain node for " + errorCompList.size() + " of the " + components.size() + " components selected for adding into teams, components not added to teams are " + errorCompList.toString() + ".");
                    // throw new Exception("Error while adding " + errorCompList + "(s) to this process. Please contact System Admin.");
                    //throw new Exception("Resolve does not contain node for these components: " + errorCompList.toString() + ".");
                    return errorCompList;
                }

                if (userNames.size() > 0 && addUsersToGroup)
                {
                    /*
                     * addUsersToGroup would also add users to the team.
                     */
                    ServiceHibernate.addUsersToGroup(null, parentTeamNode.getUCompName(), userNames, userName);
                }

            }
            else if (containerType.equalsIgnoreCase("forums"))
            {
                ResolveNodeVO parentForumNode = ServiceGraph.findNode(null, containerId, null, NodeType.FORUM, userName);
                if (parentForumNode == null)
                {
                    throw new Exception("Node for the Forum SysId: " + containerId + " could not be found.");
                }
                if (components != null)
                {
                    
                    for (SocialDTO dto : components)
                    {
                        if (dto.getCompType().contains("User"))
                        {
                            String message = addMember(parentForumNode, dto, NodeType.USER, userName);
                            if (message != null)
                            {
                                errorCompList.add(message);
                            }
                            else
                            {
                                NotificationHelper.getForumNotification(parentForumNode.getUCompSysId(), dto.getU_display_name(), UserGlobalNotificationContainerType.FORUM_USER_ADDED, userName, true, true);
                                NotificationHelper.getUserNotification(dto.getSys_id(), parentForumNode.getUCompName(), UserGlobalNotificationContainerType.USER_ADDED_TO_FORUM, userName, true, true);
                            }
                        }
                    }
                }
                
                if (errorCompList.size() > 0)
                {
                    Log.log.warn("Resolve does not contain node for " + errorCompList.size() + " of the " + components.size() + " components selected for adding into forums, components not added to forums are " + errorCompList.toString() + ".");
                    // throw new Exception("Error while adding " + errorCompList + "(s) to this process. Please contact System Admin.");
                    //throw new Exception("Resolve does not contain node for these components: " + errorCompList.toString());
                    return errorCompList;
                }
            }
        }
        
        return errorCompList;
    }
    
    private static String addMember(ResolveNodeVO destComponentNode, SocialDTO dto, NodeType compNodeType, String userName)
    {
        String errorMessage = null;
        Collection<ResolveNodeVO> destComponentNodes = new ArrayList<ResolveNodeVO>();
        try
        {
            ResolveNodeVO srcComponentNode = ServiceGraph.findNode(null, dto.getSys_id(), null, compNodeType, userName);
            if (srcComponentNode != null)
            {
                destComponentNodes.add(destComponentNode);
                
                //srcComponentNode is MEMBER of destComponentNode
                ServiceGraph.addRelation(srcComponentNode.getSys_id(), srcComponentNode.getUCompSysId(), srcComponentNode.getUCompName(), srcComponentNode.getUType(),
                                         destComponentNodes, followerEdgeProps, userName, NonNeo4jRelationType.MEMBER.name());
            }
            else
            {
                errorMessage = compNodeType.name() + ": " + dto.getU_display_name();
            }
        }
        catch (Exception e)
        {
            errorMessage = "Error adding " + dto.getU_display_name() + " as MEMBER to " + destComponentNode.getUType().name() +  " " + destComponentNode.getUCompName();
            Log.log.error(errorMessage, e);
            //throw new Exception(errorMessage);
        }
        
        return errorMessage;
    }

//    public static void addUserToTeam(String userSysId, String teamId, String userName) throws Exception
//    {
//        Team parentTeam = SocialCompConversionUtil.createTeam(teamId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userSysId);
//        ServiceSocial.addUserToTeam(socialUser, parentTeam);
//
//        NotificationHelper.getUserNotification(socialUser, parentTeam, UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM, userName, true, true);
//        NotificationHelper.getTeamNotificationUserLeftTeam(parentTeam, socialUser, UserGlobalNotificationContainerType.TEAM_USER_ADDED, userName, true, true);
//    }

    public static SocialTeamDTO getTeamByName(String teamname, String username)
    {
        SocialTeamDTO result = null;

        if (StringUtils.isNotBlank(teamname))
        {
            result = (SocialTeamDTO) getSocialDTO(AdvanceTree.TEAMS, null, teamname, username);
            if (result == null)
            {
                throw new RuntimeException("No Team with name '" + teamname + "' exist.");
            }
        }

        return result;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Social Forum CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static SocialForumDTO getForum(String sysId, String userName)
    {
        SocialForumDTO result = (SocialForumDTO) getSocialDTO(AdvanceTree.FORUMS, sysId, null, userName);
        if (result == null)
        {
            throw new RuntimeException("No Forum with sys_id '" + sysId + "' exist.");
        }

        checkIsEditable(result, userName);

        return result;
    }

    public static SocialForumDTO getForumByName(String forumName, String userName)
    {
        SocialForumDTO result = (SocialForumDTO) getSocialDTO(AdvanceTree.FORUMS, null, forumName, userName);
        if (result == null)
        {
            throw new RuntimeException("No Forum with name '" + forumName + "' exist.");
        }

        checkIsEditable(result, userName);

        return result;
    }

    public static Set<SocialDTO> getForumComponents(String forumCompSysId, String userName) throws Exception
    {
        Set<SocialDTO> resultList = new TreeSet<SocialDTO>();
        Collection<ResolveNodeVO> nodes = null;
        try
        {
            // Users
            nodes = ServiceGraph.getMemberNodes(null, forumCompSysId, null, NodeType.FORUM, userName);
        }
        catch (Exception e)
        {
            Log.log.error("ERROR getting members of forum componentsSysId: " + forumCompSysId, e);
        }
        
        if (nodes != null && nodes.size() > 0)
        {
            for (ResolveNodeVO node : nodes)
            {
                if (node.getUType().name().equalsIgnoreCase("user"))
                {
                    UsersVO userVO = UserUtils.getUserById(node.getUCompSysId());
                    if (userVO != null)
                    {
                        SocialDTO socialDto = new SocialDTO();
                        socialDto.setSys_id(userVO.getSys_id());
                        socialDto.setCompType("User");
                        socialDto.setU_display_name(userVO.getUUserName());
                        String firstName = StringUtils.isBlank(userVO.getUFirstName()) ? "" : userVO.getUFirstName();
                        String lastName = StringUtils.isBlank(userVO.getULastName()) ? "" : userVO.getULastName();
                        socialDto.setU_description(firstName + " " + lastName);
                        resultList.add(socialDto);
                    }
                }
            }
        }

        return resultList;
    }

    // RSS
    public static SocialRssDTO getRss(String sysId, String userName)
    {
        SocialRssDTO result = (SocialRssDTO) getSocialDTO(AdvanceTree.RSS, sysId, null, userName);
        if (result == null)
        {
            throw new RuntimeException("No RSS with sys_id '" + sysId + "' exist.");
        }

        checkIsEditable(result, userName);

        return result;
    }

    public static SocialRssDTO getRssByName(String rssName, String userName)
    {
        SocialRssDTO result = (SocialRssDTO) getSocialDTO(AdvanceTree.RSS, null, rssName, userName);
        if (result == null)
        {
            throw new RuntimeException("No RSS with name '" + rssName + "' exist.");
        }

        checkIsEditable(result, userName);

        return result;
    }

    public static SocialRssDTO saveRss(SocialRssDTO rss, String userName, Boolean validate) throws Exception
    {
        if (rss != null)
        {
            validateName(rss.getU_display_name());
            UsersVO user = ServiceHibernate.getUser(userName);
            String orgSysId = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

//            User socialUser = SocialCompConversionUtil.convertUserToSocialUser(user, null);
//            RSComponent comp = null;

            boolean isInsert = StringUtils.isBlank(rss.getSys_id()) ? true : false;
            Date now = GMTDate.getDate();
//            if (isInsert)
//            {
//                rss.setSys_created_by(userName);
//                rss.setSys_created_on(now);
//                rss.setSys_updated_on(now);
//                rss.setSys_updated_by(userName);
//            }
//            else
//            {
//                rss.setSys_created_on(GMTDate.getDate(rss.getSys_created_on()));
//                rss.setSys_updated_on(now);
//                rss.setSys_updated_by(userName);
//            }

            String name = rss.getU_display_name();
            if (StringUtils.isBlank(name))
            {
                throw new RuntimeException("RSS name cannot be empty and is mandatory.");
            }
            
            Map<String, Object> whereClauseParams = new HashMap<String, Object>();
            
            //String whereClause = "LOWER(u_display_name) = '" + name.toLowerCase() + "'";
            String whereClause = "LOWER(u_display_name) = :u_display_name";
            whereClauseParams.put("u_display_name", name.toLowerCase());
            
            if (StringUtils.isNotBlank(orgSysId))
            {
                //whereClause += " and ( sys_org is null OR sys_org = '" + orgSysId + "')";
                whereClause += " and ( sys_org is null OR sys_org = :sys_org)";
                whereClauseParams.put("sys_org", orgSysId);
            }

            List<Map<String, Object>> qryResult = ServiceHibernate.queryCustomTable(SocialRssDTO.TABLE_NAME, whereClause, whereClauseParams);
            Map<String, Object> dbRec = null;

            //if its a create - than check for uniqueness
            if (qryResult != null && qryResult.size() > 0)
            {
                dbRec = qryResult.get(0);
                if (isInsert)
                {
                    throw new RuntimeException("RSS name already exist. This has to be unique.");
                }
                else
                {
                    //this is update..make sure that the sys_id of both are same , than its the same rec else name already exist
                    String dbSysId = (String) dbRec.get("sys_id");
                    if (!rss.getSys_id().equalsIgnoreCase(dbSysId))
                    {
                        throw new RuntimeException("RSS name already exist. This has to be unique.");
                    }
                }
            }
            else
            {
                if (StringUtils.isNotBlank(rss.getSys_id()))
                {
                    whereClause = "sys_id = '" + rss.getSys_id() + "'";
                    qryResult = ServiceHibernate.queryCustomTable(SocialRssDTO.TABLE_NAME, whereClause, null);
                    dbRec = qryResult.get(0);
                }
            }
            
            
            //updated sys columns ...and make sure that u_feed_updated is not changed
            if(dbRec != null)
            {
                //this means this is an update
                rss.setU_feed_updated((Date) dbRec.get("u_feed_updated"));
                rss.setSys_created_on((Date) dbRec.get("sys_created_on"));
                rss.setSys_created_by((String) dbRec.get("sys_created_by"));
                rss.setSys_updated_on(now);
                rss.setSys_updated_by(userName);
            }
            else
            {
                //this is for insert
                rss.setSys_created_by(userName);
                rss.setSys_created_on(now);
                rss.setSys_updated_on(now);
                rss.setSys_updated_by(userName);
            }

            //pwds
            String imPwd = rss.getU_im_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                rss.setU_im_pwd(CryptUtils.encrypt(imPwd));
            }

            //receive pwd
            String receivePwd = rss.getU_receive_pwd();
            if (StringUtils.isNotBlank(imPwd) && !imPwd.startsWith("ENC1:"))
            {
                rss.setU_receive_pwd(CryptUtils.encrypt(receivePwd));
            }

            //roles manipulation
            String readRoles = evaluateRoles(rss.getU_read_roles(), PropertiesUtil.getPropertyString(HibernateConstants.RSS_RIGHTS_READ));
            rss.setU_read_roles(readRoles);

            String editRoles = evaluateRoles(rss.getU_edit_roles(), PropertiesUtil.getPropertyString(HibernateConstants.RSS_RIGHTS_EDIT));
            rss.setU_edit_roles(editRoles);

            //populate the sys_org and prepare a map 
            rss.setSys_org(orgSysId);
            Map<String, Object> record = rss.convertDTOToMap();

            //persist it
            record.put("u_display_name", ((String)record.get("u_display_name")).trim());

            ResolveNodeVO rssNodeVO = null;
            try
            {                
                class TupleFinal {
                	Map<String, Object> record;
                	SocialRssDTO rss;
                	ResolveNodeVO rssNodeVO;
            		
            	}            	
                Map<String, Object> recordFinal = record;
              HibernateProxy.setCurrentUser(userName);
            	TupleFinal tupleResult = (TupleFinal) HibernateProxy.execute(() -> {                	
                	TupleFinal tuple = new TupleFinal();
                	tuple.record = GeneralHibernateUtil.insertOrUpdateCustomTable(userName, SocialRssDTO.TABLE_NAME, recordFinal);
                	tuple.rss = new SocialRssDTO(tuple.record);
                	tuple.rssNodeVO = createOrUpdateSocialCompNode(tuple.rss , NodeType.RSS, userName);
                	return tuple;
                });
            	record = tupleResult.record;
            	rss = tupleResult.rss;
            	rssNodeVO = tupleResult.rssNodeVO;
            
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                
                throw new Exception("Error while Inserting / Updating " + rss.getU_display_name());
            }
            
            if (isInsert)
            {
                // Add current user to the process
                //addUserToSocialComp(rss, NodeType.RSS, userName);
                NotificationHelper.getRssNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.RSS_CREATE, userName, true, true);
            }
            else
            {
                NotificationHelper.getRssNotification((String)record.get("sys_id"), UserGlobalNotificationContainerType.RSS_UPDATE, userName, true, true);
            }
            
            addUserToSocialComp(rssNodeVO, NodeType.RSS, userName);
            
            //there may be a record in cron table for this rss. Delete it. It will get created again when a User is following that RSS and system loads it
            CronUtil.deleteCronByName("RSS_" + rss.getU_display_name().toUpperCase(), "system");
            
            checkIsEditable(rss, userName);

            rss.setSys_created_on(GMTDate.getLocalServerDate(rss.getSys_created_on()));
            rss.setSys_updated_on(GMTDate.getLocalServerDate(rss.getSys_updated_on()));
        }
        
        UsersVO user = ServiceHibernate.getUser(rss.getU_rss_owner());
        if (user != null)
        {
            rss.setU_user_display_name(ServiceHibernate.getUser(rss.getU_rss_owner()).getUDisplayName());
        }
        return rss;
    }

    @SuppressWarnings("rawtypes")
    public static void updateFeedUpdatedDateForRss(SocialRssDTO vo, String username)
    {
        if (vo != null && StringUtils.isNotBlank(username))
        {
            String sysId = vo.getSys_id();
            String updateSql = "update " + SocialRssDTO.TABLE_NAME + " set u_feed_updated = :u_feed_updated where sys_id = :sys_id";
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

                Query query = HibernateUtil.createQuery(updateSql);
                query.setParameter("u_feed_updated", GMTDate.getDate(), TimestampType.INSTANCE);
                query.setParameter("sys_id", sysId);

                query.executeUpdate();

                });

            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

//    public static Rss createSocialRssAndAddUser(SocialRssDTO rss, User socialUser) throws Exception
//    {
//        Rss socialRss = SocialCompConversionUtil.convertSocialRssDTOToRss(rss, null);
//        //Create new RSS. 
//        ServiceSocial.insertOrUpdateRSComponent(socialRss);
//
//        //Subscribe RSS to current user
//        com.resolve.graph.social.util.legacy.SocialUtil.subscribeRssToUser(socialUser, socialRss);
//
//        return socialRss;
//    }

    public static void deleteRss(String[] rssSysIds, String username) throws Exception
    {
        if (rssSysIds != null && StringUtils.isNotBlank(username))
        {
            UsersVO user = ServiceHibernate.getUser(username);
            for (String sysId : rssSysIds)
            {
                // Send notification
                NotificationHelper.getRssNotification(sysId, UserGlobalNotificationContainerType.RSS_PURGED, username, true, false);
                
                deleteSocialCompNode(sysId, NodeType.RSS, user);
            }
        }
    }
    
    public static void setStreamLockUnlock(User user, String id, boolean locked) throws Exception
    {
        String username = user.getName();
        ResolveNodeVO node = ServiceGraph.findNode(null, id, null, null, username);
        if(node != null)
        {
            //get user roles
            Set<String> userRoles = UserUtils.getUserRoles(username);
            boolean isUserAdmin = userRoles.contains("admin") || username.equalsIgnoreCase("system") || username.equalsIgnoreCase("resolve.maint");
            
            String compRoles = null;
            NodeType type = node.getUType();
            
            SocialDTO socialObject = null;
            if(type == NodeType.PROCESS) 
            {
                socialObject = getProcess(node.getUCompSysId(), username);
            }
            else if(type == NodeType.TEAM)
            {
                socialObject = getTeam(node.getUCompSysId(), username);
            }
            else if(type == NodeType.RSS)
            {
                socialObject = getRss(node.getUCompSysId(), username);
            }
            else if(type == NodeType.FORUM)
            {
                socialObject = getForum(node.getUCompSysId(), username);
            }
            else
            {
                //this can be user, worksheet, at, wiki
                if(type == NodeType.USER)
                {
                    if(!isUserAdmin)
                    {
                        boolean isUserSelf = node.getUCompName().equalsIgnoreCase(username);
                        if(!isUserSelf)
                        {
                            throw new Exception("User does not have rights to lock other Users stream");
                        }
                    }
                }
                else if(type == NodeType.ACTIONTASK)
                {
                    compRoles = node.getUEditRoles();
                    compRoles = SocialUtil.convertSocialRolesStringToRoles(compRoles);
                    if (!isUserAdmin)
                    {
                        //check if the user has rights to edit this component
                        boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(username, userRoles, compRoles);
                        if (!userCanEdit)
                        {
                            throw new Exception("User does not have rights for Actiontask " + node.getUCompName());
                        }
                    }
                    
                }
                else if(type == NodeType.DOCUMENT)
                {
                    compRoles = node.getUEditRoles();
                    compRoles = SocialUtil.convertSocialRolesStringToRoles(compRoles);
                    if (!isUserAdmin)
                    {
                        //check if the user has rights to edit this component
                        boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(username, userRoles, compRoles);
                        if (!userCanEdit)
                        {
                            throw new Exception("User does not have rights for Document " + node.getUCompName());
                        }
                    }
                }
                else if(type == NodeType.WORKSHEET)
                {
                    if(!isUserAdmin)
                    {
                        throw new Exception("User does not have rights to lock Worksheet " + node.getUCompName());
                    }
                }

                //update the value in graph db
                node.setULock(locked);
                ServiceGraph.persistNode(node, username);
                
            }
            
            //for social components only
            if(socialObject != null)
            {
                //this is a social comp
                compRoles = socialObject.getU_edit_roles();
                
                if (!isUserAdmin)
                {
                    //check if the user has rights to edit this component
                    boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(username, userRoles, compRoles);
                    if (!userCanEdit)
                    {
                        //check if the user is the owner of this comp
                        if (username.equalsIgnoreCase(socialObject.getU_owner()))
                        {
                            userCanEdit = true;
                        }
                    }
                    if (!userCanEdit)
                    {
                        throw new Exception("User does not have rights for " + type + " with name " + socialObject.getU_display_name());
                    }
                }
                //update the row
                socialObject.setU_is_locked(locked);
                if(type == NodeType.PROCESS) 
                {
                    saveProcess((SocialProcessDTO) socialObject, username, true);
                }
                else if(type == NodeType.TEAM)
                {
                    saveTeam((SocialTeamDTO) socialObject, username, true); 
                }
                else if(type == NodeType.RSS)
                {
                    saveRss((SocialRssDTO) socialObject, username, true); 
                }
                else if(type == NodeType.FORUM)
                {
                    saveForum((SocialForumDTO) socialObject, username, true); 
                }
            }//end of if
            
            //lock targets in the post, 1000 is good enough as the size of the posts
            QueryDTO query = new QueryDTO(0, 1000);
            ResponseDTO<SocialPostResponse> socialPosts = ServiceSocial.searchPosts(query, id, type, false, false, user);
            for(SocialPostResponse socialPost : socialPosts.getRecords())
            {
                for(SocialTarget target : socialPost.getTargets())
                {
                    if(id.equals(target.getSysId()))
                    {
                        SocialPost tmpSocialPost = (SocialPost) socialPost;
                        target.setLocked(locked);
                        tmpSocialPost.getTargets().add(target);
                        SocialIndexAPI.indexSocialPost(tmpSocialPost, username, tmpSocialPost.getType(), false);
                    }
                }
            }            
        }
        else
        {
            throw new Exception("Component with sysId " + id + " does not exist in graph." );
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private apis
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static String evaluateRoles(String rolesFromUi, String defaultRoles)
    {
        Set<String> roles = new HashSet<String>();
        roles.add("admin");

        String result = StringUtils.isNotBlank(rolesFromUi) ? rolesFromUi : defaultRoles;
        String[] rolesArr = result.split(",");

        for (String role : rolesArr)
        {
            if (StringUtils.isNotEmpty(role))
            {
                roles.add(role.trim());
            }
        }

        result = StringUtils.listToString(new ArrayList<String>(roles), ",");

        return result;
    }
    
    public static void deleteSocialCompNode(String compSysId, NodeType nodeType, UsersVO user)
    {
        String tableName = null;
        if (nodeType.equals(NodeType.PROCESS))
        {
            tableName = SocialProcessDTO.TABLE_NAME;
        }
        else if (nodeType.equals(NodeType.TEAM))
        {
            tableName = SocialTeamDTO.TABLE_NAME;
        }
        else if (nodeType.equals(NodeType.FORUM))
        {
            tableName = SocialForumDTO.TABLE_NAME;
        }
        else if (nodeType.equals(NodeType.RSS))
        {
            tableName = SocialRssDTO.TABLE_NAME;
        }
        
        // Delete comp from CT
        String sysOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

        Map<String, Object> whereClauseParams = new HashMap<String, Object>();
        
        //String whereClause = "sys_id = '" + compSysId + "'";
        String whereClause = "sys_id = :sys_id";
        whereClauseParams.put("sys_id", compSysId);
        
        if (StringUtils.isNotBlank(sysOrg))
        {
            //whereClause += " and sys_org = '" + sysOrg + "'";
            whereClause += " and sys_org = :sys_org";
            whereClauseParams.put("sys_org", sysOrg);
        }

        List<Map<String, Object>> qryResult = ServiceHibernate.queryCustomTable(tableName, whereClause, whereClauseParams);
        if (qryResult != null && qryResult.size() > 0)
        {
            Map<String, Object> record = qryResult.get(0);
            SocialProcessDTO result = new SocialProcessDTO(record);

            //check if this user has rights to edit this process, else make the editable to false
            String editRoles = result.getU_edit_roles();
            Set<String> userRoles = UserUtils.getUserRoles(user.getUUserName());
            boolean hasEditRights = com.resolve.services.util.UserUtils.hasRole(user.getUUserName(), userRoles, editRoles);
            boolean isOwner = StringUtils.isNotBlank(result.getU_owner()) ? result.getU_owner().equalsIgnoreCase(user.getUUserName()) : false;
            if (hasEditRights || isOwner)
            {
                //delete it
                ServiceHibernate.deleteDBRow(user.getUUserName(), tableName, compSysId, true);
                
                // Delete Nodes and Edges
                try
                {
                    ServiceGraph.deleteNode(null, compSysId, null, nodeType, user.getUUserName());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
                
                if(nodeType.name().equals("TEAM"))
                {
                    UserUtils.updateGroupLinkToTeamFlag(result.getU_display_name(), false, user.getUUserName());
                }
                else if (nodeType.name().equals("RSS"))
                {
                    //delete the cron job related to this RSS as RSCONTROL may or may not be up and running
                    CronUtil.deleteCronByName("RSS_" + result.getU_display_name().toUpperCase(), "system");
                    
                    //remove the rss feed - the cron job
                    SocialUtil.removeRssCronJob(compSysId, result.getU_display_name());
                }
            }
            else
            {
                //user has no rights to delete this process
                Log.log.error("User '" + user.getUUserName() + "' has no rights to delete " + nodeType.name() + " with sys_id '" + compSysId + "'.");
            }
        }
        else
        {
            Log.log.error("No " + nodeType.name() + " with sys_id '" + compSysId + "' exist.");
        }
    }

    public static SocialDTO getSocialDTO(String type, String sysId, String name, String username)
    {
        SocialDTO dto = null;

        if ((StringUtils.isNotBlank(sysId) || StringUtils.isNotBlank(name)) && StringUtils.isNotBlank(type) && StringUtils.isNotBlank(username))
        {
            UsersVO user = ServiceHibernate.getUser(username);
            String orgSysId = null;
            if(user == null)
            {
                Log.log.debug("user record not found in the database : " + username);
            }
            else
            {
                if(user.getBelongsToOrganization() != null)
                {
                    orgSysId = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;
                }
            }

            String whereClause = "";
            
            Map<String, Object> whereClauseParams = new HashMap<String, Object>();
            
            if (StringUtils.isNotBlank(sysId))
            {
                //whereClause = "sys_id = '" + sysId.trim() + "'";
                whereClause = "sys_id = :sys_id";
                whereClauseParams.put("sys_id", sysId.trim());
            }
            else if (StringUtils.isNotBlank(name))
            {
                //whereClause = "u_display_name = '" + name + "'";
                whereClause = "u_display_name = :u_display_name";
                whereClauseParams.put("u_display_name", name);
            }

            if (StringUtils.isNotBlank(orgSysId))
            {
                //if the sys_org is empty/null, its global
                //whereClause += " and (sys_org is null or sys_org = '" + orgSysId + "')";
                whereClause += " and (sys_org is null or sys_org = :sys_org)";
                whereClauseParams.put("sys_org", orgSysId);
            }

            String tableName = null;
            if (type.equalsIgnoreCase(AdvanceTree.PROCESS))
            {
                tableName = SocialProcessDTO.TABLE_NAME;
            }
            else if (type.equalsIgnoreCase(AdvanceTree.TEAMS))
            {
                tableName = SocialTeamDTO.TABLE_NAME;
            }
            else if (type.equalsIgnoreCase(AdvanceTree.FORUMS))
            {
                tableName = SocialForumDTO.TABLE_NAME;
            }
            else if (type.equalsIgnoreCase(AdvanceTree.RSS))
            {
                tableName = SocialRssDTO.TABLE_NAME;
            }

            //get the data
            if (StringUtils.isNotBlank(tableName))
            {
                String userDisplayName = null; 
                List<Map<String, Object>> qryResult = ServiceHibernate.queryCustomTable(tableName, whereClause, whereClauseParams);
                if (qryResult != null && qryResult.size() > 0)
                {
                    Map<String, Object> record = qryResult.get(0);
                    boolean isRSSOwner = false;
                    if (type.equalsIgnoreCase(AdvanceTree.PROCESS))
                    {
                        dto = new SocialProcessDTO(record);
                        userDisplayName = getUserDisplayName(dto.getU_owner());
                    }
                    else if (type.equalsIgnoreCase(AdvanceTree.TEAMS))
                    {
                        dto = new SocialTeamDTO(record);
                        userDisplayName = getUserDisplayName(dto.getU_owner());
                    }
                    else if (type.equalsIgnoreCase(AdvanceTree.FORUMS))
                    {
                        dto = new SocialForumDTO(record);
                        userDisplayName = getUserDisplayName(dto.getU_owner());
                    }
                    else if (type.equalsIgnoreCase(AdvanceTree.RSS))
                    {
                        dto = new SocialRssDTO(record);
                        isRSSOwner = StringUtils.isNotBlank(((SocialRssDTO) dto).getU_rss_owner()) ? ((SocialRssDTO) dto).getU_rss_owner().equalsIgnoreCase(username) : false;
                        userDisplayName = getUserDisplayName(((SocialRssDTO) dto).getU_rss_owner());
                    }
                    
                    dto.setU_user_display_name(userDisplayName);

                    //check if this user has rights to edit this process, else make the editable to false
                    String editRoles = dto.getU_edit_roles();
                    Set<String> userRoles = UserUtils.getUserRoles(username);
                    boolean hasEditRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, editRoles);
                    boolean isOwner = StringUtils.isNotBlank(dto.getU_owner()) ? dto.getU_owner().equalsIgnoreCase(username) : false;
                    if (hasEditRights || isOwner || isRSSOwner)
                    {
                        dto.setU_editable(true);
                    }

                    dto.setSys_created_on(GMTDate.getLocalServerDate(dto.getSys_created_on()));
                    dto.setSys_updated_on(GMTDate.getLocalServerDate(dto.getSys_updated_on()));
                }
            }
        }
        return dto;
    }
    
    private static String getUserDisplayName (String userName)
    {
        String displayName = userName;
        UsersVO userVO = ServiceHibernate.getUser(userName);
        if (userVO != null)
        {
            displayName = userVO.getUDisplayName();
        }
        
        return displayName;
    }

    private static void checkComponentForChange(SocialDTO oldComp, SocialDTO newComp)
    {
        oldComp.getU_im_pwd();
        if (!(StringUtils.isBlank(oldComp.getU_receive_from())? "" : oldComp.getU_receive_from()).trim()
        .equalsIgnoreCase((StringUtils.isBlank(newComp.getU_receive_from())? "" : newComp.getU_receive_from())))
        {
            GatewayUtil.synchronizeEmailGateway();
        }

        else if (!(StringUtils.isBlank(oldComp.getU_receive_pwd())? "" : oldComp.getU_receive_pwd()).trim()
        .equalsIgnoreCase((StringUtils.isBlank(newComp.getU_receive_pwd())? "" : newComp.getU_receive_pwd())))
        {
            {
                GatewayUtil.synchronizeEmailGateway();
            }
        }
        
        if (!(StringUtils.isBlank(oldComp.getU_im())? "" : oldComp.getU_im()).trim()
        .equalsIgnoreCase((StringUtils.isBlank(newComp.getU_im())? "" : newComp.getU_im())))
        {
            GatewayUtil.synchronizeXMPPGateway();
        }

        else if (!(StringUtils.isBlank(oldComp.getU_im_pwd())? "" : oldComp.getU_im_pwd()).trim()
        .equalsIgnoreCase((StringUtils.isBlank(newComp.getU_im_pwd())? "" : newComp.getU_im_pwd())))
        {
            {
                GatewayUtil.synchronizeXMPPGateway();
            }
        }
    }
}
