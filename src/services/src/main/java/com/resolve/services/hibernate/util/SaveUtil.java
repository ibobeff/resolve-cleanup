/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import com.resolve.persistence.dao.MetaFormViewDAO;
import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.AuditLog;
import com.resolve.persistence.model.CatalogAttachment;
import com.resolve.persistence.model.ConfigActiveDirectory;
import com.resolve.persistence.model.ConfigLDAP;
import com.resolve.persistence.model.ConfigRADIUS;
import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.MetaViewLookup;
import com.resolve.persistence.model.MetricThreshold;
import com.resolve.persistence.model.Organization;
import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.model.PlaybookActivities;
import com.resolve.persistence.model.Properties;
import com.resolve.persistence.model.RBGeneral;
import com.resolve.persistence.model.ResolveApps;
import com.resolve.persistence.model.ResolveArchive;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveAssessRel;
import com.resolve.persistence.model.ResolveBusinessRule;
import com.resolve.persistence.model.ResolveCron;
import com.resolve.persistence.model.ResolveEvent;
import com.resolve.persistence.model.ResolveImpexGlide;
import com.resolve.persistence.model.ResolveImpexModule;
import com.resolve.persistence.model.ResolveImpexWiki;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolveParserRel;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.model.ResolvePreprocessRel;
import com.resolve.persistence.model.ResolveProperties;
import com.resolve.persistence.model.ResolveRegistration;
import com.resolve.persistence.model.ResolveSession;
import com.resolve.persistence.model.ResolveSysScript;
import com.resolve.persistence.model.ResolveWikiLookup;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.SIRActivityMetaData;
import com.resolve.persistence.model.SIRPhaseMetaData;
import com.resolve.persistence.model.SocialPostAttachment;
import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysPerspective;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.model.WikiArchive;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikiDocumentMetaFormRel;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.model.WikidocStatistics;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.rb.util.ResolutionBuilderUtils;
import com.resolve.services.sir.util.ActivityUtils;
import com.resolve.services.sir.util.PhaseUtils;
import com.resolve.services.util.ParseUtil;
import com.resolve.util.CryptUtils;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


/**
 * this is just to persist the models. Try to avoid the VOs in this class
 * 
 * @author jeet.marwah
 *
 */
public class SaveUtil
{
    public static WikiDocument saveWikiDocument(WikiDocument model, String userNameIn) throws Exception
    {
        List<WikiDocument> wikiDocList = new ArrayList<>();
        Map<String, Integer> currVerWikisRefedWithCnt = new HashMap<>();
        List<String> resolutionBuilderIdList = new ArrayList<>();
        List<Boolean> newPlaybookVersionList = Arrays.asList(false);
        try
        {
            HibernateUtil.setCurrentUserByUsername(userNameIn);
            wikiDocList.add((WikiDocument) HibernateProxy.execute(() -> {
        		WikiDocument modelFinal = model;
        		String username = userNameIn;
        		if(StringUtils.isEmpty(username))
                {
                    username = "system";
                }

        		AccessRights ar = model.getAccessRights();
	            WikiDocumentDAO wikiDao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	
	            //encrypt wiki parameters before saving to the database
	            modelFinal.setUWikiParameters(encryptWikiParameters(modelFinal.getUWikiParameters()));
	            WikiDocument updatedDoc = null;
	            if (StringUtils.isNotBlank(modelFinal.getSys_id()))
	            {
	                //to make sure that created fields are there
	                if(StringUtils.isBlank(modelFinal.getSysCreatedBy()))
	                {
	                    modelFinal.setSysCreatedBy(modelFinal.getSysUpdatedBy());
	                }
	                if(modelFinal.getSysCreatedOn() == null)
	                {
	                    modelFinal.setSysCreatedOn(modelFinal.getSysUpdatedOn());
	                }
	                
	                updatedDoc = wikiDao.update(modelFinal);
	                    
	                ar.setUResourceName(updatedDoc.getUFullname());
	                HibernateUtil.getDAOFactory().getAccessRightsDAO().update(ar);
	            }
	            else
	            {
	                //this will be insert - so see if there is a sys_id with the import
	                if (StringUtils.isNotBlank(modelFinal.getUImpexSysId()))
	                {
	                    //use this sysid
	                    modelFinal.setSys_id(modelFinal.getUImpexSysId());
	                    wikiDao.replicate(modelFinal);
	                }
	                else
	                {
	                    modelFinal.setSys_id(null);
	                    wikiDao.persist(modelFinal);
	                }
	                ar.setSys_id(null);
	                ar.setUResourceId(modelFinal.getSys_id());
	                ar.setUResourceName(modelFinal.getUFullname());
	                ar.setUResourceType(WikiDocument.RESOURCE_TYPE);
	                HibernateUtil.getDAOFactory().getAccessRightsDAO().persist(ar);
	                
	                // Make sure it's not import operation.
	                if (StringUtils.isBlank(modelFinal.getUImpexSysId()))
	                {
		                /*
		                 * if oldResolutionBuilderId is not blank and it's an insert operation,
		                 * UI is calling a 'Save As...' operation from Wiki Automation model.
		                 */
	                    resolutionBuilderIdList.add(modelFinal.getUResolutionBuilderId());
		                modelFinal.setUResolutionBuilderId(null);
	                }
	                modelFinal.setAccessRights(ar);
	                updatedDoc = wikiDao.update(modelFinal);
	            }
	            
	            Log.log.debug("Updated/Inserted Document : " + updatedDoc + " with hash code = " + updatedDoc.hashCode());
	            
	            modelFinal = updatedDoc;
	            
	            if (StringUtils.isNotBlank(modelFinal.getUDisplayMode()) &&
	                modelFinal.getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK) &&
	                StringUtils.isNotBlank(modelFinal.getUContent()) &&
	                ((modelFinal.ugetPbActivities() == null || modelFinal.ugetPbActivities().isEmpty()) ||
	                		 (!modelFinal.ugetPbActivities().iterator().next().getUVersion().equals(modelFinal.getUVersion()))
	                ))
	            {
	                newPlaybookVersionList.set(0, true);
	                List<Map<String, String>> activityMaps = ParseUtil.parseListOfActivityMaps(modelFinal.getUContent());
	                
	                JSONArray pbaJSONArray = new JSONArray();
	                
	                for (Map<String, String> activityMap : activityMaps)
	                {
	                    if (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY) && 
	                        StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)) &&
	                        activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY) && 
	                        StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)))
	                    {
	                        // Generate AltActivityId and add it in JSON for new activities only
	                        
	                        Log.log.debug(StringUtils.mapToLogString(activityMap));
	                        
	                        if (!activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY) ||
	                            StringUtils.isBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY)) || 
	                            StringUtils.isNotBlank(modelFinal.getUImpexSysId()))
	                        {
	                        	String altActivityId = null;
	                        	if (StringUtils.isNotBlank(modelFinal.getUImpexSysId()))
	                        	{
	                        		altActivityId = activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY);
	                        	}
	                        	else
	                        	{
	                        		altActivityId = Guid.getGuid();
	                        	}
	                        	
	                            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY, altActivityId);
	                            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_TEMPLATE_ACTIVITY_KEY, Boolean.toString(true));
	                        }
	                    }
	                    
	                    /* For EXISTING or new activity check if Activity is Required. If the Actvity is Required flag is not found
	                     * in the activities map then add it and set it to true i.e. all activities will be considered as
	                     * Required unless specifically set as one.
	                     */
	                    
	                    if (!activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY))
	                    {
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY, Boolean.toString(true));
	                    }
	                    
	                    if (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY) &&
	                        StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)))
	                    {
	                        /*
	                         * Create Phase MetaData (for Resolve 6.2 i.e. SIR 2.0 and above only).
	                         * Please note Phase Meta Data is not updatable and is unique per Org.
	                         * 
	                         * Unique Phase Meta Data Key
	                         * 
	                         * Org + Name
	                         */
	                        SIRPhaseMetaData sirPhaseMetaData = 
	                                            PhaseUtils.createSIRPhaseMetaData(
	                                                activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY), 
	                                                                modelFinal.getSysOrg(), username);
	                        
	                        if (sirPhaseMetaData == null)
	                        {
	                            throw new Exception("Failed to create SIR phase meta data.");
	                        }
	                        
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY, 
	                                        sirPhaseMetaData.getSys_id());
	                    }
	                    
	                    
	                    if (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY) &&
	                        StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)))
	                    {                        
	                        /*
	                         * Create Activity MetaData (for Resolve 6.2 i.e. SIR 2.0 and above only)
	                         * Please note Activity Meta Data is not updatable and is unique per Org.
	                         * 
	                         * Unique Activity Meta Data Keys
	                         * 
	                         * Org + Name + Referenced Wiki Id + is Required + SLA Duration
	                         * Org + Name (unique)
	                         * 
	                         * Please check SIRActivityMetaData legacyName comments.
	                         */
	                        
	                        Duration sla = SIRActivityMetaData.DEFAULT_SLA_DURATION;
	                        
	                        if (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY) &&
	                            StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY)))
	                        {
	                            try
	                            {
	                                Long slaDays = Long.parseLong(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY), 10);
	                                
	                                if ((Long.MAX_VALUE -  TimeUnit.NANOSECONDS.convert(slaDays, TimeUnit.DAYS)) > 0)
	                                {
	                                    sla = sla.plusDays(slaDays);
	                                }
	                                else
	                                {
	                                    sla = sla.plusDays(Long.MAX_VALUE / TimeUnit.NANOSECONDS.convert(1, TimeUnit.DAYS));
	                                }
	                            }
	                            catch (NumberFormatException nfe)
	                            {
	                                Log.log.warn("Error " + nfe.getLocalizedMessage() + " occurred while converting specified SLA " +
	                                             WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY + " value " + 
	                                             activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY).toString() +
	                                             " to " + Long.class.getName() + ".", nfe);
	                            }
	                        }
	                        
	                        if (sla.toNanos() < Long.MAX_VALUE && activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY) &&
	                            StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY)))
	                        {
	                            try
	                            {
	                                Long slaHours = Long.parseLong(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY), 10);
	                                
	                                if ((Long.MAX_VALUE - sla.toNanos()) > TimeUnit.NANOSECONDS.convert(slaHours, TimeUnit.HOURS))
	                                {
	                                    sla = sla.plusHours(slaHours);
	                                }
	                                else
	                                {
	                                    sla = sla.plusHours((Long.MAX_VALUE - sla.toNanos()) / TimeUnit.NANOSECONDS.convert(1, TimeUnit.HOURS));
	                                }
	                            }
	                            catch (NumberFormatException nfe)
	                            {
	                                Log.log.warn("Error " + nfe.getLocalizedMessage() + " occurred while converting specified SLA " +
	                                             WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY + " value " + 
	                                             activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY).toString() +
	                                             " to " + Long.class.getName() + ".", nfe);
	                            }
	                        }
	                        
	                        if (sla.toNanos() < Long.MAX_VALUE && activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY) &&
	                            StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY)))
	                        {
	                            try
	                            {
	                                Long slaMinutes = Long.parseLong(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY), 10);
	                                
	                                if ((Long.MAX_VALUE - sla.toNanos()) > TimeUnit.NANOSECONDS.convert(slaMinutes, TimeUnit.MINUTES))
	                                {
	                                    sla = sla.plusMinutes(slaMinutes);
	                                }
	                                else
	                                {
	                                    sla = sla.plusMinutes((Long.MAX_VALUE - sla.toNanos()) / TimeUnit.NANOSECONDS.convert(1, TimeUnit.MINUTES));
	                                }                                
	                            }
	                            catch (NumberFormatException nfe)
	                            {
	                                Log.log.warn("Error " + nfe.getLocalizedMessage() + " occurred while converting specified SLA " +
	                                             WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY + " value " + sla.toString() +
	                                             " to " + Long.class.getName() + ".", nfe);
	                            }
	                        }
	                        
	                        /*
	                         * DO NOT REMOVE : SLA granularity is upto minutes only.
	                         * SLA is persisted in DB with default granularity which 
	                         * is nano seconds. Code below normalizes SLA to minutes
	                         * granularity.
	                         */
	                        
	                        if (!sla.isZero())
	                        {
	                            sla = Duration.ofMinutes(sla.toMinutes());
	                        }
	                        
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY, Long.toString(sla.toDays()));
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY, Long.toString(sla.toHours() - (TimeUnit.HOURS.convert(sla.toDays(), TimeUnit.DAYS))));
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY, Long.toString(sla.toMinutes() - (TimeUnit.MINUTES.convert(sla.toHours(), TimeUnit.HOURS))));
	                        
	                        Log.log.debug("saveWikiDocument SLA: " + sla + ", Activity Map Values: " +
	                                        activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY) + "D" + 
	                                        activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY) + "H" + 
	                                        activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY) + "M");
	                        
	                        
	                        SIRActivityMetaData sirActivityMetaData = 
	                                        ActivityUtils.createSIRActivityMetaData(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
	                                                                                activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_DESCRIPTION_KEY), 
	                                                                                Boolean.valueOf(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY)).booleanValue(), 
	                                                                                activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY), null, 
	                                                                                Boolean.valueOf(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_TEMPLATE_ACTIVITY_KEY)).booleanValue(), 
	                                                                                sla, activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY),
	                                                                                modelFinal.getSysOrg(), username);
	                        
	                        if (sirActivityMetaData == null)
	                        {
	                            throw new Exception("Failed to create SIR activity meta data.");
	                        }
	                        
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY, 
	                                        sirActivityMetaData.getName());
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY, 
	                                        sirActivityMetaData.getAltActivityId());
	                        activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY,
	                                        sirActivityMetaData.getSys_id());
	                    }
	                    
	                    JSONObject pbaJSONObj = new JSONObject();
	                    pbaJSONObj.putAll(activityMap);
	                    
	                    pbaJSONArray.add(pbaJSONObj);
	                }
	                
	                //Log.log.debug(StringUtils.mapToLogString(addedActivityWikiRefs));
	                
	                PlaybookActivities pbActivities = new PlaybookActivities();
	                
	                pbActivities.setSysCreatedBy(username);
	                pbActivities.setSysCreatedOn(modelFinal.getSysUpdatedOn());
	                pbActivities.setSysUpdatedOn(modelFinal.getSysUpdatedOn());
	                pbActivities.setSysModCount(new Integer(0));
	                pbActivities.setSysOrg(modelFinal.getSysOrg());
	                
	                pbActivities.setUFullname(modelFinal.getUFullname());
	                pbActivities.setUVersion(modelFinal.getUVersion());
	                
	                pbActivities.setUActivitiesJSON(StringUtils.jsonArrayToString(pbaJSONArray));
	                
	                currVerWikisRefedWithCnt.putAll(PlaybookActivityUtils.getReferencedWikiNamesWithCounts(pbaJSONArray));
	                
	                // Update procedure section in content to include newly added fields such as altActivityId, templateActivity etc.
	                
	                modelFinal.setUContent(ParseUtil.replaceProcedureSection(modelFinal.getUContent(), pbActivities.getUActivitiesJSON()));
	                
	                PlaybookActivities nPBActivities = HibernateUtil.getDAOFactory().getPlaybookActivitiesDAO().persist(pbActivities);
	                
	                Set<PlaybookActivities> pbActivitiesSet = new HashSet<PlaybookActivities>();
	                pbActivitiesSet.add(nPBActivities);
	                modelFinal.usetPbActivities(pbActivitiesSet);
	            }
	            
	            if (Log.log.isDebugEnabled() && StringUtils.isNotBlank(modelFinal.getUDisplayMode()) &&
	                modelFinal.getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK) &&
	                StringUtils.isNotBlank(modelFinal.getUContent()))
	            {
	                Log.log.debug("Playbook template " + modelFinal.getUFullname() + " has new version!");
	            }
	            
	            HibernateUtil.getCurrentSession().flush();
	            return modelFinal;
            }));
	           
	            
            String resolutionBldrId = wikiDocList.get(0).getUResolutionBuilderId();
            /*
             * If it's an import operation, RBGeneral record will be imported after Wiki save operation,
             * so, skip this step if the save is called from Import.
             */
            if(StringUtils.isBlank(wikiDocList.get(0).getUImpexSysId()) && StringUtils.isBlank(resolutionBldrId))
            {
                List<RBGeneral> generalEntityList = new ArrayList<>();              
                HibernateProxy.execute(() -> {
                    RBGeneral generalEntity = new RBGeneral();
                    generalEntity.setName(wikiDocList.get(0).getUName());
                    generalEntity.setNamespace(wikiDocList.get(0).getUNamespace());
                    //now create general with wiki id
                    RBGeneral tempRbGeneral = HibernateUtil.getDAOFactory().getRBGeneralDAO().findFirst(generalEntity, new String[1]);
                    if (tempRbGeneral == null)
                    {
                        generalEntity.setWikiId(wikiDocList.get(0).getSys_id());
                        generalEntity = HibernateUtil.getDAOFactory().getRBGeneralDAO().persist(generalEntity);
                    }
                    else
                    {
                        generalEntity = tempRbGeneral;
                    }
                    generalEntityList.add(generalEntity);
                    wikiDocList.get(0).setUResolutionBuilderId(generalEntity.getSys_id());
                    HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(wikiDocList.get(0));
                });        
            }
            
            /*
             * oldResolutionBuilderId will be non-null only if it's an insert Wiki operation,
             * that means, UI is invoking 'Save As...' operation from Wiki Automation. We had to tweak
             * Wiki Save operation a bit cause from UI it was tad difficult to clear all
             * Automation builder meta data hierarchy.
             */
            if (CollectionUtils.isNotEmpty(resolutionBuilderIdList) && StringUtils.isNotEmpty(resolutionBuilderIdList.get(0)))
            {
            	RBGeneralVO rbGeneralVO = ResolutionBuilderUtils.copy(resolutionBuilderIdList.get(0), wikiDocList.get(0).getSys_id(), wikiDocList.get(0).getUName(), wikiDocList.get(0).getUNamespace(), userNameIn);
            	
            	if (rbGeneralVO != null && StringUtils.isNotBlank(rbGeneralVO.getSys_id()))
            	{
            	    wikiDocList.get(0).setUResolutionBuilderId(rbGeneralVO.getSys_id());
            	}
            }
            
            /*
             *  SIR reference count of all activities in new version is incremented by 1. 
             *  Reference count of deleted activities is decremented by 1.  
             */
            
            if (StringUtils.isNotBlank(wikiDocList.get(0).getUDisplayMode()) &&
                            wikiDocList.get(0).getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK) &&
                StringUtils.isNotBlank(wikiDocList.get(0).getUContent()) && newPlaybookVersionList.get(0))
            {
                // Increment SIR reference count of wikis referenced in newly added activities
                // Increment SIR reference count of wikis referenced
                List<Map<String, String>> activityMaps = ParseUtil.parseListOfActivityMaps(wikiDocList.get(0).getUContent());
                
                for (Map<String, String> activityMap : activityMaps)
                {
                    if (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY) && 
                        StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)) /*&&
                        addedActivityWikiRefs.containsKey(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY))*/)
                    {
                        WikiDocumentVO activityDoc = WikiUtils.getWikiDoc(null, activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY), "system");
                        
                        if (activityDoc != null)
                        {
                            activityDoc.setUSIRRefCount(activityDoc.getUSIRRefCount().longValue() + 1l/*addedActivityWikiRefs.get(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)).intValue()*/);
                            //addedActivityWikiRefs.remove(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY));
                            
                            // No possibility of recursion as playbook cannot reference itself or any other playbook
                            try
                            { 
                                HibernateProxy.execute(() -> {
                                    WikiDocument activityWikiDoc = new WikiDocument();
                                    activityWikiDoc.applyVOToModel(activityDoc);
                                    HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(activityWikiDoc);
                                    HibernateUtil.getCurrentSession().flush();   
                                });
                            }
                            catch(Throwable e)
                            {
                                Log.log.warn("Failed to increment SIR reference count of the wiki document " + 
                                             (activityDoc != null ? activityDoc.getUFullname() : "null") + " by 1" +
                                             /*addedActivityWikiRefs.get(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)) + */
                                             ". " + e.getMessage(), e);	    
                                
                            }
                        }
                    }
                }
                
                if (wikiDocList.get(0).getUVersion().intValue() > 1 && MapUtils.isNotEmpty(currVerWikisRefedWithCnt))
                {
                    // Handle decrementing SIR reference count of wikis referenced in deleted activities
                    
                    Map<String, Integer> prevVerWikisRefedWithCnt =
                        PlaybookActivityUtils.getReferencedWikiNamesWithCounts(
                                        wikiDocList.get(0).getUFullname(),
                        												new Integer(wikiDocList.get(0).getUVersion().intValue() - 1),
                        												userNameIn);
                    
                    if (prevVerWikisRefedWithCnt != null && !prevVerWikisRefedWithCnt.isEmpty())
                    {
                        Map<String, Integer> substractWikiRefedWithCnt = new HashMap<String, Integer>();
                        
                        for (String prevWikiRefed : prevVerWikisRefedWithCnt.keySet())
                        {
                            Integer currActivityCnt = currVerWikisRefedWithCnt.get(prevWikiRefed);
                            
                            if (currActivityCnt == null)
                            {
                                currActivityCnt = new Integer(0);
                            }
                            
                            if (currActivityCnt.intValue() < prevVerWikisRefedWithCnt.get(prevWikiRefed).intValue())
                            {
                                substractWikiRefedWithCnt.put(prevWikiRefed, new Integer(currActivityCnt.intValue() - prevVerWikisRefedWithCnt.get(prevWikiRefed).intValue()));
                            }
                        }
                        
                        if (!substractWikiRefedWithCnt.isEmpty())
                        {
                            for (String wikiFullNameToAdjustSIRRefCnt : substractWikiRefedWithCnt.keySet())
                            {
                                WikiDocumentVO activityDoc = WikiUtils.getWikiDoc(null, wikiFullNameToAdjustSIRRefCnt, "system");
                                
                                if (activityDoc != null)
                                {
                                    activityDoc.setUSIRRefCount(activityDoc.getUSIRRefCount().longValue() + substractWikiRefedWithCnt.get(wikiFullNameToAdjustSIRRefCnt).intValue());
                                    
                                    // No possibility of recursion as playbook cannot reference itself or any other playbook
                                    try
                                    {
                                        HibernateProxy.execute(() -> {
                                            WikiDocument activityWikiDoc = new WikiDocument();
                                            activityWikiDoc.applyVOToModel(activityDoc);
                                            HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(activityWikiDoc);
                                            HibernateUtil.getCurrentSession().flush();
                                        });
                                    }
                                    catch(Throwable e)
                                    {
                                        Log.log.warn("Failed to decrement SIR reference count of the wiki document " +
                                                     (activityDoc != null ? activityDoc.getUFullname() : "null") + " by " +
                                                     (substractWikiRefedWithCnt.get(wikiFullNameToAdjustSIRRefCnt).intValue() * -1) +
                                                     ". " + e.getMessage(), e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
	    }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
           
            throw new Exception(e);
        }
        return wikiDocList.get(0);
    }
    
    /**
     * This method encrypts the parameters whose type is Encrypted.
     * 
     * @param uWikiParameters
     * @return
     */
    private static String encryptWikiParameters(final String uWikiParameters)
    {
        String result = uWikiParameters;
        
        if(StringUtils.isNotBlank(uWikiParameters) && !uWikiParameters.equals(VO.STRING_DEFAULT))
        {
            result = uWikiParameters.trim();
            
            try
            {
                JSONArray jsonArr = StringUtils.stringToJSONArray(result);
                if(jsonArr != null && jsonArr.size() > 0)
                {
                    boolean needChange = false;
                    
                    for(Object o : jsonArr)
                    {
                        JSONObject jsonObj = (JSONObject) o;
                        //if it is encrypted and constant, then encypt the value
                        if("encrypted".equalsIgnoreCase(jsonObj.getString("type")) && "constant".equalsIgnoreCase(jsonObj.getString("source")))
                        {
                            jsonObj.put("sourceName", CryptUtils.encrypt(jsonObj.getString("sourceName")));
                            needChange = true;
                        }
                    }
                    
                    if(needChange)
                    {
                        result = StringUtils.jsonArrayToString(jsonArr);
                    }
                }//end of if
            }
            catch (Exception e)
            {
                //not req - if there is an exception - ignore it.
                Log.log.info(e.getMessage());
            }
        }
        
        return result;
    }



    public static void saveResolveSession(ResolveSession model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveSessionDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveSessionDAO().persist(model);
	            }
	            //HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }
    }
    
    public static void saveAuditLog(AuditLog model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getAuditLogDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getAuditLogDAO().persist(model);
	            }
//            HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }
    }
    
    public static ResolveImpexModule saveResolveImpexModule(ResolveImpexModule model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.executeNoCache(() -> {
            	  if (StringUtils.isNotBlank(model.getSys_id()))
                  {
                      HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().update(model);
                  }
                  else
                  {
                      model.setSys_id(null);
                      HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().persist(model);
                  }
            });
            
        }
        catch (Exception ex)
        {
            Log.log.warn(ex.getMessage(), ex);
            throw ex;           
        }
        
        return model;
    }
    
    public static ResolveImpexGlide saveResolveImpexGlide(ResolveImpexGlide model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        /*String sqlToCheckUniqueness = "from ResolveImpexGlide where UType = '" + model.getUType() + "' and UModule = '" + model.getUModule() + "' "
                        + " and UName = " + (StringUtils.isNotBlank(model.getUName()) ? "'" + model.getUName() + "'" : null) 
                        + " and resolveImpexModule.sys_id = '" + model.getResolveImpexModule().getSys_id() + "'";*/
        
        String sqlToCheckUniqueness = "from ResolveImpexGlide where UType = :UType and UModule = :UModule "
                        + " and UName = :UName" 
                        + " and resolveImpexModule.sys_id = :sys_id";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UType", model.getUType());
        queryParams.put("UModule", model.getUModule());
        queryParams.put("UName", model.getUName());
        queryParams.put("sys_id", model.getResolveImpexModule().getSys_id());
        
        try
        {
	            List<? extends Object> items = GeneralHibernateUtil.executeHQLSelect(sqlToCheckUniqueness, queryParams);
	            if(items == null || items.size() == 0)
	            {
	                HibernateUtil.setCurrentUserByUsername(username);
	            	HibernateProxy.execute(() -> {
		                if (StringUtils.isNotBlank(model.getSys_id()))
		                {
		                    HibernateUtil.getDAOFactory().getResolveImpexGlideDAO().update(model);
		                }
		                else
		                {
		                    model.setSys_id(null);
		                    HibernateUtil.getDAOFactory().getResolveImpexGlideDAO().persist(model);
		                }
	            	});
	            }
        }
        catch (Exception t)
        {
            Log.log.warn(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return model;
    }
    
    public static ResolveImpexWiki saveResolveImpexWiki(ResolveImpexWiki model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        /*String sqlToCheckUniqueness = "from ResolveImpexWiki where UType = '" + model.getUType() + "' and UValue = '" + model.getUValue().replaceAll("'", "''") + "' "
                        + " and resolveImpexModule.sys_id = '" + model.getResolveImpexModule().getSys_id() + "'";*/

        String sqlToCheckUniqueness = "from ResolveImpexWiki where UType = :UType and UValue = :UValue "
                        + " and resolveImpexModule.sys_id = :sys_id";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UType", model.getUType());
        queryParams.put("UValue", model.getUValue());
        queryParams.put("sys_id", model.getResolveImpexModule().getSys_id());
        
        try
        {
            List<? extends Object> items = GeneralHibernateUtil.executeHQLSelect(sqlToCheckUniqueness, queryParams);
            if(items == null || items.size() == 0)
            {
                HibernateUtil.setCurrentUserByUsername(username);
                HibernateProxy.execute(() -> {
	                if (StringUtils.isNotBlank(model.getSys_id()))
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexWikiDAO().update(model);
	                }
	                else
	                {
	                    model.setSys_id(null);
	                    HibernateUtil.getDAOFactory().getResolveImpexWikiDAO().persist(model);
	                }
                });
            }
        }
        catch (Exception t)
        {
            Log.log.warn(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return model;
    }
    
    public static Properties saveProperties(Properties model, String username)
    {
        Properties savedProperties = null;
        
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        // persist it
        try
        {
          HibernateProxy.setCurrentUser(username);
        	savedProperties = (Properties) HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                return HibernateUtil.getDAOFactory().getPropertiesDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                return HibernateUtil.getDAOFactory().getPropertiesDAO().persist(model);
	            }
            

            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return /*model*/savedProperties;
    }
    
    public static ResolveArchive saveResolveArchive(ResolveArchive model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveArchiveDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveArchiveDAO().persist(model);
	            }
            
	            //HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static Users saveUser(Users model, String username) throws Exception
    {
        
        
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            return (Users) HibernateProxy.execute(() -> {
            	Users savedModel = null;
            
	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                savedModel = HibernateUtil.getDAOFactory().getUsersDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                savedModel = HibernateUtil.getDAOFactory().getUsersDAO().persist(model);
	            }
	            return savedModel;
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);            
            throw new Exception(" User could not be saved. Please refer to rsview log for more details.");
        }        
    }
    
    public static ResolveProperties saveResolveProperties(ResolveProperties model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolvePropertiesDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolvePropertiesDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }

    public static ResolveRegistration saveResolveRegistration(ResolveRegistration model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveRegistrationDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveRegistrationDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static ResolveEvent saveResolveEvent(ResolveEvent model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveEventDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveEventDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static ResolveCron saveResolveCron(ResolveCron model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveCronDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveCronDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static SysPerspective saveSysPerspective(SysPerspective model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getSysPerspectiveDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getSysPerspectiveDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static ResolveBusinessRule saveResolveBusinessRule(ResolveBusinessRule model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveBusinessRuleDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveBusinessRuleDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static Roles saveRoles(Roles model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getRolesDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getRolesDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }

    public static Groups saveGroup(Groups model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getGroupsDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getGroupsDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist Group " + model.getUName() + ": " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
         
        return model;
    }

    public static ResolveWikiLookup saveResolveWikiLookup(ResolveWikiLookup model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveWikiLookupDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveWikiLookupDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static CatalogAttachment saveCatalogAttachment(CatalogAttachment model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getCatalogAttachmentDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getCatalogAttachmentDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static MetaViewLookup saveMetaViewLookup(MetaViewLookup model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getMetaViewLookupDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getMetaViewLookupDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static MetaFieldProperties saveMetaFieldProperties(MetaFieldProperties model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    

    public static ResolveAssess saveResolveAssess(ResolveAssess model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveAssessDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveAssessDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolveAssess: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static ResolveParser saveResolveParser(ResolveParser model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveParserDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveParserDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolveAssess: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }

    public static ResolvePreprocess saveResolvePreprocess(ResolvePreprocess model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolvePreprocessDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolvePreprocessDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolvePreprocess: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }

    public static MetricThreshold saveMetricThreshold(MetricThreshold model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
	            if (model.getSys_id()==null)
	            {
	                HibernateUtil.getCurrentSession().saveOrUpdate("com.resolve.persistence.MetricThreshold", model);
	            }
	            else
	            {
	                HibernateUtil.getCurrentSession().replicate(model, org.hibernate.ReplicationMode.OVERWRITE);
	            }
            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist MetricThreshold: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static Organization saveOrganization(Organization model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getOrganizationDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getOrganizationDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist Organization: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static ConfigActiveDirectory saveConfigActiveDirectory(ConfigActiveDirectory model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        Organization org = model.getBelongsToOrganization();
        if(StringUtils.isBlank(org.getSys_id()))
        {
            org = null;
        }
        
        model.setBelongsToOrganization(org);
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getConfigActiveDirectoryDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getConfigActiveDirectoryDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ConfigActiveDirectory: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static ConfigLDAP saveConfigLDAP(ConfigLDAP model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        Organization org = model.getBelongsToOrganization();
        if(StringUtils.isBlank(org.getSys_id()))
        {
            org = null;
        }
        
        model.setBelongsToOrganization(org);
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getConfigLDAPDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getConfigLDAPDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ConfigLDAP: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static ResolveSysScript saveResolveSysScript(ResolveSysScript model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveSysScriptDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveSysScriptDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolveSysScript: " + e.getMessage(), e);           
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static WikiDocumentMetaFormRel saveWikiDocumentMetaFormRel(WikiDocumentMetaFormRel model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        AccessRights ar = model.getAccessRights();
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

            if (StringUtils.isNotBlank(model.getSys_id()))
            {
                //access rights
//                AccessRights dbAR = model.getAccessRights();
//                dbAR.setUAdminAccess(ar.getUAdminAccess());
//                dbAR.setUWriteAccess(ar.getUWriteAccess());
//                dbAR.setUReadAccess(ar.getUReadAccess());
//                
                HibernateUtil.getDAOFactory().getAccessRightsDAO().update(ar);

                HibernateUtil.getDAOFactory().getWikiDocumentMetaFormRelDAO().update(model);
                
            }
            else
            {
                model.setSys_id(null);
                HibernateUtil.getDAOFactory().getWikiDocumentMetaFormRelDAO().persist(model);
                
                ar.setUResourceId(model.getSys_id());
                ar.setUResourceName(model.getUName());
                ar.setUResourceType(WikiDocumentMetaFormRel.RESOURCE_TYPE);
                HibernateUtil.getDAOFactory().getAccessRightsDAO().persist(ar);
                
                model.setAccessRights(ar);
                HibernateUtil.getDAOFactory().getWikiDocumentMetaFormRelDAO().persist(model);
                
            }

            //HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist WikiDocumentMetaFormRel: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    
    public static ResolveApps saveResolveApps(ResolveApps model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveAppsDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveAppsDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolveApps: " + e.getMessage(), e);            
            //sometimes it may already at the outest transaction so the above rethrowNestedTransaction may not rethrow the exception
            throw e;
        }
        
        return model;
    }

    public static SocialPostAttachment saveSocialPostAttachment(SocialPostAttachment model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getSocialPostAttachmentDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getSocialPostAttachmentDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist SocialPostAttachment: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
        
    }// saveSocialPostAttachment

    public static SysAppApplication saveSysAppApplication(SysAppApplication model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getSysAppApplicationDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getSysAppApplicationDAO().persist(model);
	                
	                //name has the sysId value in it when it gets created
	                model.setName(model.getSys_id());
	                HibernateUtil.getDAOFactory().getSysAppApplicationDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist SysAppApplication: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
        
    }// saveSocialPostAttachment

    public static WikiArchive saveWikiArchive(WikiArchive model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getWikiArchiveDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getWikiArchiveDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist WikiArchive: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
        
    }// saveSocialPostAttachment
    
    public static MetaFormView saveMetaFormView(MetaFormView model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }

        MetaAccessRights ar = model.getMetaAccessRights();
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            MetaFormViewDAO formDao = HibernateUtil.getDAOFactory().getMetaFormViewDAO();
	            
	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                formDao.update(model);
	                
	                ar.setUResourceName(model.getUViewName());
	                HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().update(ar);
	            }
	            else
	            {
	                model.setSys_id(null);
	                formDao.persist(model);
	                
	                ar.setUResourceId(model.getSys_id());
	                ar.setUResourceName(model.getUViewName());
	                ar.setUResourceType(MetaFormView.RESOURCE_TYPE);
	                HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(ar);
	                
	                model.setMetaAccessRights(ar);
	                formDao.update(model);
	                
	            }
	
	            //HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static WikidocStatistics saveWikidocStatistics(WikidocStatistics model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getWikidocStatisticsDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getWikidocStatisticsDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist WikidocStatistics: " + e.getMessage(), e);            
            throw e;
        }
        
        return model;
        
    }// saveWikidocStatistics

    public static WikidocAttachmentRel saveWikidocAttachmentRel(WikidocAttachmentRel model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist WikidocAttachmentRel: " + e.getMessage(), e);           
            throw e;
        }
        
        return model;
        
    }// saveWikidocAttachmentRel

    
    public static ResolveAssessRel saveResolveAssessRel(ResolveAssessRel model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveAssessRelDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveAssessRelDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolveAssessRel: " + e.getMessage(), e);
            throw e;
        }
        
        return model;
        
    }// saveResolveAssessRel
    
    public static ResolveParserRel saveResolveParserRel(ResolveParserRel model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolveParserRelDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolveParserRelDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolveParserRel: " + e.getMessage(), e);
            throw e;
        }
        
        return model;
        
    }// saveResolveParserRel
    
    public static ResolvePreprocessRel saveResolvePreprocessRel(ResolvePreprocessRel model, String username) throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolvePreprocessRelDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolvePreprocessRelDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ResolvePreprocessRel: " + e.getMessage(), e);
            throw e;
        }
        
        return model;
        
    }// saveResolvePreprocessRel
    
    public static ConfigRADIUS saveConfigRADIUS(ConfigRADIUS model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        Organization org = model.getBelongsToOrganization();
        if(StringUtils.isBlank(org.getSys_id()))
        {
            org = null;
        }
        
        model.setBelongsToOrganization(org);
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

            if (StringUtils.isNotBlank(model.getSys_id()))
            {
                HibernateUtil.getDAOFactory().getConfigRADIUSDAO().update(model);
            }
            else
            {
                model.setSys_id(null);
                HibernateUtil.getDAOFactory().getConfigRADIUSDAO().persist(model);
            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist ConfigRADIUS: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
    
    public static Orgs saveOrg(Orgs model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
        	Orgs modelFinal = model;
          HibernateProxy.setCurrentUser(username);
            model = (Orgs) HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(modelFinal.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getOrgsDAO().update(modelFinal);
	            }
	            else
	            {
	            	modelFinal.setSys_id(null);
	                return HibernateUtil.getDAOFactory().getOrgsDAO().persist(modelFinal);
	            }
	            return modelFinal;

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist Org " + model.getUName() + ": " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
         
        return model;
    }
}
