/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.message;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public enum NotificationTypeEnum
{
    //Document
    DOCUMENT_CREATE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_CREATE, "Create"),
    DOCUMENT_UPDATE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_UPDATE, "Update"),
    DOCUMENT_ACTIVE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_ACTIVE, "Active"),
    DOCUMENT_INACTIVE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_INACTIVE, "In Active"),
    DOCUMENT_LOCK_TYPE(UserGlobalNotificationContainerType.DOCUMENT_LOCKED, "Lock"),
    DOCUMENT_UNLOCK_TYPE(UserGlobalNotificationContainerType.DOCUMENT_UNLOCKED, "Un-Lock"),
    DOCUMENT_HIDE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_HIDE, "Hide"),
    DOCUMENT_UNHIDE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_UNHIDE, "Un-Hide"),
    DOCUMENT_DELETE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_DELETE, "Delete"),
    DOCUMENT_UNDELETE_TYPE(UserGlobalNotificationContainerType.DOCUMENT_UNDELETE, "Un-Delete"),
    DOCUMENT_PURGED_TYPE(UserGlobalNotificationContainerType.DOCUMENT_PURGED, "Purged"),
    DOCUMENT_ADDED_TO_PROCESS_TYPE(UserGlobalNotificationContainerType.DOCUMENT_ADDED_TO_PROCESS, "Added to Process"),
    DOCUMENT_REMOVED_FROM_PROCESS_TYPE(UserGlobalNotificationContainerType.DOCUMENT_REMOVED_FROM_PROCESS, "Removed from Process"),
    DOCUMENT_COMMIT_TYPE(UserGlobalNotificationContainerType.DOCUMENT_COMMIT, "Commit"),
    DOCUMENT_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.DOCUMENT_DIGEST_EMAIL, "Digest Email"),
    DOCUMENT_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.DOCUMENT_FORWARD_EMAIL, "Forward Email"),
    
    //Namespace
    NAMESPACE_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.NAMESPACE_DIGEST_EMAIL, "Digest Email"),
    NAMESPACE_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.NAMESPACE_FORWARD_EMAIL, "Forward Email"),

    //Actiontask
    ACTIONTASK_CREATE_TYPE(UserGlobalNotificationContainerType.ACTIONTASK_CREATE, "Create"),
    ACTIONTASK_UPDATE_TYPE(UserGlobalNotificationContainerType.ACTIONTASK_UPDATE, "Update"),
    ACTIONTASK_PURGED_TYPE(UserGlobalNotificationContainerType.ACTIONTASK_PURGED, "Purged"),
    ACTIONTASK_ADDED_TO_PROCESS_TYPE(UserGlobalNotificationContainerType.ACTIONTASK_ADDED_TO_PROCESS, "Added to Process"),
    ACTIONTASK_REMOVED_FROM_PROCESS_TYPE(UserGlobalNotificationContainerType.ACTIONTASK_REMOVED_FROM_PROCESS, "Removed from Process"),
    ACTIONTASK_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.ACTIONTASK_DIGEST_EMAIL, "Digest Email"),
    ACTIONTASK_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.ACTIONTASK_FORWARD_EMAIL, "Forward Email"),
    
    //WorkSheet
    WORKSHEET_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.WORKSHEET_DIGEST_EMAIL, "Digest Email"),
    WORKSHEET_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.WORKSHEET_FORWARD_EMAIL, "Forward Email"),
    
    //Rss
    RSS_CREATE_TYPE(UserGlobalNotificationContainerType.RSS_CREATE, "Create"),
    RSS_UPDATE_TYPE(UserGlobalNotificationContainerType.RSS_UPDATE, "Update"),
    RSS_PURGED_TYPE(UserGlobalNotificationContainerType.RSS_PURGED, "Purged"),
    RSS_ADDED_TO_PROCESS_TYPE(UserGlobalNotificationContainerType.RSS_ADDED_TO_PROCESS, "Added to Process"),
    RSS_REMOVED_FROM_PROCESS_TYPE(UserGlobalNotificationContainerType.RSS_REMOVED_FROM_PROCESS, "Removed from Process"),
    RSS_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.RSS_DIGEST_EMAIL, "Digest Email"),
    RSS_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.RSS_FORWARD_EMAIL, "Forward Email"),
    
    //Forum
    FORUM_CREATE_TYPE(UserGlobalNotificationContainerType.FORUM_CREATE, "Create"),
    FORUM_UPDATE_TYPE(UserGlobalNotificationContainerType.FORUM_UPDATE, "Update"),
    FORUM_PURGED_TYPE(UserGlobalNotificationContainerType.FORUM_PURGED, "Purged"),
    FORUM_ADDED_TO_PROCESS_TYPE(UserGlobalNotificationContainerType.FORUM_ADDED_TO_PROCESS, "Added to Process"),
    FORUM_REMOVED_FROM_PROCESS_TYPE(UserGlobalNotificationContainerType.FORUM_REMOVED_FROM_PROCESS, "Removed from Process"),
    FORUM_USER_ADDED_TYPE(UserGlobalNotificationContainerType.FORUM_USER_ADDED, "User added"),
    FORUM_USER_REMOVED_TYPE(UserGlobalNotificationContainerType.FORUM_USER_REMOVED, "User removed"),
    FORUM_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.FORUM_DIGEST_EMAIL, "Digest Email"),
    FORUM_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.FORUM_FORWARD_EMAIL, "Forward Email"),
    
    //team
    TEAM_CREATE_TYPE(UserGlobalNotificationContainerType.TEAM_CREATE, "Create"),
    TEAM_UPDATE_TYPE(UserGlobalNotificationContainerType.TEAM_UPDATE, "Update"),
    TEAM_PURGED_TYPE(UserGlobalNotificationContainerType.TEAM_PURGED, "Purged"),
    TEAM_ADDED_TO_PROCESS_TYPE(UserGlobalNotificationContainerType.TEAM_ADDED_TO_PROCESS, "Added to Process"),
    TEAM_REMOVED_FROM_PROCESS_TYPE(UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_PROCESS, "Removed from Process"),
    TEAM_ADDED_TO_TEAM_TYPE(UserGlobalNotificationContainerType.TEAM_ADDED_TO_TEAM, "Added to Team"),
    TEAM_REMOVED_FROM_TEAM_TYPE(UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_TEAM, "Removed from Team"),
    TEAM_USER_ADDED_TYPE(UserGlobalNotificationContainerType.TEAM_USER_ADDED, "User added"),
    TEAM_USER_REMOVED_TYPE(UserGlobalNotificationContainerType.TEAM_USER_REMOVED, "User removed"),
    TEAM_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.TEAM_DIGEST_EMAIL, "Digest Email"),
    TEAM_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.TEAM_FORWARD_EMAIL, "Forward Email"),
    
    
    //Process
    PROCESS_CREATE_TYPE(UserGlobalNotificationContainerType.PROCESS_CREATE, "Create"),
    PROCESS_UPDATE_TYPE(UserGlobalNotificationContainerType.PROCESS_UPDATE, "Update"),
    PROCESS_PURGED_TYPE(UserGlobalNotificationContainerType.PROCESS_PURGED, "Purged"),
    PROCESS_DOCUMENT_ADDED_TYPE(UserGlobalNotificationContainerType.PROCESS_DOCUMENT_ADDED, "Document added"),
    PROCESS_DOCUMENT_REMOVED_TYPE(UserGlobalNotificationContainerType.PROCESS_DOCUMENT_REMOVED, "Document removed"),
    PROCESS_ACTIONTASK_ADDED_TYPE(UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_ADDED, "ActionTask added"),
    PROCESS_ACTIONTASK_REMOVED_TYPE(UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_REMOVED, "ActionTask removed"),
    PROCESS_RSS_ADDED_TYPE(UserGlobalNotificationContainerType.PROCESS_RSS_ADDED, "Rss added"),
    PROCESS_RSS_REMOVED_TYPE(UserGlobalNotificationContainerType.PROCESS_RSS_REMOVED, "Rss removed"),
    PROCESS_FORUM_ADDED_TYPE(UserGlobalNotificationContainerType.PROCESS_FORUM_ADDED, "Forum added"),
    PROCESS_FORUM_REMOVED_TYPE(UserGlobalNotificationContainerType.PROCESS_FORUM_REMOVED, "Forum removed"),
    PROCESS_TEAM_ADDED_TYPE(UserGlobalNotificationContainerType.PROCESS_TEAM_ADDED, "Team added"),
    PROCESS_TEAM_REMOVED_TYPE(UserGlobalNotificationContainerType.PROCESS_TEAM_REMOVED, "Team removed"),
    PROCESS_USER_ADDED_TYPE(UserGlobalNotificationContainerType.PROCESS_USER_ADDED, "User added"),
    PROCESS_USER_REMOVED_TYPE(UserGlobalNotificationContainerType.PROCESS_USER_REMOVED, "User removed"),
    PROCESS_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.PROCESS_DIGEST_EMAIL, "Digest Email"),
    PROCESS_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL, "Forward Email"),
    
      
    //User
    USER_ADDED_TO_PROCESS_TYPE(UserGlobalNotificationContainerType.USER_ADDED_TO_PROCESS, "Added to Process"),
    USER_ADDED_TO_TEAM_TYPE(UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM, "Added to Team"),
    USER_FOLLOW_ME_TYPE(UserGlobalNotificationContainerType.USER_FOLLOW_ME, "User following me"),
    USER_REMOVED_FROM_PROCESS_TYPE(UserGlobalNotificationContainerType.USER_REMOVED_FROM_PROCESS, "Removed from Process"),
    USER_REMOVED_FROM_TEAM_TYPE(UserGlobalNotificationContainerType.USER_REMOVED_FROM_TEAM, "Removed from Team"),
    USER_UNFOLLOW_ME_TYPE(UserGlobalNotificationContainerType.USER_UNFOLLOW_ME, "User unfollow me"),
    USER_ADDED_TO_FORUM_TYPE(UserGlobalNotificationContainerType.USER_ADDED_TO_FORUM, "Added to Forum"),
    USER_REMOVED_FROM_FORUM_TYPE(UserGlobalNotificationContainerType.USER_REMOVED_FROM_FORUM, "Removed from Forum"),
    USER_PROFILE_CHANGE_TYPE(UserGlobalNotificationContainerType.USER_PROFILE_CHANGE, "User profile change"),
    USER_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.USER_DIGEST_EMAIL, "Inbox Digest Email"),
    USER_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.USER_FORWARD_EMAIL, "Inbox Forward Email"),
    USER_SYSTEM_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL, "System Digest Email"),
    USER_SYSTEM_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL, "System Forward Email"),
    USER_ALL_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL, "All Digest Email"),
    USER_ALL_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL, "All Forward Email"),
    
    //Playbook Template
    PLAYBOOK_TEMPLATE_CREATE_TYPE(UserGlobalNotificationContainerType.PLAYBOOK_TEMPLATE_CREATE, "Create"),
    PLAYBOOK_TEMPLATE_UPDATE_TYPE(UserGlobalNotificationContainerType.PLAYBOOK_TEMPLATE_UPDATE, "Update"),
    PLAYBOOK_TEMPLATE_COMMIT_TYPE(UserGlobalNotificationContainerType.PLAYBOOK_TEMPLATE_COMMIT, "Commit"),
    PLAYBOOK_TEMPLATE_DIGEST_EMAIL_TYPE(UserGlobalNotificationContainerType.PLAYBOOK_TEMPLATE_DIGEST_EMAIL, "Digest Email"),
    PLAYBOOK_TEMPLATE_FORWARD_EMAIL_TYPE(UserGlobalNotificationContainerType.PLAYBOOK_TEMPLATE_FORWARD_EMAIL, "Forward Email");
    
    public static final EnumSet<NotificationTypeEnum> enumSetDocument = EnumSet.range(DOCUMENT_CREATE_TYPE, DOCUMENT_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetNamespace = EnumSet.range(NAMESPACE_DIGEST_EMAIL_TYPE, NAMESPACE_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetActionTask = EnumSet.range(ACTIONTASK_CREATE_TYPE, ACTIONTASK_FORWARD_EMAIL_TYPE);  
    public static final EnumSet<NotificationTypeEnum> enumSetWorksheet = EnumSet.range(WORKSHEET_DIGEST_EMAIL_TYPE, WORKSHEET_FORWARD_EMAIL_TYPE);  
    public static final EnumSet<NotificationTypeEnum> enumSetRss = EnumSet.range(RSS_CREATE_TYPE, RSS_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetForum = EnumSet.range(FORUM_CREATE_TYPE, FORUM_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetTeam = EnumSet.range(TEAM_CREATE_TYPE, TEAM_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetProcess = EnumSet.range(PROCESS_CREATE_TYPE, PROCESS_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetUser = EnumSet.range(USER_ADDED_TO_PROCESS_TYPE, USER_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetPlaybookTemplate = EnumSet.range(PLAYBOOK_TEMPLATE_CREATE_TYPE, PLAYBOOK_TEMPLATE_FORWARD_EMAIL_TYPE);
    
    public static final EnumSet<NotificationTypeEnum> enumSetDocumentEmail = EnumSet.of(DOCUMENT_DIGEST_EMAIL_TYPE, DOCUMENT_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetNamespaceEmail = EnumSet.of(NAMESPACE_DIGEST_EMAIL_TYPE, NAMESPACE_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetActionTaskEmail = EnumSet.of(ACTIONTASK_DIGEST_EMAIL_TYPE, ACTIONTASK_FORWARD_EMAIL_TYPE);  
    public static final EnumSet<NotificationTypeEnum> enumSetWorksheetEmail = EnumSet.of(WORKSHEET_DIGEST_EMAIL_TYPE, WORKSHEET_FORWARD_EMAIL_TYPE);  
    public static final EnumSet<NotificationTypeEnum> enumSetRssEmail = EnumSet.of(RSS_DIGEST_EMAIL_TYPE, RSS_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetForumEmail = EnumSet.of(FORUM_DIGEST_EMAIL_TYPE, FORUM_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetTeamEmail = EnumSet.of(TEAM_DIGEST_EMAIL_TYPE, TEAM_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetProcessEmail = EnumSet.of(PROCESS_DIGEST_EMAIL_TYPE, PROCESS_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetUserEmail = EnumSet.range(USER_DIGEST_EMAIL_TYPE, USER_ALL_FORWARD_EMAIL_TYPE);
    public static final EnumSet<NotificationTypeEnum> enumSetPlaybookTemplateEmail = EnumSet.of(PLAYBOOK_TEMPLATE_DIGEST_EMAIL_TYPE, PLAYBOOK_TEMPLATE_FORWARD_EMAIL_TYPE);
       
    private final UserGlobalNotificationContainerType type;
    private final String displayName;
    
    NotificationTypeEnum(UserGlobalNotificationContainerType type, String displayName)
    {
        this.type = type;
        this.displayName = displayName;
    }
    
    public UserGlobalNotificationContainerType getType()
    {
        return type;
    }
    
    public static Set<UserGlobalNotificationContainerType> prepareNotificationTypes(EnumSet<NotificationTypeEnum> enumSet)
    {
        Set<UserGlobalNotificationContainerType> result = new HashSet<UserGlobalNotificationContainerType>();
        
        if(enumSet != null)
        {
            Iterator<NotificationTypeEnum> it = enumSet.iterator();
            while(it.hasNext())
            {
                result.add(it.next().getType());
            }
        }
        
        return result;
        
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
       
}
