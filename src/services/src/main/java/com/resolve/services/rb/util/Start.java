/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

public class Start extends TerminatorNode
{
    public Start(int id, int x, int y, String inputs, String outputs)
    {
        super(id, "Start", x, y, inputs, outputs, null);
    }

    @Override
    String getStyle()
    {
        return "symbol;image=/resolve/jsp/model/images/symbols/start.png";
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("    <Start ").append("label=\"Start\" description=\"PROCESS_LOOP%3Dtrue\" ").append("id=\"" + getId() + "\">\n"); 
        sb.append(getCell().toString()).append("\n"); 
        sb.append(getParams().toString()).append("\n");
        sb.append("    </Start>"); 
        
        return sb.toString();
    }

    public int compareTo(Object o)
    {
        // TODO Auto-generated method stub
        return 0;
    }
}
