/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.social;

import java.util.Map;

import com.resolve.services.interfaces.SocialDTO;

public class SocialTeamDTO extends SocialDTO
{
    private static final long serialVersionUID = 9017186432192470023L;
    public final static String TABLE_NAME = "social_team";
    public Boolean hasGroup = false;
    
    public SocialTeamDTO() {}
    
    public SocialTeamDTO(Map<String, Object> data)
    {
        super(data);
    }

    public Boolean getHasGroup()
    {
        return hasGroup;
    }

    public void setHasGroup(Boolean hasGroup)
    {
        this.hasGroup = hasGroup;
    }
    
//    List<UserDTO> userList = new ArrayList<UserDTO>();
//    List<SocialTeamDTO> teamList = new ArrayList<SocialTeamDTO>();
//    
//    public List<UserDTO> getUserList()
//    {
//        return userList;
//    }
//    public void setUserList(List<UserDTO> userList)
//    {
//        this.userList = userList;
//    }
//    public List<SocialTeamDTO> getTeamList()
//    {
//        return teamList;
//    }
//    public void setTeamList(List<SocialTeamDTO> teamList)
//    {
//        this.teamList = teamList;
//    }
}
