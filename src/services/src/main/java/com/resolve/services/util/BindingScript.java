/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import groovy.lang.Binding;

import com.resolve.groovy.script.GroovyScript;
import com.resolve.services.ServiceHibernate;

public class BindingScript
{
    public static String TYPE_ASSESS = "assess";
    public static String TYPE_PREPROCESS = "preprocess";
    public static String TYPE_PARSER = "parser";
    
    String type = null;
    Binding binding = null;

    public BindingScript(String type, Binding binding)
    {
        this.type = type;
        this.binding = binding;
    } // BindingScript

    public Object execute(String name, String methodName, Object args) throws Exception
    {
        Object result = null;
        
        if (args != null)
        {
            binding.setVariable("ARGS", args);
        }
        
        if (TYPE_ASSESS.equals(type))
        {
            result = GroovyScript.execute(ServiceHibernate.getAssessorScript(name, "system"), name, methodName, true, binding, new Object[] {args});
        }
        else if (TYPE_PREPROCESS.equals(type))
        {
            result = GroovyScript.execute(ServiceHibernate.getPreprocessorScript(name, "system"), methodName, name, true, binding, null);
        }
        else if (TYPE_PARSER.equals(type))
        {
            result = GroovyScript.execute(ServiceHibernate.getParserScript(name, "system"), methodName, name, true, binding, null);
        }
        
        return result;
    } // execute

    public Object execute(String name, Object args) throws Exception
    {
        Object result = null;
        
        if (args != null)
        {
            binding.setVariable("ARGS", args);
        }
        
        if (TYPE_ASSESS.equals(type))
        {
            result = GroovyScript.execute(ServiceHibernate.getAssessorScript(name, "system"), name, true, binding, null);
        }
        else if (TYPE_PREPROCESS.equals(type))
        {
            result = GroovyScript.execute(ServiceHibernate.getPreprocessorScript(name, "system"), name, true, binding, null);
        }
        else if (TYPE_PARSER.equals(type))
        {
            result = GroovyScript.execute(ServiceHibernate.getParserScript(name, "system"), name, true, binding, null);
        }
        
        return result;
    } // execute

    public Object execute(String name) throws Exception
    {

        return execute(name, null);
    } // execute

} // BindingScript
