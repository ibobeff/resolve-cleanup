/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.graph.social.model.RSSubscriber;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

public class User extends RSComponent implements RSPublisher, RSSubscriber
{
    private static final long serialVersionUID = 1109656701824385257L;

    public User()
    {
        super(NodeType.USER);
    }
    
    public User(String name)
    {
        super(NodeType.USER);
        setName(name);
    }
    
    public User(ResolveNodeVO node) throws Exception
    {
        super(node);
    }
}
