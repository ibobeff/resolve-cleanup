/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.menu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.hibernate.util.vo.MenuSetVO;
import com.resolve.services.hibernate.vo.SysPerspectiveVO;
import com.resolve.services.vo.menu.MenuSetModel;
import com.resolve.util.StringUtils;

/**
 * This class provide menuSet services.
 */
public class MenuService
{

    protected String defaultMenuSet;
    protected boolean isDefaultMenuSetDisabled = false;
    
    /**
     * get menu sets.
     * 
     * @param userName
     * @param forNavigation
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<MenuSetModel> getMenuSets(String userName, boolean forNavigation)
    {
        List<MenuSetModel> menuSetList = new ArrayList<MenuSetModel>();
        
        //get the data
        Map<String, Object> references = MenuServiceUtil.getMenuSets(userName, forNavigation, defaultMenuSet);//used to pass data from service layer
        
        //prepare for UI
        List<MenuSetVO> listForUser = (List<MenuSetVO>) references.get(HibernateConstants.MENU_SETS);
        isDefaultMenuSetDisabled = (Boolean) references.get(HibernateConstants.DEFAULT_MENUSET_DISABLED);
        
        MenuSetModel menuSet = null;
        for(MenuSetVO vo : listForUser)
        {
            SysPerspectiveVO sysPersp = vo.getSysPerspectiveVO(); 
            
            Integer sequence = sysPersp.getSequence();
            menuSet = new MenuSetModel(sysPersp.getName(), vo.getApplicationsStr(), vo.getRolesStr(), sysPersp.getSys_id());
            menuSet.setSequence(sequence);
            menuSet.setIsActive(sysPersp.getActive()==null?true:sysPersp.getActive());
            menuSetList.add(menuSet);
        }
        
        if (forNavigation)
        {
            menuSet = new MenuSetModel("All", "", "", "");
            menuSet.setSequence(new Integer(-1));
            menuSetList.add(menuSet);
        }

        menuSetList = sortMenuSetList(menuSetList);
        
        return menuSetList;
    } // getMenuSets

    /**
     * sort menu set by sequence.
     * 
     * @param menuSetList
     * @return
     */
    private List<MenuSetModel> sortMenuSetList(List<MenuSetModel> menuSetList)
    {
        if (menuSetList != null && !menuSetList.isEmpty())
        {
            Collections.sort(menuSetList, new Comparator<MenuSetModel>()
            {
                public int compare(MenuSetModel p1, MenuSetModel p2)
                {
                    int sequence1 = p1.getSequence().intValue();
                    int sequence2 = p2.getSequence().intValue();
                    return compareInt(sequence1, sequence2);
                }
            });
        }

        return menuSetList;
    } // sortMenuSetList

    private static int compareInt(int sequence1, int sequence2)
    {
        if(sequence1 == 0 || sequence2 == 0)
        {
            return -1;
        }
        
        if(sequence1 == sequence2)
        {
            return 0;
        }
        else if(sequence1 > sequence2)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    /**
     * 
     * get menu sections.
     * 
     * @param userName
     * @param selectedMenuSet
     * @param menuSetID
     * @return
     */
    public List<Map<String, String>> getMenuSections(String userName, String selectedMenuSet, String menuSetID)
    {
        if (StringUtils.isBlank(selectedMenuSet) || selectedMenuSet.equalsIgnoreCase(defaultMenuSet))
        {
            if (isDefaultMenuSetDisabled)
            {
                selectedMenuSet = "All";
            }
            else
            {
                selectedMenuSet = defaultMenuSet;
            }
        }

        return MenuServiceUtil.getMenuSections(userName, selectedMenuSet, menuSetID);

    } // getMenuSections
}
