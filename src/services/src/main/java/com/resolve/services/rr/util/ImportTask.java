package com.resolve.services.rr.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.vo.RRRuleFieldVO;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ImportTask extends BaseBulkTask
{
    private FileItem item;
    private static final String tmpDirPath = MainBase.getTmpDir() + "/rr_tmp/";
    private String tmpFilePath = "";
    private Double percent;
    private boolean override;
    static
    {
        File file = new File(tmpDirPath);
        file.mkdir();
    }

    public static interface ExtraAction
    {
        public void action();
    }

    // we can do this by counting the file size already read..
    public double getPercentage()
    {
        return this.percent;
    }

    public ImportTask(FileItem item, String username, boolean override)
    {
        this.item = item;
        this.override = override;
        this.percent = 0.0;
        this.init(username, BaseBulkTask.IMPORT);
    }

    private File extractFile() throws Exception
    {
        String filename = String.format("%s%s%s", tmpDirPath, File.separator, username);
        File file = FileUtils.getFile(filename);
        // File file = new File(tmpDirPath + "/" + username);
        if (!file.exists()) file.mkdir();
        // open tmp file
        String sep = File.separator;
        this.tmpFilePath = String.format("%s%s%s%s__rr_import_%s", tmpDirPath, sep, username, sep, (new Date()).getTime()); 
        // tmpDirPath + "/" + username + "/__rr_import_" + (new Date()).getTime();
        file = FileUtils.getFile(this.tmpFilePath);
        // file = new File(this.tmpFilePath);

        if (file.exists()) file.delete();
        this.item.write(file);
        return file;
    }

    private RRRuleVO lineToRule(String[] headers, String line) throws Exception
    {
        RRRuleVO rule = new RRRuleVO();
        String[] fields = line.split(",");
        List<RRRuleFieldVO> ridFields = new ArrayList<RRRuleFieldVO>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonFields = null;
        boolean found = false;
        for (int i = 0; i < headers.length && i < fields.length; i++)
        {
            String header = headers[i];
            String field = fields[i];
            if (header.equals("module"))
                rule.setModule(field);
            else if (header.equals("schema"))
            {
                RRSchemaVO schema = ResolutionRoutingUtil.getSchemaByName(field, this.username);
                jsonFields = mapper.readTree(schema.getJsonFields());
                rule.setSchema(schema);
            }
            else if (header.equals("wiki"))
                rule.setWiki(field);
            else if (header.equals("automation"))
                rule.setAutomation(field);
            else if (header.equals("runbook"))
                rule.setRunbook(field);
            else if (header.equals("eventid"))
                rule.setEventId(field);
            else if (header.equals("active"))
                rule.setActive(Boolean.valueOf(field));
            else if (header.equals("newWorksheetOnly"))
                rule.setNewWorksheetOnly(Boolean.valueOf(field));
            else if (header.equals("sir"))
            	rule.setSir(Boolean.valueOf(field));
            else if (header.equals("source"))
                rule.setSirSource(field);
            else if (header.equals("title"))
                rule.setSirTitle(field);
            else if (header.equals("type"))
                rule.setSirType(field);
            else if (header.equals("playbook"))
                rule.setSirPlaybook(field);
            else if (header.equals("severity"))
                rule.setSirSeverity(field);
            else if (header.equals("owner"))
                rule.setSirOwner(field);
            else
            {
                for (JsonNode jsonField : jsonFields)
                    if (jsonField.get("name").asText().equals(header)) found = true;
                if (!found) continue;
                RRRuleFieldVO fieldVO = new RRRuleFieldVO(null, header, field, 0, rule);
                ridFields.add(fieldVO);
                rule.setRidFields(ridFields);
            }
        }

        for (JsonNode jsonField : jsonFields)
        {
            found = false;
            for (RRRuleFieldVO ridField : rule.getRidFields())
            {
                if (jsonField.get("name").equals(ridField.getName()))
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
            	rule.getRidFields().add(new RRRuleFieldVO(null, jsonField.get("name").asText(), "", 0, rule));
            }
        }

        List<String> rid = new ArrayList<String>();
        for (RRRuleFieldVO ridField : rule.getRidFields())
        {
            for (JsonNode jsonField : jsonFields)
                if (jsonField.get("name").asText().equals(ridField.getName()))
                {
                    ridField.setOrder(jsonField.get("order").asInt());
                    break;
                }

            rid.add(ridField.getValue());
        }
        rule.setRid(StringUtils.join(rid, StringUtils.DELIMITER));
        return rule;
    }

    /**
     * 
     * @param errorOccured
     *            if true, it means the task stop because of an error. so this
     *            thread will not terminate immediately it will wait a little
     *            bit so that the polling can get the error status
     */
    public void cleanUp(boolean errorOccured)
    {
        if (StringUtils.isEmpty(this.tmpFilePath)) return;
        try
        {
            File file = FileUtils.getFile(this.tmpFilePath);
            // File file = new File(this.tmpFilePath);
            if (file.exists()) file.delete();
            // this takes out the thread from the map

            int count = 0;
            while (count++ <= 5 && errorOccured)
                Thread.sleep(1000);// give the client side a chance to check the
                                   // final state
            ServiceResolutionRouting.poll(this.getTid(), this.username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    private boolean validateHeader(String[] headers)
    {
        String[] defaults = "module,schema,wiki,runbook,automation,eventid,active,newWorksheetOnly,sir,source,title,type,playbook,severity,owner".split(",");
        boolean valid = false;
        for (int i = 0; i < defaults.length; i++)
        {
            valid = false;
            for (int j = 0; j < headers.length; j++)
                if (defaults[i].equals(headers[j])) valid = true;
            if (!valid) return false;
        }
        return true;
    }

    public void run()
    {
        long start = System.currentTimeMillis();
        long current = -1;
        File file = null;
        try
        {
            file = this.extractFile();
        }
        catch (Exception e)
        {
            this.recorder.globalError("ERROR: Cannot access the tmp file!");
            Log.log.error(e.getMessage(), e);
            this.cleanUp(true);
            return;
        }

        String[] headers = null;
        FileReader fr = null;
        BufferedReader br = null;
        int approximateReadCount = 0;
        int lineNum = -1;
        try
        {
            fr = new FileReader(file);
            br = new BufferedReader(fr);

            while (br.ready())
            {
                current = System.currentTimeMillis();
                if ((current - start) / Locker.MINUTE_IN_MILLIS > Locker.TIMEOUT_IN_MINUTE)
                {
                    start = current;
                    Locker.updateLock(this.getTid(), this.username, current);
                }
                if (this.shouldStop) break;
                RRRuleVO rule = null;
                String line = "";
                try
                {
                    line = br.readLine();
                    if (StringUtils.isEmpty(line)) continue;
                    lineNum++;
                    if (headers == null)
                    {
                        headers = line.split(",");
                        approximateReadCount += line.getBytes().length;
                        continue;
                    }
                    approximateReadCount += line.getBytes().length;
                    this.percent = approximateReadCount * 1.0 / file.length();
                }
                catch (Exception e)
                {
                    this.recorder.importFailure(lineNum, line, "Error reading csv line!");
                    Log.log.error(e);
                    continue;
                }
                if (!this.validateHeader(headers))
                {
                    this.recorder.globalError("Cannot find mandatory headers(module,schema,wiki,runbook,automation,eventid,active,newWorksheetOnly,sir,source,title,type,playbook,severity,owner)!");
                    this.percent = 1.0;
                    this.recorder.finish();
                    this.cleanUp(true);
                    return;
                }

                try
                {
                    // Thread.sleep(1000);
                    rule = this.lineToRule(headers, line);
                }
                catch (RRInterestingException e)
                {
                    this.recorder.importFailure(lineNum, line, e.getMessage());
                    continue;
                }
                catch (Exception e)
                {
                    this.recorder.importFailure(lineNum, line, "Error converting csv to mapping!");
                    Log.log.error(e);
                    continue;
                }
                try
                {
                    try
                    {
                        ServiceResolutionRouting.saveRule(rule, this.username);
                    }
                    catch (RRInterestingException e)
                    {
                        if (!this.override) throw e;
                        RRRuleVO vo = (RRRuleVO) e.getData();
                        rule.setId(vo.getId());
                        List<RRRuleFieldVO> oFields = vo.getRidFields();
                        List<RRRuleFieldVO> cFields = rule.getRidFields();
                        if (oFields.size() != cFields.size())
                        {
                            this.recorder.importFailure(lineNum, line, "Field mismatch when updating mapping in db:" + vo.getRid());
                            continue;
                        }
                        boolean found = false;
                        for (RRRuleFieldVO oField : oFields)
                        {
                            found = false;
                            for (RRRuleFieldVO cField : cFields)
                            {
                                if (oField.getName().equals(cField.getName()))
                                {
                                    found = true;
                                    cField.setId(oField.getId());
                                }
                            }
                            if (!found) break;
                        }
                        if (!found)
                        {
                            this.recorder.importFailure(lineNum, line, "Field mismatch when updating mapping in db:" + vo.getRid());
                            continue;
                        }
                        ServiceResolutionRouting.saveRule(rule, this.username);
                    }
                }
                catch (Exception e)
                {
                    this.recorder.importFailure(lineNum, line, "Error saving mapping to database!");
                    Log.log.error(e);
                    continue;
                }
            }
        }
        catch (Exception e)
        {
            this.recorder.globalError("Error reading tmp file or getting update lock!");
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                br.close();
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
            try
            {
                fr.close();
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        this.percent = 1.0;
        this.recorder.finish();
        this.cleanUp(false);

    }
}
