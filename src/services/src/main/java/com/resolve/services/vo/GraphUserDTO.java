/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.Set;

import com.resolve.services.graph.social.model.component.User;

public class GraphUserDTO
{
    private User user;
     
    private Set<String> readPostIds;
    private Set<String> starredPostIds;
    private Set<String> likePostIds;
    
    
    public User getUser()
    {
        return user;
    }
    public void setUser(User user)
    {
        this.user = user;
    }
    public Set<String> getReadPostIds()
    {
        return readPostIds;
    }
    public void setReadPostIds(Set<String> readPostIds)
    {
        this.readPostIds = readPostIds;
    }
    public Set<String> getStarredPostIds()
    {
        return starredPostIds;
    }
    public void setStarredPostIds(Set<String> starredPostIds)
    {
        this.starredPostIds = starredPostIds;
    }
    public Set<String> getLikePostIds()
    {
        return likePostIds;
    }
    public void setLikePostIds(Set<String> likePostIds)
    {
        this.likePostIds = likePostIds;
    }
    
    
    

}
