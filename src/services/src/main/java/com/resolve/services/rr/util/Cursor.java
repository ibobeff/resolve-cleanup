package com.resolve.services.rr.util;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.hibernate.vo.RRRuleVO;

public abstract class Cursor
{
    //we record error inside this function
    public abstract boolean hasNext() throws Exception;
    public abstract RRRuleVO next() throws ReadRuleException;
    protected List<String> getDefaultHeader(){
        List<String> headers = new ArrayList<String>();
        headers.add("module");
        headers.add("schema");
        headers.add("wiki");
        headers.add("runbook");
        headers.add("automation");
        headers.add("eventid");
        headers.add("active");
        headers.add("newWorksheetOnly");
        headers.add("sir");
        headers.add("source");
        headers.add("title");
        headers.add("type");
        headers.add("playbook");
        headers.add("severity");
        headers.add("owner");
        return headers;
    }
    public abstract List<String> appendRuleHeaders() throws Exception;
    public abstract Double getPercentage();
}
