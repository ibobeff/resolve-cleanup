package com.resolve.services.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SecurityUtil
{
    public static boolean isCommandBlacklisted(String command)
    {
        boolean result = false;
        if (StringUtils.isNotBlank(command))
        {
            String blacklistRegExDefinitionsDelimited = PropertiesUtil.getPropertyString(Constants.SECURITY_COMMANDS_BLACKLIST);
            if (StringUtils.isBlank(blacklistRegExDefinitionsDelimited))
            {
                Log.log.debug("No blacklisted commands defined.");
            }
            else
            {
                String[] regExArray = blacklistRegExDefinitionsDelimited.split(Constants.SECURITY_COMMANDS_BLACKLIST_DELIMITER_REGEX);
                for (String regEx : regExArray)
                {
                    Pattern par = Pattern.compile(regEx);
                    Matcher matcher = par.matcher(command);
                    if (matcher.find())
                    {
                        result = true;
                    }
                }
                Log.log.debug("The command is not blacklisted.");
            }
        }
        return result;
    }

    
    public static boolean isCommandBlacklistEnabled()
    {
        boolean result = false;
        String blacklistRegExDefinitionsDelimited = PropertiesUtil.getPropertyString(Constants.SECURITY_COMMANDS_BLACKLIST);
        if (StringUtils.isBlank(blacklistRegExDefinitionsDelimited))
        {
            Log.log.debug("No blacklisted commands defined.");
        }
        else
        {
            result = true;
        }
        return result;
    }

}
