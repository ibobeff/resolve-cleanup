/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

/**
 * Reports for 
 *      Path Completed Detail Reports
 *      Path Aborted Detail Reports
 * 
 * @author jeet.marwah
 *
 */
public class PathDetailReportDTO
{
    private String pathId;
    private String dtFullName;
    private double totalTimeInSecs;
    
    private DTReportDetailDTO details;
    
    public PathDetailReportDTO() {}
    
    public PathDetailReportDTO(DTGroupByPathIdDTO path)
    {
        if(path != null)
        {
            DTRecord dtRecord = path.getRecords().get(0);
            dtFullName = dtRecord.getDtRootDoc();
            pathId = dtRecord.getPathId();                            

            //update the details
            details = new DTReportDetailDTO(path.getRecords());
            
            //set the total time in secs
            totalTimeInSecs = details.getTotalTimeInSecs();
        }
    }

    public String getPathId()
    {
        return pathId;
    }

    public void setPathId(String pathId)
    {
        this.pathId = pathId;
    }

    public String getDtFullName()
    {
        return dtFullName;
    }

    public void setDtFullName(String dtFullName)
    {
        this.dtFullName = dtFullName;
    }

    public double getTotalTimeInSecs()
    {
        return totalTimeInSecs;
    }

    public void setTotalTimeInSecs(double totalTimeInSecs)
    {
        this.totalTimeInSecs = totalTimeInSecs;
    }

    public DTReportDetailDTO getDetails()
    {
        return details;
    }

    public void setDetails(DTReportDetailDTO details)
    {
        this.details = details;
    }
    

}
