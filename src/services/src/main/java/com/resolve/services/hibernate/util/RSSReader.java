/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.resolve.search.SearchConstants;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.SocialPost;
import com.resolve.search.social.SocialSearchAPI;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public class RSSReader
{
    public final static long EXPIRATION_SECS = 24 * 60 * 60; // seconds in a day

    @SuppressWarnings("unchecked")
    public static void update(String sys_id, String username)
    {
        if (StringUtils.isNotBlank(sys_id))
        {
            try
            {
                SocialRssDTO vo = null;
                
                try
                {
                    vo = ServiceHibernate.getRss(sys_id, username);
                }
                catch(RuntimeException re)
                {
                    Log.log.info("Error " + re.getLocalizedMessage() + " in getting RSS for sys_id " + sys_id);
                }
                
                if (vo != null)
                {
                    Collection<ResolveNodeVO> usersFollowingRss = ServiceGraph.getNodesFollowingMe(null, vo.getSys_id(), null, NodeType.RSS, username);
                    long follows = usersFollowingRss != null ? usersFollowingRss.size() : 0;//ServiceSocial.getFollowerCountForComp(rss.getSys_id());
                    
                    Rss rss = SocialCompConversionUtil.convertSocialRssDTOToRss(vo, null);

                    // add locking for each subscription processing
                    Boolean locked = vo.getU_is_locked();
                    if (locked == null)
                    {
                        locked = false;
                    }

                    if (!locked && follows > 0)
                    {
                        String url = vo.getU_url();
                        Log.log.debug("processing rss feed url: " + url);
                        URL feedUrl = new URL(url);

                        SyndFeedInput input = new SyndFeedInput();
                        SyndFeed feed = input.build(new XmlReader(feedUrl));
                        List<SyndEntry> entries = feed.getEntries();
                        for (SyndEntry syndEntry : entries)
                        {
                             postRSS(rss, syndEntry);
                        }

                        // update subscription
                        ServiceHibernate.updateFeedUpdatedDateForRss(vo, username);
                    }
                    else
                    {
                        //remove the thread for this rss as its locked or nobody is following it
                        SocialUtil.removeRssCronJob(vo.getSys_id(), vo.getU_display_name());
                    }

                }

            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }
    } // update

    @SuppressWarnings("unchecked")
    private static void postRSS(Rss rss, SyndEntry entry)
    {
        // sysid and title
        String title = entry.getTitle();

        // url
        // String url = entry.getLink();//entry.getUri();

        // authors
        String author = entry.getAuthor();
        if (StringUtils.isEmpty(author))
        {
            author = "";
        }

        List<Object> authors = entry.getAuthors();
        if (authors != null)
        {
            for (Object auth : authors)
            {
                String authStr = "";
                if (auth instanceof SyndPerson)
                {
                    SyndPerson person = (SyndPerson) auth;
                    authStr = person.getName();
                }
                else
                {
                    authStr = (String) auth;
                }

                // prepare the author string
                if (StringUtils.isNotEmpty(author))
                {
                    author += " " + authStr;
                }
                else
                {
                    author = authStr + "";
                }
            }
        }

        // content
        String content = entry.getDescription().getValue();

        // publish date
        Date publishDate = entry.getPublishedDate();
        if (publishDate == null)
        {
            publishDate = new Date();
        }

        // expiration date
        long expireDate = System.currentTimeMillis() + PropertiesUtil.getPropertyLong(Constants.SOCIAL_EXPIRATION_RSS_INTERVAL) * EXPIRATION_SECS;

        List<RSComponent> targets = new ArrayList<RSComponent>();
        targets.add(rss);

        try
        {
            List<String> targetSysIds = new LinkedList<String>();
            
            // post
            SocialPost post = SocialSearchAPI.findRSSByUri(entry.getUri(), SearchConstants.SYSTEM_USERNAME);
            if(post == null)
            {
                if(StringUtils.isNotBlank(entry.getUri()))
                {
                    User user = new User(SearchConstants.SYSTEM_USERNAME);
                    user.setSys_id(SearchConstants.SYSTEM_USERNAME);
                    user.setDisplayName(SearchConstants.SYSTEM_USERNAME);
                    post = SearchUtils.prepareSocialPost(null, content, user);                
                    post.setType(SearchConstants.SOCIALPOST_TYPE_RSS);
                    post.setUri(entry.getUri().trim());
                    post.setTitle(title);
                    post.setAuthorUsername(author);
                    content = content.replaceAll("(?i)<a", "<a target=\"_blank\"");
                    post.setContent(content);
                    post.setAuthorUsername(author);
                    post.setSysCreatedOn(publishDate.getTime());
                    post.setSysUpdatedOn(publishDate.getTime());
                    targetSysIds.add(rss.getSys_id());
                    ServiceSocial.postRSS(post, targetSysIds, "system");
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("error in posting for RSS:" + rss.getDisplayName(), e);
        }
    } // postRSS
} // RSSReader
