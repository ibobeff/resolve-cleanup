/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util.vo;

import java.util.List;
import java.util.Map;

public class MenuSetModelVO
{
    private String name;
    private String applications;
    private String roles;
    private String menuSetID;
    private Boolean active;
    private Integer sequence;
    private List<Map<String, String>> menuSetApplicationsList;
    private List<Map<String, String>> menuSetRolesList;
    private List<Map<String, String>> availableApplicationsList;
    private List<Map<String, String>> availableRolesList;
    
    public MenuSetModelVO(String name, String applications, String roles, String menuSetID)
    {
        this.setName(name);
        this.setApplications(applications);
        this.setRoles(roles);
        this.setMenuSetID(menuSetID);
        
    } //MenuSet
    
    public MenuSetModelVO() {}
    
    
    
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getApplications()
    {
        return applications;
    }
    public void setApplications(String appliations)
    {
        this.applications = appliations;
    }
    public String getRoles()
    {
        return roles;
    }
    public void setRoles(String roles)
    {
        this.roles = roles;
    }
    public String getMenuSetID()
    {
        return menuSetID;
    }
    public void setMenuSetID(String menuSetID)
    {
        this.menuSetID = menuSetID;
    }
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    public Integer getSequence()
    {
        return sequence;
    }
    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }
    public List<Map<String, String>> getMenuSetApplicationsList()
    {
        return menuSetApplicationsList;
    }
    public void setMenuSetApplicationsList(List<Map<String, String>> menuSetApplicationsList)
    {
        this.menuSetApplicationsList = menuSetApplicationsList;
    }
    public List<Map<String, String>> getMenuSetRolesList()
    {
        return menuSetRolesList;
    }
    public void setMenuSetRolesList(List<Map<String, String>> menuSetRolesList)
    {
        this.menuSetRolesList = menuSetRolesList;
    }
    public List<Map<String, String>> getAvailableApplicationsList()
    {
        return availableApplicationsList;
    }
    public void setAvailableApplicationsList(List<Map<String, String>> availableApplicationsList)
    {
        this.availableApplicationsList = availableApplicationsList;
    }
    public List<Map<String, String>> getAvailableRolesList()
    {
        return availableRolesList;
    }
    public void setAvailableRolesList(List<Map<String, String>> availableRolesList)
    {
        this.availableRolesList = availableRolesList;
    }
    
}
