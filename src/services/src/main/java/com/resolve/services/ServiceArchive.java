/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Level;

import com.resolve.services.archive.ArchiveSIRData;
import com.resolve.services.archive.ArchiveTable;
import com.resolve.services.archive.ArchiveTableUtil;
import com.resolve.services.hibernate.util.vo.ActionTaskInfoVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ServiceArchive {
    public static void archive(String dbType, boolean force) {
    	long methodStartTime = Log.start(String.format("Starting Archive (DB Type %s, force %b): ", dbType, force));
    	
    	try {
    		if (!ArchiveSIRData.archiveSIRDataInProgress) {
    			ArchiveSIRData.startArchive();
    			
    			/*
    			 * Archive SIR WS and associated SIR WS data,
    			 * updating of SIR DB to archived (deleted) and removal of
    			 * SIR Data (notes, artifacts, attachments, and audit log)
    			 * from ES SIR indices pbxxxx is performed only after
    			 * modified SIR Data (if any) is backed up one last time
    			 * before archival. 
    			 */
			
    			ArchiveTable.archive(dbType, force, true);
    		} else {
    			Log.log.warn("Backup of modified SIR Data (if any) such as notes, artifacts, " +
    						 "attachments, and audit log into DB and subsequent archival of SIR " +
    						 "worksheet data is skipped as backup of modified SIR Data is " +
    					     "currently in progress, proceeding to archive Non-SIR Worksheet and " +
    						 "Non-SIR worksheet data...");
    		}
		} catch (Exception e) {
			Log.log.error(String.format("Error %sin backing up modified SIR Data (if any) such as notes, artifacts, " +
										"attachments, and audit log into DB, proceeding to archive Non-SIR Worksheet and " +
										"Non-SIR worksheet data...", 
										StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), e);
		}
    	
    	// Always archive Non-SIR WS and associated Non-SIR WS data
    	
        ArchiveTable.archive(dbType, force, false);
        
        Log.duration(String.format("Completed Archive (DB Type %s, force %b): ", dbType, force), Level.INFO, methodStartTime);
    }
    
    @Deprecated
    public static List<ActionTaskInfoVO> getListForResultMacroArchiveWorksheet(String problemId, String[] actionNamespaceArr, 
    																		   String[] actionNameArr, String[] nodeIdArr, 
    																		   String[] actionWikiArr, String[] descriptionArr, 
    																		   boolean isOrderByAsc, 
    																		   List<String> actionTaskids) {
        return ArchiveTable.getListForResultMacroArchiveWorksheet(problemId, actionNamespaceArr, actionNameArr, nodeIdArr, actionWikiArr, descriptionArr, isOrderByAsc, actionTaskids);
    }
    
    public static Set<String> getAllArchiveTaskSysIdsForProblemId(String problemId)
    {
        return ArchiveTableUtil.getAllArchiveTaskSysIdsForProblemId(problemId);
    }
    
}
