/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util.vo;

import com.resolve.services.hibernate.vo.SysPerspectiveVO;

public class MenuSetVO
{
    private SysPerspectiveVO sysPerspectiveVO;
    private String applicationsStr;
    private String rolesStr;
    
    
    public SysPerspectiveVO getSysPerspectiveVO()
    {
        return sysPerspectiveVO;
    }
    public void setSysPerspectiveVO(SysPerspectiveVO sysPerspectiveVO)
    {
        this.sysPerspectiveVO = sysPerspectiveVO;
    }
    public String getApplicationsStr()
    {
        return applicationsStr;
    }
    public void setApplicationsStr(String applicationsStr)
    {
        this.applicationsStr = applicationsStr;
    }
    public String getRolesStr()
    {
        return rolesStr;
    }
    public void setRolesStr(String rolesStr)
    {
        this.rolesStr = rolesStr;
    }
    
    
    
    

}
