package com.resolve.services.hibernate.wiki;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;
import com.resolve.util.ParseUtil;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Validates a {@link WikiDocumentVO}
 */
public class WikiDocumentValidatorService {
	public boolean validate(final WikiDocumentVO wikiDocumentVO) {
		try {
			WikiUtils.validateWikiDocName(wikiDocumentVO.getUFullname());
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		wikiDocumentVO.cleanContent();

		if (StringUtils.isNotBlank(wikiDocumentVO.getUDisplayMode())
				&& !wikiDocumentVO.getUDisplayMode().equals("UNDEFINED")) {
			if (!(wikiDocumentVO.getUDisplayMode().equals(WikiDocumentVO.WIKI)
					|| wikiDocumentVO.getUDisplayMode().equals(WikiDocumentVO.CATALOG)
					|| wikiDocumentVO.getUDisplayMode().equals(WikiDocumentVO.DECISIONTREE)
					|| wikiDocumentVO.getUDisplayMode().equals(WikiDocumentVO.PLAYBOOK))) {
				throw new RuntimeException(
						"Display Mode does not contain valid string. It should either be '" + WikiDocumentVO.WIKI
								+ "' or '" + WikiDocumentVO.CATALOG + "' or '" + WikiDocumentVO.DECISIONTREE + "' or '"
								+ WikiDocumentVO.PLAYBOOK + "' but has '" + wikiDocumentVO.getUDisplayMode() + "'");
			}
		} else {
			wikiDocumentVO.setUDisplayMode(WikiDocumentVO.WIKI);
		}

		// validate the params
		validateParams(wikiDocumentVO);

		/*
		 * HP TODO For playbook template or SIR playbook, activities don't reference
		 * playbook template or SIR playbook.
		 */

		if (StringUtils.isNotBlank(wikiDocumentVO.getUDisplayMode())
				&& wikiDocumentVO.getUDisplayMode().equals(WikiDocumentVO.PLAYBOOK)) {
			validatePlaybookActivities(wikiDocumentVO);
		}
		return true;
	}

	private void validateParams(final WikiDocumentVO wikiDocumentVO) {
		String paramsJson = wikiDocumentVO.getUWikiParameters();
		if (StringUtils.isNotBlank(paramsJson) && !paramsJson.equals(VO.STRING_DEFAULT)) {
			paramsJson = paramsJson.trim();
			JSONArray jsonArr = null;

			try {
				jsonArr = StringUtils.stringToJSONArray(paramsJson);
			} catch (Exception e) {
				// not req - if there is an exception - ignore it.
				Log.log.info(e.getMessage());
			}

			if (jsonArr != null && jsonArr.size() > 0) {
				for (Object o : jsonArr) {
					JSONObject jsonObj = (JSONObject) o;
					String name = jsonObj.getString("name");
					if (StringUtils.isBlank(name)) {
						throw new RuntimeException(
								"'Name' is mandatory for Wiki Parameters. Please make sure it is populated.");
					}
				}
			}
		}
	}

	private void validatePlaybookActivities(final WikiDocumentVO wikiDocumentVO) {
		if (StringUtils.isBlank(wikiDocumentVO.getUContent())) {
			return;
		}

		List<String> procedures = ParseUtil.getListOfProcedures(wikiDocumentVO.getUContent());

		if (procedures == null || procedures.isEmpty()) {
			return;
		}

		if (procedures.size() > 1) {
			throw new RuntimeException(
					"Invalid procedure sections in playbook. Playbook cannot contain more than one procedure section.");
		}

		List<Map<String, String>> activityMaps = ParseUtil.getListOfActivityMaps(procedures.get(0));
		if (CollectionUtils.isEmpty(activityMaps)) {
			return;
		}

		for (Map<String, String> activityMap : activityMaps) {
			if (!activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)
					|| StringUtils.isBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY))) {
				continue;
			}
			WikiDocumentVO activityDoc = WikiUtils.getWikiDoc(null,
					activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY), "system");

			if (activityDoc == null) {
				throw new RuntimeException("Playbook referenced activity "
						+ (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)
								? "named " + activityMap
										.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)
								: "")
						+ (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)
								? " in category "
										+ activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)
								: "")
						+ " referencing document "
						+ activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)
						+ " cannot be found.");
			}

			if (activityDoc != null && StringUtils.isNotBlank(activityDoc.getUDisplayMode())
					&& activityDoc.getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK)) {
				throw new RuntimeException("Playbook referenced activity "
						+ (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)
								? "named " + activityMap
										.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)
								: "")
						+ (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)
								? " in category "
										+ activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)
								: "")
						+ " referencing document "
						+ activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)
						+ " is a Playbook. Playbook activity cannot reference same or other Playbook.");
			}
		}
	}
}