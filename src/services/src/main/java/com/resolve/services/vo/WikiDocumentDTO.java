/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.List;

import com.resolve.util.StringUtils;


public class WikiDocumentDTO
{
    private String id;
    private String UName;
    private String UFullname;
    private String UTitle;
    private String USummary;
    private String UContent;
    private List<String> attachments = new ArrayList<String>();;

    public WikiDocumentDTO()
    {
    }

    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    public String getUFullname()
    {
        return UFullname;
    }

    public void setUFullname(String uFullname)
    {
        UFullname = uFullname;
    }

    public String getUTitle()
    {
        return UTitle;
    }

    public void setUTitle(String uTitle)
    {
        UTitle = uTitle;
    }

    public String getUSummary()
    {
        return USummary;
    }

    public void setUSummary(String uSummary)
    {
        USummary = uSummary;
    }

    public String getUContent()
    {
        return UContent;
    }

    public void setUContent(String uContent)
    {
        UContent = uContent;
    }

    public List<String> getAttachments()
    {
        return attachments;
    }

    public void setAttachments(List<String> attachments)
    {
        this.attachments = attachments;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean result = false;
        if (object == null || object.getClass() != getClass())
        {
            result = false;
        }
        else
        {
            WikiDocumentDTO that = (WikiDocumentDTO) object;
            if(StringUtils.isNotEmpty(this.getId()) && StringUtils.isNotEmpty(that.getId()))
            {
                if(this.getId().equalsIgnoreCase(that.getId()))
                {
                    result = true;
                }
            }
            else if(StringUtils.isNotEmpty(this.getUFullname()) && StringUtils.isNotEmpty(that.getUFullname()))
            {
                if(this.getUFullname().equalsIgnoreCase(that.getUFullname()))
                {
                    result = true;
                }
            }
            
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 7 * hash + (StringUtils.isNotEmpty(this.getId()) ? this.getId().hashCode() : 1);
        hash = 7 * hash + (StringUtils.isNotEmpty(this.getUFullname()) ? this.getUFullname().hashCode() : 1);
        return hash;
    }

}
