/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.archive;

import com.resolve.util.Constants;

public class ArchiveOracleTable extends BaseArchiveTable {
    public ArchiveOracleTable() {
        super();
        this.dbType = Constants.DB_TYPE_ORACLE;
    }
} // ArchiveOracleTable

