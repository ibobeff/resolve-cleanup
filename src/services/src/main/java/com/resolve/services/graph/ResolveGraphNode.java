/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph;

import java.io.Serializable;
import java.lang.reflect.Field;

import org.joda.time.DateTime;

import com.resolve.services.util.WikiUtils;
import com.resolve.util.StringUtils;

//@Deprecated
public abstract class ResolveGraphNode implements Serializable
{

    private static final long serialVersionUID = 3097707333544347344L;

    // common header parameters
    public static final String SYS_ID = "sys_id";
    public static final String DISPLAYNAME = "displayName";
//    public static final String TYPE = "type";
    public static final String RESOLVE_GRAPH_ROOT="isResolveGraphRootNode";
    public static final String ORDER = "order";
    public static final String SYS_CREATED_ON = "sys_created_on";
    public static final String SYS_UPDATED_ON = "sys_updated_on";
    public static final String SYS_CREATED_BY = "sys_created_by";
    public static final String SYS_UPDATED_BY = "sys_updated_by";
    public static final String SYS_ORG = "sysOrg";
    public static final String LAST_ACTIVITY_ON = "lastActivityOn";

    public static final String TAG_NAME = "tagname";

    // access control
    public static final String ROLES = "roles";
    public static final String EDIT_ROLES = "editRoles";
    public static final String POST_ROLES = "postRoles";

    // static property parameters
    public static final String DESC = "description";

    public static final String IS_ROOT = "isRoot";
    public static final String IS_ROOT_REF = "isRootRef";

    public static final String LOCK = "lock";

    protected String id;
    protected String name;
    protected String type;
    protected int order;
    protected String sys_id;
    protected long sys_created_on;
    protected long sys_updated_on;
    protected long lastActivityOn;
    protected String sys_created_by;
    protected String sys_updated_by;
    protected String sys_org;

    protected String operation;

    protected String roles = "";
    protected String editRoles = "";
    protected String postRoles = "";

    protected boolean lock;

    protected String description;

    protected boolean isRoot;
    protected boolean isRootRef;

    public enum NODE_TYPE
    {
        RESOLVE_NODE
    }

    public ResolveGraphNode()
    {
    }

    public ResolveGraphNode(String id, String name)
    {
        this.id = id;
        this.sys_id = id;
        this.name = name;
    }

    //
    // ************** getting/setting for header parameters **********
    //
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        if (StringUtils.isNotEmpty(id))
        {
            this.id = id.trim();
        }
        else
        {
            this.id = id;
        }

        if (StringUtils.isEmpty(this.name))
        {
            this.name = id;
        }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNamespace()
    {
        return WikiUtils.extractNamespace(getName());
    }

    /**
     * The metaType =
     *
     * @return
     */
    public String getType()
    {
        return this.type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getOrder()
    {
        return this.order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }

    public long getSys_created_on()
    {
        if(sys_created_on == 0l)
        {
            sys_created_on = System.currentTimeMillis();

        }
        return sys_created_on;
    }

    public void setSys_created_on(long sys_created_on)
    {
        this.sys_created_on = sys_created_on;
    }

    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    public long getSys_updated_on()
    {
        if(sys_updated_on == 0l)
        {
            sys_updated_on = System.currentTimeMillis();
        }
        return sys_updated_on;
    }

    public void setSys_updated_on(long sys_updated_on)
    {
        this.sys_updated_on = sys_updated_on;
    }

    public long getLastActivityOn()
    {
        return this.lastActivityOn;
    }

    public void setLastActivityOn(long lastActivityOn)
    {
        this.lastActivityOn = lastActivityOn;
    }

    public String getSys_created_by()
    {
        return sys_created_by;
    }

    public void setSys_created_by(String sys_created_by)
    {
        this.sys_created_by = sys_created_by;
    }

    public String getSys_updated_by()
    {
        return sys_updated_by;
    }

    public void setSys_updated_by(String sys_updated_by)
    {
        this.sys_updated_by = sys_updated_by;
    }

    //
    // *************** getting/setting for roles **********************
    public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    public String getPostRoles()
    {
        return postRoles;
    }

    public void setPostRoles(String postRoles)
    {
        this.postRoles = postRoles;
    }

    //
    // *************** getting/setting for static properties ******************
    //
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isRoot()
    {
        return this.isRoot;
    }

    public void setIsRoot(boolean isRoot)
    {
        this.isRoot = isRoot;
    }

    public boolean isRootRef()
    {
        return this.isRootRef;
    }

    public void setIsRootRef(boolean isRootRef)
    {
        this.isRootRef = isRootRef;
    }

    public boolean validate() throws Exception
    {
        return true;
    }

    //
    // ******************** Object - Node convertion ********************
    //
//    public void convertObjectToNode(Node node)  throws Exception
//    {
//        boolean isValid = validate();
//        if (isValid)
//        {
//            //mandatory fields for components
//            node.setProperty(SocialFactory.NODE_TYPE, (StringUtils.isNotEmpty(getType()) ? getType() : ""));
//            node.setProperty(ROLES, (StringUtils.isNotEmpty(getRoles()) ? getRoles() : ""));
//            node.setProperty(EDIT_ROLES, (StringUtils.isNotEmpty(getEditRoles()) ? getEditRoles() : ""));
//            node.setProperty(POST_ROLES, (StringUtils.isNotEmpty(getPostRoles()) ? getPostRoles() : ""));
//
//            // sys_id
//            if (StringUtils.isEmpty(getSys_id()))
//            {
//                setId(Long.toString(node.getId()) + "." + Long.toString(sys_updated_on));
//                setSys_id(Long.toString(node.getId()) + "." + Long.toString(sys_updated_on));
//            }
//
//            //update the sysId only if it does not exist
//            if(!node.hasProperty(SYS_ID))
//                node.setProperty(SYS_ID, getSys_id());
//
//
//            // displayName
//            if (StringUtils.isEmpty(name))
//            {
//                setName(getSys_id());
//            }
//            node.setProperty(DISPLAYNAME, name);
//
//            node.setProperty(ORDER, getOrder());
//            node.setProperty(DESC, (StringUtils.isNotEmpty(description)) ? description : "");
//
//            node.setProperty(IS_ROOT, isRoot);
//            node.setProperty(IS_ROOT_REF, isRootRef);
//
//            //set the lock property
//            node.setProperty(LOCK, isLock());
//
//            //set the sys fields
//            convertObjectToNodeForSysFields(node);
//
//        }
//
//    }
//
//    protected void convertObjectToNodeForSysFields(Node node)  throws Exception
//    {
//        // sys_id - if not there, than add it - possibly only when insert is happening
//        if (!node.hasProperty(SYS_ID))
//        {
//            node.setProperty(SYS_ID, getSys_id());
//        }
//
//        // sys_created_on - if not there, than add it - possibly only when insert is happening
//        if (!node.hasProperty(SYS_CREATED_ON))
//        {
//            node.setProperty(SYS_CREATED_ON, DateManipulator.GetUTCDateLong());
//        }
//
//        // sys_updated_on - should always get updated
//        node.setProperty(SYS_UPDATED_ON, DateManipulator.GetUTCDateLong());
//
//        // lastActivityOn
//        node.setProperty(LAST_ACTIVITY_ON, lastActivityOn);
//
//        // sys_created_by
//        if (!node.hasProperty(SYS_CREATED_BY))
//        {
//            node.setProperty(SYS_CREATED_BY, StringUtils.isNotBlank(sys_created_by) ? sys_created_by : "system");
//        }
//
//        // sys_updated_by
//        node.setProperty(SYS_UPDATED_BY, StringUtils.isNotBlank(sys_updated_by) ? sys_updated_by : "system");
//        
//        if(StringUtils.isNotEmpty(sys_org))
//        {
//            node.setProperty(SYS_ORG, sys_org);
//        }
//    }
//
//    public void convertNodeToObject(Node node)
//    {
//        setId((String) node.getProperty(SYS_ID, ""));
//        setName((String) node.getProperty(DISPLAYNAME, ""));
//
//        if(node.hasProperty(SocialFactory.NODE_TYPE))
//        {
//            type = (String) node.getProperty(SocialFactory.NODE_TYPE);
//
//            //only for Runbook, Document and DT, make this id with Type combination to make it unique on the UI
//            if(type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name())
//                            || type.equalsIgnoreCase(SocialRelationshipTypes.RUNBOOK.name())
//                            || type.equalsIgnoreCase(SocialRelationshipTypes.DECISIONTREE.name()))
//            {
//                id = id + "-" + type;
//            }
//        }
//
//        order = (Integer) node.getProperty(ORDER, 0);
//
//
//        roles = (String) node.getProperty(ROLES, "");
//        editRoles = (String) node.getProperty(EDIT_ROLES, "");
//        postRoles = (String) node.getProperty(POST_ROLES, "");
//
//        try
//        {
//            description = (node.getProperty(DESC) == null) ? "" : (String) node.getProperty(DESC);
//        }
//        catch(Exception exp)
//        {
//        }
//
//        isRoot = (Boolean) node.getProperty(IS_ROOT, false);
//        isRootRef = (Boolean) node.getProperty(IS_ROOT_REF, false);
//
//        try
//        {
//            Boolean isLocked = (Boolean) node.getProperty(LOCK);
//            lock = isLocked == null ? false : isLocked;
//        }
//        catch(Exception e)
//        {
//            lock = false;
//        }
//
//        //sys fields
//        convertNodeToObjectForSysFields(node);
//
//    }
//
//    protected void convertNodeToObjectForSysFields(Node node)
//    {
//        sys_id = (String) node.getProperty(SYS_ID);
//        sys_created_on = (Long) node.getProperty(SYS_CREATED_ON, DateTime.now().getMillis());
//        sys_updated_on = (Long) node.getProperty(SYS_UPDATED_ON, DateTime.now().getMillis());
//        lastActivityOn = (Long) node.getProperty(LAST_ACTIVITY_ON, DateTime.now().getMillis());
//        sys_created_by = (String) node.getProperty(SYS_CREATED_BY, "");
//        sys_updated_by = (String) node.getProperty(SYS_UPDATED_BY, "");
//        sys_org = (String) node.getProperty(SYS_ORG, "");
//
//    }

    //
    // **************** define uniq of object ********************************
    @Override
    public int hashCode()
    {
        int hashcode = 1;
        hashcode = 31 * hashcode + ((id == null) ? 0 : id.hashCode());
        return hashcode;
    }

    public boolean equals(Object obj)
    {
        boolean result = true;

        if (this == obj)
        {
            result = true;
        }

        if (obj == null)
        {
            result = false;
        }

        if (getClass() != obj.getClass())
        {
            result = false;
        }

        ResolveGraphNode graphNodeObj = (ResolveGraphNode) obj;

        if (id == null)
        {
            if (graphNodeObj.getSys_id() != null)
            {
                result = false;
            }
        }
        else if (!id.equals(graphNodeObj.getSys_id()))
        {
            result = false;
        }

        return result;
    }

//    public void toNode(Node node)
//    {
//        Class<? extends ResolveGraphNode> c = this.getClass();
//        for (Field field : c.getDeclaredFields())
//        {
//            if (field.getType().isPrimitive())
//            {
//                try
//                {
//                    String name = field.getName();
//                    Object value = field.get(name);
//                    node.setProperty(name, value);
//                }
//                catch (Exception e)
//                {
//                    Log.log.error("Unable to convert to node", e);
//                }
//            }
//        }
//    }
//
//    public void fromNode(Node node)
//    {
//        Class<? extends ResolveGraphNode> c = this.getClass();
//        for (Field field : c.getDeclaredFields())
//        {
//            if (field.getType().isPrimitive())
//            {
//                field.setAccessible(true);
//                try
//                {
//                    String name = field.getName();
//                    Object defaultValue = getDefaultValue(field);
//                    Object value = node.getProperty(name, defaultValue);
//                    field.set(this, value); // might have to cast? value.getClass().asSubclass(field.getType())
//                }
//                catch (Exception e)
//                {
//                    Log.log.error("Unable to parse from node", e);
//                }
//            }
//        }
//    }

    private Object getDefaultValue(Field field)
    {
        Class<?> c = field.getType();
        String name = c.getCanonicalName();

        if (name.equals("Integer"))
        {
            return 0;
        }

        if (name.equals("Long"))
        {
            return DateTime.now().getMillis();
        }

        return "";
    }

    public String getSys_org()
    {
        return sys_org;
    }

    public void setSys_org(String sys_org)
    {
        this.sys_org = sys_org;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public String getOperation()
    {
        return this.operation;
    }

    public boolean isLock()
    {
        return lock;
    }

    public void setLock(boolean locked)
    {
        this.lock = locked;
    }
}
