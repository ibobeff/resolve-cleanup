/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.List;

import com.resolve.services.hibernate.util.vo.ActionTaskInfoVO;

public class UserWorksheetData
{
    public List<ActionTaskInfoVO> data;
    public String problemId;
    public long lastQueryTime;
    public int interval;
    public boolean isCompleted;
}
