/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

import com.resolve.persistence.model.BaseModel;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;

public abstract class Entities implements Runnable, Callable<Collection<String>>
{
    protected static final int RESOLVE_NODE_REC_COUNT = 1;
    
    protected ResolveNodePropertiesVO createProp(String key, String value)
    {
        ResolveNodePropertiesVO prop = new ResolveNodePropertiesVO();
        prop.setUKey(key);
        prop.setUValue(value);

        return prop;
    }

    protected ResolveNodeVO persistNode(ResolveNodeVO node)
    {
        ResolveNodeVO pnode = null;
        
        try
        {
            pnode = GraphUtil.persistNode(node, "admin");
        }
        catch (Exception e)
        {
           Log.log.error("Error persisting node:" + node.getUCompName() + ": Msg:" + e.getMessage());
        }
        
        return pnode;
    }
    
    public Collection<String> call() throws Exception
    {
        run();
        
        return new ArrayList<String>();
    }
    
    protected Collection<ResolveNodeVO> persistNodes(Collection<ResolveNodeVO> nodes, int batchSize)
    {
        Collection<ResolveNodeVO> pnodes = new ArrayList<ResolveNodeVO>();
        
        try
        {
            pnodes = GraphUtil.persistNodes(nodes, "admin", batchSize);
        }
        catch (Exception e)
        {
           Log.log.error("Error persisting batch of " + pnodes.size() + ": Msg:" + e.getMessage());
        }
        
        return pnodes;
    }
    
    protected void copyBaseModelToResolveNode(ResolveNodeVO nodeVO, BaseModel<? extends VO> compBaseModel)
    {
        nodeVO.setSysCreatedOn(compBaseModel.getSysCreatedOn());
        nodeVO.setSysCreatedBy(compBaseModel.getSysCreatedBy());
        nodeVO.setSysUpdatedOn(compBaseModel.getSysUpdatedOn());
        nodeVO.setSysUpdatedBy(compBaseModel.getSysUpdatedBy());
        nodeVO.setSysModCount(compBaseModel.getSysModCount());
        nodeVO.setSysOrg(compBaseModel.getSysOrg());
    }
}
