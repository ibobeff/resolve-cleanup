/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.util.List;

import com.resolve.util.StringUtils;

public class RsCustomTableDTO
{
    private boolean newTable = false;

    private String sys_id;
    private String uname;
    private String udisplayName;
    private String umodelName;
    private String utype;
    private String usource;
    private String udestination;

    private String ureferenceTableNames;
    private String uwikiName;

    private List<RsUIField> fields;
    
    // roles for a form - CSVs - can get it from Form 
    private String viewRoles;
    private String editRoles;
    private String adminRoles;

    private String errorLog = null;

    //validate the mandatory fields
    public boolean validate()
    {
        boolean valid = true;

        if (StringUtils.isEmpty(this.uname))
        {
            errorLog = (errorLog == null) ? "uname" : ", uname";
        }

        if (StringUtils.isEmpty(this.udisplayName))
        {
            errorLog = (errorLog == null) ? "udisplayName" : errorLog + ", udisplayName";
        }

        if (StringUtils.isEmpty(this.umodelName))
        {
            errorLog = (errorLog == null) ? "umodelName" : errorLog + ", umodelName";
        }

        if (StringUtils.isEmpty(this.utype))
        {
            errorLog = (errorLog == null) ? "utype" : errorLog + ", utype";
        }

        return valid;
    }

    public String getErrorLog()
    {
        return errorLog;
    }

    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    public String getUName()
    {
        return uname;
    }

    public void setUName(String uName)
    {
        uname = uName;
    }

    public String getUModelName()
    {
        return umodelName;
    }

    public void setUModelName(String uModelName)
    {
        umodelName = uModelName;
    }

    public String getUDisplayName()
    {
        return udisplayName;
    }

    public void setUDisplayName(String uDisplayName)
    {
        udisplayName = uDisplayName;
    }

    public String getUType()
    {
        return utype;
    }

    public void setUType(String uType)
    {
        utype = uType;
    }

    public String getUSource()
    {
        return usource;
    }

    public void setUSource(String uSource)
    {
        usource = uSource;
    }

    public String getUDestination()
    {
        return udestination;
    }

    public void setUDestination(String uDestination)
    {
        udestination = uDestination;
    }

    public List<RsUIField> getFields()
    {
        return fields;
    }

    public void setFields(List<RsUIField> fields)
    {
        this.fields = fields;
    }

    public boolean isNewTable()
    {
        return newTable;
    }

    public void setNewTable(boolean isNewTable)
    {
        this.newTable = isNewTable;
    }

    public String getUreferenceTableNames()
    {
        return ureferenceTableNames;
    }

    public void setUreferenceTableNames(String ureferenceTableNames)
    {
        this.ureferenceTableNames = ureferenceTableNames;
    }

    public String getUwikiName()
    {
        return uwikiName;
    }

    public void setUwikiName(String uwikiName)
    {
        this.uwikiName = uwikiName;
    }

    public String getViewRoles()
    {
        return viewRoles;
    }

    public void setViewRoles(String viewRoles)
    {
        this.viewRoles = viewRoles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    public String getAdminRoles()
    {
        return adminRoles;
    }

    public void setAdminRoles(String adminRoles)
    {
        this.adminRoles = adminRoles;
    }
    

}
