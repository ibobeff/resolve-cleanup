/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.HashMap;
import java.util.Map;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
/**
 * used to execute a Runbook or actiontask from UI
 * 
 * @author jeet.marwah
 *
 */
public class ExecuteDTO
{
    private String wiki;//runbook fullname
    private String actiontask;//actiontask fullname
    private String actiontaskSysId;//actiontask sysId
    private String mockName;
    private String problemId = "NEW";//valid values are NEW, ACTIVE, LOOKUP
    private String problemNumber;
    private String processId;
    private String redirectUrl;
    private String redirectWiki;//fullname of the wiki to be redirected to 
    private String reference;
    private String token;
    private String username;
    private String query;//additional params for the redirect url
    private String action = "EXECUTEPROCESS";//default value, valid values are EXECUTETASK, TASK, EXECUTERESULT, EXECUTEPROCESS
    private String eventType;//valid values are WEBSERVICE, RSVIEW
    
    private Map<String, Object> params;//additional params that need to be passed, eg. Form elements with name-value getting submitted to runbook
    private boolean isDebug = false;//default is execution
    
    private String rsviewIdGuid; // Authenticatiing RSView id
    
    public ExecuteDTO() {}
    
    public ExecuteDTO(Map<String, Object> params) 
    {
        setMap(params);
    }
    
    public Map<String, Object> getMap()
    {
        // data example --> {WIKI=Runbook.WebHome, PROBLEMID=NEW, ACTION=EXECUTEPROCESS, USERID=admin}
        Map<String, Object> result = new HashMap<String, Object>();

        if (getWiki() != null) result.put(Constants.HTTP_REQUEST_WIKI, getWiki());
        if (getActiontask() != null) result.put(Constants.EXECUTE_ACTIONNAME, getActiontask());
        if (getActiontaskSysId() != null) result.put(Constants.EXECUTE_ACTIONID, getActiontaskSysId());
        if (getMockName() != null) result.put(Constants.EXECUTE_PROCESS_MOCK, getMockName());

        if (getProblemId() != null) result.put(Constants.HTTP_REQUEST_PROBLEMID, getProblemId());
        if (getProblemNumber() != null) result.put(Constants.HTTP_REQUEST_PROBLEM_NUMBER, getProblemNumber());
        if (getProcessId() != null) result.put(Constants.HTTP_REQUEST_PROCESSID, getProcessId());
        if (getRedirectUrl() != null) result.put(Constants.HTTP_REQUEST_REDIRECT, getRedirectUrl());
        if (getRedirectWiki() != null) result.put(Constants.HTTP_REQUEST_REDIRECT_WIKI, getRedirectWiki());
        if (getReference() != null) result.put(Constants.HTTP_REQUEST_REFERENCE, getReference());
        if (getToken() != null) result.put(Constants.HTTP_REQUEST_TOKEN, getToken());
        if (getQuery() != null) result.put(Constants.HTTP_REQUEST_QUERY, getQuery());
        if (getAction() != null) result.put(Constants.HTTP_REQUEST_ACTION, getAction());
        result.put(Constants.EXECUTE_PROCESS_DEBUG, isDebug() + "");

        // if(getUsername() != null) result.put(Constants.HTTP_REQUEST_USERNAME, getUsername());
        if (getUsername() != null)
        {
            result.put(Constants.EXECUTE_USERID, getUsername());
            if (params!=null && StringUtils.isNotEmpty((String)params.get(Constants.EXECUTE_USERID)))
            {
                result.put(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID, getUsername());
            }
         }

        if (getEventType() != null) result.put(Constants.ESB_PARAM_EVENTTYPE, getEventType());
        if (getParams() != null) result.putAll(getParams());
        if (getRsviewIdGuid() != null) result.put(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID, getRsviewIdGuid());

        return result;
    }
    
    public void setMap(Map<String, Object> params)
    {
        setWiki(params.get(Constants.HTTP_REQUEST_WIKI)!= null ? (String) params.get(Constants.HTTP_REQUEST_WIKI) : null);
        setActiontask(params.get(Constants.EXECUTE_ACTIONNAME)!= null ? (String) params.get(Constants.EXECUTE_ACTIONNAME) : null);
        setActiontaskSysId(params.get(Constants.EXECUTE_ACTIONID)!= null ? (String) params.get(Constants.EXECUTE_ACTIONID) : null);
        setMockName(params.get(Constants.EXECUTE_PROCESS_MOCK)!= null ? (String) params.get(Constants.EXECUTE_PROCESS_MOCK) : null);
        
        setProblemId(params.get(Constants.HTTP_REQUEST_PROBLEMID)!= null ? (String) params.get(Constants.HTTP_REQUEST_PROBLEMID) : null);
        setProblemNumber(params.get(Constants.HTTP_REQUEST_PROBLEM_NUMBER)!= null ? (String) params.get(Constants.HTTP_REQUEST_PROBLEM_NUMBER) : null);
        
        setProcessId(params.get(Constants.HTTP_REQUEST_PROCESSID)!= null ? (String) params.get(Constants.HTTP_REQUEST_PROCESSID) : null);
        setRedirectUrl(params.get(Constants.HTTP_REQUEST_REDIRECT)!= null ? (String) params.get(Constants.HTTP_REQUEST_REDIRECT) : null);
        setRedirectWiki(params.get(Constants.HTTP_REQUEST_REDIRECT_WIKI)!= null ? (String) params.get(Constants.HTTP_REQUEST_REDIRECT_WIKI) : null);
        setReference(params.get(Constants.HTTP_REQUEST_REFERENCE)!= null ? (String) params.get(Constants.HTTP_REQUEST_REFERENCE) : null);
        setToken(params.get(Constants.HTTP_REQUEST_TOKEN)!= null ? (String) params.get(Constants.HTTP_REQUEST_TOKEN) : null);
        setQuery(params.get(Constants.HTTP_REQUEST_QUERY)!= null ? (String) params.get(Constants.HTTP_REQUEST_QUERY) : null);
        setAction(params.get(Constants.HTTP_REQUEST_ACTION)!= null ? (String) params.get(Constants.HTTP_REQUEST_ACTION) : null);
        setEventType(params.get(Constants.ESB_PARAM_EVENTTYPE)!= null ? (String) params.get(Constants.ESB_PARAM_EVENTTYPE) : null);
        setUsername(params.get(Constants.HTTP_REQUEST_USERNAME)!= null ? (String) params.get(Constants.HTTP_REQUEST_USERNAME) : (params.get(Constants.EXECUTE_USERID)!= null ? (String) params.get(Constants.EXECUTE_USERID) : null));
        
        setRsviewIdGuid(params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID) != null ? (String) params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID) : null);
        
        //TODO: no handle for the params map right now. We can add it when need arises.        

    }

    public String getWiki()
    {
        return wiki;
    }

    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }

    public String getActiontask()
    {
        return actiontask;
    }

    public void setActiontask(String actiontask)
    {
        this.actiontask = actiontask;
    }

    public String getProblemId()
    {
        return problemId;
    }

    public void setProblemId(String problemId)
    {
        this.problemId = problemId;
    }

    public String getProblemNumber()
    {
        return problemNumber;
    }

    public void setProblemNumber(String problemNumber)
    {
        this.problemNumber = problemNumber;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setProcessId(String processId)
    {
        this.processId = processId;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

    public String getRedirectWiki()
    {
        return redirectWiki;
    }

    public void setRedirectWiki(String redirectWiki)
    {
        this.redirectWiki = redirectWiki;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getEventType()
    {
        String result = null;
        
        if(StringUtils.isNotEmpty(eventType))
        {
            if(eventType.equalsIgnoreCase(Constants.GATEWAY_EVENT_TYPE_RSVIEW))
            {
                result = Constants.GATEWAY_EVENT_TYPE_RSVIEW;
            }
            else if(eventType.equalsIgnoreCase(Constants.GATEWAY_EVENT_TYPE_WEBSERVICE))
            {
                result = Constants.GATEWAY_EVENT_TYPE_WEBSERVICE;
            }
        }
        
        return result;
    }

    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }

    public Map<String, Object> getParams()
    {
        return params;
    }

    public void setParams(Map<String, Object> params)
    {
        this.params = params;
    }

    public boolean isDebug()
    {
        return isDebug;
    }

    public void setIsDebug(boolean isDebug)
    {
        this.isDebug = isDebug;
    }

    public String getMockName()
    {
        return mockName;
    }

    public void setMockName(String mockName)
    {
        this.mockName = mockName;
    }

    public String getActiontaskSysId()
    {
        return actiontaskSysId;
    }

    public void setActiontaskSysId(String actiontaskSysId)
    {
        this.actiontaskSysId = actiontaskSysId;
    }
    
    public String getRsviewIdGuid()
    {
        return rsviewIdGuid;
    }

    public void setRsviewIdGuid(String rsviewIdGuid)
    {
        this.rsviewIdGuid = rsviewIdGuid;
    }
}
