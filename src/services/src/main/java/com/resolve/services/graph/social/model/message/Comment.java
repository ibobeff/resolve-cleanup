/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.message;

import java.io.Serializable;
import java.util.Map;

import com.resolve.services.constants.ServiceSocialConstants;


public class Comment implements Serializable
{   
    private static final long serialVersionUID = -8248638256401283582L;
    
    private String sys_id="";
    private String _ui_id;
    protected long sys_created_on;
    private String userName;
    private String commentText; 
    private String displayName = "";
    
    private String timezone;
    private boolean offset = false;
    public static final String COMMENT_TYPE = "COMMENT";
    
    public Comment()
    {
    }
    
    @SuppressWarnings("rawtypes")
    public Comment(Map params)
    {
        setUserName((String)params.get(ServiceSocialConstants.PARAMS_USERNAME));
        setCommentText((String)params.get(ServiceSocialConstants.PARAMS_COMMENT));
    }
    
    /**
     * @return the sys_id
     */
    public String getSys_id()
    {
        return sys_id;
    }
    /**
     * @param sys_id the sys_id to set
     */
    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
    
    public String get_ui_id()
    {
        return _ui_id;
    }

    public void set_ui_id(String _ui_id)
    {
        this._ui_id = _ui_id;
    }

    public long getSys_created_on()
    {
        return this.sys_created_on;
    }
    
    public void setSys_created_on(long sys_created_on)
    {
        this.sys_created_on = sys_created_on;
    }
    
    public String getUserName()
    {
        return this.userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public void setCommentText(String commentText)
    {
        this.commentText = commentText;
    }
    
    public String getComment()
    {
        return commentText;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    /**
     * @return the timezone
     */
    public String getTimezone()
    {
        return timezone;
    }

    /**
     * @param timezone the timezone to set
     */
    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    /**
     * @return the offset
     */
    public boolean isOffset()
    {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    public void setOffset(boolean offset)
    {
        this.offset = offset;
    }

}
