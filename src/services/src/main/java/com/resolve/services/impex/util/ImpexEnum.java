package com.resolve.services.impex.util;

public enum ImpexEnum
{
    ERROR("ERROR"),
    ERROR_RETRIEVING_COMPONENT_DETAILS("Error while retrieving component details."),
    ERROR_LISTING_IMPEX_DEFINITIONS("Error while listing Import/Export definitions"),
    IMPEX_MODEL("ResolveImpexModule"),
    IMPEX_DEFINITION_WHERE_CLAUSE("(UDirty = false OR UDirty is null)"),
    DEFINITION_LIST_COLUMNS("sys_id,UName,UZipFileName,UDescription,UDirty,UVersion,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy"),
    DEFINITION_LIST_NEW_COLUMNS("isNew"),
    DEFINITION_SELECT_SQL("select sys_id,UName,UDescription,UForwardWikiDocument,UScriptName,UZipFileName,UVersion,manifestGraph,excludeList,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy from ResolveImpexModule "),
    IMPEX_GLIDE_SELECT_SQL("select sys_id, UType, UModule, UName, UDescription, UOptions, UScan, sysCreatedOn, sysCreatedBy, sysUpdatedOn, sysUpdatedBy from ResolveImpexGlide where resolveImpexModule.sys_id = :sysId"),
    IMPEX_WIKI_SELECT_SQL("select sys_id, UType, UValue, UScan, UOptions, UDescription, sysCreatedOn, sysCreatedBy, sysUpdatedOn, sysUpdatedBy from ResolveImpexWiki where resolveImpexModule.sys_id = :sysId"),
    ERROR_DELETING_IMPEX_DEFINITIONS("Error while deleting Import/Export definition(s)"),
    ERROR_GETTING_IMPEX_DEFINITION("Error getting Impex definition details."),
    COMMA(","),
    COLON(":"),
    DASH("-"),
    EQUAL("="),
    AMPERCENT("&"),
    QUESTION("?"),
    WIKI("wiki"),
    RUNBOOK("runbook"),
    FORM("form"),
    TASK("task"),
    SCRIPT("script"),
    PROPERTIES("properties"),
    ERROR_CODE("ERROR_CODE"),
    MANIFEST_GRAPH("MANIFEST_GRAPH"),
    MESSAGE("MESSAGE"),
    NAME("NAME"),
    MODULEID("moduleId"),
    FILENAME("filename"),
    IMPEX_DOWNLOAD_URL("/resolve/service/wiki/impex/download"),
    DECISIONTREE("decisiontree"),
    READ_MANIFEST_RECORDS("READ_MANIFEST_RECORDS"),
    REMOVE_SYS_COLUMNS_REGEX("<(id|property name=\"sys_).*?(\\/>)|(<\\/id>)"),
    IMPEX_LOG_ERROR_MARKER("<font face=\"courier new\" color=\"red\" size=\"2\">"),
    CONTENT_ERROR("Content error:"),
    SECURITY_TEMPLATE("securityTemplate"),
    RID_MAPPING("ridMapping"),
    ARTIFACT_ACTION("artifactAction"),
    ARTIFACT_CEF_KEY("cefKeys"),
    DatabaseConnectionPool ("DatabaseConnectionPool"),
    EmailAddress("EmailAddress"),
    EWSAddress("EWSAddress"),
    RemedyxForm("RemedyxForm"),
    XMPPAddress("XMPPAddress"),
    MSGGatewayFilter("MSGGatewayFilter"),
    PushGatewayFilter("PushGatewayFilter"),
    PullGatewayFilter("PullwayFilter"),
    SSH("SSH"),
    TELNET("TELNET"),
    SSHPool("SSHPool"),
    TelnetPool("TelnetPool"),
    companion("companion"),
    filters("filters"),
    default_queue("<|Default|Queue|>"),
    DatabaseConnectionPoolVO("DatabaseConnectionPoolVO"),
    EmailAddressVO("EmailAddressVO"),
    EWSAddressVO("EWSAddressVO"),
    RemedyxFormVO("RemedyxFormVO"),
    XMPPAddressVO("XMPPAddressVO"),
    SSHPoolVO("SSHPoolVO"),
    TelnetPoolVO("TelnetPoolVO"),
    id("id"),
    checksum("checksum"),
    filterModel("filterModel"),
    type("type");
    
    private final String value;
    
    private ImpexEnum(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
}
