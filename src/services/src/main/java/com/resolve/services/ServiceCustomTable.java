/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.Collection;
import java.util.List;

import com.resolve.dto.ViewLookupTableDTO;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.services.hibernate.customtable.DeleteCustomTableRecords;
import com.resolve.services.hibernate.util.vo.RsMetaFilterDTO;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.MetaFieldPropertiesVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;

/**
 * Custom table and form related apis
 * 
 * @author jeet.marwah
 *
 */
public class ServiceCustomTable
{
    ////////////////////////////////////// Custom Table apis   /////////////////////////////////////////////////////////
    public static ResponseDTO<CustomTableVO> getCustomTables(QueryDTO query, String username) throws Exception
    {
        return CustomTableUtil.getCustomTables(query, username);
    }

    public static CustomTableVO findCustomTableWithFields(String sysId, String name, String username) throws Exception
    {
        return CustomTableUtil.findCustomTableWithFields(sysId, name, username);
    }
    
    public static CustomTableVO saveCustomTable(CustomTableVO customTableVO, String username) throws Exception
    {
        return CustomTableUtil.saveCustomTable(customTableVO, username);
    }
    
    public static void deleteCustomTableByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {
        CustomTableUtil.deleteCustomTableByIds(sysIds, deleteAll, username);
    }
    
    public static void deleteCustomTableData(String tableName, Collection<String> sysIds, String username) throws Exception
    {
        new DeleteCustomTableRecords(tableName, sysIds, username).delete();
    }
    
    public static List<CustomTableVO> getAllCustomTables(String username)  throws Exception
    {
        return CustomTableUtil.getAllCustomTables(username);
    }
    
    public static Collection<MetaFieldVO> getColumnsForTable(String tableSysId, String tableModelName, String username)  throws Exception
    {
        return CustomTableUtil.getColumnsForTable(tableSysId, tableModelName, username);
    }

    public static CustomTableVO getCustomTableWithReferences(String tableSysId, String tableName, String username)  throws Exception
    {
        return  CustomTableUtil.getCustomTableWithReferences(tableSysId, tableName, username);
    }
    
    public static List<MetaFieldPropertiesVO> getReferenceTables(String tableName, String username)  throws Exception
    {
        return  CustomTableUtil.getReferenceTables(tableName, username);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    public static ViewLookupTableDTO saveViewLookup(ViewLookupTableDTO viewLookup, String username)  throws Exception
    {
        return  CustomTableUtil.saveViewLookup(viewLookup, username);
    }

    public static void updateMetaViewLookupOrder(List<String> sysIds, String username)  throws Exception
    {
        CustomTableUtil.updateMetaViewLookupOrder(sysIds, username);
    }

    //not used anywhere
//    public static RsMetaFilterDTO getFilter(String filterName, String sysId, String username)  throws Exception
//    {
//        return  CustomTableUtil.getFilter(filterName, sysId, username);
//    }

    public static List<RsMetaFilterDTO> getUserFilters(String tablename, String username)  throws Exception
    {
        return CustomTableUtil.getUserFilters(tablename, username);
    }

    public static RsMetaFilterDTO saveMetaFilter(RsMetaFilterDTO filterDTO, String username)  throws Exception
    {
        return  CustomTableUtil.saveMetaFilter(filterDTO, username);
    }

    //not used anywhere
//    public static List<MetaViewLookupVO> getViewLookupForApp(String appName, String username)  throws Exception
//    {
//        return  null;//CustomTableUtil.getViewLookupForApp(appName, username);
//    }


}
