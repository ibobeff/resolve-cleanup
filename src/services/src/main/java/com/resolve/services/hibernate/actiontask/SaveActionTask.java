/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.actiontask;

import static org.apache.commons.lang.exception.ExceptionUtils.getRootCauseMessage;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.hibernate.DuplicateMappingException.Type.ENTITY;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.DuplicateMappingException;
import org.hibernate.Session;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ActionTaskArchive;
import com.resolve.persistence.model.ActionTaskArchiveBuilder;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionInvocOptions;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskMockData;
import com.resolve.persistence.model.ResolveActionTaskResolveTagRel;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.ResolveTaskExpression;
import com.resolve.persistence.model.ResolveTaskOutputMapping;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceTag;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.menu.MenuDetails;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.AssessorUtil;
import com.resolve.services.hibernate.util.ParserUtil;
import com.resolve.services.hibernate.util.PreprocessorUtil;
import com.resolve.services.hibernate.util.ResolveArchiveUtils;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.hibernate.vo.ResolvePreprocessVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.wiki.EvaluateAccessRights;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.ERR;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SaveActionTask
{
	private static final String ACTION_TASK_LC = "action task";
	
    private ResolveActionTask uiTask = null;
    private ResolveActionTask dbTask = null;
    private String username = null;
    private Set<String> tags = new HashSet<String>();
    
    private AccessRights uiAtAccessRights = null;
    private ResolveActionInvoc uiResolveActionInvoc = null;
    private ResolvePreprocess uiPreprocess;   
    private ResolveParser uiParser;           
    private ResolveAssess uiAssess;
    
    private Collection<ResolveActionInvocOptions> uiResolveActionInvocOptions;
    private Collection<ResolveActionParameter> uiResolveActionParameters;
    private Collection<ResolveActionTaskMockData> uiMockDataCollection;
    
    private Collection<ResolveTaskOutputMapping> uiOutputMappings;
    private Collection<ResolveTaskExpression> uiExpressions;
    
    private String rawContent;
    private String comment;
    private VersionPersistAction versionPersistAction;
    
	public SaveActionTask(ResolveActionTaskVO vo, String username) throws Exception {
		this.username = username;

		validateVO(vo);

		if (vo.getTags() != null) {
			this.tags.addAll(vo.getTags());
		}
		this.uiTask = new ResolveActionTask(vo);

	}

	public SaveActionTask(ResolveActionTaskVO vo, String rawContent, String username, String comment, VersionPersistAction versionPersistAction) throws Exception {
		this.username = username;

		validateVO(vo);

		if (vo.getTags() != null) {
			this.tags.addAll(vo.getTags());
		}
		this.uiTask = new ResolveActionTask(vo);
		this.rawContent = rawContent;
		this.comment = comment;
		this.versionPersistAction = versionPersistAction;
	}

	private void validateVO(ResolveActionTaskVO vo) throws Exception {
		if (vo == null || vo.getResolveActionInvoc() == null || StringUtils.isEmpty(username)) {
			throw new Exception("Actiontask object,  invoc and Username are mandatory");
		}
		vo.validate();
	}
    
    //save it
    public ResolveActionTaskVO save()throws Exception
    {
    	// Check for expired V2 license in NON-PROD environments/instances 
    	
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndExpiredAndNonProd() &&
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10024.getMessage().replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
    														  ACTION_TASK_LC + SaveHelper.SPACE + uiTask.getUFullName());
    		Log.alert(ERR.E10024.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    	
    	// Check for User Count violated V2 license in NON-PROD environments/instances
    	
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndNonProd() &&
    		MainBase.main.getLicenseService().getIsUserCountViolated() &&	
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10031.getMessage().replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
    														  ACTION_TASK_LC + SaveHelper.SPACE + uiTask.getUFullName());
    		
    		Log.alert(ERR.E10031.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    	
    	// Check for HA violated V2 license in NON-PROD environments/instances
    	
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndNonProd() &&
    		MainBase.main.getLicenseService().getIsHAViolated() &&	
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10030.getMessage().replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
    														  ACTION_TASK_LC + SaveHelper.SPACE + uiTask.getUFullName());
    		
    		Log.alert(ERR.E10030.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    	
        saveResolveActionTaskModel();
        ResolveActionTaskVO taskVO = ActionTaskUtil.getActionTaskWithReferences(uiTask.getSys_id(), null, username);
        ActionTaskUtil.updateTaskChecksum(taskVO, username);
        return taskVO; 
    }
    
    private void saveResolveActionTaskModel() throws Exception
    {
        String sysId = this.uiTask.getSys_id();
        if(StringUtils.isNotBlank(sysId))
        {
            update();
        }
        else
        {
            insert();
        }
    }
    
    
    private void insert() throws Exception
    {
        String fullName = this.uiTask.getUFullName();
        
        //make sure that actiontask does not exist with the same name
        ResolveActionTask dbAt = ActionTaskUtil.getResolveActionTaskModel(null, fullName, false, username);
        if(dbAt != null)
        {
            throw new Exception("Actiontask with name '" + fullName + "' already exist.");
        }
        
        //update the DB
        upsert();
    }
    
    
    private void update() throws Exception
    {
        String sysId = this.uiTask.getSys_id();

        //make sure that actiontask does not exist with the same name
        dbTask = ActionTaskUtil.validateUserForActionTaskNoCache(sysId, null, RightTypeEnum.edit, username);
        
        //update the DB
        upsert();
    }
    
    //common operations for insert and update
    private void upsert() throws Exception
    {
        //update the access rights
        EvaluateAccessRights.updateAccessRightsFor(uiTask, username);
        
        uiAtAccessRights = this.uiTask.getAccessRights();
        uiResolveActionInvoc = this.uiTask.getResolveActionInvoc();
        
        uiPreprocess = this.uiTask.getResolveActionInvoc().getPreprocess();
        uiParser = this.uiTask.getResolveActionInvoc().getParser();
        uiAssess = this.uiTask.getResolveActionInvoc().getAssess();
        
        uiExpressions = this.uiTask.getResolveActionInvoc().getExpressions();
        uiOutputMappings = this.uiTask.getResolveActionInvoc().getOutputMappings();
        
        uiResolveActionInvocOptions = this.uiTask.getResolveActionInvoc().getResolveActionInvocOptions();
        uiResolveActionParameters = this.uiTask.getResolveActionInvoc().getResolveActionParameters();
        uiMockDataCollection = this.uiTask.getResolveActionInvoc().getMockDataCollection();
        
        validate();
        
        //first update/create dependent references - else it throws hibernate exception if done later 
        //update preprocessor
        updatePreprocessor();

        //update parser
        updateParser();
        
        //update assessor
        updateAssessor();

        try {
              HibernateProxy.setCurrentUser(username);
        	HibernateProxy.executeNoCache(() -> {
				// update the actiontask model
				updateResolveActionTaskModel();

				// just create the rec in the db
				updateActionTaskInvoc();
				
				// update options - including the CONTENT with REvision
				updateOptions();
        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            
            if(e instanceof DuplicateMappingException) {
            	throw e;
            }
            throw new Exception(e);
        }
        
        //update the main actiontask record 
		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.executeNoCache(() -> {
				// update invoc
				updateActionTaskInvocReferences();

				// update parameters
				updateParameters();

				// Update TaskOutput Metadata
				updateTaskOutputMetadata();

				// update mock
				updateMock();

				// update Tags
				updateTagsForThisActiontask();

            // TODO: uncomment after 6.4
//            updateActionTaskVersion();

			});

		}
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            
            if(e instanceof DuplicateMappingException) {
            	throw e;
            }
            throw new Exception(e);
        }
        
        //social comp creation and other operations 
        socialOperations();
        
        //indexing
        ActionTaskIndexAPI.indexActionTask(uiTask.getSys_id(), username);
        
        //invalidate the actiontask
        invalidateActionTask();
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void updateActionTaskVersion() throws Exception {
	    if(this.versionPersistAction.equals(VersionPersistAction.UPDATE)){
	        updateActionTaskVersions();
	    } else {
	        createActiontaskVersion();
	    }
	}

    private void updateActionTaskVersions() throws Exception
    {
        List<ActionTaskArchive> savedVersions = getVersionsForTask(uiTask.getSys_id());
        List<ActionTaskArchive> updatedATVersions = new ArrayList<>();
        for(ActionTaskArchive actionTaskVersion : savedVersions) {
            ObjectMapper mapper = new ObjectMapper();
            ResolveActionTaskVO updatedActionTask = mapper.readValue(actionTaskVersion.getUContent(), ResolveActionTaskVO.class);
            updatedActionTask.setUFullName(this.uiTask.getUFullName());
            updatedActionTask.setUName(this.uiTask.getUName());
            updatedActionTask.setUNamespace(this.uiTask.getUNamespace());
            updatedActionTask.getResolveActionInvoc().getPreprocess().setUName(this.uiTask.getResolveActionInvoc().getPreprocess().getUName());
            updatedActionTask.getResolveActionInvoc().getParser().setUName(this.uiTask.getResolveActionInvoc().getParser().getUName());
            updatedActionTask.getResolveActionInvoc().getAssess().setUName(this.uiTask.getResolveActionInvoc().getAssess().getUName());
            
            String updatedContent = mapper.writeValueAsString(updatedActionTask);
            actionTaskVersion.setUContent(updatedContent);
            updatedATVersions.add(actionTaskVersion);
        }                             
        HibernateUtil.getDAOFactory().getResolveActionTaskArchiveDAO().update(updatedATVersions);     
    }

    private List<ActionTaskArchive> getVersionsForTask(String taskId) throws Exception {
        CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<ActionTaskArchive> criteriaQuery = criteriaBuilder.createQuery(ActionTaskArchive.class);                  
        Root<ActionTaskArchive> from = criteriaQuery.from(ActionTaskArchive.class);
        criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("UTableId"), taskId));
        return HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();
    }
    
    private void createActiontaskVersion() throws DuplicateMappingException, Exception
    {
		Session session = HibernateUtil.getCurrentSession();
		int version = (int) session
				.createQuery("SELECT max(ata.UVersion) FROM ActionTaskArchive AS ata WHERE ata.UTableId = :UTableId")
				.setParameter("UTableId", uiTask.getSys_id())
				.uniqueResultOptional()
				.orElse(0);

		try {
			ActionTaskArchiveBuilder builder = new ActionTaskArchiveBuilder(++version, rawContent, uiTask.getSys_id(),
					comment);
			builder.setUIsStable(this.versionPersistAction.equals(VersionPersistAction.COMMIT) ? false : true);
			
			session.persist(builder.build());
		} catch (Exception e) {
			if (containsIgnoreCase(getRootCauseMessage(e), "Duplicate entry")) {
				throw new DuplicateMappingException(ENTITY,
						String.format(
								"Cannot create version - there's already a document with version: '%s' for ID: '%s'",
								version, uiTask.getSys_id()));
			}
			throw e;
		}
	}
    
    private void updateResolveActionTaskModel() throws Exception
    {
        if (StringUtils.isNotBlank(this.uiTask.getSys_id()))
        {
            uiTask.setSysCreatedBy(this.dbTask.getSysCreatedBy());
            uiTask.setSysCreatedOn(this.dbTask.getSysCreatedOn());
            
            AccessRights dbAr = this.dbTask.getAccessRights();
            if(dbAr != null)
            {
                uiAtAccessRights.setSysCreatedBy(dbAr.getSysCreatedBy());
                uiAtAccessRights.setSysCreatedOn(dbAr.getSysCreatedOn());
            }
            else 
            {
                uiAtAccessRights.setSysCreatedBy(this.dbTask.getSysCreatedBy());
                uiAtAccessRights.setSysCreatedOn(this.dbTask.getSysCreatedOn());
            }
            uiAtAccessRights.setUResourceId(this.uiTask.getSys_id());
            uiAtAccessRights.setUResourceName(this.uiTask.getUFullName());
            uiAtAccessRights.setUResourceType(ResolveActionTask.RESOURCE_TYPE);
            
            
            HibernateUtil.getDAOFactory().getResolveActionTaskDAO().update(uiTask);
            HibernateUtil.getDAOFactory().getAccessRightsDAO().persist(uiAtAccessRights);
        }
        else
        {
            //this will be insert - so see if there is a sys_id with the import
            this.uiTask.setSys_id(null);
            HibernateUtil.getDAOFactory().getResolveActionTaskDAO().persist(this.uiTask);
            
            uiAtAccessRights.setUResourceId(this.uiTask.getSys_id());
            uiAtAccessRights.setUResourceName(this.uiTask.getUFullName());
            uiAtAccessRights.setUResourceType(ResolveActionTask.RESOURCE_TYPE);
            HibernateUtil.getDAOFactory().getAccessRightsDAO().persist(uiAtAccessRights);
        }

        //make sure that the execute roles are updated as its used by RSCONTROL
        this.uiTask.setURoles(prepareUniqueList(this.uiAtAccessRights.getUExecuteAccess() + "," + this.uiAtAccessRights.getUAdminAccess()));
        this.uiTask.setAccessRights(uiAtAccessRights);
        HibernateUtil.getDAOFactory().getResolveActionTaskDAO().update(this.uiTask);
    }
    
    private void updateActionTaskInvoc() throws Exception
    {
        if(this.dbTask != null && this.dbTask.getResolveActionInvoc() != null)
        {
            ResolveActionInvoc dbResolveActionInvoc = this.dbTask.getResolveActionInvoc();
            uiResolveActionInvoc.setSys_id(dbResolveActionInvoc.getSys_id());
            uiResolveActionInvoc.setSysCreatedBy(dbResolveActionInvoc.getSysCreatedBy());
            uiResolveActionInvoc.setSysCreatedOn(dbResolveActionInvoc.getSysCreatedOn());
            HibernateUtil.getDAOFactory().getResolveActionInvocDAO().update(uiResolveActionInvoc);
        }  
        else
        {
            //this will be insert
            this.uiResolveActionInvoc.setSys_id(null);
            HibernateUtil.getDAOFactory().getResolveActionInvocDAO().persist(uiResolveActionInvoc);
        }
        
        //update the ref to action task
        this.uiResolveActionInvoc.setUDescription(this.uiTask.getUFullName());
        this.uiTask.setResolveActionInvoc(uiResolveActionInvoc);
        HibernateUtil.getDAOFactory().getResolveActionTaskDAO().update(this.uiTask);
    }
    
    private void updateActionTaskInvocReferences() throws Exception
    {
        uiResolveActionInvoc.setAssess(this.uiAssess);
        uiResolveActionInvoc.setParser(this.uiParser);
        uiResolveActionInvoc.setPreprocess(this.uiPreprocess);
        HibernateUtil.getDAOFactory().getResolveActionInvocDAO().persist(uiResolveActionInvoc);
    }
    
    private void updatePreprocessor() throws Exception
    {
        if(this.uiPreprocess == null)
        {
            ResolvePreprocessVO dbPreprocessor = PreprocessorUtil.findResolvePreprocessNoCache(null, this.uiTask.getUFullName(), username);
            if(dbPreprocessor != null)
            {
                this.uiPreprocess = new ResolvePreprocess(dbPreprocessor);
            }
            else
            {
                this.uiPreprocess = new ResolvePreprocess();
            }
        }
        //for a case when the object is not null but the values are
        else if((StringUtils.isEmpty(this.uiPreprocess.getSys_id()) && StringUtils.isEmpty(this.uiPreprocess.getUName())))
        {
            ResolvePreprocessVO dbPreprocessor = PreprocessorUtil.findResolvePreprocessNoCache(null, this.uiTask.getUFullName(), username);
            if(dbPreprocessor != null)
            {
                ResolvePreprocess preprocessor = new ResolvePreprocess(dbPreprocessor);
                preprocessor.setUScript(this.uiPreprocess.getUScript());

                this.uiPreprocess = preprocessor;
            }
        }

        
        //set the name if its a new object
        if(StringUtils.isEmpty(this.uiPreprocess.getSys_id()))
        {
            this.uiPreprocess.setUName(this.uiTask.getUFullName());
        }
        
        //update only if the name of the preprocessor is same as at name
        if(this.uiTask.getUFullName().equalsIgnoreCase(this.uiPreprocess.getUName()))
        {
//            if(this.dbTask != null && this.dbTask.getResolveActionInvoc() != null && this.dbTask.getResolveActionInvoc().getPreprocess() != null)
//            {
//                //for update
//                ResolvePreprocess dbResolvePreprocess = this.dbTask.getResolveActionInvoc().getPreprocess();
//                uiPreprocess.setSysCreatedBy(dbResolvePreprocess.getSysCreatedBy());
//                uiPreprocess.setSysCreatedOn(dbResolvePreprocess.getSysCreatedOn());
//            }
            
//            this.uiPreprocess = SaveUtil.saveResolvePreprocess(this.uiPreprocess, username);
            
            //TODO: investigate why it throws exception
            ResolvePreprocessVO vo = this.uiPreprocess.doGetVO();
            vo = PreprocessorUtil.saveResolvePreprocess(vo, username);
            this.uiPreprocess.applyVOToModel(vo);
            
//            this.uiPreprocess = HibernateUtil.getDAOFactory().getResolvePreprocessDAO().findById(vo.getSys_id());
        }
    }
    
    private void updateParser() throws Exception
    {
        if(this.uiParser == null)
        {
            ResolveParserVO dbParser = ParserUtil.findResolveParserNoCache(null, this.uiTask.getUFullName(), username);
            if(dbParser != null)
            {
                this.uiParser = new ResolveParser(dbParser);
            }
            else
            {
                this.uiParser = new ResolveParser();
            }
        }
        //for a case when the object is not null but the values are
        else if((StringUtils.isEmpty(this.uiParser.getSys_id()) && StringUtils.isEmpty(this.uiParser.getUName())))
        {
            ResolveParserVO dbParser = ParserUtil.findResolveParserNoCache(null, this.uiTask.getUFullName(), username);
            if(dbParser != null)
            {
                ResolveParser parser = new ResolveParser(dbParser);
                parser.setUScript(this.uiParser.getUScript());
                parser.setUConfiguration(this.uiParser.getUConfiguration());

                this.uiParser = parser;
            }
        }

        //set the name if its a new object
        if(StringUtils.isEmpty(this.uiParser.getSys_id()))
        {
            this.uiParser.setUName(this.uiTask.getUFullName());
        }
        
        //update only if the name of the preprocessor is same as at name
        if(this.uiTask.getUFullName().equalsIgnoreCase(this.uiParser.getUName()))
        {
            //for update
//            if(this.dbTask != null && this.dbTask.getResolveActionInvoc() != null &&  this.dbTask.getResolveActionInvoc().getParser() != null)
//            {
//                ResolveParser dbResolveParser = this.dbTask.getResolveActionInvoc().getParser();
//                uiParser.setSysCreatedBy(dbResolveParser.getSysCreatedBy());
//                uiParser.setSysCreatedOn(dbResolveParser.getSysCreatedOn());
//            }
            
//            this.uiParser = SaveUtil.saveResolveParser(this.uiParser, username);
            
            //TODO: investigate why it throws exception
            ResolveParserVO vo = this.uiParser.doGetVO();
            vo = ParserUtil.saveResolveParser(vo, username);
            this.uiParser.applyVOToModel(vo);
            
//            this.uiParser = HibernateUtil.getDAOFactory().getResolveParserDAO().findById(vo.getSys_id());
        }
    }
    
    private void updateAssessor() throws Exception
    {
        if(this.uiAssess == null)
        {
            ResolveAssessVO dbAssess = AssessorUtil.findResolveAssessNoCache(null, this.uiTask.getUFullName(), username);
            if(dbAssess != null)
            {
                this.uiAssess = new ResolveAssess(dbAssess);
            }
            else
            {
                this.uiAssess = new ResolveAssess();    
            }
        }
        //for a case when the object is not null but the values are
        else if((StringUtils.isEmpty(this.uiAssess.getSys_id()) && StringUtils.isEmpty(this.uiAssess.getUName())))
        {
            ResolveAssessVO dbAssess = AssessorUtil.findResolveAssessNoCache(null, this.uiTask.getUFullName(), username);
            if(dbAssess != null)
            {
                ResolveAssess assess = new ResolveAssess(dbAssess);
                assess.setUScript(this.uiAssess.getUScript());

                this.uiAssess = assess;
            }
        }
        
        //set the name if its a new object
        if(StringUtils.isEmpty(this.uiAssess.getSys_id()))
        {
            this.uiAssess.setUName(this.uiTask.getUFullName());
        }
        
        //update only if the name of the preprocessor is same as at name
        if(this.uiTask.getUFullName().equalsIgnoreCase(this.uiAssess.getUName()))
        {
        	List<ResolveTaskExpression> objsFromUI = new ArrayList<ResolveTaskExpression>();
        	String severityAndConditionScript = null;
        	
        	if (uiExpressions != null)
        	{
        		objsFromUI.addAll(uiExpressions);
        		severityAndConditionScript = ActionTaskUtil.generateSeverityAndConditionScript(objsFromUI);
        	}
        	if (StringUtils.isNotBlank(severityAndConditionScript))
        	{
        		uiAssess.setUExpressionScript(severityAndConditionScript);
        	}
        	
        	List<ResolveTaskOutputMapping> uiOutputMappingList = new ArrayList<ResolveTaskOutputMapping>();
        	String outputMappingScript = null;
        	
        	if (uiOutputMappings != null)
        	{
        		uiOutputMappingList.addAll(uiOutputMappings);
        		outputMappingScript = ActionTaskUtil.generateOutputMappingScript(uiOutputMappingList);
        	}
        	if (StringUtils.isNotBlank(outputMappingScript))
        	{
        		uiAssess.setUOutputMappingScript(outputMappingScript);
        	}
        	
//            //for update
//            if(this.dbTask != null && this.dbTask.getResolveActionInvoc() != null &&  this.dbTask.getResolveActionInvoc().getAssess() != null)
//            {
//                ResolveAssess dbResolveAssess = this.dbTask.getResolveActionInvoc().getAssess();
//                uiAssess.setSysCreatedBy(dbResolveAssess.getSysCreatedBy());
//                uiAssess.setSysCreatedOn(dbResolveAssess.getSysCreatedOn());
//            }
            
//            this.uiAssess = SaveUtil.saveResolveAssess(this.uiAssess, username);
            
          //TODO: investigate why it throws exception
            ResolveAssessVO vo = this.uiAssess.doGetVO();
            vo = AssessorUtil.saveResolveAssess(vo, username);
            this.uiAssess.applyVOToModel(vo);
            
//            this.uiAssess = HibernateUtil.getDAOFactory().getResolveAssessDAO().findById(vo.getSys_id());
        }
    }
    
    private void updateOptions() throws Exception
    {
        ResolveActionInvocOptions contentOption = null; //used for archiving the INPUT/content of this actiontask
        List<ResolveActionInvocOptions> objsFromUI = new ArrayList<ResolveActionInvocOptions>();//used for local manipulation
        if(this.uiResolveActionInvocOptions != null)
        {
            objsFromUI.addAll(uiResolveActionInvocOptions);
        }
        
        if (dbTask != null)
        {
            try
            {
                    
                    Collection<ResolveActionInvocOptions> dbResolveActionInvocOptions = dbTask.getResolveActionInvoc() != null ? dbTask.getResolveActionInvoc().getResolveActionInvocOptions() : null;
					if (dbResolveActionInvocOptions != null)
                    {
                        for (ResolveActionInvocOptions dbOption : dbResolveActionInvocOptions)
                        {
                            String dbSysId = dbOption.getSys_id();
                          //for loop for the UI options
                            for(ResolveActionInvocOptions uiOption : objsFromUI)
                            {
                                String uiSysId = uiOption.getSys_id();
                                if(StringUtils.isNotBlank(uiSysId))
                                {
                                    if(uiSysId.equalsIgnoreCase(dbSysId))
                                    {
                                        //update
                                        uiOption.setSysCreatedBy(dbOption.getSysCreatedBy());
                                        uiOption.setSysCreatedOn(dbOption.getSysCreatedOn());
                                        uiOption.setInvocation(uiResolveActionInvoc);
                                        
                                        //INPUTFILE object
                                        if(StringUtils.isNotBlank(uiOption.getUName()) && uiOption.getUName().equalsIgnoreCase("INPUTFILE"))
                                        {
                                            uiOption.setUDescription(this.uiTask.getUFullName());
                                            contentOption = uiOption;
                                        }
                                        
                                        //come out of the for loop
                                        break;
                                    } else {
                                    	if (!objsFromUI.contains(dbOption)) {
                                    		HibernateUtil.getDAOFactory().getResolveActionInvocOptionsDAO().delete(dbOption);
                                    	}
                                    }
                                    
                                }
                                
                            }//end of for
                            
                        }//end of for loop
                    }     
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
            
        }
        
        //insert remaining recs - they won't have any sysIds
        if (objsFromUI.size() > 0)
        {
            for (ResolveActionInvocOptions uiOption : objsFromUI)
            {
                //set the invocation ref - make sure that the name is not null and it is mandatory
                if(StringUtils.isNotBlank(uiOption.getUName()))
                {
                    uiOption.setInvocation(uiResolveActionInvoc);
                    //INPUTFILE object
                    if(uiOption.getUName().equalsIgnoreCase("INPUTFILE"))
                    {
                        uiOption.setUDescription(this.uiTask.getUFullName());
                        contentOption = uiOption;
                    }
                    
                    if(StringUtils.isNotBlank(uiOption.getSys_id()))
                    {
                        HibernateUtil.getDAOFactory().getResolveActionInvocOptionsDAO().update(uiOption);
                    }
                    else
                    {
                        HibernateUtil.getDAOFactory().getResolveActionInvocOptionsDAO().persist(uiOption);
                    }
                    
                }
            }
        }    
        
        if(contentOption != null)
        {
            //archive only if there is some thing in value
            if(StringUtils.isNotBlank(contentOption.getUValue()))
            {
                //submit a request to archive it
                ResolveArchiveUtils.submitArchiveRequest("ResolveActionInvocOptions", "UValue", contentOption.getSys_id(), username);
            }
        }
    }
    
    private void updateParameters() throws Exception
    {
        List<ResolveActionParameter> objsFromUI = new ArrayList<ResolveActionParameter>();//used for local manipulation
        if(this.uiResolveActionParameters != null)
        {
            objsFromUI.addAll(uiResolveActionParameters);
        }
        
        if (dbTask != null)
        {
            //this is update of AT - so first delete the existing ones
            Collection<ResolveActionParameter> dbResolveActionParameters = dbTask.getResolveActionInvoc() != null ? dbTask.getResolveActionInvoc().getResolveActionParameters() : null;
            if (dbResolveActionParameters != null)
            {
                for (ResolveActionParameter dbParameter : dbResolveActionParameters)
                {
                    String dbSysId = dbParameter.getSys_id();
                    boolean delete = true;
                    
                    //for loop for the UI options
                    for(ResolveActionParameter uiParameter : objsFromUI)
                    {
                        String uiSysId = uiParameter.getSys_id();
                        if(StringUtils.isNotBlank(uiSysId))
                        {
                            if(uiSysId.equalsIgnoreCase(dbSysId))
                            {
                                delete = false;
                                
                                //update
                                uiParameter.setSysCreatedBy(dbParameter.getSysCreatedBy());
                                uiParameter.setSysCreatedOn(dbParameter.getSysCreatedOn());
                                uiParameter.setInvocation(uiResolveActionInvoc);
                                
                                //update the parameter based on the encryption flag
                                updateEncryptedParameter(uiParameter);

                                HibernateUtil.getDAOFactory().getResolveActionParameterDAO().persist(uiParameter);
                                
                                //remove it from list as its work is done
                                objsFromUI.remove(uiParameter);
                                
                                //come out of the for loop
                                break;
                            }
                        }
                    }//end of for loop
                    
                    if(delete)
                    {
                        HibernateUtil.getDAOFactory().getResolveActionParameterDAO().delete(dbParameter);
                    }
                }//end of for 
            }
        }

        //insert/update now
        if (objsFromUI.size() > 0)
        {
            for (ResolveActionParameter uiParameter : objsFromUI)
            {
                //update the parameter based on the encryption flag
                updateEncryptedParameter(uiParameter);
                
                //make sure that there is a TYPE, default it to INPUT
                if(StringUtils.isBlank(uiParameter.getUType()))
                {
                    uiParameter.setUType("INPUT");
                }
                
                //set the invocation ref
                uiParameter.setInvocation(uiResolveActionInvoc);
                HibernateUtil.getDAOFactory().getResolveActionParameterDAO().persist(uiParameter);
            }
        }        
        
    }
    
    private void updateTaskOutputMetadata()
    {
    	updateExpressions();
    	updateOutputMappings();
    }
    
    private void updateExpressions()
    {
    	List<ResolveTaskExpression> objsFromUI = new ArrayList<ResolveTaskExpression>();
    	if (uiExpressions != null)
    	{
    		objsFromUI.addAll(uiExpressions);
    	}
    	
    	if (dbTask != null)
    	{
    		if (dbTask.getResolveActionInvoc() != null)
    		{
    			Collection<ResolveTaskExpression> dbExpressions = dbTask.getResolveActionInvoc().getExpressions();
    			if (dbExpressions != null && dbExpressions.size() > 0)
    			{
    				for (ResolveTaskExpression dbExpression : dbExpressions)
    				{
    					String dbSysId = dbExpression.getSys_id();
    					boolean delete = true;
    					
    					for (ResolveTaskExpression uiExpression : objsFromUI)
    					{
    						String uiSysId = uiExpression.getSys_id();
    						if (StringUtils.isNotBlank(uiSysId) && 
    								uiSysId.equalsIgnoreCase(dbSysId))
    						{
    							delete = false;
    							
    							uiExpression.setSysCreatedBy(dbExpression.getSysCreatedBy());
    							uiExpression.setSysCreatedOn(dbExpression.getSysCreatedOn());
    							uiExpression.setInvocation(uiResolveActionInvoc);
    							
    							HibernateUtil.getDAOFactory().getResolveTaskExpressionDAO().persist(uiExpression);
    							
    							objsFromUI.remove(uiExpression);
    							break;
    						}
    					}
    					
    					if (delete)
    					{
    						HibernateUtil.getDAOFactory().getResolveTaskExpressionDAO().delete(dbExpression);
    					}
    				}
    			}
    		}
    	}
    	
    	for (ResolveTaskExpression uiExpression : objsFromUI)
		{
    		uiExpression.setInvocation(uiResolveActionInvoc);
    		HibernateUtil.getDAOFactory().getResolveTaskExpressionDAO().persist(uiExpression);
		}
    }
    
    private void updateOutputMappings()
    {
    	List<ResolveTaskOutputMapping> objsFromUI = new ArrayList<ResolveTaskOutputMapping>();
    	if (uiOutputMappings != null)
    	{
    		objsFromUI.addAll(uiOutputMappings);
    	}
    	
    	if (dbTask != null)
    	{
    		if (dbTask.getResolveActionInvoc() != null)
    		{
    			Collection<ResolveTaskOutputMapping> dbOutputMappings = dbTask.getResolveActionInvoc().getOutputMappings();
    			if (dbOutputMappings != null && dbOutputMappings.size() > 0)
    			{
    				for (ResolveTaskOutputMapping dboutputMapping : dbOutputMappings)
    				{
    					String dbSysId = dboutputMapping.getSys_id();
    					boolean delete = true;
    					
    					for (ResolveTaskOutputMapping uiOutputMapping : objsFromUI)
    					{
    						String uiSysId = uiOutputMapping.getSys_id();
    						if (StringUtils.isNotBlank(uiSysId) && 
    								uiSysId.equalsIgnoreCase(dbSysId))
    						{
    							delete = false;
    							
    							uiOutputMapping.setSysCreatedBy(dboutputMapping.getSysCreatedBy());
    							uiOutputMapping.setSysCreatedOn(dboutputMapping.getSysCreatedOn());
    							uiOutputMapping.setInvocation(uiResolveActionInvoc);
    							
    							HibernateUtil.getDAOFactory().getResolveTaskOutputMappingDAO().persist(uiOutputMapping);
    							
    							objsFromUI.remove(uiOutputMapping);
    							break;
    						}
    					}
    					
    					if (delete)
    					{
    						HibernateUtil.getDAOFactory().getResolveTaskOutputMappingDAO().delete(dboutputMapping);
    					}
    				}
    			}
    		}
    	}
    	
    	for (ResolveTaskOutputMapping uiOutputMapping : objsFromUI)
		{
    		uiOutputMapping.setInvocation(uiResolveActionInvoc);
    		HibernateUtil.getDAOFactory().getResolveTaskOutputMappingDAO().persist(uiOutputMapping);
		}
    }

    private void updateMock() throws Exception
    {
        List<ResolveActionTaskMockData> objsFromUI = new ArrayList<ResolveActionTaskMockData>();//used for local manipulation
        if(this.uiMockDataCollection != null)
        {
            objsFromUI.addAll(uiMockDataCollection);
        }
        
        if (dbTask != null)
        {
            Collection<ResolveActionTaskMockData> dbMocks = dbTask.getResolveActionInvoc() != null ? dbTask.getResolveActionInvoc().getMockDataCollection() : null;
            if(dbMocks != null)
            {
                for(ResolveActionTaskMockData dbMock : dbMocks)
                {
                    String dbSysId = dbMock.getSys_id();
                    boolean delete = true;
                    
                    //for loop for the UI options
                    for (ResolveActionTaskMockData uiMock : objsFromUI)
                    {
                        String uiSysId = uiMock.getSys_id();
                        if (StringUtils.isNotBlank(uiSysId))
                        {
                            if (uiSysId.equalsIgnoreCase(dbSysId))
                            {
                                delete = false;

                                // check whether the mock data is modified. If yes, persist it or ignore it.
                                if (!(StringUtils.isBlank(uiMock.getUParams()) ? "" : uiMock.getUParams()).equals((StringUtils.isBlank(dbMock.getUParams()) ? "" : dbMock.getUParams())) || 
                                		!(StringUtils.isBlank(uiMock.getUInputs()) ? "" : uiMock.getUInputs()).equals((StringUtils.isBlank(dbMock.getUInputs()) ? "" : dbMock.getUInputs())) ||
                                		!(StringUtils.isBlank(uiMock.getUFlows()) ? "" : uiMock.getUFlows()).equals((StringUtils.isBlank(dbMock.getUFlows()) ? "" : dbMock.getUFlows())) ||
                                		!(StringUtils.isBlank(uiMock.getUName()) ? "" : uiMock.getUName()).equals((StringUtils.isBlank(dbMock.getUName()) ? "" : dbMock.getUName())) ||
                                		!(StringUtils.isBlank(uiMock.getUData()) ? "" : uiMock.getUData()).equals((StringUtils.isBlank(dbMock.getUData()) ? "" : dbMock.getUData())))
                                {
                                	uiMock.setSysCreatedBy(dbMock.getSysCreatedBy());
                                    uiMock.setSysCreatedOn(dbMock.getSysCreatedOn());
                                    uiMock.setResolveActionInvoc(uiResolveActionInvoc);
                                    HibernateUtil.getDAOFactory().getResolveActionTaskMockDataDAO().persist(uiMock);
                                }
                                
                                //remove it from list as its work is done
                                objsFromUI.remove(uiMock);

                                //come out of the for loop
                                break;
                            }
                        }
                    }//end of for loop
                    
                    if(delete)
                    {
                        HibernateUtil.getDAOFactory().getResolveActionTaskMockDataDAO().delete(dbMock);
                    }
                }//end of for loop
            }//end of if
        }//end of if
        
        //insert/update now
        if (objsFromUI.size() > 0)
        {
            for (ResolveActionTaskMockData uiMock : objsFromUI)
            {
                //set the invocation ref
                uiMock.setResolveActionInvoc(uiResolveActionInvoc);
                HibernateUtil.getDAOFactory().getResolveActionTaskMockDataDAO().persist(uiMock);
            }
        }        
    }
    
    
    private void socialOperations() throws Exception
    {
        //social comp creation  
        createOrUpdateActiontaskNodeInGraph();
        
        //let the creator follow the actiontask
        addUserToFollowThisActiontask();
        
        //add tags to this comp
//        updateTagsForThisActiontask();
        
        //send notifications 
        sendNotifications();
        
    }
    
    private void createOrUpdateActiontaskNodeInGraph() throws Exception
    {
        ResolveNodeVO atNode = ServiceGraph.findNode(null, uiTask.getSys_id(), null, NodeType.ACTIONTASK, username);
        if(atNode == null)
        {
            atNode = new ResolveNodeVO();
            atNode.setUCompSysId(uiTask.getSys_id());
            atNode.setUType(NodeType.ACTIONTASK);
            atNode.setUMarkDeleted(false);
            atNode.setULock(false);
            atNode.setUPinned(false);
        }
        
        atNode.setUCompName(this.uiTask.getUFullName());
        
        //access rights
        AccessRights ar = this.uiTask.getAccessRights();
        if(ar != null)
        {
            atNode.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
            atNode.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUWriteAccess() + "," + ar.getUAdminAccess()));
            atNode.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
        }
        
        try
        {
            //persist the node
            ServiceGraph.persistNode(atNode, username);
        }
        catch (Exception e)
        {
            Log.log.error("error while creating actiontask in graph " + this.uiTask.getUFullName(), e);
            throw e;
        }
    }
    
    private void updateTagsForThisActiontask() throws Exception
    {
        //delete the old tags
        if(this.dbTask != null)
        {
            Collection<ResolveActionTaskResolveTagRel> rels = this.dbTask.getAtTagRels();
            if(rels != null && rels.size() > 0)
            {
                for(ResolveActionTaskResolveTagRel rel : rels)
                {
                    HibernateUtil.getDAOFactory().getResolveActionTaskResolveTagRelDAO().delete(rel);
                }
                //making sure that its deleted
                HibernateUtil.getCurrentSession().flush();
            }
        }
        
        //add the new tags
        if(tags.size() > 0)
        {
            for(String tag : tags)
            {
                ResolveTagVO tagModel = ServiceTag.getTag(null, tag, username);
                if(tagModel != null)
                {
                    ResolveActionTaskResolveTagRel rel = new ResolveActionTaskResolveTagRel();
                    rel.setTask(uiTask);
                    rel.setTag(new ResolveTag(tagModel.getSys_id()));
                    
                    HibernateUtil.getDAOFactory().getResolveActionTaskResolveTagRelDAO().persist(rel);
                }
            }//end of for loop
        }
    }
    
    private void addUserToFollowThisActiontask() throws Exception
    {
        //if its an INSERT, than let the user follow it
        if(dbTask == null)
        {
            ResolveNodeVO docNode = new ResolveNodeVO();
            docNode.setUCompName(uiTask.getUFullName());
            docNode.setUCompSysId(uiTask.getSys_id());
            docNode.setUType(NodeType.ACTIONTASK);
            
            Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
            followComponents.add(docNode);
            
            try
            {
                //user following this Actiontask
                ServiceGraph.follow(null, null, username, NodeType.USER, followComponents, null, username);
            }
            catch (Exception e)
            {
                Log.log.error("Error following Actiontask :" + uiTask.getUFullName() + " by User:" + username, e);
                throw e;
            }
        }
    }
    
    private void sendNotifications() throws Exception
    {
        List<SubmitNotification> submitNotifications = new ArrayList<SubmitNotification>();
        
        if(this.dbTask != null)
        {
            //update
            submitNotifications.add(NotificationHelper.getActionTaskNotification(uiTask, UserGlobalNotificationContainerType.ACTIONTASK_UPDATE, username, false, true));
        }
        else
        {
            //insert
            submitNotifications.add(NotificationHelper.getActionTaskNotification(uiTask, UserGlobalNotificationContainerType.ACTIONTASK_CREATE, username, false, true));
            
        }
        
        //send the ESB message
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ConstantValues.SOCIAL_NOTIFICATION_LIST, submitNotifications);
        
        SocialUtil.submitESBForUpdateSocialForWiki(params);
    }
    
    private void invalidateActionTask()
    {
        // TODO: not sure if we need this call 
        //call this API after the commit is successful
        Log.log.debug("**** CALLING THE MENUDETAILS-REFRESHCACHE");
        MenuDetails.getInstance().resetPopupMenuMap();
        
        // send actiontask invalidation to all rscontrols
        Map<String, String> content = new HashMap<String, String>();
        content.put(Constants.ESB_PARAM_ACTIONTASKID, this.uiTask.getSys_id());
        
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.invalidateActionTask", content);
    }
    
    //all the validations that are required for an actiontask goes here
    private void validate()  throws Exception
    {
        validateMockNameUniqueness();
    }
    
    private void validateMockNameUniqueness() throws Exception
    {
        if(this.uiMockDataCollection != null && this.uiMockDataCollection.size() > 0)
        {
            Set<String> names = new HashSet<String>();
            for(ResolveActionTaskMockData mock : this.uiMockDataCollection)
            {
                String name = mock.getUName();
                if(names.contains(name))
                {
                    throw new Exception("Mock with name " + name + " duplicated. Please make the Mock names unique.");
                }
                
                names.add(mock.getUName());
            }
        }
    }
    
    private String prepareUniqueList(String roles)
    {
        String result = roles ;
        
        if(StringUtils.isNotBlank(roles))
        {
            Set<String> set = new HashSet<String>(StringUtils.convertStringToList(roles, ","));
            result = StringUtils.convertCollectionToString(set.iterator(), ",");
        }
        
        return result;
    }
    
    private void updateEncryptedParameter(ResolveActionParameter uiParameter) throws Exception
    {
        if (uiParameter != null)
        {
            String defaultValue = uiParameter.getUDefaultValue();
            
            if (StringUtils.isNotBlank(defaultValue))
            {
                //if its set to encrupt, than encrypt it.
                if (uiParameter.ugetUEncrypt())
                {

                    if (!CryptUtils.isEncrypted(defaultValue))
                    {
                        String encDefaultValue = CryptUtils.encrypt(defaultValue);
                        uiParameter.setUDefaultValue(encDefaultValue);
                    }
                }
                else
                {
                    /* THIS IS SECURITY HOLE DO NOT UNCOMMENT
                    //if the flag is false and the value is encrypted, decrypt it
                    if (CryptUtils.isEncrypted(defaultValue))
                    {
                        String decryptDefaultValue = CryptUtils.decrypt(defaultValue);
                        uiParameter.setUDefaultValue(decryptDefaultValue);
                    }*/
                }
            }
        }
    }
    
    public void setVersionPersistAction(VersionPersistAction versionPersistAction)
    {
        this.versionPersistAction = versionPersistAction;
    }

}
