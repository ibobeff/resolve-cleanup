/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.OrganizationUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SearchWiki
{
    private QueryDTO query = null; 
    private String username = null;
    
    //By default, everything, valid values are HibernateConstants.SOCIAL_TYPE_DECISIONTREE, HibernateConstants.SOCIAL_TYPE_DOCUMENT, HibernateConstants.SOCIAL_TYPE_RUNBOOK
    private NodeType type = null; 
    
    private int start = -1;
    private int limit = -1;
    private Set<String> userRoles = null;

    public SearchWiki(QueryDTO query, NodeType type, String username) 
    {
        if(query == null || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Query and username are mandatory to search for wiki");
        }
        
        this.query = query;
        this.username = username;
        this.type = type;
        
        this.start = query.getStart();
        this.limit = query.getLimit();
        userRoles = UserUtils.getUserRoles(this.username);
    }
    
    public List<WikiDocumentVO> execute()
    {
        List<WikiDocumentVO> result = new ArrayList<WikiDocumentVO>();
        this.query.setPrefixTableAlias("wd.");
        
        String hql = createHql();
        
        Log.log.trace("Query : [" + query + "]");
        Log.log.trace("HQL : [" + hql + "]");
        
        //set it in the query object
        this.query.setHql(hql.toString());

        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        WikiDocument instance = new WikiDocument();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        //get the access rights for this doc. - ** takes longer...from 80ms to 200 ms
//                        AccessRights ar = getAccessRightsFor(instance.getSys_id());
//                        instance.setAccessRights(ar);

                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        WikiDocument instance = (WikiDocument) o;
                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }
                }
            }//end of if
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sqk:" + hql.toString(), e);
        }
        
        
        return result;
    }
    
    private String createHql()
    {
        StringBuilder hql = new StringBuilder("select ");
        
        if(StringUtils.isNotBlank(query.getSelectColumns()))
        {
            StringBuilder updatedCols = new StringBuilder();
            String[] selectedCols = query.getSelectColumns().split(",");
            for(String col : selectedCols)
            {
                updatedCols.append("wd.").append(col).append(",");
            }
            
            //remove the last comma
            updatedCols.setLength(updatedCols.length()-1);
            
            hql.append(updatedCols);
        }
        else
        {
            hql.append(" wd ");
        }
        
        
        hql.append(" from WikiDocument as wd, AccessRights as ar");
        
        if (type == NodeType.AB_RUNBOOK)
        {
            hql.append(", RBGeneral as rb" );
        }
        
        hql.append(" where ");
        
        
        if (type != null)
        {
            if (type == NodeType.DECISIONTREE)
            {
                hql.append("  (wd.UIsRoot is true) ");
            }
            else if (type == NodeType.DOCUMENT)
            {
                hql.append("  (wd.UHasActiveModel is null or wd.UHasActiveModel is false) ");
            }
            else if (type == NodeType.RUNBOOK)
            {
                hql.append(" (wd.UHasActiveModel is true) ");
            }
            else if (type == NodeType.AB_RUNBOOK)
            {
                hql.append(" (wd.UHasActiveModel is true) and (rb.sys_id = wd.UResolutionBuilderId) ");
            }
            else if (type == NodeType.PLAYBOOK_TEMPLATE)
            {
                hql.append(" (wd.UDisplayMode = \'" + WikiDocumentVO.PLAYBOOK + "\') and (wd.UIsTemplate is true) ");
            }
            /* SIR Playbooks does not result in new record in wikidoc table anymore
            else if (type == NodeType.SIR_PLAYBOOK)
            {
                hql.append(" (wd.UDisplayMode = \'" + WikiDocumentVO.PLAYBOOK + "\') and (wd.UIsTemplate is false) ");
            }*/
            else 
            {
                //hql.append(" 1=1 ");
                hql.append(" (wd.UDisplayMode is null or wd.UDisplayMode != \'" + WikiDocumentVO.PLAYBOOK + "\') ");
            }
        }
        else
        {
            // type = null is considered as No playbook wikis
            //hql.append(" 1=1 ");
            hql.append(" (wd.UDisplayMode is null or wd.UDisplayMode != \'" + WikiDocumentVO.PLAYBOOK + "\') ");
        }
        
//        hql.append(" and wd.UIsDeleted = false and wd.UIsLocked = false and wd.UIsHidden = false and ar.UResourceId = wd.sys_id and ar.UResourceType ='wikidoc' and ");
        hql.append(" and ar.UResourceId = wd.sys_id and ar.UResourceType ='wikidoc' and (wd.sysIsDeleted = false or wd.sysIsDeleted is null) and ");

        //role filter 
        String roleFilter = "";
        //qry only if its not an 'admin', else show everything
        if (!this.userRoles.contains("admin") && !username.equals("resolve.maint"))
        {
            StringBuilder filters = new StringBuilder("(");
            for (String userRole : userRoles)
            {
                filters.append(" ar.UAdminAccess like '%" + userRole + "%' or ar.UReadAccess like '%" + userRole + "%' or ar.UWriteAccess like '%" + userRole + "%' or");
            }
            filters.setLength(filters.length() - 2);
            filters.append(")");  
            roleFilter = filters.toString();
        }
        else
        {
            roleFilter = " 1=1 ";
        }
        hql.append(roleFilter);
        
        //additional where clause
        String whereClause = this.query.getWhereClause();
        if(StringUtils.isNotBlank(whereClause))
        {
            StringBuilder updatedWhereClause = new StringBuilder();
            String[] clauses = whereClause.split("and");
            for(String clause : clauses)
            {
                String localClause = clause.toLowerCase();
                if (localClause.contains("lower") || localClause.contains("upper"))
                {
                    /*
                     * This is for custom queries when we need case insensitive searches.
                     * It's assumed that table prefix is taken care already.
                     */
                    updatedWhereClause.append(clause + " and ");
                }
                else
                {
                    //sometime caller could pass something that should be added directly
                    //assume the prefix is correctly set inside the clause.
                    String tmpClause = (clause.trim()).replaceFirst("^ +", "");
                    if(tmpClause.startsWith("(") && tmpClause.endsWith(")"))
                    {
                        updatedWhereClause.append(tmpClause + " and ");
                    }
                    else
                    {
                        updatedWhereClause.append(query.getPrefixTableAlias() + clause + " and ");
                    }
                }
            }
            
            updatedWhereClause.setLength(updatedWhereClause.length() - 4);
            
            hql.append(" and ").append(updatedWhereClause.toString());
        }
        
        //search filter
        String searchQry = this.query.getFilterWhereClause();
        if (StringUtils.isNotBlank(searchQry))
        {
            hql.append(" and ").append(searchQry);
        }
        
        //sort condition --> [{"property":"name","direction":"DESC"}]
        //comes from the UI...so will have to add the 'wd.' in front of the properties
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
//        else
//        {
//            String[] sortArr = sortBy.split(",");
//            StringBuilder newSortStr = new StringBuilder();
//            for(String sortStr : sortArr)
//            {
//                newSortStr.append(query.getPrefixTableAlias() + sortStr + ",");
//            }
//            newSortStr.setLength(newSortStr.length()-1);
//            sortBy = newSortStr.toString();
//        }
        
        if(StringUtils.isNotBlank(sortBy))
        {
            hql.append(" order by ").append(sortBy.trim());
        }
        
        
        return hql.toString();
    }
    
    
    
    
    //private apis    
    private static WikiDocumentVO convertModelToVO(WikiDocument model, Map<String, String> mapOfOrganization)
    {
        WikiDocumentVO vo = null;

        if (model != null)
        {
            if (mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }

            vo = model.doGetVO();
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }

        }

        return vo;
    }
    
    private static AccessRights getAccessRightsFor(String docSysId)
    {
        AccessRights ar = null;
        
        if(StringUtils.isNotBlank(docSysId))
        {
            //String sql = "from AccessRights where UResourceId = '" + docSysId + "' and UResourceType = '" + WikiDocument.RESOURCE_TYPE + "'";
            String sql = "from AccessRights where UResourceId = :UResourceId and UResourceType = '" + WikiDocument.RESOURCE_TYPE + "'";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UResourceId", docSysId);
            
            try
            {
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(data != null && data.size() > 0) 
                {
                    ar = (AccessRights) data.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("error getting the access rights for " + docSysId);
            }
        }
        
        return ar;
    }
    
}
