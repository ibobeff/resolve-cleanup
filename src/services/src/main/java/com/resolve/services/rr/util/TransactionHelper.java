package com.resolve.services.rr.util;

import java.util.ArrayList;
import java.util.List;

import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Log;

public class TransactionHelper<T>
{   
    private String username;
    public TransactionHelper(String username) {
        this.username = username;
    }
    public static interface AtomicObjectOperation<T>{
        public BaseModel<T> action() throws Exception;
    }
    
    public static interface AtomicCollectionOperation<T>{
        public List<BaseModel<T>> action() throws Exception;
    }
    
    public static interface AtomicRawOperation<T>{
        public T action() throws Exception;
    }
    
    @SuppressWarnings("unchecked")
	public BaseModel<T> doAtomicOp(AtomicObjectOperation<T> operation) throws Exception {
        BaseModel<T> value = null;
		try {
        HibernateProxy.setCurrentUser(this.username);
			value = (BaseModel<T>) HibernateProxy.execute(() -> {
				return operation.action();
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new Exception(e);
		}
        
        return value;
    }
    
    @SuppressWarnings("unchecked")
	public List<BaseModel<T>> doAtomicOp(AtomicCollectionOperation<T> operation) throws Exception {
        List<BaseModel<T>> list = new ArrayList<BaseModel<T>>();
		try {
        HibernateProxy.setCurrentUser(this.username);
			list = (List<BaseModel<T>>) HibernateProxy.execute(() -> {
				return operation.action();
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new Exception(e);
		}
        
        return list;
    }
    
    @SuppressWarnings("unchecked")
	public T doAtomicOp(AtomicRawOperation<T> operation) throws Exception {
        T data = null;
		try {
        HibernateProxy.setCurrentUser(this.username);
			data = (T) HibernateProxy.execute(() -> {
				return operation.action();
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new Exception(e);
		}
        
        return data;
    }
}
