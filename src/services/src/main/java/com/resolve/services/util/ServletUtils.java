/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.reference.DefaultHTTPUtilities;

import com.resolve.util.Log;
import com.resolve.util.MIMEUtils;

public class ServletUtils
{
	/**
	 * Sends headers to allow a file to be download to the workstation vs. opened in the browser.
	 * 
	 * @param fileName
	 *            the name of the file. This will be encoded as UTF-8.
	 * @param contentLen
	 *            the length of the file, zero (0) if unknown.
	 * @param request
	 *            the {@code HttpServletRequest}.
	 * @param response
	 *            the {@code HttpSerletResponse}.
	 * @throws UnsupportedEncodingException
	 */
    @Deprecated
	public static void sendHeaders(String fileName, int contentLen, HttpServletRequest request,	HttpServletResponse response) throws UnsupportedEncodingException 
	{	
		String ua = request.getHeader("User-Agent").toLowerCase();
		boolean isIE6or7 = ((ua.indexOf("msie 6.0") != -1) || (ua.indexOf("msie 7.0") != -1)) ? true : false;
		boolean isIE = ua.indexOf("msie") != -1 ? true : false;
		String encName = fileName;// URLEncoder.encode(fileName, "UTF-8");
		//encName = StringUtils.removeNewLineAndCarriageReturn(encName);
		
		DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();

		// Derived from Squirrel Mail and from http://www.jspwiki.org/wiki/BugSSLAndIENoCacheBug
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
		if (request.isSecure()) {
			response.addHeader("Expires", "-1");
		}

		String headerValue = "attachment;filename=\"" +  encName + "\"";
		String mimeType = MIMEUtils.getMIMEType(fileName);
		Log.log.debug("File Name = " + fileName + ", File Name based MIME Type = " + mimeType);
		
		dhttp.addHeader(response, "Content-Disposition", headerValue);
		ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava(mimeType + ";name=\"" +    encName + "\""));
		
		if (isIE6or7) 
		{
			//dhttp.addHeader(response, "Content-Disposition", headerValue);
			//response.addHeader("Content-Disposition", StringUtils.removeNewLineAndCarriageReturn(headerValue));
			response.addHeader("Connection", "close");
			//ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava("application/force-download;name=\"" +    encName + "\""));			
			// response.setContentType(StringEscapeUtils.escapeJava("application/force-download;name=\"" +	encName + "\""));
		}
		else 
		{
		    //dhttp.addHeader(response, "Content-Disposition", headerValue);
		    //response.addHeader("Content-Disposition", StringUtils.removeNewLineAndCarriageReturn(headerValue));
		    //ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava("application/octet-stream;name=\"" +    encName + "\""));
			// response.setContentType(StringEscapeUtils.escapeJava("application/octet-stream;name=\"" +	encName + "\"" ));
			if (contentLen > 0)
			{
				response.setContentLength(contentLen);
			}
		}
	}
	
	/**
     * Sends headers to allow a file to be download to the workstation vs. opened in the browser.
     * 
     * @param fileName
     *            the name of the file. This will be encoded as UTF-8.
     * @param contentLen
     *            the length of the file, zero (0) if unknown.
     * @param bytes
     *             first max 128 bytes of file.
     * @param request
     *            the {@code HttpServletRequest}.
     * @param response
     *            the {@code HttpSerletResponse}.
     * @throws UnsupportedEncodingException
     */
    public static void sendHeaders(String fileName, int contentLen, byte[] bytes, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException 
    {
        String ua = request.getHeader("User-Agent").toLowerCase();
        boolean isIE6or7 = ((ua.indexOf("msie 6.0") != -1) || (ua.indexOf("msie 7.0") != -1)) ? true : false;
        boolean isIE = ua.indexOf("msie") != -1 ? true : false;
        String encName = fileName;
        
        DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();

        // Derived from Squirrel Mail and from http://www.jspwiki.org/wiki/BugSSLAndIENoCacheBug
        if (request.isSecure()) 
        {
            response.addHeader("Expires", "-1");
            if (!isIE)
            {
                response.addHeader("Pragma", "no-cache");
                response.addHeader("Cache-Control", "no-cache");
            }
        }
        else 
        {
            response.addHeader("Cache-Control", "private");
            response.addHeader("Pragma", "no-cache");
        }

        String headerValue = "attachment;filename=\"" +  encName + "\"";
        String mimeType = MIMEUtils.getMIMEType(bytes);
        Log.log.debug("File Name = " + fileName + ", Data based MIME Type = " + mimeType);
        
        dhttp.addHeader(response, "Content-Disposition", headerValue);
        ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava(mimeType + ";name=\"" +    encName + "\""));
        
        if (isIE6or7) 
        {
            response.addHeader("Connection", "close");
        }
        else 
        {
            if (contentLen > 0)
            {
                response.setContentLength(contentLen);
            }
        }
    }
    
	public static void sendHeaders(String fileName,String contentType, int contentLen, HttpServletRequest request,    HttpServletResponse response) throws UnsupportedEncodingException 
    {
    
        String ua = request.getHeader("User-Agent").toLowerCase();
        boolean isIE6or7 = ((ua.indexOf("msie 6.0") != -1) || (ua.indexOf("msie 7.0") != -1)) ? true : false;
        boolean isIE = ua.indexOf("msie") != -1 ? true : false;
        String encName = fileName;// URLEncoder.encode(fileName, "UTF-8");
        
        DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        
        // sanitize encName
        //encName = StringUtils.removeNewLineAndCarriageReturn(encName);

        // Derived from Squirrel Mail and from http://www.jspwiki.org/wiki/BugSSLAndIENoCacheBug
        if (request.isSecure()) 
        {
            response.addHeader("Expires", "-1");
            if (!isIE)
            {
                response.addHeader("Pragma", "no-cache");
                response.addHeader("Cache-Control", "no-cache");
            }
        }
        else 
        {
            response.addHeader("Cache-Control", "private");
            response.addHeader("Pragma", "no-cache");
        }

        if (isIE6or7) 
        {
            String headerValue = "attachment;filename=\"" +    encName + "\"" ;
            dhttp.addHeader(response, "Content-Disposition", headerValue);
            //response.addHeader("Content-Disposition", StringUtils.removeNewLineAndCarriageReturn(headerValue) );
            response.addHeader("Connection", "close");
            response.setContentType(contentType );
        }
        else 
        {
            String headerValue = "attachment; filename=\"" +   encName + "\"";
            dhttp.addHeader(response, "Content-Disposition", headerValue);
            //response.addHeader("Content-Disposition", StringUtils.removeNewLineAndCarriageReturn(headerValue) );
            response.setContentType(contentType);
            if (contentLen > 0)
            {
                response.setContentLength(contentLen);
            }
        }
    }
}
