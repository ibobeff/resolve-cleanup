/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.jdbc.util;

import java.util.HashMap;
import java.util.Map;

public class MappingAttributeToColumnsUtil
{
    
    public static Map<String, String> getMappingForResolveEvent()
    {
        Map<String, String> attColMap = new HashMap<String, String>();
        attColMap.put("sysUpdatedBy", "sys_updated_by");
        attColMap.put("sysUpdatedOn", "sys_updated_on");
        attColMap.put("sysCreatedBy", "sys_created_by");
        attColMap.put("sysCreatedOn", "sys_created_on");
        attColMap.put("sysModCount", "sys_mod_count");
        attColMap.put("sys_id", "sys_id");
        attColMap.put("UValue", "u_value");
        
        return attColMap;
    }

    public static Map<String, String> getMappingForTMetricLookup()
    {
        Map<String, String> attColMap = new HashMap<String, String>();
        attColMap.put("sysUpdatedBy", "sys_updated_by");
        attColMap.put("sysUpdatedOn", "sys_updated_on");
        attColMap.put("sysCreatedBy", "sys_created_by");
        attColMap.put("sysCreatedOn", "sys_created_on");
        attColMap.put("sysModCount", "sys_mod_count");
        attColMap.put("sys_id", "sys_id");
        attColMap.put("UPathId", "u_path_id");
        attColMap.put("UFullPath", "u_full_path");
        
        return attColMap;
    }
    
    
    
}
