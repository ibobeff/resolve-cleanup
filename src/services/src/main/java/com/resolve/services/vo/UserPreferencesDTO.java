/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

public class UserPreferencesDTO
{

    private String prefGroup;
    private String prefKey;
    private String prefValue;
    
    public String getPrefGroup()
    {
        return prefGroup;
    }
    public void setPrefGroup(String prefGroup)
    {
        this.prefGroup = prefGroup;
    }
    public String getPrefKey()
    {
        return prefKey;
    }
    public void setPrefKey(String prefKey)
    {
        this.prefKey = prefKey;
    }
    public String getPrefValue()
    {
        return prefValue;
    }
    public void setPrefValue(String prefValue)
    {
        this.prefValue = prefValue;
    }

    
    
    
}
