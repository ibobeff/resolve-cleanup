/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.io.Serializable;
import java.util.ArrayList;

import com.resolve.dto.ComboBoxModel;

/**
 * Used in the META_FORM to decise where the data is coming from 
 * 
 * @author jeet.marwah
 *
 */

public enum Operation implements Serializable
{
	//this is mapped to wikiarchive table
//	DB("DB"),
//	EXECUTEPROCESS("EXECUTEPROCESS"),
//	BUSINESSRULE("BUSINESSRULE");
    DB("DB"),
    RUNBOOK("RUNBOOK"),
    ACTIONTASK("ACTIONTASK"),
    MAPPING("MAPPING"),
    REDIRECT("REDIRECT"),
    EVENT("EVENT"),
    CLOSE("CLOSE"),
    SCRIPT("SCRIPT");
	
	//definition of the
	private final String tagName;
	
	private Operation(String tagName)
	{
		this.tagName = tagName;
	}
	
	public String getTagName()
	{
		return this.tagName;
	}
	
	public static ArrayList<String> getDataSourceList()
	{
	    ArrayList<String> list = new ArrayList<String>();
	    
	    Operation[] arr = Operation.values();
	    for(Operation ds : arr)
	    {
	        list.add(ds.getTagName());
	    }
	    
	    return list;
	}//getDataSourceList
	
	public static ArrayList<ComboBoxModel> getOperations()
    {
        ArrayList<ComboBoxModel> list = new ArrayList<ComboBoxModel>();
        
        Operation[] arr = Operation.values();
        for(Operation ds : arr)
        {
            list.add(new ComboBoxModel(ds.getTagName(), ds.getTagName()));
        }
        
        return list;
    }//getOperations
	
	public static Operation getRevisionType(String tagName)
	{
		if(tagName == null)
		{
			return null;
		}
		else if (tagName.equals(DB.toString()))
		{
			return Operation.DB;
		}
        else if (tagName.equals(RUNBOOK.toString()))
        {
            return Operation.RUNBOOK;
        }
		else if (tagName.equals(SCRIPT.toString()))
		{
			return Operation.SCRIPT;
		}
		else
		{
			return null;
		}
			
	}
}
