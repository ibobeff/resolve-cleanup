/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.type.StringType;
import org.hibernate.type.TypeFactory;

import com.resolve.persistence.model.MCPComponent;
import com.resolve.persistence.model.MCPFile;
import com.resolve.persistence.model.MCPFileContent;
import com.resolve.persistence.model.MCPServer;
import com.resolve.persistence.model.ModelToVO;
import com.resolve.persistence.model.ResolveMCPCluster;
import com.resolve.persistence.model.ResolveMCPComponent;
import com.resolve.persistence.model.ResolveMCPGroup;
import com.resolve.persistence.model.ResolveMCPHost;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsbase.MainBase;
import com.resolve.services.hibernate.vo.MCPComponentVO;
import com.resolve.services.hibernate.vo.MCPFileContentVO;
import com.resolve.services.hibernate.vo.MCPFileVO;
import com.resolve.services.hibernate.vo.MCPServerVO;
import com.resolve.services.hibernate.vo.ResolveMCPGroupVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.MCPFileTreeDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.QuerySort;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.MCPConstants;
import com.resolve.util.Properties;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class MCPUtils
{
    // ************************************************************** //
    // New MCP API's
    // ************************************************************** //
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void registerInstance(String blueprint) throws IOException
    {
        File blueprintFile = new File(String.format("%s/blueprint.properties", MainBase.getTmpDir()));
        FileUtils.writeStringToFile(blueprintFile, blueprint, StandardCharsets.UTF_8);
        
        Properties configureProperties = new Properties();
        FileInputStream fis = new FileInputStream(blueprintFile);
        configureProperties.load(fis); 
        fis.close();
        
        String group = configureProperties.get("GROUPNAME");
        String cluster = configureProperties.get("CLUSTERNAME");
        String rsmqHost = configureProperties.get("RSMQ_PRIMARY_HOST");
        
        if (StringUtils.isBlank(group))
        {
            group = Constants.MCP_DEFAULT_GROUP;
        }
        
        try
        {
        	String groupFinal = group;
         
          HibernateProxy.setCurrentUser("admin");
        	HibernateProxy.execute(() -> {
        		   boolean foundCluster = false; 
                   String hql = " from ResolveMCPCluster r where r.UClusterName = :cl and r.clusterGroup.UGroupName = :gp "; 
                   Query q = HibernateUtil.createQuery(hql);
                   q.setParameter("cl", cluster);
                   q.setParameter("gp", groupFinal);
                   List<Object> l =  q.list();
                   if (l.size()>0)
                   {
                       foundCluster = true;
                   }
                   
                   boolean existingCluster = false; 
                   if (foundCluster)
                   {
                       hql = " from ResolveMCPCluster r where r.UClusterName = :cl and r.clusterGroup.UGroupName = :gp and r.URsmqAddr = :ra "; 
                       q = HibernateUtil.createQuery(hql);
                       q.setParameter("cl", cluster);
                       q.setParameter("gp", groupFinal);
                       q.setParameter("ra", rsmqHost);
                       l =  q.list();
                       if (l.size()>0)
                       {
                           existingCluster = true;
                       }
                  }
                   
                  if (foundCluster&&!existingCluster) 
                  {
                      throw new RuntimeException("Cluster with same name is already registered: " + cluster + ". Please rename your cluster and try again"); 
                  }
                   
                   ResolveMCPGroup mcpGroup = saveGroup(groupFinal);
                   ResolveMCPCluster mclCluster = saveMcpCluster(cluster, mcpGroup, rsmqHost);
                   ResolveMCPHost mcpHost = saveMcpHost(configureProperties, mclCluster, mcpGroup, blueprint);
                   saveComponent(configureProperties, mcpHost);
                   
        	});
        }
        catch(Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new IOException(e);
        }
        finally
        {
            blueprintFile.delete();
        }
    }
    
    @SuppressWarnings("rawtypes")
    public static ResolveMCPGroup saveGroup(String groupName)
    {
    	Log.log.debug("Registering Group: " + groupName);
        
        try
        {	
          HibernateProxy.setCurrentUser("admin");
            return (ResolveMCPGroup) HibernateProxy.execute(() -> {
            	ResolveMCPGroup mcpGroup = null; 
            
	            Query query = HibernateUtil.createQuery("select obj from ResolveMCPGroup obj where LOWER(UGroupName)=:name");
	            query.setParameter("name", groupName.toLowerCase());
	            
	            mcpGroup = (ResolveMCPGroup)query.uniqueResult();
	            
	            if (mcpGroup == null)
	            {
	                mcpGroup = new ResolveMCPGroup();
	                mcpGroup.setSys_id(null);
	                mcpGroup.setUGroupName(groupName);
	                mcpGroup = HibernateUtil.getDAOFactory().getResolveMCPGroupDAO().persist(mcpGroup);
	                Log.log.debug("It's a New Group");
	            }
	            return mcpGroup;
            
            });
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return null;
    }
    
    public static ResolveMCPCluster saveMcpCluster(String clusterName, ResolveMCPGroup group, String rsmqHost)
    {
    	Log.log.debug("Registering Cluster: " + clusterName);
        ResolveMCPCluster mcpCluster = null;
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	mcpCluster = (ResolveMCPCluster) HibernateProxy.execute(() -> {
            	ResolveMCPCluster mcpClusterFinal = (ResolveMCPCluster)HibernateUtil.getCurrentSession().createCriteria(ResolveMCPCluster.class).add(Restrictions.eq("UClusterName", clusterName).ignoreCase()).add(Restrictions.eq("clusterGroup.id", group.getSys_id()).ignoreCase()).uniqueResult();
	            
	            if (mcpClusterFinal == null)
	            {
	                mcpClusterFinal = new ResolveMCPCluster();
	                mcpClusterFinal.setSys_id(null);
	                mcpClusterFinal.setUClusterName(clusterName);
	                Log.log.debug("It's a New Cluster");
	            }
	            mcpClusterFinal.setUIsMonitored(true);
	            mcpClusterFinal.setClusterGroup(group);
	            mcpClusterFinal.setURsmqAddr(rsmqHost);
	            
	            mcpClusterFinal = HibernateUtil.getDAOFactory().getResolveMCPClusterDAO().persist(mcpClusterFinal);
	            return mcpClusterFinal;
	            
            });
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        Log.log.debug("Registered Cluster: " + clusterName);
        return mcpCluster;
    }
    
    public static ResolveMCPHost saveMcpHost(Properties configureProperties, ResolveMCPCluster mcpCluster, ResolveMCPGroup mcpGroup, String blueprintString) throws IOException
    {
    	ResolveMCPHost mcpHost = null;
        
        String localhost = configureProperties.get("LOCALHOST");
        InetAddress address = InetAddress.getByName(localhost);
        String hostName = address.getHostName().toUpperCase();
        String hostIp = address.getHostAddress().toUpperCase();
        Log.log.debug("Registering Host: " + hostName);
        
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	mcpHost = (ResolveMCPHost) HibernateProxy.execute(() -> {
            	ResolveMCPHost mcpHostFinal = null;
	            mcpHostFinal = (ResolveMCPHost)HibernateUtil.getCurrentSession().createCriteria(ResolveMCPHost.class).add(Restrictions.eq("UHostName", hostName).ignoreCase()).add(Restrictions.eq("cluster.id", mcpCluster.getSys_id()).ignoreCase()).uniqueResult();            
	            
	            if (mcpHostFinal == null)
	            {
	                mcpHostFinal = new ResolveMCPHost();
	                mcpHostFinal.setSys_id(null);
	                Log.log.debug("It's a New Host");
	            }
	            mcpHostFinal.setUIsMonitored(true);
	            mcpHostFinal.setUHostName(hostName);
	            mcpHostFinal.setUHostIp(hostIp);
	            mcpHostFinal.setCluster(mcpCluster);
	            mcpHostFinal.setHostGroup(mcpGroup);
	            mcpHostFinal.setUBluePrint(blueprintString);
	            
	            mcpHostFinal = HibernateUtil.getDAOFactory().getResolveMCPHostDAO().persist(mcpHostFinal);
	            return mcpHostFinal;
            });
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        Log.log.debug("Registered Host: " + hostName);
        return mcpHost;
    }
    
    public static ResolveMCPComponent saveComponent(Properties configureProperties, ResolveMCPHost host) throws IOException
    {
        ResolveMCPComponent mcpComp = null;
        
        String component = configureProperties.get("resolve.rscontrol");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            registerComponent("RSControl", host);
        }
        
        component = configureProperties.get("resolve.rssync");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            registerComponent("RSSynch", host);
        }
        
        component = configureProperties.get("resolve.rsconsole");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            registerComponent("RSConsole", host);
        }
        
        component = configureProperties.get("resolve.rsmgmt");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            registerComponent("RSMgmt", host);
        }
        
        component = configureProperties.get("resolve.rsremote");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            registerComponent("RSRemote", host);
        }

        component = configureProperties.get("resolve.rsview");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            String port = configureProperties.get("rsview.tomcat.connector.http.port");
            String compName = "RSView";
            if (StringUtils.isNoneBlank(port))
            {
                compName = compName + Constants.MCP_NAMEPORT_SEPARATOR + port;
            }
            registerComponent(compName, host);
        }
        
        component = configureProperties.get("resolve.rssearch");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            registerComponent("RSSearch", host);
        }
        
        component = configureProperties.get("resolve.rsmq");
        if (StringUtils.isNotBlank(component) && "true".equals(component))
        {
            registerComponent("RSMQ", host);
        }
        
        return mcpComp;
    }
    
    @SuppressWarnings("rawtypes")
    public static ResolveMCPComponent registerComponent(String compName, ResolveMCPHost host)
    {
    	
        try
        {
          HibernateProxy.setCurrentUser("admin");
            return (ResolveMCPComponent) HibernateProxy.execute(() -> {
            	Log.log.debug("Registering Component: " + compName + " on Host: " + host.getUHostName());
                ResolveMCPComponent mcpComponent = null;
            	
            	boolean isNew = false;
                Query query = HibernateUtil.createQuery("select obj from ResolveMCPComponent obj where UComponentName=:name and UComponentHost=:host");
                query.setParameter("name", compName);
                query.setParameter("host", host, new TypeFactory().manyToOne(host.getClass().getName()));
                
                mcpComponent = (ResolveMCPComponent)query.uniqueResult();
                
                if (mcpComponent == null)
                {
                    mcpComponent = new ResolveMCPComponent();
                    mcpComponent.setSys_id(null);
                    isNew = true;
                }
                
                mcpComponent.setUComponentName(compName);
                mcpComponent.setUComponentHost(host);
                
                if (isNew)
                {
                    mcpComponent = HibernateUtil.getDAOFactory().getResolveMCPComponentDAO().persist(mcpComponent);
                }
                
                Log.log.debug("Registered Component: " + compName + " on Host: " + host.getUHostName());
                return mcpComponent;
                
            });
        }
        catch (Exception e)
        {
            Log.log.error("Error while registering " + compName + " on host: " + host.getUHostName(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return null;       
        
    }
    
    @SuppressWarnings("rawtypes")
    public static List<ResolveMCPGroupVO> getRegisteredInstances(boolean withBlueprintFile)
    {
        List<ResolveMCPGroupVO> groupList = new ArrayList<ResolveMCPGroupVO>();
        
        try
        {
         
          HibernateProxy.setCurrentUser("admin");
        	HibernateProxy.execute(() -> {
        		
        		   List list = null;
        		   Query query = HibernateUtil.createQuery("from ResolveMCPGroup");
                   list = query.list();
                   
                   if (list != null)
                   {
                       for (Object obj : list)
                       {
                           ResolveMCPGroup group = (ResolveMCPGroup)obj;
                           if (group.getClusters() != null)
                           {
                               group.getClusters().size();
                               
                               Iterator<ResolveMCPCluster> clusterIterator = group.getClusters().iterator();
                               while (clusterIterator.hasNext())
                               {
                               	ResolveMCPCluster mcpCluster = clusterIterator.next();
                               	if (mcpCluster.getUIsMonitored())
                               	{
                               		if (mcpCluster.getHosts() != null)
                                       {
                                           mcpCluster.getHosts().size();
                                           
                                           Iterator<ResolveMCPHost> iterator = mcpCluster.getHosts().iterator();
                                           while (iterator.hasNext())
                                           {
                                           	ResolveMCPHost host = iterator .next();
                                           	if (host.getUIsMonitored())
                                           	{
                                           		host.getMcpComponents().size();
                                           		if (!withBlueprintFile)
                                           		{
                                           			host.setUBluePrint(null);
                                           		}
                                           	}
                                           	else
                                           	{
                                           		iterator.remove();
                                           	}
                                           }
                                       }
                               	}
                               	else
                               	{
                               		clusterIterator.remove();
                               	}
                               }
                           }
                           ResolveMCPGroupVO groupVO = group.doGetVO();
                           groupList.add(groupVO);
                       }
                   }
        	});
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return groupList;
    }
    
    @SuppressWarnings("rawtypes")
    public static String deregisterInstance(String clusterName, String hostName) throws IOException
    {
        String status = null;
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	status = (String) HibernateProxy.execute(() -> {
        		String statusInternal = null;
	            Query query = HibernateUtil.createQuery("select obj from ResolveMCPCluster obj where LOWER(UClusterName)=:name");
	            query.setParameter("name", clusterName.toLowerCase());
	            
	            ResolveMCPCluster mcpCluster = (ResolveMCPCluster)query.uniqueResult();
	            
	            if (mcpCluster != null)
	            {
		            if (StringUtils.isNotBlank(hostName))
		            {
		            	Collection<ResolveMCPHost> hosts = mcpCluster.getHosts();
		            	if (hosts != null)
		            	{
		            		boolean hostFound = false;
		            		for (ResolveMCPHost host : hosts)
		            		{
		            			if (host.getUHostName().equalsIgnoreCase(hostName))
		            			{
		            				hostFound = true;
		            				host.setUIsMonitored(false);
		            				statusInternal = "Host: " + hostName + " on cluster: " + clusterName + " is marked as not to be monitored";
		            				break;
		            			}
		            		}
		            		
		            		if (!hostFound)
		            		{
		            			statusInternal = "Error: No host found with the name '" + hostName + "' for deregistration";
		            		} 
		            	}
		            }
		            else
		            {
		            	mcpCluster.setUIsMonitored(false);
		            	statusInternal = "Cluster: " + clusterName + " is marked as not to be monitored";
		            }
	            }
	            else
	            {
	            	statusInternal = "Error: No cluster with the name: '" + clusterName + "' found for deregistration.";
	            }
            
	            return statusInternal;
        	});
            Log.log.info(status);
        }
        catch(Exception e)
        {
            status = "Error while marking " + (StringUtils.isNotBlank(hostName) ? " host: " + hostName + " on " : " cluster: ") + clusterName + " as not to be monitored.";
            Log.log.error(status, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return status;
    }
                    
    // ************************************************************** //
    // New MCP API's End
    // ************************************************************** //
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<MCPServerVO> getAllMCPServer(QueryDTO queryDTO, String username)
    {
        List<MCPServerVO> result = new ArrayList<MCPServerVO>();
        try
        {
        	HibernateProxy.execute(() -> {
                int start = queryDTO.getStart();
                int limit = queryDTO.getLimit();

                String hql = queryDTO.getSelectHQL();
                Query hqlQuery = HibernateUtil.createQuery(hql);
                hqlQuery.setFirstResult(start);
                hqlQuery.setMaxResults(limit);

                // Add in all the filtering parameters
                for (QueryParameter param : queryDTO.getWhereParameters())
                {
                    if (hql.contains(param.getName()))
                    {
                        hqlQuery.setParameter(param.getName(), param.getValue());
                    }
                }
                
                // Add in all sort parameters
                for (QuerySort qsort : queryDTO.getParsedSorts())
                {
                    if (hql.contains(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_")))
                    {
                        hqlQuery.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
                                              qsort.getProperty());
                    }
                }
                List<ModelToVO<VO>> rows = hqlQuery.list();

                for (ModelToVO<VO> row : rows)
                {
                    result.add((MCPServerVO) row.doGetVO());
                }

        	});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    }

    public static MCPServerVO saveMCPServer(MCPServerVO vo, String username)
    {
        if (StringUtils.isEmpty(username))
        {
            username = "system";
        }

        if (vo != null)
        {
        	final MCPServerVO internalVo = vo;
        	final String internalUsername = username;
        	
            try
            {
            	MCPServer result = (MCPServer) HibernateProxy.execute(() -> {
                	MCPServer model = null;
                	
                	
                    if (StringUtils.isNotBlank(internalVo.getSys_id()))
                    {
                    	
                        model = findServerById(internalVo.getSys_id(), internalUsername);
                        if (model != null)
                        {
                            //user cannot change the Server IP in the blueprint as it could
                            //have unpredictable result.
                            String serverIp = model.getServerIp();

                            if (!CryptUtils.isEncrypted(internalVo.getRemotePassword()))
                            {
                                internalVo.setRemotePassword(CryptUtils.encrypt(internalVo.getRemotePassword()));
                            }
                            model.applyVOToModel(internalVo);
                            //after applying the vo we'll find out if the server ip was changed
                            if(StringUtils.isNotBlank(serverIp) && !serverIp.equals(model.getServerIp()))
                            {
                                throw new Exception("LOCALHOST property in the blueprint cannot be changed, you may try recreating the server by deleting it first.");
                            }
                            HibernateUtil.getDAOFactory().getMCPServerDAO().update(model);
                        }
                    }

                    if (model == null || model.getSys_id() == null)
                    {
                        model = new MCPServer(internalVo);
                        model.setSys_id(null);
                        model.setStatus(MCPConstants.MCP_SERVER_STATUS_BLUEPRINT_NOT_DEPLOYED);
                        if (!CryptUtils.isEncrypted(internalVo.getRemotePassword()))
                        {
                            model.setRemotePassword(CryptUtils.encrypt(internalVo.getRemotePassword()));
                        }
                        HibernateUtil.getDAOFactory().getMCPServerDAO().persist(model);
                    }
                    
                    return model;
                });

                //now save the components.
                if (result != null)
                {
                    vo = result.doGetVO();

                    //manage the components
                    try
                    {
                        saveMCPComponent(vo);
                    }
                    catch (IOException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        return vo;
    }

    public static void saveMCPComponent(MCPComponentVO vo) throws IOException
    {
        try
        {
            HibernateProxy.execute(() -> {
            	MCPComponent component = new MCPComponent(vo);
                if (StringUtils.isBlank(vo.getSys_id()))
                {
                    HibernateUtil.getDAOFactory().getMCPComponentDAO().persist(component);
                }
                else
                {
                    HibernateUtil.getDAOFactory().getMCPComponentDAO().update(component);
                }
            });
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }

    private static void saveMCPComponent(MCPServerVO vo) throws IOException
    {
        String blueprintContent = vo.getBlueprint();
        InputStream inStream = new ByteArrayInputStream(blueprintContent.getBytes());
        Properties properties = new Properties();
        properties.load(inStream);

        boolean isConfigured = "true".equalsIgnoreCase(properties.get("resolve.rscontrol"));
        saveMCPComponent("rscontrol", properties, vo, isConfigured);

        isConfigured = "true".equalsIgnoreCase(properties.get("resolve.rsview"));
        saveMCPComponent("rsview", properties, vo, isConfigured);

        isConfigured = "true".equalsIgnoreCase(properties.get("resolve.rsmgmt"));
        saveMCPComponent("rsmgmt", properties, vo, isConfigured);

        isConfigured = "true".equalsIgnoreCase(properties.get("resolve.rsmq"));
        saveMCPComponent("rsmq", properties, vo, isConfigured);

        isConfigured = "true".equalsIgnoreCase(properties.get("resolve.cassandra"));
        saveMCPComponent("cassandra", properties, vo, isConfigured);

        isConfigured = "true".equalsIgnoreCase(properties.get("resolve.rsremote"));
        int rsremoteCount = 0;
        if(StringUtils.isNumeric(properties.get("rsremote.instance.count")))
        {
            rsremoteCount = Integer.parseInt(properties.get("rsremote.instance.count"));
        }
        saveRSRemoteInstance(rsremoteCount, properties, vo, isConfigured);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<MCPFileVO> getAllMCPFile(QueryDTO queryDTO, String username)
    {
        List<MCPFileVO> result = new ArrayList<MCPFileVO>();

        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                int start = queryDTO.getStart();
                int limit = queryDTO.getLimit();

                String hql = queryDTO.getSelectHQL();
                Query hqlQuery = HibernateUtil.createQuery(hql);
                if (start >= 0)
                {
                    hqlQuery.setFirstResult(start);
                }
                if (limit > 0)
                {
                    hqlQuery.setMaxResults(limit);
                }

                // Add in all the filtering parameters
                for (QueryParameter param : queryDTO.getWhereParameters())
                {
                    if (hql.contains(param.getName()))
                    {
                        hqlQuery.setParameter(param.getName(), param.getValue());
                    }
                }
                
                // Add in all sort parameters
                for (QuerySort qsort : queryDTO.getParsedSorts())
                {
                    if (hql.contains(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_")))
                    {
                        hqlQuery.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
                                              qsort.getProperty());
                    }
                }
                
                List<ModelToVO<VO>> rows = hqlQuery.list();

                for (ModelToVO<VO> row : rows)
                {
                    result.add((MCPFileVO) row.doGetVO());
                }


        	});
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    } //getAllMCPFile
    
    public static List<MCPFileTreeDTO> getMCPFileTree(QueryDTO queryDTO, String field, String username)
    {
        List<MCPFileVO> mcpFiles = getAllMCPFile(queryDTO, username);
        
        List<MCPFileTreeDTO> result = new ArrayList<MCPFileTreeDTO>();
        
        for (MCPFileVO mcpFile : mcpFiles)
        {
            MCPFileTreeDTO mcpFileTree = new MCPFileTreeDTO();
            if ("server".equalsIgnoreCase(field))
            {
                mcpFileTree.setName(mcpFile.getServerName());
                mcpFileTree.setId(mcpFile.getServerID());
                mcpFileTree.setLeaf(false);
            }
            if ("name".equalsIgnoreCase(field))
            {
                mcpFileTree.setName(mcpFile.getName());
                mcpFileTree.setId(mcpFile.getId());
                mcpFileTree.setDate(mcpFile.getDate());
                mcpFileTree.setLeaf(true);
            }
            
            if (!result.contains(mcpFileTree))
            {
                result.add(mcpFileTree);
            }
        }
        
        //sort it
        Collections.sort(result, new Comparator<MCPFileTreeDTO>()
        {
            public int compare(final MCPFileTreeDTO dto1, final MCPFileTreeDTO dto2)
            {
                int compare = 0;
                if (dto1.getDate() != null && dto2.getDate() != null)
                {
                    compare = dto1.getDate().compareTo(dto2.getDate()); 
                }
                if (compare == 0)
                {
                    compare = dto1.getName().compareToIgnoreCase(dto2.getName());
                }
                return compare;
            }
        });
        
        return result;
    } //getMCPFileTree
    
    public static boolean saveMCPFile(MCPFileVO mcp, String user) throws IOException
    {
        boolean result = false;
        try
        {       
            
            result = (boolean) HibernateProxy.execute(() -> {
            	
            	MCPFileVO mcpFileVO = mcp;
            	String username = user;
            	
            	if (StringUtils.isBlank(username))
                {
                    username = "system";
                }

            	boolean res = false;
                MCPFileContentVO mcpFileContentVO = mcpFileVO.ugetMCPFileContent();

                if (StringUtils.isBlank(mcpFileVO.getSys_id()) || mcpFileVO.getSys_id().equals(VO.STRING_DEFAULT))
                {
                    MCPFile file = new MCPFile(mcpFileVO);
                    file.setSysCreatedBy(username);
                    file.setSysUpdatedBy(username);
                    file = HibernateUtil.getDAOFactory().getMCPFileDAO().persist(file);
                    mcpFileVO = file.doGetVO();
                }
                else
                {
                    MCPFile file = HibernateUtil.getDAOFactory().getMCPFileDAO().findById(mcpFileVO.getSys_id());
                    if (file == null)
                    {
                        file = new MCPFile(mcpFileVO);
                        file.setSysCreatedBy(username);
                        file.setSysUpdatedBy(username);
                        HibernateUtil.getDAOFactory().getMCPFileDAO().persist(file);
                    }
                    else
                    {
                        file.applyVOToModel(mcpFileVO);
                        file.setSysUpdatedBy(username);
                        HibernateUtil.getDAOFactory().getMCPFileDAO().update(file);
                    }
                    mcpFileVO = file.doGetVO();
                }                
            
            

	            res = true;
	            
	            if (mcpFileContentVO != null)
	            {
	                res = saveMCPFileContent(mcpFileVO, mcpFileContentVO);
	                if (!res)
	                {
	                    deleteFileById(mcpFileVO.getId(), username);
	                }
	            }
	            
	            return res;
            
            });
        }
        catch (Throwable t)
        {
            result = false;
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        return result;
    } 
    
    @SuppressWarnings("unchecked")
    private static boolean saveMCPFileContent(MCPFileVO mcpFileVO, MCPFileContentVO mcpFileContentVO)
    {
        boolean result = false;
        try
        {
            MCPFileContent fileContent;
                
            String sql = " from MCPFileContent where mcpFile = '" + mcpFileVO.getSys_id() + "'";
                
            List<MCPFileContent> list = (List<MCPFileContent>) HibernateProxy.execute(() -> {
            	return HibernateUtil.createQuery(sql).list();
            });
                
            if (list != null && list.size() > 0)
            {
                fileContent = list.get(0);
                fileContent.setContent(mcpFileContentVO.getContent());
            }
            else
            {
                fileContent = new MCPFileContent(mcpFileContentVO);
                fileContent.setMcpFile(new MCPFile(mcpFileVO));
            }
            
            HibernateProxy.execute(() -> {
            	HibernateUtil.getDAOFactory().getMCPFileContentDAO().persist(fileContent);
            });
            result = true;
        }
        catch (Throwable t)
        {
            result = false;
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        return result;
    }

    private static String getComponentGuid(String component, Properties properties)
    {
        String result = null;
        if(StringUtils.isNotBlank(component))
        {
            result = properties.get(component + ".id.guid");
        }
        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void saveRSRemoteInstance(int rsremoteCount, Properties properties, MCPServerVO vo, boolean isConfigured)
    {
        try
        {
            HibernateProxy.execute(() -> {
                String componentName = "rsremote";
                Query query = HibernateUtil.createQuery("select obj from MCPComponent obj where serverIp=:serverIp and componentName=:componentName");
                query.setParameter("serverIp", vo.getServerIp(), StringType.INSTANCE);
                query.setParameter("componentName", componentName.toUpperCase());

                List<MCPComponent> components = query.list();
                if (!isConfigured)
                {
                    for (MCPComponent component : components)
                    {
                        HibernateUtil.getDAOFactory().getMCPComponentDAO().delete(component);
                    }
                }
                else
                {
                    //first remove all instances, simple to manage.
                    Map<String, MCPComponentVO> componentMap = new HashMap<String, MCPComponentVO>();
                    for (MCPComponent component : components)
                    {
                        componentMap.put(componentName + "#" + component.getInstanceName(), component.doGetVO());
                        //HibernateUtil.getDAOFactory().getMCPComponentDAO().delete(component);
                    }
                    for (int i = 1; i <= rsremoteCount; i++)
                    {
                        String instanceName = properties.get("rsremote.instance" + i + ".name");

                        if (!componentMap.containsKey(componentName + "#" + instanceName))
                        {
                            MCPComponent component = new MCPComponent();
                            MCPComponentVO componentVO = new MCPComponentVO();
                            componentVO.setComponentName(componentName.toUpperCase());
                            componentVO.setInstanceName(instanceName);
                            componentVO.setServerIp(vo.getServerIp());
                            componentVO.setServerName(vo.getName());
                            componentVO.setGuid(getComponentGuid(instanceName.toLowerCase(), properties));
                            component.applyVOToModel(componentVO);
                            component.setSys_id(null);
                            HibernateUtil.getDAOFactory().getMCPComponentDAO().persist(component);
                        }
                    }
                    //need to remove if the blueprint consist less instances now.
                    if (rsremoteCount < componentMap.size())
                    {
                        for (int i = componentMap.size(); i > rsremoteCount; i--)
                        {
                            String instanceName = properties.get("rsremote.instance" + i + ".name");
                            if (componentMap.containsKey(componentName + "#" + instanceName))
                            {
                                MCPComponent component = new MCPComponent();
                                MCPComponentVO componentVO = componentMap.get(componentName + "#" + instanceName);
                                component.applyVOToModel(componentVO);
                                HibernateUtil.getDAOFactory().getMCPComponentDAO().delete(component);
                            }
                        }
                    }
                }                
            });
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }

    @SuppressWarnings("rawtypes")
    private static void saveMCPComponent(String componentName, Properties properties, MCPServerVO vo, boolean isConfigured)
    {
        try
        {
            //see if the data already available

            HibernateProxy.execute(() -> {
            	Query query = HibernateUtil.createQuery("select obj from MCPComponent obj where serverIp=:serverIp and componentName=:componentName");
                query.setParameter("serverIp", vo.getServerIp(), StringType.INSTANCE);
                query.setParameter("componentName", componentName.toUpperCase());

                MCPComponent component = (MCPComponent) query.uniqueResult();
                if (isConfigured)
                {
                    if(component == null)
                    {
                        component = new MCPComponent();
                        component.setSys_id(null);
                    }
                    MCPComponentVO componentVO = new MCPComponentVO();
                    componentVO.setComponentName(componentName.toUpperCase());
                    componentVO.setServerIp(vo.getServerIp());
                    componentVO.setServerName(vo.getName());
                    componentVO.setGuid(getComponentGuid(componentName.toLowerCase(), properties));
                    component.applyVOToModel(componentVO);

                    HibernateUtil.getDAOFactory().getMCPComponentDAO().persist(component);
                }
                else
                {
                    //user may have made the component false in blueprint, we'll remove the component
                    if (component != null && StringUtils.isNotBlank(component.getSys_id()))
                    {
                        HibernateUtil.getDAOFactory().getMCPComponentDAO().delete(component);
                    }
                }
            });
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }

    public static MCPServer findServerById(String sysId, String username)
    {
        MCPServer model = null;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	model = (MCPServer) HibernateProxy.execute(() -> {

            		return HibernateUtil.getDAOFactory().getMCPServerDAO().findById(sysId);

            	});
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return model;
    }
    
    public static MCPFile findFileById(String sysId, String username)
    {
        MCPFile model = null;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	model = (MCPFile) HibernateProxy.execute(() -> {

            		return HibernateUtil.getDAOFactory().getMCPFileDAO().findById(sysId);

            	});
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return model;
    }
    
    public static List<MCPFileVO> findFilesByName(String fileName, String username)
    {
        List<MCPFileVO> files = null;
        
        if (StringUtils.isNotEmpty(fileName))
        {
            files = findFiles(fileName, null, null, username);
        }
        
        return files;
    }
    
    public static List<MCPFileVO> findFiles(String fileName, String version, Date buildDate, String username)
    {
        List<MCPFileVO> files = null;
        
        if (StringUtils.isNotEmpty(fileName) || StringUtils.isNotEmpty(version) || buildDate != null)
        {
            try
            {
                MCPFile exampleFile = new MCPFile();
                if (StringUtils.isNotEmpty(fileName))
                {
                    exampleFile.setName(fileName);
                }
                if (StringUtils.isNotEmpty(version))
                {
                    exampleFile.setVersion(version);
                }
                if (buildDate != null)
                {
                    exampleFile.setDate(buildDate);
                }

              HibernateProxy.setCurrentUser(username);
                @SuppressWarnings("unchecked")
				List <MCPFile> tmpFiles = (List<MCPFile>) HibernateProxy.execute(() -> {
	                return HibernateUtil.getDAOFactory().getMCPFileDAO().find(exampleFile);
                });
	                
	                files = new ArrayList<MCPFileVO>();
	                
	                for (MCPFile tmpFile : tmpFiles)
	                {
	                    files.add(tmpFile.doGetVO());
	                }
                
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        
        return files;
    }
    
    @SuppressWarnings("unchecked")
    public static void populateMCPFileContent(MCPFile file)
    {
        try
        {
        	HibernateProxy.execute(() -> {
        		String sql = " from MCPFileContent where mcpFile = '" + file.getSys_id() + "'";
                MCPFileContent content = null;
        		
        		List<MCPFileContent> list = HibernateUtil.createQuery(sql).list();
                if (list != null && list.size() > 0)
                {
                    content = list.get(0);
                }

                file.usetMCPFileContent(content);
        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } 

    @SuppressWarnings("rawtypes")
    public static MCPServer findServerByIP(String serverIp, String username)
    {
        MCPServer model = null;

        if (StringUtils.isNotBlank(serverIp))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	model = (MCPServer) HibernateProxy.execute(() -> {
            		
	                Query query = HibernateUtil.createQuery("select obj from MCPServer obj where serverIp=:serverIp");
	                query.setParameter("serverIp", serverIp, StringType.INSTANCE);
	
	                return (MCPServer) query.uniqueResult();

            	});
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return model;
    }

    public static boolean deleteServerById(String sysId, String username)
    {
        boolean result = true;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
	
	                MCPServer model = HibernateUtil.getDAOFactory().getMCPServerDAO().findById(sysId);
	                if (model != null)
	                {
	                    String serverIp = model.getServerIp();
	                    deleteComponentByServerIP(serverIp, username);
	
	                    HibernateUtil.getDAOFactory().getMCPServerDAO().delete(model);
	                }

            	});
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                result = false;
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return result;
    }
    
    @SuppressWarnings("rawtypes")
    public static boolean deleteFileById(String sysId, String username)
    {
        boolean result = true;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
	
	                MCPFile model = HibernateUtil.getDAOFactory().getMCPFileDAO().findById(sysId);
	                if (model != null)
	                {
	                    HibernateUtil.getDAOFactory().getMCPFileDAO().delete(model);
	                    
	                    //String sql = "delete MCPFileContent where mcpFile='" + model.getSys_id() + "'";
	                    String sql = "delete MCPFileContent where mcpFile = :modelSysId";
	                    Query hqlQuery = HibernateUtil.createQuery(sql);
	                    int i = hqlQuery.setParameter("modelSysId", model.getSys_id()).executeUpdate();
	                    Log.log.debug("mcp content deleted: " + i);
	                }
                
            	});
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                result = false;
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<MCPComponentVO> getAllMCPComponent(QueryDTO queryDTO, String username)
    {
        List<MCPComponentVO> result = new ArrayList<MCPComponentVO>();

        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                int start = queryDTO.getStart();
                int limit = queryDTO.getLimit();

                String hql = queryDTO.getSelectHQL();
                Query hqlQuery = HibernateUtil.createQuery(hql);
                hqlQuery.setFirstResult(start);
                hqlQuery.setMaxResults(limit);

                // Add in all the filtering parameters
                for (QueryParameter param : queryDTO.getWhereParameters())
                {
                    if (hql.contains(param.getName()))
                    {
                        hqlQuery.setParameter(param.getName(), param.getValue());
                    }
                }
                
                // Add in all sort parameters
                for (QuerySort qsort : queryDTO.getParsedSorts())
                {
                    if (hql.contains(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_")))
                    {
                        hqlQuery.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
                                              qsort.getProperty());
                    }
                }
                
                List<ModelToVO<VO>> rows = hqlQuery.list();

                for (ModelToVO<VO> row : rows)
                {
                    result.add((MCPComponentVO) row.doGetVO());
                }

        	});
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<MCPComponentVO> findComponentsByIds(String[] ids, String username, ConfigSQL configSQL)
    {
        List<MCPComponentVO> result = new ArrayList<MCPComponentVO>();

        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	            //see if the data already available
	            String dbType = configSQL.getDbtype().toUpperCase();
	            String queryString = SQLUtils.prepareQueryString(ids, dbType);
	            Query query = HibernateUtil.createQuery("select obj from MCPComponent obj where obj.sys_id in (" + queryString + ") order by obj.serverIp");
	            List<ModelToVO<VO>> rows = query.list();
	            for (ModelToVO<VO> row : rows)
	            {
	                result.add((MCPComponentVO) row.doGetVO());
	            }

        	});
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<MCPComponentVO> findComponentByServerIP(String serverIp, String username)
    {
        List<MCPComponentVO> result = new ArrayList<MCPComponentVO>();

        if (StringUtils.isNotBlank(serverIp))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {

	                Query query = HibernateUtil.createQuery("select obj from MCPComponent obj where serverIp=:serverIp");
	                query.setParameter("serverIp", serverIp);
	
	                List<ModelToVO<VO>> rows = query.list();
	                for (ModelToVO<VO> row : rows)
	                {
	                    result.add((MCPComponentVO) row.doGetVO());
	                }

            	});
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);

            }
        }

        return result;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void deleteComponentByServerIP(String serverIp, String username)
    {
        if (StringUtils.isNotBlank(serverIp))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                Query query = HibernateUtil.createQuery("select obj from MCPComponent obj where serverIp=:serverIp");
	                query.setParameter("serverIp", serverIp);
	
	                List<MCPComponent> rows = query.list();
	                for (MCPComponent row : rows)
	                {
	                    HibernateUtil.getDAOFactory().getMCPComponentDAO().delete(row);
	                }

                });
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }
}
