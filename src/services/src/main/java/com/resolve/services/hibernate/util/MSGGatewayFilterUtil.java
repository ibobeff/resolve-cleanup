package com.resolve.services.hibernate.util;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;

import com.resolve.persistence.model.MSGGatewayFilter;
import com.resolve.persistence.model.MSGGatewayFilterAttr;
import com.resolve.persistence.model.PullGatewayFilter;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.MSGGatewayFilterVO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class MSGGatewayFilterUtil extends GatewayFilterUtil
{
    private static final String msgGatewayFilterVOClassName = "com.resolve.services.hibernate.vo.MSGGatewayFilterVO";
    
    // This method is called by RSView to save filters.
    public static GatewayFilterVO insertMSGGatewayFilter(Map<String, String>params, String gatewayName) throws Exception {
        
        MSGGatewayFilterVO filter = null;
        
        try {
            if(!insertMSGGatewayFilterFields(params, gatewayName)) {
                Log.log.error("Failed to insert msg gateway fields.");
                throw new Exception("Failed to insert pull gateway fields.");
            }
            
            String filterId = null;
            String filterName = params.get("uname");
            String queueName = params.get("uqueue");
            
            if(StringUtils.isBlank(queueName))
                queueName = Constants.DEFAULT_GATEWAY_QUEUE_NAME;

            filterId = getMSGGatewayFilterId(gatewayName, filterName, queueName);
            
            if(filterId == null) {
                Log.log.error("Failed to insert msg gateway fields for " + filterName + ".");
                throw new Exception("Failed to insert msg gateway fields for " + filterName + ".");
            }

            String username = params.get("username");
            
            if(filterId != null) {
                Map<String, String> attrs = findMSGGatewayFilterAttributes(params);
                
                if(attrs != null) {
                    for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                        String attrName = it.next();
                        String value = attrs.get(attrName);
                        
                        if (StringUtils.isNotBlank(attrName) && attrName.equals("upassword") && StringUtils.isNotBlank(value)) {
                            try {
                                value = CryptUtils.encrypt(value);
                            } catch(Exception e) {
                                Log.log.error(e.getMessage(), e);
                            }
                        }

                        insertMSGGatewayFilterAttribute(attrName, value, filterId, username);
                    }
                }
            }
            
            filter = getMSGFilterByName(gatewayName, filterName, queueName);
            
        } catch(Exception e) {
            
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return filter;
    }
    
    private static boolean insertMSGGatewayFilterFields(Map<String, String> params, String gatewayName) throws Exception
    {

        boolean inserted = false;
        try
        {
            MSGGatewayFilter msgGatewayFilter = new MSGGatewayFilter();
            msgGatewayFilter = getMsgGatewayFilterFromMap(params);

            msgGatewayFilter.setUGatewayName(StringUtils.toCamelCase(gatewayName));
            msgGatewayFilter.setSysCreatedOn(new Timestamp(System.currentTimeMillis()));

            msgGatewayFilter.setSys_id(null);
            
            MSGGatewayFilter msgGatewayFilterFinal = msgGatewayFilter;

            HibernateProxy.execute(() -> {
            	HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().persist(msgGatewayFilterFinal);
            });            

            inserted = true;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return inserted;
    }

    private static MSGGatewayFilter getMsgGatewayFilterFromMap(Map<String, String> params)
    {
        MSGGatewayFilter msgGatewayFilter = new MSGGatewayFilter();
        msgGatewayFilter.setUActive(new Boolean(params.get("uactive")));
        msgGatewayFilter.setUDeployed(new Boolean(params.get("udeployed")));
        msgGatewayFilter.setUUpdated(new Boolean(params.get("uupdated")));
        msgGatewayFilter.setUOrder(new Integer(params.get("uorder")));   
        msgGatewayFilter.setUName(params.get("uname"));
        String queueName = params.get("uqueue");
        if(StringUtils.isBlank(queueName))
            msgGatewayFilter.setUQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
        else
            msgGatewayFilter.setUQueue(queueName);
         msgGatewayFilter.setUEventEventId(params.get("ueventEventId"));
         msgGatewayFilter.setURunbook(params.get("urunbook"));
         msgGatewayFilter.setUScript(params.get("uscript"));
         
         msgGatewayFilter.setUUsername(params.get("uusername"));
         msgGatewayFilter.setUPassword(params.get("upassword"));
         msgGatewayFilter.setUSsl(params.get("ussl"));
         msgGatewayFilter.setSysCreatedBy(params.get("username"));
         
        return msgGatewayFilter;
    }

    // This method is called by RSView to update filters.
    public static void updateMSGGatewayFilter(Map<String, String>params, String gatewayName, boolean isUpdated) throws Exception {

        String username = params.get("username");
        String filterName = params.get("uname");
        String queueName = params.get("uqueue");
        
        if(filterName == null)
            throw new Exception("Filter name is not available.");
        
        if(StringUtils.isBlank(queueName)) {
            queueName = Constants.DEFAULT_GATEWAY_QUEUE_NAME;
            params.put("uqueue", queueName);
        }
        
        String filterId = null;
        
        try {
            filterId = getMSGGatewayFilterId(gatewayName, filterName, queueName);
            
            if(filterId == null) {
                insertMSGGatewayFilterFields(params, gatewayName);
                filterId = getMSGGatewayFilterId(gatewayName, filterName, queueName);
            }
            
            else
                updateMSGGatewayFilterFields(params, gatewayName, isUpdated);
            
            Map<String, String> attrs = findMSGGatewayFilterAttributes(params);
            
            if(attrs != null) {
                HashMap<String, String> attributes = getMSGGatewayFilterAttributes(filterId);
                
                for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                    String attrName = it.next();
                    String value = attrs.get(attrName);
                    
                    if (StringUtils.isNotBlank(value) && attrName.equals("upassword") && StringUtils.isNotBlank(value) && !value.startsWith("ENC1:")) {
                        try {
                            value = CryptUtils.encrypt(value);
                        } catch(Exception e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    
                    if(attributes != null && attributes.containsKey(attrName))
                        updateMSGGatewayFilterAttribute(attrName, value, filterId, username);
                    else
                        insertMSGGatewayFilterAttribute(attrName, value, filterId, username);
                }
            }
            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    private static Map<String, String> findMSGGatewayFilterAttributes(Map<String, String> params) throws Exception {
        
        Map<String, String> attrs = new HashMap<String, String>();
        
        try {
            Method[] methods = Class.forName(msgGatewayFilterVOClassName).newInstance().getClass().getMethods();
            
            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                if(key == null)
                    continue;
                
                boolean found= false;
                
                for(Method method:methods) {
                    String name = method.getName();
                    if(method.getAnnotation(MappingAnnotation.class) != null) {
                        String attr = name.substring(3);
                        if(key.equalsIgnoreCase(attr)) {
                            found = true;
                            break;
                        }
                    }
                }
                
                if(found)
                    continue;
                else if(!key.startsWith("u") || key.equals("username"))
                    continue;
                else
                    attrs.put(key,  params.get(key));
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return attrs;
    }
    
    @SuppressWarnings("unchecked")
	public static HashMap<String, String> getMSGGatewayFilterAttributes(String filterId) throws Exception
    {
        HashMap<String, String> attributes = new HashMap<String, String>();
        List<MSGGatewayFilterAttr> filterAttrs = new ArrayList<MSGGatewayFilterAttr>();
        MSGGatewayFilterAttr filterAttr = new MSGGatewayFilterAttr();
        try
        {

            filterAttr.setUMSGFilterId(filterId);
            filterAttrs = (List<MSGGatewayFilterAttr>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getMSGGatewayFilterAttrDAO().find(filterAttr);
            });

            if (filterAttrs != null && filterAttrs.size() >= 0)
            {
                for (MSGGatewayFilterAttr msgFilterAttr : filterAttrs)
                {
                    if (msgFilterAttr != null)
                    {
                        attributes.put(msgFilterAttr.getUName(), msgFilterAttr.getUValue());
                    }
                }
            }

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return attributes;
    }
    
    @SuppressWarnings("unchecked")
	public static List<String> getMSGGatewayFilterAttributeIds(String filterId)
    {
        MSGGatewayFilterAttr filterAttr = new MSGGatewayFilterAttr();
        List<MSGGatewayFilterAttr> filterAttrs = new ArrayList<MSGGatewayFilterAttr>();
        List<String> attributeIds = new ArrayList<String>();
        try
        {
            filterAttr.setUMSGFilterId(filterId);
            filterAttrs = (List<MSGGatewayFilterAttr>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getMSGGatewayFilterAttrDAO().find(filterAttr);
            });
            if (filterAttrs != null && filterAttrs.size() >= 0)
            {
                for (MSGGatewayFilterAttr filter : filterAttrs)
                {
                    attributeIds.add(filter.getSys_id());
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return attributeIds;
    }
    
    private static void insertMSGGatewayFilterAttribute(String attrName, String value, String filterId, String username) throws Exception
    {
        try
        {
            HibernateProxy.execute(() -> {
            	 MSGGatewayFilterAttr pullGatewayFilterAttr = new MSGGatewayFilterAttr();
                 pullGatewayFilterAttr.setSysCreatedBy(username);
                 pullGatewayFilterAttr.setSysCreatedOn(new Timestamp(System.currentTimeMillis()));
                 pullGatewayFilterAttr.setUName(attrName);
                 pullGatewayFilterAttr.setUValue(value);
                 pullGatewayFilterAttr.setUMSGFilterId(filterId);
                 pullGatewayFilterAttr.setSys_id(null);

                 HibernateUtil.getDAOFactory().getMSGGatewayFilterAttrDAO().persist(pullGatewayFilterAttr);
            });
           
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    private static void updateMSGGatewayFilterAttribute(String attrName, String value, String filterId, String username) throws Exception
    {
        try
        {
            HibernateProxy.execute(() -> {
            	 Query query = HibernateUtil.getCurrentSession().createQuery("update MSGGatewayFilterAttr " + " set UValue = :uvalue, sysUpdatedBy= :sysupdatedby, sysUpdatedOn= :sysupdatedon" + " where UMSGFilterId = :msgfilterid and UName= :uname ");
                 query.setParameter("uvalue", value);
                 query.setParameter("sysupdatedby", username);
                 query.setParameter("sysupdatedon", (new Timestamp(System.currentTimeMillis())));
                 query.setParameter("msgfilterid", filterId);
                 query.setParameter("uname", attrName);

                 return query.executeUpdate();
            });
           
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    private static void updateMSGGatewayFilterFields(Map<String, String>params, String gatewayName, boolean isUpdated) throws Exception {
        
        try {
            
            HibernateProxy.execute(() -> {
            	 Query query = HibernateUtil.getCurrentSession().createQuery("update MSGGatewayFilter"
                         + " set UActive = :uactive, UDeployed= :udeployed, UOrder = :uorder, " +
                          "  UEventEventId = :ueventid, URunbook= :urunbook, UScript= :uscript, "
                          + "sysUpdatedBy = :updtBy, " 
                          + "sysUpdatedOn = :updtOn, "
                          + "UUpdated = :isUpdated, "
                          + "UUsername = :uusername, "
                          + "UPassword = :upassword, "
                          + "USsl = :ussl "
                          + "where UQueue = :uqueue and UName = :uname and UGatewayName= :ugatewayname");

                 query.setParameter("uactive", new Boolean(params.get("uactive")));
                 query.setParameter("udeployed", new Boolean(params.get("udeployed")));
                 query.setParameter("uorder", new Integer(params.get("uorder")));
                 query.setParameter("ueventid", params.get("ueventeventid"));
                 query.setParameter("urunbook", params.get("urunbook"));
                 query.setParameter("uscript", params.get("uscript"));
                 query.setParameter("updtBy", params.get("username"));
                 query.setParameter("updtOn", new Timestamp(System.currentTimeMillis()));
                 if(isUpdated)
                     query.setParameter("isUpdated", new Boolean(true));
                 else
                     query.setParameter("isUpdated", new Boolean(false));
                 query.setParameter("uname",params.get("uname"));
                 query.setParameter("ugatewayname", gatewayName);
                 query.setParameter("uqueue", params.get("uqueue"));
                 query.setParameter("uusername", params.get("uusername"));
                 query.setParameter("upassword", params.get("upassword"));
                 query.setParameter("ussl", params.get("ussl"));
                 
                 return query.executeUpdate();
            });
           

        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    public static MSGGatewayFilterVO getMSGFilterByName(String gatewayName, String filterName, String queueName) throws Exception {
        
        List<MSGGatewayFilterVO> filters = getMSGFiltersByName(gatewayName, filterName, queueName);
        if(filters.size() > 0)
            return filters.get(0);
        
        return null;
    }
    
    @SuppressWarnings("unchecked")
	public static List<MSGGatewayFilterVO> getMSGFiltersByName(String gatewayName, String filterName, String queueName) throws Exception
    {

        MSGGatewayFilterVO filter = null;
        List<MSGGatewayFilterVO> filters = new ArrayList<MSGGatewayFilterVO>();
        List<MSGGatewayFilter> msgFilters = new ArrayList<MSGGatewayFilter>();
        MSGGatewayFilter msgFilter = new MSGGatewayFilter();

        try
        {
        	if (StringUtils.isNotBlank(filterName)) msgFilter.setUName(filterName);
            msgFilter.setUGatewayName(StringUtils.toCamelCase(gatewayName));
            if (StringUtils.isNotBlank(queueName)) msgFilter.setUQueue(queueName);
            msgFilters = (List<MSGGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().find(msgFilter);
            });
            if (msgFilters != null && msgFilters.size() >= 0)
            {
                for (MSGGatewayFilter gatewayMSGFilter : msgFilters)
                {
                    filter = gatewayMSGFilter.doGetVO();
                    filter.setAttrs(getMSGGatewayFilterAttributes(gatewayMSGFilter.getSys_id()));
                    filter.setChecksum(gatewayMSGFilter.getChecksum());
                    filters.add(filter);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return filters;
    }

    public static MSGGatewayFilterVO getMSGFilter(String id) throws Exception
    {
        
        MSGGatewayFilter filter = (MSGGatewayFilter) HibernateProxy.execute(() -> {
        	return HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().findById(id);
        });        

        MSGGatewayFilterVO filterVo = new MSGGatewayFilterVO();
        if (filter != null)
        {
            filterVo = filter.doGetVO();
            Map<String, String> attrs = getMSGGatewayFilterAttributes(id);
            filterVo.setAttrs(attrs);
        }

        return filterVo;
    }
    
    @SuppressWarnings("unchecked")
	public static List<GatewayVO> listMSGFilters(String gatewayName, String queueName, Boolean deployed) throws Exception
    {

        List<GatewayVO> filters = new ArrayList<GatewayVO>();
        MSGGatewayFilter msgGatewayFilter = new MSGGatewayFilter();
        List<MSGGatewayFilter> msgGatewayFilters = new ArrayList<MSGGatewayFilter>();

        try
        {
            msgGatewayFilter.setUGatewayName(StringUtils.toCamelCase(gatewayName));
            if (deployed != null)
            {
                if (!deployed)
                    msgGatewayFilter.setUQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
                else
                    msgGatewayFilter.setUQueue(queueName.toUpperCase());
            }

            msgGatewayFilters = (List<MSGGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().find(msgGatewayFilter);
            });            

            for (MSGGatewayFilter msgFilter : msgGatewayFilters)
            {

                MSGGatewayFilterVO filter = msgFilter.doGetVO();
                filter.setAttrs(getMSGGatewayFilterAttributes(msgFilter.getSys_id()));
                filters.add(filter);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return filters;
    }
    
    public static boolean deleteMSGFilters(String gatewayName, String[] ids)
    {

        boolean result = false;
        try
        {

			List<String> idList = Arrays.asList(ids);
			result = (boolean) HibernateProxy.execute(() -> {
				HibernateUtil.deleteQuery("MSGGatewayFilter", "sys_id", idList);
				HibernateUtil.deleteQuery("MSGGatewayFilterAttr", "UMSGFilterId", idList);
				return true;
			});
        }
        catch (Exception e)
        {
            Log.log.error("++++++++++++++++++++++++++++++++++++++++");
            Log.log.error("Error thrown when deleting MSG filters, the delete may have failed");
            Log.log.error("++++++++++++++++++++++++++++++++++++++++");
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    }
    
    public static Map<String, Map<String, Object>> loadMSGGatewayFilters(String gatewayName, String queueName, Boolean deployed) throws Exception {
        
        Map<String, Map<String, Object>> filterMap = null;
        List<MSGGatewayFilter> results = new ArrayList<MSGGatewayFilter>();
        try {
            MSGGatewayFilter gatewayFilter = new MSGGatewayFilter();
            gatewayFilter.setUGatewayName(StringUtils.toCamelCase(gatewayName));
//            gatewayFilter.setUQueue(queueName);
            
            MSGGatewayFilter gf = gatewayFilter;
            results = (List<MSGGatewayFilter>) HibernateProxy.execute(() -> {
            	return  HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().find(gf);
            });
            
            filterMap = new HashMap<String, Map<String, Object>>();
             Map<String, Object> filter = new HashMap<String, Object>();
             
             if(results.size() > 0) {
                 gatewayFilter = results.get(0);
                 filter.put("SYS_ID",gatewayFilter.getSys_id());
                 filter.put("ACTIVE", gatewayFilter.getUActive());
                 filter.put("DEPLOYED", gatewayFilter.getUDeployed());
                 filter.put("ORDER", gatewayFilter.getUOrder());
                 filter.put("EVENT_ID", gatewayFilter.getUEventEventId());
                 filter.put("RUNBOOK", gatewayFilter.getURunbook());
                 filter.put("SCRIPT", gatewayFilter.getUScript());
                 filter.put("UPDATED", gatewayFilter.getUUpdated());
                 filter.put("QUEUE", gatewayFilter.getUQueue());
                 filter.put("ID", gatewayFilter.getUName());
                 filter.put("GATEWAY_NAME", gatewayFilter.getUGatewayName());

                 filter.put("USERNAME", gatewayFilter.getUUsername());
                 filter.put("PASSWORD", gatewayFilter.getUPassword());
                 filter.put("USSL", gatewayFilter.getUSsl());
                 
                 filterMap.put(gatewayFilter.getUName(), filter);
            }
        }catch(Exception ex) {
            Log.log.error(ex.getMessage(), ex);
            throw ex;
        }
        return filterMap;
    }

    public static void updateMSGGatewayFilter(MSGGatewayFilterVO msg, String filterId) throws Exception
    {
        try
        {
            HibernateProxy.execute(() -> {
            	MSGGatewayFilter filter = new MSGGatewayFilter();
                filter.applyVOToModel(msg);
                HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().update(filter);
            });
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    public static String getMSGGatewayFilterId(String gatewayName, String filterName, String queueName) throws Exception
    {
        try
        {
            String id = null;
            MSGGatewayFilter msgFilter = new MSGGatewayFilter();
            msgFilter.setUName(filterName);
            msgFilter.setUGatewayName(StringUtils.toCamelCase(gatewayName));
            if (StringUtils.isNotBlank(queueName)) msgFilter.setUQueue(queueName);
            @SuppressWarnings("unchecked")
			List<MSGGatewayFilter> msgFilters = (List<MSGGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().find(msgFilter);
            });
            
            if (!msgFilters.isEmpty() && msgFilters.size() == 1)
            {
                id = msgFilters.get(0).getSys_id();
            }

            return id;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public static List<? extends GatewayVO> getMSGFilterForQueue(String gatewayName, String queueName) throws Exception {
        return getMSGFiltersByName(gatewayName, null, queueName);
    }
}
