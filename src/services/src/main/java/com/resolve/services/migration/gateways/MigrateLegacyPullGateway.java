/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.gateways;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.migration.gateways.MigrateLegacyGateways;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;

public class MigrateLegacyPullGateway extends MigrateLegacyGateways {
    
    private String gatewayName;
    
    public MigrateLegacyPullGateway(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public void migrate() {
        
        if(StringUtils.isBlank(gatewayName))
            return;

        Log.log.info("======= Start migration of " + gatewayName + " gateway ========");
        Log.log.info("");
        
        long startAt = System.currentTimeMillis();       
        
        try {
        	HibernateProxy.execute(() -> {
            	SQLConnection conn = new SQLConnection(HibernateUtil.getCurrentSession().connection());
            	migrateStandardFields(conn);
            	migrateCustomFields(conn);
            	updateSysAppModule(gatewayName, gatewayName+" Gateway", conn);            
        	});
        } catch(Exception e) {            
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
        long endAt = System.currentTimeMillis();
        
        Log.log.info("Migration is completed in " + (endAt - startAt)/100 + " seconds");
        Log.log.info("");
        
        Log.log.info("======= Finish migration of " + gatewayName + " gateway ========");
        Log.log.info("");
    }
    
/* ========================================================================== */

    public List<String> getStandardQueries() {
        
        List<String> sqlList = new ArrayList<String>();
        
        String sql = "INSERT INTO pull_gateway_filter (sys_id, sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, "
                        + "sys_updated_by, sys_updated_on, u_active, u_event_eventid, u_interval, u_name, u_order, u_queue, u_runbook, u_script, u_gateway_name, "
                        + "u_deployed, u_updated, u_query, u_last_id, u_last_value, u_last_timestamp, u_time_range) "
                        + "SELECT sys_id, sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, "
                        + "sys_updated_by, sys_updated_on, u_active, u_event_eventid, u_interval, u_name, u_order, u_queue, u_runbook, u_script, '" + gatewayName + "', ";
        
        StringBuilder sb = new StringBuilder(sql);
        
        String modelName = gatewayName.toLowerCase() + "_filter";
        
        // Append the column names in the legacy filter table to populate the u_script and u_query fields in the new SDK filter table.
        // If those two fields are not available, simple append '' for these two fields.
        switch(modelName) {
            case "remedyx_filter":
                sb.append("0, 0, u_query, '', '', '', '' ");
                break;
            case "netcool_filter":
                sb.append("0, 0, u_sql, '', '', '', '' ");
                break;
            default:
                break;
        }
        
        sb.append("FROM ").append(modelName);
        
        sqlList.add(sb.toString());
        
        String sql2 = "UPDATE pull_gateway_filter SET u_deployed=1 WHERE u_queue<>'" + Constants.DEFAULT_GATEWAY_QUEUE_NAME + "'";
        sqlList.add(sql2);
        
        return sqlList;
    }
    
    public List<String> getCustomQueries(SQLConnection conn) throws Exception {
        
        List<String> sqlList = new ArrayList<String>();
        
        String modelName = gatewayName.toLowerCase() + "_filter";
        
        switch(modelName) {
            case "remedyx_filter":
                List<String> filterIds = getAllFilterIds(modelName, conn);
                sqlList= getRemedyxCustomQueries(filterIds, conn);
                break;
            default:
                break;
        }
        
        return sqlList;
    }

    private static List<List<String>> getFormQueueList(String filterId, SQLConnection conn) throws Exception {
        
        List<List<String>> list = new ArrayList<List<String>>();
        
        PreparedStatement selectStmt = null;
        ResultSet rs = null;
        String queueName = null;
        String formName = null;
        
        String sql = "SELECT filter.u_queue, attr.u_value FROM pull_gateway_filter filter, pull_gateway_filter_attr attr " + 
                     "WHERE attr.pull_filter_id=filter.sys_id AND attr.u_name='uformname' AND filter.sys_id=?";
        
        try {
            conn = SQL.getConnection();
            selectStmt = conn.prepareStatement(sql);
            selectStmt.setString(1, filterId);
            rs = selectStmt.executeQuery();

            while(rs.next()) {
                queueName = rs.getString(1);
                formName = rs.getString(2);
                
                if(StringUtils.isNotEmpty(queueName) && StringUtils.isNotEmpty(formName)) {
                    List<String> pair = new ArrayList<String>();
                    pair.add(queueName);
                    pair.add(formName);
                    
                    list.add(pair);
                }
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try
            {
                if (selectStmt != null)
                    selectStmt.close();
                if (rs != null)
                    rs.close();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return list;
    }
    
    private static List<String> getRemedyxCustomQueries(List<String> filterIds, SQLConnection conn) throws Exception {
        
        List<String> sqlList = new ArrayList<String>();
        
        if(filterIds.size() > 0) {
            for(String filterId:filterIds) {
                String sysId = SysId.generate(conn);
                
                String sql1 = "INSERT INTO pull_gateway_filter_attr (sys_id, sys_created_by, sys_created_on, sys_is_deleted, "
                                + "sys_mod_count, sys_org, sys_updated_by, sys_updated_on, u_name, u_value, pull_filter_id) "
                                + "SELECT '" + sysId + "', sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, sys_updated_by, sys_updated_on, "
                                + "'uformName'" + ", u_form_name, '" + filterId + "' FROM remedyx_filter WHERE sys_id='" + filterId + "'";
                
                sysId = SysId.generate(conn);
                String sql2 = "INSERT INTO pull_gateway_filter_attr (sys_id, sys_created_by, sys_created_on, sys_is_deleted, "
                                + "sys_mod_count, sys_org, sys_updated_by, sys_updated_on, u_name, u_value, pull_filter_id) "
                                + "SELECT '" + sysId + "', sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, sys_updated_by, sys_updated_on, "
                                + "'ulastValueField'" + ", u_last_value_field, '" + filterId + "' FROM remedyx_filter WHERE sys_id='" + filterId + "'";
                
                Log.log.debug(sql1);
                Log.log.debug(sql2);
                
                sqlList.add(sql1);
                sqlList.add(sql2);
                
                List<List<String>> list = getFormQueueList(filterId, conn);
                for(List<String> entry:list) {
                    sysId = SysId.generate(conn);
                    
                    String queueName = entry.get(0);
                    String formName = entry.get(1);
                    
                    String sql = "INSERT INTO pull_gateway_filter_attr (sys_id, sys_created_by, sys_created_on, sys_is_deleted, "
                                    + "sys_mod_count, sys_org, sys_updated_by, sys_updated_on, u_name, u_value, pull_filter_id) "
                                    + "SELECT '" + sysId + "', sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, sys_updated_by, sys_updated_on, "
                                    + "'ufieldList'" + ", form.u_field_list, '" + filterId + "' FROM remedyx_form form, remedyx_filter filter " 
                                    + "WHERE form.u_name='" + formName + "' AND form.u_queue='" + queueName + "'";
                    
                    Log.log.debug(sql);
                    sqlList.add(sql);
                }
            }
        }
        
        return sqlList;
    }

} // class MigrateLegacyPullGateway
