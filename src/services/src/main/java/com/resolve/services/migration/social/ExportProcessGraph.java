/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.social;

import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;

public class ExportProcessGraph extends ExportComponentGraph
{
    public ExportProcessGraph(String sysId) throws Exception
    {
        super(sysId, SocialRelationshipTypes.PROCESS);
    }

    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode) throws Exception
    {
        //get all the nodes that this process node is refering too
        Iterable<Node> membersOfProcessNode = findMembersBelongingToProcess(compNode);
        if (membersOfProcessNode != null)
        {
            for (Node anyNode : membersOfProcessNode)
            {
                    prepareReturnNode(compNode, anyNode);    
            }//end of for
        }

        return relationships;
    }

    private void prepareReturnNode(Node compNode, Node anyNode) throws Exception
    {
        String type = (String) anyNode.getProperty(GraphDBManager.TYPE);
        String displayName = (String) anyNode.getProperty(GraphDBManager.DISPLAYNAME);
        String sysId = (String) anyNode.getProperty(GraphDBManager.SYS_ID);

        if (type.equalsIgnoreCase(SocialRelationshipTypes.FORUM.name()))
        {
            addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());
        }
        else if (type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
        {
            addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());
        }
        else if (type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
        {
            addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());
        }
        else if (type.equalsIgnoreCase(SocialRelationshipTypes.ACTIONTASK.name()))
        {
            addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());
        }
        else if (type.equalsIgnoreCase(SocialRelationshipTypes.RSS.name()))
        {
            addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());
        }
        else if (type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
        {
            addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());
        }

    }
    
    private Iterable<Node> findMembersBelongingToProcess(Node processNode)
    {
        TraversalDescription traversalProcessForAllItsMembers =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1));
                        
        return traversalProcessForAllItsMembers.traverse(processNode).nodes();
        
    }

}
