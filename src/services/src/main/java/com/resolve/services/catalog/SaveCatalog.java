/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.catalog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.ResolveCatalog;
import com.resolve.persistence.model.ResolveCatalogEdge;
import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.model.ResolveCatalogNodeTagRel;
import com.resolve.persistence.model.ResolveCatalogNodeWikidocRel;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceTag;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.TagUtil;
import com.resolve.services.hibernate.vo.ResolveCatalogVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

/**
 * 
 * util class that converts the UI Tree Catalog struture into records to be inserted in DB
 * 
 * @author jeet.marwah
 *
 */
public class SaveCatalog
{
    private Catalog catalog = null;
    private String username = null;
    
    public SaveCatalog(Catalog catalog, String username) throws Exception
    {
        this.catalog = catalog;
        this.username = username;
        
        //verify the catalog object
        String errors = verifyCatalog(catalog, username);
        if(StringUtils.isNotEmpty(errors))
        {
            throw new Exception(errors);
        }
    }

    public Catalog persist() throws Exception
    {
        String catalogName = catalog.getName();
        String type = catalog.getCatalogType();
        
        Set<String> goodSysIds = getSysIdsOfResolveCatalogNodes(catalog);
        ResolveCatalog resolveCatalog = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	resolveCatalog = (ResolveCatalog) HibernateProxy.execute(() -> {
        		ResolveCatalog resolveCatalogResult = null;
        		
	            //persist the ROOT rec
	            if(StringUtils.isNotEmpty(catalogName))
	            {
	                resolveCatalogResult = CatalogUtil.findCatalogModel(null, catalogName, username);
	            }
	
	            if (resolveCatalogResult == null && catalog.getParentSysId() != null)
	            {
	                resolveCatalogResult = CatalogUtil.findCatalogModel(catalog.getParentSysId(), null, username);    
	            }
	            
	            
	            //for older recs, the catalog rec was not there
	            if(resolveCatalogResult == null)
	            {
	                //this is new catalog
	                resolveCatalogResult = new ResolveCatalog();
	            }
	            else
	            {
	                if (StringUtils.isBlank(catalog.getId()))
	                {
	                    /*
	                     * The resolveCatalog is not null but catalog.getId() is null. That means, user is trying to create a
	                     * new catalog with the same name as that of an existing catalog.
	                     */
	                    throw new Exception("Catalog with the name " + catalog.getName() + " already exist. Use different name.");
	                }
	                //delete the unwanted sysIds that were deleted from the UI
	                deleteUnwantedNodes(resolveCatalogResult.getSys_id(), goodSysIds);
	            }
	            
	            //this may be a Rename, so update the parent rec
	            resolveCatalogResult.setUName(catalogName);
	            resolveCatalogResult.setUType(type);
	            
	            //persist it
	            HibernateUtil.getDAOFactory().getResolveCatalogDAO().persist(resolveCatalogResult);
	            
	            //delete the records in the edges table for this catalogId/graphId
	            //Note that we can do it at each node level also but that will be too many deletes for each 'source' and 'catalog'
	            deleteRelationshipsForCatalog(resolveCatalogResult.getSys_id());
	            
	            //build the catalog hierarchy
	            persistNodes(resolveCatalogResult, catalog);
	            
	            return resolveCatalogResult;
        	});
            
            //get the updated catalog
            catalog = new GetCatalog(resolveCatalog.getSys_id(), null, null).getCatalog();
            
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        
        return catalog;
    }
    
    //**KEEP AN EYE ON THE BAD RECS..MAY HAVE TO DO SELECT FIRST, get the SYSIDs and than DELETE THAT EXPLICITLY
    private void deleteRelationshipsForCatalog(String catalogSysId) throws Exception
    {
        //delete the records in the edges table for this catalogId/graphId
        //Note that we can do it at each node level also but that will be too many deletes for each 'source' and 'catalog'
        deleteRelationships(catalogSysId, "ResolveCatalogEdge");
        deleteRelationships(catalogSysId, "ResolveCatalogNodeWikidocRel");
        deleteRelationships(catalogSysId, "ResolveCatalogNodeTagRel");
        
//        String sql = "delete from ResolveCatalogEdge where catalog = '" + catalogSysId + "'";
//        ServiceHibernate.executeHQLUpdate(sql, username);
//        
//        sql = "delete from ResolveCatalogNodeWikidocRel where catalog = '" + catalogSysId + "'";
//        ServiceHibernate.executeHQLUpdate(sql, username);
//        
//        sql = "delete from ResolveCatalogNodeTagRel where catalog = '" +catalogSysId + "'";
//        ServiceHibernate.executeHQLUpdate(sql, username);            
        
        HibernateUtil.getCurrentSession().flush(); //making sure that deletes are done
    }
    
    private ResolveCatalogNode persistNodes(ResolveCatalog resolveCatalog, Catalog catalog) throws Exception
    {
        ResolveCatalogNode sourceNode = transformCatalogToVO(catalog);
        sourceNode.setCatalog(resolveCatalog);
        
        //persist it right away
//        if(StringUtils.isNotBlank(sourceNode.getSys_id()))
//            HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().replicate(sourceNode);
//        else
            HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().persist(sourceNode);
           
        //tags
        saveTags(resolveCatalog, sourceNode, catalog.getTags());
        
        //wiki/document
        saveWikidocument(resolveCatalog, sourceNode, catalog);
        
        //children nodes
        if(catalog.getType().equalsIgnoreCase("CatalogReference") && !catalog.isRoot())
        {
            //skip the build of 'children' for reference types
        }
        else
        {
            if (catalog.getChildren() != null && catalog.getChildren().size() > 0)
            {
                //NOTE:We can delete here but to reduce the number of delete queries, doing at the top with the 'catalogId'
                //Create new relationships
                int order = 1;
                for (Catalog child : catalog.getChildren())
                {
                    ResolveCatalogNode destNode = persistNodes(resolveCatalog, child); //** RECURSIVE
                    
                    //create and persist the edge
                    persistEdge(resolveCatalog, sourceNode, destNode, order);
                    
                    order++;
                }//end of for loop
            }//end of if
        }//end of else 
        
        return sourceNode;
    }
    
    private void persistEdge(ResolveCatalog resolveCatalog, ResolveCatalogNode sourceNode, ResolveCatalogNode destNode, int order)
    {
        //create and persist the edge
        ResolveCatalogEdge edge = new ResolveCatalogEdge();
        edge.setCatalog(resolveCatalog);
        edge.setSourceNode(sourceNode);
        edge.setDestinationNode(destNode);
        edge.setUOrder(order);
        
        HibernateUtil.getDAOFactory().getResolveCatalogEdgeDAO().persist(edge);
    }
    
    private ResolveCatalogNode transformCatalogToVO(Catalog catalog)
    {
        ResolveCatalogNode catalogNode = null;
        
        if(StringUtils.isNotEmpty(catalog.getId()))
        {
            //update
            catalogNode = HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().findById(catalog.getId());
            if(catalogNode == null)
            {
                //this can happen in case of migration
                catalogNode = new ResolveCatalogNode();
                catalogNode.setSysCreatedBy(catalog.getSys_created_by());
                catalogNode.setSysUpdatedBy(catalog.getSys_updated_by());
                catalogNode.setSysCreatedOn(new Date(catalog.getSys_created_on()));
                catalogNode.setSysUpdatedOn(new Date(catalog.getSys_updated_on()));
            }
        }
        else
        {
            //create
            catalogNode = new ResolveCatalogNode();
        }
        
        catalogNode.setUName(catalog.getName());
        catalogNode.setUType(catalog.getType());
        catalogNode.setUOperation(catalog.getOperation());
//        catalogNode.setUOrder(catalog.getOrder());
        catalogNode.setURoles(catalog.getRoles());
        catalogNode.setUEditRoles(catalog.getEditRoles());
        catalogNode.setUTitle(catalog.getTitle());
        catalogNode.setUDescription(catalog.getDescription());
        catalogNode.setUImage(catalog.getImage());
        catalogNode.setUImageName(catalog.getImageName());
        catalogNode.setUTooltip(catalog.getTooltip());
        catalogNode.setUWiki(catalog.getWiki());
        catalogNode.setULink(catalog.getLink());
        catalogNode.setUForm(catalog.getForm());
        catalogNode.setUDisplayType(catalog.getDisplayType());
        catalogNode.setUMaxImageWidth(catalog.getMaxImageWidth());
        catalogNode.setUPath(catalog.getPath());
        catalogNode.setUOpenInNewTab(catalog.isOpenInNewTab());
        catalogNode.setUSideWidth(catalog.getSideWidth());
        catalogNode.setUCatalogType(catalog.getCatalogType());
        
        catalogNode.setUWikidocSysID(catalog.getWikidocSysID());
        catalogNode.setUInternalName(catalog.getInternalName());
        catalogNode.setUIcon(catalog.getIcon());
        
        catalogNode.setUNamespace(catalog.getNamespace());
        catalogNode.setUIsRoot(catalog.isRoot());
        catalogNode.setUIsRootRef(catalog.isRootRef());
        
        //tags
//        vo.setTags(catalog.getTags());
        
        if(catalog.getType().equalsIgnoreCase("CatalogReference") && !catalog.isRoot())
        {
            //this is reference, so get the refeerence sysId and populate
            ResolveCatalogVO refCat = CatalogUtil.findCatalog(null, catalogNode.getUName(), "admin");
            if(refCat != null)
            {
                catalogNode.setUReferenceCatalogSysId(refCat.getSys_id());
            }
        }
        
        
        
        return catalogNode;
        
    }
    
    
    private String verifyCatalog(Catalog catalog, String username)
    {
        StringBuffer errors = new StringBuffer();
        
        if(catalog != null)
        {
            //validate if the catalog name already exist
//            try
//            {
//                String catalogName = catalog.getName();
//                String parentSysId = catalog.getParentSysId();
//                
//                if(StringUtils.isNotBlank(parentSysId))
//                {
//                    
//                }
//                else
//                {
//                    
//                }
//                
//                
//                ResolveCatalog testCatalogName = CatalogUtil.findCatalogModel(null, catalogName, username);
//                if(testCatalogName != null)
//                {
//                    if(StringUtils.isNotBlank(parentSysId) && !testCatalogName.getSys_id().equalsIgnoreCase(parentSysId))
//                    {
//                        errors.append("Catalog with name " + catalogName + " already exist. Please make the name unique.");
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Log.log.error("error getting the catalog with name for validation", e);
//                errors.append(e.getMessage());
//            }

            //validate if the doc names are valid
            try
            {
                catalog.validate();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                errors.append(e.getMessage());
            }
            
            
           //validate if the docs referenced are valid - we will be creating stubs/place holder for docs
//           Set<String> docNames = catalog.findAllDocumentNames();
//           if(docNames != null && docNames.size() > 0)
//           {
//               for(String docFullName : docNames)
//               {
//                   if(StringUtils.isNotEmpty(docFullName))
//                   {
//                       WikiDocumentVO doc = ServiceWiki.getWikiDoc(null, docFullName, "system");
//                       if(doc == null)
//                       {
//                           errors.append("Document " + docFullName + " does not exist.").append("\n");
//                       }
//                   }
//               }
//           }
        }
        
        return errors.toString();
    }
    
    private Set<String> getSysIdsOfResolveCatalogNodes(Catalog catalog)
    {
        Set<String> goodSysIds = new HashSet<String>();
        if(StringUtils.isNotBlank(catalog.getId()))
        {
            goodSysIds.add(catalog.getId());
        }

        
        if(catalog.getChildren() != null && catalog.getChildren().size() > 0)
        {
            for(Catalog child : catalog.getChildren())
            {
                goodSysIds.addAll(getSysIdsOfResolveCatalogNodes(child));// ** RECURSIVE
            }
        }
        
        return goodSysIds;
    }
    
    private void deleteRelationships(String catalogSysId, String tableModelName) throws Exception
    {
        //String whereClause = "catalog = '" + catalogSysId + "'";
        Collection<String> catalogSysIds = new ArrayList<String>();
        catalogSysIds.add(catalogSysId);
        
        Set<String> sysIdsToDelete = GeneralHibernateUtil.getSysIdsFor(tableModelName, "catalog.id" , username, catalogSysIds);
        
        if(sysIdsToDelete != null && sysIdsToDelete.size() > 0)
        {
            String deleteWhereClause = "catalog = '" + catalogSysId + "' and sys_id IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(sysIdsToDelete)) + ")";
            String sql = "delete from " + tableModelName + " where " + deleteWhereClause;
            ServiceHibernate.executeHQLUpdate(sql, username);
        }
    }
    

    private void deleteUnwantedNodes(String catalogSysId, Set<String> goodSysIds)
    {
        //delete the unwanted sysIds that were deleted from the UI
        Collection<String> catSysIds = new ArrayList<String>();
        catSysIds.add(catalogSysId);
        
        Set<String> sysIdsToDelete = GeneralHibernateUtil.getSysIdsFor("ResolveCatalogNode", "catalog.id", username, catSysIds, 
                                                                       "id", goodSysIds);
                
        if(sysIdsToDelete != null && sysIdsToDelete.size() > 0)
        {
            for(String sysId : sysIdsToDelete)
            {
                ResolveCatalogNode deleteNode = HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().findById(sysId);
                
                Collection<ResolveCatalogNodeWikidocRel> catalogWikiRels = deleteNode.getCatalogWikiRels();
                if(catalogWikiRels != null && catalogWikiRels.size() > 0)
                {
                    for(ResolveCatalogNodeWikidocRel rel : catalogWikiRels)
                    {
                        HibernateUtil.getDAOFactory().getResolveCatalogNodeWikidocRelDAO().delete(rel);
                    }
                }
                
                Collection<ResolveCatalogNodeTagRel> catalogTagRels = deleteNode.getCatalogTagRels();
                if(catalogTagRels != null && catalogTagRels.size() > 0)
                {
                    for(ResolveCatalogNodeTagRel rel : catalogTagRels)
                    {
                        HibernateUtil.getDAOFactory().getResolveCatalogNodeTagRelDAO().delete(rel);
                    }
                }
                
                Collection<ResolveCatalogEdge> catalogEdges = deleteNode.getCatalogEdges();
                if(catalogEdges != null && catalogEdges.size() > 0)
                {
                    for(ResolveCatalogEdge rel : catalogEdges)
                    {
                        HibernateUtil.getDAOFactory().getResolveCatalogEdgeDAO().delete(rel);
                    }
                }
                
                //delete the final rec 
                HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().delete(deleteNode);
                
            }//end of for loop

            //to commit the deletes
            HibernateUtil.getCurrentSession().flush();
        }
    }
    
    private void saveTags(ResolveCatalog resolveCatalog, ResolveCatalogNode sourceNode, List<ResolveTagVO> tags) throws Exception
    {
        if(tags != null && tags.size() > 0)
        {
            for(ResolveTagVO tagVO : tags)
            {
                ResolveTag model = null;
                if(StringUtils.isNotEmpty(tagVO.getSys_id()))
                {
                    model = HibernateUtil.getDAOFactory().getResolveTagDAO().findById(tagVO.getSys_id());
                }
                if(model == null && StringUtils.isNotEmpty(tagVO.getId()))
                {
                    model = HibernateUtil.getDAOFactory().getResolveTagDAO().findById(tagVO.getId());
                }
                if(model == null && StringUtils.isNotEmpty(tagVO.getName()))
                {
                    ResolveTagVO tempTagVO = ServiceTag.getTag(null, tagVO.getName(), username);
                    if (tempTagVO == null)
                    {
                        tempTagVO = TagUtil.createTag(tagVO.getName(), username);
                    }
                    
                    if(tempTagVO != null)
                    {
                        model = HibernateUtil.getDAOFactory().getResolveTagDAO().findById(tempTagVO.getSys_id());
                    }
                }
                
                //add the relationship
                if(model != null)
                {
                    ResolveCatalogNodeTagRel tagRel = new ResolveCatalogNodeTagRel();
                    tagRel.setCatalog(resolveCatalog);
                    tagRel.setCatalogNode(sourceNode);
                    tagRel.setTag(model);
                    
                    HibernateUtil.getDAOFactory().getResolveCatalogNodeTagRelDAO().persist(tagRel);
                }
            }
        }
    }
    
    private void saveWikidocument(ResolveCatalog resolveCatalog, ResolveCatalogNode sourceNode,  Catalog node) throws Exception
    {
        String displayType = node.getDisplayType();
        if(StringUtils.isNotBlank(displayType))
        {
            if(displayType.equalsIgnoreCase(CatalogDisplayType.wiki.name()))
            {
                String wiki = node.getWiki();
                if(StringUtils.isNotBlank(wiki))
                {
                    //get the doc object 
                    WikiDocumentVO doc = ServiceWiki.getWikiDoc(null, wiki, "system");
                    if(doc == null)
                    {
                        doc = createWikiPlaceHolder(wiki, username);
                    }
                    
                    //add the relationship
                    persistCatalogWikiRel(resolveCatalog, sourceNode, doc);
                }
                
            }
            else if(displayType.equalsIgnoreCase(CatalogDisplayType.listOfDocuments.name()))
            {
                List<WikiDocumentVO> docs = node.getDocs();
                if(docs != null && docs.size() > 0)
                {
                    for(WikiDocumentVO doc : docs)
                    {
                        persistCatalogWikiRel(resolveCatalog, sourceNode, doc);
                    }
                }
            }
        }
    }

    private void persistCatalogWikiRel(ResolveCatalog resolveCatalog, ResolveCatalogNode sourceNode, WikiDocumentVO docVO) throws Exception
    {
        WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(docVO.getSys_id());
        if(doc == null)
        {
            throw new Exception("Document with sysId " + docVO.getSys_id() + " does not exist");
        }
        
        ResolveCatalogNodeWikidocRel rel = new ResolveCatalogNodeWikidocRel();
        rel.setCatalog(resolveCatalog);
        rel.setCatalogNode(sourceNode);
        rel.setWikidoc(doc);
        
        HibernateUtil.getDAOFactory().getResolveCatalogNodeWikidocRelDAO().persist(rel);
    }

    private static WikiDocumentVO createWikiPlaceHolder(String documentFullName, String username) throws Exception
    {
        WikiDocumentVO doc = null;
        try
        {
            String[] arr = documentFullName.split("\\.");
            if(StringUtils.isEmpty(username))
            {
                username = "system";
            }
            
            //create a stub/place holder for this document
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(ConstantValues.WIKI_NAMESPACE_KEY, arr[0]);
            params.put(ConstantValues.WIKI_DOCNAME_KEY, arr[1]);
            params.put(ConstantValues.WIKI_CONTENT_KEY, "Missing Wiki Content");

            doc = ServiceWiki.createWiki(params, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error in creating doc :" + documentFullName, e);
            throw new Exception("Document " + documentFullName + " was not created successfully.");
        }
        
        return doc;
    }

}
