/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.catalog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeTagRelVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeWikidocRelVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.StringUtils;

/**
 * 
 * represents the ResolveCatalog
 * 
 * @author jeet.marwah
 *
 */
public class Catalog implements Serializable
{
    private static final long serialVersionUID = -5301159629066131460L;
    
    //constants
    public static final String DISPLAY_TYPE_WIKI = "wiki";
    public static final String DISPLAY_TYPE_LIST_OF_DOCUMENTS = "listOfDocuments";
    public static final String DISPLAY_TYPE_URL = "url";
    
    //attributes
    private String id;
    private String name;
    private String type;
    protected String operation;

    private int order;
    private long sys_created_on;
    private long sys_updated_on;
    private String sys_created_by;
    private String sys_updated_by;

    private String roles = "";
    private String editRoles = "";

    private String title;
    private String description;
    private String image;
    private String imageName;
    private String tooltip;
    private String wiki;
    private String link;
    private String form;
    private String displayType; //valid values are - wiki, listOfDocuments, url
    private int maxImageWidth;
    private String path;
    private boolean openInNewTab;
    private int sideWidth;

    private String catalogType;

    private String wikidocSysID;

    private boolean isRoot;
    private boolean isRootRef;
    
    private String internalName;
    private String icon;
    
    // Children are of type Catalog
    private List<Catalog> children = new ArrayList<Catalog>();
    
    // Tags are of type Tag - the picker is using Tag, so changing the ResolveTag to Tag
    private List<ResolveTagVO> tags = new ArrayList<ResolveTagVO>(); 
    
    // Documents/Wiki for the catalog node - for displayType=listOfDocuments
    private List<WikiDocumentVO> docs = new ArrayList<WikiDocumentVO>();

    //list of errors 
    private List<String> errors = new ArrayList<String>();
    
    //wiki namespace - for the root node. This will be applied to all the nodes if the namespace of the doc is not present
    private String namespace = null;
    
    //attributes for manipulating the object to sql and vice versa
    private int lft;
    private int rgt;
    private int depth;
    
    //parent sysId for this catalog
    private String parentSysId;
    
    
    
    public Catalog() {}
    public Catalog(ResolveCatalogNodeVO vo) 
    {
        applyVOToCatalog(vo);
    }
    
    
    public void applyVOToCatalog(ResolveCatalogNodeVO vo)
    {
        this.setId(vo.getSys_id());
        this.setName(vo.getUName());
        this.setType(vo.getUType());
        this.setOperation(vo.getUOperation());
//        this.setOrder(order);
        this.setSys_created_by(vo.getSysCreatedBy());
        this.setSys_created_on(vo.getSysCreatedOn() != null ? vo.getSysCreatedOn().getTime() : new Date().getTime());
        this.setSys_updated_by(vo.getSysUpdatedBy());
        this.setSys_updated_on(vo.getSysUpdatedOn() != null ? vo.getSysUpdatedOn().getTime() : new Date().getTime());

        this.setRoles(vo.getURoles());
        this.setEditRoles(vo.getUEditRoles());

        this.setTitle(vo.getUTitle());
        this.setDescription(vo.getUDescription());
        this.setImage(vo.getUImage());
        this.setImageName(vo.getUImageName());
        this.setTooltip(vo.getUTooltip());
        this.setLink(vo.getULink());
        this.setForm(vo.getUForm());
        this.setDisplayType(vo.getUDisplayType());
        this.setMaxImageWidth(vo.getUMaxImageWidth());
        this.setPath(vo.getUPath());
        this.setOpenInNewTab(vo.getUOpenInNewTab());
        this.setSideWidth(vo.getUSideWidth());

        this.setCatalogType(vo.getUCatalogType());
        this.setWikidocSysID(vo.getUWikidocSysID());

        this.setRoot(vo.getUIsRoot());
        this.setRootRef(vo.getUIsRootRef());

        this.setInternalName(vo.getUInternalName());
        this.setIcon(vo.getUIcon());

        this.setNamespace(vo.getUNamespace());
        
        //tags
        this.setTags(getTagsForNode(vo.getCatalogTagRels()));
        
        //wiki related
        if(StringUtils.isNotEmpty(this.getDisplayType()))
        {
            if(this.getDisplayType().equalsIgnoreCase(CatalogDisplayType.wiki.name()))
            {
                //for wiki only
                this.setWiki(getDocumentNameForNode(vo.getCatalogWikiRels()));
            }
            else if(this.getDisplayType().equalsIgnoreCase(CatalogDisplayType.listOfDocuments.name()))
            {
                //list of wiki
                this.setDocs(getDocumentsForNode(vo.getCatalogWikiRels()));
            }
        }
        
    }
    

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        if (StringUtils.isNotEmpty(id))
        {
            this.id = id.trim();
        }

        if (StringUtils.isEmpty(this.name))
        {
            this.name = id;
        }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public int getOrder()
    {
        return order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }

    public long getSys_created_on()
    {
        return sys_created_on;
    }

    public void setSys_created_on(long sys_created_on)
    {
        this.sys_created_on = sys_created_on;
    }

    public long getSys_updated_on()
    {
        return sys_updated_on;
    }

    public void setSys_updated_on(long sys_updated_on)
    {
        this.sys_updated_on = sys_updated_on;
    }

    public String getSys_created_by()
    {
        return sys_created_by;
    }

    public void setSys_created_by(String sys_created_by)
    {
        this.sys_created_by = sys_created_by;
    }

    public String getSys_updated_by()
    {
        return sys_updated_by;
    }

    public void setSys_updated_by(String sys_updated_by)
    {
        this.sys_updated_by = sys_updated_by;
    }

    public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImageName()
    {
        return imageName;
    }

    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    public String getTooltip()
    {
        return tooltip;
    }

    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }

    public String getWiki()
    {
        return wiki;
    }

    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }

    public String getLink()
    {
        return link;
    }

    public void setLink(String link)
    {
        this.link = link;
    }

    public String getForm()
    {
        return form;
    }

    public void setForm(String form)
    {
        this.form = form;
    }

    public String getDisplayType()
    {
        return displayType;
    }

    public void setDisplayType(String displayType)
    {
        this.displayType = displayType;
    }

    public int getMaxImageWidth()
    {
        return maxImageWidth;
    }

    public void setMaxImageWidth(int maxImageWidth)
    {
        this.maxImageWidth = maxImageWidth;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public boolean isOpenInNewTab()
    {
        return openInNewTab;
    }

    public void setOpenInNewTab(boolean openInNewTab)
    {
        this.openInNewTab = openInNewTab;
    }

    public int getSideWidth()
    {
        return sideWidth;
    }

    public void setSideWidth(int sideWidth)
    {
        this.sideWidth = sideWidth;
    }

    public String getCatalogType()
    {
        return catalogType;
    }

    public void setCatalogType(String catalogType)
    {
        this.catalogType = catalogType;
    }

    public String getWikidocSysID()
    {
        return wikidocSysID;
    }

    public void setWikidocSysID(String wikidocSysID)
    {
        this.wikidocSysID = wikidocSysID;
    }

    public boolean isRoot()
    {
        return isRoot;
    }

    public void setRoot(boolean isRoot)
    {
        this.isRoot = isRoot;
    }
    
    public void setIsRoot(boolean isRoot)
    {
        setRoot(isRoot);
    }

    public boolean isRootRef()
    {
        return isRootRef;
    }

    public void setRootRef(boolean isRootRef)
    {
        this.isRootRef = isRootRef;
    }
    
    public void setIsRootRef(boolean isRootRef)
    {
        setRootRef(isRootRef);
    }

    public String getInternalName()
    {
        return internalName;
    }

    public void setInternalName(String internalName)
    {
        this.internalName = internalName;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public List<Catalog> getChildren()
    {
        return children;
    }

    public void setChildren(List<Catalog> children)
    {
        this.children = children;
    }

    public List<ResolveTagVO> getTags()
    {
        return tags;
    }

    public void setTags(List<ResolveTagVO> tags)
    {
        this.tags = tags;
    }
    
    public List<WikiDocumentVO> getDocs()
    {
        return docs;
    }

    public void setDocs(List<WikiDocumentVO> docs)
    {
        this.docs = docs;
    }

    public List<String> getErrors()
    {
        return errors;
    }

    public void setErrors(List<String> errors)
    {
        this.errors = errors;
    }
    
    public String getParentSysId()
    {
        return parentSysId;
    }

    public void setParentSysId(String parentSysId)
    {
        this.parentSysId = parentSysId;
    }

    @JsonIgnore
    public boolean isValid()
    {
        return this.getErrors().size() == 0;
    }
    
    @JsonIgnore
    public String getError()
    {
        StringBuilder sb = new StringBuilder();
        for (String error : this.errors)
        {
            if (sb.length() > 0) sb.append(", ");
            sb.append(error);
        }
        return sb.toString();
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }
    
    public int getLft()
    {
        return lft;
    }

    public void setLft(int lft)
    {
        this.lft = lft;
    }

    public int getRgt()
    {
        return rgt;
    }

    public void setRgt(int rgt)
    {
        this.rgt = rgt;
    }

    public int getDepth()
    {
        return depth;
    }

    public void setDepth(int depth)
    {
        this.depth = depth;
    }

    //additional apis
    public void applyPath(String oldRoot, String newRoot)
    {
        if (StringUtils.isNotEmpty(this.path))
        {
//            String originalPath = this.path;
            String newPath = this.path.replaceFirst(oldRoot,  newRoot);
            this.path = newPath;
        }

        for (Object child : this.children)
        {
            ((Catalog) child).applyPath(oldRoot, newRoot);
        }
    }
    
    @Override
    public int hashCode()
    {
        int hashcode = 1;
        hashcode = 31 * hashcode + ((id == null) ? 0 : id.hashCode());
        return hashcode;
    }

    public boolean equals(Object obj)
    {
        boolean result = true;

        if (this == obj)
        {
            result = true;
        }

        if (obj == null)
        {
            result = false;
        }

        if (getClass() != obj.getClass())
        {
            result = false;
        }

        Catalog catalogObj = (Catalog) obj;

        if (id == null)
        {
            if (catalogObj.getId() != null)
            {
                result = false;
            }
        }
        else if (!id.equals(catalogObj.getId()))
        {
            result = false;
        }

        return result;
    }
    
    public void purgeIds()
    {
        this.id = "";
        if (!"CatalogReference".equals(this.type) || this.isRoot)
        {
            for (Object child : this.children)
            {
                ((Catalog) child).purgeIds();
            }
        }
        
        //purge the parent Ids
        this.parentSysId = null;
    }
    
    public Set<String> findAllDocumentNames()
    {
        Set<String> result = new HashSet<String>();
        
        findAllWikiNames(result, this);
        
        return result;
    }

    public void validate() throws Exception
    {
        //first update the values
        updatePath();
        updateWikiDocumentNames();
        
        //validate the doc names first
        validateWikiDocumentNames();
        validatePaths();
    }
    
    public Set<String> findAllTagIds()
    {
        Set<String> result = new HashSet<String>();
        
        findAllTagIs(result, this);
        
        return result;
    }
    
    public Catalog findCatalogNodeWithPath(String path)
    {
        Catalog result = null;
        
        if(StringUtils.isNotBlank(path))
        {
            result = findCatalogNodeWithPath(this, path);
        }
        
        return result;
    }
    
    public Set<String> findAllCatalogNodeIds()
    {
        Set<String> result = new HashSet<String>();
        result.addAll(findAllCatalogNodeIds(this));
        return result;
    }
    
    public String toString() 
    {
        return "Id:" + id + ", Name:" + name + ", Path:" + path +", order:" + order;
    }
    
    //private apis
    private void updatePath()
    {
        updatePath(this, "");
    }
    
    private void updateWikiDocumentNames()
    {
        updateWikiDocumentNames(this, this.getNamespace());
    }
    
    private void findAllWikiNames(Set<String> result, Catalog catalog)
    {
        //get docs only where the displayType = 'wiki'
        if (StringUtils.isNotBlank(catalog.getDisplayType()) 
                        /*&& StringUtils.isNotBlank(catalog.getWiki())*/ 
                        && (catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_WIKI) || catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
        {
            String wiki = catalog.getWiki();
            if (StringUtils.isNotBlank(wiki))
            {
                List<String> wikiNames = StringUtils.convertStringToList(wiki, ",");
                result.addAll(wikiNames);
            }
            List<WikiDocumentVO> list = catalog.getDocs();
            if (list != null && list.size() > 0)
            {
                for (WikiDocumentVO wikiDoc : list)
                {
                    String wikiFullName = WikiUtils.getWikidocFullName(wikiDoc.getSys_id());
                    if (StringUtils.isNotBlank(wikiFullName))
                    {
                        result.add(wikiFullName);
                    }
                }
            }
        }
        
        List<Catalog> objects = catalog.getChildren();
        if (objects != null && objects.size() > 0)
        {
            for (Catalog localCatalog : objects)
            {
                if (localCatalog.getChildren() != null && localCatalog.getChildren().size() > 0)
                {
                    //** RECURSIVE
                    findAllWikiNames(result, localCatalog);
                }
                else if (StringUtils.isNotBlank(localCatalog.getDisplayType())
                                && (localCatalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_WIKI) 
                                || localCatalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
                {
                    String wiki = localCatalog.getWiki();
                    if (StringUtils.isNotBlank(wiki))
                    {
                        List<String> wikiNames = StringUtils.convertStringToList(wiki, ",");
                        result.addAll(wikiNames);
                    }
                    List<WikiDocumentVO> list = localCatalog.getDocs();
                    if (list != null && list.size() > 0)
                    {
                        for (WikiDocumentVO wikiDoc : list)
                        {
                            String wikiFullName = WikiUtils.getWikidocFullName(wikiDoc.getSys_id());
                            if (StringUtils.isNotBlank(wikiFullName))
                            {
                                result.add(wikiFullName);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void updatePath(Catalog catalog, String path)
    {
        if(catalog != null)
        {
            String catalogName = catalog.getName();
            if(StringUtils.isEmpty(catalogName))
            {
                throw new RuntimeException("Name is mandatory. Name for one of the node is empty. ");
            }
            
            String newPath = path + "/" + catalogName;
            catalog.setPath(newPath);
            
            //for the child elements
            List<Catalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (Catalog localCatalog : objects)
                {
                  //** RECURSIVE
                    updatePath(localCatalog, newPath);
                }//end of for loop
            }
        }
    }
    
    private void updateWikiDocumentNames(Catalog catalog, String rootNamespace)
    {
        if(catalog != null && StringUtils.isNotBlank(rootNamespace))
        {
            String catalogName = catalog.getName();
            if(StringUtils.isEmpty(catalogName))
            {
                throw new RuntimeException("Name is mandatory. Name for one of the node is empty. ");
            }
            
            //get docs only where the displayType = 'wiki'
            if (StringUtils.isNotBlank(catalog.getDisplayType()) 
                            && StringUtils.isNotBlank(catalog.getWiki()) 
                            && (catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_WIKI) || catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
            {
                String wiki = catalog.getWiki();
                wiki = updateNamespacesToWikiDocumentNames(rootNamespace, wiki);
                catalog.setWiki(wiki);
            }
  
            //for the child elements
            List<Catalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (Catalog localCatalog : objects)
                {
                    //** RECURSIVE
                    updateWikiDocumentNames(localCatalog, rootNamespace);
                }//end of for loop
            }
        }
    }
    
    //wiki - maybe CSV of names or just 1 wiki name
    private String updateNamespacesToWikiDocumentNames(String rootNamespace, String wiki)
    {
        Set<String> wikiFullNames = new HashSet<String>();
        List<String> wikiNames = StringUtils.convertStringToList(wiki, ",");
        for(String wikiName : wikiNames)
        {
            if(StringUtils.isNotBlank(wikiName))
            {
                if( wikiName.indexOf('.') == -1)
                {
                    wikiFullNames.add(rootNamespace + "." + wikiName);    
                }
                else
                {
                    wikiFullNames.add(wikiName);
                }
            }
        }
        
        String fullname = StringUtils.convertCollectionToString(wikiFullNames.iterator(), ",");
        return fullname;
    }
    
    private void validateWikiDocumentNames() throws Exception
    {
        Set<String> docFullNames = findAllDocumentNames();
        Set<String> invalidDocNames = new HashSet<String>();

        if(docFullNames != null && docFullNames.size() > 0)
        {
            for(String docFullName : docFullNames)
            {
                try
                {
                    //validate if the name of document is correct
                    WikiUtils.validateWikiDocName(docFullName);
                }
                catch (Exception e)
                {
                    invalidDocNames.add(docFullName);
                }
            }//end of for loop
        
            if(invalidDocNames.size() > 0)
            {
                throw new Exception("Invalid documents :" + StringUtils.convertCollectionToString(invalidDocNames.iterator(), ","));
            }
        }
    }
    
    private void validatePaths() throws Exception
    {
        //set for comparing the uniqueness
        Set<String> paths = new HashSet<String>();
        paths.add(this.getPath());
        
        //list of children for the catalog
        List<Catalog> children = this.getChildren();
        
        //validate 
        validatePathUniqueness(children, paths);
    }
    
    private void validatePathUniqueness(List<Catalog> catalogs, Set<String> existingPaths) throws Exception
    {
        if (catalogs != null && catalogs.size() > 0)
        {
            for (Catalog localCatalog : catalogs)
            {
                String path = localCatalog.getPath();
                if (StringUtils.isBlank(path))
                {
                    throw new Exception("Path is not available for catalog " + localCatalog.getName());
                }
                else
                {
                    if(existingPaths.contains(path))
                    {
                        throw new Exception("Duplicate paths found for " + path);
                    }
                    else
                    {
                        existingPaths.add(path);
                    }
                }
                
                //validate displayType
                if (StringUtils.isNotEmpty(localCatalog.getDisplayType()))
                {
                    boolean isValidDisplaytype = EnumUtils.isValidEnum(CatalogDisplayType.class, localCatalog.getDisplayType());
                    if (!isValidDisplaytype)
                    {
                        throw new Exception("Invalid DisplayType: " + localCatalog.getDisplayType());
                    }
                }
                
                //validate deeper
                //** RECURSIVE
                validatePathUniqueness(localCatalog.getChildren(), existingPaths);
            }//end of for loop
        }
    }
    
    private void findAllTagIs(Set<String> result, Catalog catalog)
    {
        if (catalog.getTags() != null && catalog.getTags().size() > 0)
        {
            List<ResolveTagVO> objects = catalog.getTags();
            for (ResolveTagVO tag : objects)
            {
                result.add(tag.getSys_id());
            }
        }
        
        List<Catalog> objects = catalog.getChildren();
        if (objects != null && objects.size() > 0)
        {
            for (Catalog localCatalog : objects)
            {
                findAllTagIs(result, localCatalog);
            }
        }
    }
    
    private Catalog findCatalogNodeWithPath(Catalog catalog, String path)
    {
        Catalog result = null;
        
        if(catalog != null && StringUtils.isNotBlank(path))
        {
            List<Catalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (Catalog localCatalog : objects)
                {
                    String localCatalogPath = localCatalog.getPath();
                    if(StringUtils.isNotBlank(localCatalogPath) && localCatalogPath.equalsIgnoreCase(path))
                    {
                        result = localCatalog;
                        break;
                    }
         
                    //if not - check deeper
                    //** RECURSIVE
                    result = findCatalogNodeWithPath(localCatalog, path);
                    if(result != null)
                    {
                        break;
                    }
                }//end of for loop
            }
        }
        
        return result;
    }
    
    private Set<String> findAllCatalogNodeIds(Catalog catalog)
    {
        Set<String> result = new HashSet<String>();
        
        if(catalog != null)
        {
            //add the id
            if (StringUtils.isNotBlank(catalog.getId()))
            {
                result.add(catalog.getId());
            }
            
            //add the child ids
            List<Catalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (Catalog localCatalog : objects)
                {
                  //** RECURSIVE
                    result.addAll(findAllCatalogNodeIds(localCatalog));
                }//end of for loop
            }
        }
        
        return result;
    }
    
    private List<ResolveTagVO> getTagsForNode(Collection<ResolveCatalogNodeTagRelVO> catalogTagRels)
    {
        List<ResolveTagVO> tags = new ArrayList<ResolveTagVO>();
        
        if(catalogTagRels != null && catalogTagRels.size() > 0)
        {
            for(ResolveCatalogNodeTagRelVO relVO : catalogTagRels)
            {
                ResolveTagVO tag = relVO.getTag();
                
//                Tag t = new Tag();
//                t.setId(tag.getSys_id());
//                t.setName(tag.getUName());
                
                //add to the list
                tags.add(tag);
            }//end of for loop
        }
        
        return tags;
        
    }
    
    private List<WikiDocumentVO> getDocumentsForNode(Collection<ResolveCatalogNodeWikidocRelVO> rels)
    {
        List<WikiDocumentVO> docs = new ArrayList<WikiDocumentVO>();
        
        if(rels != null && rels.size() > 0)
        {
            for(ResolveCatalogNodeWikidocRelVO rel : rels)
            {
                docs.add(rel.getWikidoc());
            }
        }
        
        return docs;
    }
    
    private String getDocumentNameForNode(Collection<ResolveCatalogNodeWikidocRelVO> rels)
    {
        String docName = "";
        
        if(rels != null && rels.size() > 0)
        {
            for(ResolveCatalogNodeWikidocRelVO rel : rels)
            {
                docName = rel.getWikidoc().getUFullname();
                break;
            }
        }
        
        return docName;
    }
    
}
