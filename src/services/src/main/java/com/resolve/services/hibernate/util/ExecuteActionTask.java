/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.Map;

import com.resolve.esb.MMsgContent;
import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.vo.ActionTaskResultVO;

public class ExecuteActionTask
{
    /**
     * This is SYNCHRONOUS API for executing a an ACTIONTASK. So it will wait to fetch the results
     * 
     * Returns the output of the execution
     * 
     * @param params
     * @return
     */
    public static String execute(Map<String, Object> params)
    {
        StringBuffer result = new StringBuffer();

        try
        {
            if(validate(params))
            {
                validateOrg(params);
                result.append(submitExecuteRequestForActionTask(params));
            }
        }
        catch(Exception e)
        {
            result.append(e.getMessage());
            Log.log.warn(e.getMessage(), e);
        }
        
        return result.toString();
        
    }
    

    /**
     * This is a ASYNCHRONOUS API
     * 
     * this is generic api that exeutes the Runbook or an Actiontask based on
     * the params. Returns the processId.
     * 
     * @param params
     * @return
     */
    public static String start(Map<String, Object> params)
    {
        StringBuffer result = new StringBuffer();
        String task = (String)params.get(Constants.EXECUTE_ACTIONNAME);

        try
        {
            task = HttpUtil.sanitizeValue(task, "ResolveText" , 1000);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
            result.append("Failed to generate correct result");
            return result.toString();
        }

        try
        {
            if(validate(params))
            {
                validateOrg(params);
                
                submitExecuteRequestForActionTask(params);            
                result.append("ActionTask Name:" + task);
            }
            else
            {
                result.append(" Access Rights Error: Current user does not have execute rights to ActionTask: " + task);
            }
        }
        catch(Exception e)
        {
            result.append(e.getMessage());
            Log.log.warn(e.getMessage(), e);
        }
        
        return result.toString();
    }
    
    /**
     * This methods submits an actiontask result for the execute request.
     * 
     * @param params A {@link Map} containing following keys:
     *      PARAMSTR      		- actiontask result parameter encoded string
     *      USERID        		- userid
     *      COMPLETION    		- actiontask completiion (true|false)
     *      COMMAND       		- actiontask command
     *      RAW           		- raw results
     *      REDIRECT      		- redirect wiki or url
     *      ADDRESS               - target address
     *      REQUEST_TIMESTAMP     - request timestamp in millis
     * 
     * @return null if successful, otherwise returns error message string
     */
    
    public static String result(Map<String, String> params)
    {
        // success is null - otherwise return error msg
        String result = null;
        
        try
        {
			// remove VO object string
			ActionTaskResultVO vo = new ActionTaskResultVO(params.get(Constants.EXECUTE_VO));
			
			// clean up params
			params.remove(Constants.EXECUTE_VO);
			params.remove(Constants.EXECUTE_ACTION);
			params.remove(Constants.EXECUTE_USERID);
			
			// target
			vo.setTarget(MainBase.main.configId.getGuid());
			
			// override parameters			String userid = params.get(Constants.EXECUTE_USERID);
			if (StringUtils.isNotEmpty(userid))
			{
				vo.setUserID(userid);
			}
			String returncode = params.get(Constants.EXECUTE_RETURNCODE);
			if (StringUtils.isEmpty(returncode))
			{
				vo.setReturnCode("0");
			}
			String completion = params.get(Constants.EXECUTE_COMPLETION);
			if (StringUtils.isNotEmpty(completion))
			{
				vo.setCompletion(completion);
			}
			String command = params.get(Constants.EXECUTE_COMMAND);
			if (StringUtils.isNotEmpty(command))
			{
			    vo.setCommand(command);
			}
			String targetAddr = params.get(Constants.EXECUTE_ADDRESS);
			if (StringUtils.isNotEmpty(targetAddr))
			{
			    vo.setAddress(targetAddr);
			}
			String reqTimestampStr = params.get(Constants.EXECUTE_REQUEST_TIMESTAMP);
			if (StringUtils.isNotEmpty(reqTimestampStr))
			{
			    long reqTimestamp = Long.parseLong(reqTimestampStr);
			    long duration = System.currentTimeMillis() - reqTimestamp;
			    
			    vo.setDuration(""+duration);
			}
			String raw = params.get(Constants.EXECUTE_RAW);
			if (StringUtils.isNotEmpty(raw))
			{
			    vo.setRaw(raw);
			}
			else
			{
			    vo.setRaw(StringUtils.remove(StringUtils.mapToString(params), "null"));
			}
			
			// send result msg
            MMsgContent content = new MMsgContent();
            content.setVO(vo.encode());
            
            // TODO - reattach FLOWS, PARAMS
            MainBase.esb.sendMessage(Constants.ESB_NAME_RSSERVER, "MResult.actionResult", content);
			
			result = null;
        }
        catch(Exception e)
        {
            result = StringUtils.getStackTrace(e);
            Log.log.warn(e.getMessage(), e);
        }
        
        return result;
    } // result
    
    private static boolean validate(Map<String, Object> params) throws Exception
    {
        boolean validate = true;
        String action = (String) params.get(Constants.HTTP_REQUEST_ACTION);
        
        if(StringUtils.isEmpty(action))
        {
            throw new Exception("This is not a request to execute an Action Task. The value for " + Constants.HTTP_REQUEST_ACTION + " is <" + action + ">");
        }
        else if (!(action.equalsIgnoreCase("EXECUTETASK") || action.equalsIgnoreCase("TASK")))
        {
            throw new Exception("This is not a request to execute an Action Task. The value for " + Constants.HTTP_REQUEST_ACTION + " is <" + action + ">");            
        }
        
        //validate the user that it has access rights to execute this runbook
        String user = (String) (StringUtils.isNotEmpty((String)params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID)) ? params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID) : params.get(Constants.EXECUTE_USERID));
        String actionTaskFullName = (String) params.get(Constants.EXECUTE_ACTIONNAME);
        String actionTaskId = (String) params.get(Constants.EXECUTE_ACTIONID);
        String namespace = (String) params.get(Constants.EXECUTE_NAMESPACE);
        if(StringUtils.isEmpty(actionTaskFullName) && StringUtils.isNotEmpty(actionTaskId))
        {
            actionTaskFullName = ServiceHibernate.getActionTaskNameFromId(actionTaskId, user);
        }
        if (!actionTaskFullName.contains("#") && StringUtils.isNotEmpty(namespace))
        {
            actionTaskFullName = actionTaskFullName.trim() + "#" + namespace.trim();
        }
        
        validate = ServiceHibernate.hasRightsToActionTask(null, actionTaskFullName, user, RightTypeEnum.execute);//Wiki.getInstance().getActionTaskRightService().checkAccess(EXECUTE_ACTION, actionTaskFullName, user);
        
        return validate;
    }
    
    @SuppressWarnings("unused")
    private static String submitExecuteRequestForActionTask(Map<String, Object> params)
    {
        String result = null;
        
        String userid = (String) (StringUtils.isNotEmpty((String)params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID)) ? params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID) : params.get(Constants.EXECUTE_USERID));
        String problemid        = (String) params.get(Constants.EXECUTE_PROBLEMID);      // required
        String actionname       = (String) params.get(Constants.EXECUTE_ACTIONNAME);     // required ACTIONANME or ACTIONID
        String namespace        = (String) params.get(Constants.EXECUTE_NAMESPACE);      // optional 
        String actionid         = (String) params.get(Constants.EXECUTE_ACTIONID);       // optional
        String executeid        = (String) params.get(Constants.EXECUTE_EXECUTEID);      // optional
        String processid        = (String) params.get(Constants.EXECUTE_PROCESSID);      // optional
        String wiki             = (String) params.get(Constants.EXECUTE_WIKI);           // optional
        String reference        = (String) params.get(Constants.EXECUTE_REFERENCE);      // optional
        String alertid          = (String) params.get(Constants.EXECUTE_ALERTID);        // optional
        String correlationid    = (String) params.get(Constants.EXECUTE_CORRELATIONID);  // optional
        String input            = (String) params.get(Constants.EXECUTE_INPUT);          // optional
        String delay            = (String) params.get(Constants.EXECUTE_DELAY);          // optional
        String processDebug     = (String) params.get(Constants.EXECUTE_PROCESS_DEBUG);  // optional
        
        String action = (String) params.get(Constants.HTTP_REQUEST_ACTION);
        String redirect = (String) params.get(Constants.HTTP_REQUEST_REDIRECT);
        String debug = (String) params.get(Constants.HTTP_REQUEST_DEBUG);
        String token = (String) params.get(Constants.HTTP_REQUEST_TOKEN);

        // parameters
        String guid = (String) params.get(Constants.EXECUTE_GUID); // optional

        // start parameters
        String processTimeoutStr = "10";
        if(params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
            processTimeoutStr = (String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT); // optional
        } else if (params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
            processTimeoutStr = "" + params.get(Constants.EXECUTE_PROCESS_TIMEOUT);
        }
        String processLoop = (String) params.get(Constants.EXECUTE_PROCESS_LOOP); // optional
        String processConcurrentLimit = (String) params.get(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT); // optional

        try
        {
            // get the execute_id for the action task
            result = initExecuteActionTask(params, problemid, userid, reference, alertid, correlationid, null);
            MainBase.esb.sendMessage(Constants.ESB_NAME_RSCONTROL, "MAction.executeTask", params);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        return result;
    }// submitExecuteRequestForActionTask

    
    @SuppressWarnings("unused")
    private static String initExecuteActionTask(Map<String, Object> params, String problemid, String userid, String reference, String alertid, String correlationid, String queryString) throws Exception
    {
        String execute_sid = null;
        String actionTaskName = (String) params.get(Constants.EXECUTE_ACTIONNAME);
        String actionid = (String) params.get(Constants.EXECUTE_ACTIONID); 
        String namespace = (String) params.get(Constants.EXECUTE_NAMESPACE); 

        //get the references if the objects that we need
        String newExecNumber = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_EXEC);
        if(actionTaskName.indexOf("#") == -1 && StringUtils.isNotBlank(namespace))
        {
            actionTaskName = actionTaskName+"#"+namespace;
        }
        boolean isActionTaskExist = ServiceHibernate.isActionTaskExist(actionid, actionTaskName);
        if(!isActionTaskExist)
        {
            throw new Exception("Actiontask name and sys_id are not available. This information is mandatory to execute an Actiontask.");
        }
            
        // create new worksheet and set active
        if (StringUtils.isEmpty(problemid) || problemid.equalsIgnoreCase("NEW")
                || problemid.equalsIgnoreCase("ACTIVE"))
        {
            // create worksheet
            problemid = WorksheetUtils.initWorksheet(problemid, reference, alertid, correlationid, userid, null, false, params);

            // set active worksheet
            WorksheetUtils.setActiveWorksheet(userid, problemid, 
                                              (String)params.get(Constants.EXECUTE_ORG_ID), 
                                              (String)params.get(Constants.EXECUTE_ORG_NAME));

            // set params
            params.put(Constants.EXECUTE_PROBLEMID, problemid);

            // replace NEW in query string
            if (!StringUtils.isEmpty(queryString))
            {
                queryString = queryString.replace("NEW", problemid);
            }
        }
        return execute_sid;
    }//initExecuteActionTask
    
    private static void validateOrg(Map<String, Object> params) throws Exception
    {
        String orgName = (String) params.get(Constants.EXECUTE_ORG_NAME); // optional
        String orgId = (String) params.get(Constants.EXECUTE_ORG_ID); // optional
        
        if ((StringUtils.isNotBlank(orgName) && orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME)) ||
            (StringUtils.isNotBlank(orgId) && orgId.equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            params.remove(Constants.EXECUTE_ORG_NAME);
            params.remove(Constants.EXECUTE_ORG_ID);
            
            orgName = (String) params.get(Constants.EXECUTE_ORG_NAME); // optional
            orgId = (String) params.get(Constants.EXECUTE_ORG_ID); // optional
        }
            
        if (StringUtils.isNotBlank(orgName) && StringUtils.isBlank(orgId))
        {
            // Identify org id based on org name
            
            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
            
            if (orgsVO != null)
            {
                params.put(Constants.EXECUTE_ORG_ID, orgsVO.getSys_id());
                orgId = (String) params.get(Constants.EXECUTE_ORG_ID);
            }
        }
        
        if ((StringUtils.isNotBlank(orgName) && StringUtils.isBlank(orgId)) ||
            StringUtils.isNotBlank(orgId))
        {
            // Validate Org Id either found from name or specified directly
            
            if (StringUtils.isBlank(orgId) || (OrgsUtil.findOrgById(orgId) == null))
            {
                throw new Exception ("Invalid " + (StringUtils.isNotBlank(orgName) ? 
                                     "Org Name " + orgName : 
                                     "Org Id " + orgId) + " specified in parameters passed for execution.");
            }
        }
    }//validateOrg
}//ExecuteActionTask
