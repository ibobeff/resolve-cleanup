/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.menu;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysAppApplicationroles;
import com.resolve.persistence.model.SysAppModule;
import com.resolve.persistence.model.SysAppModuleroles;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.vo.SysAppApplicationVO;
import com.resolve.services.hibernate.vo.SysAppModuleVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MenuDefinitionUtil
{
    public static List<SysAppApplicationVO> getMenuDefinitions(QueryDTO query, String username)
    {
        List<SysAppApplicationVO> result = new ArrayList<SysAppApplicationVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
//                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        SysAppApplication instance = new SysAppApplication();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        SysAppApplication instance = (SysAppApplication) o;
                        
                        //we have to reload the collections also...so doing this
                        result.add(findMenuDefinition(instance.getSys_id(), null, username));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static SysAppApplicationVO findMenuDefinition(String sys_id, String appName, String username)
    {
        SysAppApplication model = null;
        SysAppApplicationVO result = null;
        
        
            try
            {
                if(StringUtils.isNotBlank(sys_id))
                {
                    model = findSysAppApplicationModel(sys_id, null, username);
                }
                else if (StringUtils.isNotBlank(appName))
                {
                    model = findSysAppApplicationModel(null, appName, username);
                }
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        
        
        if(model != null)
        {
            result = model.doGetVO();
        }
        
        return result;
    }
    
    public static void deleteMenuDefinition(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        if (sysIds != null)
        {
            //TODO: prepare the list of sys_ids to delete based on qry
            if (sysIds == null || sysIds.length == 0)
            {
                //prepare the sysIds with the where clause of this obj
                String selectQry = query.getSelectHQL();
                String sql = "select sys_id " + selectQry.substring(selectQry.indexOf("from"));
                System.out.println("MenuDefinitionUtil: #128: Delete qry :" + sql);
            }

            //delete one by one
            for (String sysId : sysIds)
            {
                deleteSysAppApplicationById(sysId, username);
            }
            
            //invalidate the menu cache
            MenuServiceUtil.invalidateMenuCache();
        }
    }
    
    public static SysAppApplicationVO saveSysAppApplication(SysAppApplicationVO vo, String username)  throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        if (vo != null && vo.validate())
        {
            SysAppApplication model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                //update
                model = findSysAppApplicationModel(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("Menu Definition with sysId " + vo.getSys_id() + " does not exist.");
                }
                
                //check if the title is same or not, if not, than validate if the title is unique
                //this will throw exception if there are more than 1 recs with the same title
                SysAppApplication duplicate = findSysAppApplicationModel(null, vo.getTitle(), username);
                if(duplicate != null && !duplicate.getSys_id().equalsIgnoreCase(vo.getSys_id()))
                {
                    throw new Exception("'" + vo.getTitle() + "' already exist. Please try again.");
                }
            }
            else
            {
                //insert
                model = findSysAppApplicationModel(null, vo.getTitle(), username);
                if(model != null)
                {
                    throw new Exception("'" + vo.getTitle() + "' already exist. Please try again.");
                }
                
                int maxSequence = getLatestSequenceNumberForMenuDefinition();
                
                model = new SysAppApplication();
                model.setOrder(new BigDecimal(maxSequence + 1));
                
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveSysAppApplication(model, username);
            
            //add roles and apps to menu set
            updateReferencesForMenuDefinition(model, vo, username);
            
            //get the updated info
            vo = findMenuDefinition(model.getSys_id(), null, username);
            
            //invalidate the menu cache
            MenuServiceUtil.invalidateMenuCache();
        }
        
        return vo;
    }
    
    
    public static void setMenuDefinitionSequence(String[] menuDefinitionSysIds, String username)
    {
        if (menuDefinitionSysIds != null)
        {
            
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
	                int sequence = 1;
	
	                for(String sysId : menuDefinitionSysIds)
	                {
	                    SysAppApplication menuDefinition = HibernateUtil.getDAOFactory().getSysAppApplicationDAO().findById(sysId);
	                    if(menuDefinition != null)
	                    {
	                        menuDefinition.setOrder(new BigDecimal(sequence++));
	                        HibernateUtil.getDAOFactory().getSysAppApplicationDAO().persist(menuDefinition);
	                    }
	                }

                });
                
                //invalidate the cache so that the sequence get into affect
                MenuServiceUtil.invalidateMenuCache();

            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);                
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public static void setCustomSDKMenus(final String resolveHome) throws Exception
    {
        QueryDTO query = new QueryDTO();
        query.setModelName("SysAppApplication");
        query.setWhereClause("title='Gateway Administration'");
        
        List<SysAppApplicationVO> data = ServiceHibernate.getMenuDefinitions(query, "admin");
        if (data != null && data.size() > 0)
        {
            SysAppApplicationVO sysAppApplicationVO = data.get(0);
            if (sysAppApplicationVO != null)
            {
                List<SysAppModuleVO> sysAppModuleVOList = sysAppApplicationVO.getSysAppModules();
                int menuCount = 0;
                if (sysAppModuleVOList != null)
                {
                    menuCount = sysAppModuleVOList.size();
                }
                else
                {
                    sysAppModuleVOList = new ArrayList<SysAppModuleVO>();
                }
                int order = 1;
                //Set<String> customGatewayNames = new HashSet<String>();
                Map<String, Object> recordsMap = ServiceGateway.getCustomSDKFields("", resolveHome);
                Set<String> roles = new HashSet<String>();
                roles.add("admin");
                
                List<String> existingSDKMenuList = new ArrayList<String>();
                
                List<Map<String, String>> mapList = (List<Map<String, String>>)recordsMap.get("records");
                for (Map<String, String> map : mapList)
                {
                    if (map.get("IMPLPREFIXES") != null)
                    {
                        String menuTitle = map.get("MENUTITLE");
                        existingSDKMenuList.add(menuTitle);
                        
                        /*
                         *  Error in rsview Config.xml for SDK developed gateways before it was fixed 
                         *  may have resulted in multiple SysAppModule entries for same menu title in DB.
                         *  Cleanup multiple entries keeping only entry with lowest order # intact. 
                         */
                        
                        SortedSet<SysAppModuleVO> sysAppModuleVOs = getSysAppModules(menuTitle);
                        
                        if (sysAppModuleVOs.isEmpty())
                        {
                            SysAppModuleVO customGWAppMenu = new SysAppModuleVO();
                            customGWAppMenu.setActive(true);
                            customGWAppMenu.setTitle(menuTitle);
                            customGWAppMenu.setOrder(new BigDecimal(menuCount + order++));
                            customGWAppMenu.setRoles(roles);
                            customGWAppMenu.setQuery("RS.gateway.Custom/name=" + map.get("IMPLPREFIXES"));
                            customGWAppMenu.setSysCreatedBy("admin");
                            customGWAppMenu.setSysCreatedOn(new Date());
                            customGWAppMenu.setSysUpdatedBy("admin");
                            customGWAppMenu.setSysUpdatedOn(new Date());
                            
                            customGWAppMenu.setSys_id(null);
                            customGWAppMenu.setId(null);
                            customGWAppMenu.setFilter(null);
                            customGWAppMenu.setAppModuleGroup(null);
                            customGWAppMenu.setHint(null);
                            customGWAppMenu.setViewName(null);
                            customGWAppMenu.setWindowName(null);
                            
                            sysAppModuleVOList.add(customGWAppMenu);
                        }
                        else
                        {
                            if (sysAppModuleVOs.size() > 1)
                            {
                                Iterator<SysAppModuleVO> sysAppModuleVOIter = sysAppModuleVOs.iterator();
                                
                                for (int i = 0; sysAppModuleVOIter.hasNext(); i++)
                                {
                                    SysAppModuleVO dupSysAppModuleVO = sysAppModuleVOIter.next();
                                    
                                    if (i > 0)
                                    {
                                        sysAppModuleVOList.remove(dupSysAppModuleVO);
                                    }          
                                }
                            }
                        }
                    }
                }
                
                if (sysAppModuleVOList != null)
                {
                    Iterator<SysAppModuleVO> iterator = sysAppModuleVOList.iterator();
                    while (iterator.hasNext())
                    {
                        SysAppModuleVO sysAppModuleVO = iterator.next();
                        if (StringUtils.isNotBlank(sysAppModuleVO.getTitle()) && sysAppModuleVO.getQuery().contains("RS.gateway.Custom/name="))
                        {
                            if (!existingSDKMenuList.contains(sysAppModuleVO.getTitle()))
                            {
                                iterator.remove();
                            }
                        }
                    }
                }
                
                sysAppApplicationVO.setSysAppModules(sysAppModuleVOList);
                
                ServiceHibernate.saveSysAppApplication(sysAppApplicationVO, "admin");
            }
        }
    }
    
    //private apis
    @SuppressWarnings("unchecked")
    private static SortedSet<SysAppModuleVO> getSysAppModules (String menuTitle)
    {
        SortedSet<SysAppModuleVO> sysAppModuleVOs = new TreeSet<SysAppModuleVO>();
        List<SysAppModule> sysAppModules = null;
        
        try
        {
            
            String sql = "from SysAppModule sam where LOWER(sam.title) = '" + menuTitle.trim().toLowerCase() + "' order by sam.order";
          HibernateProxy.setCurrentUser("admin");
            sysAppModules = (List<SysAppModule>) HibernateProxy.execute(() -> {
            	return HibernateUtil.createQuery(sql).list();
            });
        }
        
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if (sysAppModules != null && !sysAppModules.isEmpty())
        {
            for (SysAppModule sysAppModule : sysAppModules)
            {
                sysAppModuleVOs.add(sysAppModule.doGetVO());
            }
        }
        
        return sysAppModuleVOs;
    }

    @SuppressWarnings("unchecked")
    private static SysAppApplication findSysAppApplicationModel(String sys_id, String appName, String username)  throws Exception
    {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (SysAppApplication) HibernateProxy.execute(() -> {
        		SysAppApplication result = null;

	            if (StringUtils.isNotEmpty(sys_id))
	            {
	                result = HibernateUtil.getDAOFactory().getSysAppApplicationDAO().findById(sys_id);
	            }
	            else if (StringUtils.isNotEmpty(appName))
	            {
	                String sql = "from SysAppApplication where LOWER(title) = '" + appName.trim().toLowerCase() + "'";
	                List<SysAppApplication> list = HibernateUtil.createQuery(sql).list();
	                if (list != null && list.size() > 0)
	                {
	                    if(list.size() == 1)
	                        result = list.get(0);
	                    else
	                        throw new Exception("There is more than one Menu Definition with Title " + appName);
	                }
	            }
	
	            if (result != null)
	            {
	                if (result.getSysAppApplicationroles() != null) result.getSysAppApplicationroles().size();
	
	                if (result.getSysAppModules() != null)
	                {
	                    for (SysAppModule module : result.getSysAppModules())
	                    {
	                        if (module.getSysAppModuleroles() != null)
	                        {
	                            module.getSysAppModuleroles().size();
	                        }
	                    }
	                }
	            }

	            return result;
        	});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    }
    
    private static void deleteSysAppApplicationById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	
	            SysAppApplication model = HibernateUtil.getDAOFactory().getSysAppApplicationDAO().findById(sysId);
	            if (model != null)
	            {
	                //roles
	                Collection<SysAppApplicationroles> sysAppApplicationroles = model.getSysAppApplicationroles();
	                if(sysAppApplicationroles != null)
	                {
	                    for(SysAppApplicationroles role : sysAppApplicationroles)
	                    {
	                        HibernateUtil.getDAOFactory().getSysAppApplicationrolesDAO().delete(role);
	                    }
	                }
	                
	                //modules
	                Collection<SysAppModule> sysAppModules = model.getSysAppModules();
	                if(sysAppModules != null)
	                {
	                    for(SysAppModule module : sysAppModules)
	                    {
	                        //roles for module
	                        Collection<SysAppModuleroles> sysAppModuleroles = module.getSysAppModuleroles();
	                        if(sysAppModuleroles != null)
	                        {
	                            for(SysAppModuleroles role : sysAppModuleroles)
	                            {
	                                HibernateUtil.getDAOFactory().getSysAppModulerolesDAO().delete(role);
	                            }
	                        }
	                        
	                        HibernateUtil.getDAOFactory().getSysAppModuleDAO().delete(module);
	                    }//end of for loop
	                }               
	                
	                //delete the main rec
	                HibernateUtil.getDAOFactory().getSysAppApplicationDAO().delete(model);
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    private static void updateReferencesForMenuDefinition(SysAppApplication inModel, SysAppApplicationVO vo, String username)
    {
//        List<SysAppApplicationrolesVO> roles = vo.getSysAppApplicationroles();
        Set<String> roles = new HashSet<String>();
        if(vo.getRoles()!=null)
        {
            roles.addAll(vo.getRoles());
        }
        
        List<SysAppModuleVO> appModules = vo.getSysAppModules();

        //set the sequence #
        int seq = 1;
        for(SysAppModuleVO moduleVO : appModules)
        {
            moduleVO.setOrder(new BigDecimal(seq++));
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            SysAppApplication model = HibernateUtil.getDAOFactory().getSysAppApplicationDAO().findById(inModel.getSys_id());
	            if (model != null)
	            {
	                //delete the roles
	                Collection<SysAppApplicationroles> sysAppApplicationroles = model.getSysAppApplicationroles();
	                if(sysAppApplicationroles != null)
	                {
	                    for(SysAppApplicationroles role : sysAppApplicationroles)
	                    {
	                        String existingRole = role.getValue();
	                        boolean roleExist = roles.contains(existingRole);
	                        if(roleExist)
	                        {
	                            //don't touch it...leave it alone..remove it from the UI list as it already exist
	                            roles.remove(role.getValue());
	                            
	                        }
	                        else
	                        {
	                            //delete it as it has been removed from UI
	                            HibernateUtil.getDAOFactory().getSysAppApplicationrolesDAO().delete(role);
	                        }
	                    }
	                }
	                
	
	                //create new ones 
	                for (String role : roles)
	                {
	                    //create a new one
	                    SysAppApplicationroles newRec = new SysAppApplicationroles();
	                    newRec.setValue(role);
	                    newRec.setSequence(0);
	                    newRec.setSysAppApplication(model);
	
	                    HibernateUtil.getDAOFactory().getSysAppApplicationrolesDAO().persist(newRec);
	                }
	                
	                //save the module
	                saveSysAppModule(model, appModules);
	                
	                //save the main rec
	                HibernateUtil.getDAOFactory().getSysAppApplicationDAO().persist(model);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }  
        
    }
    
    //should be within a transaction
    private static void saveSysAppModule(SysAppApplication model, final List<SysAppModuleVO> appModules)
    {
        Collection<SysAppModule> sysAppModules = model.getSysAppModules();
        
        if(sysAppModules != null)
        {
            for(SysAppModule dbModule : sysAppModules)
            {
                SysAppModuleVO uiModuleVO = findModuleUpdatedFromUI(dbModule, appModules);
                
                //this is update
                if(uiModuleVO != null)
                {
                    dbModule.applyVOToModel(uiModuleVO);
                    HibernateUtil.getDAOFactory().getSysAppModuleDAO().persist(dbModule);
                    
                    Set<String> roles = new HashSet<String>();
                    if(uiModuleVO.getRoles()!=null)
                    {
                        roles.addAll(uiModuleVO.getRoles());
                    }
                    
                    
                    //delete the roles and recreate them 
                    Collection<SysAppModuleroles> sysAppModuleroles = dbModule.getSysAppModuleroles();
                    if(sysAppModuleroles != null)
                    {
                        for(SysAppModuleroles role : sysAppModuleroles)
                        {
                            String existingRole = role.getValue();
                            boolean roleExist = roles.contains(existingRole);
                            if(roleExist)
                            {
                                //don't touch it...leave it alone..remove it from the UI list as it already exist
                                roles.remove(role.getValue());
                            }
                            else
                            {
                                //delete it as it has been removed from UI
                                HibernateUtil.getDAOFactory().getSysAppModulerolesDAO().delete(role);
                            }
                        }
                    }
                    
                    //add new roles
                    for (String role : roles)
                    {
                        SysAppModuleroles moduleRole = new SysAppModuleroles();
                        moduleRole.setSequence(0);
                        moduleRole.setValue(role);
                        moduleRole.setSysAppModule(dbModule);

                        HibernateUtil.getDAOFactory().getSysAppModulerolesDAO().persist(moduleRole);
                    }
                    
                }
                else
                {
                    //this is deleted from the UI
                    //roles for module
                    Collection<SysAppModuleroles> sysAppModuleroles = dbModule.getSysAppModuleroles();
                    if(sysAppModuleroles != null)
                    {
                        for(SysAppModuleroles role : sysAppModuleroles)
                        {
                            HibernateUtil.getDAOFactory().getSysAppModulerolesDAO().delete(role);
                        }
                    }
                    
                    HibernateUtil.getDAOFactory().getSysAppModuleDAO().delete(dbModule);
                    
                }
                
            }//end of for loop
        }
        
        //now create the new ones ONLY
        if(appModules != null)
        {
            for (SysAppModuleVO appModule : appModules)
            {
                if (StringUtils.isBlank(appModule.getSys_id()))
                {
                    //                List<SysAppModulerolesVO> roles = appModule.getSysAppModuleroles();
                    Set<String> roles = appModule.getRoles();

                    //prepare the module obj
                    SysAppModule module = new SysAppModule(appModule);
//                    module.setSys_id(null);//TODO: find out why the sysid was empty string
                    module.setSysAppApplication(model);

//                    if (StringUtils.isBlank(module.getSys_id()))
//                    {
                        HibernateUtil.getDAOFactory().getSysAppModuleDAO().persist(module);
//                    }
//                    else
//                    {
//                        //use replicate() api
//                        HibernateUtil.getDAOFactory().getSysAppModuleDAO().replicate(module);
//                    }

                    //add roles to this module
                    if (roles != null && roles.size() > 0)
                    {
                        for (String role : roles)
                        {
                            SysAppModuleroles moduleRole = new SysAppModuleroles();
                            moduleRole.setSequence(0);
                            moduleRole.setValue(role);
                            moduleRole.setSysAppModule(module);

                            HibernateUtil.getDAOFactory().getSysAppModulerolesDAO().persist(moduleRole);
                        }
                    }
                }
                
            }//end of for loop
        }
        
    }
    
    private static SysAppModuleVO findModuleUpdatedFromUI(SysAppModule dbModule, List<SysAppModuleVO> appModules)
    {
        SysAppModuleVO result = null;
        
        if(appModules != null)
        {
            for(SysAppModuleVO vo : appModules)
            {
                if(StringUtils.isNotBlank(vo.getSys_id()) && vo.getSys_id().equals(dbModule.getSys_id()))
                {
                    result = vo;
                    break;
                }
            }
        }     
        
        return result;
    }
    
    private static int getLatestSequenceNumberForMenuDefinition()
    {
        int result = 0;
        String hql = "select MAX(a.order) from SysAppApplication a";
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(hql, new HashMap<String, Object>());
            if(data != null && data.size() > 0)
            {
                result = ((BigDecimal) data.get(0)).intValue();
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
 
    public static void updateActiveForMenuDefinition(String sysId, boolean active)
    {      
        try
        {
          HibernateProxy.setCurrentUser("admin");
            HibernateProxy.execute(() -> {

	            SysAppApplication model = HibernateUtil.getDAOFactory().getSysAppApplicationDAO().findById(sysId);
	            
	            if (model != null)
	            {               
	                model.setActive(active);
	                HibernateUtil.getDAOFactory().getSysAppApplicationDAO().persist(model);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);           
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
}
