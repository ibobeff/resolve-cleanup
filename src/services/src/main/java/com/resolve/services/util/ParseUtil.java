/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;

public class ParseUtil
{
    private static final String JSON_ARRAY_PREFIX = "[";
    private static final String JSON_ARRAY_SUFFIX = "]";
    private static final String PROCEDURE = "procedure";
    private static final String JSON_OBJ_PREFIX = "{";
    private static final String JSON_OBJ_SUFFIX = "}";
    
	private static final Pattern RESULT2_REGEX = Pattern.compile("\\{result2:.*?\\}\\s*(?<items>(\\{.*?\\}\\s*)*)\\{result2\\}") ;
	private static final Pattern RESULT_REGEX = Pattern.compile("\\{result:.*?\\}\\s*(?<items>((.*?)[\\n\\r]*)*)\\{result\\}") ;  
	private static final Pattern RESULT_TASK_REGEX = Pattern.compile("^(.*)$", Pattern.MULTILINE) ;  

	private static final Pattern DETAIL_REGEX = Pattern.compile("\\{detail:.*?\\}\\s*(?<items>(\\{.*?\\}\\s*)*)\\{detail\\}") ;  
	private static final Pattern OLD_DETAIL_REGEX = Pattern.compile("\\{detail:.*\\}(\\s)*([^\\{\\}\n\r]*)(\\s)*\\{detail\\}") ;  
	private static final Pattern TASK_NAME_REGEX = Pattern.compile("task\\=(?<atname>.*?)[\\|\\}]");
	private static final Pattern TASK_NAMESPACE_REGEX = Pattern.compile("namespace\\=(?<nsname>.*?)[\\|\\}]");
	private static final Pattern CONTENT_ITEM_REGEX =Pattern.compile("\\{.*?\\}");
	private static final Pattern WIKI_ACTION_REGEX =Pattern.compile("\\{action\\}(.*?)\\{action\\}", Pattern.DOTALL);	
	
	
    
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Public static methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Replaces the string and returns the new value 
	 *  
	 * @param oldValue
	 * @param newValue
	 * @param originalContent
	 * @return
	 */
	public static String replaceString(String oldValue, String newValue, String originalContent)
	{
		String newContent = originalContent;
		if(originalContent != null)
		{
			newContent = originalContent.replace(oldValue, newValue);
		}
		
		return newContent;
	}
	
	/**
	 * This utility is to return the text between html tags. Note that it will just return at the first level and will not be correct for nested tags
	 * 
	 * eg . &lt;script javascript&gt; function....&lt;/script&gt; ==&gt; will return &quot;function...&quot;
	 * 
	 * @param htmlTagContent
	 * @return
	 */
	public static String getContentBetweenTags(String htmlTagContent)
	{
		String htmlTagContentStr = new String(htmlTagContent);
		htmlTagContentStr = htmlTagContentStr.substring(htmlTagContentStr.indexOf(">")+1, htmlTagContentStr.lastIndexOf("<")).trim();
		return htmlTagContentStr;
	}
	
	/**
	 * Returns the list of names of the documents reference by this wiki document
	 * 
	 * #includeForm("Test.test2"), [Test.test3]
	 * 
	 * @param wikiDocument
	 * @return
	 */
	public static Set<String> getListOfWikiDocumentReferencedBy(WikiDocumentVO wikiDocument)
	{
		Set<String> listOfWikiDocs = new HashSet<String>();
		if(wikiDocument.getUContent() == null || wikiDocument.getUContent().trim().length() == 0)
		{
			return listOfWikiDocs;
		}

		listOfWikiDocs.addAll(getListOfWikiDocumentIncludeFormPattern(wikiDocument));
		listOfWikiDocs.addAll(getListOfWikiDocumentIncludeTopicPattern(wikiDocument));
		listOfWikiDocs.addAll(getListOfWikiDocumentBracketPattern(wikiDocument));
		listOfWikiDocs.addAll(getListOfWikiDocumentFilterPattern(wikiDocument));

		return listOfWikiDocs;
	}

	/**
	 * {action}...{action} {action:manual}...{action}
	 * 
	 * @param content : String representing Wiki content
	 * @return Set of task full names
	 */
	public static Set<String> getListOfActionTaskReferencedBy(String content)
	{
		Set<String> listOfActionTask = new HashSet<String>();

		if(content == null || content.trim().length() == 0)
		{
			return listOfActionTask;
		}
		Matcher mWikiAction = WIKI_ACTION_REGEX.matcher(content);
		while (mWikiAction.find())
		{
			String taskName = mWikiAction.group(1);
			int idx = taskName.indexOf(">");
			if (idx!=-1)
			{
				taskName = taskName.substring(idx+1);
			}
			
			idx = taskName.indexOf("#");
			if (idx==-1)
			{
				taskName+="#resolve";  // add default namespace for action task if it's missing
			}
			
			if (StringUtils.isNotEmpty(taskName))
			{
	            listOfActionTask.add(taskName);
			}
		}

		return listOfActionTask;
	}

	/**
	 * This method takes the XML content of Main, Exception(Abort) or Final and provides a list of Action Tasks referenced in it
	 * 
	 * @param xml
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public static Set<String> getListOfActionTasksReferencedByXML(String xml)
	{
		Set<String> listOfActionTasks = new HashSet<String>();
		if (xml == null || xml.trim().length() == 0)
		{
			return listOfActionTasks;
		}

		try
		{
		    SAXReader reader = new SAXReader();
	        reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
	        reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
	        reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
	        Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
			//Document document = DocumentHelper.parseText(xml);
			List<Node> listActionTaskNode = document.selectNodes("/mxGraphModel/root/Task");
			if (listActionTaskNode.size() > 0)
			{
//				String taskName = "";
//				String taskNamespace = "";
				for (Iterator<Node> i = listActionTaskNode.iterator(); i.hasNext();)
				{
					//Reference - ProcessRequest.java
					Node xmlNode = i.next();
					//eg. description="Update CR History#resolve.cr?comment%3DStarted%20Developing
					String taskdef = xmlNode.valueOf("@description");
//					String href = xmlNode.valueOf("@href");
					if(taskdef.indexOf('?') > -1)
					{
					    taskdef = taskdef.substring(0, taskdef.indexOf('?'));
					}
					                
					listOfActionTasks.add(taskdef);
				}
			}
		}
		catch (Exception e)
		{
			//don;t do anything
//			e.printStackTrace();
		}

		return listOfActionTasks;
	}
	
	@SuppressWarnings("unchecked")
	public static Set<String> getPropertiesFromGraphModel (String xml) throws SAXException
	{
	    Set<String> propertyNameList = new HashSet<String>();
	    if (StringUtils.isNotBlank(xml))
	    {
	        try
            {
	            SAXReader reader = new SAXReader();
	            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
	            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
	            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
	            Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
                //Document document = DocumentHelper.parseText(xml);
                List<Node> listActionTaskNode = document.selectNodes("/mxGraphModel/root/Task/@description");
                if (listActionTaskNode.size() > 0)
                {
                    for (Iterator<Node> iter = listActionTaskNode.iterator(); iter.hasNext(); )
                    {
                        Attribute attribute = (Attribute) iter.next();
                        String description = attribute.getValue();
                        description = java.net.URLDecoder.decode(description, "UTF-8");
                        if (description.indexOf("PROPERTY") >= 0)
                        {
                            description = description.substring(description.indexOf("PROPERTY"));
                            String [] properties = description.split("\\$PROPERTY");
                            for (String prop : properties)
                            {
                                if (StringUtils.isNotBlank(prop))
                                {
                                    if (prop.indexOf('{') >=0 && prop.indexOf('}') >=1) {
                                        {
                                            String property = prop.substring(prop.indexOf('{')+1 , prop.indexOf('}'));
                                            propertyNameList.add(property);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (DocumentException e)
            {
                Log.log.error(e.getMessage(), e);
            }
            catch (UnsupportedEncodingException e)
            {
                Log.log.error(e.getMessage(), e);
            }
	    }
	    return propertyNameList;
	}

	/**
	 * This method takes the XML content of Main, Exception(Abort) or Final and provides a list of Wiki Docs referenced in it
	 * 
	 * @param xml
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public static Set<String> getListOfWikiDocumentReferencedByXML(String xml)
	{
		Set<String> listOfWikiDocs = new HashSet<String>();
		if (xml == null || xml.trim().length() == 0)
		{
			return listOfWikiDocs;
		}

		try
		{
		    SAXReader reader = new SAXReader();
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
			//Document document = DocumentHelper.parseText(xml);
			List<Node> listWikiDocNode = document.selectNodes("/mxGraphModel/root/Subprocess");
			if (listWikiDocNode.size() > 0)
			{
				for (Iterator<Node> i = listWikiDocNode.iterator(); i.hasNext();)
				{
					//Reference - ProcessRequest.java
					Node xmlNode = i.next();
					String wiki = xmlNode.valueOf("@description");
					if (StringUtils.isBlank(wiki))
					{
						wiki = xmlNode.valueOf("@label");
					}
					// remove carriage returns
                    wiki = wiki.replaceAll("\\r\\n", " ");
                    wiki = wiki.replaceAll("\\n\\r", " ");
                    wiki = wiki.replaceAll("\\n", " ");
                    
					listOfWikiDocs.add(wiki);
				}
			}
		}
		catch (Exception e)
		{
		    Log.log.error("Failed to pull list of Documents Referenced by the Main or Exception (Abort) Model", e);
		}

		return listOfWikiDocs;
	}//getListOfWikiDocumentReferencedByXML
    
    /**
     * This method takes the XML from a Decision Tree and provides a list of Wiki Docs referenced in it
     * 
     * @param xml
     * @return
     */
	@SuppressWarnings("unchecked")
    public static Set<String> getListOfWikiDocumentReferencedByDT(String xml)
	{
        Set<String> listOfWikiDocs = new HashSet<String>();
        if (xml == null || xml.trim().length() == 0)
        {
            return listOfWikiDocs;
        }

        try
        {
            SAXReader reader = new SAXReader();
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
            //Document document = DocumentHelper.parseText(xml);
            List<Node> listWikiDocNode = document.selectNodes("/mxGraphModel/root/Document");
            if (listWikiDocNode.size() > 0)
            {
                for (Iterator<Node> i = listWikiDocNode.iterator(); i.hasNext();)
                {
                    //Reference - ProcessRequest.java
                    Node xmlNode = i.next();
                    
                    String wiki = xmlNode.valueOf("@description");
                    if (StringUtils.isEmpty(wiki))
                    {
                        wiki = xmlNode.valueOf("@label");
                    }
                    // remove carriage returns
                    wiki = wiki.replaceAll("\\r\\n", " ");
                    wiki = wiki.replaceAll("\\n\\r", " ");
                    wiki = wiki.replaceAll("\\n", " ");
                    
                    listOfWikiDocs.add(wiki);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to pull list of Documents Referenced by the Decision Tree XML", e);
        }

        return listOfWikiDocs;
    } //getListOfWikiDocumentReferencedByDT
	
	/**
	 * Prepares a set of Properties String by parsing the text
	 * 
	 * - $PROPERTY{PROP_USERNAME}
	 * 
	 * 
	 * @param content
	 * @return
	 */
	public static Set<String> getListOfProperties(String content)
	{
		Set<String> listOfProperties = new HashSet<String>();
		if(StringUtils.isBlank(content))
		{
			return listOfProperties;
		}

		listOfProperties.addAll(parse$Properties(content));
		
		return listOfProperties;
	}
	
	/**
	 * This is to preprocess the content before saving the doc 
	 * 
	 * for eg. [External Link No Namespace>Move Page] ==> [External Link No Namespace>RSQA.Move Page] where RSQA is the doc namespace
	 * 		   [WebHome] ==> [RSQA.WebHome]
	 * 
	 * @param wikiDoc
	 */
	public static void processWikiDocumentContent(WikiDocumentVO wikiDoc)
	{
	    // comment out for now until added support for javascript, groovy line-by-line processing
	    /*
		String content = new String(wikiDoc.getUContent());
	
		String bracketPattern = "(?<!\\\\)\\[(.*?(?<!\\\\))\\]";

		Pattern p = Pattern.compile(bracketPattern);
		Matcher matcher = p.matcher(content);

		try
		{
			while (matcher.find())
			{
				String str = matcher.group();
				String fileName = str.substring(str.indexOf("[") + 1, str.indexOf("]"));
	
				if(isValidFileName(fileName) && fileName.indexOf('.') == -1)
				{
					StringBuffer newFileName = new StringBuffer("[");
					
					//remove the >
					if(fileName.indexOf(">") > 0)
					{
						newFileName.append(fileName.substring(0, fileName.indexOf(">")+1));
					}
					
					if(fileName.indexOf("#") > 0)
					{
						newFileName.append(fileName.substring(0, fileName.indexOf("#")+1));
					}
					
					//append new file name
					newFileName.append(getDocumentFullName(wikiDoc.getUNamespace(), fileName));
					newFileName.append("]");
					
					//replace the content
					content = content.replace(str, newFileName.toString());
					
				}
		
			}
			
			//update the wikidoc
			wikiDoc.setUContent(content);

		}
		catch(Exception e)
		{
			//just return without any change.
			Log.log.error(e.getMessage(), e);
			
		}
		*/
	}//processWikiDocumentContent
	
	/**
	 * Returns the full doc name 'Test.test2' from the url /wiki/service...Test/test2'
	 * 
	 * @param url
	 * @return
	 */
	public static String getDocumentNameFrom(String url)
	{
		String fullDocName = "";
		
		try
		{
			String requestURI = url;///resolve/service/wiki/view/Test/test1?UIControlId=wYOGrjiXZslXHWX
			if(requestURI.indexOf("?") > 0)
			{
				requestURI = requestURI.substring(0, requestURI.indexOf("?"));///resolve/service/wiki/view/Test/test1
			}
			String filename = requestURI.substring(requestURI.lastIndexOf("/") + 1);
			String temp = requestURI.substring(0, requestURI.indexOf(filename) - 1);
			String namespace = temp.substring(temp.lastIndexOf("/") + 1);	
			fullDocName = namespace + "." + filename;
		}
		catch (Exception e)
		{
			//if there is an error,it should not be saved anyway. So set it to system so that it does not update the list.
			fullDocName = null;
		}
		return fullDocName;
	}//getDocumentNameFrom
	
	
	/**
	 * get the namespace from the URL
	 * 
	 * @param url
	 * @return
	 */
	public static  String getNamespaceForURL(String url)
	{
		String namespace = null;
		
		try
		{
			String fullName = getDocumentNameFrom(url);
			String[] array = fullName.split("\\.");
			namespace = array[0];
		}
		catch (Exception e)
		{
			//if there is an error,it should not be saved anyway. So set it to system so that it does not update the list.
			namespace = HibernateConstants.NAMESPACE_SYSTEM;
		}

		return namespace;
	}//getNamespaceForURL
	

	/**
	 * 
	 * eg.  1) Complete url@target>/resolve/service/wiki/view/Test/test1
	 * 		2) Test.test1
	 * 		3) Name of the doc@target>Test.test1
	 * 		4) RSCONTROL#MAction.executeProcess
	 * 		5) $result.getSummary()>${result.web}.$result.name
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getDocumentFullName(String defaultNamespace, String fileName)
	{
		//filter out these names as they are not document names
//		if(fileName.indexOf('.') == -1 
//				|| 
		if(fileName.contains("http:")
				|| fileName.contains("https:") 
				|| fileName.contains("ssh2:")
				|| fileName.contains("Encryption:") 
				|| fileName.contains("ftp:") 
				|| fileName.contains("file") 
				|| fileName.contains("ldap:")
				|| fileName.contains(".jpg")
				|| fileName.contains(".bmp")
				|| fileName.contains(".mpg")
				|| fileName.contains(".pdf")
				|| fileName.contains("telnet:")
				|| fileName.contains("/dd/yy")
				|| fileName.contains("mailto:")
				|| fileName.lastIndexOf("/n") > 0
				|| fileName.lastIndexOf('$') > 0
				|| fileName.lastIndexOf('{') > 0
				|| fileName.lastIndexOf('|') > 0
				|| fileName.lastIndexOf('!') > 0
				|| fileName.lastIndexOf(':') > 0
				|| fileName.lastIndexOf('=') > 0
				|| fileName.lastIndexOf('}') > 0
		)
		{
			return null;
		}
		
		
		String docFullName = new String(fileName);
		
		//remove the >
		if(docFullName.indexOf(">") > 0)
		{
			docFullName = docFullName.substring(docFullName.indexOf(">")+1);
		}
		
		if(docFullName.indexOf("#") > 0)
		{
			docFullName = docFullName.substring(docFullName.indexOf("#")+1);
		}
		
		///resolve/service/wiki/view/Test/test1  OR  Test.test1
		if(docFullName.lastIndexOf('/') > 0)
		{
			if (docFullName.contains("resolve/service/wiki/view"))
			{
				String filename = docFullName.substring(docFullName.lastIndexOf("/") + 1);
				String temp = docFullName.substring(0, docFullName.indexOf(filename) - 1);
				String namespace = temp.substring(temp.lastIndexOf("/") + 1);	
				docFullName = namespace + "." + filename;
			}
		}
		
		//if there is no namespace value, use the NS of the current document
		if(docFullName.indexOf('.') == -1)
		{
			docFullName = defaultNamespace + "." + docFullName;
		}
		
		return docFullName;
	}
	
	/**
	 * This returns a list of string with <span> tags 
	 * 
	 * @param content
	 * @return
	 */
	public static List<String> getHtmlSpanTag(String content)
	{
		String groovyText = new String(content);
		List<String> listOfSpanTags = new ArrayList<String>();
		
		String pattern = "<span[^>]*\"[^>]*>(.*?)</span>";
		
		Pattern p = Pattern.compile(pattern, Pattern.DOTALL);
		Matcher matcher = p.matcher(groovyText);

		while (matcher.find())
		{
			String str = matcher.group();
			listOfSpanTags.add(str);
		}
		
		return listOfSpanTags;
	}//getHtmlSpanTag

	/**
	 * Returns the list of <p class="groovy">sdfsd</p> tags
	 * 
	 * @param content
	 * @return
	 */
	public static List<String> getHtml_P_Tag(String content)
	{
		String contentText = new String(content);
		List<String> listOfSpanTags = new ArrayList<String>();
		
		String pattern = "(<p[^>]*>(.*?)</p>)";//"<*" + search + "*[^>]*\"[^>]*>(.*?)</span>";
		
		Pattern p = Pattern.compile(pattern, Pattern.DOTALL);
		Matcher matcher = p.matcher(contentText);

		while (matcher.find())
		{
			String str = matcher.group();
			listOfSpanTags.add(str);
		}
		
		return listOfSpanTags;
	}//getHtml_P_Tag
	
	/**
	 * RS_BR_RS<p>#if ($request.getParameter("processID")) </p>RS_BR_RS
RS_BR_RS<p>#set ($page = "Process." + $request.getParameter("processID"))</p>RS_BR_RS
RS_BR_RS<p>#includeForm($page)</p>RS_BR_RS

	 * @param content
	 * @return
	 */
	public static List<String> getHtml_br_Tag(String content)
    {
        String contentText = new String(content);
        List<String> listOfSpanTags = new ArrayList<String>();
        
        String pattern = "(<br[^>]*>(.*?)<br>)";//"<*" + search + "*[^>]*\"[^>]*>(.*?)</span>";
        
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(contentText);

        while (matcher.find())
        {
            String str = matcher.group();
            listOfSpanTags.add(str);
        }
        
        return listOfSpanTags;
    }//getHtml_br_Tag
	
	/**
	 * returns a list of all the tags
	 * eg. {action}sdfsd{action}, {test}asdfasdfa{test}
	 * 
	 * @param content
	 * @return
	 */
	public static List<String> getMacroTags(String content)
	{
		String contentText = new String(content);
		List<String> listOfSpanTags = new ArrayList<String>();
		
		String pattern = "\\{([^:}]+)(?::([^\\}]*))?\\}(.*?)\\{\\1\\}";
		
		Pattern p = Pattern.compile(pattern, Pattern.DOTALL);
		Matcher matcher = p.matcher(contentText);
		
		while (matcher.find())
		{
			String str = matcher.group();
			listOfSpanTags.add(str);
		}
		
		return listOfSpanTags;
	}
	
	
	/**
	 * this utility method  removes all the HTML tags from the content
	 * 
	 * @param htmlString
	 * @return
	 */
	public static String removeHTML(String htmlString)
	{
		String noHTMLString = new String(htmlString);
		
		//convert all the <br> to \n
		noHTMLString = noHTMLString.replaceAll("<br>", "");
		noHTMLString = noHTMLString.replaceAll("</p>", "\n");
		
		// Remove HTML tag from java String    
		noHTMLString = noHTMLString.replaceAll("\\<.*?\\>", "");

		//convert the symbols as it can be code 
		noHTMLString = StringEscapeUtils.unescapeHtml(noHTMLString);
//		noHTMLString = noHTMLString.replaceAll("&lt;", "<");
//		noHTMLString = noHTMLString.replaceAll("&gt;", ">");
//		noHTMLString = noHTMLString.replaceAll("&amp;", "&");
//		noHTMLString = noHTMLString.replaceAll("&nbsp;", " ");
		
		// Remove Carriage return from java String
//		noHTMLString = noHTMLString.replaceAll("\r", "<br/>");

		// Remove New line from java string and replace html break
//		noHTMLString = noHTMLString.replaceAll("\n", " ");
//		noHTMLString = noHTMLString.replaceAll("\'", "&#39;");
//		noHTMLString = noHTMLString.replaceAll("\"", "&quot;");
		return noHTMLString;
	}
	
	/**
	 * remove anything that has {}, [], #, <%..%>
	 * 
	 * @param content
	 * @return
	 */
	public static String removeWikiTags(String content)
	{
		String contentStr = new String(content);
		contentStr = contentStr.replaceAll("\\{.*?\\}", "");
		contentStr = contentStr.replaceAll("\\[.*?\\]", "");
		contentStr = contentStr.replaceAll("#", "");
		contentStr = contentStr.replaceAll("\\<%(.*?)%>", "");
		
		return contentStr;
	}
	
	public static String replaceHiddenCharacters(String content)
	{
		String contentStr = new String(content);
		
		contentStr = contentStr.replaceAll("\n", "<br>");
		
		return contentStr;
	}
	
	
	/**
	 * returns the content in sections
	 * 
	 * @param content
	 * @return
	 */
	public static List<String> getListOfSections(String content)
    {
        List<String> list = new ArrayList<String>();

        String includeFormPattern = "\\{section(.*?)\\{section\\}";
//        String elementSection = "{section}";

        Pattern p = Pattern.compile(includeFormPattern, Pattern.DOTALL);
        Matcher matcher = p.matcher(content);

        while (matcher.find())
        {
            String str = matcher.group();
            list.add(str);
        }
        
        return list;
    }//getListOfSections

//	private static List<String> getGroovyTags(String content)
//	{
//		String contentText = new String(content);
//		List<String> listOfGroovy = new ArrayList<String>();
//		
//		String pattern = "\\<%(.*)%\\>";
//		
//		Pattern p = Pattern.compile(pattern, Pattern.DOTALL);
//		Matcher matcher = p.matcher(contentText);
//		
//		while (matcher.find())
//		{
//			String str = matcher.group();
//			System.out.println(str);
//			listOfGroovy.add(str);
//		}
//		
//		return listOfGroovy;
//	}
	
    public static String preProcessCodeMacro(String content)
    {
        String result = content;

        String includeFormPattern = "\\{code(.*?)\\{code\\}";

        Pattern p = Pattern.compile(includeFormPattern, Pattern.DOTALL);
        Matcher matcher = p.matcher(content);

        while (matcher.find())
        {
            String codeMacrostr = matcher.group();// {code} something here {code}
            if(codeMacrostr.indexOf("{pre}") == -1)
            {
                String modifiedCodeMacroStr = codeMacrostr.replace("{code}", "");
                modifiedCodeMacroStr = "{code}{pre}" + modifiedCodeMacroStr + "{pre}{code}";

                result = result.replace(codeMacrostr, modifiedCodeMacroStr);
            }
        }

        return result;
    }// getListOfSections
    
    public static Set<String> parseAssessorNamesFromScript(String script)
    {
        Set<String> result = new HashSet<String>();

        if (StringUtils.isNotBlank(script))
        {
            String pattern = Constants.GROOVY_BINDING_ASSESS + ".execute\\((?<!\\\\)([\"'/])(.+?)(?<!\\\\)\\1";
            result.addAll(parseNamesFromScript(script, pattern));
        }
        return result;
    }
    
    public static Set<String> parseParserNamesFromScript(String script)
    {
        Set<String> result = new HashSet<String>();

        if (StringUtils.isNotBlank(script))
        {
            String pattern = Constants.GROOVY_BINDING_PARSER + ".execute\\((?<!\\\\)([\"'/])(.+?)(?<!\\\\)\\1";
            result.addAll(parseNamesFromScript(script, pattern));
        }
        return result;
    }
    
    
    public static Set<String> parsePreprocessorNamesFromScript(String script)
    {
        Set<String> result = new HashSet<String>();

        if (StringUtils.isNotBlank(script))
        {
            String pattern = Constants.GROOVY_BINDING_PREPROCESS + ".execute\\((?<!\\\\)([\"'/])(.+?)(?<!\\\\)\\1";
            result.addAll(parseNamesFromScript(script, pattern));
        }
        return result;
    }
	
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Private methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * for [Test.WebHome]
	 * It will ignore the commented file names like '\\[Test.WebHome]' or '\\[Test.WebHome\\]'
	 * 
	 * @param wikiDocument
	 * @return
	 */
	private static Set<String> getListOfWikiDocumentBracketPattern(WikiDocumentVO wikiDocument)
	{
		Set<String> listOfWikiDocs = new HashSet<String>();
		String content = wikiDocument.getUContent();
		content = filterMarkupContent(content);
		
		if(content == null || content.trim().length() == 0)
		{
			return listOfWikiDocs;
		}
		String bracketPattern = "(?<!\\\\)\\[(.*?(?<!\\\\))\\]";

		Pattern p = Pattern.compile(bracketPattern);
		Matcher matcher = p.matcher(content);

		while (matcher.find())
		{
			String str = matcher.group();
			String fileName = str.substring(str.indexOf("[") + 1, str.indexOf("]"));
			fileName = getDocumentFullName(wikiDocument.getUNamespace(), fileName);
			if(fileName != null)
			{
			    if (!fileName.equalsIgnoreCase(wikiDocument.getUFullname()))
				listOfWikiDocs.add(fileName);
			}

		}

		return listOfWikiDocs;
	}

	/**
	 * for #includeTopic("Test.WebHome")
	 * 
	 * @param wikiDocument
	 * @return
	 */
	private static Set<String> getListOfWikiDocumentIncludeTopicPattern(WikiDocumentVO wikiDocument)
	{
		Set<String> listOfWikiDocs = new HashSet<String>();
		String content = wikiDocument.getUContent();
		//		Pattern p1 = Pattern.compile("#includeForm");
		String includeFormPattern = "#includeTopic(.*?)\\)";//"#includeTopic";
		
		Pattern p = Pattern.compile(includeFormPattern);
		Matcher matcher = p.matcher(content);

		while (matcher.find())
		{
			String str = matcher.group();
			String[] arr = str.split("\"");
			String fullName = arr[1];
			//if there is no namespace, use the current namespace by default
			if(fullName.indexOf(".") == -1)
			{
				fullName = wikiDocument.getUNamespace() + "." + fullName;
			}
			if (!fullName.equalsIgnoreCase(wikiDocument.getUFullname()))
			listOfWikiDocs.add(fullName);
		}
		
		return listOfWikiDocs;
	} //getListOfWikiDocumentIncludeTopicPattern

	/**
	 * for #includeForm("Test.WebHome")
	 * 
	 * @param wikiDocument
	 * @return
	 */
	private static Set<String> getListOfWikiDocumentIncludeFormPattern(WikiDocumentVO wikiDocument)
	{
		Set<String> listOfWikiDocs = new HashSet<String>();
		String content = wikiDocument.getUContent();
		String includeFormPattern = "#includeForm\\(\\\"((?!\\$\\{).*?)\\\"\\)";

		Pattern p = Pattern.compile(includeFormPattern);
		Matcher matcher = p.matcher(content);

		while (matcher.find())
		{
			String str = matcher.group(1);
			
			try
			{
    			String fullName = str;
    			//if there is no namespace, use the current namespace by default
    			if(fullName.indexOf(".") == -1)
    			{
    				fullName = wikiDocument.getUNamespace() + "." + fullName;
    			}
    			
    			int idx = fullName.indexOf("#");
    			if (idx!=-1)
    			{
    				fullName = fullName.substring(0, idx);   // resmove trailing "#" like  #includeForm("Demo.CSS#stylesheet") in wiki 
    			}
    			
    			if (!fullName.equalsIgnoreCase(wikiDocument.getUFullname()))
    			listOfWikiDocs.add(fullName);
			}
			catch(Throwable t)
			{
			    //do nothing
			}
		}
		return listOfWikiDocs;
	} //getListOfWikiDocumentIncludeFormPattern
	
	/**
     * for decision tree section references
     * 
     * @param wikiDocument
     * @return
     */
    private static Set<String> getListOfWikiDocumentDecisionTreePattern(WikiDocumentVO wikiDocument)
    {
        Set<String> listOfWikiDocs = new HashSet<String>();
        String content = wikiDocument.getUContent();
        String sectionTitlePattern = "\\{section:.*?\\|title=dt_(.*?)_\\d+\\|";

        Pattern p = Pattern.compile(sectionTitlePattern);
        Matcher matcher = p.matcher(content);

        while (matcher.find())
        {
            String str = matcher.group(1);
            
            String fullName = str;
            //if there is no namespace, use the current namespace by default
            if(fullName.indexOf(".") == -1)
            {
                fullName = wikiDocument.getUNamespace() + "." + fullName;
            }
            if (!fullName.equalsIgnoreCase(wikiDocument.getUFullname()))
            listOfWikiDocs.add(fullName);
        }
        
        String decisionRefencePattern = "(?ms)\\{decision:.*?\\}(.*)\\{decision\\}";
//        String decisionDocPattern = "(?m)^.*?=(?:\\{pre\\})?(.+?)(?>\\?|$)";
        String decisionDocPattern = "(?m)^.*?=(?:\\{pre\\})?(" + HibernateConstants.REGEX_WIKI_DOCUMENT_NAME_AND_FULLNAME + ")(?>\\?|$)";

        p = Pattern.compile(decisionRefencePattern);
        matcher = p.matcher(content);
        Pattern p2 = Pattern.compile(decisionDocPattern);

        while (matcher.find())
        {
            String str = matcher.group(1);
            Matcher matcher2 = p2.matcher(str);
            
            while (matcher2.find())
            {
                String fullName = matcher2.group(1);
                //if there is no namespace, use the current namespace by default
                if(fullName.indexOf(".") == -1)
                {
                    fullName = wikiDocument.getUNamespace() + "." + fullName;
                }
                if (!fullName.equalsIgnoreCase(wikiDocument.getUFullname()))
                listOfWikiDocs.add(fullName);
            }
        }
        return listOfWikiDocs;
    }//getListOfWikiDocumentDecisionTreePattern
    

    private static Set<String> getListOfWikiDocumentFilterPattern(WikiDocumentVO wiki) {
        
        Set<String> listOfWikiDocs = new HashSet<String>();
        String content = wiki.getUContent();

        String specificWiki = "";
        
        List<String> resultMacros = getAllResultMacros(content);
        
        for(String resultMacro: resultMacros) {
            specificWiki = getFieldValue(resultMacro, "showWiki");
            Log.log.debug("specificWiki = " + specificWiki);
            if(StringUtils.isNotEmpty(specificWiki))
                if (!specificWiki.equalsIgnoreCase(wiki.getUFullname()))
                listOfWikiDocs.add(specificWiki);
        }
        
        return listOfWikiDocs;
    } // getListOfWikiDocumentFilterPattern
    

    private static List<String> getAllResultMacros(String content) {

        List<String> resultMacros = null;
        
        String[] components = content.split("}");
        if(components != null && components.length != 0) {
            resultMacros = new ArrayList<String>();
            for(String component:components) {
                if(StringUtils.isNotEmpty(component) && component.contains("{result2:")) {
                    int index = component.indexOf("{");
                    resultMacros.add(component.substring(index, component.length()));
                }
            }
        }
        
        return resultMacros;
    }
    
    private static String getFieldValue(String component, String key) {
        
        if(StringUtils.isBlank(component) || StringUtils.isBlank(key))
            return null;
        
        int index = -1;
        String value = null;
        
        if(component.contains(key)) {
            String[] fields = component.split("\\|");
            if(fields != null && fields.length != 0) {
                for(String field: fields) {
                    if(StringUtils.isNotEmpty(field) && field.contains(key)) {
                        index = field.indexOf("=");
                        if(index != -1) {
                            value = field.substring(index+1);
                            break;
                        }
                    }
                }
            }
        }

        return value;
    }
    
	private static Set<String> parse$Properties(String content)
	{
		Set<String> listOfProperties = new HashSet<String>();

		String includeFormPattern = "PROPERTY\\{(.*?)\\}";

		Pattern p = Pattern.compile(includeFormPattern);
		Matcher matcher = p.matcher(content);

		while (matcher.find())
		{
			String str = matcher.group();
			String actiontaskName = str.substring(str.indexOf("{") + 1, str.length() - 1);
			listOfProperties.add(actiontaskName);
		}

		return listOfProperties;
	}
	
	/**
	 * filter out the lines that has markup's 
	 * 
	 * @param content
	 * @return
	 */
	private static String filterMarkupContent(String content)
	{
		String result = "";
		
		boolean isVelocity = false;
		boolean isPre = false;
		boolean isGroovy = false;
		boolean isCode = false;

		String[] lines = content.split("\n");
		for(String eachLine : lines)
		{
			String compareLine = eachLine.toLowerCase().trim();
			if(compareLine.startsWith("#"))
			{
				isVelocity = true;
			}
			else
			{
				isVelocity = false;
			}
			
			if(compareLine.indexOf("{pre}") > -1)
			{
				isPre = !isPre;
			}
			
			if(compareLine.indexOf("<%") > -1)
			{
				isGroovy = true;
			}
			
			if(compareLine.indexOf("%>") > -1)
			{
				isGroovy = false;
			}
			
			if(compareLine.indexOf("{code}") > -1)
			{
				isCode = !isCode;
			}

	
			if(!isVelocity  && !isPre && !isGroovy && !isCode)
			{
				result += eachLine;
			}
			
		}
		
		return result;
	}//filterMarkupContent
	
    private static Set<String> parseNamesFromScript(String script, String pattern)
    {
        Set<String> result = new HashSet<String>();

        if (StringUtils.isNotBlank(script))
        {
            Pattern p = Pattern.compile(pattern);
            Matcher matcher = p.matcher(script);
            while (matcher.find())
            {
                String str = matcher.group(2);
                str = str.replaceAll("(?<!\\\\)\\\\\"", "\"");
                str = str.replaceAll("(?<!\\\\)\\\\'", "'");
                str = str.replaceAll("(?<!\\\\)\\\\/", "/");
                
                result.add(str);
            }
        }
        return result;
    }
    
    public static Set<String> getListOfReferredForms(String content)
    {
        Set<String> formNames = new HashSet<String>();
        
        if (StringUtils.isNotBlank(content))
        {
            Pattern p = Pattern.compile("^.form:name.*$", Pattern.MULTILINE);
            Matcher matcher = p.matcher(content);
    
            while (matcher.find())
            {
                String matchedString = matcher.group();
                if (matchedString.contains("|"))
                {
                    String formName = matchedString.substring(matchedString.indexOf(":name=")+6, matchedString.indexOf("|"));
                    formNames.add(formName);
                }
                else
                {
                    String formName = matchedString.substring(matchedString.indexOf(":name=")+6, matchedString.indexOf("}"));
                    formNames.add(formName);
                }
            }
        }
        
        return formNames;
    }

    /**
     * returns the content with in {procedure} in sections of type procedure.
     * 
     * @param content
     * @return
     */
    public static List<String> getListOfProcedures(String content)
    {
        List<String> list = new ArrayList<String>();
        
        if (StringUtils.isNotBlank(content))
        {
            String includeFormPattern = "\\{section:type=procedure(.*?)\\{section\\}";
//            String elementSection = "{section}";
    
            Pattern p = Pattern.compile(includeFormPattern, Pattern.DOTALL);
            Matcher matcher = p.matcher(content);
    
            while (matcher.find())
            {
                String str = matcher.group();
                list.add(str);
            }
        }
        
        return list;
    }//getListOfProcedures
    
    /**
     * returns the list of activity maps by parsing content with in 
     * {procedure} in sections of type procedure.
     * 
     * @param procedureContent String representing playbook content
     * @return List of Map of activity attributes
     */
    public static List<Map<String, String>> getListOfActivityMaps(String procedureContent)
    {
        List<Map<String, String>> activities = new ArrayList<Map<String, String>>();

        String activitiesJSONVal = StringUtils.substringBetween(procedureContent, JSON_ARRAY_PREFIX, JSON_ARRAY_SUFFIX);
        
        if (StringUtils.isNotBlank(activitiesJSONVal))
        {
            activities = StringUtils.jsonArrayStringToList(PROCEDURE, JSON_OBJ_PREFIX + 
                                                           "\"" + PROCEDURE + "\":" + 
                                                           JSON_ARRAY_PREFIX + 
                                                           activitiesJSONVal + 
                                                           JSON_ARRAY_SUFFIX + 
                                                           JSON_OBJ_SUFFIX);
        }
        
        return activities;
    }//getListOfActivityMaps
    
    /**
     * Parses content and returns the list of activity maps by parsing content with in 
     * {procedure} in sections of type procedure.
     * 
     * @param content
     * @return
     */
    public static List<Map<String, String>> parseListOfActivityMaps(String content)
    {
        List<Map<String, String>> activities = new ArrayList<Map<String, String>>();

        List<String> procedures = ParseUtil.getListOfProcedures(content);
        
        if (procedures != null && !procedures.isEmpty())
        {
            activities = ParseUtil.getListOfActivityMaps(procedures.get(0));
        }
        
        return activities;
    }//parseListOfActivityMaps
    
    /**
     * Replaces procedure section contents (activities JSON) in passed in wiki content
     * with specified updated activities JSON.
     * 
     * @param wikiContent           Wiki content containing procedure section
     * @param updatedActivitiesJSON Updated activities JSON
     * @return 
     */
    public static String replaceProcedureSection(String wikiContent, String updatedActivitiesJSON)
    {
        StringBuilder sb = new StringBuilder();
        
        if (StringUtils.isNotBlank(wikiContent))
        {
            int procSectionMarkerStartIndx = wikiContent.indexOf("{section:type=procedure");
            
            if (procSectionMarkerStartIndx > -1)
            {
                int procSectionStartIndx = wikiContent.indexOf("[{", procSectionMarkerStartIndx);
                
                if (procSectionStartIndx > -1)
                {
                    int procSectionEndIndx = wikiContent.indexOf("}]", procSectionStartIndx);
                    
                    if (procSectionEndIndx > -1 && (procSectionEndIndx + 2) < wikiContent.length())
                    {
                        sb.append(wikiContent.substring(0, procSectionStartIndx));
                        sb.append(updatedActivitiesJSON);                    
                        sb.append(wikiContent.substring(procSectionEndIndx + 2));
                    }
                }
            }
        }
        
        return sb.toString();
    }
}