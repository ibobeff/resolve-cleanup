/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Log;

/**
 * main purpose of this utility class is to make sure that social component entities are available and the social notification master table is also
 * updated 
 * 
 * This is sequencial execution and can take longer. The multithreaded version is MigrateEntities which is faster and better for large data.
 * 
 * @author jeet.marwah
 *
 */

@Deprecated
public class MigrateSocialEntities
{
    private Set<String> namespaces = new HashSet<String>();

    public static void syncEntities()
    {
        MigrateSocialEntities cse = new MigrateSocialEntities();
        cse.syncSocialEntities();

        //TODO
        cse.updateNotificationTables();

    }

    private void syncSocialEntities()
    {
        //wiki/document
        migrateWikidocs();

        //namespace
        migrateNamespaces();

        //actiontask
        migrateActiontasks();

        //users
        migrateUsers();

        //process
        migrateProcesses();

        //team
        migrateTeams();

        //forum
        migrateForums();

        //rss
        migrateRss();

        //tags
//        migrateTags();

    }

    private void migrateWikidocs()
    {
        Log.log.debug("*** START UPDATE DOC TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("WikiDocument", null, "system"));
        if (sysIds != null && sysIds.size() > 0)
        {
            Log.log.debug("Total # of recs : " + sysIds.size());
            int count = 1;

            for (String sysId : sysIds)
            {
                Log.log.debug("Processed " + count++ + " Documents");

                //there is no need to use the save api for the doc as its just fixing the wiki content.
                WikiDocument doc = null;
                try
                {
                    doc = findWikiDocumentById(sysId);
                    if (doc != null)
                    {
                        Set<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
                        props.add(createProp(Document.IS_DT, doc.ugetUIsRoot() + ""));
                        props.add(createProp(Document.IS_RUNBOOK, doc.ugetUHasActiveModel() + ""));

                        ResolveNodeVO node = new ResolveNodeVO();
                        node.setUCompName(doc.getUFullname());
                        node.setUCompSysId(sysId);
                        node.setUType(NodeType.DOCUMENT);
                        node.setUMarkDeleted(doc.getUIsDeleted());
                        node.setULock(doc.getUIsLocked());
                        node.setUPinned(false);
                        node.setProperties(props);

                        //persist
                        persistNode(node);

                        //update the namespaces collection
                        namespaces.add(doc.getUNamespace());
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("Error saving the doc during the migration script. Please ignore this error as this will not affect the migration process. The document is " + doc.getUFullname(), e);
                }
            }//end of for
        }

        Log.log.debug("*** END OF UPDATE DOC TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }

    private void migrateNamespaces()
    {
        Log.log.debug("*** START NAMESPACES TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        Log.log.debug("Total # of recs : " + namespaces.size());
        int count = 1;

        for (String namespace : namespaces)
        {
            Log.log.debug("Processed " + count++ + " Namespaces");
            try
            {
                ResolveNodeVO node = new ResolveNodeVO();
                node.setUCompName(namespace);
                node.setUCompSysId(namespace);
                node.setUType(NodeType.NAMESPACE);
                node.setUMarkDeleted(false);
                node.setULock(false);
                node.setUPinned(false);

                //persist
                persistNode(node);
            }
            catch (Exception e)
            {
                Log.log.error("Error saving the Namespace during the migration script. Please ignore this error as this will not affect the migration process. The namespace is " + namespace, e);
            }

        }

        Log.log.debug("*** END OF UPDATE NAMESPACES TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");

    }

    private void migrateActiontasks()
    {
        Log.log.debug("*** START UPDATE AT TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("ResolveActionTask", null, "system"));
        if (sysIds != null && sysIds.size() > 0)
        {
            Log.log.debug("Total # of recs : " + sysIds.size());
            int count = 1;

            for (String sysId : sysIds)
            {
                Log.log.debug("Processed " + count++ + " Actiontasks");

                //there is no need to use the save api for the doc as its just fixing the wiki content.
                ResolveActionTask at = null;
                try
                {
                    at = findActiontaskById(sysId);
                    if (at != null)
                    {
                        //                        Collection<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
                        //                        props.add(createProp(Document.IS_DT, at.ugetUIsRoot() + ""));
                        //                        props.add(createProp(Document.IS_RUNBOOK, at.ugetUHasActiveModel() + ""));

                        ResolveNodeVO node = new ResolveNodeVO();
                        node.setUCompName(at.getUFullName());
                        node.setUCompSysId(at.getSys_id());
                        node.setUType(NodeType.ACTIONTASK);
                        node.setUMarkDeleted(false);
                        node.setULock(false);
                        node.setUPinned(false);
                        //                        node.setProperties(props);

                        //persist
                        persistNode(node);
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("Error saving the AT during the migration script. Please ignore this error as this will not affect the migration process. The AT is " + at.getUFullName(), e);
                }
            }//end of for
        }

        Log.log.debug("*** END OF UPDATE AT TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }

    private void migrateUsers()
    {
        Log.log.debug("*** START UPDATE USERS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("Users", null, "system"));
        if (sysIds != null && sysIds.size() > 0)
        {
            Log.log.debug("Total # of recs : " + sysIds.size());
            int count = 1;

            for (String sysId : sysIds)
            {
                Log.log.debug("Processed " + count++ + " Users");

                //there is no need to use the save api for the doc as its just fixing the wiki content.
                Users user = null;
                try
                {
                    user = findUserById(sysId);
                    if (user != null)
                    {
                        //                        Collection<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
                        //                        props.add(createProp(Document.IS_DT, at.ugetUIsRoot() + ""));
                        //                        props.add(createProp(Document.IS_RUNBOOK, at.ugetUHasActiveModel() + ""));

                        ResolveNodeVO node = new ResolveNodeVO();
                        node.setUCompName(user.getUUserName());
                        node.setUCompSysId(user.getSys_id());
                        node.setUType(NodeType.USER);
                        node.setUMarkDeleted(false);
                        node.setULock(user.ugetULockedOut());
                        node.setUPinned(false);
                        //                        node.setProperties(props);

                        //persist
                        persistNode(node);
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("Error saving the User during the migration script. Please ignore this error as this will not affect the migration process. The User is " + user.getUUserName(), e);
                }
            }//end of for
        }

        Log.log.debug("*** END OF UPDATE AT TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }

    @SuppressWarnings("unchecked")
    private void migrateProcesses()
    {
        Log.log.debug("*** START UPDATE PROCESS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        QueryDTO query = new QueryDTO();
        query.setModelName("social_process");
        query.setStart(0);
        query.setPage(1);

        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialProcessDTO> list = (List<SocialProcessDTO>) data.get("DATA");
            if (list != null && list.size() > 0)
            {
                Log.log.debug("Total # of recs : " + list.size());
                int count = 1;

                for (SocialProcessDTO dto : list)
                {
                    Log.log.debug("Processed " + count++ + " Processes");

                    ResolveNodeVO node = new ResolveNodeVO();
                    node.setUCompName(dto.getU_display_name());
                    node.setUCompSysId(dto.getSys_id());
                    node.setUType(NodeType.PROCESS);
                    node.setUMarkDeleted(false);
                    node.setULock(false);
                    node.setUPinned(false);
                    //                    node.setProperties(props);

                    //persist
                    persistNode(node);

                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating Process:", e);
        }

        Log.log.debug("*** END OF UPDATE OF PROCESS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }

    @SuppressWarnings("unchecked")
    private void migrateTeams()
    {
        Log.log.debug("*** START UPDATE TEAMS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        QueryDTO query = new QueryDTO();
        query.setModelName("social_team");
        query.setStart(0);
        query.setPage(1);

        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialTeamDTO> list = (List<SocialTeamDTO>) data.get("DATA");
            if (list != null && list.size() > 0)
            {
                Log.log.debug("Total # of recs : " + list.size());
                int count = 1;

                for (SocialTeamDTO dto : list)
                {
                    Log.log.debug("Processed " + count++ + " Teams");

                    ResolveNodeVO node = new ResolveNodeVO();
                    node.setUCompName(dto.getU_display_name());
                    node.setUCompSysId(dto.getSys_id());
                    node.setUType(NodeType.TEAM);
                    node.setUMarkDeleted(false);
                    node.setULock(false);
                    node.setUPinned(false);
                    //                    node.setProperties(props);

                    //persist
                    persistNode(node);

                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating Team:", e);
        }

        Log.log.debug("*** END OF UPDATE OF TEAMS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }

    @SuppressWarnings("unchecked")
    private void migrateForums()
    {
        Log.log.debug("*** START UPDATE FORUMS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        QueryDTO query = new QueryDTO();
        query.setModelName("social_forum");
        query.setStart(0);
        query.setPage(1);

        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialForumDTO> list = (List<SocialForumDTO>) data.get("DATA");
            if (list != null && list.size() > 0)
            {
                Log.log.debug("Total # of recs : " + list.size());
                int count = 1;

                for (SocialForumDTO dto : list)
                {
                    Log.log.debug("Processed " + count++ + " Forums");

                    ResolveNodeVO node = new ResolveNodeVO();
                    node.setUCompName(dto.getU_display_name());
                    node.setUCompSysId(dto.getSys_id());
                    node.setUType(NodeType.FORUM);
                    node.setUMarkDeleted(false);
                    node.setULock(false);
                    node.setUPinned(false);
                    //                    node.setProperties(props);

                    //persist
                    persistNode(node);

                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating Forums:", e);
        }

        Log.log.debug("*** END OF UPDATE OF FORUMS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }

    @SuppressWarnings("unchecked")
    private void migrateRss()
    {
        Log.log.debug("*** START UPDATE RSS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        QueryDTO query = new QueryDTO();
        query.setModelName("rss_subscription");
        query.setStart(0);
        query.setPage(1);

        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialRssDTO> list = (List<SocialRssDTO>) data.get("DATA");
            if (list != null && list.size() > 0)
            {
                Log.log.debug("Total # of recs : " + list.size());
                int count = 1;

                for (SocialRssDTO dto : list)
                {
                    Log.log.debug("Processed " + count++ + " RSS");

                    ResolveNodeVO node = new ResolveNodeVO();
                    node.setUCompName(dto.getU_display_name());
                    node.setUCompSysId(dto.getSys_id());
                    node.setUType(NodeType.RSS);
                    node.setUMarkDeleted(false);
                    node.setULock(false);
                    node.setUPinned(false);
                    //                    node.setProperties(props);

                    //persist
                    persistNode(node);

                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating RSS:", e);
        }

        Log.log.debug("*** END OF UPDATE OF RSS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }

    private void persistNode(ResolveNodeVO node)
    {
        try
        {
            GraphUtil.persistNode(node, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error persisting node:" + node.getUCompName() + ": Msg:" + e.getMessage());
        }
    }

    private void updateNotificationTables()
    {

    }

    private WikiDocument findWikiDocumentById(String sysId) throws Exception
    {
        WikiDocument doc = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	doc = (WikiDocument) HibernateProxy.execute(() -> {

        		return HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(sysId);

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error getting document with sysId: " + sysId, e);
            throw e;
        }

        return doc;
    }

    private ResolveActionTask findActiontaskById(String sysId) throws Exception
    {
        ResolveActionTask at = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
            at = (ResolveActionTask) HibernateProxy.execute(() -> {

            	return HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(sysId);

            });
        }
        catch (Exception e)
        {
            Log.log.error("Error getting AT with sysId: " + sysId, e);
            throw e;
        }

        return at;
    }

    private Users findUserById(String sysId) throws Exception
    {
        Users user = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	user = (Users) HibernateProxy.execute(() -> {

        		return HibernateUtil.getDAOFactory().getUsersDAO().findById(sysId);

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error getting Users with sysId: " + sysId, e);
            throw e;
        }

        return user;
    }

    private ResolveNodePropertiesVO createProp(String key, String value)
    {
        ResolveNodePropertiesVO prop = new ResolveNodePropertiesVO();
        prop.setUKey(key);
        prop.setUValue(value);

        return prop;
    }

}
