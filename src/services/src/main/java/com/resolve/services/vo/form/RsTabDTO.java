/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.util.List;


//map MetaFormTab with this DTO
public class RsTabDTO  extends RsUIComponent
{
    private int cols;//same as the count of the 'columns'
    private List<RsColumnDTO> columns;
    private String url;//if its a url, there will be no cols
    private List<RsFieldDependency> dependencies;
    
    public int getCols()
    {
        return cols;
    }

    public void setCols(int cols)
    {
        this.cols = cols;
    }

    public List<RsColumnDTO> getColumns()
    {
        return columns;
    }

    public void setColumns(List<RsColumnDTO> columns)
    {
        this.columns = columns;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public List<RsFieldDependency> getDependencies()
    {
        return dependencies;
    }
    
    public void setDependencies(List<RsFieldDependency> dependencies)
    {
        this.dependencies = dependencies;
    }
    
    

}
