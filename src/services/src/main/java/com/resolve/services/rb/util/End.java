/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

public class End extends TerminatorNode
{
    public End(int id, int x, int y, String inputs, String outputs)
    {
        super(id, "End", x, y, inputs, outputs, "ANY");
    }

    @Override
    String getStyle()
    {
        //return "symbol;image=/resolve/jsp/model/images/symbols/end.png";
        //dashed is for merge=any
        return "symbol;image=/resolve/jsp/model/images/symbols/end.png;dashed=1";
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("    <End ").append("label=\"End\" description=\"\" ").append("merge=\"merge = ANY\" id=\"" + getId() + "\">\n"); 
        sb.append(getCell().toString()).append("\n"); 
        sb.append(getParams().toString()).append("\n");
        sb.append("    </End>"); 
        
        return sb.toString();
    }
}
