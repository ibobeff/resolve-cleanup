/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

public class DecisionTree extends Document implements RSPublisher
{
    private static final long serialVersionUID = 2248014240239326233L;
    
//    public static final String TYPE = "decisiontree";

    public DecisionTree() throws Exception
    {
        this.setType(NodeType.DECISIONTREE);
    }
    
    public DecisionTree(ResolveNodeVO node) throws Exception
    {
        super(node);
        this.setId(node.getUCompSysId() + "-" + NodeType.DECISIONTREE.name());
        this.setSys_id(node.getUCompSysId() + "-" + NodeType.DECISIONTREE.name());
        this.setType(NodeType.DECISIONTREE);
    }

    public DecisionTree(String sysId, String name) throws Exception
    {
        this.setType(NodeType.DECISIONTREE);
        this.setId(sysId + "-" + NodeType.DECISIONTREE.name());
        this.setSys_id(sysId + "-" + NodeType.DECISIONTREE.name());
        this.setName(name);
    }

}
