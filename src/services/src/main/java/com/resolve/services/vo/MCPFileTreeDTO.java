/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MCPFileTreeDTO
{
    private String id;
    private String name;
    private Date date;
    private Boolean leaf;
    private List<MCPFileTreeDTO> children = new ArrayList<MCPFileTreeDTO>();
    
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public Date getDate()
    {
        return date;
    }
    public void setDate(Date date)
    {
        this.date = date;
    }
    public Boolean getLeaf()
    {
        return leaf;
    }
    public void setLeaf(Boolean leaf)
    {
        this.leaf = leaf;
    }
    
    public List<MCPFileTreeDTO> getChildren()
    {
        return children;
    }
    public void setChildren(List<MCPFileTreeDTO> children)
    {
        this.children = children;
    }
    
    @Override
    public String toString()
    {
        return name;
    }
    
    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof MCPFileTreeDTO)
        {
            MCPFileTreeDTO that = (MCPFileTreeDTO) otherObj;
            // if sys_id are not equal, they are diff
            if (that.getId().equals(this.getId()))
            {
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 37 + (this.getName() == null ? 0 : this.getName().hashCode());
        return hash;
    }
}
