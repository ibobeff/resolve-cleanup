/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.social;

import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * 
 * @author jeet.marwah
 *
 */
public class UserNotificationDTO
{
    private String username;
    
    private String compName;
    private NodeType compType;
    
    private UserGlobalNotificationContainerType notificationType;
    private String notificationValue;
    
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(username) || notificationType == null)
        {
            Log.log.debug("Username and Notificationtype is mandatory for migrating the notifications");
            valid = false;
        }
                
        return valid;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getCompName()
    {
        return compName;
    }

    public void setCompName(String compName)
    {
        this.compName = compName;
    }

    public NodeType getCompType()
    {
        return compType;
    }

    public void setCompType(NodeType compType)
    {
        this.compType = compType;
    }

    public UserGlobalNotificationContainerType getNotificationType()
    {
        return notificationType;
    }

    public void setNotificationType(UserGlobalNotificationContainerType notificationType)
    {
        this.notificationType = notificationType;
    }

    public String getNotificationValue()
    {
        return notificationValue;
    }

    public void setNotificationValue(String notificationValue)
    {
        this.notificationValue = notificationValue;
    }
    

}
