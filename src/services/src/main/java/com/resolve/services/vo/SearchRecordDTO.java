package com.resolve.services.vo;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.resolve.services.validator.sequence.Level1;

/**
 * The search DTO is used to represent search criteria for requests to the
 * aggregated content search.
 */
@JsonIgnoreProperties({ "readAccess", "writeAccess", "adminAccess", "executeAccess" })
public class SearchRecordDTO {

	@NotBlank(message = "Parameter type must not be empty", groups = Level1.class)
	private String type;
	@NotBlank(message = "Parameter sysId must not be empty", groups = Level1.class)
	private String sysId;
	private String name;
	private String summary;
	private String namespace;
	private String menupath;
	private Long sysCreatedOn;
	private String sysCreatedBy;
	private Long sysUpdatedOn;
	private String sysUpdatedBy;
	private List<String> tags;
	private String readAccess;
	private String writeAccess;
	private String adminAccess;
	private String executeAccess;
	private String author;

	public ContentType getType() {
		return ContentType.valueOf(type.toUpperCase());
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setType(ContentType type) {
		if (type != null) {
			this.type = type.toString();
		}
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getMenupath() {
		return menupath;
	}

	public void setMenupath(String menupath) {
		this.menupath = menupath;
	}

	public Long getSysCreatedOn() {
		return sysCreatedOn;
	}

	public void setSysCreatedOn(Long sysCreatedOn) {
		this.sysCreatedOn = sysCreatedOn;
	}

	public String getSysCreatedBy() {
		return sysCreatedBy;
	}

	public void setSysCreatedBy(String sysCreatedBy) {
		this.sysCreatedBy = sysCreatedBy;
	}

	public Long getSysUpdatedOn() {
		return sysUpdatedOn;
	}

	public void setSysUpdatedOn(Long sysUpdatedOn) {
		this.sysUpdatedOn = sysUpdatedOn;
	}

	public String getSysUpdatedBy() {
		return sysUpdatedBy;
	}

	public void setSysUpdatedBy(String sysUpdatedBy) {
		this.sysUpdatedBy = sysUpdatedBy;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getReadAccess() {
		return readAccess;
	}

	public void setReadAccess(String readAccess) {
		this.readAccess = readAccess;
	}

	public String getWriteAccess() {
		return writeAccess;
	}

	public void setWriteAccess(String writeAccess) {
		this.writeAccess = writeAccess;
	}

	public String getAdminAccess() {
		return adminAccess;
	}

	public void setAdminAccess(String adminAccess) {
		this.adminAccess = adminAccess;
	}

	public String getExecuteAccess() {
		return executeAccess;
	}

	public void setExecuteAccess(String executeAccess) {
		this.executeAccess = executeAccess;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
