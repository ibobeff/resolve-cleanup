/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.social;

import java.util.List;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;


public class ExportTagGraph extends ExportComponentGraph
{
    public ExportTagGraph(String sysId) throws Exception
    {
       super(sysId, SocialRelationshipTypes.ResolveTag);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode) throws Exception
    {
        //get all the rels for the tag
        Iterable<Relationship> rels = compNode.getRelationships();
        for(Relationship rel : rels)
        {
            Node othernode = rel.getOtherNode(compNode);
            
            if(othernode.hasProperty(GraphDBManager.TYPE))
            {
//                String otherSysId = (String) othernode.getProperty(GraphDBManager.SYS_ID);
//                String otherName = (String) othernode.getProperty(GraphDBManager.DISPLAYNAME);
//                String type = (String) othernode.getProperty(GraphDBManager.TYPE);
                
                addRelationship(compNode, othernode, null);
            }
        }
        
        
        return relationships;
    }
}
