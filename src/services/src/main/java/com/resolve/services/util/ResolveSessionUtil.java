/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.List;

import com.resolve.persistence.model.ResolveSession;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveSessionVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ResolveSessionUtil
{
    
    public static ResolveSessionVO getResolveSession(String userid)
    {
        ResolveSessionVO vo = null;
        ResolveSession session = null;

        if (StringUtils.isNotBlank(userid))
        {
            session = getResolveSessionPrivate(userid);
            if (session != null)
            {
                vo = session.doGetVO();
            }
        }

        return vo;
    } // getActiveWorksheet

    private static ResolveSession getResolveSessionPrivate(String userid)
    {
        ResolveSession session = null;

        if (StringUtils.isNotBlank(userid))
        {
            try
            {
               
                session = (ResolveSession) HibernateProxy.execute( () -> {
                	ResolveSession query = new ResolveSession();
                    query.setUActiveType(userid);

                     return HibernateUtil.getDAOFactory().getResolveSessionDAO().findFirst(query);
                });

            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return session;
        
    } // getResolveSessionPrivate
    
    public static void deleteResolveSession(String userName)
    {
        ResolveSession resolveSession = getResolveSessionPrivate(userName);
        
        try
        {
        	HibernateProxy.execute(() -> {
        		HibernateUtil.getDAOFactory().getResolveSessionDAO().delete(resolveSession);
        	});
            
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    private static List<ResolveSession> getResolveSessionsPrivate(String userid)
    {
        List<ResolveSession> sessions = null;

        if (StringUtils.isNotBlank(userid))
        {
            try
            {                
            	sessions = (List<ResolveSession>) HibernateProxy.execute( () -> {
                	ResolveSession query = new ResolveSession();
                    query.setUActiveType(userid);

                    return HibernateUtil.getDAOFactory().getResolveSessionDAO().find(query);
                });                
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return sessions;
        
    } // getResolveSessionsPrivate
    
    public static void deleteResolveSessions(String userName)
    {
        try
        {
        	HibernateProxy.execute(() -> {
	            
	            List<ResolveSession> sessions = getResolveSessionsPrivate(userName);
	            
	            if (sessions != null && !sessions.isEmpty())
	            {
	                for (ResolveSession rs : sessions)
	                {
	                    HibernateUtil.getDAOFactory().getResolveSessionDAO().delete(rs);
	                }
	            }
	            
        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
}
