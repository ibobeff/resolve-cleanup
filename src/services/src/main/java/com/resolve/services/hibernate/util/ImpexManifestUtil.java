/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.Query;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.Table;

import com.resolve.persistence.model.ResolveImpexManifest;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveImpexManifestVO;
import com.resolve.util.Log;

public class ImpexManifestUtil
{
    public static void saveManifest(ResolveImpexManifestVO manifestVO, String userName)
    {
        if (manifestVO != null)
        {
            ResolveImpexManifest manifestModel = new ResolveImpexManifest();
            manifestModel.applyVOToModel(manifestVO);

            // Resolution routing impex manifest throwing exception (http://10.20.2.111/browse/RBA-13376)
            // Cutting the following values so they can fit in the DB columns and not throw SQL exception:
            // - u_fullname
            // - u_module
            // - u_name
            for (@SuppressWarnings("unchecked")
            Iterator<Table> tableIterator = HibernateUtil.getTableMappings().iterator(); tableIterator.hasNext();)
            {
                Table table = tableIterator.next();
                javax.persistence.Table annotationTable = ResolveImpexManifest.class.getAnnotation(javax.persistence.Table.class);
                if (annotationTable != null) if (table.getName().equals(annotationTable.name()))
                {
                    for (@SuppressWarnings("unchecked")
                    Iterator<Column> columnIterator = table.getColumnIterator(); columnIterator.hasNext();)
                    {
                        Column column = columnIterator.next();
                        int columnLength;
                        if (column.getName().equals("u_fullname")) {
                            columnLength = column.getLength();
                            if (manifestModel.getUFullName() != null && manifestModel.getUFullName().length() > columnLength)
                            {  
                                manifestModel.setUFullName(manifestModel.getUFullName().substring(0, columnLength -1));
                            }
                        } else if (column.getName().equals("u_module")) {
                            columnLength = column.getLength();
                            if (manifestModel.getUModule() != null && manifestModel.getUModule().length() > columnLength)
                            {  
                                manifestModel.setUModule(manifestModel.getUModule().substring(0, columnLength -1));
                            }
                        } else if (column.getName().equals("u_name")) {
                            columnLength = column.getLength();
                            if (manifestModel.getUName() != null && manifestModel.getUName().length() > columnLength)
                            {  
                                manifestModel.setUName(manifestModel.getUName().substring(0, columnLength -1));
                            }
                        }
                    }
                    break;
                }
            }

            try
            {
              HibernateProxy.setCurrentUser(userName);
                HibernateProxy.execute(() -> {
                
	                if (StringUtils.isNotBlank(manifestModel.getSys_id()))
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexManifestDAO().update(manifestModel);
	                }
	                else
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexManifestDAO().persist(manifestModel);
	                }
                
                });
            }
            catch(Exception e)
            {
                Log.log.error("Failed to persist ResolveImpexManifest: " + e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
	public static void deleteManifest(String moduleName, String operationType, String userName) {
		try {
			HibernateProxy.execute(() -> {
				String sqlQuery = "delete from ResolveImpexManifest where UModuleName = :moduleName";
				if (StringUtils.isNotBlank(operationType)) {
					sqlQuery = sqlQuery + " and UOperationType = :operationType ";
				}

				Query query = HibernateUtil.createQuery(sqlQuery);
				query.setParameter("moduleName", moduleName);

				if (StringUtils.isNotBlank(operationType)) {
					query.setParameter("operationType", operationType);
				}

				query.executeUpdate();
			});
		} catch (Throwable e) {
			Log.log.error("Error while deleting Impex Manifest. Module: " + moduleName, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
	}
    
    @SuppressWarnings("rawtypes")
    public static int getCount(String moduleName, String operationType, String userName)
    {
        int result = -1;
        
        String sqlQuery = "from ResolveImpexManifest where UModuleName = '" + moduleName + "' and UOperationType = '" + operationType + "'";
        if (StringUtils.isNotBlank(operationType) && StringUtils.isNotBlank(moduleName))
        {
            try
            {
                
            	@SuppressWarnings("unchecked")
				List<Object> list = (List<Object>) HibernateProxy.execute(() -> {
            		Query query = HibernateUtil.createQuery(sqlQuery);
                    return query.list();
                });
                
                if (list != null)
                {
                    result = list.size();
                }
            }
            catch (Throwable e)
            {
                Log.log.error("Error while getCount. Module: " + moduleName, e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
}
