/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.io.Serializable;
import java.util.ArrayList;

import com.resolve.dto.ComboBoxModel;

/**
 * Used in the META_FORM to decise where the data is coming from 
 * 
 * @author jeet.marwah
 *
 */

public enum DataSource implements Serializable
{
	//this is mapped to wikiarchive table
	DB("DB"),
	INPUT("INPUT");
//	URL("URL"),//this will be at the tab level
//	PROPERTIES("PROPERTIES");
	
	//definition of the
	private final String tagName;
	
	private DataSource(String tagName)
	{
		this.tagName = tagName;
	}
	
	public String getTagName()
	{
		return this.tagName;
	}
	
    public static ArrayList<ComboBoxModel> getDataSourceModels()
    {
        ArrayList<ComboBoxModel> list = new ArrayList<ComboBoxModel>();

        DataSource[] arr = DataSource.values();
        for (DataSource ds : arr)
        {
            list.add(new ComboBoxModel(ds.getTagName(), ds.getTagName()));
        }

        return list;
    }// getDataSourceModels
	
	public static ArrayList<String> getDataSourceList()
	{
	    ArrayList<String> list = new ArrayList<String>();
	    
	    DataSource[] arr = DataSource.values();
	    for(DataSource ds : arr)
	    {
	        list.add(ds.getTagName());
	    }
	    
	    return list;
	}//getDataSourceList
	
	public static DataSource getRevisionType(String tagName)
	{
		if(tagName == null)
		{
			return null;
		}
		else if (tagName.equals(DB.toString()))
		{
			return DataSource.DB;
		}
        else if (tagName.equals(INPUT.toString()))
        {
            return DataSource.INPUT;
        }
		else
		{
			return null;
		}
			
	}
}
