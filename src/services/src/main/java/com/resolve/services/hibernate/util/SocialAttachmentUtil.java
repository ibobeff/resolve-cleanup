/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.SocialPostAttachment;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.SocialPostAttachmentVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialAttachmentUtil
{
    public static final String POSTID = "postid";
    public static final String FILE_DISPLAY_NAME = "newfilename";
    public static final String HTML_ELEMENT = "htmlelement";
    
    public static List<SocialPostAttachmentVO> getAllPostAttachments(String username) throws Exception
    {
        List<SocialPostAttachmentVO> result = new ArrayList<SocialPostAttachmentVO>();
        List<? extends Object> models = null;
        String selectedColumns = "sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UFilename,UDisplayName,UType,ULocation,USize,UPostId";
        String[] columns = selectedColumns.split(",");

        //allowing all the files for now as ideally all the attachments must be valid. The valid flag gets evaluated when the Post is done
        String sql = "select " + selectedColumns +" from SocialPostAttachment";
               
        try
        {
            models = GeneralHibernateUtil.executeHQLSelect(sql, new HashMap<String, Object>());
            
            if (models != null && models.size() > 0)
            {
                //this will be the array of objects
                for (Object o : models)
                {
                    Object[] record = (Object[]) o;
                    SocialPostAttachment instance = new SocialPostAttachment();

                    //set the attributes in the object
                    for (int x = 0; x < columns.length; x++)
                    {
                        String column = columns[x].trim();
                        Object value = record[x];

                        PropertyUtils.setProperty(instance, column, value);
                    }

                    result.add(convertModelToVO(instance, false));
                }//end of for loop
            }
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
        
        return result;
    }
    
    public static SocialPostAttachmentVO findSocialPostAttachmentById(String sys_id, String username)
    {
        SocialPostAttachment model = null;
        SocialPostAttachmentVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findSocialPostAttachmentModelById(sys_id, username);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = convertModelToVO(model, true);
        }
        
        return result;
    }
    
    public static void updateSocialPostAttachmentActiveFlag(String postid, boolean active, String username) throws Exception
    {
        String sql = "update SocialPostAttachment set UIsValid=" + active + " where UPostId='" + postid + "'";
        GeneralHibernateUtil.executeHQLUpdate(sql, username);
        
    }// updateSocialPostAttachmentActiveFlag
    
    public static SocialPostAttachmentVO saveSocialPostAttachment(SocialPostAttachmentVO vo, String username)
    {
        SocialPostAttachment postAttachment = new SocialPostAttachment(vo);
        postAttachment = SaveUtil.saveSocialPostAttachment(postAttachment, username);
        vo = postAttachment.doGetVO();
        
        return vo;
    }
    
    //private apis
    
    private static SocialPostAttachment findSocialPostAttachmentModelById(String sys_id, String username)
    {
        SocialPostAttachment result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	result = (SocialPostAttachment) HibernateProxy.execute(() -> {
                
            		return HibernateUtil.getDAOFactory().getSocialPostAttachmentDAO().findById(sys_id);
                
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static SocialPostAttachmentVO convertModelToVO(SocialPostAttachment model, boolean content)
    {
        SocialPostAttachmentVO vo = null;
        
        if(model != null)
        {
            vo = model.doGetVO();
            
            //for list, the content will not be there
            if(!content)
            {
                vo.setUContent(null);
            }
        }
        
        return vo;
    }
}
