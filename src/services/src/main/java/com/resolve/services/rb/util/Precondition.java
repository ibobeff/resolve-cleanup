/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;


public class Precondition extends Common implements Comparable<Common>
{
    public static final int width = 32;
    private static final int height = 32;
    
    private int order=9999;
    private String status="";
    private String href="true";

    
    public Precondition(int id, int x, int y, String description)
    {
        super(id, "Precondition", description, x, y, width, height, null);
    }

    @Override
    String getStyle()
    {
        return "rhombus;labelBackgroundColor=none;strokeColor=#FFA500;fillColor=#FFA500;gradientColor=white";
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("    <Precondition ").append("label=\"Precondition\" ")
        .append("description=\"").append(getDescription()).append("\" ")
        .append("order=\"").append(order).append("\" ")
        .append(" status=\"\" ").append("href=\"true\" ")
        .append("id=\"").append(getId()).append("\" >\n");
        sb.append(cell.toString()).append("\n");
        sb.append("    </Precondition>");
        return sb.toString();
    }

    public int compareTo(Common o)
    {
        return this.getId().compareTo(o.getId());
    }
}
