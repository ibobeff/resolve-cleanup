package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.resolve.persistence.model.SavedQueryFilter;
import com.resolve.persistence.model.SavedQuerySort;
import com.resolve.persistence.model.SavedSearch;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.SavedQueryFilterVO;
import com.resolve.services.hibernate.vo.SavedQuerySortVO;
import com.resolve.services.hibernate.vo.SavedSearchVO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.SavedSearchDTO;
import com.resolve.util.Log;

public class SavedSearchUtil {
	private final static int MAX_SAVED_SEARCH_COUNT = 20;
	private static final String ERROR_INCORRECT_NAME = "Saved search should has correct name";
	private static final String ERROR_NO_USERNAME = "No username provided";
	private static final String ERROR_NOT_FOUND = "Saved search not found";
	
	public static List<SavedSearchVO> getAllUserSavedSearch(String username) {
		List<SavedSearchVO> searchVOs = new ArrayList<>();
		try {
			HibernateProxy.execute( () -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<SavedSearch> criteriaQuery = criteriaBuilder.createQuery(SavedSearch.class);					
				Root<SavedSearch> from = criteriaQuery.from(SavedSearch.class);
				criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("sysCreatedBy"), username.toLowerCase()))
						.orderBy(criteriaBuilder.desc(from.get("sysUpdatedOn")));
				
				List<SavedSearch> result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery)
						.setMaxResults(MAX_SAVED_SEARCH_COUNT).getResultList();
				for (SavedSearch search : result) {
					searchVOs.add(search.doGetVO());
				}
			});			
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return searchVOs;
	}

	
	public static SavedSearchVO saveSavedSearch(SavedSearchDTO dto, String username) throws Throwable {
		SavedSearchVO vo = null;
		try {
			if (StringUtils.isEmpty(username)) {
				throw new IllegalArgumentException(ERROR_NO_USERNAME);
			}

			if (StringUtils.isEmpty(dto.getName())) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			vo = (SavedSearchVO) HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<SavedSearch> criteriaQuery = criteriaBuilder.createQuery(SavedSearch.class);					
				Root<SavedSearch> from = criteriaQuery.from(SavedSearch.class);

				Predicate pred = criteriaBuilder.and(
						criteriaBuilder.equal(from.get("sysCreatedBy"), username.toLowerCase()),
						criteriaBuilder.equal(from.get("name"), dto.getName()));

				criteriaQuery.select(from).where(pred);

				SavedSearch oldSearch = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();
				
				if (oldSearch == null) {
					oldSearch = new SavedSearch();
				}
				
				oldSearch.setName(dto.getName());
				oldSearch.setSysCreatedBy(username);
				oldSearch.setSysUpdatedBy(username);

				if (oldSearch.getSorts() != null) {
					oldSearch.getSorts().clear();
					oldSearch.getSorts().addAll(getSavedQuerySortItems(dto.getSorts(), username));
				} else {
					oldSearch.setSorts(getSavedQuerySortItems(dto.getSorts(), username));
				}
				
				if (oldSearch.getFilters() != null) {
					oldSearch.getFilters().clear();
					oldSearch.getFilters().addAll(getSavedQueryFilterItems(dto.getFilters(), username));
				} else {
					oldSearch.setFilters(getSavedQueryFilterItems(dto.getFilters(), username));
				}
				
				SavedSearch result = HibernateUtil.getDAOFactory().getSavedSearchDAO().insertOrUpdate(oldSearch);
				return result.doGetVO();
			});

			return vo;
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
			throw t;
		}
	}

	public static boolean deleteSavedSearch(String username, String searchName) {
		try {
			if (StringUtils.isEmpty(searchName)) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			return (boolean) HibernateProxy.execute(() -> {

				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<SavedSearch> criteriaQuery = criteriaBuilder.createQuery(SavedSearch.class);					
				Root<SavedSearch> from = criteriaQuery.from(SavedSearch.class);
				
				Predicate pred = criteriaBuilder.and(
						criteriaBuilder.equal(from.get("sysCreatedBy"), username.toLowerCase()),
						criteriaBuilder.equal(from.get("name"), searchName));
	
				criteriaQuery.select(from).where(pred);
				
				SavedSearch oldSearch = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();
	
				if (oldSearch == null) {
					throw new IllegalArgumentException(ERROR_NOT_FOUND);
				}
				
				HibernateUtil.getDAOFactory().getSavedSearchDAO().delete(oldSearch);

				return true;
			});

		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		
		return false;
	}

	public static List<SavedSearchDTO> covertSavedSearchDTOList(List<SavedSearchVO> vos) {
		List<SavedSearchDTO> dtos = new ArrayList<>();
		if (vos != null) {
			for (SavedSearchVO vo : vos) {
				dtos.add(covertSavedSearchDTO(vo));
			}
			
		}
		
		return dtos;
	}

	public static SavedSearchDTO covertSavedSearchDTO(SavedSearchVO vo) {
		SavedSearchDTO dto = new SavedSearchDTO();
		dto.setName(vo.getName());

		List<QueryFilter> filterDtos = new ArrayList<>();
		if (vo.getFilters() != null) {
			for (SavedQueryFilterVO filterVO : vo.getFilters()) {
				QueryFilter filter = new QueryFilter();
				filter.setCaseSensitive(filterVO.getCaseSensitive());
				filter.setCondition(filterVO.getCondition());
				filter.setEndValue(filterVO.getEndValue());
				filter.setField(filterVO.getField());
				filter.setParamName(filterVO.getParamName());
				filter.setStartCount(filterVO.getStartCount());
				filter.setStartValue(filterVO.getStartValue());
				filter.setType(filterVO.getType());
				filter.setValue(filterVO.getValue());

				filterDtos.add(filter);
			}
		}
		dto.setFilters(filterDtos);

		List<QuerySort> sortDtos = new ArrayList<>();
		if (vo.getSorts() != null) {
			for (SavedQuerySortVO sortVO : vo.getSorts()) {
				sortDtos.add(new QuerySort(sortVO.getProperty(), sortVO.getDirection()));
			}
		}
		dto.setSorts(sortDtos);
		
		return dto;
	}

	private static List<SavedQuerySort> getSavedQuerySortItems(List<QuerySort> sorts, String username) {
		List<SavedQuerySort> items = new ArrayList<>();

		for (QuerySort sort : sorts) {
			SavedQuerySort savedSort = new SavedQuerySort();
			savedSort.setProperty(sort.getProperty());
			savedSort.setDirection(sort.getDirection());
			savedSort.setSysCreatedBy(username);
			savedSort.setSysUpdatedBy(username);
			
			items.add(savedSort);
		}
		return items;
	}

	private static List<SavedQueryFilter> getSavedQueryFilterItems(List<QueryFilter> filters, String username) {
		List<SavedQueryFilter> items = new ArrayList<>();

		for (QueryFilter filter : filters) {
			SavedQueryFilter savedFilter = new SavedQueryFilter();
			savedFilter.setCaseSensitive(filter.getCaseSensitive());
			savedFilter.setCondition(filter.getCondition());
			savedFilter.setEndValue(filter.getEndValue());
			savedFilter.setField(filter.getField());
			savedFilter.setParamName(filter.getParamName());
			savedFilter.setStartCount(filter.getStartCount());
			savedFilter.setStartValue(filter.getStartValue());
			savedFilter.setType(filter.getType());
			savedFilter.setValue(filter.getValue());
			savedFilter.setSysCreatedBy(username);
			savedFilter.setSysUpdatedBy(username);

			items.add(savedFilter);
		}
		return items;
	}
}
