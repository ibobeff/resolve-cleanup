/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.exception.ActionTaskAbortException;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.Log;
import com.resolve.util.NameTypeValue;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;


public class VariableUtil
{
    private final static Pattern VAR_REGEX_URL_PARAM_VARIABLE = Pattern.compile(Constants.VAR_REGEX_URL_PARAM_VARIABLE);
    private final static Pattern VAR_REGEX_PROPERTY = Pattern.compile(Constants.VAR_REGEX_PROPERTY);
    private final static Pattern VAR_REGEX_PARAM = Pattern.compile(Constants.VAR_REGEX_PARAM);
    private final static Pattern VAR_REGEX_INPUT = Pattern.compile(Constants.VAR_REGEX_INPUT);
    private final static Pattern VAR_REGEX_OUTPUT = Pattern.compile(Constants.VAR_REGEX_OUTPUT);
    private final static Pattern VAR_REGEX_FLOW = Pattern.compile(Constants.VAR_REGEX_FLOW);
    private final static Pattern VAR_REGEX_WSDATA = Pattern.compile(Constants.VAR_REGEX_WSDATA);
    
    private final static Pattern VAR_REGEX_NESTED_PARAM = Pattern.compile(Constants.VAR_REGEX_NESTED_PARAM);
    private final static Pattern VAR_REGEX_NESTED_INPUT = Pattern.compile(Constants.VAR_REGEX_NESTED_INPUT);
    private final static Pattern VAR_REGEX_NESTED_OUTPUT = Pattern.compile(Constants.VAR_REGEX_NESTED_OUTPUT);
    private final static Pattern VAR_REGEX_NESTED_FLOW = Pattern.compile(Constants.VAR_REGEX_NESTED_FLOW);
    private final static Pattern VAR_REGEX_NESTED_WSDATA = Pattern.compile(Constants.VAR_REGEX_NESTED_WSDATA);

    // NOTE: Does not include PROPERTIES as they dont change very much but the
    // benefit outweighs the minimal permgen leak
    private final static Pattern VAR_REGEX_VARSTR = Pattern.compile(Constants.VAR_REGEX_INPUT + "|" + Constants.VAR_REGEX_OUTPUT + "|" + Constants.VAR_REGEX_PARAM + "|" + Constants.VAR_REGEX_FLOW);

    public static boolean hasVarsString(String content)
    {
        return VAR_REGEX_VARSTR.matcher(content).find();
    } // hasVarsString

    /*
    public static void main(String[] args)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("problemid", "1234");
        params.put("executeid", "5678");

        System.out.println(replaceUrlParams("PROBLEMID=${problemid}&EXECUTEID=${executeid}", params));
    }
    */

    public static String replaceUrlParams(String replaceStr, Map<String, String> params)
    {
        String result = replaceStr;

        Matcher matcher = VAR_REGEX_URL_PARAM_VARIABLE.matcher(replaceStr);
        while (matcher.find())
        {
            String name = matcher.group(1);
            String value = params.get(name);
            if (value == null)
            {
                value = "";
            }

            result = result.replaceAll("\\$\\{" + name + "\\}", value);
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    public static String replaceVarsString(String replaceStr, Map wsData, Map inputs, Map outputs, Map params, Map flows) throws Exception
    {
        String result = null;

        Object obj = VariableUtil.replaceVars(replaceStr, wsData, inputs, outputs, params, flows, null);
        if (obj != null)
        {
            if (obj instanceof String)
            {
                result = (String) obj;
            }
            else if (obj instanceof EncryptionTransportObject)
            {
                result = ((EncryptionTransportObject) obj).getPlainText();
            }
            else
            {
                throw new Exception("replaceVars did not return a String");
            }
        }

        return result;
    } // replaceVarsString

    @SuppressWarnings("rawtypes")
    public static String replaceVarsString(String replaceStr, Map inputs, Map outputs, Map params, Map flows) throws Exception
    {
        String result = null;

        Object obj = VariableUtil.replaceVars(null, replaceStr, inputs, outputs, params, flows, null);
        if (obj != null)
        {
            if (obj instanceof String)
            {
                result = (String) obj;
            }
            else if (obj instanceof EncryptionTransportObject)
            {
                result = ((EncryptionTransportObject) obj).getPlainText();
            }
            else
            {
                throw new Exception("replaceVars did not return a String");
            }
        }

        return result;
    } // replaceVarsString

    @SuppressWarnings("rawtypes")
    public static Object replaceVars(String replaceStr, Map wsData, Map inputs, Map outputs, Map params, Map flows, Map resultAttr)
    {
        Object result = null;

        if (!StringUtils.isEmpty(replaceStr))
        {
            if (isPatternAndValueIsObjectNotString(VAR_REGEX_PARAM, params, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_PARAM, params, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_INPUT, inputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_INPUT, inputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_OUTPUT, outputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_OUTPUT, outputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_FLOW, flows, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_FLOW, flows, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_WSDATA, flows, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_WSDATA, wsData, replaceStr);
            }
            else
            {
                // replace $PARAM{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_PARAM, params, replaceStr);

                // replace $INPUT{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_INPUT, inputs, replaceStr);

                // replace $OUTPUT{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_OUTPUT, outputs, replaceStr);

                // replace $FLOW{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_FLOW, flows, replaceStr);

                // replace $PROPERTY{NAME}
                replaceStr = replaceProperties(replaceStr, resultAttr);
                
                // replace $WSDATA{NAME}
                replaceStr = replaceWSDATA((String)params.get(Constants.EXECUTE_PROBLEMID), replaceStr);

                result = replaceStr;
            }
        }

        return result;
    } // replaceVars
    @SuppressWarnings("rawtypes")
    public static Object replaceVarsNested(String name,String replaceStr, Map wsdata, Map inputs, Map outputs, Map params, Map flows, Map resultAttr, Map inputsToTryForNested) throws Exception
    {
        Object result = null;
        if (!StringUtils.isEmpty(replaceStr))
        {
            if (isPatternAndValueIsObjectNotString(VAR_REGEX_PARAM, params, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_PARAM, params, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_INPUT, inputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_INPUT, inputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_OUTPUT, outputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_OUTPUT, outputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_FLOW, flows, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_FLOW, flows, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_WSDATA,wsdata,replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_WSDATA, wsdata, replaceStr);
            }
            else
            {                               
                if (replaceStr != null && replaceStr.matches(".*\\$.*?\\{(.*)\\}.*"))
                {
                    result= recursiveReplaceOutputs(name, replaceStr, inputsToTryForNested, outputs, params, flows, resultAttr);
                    // Circular dependency, abort
                    if (result == null)
                    {
                        throw new Exception("FAIL: Circular dependency or reference to a non-existing INPUT parameter in the definition for INPUT parameter '" + name + "'");
                    }
                }
                else
                {
                    result = replaceString(replaceStr,inputs,outputs,params,flows,resultAttr);
                }
            }
        }

        return result;
    } // replaceVars
    
    // RBA-14814: Implementation - Create and Process Resolution Routing Rule for Security IR
    private static String recursiveReplaceInputs(String name, String replaceStr, Map inputs, Map outputs, Map params, Map flows, Map resultAttr) {
        String result = "";
        String value = replaceStr;
        String nestedInputName = "";
		// Pattern patternInput = Pattern.compile("(?<=\\$INPUT\\{).+(?=\\})"); // looking for containing $INPUT{<NAME>}
		Pattern patternInput = Pattern.compile("\\$INPUT\\{(.*?[\\}]*)\\}[\\}]*"); // \$INPUT\{(.*?[\}]*)\}[\}]*
        Matcher matcher = patternInput.matcher(replaceStr);
        while (matcher.find()) {
            nestedInputName = matcher.group(1);
            Matcher matcherNested = patternInput.matcher(nestedInputName);
            if(matcherNested.find()) {
            	String matcherNestedName = matcherNested.group(1); 
            	// Avoid circular dependency, after evaluation of the nested group
                if (value == null || name.equals(matcherNestedName)) {
                    return null;
                }
            	value = recursiveReplaceInputs(name, nestedInputName, inputs, outputs, params, flows, resultAttr);
                int groupLocation = replaceStr.indexOf(nestedInputName);
                value = replaceStr.substring(0,replaceStr.indexOf(nestedInputName)) + value;
                value += replaceStr.substring(groupLocation+nestedInputName.length(), replaceStr.length());
                // Avoid reference to non-existing entry from either INPUTS or PARAMS
            } else if (inputs == null || inputs.size() == 0 || !inputs.containsKey(nestedInputName) || name.equals(nestedInputName)) {
            	return null;	
            }
        }
        result = replaceString(value,inputs,outputs,params,flows,resultAttr);
        Pattern patternGeneric = Pattern.compile("(?<=\\$.{0,10}\\{).+(?=\\})"); // looking for containing any $<CONTAINER>{<NAME>}
        if(patternGeneric.matcher(result).find()) {
            result = recursiveReplaceInputs(name, result,inputs,outputs,params,flows,resultAttr);
        }
        return result;
    }
    
    private static String recursiveReplaceOutputs(String name, String replaceStr, Map inputstoTryforNested, Map outputs, Map params, Map flows, Map resultAttr) {
        String result = "";
        String value = replaceStr;
        String nestedGroup = "";
        // Pattern par = Pattern.compile("\\$.*?\\{(.*)\\}");
        Pattern par = Pattern.compile("\\$[^{]*\\{([^}]*\\}*)\\}");
        Matcher matcher = par.matcher(replaceStr);
        if(matcher.find()) {
            nestedGroup = matcher.group(1);
            // RBA-14560 : Allow use of input param in another input param in Action Task and Runbook
            // Avoid circular dependency
            if (name != null && name.equals(nestedGroup))
            {
                return null;
            }
            Matcher nested = par.matcher(nestedGroup);
            if(nested.find()) {
                value = recursiveReplaceOutputs(name, nestedGroup, inputstoTryforNested, outputs, params, flows, resultAttr);
                int groupLocation = replaceStr.indexOf(nestedGroup);
                value = replaceStr.substring(0,replaceStr.indexOf(nestedGroup)) + value;
                value += replaceStr.substring(groupLocation+nestedGroup.length(), replaceStr.length());
            }
        }
        result = replaceString(value,inputstoTryforNested,outputs,params,flows,resultAttr);
        Matcher nestedResult = par.matcher(result);
        Matcher nestedGroupMatcher = par.matcher(nestedGroup);
        if(nestedResult.find()) {
            result = recursiveReplaceOutputs(name, result,inputstoTryforNested,outputs,params,flows,resultAttr);
        } 
        
        // Abort upon returning a replace variable
        else if (!nestedGroupMatcher.find() && !inputstoTryforNested.containsKey(nestedGroup))
        {
            return null;
        }
        return result;
    }
    private static String replaceString(String replaceStr, Map inputs, Map outputs, Map params, Map flows, Map resultAttr) {
        // replace $PARAM{NAME}
        replaceStr = replaceParamMap(VAR_REGEX_PARAM, params, replaceStr);

        // replace $INPUT{NAME}
        replaceStr = replaceParamMap(VAR_REGEX_INPUT, inputs, replaceStr);

        // replace $OUTPUT{NAME}
        replaceStr = replaceParamMap(VAR_REGEX_OUTPUT, outputs, replaceStr);

        // replace $FLOW{NAME}
        replaceStr = replaceParamMap(VAR_REGEX_FLOW, flows, replaceStr);

        // replace $PROPERTY{NAME}
        replaceStr = replaceProperties(replaceStr, resultAttr);
        
        // replace $WSDATA{NAME}
        replaceStr = replaceWSDATA((String)params.get(Constants.EXECUTE_PROBLEMID), replaceStr);
        return replaceStr;
    }
    
    @SuppressWarnings("rawtypes")
    public static Object replaceVars(String name, String replaceStr, Map inputs, Map outputs, Map params, Map flows, Map resultAttr)
    {
        Object result = null;

        if (!StringUtils.isEmpty(replaceStr))
        {
            if (isPatternAndValueIsObjectNotString(VAR_REGEX_PARAM, params, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_PARAM, params, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_INPUT, inputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_INPUT, inputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_OUTPUT, outputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_OUTPUT, outputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_FLOW, flows, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_FLOW, flows, replaceStr);
            }
            else
            {
                // replace $PARAM{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_PARAM, params, replaceStr);

                // replace $INPUT{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_INPUT, inputs, replaceStr);

                // replace $OUTPUT{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_OUTPUT, outputs, replaceStr);

                // replace $FLOW{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_FLOW, flows, replaceStr);

                // replace $PROPERTY{NAME}
                replaceStr = replaceProperties(replaceStr, resultAttr);

                // replace $WSDATA{NAME}
                replaceStr = replaceWSDATA((String)params.get(Constants.EXECUTE_PROBLEMID), replaceStr);
                result = replaceStr;
            }
        }

        return result;
    } // replaceVars
    
    @SuppressWarnings("rawtypes")
    public static Object replaceVarsIncludingNestedInputs(String name, String replaceStr, Map inputs, Map outputs, Map params, Map flows, Map resultAttr, Map inputsToTryForNested) throws Exception
    {
        Object result = null;
        
        if (!StringUtils.isEmpty(replaceStr))
        {
            if (isPatternAndValueIsObjectNotString(VAR_REGEX_PARAM, params, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_PARAM, params, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_INPUT, inputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_INPUT, inputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_OUTPUT, outputs, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_OUTPUT, outputs, replaceStr);
            }
            else if (isPatternAndValueIsObjectNotString(VAR_REGEX_FLOW, flows, replaceStr))
            {
                result = getValueOfObject(VAR_REGEX_FLOW, flows, replaceStr);
            }
            else
            {
                // replace $PARAM{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_PARAM, params, replaceStr);

                // RBA-14560 : Allow use of input param in another input param in Action Task and Runbook
                // replace $INPUT{NAME}
                Pattern patternInput = Pattern.compile("(?<=\\$INPUT\\{).+(?=\\})"); // looking for containing $INPUT{<NAME>}
                Matcher matcher = patternInput.matcher(replaceStr);
                if (replaceStr != null && matcher.find())
                {
                	String originalReplaceStr = replaceStr;
                	replaceStr = recursiveReplaceInputs(name, replaceStr, inputsToTryForNested, outputs, params, flows, resultAttr);
            		// Circular dependency, abort
                    if (replaceStr == null)
                    {
                        throw new ActionTaskAbortException("ActionTask has been blocked from execution : Circular dependency or reference to a non-existing INPUT parameter in the definition for INPUT parameter '" + name + "' : '" + originalReplaceStr + "'.");
                    }
                }
                else
                {
                    replaceStr = replaceParamMap(VAR_REGEX_INPUT, inputs, replaceStr);    
                }

                // replace $OUTPUT{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_OUTPUT,outputs,replaceStr);

                // replace $FLOW{NAME}
                replaceStr = replaceParamMap(VAR_REGEX_FLOW, flows, replaceStr);

                // replace $PROPERTY{NAME}
                replaceStr = replaceProperties(replaceStr, resultAttr);

                // replace $WSDATA{NAME}
                replaceStr = replaceWSDATA((String)params.get(Constants.EXECUTE_PROBLEMID), replaceStr);
                result = replaceStr;
            }
        }

        return result;
    } // replaceVars

    @SuppressWarnings("rawtypes")
    static String replaceParamMap(Pattern pattern, Map params, String matchContent)
    {
        String result = "";

        if (!StringUtils.isEmpty(matchContent) && params != null)
        {
            // replace $<pattern>{NAME}
            int last = 0;
            Matcher matcher = pattern.matcher(matchContent);
            while (matcher.find())
            {
                String name = matcher.group(1);
                String value = getParam(name, params);

                if (value == null)
                {
                    result += matchContent.substring(last, matcher.start());
                }
                else
                {
                    result += matchContent.substring(last, matcher.start()) + value;
                }
                last = matcher.end();
            }
            result += matchContent.substring(last, matchContent.length());
        }

        return result;
    } // replaceParamMap

    @SuppressWarnings("rawtypes")
    static boolean isPatternAndValueIsObjectNotString(Pattern pattern, Map params, String matchContent)
    {
        boolean isPatternAndValueIsObjectNotString = false;
        Object result = null;

        if (!StringUtils.isEmpty(matchContent) && params != null)
        {
            Matcher matcher = pattern.matcher(matchContent);

            while (matcher.find())
            {
                String name = matcher.group(1);
                result = getParamObject(name, params);

                if (result != null && !(result instanceof String) && !(result instanceof EncryptionTransportObject) )
                {
                    isPatternAndValueIsObjectNotString = true;
                }

                break;
            }
        }

        return isPatternAndValueIsObjectNotString;
    } // isPatternAndValueIsObjectNotString

    @SuppressWarnings("rawtypes")
    static Object getValueOfObject(Pattern pattern, Map params, String matchContent)
    {
        Object result = null;

        if (!StringUtils.isEmpty(matchContent) && params != null)
        {
            Matcher matcher = pattern.matcher(matchContent);
            while (matcher.find())
            {
                String name = matcher.group(1);
                result = getParamObject(name, params);
                break;
            }
        }

        return result;
    } // isPattenAndValueIsObjectNotString

    public static String replaceProperties(String matchContent)
    {
        return replaceProperties(matchContent, null);
    } // replaceProperties
    
    public static String replaceProperties(String matchContent, Map resultAttr)
    {
        String result = "";

        if (!StringUtils.isEmpty(matchContent))
        {
            // replace $PROPERTY{NAME}
            int last = 0;
            Matcher matcher = VAR_REGEX_PROPERTY.matcher(matchContent);
            while (matcher.find())
            {
                String name = matcher.group(1);
                String value = getProperty(name, resultAttr);
                if (value == null)
                {
                    result += matchContent.substring(last, matcher.start());
                }
                else
                {
                    result += matchContent.substring(last, matcher.start()) + value;
                }
                last = matcher.end();
            }
            result += matchContent.substring(last, matchContent.length());
        }

        return result;
    } // replaceProperties

    static String replaceWSDATA(String problemId, String matchContent)
    {
        String result = "";

        if (StringUtils.isNotBlank(matchContent) && StringUtils.isNotBlank(problemId))
        {
            // replace $WSDATA{NAME} from a remote task's content or other sections.
            // Ex: def hello
            int last = 0;
            Matcher matcher = VAR_REGEX_WSDATA.matcher(matchContent);
            while (matcher.find())
            {
                String name = matcher.group(1);
                String value = null;
                try{
                    value = (String) ServiceWorksheet.getWorksheetWSDATAProperty(problemId, name, "system");
                }
                catch(ClassCastException e) {
                    value = ServiceWorksheet.getWorksheetWSDATAProperty(problemId, name, "system").toString();
                }
                if (value == null)
                {
                    result += matchContent.substring(last, matcher.start());
                }
                else
                {
                    result += matchContent.substring(last, matcher.start()) + value;
                }
                last = matcher.end();
            }
            result += matchContent.substring(last, matchContent.length());
        }

        return result;
    } // replaceWSDATA

    @SuppressWarnings("rawtypes")
    static Object getParamObject(String name, Map params)
    {
        Object resultObject = null;

        try
        {
            if (params != null)
            {
                if (params.containsKey(name))
                {
                    resultObject = params.get(name);
                }
                else if (params.containsKey(name.toUpperCase()))
                {
                    resultObject = params.get(name.toUpperCase());
                }
                else if (params.containsKey(name.toLowerCase()))
                {
                    resultObject = params.get(name.toLowerCase());
                }
            }

        }
        catch (Exception e)
        {
            Log.log.warn("Failed to retrieve parameters: " + e.getMessage(), e);
        }

        return resultObject;
    } // getParamObject

    @SuppressWarnings("rawtypes")
    static String getParam(String name, Map params)
    {
        String result = "";

        try
        {
            if (params != null)
            {
                if (params.containsKey(name) && params.get(name) != null)
                {
                    result = params.get(name).toString();
                }
                else if (params.containsKey(name.toUpperCase()) && params.get(name.toUpperCase()) != null)
                {
                    result = params.get(name.toUpperCase()).toString();
                }
                else if (params.containsKey(name.toLowerCase()) && params.get(name.toLowerCase()) != null)
                {
                    result = params.get(name.toLowerCase()).toString();
                }

                if (result != null)
                {
                    result = CryptUtils.decrypt(result);
                }
            }

        }
        catch (Exception e)
        {
            Log.log.warn("Failed to retrieve parameter [" + name + "] from params [" + StringUtils.mapToLogString(params)+ "]: " + e.getMessage(), e);
        }

        return result;
    } // getParam

    public static String getProperty(String name)
    {
        return ServiceHibernate.getProperty(name);
    } // getProperty
    
    public static String getProperty(String name, Map resultAttr)
    {
        return ServiceHibernate.getProperty(name, resultAttr);
    } // getProperty


    public static void setProperty(String name, String value)
    {
        ServiceHibernate.setProperty(name, value, null, false);
    } // setProperty

    public static void setProperty(String name, String value, String module)
    {
        ServiceHibernate.setProperty(name, value, module, false);
    } // setProperty

    public static void setEncryptProperty(String name, String value)
    {
        ServiceHibernate.setProperty(name, value, null, true);
    } // setEncryptProperty

    public static void setEncryptProperty(String name, String value, String module)
    {
        ServiceHibernate.setProperty(name, value, module, true);
    } // setEncryptProperty

    public static void removeProperty(String name)
    {
       ServiceHibernate.removeProperty(name);
       
    } // setProperty

    public static NameTypeValue getNameTypeValue(String content)
    {
        NameTypeValue result = null;
        if (!StringUtils.isEmpty(content) && !content.equalsIgnoreCase("[Please Enter Value]"))
        {
            // replace $PARAM{NAME}
            Matcher matcher = VAR_REGEX_NESTED_PARAM.matcher(content);
            if (matcher.find())
            {
                result = new NameTypeValue(matcher.group(1), "PARAM", content);
            }

            // replace $INPUT{NAME}
            if (result == null)
            {
                matcher = VAR_REGEX_NESTED_INPUT.matcher(content);
                if (matcher.find())
                {
                    result = new NameTypeValue(matcher.group(1), "INPUT", content);
                }
            }

            // replace $OUTPUT{NAME}
            if (result == null)
            {
                matcher = VAR_REGEX_NESTED_OUTPUT.matcher(content);
                if (matcher.find())
                {
                    result = new NameTypeValue(matcher.group(1), "OUTPUT", content);
                }
            }

            // replace $FLOW{NAME}
            if (result == null)
            {
                matcher = VAR_REGEX_NESTED_FLOW.matcher(content);
                if (matcher.find())
                {
                    result = new NameTypeValue(matcher.group(1), "FLOW", content);
                }
            }
            
            // replace $WSDATA{NAME}
            if (result == null)
            {
                matcher = VAR_REGEX_NESTED_WSDATA.matcher(content);
                if (matcher.find())
                {
                    result = new NameTypeValue(matcher.group(1), "WSDATA", content);
                }
            }
        }

        return result;
    } // getNameTypeValue

} // VariableUtil