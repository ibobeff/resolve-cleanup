/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import groovy.lang.Binding;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Level;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.resolve.rsbase.MainBase;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.persistence.model.ResolveSysScript;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExecuteSysScriptUtil
{
    public static String executeScript(String name, Map<String, Object> params)
    {
        String result = HibernateConstants.STATUS_SUCCESS;

        // init
        String script = null;

        Transaction tx = null;
		try
        {
            // get script
            Session session = HibernateUtil.getSession();
    		tx = HibernateUtil.beginTransaction(session);

            ResolveSysScript sysScript = new ResolveSysScript();
            sysScript.setUName(name);
            sysScript = HibernateUtil.getDAOFactory().getResolveSysScriptDAO().findFirst(sysScript);
            if (sysScript != null)
            {
                script = sysScript.getUScript();
            }
            HibernateUtil.commitTransaction(tx);

            // execute system script
            if (script != null && !script.equals(""))
            {
                Object obj = executeSysScript(script, name, params);
                if (obj != null)
                {
                    result = obj.toString();
                }
            }else 
            {
                result = HibernateConstants.STATUS_FAILED;
                HibernateUtil.rollbackTransaction(tx);
                Log.log.error("Trying to execute a script that does not exist");
                throw new RuntimeException("Trying to execute a script that does not exist");
            }
        }
        catch (Throwable t)
        {
            result = HibernateConstants.STATUS_FAILED;

            Log.log.error(t.getMessage(), t);
            HibernateUtil.rollbackTransaction(tx);
            throw new RuntimeException(t);
        }

        return result;
    } // executeScript

    private static Object executeSysScript(String script, String scriptName, Map<String, Object> params) throws Throwable
    {
        Object result = null;

        if (!StringUtils.isEmpty(script))
        {
            Connection conn = null;

            try
            {
                Log.start();
                
                // init timeout
                long timeoutLong = 10000;   //default 10 secs
                String timeout = (String)params.get(Constants.EXECUTE_TIMEOUT);
                if (StringUtils.isNotEmpty(timeout))
                {
                    timeoutLong = Long.parseLong(timeout);
                }
                else
                {
                    // If timeout is not present in the params, check whether global script timeout is set.
                    String globalTimeout = PropertiesUtil.getPropertyString(Constants.GLOBAL_EXECUTE_TIMEOUT);
                    if (StringUtils.isNotBlank(globalTimeout))
                    {
                        if (globalTimeout.matches("\\d+"))
                        {
                            timeoutLong = Long.parseLong(globalTimeout);
                        }
                    }
                }

                // add DB connection
                conn = null;
//                if (GroovyScript.checkOpenTransaction(script))
//                {
//                    transaction = true;
                    if (GroovyScript.checkScriptUseDB(script, true))
                    {
                        conn = ServiceHibernate.getConnection();
                    }
//                }

                Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_DB, conn);
                binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);
                binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                
                // add REQUEST if exists
                if (params.containsKey(Constants.GROOVY_BINDING_REQUEST))
                {
                    binding.setVariable(Constants.GROOVY_BINDING_REQUEST, params.get(Constants.GROOVY_BINDING_REQUEST));
                }
                else {
                    binding.setVariable(Constants.GROOVY_BINDING_REQUEST, new HashMap<String, Object>());
                }
                
                // add RESPONSE if exists
                if (params.containsKey(Constants.GROOVY_BINDING_RESPONSE))
                {
                    binding.setVariable(Constants.GROOVY_BINDING_RESPONSE, params.get(Constants.GROOVY_BINDING_RESPONSE));
                }
                else {
                    binding.setVariable(Constants.GROOVY_BINDING_RESPONSE, new HashMap<String, Object>());
                }
                
                // add OLD_PARAMS if exists
                if (params.containsKey(Constants.GROOVY_BINDING_OLD_PARAMS))
                {
                    binding.setVariable(Constants.GROOVY_BINDING_OLD_PARAMS, params.get(Constants.GROOVY_BINDING_OLD_PARAMS));
                } else {
                    // System scripts are looking for OLD_PARAMS and getting null. Need to instantiate this with an empty map.
                    binding.setVariable(Constants.GROOVY_BINDING_OLD_PARAMS, new HashMap<String, Object>());
                }
                
                result = GroovyScript.executeAsync(script, scriptName, true, binding, timeoutLong);
                Log.duration("Script duration: ", Level.DEBUG);
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
            finally
            {
                try
                {
                    if (conn != null && !conn.isClosed())
                    {
                        conn.close();
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

        return result;
    } // executeSysScript
    

}
