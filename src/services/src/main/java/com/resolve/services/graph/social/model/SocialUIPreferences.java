/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model;

public class SocialUIPreferences
{    
    private String postAutoRefreshInterval = "2";
    private Boolean userAutoRefreshFlag = false;
    private Boolean socialuserAdvanceTab = false;
    private Boolean socialuserProcessTab = false;
    private Boolean socialuserTeamTab = false;
    private Boolean socialuserForumTab = false;
    private Boolean socialuserDocumentTab = false;
    private Boolean socialuserRunbookTab = false;
    private Boolean socialuserActionTaskTab = false;
    private Boolean socialuserUserTab = false;
    private Boolean socialuserFeedTab = false;
    private Boolean socialuserShowEastpanel = false;
    private Boolean socialuserShowEastpanelWhatToDoWidget = false;
    private Boolean doSearch = false;
    
    //for viewing a runbook/doc app
    private String compFullName = null;
    private String compSysId = null;
    private String compType = null;
    
    //for viewing the actiontask app
    private String atFullName = null;
    private String atSysId = null;
    
    //str search for post
    private String searchKey = null;
    private String searchTime = null;
    private String queryType = null;
    private String resultsType = null;
    
    private Boolean hasAccessRights = true;
    
    private String permaLink = null;
    
    private String menuSets = null;
    
    public String getPostAutoRefreshInterval()
    {
        return postAutoRefreshInterval;
    }
    public void setPostAutoRefreshInterval(String postAutoRefreshInterval)
    {
        this.postAutoRefreshInterval = postAutoRefreshInterval;
    }
    public Boolean getUserAutoRefreshFlag()
    {
        return userAutoRefreshFlag;
    }
    public void setUserAutoRefreshFlag(Boolean userAutoRefreshFlag)
    {
        this.userAutoRefreshFlag = userAutoRefreshFlag;
    }
    public Boolean getSocialuserAdvanceTab()
    {
        return socialuserAdvanceTab;
    }
    public void setSocialuserAdvanceTab(Boolean socialuserAdvanceTab)
    {
        this.socialuserAdvanceTab = socialuserAdvanceTab;
    }
    public Boolean getSocialuserProcessTab()
    {
        return socialuserProcessTab;
    }
    public void setSocialuserProcessTab(Boolean socialuserProcessTab)
    {
        this.socialuserProcessTab = socialuserProcessTab;
    }
    public Boolean getSocialuserTeamTab()
    {
        return socialuserTeamTab;
    }
    public void setSocialuserTeamTab(Boolean socialuserTeamTab)
    {
        this.socialuserTeamTab = socialuserTeamTab;
    }
    public Boolean getSocialuserForumTab()
    {
        return socialuserForumTab;
    }
    public void setSocialuserForumTab(Boolean socialuserForumTab)
    {
        this.socialuserForumTab = socialuserForumTab;
    }
    public Boolean getSocialuserDocumentTab()
    {
        return socialuserDocumentTab;
    }
    public void setSocialuserDocumentTab(Boolean socialuserDocumentTab)
    {
        this.socialuserDocumentTab = socialuserDocumentTab;
    }
    public Boolean getSocialuserRunbookTab()
    {
        return socialuserRunbookTab;
    }
    public void setSocialuserRunbookTab(Boolean socialuserRunbookTab)
    {
        this.socialuserRunbookTab = socialuserRunbookTab;
    }
    public Boolean getSocialuserActionTaskTab()
    {
        return socialuserActionTaskTab;
    }
    public void setSocialuserActionTaskTab(Boolean socialuserActionTaskTab)
    {
        this.socialuserActionTaskTab = socialuserActionTaskTab;
    }
    public Boolean getSocialuserUserTab()
    {
        return socialuserUserTab;
    }
    public void setSocialuserUserTab(Boolean socialuserUserTab)
    {
        this.socialuserUserTab = socialuserUserTab;
    }
    public Boolean getSocialuserFeedTab()
    {
        return socialuserFeedTab;
    }
    public void setSocialuserFeedTab(Boolean socialuserFeedTab)
    {
        this.socialuserFeedTab = socialuserFeedTab;
    }
    public Boolean getSocialuserShowEastpanel()
    {
        return socialuserShowEastpanel;
    }
    public void setSocialuserShowEastpanel(Boolean socialuserShowEastpanel)
    {
        this.socialuserShowEastpanel = socialuserShowEastpanel;
    }
    public String getCompFullName()
    {
        return compFullName;
    }
    public void setCompFullName(String docFullName)
    {
        this.compFullName = docFullName;
    }
    public String getCompSysId()
    {
        return compSysId;
    }
    public void setCompSysId(String docSysId)
    {
        this.compSysId = docSysId;
    }
    public String getCompType()
    {
        return compType;
    }
    public void setCompType(String compType)
    {
        this.compType = compType;
    }
    public Boolean getHasAccessRights()
    {
        return hasAccessRights;
    }
    public void setHasAccessRights(Boolean hasAccessRights)
    {
        this.hasAccessRights = hasAccessRights;
    }
    public String getAtFullName()
    {
        return atFullName;
    }
    public void setAtFullName(String atFullName)
    {
        this.atFullName = atFullName;
    }
    public String getAtSysId()
    {
        return atSysId;
    }
    public void setAtSysId(String atSysId)
    {
        this.atSysId = atSysId;
    }
    public String getSearchKey()
    {
        return searchKey;
    }
    public void setSearchKey(String searchKey)
    {
        this.searchKey = searchKey;
    }
    public String getSearchTime()
    {
        return searchTime;
    }
    public void setSearchTime(String searchTime)
    {
        this.searchTime = searchTime;
    }
    
    public String getQueryType()
    {
        return this.queryType;
    }
    
    public void setQueryType(String queryType)
    {
        this.queryType = queryType;
    }
    
    public String getResultsType()
    {
        return this.resultsType;
    }
    
    public void setResultsType(String resultsType)
    {
        this.resultsType = resultsType;
    }
    
    public Boolean getSocialuserShowEastpanelWhatToDoWidget()
    {
        return socialuserShowEastpanelWhatToDoWidget;
    }
    public void setSocialuserShowEastpanelWhatToDoWidget(Boolean socialuserShowEastpanelWhatToDoWidget)
    {
        this.socialuserShowEastpanelWhatToDoWidget = socialuserShowEastpanelWhatToDoWidget;
    }
    public Boolean getDoSearch()
    {
        return doSearch;
    }
    public void setDoSearch(Boolean doSearch)
    {
        this.doSearch = doSearch;
    }
    public String getPermaLink()
    {
        return permaLink;
    }
    public void setPermaLink(String permaLink)
    {
        this.permaLink = permaLink;
    }
    public String getMenuSets()
    {
        return menuSets;
    }
    public void setMenuSets(String menuSets)
    {
        this.menuSets = menuSets;
    }
}
