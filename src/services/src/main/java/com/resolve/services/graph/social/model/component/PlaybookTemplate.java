/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;


/**
 * PlaybookTemplate is only a publisher  
 * 
 * @author hemant.phanasgaonkar
 *
 */
public class PlaybookTemplate extends RSComponent implements RSPublisher
{
    private static final long serialVersionUID = -8379594483861554580L;

    public PlaybookTemplate()
    {
        super(NodeType.PLAYBOOK_TEMPLATE);
    }
    
    public PlaybookTemplate(String sysId, String name) 
    {
        super(NodeType.PLAYBOOK_TEMPLATE);
        setSys_id(sysId);
        setName(name);
    }

    
    public PlaybookTemplate(String name)
    {
        super(NodeType.PLAYBOOK_TEMPLATE);
        setName(name);
    }
    
    public PlaybookTemplate(ResolveNodeVO node) throws Exception
    {
        super(node);
    }
}
