/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util.vo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
import static com.resolve.services.constants.ConstantValues.*;

/**
 * This is a value object to hold data that will be displayed on the UI
 * 
 * @author jeet.marwah
 * 
 */
public class ActionTaskInfoVO
{
    private String sysId;
	private String namespace;
	private String name;
	private String actionTaskFullName;
	private String viewLink;
	private String modifiedBy;
	private Date modifiedOn;

	private String roleName;//used for homepage list display
	
	private  Map<String, String> rowInfo = null;//used for the ResultMacro
	private String key;                  // full key used by ResultAjax - name#namespace|wiki::id
	private String keyNameNamespace;     // name#namespace
	private String keyName;              // name only

	
	public Map<String, String> getRenderedRowInfo(String atActionWiki, String atDescription)
	{
	    Map<String, String> renderedRowInfo = new HashMap<String, String>();
	    
        if(this.rowInfo != null)
        {
            rowInfo.put("LINK:RESULT", URL_RESOLVE_SERVICE+URL_RESULT_TASK_DETAIL+"?"+Constants.HTTP_REQUEST_TASKRESULTID+"="+this.sysId); 
            
            if(StringUtils.isNotEmpty(atDescription))
            {
                rowInfo.put("DESCRIPTION", atDescription);
            }
            
            renderedRowInfo.putAll(rowInfo);
            
            if(StringUtils.isNotEmpty(atActionWiki))
            {
                // check if wiki = description-wiki|result-wiki
                int pos = atActionWiki.indexOf('|');
                if (pos >= 0)
                {
                    String descWiki = atActionWiki.substring(0, pos).trim();
                    String resultWiki = atActionWiki.substring(pos+1, atActionWiki.length()).trim();
                    
                    if(StringUtils.isNotEmpty(descWiki))
                    {
                        renderedRowInfo.put("LINK:DESCRIPTION", URL_RESOLVE_SERVICE+URL_VIEW +"/"+ descWiki.replace('.', '/'));
                    }
                    
                    if(StringUtils.isNotEmpty(resultWiki))
                    {
                        renderedRowInfo.put("LINK:RESULT", URL_RESOLVE_SERVICE+URL_VIEW +"/"+ resultWiki.replace('.', '/'));
                    }
                }
                else
                {
                    renderedRowInfo.put("LINK:DESCRIPTION", URL_RESOLVE_SERVICE+URL_VIEW +"/"+ atActionWiki.replace('.', '/'));
                }
            }
        }
	    
	    return renderedRowInfo;
	}
	
	public String getNamespace()
	{
		return namespace;
	}

	public void setNamespace(String namespace)
	{
		this.namespace = namespace;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getActionTaskFullName()
	{
		return actionTaskFullName;
	}

	public void setActionTaskFullName(String actionTaskFullName)
	{
		this.actionTaskFullName = actionTaskFullName;
	}

	public String getViewLink()
	{
		return viewLink;
	}

	public void setViewLink(String viewLink)
	{
		this.viewLink = viewLink;
	}

	public String getModifiedBy()
	{
		if(modifiedBy == null)
			modifiedBy = "";
		
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}
	public String getRoleName()
	{
		return roleName;
	}

	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}

    public Map<String, String> getRowInfo()
    {
        return rowInfo;
    }

    public void setRowInfo(Map<String, String> rowInfo)
    {
        this.rowInfo = rowInfo;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getKeyNameNamespace()
    {
        return keyNameNamespace;
    }

    public void setKeyNameNamespace(String keyNameNamespace)
    {
        this.keyNameNamespace = keyNameNamespace;
    }

    public String getKeyName()
    {
        return keyName;
    }

    public void setKeyName(String keyName)
    {
        this.keyName = keyName;
    }

    public String getSysId()
    {
        return sysId;
    }

    public void setSysId(String sysId)
    {
        this.sysId = sysId;
    }
    

}
