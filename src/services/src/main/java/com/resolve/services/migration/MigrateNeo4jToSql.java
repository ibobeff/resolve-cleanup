/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.neo4j.graphdb.Node;

import com.resolve.services.ServiceTag;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.migration.catalog.MigrateCatalogs;
import com.resolve.services.migration.entities.MigrateEntities;
import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

/**
 * Utility to migration all the Neo4j data to Sql. 
 * 
 * Possible create a separate jar file to export and make it a utility
 * 
 * @author jeet.marwah
 *
 */
public class MigrateNeo4jToSql
{
    public MigrateNeo4jToSql() {}
    
    public static void migrate()
    {
        long startTime = System.currentTimeMillis();
        long localStartTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        try
        {
            //wiki, user, process, forums, etc in the graph sql tables - THIS WE NEED TO DO WHEN TOMCAT COMES UP TOO
            //**  I think we could move this in the Main.init of rsview but still deciding which way to go and is better
            pCompSysIds.addAll(migrateSocialComponents());            
            Log.log.debug(">>>>>>>>>>>> Migrated " + pCompSysIds.size() + " Components (secs):" + ((System.currentTimeMillis() - localStartTime) / 1000d));
            localStartTime = System.currentTimeMillis();
            
            //check if the migration was already done before
            if(doGraphMigration())
            {
                //initialize the Neo4j DB - if this fails that means Neo4j may not exist which is the case in brand new install
                GraphDBManager neo4jDB = GraphDBManager.getInstance();
                Log.log.debug(">>>>>>>>>>>> Graphdb Initialized (secs):" + ((System.currentTimeMillis() - localStartTime) / 1000d));
                localStartTime = System.currentTimeMillis();
                
                //worksheet - as that is in graph
                pCompSysIds.addAll(migrateWorksheets());
                Log.log.debug(">>>>>>>>>>>> Migrated Worksheets (secs):" + ((System.currentTimeMillis() - localStartTime) / 1000d));
                localStartTime = System.currentTimeMillis();
                
                //tags - as that is in graph
                pCompSysIds.addAll(migrateTags());
                Log.log.debug(">>>>>>>>>>>> Migrated Tags (secs):" + ((System.currentTimeMillis() - localStartTime) / 1000d));
                localStartTime = System.currentTimeMillis();
                
                pCompSysIds.addAll(migrateCatalog());
                Log.log.debug(">>>>>>>>>>>> Migrated Catalog (secs):" + ((System.currentTimeMillis() - localStartTime) / 1000d));
                localStartTime = System.currentTimeMillis();
                
                //migrate and add the relationships between the entities 
                migrateSocialRelationships(pCompSysIds);
                Log.log.debug(">>>>>>>>>>>> Migrated Social Relationships (secs):" + ((System.currentTimeMillis() - localStartTime) / 1000d));
                
                //TODO
                //migrate the notifications if there is no property file for that ...I think property file will be a better idea, then we don't have to do this one
                migrateNotificationDefinitions();
                
                neo4jDB.shutdown();
                Log.log.debug(">>>>>>>>>>>> TOTAL TIME TO MIGRATE (secs):" + ((System.currentTimeMillis() - startTime) / 1000d));
            }
            else
            {
                Log.log.debug("Neo4j MIgration is Already Done.");
            }
        }
        catch (Exception e)
        {
           Log.log.error("Error in Neo4j Migration", e);
        }
    }
    
    private static Collection<String> migrateSocialComponents()
    {
        //this is single threaded - will be slower
//        MigrateSocialEntities.syncEntities();
        

        //this is Multithreaded migration - better performance 
        return MigrateEntities.startMigration();
    }
    
    private static Collection<String> migrateWorksheets()
    {
        Log.log.debug("*** START UPDATE WORKSHEET TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pSysIds = new ArrayList<String>();

        try
        {
            Set<Node> worksheetNeo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.WORKSHEET);
            
            Log.log.debug("Total # of Worksheets : " + worksheetNeo4jNodes.size());
            for(Node node : worksheetNeo4jNodes)
            {
                //Log.log.trace("Processed " + count++ + " Worksheets");                
                Log.log.trace("Display Name:" + node.getProperty(GraphDBManager.DISPLAYNAME));
                ResolveNodeVO wsNode = prepareWorksheet(node);

                //persist the WS
                try
                {
                    ResolveNodeVO pnode = GraphUtil.persistNode(wsNode, "admin");
                    
                    if(pnode != null)
                    {
                        pSysIds.add(pnode.getSys_id());
                    }
                }
                catch (Exception e)
                {
                   Log.log.error("Error persisting node:" + wsNode.getUCompName() + ", Msg:" + e.getMessage());
                }
            }
            
            Log.log.debug("Total # of Worksheets Migrated: " + pSysIds.size());
//            Log.log.debug("============ PROCESS ----------:");
//            worksheetNeo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.PROCESSES);
//            for(Node node : worksheetNeo4jNodes)
//            {
//                Log.log.debug("Display Name:" + node.getProperty(DISPLAYNAME));
//            }
//            
//            Log.log.debug("============ TEAM ----------:");
//            worksheetNeo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.TEAMS);
//            for(Node node : worksheetNeo4jNodes)
//            {
//                Log.log.debug("Display Name:" + node.getProperty(DISPLAYNAME));
//            }
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating worksheet.", e);
        } 
        
        Log.log.debug("*** END OF UPDATE WORKSHEET TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pSysIds;
    }
    
    private static Collection<String> migrateTags()
    {
        Log.log.debug("*** START UPDATE TAGS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pSysIds = new ArrayList<String>();

        try
        {
            Set<Node> tagsNeo4jNodes = GraphDBManager.getInstance().findAllNodesFor(SocialRelationshipTypes.ResolveTag);
            
            Log.log.debug("Total # of ResolveTags : " + tagsNeo4jNodes.size());
            for(Node node : tagsNeo4jNodes)
            {
                if(node.hasProperty(GraphDBManager.DISPLAYNAME))
                {
                    String tagName = (String) node.getProperty(GraphDBManager.DISPLAYNAME);
                    String desc = (String) node.getProperty("description");
                    //Log.log.debug("Processed " + count++ + " Tags");                
                    Log.log.trace("Display Name:" + tagName);
                    
                    try
                    {
                        ResolveTagVO tag = new ResolveTagVO();
                        tag.setUName(tagName);
                        tag.setUDescription(desc);
                        
                        ResolveTagVO ptag = ServiceTag.saveTag(tag, "system");
                        
                        if(ptag != null)
                        {
                            pSysIds.add(ptag.getSys_id());
                        }
                    }
                    catch(Throwable t)
                    {
                        Log.log.error("Error migrating tag:" + tagName, t);
                    }
                }
            }
            
            Log.log.debug("Total # of ResolveTags Migrated: " + pSysIds.size());
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating Tags.", e);
        } 
        
        Log.log.debug("*** END OF UPDATE TAGS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pSysIds;
    }
    
    private static Collection<String> migrateCatalog()
    {
        return MigrateCatalogs.migrate();
    }
    
    private static void migrateSocialRelationships(Collection<String> pCompSysIds) throws Exception
    {
        MigrateSocialRelationships.syncRelationships(pCompSysIds);
    }
    
    private static void migrateNotificationDefinitions()
    {
        MigrateNotifications.migrate();
    }

    private static ResolveNodeVO prepareWorksheet(Node node)
    {
        ResolveNodeVO nodeVO = new ResolveNodeVO();
        nodeVO.setUCompName((String) node.getProperty(GraphDBManager.DISPLAYNAME));
        nodeVO.setUCompSysId((String) node.getProperty(GraphDBManager.SYS_ID));
        nodeVO.setUType(NodeType.WORKSHEET);
        nodeVO.setUMarkDeleted(false);
        nodeVO.setULock(false);
        nodeVO.setUPinned(false);
        nodeVO.setUReadRoles("resolve_process resolve_dev admin postmrole");
        
        return nodeVO;
    }
    
    private static boolean doGraphMigration()
    {
        return !GeneralHibernateUtil.modelExists("ResolveEdge", "system");
    }

}
