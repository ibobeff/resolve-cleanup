/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.NonUniqueObjectException;

import com.google.gson.JsonObject;
import com.resolve.dto.SectionType;
import com.resolve.html.HtmlUtil;
import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaControl;
import com.resolve.persistence.model.MetaControlItem;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaFormAction;
import com.resolve.persistence.model.MetaFormTab;
import com.resolve.persistence.model.MetaFormTabField;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.MetaSource;
import com.resolve.persistence.model.MetaxFieldDependency;
import com.resolve.persistence.model.MetaxFormViewPanel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.customform.CustomFormIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.services.vo.form.DataSource;
import com.resolve.services.vo.form.RsButtonAction;
import com.resolve.services.vo.form.RsButtonPanelDTO;
import com.resolve.services.vo.form.RsColumnDTO;
import com.resolve.services.vo.form.RsCustomTableDTO;
import com.resolve.services.vo.form.RsFieldDependency;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsPanelDTO;
import com.resolve.services.vo.form.RsTabDTO;
import com.resolve.services.vo.form.RsUIButton;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

public class ExtSaveMetaFormView
{
    private RsFormDTO viewx = null;
    private String userId = null;
    
    private MetaFormView dbView = null;
    private CustomTable customTable = null;
    
    //reference is kept global as this is used for all the SOURCE fields also
    private MetaAccessRights viewAccessRights = null;
    
    // this is a list of sysid of INPUT types that are valid, anything that is not in this list will be deleted as it is not referenced on the form
    private List<String> validMetaSourceSysIds = new ArrayList<String>();

    private String referenceTableNames = null;
    
    public ExtSaveMetaFormView(RsFormDTO view, String userId)
    {
        this.viewx = view;
        this.userId = userId;

    }// ExtSaveMetaFormView
    
    //tablenames can be as big as 25 chars. So reducing it for the column name which is a restriction for Oracle
    public static String prepareRefColumnNameBasedOnTableName(String customTableName)
    {
        String columnName = "u_" + customTableName.toLowerCase() + "_sys_id";
        
        if(customTableName.length() > 15)
        {
            columnName = "u_" + customTableName.toLowerCase().substring(0, 15) + "_sys_id";
        }
        
        Log.log.debug("prepareRefColumnNameBasedOnTableName[" + customTableName + 
                      "] is [" + columnName + "]");
        return columnName;
    }
    
    public RsFormDTO save() throws Exception
    {
        String customTableName = viewx.getTableName();

        //validate the data
        Log.log.debug("ExtSaveMetaFormView");
        long t = System.currentTimeMillis();
        long tStart = t;
        viewx.validate();
        Log.log.debug("viewx.validate() took " + (System.currentTimeMillis() - t) + "ms");
        
        try
        {
        	t = System.currentTimeMillis();
            //pre operations - like creating the table from form, massage the data, create dependency tables
        	preSaveOperation();
        	Log.log.debug("preSaveOperation() took " + (System.currentTimeMillis() - t) + "ms");
            //start the transaction
          HibernateProxy.setCurrentUser(userId);
            HibernateProxy.execute(() -> {
            	long now = System.currentTimeMillis();
	            //load the custom table if related to this form
	            if(StringUtils.isNotBlank(customTableName))
	            {
	            	now = System.currentTimeMillis();
	                customTable = CustomTableUtil.findCustomTableAndLoadAllReferences(null, customTableName, userId);
	                Log.log.debug("CustomTableUtil.findCustomTableAndLoadAllReferences("+ customTableName + ") took " + (System.currentTimeMillis() - now) + "ms");
	            }
	
	            // convert the UI model to DB model
	            now = System.currentTimeMillis();
	            convertViewToMetaFormView();
	            Log.log.debug("convertViewToMetaFormView() took " + (System.currentTimeMillis() - now) + "ms");

            });
            Log.log.debug("commitTransaction() took " + (System.currentTimeMillis() - t) + "ms");
            
            //post save operations
            t = System.currentTimeMillis();
            postSaveOperations();
            Log.log.debug("postSaveOperations() took " + (System.currentTimeMillis() - t) + "ms");
            
            // get/refresh the view from db
            t = System.currentTimeMillis();
            viewx = ServiceHibernate.getMetaFormView(dbView.getSys_id(), dbView.getUViewName(), null, userId, RightTypeEnum.admin, null, false);
            Log.log.debug("ServiceHibernate.getMetaFormView(" + dbView.getSys_id() + ", " + dbView.getUViewName() + ") after postSaveOperations() took " + (System.currentTimeMillis() - t) + "ms");
            Log.log.info("Save " + viewx.getFormName() + " took " + (System.currentTimeMillis() - tStart) + "ms");
            populateFormJsonTreeAndChecksum(viewx);
        }
        catch (Throwable thr)
        {
            Log.log.error(thr.getMessage(), thr);
            throw new Exception(thr);
        }
        
        return viewx;
    }
    
    private void preSaveOperation()  throws Exception
    {
        // create the FileUpload table if the FileUploadwidget is there on the
        // form
        long t = System.currentTimeMillis();
        doWidgetOperations();
        Log.log.debug("doWidgetOperations() took " + (System.currentTimeMillis() - t) + "ms");

        // create the custom table
        t = System.currentTimeMillis();
        createCustomTableFromForm();
        Log.log.debug("createCustomTableFromForm() took " + (System.currentTimeMillis() - t) + "ms");
    }
    
    private void convertViewToMetaFormView() throws Exception
    {
        boolean modified = false;
        
        if (viewx != null)
        {
            String id = viewx.getId();
            if (StringUtils.isNotEmpty(id))
            {
                // update
                dbView = CustomFormUtil.findMetaFormView(id, null, userId);
                
                if (dbView == null)
                {
                    throw new Exception(viewx.getViewName() + " with id " + id + " not found in database.");
                }
                
                if ((StringUtils.isNotBlank(viewx.getFormName()) && !viewx.getFormName().equals(dbView.getUFormName())) ||
                    (StringUtils.isBlank(viewx.getFormName()) && StringUtils.isNotBlank(dbView.getUFormName())))
                {
                    modified = true;
                }
                
                if (!modified && !viewx.isWizard().equals(dbView.getUIsWizard()))
                {
                    modified = true;
                }
                
                if (!modified && ((StringUtils.isNotBlank(viewx.getViewName()) && !viewx.getViewName().equals(dbView.getUViewName())) ||
                                  (StringUtils.isBlank(viewx.getViewName()) && StringUtils.isNotBlank(dbView.getUViewName()))))
                {
                    modified = true;
                }
                
                if (!modified && ((StringUtils.isNotBlank(viewx.getDisplayName()) && !viewx.getDisplayName().equals(dbView.getUDisplayName())) ||
                                  (StringUtils.isBlank(viewx.getDisplayName()) && StringUtils.isNotBlank(dbView.getUDisplayName()))))
                {
                    modified = true;
                }
                
                if (!modified && ((StringUtils.isNotBlank(viewx.getWindowTitle()) && !viewx.getWindowTitle().equals(dbView.getUWindowTitle())) ||
                                  (StringUtils.isBlank(viewx.getWindowTitle()) && StringUtils.isNotBlank(dbView.getUWindowTitle()))))
                {
                    modified = true;
                }
                
                if (!modified && ((StringUtils.isNotBlank(viewx.getWikiName()) && !viewx.getWikiName().equals(dbView.getUWikiName())) ||
                                  (StringUtils.isBlank(viewx.getWikiName()) && StringUtils.isNotBlank(dbView.getUWikiName()))))
                {
                    modified = true;
                }
                
                if (!modified && ((StringUtils.isNotBlank(viewx.getTableName()) && !viewx.getTableName().equals(dbView.getUTableName())) ||
                                  (StringUtils.isBlank(viewx.getTableName()) && StringUtils.isNotBlank(dbView.getUTableName()))))
                {
                    modified = true;
                }
                
                if (!modified && ((StringUtils.isNotBlank(viewx.getTableDisplayName()) && !viewx.getTableDisplayName().equals(dbView.getUTableDisplayName())) ||
                                  (StringUtils.isBlank(viewx.getTableDisplayName()) && StringUtils.isNotBlank(dbView.getUTableDisplayName()))))
                {
                    modified = true;
                }
            }
            else
            {
                String viewName = viewx.getViewName();
                dbView = CustomFormUtil.findMetaFormView(null, viewName, userId);
                if(dbView != null)
                {
                    throw new Exception("View with " + viewName + " already exist.");
                } 
                
                // insert - create a new view
                dbView = new MetaFormView();
                modified = true;
            }

            dbView.setUFormName(viewx.getFormName());
            dbView.setUIsWizard(viewx.isWizard());
            dbView.setUViewName(viewx.getViewName());
            dbView.setUDisplayName(viewx.getDisplayName());
            dbView.setUWindowTitle(viewx.getWindowTitle());
            dbView.setUWikiName(viewx.getWikiName());
            dbView.setUTableName(viewx.getTableName());
            dbView.setUTableDisplayName(viewx.getTableDisplayName());
            
            if (modified)
            {
                dbView.setSysUpdatedOn(GMTDate.getDate());
                
                if (Log.log.isDebugEnabled())
                {
                    Log.log.debug((dbView.getSys_id() != null ? dbView.getSys_id() + " " : "new ") + "meta_form_view " + 
                                  (dbView.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
                }
                
                
                //HibernateUtil.getSessionFactory().getCurrentSession().clear();
                try
                {
                    HibernateUtil.getDAOFactory().getMetaFormViewDAO().persist(dbView);
                }
                catch(NonUniqueObjectException nuoe)
                {
                    Log.log.debug("Non uniqueue exception while persisting metax_form_view_panel for " + dbView.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                    HibernateUtil.getCurrentSession().merge(dbView);
                }
                
                if (Log.log.isDebugEnabled())
                {
                    Log.log.debug("meta_form_view.sys_id = " + dbView.getSys_id());
                }
                //modified = false;
            }

            // access rights for the form
            viewAccessRights = convertViewToMetaAccessRights(dbView, viewx.getViewRoles(), viewx.getEditRoles(), viewx.getAdminRoles(), dbView.getMetaAccessRights());
            /*
            if (!modified && ((dbView.getMetaAccessRights() == null && viewAccessRights != null) ||
                              (dbView.getMetaAccessRights() != null && viewAccessRights == null) ||
                              ((dbView.getMetaAccessRights() != null && viewAccessRights != null) &&
                               !(dbView.getMetaAccessRights().getSys_id().equalsIgnoreCase(viewAccessRights.getSys_id())))))
            {
                modified = true;
            }
            */
            dbView.setMetaAccessRights(viewAccessRights);
            //HibernateUtil.getSessionFactory().getCurrentSession().merge(dbView);

            // for the button panel
            
            MetaControl viewMetaCtrl = convertViewToMetaControl(viewx.getButtonPanel(), dbView.getMetaControl());
            /*
            if (!modified && ((dbView.getMetaControl() == null && viewMetaCtrl != null) ||
                              (dbView.getMetaControl() != null && viewMetaCtrl == null) ||
                              ((dbView.getMetaControl() != null && viewMetaCtrl != null) &&
                               !(dbView.getMetaControl().getSys_id().equalsIgnoreCase(viewMetaCtrl.getSys_id())))))
            {
                modified = true;
            }
            */
            dbView.setMetaControl(viewMetaCtrl);
            HibernateUtil.getCurrentSession().merge(dbView);

            // set the additional properties of the form
//            dbView.setMetaFormViewProperties(convertViewToMetaFormViewProperties(viewx.getProperties(), dbView.getMetaFormViewProperties()));

            // get the list of tabs for this form
            List<MetaxFormViewPanel> formViewPanels = convertViewToMetaxFormViewPanels(dbView, viewx.getPanels(), dbView.getMetaxFormViewPanels());
            /*
            if (!modified && (((dbView.getMetaxFormViewPanels() == null || dbView.getMetaxFormViewPanels().isEmpty()) && formViewPanels != null && !formViewPanels.isEmpty()) ||
                              (dbView.getMetaxFormViewPanels() != null && !dbView.getMetaxFormViewPanels().isEmpty() && (formViewPanels == null || formViewPanels.isEmpty())) ||
                              ((dbView.getMetaxFormViewPanels() != null && !dbView.getMetaxFormViewPanels().isEmpty() && formViewPanels != null && !formViewPanels.isEmpty()) &&
                               (dbView.getMetaxFormViewPanels().size() != formViewPanels.size()))))
            {
                modified = true;
            }
            */
            dbView.setMetaxFormViewPanels(formViewPanels);
            HibernateUtil.getCurrentSession().merge(dbView);
            
            // cleanup
            cleanup();

            // save again with all the references
            /*
            if (modified)
            {
                if (Log.log.isDebugEnabled() && StringUtils.isNotEmpty(id))
                {
                    Log.log.debug(dbView.getSys_id() + " meta_form_view modified, persisting!!!");
                }
                HibernateUtil.getSessionFactory().getCurrentSession().clear();
                HibernateUtil.getDAOFactory().getMetaFormViewDAO().persist(dbView);
            }
            */
        }// end of if
    }// convertUiViewToDbModel

    private MetaAccessRights convertViewToMetaAccessRights(MetaFormView parentObject, String viewRights, String editRights, String adminRights, MetaAccessRights dbMetaAccess)
    {
        boolean modified = false;
        
        if (dbMetaAccess == null)
        {
            dbMetaAccess = new MetaAccessRights();
            modified = true;
        }
        dbMetaAccess.setUResourceId(parentObject.getSys_id());
        dbMetaAccess.setUResourceType(MetaFormView.RESOURCE_TYPE);
        dbMetaAccess.setUResourceName(parentObject.getUViewName());
        
        if (!modified && ((StringUtils.isNotBlank(viewRights) && viewRights.equals(dbMetaAccess.getUReadAccess())) || 
                          (StringUtils.isBlank(viewRights) && StringUtils.isNotBlank(dbMetaAccess.getUReadAccess()))))
        {
            modified = true;
        }
        
        dbMetaAccess.setUReadAccess(viewRights);
        
        if (!modified && ((StringUtils.isNotBlank(editRights) && editRights.equals(dbMetaAccess.getUWriteAccess())) || 
                          (StringUtils.isBlank(editRights) && StringUtils.isNotBlank(dbMetaAccess.getUWriteAccess()))))
        {
            modified = true;
        }
        
        dbMetaAccess.setUWriteAccess(editRights);
        
        if (!modified && ((StringUtils.isNotBlank(adminRights) && adminRights.equals(dbMetaAccess.getUAdminAccess())) || 
                          (StringUtils.isBlank(adminRights) && StringUtils.isNotBlank(dbMetaAccess.getUAdminAccess()))))
        {
            modified = true;
        }
        
        dbMetaAccess.setUAdminAccess(adminRights);
        // dbMetaAccess.setUExecuteAccess("admin");

        if (modified)
        {
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug((dbMetaAccess.getSys_id() != null ? dbMetaAccess.getSys_id() + " " : "new ") + "meta_access_rights " + 
                              (dbMetaAccess.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
            }
            
            //HibernateUtil.getSessionFactory().getCurrentSession().clear();
            try
            {
                HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(dbMetaAccess);
            }
            catch(NonUniqueObjectException nuoe)
            {
                Log.log.debug("Non uniqueue exception while persisting meta_access_rights for " + dbMetaAccess.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                HibernateUtil.getCurrentSession().merge(dbMetaAccess);
            }
        }

        return dbMetaAccess;
    }// convertViewToMetaAccessRights
    
    private MetaControl convertViewToMetaControl(RsButtonPanelDTO viewObj, MetaControl dbObj)
    {
        boolean modified = false;
        
        if (dbObj == null)
        {
            dbObj = new MetaControl();
            modified = true;
        }

        // this is for convenience (huh!) 
        
        if (!modified && ((dbObj.getUName() == null && dbView.getUViewName() != null) || 
                          (dbObj.getUName() != null && dbView.getUViewName() == null) ||
                          ((dbObj.getUName() != null && dbView.getUViewName() != null) && 
                           !(dbObj.getUName().equals(dbView.getUViewName())))))
        {
            modified = true;
        }
                          
        dbObj.setUName(dbView.getUViewName());// viewObj.getName());
        
        if (!modified && ((dbObj.getUDisplayName() == null && dbView.getUDisplayName() != null) || 
                          (dbObj.getUDisplayName() != null && dbView.getUDisplayName() == null) ||
                          ((dbObj.getUDisplayName() != null && dbView.getUDisplayName() != null) && 
                           !(dbObj.getUDisplayName().equals(dbView.getUDisplayName())))))
        {
            modified = true;
        }
        
        dbObj.setUDisplayName(dbView.getUDisplayName());// viewObj.getDisplayName());

        // persist it right away
        
        if (modified)
        {
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug((dbObj.getSys_id() != null ? dbObj.getSys_id() + " " : "new ") + "meta_control " + 
                              (dbObj.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
            }
            
            //HibernateUtil.getSessionFactory().getCurrentSession().clear();
            try
            {
                HibernateUtil.getDAOFactory().getMetaControlDAO().persist(dbObj);
            }
            catch(NonUniqueObjectException nuoe)
            {
                Log.log.debug("Non uniqueue exception while persisting meta_control for " + dbObj.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                HibernateUtil.getCurrentSession().merge(dbObj);
            }
            
            modified = false;
        }

        // add the items
        
        Collection<MetaControlItem> metaControls = convertViewToMetaControlItems(dbObj, viewObj.getControls(), dbObj.getMetaControlItems());
        /*
        if (!modified && (((dbObj.getMetaControlItems() == null || dbObj.getMetaControlItems().isEmpty()) && metaControls != null && !metaControls.isEmpty()) ||
                          (dbObj.getMetaControlItems() != null && !dbObj.getMetaControlItems().isEmpty() && (metaControls == null || metaControls.isEmpty())) ||
                          ((dbObj.getMetaControlItems() != null && !dbObj.getMetaControlItems().isEmpty() && metaControls != null && !metaControls.isEmpty()) &&
                           (dbObj.getMetaControlItems().size() != metaControls.size()))))
        {
            modified = true;
        }*/
        
        dbObj.setMetaControlItems(metaControls);
        /*
        if (modified)
        {
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug((dbObj.getSys_id() != null ? dbObj.getSys_id() + " " : "new ") + "meta_control " + 
                                (dbObj.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
            }
            HibernateUtil.getSessionFactory().getCurrentSession().clear();
            HibernateUtil.getDAOFactory().getMetaControlDAO().persist(dbObj);
        }
        */
        return dbObj;

    }// convertMetaControlFromUIToDbModel

    private Collection<MetaControlItem> convertViewToMetaControlItems(MetaControl parentMetaControl, Collection<RsUIButton> viewObjs, Collection<MetaControlItem> dbObjs)
    {
        Collection<MetaControlItem> newDbObjs = new ArrayList<MetaControlItem>();

        if (viewObjs != null)
        {
            //int position = 1;
            for (RsUIButton item : viewObjs)
            {
                boolean modified = false;
                MetaControlItem dbItem = null;
                String id = item.getId();
                if (StringUtils.isNotEmpty(id))
                {
                    dbItem = findMetaControlItem(id, dbObjs);
                }

                if (dbItem == null)
                {
                    dbItem = new MetaControlItem();
                    modified = true;
                }

                if (!modified && ((StringUtils.isNotBlank(item.getName()) && !item.getName().equals(dbItem.getUName())) ||
                                  (StringUtils.isBlank(item.getName()) && StringUtils.isNotBlank(dbItem.getUName()))))
                {
                    modified = true;
                }
                
                dbItem.setUName(item.getName());
                
                if (!modified && ((StringUtils.isNotBlank(item.getDisplayName()) && !item.getDisplayName().equals(dbItem.getUDisplayName())) ||
                                  (StringUtils.isBlank(item.getDisplayName()) && StringUtils.isNotBlank(dbItem.getUDisplayName()))))
                {
                    modified = true;
                }
                
                dbItem.setUDisplayName(item.getDisplayName());
                
                if (!modified && ((StringUtils.isNotBlank(item.getType()) && !item.getType().equals(dbItem.getUType())) ||
                                  (StringUtils.isBlank(item.getType()) && !dbItem.getUType().equals("button"))))
                {
                    modified = true;
                }
                
                dbItem.setUType(StringUtils.isNotEmpty(item.getType()) ? item.getType() : "button");
                
                boolean dbItemCustomTableDisplay = dbItem.getUCustomTableDisplay() != null ? dbItem.getUCustomTableDisplay().booleanValue() : false;
                if (!modified && (item.isCustomTableDisplay() != dbItemCustomTableDisplay))
                {
                    modified = true;
                }
                
                dbItem.setUCustomTableDisplay(item.isCustomTableDisplay());
                // dbItem.setUParam(item.getParam());
                
                if (!modified && dbItem.getUSequence().intValue() != item.getOrderNumber())
                {
                    if (Log.log.isDebugEnabled() && StringUtils.isNotEmpty(id))
                    {
                        Log.log.debug(dbItem.getSys_id() + " meta_control_item.u_sequence " + dbItem.getUSequence().intValue() + " does not match " + item.getOrderNumber() + "!!!");
                    }
                    modified = true;
                }
                
                dbItem.setUSequence(item.getOrderNumber());
                dbItem.setMetaControl(parentMetaControl);
                
                JsonObject propertiesJSON = new JsonObject();
                propertiesJSON.addProperty("backgroundColor", item.getBackgroundColor());
                propertiesJSON.addProperty("fontColor", item.getFontColor());
                propertiesJSON.addProperty("font", item.getFont());
                propertiesJSON.addProperty("fontSize", item.getFontSize());
                dbItem.setUPropertiesJSON(propertiesJSON.toString());

                // persist it right away
                if (modified)
                {
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ") + " meta_control_item " +
                                       (dbItem.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
                    }
                    
                    //HibernateUtil.getSessionFactory().getCurrentSession().clear();
                    try
                    {
                        HibernateUtil.getDAOFactory().getMetaControlItemDAO().persist(dbItem);
                    }
                    catch(NonUniqueObjectException nuoe)
                    {
                        Log.log.debug("Non uniqueue exception while persisting meta_control_item for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                        HibernateUtil.getCurrentSession().merge(dbItem);
                    }
                }

                // add the form actions
                dbItem.setMetaFormActions(convertViewToMetaFormActions(dbItem, item.getActions(), dbItem.getMetaFormActions()));
                
                // add the dependencies
                dbItem.setMetaxFieldDependencys(convertViewToMetaxFieldDependency(dbItem, item.getDependencies(), dbItem.getMetaxFieldDependencys()));
                
                newDbObjs.add(dbItem);
                //position++;

            }// end of for

            //filterOutControlItemsToDelete(newDbObjs, dbObjs);

        }
        else
        {
            // make sure that its null then
            newDbObjs = null;
        }

        filterOutControlItemsToDelete(newDbObjs, dbObjs);
        
        return newDbObjs;
    }// convertViewToMetaControlItems
    
    private Collection<MetaFormAction> convertViewToMetaFormActions(MetaControlItem parentMetaControlItem, Collection<RsButtonAction> viewObjs, Collection<MetaFormAction> dbObjs)
    {
        Collection<MetaFormAction> newDbObjs = new ArrayList<MetaFormAction>();

        if (viewObjs != null)
        {
            //int position = 1;
            for (RsButtonAction item : viewObjs)
            {
                boolean modified = false;
                MetaFormAction dbItem = null;
                String id = item.getId();
                if (StringUtils.isNotEmpty(id))
                {
                    dbItem = findMetaFormAction(id, dbObjs);
                }

                if (dbItem == null)
                {
                    dbItem = new MetaFormAction();
                    modified = true;
                }

                if (!modified && ((StringUtils.isNotBlank(item.getOperation()) && !item.getOperation().equals(dbItem.getUOperation())) ||
                                  (StringUtils.isBlank(item.getOperation()) && StringUtils.isNotBlank(dbItem.getUOperation()))))
                {
                    modified = true;
                }
                
                dbItem.setUOperation(item.getOperation());
                
                if (!modified && ((StringUtils.isNotBlank(item.getCrudAction()) && !item.getCrudAction().equals(dbItem.getUCrudAction())) ||
                                  (StringUtils.isBlank(item.getCrudAction()) && StringUtils.isNotBlank(dbItem.getUCrudAction()))))
                {
                    modified = true;
                }
                
                dbItem.setUCrudAction(item.getCrudAction());
                
                if (!modified && ((StringUtils.isNotBlank(item.getRunbook()) && !item.getRunbook().equals(dbItem.getURunbookName())) ||
                                  (StringUtils.isBlank(item.getRunbook()) && StringUtils.isNotBlank(dbItem.getURunbookName()))))
                {
                    modified = true;
                }
                
                dbItem.setURunbookName(item.getRunbook());
                
                if (!modified && ((StringUtils.isNotBlank(item.getScript()) && !item.getScript().equals(dbItem.getUScriptName())) ||
                                  (StringUtils.isBlank(item.getScript()) && StringUtils.isNotBlank(dbItem.getUScriptName()))))
                {
                    modified = true;
                }
                
                dbItem.setUScriptName(item.getScript());

                if (!modified && ((StringUtils.isNotBlank(item.getActionTask()) && !item.getActionTask().equals(dbItem.getUActionTaskName())) ||
                                  (StringUtils.isBlank(item.getActionTask()) && StringUtils.isNotBlank(dbItem.getUActionTaskName()))))
                {
                    modified = true;
                }

                dbItem.setUActionTaskName(item.getActionTask());
                
                if (!modified && ((StringUtils.isNotBlank(item.getAdditionalParam()) && !item.getAdditionalParam().equals(dbItem.getUAdditionalParams())) ||
                                  (StringUtils.isBlank(item.getAdditionalParam()) && StringUtils.isNotBlank(dbItem.getUAdditionalParams()))))
                {
                    modified = true;
                }
                
                dbItem.setUAdditionalParams(item.getAdditionalParam());
                
                if (!modified && ((dbItem.getUSequence() == null && item.getOrderNumber() != null) ||
                                  (dbItem.getUSequence() != null && item.getOrderNumber() == null) ||
                                  ((dbItem.getUSequence() != null && item.getOrderNumber() != null) &&
                                   !(dbItem.getUSequence().equals(item.getOrderNumber())))))
                {
                    if (Log.log.isDebugEnabled() && StringUtils.isNotEmpty(id))
                    {
                        Log.log.debug(dbItem.getSys_id() + " meta_form_action.u_sequence " + dbItem.getUSequence().intValue() + " does not match " + item.getOrderNumber() + "!!!");
                    }
                    modified = true;
                }
                
                dbItem.setUSequence(item.getOrderNumber());
                dbItem.setMetaControlItem(parentMetaControlItem);
                
                if (!modified && ((StringUtils.isNotBlank(item.getRedirectUrl()) && !item.getRedirectUrl().equals(dbItem.getURedirectUrl())) ||
                                  (StringUtils.isBlank(item.getRedirectUrl()) && StringUtils.isNotBlank(dbItem.getURedirectUrl()))))
                {
                    modified = true;
                }
                
                dbItem.setURedirectUrl(item.getRedirectUrl());
                
                if (!modified && ((StringUtils.isNotBlank(item.getRedirectTarget()) && !item.getRedirectTarget().equals(dbItem.getURedirectTarget())) ||
                                  (StringUtils.isBlank(item.getRedirectTarget()) && StringUtils.isNotBlank(dbItem.getURedirectTarget()))))
                {
                    modified = true;
                }
                
                dbItem.setURedirectTarget(item.getRedirectTarget());

                newDbObjs.add(dbItem);
                //position++;

                // persist it right away
                
                if (modified)
                {
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ") + "meta_form_action " + 
                                      (dbItem.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
                    }
                    
                    try
                    {
                        HibernateUtil.getDAOFactory().getMetaFormActionDAO().persist(dbItem);
                    }
                    catch(NonUniqueObjectException nuoe)
                    {
                        Log.log.debug("Non uniqueue exception while persisting meta_form_action for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                        HibernateUtil.getCurrentSession().merge(dbItem);
                    }
                }
            }// end of for
        }
        else
        {
            newDbObjs = null;
        }

        // clenaup that is not required
        filterOutFormActionItemsToDelete(newDbObjs, dbObjs);

        return newDbObjs;
    }// convertViewToMetaFormActions
    
    private Collection<MetaxFieldDependency> convertViewToMetaxFieldDependency(MetaControlItem parentMetaControlItem, Collection<RsFieldDependency> viewObjs, Collection<MetaxFieldDependency> dbObjs)
    {
        Collection<MetaxFieldDependency> newDbObjs = new ArrayList<MetaxFieldDependency>();

        if (viewObjs != null)
        {
            for (RsFieldDependency item : viewObjs)
            {
                MetaxFieldDependency dbItem = getMetaxFieldDependency(item, dbObjs);
                boolean modified = dbItem.getMetaControlItem() != null && dbItem.getMetaControlItem().getSys_id().equalsIgnoreCase(parentMetaControlItem.getSys_id()) ? false : true;
                
                if (!modified && ((StringUtils.isNotBlank(item.getAction()) && !item.getAction().equals(dbItem.getUAction())) ||
                                  (StringUtils.isBlank(item.getAction()) && StringUtils.isNotBlank(dbItem.getUAction()))))
                {
                    modified = true;
                }
                
                dbItem.setUAction(item.getAction());
                
                if (!modified && ((StringUtils.isNotBlank(item.getCondition()) && !item.getCondition().equals(dbItem.getUCondition())) ||
                                  (StringUtils.isBlank(item.getCondition()) && StringUtils.isNotBlank(dbItem.getUCondition()))))
                {
                    modified = true;
                }
                
                dbItem.setUCondition(item.getCondition());
                
                if (!modified && ((StringUtils.isNotBlank(item.getTarget()) && !item.getTarget().equals(dbItem.getUTarget())) ||
                                  (StringUtils.isBlank(item.getTarget()) && StringUtils.isNotBlank(dbItem.getUTarget()))))
                {
                    modified = true;
                }
                
                dbItem.setUTarget(item.getTarget());
                
                if (!modified && ((StringUtils.isNotBlank(item.getValue()) && !item.getValue().equals(dbItem.getUValue())) ||
                                  (StringUtils.isBlank(item.getValue()) && StringUtils.isNotBlank(dbItem.getUValue()))))
                {
                    modified = true;
                }
                
                dbItem.setUValue(item.getValue());
                
                if (!modified && ((StringUtils.isNotBlank(item.getActionOptions()) && !item.getActionOptions().equals(dbItem.getUActionOptions())) ||
                                  (StringUtils.isBlank(item.getActionOptions()) && StringUtils.isNotBlank(dbItem.getUActionOptions()))))
                {
                    modified = true;
                }
                
                dbItem.setUActionOptions(item.getActionOptions());
                
                dbItem.setMetaControlItem(parentMetaControlItem);

                // persist it right away
                if (modified)
                {
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ") + "metax_field_dependency associated with meta_control_item " + 
                                      (dbItem.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
                    }
                    
                    try
                    {
                        HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dbItem);
                    }
                    catch(NonUniqueObjectException nuoe)
                    {
                        Log.log.debug("Non uniqueue exception while persisting metax_field_dependency for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                        HibernateUtil.getCurrentSession().merge(dbItem);
                    }
                }
                
                HibernateUtil.getCurrentSession().merge(dbItem);
                
                newDbObjs.add(dbItem);
            }// end of for loop
        }
        else
        {
            newDbObjs = null;
        }

        // clenaup that is not required
        filterOutMetaxFieldDependencyToDelete(newDbObjs, dbObjs);

        return newDbObjs;
    }
    
    private static MetaxFieldDependency getMetaxFieldDependency(RsFieldDependency item, Collection<MetaxFieldDependency> dbObjs)
    {
        MetaxFieldDependency dbItem = null;

        String id = item.getId();
        if (StringUtils.isNotEmpty(id))
        {
            dbItem = findMetaxFieldDependency(id, dbObjs);
        }

        if (dbItem == null)
        {
            dbItem = new MetaxFieldDependency();
        }
        /*
        dbItem.setUAction(item.getAction());
        dbItem.setUCondition(item.getCondition());
        dbItem.setUTarget(item.getTarget());
        dbItem.setUValue(item.getValue());
        dbItem.setUActionOptions(item.getActionOptions());
        */
        return dbItem;
    }
    
    private static void filterOutMetaxFieldDependencyToDelete(Collection<MetaxFieldDependency> newDbObjs, Collection<MetaxFieldDependency> dbObjs)
    {
        if (dbObjs != null && dbObjs.size() > 0)
        {
            for (MetaxFieldDependency dependency : dbObjs)
            {
                if (shouldDeleteMetaxFieldDependency(newDbObjs, dependency))
                {
                    deleteMetaxFieldDependency(dependency);
                }

            }// end of for
        }// end of if

    }// filterOutControlItemsToDelete
    
    private static MetaxFieldDependency findMetaxFieldDependency(String id, Collection<MetaxFieldDependency> dbObjs)
    {
        MetaxFieldDependency result = null;

        if (dbObjs != null)
        {
            for (MetaxFieldDependency item : dbObjs)
            {
                if (item.getSys_id().equalsIgnoreCase(id))
                {
                    result = item;
                    break;
                }
            }// end of for
        }// end of if

        return result;
    }// findMetaxFieldDependency
    
    private static boolean shouldDeleteMetaxFieldDependency(Collection<MetaxFieldDependency> newDbObjs, MetaxFieldDependency dependency)
    {
        boolean shouldDelete = true;
        String dbId = dependency.getSys_id();

        for (MetaxFieldDependency newDependency : newDbObjs)
        {
            String newId = newDependency.getSys_id();
            if (StringUtils.isNotEmpty(newId))
            {
                if (newId.equals(dbId))
                {
                    shouldDelete = false;
                    break;
                }
            }
        }// end of for loop

        return shouldDelete;

    }// shouldDelete
    
    private static void deleteMetaxFieldDependency(MetaxFieldDependency dependency)
    {
        HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().delete(dependency);
    }// deleteMetaFormAction
    
//    private MetaFormViewProperties convertViewToMetaFormViewProperties(RsFormProperties viewObj, MetaFormViewProperties dbObj)
//    {
//        if (dbObj == null)
//        {
//            dbObj = new MetaFormViewProperties();
//        }
//
//        if (viewObj != null)
//        {
//            dbObj.setULabelWidth(viewObj.getLabelWidth());
//            dbObj.setULabelAlign(viewObj.getLabelAlign());
//        }
//
//        // persist it right away
//        HibernateUtil.getDAOFactory().getMetaFormViewPropertiesDAO().persist(dbObj);
//
//        return dbObj;
//    }// convertViewToMetaFormViewProperties
    
    private List<MetaxFormViewPanel> convertViewToMetaxFormViewPanels(MetaFormView parent, List<RsPanelDTO> viewObjs, Collection<MetaxFormViewPanel> dbObjs)
    {
        List<MetaxFormViewPanel> newDbObjs = new ArrayList<MetaxFormViewPanel>();

        if (viewObjs != null && viewObjs.size() > 0)
        {
            //int position = 1;
            for (RsPanelDTO uipanel : viewObjs)
            {
                MetaxFormViewPanel dbItem = null;
                String id = uipanel.getId();
                boolean modified = false;
                if (StringUtils.isNotEmpty(id))
                {
                    dbItem = findMetaxFormViewPanel(id, dbObjs);
                }

                if (dbItem == null)
                {
                    dbItem = new MetaxFormViewPanel();
                    modified = true;
                }

                // boolean isTabPanel = uipanel.getIsTabPanel();//ALWAYS WILL BE
                // TRUE as all the Forms will have atlease 1 panel

                if (!modified && ((StringUtils.isNotBlank(uipanel.getDisplayName()) && !uipanel.getDisplayName().equals(dbItem.getUPanelTitle())) ||
                                  (StringUtils.isBlank(uipanel.getDisplayName()) && StringUtils.isNotBlank(dbItem.getUPanelTitle()))))
                {
                    modified = true;
                }
                                            
                dbItem.setUPanelTitle(uipanel.getDisplayName());
                
                if (!modified && (dbItem.getUOrder().intValue() != uipanel.getOrderNumber()))
                {
                    if (Log.log.isDebugEnabled() && StringUtils.isNotEmpty(id))
                    {
                        Log.log.debug(dbItem.getSys_id() + " metax_form_view_panel.u_order " + dbItem.getUOrder().intValue() + " does not match " + uipanel.getOrderNumber() + "!!!");
                    }
                    modified = true;
                }
                
                dbItem.setUOrder(uipanel.getOrderNumber());
                
                if (!modified && ((StringUtils.isNotBlank(uipanel.getUrl()) && !uipanel.getUrl().equals(dbItem.getUUrl())) ||
                                  (StringUtils.isBlank(uipanel.getUrl()) && StringUtils.isNotBlank(dbItem.getUUrl()))))
                {
                    modified = true;
                }
                
                dbItem.setUUrl(uipanel.getUrl());
                
                if (!modified && (dbItem.getUNoOfVerticalColumns().intValue() != uipanel.getCols()))
                {
                    modified = true;
                }
                
                dbItem.setUNoOfVerticalColumns(uipanel.getCols());

                dbItem.setMetaFormView(parent);

                // persist it right away
                
                if (modified)
                {
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ") + "metax_form_view_panel " + 
                                      (dbItem.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
                    }
                    //HibernateUtil.getSessionFactory().getCurrentSession().clear();
                    try
                    {
                        HibernateUtil.getDAOFactory().getMetaxFormViewPanelDAO().persist(dbItem);
                    }
                    catch(NonUniqueObjectException nuoe)
                    {
                        Log.log.debug("Non uniqueue exception while persisting metax_form_view_panel for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                        HibernateUtil.getCurrentSession().merge(dbItem);
                    }
                }

                dbItem.setMetaFormTabs(convertUITabPanelToDbModel(uipanel.getTabs(), dbItem.getMetaFormTabs(), dbItem));
                
                HibernateUtil.getCurrentSession().merge(dbItem);
                /*
                HibernateUtil.getDAOFactory().getMetaxFormViewPanelDAO().persist(dbItem);// save
                                                                                         // again
                 */

                // add it to the collection
                newDbObjs.add(dbItem);
                //position++;
            }// end of for loop
        }
        else
        {
            newDbObjs = null;
        }

        // clenaup that is not required
        // SaveMetaFormView.filterOutFormActionItemsToDelete(newDbObjs, dbObjs);

        return newDbObjs;
    }// convertViewToMetaxFormViewPanels
    
    private static MetaxFormViewPanel findMetaxFormViewPanel(String id, Collection<MetaxFormViewPanel> dbObjs)
    {
        MetaxFormViewPanel result = null;

        if (dbObjs != null)
        {
            for (MetaxFormViewPanel item : dbObjs)
            {
                if (item.getSys_id().equalsIgnoreCase(id))
                {
                    result = item;
                    break;
                }
            }// end of for
        }// end of if

        return result;
    }// findMetaxFormViewPanel
    
    private Collection<MetaFormTab> convertUITabPanelToDbModel(Collection<RsTabDTO> viewObjs, Collection<MetaFormTab> dbObjs, MetaxFormViewPanel parent)
    {
        Collection<MetaFormTab> newDbObjs = new ArrayList<MetaFormTab>();

        if (viewObjs != null)
        {
            MetaFormTab dbItem = null;
            //int orderNo = 1;
            for (RsTabDTO item : viewObjs)
            {
                dbItem = convertUITabToDBTab(item, dbObjs, item.getOrderNumber(), parent);
                
                if (dbItem != null)
                {
                    newDbObjs.add(dbItem);
                }

                //orderNo++;

            }// end of for

            // delete the tabs that were before and now deleted by the user
            filterOutTabsToDelete(newDbObjs, dbObjs);

        }
        else
        {
            // make sure that its null then
            newDbObjs = null;
        }
        
        filterOutTabsToDelete(newDbObjs, dbObjs);
        
        return newDbObjs;

    }// convertMetaFormTabFromUIToDbModel
    
    private boolean shouldDeleteMetaFormTabField(Collection<MetaFormTabField> newDbObjs, MetaFormTabField tabField)
    {
       boolean shouldDelete = true;
       String dbId = tabField.getSys_id();

       if (newDbObjs != null)
       {
           for(MetaFormTabField newTabField : newDbObjs)
           {
               String newId = newTabField.getSys_id();
               if(StringUtils.isNotEmpty(newId))
               {
                   if(newId.equals(dbId))
                   {
                       // If it is Reference type field always delete irrespective of whether its same or modified.
                       /*
                        * Fix for SUP-49: RS-26744 FIS: Nullpointer exception with Saving Custom Forms
                        * For some legacy forms, the UType of a Field is read differently.  
                        */
                       String type = null;
                       if (tabField.getMetaFieldFormProperties() != null)
                       {
                           type = tabField.getMetaFieldFormProperties().getUUIType();
                       }
                       else
                       {
                           if (tabField.getMetaSource() != null && tabField.getMetaSource().getMetaFieldProperties() != null)
                           {
                               type = tabField.getMetaSource().getMetaFieldProperties().getUUIType();
                           }
                       }
                       if (type == null || !type.equals(HibernateConstantsEnum.REFERENCE_TYPE.getTagName()))
                       {
                           shouldDelete = false;
                           break;
                       }
                   }
               }
           }//end of for loop
       }
       
       return shouldDelete;

    }//shouldDeleteMetaFormTabField
    
    private void filterOutTabFieldsToDelete(Collection<MetaFormTabField> newDbObjs, Collection<MetaFormTabField> dbObjs)
    {
        Log.log.debug("# of MetaFormTabFields in Form is " + newDbObjs.size() + ", # of MetaFormTabFields in DB for the Form is " + 
                      (dbObjs != null ? dbObjs.size() : "null"));
        
        if(dbObjs != null && dbObjs.size() > 0)
        {
            for(MetaFormTabField tabField : dbObjs)
            {
                if(shouldDeleteMetaFormTabField(newDbObjs, tabField))
                {
                    //HibernateUtil.getSessionFactory().getCurrentSession().clear();
                    HibernateUtil.getCurrentSession().merge(tabField);
                    
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug("deleting meta_form_tab_field with sys_id " + tabField.getSys_id());
                    }
                    
                    deleteOrUpdateMetaFormTabField(tabField);
                }

            }//end of for
        }//end of if
    }//filterOutTabFieldsToDelete
    
    private MetaFormTab convertUITabToDBTab(RsTabDTO item, Collection<MetaFormTab> dbObjs, int orderNo, MetaxFormViewPanel parent)
    {
        MetaFormTab dbItem = null;
        boolean modified = false;
        
        String id = item.getId();
        if (StringUtils.isNotEmpty(id))
        {
            dbItem = findMetaFormTab(id, dbObjs);
        }
        else
        {
            Log.log.debug("RsTabDTO for " + item.getDisplayName() + " at order " + item.getOrderNumber() + " id is null");
            /*
             *  UI seems to be sending blank tab id but non-blank tab field ids,
             *  in such case get the correct tab id associated with tab field
             *  instead of creating new MetaFormTab.
             */
            
            if (item.getColumns() == null || item.getColumns().isEmpty())
                return dbItem;
            
            RsColumnDTO rsColumn = item.getColumns().get(0);
            
            if (rsColumn.getFields() == null || rsColumn.getFields().isEmpty())
                return dbItem;
            
            RsUIField rsUIField = rsColumn.getFields().get(0);
            
            if (StringUtils.isNotBlank(rsUIField.getId()))
            {
                Log.log.debug("RsTabDTO for " + item.getDisplayName() + " at order " + item.getOrderNumber() + 
                              ", id is null but has RsUIField(s) with non-null id!!!");

                // Get MetaFormTabFields of MetaSource (Non-DB) or MetaField (DB) types 
                
                //String sql = "from MetaFormTabField where metaSource.sys_id = '" + rsUIField.getId() + "' or metaField.sys_id = '" + rsUIField.getId() + "'";
                String sql = "from MetaFormTabField where metaSource.sys_id = :sys_id or metaField.sys_id = :sys_id";
                
                Map<String, Object> queryParams = new HashMap<String, Object>();
                
                queryParams.put("sys_id", rsUIField.getId());
                
                List<? extends Object> recs;
                
                try
                {
                    recs = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                }
                catch (Exception e)
                {
                    Log.log.debug("Could not query MetaFormTabFields for " + rsUIField.getId() + " as MetaSource sys_id or MetaField sys_id due to " + e.getMessage(), e);
                    throw new RuntimeException("Could not query MetaFormTabFields for " + rsUIField.getId() + " as MetaSource sys_id or MetaField sys_id due to " + e.getMessage());
                }
                
                if (recs != null && recs.size() > 0)
                {
                    Log.log.debug("Found " + recs.size() + " MetaFormTabField(s) with MetaSource or MetaField sys_id as " + rsUIField.getId());
                    dbItem = ((MetaFormTabField)recs.get(0)).getMetaFormTab();
                    Log.log.debug("Setting MetaFormTab sys_id=" + dbItem.getSys_id() + ", displayName=" + dbItem.getUTabName() + ", order #=" + dbItem.getUOrder());
                }
                else
                {
                    dbItem = new MetaFormTab();
                    modified = true;
                }
            }
            else
            {
                dbItem = new MetaFormTab();
                modified = true;
            }
        }
        
        if (!modified && ((StringUtils.isNotBlank(item.getName()) && StringUtils.isBlank(dbItem.getUTabName())) ||
                          (StringUtils.isBlank(item.getName()) && StringUtils.isNotBlank(dbItem.getUTabName())) ||
                          ((StringUtils.isNotBlank(item.getName()) && StringUtils.isNotBlank(dbItem.getUTabName())) &&
                           !(item.getName().equals(dbItem.getUTabName())))))
        {
            modified = true;
        }
        
        dbItem.setUTabName(item.getName());
        
        if (!modified && ((orderNo > 0 && dbItem.getUOrder() == null) ||
                          (orderNo > 0 && dbItem.getUOrder().intValue() != orderNo)))
        {
            if (Log.log.isDebugEnabled() && StringUtils.isNotEmpty(id))
            {
                Log.log.debug(dbItem.getSys_id() + " meta_form_tab.u_order " + (dbItem.getUOrder() != null ? dbItem.getUOrder().intValue() : "null") + " does not match " + orderNo + "!!!");
            }
            modified = true;
        }
        
        if (orderNo > 0)
        {
            dbItem.setUOrder(Integer.valueOf(orderNo));
        }
        else
        {
            dbItem.setUOrder(Integer.getInteger("1"));
        }
        // dbItem.setUPosition(item.getPosition());
        // dbItem.setUTabWidth(item.getTabWidth());
        // dbItem.setUIsSelectedDefault(item.isSelectedDefault());
        
        if (!modified && (dbItem.getUNoOfVerticalColumns().intValue() != item.getCols()))
        {
            modified = true;
        }
        
        dbItem.setUNoOfVerticalColumns(item.getCols());
        
        if (!modified && ((StringUtils.isNotBlank(item.getUrl()) && !item.getUrl().equals(dbItem.getUUrl())) ||
                          (StringUtils.isBlank(item.getUrl()) && StringUtils.isNotBlank(dbItem.getUUrl()))))
        {
            modified = true;
        }
        
        dbItem.setUUrl(item.getUrl());
        // dbItem.setMetaFormView(dbView);//**DO NOT SET THIS AS THIS IS NOT THE
        // PARENT ANYMORE
        dbItem.setMetaxFormViewPanel(parent);

        // persist it right away
        
        if (modified)
        {
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ") + "meta_form_tab " +
                              (dbItem.getSys_id() != null ? "modified" : "added")+ ", persisting!!!");
            }
            
            //HibernateUtil.getSessionFactory().getCurrentSession().clear();
            try
            {
                HibernateUtil.getDAOFactory().getMetaFormTabDAO().persist(dbItem);
            }
            catch(NonUniqueObjectException nuoe)
            {
                Log.log.debug("Non uniqueue exception while persisting meta_form_tab for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                HibernateUtil.getCurrentSession().merge(dbItem);
            }
        }

        // delete the existing references
        // delete all the records from meta_form_tab_field if any as this tab is
        // URL and not have any fields
        //deleteMetaFormTabFields(dbItem.getMetaFormTabFields()); 

        // manipulate the tab fields
        if (StringUtils.isEmpty(dbItem.getUUrl()))
        {
            List<MetaFormTabField> newdbFields = new ArrayList<MetaFormTabField>();
            int col = 1;
            for (RsColumnDTO column : item.getColumns())
            {
                newdbFields.addAll(createMetaFormTabFieldFromUIFields(dbItem, column.getFields(), col));
                col++;
            }// end of for loop

            filterOutTabFieldsToDelete(newdbFields, dbItem.getMetaFormTabFields());
            
            dbItem.setMetaFormTabFields(newdbFields);
        }

        // dependencies
        dbItem.setMetaxFieldDependencys(convertViewToMetaxFieldDependencyForTab(dbItem, item.getDependencies(), dbItem.getMetaxFieldDependencys()));
        
        HibernateUtil.getCurrentSession().merge(dbItem);
        
        return dbItem;
    }
    
    private static void deleteMetaFormTabField(MetaFormTabField metaFormTabField)
    {
        try
        {
            // NEED THIS FOR ORACLE as the DELETES are not COMMITTED immediately
          HibernateProxy.setCurrentUser("admin");
            HibernateProxy.execute(() -> {

	            if (metaFormTabField.getMetaFieldFormProperties() != null)
	            {
	                if (metaFormTabField.getMetaField() != null)
	                { // DB Field (MetaField)
	                    // Delete only if custom property, i.e. not default MetaField property
	                
	                    if (metaFormTabField.getMetaField().getMetaFieldProperties() != null)
	                    {
	                        if (!metaFormTabField.getMetaField().getMetaFieldProperties().getSys_id().equals(metaFormTabField.getMetaFieldFormProperties().getSys_id()))
	                        {   
	                            DeleteMetaForm.deleteMetaFieldProperties(metaFormTabField.getMetaFieldFormProperties(), null);
	                        }
	                        else
	                        {
	                            Log.log.debug("Skipping Deletion of MetaFieldProperties for deleted MetaFormTabField " + metaFormTabField.getSys_id() + 
	                                          " as it is default MetaFieldPropoert for MetaField " + metaFormTabField.getMetaField().getUDisplayName());
	                        }
	                    }
	                    else
	                    {
	                        Log.log.warn("MetaField " + metaFormTabField.getMetaField().getUDisplayName() + " does not have MetaFieldProperties!!!");
	                    }
	                }
	                else
	                { // Non-DB Field (MetaSource)
	                    DeleteMetaForm.deleteMetaFieldProperties(metaFormTabField.getMetaFieldFormProperties(), null);
	                }
	            }
	            HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().delete(metaFormTabField);

            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }// dceleteMetaFormTabField
    
    private static void deleteOrUpdateMetaFormTabField(MetaFormTabField metaFormTabField)
    {
        try
        {
            // NEED THIS FOR ORACLE as the DELETES are not COMMITTED immediately
          HibernateProxy.setCurrentUser("admin");
	        	HibernateProxy.execute(() -> {
	
	            boolean delMtFrmTblFld = true;
	            
	            if (metaFormTabField.getMetaFieldFormProperties() != null)
	            {
	                if (metaFormTabField.getMetaField() != null)
	                { // DB Field (MetaField)
	                    // Delete only if custom property, i.e. not default MetaField property
	                
	                    if (metaFormTabField.getMetaField().getMetaFieldProperties() != null)
	                    {
	                        if (!metaFormTabField.getMetaField().getMetaFieldProperties().getSys_id().equals(metaFormTabField.getMetaFieldFormProperties().getSys_id()))
	                        {   
	                            DeleteMetaForm.deleteMetaFieldProperties(metaFormTabField.getMetaFieldFormProperties(), null);
	                        }
	                        else if (metaFormTabField.getMetaField().getMetaFieldProperties().getUUIType() != null &&
	                                 metaFormTabField.getMetaFieldFormProperties().getUUIType() != null &&
	                                 metaFormTabField.getMetaField().getMetaFieldProperties().getUUIType().equals(HibernateConstantsEnum.REFERENCE_TYPE.getTagName()) &&
	                                 metaFormTabField.getMetaFieldFormProperties().getUUIType().equals(HibernateConstantsEnum.REFERENCE_TYPE.getTagName()))
	                        {
	                            Log.log.debug("Updating MetaFieldProperties for updated MetaFormTabField " + metaFormTabField.getSys_id() + 
	                                          " as it's field type is of " + HibernateConstantsEnum.REFERENCE_TYPE.getTagName());
	                            // Update MetaFieldProperties for Reference type field
	                            HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().update(metaFormTabField.getMetaFieldFormProperties());
	                            delMtFrmTblFld = false;
	                        }
	                        else
	                        {
	                            Log.log.debug("Skipping Deletion of MetaFieldProperties for deleted MetaFormTabField " + metaFormTabField.getSys_id() + 
	                                          " as it is default MetaFieldPropoert for MetaField " + metaFormTabField.getMetaField().getUDisplayName());
	                        }
	                    }
	                    else
	                    {
	                        Log.log.warn("MetaField " + metaFormTabField.getMetaField().getUDisplayName() + " does not have MetaFieldProperties!!!");
	                    }
	                }
	                else
	                { // Non-DB Field (MetaSource)
	                    DeleteMetaForm.deleteMetaFieldProperties(metaFormTabField.getMetaFieldFormProperties(), null);
	                }
	            }
	            
	            if (delMtFrmTblFld)
	            {
	                HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().delete(metaFormTabField);
	            }
	            else if (Log.log.isDebugEnabled())
	            {
	                Log.log.debug("Skipping deletion of MetaFormTabField " + metaFormTabField.getSys_id() + " as its MetaFieldProperties got updated." );
	            }

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }// deleteOrUpdateMetaFormTabField
    
    private MetaFormTabField findMetaFormTabField(String id, Collection<MetaFormTabField> dbObjs)
    {
        MetaFormTabField result = null;

        if (dbObjs != null)
        {
            for(MetaFormTabField item : dbObjs)
            {
                if(item.getSys_id().equalsIgnoreCase(id) || 
                   (item.getMetaSource() != null && item.getMetaSource().getSys_id().equalsIgnoreCase(id)) ||
                   (item.getMetaField() != null && item.getMetaField().getSys_id().equalsIgnoreCase(id)))
                {
                    result = item;
                    break;
                }
            }
        }

        return result;
    }//findMetaFormTabField
    
    private List<MetaFormTabField> createMetaFormTabFieldFromUIFields(MetaFormTab parent, List<RsUIField> fields, int colNumber)
    {
        List<MetaFormTabField> newdbFields = new ArrayList<MetaFormTabField>();

        if (fields != null && fields.size() > 0)
        {
            //int fieldOrderNo = 1;
            //Set<MetaFormTabField> nonTabFlds = new HashSet<MetaFormTabField>();
            
            for (RsUIField item : fields)
            {
                if (StringUtils.isEmpty(item.getSourceType()))
                {
                    throw new RuntimeException("Source type (DB or INPUT) is mandatory.");
                }
                
                MetaFormTabField newdbItem = null;
                boolean modified = false;
                
                // Old : Tab Id, New: Panel Only (Default Tab), # of Tabs > 1 then Tab Id.
                
                String id = item.getId();
                if (StringUtils.isNotEmpty(id))
                {
                    Set<MetaFormTabField> tabNMetaSrcFlds = new HashSet<MetaFormTabField>();
                    
                    if (parent.getMetaFormTabFields() != null)
                    {
                        tabNMetaSrcFlds.addAll(parent.getMetaFormTabFields());
                    }
                    else
                    {
                        // Get MetaFormTabFields which are not part of Tabs
                        
                        //String sql = "from MetaFormTabField where metaSource.sys_id = '" + id + "' or metaField.sys_id = '" + id + "'";
                        String sql = "from MetaFormTabField where metaSource.sys_id = :sys_id or metaField.sys_id = :sys_id";
                        
                        Map<String, Object> queryParams = new HashMap<String, Object>();
                        queryParams.put("sys_id", id);
                        
                        List<? extends Object> recs;
                        
                        try
                        {
                            recs = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                        }
                        catch (Exception e)
                        {
                            Log.log.debug("Could not query MetaFormTabFields for " + id + " as MetaSource sys_id due to " + e.getMessage(), e);
                            throw new RuntimeException("Could not query MetaFormTabFields for " + id + " as MetaSource sys_id due to " + e.getMessage());
                        }
                        
                        if (recs != null && recs.size() > 0)
                        {
                            Log.log.debug("Found " + recs.size() + " MetaFormTabField(s) with MetaSource sys_id " + id + " as MetaSource sys_id");
                            
                            for (Object rec : recs)
                            {
                                tabNMetaSrcFlds.add((MetaFormTabField)rec);
                                //nonTabFlds.add((MetaFormTabField)rec);
                            }
                        }
                    }
                   
                    Log.log.debug("Checking for id " + id + " in MetaFormTabField(s) of size " + tabNMetaSrcFlds.size());
                    
                    newdbItem = findMetaFormTabField(id, tabNMetaSrcFlds);
                    
                    if (newdbItem == null)
                    {
                        throw new RuntimeException("Could not find record in MetaFormTabField for " + item.getName() + " of source type " + item.getSourceType() + " with id " + id);
                    }
                }
                else
                {
                    newdbItem = new MetaFormTabField();
                    modified = true;
                }
                
                if (!modified && ((newdbItem.getUOrder() == null) || (newdbItem.getUOrder().intValue() != item.getOrderNumber() /*fieldOrderNo*/)))
                {
                    if (Log.log.isDebugEnabled() && StringUtils.isNotEmpty(id))
                    {
                        Log.log.debug(newdbItem.getSys_id() + " meta_form_tab_field.u_order " + (newdbItem.getUOrder() != null ? newdbItem.getUOrder().intValue() : "null") + " does not match " + item.getOrderNumber() + "!!!");
                    }
                    modified = true;
                }
                
                newdbItem.setUOrder(/*fieldOrderNo*/item.getOrderNumber());// (item.getOrderNumber());
                
                if (!modified && (newdbItem.getUColumnNumber().intValue() != colNumber))
                {
                    modified = true;
                }
                
                newdbItem.setUColumnNumber(colNumber);
                
                newdbItem.setMetaFormTab(parent);

                if (item.getSourceType().equalsIgnoreCase(DataSource.DB.getTagName()))
                {
                    newdbItem.setMetaSource(null);
                    
                    // DB - will always have a id
                    // MetaField metaField =
                    // HibernateUtil.getDAOFactory().getMetaFieldDAO().findById(item.getDbcolumnId());
                    MetaField metaField = findMetaField(item.getName());
                    
                    if (!modified && ((metaField != null && newdbItem.getMetaField() == null) ||
                                      (metaField == null && newdbItem.getMetaField() != null) ||
                                      !(metaField.getSys_id().equalsIgnoreCase(newdbItem.getMetaField().getSys_id()))))
                    {
                        modified = true;
                    }
                                      
                    newdbItem.setMetaField(metaField);

                    // create the MetaFieldProperties for this form
                    MetaFieldProperties metaFieldProperties = convertINPUTUIToDbModelProperties(item, null, metaField.getMetaFieldProperties(), true);
                    
                    if (!modified && ((metaFieldProperties != null && newdbItem.getMetaFieldFormProperties() == null) ||
                                      (metaFieldProperties == null && newdbItem.getMetaFieldFormProperties() != null) ||
                                      !(metaFieldProperties.getSys_id().equalsIgnoreCase(newdbItem.getMetaFieldFormProperties().getSys_id()))))
                    {
                        modified = true;
                    }
                    
                    newdbItem.setMetaFieldFormProperties(metaFieldProperties);
                }
                else
                {
                    newdbItem.setMetaField(null);
                    
                    // source
                    MetaSource metaSource = null;
                    if (StringUtils.isNotEmpty(item.getId()))
                    {
                        metaSource = HibernateUtil.getDAOFactory().getMetaSourceDAO().findById(item.getId());
                    }
                    else
                    {
                        metaSource = new MetaSource();
                    }

                    metaSource = convertINPUTUIToDbModel(item, metaSource);
                    
                    if (!modified && ((metaSource != null && newdbItem.getMetaSource() == null) ||
                                      (metaSource == null && newdbItem.getMetaSource() != null) ||
                                      !(metaSource.getSys_id().equalsIgnoreCase(newdbItem.getMetaSource().getSys_id()))))
                    {
                        modified = true;
                    }
                    
                    newdbItem.setMetaSource(metaSource);
                    validMetaSourceSysIds.add(metaSource.getSys_id());
                }

                // persist it right away
                if (modified)
                {
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug((newdbItem.getSys_id() != null ? newdbItem.getSys_id() + " " : "new ") + "meta_form_tab_field " + 
                                      (newdbItem.getSys_id() != null ? "modified" : "added")+ ", persisting!!!");
                    }
                    
                    //HibernateUtil.getSessionFactory().getCurrentSession().clear();
                    try
                    {
                        HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().persist(newdbItem);
                    }
                    catch(NonUniqueObjectException nuoe)
                    {
                        Log.log.debug("Non uniqueue exception while persisting meta_form_tab_field for " + newdbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                        HibernateUtil.getCurrentSession().merge(newdbItem);
                    }
                }

                HibernateUtil.getCurrentSession().merge(newdbItem);
                
                // add to the collection
                newdbFields.add(newdbItem);

                //fieldOrderNo++;
            }
            /*
            if (!nonTabFlds.isEmpty())
            {
                Log.log.debug("Added " + nonTabFlds.size() + " MetaFormTabField(s) to default MetaFormTab named " + parent.getUTabName() + " with sys_id " + parent.getSys_id());
                parent.setMetaFormTabFields(nonTabFlds);
                HibernateUtil.getSessionFactory().getCurrentSession().merge(parent);
            }*/
        }

        return newdbFields;
    }
    
    private MetaField findMetaField(String fieldName)
    {
        MetaField field = null;

        if (StringUtils.isNotBlank(fieldName) && this.customTable != null)
        {
            Collection<MetaField> fields = this.customTable.getMetaFields();
            if (fields != null && fields.size() > 0)
            {
                for (MetaField dbField : fields)
                {
                    if (fieldName.equals(dbField.getUName()))
                    {
                        field = dbField;
                        break;
                    }
                }
            }
        }
        return field;
    }
    
    /**
     * Note: this api should be in a Hibernate  transaction to work.
     * 
     * @param uifield
     * @param dbItem
     * @param prop
     * @param isCustomForm
     * @return
     */
    public static MetaFieldProperties convertINPUTUIToDbModelProperties(RsUIField uifield, MetaSource dbItem, MetaFieldProperties prop, boolean isCustomForm)
    {
        boolean modified = false;
        
        if (prop == null)
        {
            prop = new MetaFieldProperties();
            modified = true;
        }

        // u_type is same as the u_ui_type
        if (dbItem != null)
        {
            if (!modified && ((StringUtils.isNotBlank(uifield.getUiType()) && !uifield.getUiType().equals(dbItem.getUType())) ||
                              (StringUtils.isBlank(uifield.getUiType()) && StringUtils.isNotBlank(dbItem.getUType()))))
            {
                modified = true;
            }
            
            dbItem.setUType(uifield.getUiType());
        }

        if (!modified && ((StringUtils.isNotBlank(uifield.getUiType()) && !uifield.getUiType().equals(prop.getUUIType())) ||
                          (StringUtils.isBlank(uifield.getUiType()) && StringUtils.isNotBlank(prop.getUUIType()))))
        {
            modified = true;
        }
        
        prop.setUUIType(uifield.getUiType());
        // prop.setUUIXype(uifield.getUiXtype());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getJs()) && !uifield.getJs().equals(prop.getUJavascript())) ||
                          (StringUtils.isBlank(uifield.getJs()) && StringUtils.isNotBlank(prop.getUJavascript()))))
        {
            modified = true;
        }
        
        prop.setUJavascript(uifield.getJs());
                
        if (!modified && ((prop.getUSize() != null && !prop.getUSize().equals(uifield.getSize())) ||
                           (prop.getUSize() == null && uifield.getSize() != null)))
        {
            modified = true;
        }
        
        prop.setUSize(uifield.getSize());
        
        if (!modified && ((prop.getUIsMandatory() != null && !prop.getUIsMandatory().booleanValue() == uifield.isMandatory()) ||
                          prop.getUIsMandatory() == null))
        {
            modified = true;
        }
        
        prop.setUIsMandatory(Boolean.valueOf(uifield.isMandatory()));
        
        if (!modified && ((prop.getUIsCrypt() != null && !(prop.getUIsCrypt().booleanValue() == uifield.isEncrypted())) ||
                          (prop.getUIsCrypt() == null)))
        {
            modified = true;
        }
        
        prop.setUIsCrypt(Boolean.valueOf(uifield.isEncrypted()));
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getDefaultValue()) && !uifield.getDefaultValue().equals(prop.getUDefaultValue())) ||
                          (StringUtils.isBlank(uifield.getDefaultValue()) && StringUtils.isNotBlank(prop.getUDefaultValue()))))
        {
            modified = true;
        }
        
        prop.setUDefaultValue(uifield.getDefaultValue());
        
        if (!modified && ((prop.getUWidth() != null && !prop.getUWidth().equals(uifield.getWidth())) ||
                          (prop.getUWidth() == null && uifield.getWidth() != null)))
        {
            modified = true;
        }
        
        prop.setUWidth(uifield.getWidth());
        
        if (!modified && ((prop.getUHeight() != null && !prop.getUHeight().equals(uifield.getHeight())) ||
                          (prop.getUHeight() == null && uifield.getHeight() != null )))
        {
            modified = true;
        }
        
        prop.setUHeight(uifield.getHeight());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getLabelAlign()) && !uifield.getLabelAlign().equals(prop.getULabelAlign())) ||
                          (StringUtils.isBlank(uifield.getLabelAlign()) && StringUtils.isNotBlank(prop.getULabelAlign()))))
        {
            modified = true;
        }
        
        prop.setULabelAlign(uifield.getLabelAlign());

        if (!modified && ((StringUtils.isNotBlank(uifield.getName()) && !uifield.getName().equals(prop.getUName())) ||
                          (StringUtils.isBlank(uifield.getName()) && StringUtils.isNotBlank(prop.getUName()))))
        {
            modified = true;
        }
        
        prop.setUName(uifield.getName());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getDisplayName()) && !uifield.getDisplayName().equals(prop.getUDisplayName())) ||
                          (StringUtils.isBlank(uifield.getDisplayName()) && StringUtils.isNotBlank(prop.getUDisplayName()))))
        {
            modified = true;
        }
        
        prop.setUDisplayName(uifield.getDisplayName());
        
        if (!modified && ((prop.getUOrder() != null && !(prop.getUOrder().intValue() != uifield.getOrderNumber())) ||
                           prop.getUOrder() == null))
        {
            modified = true;
        }
        
        prop.setUOrder(Integer.valueOf(uifield.getOrderNumber()));
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getDbtable()) && !uifield.getDbtable().equals(prop.getUTable())) ||
                          (StringUtils.isBlank(uifield.getDbtable()) && StringUtils.isNotBlank(prop.getUTable()))))
        {
            modified = true;
        }
        
        prop.setUTable(uifield.getDbtable());
                
        if (!modified && ((prop.getUBooleanMaxLength() != null && !prop.getUBooleanMaxLength().equals(uifield.getBooleanMaxLength())) ||
                          (prop.getUBooleanMaxLength() == null && uifield.getBooleanMaxLength() != null)))
        {
            modified = true;
        }
        
        prop.setUBooleanMaxLength(uifield.getBooleanMaxLength());
                
        if (!modified && ((prop.getUStringMinLength() != null && !prop.ugetUStringMinLength().equals(uifield.getStringMinLength())) ||
                           (prop.getUStringMinLength() == null && uifield.getStringMinLength() != null)))
        {
            modified = true;
        }
        
        prop.setUStringMinLength(uifield.getStringMinLength());
                
        if (!modified && ((prop.getUStringMaxLength() != null && !prop.ugetUStringMaxLength().equals(uifield.getStringMaxLength())) ||
                           (prop.getUStringMaxLength() == null && uifield.getStringMaxLength() != null)))
        {
            modified = true;
        }
        
        prop.setUStringMaxLength(uifield.getStringMaxLength());
                
        if (!modified && ((prop.getUUIStringMaxLength() != null && !prop.getUUIStringMaxLength().equals(uifield.getUiStringMaxLength())) ||
                          (prop.getUUIStringMaxLength() == null && uifield.getUiStringMaxLength() != null)))
        {
            modified = true;
        }
        
        prop.setUUIStringMaxLength(uifield.getUiStringMaxLength());
        
        if (!modified && ((prop.getUSelectIsMultiSelect() == null && uifield.getIsMultiSelect() != null) || 
                          (prop.getUSelectIsMultiSelect() != null && uifield.getIsMultiSelect() == null) || 
                          ((prop.getUSelectIsMultiSelect() != null && uifield.getIsMultiSelect() != null) && 
                           !(prop.getUSelectIsMultiSelect().equals(uifield.getIsMultiSelect())))))
        {
            modified = true;
        }
        
        prop.setUSelectIsMultiSelect(uifield.getIsMultiSelect());
        
        if (!modified && ((prop.getUSelectUserInput() == null && uifield.getSelectUserInput() != null) || 
                          (prop.getUSelectUserInput() != null && uifield.getSelectUserInput() == null) || 
                          ((prop.getUSelectUserInput() != null && uifield.getSelectUserInput() != null) &&
                           !(prop.getUSelectUserInput().equals(uifield.getSelectUserInput())))))
        {
            modified = true;
        }
        
        prop.setUSelectUserInput(uifield.getSelectUserInput());
                
        if (!modified && ((prop.getUSelectMaxDisplay() != null && !prop.getUSelectMaxDisplay().equals(uifield.getSelectMaxDisplay())) ||
                          (prop.getUSelectMaxDisplay() == null && uifield.getSelectMaxDisplay() != null)))
        {
            modified = true;
        }
        
        prop.setUSelectMaxDisplay(uifield.getSelectMaxDisplay());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getSelectValues()) && !uifield.getSelectValues().equals(prop.getUSelectValues())) ||
                          (StringUtils.isBlank(uifield.getSelectValues()) && StringUtils.isNotBlank(prop.getUSelectValues()))))
        {
            modified = true;
        }
        
        prop.setUSelectValues(uifield.getSelectValues());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getChoiceValues()) && !uifield.getChoiceValues().equals(prop.getUChoiceValues())) ||
                          (StringUtils.isBlank(uifield.getChoiceValues()) && StringUtils.isNotBlank(prop.getUChoiceValues()))))
        {
            modified = true;
        }
        
        prop.setUChoiceValues(uifield.getChoiceValues());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getCheckboxValues()) && !uifield.getCheckboxValues().equals(prop.getUCheckboxValues())) ||
                          (StringUtils.isBlank(uifield.getCheckboxValues()) && StringUtils.isNotBlank(prop.getUCheckboxValues()))))
        {
            modified = true;
        }
        
        prop.setUCheckboxValues(uifield.getCheckboxValues());
        
        if (!modified && ((prop.getUDateTimeHasCalendar() == null && uifield.getDateTimeHasCalendar() != null) || 
                          (prop.getUDateTimeHasCalendar() != null && uifield.getDateTimeHasCalendar() == null) || 
                          ((prop.getUDateTimeHasCalendar() != null && uifield.getDateTimeHasCalendar() != null) &&
                           !(prop.getUDateTimeHasCalendar().equals(uifield.getDateTimeHasCalendar())))))
        {
            modified = true;
        }
        
        prop.setUDateTimeHasCalendar(uifield.getDateTimeHasCalendar());
                
        if (!modified && ((prop.getUIntegerMinValue() != null && !prop.getUIntegerMinValue().equals(uifield.getIntegerMinValue())) ||
                          (prop.getUIntegerMinValue() == null && uifield.getIntegerMinValue() != null)))
        {
            modified = true;
        }
        
        prop.setUIntegerMinValue(uifield.getIntegerMinValue());
                
        if (!modified && ((prop.getUIntegerMaxValue() != null && !prop.getUIntegerMaxValue().equals(uifield.getIntegerMaxValue())) ||
                          (prop.getUIntegerMaxValue() != null && uifield.getIntegerMaxValue() == null)))
        {
            modified = true;
        }
        
        prop.setUIntegerMaxValue(uifield.getIntegerMaxValue());
        
        if (!modified && ((prop.getUDecimalMinValue() == null && uifield.getDecimalMinValue() != null) ||
                          (prop.getUDecimalMinValue() != null && 
                           !prop.getUDecimalMinValue().equals(uifield.getDecimalMinValue()))))
        {
            modified = true;
        }
        
        prop.setUDecimalMinValue(uifield.getDecimalMinValue());
        
        if (!modified && ((prop.getUDecimalMaxValue() == null && uifield.getDecimalMaxValue() != null) ||
                          (prop.getUDecimalMaxValue() != null && 
                           !prop.getUDecimalMaxValue().equals(uifield.getDecimalMaxValue()))))
        {
            modified = true;
        }
        
        prop.setUDecimalMaxValue(uifield.getDecimalMaxValue());
                
        if (!modified && ((prop.ugetUJournalRows() == null && uifield.getJournalRows() != null) || 
                          (prop.ugetUJournalRows() != null &&
                           !prop.ugetUJournalRows().equals(uifield.getJournalRows()))))
        {
            modified = true;
        }
        
        prop.setUJournalRows(uifield.getJournalRows());
                
        if (!modified && ((prop.getUJournalColumns() != null && !prop.getUJournalColumns().equals(uifield.getJournalColumns())) ||
                          (prop.getUJournalColumns() == null && uifield.getJournalColumns() != null)))
        {
            modified = true;
        }
        
        prop.setUJournalColumns(uifield.getJournalColumns());
                
        if (!modified && ((prop.getUJournalMinValue() != null && !prop.getUJournalMinValue().equals(uifield.getJournalMinValue())) ||
                          (prop.getUJournalMinValue() == null && uifield.getJournalMinValue() != null)))
        {
            modified = true;
        }
        
        prop.setUJournalMinValue(uifield.getJournalMinValue());
                
        if (!modified && ((prop.getUJournalMaxValue() != null && !prop.getUJournalMaxValue().equals(uifield.getJournalMaxValue())) ||
                          (prop.getUJournalMaxValue() == null && uifield.getJournalMaxValue() != null)))
        {
            modified = true;
        }
        
        prop.setUJournalMaxValue(uifield.getJournalMaxValue());
                
        if (!modified && ((prop.getUListRows() != null && !prop.getUListRows().equals(uifield.getListRows())) ||
                          (prop.getUListRows() == null && uifield.getListRows() != null)))
        {
            modified = true;
        }
        
        prop.setUListRows(uifield.getListRows());
                
        if (!modified && ((prop.getUListColumns() != null && !prop.getUListColumns().equals(uifield.getListColumns())) ||
                          (prop.getUListColumns() != null && uifield.getListColumns() != null)))
        {
            modified = true;
        }
        
        prop.setUListColumns(uifield.getListColumns());
                
        if (!modified && ((prop.getUListMaxDisplay() != null && !prop.getUListMaxDisplay().equals(uifield.getListMaxDisplay())) ||
                          (prop.getUListMaxDisplay() == null && uifield.getListMaxDisplay() != null)))
        {
            modified = true;
        }
        
        prop.setUListMaxDisplay(uifield.getListMaxDisplay());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getListValues()) && !uifield.getListValues().equals(prop.getUListValues())) ||
                          (StringUtils.isBlank(uifield.getListValues()) && StringUtils.isNotBlank(prop.getUListValues()))))
        {
            modified = true;
        }
        
        prop.setUListValues(uifield.getListValues());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getReferenceTable()) && !uifield.getReferenceTable().equals(prop.getUReferenceTable())) ||
                          (StringUtils.isBlank(uifield.getReferenceTable()) && StringUtils.isNotBlank(prop.getUReferenceTable()))))
        {
            modified = true;
        }
        
        prop.setUReferenceTable(uifield.getReferenceTable());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getReferenceDisplayColumn()) && !uifield.getReferenceDisplayColumn().equals(prop.getUReferenceDisplayColumn())) ||
                          (StringUtils.isBlank(uifield.getReferenceDisplayColumn()) && StringUtils.isNotBlank(prop.getUReferenceDisplayColumn()))))
        {
            modified = true;
        }
        
        prop.setUReferenceDisplayColumn(uifield.getReferenceDisplayColumn());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getReferenceDisplayColumnList()) && !uifield.getReferenceDisplayColumnList().equals(prop.getUReferenceDisplayColumnList())) ||
                          (StringUtils.isBlank(uifield.getReferenceDisplayColumnList()) && StringUtils.isNotBlank(prop.getUReferenceDisplayColumnList()))))
        {
            modified = true;
        }
        
        prop.setUReferenceDisplayColumnList(uifield.getReferenceDisplayColumnList());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getReferenceTarget()) && !uifield.getReferenceTarget().equals(prop.getUReferenceTarget())) ||
                          (StringUtils.isBlank(uifield.getReferenceTarget()) && StringUtils.isNotBlank(prop.getUReferenceTarget()))))
        {
            modified = true;
        }
        
        prop.setUReferenceTarget(uifield.getReferenceTarget());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getReferenceParams()) && !uifield.getReferenceParams().equals(prop.getUReferenceParams())) ||
                          (StringUtils.isBlank(uifield.getReferenceParams()) && StringUtils.isNotBlank(prop.getUReferenceParams()))))
        {
            modified = true;
        }
        
        prop.setUReferenceParams(uifield.getReferenceParams());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getLinkTarget()) && !uifield.getLinkTarget().equals(prop.getULinkTarget())) ||
                          (StringUtils.isBlank(uifield.getLinkTarget()) && StringUtils.isNotBlank(prop.getULinkTarget()))))
        {
            modified = true;
        }
        
        prop.setULinkTarget(uifield.getLinkTarget());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getLinkParams()) && !uifield.getLinkParams().equals(prop.getULinkParams())) ||
                          (StringUtils.isBlank(uifield.getLinkParams()) && StringUtils.isNotBlank(prop.getULinkParams()))))
        {
            modified = true;
        }
        
        prop.setULinkParams(uifield.getLinkParams());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getLinkTemplate()) && !uifield.getLinkTemplate().equals(prop.getULinkTemplate())) ||
                          (StringUtils.isBlank(uifield.getLinkTemplate()) && StringUtils.isNotBlank(prop.getULinkTemplate()))))
        {
            modified = true;
        }
        
        prop.setULinkTemplate(uifield.getLinkTemplate());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getSequencePrefix()) && !uifield.getSequencePrefix().equals(prop.getUSequencePrefix())) ||
                          (StringUtils.isBlank(uifield.getSequencePrefix()) && StringUtils.isNotBlank(prop.getUSequencePrefix()))))
        {
            modified = true;
        }
        
        prop.setUSequencePrefix(uifield.getSequencePrefix());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getHiddenValue()) && !uifield.getHiddenValue().equals(prop.getUHiddenValue())) ||
                          (StringUtils.isBlank(uifield.getHiddenValue()) && StringUtils.isNotBlank(prop.getUHiddenValue()))))
        {
            modified = true;
        }
        
        prop.setUHiddenValue(uifield.getHiddenValue());
        
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getChoiceTable()) && !uifield.getChoiceTable().equals(prop.getUChoiceTable())) ||
                          (StringUtils.isBlank(uifield.getChoiceTable()) && StringUtils.isNotBlank(prop.getUChoiceTable()))))
        {
            modified = true;
        }
        
        prop.setUChoiceTable(uifield.getChoiceTable());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getChoiceDisplayField()) && !uifield.getChoiceDisplayField().equals(prop.getUChoiceDisplayField())) ||
                          (StringUtils.isBlank(uifield.getChoiceDisplayField()) && StringUtils.isNotBlank(prop.getUChoiceDisplayField()))))
        {
            modified = true;
        }
        
        prop.setUChoiceDisplayField(uifield.getChoiceDisplayField());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getChoiceValueField()) && !uifield.getChoiceValueField().equals(prop.getUChoiceValueField())) ||
                          (StringUtils.isBlank(uifield.getChoiceValueField()) && StringUtils.isNotBlank(prop.getUChoiceValueField()))))
        {
            modified = true;
        }
        
        prop.setUChoiceValueField(uifield.getChoiceValueField());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getChoiceWhere()) && !uifield.getChoiceWhere().equals(prop.getUChoiceWhere())) ||
                          (StringUtils.isBlank(uifield.getChoiceWhere()) && StringUtils.isNotBlank(prop.getUChoiceWhere()))))
        {
            modified = true;
        }
        
        prop.setUChoiceWhere(uifield.getChoiceWhere());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getChoiceField()) && !uifield.getChoiceField().equals(prop.getUChoiceField())) ||
                          (StringUtils.isBlank(uifield.getChoiceField()) && StringUtils.isNotBlank(prop.getUChoiceField()))))
        {
            modified = true;
        }
        
        prop.setUChoiceField(uifield.getChoiceField());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getChoiceSql()) && !uifield.getChoiceSql().equals(prop.getUChoiceSql())) ||
                          (StringUtils.isBlank(uifield.getChoiceSql()) && StringUtils.isNotBlank(prop.getUChoiceSql()))))
        {
            modified = true;
        }
        
        prop.setUChoiceSql(uifield.getChoiceSql());
        
        if (!modified && (prop.getUChoiceOptionSelection() != uifield.getChoiceOptionSelection()))
        {
            modified = true;
        }
        
        prop.setUChoiceOptionSelection(uifield.getChoiceOptionSelection());

        
        if (!modified && ((StringUtils.isNotBlank(uifield.getGroups()) && !uifield.getGroups().equals(prop.getUGroups())) ||
                          (StringUtils.isBlank(uifield.getGroups()) && StringUtils.isNotBlank(prop.getUGroups()))))
        {
            modified = true;
        }
        
        prop.setUGroups(uifield.getGroups());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getUsersOfTeams()) && !uifield.getUsersOfTeams().equals(prop.getUUsersOfTeams())) ||
                          (StringUtils.isBlank(uifield.getUsersOfTeams()) && StringUtils.isNotBlank(prop.getUUsersOfTeams()))))
        {
            modified = true;
        }
        
        prop.setUUsersOfTeams(uifield.getUsersOfTeams());
        
        if (!modified && ((prop.getURecurseUsersOfTeam() == null && uifield.getRecurseUsersOfTeam() != null) || 
                          (prop.getURecurseUsersOfTeam() != null && uifield.getRecurseUsersOfTeam() == null) ||
                          ((prop.getURecurseUsersOfTeam() != null && uifield.getRecurseUsersOfTeam() != null) &&
                           !(prop.getURecurseUsersOfTeam().equals(uifield.getRecurseUsersOfTeam())))))
        {
            modified = true;
        }
        
        prop.setURecurseUsersOfTeam(uifield.getRecurseUsersOfTeam());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getUsersOfTeams()) && !uifield.getUsersOfTeams().equals(prop.getUUsersOfTeams())) ||
                          (StringUtils.isBlank(uifield.getUsersOfTeams()) && StringUtils.isNotBlank(prop.getUUsersOfTeams()))))
        {
            modified = true;
        }
        
        prop.setUTeamsOfTeams(uifield.getTeamsOfTeams());
        
        if (!modified && ((prop.getURecurseTeamsOfTeam() == null && uifield.getRecurseTeamsOfTeam() != null) || 
                          (prop.getURecurseTeamsOfTeam() != null && uifield.getRecurseTeamsOfTeam() == null) ||
                          ((prop.getURecurseTeamsOfTeam() != null && uifield.getRecurseTeamsOfTeam() != null) &&
                           !(prop.getURecurseTeamsOfTeam().equals(uifield.getRecurseTeamsOfTeam())))))
        {
            modified = true;
        }
        
        prop.setURecurseTeamsOfTeam(uifield.getRecurseTeamsOfTeam());
        
        if (!modified && ((prop.getUAllowAssignToMe() == null && uifield.getAllowAssignToMe() != null) || 
                          (prop.getUAllowAssignToMe() != null && uifield.getAllowAssignToMe() == null) || 
                          ((prop.getUAllowAssignToMe() != null && uifield.getAllowAssignToMe() != null) &&
                           !(prop.getUAllowAssignToMe().equals(uifield.getAllowAssignToMe())))))
        {
            modified = true;
        }
        
        prop.setUAllowAssignToMe(uifield.getAllowAssignToMe());
        
        if (!modified && ((prop.getUAutoAssignToMe() == null && uifield.getAutoAssignToMe() != null) ||
                          (prop.getUAutoAssignToMe() != null && uifield.getAutoAssignToMe() == null) ||
                          ((prop.getUAutoAssignToMe() != null && uifield.getAutoAssignToMe() != null) &&
                           !(prop.getUAutoAssignToMe().equals(uifield.getAutoAssignToMe())))))
        {
            modified = true;
        }
        
        prop.setUAutoAssignToMe(uifield.getAutoAssignToMe());

        if (!modified && ((StringUtils.isNotBlank(uifield.getTeamPickerTeamsOfTeams()) && !uifield.getTeamPickerTeamsOfTeams().equals(prop.getUTeamPickerTeamsOfTeams())) ||
                          (StringUtils.isBlank(uifield.getTeamPickerTeamsOfTeams()) && StringUtils.isNotBlank(prop.getUTeamPickerTeamsOfTeams()))))
        {
            modified = true;
        }
        
        prop.setUTeamPickerTeamsOfTeams(uifield.getTeamPickerTeamsOfTeams());

        
        if (!modified && ((StringUtils.isNotBlank(uifield.getFileUploadTableName()) && !uifield.getFileUploadTableName().equals(prop.getUFileUploadTableName())) ||
                        (StringUtils.isBlank(uifield.getFileUploadTableName()) && StringUtils.isNotBlank(prop.getUFileUploadTableName()))))
        {
            modified = true;
        }
        
        prop.setUFileUploadTableName(uifield.getFileUploadTableName());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getReferenceColumnName()) && !uifield.getReferenceColumnName().equals(prop.getUFileUploadReferenceColumnName())) ||
                          (StringUtils.isBlank(uifield.getReferenceColumnName()) && StringUtils.isNotBlank(prop.getUFileUploadReferenceColumnName()))))
        {
            modified = true;
        }
        
        prop.setUFileUploadReferenceColumnName(uifield.getReferenceColumnName());
        
        if (!modified && ((prop.getUAllowUpload() == null && uifield.getAllowUpload() != null) || 
                          (prop.getUAllowUpload() != null && uifield.getAllowUpload() == null) ||
                          ((prop.getUAllowUpload() != null && uifield.getAllowUpload() != null) &&
                           !(prop.getUAllowUpload().equals(uifield.getAllowUpload())))))
        {
            modified = true;
        }
        
        prop.setUAllowUpload(uifield.getAllowUpload());
        
        if (!modified && ((prop.getUAllowDownload() == null && uifield.getAllowDownload() != null) ||
                          (prop.getUAllowDownload() != null && uifield.getAllowDownload() == null) ||
                          ((prop.getUAllowDownload() != null && uifield.getAllowDownload() != null) &&
                           !(prop.getUAllowDownload().equals(uifield.getAllowDownload())))))
        {
            modified = true;
        }
        
        prop.setUAllowDownload(uifield.getAllowDownload());
        
        if (!modified && ((prop.getUAllowRemove() == null && uifield.getAllowRemove() != null) ||
                          (prop.getUAllowRemove() != null && uifield.getAllowRemove() == null) ||
                          ((prop.getUAllowRemove() != null && uifield.getAllowRemove() == null) &&
                           !(prop.getUAllowRemove().equals(uifield.getAllowRemove())))))
        {
            modified = true;
        }
        
        prop.setUAllowRemove(uifield.getAllowRemove());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getAllowedFileTypes()) && !uifield.getAllowedFileTypes().equals(prop.getUAllowFileTypes())) ||
                          (StringUtils.isBlank(uifield.getAllowedFileTypes()) && StringUtils.isNotBlank(prop.getUAllowFileTypes()))))
        {
            modified = true;
        }
        
        prop.setUAllowFileTypes(uifield.getAllowedFileTypes());

        // reference grid
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getRefGridReferenceByTable()) && !uifield.getRefGridReferenceByTable().equals(prop.getURefGridReferenceByTable())) ||
                          (StringUtils.isBlank(uifield.getRefGridReferenceByTable()) && StringUtils.isNotBlank(prop.getURefGridReferenceByTable()))))
        {
            modified = true;
        }
        
        prop.setURefGridReferenceByTable(uifield.getRefGridReferenceByTable());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getRefGridSelectedReferenceColumns()) && !uifield.getRefGridSelectedReferenceColumns().equals(prop.getURefGridSelectedReferenceColumns())) ||
                          (StringUtils.isBlank(uifield.getRefGridSelectedReferenceColumns()) && StringUtils.isNotBlank(prop.getURefGridSelectedReferenceColumns()))))
        {
            modified = true;
        }
        
        prop.setURefGridSelectedReferenceColumns(uifield.getRefGridSelectedReferenceColumns());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getRefGridReferenceColumnName()) && !uifield.getRefGridReferenceColumnName().equals(prop.getURefGridReferenceColumnName())) ||
                          (StringUtils.isBlank(uifield.getRefGridReferenceColumnName()) && StringUtils.isNotBlank(prop.getURefGridReferenceColumnName()))))
        {
            modified = true;
        }
        
        prop.setURefGridReferenceColumnName(uifield.getRefGridReferenceColumnName());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getAllowReferenceTableAdd()) && !uifield.getAllowReferenceTableAdd().equals(prop.getUAllowReferenceTableAdd())) ||
                          (StringUtils.isBlank(uifield.getAllowReferenceTableAdd()) && StringUtils.isNotBlank(prop.getUAllowReferenceTableAdd()))))
        {
            modified = true;
        }
        
        prop.setUAllowReferenceTableAdd(uifield.getAllowReferenceTableAdd());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getAllowReferenceTableRemove()) && !uifield.getAllowReferenceTableRemove().equals(prop.getUAllowReferenceTableRemove())) ||
                          (StringUtils.isBlank(uifield.getAllowReferenceTableRemove()) && StringUtils.isNotBlank(prop.getUAllowReferenceTableRemove()))))
        {
            modified = true;
        }
        
        prop.setUAllowReferenceTableRemove(uifield.getAllowReferenceTableRemove());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getAllowReferenceTableView()) && !uifield.getAllowReferenceTableView().equals(prop.getUAllowReferenceTableView())) ||
                          (StringUtils.isBlank(uifield.getAllowReferenceTableView()) && StringUtils.isNotBlank(prop.getUAllowReferenceTableView()))))
        {
            modified = true;
        }
        
        prop.setUAllowReferenceTableView(uifield.getAllowReferenceTableView());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getReferenceTableUrl()) && !uifield.getReferenceTableUrl().equals(prop.getUReferenceTableUrl())) ||
                          (StringUtils.isBlank(uifield.getReferenceTableUrl()) && StringUtils.isNotBlank(prop.getUReferenceTableUrl()))))
        {
            modified = true;
        }
        
        prop.setUReferenceTableUrl(uifield.getReferenceTableUrl());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getRefGridViewPopup()) && !uifield.getRefGridViewPopup().equals(prop.getURefGridViewPopup())) ||
                          (StringUtils.isBlank(uifield.getRefGridViewPopup()) && StringUtils.isNotBlank(prop.getURefGridViewPopup()))))
        {
            modified = true;
        }
        
        prop.setURefGridViewPopup(uifield.getRefGridViewPopup());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getRefGridViewPopupWidth()) && !uifield.getRefGridViewPopupWidth().equals(prop.getURefGridViewPopupWidth())) ||
                          (StringUtils.isBlank(uifield.getRefGridViewPopupWidth()) && StringUtils.isNotBlank(prop.getURefGridViewPopupWidth()))))
        {
            modified = true;
        }
        
        prop.setURefGridViewPopupWidth(uifield.getRefGridViewPopupWidth());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getRefGridViewPopupHeight()) && !uifield.getRefGridViewPopupHeight().equals(prop.getURefGridViewPopupHeight())) ||
                          (StringUtils.isBlank(uifield.getRefGridViewPopupHeight()) && StringUtils.isNotBlank(prop.getURefGridViewPopupHeight()))))
        {
            modified = true;
        }
        
        prop.setURefGridViewPopupHeight(uifield.getRefGridViewPopupHeight());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getRefGridViewTitle()) && !uifield.getRefGridViewTitle().equals(prop.getURefGridViewPopupTitle())) ||
                          (StringUtils.isBlank(uifield.getRefGridViewTitle()) && StringUtils.isNotBlank(prop.getURefGridViewPopupTitle()))))
        {
            modified = true;
        }
        
        prop.setURefGridViewPopupTitle(uifield.getRefGridViewTitle());
        
        // section control
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getSectionColumns()) && !uifield.getSectionColumns().equals(prop.getUSectionColumns())) ||
                          (StringUtils.isBlank(uifield.getSectionColumns()) && StringUtils.isNotBlank(prop.getUSectionColumns()))))
        {
            modified = true;
        }
        
        prop.setUSectionColumns(uifield.getSectionColumns());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getSectionLayout()) && !uifield.getSectionLayout().equals(prop.getUSectionLayout())) ||
                          (StringUtils.isBlank(uifield.getSectionLayout()) && StringUtils.isNotBlank(prop.getUSectionLayout()))))
        {
            modified = true;
        }
        
        prop.setUSectionLayout(uifield.getSectionLayout());
        
        if (!modified && (prop.getUSectionNumberOfColumns() != uifield.getSectionNumberOfColumns()))
        {
            modified = true;
        }
        
        prop.setUSectionNumberOfColumns(uifield.getSectionNumberOfColumns());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getSectionTitle()) && !uifield.getSectionTitle().equals(prop.getUSectionTitle())) ||
                          (StringUtils.isBlank(uifield.getSectionTitle()) && StringUtils.isNotBlank(prop.getUSectionTitle()))))
        {
            modified = true;
        }
        
        prop.setUSectionTitle(uifield.getSectionTitle());
        
        if (!modified && ((StringUtils.isNotBlank(uifield.getSectionType()) && !uifield.getSectionType().equals(prop.getUSectionType())) ||
                          (StringUtils.isBlank(uifield.getSectionType()) && StringUtils.isNotBlank(prop.getUSectionType()))))
        {
            modified = true;
        }
        
        prop.setUSectionType(uifield.getSectionType());
        
        if (!modified && ((prop.getUShowHorizontalRule() == null && uifield.getShowHorizontalRule() != null) ||
                          (prop.getUShowHorizontalRule() != null && uifield.getShowHorizontalRule() == null) ||
                          ((prop.getUShowHorizontalRule() != null && uifield.getShowHorizontalRule() != null) &&
                           !(prop.getUShowHorizontalRule().equals(uifield.getShowHorizontalRule())))))
        {
            modified = true;
        }
        
        prop.setUShowHorizontalRule(uifield.getShowHorizontalRule());
        
        if (!modified && ((prop.getUAllowJournalEdit() == null && uifield.getAllowJournalEdit() != null) ||
                          (prop.getUAllowJournalEdit() != null && uifield.getAllowJournalEdit() == null) ||
                          ((prop.getUAllowJournalEdit() != null && uifield.getAllowJournalEdit() != null) &&
                           !(prop.getUAllowJournalEdit().equals(uifield.getAllowJournalEdit())))))
        {
            modified = true;
        }
        
        prop.setUAllowJournalEdit(uifield.getAllowJournalEdit());
        
        if (!modified && ((prop.getUWidgetColumns() != null && !prop.getUWidgetColumns().equals(uifield.getWidgetColumns())) ||
                          (prop.getUWidgetColumns() == null && uifield.getWidgetColumns() != null)))
        {
            modified = true;
        }
        
        prop.setUWidgetColumns(uifield.getWidgetColumns());

        if (!modified && ((prop.getUIsHidden() != null && !prop.getUIsHidden().booleanValue() == uifield.isHidden()) ||
                          prop.getUIsHidden() == null))
        {
            modified = true;
        }
        
        prop.setUIsHidden(uifield.isHidden());
        
        if (!modified && ((prop.getUIsReadOnly() != null && !prop.getUIsReadOnly().booleanValue() == uifield.isReadOnly()) ||
                          prop.getUIsReadOnly() == null))
        {
            modified = true;
        }
        
        prop.setUIsReadOnly(uifield.isReadOnly());

        if (!modified && ((StringUtils.isNotBlank(uifield.getTooltip()) && !uifield.getTooltip().equals(prop.getUTooltip())) ||
                          (StringUtils.isBlank(uifield.getTooltip()) && StringUtils.isNotBlank(prop.getUTooltip()))))
        {
            modified = true;
        }
        
        prop.setUTooltip(uifield.getTooltip());
        
        if (!modified && isModifiedForTransientFields(prop, uifield))
        {
              modified = true;
        }
          
        prop.setTransientFieldsJsonString(MetaFieldProperties.createTransientFieldsJSONfromVO(prop));
        
        if (modified || StringUtils.isBlank(prop.getSys_id()))
        {
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug((prop.getSys_id() != null ? prop.getSys_id() + " " : "new ") + "meta_field_properties " + 
                              (prop.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
            }
            
            //HibernateUtil.getSessionFactory().getCurrentSession().evict(prop);
            try
            {
                HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().persist(prop);
            }
            catch(NonUniqueObjectException nuoe)
            {
                Log.log.debug("Non uniqueue exception while persisting meta_field_properties for " + prop.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                HibernateUtil.getCurrentSession().merge(prop);
            }
        }

        // dependencies for the properties
        if (isCustomForm)
        {
            prop.setMetaxFieldDependencys(convertViewToMetaxFieldDependency(prop, uifield.getDependencies(), prop.getMetaxFieldDependencys()));
        }

        // set metastyle - TODO - not sure we will do this or not
        // prop.setMetaStyle(convertRSMetaSytleToDBModel(uifield.getRsStyle(),
        // prop.getMetaStyle()));

        HibernateUtil.getCurrentSession().merge(prop);
        
        return prop;
    }// convertRSMetaSourceToDbMOdel
    
    private static boolean isModifiedForTransientFields(MetaFieldProperties prop, RsUIField item) {
        
        boolean modified = false;
        
        try {
            List<Method> propMethods = MetaFieldProperties.getTransientMethods();
            List<Method> UIMethods = RsUIField.getGetMethodList();

            for(Method method:propMethods) {
                String methodName = method.getName();
                String fieldName = MetaFieldProperties.getFieldName(methodName);
                String UIMethodName = "get" + fieldName.substring(1);

                Method UIMethod = RsUIField.lookupMethod(UIMethodName, UIMethods);
                if(UIMethod == null)
                    throw new Exception("UI method: " + UIMethodName + " cannot be found.");
                
                Object UICall = UIMethod.invoke(item, (Object[])null);
                Object call = method.invoke(prop, (Object[])null);
                
                String UIMethodCall = null;
                String methodCall = null;
                
                if(UICall != null && !(UICall instanceof String))
                    UIMethodCall = UICall.toString();
                else
                    UIMethodCall = (String)UICall;
                
                if(call != null && !(call instanceof String))
                    methodCall = call.toString();
                else
                    methodCall = (String)call;
                
                if((StringUtils.isNotBlank(UIMethodCall) && !UIMethodCall.equals(methodCall)) ||
                   (StringUtils.isBlank(UIMethodCall) && StringUtils.isNotBlank(methodCall)))
                    modified = true;
            } // end of for(method).
        } catch(Exception e) {
            Log.log.error("Transient method loading or invocation error: ", e);
            throw new RuntimeException("Transient method loading or invocation error: ", e);
        }
        
        return modified;
    }
    
    private static Collection<MetaxFieldDependency> convertViewToMetaxFieldDependency(MetaFieldProperties parentMetaFieldProperties, Collection<RsFieldDependency> viewObjs, Collection<MetaxFieldDependency> dbObjs)
    {
        Collection<MetaxFieldDependency> newDbObjs = new ArrayList<MetaxFieldDependency>();

        if (viewObjs != null)
        {
            for (RsFieldDependency item : viewObjs)
            {
                MetaxFieldDependency dbItem = getMetaxFieldDependency(item, dbObjs);

                boolean modified = dbItem.getMetaFieldProperties() != null && dbItem.getMetaFieldProperties().getSys_id().equalsIgnoreCase(parentMetaFieldProperties.getSys_id()) ? false : true;
                
                if (!modified && ((StringUtils.isNotBlank(item.getAction()) && !item.getAction().equals(dbItem.getUAction())) ||
                                  (StringUtils.isBlank(item.getAction()) && StringUtils.isNotBlank(dbItem.getUAction()))))
                {
                    modified = true;
                }
                
                dbItem.setUAction(item.getAction());
                
                if (!modified && ((StringUtils.isNotBlank(item.getCondition()) && !item.getCondition().equals(dbItem.getUCondition())) ||
                                  (StringUtils.isBlank(item.getCondition()) && StringUtils.isNotBlank(dbItem.getUCondition()))))
                {
                    modified = true;
                }
                
                dbItem.setUCondition(item.getCondition());
                
                if (!modified && ((StringUtils.isNotBlank(item.getTarget()) && !item.getTarget().equals(dbItem.getUTarget())) ||
                                  (StringUtils.isBlank(item.getTarget()) && StringUtils.isNotBlank(dbItem.getUTarget()))))
                {
                    modified = true;
                }
                
                dbItem.setUTarget(item.getTarget());
                
                if (!modified && ((StringUtils.isNotBlank(item.getValue()) && !item.getValue().equals(dbItem.getUValue())) ||
                                  (StringUtils.isBlank(item.getValue()) && StringUtils.isNotBlank(dbItem.getUValue()))))
                {
                    modified = true;
                }
                
                dbItem.setUValue(item.getValue());
                
                if (!modified && ((StringUtils.isNotBlank(item.getActionOptions()) && !item.getActionOptions().equals(dbItem.getUActionOptions())) ||
                                  (StringUtils.isBlank(item.getActionOptions()) && StringUtils.isNotBlank(dbItem.getUActionOptions()))))
                {
                    modified = true;
                }
                
                dbItem.setUActionOptions(item.getActionOptions());
                
                dbItem.setMetaFieldProperties(parentMetaFieldProperties);

                // persist it right away
                
                if (modified)
                {
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ") + "metax_field_dependency associated with meta_field_properties " + 
                                      (dbItem.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
                    }
                    
                    try
                    {
                        HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dbItem);
                    }
                    catch(NonUniqueObjectException nuoe)
                    {
                        Log.log.debug("Non uniqueue exception while persisting metax_field_dependency for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                        HibernateUtil.getCurrentSession().merge(dbItem);
                    }
                }
                
                HibernateUtil.getCurrentSession().merge(dbItem);
                
                newDbObjs.add(dbItem);
            }// end of for loop
        }
        else
        {
            newDbObjs = null;
        }

        // clenaup that is not required
        filterOutMetaxFieldDependencyToDelete(newDbObjs, dbObjs);

        return newDbObjs;
    }
    
    private MetaSource convertINPUTUIToDbModel(RsUIField item, MetaSource dbItem)
    {
        boolean modified = false;
        
        if (dbItem == null)
        {
            dbItem = new MetaSource();
            modified = true;
        }

        if (!modified && ((StringUtils.isNotBlank(item.getName()) && !item.getName().equals(dbItem.getUName())) ||
                          (StringUtils.isBlank(item.getName()) && StringUtils.isNotBlank(dbItem.getUName()))))
        {
            modified = true;
        }
        
        dbItem.setUName(item.getName());
        
        if (!modified && ((StringUtils.isNotBlank(item.getDisplayName()) && !item.getDisplayName().equals(dbItem.getUDisplayName())) ||
                          (StringUtils.isBlank(item.getDisplayName()) && StringUtils.isNotBlank(dbItem.getUDisplayName()))))
        {
            modified = true;
        }
        
        dbItem.setUDisplayName(item.getDisplayName());
        
        dbItem.setUSource(DataSource.INPUT.name());
        dbItem.setMetaFormView(dbView);
        
        if (!modified && ((StringUtils.isNotBlank(item.getDbtype()) && !item.getDbtype().equals(dbItem.getUDbType())) ||
                          (StringUtils.isBlank(item.getDbtype()) && StringUtils.isNotBlank(dbItem.getUDbType()))))
        {
            modified = true;
        }
        
        dbItem.setUDbType(item.getDbtype());

        // u_type is same as the u_ui_type
        // dbItem.setUType(item.getRstype());

        // persist it
        if (modified)
        {
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ")  + "meta_source " + 
                              (dbItem.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
            }
            
            //HibernateUtil.getSessionFactory().getCurrentSession().clear();
            try
            {
                HibernateUtil.getDAOFactory().getMetaSourceDAO().persist(dbItem);
            }
            catch(NonUniqueObjectException nuoe)
            {
                Log.log.debug("Non uniqueue exception while persisting meta_source for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                HibernateUtil.getCurrentSession().merge(dbItem);
            }
            
            if (StringUtils.isNotBlank(dbItem.getSys_id()))
            {
                item.setId(dbItem.getSys_id());
            }
        }
        
        /*
         * NOT SURE WHY Hibenrate Session is flushed in the middle
         */
        //HibernateUtil.getCurrentSession().flush();

        dbItem.setMetaFieldProperties(convertINPUTUIToDbModelProperties(item, dbItem, dbItem.getMetaFieldProperties(), true));

        HibernateUtil.getCurrentSession().merge(dbItem);
        
        return dbItem;
    }
    
    private Collection<MetaxFieldDependency> convertViewToMetaxFieldDependencyForTab(MetaFormTab parentMetaFormTab, Collection<RsFieldDependency> viewObjs, Collection<MetaxFieldDependency> dbObjs)
    {
        Collection<MetaxFieldDependency> newDbObjs = new ArrayList<MetaxFieldDependency>();

        if (viewObjs != null)
        {
            for (RsFieldDependency item : viewObjs)
            {
                MetaxFieldDependency dbItem = getMetaxFieldDependency(item, dbObjs);
                
                boolean modified = dbItem.getMetaFormTab() != null && dbItem.getMetaFormTab().getSys_id().equalsIgnoreCase(parentMetaFormTab.getSys_id()) ? false : true;
                
                if (!modified && ((StringUtils.isNotBlank(item.getAction()) && !item.getAction().equals(dbItem.getUAction())) ||
                                  (StringUtils.isBlank(item.getAction()) && StringUtils.isNotBlank(dbItem.getUAction()))))
                {
                    modified = true;
                }
                
                dbItem.setUAction(item.getAction());
                
                if (!modified && ((StringUtils.isNotBlank(item.getCondition()) && !item.getCondition().equals(dbItem.getUCondition())) ||
                                  (StringUtils.isBlank(item.getCondition()) && StringUtils.isNotBlank(dbItem.getUCondition()))))
                {
                    modified = true;
                }
                
                dbItem.setUCondition(item.getCondition());
                
                if (!modified && ((StringUtils.isNotBlank(item.getTarget()) && !item.getTarget().equals(dbItem.getUTarget())) ||
                                  (StringUtils.isBlank(item.getTarget()) && StringUtils.isNotBlank(dbItem.getUTarget()))))
                {
                    modified = true;
                }
                
                dbItem.setUTarget(item.getTarget());
                
                if (!modified && ((StringUtils.isNotBlank(item.getValue()) && !item.getValue().equals(dbItem.getUValue())) ||
                                  (StringUtils.isBlank(item.getValue()) && StringUtils.isNotBlank(dbItem.getUValue()))))
                {
                    modified = true;
                }
                
                dbItem.setUValue(item.getValue());
                
                if (!modified && ((StringUtils.isNotBlank(item.getActionOptions()) && !item.getActionOptions().equals(dbItem.getUActionOptions())) ||
                                  (StringUtils.isBlank(item.getActionOptions()) && StringUtils.isNotBlank(dbItem.getUActionOptions()))))
                {
                    modified = true;
                }
                
                dbItem.setUActionOptions(item.getActionOptions());
                
                dbItem.setMetaFormTab(parentMetaFormTab);

                // persist it right away
                if (modified)
                {
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug((dbItem.getSys_id() != null ? dbItem.getSys_id() + " " : "new ") + "metax_field_dependency associated with meta_form_tab " + 
                                      (dbItem.getSys_id() != null ? "modified" : "added") + ", persisting!!!");
                    }
                    
                    try
                    {
                        HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dbItem);
                    }
                    catch(NonUniqueObjectException nuoe)
                    {
                        Log.log.debug("Non uniqueue exception while persisting metax_field_dependency for " + dbItem.getSys_id() + ", " + nuoe.getIdentifier() + ", " + nuoe.getEntityName());
                        HibernateUtil.getCurrentSession().merge(dbItem);
                    }
                }
                
                HibernateUtil.getCurrentSession().merge(dbItem);
                
                newDbObjs.add(dbItem);
            }// end of for loop
        }
        else
        {
            newDbObjs = null;
        }

        // clenaup that is not required
        filterOutMetaxFieldDependencyToDelete(newDbObjs, dbObjs);

        return newDbObjs;
    }
    
    private void cleanup()
    {
        deleteMetaSourceThatAreNotReferencedInForm();
    }

    private void deleteMetaSourceThatAreNotReferencedInForm()
    {
        Collection<MetaSource> metaSources = this.dbView.getMetaSources();
        if (metaSources != null && metaSources.size() > 0)
        {
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("Count of meta_source for " + dbView.getUDisplayName() + " is " + metaSources.size() + ", valid meta_source size is " + validMetaSourceSysIds.size());
            }
            
            for (MetaSource metaSource : metaSources)
            {
                String sysId = metaSource.getSys_id();
                if (!this.validMetaSourceSysIds.contains(sysId))
                {
                    // delete this metasource record as it is no longer
                    // referenced on the form
                    //HibernateUtil.getSessionFactory().getCurrentSession().clear();
                    HibernateUtil.getCurrentSession().merge(metaSource);
                    
                    if (Log.log.isDebugEnabled())
                    {
                        Log.log.debug("deleting meta_source for " + metaSource.getUName() + ", sys_id = " + metaSource.getSys_id());
                    }
                    deleteMetaSource(metaSource);
                }
            }
        }// end of if
    }
    
    private void deleteMetaSource(MetaSource metaSource)
    {
        if (metaSource != null)
        {
            DeleteMetaForm.deleteMetaFieldProperties(metaSource.getMetaFieldProperties(), null);

            HibernateUtil.getDAOFactory().getMetaSourceDAO().delete(metaSource);
        }

    }
    
    private void postSaveOperations()
    {
        //update the SectionContainerField with all the references 
        updateSectionContainerFieldJson();
        
        // TODO:post operation
        // 1) create new wiki document for this form
        createWikiDocument(viewx.getViewName(), viewx.getWikiName());

        // update the name of the form in the template
        updateTheTemplateTable();
        
        // index custom form
        indexMetaForm();
    }
    
    private void indexMetaForm() {
        CustomFormIndexAPI.indexCustomForm(dbView.getSys_id(), userId);
    }

	private void createWikiDocument(String viewName, String wikiName)
    {
        if (StringUtils.isNotBlank(wikiName))
        {
            String fullName = wikiName.trim();
            String sectionId = GenericSectionUtil.createSectionId();
            String sectionContent = prepareSectionContentFor(viewName, sectionId);

            // find the document
            boolean isWikidocExist = ServiceWiki.isWikidocExist(fullName, true);
            if(!isWikidocExist)
            {
                // create doc with fullName
                createDocumentWithName(fullName, sectionContent, this.userId);
            }
        }
    }
    
    private String prepareSectionContentFor(String viewName, String sectionId)
    {
        StringBuffer sb = new StringBuffer();

        sb.append("{section:type=").append(SectionType.FORM.getTagName()).append("|title=").append(viewName).append("|id=").append(sectionId).append("}").append("\n");

        sb.append("#set($sys_id = $request.getParameter(\"sys_id\"))").append("\n");
        sb.append(HtmlUtil.createFormScript(GenericSectionUtil.createSectionId() + "_formviewer", StringUtils.isNotEmpty(viewName) ? viewName.trim() : null, null, "$!sys_id", null, 0));
        sb.append("\n");

        sb.append("{section}");

        return sb.toString();
    }

    private void createDocumentWithName(String fullName, String sectionContent, String userId)
    {
        String[] arr = fullName.split("\\.");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ConstantValues.USERNAME, userId);
        params.put(ConstantValues.WIKI_NAMESPACE_KEY, arr[0]);
        params.put(ConstantValues.WIKI_DOCNAME_KEY, arr[1]);
        params.put(ConstantValues.WIKI_CONTENT_KEY, sectionContent);

        params.put(ConstantValues.READ_ROLES_KEY, this.viewx.getViewRoles());
        params.put(ConstantValues.WRITE_ROLES_KEY, this.viewx.getEditRoles());
        params.put(ConstantValues.ADMIN_ROLES_KEY, this.viewx.getAdminRoles());

        try
        {
        	createWiki(params);
        }
        catch (Exception e)
        {
            Log.log.error("Error creating wiki :" + fullName, e);
        }

    }
    
    private void updateTheTemplateTable()
    {
        // this form can be a template..so update the name in the template table
        if (this.dbView != null)
        {
            String sql = "update WikiDocumentMetaFormRel set UFormName ='" + this.dbView.getUViewName() + "' where form = '" + this.dbView.getSys_id() + "'";
            try
            {
                ServiceHibernate.executeHQLUpdate(sql, userId);
            }
            catch (Throwable t)
            {
                Log.log.error("Error in updating the template with the form name " + this.dbView.getUViewName(), t);
            }
        }
    }
    
    private static void filterOutTabsToDelete(Collection<MetaFormTab> newDbObjs, Collection<MetaFormTab> dbObjs)
    {
        Log.log.debug("Size of Tabs in Form is " + newDbObjs.size() + ", size of Tabs in DB for the Form is " + 
                      (dbObjs != null ? dbObjs.size() : "null"));
        
        if(dbObjs != null && dbObjs.size() > 0)
        {
            for(MetaFormTab tab : dbObjs)
            {
                if(shouldDeleteMetaFormTab(newDbObjs, tab))
                {
                    deleteMetaFormTab(tab);
                }

            }//end of for
        }//end of if
    }//filterOutTabsToDelete
    
    private static boolean shouldDeleteMetaFormTab(Collection<MetaFormTab> newDbObjs, MetaFormTab tab)
    {
       boolean shouldDelete = true;
       String dbId = tab.getSys_id();

       if (newDbObjs != null)
       {
           for(MetaFormTab newTab : newDbObjs)
           {
               String newId = newTab.getSys_id();
               if(StringUtils.isNotEmpty(newId))
               {
                   if(newId.equals(dbId))
                   {
                       shouldDelete = false;
                       break;
                   }
               }
           }//end of for loop
       }
       
       return shouldDelete;

    }//shouldDeleteMetaFormTab
    
    private static void deleteMetaFormTab(MetaFormTab metaFormTab)
    {
        //delete the tab items
        Collection<MetaFormTabField> metaFormTabFields = metaFormTab.getMetaFormTabFields();
        if(metaFormTabFields != null)
        {
            for(MetaFormTabField tabField : metaFormTabFields)
            {
                deleteMetaFormTabField(tabField);
            }
        }

        //finally delete the tab
        HibernateUtil.getDAOFactory().getMetaFormTabDAO().delete(metaFormTab);
    }//deleteMetaFormTab
    
    private static void filterOutControlItemsToDelete(Collection<MetaControlItem> newDbObjs, Collection<MetaControlItem> dbObjs)
    {
        if(dbObjs != null && dbObjs.size() > 0)
        {
            for(MetaControlItem controlItem : dbObjs)
            {
                if(shouldDeleteMetaControlItem(newDbObjs, controlItem))
                {
                    HibernateUtil.getCurrentSession().merge(controlItem);
                    deleteMetaControlItem(controlItem);
                }

            }//end of for
        }//end of if

    }//filterOutControlItemsToDelete
    
    private static boolean shouldDeleteMetaControlItem(Collection<MetaControlItem> newDbObjs, MetaControlItem controlItem)
    {
       boolean shouldDelete = true;
       
       if (newDbObjs != null)
       {
           String dbId = controlItem.getSys_id();
    
           for(MetaControlItem newControlItem : newDbObjs)
           {
               String newId = newControlItem.getSys_id();
               if(StringUtils.isNotEmpty(newId))
               {
                   if(newId.equals(dbId))
                   {
                       shouldDelete = false;
                       break;
                   }
               }
           }//end of for loop
       }
       
       return shouldDelete;

    }//shouldDelete
    
    private static void deleteMetaControlItem(MetaControlItem controlItem)
    {
        Collection<MetaFormAction> actions = controlItem.getMetaFormActions();
        if(actions != null && actions.size() > 0)
        {
            for(MetaFormAction action : actions)
            {
                //HibernateUtil.getSessionFactory().getCurrentSession().clear();
                HibernateUtil.getCurrentSession().merge(action);
                deleteMetaFormAction(action);
            }
        }

        //delete it right away
        HibernateUtil.getDAOFactory().getMetaControlItemDAO().delete(controlItem);

    }//markTodeleteMetaControlItem
    
    private static void deleteMetaFormAction(MetaFormAction metaFormAction)
    {
        HibernateUtil.getDAOFactory().getMetaFormActionDAO().delete(metaFormAction);
    }//deleteMetaFormAction
    
    private static void filterOutFormActionItemsToDelete(Collection<MetaFormAction> newDbObjs, Collection<MetaFormAction> dbObjs)
    {
        if(dbObjs != null && dbObjs.size() > 0)
        {
            for(MetaFormAction controlItem : dbObjs)
            {
                if(shouldDeleteMetaFormAction(newDbObjs, controlItem))
                {
                    deleteMetaFormAction(controlItem);
                }

            }//end of for
        }//end of if

    }//filterOutControlItemsToDelete
    
    private static boolean shouldDeleteMetaFormAction(Collection<MetaFormAction> newDbObjs, MetaFormAction controlItem)
    {
       boolean shouldDelete = true;
       String dbId = controlItem.getSys_id();

       for(MetaFormAction newControlItem : newDbObjs)
       {
           String newId = newControlItem.getSys_id();
           if(StringUtils.isNotEmpty(newId))
           {
               if(newId.equals(dbId))
               {
                   shouldDelete = false;
                   break;
               }
           }
       }//end of for loop

       return shouldDelete;

    }//shouldDelete
    
    private static MetaControlItem findMetaControlItem(String id, Collection<MetaControlItem> dbObjs)
    {
        MetaControlItem result = null;

        if(dbObjs != null)
        {
            for(MetaControlItem item : dbObjs)
            {
                if(item.getSys_id().equalsIgnoreCase(id))
                {
                    result = item;
                    break;
                }
            }
        }

        return result;
    }//findMetaControlItem
    
    private static  MetaFormAction findMetaFormAction(String id, Collection<MetaFormAction> dbObjs)
    {
        MetaFormAction result = null;

        if(dbObjs != null)
        {
            for(MetaFormAction item : dbObjs)
            {
                if(item.getSys_id().equalsIgnoreCase(id))
                {
                    result = item;
                    break;
                }
            }//end of for
        }//end of if

        return result;
    }//findMetaFormAction
    
    
    private static MetaFormTab findMetaFormTab(String id, Collection<MetaFormTab> dbObjs)
    {
        MetaFormTab result = null;

        if (dbObjs != null)
        {
            for(MetaFormTab item : dbObjs)
            {
                if(item.getSys_id().equalsIgnoreCase(id))
                {
                    result = item;
                    break;
                }
            }
        }

        return result;
    }//findMetaFormTab
    
    /**
     * Get list of all the fields that are of type SectionContainerField
     * Get the sectionColumn for that field
     * get the list of columns that are referenced by this section column and convert them to RsUIField 
     * add new set of RsUIField to this 
     */
    private void updateSectionContainerFieldJson()
    {
        List<RsUIField> inputFields = this.viewx.filterINPUTFields();
        for(RsUIField inputField : inputFields)
        {
            String uiType = inputField.getUiType();
            if(uiType.equalsIgnoreCase(CustomFormUIType.SectionContainerField.name()))
            {
                //this is the one that we want to work on
                updateSectionContainerField(inputField);
            }
        }
    }
    
    private void updateSectionContainerField(RsUIField inputField)
    {
        String sectionColumnsJson = inputField.getSectionColumns();
        if(StringUtils.isNotBlank(sectionColumnsJson))
        {
            List<JSONObject> updatedFields = new ArrayList<JSONObject>();
            boolean jsonUpdated = false;
            
            //convert them to RsUIField list 
            JSONArray cols = StringUtils.stringToJSONArray(sectionColumnsJson);
            // iterate through the fields in the column an convert
            // them to base models and add them to fields list
            for (int i = 0; i < cols.size(); i++)
            {
                List<RsUIField> updatedControls = new ArrayList<RsUIField>();
                JSONObject col = cols.getJSONObject(i);
                JSONArray columnFields = (JSONArray) col.get(HibernateConstants.META_FIELD_PROP_SECTION_COLUMN_CONTROLS);
                for (int j = 0; j < columnFields.size(); j++)
                {
                    JsonConfig jsonConfig = new JsonConfig();
                    jsonConfig.setRootClass(RsUIField.class);
                    RsUIField rf = (RsUIField) JSONSerializer.toJava(columnFields.getJSONObject(j), jsonConfig);
                    if (rf != null)
                    {
                        String id = rf.getId();
                        
                        if(StringUtils.isEmpty(id))
                        {
                            jsonUpdated = true;
                            
                            //than update this
                            rf = getUpdatedFieldInfo(rf);
                        }
                        
                        updatedControls.add(rf);
                    }//END OF if
                }//end of for loop
                
                //update the section columns, convert the list to json string
                JSONObject o = new JSONObject();
                o.put(HibernateConstants.META_FIELD_PROP_SECTION_COLUMN_CONTROLS, updatedControls);
                
                updatedFields.add(o);
                
            }//end of outer for loop
            
            if (jsonUpdated)                
            {
                //prepare the updated json string
                sectionColumnsJson = convertObjectToJson(updatedFields);
                
                //save the section column info
                saveSectionColumns(inputField, sectionColumnsJson);
            }
            
        }//end of if
    }
    
    private RsUIField getUpdatedFieldInfo(RsUIField inputField)
    {
        String fieldName = inputField.getName();
        String type = inputField.getSourceType();
        RsUIField dbField = null;
        
        if(type.equalsIgnoreCase(HibernateConstantsEnum.DB.getTagName()))
        {
            //get the meta field
            MetaField field = findField(fieldName, this.customTable.getMetaFields());
            
            //convert to RsUIField
            dbField = ExtMetaFormLookup.convertMetaFieldToRsUIField(null, field.doGetVO(), null, RightTypeEnum.admin, "admin", null, null);
        }
        
        if(dbField == null)
        {
            dbField = inputField;
        }
        
        //return 
        return dbField;
    }
    
    
    private MetaField findField(String fieldName, Collection<MetaField> fields)
    {
        MetaField result =  null;
        
        for(MetaField field : fields)
        {
            if(field.getUName().equalsIgnoreCase(fieldName))
            {
                result = field;
                break;
            }
        }
        
        return result;
    }
    
    private String convertObjectToJson(Object updatedFields)
    {
        String json = JSONSerializer.toJSON(updatedFields).toString();
        return json;
    }
    
    private void saveSectionColumns(RsUIField sourceField, String sectionColumnsJson)
    {
        String sysId = sourceField.getId();
        
        try
        {   
          HibernateProxy.setCurrentUser(this.userId);
            HibernateProxy.execute(() -> {
            	MetaSource source = null;

	            // we have to do this as hibernate does not qry for ids
	            source = HibernateUtil.getDAOFactory().getMetaSourceDAO().findById(sysId);
	            if (source != null)
	            {
	                source.getMetaFieldProperties().setUSectionColumns(sectionColumnsJson);
	            }

            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);            
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }
    
    // api to go through all the widgets and do the pre requisite steps
    private void doWidgetOperations() throws Exception
    {
        if (this.viewx.getPanels() != null)
        {
            for (RsPanelDTO panel : this.viewx.getPanels())
            {
                // just a panel
                if (panel.getColumns() != null)
                {
                    preprocessFields(panel.getColumns());
                }
                // has tab panels
                else if (panel.getTabs() != null)
                {
                    for (RsTabDTO tab : panel.getTabs())
                    {
                        if (tab.getColumns() != null)
                        {
                            preprocessFields(tab.getColumns());
                        }
                    }
                }
            }// end of for
        }
    }
    
    private void preprocessFields(List<RsColumnDTO> columns) throws Exception
    {
        for (RsColumnDTO column : columns)
        {
            if (column.getFields() != null)
            {
                for (RsUIField field : column.getFields())
                {
                    String uiType = field.getUiType();

                    if (CustomFormUIType.FileUploadField.name().equalsIgnoreCase(uiType))
                    {
                        prepareFileUploadField(field);
                    }

                    if (CustomFormUIType.TabContainerField.name().equalsIgnoreCase(uiType))
                    {
                        // Check to see if the popup contains a FileManager type
                        String titles = field.getRefGridViewTitle();
                        if (titles != null && titles.length() > 0)
                        {
                            String[] vals = titles.split("\\|");
                            for (String val : vals)
                            {
                                if ("FileManager".equalsIgnoreCase(val))
                                {
                                    prepareFileUploadField(field);
                                }
                            }
                        }
                    }
                    
                    if (CustomFormUIType.SectionContainerField.name().equalsIgnoreCase(uiType))
                    {
                        String sectionColumnsJson = field.getSectionColumns();
                        
                        //for fields in the Section
                        if(StringUtils.isNotBlank(sectionColumnsJson))
                        {
                            List<JSONObject> updatedFields = new ArrayList<JSONObject>();

                            //convert them to RsUIField list 
                            JSONArray cols = StringUtils.stringToJSONArray(sectionColumnsJson);
                            // iterate through the fields in the column an convert
                            // them to base models and add them to fields list
                            for (int i = 0; i < cols.size(); i++)
                            {
                                List<RsUIField> updatedControls = new ArrayList<RsUIField>();
                                JSONObject col = cols.getJSONObject(i);
                                JSONArray columnFields = (JSONArray) col.get(HibernateConstants.META_FIELD_PROP_SECTION_COLUMN_CONTROLS);
                                for (int j = 0; j < columnFields.size(); j++)
                                {
                                    JsonConfig jsonConfig = new JsonConfig();
                                    jsonConfig.setRootClass(RsUIField.class);
                                    RsUIField rf = (RsUIField) JSONSerializer.toJava(columnFields.getJSONObject(j), jsonConfig);
                                    
                                    if (rf != null)
                                    {
                                        if (CustomFormUIType.FileUploadField.name().equalsIgnoreCase(rf.getUiType()))
                                        {
                                            prepareFileUploadField(rf);
                                        }
                                        
                                        updatedControls.add(rf);
                                    }//END OF if
                                }//end of for loop
                                
                                //update the section columns, convert the list to json string
                                JSONObject o = new JSONObject();
                                o.put(HibernateConstants.META_FIELD_PROP_SECTION_COLUMN_CONTROLS, updatedControls);
                                
                                updatedFields.add(o);
                                
                            }//end of outer for loop
                            
                            //change the obj to json
                            sectionColumnsJson = convertObjectToJson(updatedFields);
                            
                            //update the field
                            field.setSectionColumns(sectionColumnsJson);
                        }
                    }
                }// end of for loop
            }// end of if
        }// end of for loop
    }
    
    private void prepareFileUploadField(RsUIField field) throws Exception
    {
        // create it only if it is new field
        if (StringUtils.isBlank(field.getId()))
        {
            // create the table
            String tableName = field.getFileUploadTableName();
            String customTableName = this.viewx.getTableName();
            if (StringUtils.isNotBlank(customTableName))
            {
                String referenceColumnName = prepareRefColumnNameBasedOnTableName(customTableName);//"u_" + customTableName.toLowerCase().substring(0)ddd + "_sys_id";
                // set the value of the column
                field.setFileUploadTableName(tableName);
                field.setReferenceColumnName(referenceColumnName);
                // create the table
                new CreateFileUploadCustomTable(tableName, referenceColumnName, null, null).create();
                // update the referenceTable field
                if (StringUtils.isNotBlank(referenceTableNames))
                {
                    referenceTableNames += "," + tableName;
                }
                else
                {
                    referenceTableNames = tableName;
                }
            }
        }
    }
    
    private void createCustomTableFromForm() throws Exception
    {
        this.viewx.setReferenceTableNames(referenceTableNames);

        //New Implementation
        String customTableName = this.viewx.getTableName();
        if (StringUtils.isNotEmpty(customTableName))
        {
            RsCustomTableDTO customTableMetaData = this.viewx.u_getCustomTable();

            System.out.println("this is to update the meta tables");

            try
            {
                ServiceHibernate.createCustomTable(customTableMetaData, userId, null);
            }
            catch (Throwable e)
            {
                Log.log.error("Error in updating the meta tables when form is updated for form:" + this.viewx.getDisplayName(), e);
                throw new Exception(e.getMessage());
            }
        }        
        
    }
    
    private void createWiki(Map<String, Object> params)
    {
        if (MapUtils.isEmpty(params)) {
        	return;
        }
    	
        String username = (String) params.get(ConstantValues.USERNAME);
        String namespace = (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String docname = (String) params.get(ConstantValues.WIKI_DOCNAME_KEY);
        if (StringUtils.isNotBlank(username))
        {
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(username);
                
                ServiceWiki.createWiki(params, username);
            }
            catch (Exception e)
            {
                Log.log.error("Error in creating doc :" + namespace + "." + docname, e);
            }
        }

    }
    
    private void validateUser(String username) throws Exception
    {
        //Make sure that user is a valid resolve user
        if(!username.equalsIgnoreCase("resolve.maint"))
        {
            boolean isValidUser = ServiceHibernate.isValidUser(username);
            if(!isValidUser)
            {
                throw new Exception("User " + username + " does not exist");
            }
        }
    }
    
    /**
     * API to be called from update script ONLY!
     */
    private static final String FORM_READ_ERR_MSG = "Error while reading RsFormDTO for a form %s";
    public static void updateAllFormsForChecksum()
    {
        try
        {
            List<ComboboxModelDTO> customFormNamesWithIds = CustomFormUtil.getAllMetaFormViewNamesWithIds();
            customFormNamesWithIds.stream().forEach(model -> {
                try
                {
                    RsFormDTO viewx = ServiceHibernate.getMetaFormView(model.getValue(), model.getName(), null, "admin", RightTypeEnum.admin, null, false);
                    populateFormJsonTreeAndChecksum(viewx);
                }
                catch (Exception e)
                {
                    Log.log.error(String.format(FORM_READ_ERR_MSG, model.getName()), e);
                }
            });
        }
        catch(Exception e)
        {
            Log.log.error("Error while getting all form names", e);
        }
    }
    
    private static final String HQL = "update MetaFormView set formJsonTree = :jsonTree, checksum = :checksum where sys_id = :id";
    private static final String JSON_TREE = "jsonTree";
    private static final String CHECKSUM = "checksum";
    private static final String ID = "id";
    private static final String MATCH_IDS_FROM_JSON_REGEX = "\"(id|tableSysId|dbcolumnId)\":(\"\\S{32}\"|null),";
    private static final String ERROR_MSG = "Error while populating json tree and checksum for a form '%s'";
    private static final void populateFormJsonTreeAndChecksum(RsFormDTO viewx)
    {
        if (viewx != null)
        {
            JSONObject jsonObject = new ObjectMapper().convertValue(viewx, JSONObject.class);
            if (jsonObject != null)
            {
                try
                {
                    String tree = jsonObject.toString().replaceAll(MATCH_IDS_FROM_JSON_REGEX,"");
                    Integer checksum = Objects.hash(tree);
                     HibernateProxy.execute(() -> {
                    	 HibernateUtil.createQuery(HQL).setParameter(JSON_TREE, tree).setParameter(CHECKSUM, checksum)
                         .setParameter(ID, viewx.getId()).executeUpdate(); 
                     });
                }
                catch (Exception e)
                {
                    Log.log.error(String.format(ERROR_MSG, viewx.getName()), e);
                                      HibernateUtil.rethrowNestedTransaction(e);
                }
            }
        }
    }
}
