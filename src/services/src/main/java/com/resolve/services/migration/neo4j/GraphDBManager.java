/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.neo4j;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.IndexHits;

import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class GraphDBManager
{
    // 
	private static final String WEB_HOME = "";
	//private static final String WEB_HOME = RSContext.getWebHome();//"C:/project/resolve3/dist/tomcat/webapps/resolve/";// RSContext.getWebHome()
    private static final String NEO4J_PROPERTIES_FILE = WEB_HOME + "WEB-INF/neo4j.properties";
    private static final String NEO4J_DB_FOLDER = WEB_HOME + "WEB-INF/graphdb";

    
    public static final String SYS_ID = "sys_id";
    public static final String DISPLAYNAME = "displayName";
    public static final String TYPE = "NODE_TYPE";
    public static final String GRAPH_RELATION_TYPE = "GRAPH_REL_TYPE";

    private static GraphDBManager graphDbManager;
    private static GraphDatabaseService neo4jDB;
    private static Properties configNeo = null;

    private GraphDBManager() throws Exception
    {
        connect();
    }

    public static GraphDBManager getInstance() throws Exception
    {
        if (graphDbManager == null)
        {
            synchronized (GraphDBManager.class)
            {
                if (graphDbManager == null)
                {
                    verifyNeo4j();
                    graphDbManager = new GraphDBManager();
                }
            }
        }

        return graphDbManager;
    }
    
    public Node findNodeByIndexedID(String id)
    {
        Node nodeLocal = null;

        if(StringUtils.isNotEmpty(id))
        {
            IndexHits<Node> nodes = neo4jDB.index().getNodeAutoIndexer().getAutoIndex().get(SYS_ID, id);
            for(Node node: nodes)
            {
                if(node.getProperty(SYS_ID).equals(id))
                {
                    nodeLocal = node;
                    break;
                }
            }
        }

        return nodeLocal;
    }
    

    /**
     * returns the root node of the specific type
     * 
     * @param type
     * @return
     * @throws Exception
     */
    public Node findRootNodeFor(SocialRelationshipTypes type) throws Exception
    {
        Node rootNodeOfType = null;
        if(type == null)
        {
            throw new Exception("No type to lookup");
        }
        
        Transaction tx = null;
        try
        {
            tx = neo4jDB.beginTx();

            //get the root node...always starts from 0
            Node rootNode = neo4jDB.getNodeById(0);
            if (rootNode == null)
            {
                throw new Exception("Root node for Neo4j does not exist.");
            }

            Iterable<Relationship> rootRels = rootNode.getRelationships(type, Direction.OUTGOING);
            if (rootRels != null)
            {
                //root node of the worksheet or process or ...
                rootNodeOfType = rootRels.iterator().next().getOtherNode(rootNode);
            }

            tx.success();
        }
        catch (Exception t)
        {
            Log.log.error("Error in finding nodes:", t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }
        
        return rootNodeOfType;
    }
    
    public Set<Node> findAllNodesFor(SocialRelationshipTypes type) throws Exception
    {
        Set<Node> result = new HashSet<Node>();
        Transaction tx = null;
        try
        {
            tx = neo4jDB.beginTx();

            //get the root node...always starts from 0
            Node rootNode = neo4jDB.getNodeById(0);
            if (rootNode == null)
            {
                throw new Exception("Root node for Neo4j does not exist.");
            }

            Iterable<Relationship> rootRels = rootNode.getRelationships(type, Direction.OUTGOING);
            if (rootRels != null)
            {
                //root node of the worksheet or process or ...
                Node rootNodeOfType = rootRels.iterator().next().getOtherNode(rootNode);

                SocialRelationshipTypes subType = type;
                if (type == SocialRelationshipTypes.PROCESSES)
                {
                    subType = SocialRelationshipTypes.PROCESS;
                }
                else if(type == SocialRelationshipTypes.TEAMS)
                {
                    subType = SocialRelationshipTypes.TEAM;
                }
                else if(type == SocialRelationshipTypes.FORUMS)
                {
                    subType = SocialRelationshipTypes.FORUM;
                }
                else if(type == SocialRelationshipTypes.USERS)
                {
                    subType = SocialRelationshipTypes.USER;
                }
                else if(type == SocialRelationshipTypes.ACTIONTASKS)
                {
                    subType = SocialRelationshipTypes.ACTIONTASK;
                }

                //get all the nodes connected to this rootNodeOfType
                Iterable<Relationship> rels = rootNodeOfType.getRelationships(subType, Direction.OUTGOING);
                for (Relationship rel : rels)
                {
                    result.add(rel.getOtherNode(rootNodeOfType));
                }
            }

            tx.success();
        }
        catch (Exception t)
        {
            Log.log.error("Error in finding nodes:", t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return result;
    }

    private void connect() throws Exception
    {
        //read the properties
        configNeo = new Properties(NEO4J_PROPERTIES_FILE);
        configNeo.put(org.neo4j.kernel.configuration.Config.NODE_AUTO_INDEXING, "true");
        configNeo.put(org.neo4j.kernel.configuration.Config.RELATIONSHIP_AUTO_INDEXING, "true");

        //for Standalone
        neo4jDB = new GraphDatabaseFactory().newEmbeddedDatabase(NEO4J_DB_FOLDER);
        
        Log.log.info("Neo4j DB Initialized Successfully");
    }
    
    private static void verifyNeo4j() throws Exception
    {
        File graphPropFile = new File(NEO4J_PROPERTIES_FILE);
        File graphFolder = new File(NEO4J_DB_FOLDER);
        
        if(graphPropFile.exists() && graphFolder.exists())
        {
            //this is good...so proceed
        }
        else
        {
            throw new Exception("No GraphDB !! Nothing to migrate.");
        }
        
    }

    public void shutdown() throws Exception
    {
        try
        {
            if (neo4jDB != null)
            {
                Log.log.info("Shutdown graphdb");
                neo4jDB.shutdown();
                neo4jDB = null;
            }
        }
        catch (Throwable t)
        {
            Log.log.info(t, t);
        }
    }

}
