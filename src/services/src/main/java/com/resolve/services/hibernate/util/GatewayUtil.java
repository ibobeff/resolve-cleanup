/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.hibernate.query.Query;
import org.hibernate.type.StringType;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.DatabaseConnectionPool;
import com.resolve.persistence.model.EWSAddress;
import com.resolve.persistence.model.EmailAddress;
import com.resolve.persistence.model.GatewayFilter;
import com.resolve.persistence.model.MSGGatewayFilter;
import com.resolve.persistence.model.ModelToVO;
import com.resolve.persistence.model.NameProperty;
import com.resolve.persistence.model.PullGatewayFilter;
import com.resolve.persistence.model.PushGatewayFilter;
import com.resolve.persistence.model.RemedyxForm;
import com.resolve.persistence.model.SSHPool;
import com.resolve.persistence.model.XMPPAddress;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.CustomTableAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.DatabaseConnectionPoolVO;
import com.resolve.services.hibernate.vo.EWSAddressVO;
import com.resolve.services.hibernate.vo.EmailAddressVO;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.NamePropertyVO;
import com.resolve.services.hibernate.vo.RemedyxFormVO;
import com.resolve.services.hibernate.vo.ResolveBlueprintVO;
import com.resolve.services.hibernate.vo.SSHPoolVO;
import com.resolve.services.hibernate.vo.TelnetPoolVO;
import com.resolve.services.hibernate.vo.XMPPAddressVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.BeanUtil;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;

public class GatewayUtil
{
    private static volatile GatewayUtil instance = null;

    public static GatewayUtil getInstance()
    {
        if (instance == null)
        {
            instance = new GatewayUtil();
        }
        return instance;
    }

    /**
     * This method returns a List of queue name from a gateway table.
     *
     * @param modelName
     *            the object that includes actual hibernate entity name
     * @return {@link Set} of {@link String}s representing queue names
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Set<String> getAllQueue(String modelName) throws Exception
    {
        Set<String> result = new TreeSet<String>();

        if (StringUtils.isBlank(modelName))
        {
            throw new Exception("Missing modelName");
        }
        else
        {
            try
            {
            	HibernateProxy.executeNoCache(() -> {
                    String hql = "select obj from " + modelName + " obj where obj.UQueue <> '" + Constants.DEFAULT_GATEWAY_QUEUE_NAME + "' order by obj.UQueue";
                    Query hqlQuery = HibernateUtil.createQuery(hql);

                    List<ModelToVO<GatewayVO>> rows = hqlQuery.list();

                    for (ModelToVO<GatewayVO> row : rows)
                    {
                        GatewayVO vo = (GatewayVO) row.doGetVO();
                        result.add(vo.getUQueue());
                    }
            	});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e.getMessage());
            }
        }
        return result;
    }

    @SuppressWarnings("rawtypes")
    public String deleteByQueue(String entityName, Map filters) throws Exception
    {
        // the incoming Map must have an item with key QUEUE
        String queue = (String) filters.get("QUEUE");
        if (StringUtils.isBlank(queue))
        {
            throw new Exception("Missing QUEUE name");
        }
        // once we read the queue, we don't need the Map item.
        filters.remove("QUEUE");

        try
        {
            HibernateProxy.execute( () -> {
            	Query query = HibernateUtil.createQuery("DELETE FROM " + entityName + " WHERE UQueue = :UQueue");
                query.setParameter("UQueue", queue, StringType.INSTANCE);
                query.executeUpdate();
            
            });

            // remove all entries for queue
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }

        return queue;
    } // deleteByQueue

    public String getGatewayType(String gatewayName) throws Exception {
        
        final Map<String, String> typeByBlueprints = new HashMap<String, String>();

        final List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("Found %d blueprint.properties", 
        								CollectionUtils.isNotEmpty(blueprints) ? blueprints.size() : 0));
        }
        
        ResolveBlueprintVO foundBluePrintVO = null;
        
        if (CollectionUtils.isNotEmpty(blueprints)) {
        	foundBluePrintVO = blueprints.parallelStream().filter(blueprintVO -> {
        		if (blueprintVO == null) {
        			return false;
        		}
        		
        		if (MapUtils.isNotEmpty(typeByBlueprints)) {
        			return false;
        		}
        		
	            Properties properties = new Properties();
	            try {
					properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));
					
					if (Log.log.isDebugEnabled()) {
						Log.log.debug(String.format("Found %d properties from blueprint.properties Guid: %s, " +
													"Local Host:%s", (properties != null ? properties.size() : 0), 
													blueprintVO.getUGuid(), blueprintVO.getULocalhost()));
					}
				} catch (IOException e) {
					Log.log.error(String.format("Failed to load blueprint properties from Guid: %s, Local Host: %s" +
												blueprintVO.getUGuid(), blueprintVO.getULocalhost()));
					return false;
				}
	
	            if (properties.containsKey("rsremote.instance.count")) {
	                int rmtInstCnt = 0;
	                
	                try {
	                    rmtInstCnt = Integer.parseInt(properties.getProperty("rsremote.instance.count", "0"));
	                } catch(NumberFormatException nfe) {
	                    Log.log.warn("Blueprint property rsremote.instance.count value " + 
	                                 properties.getProperty("rsremote.instance.count") + 
	                                 " is not a number!!!");
	                }
	                
	                if (rmtInstCnt > 0) {
	                	if (Log.log.isDebugEnabled()) {
	                		Log.log.debug(String.format("Found %d remote instance counts in blueprint.properties " +
	                									"Guid: %s, Local Host:%s", rmtInstCnt, blueprintVO.getUGuid(),
	                									blueprintVO.getULocalhost()));
	                	}
	                	
	                    for (int i = 1; i <= rmtInstCnt; i++) {
	                        if (properties.containsKey("rsremote.instance" + i + ".name")) {
	                            String rmtInstName = properties.getProperty("rsremote.instance" + i + ".name");
	                            
	                            if (Log.log.isDebugEnabled()) {
	                            	Log.log.debug(String.format("Remote Instance(%d) Name: %s", i, rmtInstName));
	                            }
	                            
	                            String packageName = rmtInstName + ".receive."+ gatewayName.toLowerCase() + ".package";
	                            String packageValue = (String)properties.get(packageName);
	                            
	                            if(StringUtils.isBlank(packageValue)) { // Legacy
	                            	if (Log.log.isDebugEnabled()) {
	                            		Log.log.debug(String.format("Package value is blank. Gateway %s is legacy gateway", 
	                            									gatewayName));
	                            	}
	                            	
	                            	String normalizedLegacyGatewayName = gatewayName;
	                            	
	                                if(gatewayName.equalsIgnoreCase("database") || gatewayName.equalsIgnoreCase("db")) {
	                                	normalizedLegacyGatewayName = "dbgw";
	                                }
	                                
	                                String key = rmtInstName + ".run."+ normalizedLegacyGatewayName.toLowerCase();
	                                
	                                if (Log.log.isDebugEnabled()) {
	                                	Log.log.debug(String.format("Key looking for is %s", key));
	                                }
	                                
	                                if(StringUtils.isNotBlank((String)properties.get(key))) {
	                                    if (Log.log.isDebugEnabled()) {
	                                    	Log.log.debug(String.format("Found blueprint property %s for gateway %s " +
	                                    								"(legacy gateway %s), gateway type is legacy " +
	                                    								"in blueprint.properties Guid: %s, Local Host: %s", 
	                                    								key, gatewayName, normalizedLegacyGatewayName,
	                                    								blueprintVO.getUGuid(), 
	                                    								blueprintVO.getULocalhost()));
	                                    								
	                                    }
	                                    
	                                    typeByBlueprints.put(blueprintVO.getUGuid(), "None");
	                                    return true;
	                                } else { 
	                                	if (Log.log.isDebugEnabled()) {
	                                	Log.log.debug(String.format("Missing blueprint property %s for gateway %s " +
	            														"(legacy gateway %s) in blueprint.properties Guid: " +
	            														"%s, Local Host: %s", key, gatewayName, 
	            														normalizedLegacyGatewayName, blueprintVO.getUGuid(),
	                                									blueprintVO.getULocalhost()));
	                                }
	                                	
	                                	// If filter model exists then it is legacy gateway filter
	                                	
	                                	if (StringUtils.isNotBlank(HibernateUtil.getGatewayModelFilter(gatewayName))) {
	                                		typeByBlueprints.put(blueprintVO.getUGuid(), "None");
	                                		return true;
	                                	} else {
	                                		Log.log.warn(String.format("Failed to find gateway model filter for gateway " +
	                                								   "%s (legacy gateway %s), gateway type is legacy",
	                                								   gatewayName, normalizedLegacyGatewayName));
	                                	}
	                                }
	                            } else if(StringUtils.isNotBlank(packageValue)) { // SDK 1.0/2.0
	                                if(packageValue.indexOf("com.resolve.gateway.push.") != -1) {
	                                	typeByBlueprints.put(blueprintVO.getUGuid(), "Push");
	                                    
	                                    if (Log.log.isDebugEnabled()) {
	                                    	Log.log.debug(String.format("Gateway %s, package %s type is SDK2 %s", 
	                                    								gatewayName, packageValue, 
	                                    								typeByBlueprints.get(blueprintVO.getUGuid())));
	                                    }
	                                    
	                                    return true;
	                                } else if(packageValue.indexOf("com.resolve.gateway.pull.") != -1) {
	                                	typeByBlueprints.put(blueprintVO.getUGuid(), "Pull");
	                                    
	                                    if (Log.log.isDebugEnabled()) {
	                                    	Log.log.debug(String.format("Gateway %s, package %s type is SDK2 %s", 
	                                    								gatewayName, packageValue, 
	                                    								typeByBlueprints.get(blueprintVO.getUGuid())));
	                                    }
	                                    
	                                    return true;
	                                } else if(packageValue.indexOf("com.resolve.gateway.msg.") != -1) {
	                                	typeByBlueprints.put(blueprintVO.getUGuid(), "MSG");
	                                    
	                                    if (Log.log.isDebugEnabled()) {
	                                    	Log.log.debug(String.format("Gateway %s, package %s type is SDK2 %s", 
	                                    								gatewayName, packageValue, 
	                                    								typeByBlueprints.get(blueprintVO.getUGuid())));
	                                    }
	                                    
	                                    return true;
	                                } else { // SDK 1.0
	                                	typeByBlueprints.put(blueprintVO.getUGuid(), "None");
	                                	
	                                    if (Log.log.isDebugEnabled()) {
	                                    	Log.log.debug(String.format("Gateway %s, package %s is SDK1", gatewayName, 
	                                    								packageValue));
	                                    }
	                                    
	                                    return true;
	                                }
	                            }
	                        }	                        
	                    }
	                    
	                }
	            }
	            
	            return false;
	        }).findAny().orElse(null);
        }
        
        if (foundBluePrintVO == null || MapUtils.isEmpty(typeByBlueprints)) {
           throw new Exception(String.format("Failed to identify gateway type (legacy, SDK1, SDK2) for gateway %s from " +
        		   							 "rsremote configurations found in blueprint(s) in databsse.", gatewayName));
        }
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("getGatewayType(%s) returning %s", gatewayName, 
        								typeByBlueprints.get(foundBluePrintVO.getUGuid())));
        }
        
        return typeByBlueprints.get(foundBluePrintVO.getUGuid());
    }
    
    public String getSDKGatewayType(String gatewayName) throws Exception {
        
    	Map<String, String> gatewayTypeByBlueprints = new HashMap<String, String>();
	  
    	if(StringUtils.isBlank(gatewayName)) {
    		throw new IllegalArgumentException("gatewayName");
    	}
        
    	List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
        
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Found %d blueprint.properties", 
    									CollectionUtils.isNotEmpty(blueprints) ? blueprints.size() : 0));
    	}
    
        ResolveBlueprintVO foundBluePrintVO = null;
        
        if (CollectionUtils.isNotEmpty(blueprints)) {
        	foundBluePrintVO = blueprints.parallelStream().filter(blueprintVO -> {
        		if (blueprintVO == null) {
        			return false;
        		}
        		
        		if (MapUtils.isNotEmpty(gatewayTypeByBlueprints)) {
        			return false;
        		}
        		
	            Properties properties = new Properties();
	            try {
					properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));
				} catch (IOException e) {
					Log.log.error(String.format("Failed to load blueprint properties from Guid: %s, Local Host: %s, " +
												"Content:[%s]", blueprintVO.getUGuid(), blueprintVO.getULocalhost(),
												blueprintVO.getUBlueprint()));
					return false;
				}
	
	            if (properties.containsKey("rsremote.instance.count")) {
	                int rmtInstCnt = 0;
	                
	                try {
	                    rmtInstCnt = Integer.parseInt(properties.getProperty("rsremote.instance.count", "0"));
	                } catch(NumberFormatException nfe) {
	                    Log.log.warn("Blueprint property rsremote.instance.count value " + 
	                                 properties.getProperty("rsremote.instance.count") + 
	                                 " is not a number!!!");
	                }
	                
	                if (rmtInstCnt > 0) {
	                	if (Log.log.isDebugEnabled()) {
	                		Log.log.debug(String.format("Found %d remote instance counts in blueprint.properties " +
	                									"Guid: %s, Local Host: %s", rmtInstCnt, blueprintVO.getUGuid(), 
	                									blueprintVO.getULocalhost()));
	                	}
	                	
	                    for (int i = 1; i <= rmtInstCnt; i++) {
	                        if (properties.containsKey(String.format("rsremote.instance%d.name", i))) {
	                            String rmtInstName = properties.getProperty("rsremote.instance" + i + ".name");
	                            
	                            if (Log.log.isDebugEnabled()) {
	                            	Log.log.debug(String.format("Remote instance %d name is %s", i, rmtInstName));
	                            }
	                            
	                            String packageName = rmtInstName + ".receive."+ gatewayName.toLowerCase() + ".package";
	                            String packageValue = (String)properties.get(packageName);
	                            
	                            if (StringUtils.isNotBlank(packageValue)) {
		                            if(packageValue.indexOf("com.resolve.gateway.push.") != -1) {
		                            	gatewayTypeByBlueprints.put(blueprintVO.getUGuid(), "push");
		                            	return true;
		                            } else  if(packageValue.indexOf("com.resolve.gateway.pull.") != -1) {
		                            	gatewayTypeByBlueprints.put(blueprintVO.getUGuid(), "pull");
		                            	return true;
		                            } else if(packageValue.indexOf("com.resolve.gateway.msg.") != -1) {
		                            	gatewayTypeByBlueprints.put(blueprintVO.getUGuid(), "msg");
		                                return true;
		                            } else {
		                            	gatewayTypeByBlueprints.put(blueprintVO.getUGuid(), "old");
		                            	return true;
		                            }
		                        }
	                        }
	                    }
	                }
	            }
	            
	            return false;
	        }).findAny().orElse(null);
        }
        
        if (foundBluePrintVO == null || MapUtils.isEmpty(gatewayTypeByBlueprints)) {
            throw new Exception(String.format("Failed to identify SDK2 gateway type (push, pull, msg) for gateway %s " +
            								  "from rsremote configurations found in blueprint(s) in databsse.",
            								  gatewayName));
        }
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("getSDKGatewayType(%s) returning %s", gatewayName, 
        								gatewayTypeByBlueprints.get(foundBluePrintVO.getUGuid())));
        }
        
        return gatewayTypeByBlueprints.get(foundBluePrintVO.getUGuid());
    }
    
    
    @SuppressWarnings("rawtypes")
    public void deleteNameProperty(String queueName, String nameProperty) throws Exception
    {
        try
        {
            HibernateProxy.executeNoCache(() -> {
            	// remove all entries for queue
                Query query = HibernateUtil.createQuery("DELETE FROM NameProperty WHERE UQueue = :UQueue AND UName = :UName");
                query.setParameter("UQueue", queueName, StringType.INSTANCE);
                query.setParameter("UName", nameProperty, StringType.INSTANCE);
                query.executeUpdate();
            });            
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
    } // deleteNameProperty

    public List<Map<String, Object>> getCustomTableData(String query)
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        // find the email or IM for the process
        QueryDTO queryDTO = new QueryDTO();
        queryDTO.setUseSql(true);
        queryDTO.setSqlQuery(query);
        try
        {
            result = CustomTableAPI.getRecordData(queryDTO);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }
    
    public List<GatewayVO> getAllFilter(QueryDTO query, String username) throws Exception {
        return getAllFilter(query, username, false);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<GatewayVO> getAllFilter(QueryDTO query, String username, boolean secured) throws Exception
    {
        boolean blockDBConnectionURL = false;
        String modelName = query.getModelName();
        if (StringUtils.isNotBlank(username) && !UserUtils.isAdminUser(username) && modelName.equalsIgnoreCase("DatabaseConnectionPool"))
        {
            blockDBConnectionURL = true;
        }
        
        username = StringUtils.isBlank(username) ? "system" : username;
        List<ModelToVO<GatewayVO>> rows = null;
        List<GatewayVO> result = new ArrayList<GatewayVO>();

        if (StringUtils.isBlank(query.getModelName()))
        {
            throw new Exception("Missing modelName");
        }
        else
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                rows = (List<ModelToVO<GatewayVO>>) HibernateProxy.executeNoCache(() -> {
                	
                	Query hqlQuery = HibernateUtil.createQuery(query);
                    return hqlQuery.list();
                });
                
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e.getMessage());
            }
            
            if (rows != null)
            {
                for (ModelToVO<GatewayVO> row : rows)
                {
                    GatewayVO gatewayVO = row.doGetVO(secured);
                    if (blockDBConnectionURL)
                    {
                        DatabaseConnectionPoolVO vo = (DatabaseConnectionPoolVO)gatewayVO;
                        vo.setUDBConnectURI(null);
                        gatewayVO = vo;
                        Log.log.info("Logged in user does not have admin role. So, the connection pool URL is blanked out.");
                    }
                    result.add(gatewayVO);
                }
            }
        }
        return result;
    }
    
    public List<GatewayVO> getAllFilters(String modelName, String queueName, String username) throws Exception {
        return getAllFilters(modelName, queueName, username, false);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<GatewayVO> getAllFilters(String modelName, String queueName, String username, boolean secured) throws Exception
    {
        List<GatewayVO> result = new ArrayList<GatewayVO>();
        
        try
        {
        	List<ModelToVO<GatewayVO>> rows = (List<ModelToVO<GatewayVO>>) HibernateProxy.executeNoCache(() -> {
        		Query hqlQuery = HibernateUtil.createQuery("from " + modelName + " where UQueue = :queueName")
                        .setParameter("queueName", queueName);
        
        		return hqlQuery.list();
        	});           
            
            if (rows != null)
            {
                for (ModelToVO<GatewayVO> row : rows)
                {
                    result.add(row.doGetVO(secured));
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        
        return result;
    }
    
    public GatewayVO getFilter(String modelName, String id, String username) throws Exception {
        return getFilter(modelName, id, username, false);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public GatewayVO getFilter(String modelName, String id, String username, boolean secured) throws Exception
    {
        GatewayVO result = null;
        ModelToVO<GatewayVO> row = null;
        
        boolean blockDBConnectionURL = false;
        if (StringUtils.isNotBlank(username) && !UserUtils.isAdminUser(username) && modelName.equalsIgnoreCase("DatabaseConnectionPool"))
        {
            blockDBConnectionURL = true;
        }

        try
        {
        	List<ModelToVO<GatewayVO>> rows = (List<ModelToVO<GatewayVO>>) HibernateProxy.executeNoCache(() -> {
        		Query hqlQuery = HibernateUtil.createQuery("select obj from " + modelName + " obj where obj.sys_id=:id")
                        .setParameter("id", id);

        		return hqlQuery.list();
           });

            

            if (rows != null && rows.size() == 1)
            {
                row = rows.get(0);
            }
            else
            {
                throw new Exception("Multiple rows returned for query: select obj from " + modelName + " obj where obj.sys_id=:id");
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        
        if (row != null)
        {
            GatewayVO gatewayVO = row.doGetVO(secured);
            if (blockDBConnectionURL)
            {
                DatabaseConnectionPoolVO vo = (DatabaseConnectionPoolVO)gatewayVO;
                vo.setUDBConnectURI(null);
                gatewayVO = vo;
                Log.log.info("Logged in user does not have admin role. So, the connection pool URL is blanked out.");
            }
            result = gatewayVO;
        }
        
        
        return result;
    }
    
    public GatewayVO getFilterByName(String modelName, String queueName, String name) throws Exception {
        return getFilterByName(modelName, queueName, name, false);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public GatewayVO getFilterByName(String modelName, String queueName, String name, boolean secured) throws Exception
    {
        GatewayVO result = null;

        try
        {
        	result = (GatewayVO) HibernateProxy.executeNoCache(() -> {
                //TODO very special case hence the hack here, NEED refactor later when we get some time
                String fieldName = "UName";
                if("EmailAddress".equals(modelName))
                {
                    fieldName = "UEmailAddress";
                }
                else if("EWSAddress".equals(modelName))
                {
                    fieldName = "UEWSAddress";
                }
                else if("DatabaseConnectionPool".equals(modelName))
                {
                    fieldName = "UPoolName";
                }
                else if("XMPPAddress".equals(modelName))
                {
                    fieldName = "UXMPPAddress";
                }
                else if("TelnetPool".equals(modelName))
                {
                    fieldName = "USubnetMask";
                }
                else if("SSHPool".equals(modelName))
                {
                    fieldName = "USubnetMask";
                }

                StringBuilder sb = new StringBuilder();
                sb.append("select obj from ").append(modelName);
                
                if(!StringUtils.isBlank(queueName))
                    sb.append(" obj where obj.UQueue='").append(queueName.trim()).append("' and");
                else
                    sb.append(" obj where");

                sb.append(" obj.").append(fieldName).append("='").append(name.trim()).append("'");
                
                Log.log.debug(sb);
                
                Query hqlQuery = HibernateUtil.createQuery(sb.toString());

                List<ModelToVO<GatewayVO>> rows = hqlQuery.list();

                if (rows != null)
                {
                    if(rows.size() > 1)
                    {
                        throw new Exception("Multiple rows returned for query:" + hqlQuery.getQueryString());
                    }
                    else if(rows.size() == 1)
                    {
                        ModelToVO<GatewayVO> row = rows.get(0);
                        return row.doGetVO();
                    }
                }

                return null;
        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        return result;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public String getFilterDeployed(String modelName, String gatewayName, String id) throws Exception {

        String result = "";
        
        String fieldName = "UName";
        if("EmailAddress".equals(modelName))
            fieldName = "UEmailAddress";

        else if("EWSAddress".equals(modelName))
            fieldName = "UEWSAddress";

        else if("DatabaseConnectionPool".equals(modelName))
            fieldName = "UPoolName";

        else if("XMPPAddress".equals(modelName))
            fieldName = "UXMPPAddress";

        else if("TelnetPool".equals(modelName))
            fieldName = "USubnetMask";

        else if("SSHPool".equals(modelName))
            fieldName = "USubnetMask";
            
        StringBuilder sb = new StringBuilder();
        sb.append("select obj1.").append(fieldName).append(" from ").append(modelName).append(" obj1, ").append(modelName).append(" obj2 ");
        sb.append("WHERE obj1.").append(fieldName).append("=obj2.").append(fieldName).append(" AND obj1.sys_id=:sysId");
        
        if(gatewayName != null)
            sb.append(" AND obj1.UGatewayName=:gateway").append(" AND obj2.UGatewayName=:gateway");
        
        try {
        	result = (String) HibernateProxy.executeNoCache(() -> {
        	   Query hqlQuery = HibernateUtil.createQuery(sb.toString());
               hqlQuery.setParameter("sysId", id, StringType.INSTANCE);
               if(gatewayName != null)
                   hqlQuery.setParameter("gateway", gatewayName, StringType.INSTANCE);

               List<String> rows = hqlQuery.list();

               if(rows != null) {
                   if(rows.size() > 1)
                       return (String)rows.get(0);
               } 
               return "";
           });           

        } catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        
        return result;
    }

    public boolean deleteModel(String modelName, String[] ids) throws Exception
    {
        boolean result = true;

        try
        {
        	HibernateProxy.executeNoCache(() -> {
        		HibernateUtil.delete(modelName, Arrays.asList(ids));
        	});

        }
        catch (Throwable e)
        {
            result = false;
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        return result;
    }

    public boolean deleteFilter(String modelName, String[] ids) throws Exception
    {
        return deleteModel(modelName, ids);
    }

    @SuppressWarnings("unchecked")
    public <T> Object saveFilter(String modelName, GatewayVO filterVo, String username) throws Exception
    {
        Object result = null;
        boolean inHibernateTransaction = false;
        
        if (StringUtils.isNotBlank(username) && !UserUtils.isAdminUser(username) && modelName.equalsIgnoreCase("DatabaseConnectionPool"))
        {
            throw new Exception("Only user with admin role is allowed to modify/save DB Conneection Pool.");
        }
        
        try
        {
            if (filterVo instanceof GatewayFilterVO) {
                int filterValidationErrCode = ((GatewayFilterVO)filterVo).isValid();
                if (filterValidationErrCode != 0) {
                    throw new Exception("Filter Validation Returned Error Code " + filterValidationErrCode + " : " + 
                                        ((GatewayFilterVO)filterVo).getValidationError(filterValidationErrCode));
                }
            }
            filterVo.setChecksum(calculateChecksum(filterVo, modelName));
            String fullModelName = "com.resolve.persistence.model." + modelName;
            BaseModel<GatewayVO> filterModel = BeanUtil.getInstance(fullModelName);

            String id = BeanUtil.getStringProperty(filterVo, "id");

            GatewayVO vo = (GatewayVO) filterVo;
            if (StringUtils.isNotBlank(id))
            {
                //we should find the model from DB
              HibernateProxy.setCurrentUser(username);
            	filterModel =(BaseModel<GatewayVO>) HibernateProxy.executeNoCache(() -> {
            		return (BaseModel<GatewayVO>) HibernateUtil.findById(fullModelName, id);
            	});
                 
                //TODO, in  a bizzare twist of event some untracebale (i couldn't find it) code changes the createdOn to
                //some weird number. With this hack it seems to work fine so far. It's guranteed that
                //this will fail as soon as someone changes the "untraceble" code in future.
                vo.setSysCreatedOn(DateUtils.convertGMTToLocal(vo.getSysCreatedOn().getTime(), TimeZone.getDefault().getID()));
                filterModel.setSysUpdatedOn(null);
            }
            else
            {
                vo.setSys_id(null);
                vo.setSysOrg(null);
                vo.setId(null);
                vo.setSysCreatedBy(username);
                vo.setSysUpdatedBy(username);
                vo.setSysModCount(0);
                GatewayVO tmpVo = getFilterByName(modelName, Constants.DEFAULT_GATEWAY_QUEUE_NAME, vo.getUniqueId());
                if(tmpVo != null)
                {
                    throw new Throwable("Record already exists: " + vo.getUniqueId());
                }
            }
            //TODO, this smells like hack :( why do we have "UNDEFINED" coming from UI? Anyhow, set null when value is "UNDEFINED"?
            replaceUndefined(vo);

            if(StringUtils.isBlank(vo.getUQueue()))
            {
                vo.setUQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
            }
            
            filterModel.applyVOToModel(filterVo);
            
            //validate if there is any
            validate(modelName, vo);
            
            BaseModel<GatewayVO> filterModelFinal = filterModel;
            
            BaseModel<T> savedModel = (BaseModel<T>) HibernateProxy.executeNoCache(() -> {
            	return (BaseModel<T>) HibernateUtil.saveObj(modelName, filterModelFinal);
            });
            
            if(savedModel != null)
            {
                result = savedModel.doGetVO();
            }
        }
        catch (Throwable e)
        {
            result = false;
            Log.log.error(e.getMessage(), e);
            
            throw new Exception(e.getMessage());
        }
        return result;
    }
    
    private static Integer calculateChecksum (Object filterVo, String modelName) {
        Integer checksum = null;
        if (filterVo instanceof GatewayFilterVO) {
            GatewayFilterVO gatewayFilterVO = (GatewayFilterVO)filterVo;
            checksum = gatewayFilterVO.hashCode();
        } else { // special case where Gateway type VO does not extends from GatewayFilterVO
            String voModel = String.format("%sVO", modelName);
            if (EnumUtils.isValidEnum(ImpexEnum.class, voModel)) {
                ImpexEnum modelEnum = ImpexEnum.valueOf(voModel);
                switch (modelEnum) {
                    case DatabaseConnectionPoolVO : {
                        checksum = ((DatabaseConnectionPoolVO)filterVo).hashCode();
                        break;
                    }
                    case EmailAddressVO : {
                        checksum = ((EmailAddressVO)filterVo).hashCode();
                        break;
                    }
                    case EWSAddressVO : {
                        checksum = ((EWSAddressVO)filterVo).hashCode();
                        break;
                    }
                    case RemedyxFormVO : {
                        checksum = ((RemedyxFormVO)filterVo).hashCode();
                        break;
                    }
                    case XMPPAddressVO : {
                        checksum = ((XMPPAddressVO)filterVo).hashCode();
                        break;
                    }
                    case SSHPoolVO : {
                        checksum = ((SSHPoolVO)filterVo).hashCode();
                    }
                    case TelnetPoolVO : {
                        checksum = ((TelnetPoolVO)filterVo).hashCode();
                    }
                    default : {
                        // Do nothing here
                    }
                }
            }
        }
        return checksum;
    }
    
    @SuppressWarnings({ "rawtypes" })
    public static Integer calculateChecksumForUpdate(Object modelObject, String modelName) {
        Integer checksum = null;
        
        if (modelObject instanceof GatewayFilter) {
            GatewayFilter filter = (GatewayFilter)modelObject;
            checksum = calculateChecksum(filter.doGetVO(), modelName);
        } else {
            ImpexEnum modelEnum = ImpexEnum.valueOf(modelName);
            switch (modelEnum) {
                case DatabaseConnectionPool : {
                    checksum = ((DatabaseConnectionPool)modelObject).doGetVO().hashCode();
                    break;
                }
                case EmailAddress : {
                    checksum = ((EmailAddress)modelObject).doGetVO().hashCode();
                    break;
                }
                case EWSAddress : {
                    checksum = ((EWSAddress)modelObject).doGetVO().hashCode();
                    break;
                }
                case RemedyxForm : {
                    checksum = ((RemedyxForm)modelObject).doGetVO().hashCode();
                    break;
                }
                case XMPPAddress : {
                    checksum = ((XMPPAddress)modelObject).doGetVO().hashCode();
                    break;
                }
                case SSHPool : {
                    checksum = ((SSHPool)modelObject).doGetVO().hashCode();
                    break;
                }
                case MSGGatewayFilter : {
                    checksum = GatewayFilterUtil.getMSGFilterChecksum(((MSGGatewayFilter)modelObject).doGetVO(), "admin");
                    break;
                }
                case PushGatewayFilter : {
                    checksum = GatewayFilterUtil.getPushFilterChecksum(((PushGatewayFilter)modelObject).doGetVO(), "admin");
                    break;
                }
                case PullGatewayFilter : {
                    checksum = GatewayFilterUtil.getPullFilterChecksum(((PullGatewayFilter)modelObject).doGetVO(), "admin");
                    break;
                }
                default : {
                    // Do nothing here
                }
            }
        }
        
        return checksum;
    }

    /**
     * This method is used by the message listener that receives message from RSRemote
     * and saves the filter into the database for a particular gateway.
     *
     * @param modelName
     * @param filters is Map&ltString, String&gt where value is a Map in String form.
     */
    @SuppressWarnings("unchecked")
    public void setGatewayObjects(String modelName, Map<String, String> filters)
    {
        try
        {
        	HibernateProxy.executeNoCache(() -> {
                deleteByQueue(modelName, filters);

                for (String filterStringMap : filters.values())
                {
                    String fullModelName = "com.resolve.persistence.model." + modelName;
                    BaseModel<GatewayVO> filterModel = BeanUtil.getInstance(fullModelName);

                    Map<String, String> filterMap = StringUtils.stringToMap(filterStringMap);

                    //e.g., EmailFilterVO
                    String fullVOName = "com.resolve.services.hibernate.vo." + modelName + "VO";
                    GatewayVO filterVo = BeanUtil.getInstance(fullVOName);
                    //now move data from the Map to this vo.
                    convert(filterMap, filterVo);

                    //apply the VO to model.
                    filterModel.applyVOToModel(filterVo);

                    HibernateUtil.saveObj(modelName, filterModel);
                    HibernateUtil.getCurrentSession().flush();
                }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setFilters

    /**
     * This method is used by the message listener that receives message from RSRemote
     * and saves the filter into the database for a particular gateway.
     *
     * @param modelName
     * @param filters is Map<String, String) where value is a Map in String form.
     */
    @SuppressWarnings("unchecked")
    public void setFilters(String modelName, Map<String, String> filters)
    {
        try
        {
        	HibernateProxy.executeNoCache(() -> {
                deleteByQueue(modelName, filters);

                for (String filterStringMap : filters.values())
                {
                    String fullModelName = "com.resolve.persistence.model." + modelName;
                    BaseModel<GatewayVO> filterModel = BeanUtil.getInstance(fullModelName);

                    Map<String, String> filterMap = StringUtils.stringToMap(filterStringMap);

                    //e.g., EmailFilterVO
                    String fullVOName = "com.resolve.services.hibernate.vo." + modelName + "VO";
                    GatewayVO filterVo = BeanUtil.getInstance(fullVOName);
                    //now move data from the Map to this vo.
                    convert(filterMap, filterVo);
                    String updatedBy = filterMap.get(Constants.SYS_UPDATED_BY);
                    if (StringUtils.isNotEmpty(updatedBy)) {
                        filterVo.setSysUpdatedBy(updatedBy);
                    }
                    //apply the VO to model.
                    filterModel.applyVOToModel(filterVo);

                    HibernateUtil.saveObj(modelName, filterModel);
                    HibernateUtil.getCurrentSession().flush();
                }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setFilters

    private void convert(Map<String, String> filterMapObj, Object filterVo)
    {
        Method[] methods = filterVo.getClass().getMethods();
        for (Method method : methods)
        {
            MappingAnnotation mappingAnnotation = method.getAnnotation(MappingAnnotation.class);
            if (mappingAnnotation != null)
            {
                try
                {
                    String columnName = mappingAnnotation.columnName();
                    Object value = filterMapObj.get(columnName);
                    //from gateway filter perspective we guarantee that
                    //filters are proper java bean and there are getter and setter method.
                    String propertyName = method.getName().substring(3);
                    if (value != null) //if the value is null ignore.
                    {
                        BeanUtil.setProperty(filterVo, propertyName, value);
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * Replaces the value VO.UNDEFINED with null.
     *
     * @param filterVo
     */
    private void replaceUndefined(Object filterVo)
    {
        Method[] methods = filterVo.getClass().getMethods();
        for (Method method : methods)
        {
            try
            {
                String methodName = method.getName();
                if (methodName.startsWith("get") && method.getParameterTypes().length == 0)
                {
                    Object value = method.invoke(filterVo);
                    if (value != null && value instanceof String)
                    {
                        if (VO.STRING_DEFAULT.equals(value.toString()))
                        {
                            Method setter = filterVo.getClass().getMethod("set" + methodName.substring(3), String.class);
                            setter.invoke(filterVo, "");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Erorr invoking set" + method.getName().substring(3) + " : " + e.getMessage());
            }
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<NamePropertyVO> getAllNameProperties(QueryDTO query) throws Exception
    {
        List<NamePropertyVO> result = new ArrayList<NamePropertyVO>();

        if (StringUtils.isBlank(query.getModelName()))
        {
            throw new Exception("Missing modelName");
        }
        else
        {
            try
            {
                HibernateProxy.executeNoCache(() -> {
                	Query hqlQuery = HibernateUtil.createQuery(query);

                    List<ModelToVO<NamePropertyVO>> rows = hqlQuery.list();

                    for (ModelToVO<NamePropertyVO> row : rows)
                    {
                        result.add(row.doGetVO());
                    }
                });
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e.getMessage());
            }
        }
        return result;
    }

    /**
     * This method saves the name properties but it first deletes them from the 
     * table and then saves them. So to remove name properties simply send a nameProperties
     * Map without "CONTENT". 
     * 
     * @param modelName
     * @param nameProperties
     */
    @SuppressWarnings("unchecked")
    public void setNameProperties(String modelName, Map<String, Object> nameProperties)
    {
        try
        {
            // the incoming Map must have an item with key QUEUE
            String queue = (String) nameProperties.get("QUEUE");
            String name = (String) nameProperties.get("NAME");
            if (StringUtils.isBlank(queue))
            {
                throw new Exception("Missing QUEUE name");
            }
            deleteNameProperty(queue, name);

            if(nameProperties != null && nameProperties.size() > 0)
            {
                // once we read the queue, we don't need the Map item.
                nameProperties.remove("QUEUE");

                HashMap<String, String> content = (HashMap<String, String>) HibernateProxy.executeNoCache(() -> {
                    //is a Map in String form
                    HashMap<String, String> c = (HashMap<String, String>) nameProperties.get("CONTENT");
                    NameProperty nameProperty = new NameProperty();
                    nameProperty.setUQueue(queue);
                    nameProperty.setUName(name);
                    nameProperty.setUContent(ObjectProperties.serialize(c));

                    HibernateUtil.saveObj("NameProperty", nameProperty);
                    return c; 
                    
                });
                
                if(Log.log.isDebugEnabled())
                {
                    Log.log.debug("Saving name properties for : " + name);
                    int i=1;
                    for(String key : content.keySet())
                    {
                        Object obj = ObjectProperties.loadObject(content.get(key));
                        if(obj == null)
                        {
                            //just a simple String property
                            Log.log.debug(i + ". String value for key : " + key);
                        }
                        else
                        {
                            Log.log.debug(i + ". Object value for key : " + key + ", type : " + obj.getClass());
                        }
                        i++;
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setFilters

    public boolean deleteNameProperty(String modelName, String[] ids) throws Exception
    {
        return deleteModel(modelName, ids);
    }

    public boolean saveNameProperty(String modelName, Object namePropertyVO, String username) throws Exception
    {
        boolean result = true;

        try
        {
            String fullModelName = "com.resolve.persistence.model." + modelName;           

            String id = BeanUtil.getStringProperty(namePropertyVO, "id");

            NamePropertyVO vo = (NamePropertyVO) namePropertyVO;
            
            HibernateProxy.executeNoCache(() -> {
            	 NameProperty namePropertyModel = BeanUtil.getInstance(fullModelName);
            	if (StringUtils.isNotBlank(id))
                {
                    //we should find the model from DB
                    namePropertyModel = (NameProperty) HibernateUtil.findById(fullModelName, id);
                    vo.setSysUpdatedBy(username);
                }
                else
                {
                    vo.setSys_id(null);
                    vo.setSysOrg(null);
                    vo.setId(null);
                    vo.setSysCreatedBy(username);
                    vo.setSysUpdatedBy(username);
                    vo.setSysCreatedOn(new Date());
                    vo.setSysUpdatedOn(new Date());
                    vo.setSysModCount(0);
                }

                namePropertyModel = BeanUtil.convert(vo, namePropertyModel);

                HibernateUtil.saveObj(modelName, namePropertyModel);
            });
        }
        catch (Throwable e)
        {
            result = false;
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        return result;
    }

    /**
     * This is a common validation method. From here we will differentiate the specific validator.
     * 
     * @param modelName
     * @param filterVo
     * @throws Exception
     */
    private void validate(String modelName, GatewayVO filterVo) throws Exception
    {
        if (StringUtils.isBlank(filterVo.getSys_id()))
        {
            String fieldName = "u_name";
            if("EmailAddress".equals(modelName))
            {
                fieldName = "u_emailaddress";
            }
            else if("EWSAddress".equals(modelName))
            {
                fieldName = "u_ewspassword";
            }
            else if("XMPPAddress".equals(modelName))
            {
                fieldName = "u_xmppaddress";
            }
            else if("TelnetPool".equals(modelName))
            {
                fieldName = "u_subnetmask";
            }
            else if("SSHPool".equals(modelName))
            {
                fieldName = "u_subnetmask";
            }
            List<Object> objList = new ArrayList<Object>();
            objList.add(filterVo.getUniqueId().toLowerCase());
            String whereClause = " where lower(" + fieldName + ") = ?";
            long count = GeneralHibernateUtil.getTotalRecordsIn(modelName, whereClause, objList);
            if (count > 0)
            {
                throw new Exception("Record already exists: " + filterVo.getUniqueId());
            }
        }
    }

}
