/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.resolve.services.jdbc.util.GenericJDBCHandler;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


/**
 * 
 *  Report for 
 *      Top Executed DT Reports
 * 
 * @author jeet.marwah
 *
 */
public class DTPathDetailReport
{
    @SuppressWarnings("unused")
    private String username;
    
    private String dtFullNames; //comma seperated DT full names
    //range of how old the documents should be, default to first 90 days
    private long startDate = -1;
    private long endDate = -1;
    
    private int noOfRecs = -1; //top 5 recs or bottom 5 recs
    
    private Map<String, Object> data = new HashMap<String, Object>();
    public DTPathDetailReport(QueryDTO query, String username)
    {
        this.username = username;
        evaluateQueryDTO(query);
    }

    public ResponseDTO<Map<String, Object>> getData() throws Exception
    {
        ResponseDTO<Map<String, Object>> response = new ResponseDTO<Map<String, Object>>();
        List<Map<String, Object>> reportData = new ArrayList<Map<String,Object>>();
        String sql = getSql();
        if (StringUtils.isNotBlank(sql)) {
            try {
                reportData = GenericJDBCHandler.executeSQLSelect(sql, -1, this.noOfRecs, false);
            }
            catch (Exception e) {
                Log.log.error("Could not execute DTPathDetail query", e);
            }
        }
        
        response.setRecords(reportData);
        response.setTotal(reportData.size());
        return response;
    }
    private void evaluateQueryDTO(QueryDTO query)
    {
        /**
            filter:[{"field":"sysCreatedOn","type":"date","condition":"between","startValue":"2014-12-10T10:27:31-08:00","endValue":"2014-12-12T10:27:31-08:00"},{"field":"top","type":"boolean","condition":"equals","value":true}]
            page:1
            start:0
            limit:50
            group:[{"property":"unamespace","direction":"ASC"}]
            sort:[{"property":"unamespace","direction":"ASC"},{"property":"sysUpdatedOn","direction":"DESC"}]         
            
        */

        if (query != null)
        {
            List<QueryFilter> filters = query.getFilterItems();
            if (filters != null && filters.size() > 0)
            {
                for (QueryFilter filter : filters)
                {
                    setFilter(filter);
                }
            }
        }
    }

    private void setFilter(QueryFilter filter)
    {
        if (filter.isValid())
        {
            String field = filter.getField();
            if (field.equalsIgnoreCase("dtFullNames"))
            {
                this.dtFullNames = filter.getValue();
            }
            else if (field.equalsIgnoreCase("noOfRecs"))
            {
                this.noOfRecs = StringUtils.isNotEmpty(filter.getValue()) ? new Integer(filter.getValue()) : -1;
            }
            else if (field.equalsIgnoreCase("startDate"))
            {
                this.startDate = StringUtils.isNotEmpty(filter.getValue()) ? new Long(filter.getValue()) : -1;
            }
            else if (field.equalsIgnoreCase("endDate")) {
                this.endDate = StringUtils.isNotEmpty(filter.getValue()) ? new Long(filter.getValue()) : -1;
            }
        }
    }
    
    private String getSql()
    {
        StringBuilder sql = new StringBuilder();
        if (StringUtils.isNotBlank(dtFullNames))
        {
            sql.append("SELECT count, pathid, u_dt_root_doc as dtname, u_full_path as path ");
            sql.append(" FROM (");
            sql.append(" SELECT sum(tot_cnt) as count, pathid ");
            sql.append(" FROM tmetric_hr_dt ");
            sql.append(" WHERE seqid = 1 ");
            sql.append(" AND nodeid IN (" + getDTList() + ") ");
            sql.append(" AND ts < " + this.endDate + " AND ts > " + this.startDate);
            sql.append(" GROUP BY pathid ) rs LEFT JOIN tmetric_lookup ON tmetric_lookup.u_path_id = rs.pathid");
            sql.append(" ORDER BY count DESC, u_dt_root_doc");
            Log.log.debug("DTExecutionReport Sql:" + sql.toString());
        }

        return sql.toString();
    }
    
    private String getDTList()
    {
        StringBuilder builder = new StringBuilder();
        List<String> docs = Arrays.asList(dtFullNames.split(","));
        if (docs != null || docs.size() > 0)
        {
            for (String doc : docs)
            {
                builder.append("'").append(doc.trim()).append("',");
            }
            builder.append("''");
        }
        
        return builder.toString();
    }
}
