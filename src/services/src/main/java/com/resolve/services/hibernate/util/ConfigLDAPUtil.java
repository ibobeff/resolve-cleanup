/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.model.ConfigLDAP;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ConfigLDAPVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ConfigLDAPUtil
{
    public static List<ConfigLDAPVO> getConfigLDAP(QueryDTO query, String username)
    {
        List<ConfigLDAPVO> result = new ArrayList<ConfigLDAPVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ConfigLDAP instance = new ConfigLDAP();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(convertModelToVO(instance));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ConfigLDAP instance = (ConfigLDAP) o;
                        result.add(convertModelToVO(instance));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static ConfigLDAPVO findConfigLDAPById(String sys_id, String username)
    {
        ConfigLDAP model = null;
        ConfigLDAPVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findConfigLDAPModelById(sys_id);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = convertModelToVO(model);
        }
        
        return result;
    }
    
    public static ConfigLDAPVO saveConfigLDAP(ConfigLDAPVO vo, String username)
    {
        if(vo != null)
        {
            ConfigLDAP model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                model = findConfigLDAPModelById(vo.getSys_id());
            }
            else
            {
                model = new ConfigLDAP();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveConfigLDAP(model, username);
            
            //invalidate the cache by sending the JMS to the gateway
            sendJmsMessageToInvalidate(model);
            
            if(model.getUSsl())
            {
                sendMessageToImportSSLCertificate(model);
            }
            
            vo = convertModelToVO(model);
        }
        
        return vo;
    }
    
    public static ConfigLDAPVO findConfigLDAPByDomainName(String domainName)
    {
        ConfigLDAPVO result = null;
        
        if(StringUtils.isNotBlank(domainName))
        {
            ConfigLDAP model = findConfigLDAPModelByDomainName(domainName);
            if(model != null)
            {
                result = convertModelToVO(model);
            }
        }
        
        return result;
    }
    
    public static ConfigLDAPVO findDefaultConfigLDAP()
    {
        ConfigLDAPVO result = null;
        ConfigLDAP model = null;
        
        try
        {
            
            model = (ConfigLDAP) HibernateProxy.execute(() -> {
            	ConfigLDAP searchedModel = new ConfigLDAP();
            	searchedModel.setUIsDefault(true);

            	return HibernateUtil.getDAOFactory().getConfigLDAPDAO().findFirst(searchedModel);

            });
        }
        catch (Throwable e)
        {
        	Log.log.warn(e.getMessage(), e);
                   HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if(model != null)
        {
            result = convertModelToVO(model);
        }
        
        return result;
    }
    
    public static void deleteConfigLDAPByIds(String[] sysIds, String username) throws Exception
    {
        if(sysIds != null)
        {
            for(String sysId : sysIds)
            {
                deleteConfigLDAPById(sysId, username);
            }
        }
    }
    
    private static void deleteConfigLDAPById(String sysId, String username) throws Exception
    {
        ConfigLDAP model = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	model = (ConfigLDAP) HibernateProxy.execute(() -> {
            
        		ConfigLDAP modelResult = HibernateUtil.getDAOFactory().getConfigLDAPDAO().findById(sysId);
	            if(modelResult != null)
	            {
	                HibernateUtil.getDAOFactory().getConfigLDAPDAO().delete(modelResult);
	            }

	            return modelResult;
        	});
            
            //invalidate the cache by sending the JMS to the gateway
            sendJmsMessageToInvalidate(model);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e); 
        }
    }
    
    
    //private apis
    private static ConfigLDAP findConfigLDAPModelByDomainName(String domainName)
    {
        ConfigLDAP result = null;
        if(StringUtils.isNotBlank(domainName))
        {
            ConfigLDAP example = new ConfigLDAP();
            example.setUDomain(domainName);
            
            try
            {
            	result = (ConfigLDAP) HibernateProxy.execute(() -> {
					return HibernateUtil.getDAOFactory().getConfigLDAPDAO().findFirst(example);
				});
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static ConfigLDAP findConfigLDAPModelById(String sys_id)
    {
        ConfigLDAP result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
            	result = (ConfigLDAP) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getConfigLDAPDAO().findById(sys_id);
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static ConfigLDAPVO convertModelToVO(ConfigLDAP model)
    {
        ConfigLDAPVO vo = null;
        
        if(model != null)
        {
            vo = model.doGetVO();
            if(vo.getBelongsToOrganization() != null)
            {
                vo.setSysOrganizationName(vo.getBelongsToOrganization().getUOrganizationName());
            }
            else
            {
                vo.setSysOrganizationName("");
            }            
        }
        
        return vo;
    }
    
    private static void sendJmsMessageToInvalidate(ConfigLDAP model)
    {
      //invalidate the cache by sending the JMS to the gateway - this is RSREMOTE
        String gateway = model.getUGateway();
        if(StringUtils.isNotBlank(gateway))
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.AUTH_DOMAIN, model.getUDomain());
            
            //jms message
            MainBase.getESB().sendMessage(gateway, "MAuth.invalidateDomain", params);
        }  
        
        //this is invalidate the RSVIEWS
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.AUTH_DOMAIN, model.doGetVO()); 
        
        MainBase.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.invalidateLDAP", params);
       
        
    }

    private static void sendMessageToImportSSLCertificate(ConfigLDAP model)
    {
        //inform rsremote to import certificate from the LDAP server.
        String gateway = model.getUGateway();
        if(StringUtils.isNotBlank(gateway))
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.AUTH_LDAP_HOST, model.getUIpAddress());
            params.put(Constants.AUTH_LDAP_PORT, "" + model.getUPort());
            
            //message
            MainBase.getESB().sendMessage(gateway, "MAuth.importSSLCertificate", params);
        }  
    }
}
