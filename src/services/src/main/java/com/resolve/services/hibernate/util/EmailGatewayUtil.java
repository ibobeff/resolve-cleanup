/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.dao.EmailAddressDAO;
import com.resolve.persistence.model.EmailAddress;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class EmailGatewayUtil extends GatewayUtil
{
    private static volatile EmailGatewayUtil instance = null;
    public static EmailGatewayUtil getInstance()
    {
        if(instance == null)
        {
            instance = new EmailGatewayUtil();
        }
        return instance;
    }

    @SuppressWarnings("rawtypes")
    public void setEmailAddresses(Map emailAddresses)
    {
        try
        {
        	HibernateProxy.execute(() -> {
        		 EmailAddressDAO filterDao = HibernateUtil.getDAOFactory().getEmailAddressDAO();

                 String queue = deleteByQueue(EmailAddress.class.getSimpleName(), emailAddresses);

                 // update each filter
                 for (Object filterMapObj : emailAddresses.values())
                 {
                     String filterStringMap = (String) filterMapObj;
                     Map filterMap = StringUtils.stringToMap(filterStringMap);

                     String emailAddress = (String) filterMap.get("EMAILADDRESS");
                     String emailPassword = (String) filterMap.get("EMAILPASSWORD");
                     String sysModifiedBy = (String) filterMap.get(Constants.SYS_UPDATED_BY);

                     // init record entry with values
                     EmailAddress row = new EmailAddress();
                     row.setUEmailAddress(emailAddress);
                     row.setUEmailPassword(emailPassword);
                     row.setUQueue(queue);
                     row.setSysCreatedBy(sysModifiedBy);
                     row.setSysUpdatedBy(sysModifiedBy);

                     filterDao.save(row);
                 }
        	});
           
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setEmailAddresses

    @SuppressWarnings("unchecked")
    public List<Map<String, String>> getEmailAddresses(String queueName, boolean isSocialPoster)
    {
        List<Map<String, String>> emailAddresses = new ArrayList<Map<String, String>>();

        try
        {
        	HibernateProxy.execute(() -> {
                List<EmailAddress> addressList = HibernateUtil.createQuery("select o from EmailAddress o where UQueue='" + queueName + "'").list();

                for (EmailAddress filter : addressList)
                {
                    Map<String, String> addressMap = new HashMap<String, String>();
                    addressMap.put("EMAILADDRESS", filter.getUEmailAddress());
                    addressMap.put("EMAILPASSWORD", filter.getUEmailPassword());
                    addressMap.put("QUEUE", filter.getUQueue());
                    addressMap.put(Constants.SYS_UPDATED_BY, filter.getSysUpdatedBy());

                    emailAddresses.add(addressMap);
                }

                //get Email address from process, forums, team etc.
                if (isSocialPoster)
                {
                    //found out that there is no active flag for these anymore and by default they are active. but in the future
                    //they may add it. the day they do that hopefully someone will remember this to turn on. 
                    //List<Map<String, Object>> addresses = getCustomTableData("select * from social_process where u_is_active=1 and u_receive_from is not null");
                    //addresses.addAll(getCustomTableData("select * from social_forum where u_is_active=1 and u_receive_from is not null"));
                    //addresses.addAll(getCustomTableData("select * from social_team where u_is_active=1 and u_receive_from is not null"));

                    List<Map<String, Object>> addresses = getCustomTableData("select * from social_process where u_receive_from is not null");
                    addresses.addAll(getCustomTableData("select * from social_forum where u_receive_from is not null"));
                    addresses.addAll(getCustomTableData("select * from social_team where u_receive_from is not null"));

                    for (Map<String, Object> address : addresses)
                    {
                        String emailAddress = (String) address.get("u_receive_from");
                        if (StringUtils.isNotBlank(emailAddress))
                        {
                            Map<String, String> addressMap = new HashMap<String, String>();
                            addressMap.put("EMAILADDRESS", emailAddress);
                            addressMap.put("EMAILPASSWORD", (String) address.get("u_receive_pwd"));
                            addressMap.put("QUEUE", queueName);
                            addressMap.put(Constants.SYS_UPDATED_BY, (String) address.get("sys_updated_by"));
                            emailAddresses.add(addressMap);
                        }
                    }
                }

        	});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return emailAddresses;
    } // getEmailAddresses
}
