package com.resolve.services.rb.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.util.StringUtils;

public class ABUtils
{
    private static final Map<String,String>  REGEX_MAPPING;
    static
    {
        REGEX_MAPPING = new HashMap<String,String>();
        REGEX_MAPPING.put("SPACE","\\s+");
        REGEX_MAPPING.put("COMMA",",");
        REGEX_MAPPING.put("TAB", "\\t+");

        REGEX_MAPPING.put("ignoreNotGreedy",".*?");
        REGEX_MAPPING.put("ignoreRemainder",".*");
        REGEX_MAPPING.put("whitespace","\\s+");
        REGEX_MAPPING.put("notWhitespace", "\\S+");
        REGEX_MAPPING.put("spaceOrTab", "[\\s\\t]+");
        REGEX_MAPPING.put("letters","[a-zA-Z]+");
        REGEX_MAPPING.put("lettersAndNumbers", "\\w+");
        REGEX_MAPPING.put("notLettersOrNumbers","\\W+");
        REGEX_MAPPING.put("lowerCase","[a-z]+");
        REGEX_MAPPING.put("upperCase","[A-Z]+");
        REGEX_MAPPING.put("number","-?\\d+");
        REGEX_MAPPING.put("notNumber","\\D+");
        REGEX_MAPPING.put("beginOfLine","(?:\\n|\\r\\n)");
        REGEX_MAPPING.put("endOfLine","(?:\\n|\\r\\n)");
        REGEX_MAPPING.put("MAC", "(?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2})");
        REGEX_MAPPING.put("IPv4", "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
        REGEX_MAPPING.put("decimal", "-?[0-9]*\\.[0-9]+");
    }
    
    
    /***
     * Takes in a String and outputs a list of string tokens which were detected as wrapped by startDelimn and endDelmin
     * 
     * @example
     * stringToExtractFrom - "this is a ;test1-ing and ;test2-ing;test3-ing" </br>
     * startDelimn = ";" </br>
     * endDelmin = "-ing" </br>
     * return = [test1,test2,test3] </br>
     * 
     * @param stringToExtractFrom - source of the text extraction
     * @param startDelimn - start tag of the text to be extracted
     * @param endDelmin - end tag of the text to be extracted
     * @return A list of Strings representing the extracted tokens
     */
    public static List<String> extractWrappedTokens(String stringToExtractFrom, String startDelimn, String endDelmin)
    {
        String includeFormPattern = startDelimn+"(.*?)"+endDelmin;
        List<String> list = new ArrayList<String>();
        Pattern p = Pattern.compile(includeFormPattern, Pattern.DOTALL);
        Matcher matcher = p.matcher(stringToExtractFrom);

        while (matcher.find())
        {
            String str = matcher.group(1);
            list.add(str);
        }
        
        return list;
    }
    
    /***
     * Takes in a String and outputs a list of string tokens which were detected 
     * as wrapped by startDelimn and endDelmin
     * 
     * @example
     * stringToExtractFrom - "this is a ;test1; and ;test2;;test3;" </br>
     * startDelimn = ";" </br>
     * endDelmin = ";" </br>
     * return = [test1,test2,test3] </br>
     * 
     * @param stringToExtractFrom - source of the text extraction
     * @param delimn - 
     * @return A list of Strings representing the extracted tokens
     */
    public static List<String> extractWrappedTokens(String stringToExtractFrom, String delimn)
    {
        return extractWrappedTokens(stringToExtractFrom,delimn,delimn);
    }

    /***
     * Takes in a list of UI defined types, such as "COMMA","SPACE","TAB" and replaces them with
     * their respective regex equivalents. Method will replace only susbtrings of mapped 
     * definitions.
     * 
     * @example
     * ListItem: "This is a COMMA, comma,"
     * Converted: "This is a ,, comma,"
     * 
     * @param textTypeList - List of strings to have their words/characters converted to regex
     * @return Modified list, type definitions replaced with regex
     */
    public static List<String> convertUIDefinedRegex(List<String> textTypeList)
    {
        List<String> regexList = new ArrayList<String>();
        Iterator<String> listIter = textTypeList.iterator();
        while(listIter.hasNext())
        {
            String curRegexKey = listIter.next();
            String curValue = REGEX_MAPPING.get(curRegexKey);
            if(curValue != null && curValue.length() > 0)
            {
                regexList.add(curValue);
            }
            else
            {
                regexList.add(curRegexKey);
            }
        }
        
        return regexList;
    }
    
    /***
     * Takes a single string representing a UI defined regex and turns it into a java regex expression
     * 
     * @example
     * type - "notWhiteSpace" would give \\S+
     *  
     * @param type - Strings representing a regex to be converted to actual regex
     * @return actual regex expression
     */
    public static String convertUIDefinedRegex(String type)
    {
       List<String> singleItemList = new ArrayList<String>();
       singleItemList.add(type);
       
       List<String> regexList = convertUIDefinedRegex(singleItemList);
        
        return regexList.get(0);
    }
    
    //Not really implemented yet.
    //Needs work, using escapeLiterals for now
    public static String escapeRegex(String raw)
    {
       
        //alright lets build some code.
        String escaped = null;
        escaped = StringUtils.escapeJava(raw);
        return escaped;        
    }
    
    public static String escapeLiterals(String raw)
    {
        String escaped = raw;
        if(StringUtils.isNotBlank(escaped))
        {
            escaped = raw.replaceAll("([\\\\\\.\\[\\]\\{\\}\\(\\)\\*\\+\\?\\^\\$\\/\\|])", "\\\\$1");
            escaped = escaped.replaceAll("(\\n)|(\\r\\n)","(?:\\\\n|\\\\r\\\\n)"); //looks for \n or \r\n replaces with regex to cover both
        }
        return escaped;
    } 
    
    @SuppressWarnings("unchecked")
    public static <T extends JsonNode> T cloneJsonNode(T node) throws JsonProcessingException, IOException   
    {
       return (T) new ObjectMapper().readTree(node.traverse());
    }
}
