/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.query.Query;

import com.resolve.dto.UserDTO;
import com.resolve.enums.ColorTheme;
import com.resolve.persistence.dao.GroupRoleRelDAO;
import com.resolve.persistence.dao.GroupsDAO;
import com.resolve.persistence.dao.RolesDAO;
import com.resolve.persistence.dao.UserRoleRelDAO;
import com.resolve.persistence.dao.UsersDAO;
import com.resolve.persistence.model.ConfigActiveDirectory;
import com.resolve.persistence.model.ConfigLDAP;
import com.resolve.persistence.model.ConfigRADIUS;
import com.resolve.persistence.model.GroupRoleRel;
import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.OrgGroupRel;
import com.resolve.persistence.model.Organization;
import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserPreferences;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.service.License;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.exception.ColorThemeException;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.menu.MenuCache;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UserPreferencesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.interfaces.VO;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.ResolveSessionUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.WikiUser;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.ERR;
import com.resolve.util.GMTDate;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UserUtils
{
	// Resolve default users 
	
	public final static String SYSTEM = "system";
    public final static String ADMIN = "admin";
    public final static String RESOLVE_MAINT = "resolve.maint";
    public final static String RESOLVE_SYSTEM = SYSTEM;
    public final static String PUBLIC = "public";
    public final static String USER = "user";

    /*
     * ^                 # start-of-string
	 * (?=.*[0-9])       # a digit must occur at least once
	 * (?=.*[a-z])       # a lower case letter must occur at least once
	 * (?=.*[A-Z])       # an upper case letter must occur at least once
	 * (?=\S+$)          # no whitespace allowed in the entire string
	 * .{8,}             # anything, at least eight places though
	 * $                 # end-of-string
     */
    public static final String PAS_SWORD_RULES = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

    // Uploading profile pic locks out user (http://10.20.2.111/browse/RBA-13209)
    // private final static String DUMMY_USER_P_ASSWORD = "#RESOLVE123456789RESOLVE0987654321#";
    
    private final static String NO_ADMIN_RIGHTS = "User %s does not have rights for this operation.";
    
    // CRUDE 'C'reate Read(View) 'U'pdate(Modify) 'Delete & Execute permission defines
    
    public static final String VIEW_PERMISSIONS = Constants.VIEW_ACCESS;
    public static final String CREATE_PERMISSIONS = Constants.CREATE_ACCESS;
    public static final String MODIFY_PERMISSIONS = Constants.MODIFY_ACCESS;
    public static final String DELETE_PERMISSIONS = Constants.DELETE_ACCESS;
    public static final String EXECUTE_PERMISSIONS = Constants.EXECUTE_ACCESS;
    
    public static final String NOT_IMPLEMENTED_BY_UI_FUNCTION_PERMISSIONS_JSON = "[" +
        "]";
    
    public static final String NOT_IMPLEMENTED_BY_UI_RBACS_JSON = "{" +
    		"}";
    
    private static final String SUPER_USER_ORG_ACCESS_LOG = "Super User %s belongs to Org with Id %s. (%d nsecs)";
    private static final String USER_HAS_ORG_ACCESS_LOG = "User %s belongs to Org with Id %s. (%d nsecs)";
    private static final String USER_DOES_NOT_HAVE_ORG_ACCESS_LOG = 
    														"User %s does not belong to Org with Id %s. (%d nsecs)";
    private static final String USER_NO_ORG_ACCESS_LOG = "User %s %s access to None Org. (%d nsecs)";
    private static final String HAS = "has";
    private static final String DOES_NOT_HAVE = "does not have";
    private static final String HAS_NO_ORGS_LOG = "User %s has No Org(s).";
    private static final String DB_LEGACY_OR_NEW_RBAC_JSON_LOG = "DB (Legacy/New) RBAC JSON : %s";
    private static final String ARBAC_NOT_IMPLEMENTED_BY_UI_ADDED_FOR_ROLE = "A-RBAC %s, " + RBACUtils.RBAC_SIR_ENABLED +
    																		 " (not implemneted by UI) Permission %s" +
    																		 " added for role.";
    private static final String TRANSFORMED_IF_DB_LEGACY_RBAC_JSON = 
    														"Transformed (if DB permissions were Legacy) RBAC JSON : %s";
    private static final String PRESUMED_LEGACY_RBAC_HAS_NO_NEW_RBAC_MAPPING = 
    														"Presumed legacy RBAC %s%s has no mapping in new RBAC!!!";
    private static final String BLANK = "";
    private static final String RETURNING_FUNCTION_PERMISSIONS_FROM_LEGACY_LOG = "Returning %s permissions for %s%s" + 
    																			 " from legacy permissions!!!";
    private static final String FUNCTION_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG = 
    																"Function %s (not implemneted by UI) Permissions %s" +
    																" added for %s.";
    private static final String RBAC_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG = 
    																"RBAC %s (not implemneted by UI) Permission %s" +
    																" added for %s.";
    private static final String FOUND_CACHED_DB_OBJECT_FOR_USER_LOG = 
    														"Found cached DB object for user %s in " + 
    														DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue();
    private static final String FOUND_CACHED_DB_OBJECT_FOR_USER_NO_GROUPS_WITH_ORGS_LOG = 
    											"isOrgAccessible() : Found cached DB object for user %s in " + 
    											DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue() + 
    											" having no UserGroupRels with one or more Orgs configured in system!!!";
    private static final String CLEARED_CACHE_DB_OBJECT_FOR_USER_LOG = 
    														"Cleared cached DB object for user %s from " + 
    														DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue();
    private static final String CACHED_USER_IN_CACCHED_DB_OBJECT_LOG = 
    													"isOrgAcessible() : Inserted cached DB object for user %s in " + 
    													DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue();
    private static final String USER_NOT_CACHED_IN_CACHED_DB_OBJECT_LOG = 
    											"Queried user %s with all references no cache has no UserGroupRels" + 
    											" with one or more Orgs configured in system!!!, Not caching %s in " + 
    											DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue() + 
    											" having no UserGroupRels with one or more Orgs configured in system!!!";
    private static final String USER_UPDATED_AUDIT_LOG = 
    											"[Audit] %s %s %s - %s updated groups/roles/default org/password needs " +
    											"reset/locked out for user.";
    
    public static UsersVO getUser(String username)
    {
    	return getUser(username, false);
    }
    
    public static UsersVO getUser(String username, boolean orgsOnly)
    {
        UsersVO result = null;

        if(StringUtils.isNotEmpty(username))
        {
            Users userResult = getUserPrivate(null, username);

            if(userResult != null)
            {
                result = convertModelUsersToVO(userResult, null, null, null, true, false, false, orgsOnly);
            }
        }

        return result;
    } // getUser

    public static UsersVO getUserWithRolesOnlyHelper(String username)
    {
        UsersVO result = null;

        if(StringUtils.isNotEmpty(username))
        {
            Users userResult = getUserPrivate(null, username);

            if(userResult != null)
            {
                result = convertModelUsersToVO(userResult, null, null, null, true, false, true );
            }
        }

        return result;
    } // getUser
    
    public static UsersVO getUserNoReferences(String username)
    {
        UsersVO result = null;

        if(StringUtils.isNotEmpty(username))
        {
            Users userResult = getUserPrivate(null, username);

            if(userResult != null)
            {
                result = convertModelUsersToVO(userResult, null, null, null, false, true);
            }
        }

        return result;
    } // getUser
    
    public static UsersVO getUserNoCache(String sysId, String username)
    {
        UsersVO result = null;

        try
        {
            result = (UsersVO) HibernateProxy.executeNoCache(() -> {
            	return getUser(sysId, username);
            });
            
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    } // getUser

    public static UsersVO getUserWithRolesOnly(String username)
    {
        UsersVO result = null;

        try
        {
            
            result = (UsersVO) HibernateProxy.executeNoCache(() -> {
            	return getUserWithRolesOnlyHelper(username);
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    } // getUser
    
    
    /**
     * Use this api when you are trying to manipulate the collections of the model, in this case Groups and Roles
     * Where ever we are using this notation '@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)' for collections, 
     * use the NOCACHE version 
     * 
     * @param username
     * @return
     */
    
    public static UsersVO getUserNoCache(String username)
    {
    	return getUserNoCache(username, false);
    }
    
    public static UsersVO getUserNoCache(String username, boolean orgsOnly)
    {
        UsersVO result = null;

        try
        {
            result = (UsersVO) HibernateProxy.executeNoCache(() -> {
            	return getUser(username, orgsOnly);
            });
            
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    } // getUser
    
    public static String getUserNameSimple(String sysId)
    {
        if(StringUtils.isNotEmpty(sysId))
        {
            Users userResult = UserUtils.findUser(sysId, null);
            if(userResult != null)
            {
                return userResult.getUUserName();
            }
        }
        return null;
    }

    public static UsersVO getUserById(String sysId)
    {
        UsersVO result = null;

        if(StringUtils.isNotEmpty(sysId))
        {
            Users userResult = findUserWithAllReferencesNoCache(sysId, null);
            if(userResult != null)
            {
                result = convertModelUsersToVO(userResult, null, null, null, true);
            }
        }

        return result;
    }
    
    public static UsersVO getUserByNameNoCache(String username)
    {
        UsersVO result = null;

        if(StringUtils.isNotBlank(username))
        {
            Users userResult = null;
            
            if (username.trim().equals(RESOLVE_MAINT))
            {
                userResult = new Users();
                userResult.setUUserName(RESOLVE_MAINT);
            }
            else
            {
                userResult = findUserWithAllReferencesNoCache(null, username);
            }
            
            if(userResult != null)
            {
                result = convertModelUsersToVO(userResult, null, null, null, true);
                hideProtectedData(result);
            }
        }

        return result;
    }

    public static UsersVO getUserByNameNoReference(String username)
    {
        UsersVO result = null;

        if(StringUtils.isNotBlank(username))
        {
            Users userResult = null;
            
            if (username.trim().equals(RESOLVE_MAINT))
            {
                userResult = new Users();
                userResult.setUUserName(RESOLVE_MAINT);
            }
            else
            {
                userResult = getUserPrivate(null, username);
            }
            
            if(userResult != null)
            {
                result = convertModelUsersToVO(userResult, null, null, null, false, false, false, true);
                hideProtectedData(result);
            }
        }

        return result;
    }
    
    public static UsersVO getUser(String sysId, String username)
    {
        UsersVO result = null;

        result = findUserById(sysId);
        if(result == null)
        {
            result = getUser(username);
        }

        return result;
    }

    public static RolesVO getRole(String sysId, String roleName, String username)
    {
        Roles roleModel = null;
        RolesVO roleVO = null;

        try
        {
            roleModel = getRoleModel(sysId, roleName);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        if (roleModel != null)
        {
            roleVO = roleModel.doGetVO();
        }

        return roleVO;
    }

    public static GroupsVO getGroup(String sysId, String groupName, String username)
    {
        Groups groupModel = null;
        GroupsVO groupVO = null;

        try
        {
            groupModel = getGroupModel(sysId, groupName);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        if (groupModel != null)
        {
            groupVO = convertModelToVOForGroups(groupModel, null, null);
        }

        return groupVO;
    }

    public static UsersVO getUserWithRoles(String username)
    {
        Users userModel = null;
        UsersVO resultVO = null;

        if(StringUtils.isNotBlank(username))
        {
            try
            {
            	userModel = (Users) HibernateProxy.execute(() -> {
            		
            		String usernameFinal = username;
            		
                    if (username.equalsIgnoreCase(RESOLVE_MAINT))
                    {
                        usernameFinal = ADMIN;
                    }
            		
            		// get user password
                    Users res = findUser(null, usernameFinal);

                    // set the user roles
                    setUserRoles(res);
                    return res;
            	});                
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }

            if(userModel != null)
            {
                resultVO = userModel.doGetVO();
            }
        }

        return resultVO;
    } // getUser

    public static UsersVO getUserWithPreferences(String username)
    {
        Users userModel = null;
        UsersVO resultVO = null;

        if(StringUtils.isNotBlank(username))
        {
            if (username.equalsIgnoreCase(RESOLVE_MAINT))
            {
                username = ADMIN;
            }

            try
            {
            	String usernameFinal = username;
            	userModel = (Users) HibernateProxy.execute(() -> {            		
            		
            		// get user password
                    Users res = findUser(null, usernameFinal);
                    if(res.getSocialPreferences() != null)
                    {
                        res.getSocialPreferences().size();
                    }
                    
                    return res;
            	});
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }

            if(userModel != null)
            {
                resultVO = userModel.doGetVO();
            }
        }

        return resultVO;
    } // getUser

    public static Set<UserPreferencesVO> getUserPreferences(String username, Set<String> preferencesKeys)
    {
        Set<UserPreferencesVO> result = new HashSet<UserPreferencesVO>();
        Collection<UserPreferences> prefs = null;

        if (StringUtils.isNotBlank(username))
        {
            try
            {
            	Users user = (Users) HibernateProxy.execute(() -> {
                	 return findUser(null, username);
                });
                

                prefs = user.getSocialPreferences();
                if(prefs != null)
                {
                    //load the data in the memory
                    prefs.size();
                }

            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        //always CONVERT THE MODEL TO VO OUTSIDE OF THE TRANSACTION - this will take care that the objects that are suppose to load LAZILY are not loaded
        if (prefs != null)
        {
            if (preferencesKeys != null && preferencesKeys.size() > 0)
            {
                for (UserPreferences up : prefs)
                {
                    if (preferencesKeys.contains(up.getUPrefKey()))
                    {
                        result.add(up.doGetVO());
                    }
                }
            }
            else
            {
                for (UserPreferences up : prefs)
                {
                    result.add(up.doGetVO());
                }
            }
        }

        return result;

    }

    public static UsersVO findUserById(String sys_id)
    {
        return getUserById(sys_id);

    } // getUser

    public static UsersVO findUserWithRolesById(String sys_id)
    {
        Users userModel = null;
        UsersVO resultVO = null;

        if (StringUtils.isNotBlank(sys_id))
        {
            try
            {
                userModel = (Users) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getUsersDAO().findById(sys_id);
            	});
                // set the user roles
                setUserRoles(userModel);
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }

            if (userModel != null)
            {
                resultVO = userModel.doGetVO();
            }
        }

        return resultVO;
    } // getUser

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Set<String> getUserRoles(String userName)
    {
        Set<String> userRoles = new TreeSet<String>();

        Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.USER_ROLES_CACHE_REGION, userName);
        if (obj != null)
        {
            return (Set<String>) obj;
        }

        if (StringUtils.isNotBlank(userName))
        {
            if (userName.equalsIgnoreCase(RESOLVE_MAINT))
            {
                userRoles.add("admin");
            }
            else
            {
                userName = userName.toLowerCase();
                try
                {
                	String usernameFinal = userName;
                    HibernateProxy.execute(() -> {
                    	// UNION ALL is not supported by HIBERNATE. So have to prepare 2
                        // qrys and assemble it.
                        String sql = "select r.UName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r " + " where u.UUserName = :username";
                        Query q = HibernateUtil.createQuery(sql);
                        q.setParameter("username", usernameFinal);
                        List<String> roles = q.list();
                        userRoles.addAll(roles);

                        // query 2
                        sql = "select r.UName from Users as u" + " join u.userGroupRels as ugr " + " join ugr.group as g " + " join g.groupRoleRels as grr " + " join grr.role as r " + " where u.UUserName = :username";
                        q = HibernateUtil.createQuery(sql);
                        q.setParameter("username", usernameFinal);

                        roles = q.list();
                        userRoles.addAll(roles);
                    });                   

                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                }

                // trim the string in the list
                StringUtils.trimStringCollection(userRoles);
                HibernateUtil.cacheDBObject(DBCacheRegionConstants.USER_ROLES_CACHE_REGION, 
                							new String[] { "com.resolve.persistence.model.Users", 
                										   "com.resolve.persistence.model.UserRoleRel", 
                										   "com.resolve.persistence.model.Roles", 
                										   "com.resolve.persistence.model.UserGroupRel", 
                										   "com.resolve.persistence.model.Groups", 
                										   "com.resolve.persistence.model.GroupRoleRel" }, 
                							userName, userRoles);
            }
        }

        return userRoles;
    }

    @SuppressWarnings("unchecked")
    public static List<RolesVO> getUserRolesOnly(String username)
    {
        List<RolesVO> result = new ArrayList<RolesVO>();
        if(StringUtils.isNotBlank(username))
        {
            username = username.trim().toLowerCase();
            
            //String sql = "select r.UName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r " + " where u.UUserName = '"+ username +"'";
            String sql = "select r.UName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r " + " where u.UUserName = :UUserName";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UUserName", username);
            
            List<String> roles = new ArrayList<String>();
    
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql.toString(), queryParams);
                if(list != null && list.size() > 0)
                {
                    roles.addAll((List<String>) list);
                    Collections.sort(roles);
                }
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
    
            if(roles.size() > 0)
            {
                for(String role : roles)
                {
                    result.add(getRole(null, role, "system"));
                }
            }
        }

        return result;
    }

    public static List<UsersVO> getUsersWithRole(String roles, String whereClause)
    {
        Set<Users> usersSet = new TreeSet<Users>();
        List<UsersVO> result = new ArrayList<UsersVO>();

        if (StringUtils.isNotBlank(roles) && StringUtils.isNotBlank(whereClause))
        {
            try
            {
                // UNION ALL is not supported by HIBERNATE. So have to prepare 2
                // qrys and assemble it.
                StringBuffer sql = new StringBuffer("select u from Users as u ")
                                    .append(" join u.userRoleRels as urr ")
                                    .append(" join urr.role as r ")
                                    .append(" where r.UName in ").append(roles).append(" ");

                if (whereClause != null && whereClause.trim().length() > 0)
                {
                    sql.append(whereClause);
                }

                List<Users> users = queryUsersWithRoles(sql.toString());
                usersSet.addAll(users);
                // usersList.addAll(users);

                // query 2
                sql = new StringBuffer("select u from Users as u")
                        .append(" join u.userGroupRels as ugr ")
                        .append(" join ugr.group as g ")
                        .append(" join g.groupRoleRels as grr ")
                        .append(" join grr.role as r ")
                        .append(" where r.UName in ").append(roles).append(" ");

                if (whereClause != null && whereClause.trim().length() > 0)
                {
                    sql.append(whereClause);
                }

                users = queryUsersWithRoles(sql.toString());
                usersSet.addAll(users);
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }
        else if(StringUtils.isNotBlank(whereClause))
        {
            try
            {
                // UNION ALL is not supported by HIBERNATE. So have to prepare 2
                // qrys and assemble it.
                StringBuffer sql = new StringBuffer("select u from Users as u ")
                                    .append(" join u.userRoleRels as urr ")
                                    .append(" join urr.role as r ")
                                    .append(" where ");

                sql.append(whereClause);

                List<Users> users = queryUsersWithRoles(sql.toString());
                usersSet.addAll(users);

                // query 2
                sql = new StringBuffer("select u from Users as u")
                        .append(" join u.userGroupRels as ugr ")
                        .append(" join ugr.group as g ")
                        .append(" join g.groupRoleRels as grr ")
                        .append(" join grr.role as r ")
                        .append(" where ");

                sql.append(whereClause);

                users = queryUsersWithRoles(sql.toString());
                usersSet.addAll(users);
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }
        else
        {
            //get all users
            try
            {
                // UNION ALL is not supported by HIBERNATE. So have to prepare 2
                // qrys and assemble it.
                StringBuffer sql = new StringBuffer("select u from Users as u ")
                                    .append(" join u.userRoleRels as urr ")
                                    .append(" join urr.role as r ")
                                    .append(" where r.UName in ").append(roles).append(" ");


                List<Users> users = queryUsersWithRoles(sql.toString());
                usersSet.addAll(users);

                // query 2
                sql = new StringBuffer("select u from Users as u")
                        .append(" join u.userGroupRels as ugr ")
                        .append(" join ugr.group as g ")
                        .append(" join g.groupRoleRels as grr ")
                        .append(" join grr.role as r ")
                        .append(" where r.UName in ").append(roles).append(" ");

                users = queryUsersWithRoles(sql.toString());
                usersSet.addAll(users);

            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }

        if (usersSet != null && usersSet.size() > 0)
        {
            for (Users u : usersSet)
            {
                result.add(u.doGetVO());
            }
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static List<Users> queryUsersWithRoles(String sql)
    {
    	List<Users> result = new ArrayList<Users>();
        try
        {
            result = (List<Users>) HibernateProxy.execute(() -> {
            	Query q = HibernateUtil.createQuery(sql.toString());
            	List<Users> res  = q.list();

                //set the user roles
                setUserRoles(res);
                return res;
            });
            
        }
        catch (Throwable t)
        {
            Log.log.error("Error executing sql :" + sql, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return result;
    }

    @SuppressWarnings({ "unchecked" })
    public static Set<String> getAllRolesWithAnd()
    {
        Set<String> rolesWithAnd = new TreeSet<String>();

        try
        {
            HibernateProxy.execute(() -> {
            	List<String> rolesWithAndList = HibernateUtil.createQuery("select UName from Roles where UName like '%&%'").list();
                rolesWithAnd.addAll(rolesWithAndList);
            });          

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return rolesWithAnd;
    } // getAllRolesWithAnd

    @SuppressWarnings("unchecked")
    public static Set<String> getAllRoles()
    {
        Set<String> rolesWithAnd = new TreeSet<String>();

        try
        {
        	HibernateProxy.execute(() -> {
        		List<String> rolesWithAndList = HibernateUtil.createQuery("select UName from Roles").list();
                rolesWithAnd.addAll(rolesWithAndList);
        	});
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return rolesWithAnd;
    } // getAllRolesWithAnd

    @SuppressWarnings({"unchecked" })
    public static Set<String> getAllGroups()
    {
        Set<String> result = new TreeSet<String>();
        String sql = "select UName from Groups";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql.toString(), queryParams);
            if(list != null && list.size() > 0)
            {
                result.addAll((List<String>) list);
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    } // getAllGroups

    @SuppressWarnings("rawtypes")
    public static Set<String> getUserGroups(String username)
    {
        Set<String> groups = new TreeSet<String>();

        if (StringUtils.isNotBlank(username))
        {
            username = username.trim().toLowerCase();
                           
            String sql = "select g.UName from Users as u join u.userGroupRels as ugr join ugr.group as g where u.UUserName = :username";
            
            try
            {
            	String usernameFinal = username;
            	HibernateProxy.execute(() -> {
            		Query q = HibernateUtil.createQuery(sql);
                    q.setParameter("username", usernameFinal);
                    List groupNames = q.list();
                    for (Object groupName : groupNames)
                    {
                        groups.add((String) groupName);
                    }
            	});            	
                
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        return groups;
    } // getUserGroups

    public static List<GroupsVO> getUserGroupObjects(String username)
    {
        List<GroupsVO> result = new ArrayList<GroupsVO>();

        Set<String> groups = getUserGroups(username);
        if(groups != null)
        {
            for(String group : groups)
            {
                result.add(getGroup(null, group, "system"));
            }
        }


        return result;
    }

    @SuppressWarnings("rawtypes")
    public static Set<String> getUsersForGroup(String groupName)
    {
        Set<String> users = new TreeSet<String>();
        String sql = "select u.UUserName from Users as u join u.userGroupRels as ugr join ugr.group as g where g.UName = :groupname";
        
        try
        {
        	HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sql);
                q.setParameter("groupname", groupName);
                List userNames = q.list();
                for (Object userName : userNames)
                {
                    users.add((String) userName);
                }
        	});
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return users;
    } // getUsersForGroup

    @SuppressWarnings("rawtypes")
    public static Set<String> getUsersForGroup(String[] groups)
    {
        //String groupInClause = SQLUtils.prepareQueryString(groups);
        
        Set<String> users = new TreeSet<String>();
        //String sql = "select u.UUserName from Users as u join u.userGroupRels as ugr join ugr.group as g where g.UName IN ("+groupInClause+")";
        String sql = "select u.UUserName from Users as u join u.userGroupRels as ugr join ugr.group as g where g.UName IN (:groupInClause)";
        
        try
        {
        	HibernateProxy.execute(() -> {
        		 Query q = HibernateUtil.createQuery(sql);
                 q.setParameterList("groupInClause", groups);
                 List userNames = q.list();
                 for (Object userName : userNames)
                 {
                     users.add((String) userName);
                 }
        	});           
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return users;
    } // getUsersForGroup

    @SuppressWarnings("rawtypes")
    public static Set<String> getRolesForGroup(String groupName)
    {
        Set<String> roles = new TreeSet<String>();
        String sql = "select r.UName from Groups as g join g.groupRoleRels as grr join grr.role as r where g.UName = :groupname";
        
        try
        {
        	HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sql);
                q.setParameter("groupname", groupName);

                List roleNames = q.list();
                for (Object roleName : roleNames)
                {
                    roles.add((String) roleName);
                }
        	});            
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return roles;
    } // getRolesForGroup

    public static boolean checkTaskPermission(String task_sid, String username)
    {
        boolean result = false;
        ResolveActionTask task = null;
        try
        {
        	task = (ResolveActionTask) HibernateProxy.execute(() -> {
        		return HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(task_sid);
        		
        	});

        }
        catch (Throwable e)
        {
            Log.log.warn("FAIL: checkTaskPermission. " + e.getMessage());
                      HibernateUtil.rethrowNestedTransaction(e);
        }


        if (task != null)
        {
            result = com.resolve.services.util.UserUtils.checkTaskPermission(task.doGetVO(), username);
        }

        return result;
    } // checkTaskPermission

    public static boolean isLockedOut(String username)
    {
        boolean result = false;

        try
        {
            result = (boolean) HibernateProxy.execute(() -> {
                Users user = findUser(null, username);
                if (user != null)
                {
                    if (user.ugetULockedOut() == true)
                    {
                        return true;
                    }
                }
                
                return false;

            });

        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    } // isLockedOut

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static boolean isUserExist(String username)
    {
        boolean result = false;

        if(StringUtils.isNotBlank(username))
        {
            try
            {
                String sql = "from Users where UUserName = '" + username.toLowerCase() + "'";

                result = (boolean) HibernateProxy.execute(() -> {
                	Query hqlQuery = HibernateUtil.createQuery(sql);
                    List<Users> rows = hqlQuery.list();
                    if(rows != null && rows.size() > 0)
                    {
                        return true;
                    }
                    
                    return false;
                });
                
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return result;
    }

    public static boolean checkPassword(String username, String password)
    {
        boolean result = false;

        String hashPasswd = com.resolve.services.util.UserUtils.encryptP_asswd(password);
        try
        {
        	Users user = (Users) HibernateProxy.execute(() -> {
        		return findUser(null, username);
            });

            if (user != null)
            {
                // First check the salted password hash, and if not present then fall back to unsalted
                
                if (StringUtils.isNotEmpty(user.getUUserSaltedPasswordHash()) && 
                    !user.getUUserSaltedPasswordHash().equalsIgnoreCase(VO.STRING_DEFAULT))
                {
                    result = com.resolve.services.util.UserUtils.checkBcryptPasswordHash(password, user.getUUserSaltedPasswordHash());
                }
                else
                {
                    if (user.getUUserPassword() == null)
                    {
                        result = false;
                    }
                    else if (user.getUUserPassword().equals(hashPasswd))
                    {
                        result = true;
                    }
                    else
                    {
                        String oldHashPasswd = com.resolve.services.util.UserUtils.encryptPasswdOld(password);
                        if (user.getUUserPassword().equals(oldHashPasswd))
                        {
                            result = true;
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    } // checkPassword

    public static void updatePassword(String username, String password) throws Exception
    {
    	validatePassword(password);
        String encPasswordFromUI = com.resolve.services.util.UserUtils.encryptP_asswd(password);

        try
        {
            Users dbUser = findUser(null, username);
            if(dbUser == null)
            {
                throw new Exception("User " + username + " does not exist");
            }

            if (StringUtils.isNotEmpty(dbUser.getUUserSaltedPasswordHash()) && 
                !dbUser.getUUserSaltedPasswordHash().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                SaltedHistoryResult sltHistRsult = verifySaltedPasswordHashAndPrepareItsHistory(dbUser, password);
                
                dbUser.setUUserSaltedPasswordHash(sltHistRsult.getCurrentSaltedPasswordHash());                
                dbUser.setUUserSaltedPasswordHashHistory(sltHistRsult.getSaltedPasswordHashHistory());
                
                // Reset old password history and unsaltd password hash history
                dbUser.setUPasswordHistory(null);
                dbUser.setUUserPassword(null);
            }
            else
            {
                String passwordHistory = verifyPasswordAndPrepareItsHistory(dbUser, encPasswordFromUI);
                dbUser.setUPasswordHistory(passwordHistory);
                dbUser.setUUserPassword(encPasswordFromUI);
            }
            
            dbUser.setUPasswordNeedsReset(false);

            SaveUtil.saveUser(dbUser, username);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    } // updatePassword

    public static UsersVO saveUser(UsersVO user, String cloneOfUser, String username) throws Exception
    {
        if(user != null && user.validate() && StringUtils.isNotEmpty(username))
        {
            
            boolean sendNotification = false;
            boolean insertOperation = false;
            boolean invalidateUserMenuCache = false;
            
            if(user.getUUserName().equalsIgnoreCase("system") && !username.equalsIgnoreCase("resolve.maint"))
            {
                Log.syslog.warn(String.format("[Audit] %s %s %s - %s", InetAddress.getLocalHost().getHostAddress(), user.getUUserName(), 
                                InetAddress.getLocalHost().getHostAddress(), username + " failed to update user"));
                
                throw new Exception("'system' user can only be updated by resolve.maint.");
            }
            
            //first decide if this user has the rights to do this operation
            boolean isAdminUser = isAdminUser(username);
            Log.auth.trace("Into saveUser - user:" + username + " is admin ?:" + isAdminUser);
            Log.auth.trace("User Getting saved:" + user.getUUserName());
            Log.auth.trace("User Roles:" + (user.getUserRoles()!=null ? user.getUserRoles().size() : 0));
            Log.auth.trace("User Group:" + (user.getUserGroups()!=null ? user.getUserGroups().size() : 0));
            
            // From Corona onwarrds UI will pass the password in plain text and it will get hashed on the server
            
            String plainTextPasswordFromUI = user.getUUserP_assword();
            
            if(StringUtils.isBlank(plainTextPasswordFromUI))
            {
                throw new Exception("Password cannot be empty. Please provide a valid password.");
            }
            
            String encPas_swordFromUI = com.resolve.services.util.UserUtils.encryptP_asswd(plainTextPasswordFromUI);
            
            String encSaltedPas_swordHash = null;
            
            Users dbUser = getUserPrivate(null, user.getUUserName());
            
            if (dbUser == null && StringUtils.isNotBlank(user.getSys_id()) && !user.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                dbUser = getUserPrivate(user.getSys_id(), null);
                if (dbUser != null)
                {
                    // This is an error. User who is getting saved, no user with User.user_name exist in DB, whereas user with User.sysId exist.
                    throw new Exception("Looks like save user request is been tampered. Please verify the request and try again.");
                }
            }
            
            License currLic = MainBase.main.getLicenseService().getCurrentLicense();
            
            if (currLic == null) {
            	Log.alert(ERR.E10026.getCode(), ERR.E10026.getMessage() + " Shutting Down!!!", 
            			  Constants.ALERT_TYPE_LICENSE);
        		Log.log.fatal(ERR.E10026.getMessage() + " Shutting Down!!!");
        		System.exit(1);
        	}
            
            if(dbUser!= null && StringUtils.isNotBlank(user.getSys_id()) && 
               !user.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                if(!isAdminUser && !username.equalsIgnoreCase(dbUser.getUUserName()) && !AppsUtil.doesUserHaveRightsForController(Constants.USER_SAVE_USER, username))
                {
                    Log.syslog.warn(String.format("[Audit] %s %s %s - %s", InetAddress.getLocalHost().getHostAddress(), dbUser.getUUserName(), 
                                    InetAddress.getLocalHost().getHostAddress(), username + " failed to update user"));
                    
                    throw new Exception("User " + username + " does not have rights to update user");
                }
                
                // V2 expired license NON-PROD environments/instance types enforcement
                
                if (currLic.isV2() && currLic.isExpired() && 
            		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(currLic.getEnv()) &&
            		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(
            																	MainBase.main.configGeneral.getEnv())) {
                	String msg = ERR.E10024.getMessage()
                				 .replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
                						 	   String.format("user %s", dbUser.getUUserName()));
            		Log.alert(ERR.E10024.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
            		throw new Exception(msg);
            	}
                
                // Check for User Count violated V2 license in NON-PROD environments/instances
                
                if (currLic.isV2() && MainBase.main.getLicenseService().getIsUserCountViolated() && 
            		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(currLic.getEnv()) &&
            		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(
            																	MainBase.main.configGeneral.getEnv())) {
                	String msg = ERR.E10031.getMessage()
                				 .replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
                						 	   String.format("user %s", dbUser.getUUserName()));
            		Log.alert(ERR.E10031.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
            		throw new Exception(msg);
            	}
                
                // Check for HA violated V2 license in NON-PROD environments/instances
                
                if (currLic.isV2() && MainBase.main.getLicenseService().getIsHAViolated() && 
            		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(currLic.getEnv()) &&
            		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(
            																	MainBase.main.configGeneral.getEnv())) {
                	String msg = ERR.E10030.getMessage()
                				 .replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, 
                						 	   String.format("user %s", dbUser.getUUserName()));
            		Log.alert(ERR.E10030.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
            		throw new Exception(msg);
            	}
                
                // Update salted password hash if it exists in DB if password has changed
                
                if (StringUtils.isNotEmpty(dbUser.getUUserSaltedPasswordHash()) && 
                    !dbUser.getUUserSaltedPasswordHash().equalsIgnoreCase(VO.STRING_DEFAULT))
                {
                    String encSaltedPasswordHashFromDB = dbUser.getUUserSaltedPasswordHash();
                    
                    if (plainTextPasswordFromUI.equalsIgnoreCase(VO.STRING_DEFAULT))
                    {
                        encSaltedPas_swordHash = encSaltedPasswordHashFromDB;
                    }
                    else
                    {
                        validatePassword(plainTextPasswordFromUI);
                        encSaltedPas_swordHash = com.resolve.services.util.UserUtils.bcryptPasswdHash(plainTextPasswordFromUI);
                    }
                    
                    user.setUUserSaltedPasswordHash(encSaltedPas_swordHash);
                    
                    // Check if password changed, if yes then re-hash new password with new salt
                    
                    if (!encSaltedPasswordHashFromDB.equals(encSaltedPas_swordHash))
                    {
                        SaltedHistoryResult sltHistRsult = verifySaltedPasswordHashAndPrepareItsHistory(dbUser, plainTextPasswordFromUI);
                        
                        user.setUUserSaltedPasswordHash(sltHistRsult.getCurrentSaltedPasswordHash());                        
                        user.setUUserSaltedPasswordHashHistory(sltHistRsult.getSaltedPasswordHashHistory());
                    }
                    else
                    {
                        user.setUUserSaltedPasswordHashHistory(dbUser.getUUserSaltedPasswordHashHistory());
                    }
                    
                    // Reset old password history and un-salted password hash history from value object
                    user.setUPasswordHistory(null);
                    user.setUUserP_assword(null);
                    
                    // Reset old password history and un-salted password hash history from model
                    dbUser.setUPasswordHistory(null);
                    dbUser.setUUserPassword(null);
                }
                
                String encPasswordFromDB = dbUser.getUUserPassword();
                if (StringUtils.isNotEmpty(encPasswordFromDB) && !encPasswordFromDB.equalsIgnoreCase(VO.STRING_DEFAULT) &&
                    StringUtils.isNotEmpty(encPas_swordFromUI) && !encPas_swordFromUI.equalsIgnoreCase(VO.STRING_DEFAULT))
                {
                    // Uploading profile pic locks out user (http://10.20.2.111/browse/RBA-13209)
                    // String hashDummyPasswd = com.resolve.util.UserUtils.encryptP_asswd(DUMMY_USER_P_ASSWORD);
                    String hashDummyPasswd = com.resolve.services.util.UserUtils.encryptP_asswd(VO.STRING_DEFAULT);
                    //String resolvePwd = com.resolve.util.UserUtils.encryptPasswd(SKIP_USER_PASSWORD);

                    if (plainTextPasswordFromUI.equalsIgnoreCase(VO.STRING_DEFAULT))
                    {
                        encPas_swordFromUI = encPasswordFromDB;
                    }
                    
                    // check if password has changed
                    //ignore it if its the default 'resolve' pwd
                    if (encPas_swordFromUI.equals(hashDummyPasswd) || 
                        // Uploading profile pic locks out user (http://10.20.2.111/browse/RBA-13209)
                        // encPasswordFromUI.equals(DUMMY_USER_P_ASSWORD) ||
                        encPas_swordFromUI.equals(VO.STRING_DEFAULT) ||
                        encPas_swordFromUI.equals(encPasswordFromDB))
                    {
                        user.setUUserP_assword(dbUser.getUUserPassword());
                    }
                    else
                    {
                        verifyPasswordAndPrepareItsHistory(dbUser, encPas_swordFromUI);

                        //If here means updated password passed history check
                    
                        // Migrate the user with un-salted password hash to salted password hash
                        
                        encSaltedPas_swordHash = com.resolve.services.util.UserUtils.bcryptPasswdHash(plainTextPasswordFromUI);;
                        
                        user.setUUserSaltedPasswordHash(encSaltedPas_swordHash);
                        
                        //create the salted password hash history, old unsalted password history will be lost
                        user.setUUserSaltedPasswordHashHistory(encSaltedPas_swordHash);
                        
                        // Reset old password history and un-salted password hash history from value object
                        user.setUPasswordHistory(null);
                        user.setUUserP_assword(null);
                        
                        // Reset old password history and un-salted password hash history from model
                        dbUser.setUPasswordHistory(null);
                        dbUser.setUUserPassword(null);
                    }
                }//end of if
                
                if(user.getULockedOut() == null || user.getULockedOut() == false)
                {
                    user.setUFailedAttempts(0);//reset the value to 0;
                }
            }
            else
            {
                insertOperation = true;

                //only admin user can create new users
                if(!isAdminUser)
                {
                    Log.syslog.warn(String.format("[Audit] %s %s %s - %s", InetAddress.getLocalHost().getHostAddress(), user.getUUserName(), 
                                    InetAddress.getLocalHost().getHostAddress(), "User " + username + " does not have admin rights to create users"));
                    
                    throw new Exception("User " + username + " does not have admin rights to create users.");
                }
                
                //if its a new user, check if this user exist
                boolean isUserExist = (dbUser != null) ? true : false;
                if (isUserExist)
                {
                    Log.syslog.warn(String.format("[Audit] %s %s %s - %s", InetAddress.getLocalHost().getHostAddress(), user.getUUserName(), 
                                    InetAddress.getLocalHost().getHostAddress(), "User " + username + " tried to create an existing user"));
                    
                    throw new Exception("User Name " + user.getUUserName() + " already exists");
                }

                // V2 license max # of users enforcement
                v2LicenseUserEnforcement(currLic);
            	            	
                if (!plainTextPasswordFromUI.equals(VO.STRING_DEFAULT)) {
                	validatePassword(plainTextPasswordFromUI);
                }
                encSaltedPas_swordHash = com.resolve.services.util.UserUtils.bcryptPasswdHash(plainTextPasswordFromUI);
                
                user.setUUserSaltedPasswordHash(encSaltedPas_swordHash);
                user.setUUserP_assword(null);
                
                //if its a clone/copy, update the image to null, set the recovery options to null
                if(StringUtils.isNotBlank(cloneOfUser) && !cloneOfUser.equalsIgnoreCase(VO.STRING_DEFAULT))
                {
                    user.setUImage(null);
                    user.setUTempImage(null);
                    
                    //recovery options
                    user.setUQuestion1(null);
                    user.setUQuestion2(null);
                    user.setUQuestion3(null);
                    user.setUAnswer1(null);
                    user.setUAnswer2(null);
                    user.setUAnswer3(null);
                }
                
                //create salted password hash history
                user.setUUserSaltedPasswordHashHistory(encSaltedPas_swordHash);
            }

            //apply the UI values to the model
            if(dbUser != null)
            {
                if(!stringCompare(user.getUFirstName(), dbUser.getUFirstName())
                                || !stringCompare(user.getULastName(),dbUser.getULastName())
                                || !stringCompare(user.getULastName(), dbUser.getULastName())
                                || !stringCompare(user.getUTitle(), dbUser.getUTitle())
                                || !stringCompare(user.getUSummary(), dbUser.getUSummary())
                                || !stringCompare(user.getUEmail(), dbUser.getUEmail())
                                || !stringCompare(user.getUPhone(), dbUser.getUPhone())
                                || !stringCompare(user.getUMobilePhone(), dbUser.getUMobilePhone())
                                || !stringCompare(user.getUFax(), dbUser.getUFax())
                                || !stringCompare(user.getUIM(), dbUser.getUIM())
                                || !stringCompare(user.getUAddress1(), dbUser.getUAddress1())
                                || !stringCompare(user.getUCity(), dbUser.getUCity())
                                || !stringCompare(user.getUStreet(), dbUser.getUStreet())
                                || !stringCompare(user.getUZip(), dbUser.getUZip())
                                || !stringCompare(user.getUCountry(), dbUser.getUCountry())
                                )
                {
                    sendNotification = true;
                }
                
                //log if the user is 'locked'
                if(user.getULockedOut() != null && user.getULockedOut() && !dbUser.ugetULockedOut())
                {
                    Log.log.info("User '" + dbUser.getUUserName() + "' is LOCKED");
                    Log.auth.info("User '" + dbUser.getUUserName() + "' is LOCKED");
                }
                
                dbUser.applyVOToModel(user, false);
            }
            else
            {
                dbUser = new Users(user);
                
                //log if the user is 'locked'
                if(dbUser.ugetULockedOut())
                {
                    Log.log.info("User '" + dbUser.getUUserName() + "' is LOCKED");
                    Log.auth.info("User '" + dbUser.getUUserName() + "' is LOCKED");
                }
            }

            //Password Needs Reset or Locked Out flag can only be set by super user
            boolean updatedPasswordNeedsReset = false;
            boolean updatedLockedOut = false;
            
            if (isAdminUser) {
            	updatedPasswordNeedsReset = user.ugetUPasswordNeedsReset() != dbUser.ugetUPasswordNeedsReset();
            	updatedLockedOut = user.ugetULockedOut() != dbUser.ugetULockedOut();
            }
            
            //persist the object
            dbUser = SaveUtil.saveUser(dbUser, username);
                        
            //update preferences
            List<UserPreferencesVO> socialPreferences = user.getSocialPreferences();
            updateUserPreferences(dbUser, socialPreferences, username);

            //groups and roles can be assigned by the admin only
            if (isAdminUser)
            {
                boolean updatedGroups = false;
                boolean updatedRoles = false;
                boolean updatedDefaultOrg = false;
                
                Log.auth.trace("Assigning the groups and roles for the user");
                
                //groups
                List<GroupsVO> userGroups = user.getUserGroups();
                if (userGroups != null)
                {
                    Set<String> groups = new HashSet<String>();
                    for (GroupsVO group : userGroups)
                    {
                        groups.add(group.getUName());
                        Log.auth.trace("Adding Group:" + group.getUName());
                    }

                    //add groups to this user
                    updatedGroups = addGroupsToUser(dbUser.getSys_id(), null, new ArrayList<String>(groups), username);
                }
                
                //roles
                List<RolesVO> userRoles = user.getUserRoles();
                if (userRoles != null)
                {
                    Set<String> roles = new HashSet<String>();
                    for (RolesVO role : userRoles)
                    {
                        roles.add(role.getUName());
                        Log.auth.trace("Adding Role:" + role.getUName());
                    }
                    
                    //add the 'resolve_user' by default for the first time
                    if(insertOperation)
                    {
                        roles.add("resolve_user");
                    }

                    //add the roles to this user
                    updatedRoles = addRolesToUser(dbUser.getSys_id(), null, new ArrayList<String>(roles), true, username);
                }
                
                //orgs
                
                Set<OrgsVO> userOrgs = user.getUserOrgs();
                
                String oldDefaultSysOrg = dbUser.getSysOrg();
                
                dbUser.setSysOrg(null);
                
                if (userOrgs != null && !userOrgs.isEmpty())
                {
                    for (OrgsVO orgsVO : userOrgs)
                    {
                        if (orgsVO.getIsDefaultOrg().booleanValue())
                        {
                            dbUser.setSysOrg(orgsVO.getSys_id());
                            updatedDefaultOrg = true;
                            break;
                        }
                    }
                }
                
                if (!updatedDefaultOrg)
                {
                    if (StringUtils.isBlank(oldDefaultSysOrg) && StringUtils.isNotBlank(dbUser.getSysOrg()))
                    {
                        updatedDefaultOrg = true;
                    }
                    else if (StringUtils.isNotBlank(oldDefaultSysOrg) && StringUtils.isBlank(dbUser.getSysOrg()))
                    {
                        updatedDefaultOrg = true;
                    }
                    else if (StringUtils.isNotBlank(oldDefaultSysOrg) && StringUtils.isNotBlank(dbUser.getSysOrg()))
                    {
                        updatedDefaultOrg = !oldDefaultSysOrg.equals(dbUser.getSysOrg());
                    }
                }
                
                if (updatedDefaultOrg)
                {
                    dbUser = SaveUtil.saveUser(dbUser, username);
                }
                
                invalidateUserMenuCache = true;
                
                if (!insertOperation && 
                	(updatedGroups || updatedRoles || updatedDefaultOrg || updatedPasswordNeedsReset || updatedLockedOut))
                {
                    HibernateUtil.clearCachedUser(user.getUUserName(), true);
                    String updatedUserMsg = String.format(USER_UPDATED_AUDIT_LOG, 
                    									  InetAddress.getLocalHost().getHostAddress(), 
                    									  dbUser.getUUserName(), 
                    									  InetAddress.getLocalHost().getHostAddress(), 
                    									  username);
                    Log.log.debug(updatedUserMsg);
                    Log.syslog.info(updatedUserMsg);
                }
            }//end of the if clause for the admin user
            
            //create/update user node in the graph DB
            createUserInGraphDB(user.getUUserName());
            
            //get the UserVO 
            user = findUserById(dbUser.getSys_id()); 
            //user = getUserNoCache(dbUser.getUUserName());
            
            //if its cloned, copy the components followed by the cloned user
            if(StringUtils.isNotBlank(cloneOfUser) && !cloneOfUser.equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                copyGraphComponents(cloneOfUser, user.getUUserName());
            }
            
            //send the notification of Profile change
            if(sendNotification)
            {
                try
                {
                    User socialUser = SocialCompConversionUtil.convertUserToSocialUser(dbUser);
                    NotificationHelper.getUserNotification(socialUser, UserGlobalNotificationContainerType.USER_PROFILE_CHANGE, username, true, true);
                }
                catch (Exception e)
                {
                    Log.log.debug("Cannot send the notification for the user due to an error", e);
                }
            }
            
          //invalidate the cache
            if(invalidateUserMenuCache)
            {
                MenuCache.getInstance().invalidate();
            }
            
            if (insertOperation)
            {
                Log.syslog.info(String.format("[Audit] %s %s %s - %s", InetAddress.getLocalHost().getHostAddress(), dbUser.getUUserName(), 
                                InetAddress.getLocalHost().getHostAddress(), username + " created user"));
            }
        }

        return user;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<UsersVO> getAllUsers(boolean isLocked)
    {
        List<UsersVO> usersVOs = new ArrayList<UsersVO>();
        List<Users> users = null;

        String sql = "select u from Users as u where u.ULockedOut = :lockedOut";
        
        try
        {
        	users = (List<Users>) HibernateProxy.execute(() -> {
        		List<Users> usersLocal = null;

        		Query q = HibernateUtil.createQuery(sql);
                q.setParameter("lockedOut", Boolean.FALSE);
                usersLocal = q.list();

                if(usersLocal != null)
                {
                    for (Users user : usersLocal)
                    {
                        setUserRoles(user);
                    }
                }
                
                return usersLocal;
        	});
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if(users != null)
        {
            for(Users u : users)
            {
                usersVOs.add(u.doGetVO());
            }
        }

        return usersVOs;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<UsersVO> getAllUsers(String whereCaluse)
    {
        List<UsersVO> usersVOs = new ArrayList<UsersVO>();
        List<Users> users = null;

        StringBuffer sql = new StringBuffer("from Users ");
        if (StringUtils.isNotBlank(whereCaluse))
        {
            sql.append(whereCaluse);
        }
        
        try
        {
        	users = (List<Users>) HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sql.toString());
                return q.list();
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if(users != null)
        {
            for(Users u : users)
            {
                usersVOs.add(u.doGetVO());
            }
        }

        return usersVOs;
    }

    public static List<ComboboxModelDTO> getAllUsersForMentions(String typeAheadString)
    {
        List<ComboboxModelDTO>  results = new ArrayList<ComboboxModelDTO>();
        String sql = "select UUserName, UFirstName, ULastName from Users  ";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        if(StringUtils.isNotEmpty(typeAheadString))
        {
            typeAheadString = typeAheadString.toLowerCase();
            /*sql +=  " where LOWER(UUserName) like '%" + typeAheadString + "%' OR " +
                        " LOWER(UFirstName) like '%" + typeAheadString + "%' OR " +
                        " LOWER(ULastName) like '%" + typeAheadString + "%'  " +
                        "  order by UUserName";*/
            sql +=  " where LOWER(UUserName) like :typeAheadString OR " +
                            " LOWER(UFirstName) like :typeAheadString OR " +
                            " LOWER(ULastName) like :typeAheadString " +
                            "  order by UUserName";
            
            queryParams.put("typeAheadString", "%" + typeAheadString + "%");
        }

        try
        {
            List<? extends Object> data = ServiceHibernate.executeHQLSelect(sql, queryParams);
            if(data != null && data.size() > 0)
            {
                for (Object obj : data)
                {
                    Object[] arr = (Object[]) obj;
                    String UUserName = (String) arr[0];
                    if(StringUtils.isEmpty(UUserName)
                                    || UUserName.equalsIgnoreCase("dev")
                                    || UUserName.equalsIgnoreCase("process")
                                    || UUserName.equalsIgnoreCase("user")
                                    || UUserName.equalsIgnoreCase("test")
                                    || UUserName.equalsIgnoreCase("system")
                                    || UUserName.equalsIgnoreCase("public")
                                    )
                    {
                        continue;
                    }

                    String UFirstName = arr[1] != null ? (String) arr[1] : "";
                    String ULastName = arr[2] != null ? (String) arr[2] : "";

                    String fullname = (StringUtils.isEmpty(UFirstName) && StringUtils.isEmpty(ULastName)) ? UUserName : (UFirstName + " " + ULastName);

                    results.add(new ComboboxModelDTO(fullname, UUserName));
                }//end of for loop

                //sort it
                Collections.sort(results);

            }//end of if
        }
        catch (Exception e)
        {
            Log.log.error("Error getting all users sql:" + sql, e);
        }

        return results;
    }

    public static List<UsersVO> getUsers(String whereClause, String sortField, boolean asc, int limit, int offset)
    {
        List<? extends Object> users = null;
        List<UsersVO> result = new ArrayList<UsersVO>();
        String sql = " from Users ";
        if(StringUtils.isNotBlank(whereClause))
        {
            sql = sql + whereClause;
        }

        if(StringUtils.isNotBlank(sortField))
        {
            sql = sql + " order by " + sortField;
            if(!asc)
            {
                sql = sql + " desc";
            }
        }

        try
        {
            users = GeneralHibernateUtil.executeHQLSelect(sql, offset, limit);
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        if(users != null)
        {
            for(Object o : users)
            {
                Users model = (Users) o;
                result.add(model.doGetVO());
            }
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<RolesVO> getAllRoles(String whereCaluse)
    {
        List<RolesVO> roleVOs = new ArrayList<RolesVO>();
        List<Roles> roles = null;

        StringBuffer sql = new StringBuffer("from Roles ");
        if (StringUtils.isNotBlank(whereCaluse))
        {
            sql.append(whereCaluse);
        }
        try
        {
        	roles = (List<Roles>) HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sql.toString());
                return q.list();
        	});            

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if(roles != null)
        {
            for(Roles role : roles)
            {
                roleVOs.add(role.doGetVO());
            }
        }

        return roleVOs;
    }

    public static List<RolesVO> getRolesForGroup(String groupSysId, String groupName)
    {
        List<RolesVO> roleVOs = new ArrayList<RolesVO>();
        List<Roles> roles = new ArrayList<Roles>();        
        try
        {
            HibernateProxy.execute(() -> {
            	Groups group = findGroupBy(groupSysId, groupName);
                if(group != null)
                {
                    Collection<GroupRoleRel> groupRoleRels = group.getGroupRoleRels();
                    if(groupRoleRels != null)
                    {
                        for(GroupRoleRel rel : groupRoleRels)
                        {
                            roles.add(rel.getRole());
                        }
                    }
                }
            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if(roles != null)
        {
            for(Roles role : roles)
            {
                roleVOs.add(role.doGetVO());
            }
        }

        return roleVOs;
    }

    public static List<UsersVO> getUsersForGroup(String groupSysId, String groupName)
    {
        List<UsersVO> result = new ArrayList<UsersVO>();
        List<Users> users = new ArrayList<Users>();

        try
        {
        	HibernateProxy.execute(() -> {
                Groups group = findGroupBy(groupSysId, groupName);
                if(group != null)
                {
                    Collection<UserGroupRel> userGroupRel = group.getUserGroupRels();
                    if(userGroupRel != null)
                    {
                        for(UserGroupRel rel : userGroupRel)
                        {
                            users.add(rel.getUser());
                        }
                    }
                }
        	});

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if(users != null)
        {
            for(Users user : users)
            {
                result.add(user.doGetVO());
            }
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<GroupsVO> getAllGroups(String whereCaluse)
    {
        List<GroupsVO> groupVOs = new ArrayList<GroupsVO>();
        List<Groups> groups = null;

        StringBuffer sql = new StringBuffer("from Groups ");
        if (StringUtils.isNotBlank(whereCaluse))
        {
            sql.append(whereCaluse);
        }
        try
        {
        	groups = (List<Groups>) HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sql.toString());
                return q.list();
        	});

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if(groups != null)
        {
            for(Groups group : groups)
            {
                groupVOs.add(convertModelToVOForGroups(group, null, null));
            }
        }

        return groupVOs;
    }

    public static List<GroupsVO> getGroups(QueryDTO query, String username)
    {
    	return getGroups(query, username, false);
    }
    
    public static List<GroupsVO> getGroups(QueryDTO query, String username, boolean noUsers)
    {
        List<GroupsVO> result = new ArrayList<GroupsVO>();
//        UsersVO user = UserUtils.getUser(username);
//        String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

//        String hql = "from Properties ";
//        if(StringUtils.isNotBlank(userOrg))
//        {
//            hql = hql + " where sysOrg is null or sysOrg = '" + userOrg + "' ";
//        }
//        hql = hql + " order by sysOrg, UName";

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null && data.size() > 0)
            {
                //grab the map of sysId-organizationname
                Map<String, String> mapOfOrganization = OrgsUtil.getListOfAllOrgNames();

                //get a map of groups with their roles
                Map<String, String> mapofGroupAndRoles = getMapOfGroupAndRoles();

                //prepare the data
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Groups instance = new Groups();

                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        //add it to the list after converting to VO
                        result.add(convertModelToVOForGroups(getGroupModelNoCache(instance.getSys_id(), instance.getUName(), noUsers), 
                                                             mapOfOrganization, mapofGroupAndRoles, true));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        Groups instance = (Groups) o;
                        result.add(convertModelToVOForGroups(getGroupModelNoCache(instance.getSys_id(), instance.getUName(), noUsers), 
                                                             mapOfOrganization, mapofGroupAndRoles, true));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Set<String> getAllUsernames(boolean isLocked)
    {
        Set<String> users = new HashSet<String>();
        List<String> userList = null;
        String sql = "select UUserName from Users where ULockedOut = :lockedOut";

        try
        {
        	userList = (List<String>) HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sql);
                q.setParameter("lockedOut", Boolean.FALSE);

                return q.list();
        	});
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if (userList != null)
        {
            users.addAll(userList);
        }

        return users;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static UsersVO getUserByEmail(String email)
    {
        Users userModel = null;
        UsersVO resultVO = null;
        List<Users> users = new ArrayList<Users>();

        try
        {

            Query query = HibernateUtil.createQuery("select u from Users as u where u.ULockedOut = :lockedOut and u.UEmail like :email");
            query.setParameter("email", "%" + email + "%");
            query.setParameter("lockedOut", Boolean.FALSE);

            users = (List<Users>) HibernateProxy.execute(() -> {
            	return query.list();
            });
            		

            if (users != null && users.size() > 0)
            {
                userModel = users.get(0); // get the first one.
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if (userModel == null)
        {
            Log.log.warn("User with the email: " + email + " not found, please check.");
        }
        else
        {
            resultVO = userModel.doGetVO();
        }

        return resultVO;
    } // getUserByEmail

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static UsersVO getUserByIM(String im)
    {
        Users userModel = null;
        UsersVO resultVO = null;        

        try
        {
        	userModel = (Users) HibernateProxy.execute(() -> {
        		
        		List<Users> users = new ArrayList<Users>();
        		
        		 Query query = HibernateUtil.createQuery("select u from Users as u where u.ULockedOut = :lockedOut and u.UIM like :im");
                 query.setParameter("im", "%" + im + "%");
                 query.setParameter("lockedOut", Boolean.FALSE);

                 users = query.list();

                 if (users != null && users.size() > 0)
                 {
                     return users.get(0); // get the first one.
                 }
                 return null;
            });
           

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if (userModel == null)
        {
            Log.log.warn("User with the email: " + im + " not found, please check.");
        }
        else
        {
            resultVO = userModel.doGetVO();
        }

        return resultVO;
    } // getUserByEmail

    public static RolesVO saveRole(RolesVO vo, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        checkRBACDependency(vo.getUPermissions());
        
        if(vo != null && vo.validate())
        {
            Roles model = null;
            boolean isInsert = (StringUtils.isBlank(vo.getSys_id()) || vo.getSys_id().equals(VO.STRING_DEFAULT)) ? true : false;

            if(isInsert)
            {
                model = getRoleModel(null, vo.getUName());
                if(model != null)
                {
                    throw new RuntimeException("Role '" + vo.getUName() + "' already exist. Please try again.");
                }

                model = new Roles();
            }
            else
            {
                //UPDATE
                //first check if the role with the same name already exist
                model = getRoleModel(null, vo.getUName());
                if(model != null && !vo.getSys_id().equalsIgnoreCase(model.getSys_id()))
                {
                    throw new Exception("Role with name " + vo.getUName() + " already exist");
                }
                
                model = getRoleModel(vo.getSys_id(), null);
            }

            model.applyVOToModel(vo);
            model = SaveUtil.saveRoles(model, username);
            vo = model.doGetVO();

        }

        return vo;
    }
    
    private static void checkRBACDependency(JSONObject permissions) throws Exception
    {
        if (permissions != null)
        {
            Map<String, JSONObject> rulesMap = new RBACDependencyRules().getRules();
            for (Object key : permissions.keySet())
            {
                JSONObject ruleJSONObject = rulesMap.get(key.toString());
                if (ruleJSONObject != null)
                {
                    JSONObject o = (JSONObject)permissions.get(key);
                    
                    for (Object accessObj : ruleJSONObject.keySet())
                    {
                        boolean permission = o.get(accessObj.toString()) == null ? false : o.get(accessObj.toString()).equals("disabled") ? false : o.getBoolean(accessObj.toString());
                        if (permission)
                        {
                            boolean success = true;
                            JSONArray viewRuleArray = (JSONArray)ruleJSONObject.get(accessObj.toString());
                            for (Object viewRule : viewRuleArray)
                            {
                                String[] keyProperty = viewRule.toString().split("\\.");
                                success = ((JSONObject)permissions.get(keyProperty[0])).getBoolean(keyProperty[1]);
                                if (success == false)
                                {
                                    break;
                                }
                            }
                            
                            if (success == false)
                            {
                                throw new Exception("Rule dependency check failed. Please verify the permissions and try again.");
                            }
                        }
                    }
                }
            }
        }
    }

    public static GroupsVO saveGroup(GroupsVO vo, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);

        if (StringUtils.isNotBlank(vo.getId()) && !vo.getId().equalsIgnoreCase(VO.STRING_DEFAULT) &&
            (StringUtils.isBlank(vo.getSys_id()) || !vo.getSys_id().equals(vo.getId())))
        {
            vo.setSys_id(vo.getId());
        }
        
        if(vo != null && vo.validate())
        {
            Groups model = null;

            boolean isInsert = (StringUtils.isBlank(vo.getSys_id()) || vo.getSys_id().equals(VO.STRING_DEFAULT)) ? true : false;
            boolean invalidateUserMenuCache = false;

            if(isInsert)
            {
                model = getGroupModel(null, vo.getUName());
                if(model != null)
                {
                    throw new RuntimeException("Group '" + vo.getUName() + "' already exist. Please try again.");
                }

                vo.setSysCreatedOn(null);
                vo.setSysUpdatedOn(null);
                vo.setSysCreatedBy(username);
                vo.setSysUpdatedBy(username);
                
                model = new Groups();
            }
            else
            {
                //UPDATE
                //first check if the group with the same name already exist
                model = getGroupModel(null, vo.getUName());
                if(model != null && !vo.getSys_id().equalsIgnoreCase(model.getSys_id()))
                {
                    throw new Exception("Group with name " + vo.getUName() + " already exist");
                }
                
                model = getGroupModel(vo.getSys_id(), null);
            }

            model.applyVOToModel(vo);
            model = SaveUtil.saveGroup(model, username);


            //add roles to group
            Collection<RolesVO> groupRoles = vo.getGroupRoles();
            if(groupRoles != null)
            {
                Set<String> roleSysIds = new HashSet<String>();
                for(RolesVO role : groupRoles)
                {
                    roleSysIds.add(role.getSys_id());
                }

                addRolesToGroup(model.getSys_id(), roleSysIds, username, true);
                invalidateUserMenuCache = true;
            }

            //add Users to group
            Collection<UsersVO> groupUsers = vo.getGroupUsers();
            if(groupUsers != null)
            {
                Set<String> userSysIds = new HashSet<String>();
                for(UsersVO user : groupUsers)
                {
                    userSysIds.add(user.getSys_id());
                }

                addUsersToGroup(model.getSys_id(), userSysIds, username, true);
                invalidateUserMenuCache = true;
            }
            
            //add orgs to group
            Collection<OrgsVO> groupOrgs = vo.getGroupOrgs();
            
            if ( groupOrgs != null )
            {
                Set<String> orgSysIds = new HashSet<String>();
                
                for ( OrgsVO org : groupOrgs)
                {
                    orgSysIds.add(org.getSys_id());
                }
                
                addOrgsToGroup(model.getSys_id(), orgSysIds, username, true);
                
                invalidateUserMenuCache = true;
            }

            //sync Group with Team if the flag is set to true
            if(model.ugetUIsLinkToTeam())
            {
                syncGroupWithTeam(model, username);
            }


//            vo = convertModelToVOForGroups(model, null, null);//model.doGetVO();
            vo = getGroupNoCache(model.getSys_id(), null, username);
            
            //invalidate the cache
            if(invalidateUserMenuCache)
            {
                MenuCache.getInstance().invalidate();
            }
        }

        return vo;
    }

    @SuppressWarnings("unchecked")
    public static void updateGroupLinkToTeamFlag(String groupName, boolean linkToTeam, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            String sql = "from Groups where LOWER(UName) = '" + groupName.trim().toLowerCase() + "'";
	            List<Groups> list = HibernateUtil.createQuery(sql).list();
	            if (list != null && list.size() > 0)
	            {
	                Groups model = list.get(0);
	                model.setUIsLinkToTeam(linkToTeam);
	
	                HibernateUtil.getDAOFactory().getGroupsDAO().persist(model);
	            }

            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }

    public static void addRolesToGroup(String groupSysId, String groupName, List<String> roles, boolean deleteRolesNotInList, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        List<String> localRoles = new ArrayList<String>(roles);
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

            	Groups group = null;
	            RolesDAO dao = HibernateUtil.getDAOFactory().getRolesDAO();
	            GroupRoleRelDAO grRelDAO = HibernateUtil.getDAOFactory().getGroupRoleRelDAO();
	
	            group = findGroupBy(groupSysId, groupName);
	
	            if(group != null)
	            {
	                Collection<GroupRoleRel> groupRoleRel = group.getGroupRoleRels();
	                if(groupRoleRel != null)
	                {
	                    for(GroupRoleRel rel : groupRoleRel)
	                    {
	                        Roles role = rel.getRole();
	                        String roleName = role.getUName();
	                        if(localRoles.contains(roleName))
	                        {
	                            //already exist, so no need to add again
	                            localRoles.remove(roleName);
	                        }
	                        else
	                        {
	                            if(deleteRolesNotInList)
	                            {
	                                //delete this role as it has been removed from this group
	                                grRelDAO.delete(rel);
	                            }
	                        }
	                    }//end of for loop
	                }
	
	                if(localRoles.size() > 0)
	                {
	                    //these are the new ones to be added to this group
	                    for(String role : localRoles)
	                    {
	                        Roles roleToAdd = new Roles();
	                        roleToAdd.setUName(role);
	                        roleToAdd = dao.findFirst(roleToAdd);
	
	                        if(roleToAdd != null)
	                        {
	                            GroupRoleRel grRel = new GroupRoleRel();
	                            grRel.setGroup(group);
	                            grRel.setRole(roleToAdd);
	
	                            grRelDAO.persist(grRel);
	                        }
	                    }//end of for loop
	                }
	            }

            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }

    public static void addRolesAndGroupsToUser(String userSysId, String userName, List<String> roles, List<String> groups, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            addRolesToUser(userSysId, userName, roles, true, username);
	            addGroupsToUser(userSysId, userName, groups, username);

            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
            throw new Exception(t);
        }
    }

    public static Collection<String> addRolesAndGroupsToUser(Set<String> userSysIds, Set<String> roleSysIds, Set<String> groupSysIds, String username) throws Exception
    {
        Collection<String> userNames = new HashSet<String>();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            Collection<String> roleUserNames = addRolesToUsersPrivate(userSysIds, roleSysIds, username, false);
	            userNames.addAll(roleUserNames);
	            Collection<String> groupUserNames = addGroupsToUsersPrivate(userSysIds, groupSysIds, username, false);
	            userNames.addAll(groupUserNames);
            
            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
            throw new Exception(t);
        }
        
        return userNames;
    }

    public static Collection<String> addRolesToUsers(Set<String> userSysIds, Set<String> roleSysIds, String username) throws Exception
    {
        Collection<String> userNames = Collections.emptyList();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        try
        {
            userNames = addRolesAndGroupsToUser(userSysIds, roleSysIds, null, username);
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
            throw new Exception(t);
        }
        
        return userNames;
    }

    public static Collection<String> addGroupsToUsers(Set<String> userSysIds, Set<String> groupSysIds, String username) throws Exception
    {
        Collection<String> userNames = Collections.emptyList();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        try
        {
            userNames = addRolesAndGroupsToUser(userSysIds, null, groupSysIds, username);
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return userNames;
    }

    public static boolean addRolesToUser(String userSysId, String userName, List<String> rolesList, boolean deleteRolesNotInList, String username) throws Exception
    {
        boolean updated = false;
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        if (rolesList != null && rolesList.size() > 0)
        {
            List<String> roles = new ArrayList<String>(rolesList);
            try
            {
              HibernateProxy.setCurrentUser(username);
                updated = (boolean) HibernateProxy.execute(() -> {

	                RolesDAO dao = HibernateUtil.getDAOFactory().getRolesDAO();
	                UserRoleRelDAO userRoleRelDAO = HibernateUtil.getDAOFactory().getUserRoleRelDAO();
	
	                Users user = findUser(userSysId, userName);
	                boolean updatedResult = false;
	                if (user != null)
	                {
	                    Collection<UserRoleRel> userRoleRel = user.getUserRoleRels();
	                    if (userRoleRel != null)
	                    {
	                        for (UserRoleRel rel : userRoleRel)
	                        {
	                            Roles role = rel.getRole();
	                            String roleName = "";
	                            if (role != null)
	                            {
	                                roleName = role.getUName();
	                            }
	                            if (roles.contains(roleName))
	                            {
	                                //already exist, so no need to add again
	                                roles.remove(roleName);
	                            }
	                            else
	                            {
	                                if (deleteRolesNotInList)
	                                {
	                                    //delete this role as it has been removed from this group
	                                    userRoleRelDAO.delete(rel);
	                                    updatedResult = true;
	                                }
	                            }
	                        }//end of for loop
	                    }
	
	                    if (roles.size() > 0)
	                    {
	                        //these are the new ones to be added to this group
	                        for (String role : roles)
	                        {
	                            Roles roleToAdd = new Roles();
	                            roleToAdd.setUName(role);
	                            roleToAdd = dao.findFirst(roleToAdd);
	
	                            if (roleToAdd != null)
	                            {
	                                UserRoleRel rel = new UserRoleRel();
	                                rel.setUser(user);
	                                rel.setRole(roleToAdd);
	
	                                userRoleRelDAO.persist(rel);
	                                updatedResult = true;
	                            }
	                        }//end of for loop
	                    }
	                }

	                return updatedResult;
            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        else
        {
            //this is the condition to delete all the roles for a user 
            if(deleteRolesNotInList)
            {
                try
                {
                  HibernateProxy.setCurrentUser(username);
                    updated = (boolean) HibernateProxy.execute(() -> {
                    
	                    Users user = findUser(userSysId, userName);
	                    if (user != null)
	                    {
	                        Collection<UserRoleRel> userRoleRel = user.getUserRoleRels();
	                        if (userRoleRel != null)
	                        {
	                            for (UserRoleRel rel : userRoleRel)
	                            {
	                                HibernateUtil.getDAOFactory().getUserRoleRelDAO().delete(rel);
	                                return true;
	                            }//end of for loop
	                        }
	                    }
	                    
	                    return false;
                    });
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
            }//end of if
        }//end of else
        
        return updated;
    }

    public static boolean addGroupsToUser(String userSysId, String userName, List<String> groupsList, String username) throws Exception
    {
        boolean updated = false;
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        if (groupsList != null)
        {
            List<String> groups = new ArrayList<String>(groupsList);
            try
            {

              HibernateProxy.setCurrentUser(username);
            	updated = (boolean) HibernateProxy.execute(() -> {
            		boolean tempUpdated = false;
	                GroupsDAO dao = HibernateUtil.getDAOFactory().getGroupsDAO();
	
	                Users user = findUser(userSysId, userName);
	
	                if (user != null)
	                {
	                    Collection<UserGroupRel> userGroupRel = user.getUserGroupRels();
	                    if (userGroupRel != null)
	                    {
	                        for (UserGroupRel rel : userGroupRel)
	                        {
	                            Groups group = rel.getGroup();
	                            String groupName = "";
	                            if (group != null)
	                            {
	                                groupName = group.getUName();
	                            }
	                            if (groups.contains(groupName))
	                            {
	                                //already exist, so no need to add again
	                                groups.remove(groupName);
	                            }
	                            else
	                            {
	                                //delete this role as it has been removed from this group
	                                deleteUserGroupRel(rel, username);
	                                tempUpdated = true;
	                            }
	                        }//end of for loop
	                    }
	                    
	                    if (groups.size() > 0)
	                    {
	                        //these are the new ones to be added to this group
	                        for (String group : groups)
	                        {
	                            Groups groupToAdd = new Groups();
	                            groupToAdd.setUName(group);
	                            groupToAdd = dao.findFirst(groupToAdd);
	
	                            if (groupToAdd != null)
	                            {
	                                UserGroupRel rel = new UserGroupRel();
	                                rel.setUser(user);
	                                rel.setGroup(groupToAdd);
	
	                                saveUserGroupRel(rel, username);
	                                tempUpdated = true;
	                            }
	                        }//end of for loop
	                    }
	                }
	                
	                return tempUpdated;
            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }

            //sync the groups now with the teams
            syncGroupsWithTeam(groupsList, username);
        }
        
        return updated;
    }

    public static void addUsersToGroup(String groupSysId, String groupName, List<String> usernames, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        if (usernames != null && usernames.size() > 0 && (StringUtils.isNotBlank(groupSysId) || StringUtils.isNotBlank(groupName)))
        {
            try
            {
                Groups group = getGroupModel(groupSysId, groupName);
                if (group != null)
                {
                    try
                    {
                      HibernateProxy.setCurrentUser(username);
                        HibernateProxy.execute(() -> {

	                        for (String uname : usernames)
	                        {
	                            Users user = getUserPrivate(null, uname);
	                            if (user != null)
	                            {
	                                boolean  exist = doesUserExistForGroup(user.getSys_id(), group.getUserGroupRels());
	                                if(!exist)
	                                {
	                                    UserGroupRel rel = new UserGroupRel();
	                                    rel.setGroup(group);
	                                    rel.setUser(user);
	
	                                    HibernateUtil.getDAOFactory().getUserGroupRelDAO().persist(rel);
	                                }
	                            }
	                        }

                        });
                    }
                    catch (Throwable t)
                    {
                        Log.log.error(t.getMessage(), t);
                                              HibernateUtil.rethrowNestedTransaction(t);
                    }

                    //sync group with team
                    syncGroupWithTeam(group, username);
                }
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }
    }

    public static void removeUserFromGroups(String userSysId, String userName, List<String> groups, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        try
        {

          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                Users user = findUser(userSysId, userName);

                if (user != null)
                {
                    Collection<UserGroupRel> userGroupRelationList = user.getUserGroupRels();

                    if (userGroupRelationList != null)
                    {
                        for (UserGroupRel userGroupRel : userGroupRelationList)
                        {
                            Groups group = userGroupRel.getGroup();
                            if (group != null)
                            {
                                if (groups.contains(group.getUName()))
                                {
                                    deleteUserGroupRel(userGroupRel, username);
                                }
                            }
                        }
                    }
                }

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
            throw new Exception(t);
        }

    }

    public static Collection<String> removeUsersFromGroups(Set<String> userSysIds, Set<String> groups, String username) throws Exception
    {
        List<String> usernames = Collections.emptyList();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);

        if (userSysIds != null && groups != null && userSysIds.size() > 0 && groups.size() > 0)
        {
            //get list of usernames
            usernames = new ArrayList<String>(getUsernames(userSysIds));
            StringBuilder errors = new StringBuilder();

            //remove the groups
            for (String group : groups)
            {
                try
                {
                    removeUsersFromGroup(null, group, usernames, username);
                }
                catch (Exception e)
                {
                    Log.log.error("error removing the group " + group + " from user", e);
                    errors.append(e.getMessage());
                }
            }
            
            if(errors.length() > 0)
            {
                throw new Exception(errors.toString());
            }
        }
        
        return usernames;
    }

    public static void removeUsersFromGroup(String groupSysId, String groupName, List<String> usernames, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);

        if(StringUtils.isNotBlank(groupSysId) || StringUtils.isNotBlank(groupName))
        {
            if(usernames != null && usernames.size() > 0)
            {
                try
                {
                    //create a new instance
                    List<String> usernamesToRemoveGroup = new ArrayList<String>(usernames);

                    //get the groups object
                    Groups g = getGroupModel(groupSysId, groupName);
                    List<String> excludeGroups = new ArrayList<String>();
                    excludeGroups.add(g.getUName());

                    //for each user, verify that they will still have roles when this group is removed
                    String ignoreUsers = null;
                    for(String user : usernames)
                    {
                        boolean userStillHasRoles = doesUserHaveRolesAfterExcludingRolesAndGroups(user, excludeGroups, null);
                        if(!userStillHasRoles)
                        {
                            //remove it from the list as if we remove the group, than no roles will be there for this user
                            usernamesToRemoveGroup.remove(user);
                            if(ignoreUsers != null)
                                ignoreUsers = ignoreUsers + "," + user;
                            else
                                ignoreUsers = user;
                        }
                    }
                    
                    //if there are any recs, than go ahead
                    if (usernamesToRemoveGroup.size() > 0)
                    {
                        try
                        {

                          HibernateProxy.setCurrentUser(username);
                        	HibernateProxy.execute(() -> {
                                Groups group = HibernateUtil.getDAOFactory().getGroupsDAO().findById(g.getSys_id());
                                if (group != null)
                                {
                                    Collection<UserGroupRel> rels = group.getUserGroupRels();

                                    if (rels != null)
                                    {
                                        for (UserGroupRel rel : rels)
                                        {
                                            Users user = rel.getUser();
                                            if(user != null)
                                            {
                                                String uname = user.getUUserName();
                                                if (usernamesToRemoveGroup.contains(uname))
                                                {
                                                    deleteUserGroupRel(rel, username);
                                                }
                                            }
                                        }
                                    }
                                }
                        	});
                        }
                        catch (Throwable t)
                        {
                            Log.log.error(t.getMessage(), t);
                            throw new Exception(t);
                        }
                    }
                    
                    //throw an exception if there are any users which were ignored
                    if(ignoreUsers != null)
                    {
                        throw new Exception("Cannot remove groups for Users " + ignoreUsers);
                    }
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);                    
                    throw new Exception(t);
                }
            }
        }

    }

    public static Collection<String> removeUsersFromRoles(Set<String> userSysIds, Set<String> roles, String username) throws Exception
    {
        Collection<String> removedUserNames = new HashSet<String>();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);

        if (userSysIds != null && roles != null && userSysIds.size() > 0 && roles.size() > 0)
        {
            //get list of usernames
//            List<String> rolesList = new ArrayList<String>(getRoles(roleSysIds));
            List<String> rolesList = new ArrayList<String>(roles);
            StringBuilder errors = new StringBuilder();
            
            //remove the groups
            for (String userSysId : userSysIds)
            {
                try
                {
                    String userName = removeUserFromRoles(userSysId, null, rolesList, username);
                    
                    if (StringUtils.isNotBlank(userName))
                    {
                        removedUserNames.add(userName);
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("error removing the roles for user " + userSysId, e);
                    errors.append(e.getMessage());
                }
            }
            
            if(errors.length() > 0)
            {
                throw new Exception(errors.toString());
            }
        }
        
        return removedUserNames;
    }

    public static String removeUserFromRoles(String userSysId, String userName, List<String> roles, String username) throws Exception
    {
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        if (StringUtils.isNotBlank(userSysId) || StringUtils.isNotBlank(userName))
        {
            if (roles != null && roles.size() > 0)
            {

                //get the user object
                Users user = findUser(userSysId, userName);

                //verify if we delete the roles, will this user still have roles or groups
                boolean userStillHasRoles = doesUserHaveRolesAfterExcludingRolesAndGroups(user.getUUserName(), null, roles);
                if (!userStillHasRoles)
                {
                    throw new Exception("Cannot remove roles for user " + user.getUUserName());
                }
                
                //if all is good, than proceed
                try
                {
                	String userId = user.getSys_id();

                  HibernateProxy.setCurrentUser(username);
                	return (String) HibernateProxy.execute(() -> {
                		String removedRolesFromUserName = null;
                        Users u = HibernateUtil.getDAOFactory().getUsersDAO().findById(userId);
                        if (user != null)
                        {
                            removedRolesFromUserName = user.getUUserName();
                            
                            Collection<UserRoleRel> userRoleRelationList = u.getUserRoleRels();

                            if (userRoleRelationList != null)
                            {
                                for (UserRoleRel userRoleRel : userRoleRelationList)
                                {
                                    Roles role = userRoleRel.getRole();
                                    if (roles.contains(role.getUName()))
                                    {
                                        HibernateUtil.getDAOFactory().getUserRoleRelDAO().delete(userRoleRel);
                                    }
                                }
                            }
                        }
                        
                        return removedRolesFromUserName;
                	});
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                    return null;
                }
            }
        }
        return null;
    }

    public static Collection<String> deleteUserRolesByIDS(List<String> userRolesIDS, String username) throws Exception
    {
        Collection<String> deletedRolesFromUserNames = new HashSet<String>();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
            
	            RolesDAO dao = HibernateUtil.getDAOFactory().getRolesDAO();
	            GroupRoleRelDAO grRelDAO = HibernateUtil.getDAOFactory().getGroupRoleRelDAO();
	            UserRoleRelDAO urRelDAO = HibernateUtil.getDAOFactory().getUserRoleRelDAO();
	
	            Roles roles = null;
	
	            for (String rolesID : userRolesIDS)
	            {
	                roles = dao.findById(rolesID);
	
	                if (roles != null)
	                {
	                    // delete GroupRoleRel
	                    List<GroupRoleRel> grRelList = (List<GroupRoleRel>) roles.getGroupRoleRels();
	
	                    for (GroupRoleRel grRel : grRelList)
	                    {
	                        grRelDAO.delete(grRel);
	                    }
	
	                    // delete UserRoleRel
	                    List<UserRoleRel> urRelList = (List<UserRoleRel>) roles.getUserRoleRels();
	
	                    for (UserRoleRel urRel : urRelList)
	                    {
	                        if (urRel.getUser() != null && StringUtils.isNotBlank(urRel.getUser().getUUserName()))
	                        {
	                            deletedRolesFromUserNames.add(urRel.getUser().getUUserName());
	                        }
	                        else
	                        {
	                            Log.log.warn("User Role Relation with sys_id " + urRel.getSys_id() + " for role " + roles.getUName() + " is missing related user.");
	                        }
	                        
	                        urRelDAO.delete(urRel);
	                    }
	
	                    dao.delete(roles);
	                }
	            }

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return deletedRolesFromUserNames;
    }

    public static Collection<String> deleteRolesByIdsAndQuery(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        List<String> ids = new ArrayList<String>();

        //TODO: prepare the list of sys_ids to delete based on qry
        if (sysIds == null || sysIds.length == 0)
        {
            //prepare the sysIds with the where clause of this obj
            String selectQry = query.getSelectHQL();
            String sql = "select sys_id " + selectQry.substring(selectQry.indexOf("from"));
            System.out.println("UserUtils: #1855: Delete qry :" + sql);
        }
        else
        {
            ids.addAll(Arrays.asList(sysIds));
        }

        //delete one by one
        return deleteUserRolesByIDS(ids, username);
    }

    public static Collection<String> deleteUserGroupByIDS(List<String> userGroupIDS, String username) throws Exception
    {
        Collection<String> deletedGroupFromUserNames = new HashSet<String>();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		
	            GroupsDAO dao = HibernateUtil.getDAOFactory().getGroupsDAO();
	            GroupRoleRelDAO grRelDAO = HibernateUtil.getDAOFactory().getGroupRoleRelDAO();
	            Groups groups = null;
	
	
	            for (String id : userGroupIDS)
	            {
	                groups = dao.findById(id);
	
	                if (groups != null)
	                {
	
	                    // delete GroupRoleRel
	                    List<GroupRoleRel> grRelList = (List<GroupRoleRel>) groups.getGroupRoleRels();
	
	                    if (grRelList != null && !grRelList.isEmpty())
	                    {
	                        for (GroupRoleRel grRel : grRelList)
	                        {
	                            grRelDAO.delete(grRel);
	                        }
	                    }
	
	                    // delete UserGroupRel
	                    List<UserGroupRel> ugRelList = (List<UserGroupRel>) groups.getUserGroupRels();
	
	                    for (UserGroupRel ugRel : ugRelList)
	                    {
	                        //no need to call the delete api as its a complete group delete. So will be deleting the team anyway if the flag is true
	//                        deleteUserGroupRel(ugRel);
	                        if (ugRel.getUser() != null && StringUtils.isNotBlank(ugRel.getUser().getUUserName()))
	                        {                                        
	                            deletedGroupFromUserNames.add(ugRel.getUser().getUUserName());
	                        }
	                        else
	                        {
	                            Log.log.warn("User Group Relation with sys_id " + ugRel.getSys_id() + " for group " + groups.getUName() + " is missing related user.");
	                        }
	                        
	                        HibernateUtil.getDAOFactory().getUserGroupRelDAO().delete(ugRel);
	                    }
	
	                    dao.delete(groups);
	                }
	            }

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return deletedGroupFromUserNames;

    } // deleteUserGroupByIDS

    public static Collection<String> deleteGroupsByIdsAndQuery(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        List<String> ids = new ArrayList<String>();

        //TODO: prepare the list of sys_ids to delete based on qry
        if (sysIds == null || sysIds.length == 0)
        {
            //prepare the sysIds with the where clause of this obj
            // String selectQry = query.getSelectHQL();
            // String sql = "select sys_id " + selectQry.substring(selectQry.indexOf("from"));
            //System.out.println("UserUtils: #1930: Delete qry :" + sql);
        }
        else
        {
            ids.addAll(Arrays.asList(sysIds));
        }

        //delete one by one
        return deleteUserGroupByIDS(ids, username);
    }

    public static Collection<String> deleteUsersByIDS(List<String> userSysIds, String username) throws Exception
    {
        Collection<String> deletedUserNames = new ArrayList<String>();
        
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        

        if (userSysIds != null && userSysIds.size() > 0)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {

	                UsersDAO userDao = HibernateUtil.getDAOFactory().getUsersDAO();
	                UserRoleRelDAO urRelDAO = HibernateUtil.getDAOFactory().getUserRoleRelDAO();
	                Users user = null;
	
	                for (String sysId : userSysIds)
	                {
	                    user = userDao.findById(sysId);
	
	                    if (user != null)
	                    {
	                        //delete group rel
	                        Collection<UserGroupRel> userGroupRel = user.getUserGroupRels();
	                        if (userGroupRel != null)
	                        {
	                            for (UserGroupRel rel : userGroupRel)
	                            {
	                                deleteUserGroupRel(rel, username);
	                            }
	                        }
	
	                        //delete role rel
	                        Collection<UserRoleRel> userRoleRel = user.getUserRoleRels();
	                        if (userRoleRel != null)
	                        {
	                            for (UserRoleRel rel : userRoleRel)
	                            {
	                                urRelDAO.delete(rel);
	                            }
	                        }
	
	                        //delete preferences
	                        Collection<UserPreferences> socialPreferences = user.getSocialPreferences();
	                        if (socialPreferences != null)
	                        {
	                            for (UserPreferences pref : socialPreferences)
	                            {
	                                HibernateUtil.getDAOFactory().getSocialPreferencesDAO().delete(pref);
	                            }
	                        }
	                        
	                        ResolveSessionUtil.deleteResolveSessions(user.getUUserName());
	                        
	                        //delete the users graph node also
	                        deleteUserInGraphDB(userSysIds, username);
	                        
	                        deletedUserNames.add(user.getUUserName());
	                        
	                        //delete the user
	                        userDao.delete(user);
	                    }
	                }

            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                throw new Exception(t);
            }
        }
        
        return deletedUserNames;
    } // deleteUserGroupByIDS

    //TODO: decide if we want to mark Users as Deleted or purge it directly
    public static Collection<String> deleteUsersByIdsAndQuery(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);
        
        List<String> ids = new ArrayList<String>();

        //TODO: prepare the list of sys_ids to delete based on qry
        if (sysIds == null || sysIds.length == 0)
        {
            //prepare the sysIds with the where clause of this obj
            String selectQry = query.getSelectHQL();
            String sql = "select sys_id " + selectQry.substring(selectQry.indexOf("from"));
            System.out.println("UserUtils: #1900: Delete qry :" + sql);
        }
        else
        {
            ids.addAll(Arrays.asList(sysIds));
        }

        //delete one by one
        return deleteUsersByIDS(ids, username);
    }

    public static boolean copyTemplateUser(String username, String domain) throws Exception
    {
        Users checkUser = null;
        
        if (StringUtils.isNotBlank(username))
        {
            try
            {
                checkUser = findUser(null, username);
                if(checkUser == null)
                {
                	// V2 license max # of users enforcement
                    v2LicenseUserEnforcement(null);
                    
                    Log.auth.trace("Cloning user with template as user is not there in the DB:" + username);

                    Users templateUser = findUserWithAllReferencesNoCache(null, "user");
                    if(templateUser != null)
                    {
                        Collection<UserPreferences> templateSocialPreferences = templateUser.getSocialPreferences();
                        List<UserPreferencesVO> prefVOs = new ArrayList<UserPreferencesVO>();
                        if(templateSocialPreferences != null)
                        {
                            for(UserPreferences pref : templateSocialPreferences)
                            {
                                UserPreferencesVO vo = pref.doGetVO();
                                vo.setSys_id(null);
                                vo.setId(null);
                                vo.setSysCreatedOn(null);
                                vo.setSysCreatedBy("system");
                                vo.setSysUpdatedBy("system");
                                vo.setSysUpdatedOn(null);

                                prefVOs.add(vo);
                            }
                        }

                        try
                        {
                          HibernateProxy.setCurrentUser("admin");
                        	HibernateProxy.execute(() -> {
	                         // remove domain from username if defined
	                            String firstName = username;
	                            int pos = username.indexOf('\\');
	                            if (pos > 0)
	                            {
	                                firstName = username.substring(pos + 1, username.length());
	                            }
	                            else
	                            {
	                                pos = username.indexOf('@');
	                                if (pos > 0)
	                                {
	                                    firstName = username.substring(0, pos);
	                                }
	                            }
	
	                            // create new account
	                            Log.log.warn("Creating new account for user: " + username);
	                            Log.auth.warn("Creating new account for user: " + username);
	                            Users newAcct = new Users();
	                            newAcct.setUUserName(username);
	                            newAcct.setUFirstName(firstName);
	//                            newAcct.setULockedOut(templateUser.getULockedOut());
	
	                            // need to find the organization from the domain - it can be either LDAP or AD, and then add the org to the user
	                            newAcct.setBelongsToOrganization(findOrganizationFromDomain(domain));
	
	                            UsersDAO usersDAO = HibernateUtil.getDAOFactory().getUsersDAO();
	                            usersDAO.insert(newAcct);
	
	                            // get and add roles
	                            UserRoleRelDAO roleRelDAO = HibernateUtil.getDAOFactory().getUserRoleRelDAO();
	                            for (UserRoleRel rel : templateUser.getUserRoleRels())
	                            {
	                                Roles role = rel.getRole();
	
	                                UserRoleRel newRel = new UserRoleRel();
	                                newRel.setUser(newAcct);
	                                newRel.setRole(role);
	                                roleRelDAO.insert(newRel);
	                                Log.auth.trace("Assigning role :" + role.getUName());
	                            }
	
	                            // get and add groups
	                            for (UserGroupRel rel : templateUser.getUserGroupRels())
	                            {
	                                Groups group = rel.getGroup();
	
	                                UserGroupRel newRel = new UserGroupRel();
	                                newRel.setUser(newAcct);
	                                newRel.setGroup(group);
	
	                                saveUserGroupRel(newRel, username);
	                                Log.auth.trace("Assigning group :" + group.getUName());
	
	                            }
	
	                            if (StringUtils.isNotBlank(templateUser.getSysOrg()))
	                            {
	                                newAcct.setSysOrg(templateUser.getSysOrg());
	                            }
	                            
	                            //user preferences
	                            if(prefVOs.size() > 0)
	                            {
	                                for(UserPreferencesVO prefVO : prefVOs)
	                                {
	                                    UserPreferences prefModel = new UserPreferences(prefVO);
	                                    prefModel.setUser(newAcct);
	
	                                    HibernateUtil.getDAOFactory().getSocialPreferencesDAO().persist(prefModel);
	                                }
	                            }

                        	});

                            //create/update user node in the graph DB
                            createUserInGraphDB(username);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage(), e);
                                                      HibernateUtil.rethrowNestedTransaction(e);
                        }
                    }
                    else
                    {
                        Log.log.error("Missing \"user\" template account.");
                        Log.auth.error("Missing \"user\" template account.");
                    }
                }
             }
            catch (Exception e)
            {
            	if (!ERR.E10022.getMessage().equalsIgnoreCase(e.getMessage()) &&
            		!ERR.E10023.getMessage().equalsIgnoreCase(e.getMessage())) {
            		Log.log.warn(e.getMessage(), e);
            	}
                throw e;
            }
        }
        
        return checkUser == null;
    } // copyTemplateUser

    public static WikiUser getUserRolesAndGroups(String userName)
    {
        WikiUser user = null;
        Set<String> userRoles = null;
        Set<String> userGroups = null;

        if (StringUtils.isNotBlank(userName))
        {
            user = new WikiUser(userName);
            try
            {
                userRoles = getUserRoles(userName);
                user.setRoles(userRoles);
            }
            catch (Throwable e)
            {
                userRoles = new HashSet<String>();
            }
            try
            {
                userGroups = getUserGroups(userName);
                user.setGroups(userGroups);
            }
            catch (Throwable e)
            {
                userGroups = new HashSet<String>();
            }
        }

        return user;
    }

    //private apis
    private static void setUserRoles(Collection<Users> users)
    {
        if(users != null)
        {
            for(Users user : users)
            {
                setUserRoles(user);
            }
        }
    }

    private static List<RolesVO> setUserRoles(Users user)
    {
        List<RolesVO> roleVOList = new ArrayList<RolesVO>();
        
        if (user != null)
        {
            Set<String> roles = new HashSet<String>();
            // StringBuffer roles = new StringBuffer();

            if (user.getUUserName().equalsIgnoreCase(ADMIN) || user.getUUserName().equalsIgnoreCase(RESOLVE_MAINT))
            {
                roles.add("admin");
            }

//            else
//            {
                Collection<UserRoleRel> userRoleRels = user.getUserRoleRels();
                if (userRoleRels != null)
                {
                    Iterator<UserRoleRel> it = userRoleRels.iterator();
                    while (it.hasNext())
                    {
                        UserRoleRel rel = it.next();
                        Roles role = rel.getRole();
                        if (role != null)
                        {
                            RolesVO roleVO = role.doGetVO();
                            if (!roleVOList.contains(roleVO))
                            {
                                roleVOList.add(roleVO);
                            }
                            roles.add(role.getUName().replaceAll("&", "_"));
                        }
                    }// end of while
                }// end of if

//                String efQuery = "select rl from Users usr join usr.userGroupRels ugr join ugr.group g  join g.groupRoleRels grr join grr.role rl where usr.id = '" + user.getSys_id() + "'";
//                String efQuery = "select distinct rl from Users usr join usr.userGroupRels ugr join ugr.group g  join g.groupRoleRels grr join grr.role rl where usr.id = '" + user.getSys_id() + "'";

                
//                String efQuery = "select rl from Users usr left join usr.userGroupRels ugr left join ugr.group g left join g.groupRoleRels grr left join grr.role rl where usr.id = '" + user.getSys_id() + "'";
//                String efQuery = "select distinct rl from Users usr left join usr.userGroupRels ugr left join ugr.group g left join g.groupRoleRels grr left join grr.role rl where usr.id = '" + user.getSys_id() + "'" + " and rl is not null";

                
//                String efQuery = "select role from Users usr, UserGroupRel ugr, Groups g, GroupRoleRel grr, Roles role where usr.id = '" + user.getSys_id() + "'" + " and usr.id=ugr.user and ugr.group=g.id and g.id=grr.group and grr.role=role.id ";
                
//              String efQuery = "select distinct role from Users usr, UserGroupRel ugr, Groups g, GroupRoleRel grr, Roles role where usr.id = '" + user.getSys_id() + "'" + " and usr.id=ugr.user and ugr.group=g.id and g.id=grr.group and grr.role=role.id ";
//                try
//                {
//                    List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(efQuery);
//                    Roles roleList = new Roles();
//                    Map<String, Roles> rolesMap = new HashMap<String, Roles>();
//                    if(result != null && result.size() > 0)
//                    {
//                    	for (Object o : result)
//                    	{
//                    		Roles r = (Roles) o;
//                    		if (r==null)
//                    			continue;
//                    		if (rolesMap.get(r.getSys_id())==null)
//                    		{
//                    			rolesMap.put(r.getSys_id(), r);
//                    		}
//                    	}
//                    }
//                    
//                    Log.log.info(rolesMap.values());
//                }
//                catch (Exception e)
//                {
//                    Log.log.error("Error while executing hql:" + efQuery, e);
//                }
//
                
                
                Collection<UserGroupRel> userGroupRels = user.getUserGroupRels();
                if (userGroupRels  != null)
                {
                    Iterator<UserGroupRel> it = userGroupRels.iterator();
                    while (it.hasNext())
                    {
                        UserGroupRel rel = it.next();
                        Groups groups = rel.getGroup();
                        if (groups != null)
                        {
                            Collection<GroupRoleRel> groupRoleRels = groups.getGroupRoleRels();
                            if (groupRoleRels != null)
                            {
                                Iterator<GroupRoleRel> itRoles = groupRoleRels.iterator();
                                while (itRoles.hasNext())
                                {
                                    GroupRoleRel relGR = itRoles.next();
                                    if (relGR != null)
                                    {
                                        Roles role = relGR.getRole();
                                        if (role != null)
                                        {
                                            RolesVO roleVO = role.doGetVO();
                                            if (!roleVOList.contains(roleVO))
                                            {
                                                roleVOList.add(roleVO);
                                            }
                                            roles.add(role.getUName().replaceAll("&", "_"));
                                        }
                                    }
                                }
                            }
                        }// end of group
                    }// end of while loop
                }// end of userGroupRels
//            }

            user.setRoles(StringUtils.setToString(roles, ","));
        }// end of if for roles
        
        return roleVOList;
    }// setUserRoles

    public static Groups findGroupBy(String groupSysId, String groupName)
    {
        Groups group = null;

        try
        {
			group = (Groups) HibernateProxy.execute(() -> {

				if (StringUtils.isNotEmpty(groupSysId)) {
					return HibernateUtil.getDAOFactory().getGroupsDAO().findById(groupSysId);
				} else if (StringUtils.isNotBlank(groupName)) {
					Groups searchedGroup = new Groups();
					searchedGroup.setUName(groupName);

					return HibernateUtil.getDAOFactory().getGroupsDAO().findFirst(searchedGroup);
				} else {
					return null;
				}

			});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return group;
    }
    
    public static Roles findRoleBy(String roleSysId, String roleName)
    {
        Roles role = null;

        try
        {
        	role = (Roles) HibernateProxy.execute(() -> {

	            if(StringUtils.isNotEmpty(roleSysId))
	            {
	                return HibernateUtil.getDAOFactory().getRolesDAO().findById(roleSysId);
	            }
	            else if(StringUtils.isNotBlank(roleName))
	            {
	            	Roles searchedRole = new Roles();
	                searchedRole.setUName(roleName);
	
	                return HibernateUtil.getDAOFactory().getRolesDAO().findFirst(searchedRole);
	            } else {
	            	return null;
	            }

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return role;
    }

    private static Users getUserPrivate(String sysId, String username)
    {
        Users userResult = null;
        if (StringUtils.isNotBlank(username))
        {
            boolean isResolveMaint = false;
            if (username.equalsIgnoreCase(RESOLVE_MAINT))
            {
//                username = ADMIN;
                isResolveMaint = true;
            }
            
//            boolean isSystem = false;
//            
//            if (!isResolveMaint)
//            {
//                if (username.equalsIgnoreCase(RESOLVE_SYSTEM))
//                {
//                    isSystem = true;
//                }
//            }
            
            Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, username);
            
            if (obj != null)
            {
                userResult = (Users) obj;
                
                Log.log.debug(String.format(FOUND_CACHED_DB_OBJECT_FOR_USER_LOG, username));
            }
            
            if (userResult == null /* || (!isResolveMaint && !isSystem && userResult != null && 
                                       (userResult.getUserGroupRels() == null || userResult.getUserGroupRels().isEmpty())) */)
            {
                /*
                 * Remove the Users object in DB user cache which is corrupted by Groups.doGetVO() resetting UserGroupRels to null
                 * to avoid cyclic references.
                 * 
                 * Commented out as now Groups.doGetVO() removes the original DB user cached Users object before resetting
                 * UserGroupRels to null and setting Users object into Group and then restores back the original Users 
                 * object back into DB user cache.
                 *
                 * HP TODO
                 *  
                 *  Need to think about locking on Hibernate cache used for DB caching.
                 */
                
//                if (userResult != null)
//                {
//                    Log.log.warn("Found cached DB object for user " + username + " in " + HibernateUtil.USER_BY_NAME_CACHE_REGION + " having no UserGroupRels!!!");
//                    HibernateUtil.clearCachedDBObject(HibernateUtil.USER_BY_NAME_CACHE_REGION, username);
//                    Log.log.trace("Cleared cached DB object for user " + username + " from " + HibernateUtil.USER_BY_NAME_CACHE_REGION);
//                }
                
                try
                {
                    if(isResolveMaint)
                    {
                        //create a object for RESOLVE_MAINT
                        userResult = new Users();
                        userResult.setUUserName(RESOLVE_MAINT);
                    }
                    else
                    {
                        userResult = findUserWithAllReferencesNoCache(sysId, username);
                    }

                    if (userResult != null /* && (isSystem || (userResult.getUserGroupRels() != null && 
                                                            !userResult.getUserGroupRels().isEmpty())) */)
                    {
                        HibernateUtil.cacheDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, 
                        							new String[] { "com.resolve.persistence.model.Users", 
                        										   "com.resolve.persistence.model.UserRoleRel", 
                        										   "com.resolve.persistence.model.Roles", 
                        										   "com.resolve.persistence.model.UserGroupRel", 
                        										   "com.resolve.persistence.model.Groups", 
                        										   "com.resolve.persistence.model.GroupRoleRel", 
                        										   "com.resolve.persistence.model.UserPreferences", 
                        										   "com.resolve.persistence.model.Organization", 
                        										   "com.resolve.persistence.model.Orgs", 
                        										   "com.resolve.persistence.model.OrgGroupRel" }, 
                        							userResult.getUUserName(), userResult);
                        Log.log.debug("Inserted cached DB object for user " + username + " in " + 
                        			  DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue());
                    }
//                    else if (userResult != null)
//                    {                        
//                        try
//                        {
//                            throw new Exception("Queried user " + username + " with all references no cache has no UserGroupRels!!!, Not caching " + 
//                                                username +  " in " + HibernateUtil.USER_BY_NAME_CACHE_REGION + " having no UserGroupRels!!!");
//                        }
//                        catch(Exception e)
//                        {
//                            Log.log.warn(e.getMessage());
//                        }
//                        
//                        userResult = null;               
//                    }
                }
                catch (Throwable e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
        }
        else if (StringUtils.isNotBlank(sysId))
        {
            userResult = findUser(sysId, null);
        }
        return userResult;
    }
    
    private static void cacheUserPermissions(JSONObject permissions, JSONObject newPermissions, String username)
    {
        HibernateUtil.cacheDBObject(DBCacheRegionConstants.HAS_ACCESS_RIGHT_CACHE_REGION, 
                        		    new String[] { "com.resolve.persistence.model.Users", 
                        		    			   "com.resolve.persistence.model.UserRoleRel", 
                        		    			   "com.resolve.persistence.model.Roles", 
                        		    			   "com.resolve.persistence.model.UserGroupRel", 
                        		    			   "com.resolve.persistence.model.Groups", 
                        		    			   "com.resolve.persistence.model.GroupRoleRel",
                        		    			   "com.resolve.persistence.model.Organization", 
                        		    			   "com.resolve.persistence.model.Orgs", 
                                       			   "com.resolve.persistence.model.OrgGroupRel" }, 
                        		    username, 
                        		    permissions);
        
        HibernateUtil.cacheDBObject(DBCacheRegionConstants.NEW_HAS_ACCESS_RIGHT_CACHE_REGION, 
                					new String[] { "com.resolve.persistence.model.Users", 
                								   "com.resolve.persistence.model.UserRoleRel", 
                								   "com.resolve.persistence.model.Roles", 
                								   "com.resolve.persistence.model.UserGroupRel", 
                								   "com.resolve.persistence.model.Groups", 
                								   "com.resolve.persistence.model.GroupRoleRel",
                								   "com.resolve.persistence.model.Organization", 
                								   "com.resolve.persistence.model.Orgs", 
                               					   "com.resolve.persistence.model.OrgGroupRel" }, 
                		username, 
                		newPermissions);
    }

    public static Users findUser(String sysId, String username)
    {
        Users userResult = null;

        try
        {
        	userResult = (Users) HibernateProxy.execute(() -> {

	            if (StringUtils.isNotEmpty(sysId))
	            {
	                return HibernateUtil.getDAOFactory().getUsersDAO().findById(sysId);
	            }
	            else if (StringUtils.isNotBlank(username))
	            {
	              //to make this case-insensitive
	                int pos = username.indexOf('\\');
	                String normalizedUserName = username;
	                if (pos > 0)
	                {
	                    normalizedUserName = username.substring(0, pos) + "\\" + username.substring(pos + 1, username.length());
	                }
	                
	                //String sql = "from Users where UUserName = '" + normalizedUserName.trim().toLowerCase() + "'";
	                String sql = "from Users where UUserName like :UUserName";
	                
	                Map<String, Object> queryParams = new HashMap<String, Object>();
	                
	                queryParams.put("UUserName", normalizedUserName.toLowerCase());
	                
	                try
	                {
	                    List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
	                    if(result != null && result.size() > 0)
	                    {
	                        return (Users) result.get(0);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Error while executing sql:" + sql, e);
	                }
	            }
	            
	            return null;
        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return userResult;
    }

    /**
     * This api is 
     * @param sysId
     * @param username
     * @return
     */
    public static Users findUserWithAllReferencesNoCache(String sysId, String username)
    {
        Users userResult = null;
        
        try
        {
            userResult = (Users) HibernateProxy.executeNoCache(() -> {
            	return findUserWithAllReferences(sysId, username);
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return userResult;
    }
    
    
    public static GroupsVO getGroupNoCache(String sysId, String groupName, String username)
    {
        Groups groupModel = null;
        GroupsVO groupVO = null;

        try
        {            
            groupModel = (Groups) HibernateProxy.executeNoCache(() -> {
            	return getGroupModel(sysId, groupName);
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if (groupModel != null)
        {
            groupVO = convertModelToVOForGroups(groupModel, null, null);
        }
        
        return groupVO;
    }
    

    private static Users findUserWithAllReferences(String sysId, String username)
    {
        Users userResult = null;

        try
        {
        	userResult = (Users) HibernateProxy.execute(() -> {
        		Users searchedUser = null;
        		
	            if (StringUtils.isNotEmpty(sysId))
	            {
	            	searchedUser = HibernateUtil.getDAOFactory().getUsersDAO().findById(sysId);
	            }
	            else if (StringUtils.isNotBlank(username))
	            {
	                int pos = username.indexOf('\\');
	                String normalizedUserName = username;
	                if (pos > 0)
	                {
	                    normalizedUserName = username.substring(0, pos) + "\\" + username.substring(pos + 1, username.length());
	                }
	                
	                //to make this case-insensitive
	                //String sql = "from Users where UUserName = '" + normalizedUserName.trim().toLowerCase() + "'";
	                String sql = "from Users where UUserName = :UUserName";
	                
	                Map<String, Object> queryParams = new HashMap<String, Object>();
	                
	                queryParams.put("UUserName", normalizedUserName.trim().toLowerCase());
	                
	                try
	                {
	                    List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
	                    if(result != null && result.size() > 0)
	                    {
	                    	searchedUser = (Users) result.get(0);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Error while executing hql:" + sql, e);
	                }
	            }
	
	            if(searchedUser != null)
	            {
	                //set the roles
	                setUserRoles(searchedUser);
	
	                Collection<UserRoleRel> userRoleRels = searchedUser.getUserRoleRels();
	                if(userRoleRels != null)
	                {
	                    userRoleRels.size(); //load it
	                }
	
	                Collection<UserGroupRel> userGroupRels = searchedUser.getUserGroupRels();
	                if(userGroupRels != null)
	                {
	                    userGroupRels.size(); //load it
	                }
	
	                Collection<UserPreferences> socialPreferences = searchedUser.getSocialPreferences();
	                if(socialPreferences != null)
	                {
	                    socialPreferences.size(); //load it
	                }
	            }

	            return searchedUser;
        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return userResult;
    }

    private static Roles getRoleModel(String sysId, String roleName)
    {
        Roles roleModel = null;

        if(StringUtils.isNotBlank(sysId))
        {
            try
            {

                roleModel = (Roles) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getRolesDAO().findById(sysId);
                });
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(roleName))
        {
            //to make this case-insensitive
            //String sql = "from Roles where LOWER(UName) = '" + roleName.trim().toLowerCase() + "'";
            String sql = "from Roles where LOWER(UName) = :UName";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", roleName.trim().toLowerCase());
            
            try
            {
                List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(result != null && result.size() > 0)
                {
                    roleModel = (Roles) result.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }
        }

        return roleModel;
    }

    private static Groups getGroupModel(String sysId, String groupName)
    {
    	return getGroupModel(sysId, groupName, false);
    }
    
    @SuppressWarnings("unchecked")
    private static Groups getGroupModel(String sysId, String groupName, boolean noUsers)
    {
        try
        {

        	return (Groups) HibernateProxy.execute(() -> {
        		Groups groupModel = null;
        		
                if(StringUtils.isNotBlank(sysId))
                {
                    groupModel = HibernateUtil.getDAOFactory().getGroupsDAO().findById(sysId);
                }
                else if(StringUtils.isNotBlank(groupName))
                {
                    String sql = "from Groups where LOWER(UName) = '" + groupName.trim().toLowerCase() + "'";
                    List<Groups> list = HibernateUtil.createQuery(sql).list();
                    if(list != null && list.size() > 0)
                    {
                        groupModel = list.get(0);
                    }
                }

                //load the roles
                if(groupModel != null)
                {
                    if(groupModel.getGroupRoleRels() != null)
                        groupModel.getGroupRoleRels().size();

                    if (!noUsers)
                    {
    	                if(groupModel.getUserGroupRels() != null)
    	                    groupModel.getUserGroupRels().size();
                    }
                    
                    if (groupModel.getOrgGroupRels() != null)
                        groupModel.getOrgGroupRels().size();
                }
                
                return groupModel;

        	});


        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
            return null;
        }

        
    }

    public static Groups getGroupModelNoCache(String sysId, String groupName)
    {
    	return getGroupModelNoCache(sysId, groupName, false);
    }
    
    public static Groups getGroupModelNoCache(String sysId, String groupName, boolean noUsers)
    {
        Groups groupModel = null;

        try
        {
            groupModel = (Groups) HibernateProxy.executeNoCache(() -> {
            	return getGroupModel(sysId, groupName, noUsers);
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return groupModel;
    }
    
    //this api is already within a transaction
    private static Organization findOrganizationFromDomain(String domainName)
    {
        Organization result = null;

        if(StringUtils.isNotBlank(domainName))
        {
            domainName = domainName.trim();

            ConfigLDAP ldap = new ConfigLDAP();
            ldap.setUDomain(domainName);

            ldap = HibernateUtil.getDAOFactory().getConfigLDAPDAO().findFirst(ldap);
            if(ldap != null)
            {
                result = ldap.getBelongsToOrganization();
            }
            else
            {
                ConfigActiveDirectory cad = new ConfigActiveDirectory();
                cad.setUDomain(domainName);

                cad = HibernateUtil.getDAOFactory().getConfigActiveDirectoryDAO().findFirst(cad);
                if(cad != null)
                {
                    result = cad.getBelongsToOrganization();
                }
            }
            // HP TODO Need to add RADIUS and other external Authorization Types such as SiteMinder etc.
        }//end of if

        return result;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<UsersVO> getUsers(QueryDTO query, String username)
    {
        List<UsersVO> result = new ArrayList<UsersVO>();
//        UsersVO user = UserUtils.getUser(username);
//        String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

//        String hql = "from Properties ";
//        if(StringUtils.isNotBlank(userOrg))
//        {
//            hql = hql + " where sysOrg is null or sysOrg = '" + userOrg + "' ";
//        }
//        hql = hql + " order by sysOrg, UName";

        int limit = query.getLimit();
        int offset = query.getStart();
        
        //filter out the 'system' user if its not a resolve.maint user
        if(StringUtils.isNotEmpty(username) && !username.equalsIgnoreCase("resolve.maint"))
        {
            query.setWhereClause("UUserName not in ('system')");
        }

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null && data.size() > 0)
            {
                //grab the map of sysId-organizationname
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                Map<String, String> mapOfUsersAndRoles = getMapOfUserAndRoles();
                Map<String, String> mapOfUsersAndGroups = getMapOfUserAndGroups();

                //prepare the data
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Users instance = new Users();

                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        //prepare the VO
                        result.add(convertModelUsersToVO(instance, mapOfOrganization, mapOfUsersAndRoles, mapOfUsersAndGroups, false));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        Users instance = (Users) o;
                        result.add(convertModelUsersToVO(instance, mapOfOrganization, mapOfUsersAndRoles, mapOfUsersAndGroups, false));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    public static List<RolesVO> getRoles(QueryDTO query, String username)
    {
        List<RolesVO> result = new ArrayList<RolesVO>();
//        UsersVO user = UserUtils.getUser(username);
//        String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

//        String hql = "from Properties ";
//        if(StringUtils.isNotBlank(userOrg))
//        {
//            hql = hql + " where sysOrg is null or sysOrg = '" + userOrg + "' ";
//        }
//        hql = hql + " order by sysOrg, UName";

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null && data.size() > 0)
            {
                //grab the map of sysId-organizationname
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                //prepare the data
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Roles instance = new Roles();

                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        //add it to the list after converting to VO
                        result.add(convertModelToVOForRoles(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        Roles instance = (Roles) o;
                        result.add(convertModelToVOForRoles(instance, mapOfOrganization));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    public static boolean isUserOrgEnabled(String username)
    {
        boolean result = true;

        UsersVO vo = getUser(username);
        if (vo != null)
        {
            OrganizationVO orgVO = vo.getBelongsToOrganization();
            if (orgVO != null)
            {
                result = !orgVO.ugetUDisable();
            }
        }

        return result;
    }

    //for all the recs that have EXTERNAL in their description, the flag will be set to true.
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void updateExternalFlags()
    {
        String hql = "from Groups where UDescription like 'EXTERNAL:%'";
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            Query qry = HibernateUtil.createQuery(hql);
	            List<Groups> list = qry.list();
	            if(list != null)
	            {
	                for(Groups group : list)
	                {
	                    String desc = group.getUDescription();
	                    if(desc.startsWith("EXTERNAL:"))
	                    {
	                        desc = desc.substring(desc.indexOf(":")+1);
	
	                        group.setUHasExternalLink(true);
	                        group.setUDescription(desc);
	                        HibernateUtil.getDAOFactory().getGroupsDAO().persist(group);
	                    }
	                }
	            }

            });
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }

    public static boolean isAdminUser(String username)
    {
        boolean isAdmin = false;

        try
        {
            isAdmin = isSuperUser(username);

            if(!isAdmin)
            {
                Set<String> userRoles = getUserRoles(username);
                if (userRoles.contains(ADMIN))
                {
                    isAdmin = true;
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error("error validating if the user is admin or not.", t);
        }

        return isAdmin;
    }
    

    /**
     * This is specifically required for social post migration.
     *
     * @param displayName
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Users findUserByDisplayName(String displayName)
    {
        Users userResult = null;

        try
        {
        	userResult = (Users) HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(displayName))
	            {
	                String[] names = displayName.split(" ");
	                if(names.length == 2)
	                {
	                    String hql = "from Users where LOWER(UFirstName) = '" + names[0].trim() + "' and LOWER(ULastName) = '" + names[1].trim() + "'";
	                    
	                    Query qry = HibernateUtil.createQuery(hql);
	                    List<Users> list = qry.list();
	                    if(list != null && list.size() > 0)
	                    {
	                        return list.get(0);
	                    }
	                }
	                else
	                {
	                    Log.log.debug("Invalid display name for searching the user: " + displayName);
	                }
	            }
	            return null;

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return userResult;
    }

    /*
    public static void main(String[] args) throws Throwable
    {
//        Log.log = Logger.getRootLogger();
//        String logConfigFilename = "C:/project/resolve3/dist/tomcat/webapps/resolve/WEB-INF/log.cfg";
//        Log.init(logConfigFilename, "UserUtils");
//        Log.initAuth();
//        //
//        java.util.Properties props = new java.util.Properties();
//        props.setProperty(HibernateUtil.DB_TYPE, "mysql");
//        props.setProperty(HibernateUtil.DB_HOST, "localhost");
//        props.setProperty(HibernateUtil.DB_NAME, "resolve");
//        props.setProperty(HibernateUtil.DB_USERNAME, "resolve");
//        props.setProperty(HibernateUtil.DB_DROWSSAP_PROP_NAME, "resolve");
//        props.setProperty(HibernateUtil.DB_POOL_SIZE, "8");
//        props.setProperty(HibernateUtil.DB_TIMEOUT, "300");
//
//        HibernateUtil.initTransactionManager(props);
//        HibernateUtil.init("C:/project/resolve3/dist/tomcat/webapps/resolve/WEB-INF/hibernate.cfg.xml");
//        //
//        System.out.println("Connection successfull....");
//
//        String username = "user";
//
//        Set<String> roles = getUserRoles(username);
//        for (String ro : roles)
//        {
//            System.out.println(ro);
//        }

//        System.out.println(" hasRole(username, 'action_execute'): " + hasRole(username, "action_execute"));
//        System.out.println(" hasRole(username, 'admin'): " + hasRole(username, ADMIN));
//        System.out.println(" hasRole(username, 'resolve_user'): " + hasRole(username, "resolve_user"));
//        System.out.println(" hasRole(username, 'agent_admin'): " + hasRole(username, "agent_admin"));
//        System.out.println(" hasRole(username, 'list_updater'): " + hasRole(username, "list_updater"));

    } // main
    */

    //private
    private static RolesVO convertModelToVOForRoles(Roles model, Map<String, String> mapOfOrganization)
    {
        RolesVO vo = null;

        if(model != null)
        {
            if(mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }

            vo = model.doGetVO();
            if(StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }

        }

        return vo;
    }

    
    private static GroupsVO convertModelToVOForGroups(Groups model, Map<String, String> mapOfOrganization, Map<String, String> mapofGroupAndRoles)
    {
        return convertModelToVOForGroups(model, mapOfOrganization, mapofGroupAndRoles, false);
    }
    
    private static GroupsVO convertModelToVOForGroups(Groups model, Map<String, String> mapOfOrganization, Map<String, String> mapofGroupAndRoles, boolean orgsOnly)
    {
        GroupsVO vo = null;

        if(model != null)
        {
            if(mapOfOrganization == null)
            {
                mapOfOrganization = OrgsUtil.getListOfAllOrgNames();
            }

            vo = model.doGetVO(orgsOnly);
            if(StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }

            //set the comma seperated Roles
            if(mapofGroupAndRoles != null)
            {
                vo.setRoles(mapofGroupAndRoles.get(vo.getSys_id()));
            }

        }

        return vo;
    }

    private static Map<String, String> getMapOfUserAndRoles()
    {
        Map<String, String> result = new HashMap<String, String>();


        try
        {
        	HibernateProxy.execute(() -> {

                List<Users> list = HibernateUtil.getDAOFactory().getUsersDAO().findAll();
                if(list != null && list.size() > 0)
                {
                    for(Users user : list)
                    {
                        Collection<UserRoleRel> rolesRel = user.getUserRoleRels();
                        String rolesStr = "";
                        if(rolesRel != null)
                        {
                            StringBuilder roles = new StringBuilder();
                            for(UserRoleRel rel : rolesRel)
                            {
                                Roles role = rel.getRole();
                                if(role != null)
                                {
                                    roles.append(role.getUName()).append(",");
                                }
                            }

                            if(roles.indexOf(",") > -1)
                            {
                                rolesStr = roles.substring(0, roles.lastIndexOf(","));
                            }
                            else
                            {
                                rolesStr = roles.toString();
                            }
                        }

                        result.put(user.getSys_id(), rolesStr);
                    }
                }

        	});

        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }

    private static Map<String, String> getMapOfUserAndGroups()
    {
        Map<String, String> result = new HashMap<String, String>();

        try
        {
        	HibernateProxy.execute(() -> {
                List<Users> list = HibernateUtil.getDAOFactory().getUsersDAO().findAll();
                if(list != null && list.size() > 0)
                {
                    for(Users user : list)
                    {
                        Collection<UserGroupRel> groupsRel = user.getUserGroupRels();
                        String groupStr = "";
                        if(groupsRel != null)
                        {
                            StringBuilder roles = new StringBuilder();
                            for(UserGroupRel rel : groupsRel)
                            {
                                Groups group = rel.getGroup();
                                if(group != null)
                                {
                                    roles.append(group.getUName()).append(",");
                                }
                            }

                            if(roles.indexOf(",") > -1)
                            {
                                groupStr = roles.substring(0, roles.lastIndexOf(","));
                            }
                            else
                            {
                                groupStr = roles.toString();
                            }
                        }

                        result.put(user.getSys_id(), groupStr);
                    }
                }
        	});

        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }


        return result;
    }



    private static Map<String, String> getMapOfGroupAndRoles()
    {
        Map<String, String> result = new HashMap<String, String>();
        try
        {

        	HibernateProxy.execute(() -> {
                List<Groups> list = HibernateUtil.getDAOFactory().getGroupsDAO().findAll();
                if(list != null && list.size() > 0)
                {
                    for(Groups group : list)
                    {
                        Collection<GroupRoleRel> groupRel = group.getGroupRoleRels();
                        String rolesStr = "";
                        if(groupRel != null)
                        {
                            StringBuilder roles = new StringBuilder();
                            for(GroupRoleRel rel : groupRel)
                            {
                                Roles role = rel.getRole();
                                if(role != null)
                                {
                                    roles.append(role.getUName()).append(",");
                                }
                            }

                            if(roles.indexOf(",") > -1)
                            {
                                rolesStr = roles.substring(0, roles.lastIndexOf(","));
                            }
                            else
                            {
                                rolesStr = roles.toString();
                            }
                        }

                        result.put(group.getSys_id(), rolesStr);
                    }
                }                
        	});
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }

    private static Collection<String> addRolesToUsersPrivate(Set<String> userSysIds, Set<String> roleSysIds, String username, boolean deleteTheOldRoleRel)
    {
        Collection<String> userNames = new ArrayList<String>();
        
        if (userSysIds != null && roleSysIds != null)
        {
            for (String userSysId : userSysIds)
            {
                try
                {
                    String userName = addRolesToUser(userSysId, roleSysIds, username, deleteTheOldRoleRel);
                    if (StringUtils.isNotBlank(userName))
                    {
                        userNames.add(userName);
                    }
                }
                catch (Exception t)
                {
                    Log.log.error(t.getMessage(), t);
                }
            }
        }
        
        return userNames;
    }

    public static String addRolesToUser(String userSysId, Set<String> roleSysIds, String username, boolean deleteTheOldRoleRel)
    {
        String userName = null;
        
        if(StringUtils.isNotBlank(userSysId) && roleSysIds != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                username = (String) HibernateProxy.execute(() -> {

	                Users user = HibernateUtil.getDAOFactory().getUsersDAO().findById(userSysId);
	                String searchedUsername = null;
	                if(user != null)
	                {
	                	searchedUsername = user.getUUserName();
	                    Collection<UserRoleRel> userRoleRel = user.getUserRoleRels();
	                    //delete if the flag is true
	                    if(userRoleRel != null && deleteTheOldRoleRel)
	                    {
	                        for(UserRoleRel rel : userRoleRel)
	                        {
	                            HibernateUtil.getDAOFactory().getUserRoleRelDAO().delete(rel);
	                        }
	                    }
	
	
	                    for(String roleSysId : roleSysIds)
	                    {
	                        boolean roleExistForThisUser = false;
	                        //if all the recs are already deleted, no need to check anything
	                        if(!deleteTheOldRoleRel)
	                        {
	                            roleExistForThisUser = doesRoleExistForUser(roleSysId, userRoleRel);
	                        }
	
	                        if(!roleExistForThisUser)
	                        {
	                            Roles role = HibernateUtil.getDAOFactory().getRolesDAO().findById(roleSysId);
	                            if(role != null)
	                            {
	                                UserRoleRel rel = new UserRoleRel();
	                                rel.setUser(user);
	                                rel.setRole(role);
	
	                                HibernateUtil.getDAOFactory().getUserRoleRelDAO().persist(rel);
	                            }
	                        }
	                    }//end of for
	                }//end of if
	                
	                return searchedUsername;
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        
        return userName;
    }

    private static boolean  doesRoleExistForUser(String roleSysId, Collection<UserRoleRel> userRoleRel)
    {
        boolean exist = false;

        if(StringUtils.isNotBlank(roleSysId) && userRoleRel != null)
        {
            for(UserRoleRel rel : userRoleRel)
            {
                Roles role = rel.getRole();
                if(role != null)
                {
                    if(role.getSys_id().equalsIgnoreCase(roleSysId))
                    {
                        exist = true;
                        break;
                    }
                }
            }
        }

        return exist;
    }

    private static Collection<String> addGroupsToUsersPrivate(Set<String> userSysIds, Set<String> groupSysIds, String username, boolean deleteTheOldGroupRel)
    {
        Collection<String> userNames = new ArrayList<String>();
        
        if (userSysIds != null && groupSysIds != null)
        {
            for (String userSysId : userSysIds)
            {
                try
                {
                    String userName = addGroupToUser(userSysId, groupSysIds, username, deleteTheOldGroupRel);
                    
                    if (StringUtils.isNotBlank(userName))
                    {
                        userNames.add(userName);
                    }
                }
                catch (Exception t)
                {
                    Log.log.error(t.getMessage(), t);
                }
            }
        }
        
        return userNames;
    }

    public static String addGroupToUser(String userSysId, Set<String> groupSysIds, String username, boolean deleteTheOldGroupRel)
    {
        String userName = null;
        
        if(StringUtils.isNotBlank(userSysId) && groupSysIds != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
              userName = (String) HibernateProxy.execute(() -> {

	                Users user = HibernateUtil.getDAOFactory().getUsersDAO().findById(userSysId);
	                String foundUsername = null;
	                if(user != null)
	                {
	                	foundUsername = user.getUUserName();
	                    
	                    Collection<UserGroupRel> userGroupRels = user.getUserGroupRels();
	
	                    //delete if the flag is true
	                    if(userGroupRels != null && deleteTheOldGroupRel)
	                    {
	                        for(UserGroupRel rel : userGroupRels)
	                        {
	                            deleteUserGroupRel(rel, foundUsername);
	                        }
	                    }
	
	
	                    for(String groupSysId : groupSysIds)
	                    {
	                        boolean roleExistForThisUser = false;
	                        //if all the recs are already deleted, no need to check anything
	                        if(!deleteTheOldGroupRel)
	                        {
	                            roleExistForThisUser = doesGroupExistForUser(groupSysId, userGroupRels);
	                        }
	
	                        if(!roleExistForThisUser)
	                        {
	                            Groups group = HibernateUtil.getDAOFactory().getGroupsDAO().findById(groupSysId);
	                            if(group != null)
	                            {
	                                UserGroupRel rel = new UserGroupRel();
	                                rel.setUser(user);
	                                rel.setGroup(group);
	
	                                saveUserGroupRel(rel, foundUsername);
	                            }
	                        }
	                    }//end of for
	                }//end of if
	                
	                return foundUsername;
            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        
        return userName;
    }

    private static boolean  doesGroupExistForUser(String groupSysId, Collection<UserGroupRel>  userGroupRels)
    {
        boolean exist = false;

        if(StringUtils.isNotBlank(groupSysId) && userGroupRels != null)
        {
            for(UserGroupRel rel : userGroupRels)
            {
                Groups group = rel.getGroup();
                if(group != null)
                {
                    if(group.getSys_id().equalsIgnoreCase(groupSysId))
                    {
                        exist = true;
                        break;
                    }
                }
            }
        }

        return exist;
    }

//    private static void addRolesToGroupsPrivate(Set<String> groupsSysIds, Set<String> roleSysIds, String username, boolean deleteTheOldRolesRel)
//    {
//
//        if (groupsSysIds != null && roleSysIds != null)
//        {
//            for (String groupSysId : groupsSysIds)
//            {
//                try
//                {
//                    addRolesToGroup(groupSysId, roleSysIds, username, deleteTheOldRolesRel);
//                }
//                catch (Exception t)
//                {
//                    Log.log.error(t.getMessage(), t);
//                }
//            }
//        }
//
//    }

    private static void addRolesToGroup(String  groupSysId, Set<String> roleSysIds, String username, boolean deleteTheOldRolesRel)
    {
        if(StringUtils.isNotBlank(groupSysId) && roleSysIds != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                Groups group = HibernateUtil.getDAOFactory().getGroupsDAO().findById(groupSysId);
	                if(group != null)
	                {
	                    Collection<GroupRoleRel> groupRoleRels = group.getGroupRoleRels();
	
	                    //delete if the flag is true
	                    if(groupRoleRels != null && deleteTheOldRolesRel)
	                    {
	                        for(GroupRoleRel rel : groupRoleRels)
	                        {
	                            HibernateUtil.getDAOFactory().getGroupRoleRelDAO().delete(rel);
	                        }
	                    }
	
	                    //start adding the roles to this group one by one
	                    for(String roleSysId : roleSysIds)
	                    {
	                        boolean roleExistForThisGroup = false;
	                        //if all the recs are already deleted, no need to check anything
	                        if(!deleteTheOldRolesRel)
	                        {
	                            roleExistForThisGroup = doesRoleExistForGroup(roleSysId, groupRoleRels);
	                        }
	
	                        //if the role does not exist, than add it
	                        if(!roleExistForThisGroup)
	                        {
	                            Roles role = HibernateUtil.getDAOFactory().getRolesDAO().findById(roleSysId);
	                            if(role != null)
	                            {
	                                GroupRoleRel rel = new GroupRoleRel();
	                                rel.setGroup(group);
	                                rel.setRole(role);
	
	                                HibernateUtil.getDAOFactory().getGroupRoleRelDAO().persist(rel);
	//                                System.out.println("GroupRoleRel :" + rel.getSys_id());
	                            }
	                        }
	                    }//end of for
	                }//end of if

                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

    private static boolean  doesRoleExistForGroup(String roleSysId, Collection<GroupRoleRel> groupRoleRels)
    {
        boolean exist = false;

        if(StringUtils.isNotBlank(roleSysId) && groupRoleRels != null)
        {
            for(GroupRoleRel rel : groupRoleRels)
            {
                Roles role = rel.getRole();
                if(role != null)
                {
                    if(role.getSys_id().equalsIgnoreCase(roleSysId))
                    {
                        exist = true;
                        break;
                    }
                }
            }
        }

        return exist;
    }

    private static void addUsersToGroup(String  groupSysId, Set<String> userSysIds, String username, boolean deleteTheOldUsersRel)
    {
        if(StringUtils.isNotBlank(groupSysId) && userSysIds != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                Groups group = HibernateUtil.getDAOFactory().getGroupsDAO().findById(groupSysId);
	                if(group != null)
	                {
	                    Collection<UserGroupRel> userGroupRels = group.getUserGroupRels();
	
	                    //delete if the flag is true
	                    if(userGroupRels != null && deleteTheOldUsersRel)
	                    {
	                        for(UserGroupRel rel : userGroupRels)
	                        {
	                            //if this user is not 'resolve.maint' user, he cannot remove the 'system' users from any roles
	                            if(!username.equalsIgnoreCase("resolve.maint"))
	                            {
	                                String systemUsername = rel.getUser() != null ? rel.getUser().getUUserName() : "admin";
	                                if(!systemUsername.equalsIgnoreCase("system"))
	                                {
	                                    deleteUserGroupRel(rel, username);
	                                }
	                            }
	                            else
	                            {
	                                deleteUserGroupRel(rel, username);
	                            }
	                        }
	                    }
	
	                    //start adding the roles to this group one by one
	                    for(String userSysId : userSysIds)
	                    {
	                        boolean userExistForThisGroup = false;
	                        //if all the recs are already deleted, no need to check anything
	                        if(!deleteTheOldUsersRel)
	                        {
	                            userExistForThisGroup = doesUserExistForGroup(userSysId, userGroupRels);
	                        }
	
	                        //if the role does not exist, than add it
	                        if(!userExistForThisGroup)
	                        {
	                            Users user = HibernateUtil.getDAOFactory().getUsersDAO().findById(userSysId);
	                            if(user != null)
	                            {
	                                UserGroupRel rel = new UserGroupRel();
	                                rel.setGroup(group);
	                                rel.setUser(user);
	
	                                saveUserGroupRel(rel, username);
	                            }
	                        }
	                    }//end of for
	                }//end of if

                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

    private static boolean  doesUserExistForGroup(String userSysId, Collection<UserGroupRel> userGroupRels)
    {
        boolean exist = false;

        if(StringUtils.isNotBlank(userSysId) && userGroupRels != null)
        {
            for(UserGroupRel rel : userGroupRels)
            {
                Users user = rel.getUser();
                if(user != null)
                {
                    if(user.getSys_id().equalsIgnoreCase(userSysId))
                    {
                        exist = true;
                        break;
                    }
                }
            }
        }

        return exist;
    }

    //NOTE - call this api within a transaction only
    private static void saveUserGroupRel(UserGroupRel userGroupRel, String username)
    {
        HibernateUtil.getDAOFactory().getUserGroupRelDAO().persist(userGroupRel);

        //if the flag of the group is set to true, add the user to the team with the same name
//        Groups group = userGroupRel.getGroup();
//        if(group.ugetUIsLinkToTeam())
//        {
//            Users user = userGroupRel.getUser();
//            addUserToTeam(user, group, username);
//        }

    }

    //NOTE - call this api within a transaction only
    private static void deleteUserGroupRel(UserGroupRel userGroupRel, String username)
    {
        //if the flag of the group is set to true, remove the user from the same name team
//        Groups group = userGroupRel.getGroup();
//        if(group.getUIsLinkToTeam())
//        {
//            Users user = userGroupRel.getUser();
//            removeUserFromTeam(user, group, username);
//        }

        HibernateUtil.getDAOFactory().getUserGroupRelDAO().delete(userGroupRel);
    }

    private static UsersVO convertModelUsersToVO(Users model, Map<String, String> mapOfOrganization, Map<String, String> mapOfUsersAndRoles, Map<String, String> mapOfUsersAndGroups, boolean relTablesAlso)
    {
    	return convertModelUsersToVO(model, mapOfOrganization, mapOfUsersAndRoles, mapOfUsersAndGroups, relTablesAlso, false);
    }

    private static UsersVO convertModelUsersToVO(Users model, Map<String, String> mapOfOrganization, Map<String, String> mapOfUsersAndRoles, Map<String, String> mapOfUsersAndGroups, boolean relTablesAlso, boolean noReferences)
    {
        return convertModelUsersToVO(model, mapOfOrganization, mapOfUsersAndRoles, mapOfUsersAndGroups, relTablesAlso, noReferences, false);
    }

    private static UsersVO convertModelUsersToVO(Users model, Map<String, String> mapOfOrganization, Map<String, String> mapOfUsersAndRoles, Map<String, String> mapOfUsersAndGroups, boolean relTablesAlso, boolean noReferences, boolean rolesOnly)
    {
    	return convertModelUsersToVO(model, mapOfOrganization, mapOfUsersAndRoles, mapOfUsersAndGroups, relTablesAlso, noReferences, rolesOnly, false);
    }
    
    private static UsersVO convertModelUsersToVO(Users model, Map<String, String> mapOfOrganization, Map<String, String> mapOfUsersAndRoles, Map<String, String> mapOfUsersAndGroups, boolean relTablesAlso, boolean noReferences, boolean rolesOnly, boolean orgsOnly)
    {
        UsersVO vo = null;

        if(model != null)
        {
            if (mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }

            if (noReferences)
            {
            	vo = model.doGetVO(false);
            }
            else
            {
            	if (rolesOnly)
            	{
            		vo = model.doGetVO(true, true);
            	}
            	else
            	{
            		if (orgsOnly)
            		{
            			vo = model.doGetVO(true, false, true);
            		}
            		else
            		{
            			vo = model.doGetVO();
            		}
            	}
            }
            
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }


            //for one user rec
            if (relTablesAlso)
            {
                //for edit of a User rec, use the list of roles and groups
                vo.setRoles(null);
                vo.setGroups(null);
                
                Set<String> roleNames = new HashSet<String>();
                Set<String> groupNames = new HashSet<String>();
                
                //groups
                List<GroupsVO> userGroups = new ArrayList<GroupsVO>();
                Collection<UserGroupRel> userGroupRel = model.getUserGroupRels();
                if (userGroupRel != null)
                {
                    for (UserGroupRel rel : userGroupRel)
                    {
                        Groups group = rel.getGroup();
                        if (group != null)
                        {
                            userGroups.add(group.doGetVO());
                            
                            groupNames.add(group.getUName());
                            
                            Collection<GroupRoleRel> grpRoleRels = group.getGroupRoleRels();
                            
                            if (grpRoleRels != null && !grpRoleRels.isEmpty())
                            {
                                for (GroupRoleRel grpRoleRel : grpRoleRels)
                                {
                                    Roles role = grpRoleRel.getRole();
                                    
                                    if (role != null && StringUtils.isNotBlank(role.getUName()))
                                    {
                                        roleNames.add(role.getUName().replaceAll("&", "_"));
                                    }
                                }
                            }
                        }
                    }
                    
                    vo.setUserGroups(userGroups);
                    
                    if (!groupNames.isEmpty())
                    {
                        StringBuilder groupsCSStrBldr = new StringBuilder();
                        
                        for (String grpName : groupNames)
                        {
                            if (groupsCSStrBldr.length() > 0)
                            {
                                groupsCSStrBldr.append(",");
                            }
                            
                            groupsCSStrBldr.append(grpName);
                        }
                        
                        vo.setGroups(groupsCSStrBldr.toString());
                    }
                }
                
                //roles
                List<RolesVO> userRoles = new ArrayList<RolesVO>();
                Collection<UserRoleRel> userRoleRels = model.getUserRoleRels();
                if (userRoleRels != null)
                {
                    for (UserRoleRel rel : userRoleRels)
                    {
                        Roles role = rel.getRole();
                        if (role != null)
                        {
                            userRoles.add(role.doGetVO());
                            roleNames.add(role.getUName().replaceAll("&", "_"));
                        }
                    }

                    vo.setUserRoles(userRoles);    
                }
                
                if (!roleNames.isEmpty())
                {
                    StringBuilder rolesCSStrBldr = new StringBuilder();
                    
                    for (String roleName : roleNames)
                    {
                        if (rolesCSStrBldr.length() > 0)
                        {
                            rolesCSStrBldr.append(",");
                        }
                        
                        rolesCSStrBldr.append(roleName);
                    }
                    
                    vo.setRoles(rolesCSStrBldr.toString());
                } 
            }
            else
            {
                //set the comma seperated Roles
                if(mapOfUsersAndRoles != null)
                {
                    vo.setRoles(mapOfUsersAndRoles.get(vo.getSys_id()));
                }

                //set the comma seperated Groups
                if(mapOfUsersAndGroups != null)
                {
                    vo.setGroups(mapOfUsersAndGroups.get(vo.getSys_id()));
                }

            }

            //set the dummy pwd for all the users
            // Uploading profile pic locks out user (http://10.20.2.111/browse/RBA-13209)
            // vo.setUUserP_assword(DUMMY_USER_P_ASSWORD);
            vo.setUUserP_assword(VO.STRING_DEFAULT);
            vo.setUPasswordHistory(null);
        }

        return vo;
    }

//    private static void deleteTeam(String teamname, String username)
//    {
//        SocialAdminUtil.deleteTeam(null, teamname, username, false);
//    }



//    private static void removeUserFromTeam(Users user, Groups group, String username)
//    {
//        User socialUser = new User();
//        socialUser.setSys_id(user.getSys_id());
//
//        try
//        {
//            SocialTeamDTO team = SocialAdminUtil.getTeamByName(group.getUName(), username);
//            if(team != null)
//            {
//                Team socialTeam = new Team(team.getSys_id(), team.getU_display_name());
//
//                //remove the user from team
//                ServiceGraph.removeUserFromTeam(socialUser, socialTeam);
//            }
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error removing the user:" + user.getUUserName() + " from group :" + group.getUName(), e);
//        }
//
//    }
//
//    private static void addUserToTeam(Users user, Groups group, String username)
//    {
//        User socialUser = new User();
//        socialUser.setSys_id(user.getSys_id());
//
//        try
//        {
//            SocialTeamDTO team = SocialAdminUtil.getTeamByName(group.getUName(), username);
//            if(team != null)
//            {
//                Team socialTeam = new Team(team.getSys_id(), team.getU_display_name());
//
//                //add the user to team
//                ServiceGraph.addUserToTeam(socialUser, socialTeam);
//            }
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error adding the user:" + user.getUUserName() + " to team :" + group.getUName(), e);
//        }
//    }

//    ***************************************
    private static void syncGroupsWithTeam(List<String> groupList, String username)
    {
        if(groupList != null && groupList.size() > 0)
        {
            Set<String> groups = new HashSet<String>(groupList);
            for(String groupSysId : groups)
            {
                Groups group = getGroupModel(groupSysId, null);
                if(group != null)
                {
                    syncGroupWithTeam(group, username);
                }
            }

        }//end of if
    }
    /**
     * push all the users of this Group to Team with the same name
     *
     * @param model
     */
    private static void syncGroupWithTeam(Groups model, String username)
    {
        if(model.ugetUIsLinkToTeam())
        {
            try
            {
                GroupsVO groupVO = getGroup(model.getSys_id(), null, username);

                //create the team first
                createTeam(groupVO, username);

                //get the team rec
                SocialTeamDTO team = SocialAdminUtil.getTeamByName(groupVO.getUName(), username);

                if(team != null)
                {
                    Team socialTeam = SocialCompConversionUtil.convertSocialTeamDTOToTeam(team, null);

                    if(groupVO != null)
                    {
                        List<UsersVO> groupUsers = getUsersForGroup(model.getSys_id(), model.getUName());

                        //remove all the users from this team
                        Set<User> usersOfTeam = ServiceSocial.findUsersBelongingToTeamAndItsChildTeams(team.getSys_id(), true);
                        if(usersOfTeam != null)
                        {
                            for(User u : usersOfTeam)
                            {
                                //remove the user from team
                                ResolveNodeVO parentTeamNode = ServiceGraph.findNode(null, socialTeam.getId(), null, NodeType.TEAM, username);
                                SocialAdminUtil.unfollowCompByUser (parentTeamNode, u.getId(), username);
                                //ServiceSocial.removeUserFromTeam(u, socialTeam);
                            }
                        }//end of if

                        //add the user to the team who is creating/updating the group
                        UsersVO createByUser = getUser(username);
                        ResolveNodeVO parentTeamNode = ServiceGraph.findNode(null, socialTeam.getId(), null, NodeType.TEAM, username);
                        if(createByUser != null)
                        {
                            groupUsers.add(createByUser);
                            //User socialUser = SocialCompConversionUtil.convertUserToSocialUser(createByUser, null);

//                            NotificationHelper.getUserNotification(socialUser, socialTeam, UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM, username, true, true);
//                            NotificationHelper.getTeamNotificationUserLeftTeam(socialTeam, socialUser, UserGlobalNotificationContainerType.TEAM_USER_ADDED, username, true, true);
                            
                            //ServiceSocial.addUserToTeam(socialUser, socialTeam);
                        }

                        //add each user to this team
                        for(UsersVO user : groupUsers)
                        {
                            SocialAdminUtil.followCompByUser(parentTeamNode, user.getSys_id(), username);
                            //User socialUser = SocialCompConversionUtil.convertUserToSocialUser(user, null);

//                            NotificationHelper.getUserNotification(socialUser, socialTeam, UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM, username, true, true);
//                            NotificationHelper.getTeamNotificationUserLeftTeam(socialTeam, socialUser, UserGlobalNotificationContainerType.TEAM_USER_ADDED, username, true, true);
                            
                            //ServiceSocial.addUserToTeam(socialUser, socialTeam);
                        }//end of for loop


                    }//end of if
                }//end of if
            }
            catch (Exception e)
            {
                Log.log.error("error in creating and assigning users to team :" + model.getUName(), e);
            }
        }//end of if
    }



    private static void createTeam(GroupsVO groupVO, String username)
    {
//        GroupsVO groupVO = getGroup(group.getSys_id(), null, username);
        if (groupVO != null)
        {
            List<RolesVO> groupRoles = groupVO.getGroupRoles();
            //Roles
            Set<String> newRoles = new HashSet<String>();
            newRoles.add("admin");
            newRoles.add("social_admin");
            for (RolesVO role : groupRoles)
            {
                newRoles.add(role.getUName());
            }
            //team
            SocialTeamDTO team = null;
            try
            {
                try
                {
                    team = SocialAdminUtil.getTeamByName(groupVO.getUName(), username);
                }
                catch (Exception e1)
                {
                    Log.log.error("Team " + groupVO.getUName() + " does not exist. So will create now.");
                }
                if (team == null)
                {
                    team = new SocialTeamDTO();
                    team.setU_display_name(groupVO.getUName());
                    team.setU_description(groupVO.getUDescription());
                    team.setU_owner(username);
                }

                team.setU_read_roles(getCSVRoles(team.getU_read_roles(), newRoles));
                team.setU_post_roles(getCSVRoles(team.getU_post_roles(), newRoles));
                try
                {
                    //save/create the team
                    team = SocialAdminUtil.saveTeam(team, username, false);
                }
                catch (Exception e)
                {
                    Log.log.error("Error in creating team " + groupVO.getUName() + " from the group", e);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }

        }
    }

    private static String getCSVRoles(String csvRoles, Set<String> newRoles)
    {
        String result = csvRoles;

        if(newRoles != null && newRoles.size() > 0)
        {
            if(StringUtils.isNotBlank(csvRoles))
            {
                Set<String> prevRoles = new HashSet<String>(StringUtils.convertStringToList(csvRoles, ","));
                newRoles.addAll(prevRoles);
            }

            result = StringUtils.convertCollectionToString(newRoles.iterator(), ",");
        }

        return result;
    }


    private static void updateUserPreferences(Users dbUser,  List<UserPreferencesVO> socialPreferencesVO, String username)
    {
        if (socialPreferencesVO != null && socialPreferencesVO.size() > 0)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
                	Users db_User = HibernateUtil.getDAOFactory().getUsersDAO().findById(dbUser.getSys_id());
	                if (db_User != null)
	                {
	                    for (UserPreferencesVO pref : socialPreferencesVO)
	                    {
	                        UserPreferences model = null;
	                        if (StringUtils.isNotBlank(pref.getSys_id()))
	                        {
	                            model = HibernateUtil.getDAOFactory().getSocialPreferencesDAO().findById(pref.getSys_id());
	                        }
	                        else
	                        {
	                            model = new UserPreferences();
	                        }
	
	                        model.applyVOToModel(pref);
	                        model.setUser(db_User);
	
	                        HibernateUtil.getDAOFactory().getSocialPreferencesDAO().persist(model);
	
	                    }
	
	                }
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

    }

    private static void createUserInGraphDB(String username)
    {
        try
        {
            int pos = username.indexOf('\\');
            String normalizedUserName = username;
            if (pos > 0)
            {
                normalizedUserName = username.substring(0, pos) + "\\" + username.substring(pos + 1, username.length());
            }
            
            UsersVO userVO = UserUtils.getUserNoCache(normalizedUserName);
            ResolveNodeVO userNode = ServiceGraph.findNode(null, null, normalizedUserName, NodeType.USER, username);
            if(userNode == null)
            {
                userNode = new ResolveNodeVO();
                userNode.setUCompName(userVO.getUUserName());
                userNode.setUCompSysId(userVO.getSys_id());
                userNode.setUType(NodeType.USER);
                userNode.setUMarkDeleted(false);
                userNode.setUPinned(false);
                
            }
            
            userNode.setULock(userVO.getULockedOut());
            
            //roles
            String userRoles = StringUtils.isNotEmpty(userVO.getRoles()) ? SocialUtil.convertRolesToSocialRolesString(userVO.getRoles()) : "";
            userNode.setUReadRoles(userRoles);            
            userNode.setUEditRoles(userRoles);            
            userNode.setUPostRoles(userRoles);            
            
            //Set/Update user node properties
            Set<ResolveNodePropertiesVO> props = userNode.getProperties();
            
            if(props == null)
            {
                props = new HashSet<ResolveNodePropertiesVO>();
                userNode.setProperties(props);
            }
            
            ResolveNodePropertiesVO dispNameProp = null;
            
            for(ResolveNodePropertiesVO prop : props)
            {
                if(StringUtils.isNotBlank(prop.getUKey()) && prop.getUKey().equalsIgnoreCase(ResolveGraphNode.DISPLAYNAME))
                {
                    dispNameProp = prop;
                    break;
                }
            }
            
            if(dispNameProp == null)
            {
                dispNameProp = new ResolveNodePropertiesVO();
                dispNameProp.setUKey(ResolveGraphNode.DISPLAYNAME);
            }
            
            String displayName = StringUtils.isNotBlank(userVO.getUFirstName()) ? userVO.getUFirstName() : "";

            displayName += (StringUtils.isNotBlank(displayName) ? " " : "");
            
            displayName += (StringUtils.isNotBlank(userVO.getULastName()) ? userVO.getULastName() : "");
            
            displayName.trim();
            
            if(StringUtils.isBlank(displayName))
            {
                displayName = userVO.getUUserName();
            }
            
            dispNameProp.setUValue(displayName);
            
            if(dispNameProp.getSysUpdatedOn() != null && !dispNameProp.getSysUpdatedOn().equals(VO.DATE_DEFAULT))
            {
                dispNameProp.setSysUpdatedOn(GMTDate.getDate());
                dispNameProp.setSysCreatedOn(GMTDate.getDate(dispNameProp.getSysCreatedOn()));
            }
            
            props.add(dispNameProp);
            
            ServiceGraph.persistNode(userNode, "admin");
            
        }
        catch (Exception e)
        {
            Log.log.error("Error creating user in graph db:" + username, e);
        }
    }

    private static void deleteUserInGraphDB(List<String> userSysIds, String username)
    {
        try
        {
            for (String sysId : userSysIds)
            {
                try
                {
                    ServiceGraph.deleteNode(null, sysId, null, NodeType.USER, username);
                }
                catch (Exception e)
                {
                    Log.log.error("error deleting user node with sysId:" + sysId, e);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting user in graph db:", e);
        }
    }

    public static int getNumberOfUsers()
    {
        int result = 0;

        try
        {
            QueryDTO query = new QueryDTO();
            query.setSelectColumns("sys_id,UUserName");
            query.setModelName("Users");
            
            result = getUsers(query, "system").size();
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        
        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static int getNumberOfAdminUsers()
    {
        int result = 0;
        Set<String> usernames = new HashSet<String>();

        try
        {
            HibernateProxy.execute(() -> {
            	// UNION ALL is not supported by HIBERNATE. So have to prepare 2
                // qrys and assemble it.
                String sql = "select u.UUserName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r " + " where r.UName = 'admin'";
                Query q = HibernateUtil.createQuery(sql);
                List<String> usernamesLocal = q.list();
                usernames.addAll(usernamesLocal);

                // query 2
                sql = "select u.UUserName from Users as u" + " join u.userGroupRels as ugr " + " join ugr.group as g " + " join g.groupRoleRels as grr " + " join grr.role as r " + " where r.UName = 'admin'";
                q = HibernateUtil.createQuery(sql);
                usernamesLocal = q.list();
                usernames.addAll(usernamesLocal);
            });
            
            result = usernames.size();
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    }
    
    /**
     * API to get users from given group.
     * @param groupNames : String representing comma seperated group name(s).
     * @param username : String representing user name who is invoking the API.
     * 
     * @return
     * 		{@code List<Map<String, String>> }. The Map will have two properties, userId and name.
     */
    public static List<Map<String, String>> getGroupUserNames(String groupNames, String username) throws Exception
    {
    	List<Map<String, String>> userMapList = new ArrayList<Map<String, String>>();
		Set<UsersVO> userSet = new HashSet<UsersVO>();
    	
		if (StringUtils.isNotBlank(groupNames))
		{
			String[] groupNameArray = groupNames.split(",");
			for (String groupName : groupNameArray)
			{
				
				GroupsVO groupVO = UserUtils.getGroup(null, groupName.trim(), username);
				if (groupVO != null)
				{
					List<UsersVO> userList = groupVO.getGroupUsers();
					if (userList != null && userList.size() > 0)
					{
						for (UsersVO userVO : userList)
						{
							userSet.add(userVO); 
						}
					}
					
//					List<RolesVO> roleList = groupVO.getGroupRoles();
//					if (roleList != null)
//					{
//						StringBuilder roles = new StringBuilder();
//						roles.append("( ");
//						for (RolesVO roleVO : roleList)
//						{
//							roles.append("'").append(roleVO.getUName()).append("',");
//						}
//						roles.append("'')");
//						
//						userList = UserUtils.getUsersWithRole(roles.toString(), null);
//						for (UsersVO userVO : userList)
//						{
//							userSet.add(userVO);
//						}
//					}
				}
			}
			
			if (userSet.size() > 0)
			{
				for (UsersVO userVO : userSet)
				{
					userMapList.add(getUserMap(userVO));
				}
			}
		}
		
		return userMapList;
    }
    
    private static Map<String, String> getUserMap(UsersVO user)
    {
    	Map<String, String> userMap = new HashMap<String, String>();
    	
    	userMap.put("userId", user.getUUserName());
    	userMap.put("name", user.ugetDisplayName());
    	
    	return userMap;
    }

    private static String verifyPasswordAndPrepareItsHistory(Users dbUser, String encPasswordFromUI) throws Exception
    {
        String passwordHistory = null;

        if (dbUser != null && dbUser.getUUserPassword() != null && !dbUser.getUUserPassword().equalsIgnoreCase(VO.STRING_DEFAULT) &&
            dbUser.getUPasswordHistory() != null && !dbUser.getUPasswordHistory().equalsIgnoreCase(VO.STRING_DEFAULT))
        {
            // check password history
            String historyCheck = PropertiesUtil.getPropertyString("users.password.historycheck");
            int historyPasswordCount = Integer.parseInt(historyCheck);

            //list of history pwd
            List<String> history = new ArrayList<String>();
            List<String> correctListOfHistoryPasswordsToValidate = new ArrayList<String>();            
            if (StringUtils.isNotEmpty(historyCheck) && historyPasswordCount > 0)
            {
//                history.add(dbUser.getUUserName());//add the current password to verify also
                history.addAll(StringUtils.stringToList(dbUser.getUPasswordHistory()));
                
                //reduce the list to the size of the check
                if(history.size() > historyPasswordCount)
                {
                    int size = history.size();
                    correctListOfHistoryPasswordsToValidate.addAll(history.subList(size - historyPasswordCount, size));
                }
                else
                {
                    correctListOfHistoryPasswordsToValidate.addAll(history);
                }

                for (String prevPassword : correctListOfHistoryPasswordsToValidate)
                {
                    if (encPasswordFromUI.equals(prevPassword))
                    {
                        throw new Exception("Password have been previously used. Please use another password.");
                    }
                }
            }

            //add the pwd to user history
            correctListOfHistoryPasswordsToValidate.add(encPasswordFromUI);
            if (correctListOfHistoryPasswordsToValidate.size() > historyPasswordCount)
            {
                correctListOfHistoryPasswordsToValidate.remove(0);
            }

            //update the pwd history
            passwordHistory = StringUtils.listToString(correctListOfHistoryPasswordsToValidate);
        }

        return passwordHistory;
    }

    private static Set<String> getUsernames(Set<String> userSysIds)
    {
        Set<String> usernames = new HashSet<String>();

        if(userSysIds != null)
        {
            for(String userSysId : userSysIds)
            {
                Users user = findUser(userSysId, "system");
                if(user != null)
                {
                    usernames.add(user.getUUserName());
                }
            }
        }

        return usernames;
    }

    private static void copyGraphComponents(String fromUsername, String toUsername)
    {
        if(StringUtils.isNotBlank(fromUsername) && StringUtils.isNotBlank(toUsername))
        {
            User fromUser = SocialCompConversionUtil.getSocialUser(fromUsername);
            User toUser = SocialCompConversionUtil.getSocialUser(toUsername);
            
            if (fromUser != null && toUser != null)
            {
                //copy the list of components that this user is following
                try
                {
                    List<RSComponent> comps = ServiceSocial.getUserStreams(null, fromUsername, false);
                    Set<String> compSysIds = new HashSet<String>();
                    if (comps != null)
                    {
                        for (RSComponent comp : comps)
                        {
                            //if its the clone user, ignore that comp 
                            if(comp.getId().equalsIgnoreCase(fromUser.getId()))
                                continue;
                            
                            compSysIds.add(comp.getId());
                        }
                    }

                    if (compSysIds.size() > 0)
                    {
                        //add the follow for all the comps
                        ServiceSocial.setFollowStreams(toUser, new ArrayList<String>(compSysIds), true);
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("error while assigning follow streams to user :" + toUser.getName(), e);
                }
                
                
                //copy the notifications that this user has set
                try
                {
                    List<ComponentNotification> notifications =  ServiceSocial.getGlobalNotificationsForUser(fromUser);
                    Set<String> selectedNotificationTypes = new HashSet<String>();
                    for(ComponentNotification compNotify : notifications)
                    {
                        List<NotificationConfig> notificationConfigs = compNotify.getNotifyTypes();
                        for(NotificationConfig notificationConfig : notificationConfigs)
                        {
                            if(notificationConfig.getValue().equalsIgnoreCase("true"))
                            {
                                selectedNotificationTypes.add(notificationConfig.getType().name());
                            }
                        }
                    }
                    
                    
                    if(selectedNotificationTypes.size() > 0)
                    {
                        ServiceSocial.updateGlobalNotificationsForUser(toUser, selectedNotificationTypes);
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("error while assigning notification settings to user :" + toUser.getName(), e);
                }
                
            }//end of if
        }
    }
    
    private static void verifyIfAdminUser(String username) throws Exception
    {
      //first decide if this user has the rights to do this operation
        boolean isAdminUser = isAdminUser(username);
        if(!isAdminUser)
        {
            String msg = String.format(NO_ADMIN_RIGHTS, username);
            throw new Exception(msg);
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static boolean doesUserHaveRolesAfterExcludingRolesAndGroups(String username, List<String> excludeGroups, List<String> excludeRoles)
    {
        
        if (StringUtils.isNotBlank(username))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	return (boolean) HibernateProxy.execute(() -> {
            		boolean hasRoles = false;

	                // UNION ALL is not supported by HIBERNATE. So have to prepare 2
	                // qrys and assemble it.
	                //String sql = "select r.UName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r " + " where u.UUserName = '" + username.toLowerCase() + "'";
	                String sql = "select r.UName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r " + " where u.UUserName = :userNameLC";
	                if(excludeRoles != null && excludeRoles.size() > 0)
	                {
	                    //String sqlRoles = SQLUtils.prepareQueryString(excludeRoles);
	                    //sql = sql + " and r.UName NOT IN (" + sqlRoles + ") ";
	                    sql = sql + " and r.UName NOT IN (:excludeRole) ";
	                }
	                    
	                Query q = HibernateUtil.createQuery(sql);
	                q.setParameter("userNameLC", username.toLowerCase());
	                
	                if (excludeRoles != null && !excludeRoles.isEmpty())
	                {
	                    q.setParameterList("excludeRole", excludeRoles);
	                }
	                
	                List<String> result = q.list();
	                if(result != null && result.size() > 0)
	                {
	                    hasRoles = true;
	                }
	                
	                //if there are no roles, verify if there are any groups
	                if(!hasRoles)
	                {
	                    // query 2
	                    /*sql = "select r.UName from Users as u" 
	                                    + " join u.userGroupRels as ugr " 
	                                    + " join ugr.group as g " 
	                                    + " join g.groupRoleRels as grr " 
	                                    + " join grr.role as r " 
	                                    + " where u.UUserName = '" + username.toLowerCase() + "'";*/
	                    sql = "select r.UName from Users as u" 
	                                    + " join u.userGroupRels as ugr " 
	                                    + " join ugr.group as g " 
	                                    + " join g.groupRoleRels as grr " 
	                                    + " join grr.role as r " 
	                                    + " where u.UUserName = :userNameLC";
	                    if(excludeGroups != null && excludeGroups.size() > 0)
	                    {
	                        //String sqlGroups = SQLUtils.prepareQueryString(excludeGroups);
	                        //sql = sql + " and g.UName NOT IN (" + sqlGroups + ") ";
	                        sql = sql + " and g.UName NOT IN (:excludeGroup) ";
	                    }
	                    
	                    q = HibernateUtil.createQuery(sql);
	                    q.setParameter("userNameLC", username.toLowerCase());
	                    
	                    if(excludeGroups != null && excludeGroups.size() > 0)
	                    {
	                        q.setParameterList("excludeGroup", excludeGroups);
	                    }
	                    
	                    result = q.list();
	                    if(result != null && result.size() > 0)
	                    {
	                        hasRoles = true;
	                    }
	                }

	                return hasRoles;
            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }

        }

        return false;
    }
    
    private static boolean stringCompare(String str1, String str2)
    {
        boolean equal = false;
        
        if(StringUtils.isBlank(str1) || str1.equalsIgnoreCase(VO.STRING_DEFAULT)) str1 = "";
        if(StringUtils.isBlank(str2)|| str1.equalsIgnoreCase(VO.STRING_DEFAULT)) str2 = "";
        
        
        if(str1.equals(str2))
        {
            equal = true;
        }
        
        
        
        return equal;
    }
    
    public static String getUserSysId(String username)
    {
        String result = null;

        if(StringUtils.isNotEmpty(username))
        {
            Users userResult = getUserPrivate(null, username);
            result = userResult.getSys_id(); 
        }

        return result;
    } // getUser

    private static class SaltedHistoryResult
    {
        int historySize = 0;
        String saltedPasswordHashHistory = null;
        
        SaltedHistoryResult(ArrayList<String> saltedPasswordHashHistoryList)
        {
            if (saltedPasswordHashHistoryList != null &&
                !saltedPasswordHashHistoryList.isEmpty())
            {
                historySize = saltedPasswordHashHistoryList.size();                
                this.saltedPasswordHashHistory = StringUtils.listToString(saltedPasswordHashHistoryList);
            }
        }
        
        public String getCurrentSaltedPasswordHash()
        {
            String saltedPasswordHash = null;
            
            if (historySize >= 1)
            {
                List<String> saltedPasswordHashHistoryList = StringUtils.stringToList(saltedPasswordHashHistory);
                saltedPasswordHash = saltedPasswordHashHistoryList.get(saltedPasswordHashHistoryList.size() - 1);
            }
            
            return saltedPasswordHash;
        }
        
        public String getSaltedPasswordHashHistory()
        {
            return saltedPasswordHashHistory;
        }
    }
    
    private static SaltedHistoryResult verifySaltedPasswordHashAndPrepareItsHistory(Users dbUser, String plainTextPasswordFromUI) throws Exception
    {
        //lists of historical salted password hash
        
        ArrayList<String> saltedPasswordHashHistoryComplete = new ArrayList<String>();
        ArrayList<String> saltedPasswordHashHistoryToValidate = new ArrayList<String>();
        
        if (dbUser != null && dbUser.getUUserSaltedPasswordHash() != null && !dbUser.getUUserSaltedPasswordHash().equalsIgnoreCase(VO.STRING_DEFAULT) &&
            dbUser.getUUserSaltedPasswordHashHistory() != null && !dbUser.getUUserSaltedPasswordHashHistory().equalsIgnoreCase(VO.STRING_DEFAULT))
        {
            // check password history
            String historyCheck = PropertiesUtil.getPropertyString("users.password.historycheck");
            int historyPasswordCount = Integer.parseInt(historyCheck) >= 0 ? Integer.parseInt(historyCheck) : 0;

            if (StringUtils.isNotEmpty(historyCheck) && historyPasswordCount > 0)
            {
                saltedPasswordHashHistoryComplete.addAll(StringUtils.stringToList(dbUser.getUUserSaltedPasswordHashHistory()));
                
                //reduce the lists to the size of history to check
                if(saltedPasswordHashHistoryComplete.size() > (historyPasswordCount + 1))
                {
                    int size = saltedPasswordHashHistoryComplete.size();
                    saltedPasswordHashHistoryToValidate.addAll(saltedPasswordHashHistoryComplete.subList(size - (historyPasswordCount + 1), size));
                }
                else
                {
                    saltedPasswordHashHistoryToValidate.addAll(saltedPasswordHashHistoryComplete);
                }

                // Last element is the always the current salt and password hash value
                for (int i = 0; i < (saltedPasswordHashHistoryToValidate.size()); i++)
                {
                    if (com.resolve.services.util.UserUtils.checkBcryptPasswordHash(plainTextPasswordFromUI, saltedPasswordHashHistoryToValidate.get(i)))
                    {
                        throw new Exception("This password has been used previously. Please use another password.");
                    }
                }
            }

            //add the current salt and salted password hash to user salt history and salted password hash history
            
            String saltedPasswordHashForNewPassword = com.resolve.services.util.UserUtils.bcryptPasswdHash(plainTextPasswordFromUI);
            saltedPasswordHashHistoryToValidate.add(saltedPasswordHashForNewPassword);
            
            if (saltedPasswordHashHistoryToValidate.size() > (historyPasswordCount + 1))
            {
                saltedPasswordHashHistoryToValidate.remove(0);
            }
        }

        return new SaltedHistoryResult(saltedPasswordHashHistoryToValidate);
    }
    
    
    // Org methods
    
    public static List<OrgsVO> getOrgs(QueryDTO query, String username)
    {
        List<OrgsVO> result = new ArrayList<OrgsVO>();
        
//        UsersVO user = UserUtils.getUser(username);
//        String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

//        String hql = "from Properties ";
//        if(StringUtils.isNotBlank(userOrg))
//        {
//            hql = hql + " where sysOrg is null or sysOrg = '" + userOrg + "' ";
//        }
//        hql = hql + " order by sysOrg, UName";

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null && data.size() > 0)
            {
                //grab the map of sysId-org name
                //Map<String, String> mapOfOrgs = OrgsUtil.getListOfAllOrgNames();

                //prepare the data
                String selectedColumns = query.getSelectColumns();
                
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Orgs instance = new Orgs();

                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        //add it to the list after converting to VO
                        result.add(getOrgNoCache(instance.getSys_id(), instance.getUName()));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        Orgs instance = (Orgs) o;
                        result.add(getOrgNoCache(instance.getSys_id(), instance.getUName()));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static Orgs getOrgModel(String sysId, String orgName)
    {
        try
        {
            return (Orgs) HibernateProxy.execute(() -> {
            	Orgs orgModel = null;
            	 if(StringUtils.isNotBlank(sysId))
                 {
                     orgModel = HibernateUtil.getDAOFactory().getOrgsDAO().findById(sysId);
                 }
                 else if(StringUtils.isNotBlank(orgName))
                 {
                     String sql = "from Orgs where LOWER(UName) = '" + orgName.trim().toLowerCase() + "'";
                     List<Orgs> list = HibernateUtil.createQuery(sql).list();
                     if(list != null && list.size() > 0)
                     {
                         orgModel = list.get(0);
                     }
                 }
                 
                 // Load all collections eagerly (current Hibernate version allows only single collection to be fetched eagerly)
                 
                 if (orgModel != null)
                 {
                     if (orgModel.getUChildOrgs() != null )
                         orgModel.getUChildOrgs().size();
                     
                     if (orgModel.getOrgGroupRels() != null)
                         orgModel.getOrgGroupRels().size();
                 }
                 return orgModel;
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return null;
    }
    
    private static boolean  doesGroupExistsForOrg(String groupSysId, Collection<OrgGroupRel> orgGroupRels)
    {
        boolean exists = false;

        if(StringUtils.isNotBlank(groupSysId) && orgGroupRels != null)
        {
            for(OrgGroupRel rel : orgGroupRels)
            {
                Groups group = rel.getGroup();
                
                if(group != null)
                {
                    if(group.getSys_id().equalsIgnoreCase(groupSysId))
                    {
                        exists = true;
                        break;
                    }
                }
            }
        }

        return exists;
    }
    
    private static void addGroupsToOrg(String orgSysId, Set<String> groupSysIds, String username, boolean deleteTheOldGroupsRel)
    {
        if(StringUtils.isNotBlank(orgSysId) && groupSysIds != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                Orgs org = getOrgModelNoCache(orgSysId, null);
	                
	                if(org != null)
	                {
	                    Collection<OrgGroupRel> orgGroupRels = org.getOrgGroupRels();
	
	                    // delete existing OrgGroupRels if the flag is true
	                    
	                    if(orgGroupRels != null && deleteTheOldGroupsRel)
	                    {
	                        for(OrgGroupRel rel : orgGroupRels)
	                        {
	                        	boolean isPresent = false;
	                        	for (String groupSysId : groupSysIds) {
	                        		if (groupSysId.equals(rel.getGroup().getSys_id()) && orgSysId.equals(rel.getOrg().getSys_id())) {
	                        			isPresent = true;
	                        			break;
	                        		}
	                        	}
	                        	if (!isPresent) {
	                        		HibernateUtil.getDAOFactory().getOrgGroupRelDAO().delete(rel);
	                        	}
	                        }
	                    }
	
	                    // start adding the groups to this org one by one
	                    for(String groupSysId : groupSysIds)
	                    {
	                        boolean groupExistsForThisOrg = doesGroupExistsForOrg(groupSysId, orgGroupRels);
	
	                        // Add the group only if it does not exists in Org
	                        
	                        if(!groupExistsForThisOrg)
	                        {
	                            Groups group = HibernateUtil.getDAOFactory().getGroupsDAO().findById(groupSysId);
	                            
	                            if(group != null)
	                            {
	                                OrgGroupRel rel = new OrgGroupRel();
	                                
	                                rel.setOrg(org);
	                                rel.setGroup(group);
	                                
	                                HibernateUtil.getDAOFactory().getOrgGroupRelDAO().persist(rel);
	                            }
	                        }
	                    }//end of for
	                }//end of if

            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }
        
    public static OrgsVO getOrg(String sysId, String orgName)
    {
        Orgs orgModel = null;
        OrgsVO orgVO = null;

        try
        {
            orgModel = getOrgModel(sysId, orgName);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        if (orgModel != null)
        {
            orgVO = /*convertModelToVOForOrgs(orgModel, null)*/orgModel.doGetVO();
        }

        return orgVO;
    }
    
    public static OrgsVO getOrgNoCache(String sysId, String orgName)
    {
        OrgsVO orgVO = null;

        try
        {
            orgVO = (OrgsVO) HibernateProxy.executeNoCache(() ->{
            	return getOrg(sysId, orgName);
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return orgVO;
    }
    
    public static Orgs getOrgModelNoCache(String sysId, String orgName)
    {
        Orgs orgModel = null;

        try
        {
            orgModel = (Orgs) HibernateProxy.executeNoCache(() -> {
            	return getOrgModel(sysId, orgName);
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return orgModel;
    }
    
    public static OrgsVO saveOrg(OrgsVO vo, String username) throws Exception
    {
        //first decide if this user has the rights to do this operation
        verifyIfAdminUser(username);

        if(vo != null && vo.validate())
        {
            Orgs model = null;

            boolean isInsert = (StringUtils.isBlank(vo.getSys_id()) || vo.getSys_id().equals(VO.STRING_DEFAULT)) ? true : false;
            //boolean invalidateUserMenuCache = false;

            if(isInsert)
            {
                model = getOrgModel(null, vo.getUName());
                
                if(model != null)
                {
                    throw new RuntimeException("Org '" + vo.getUName() + "' already exists. Please try again.");
                }

                vo.setSysCreatedOn(null);
                vo.setSysUpdatedOn(null);
                vo.setSysCreatedBy(username);
                vo.setSysUpdatedBy(username);
                
                model = new Orgs();
            }
            else
            {
                //UPDATE
                //first check if the org with the same name and different sys_id already exist
                
                model = getOrgModel(null, vo.getUName());
                
                if(model != null && !model.getSys_id().equalsIgnoreCase(vo.getSys_id()))
                {
                    throw new Exception("Org with name " + vo.getUName() + " already exists.");
                }
                
                model = getOrgModel(vo.getSys_id(), null);
            }

            model.applyVOToModel(vo);
            model = SaveUtil.saveOrg(model, username);
            
            //add groups to org
            
            Collection<GroupsVO> orgGroups = vo.getOrgGroups();
            
            if(orgGroups != null)
            {
                Set<String> groupSysIds = new HashSet<String>();
                for(GroupsVO group : orgGroups)
                {
                    groupSysIds.add(group.getSys_id());
                }

                addGroupsToOrg(model.getSys_id(), groupSysIds, username, true);
                //invalidateUserMenuCache = true;
            }
            
//            vo = convertModelToVOForGroups(model, null, null);//model.doGetVO();
            vo = getOrgNoCache(model.getSys_id(), null);
            
            /*//invalidate the cache
            if(invalidateUserMenuCache)
            {
                MenuCache.getInstance().invalidate();
            }*/
        }

        return vo;
    }
    
    public static Map<String, String> getUserOrgHierarchyIdNameMap(String username) throws Exception
    {
        Map<String, String> orgIdNameMap = new HashMap<String, String>();
        
        UsersVO user =  getUserNoCache(username);
        
        if(user == null) {
          return orgIdNameMap;
        }
        
        Set<OrgsVO> orgs = user.getUserOrgs();
        
        if (orgs != null && orgs.size() > 0)
        {
            for (OrgsVO orgVO : orgs)
            {
                Set<OrgsVO> childOrgs = UserUtils.getAllOrgsInHierarchy(orgVO.getSys_id(), orgVO.getUName());
                if (childOrgs != null && childOrgs.size() > 0)
                {
                    for (OrgsVO childOrgVO : childOrgs)
                    {
                        if (!orgIdNameMap.containsKey(childOrgVO.getSys_id()))
                        {
                        	orgIdNameMap.put(childOrgVO.getSys_id(), childOrgVO.getUName());
                        }
                    }
                }
            }
        }
        
        return orgIdNameMap;
    }
    
    public static Collection<String> getUserOrgHierarchyIdList(String username) throws Exception
    {
    	return getUserOrgHierarchyIdNameMap(username).keySet();
    }
    
    private static void getAllOrgsInHierarchyInternal(OrgsVO curentRootOrgVO, Set<OrgsVO> orgsInHierarchy)
    {
        if ( curentRootOrgVO != null && curentRootOrgVO.getUChildOrgs() != null && 
             !curentRootOrgVO.getUChildOrgs().isEmpty())
        {
            for ( OrgsVO childOrg : curentRootOrgVO.getUChildOrgs() )
            {
                orgsInHierarchy.add(childOrg);
                
                OrgsVO childOrgsVOWithChildrens = getOrgNoCache(childOrg.getSys_id(), null);
                
                getAllOrgsInHierarchyInternal(childOrgsVOWithChildrens, orgsInHierarchy);
            }
        }
    }
    
    private static void getAllParentOrgsInHierarchy(OrgsVO curentChildOrgVO, List<OrgsVO> allParentOrgsInHierarchy)
    {
        if ( curentChildOrgVO != null && curentChildOrgVO.getUParentOrg() != null)
        {
            allParentOrgsInHierarchy.add(curentChildOrgVO.getUParentOrg());
            
            // OrgsVO maintains only its childrens but no grand chidrens in hierarchy and parent but no grand parents in hierarchy            
            OrgsVO newChildOrgVO = getOrgNoCache(curentChildOrgVO.getUParentOrg().getSys_id(), curentChildOrgVO.getUParentOrg().getUName());
            
            getAllParentOrgsInHierarchy(newChildOrgVO, allParentOrgsInHierarchy);
        }
    }
    
    public static Set<OrgsVO> getValidParentOrgs(String orgId, String orgName)
    {
        Set<OrgsVO> validParentOrgs = new HashSet<OrgsVO>();
        
        Set<OrgsVO> allOrgsInHierarchy = getAllOrgsInHierarchy(orgId, orgName);
        
        QueryDTO query = new QueryDTO();
        query.setModelName("Orgs");
        
        List<OrgsVO> allOrgs = getOrgs(query, null);
        
        if (allOrgs != null && !allOrgs.isEmpty())
        {
            if (allOrgsInHierarchy != null && !allOrgsInHierarchy.isEmpty())
            {
                allOrgs.removeAll(allOrgsInHierarchy);
            }
            
            for (OrgsVO validParentOrgVO : allOrgs)
            {
                validParentOrgs.add(validParentOrgVO);
            }
        }
        
        return validParentOrgs;
    }
    
    public static List<OrgsVO> getParentOrgsInHierarchy(String orgId, String orgName)
    {
        List<OrgsVO> parentOrgsInHierarchy = new ArrayList<OrgsVO>();
        
        OrgsVO curentChildOrgVO = getOrgNoCache(orgId, orgName);
        
        getAllParentOrgsInHierarchy(curentChildOrgVO, parentOrgsInHierarchy);
        
        return parentOrgsInHierarchy;
    }
    
    public static Set<OrgsVO> getAllOrgsInHierarchy(String orgId, String orgName)
    {
        Set<OrgsVO> allChildOrgs = new HashSet<OrgsVO>();
        
        OrgsVO rootOrgVO = getOrgNoCache(orgId, orgName);
        
        getAllOrgsInHierarchyInternal(rootOrgVO, allChildOrgs);
        
        if (rootOrgVO != null)
        {
            allChildOrgs.add(rootOrgVO);
        }
        
        return allChildOrgs;
    }
    
    public static boolean getUserfunctionPermission(String username, String fucntion, String property)
    {
        // resolve.maint/admin/system users will be granted full permission on all the functions.
        if (isSuperUser(username))
        {
            return true;
        }
        
        boolean result = false;
        
        Pair<JSONObject, JSONObject> permissionsPair = getUserPermissions(username);
        
        // Always check new permissions first and if not found search in old permissions if there
        
        boolean found = false;
        
        if (permissionsPair.getRight() != null)
        {
        	Object obj = permissionsPair.getRight().get(fucntion);
        	
        	// New function search which is without property
        	
        	if (obj != null && obj instanceof Boolean)
        	{
        		result = (Boolean)obj;
        		found = true;
        	}
        	
        	if (!found && StringUtils.isNotBlank(property))
        	{
	        	// Search by mapping old function.property to new function
	        	
	        	String newFunction = RBACUtils.legacyToNewRBAC.get(fucntion + RBACUtils.RBAC_SEPARATOR + property);
	        	
	        	if (StringUtils.isNotBlank(newFunction))
	        	{
		        	String[] newFunctionNames = newFunction.split(RBACUtils.RBAC_VALUE_SEPARATOR);
		        	
		        	for (int i = 0; i < newFunctionNames.length; i++)
		        	{
		        		obj = permissionsPair.getRight().get(newFunctionNames[i]);
			        	
			        	if (obj != null && obj instanceof Boolean)
			        	{
			        		result = (Boolean)obj;
			        		found = true;
			        		break;
			        	}
		        	}
	        	}
	        	else
	        	{
	        		Log.log.trace(String.format(PRESUMED_LEGACY_RBAC_HAS_NO_NEW_RBAC_MAPPING, fucntion, 
	        									(StringUtils.isNotBlank(property) ? 
	        									 RBACUtils.RBAC_SEPARATOR + property : BLANK)));
	        	}
        	}
        }
        
        if (!found && permissionsPair.getLeft() != null)
        {
            Object obj = permissionsPair.getLeft().get(fucntion);
            
            if (obj != null)
            {
                if (obj instanceof String)
                {
                    result = obj.toString().equals(property);
                }
                else if (obj instanceof JSONObject)
                {
                    JSONObject functionMap = (JSONObject)obj;
                    result = functionMap.getBoolean(property);
                }
                
                Log.log.trace(String.format(RETURNING_FUNCTION_PERMISSIONS_FROM_LEGACY_LOG, username, fucntion, 
                			  			    (StringUtils.isNotBlank(property) ? 
                			  			     RBACUtils.RBAC_SEPARATOR + property : BLANK)));
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static Pair<JSONObject, JSONObject> getUserPermissions(String username)
    {
        Object userPermissions = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.HAS_ACCESS_RIGHT_CACHE_REGION, username);
        Object userNewPermissions = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.NEW_HAS_ACCESS_RIGHT_CACHE_REGION, username);
        JSONObject permissions = null;
        JSONObject newPermissions = null;
        
        if (userPermissions == null && userNewPermissions == null)
        {
            Users user = findUserWithAllReferences(null, username);
            
            permissions = new JSONObject();
            newPermissions = new JSONObject();
            List<RolesVO> roleVIList = UserUtils.setUserRoles(user);
            mergePermissions(roleVIList, permissions, newPermissions);
            
            if (StringUtils.isNotBlank(NOT_IMPLEMENTED_BY_UI_FUNCTION_PERMISSIONS_JSON))
            {
                JSONArray noUIImplFunctsPermissions = StringUtils.stringToJSONArray(NOT_IMPLEMENTED_BY_UI_FUNCTION_PERMISSIONS_JSON, true);
                
                // Implemented function permissions will always override the non-implemented function permissions
                
                Set<String> implementedFunctions = permissions.keySet();
                
                if (noUIImplFunctsPermissions != null && noUIImplFunctsPermissions.isArray())
                {
                    for (int i = 0; i < noUIImplFunctsPermissions.size(); i++)
                    {
                        JSONObject noUIImplFunctPermissions = noUIImplFunctsPermissions.getJSONObject(i);
                        
                        if (!noUIImplFunctPermissions.isNullObject() && 
                            noUIImplFunctPermissions.keySet() != null &&
                            noUIImplFunctPermissions.keySet().size() == 1)
                        {
                            for (String function : (Set<String>)noUIImplFunctPermissions.keySet())
                            {
                                if (!implementedFunctions.contains(function) && 
                                    !(noUIImplFunctPermissions.get(function) instanceof String))
                                {
                                    permissions.accumulate(function, noUIImplFunctPermissions.get(function));
                                    
                                    Log.log.trace(String.format(
                                    						FUNCTION_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG,
                                    						function, 
                                    						StringUtils.jsonObjectToString(
                                    								(JSONObject)noUIImplFunctPermissions.get(function)), 
                                    						username));
                                }
                            }
                        }
                    }
                }
            }
            
            // Handle new RBACs not implemented as of yet by UI
            
            if (StringUtils.isNotBlank(NOT_IMPLEMENTED_BY_UI_RBACS_JSON))
            {
            	JSONObject noUIImplRBACPermissions = StringUtils.stringToJSONObject(NOT_IMPLEMENTED_BY_UI_RBACS_JSON);
	            
            	Set<String> implementedRBACs = newPermissions.keySet();
            	
            	// Implemented new RBAC permissions will always override the non-implemented new RBAC permissions
	            
	            if (noUIImplRBACPermissions != null && !noUIImplRBACPermissions.isEmpty() &&
	                !noUIImplRBACPermissions.isNullObject())
	            {
	            	for (String noUIImplRBAC : (Set<String>)noUIImplRBACPermissions.keySet())
                    {
	            		if (!implementedRBACs.contains(noUIImplRBAC) && 
	            			noUIImplRBACPermissions.get(noUIImplRBAC) instanceof Boolean)
	            		{
	            			newPermissions.accumulate(noUIImplRBAC, noUIImplRBACPermissions.get(noUIImplRBAC));
	            			
	            			Log.log.trace(String.format(RBAC_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG, 
	            										noUIImplRBAC, noUIImplRBACPermissions.get(noUIImplRBAC),
	            										username));
	            		}
                    }
	            }
            }
            
            cacheUserPermissions(permissions, newPermissions, username);
        }
        else
        {
            permissions = (JSONObject) userPermissions;
            newPermissions = (JSONObject) userNewPermissions;
        }
        
        return Pair.of(permissions, newPermissions);
    }
    
    @SuppressWarnings("unchecked")
    public static void mergePermissions(List<RolesVO> userRoles, JSONObject permissions, JSONObject newPermissions) {
        if (userRoles != null) {
            Iterator<RolesVO> it = userRoles.iterator();
            // The user's permissions are merged (ORed) from the same permissions of all roles.
            
            Collection<String> aRBACs = RBACUtils.getApplicationRBACs();
            
            while(it.hasNext()) {
                JSONObject cur = it.next().getUPermissions();
                
                Log.log.trace(String.format(DB_LEGACY_OR_NEW_RBAC_JSON_LOG, cur));
                JSONObject newCur = RBACUtils.mapToNewRBACs(cur);
                
                if ((cur == null || cur.isNullObject() || cur.isEmpty()) && 
                	(newCur == null || newCur.isNullObject() || newCur.isEmpty())) {
                    continue;
                }
                
                // Handles sir A-RBAC ("sir") not implemented by UI. 
                
                if (!newPermissions.keySet().contains(RBACUtils.RBAC_SIR) &&
                	!newPermissions.keySet().contains(RBACUtils.RBAC_SIR_ENABLED) &&
                    !newCur.keySet().contains(RBACUtils.RBAC_SIR) &&
                    !newCur.keySet().contains(RBACUtils.RBAC_SIR_ENABLED) &&
                	StringUtils.isNotBlank(NOT_IMPLEMENTED_BY_UI_RBACS_JSON))
                {
                	JSONObject noUIImplRBACPermissions = StringUtils.stringToJSONObject(NOT_IMPLEMENTED_BY_UI_RBACS_JSON);
    	            
                	if (noUIImplRBACPermissions != null && !noUIImplRBACPermissions.isEmpty() &&
	    	                !noUIImplRBACPermissions.isNullObject())
	    	        {
    	            	for (String noUIImplRBAC : (Set<String>)noUIImplRBACPermissions.keySet())
                        {
    	            		if (noUIImplRBAC.equals(RBACUtils.RBAC_SIR) && 
    	            			noUIImplRBACPermissions.get(noUIImplRBAC) instanceof Boolean)
    	            		{
    	            			newCur.accumulate(noUIImplRBAC, noUIImplRBACPermissions.get(noUIImplRBAC));
    	            			newCur.accumulate(RBACUtils.RBAC_SIR_ENABLED, noUIImplRBACPermissions.get(noUIImplRBAC));
    	            			
    	            			Log.log.trace(String.format(ARBAC_NOT_IMPLEMENTED_BY_UI_ADDED_FOR_ROLE, noUIImplRBAC, 
                                        	 			    noUIImplRBACPermissions.get(noUIImplRBAC)));
    	            			break;
    	            		}
                        }
	    	        }
                }
                
                Log.log.trace(String.format(TRANSFORMED_IF_DB_LEGACY_RBAC_JSON, newCur));
                
                // merge legacy RBAC permissions
                
                Iterator<String> keys = cur.keys();
                while(keys.hasNext()) {
                    String key = (String)keys.next();
                    if (permissions.get(key) == null) {
                        permissions.put(key, cur.get(key));
                    } else {
                        Object val = permissions.get(key);
                        if (val instanceof JSONObject) {
                            JSONObject permission = (JSONObject)val;
                            JSONObject curPermission = (JSONObject)cur.get(key);
                            /*
                             * 'disabled' flag will be treated as false;
                             */
                            
                            // existing view flag
                            Boolean existingview = toBoolean(permission, "view");
                            permission.put("view", existingview | toBoolean(curPermission, "view"));
                            
                            // existing create flag
                            Boolean existingCreate = toBoolean(permission, "create");
                            permission.put("create", existingCreate | toBoolean(curPermission, "create"));
                            
                            // existing modify flag
                            Boolean existingModify = toBoolean(permission, "modify");
                            permission.put("modify", existingModify | toBoolean(curPermission, "modify"));
                            
                            // existing delete flag
                            Boolean existingDelete = toBoolean(permission, "delete");
                            permission.put("delete", existingDelete | toBoolean(curPermission, "delete"));
                            
                            // existing execute flag
                            Boolean existingExecute = toBoolean(permission, "execute");
                            permission.put("execute", existingExecute | toBoolean(curPermission, "execute"));
                            
                            permissions.put(key, permission);
                        } else if (val instanceof String){
                            String permission = (String)val;
                            String curPermission = (String)cur.get(key);
                            if (permission.equals("all") || curPermission.equals("all")) {
                                permissions.put(key, "all");
                            } else {
                                permissions.put(key, "user");
                            }
                        }
                    }
                }
                
                // merge new RBAC permissions
                
                Iterator<String> newKeys = newCur.keys();
                
                // Process all ARBACs first
                
                while(newKeys.hasNext()) {
                	String newKey = (String)newKeys.next();
                	
                	if (aRBACs.contains(newKey)) {
                		if (newPermissions.get(newKey) == null) {
                    		newPermissions.put(newKey, newCur.get(newKey));
                        } else {
                        	newPermissions.put(newKey, 
                        					   Boolean.valueOf(((Boolean)newPermissions.get(newKey)).booleanValue() | 
                        							   		   ((Boolean)newCur.get(newKey)).booleanValue()));
                        }
                	}
                }
                
                /*
                 * Process RBACs after A-RBACs since effective RBAC will depend on A-RBACs.
                 * For example RBAC sir.xxx.yyy depends on A-RABC sir
                 */
                
                newKeys = newCur.keys();
                
                while(newKeys.hasNext()) {
                	String newKey = (String)newKeys.next();
                	
                	String newKeyARBAC = !aRBACs.contains(newKey) ? RBACUtils.getApplicationRBAC(newKey) : null;
                	
                	boolean newCurrValue = ((Boolean)newCur.get(newKey)).booleanValue() &
										    (StringUtils.isNotBlank(newKeyARBAC) ? 
										     ((Boolean)newPermissions.get(newKeyARBAC)).booleanValue() : true);
                	
                	if (newPermissions.get(newKey) == null) {
                		newPermissions.put(newKey, newCurrValue);
                    } else {
                    	newPermissions.put(newKey, 
                    					   Boolean.valueOf(((Boolean)newPermissions.get(newKey)).booleanValue() | 
                    							   		   newCurrValue));
                    }
                }
            }
        }
    }
    
    private static boolean toBoolean(JSONObject o, String k) {
        Object val = o.get(k);
        if (val instanceof Boolean) {
            return (Boolean)val;
        }
        return false;
    }
    
    private static boolean  doesOrgExistsForGroup(String orgSysId, Set<OrgGroupRel> orgGroupRels)
    {
        boolean exists = false;

        if(StringUtils.isNotBlank(orgSysId) && orgGroupRels != null)
        {
            for(OrgGroupRel rel : orgGroupRels)
            {
                Orgs org = rel.getOrg();
                
                if(org != null)
                {
                    if(org.getSys_id().equalsIgnoreCase(orgSysId))
                    {
                        exists = true;
                        break;
                    }
                }
            }
        }

        return exists;
    }
    
    private static void addOrgsToGroup(String  groupSysId, Set<String> orgSysIds, String username, boolean deleteTheOldOrgsRel)
    {
        if(StringUtils.isNotBlank(groupSysId) && orgSysIds != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
	
	                Groups group = HibernateUtil.getDAOFactory().getGroupsDAO().findById(groupSysId);
	                
	                if(group != null)
	                {
	                    Set<OrgGroupRel> orgGroupRels = group.getOrgGroupRels();
	
	                    //delete if the flag is true
	                    if(orgGroupRels != null && deleteTheOldOrgsRel) {
	                        
	                        for(OrgGroupRel rel : orgGroupRels)
	                        {
	                            HibernateUtil.getDAOFactory().getOrgGroupRelDAO().delete(rel);
	                        }
	                        
	                        group.setOrgGroupRels(new HashSet<OrgGroupRel>());
	                        
	                        /*Groups updatedGroup =*/ HibernateUtil.getDAOFactory().getGroupsDAO().update(group);                   
	                    }
	
	                    //start adding the orgs to this group one by one
	                    for(String orgSysId : orgSysIds)
	                    {
	                        boolean orgExistsForThisGroup = false;
	                        
	                        // check if org exists only if delete all old org relations is false
	                        
	                        if(!deleteTheOldOrgsRel)
	                        {
	                            orgExistsForThisGroup = doesOrgExistsForGroup(orgSysId, orgGroupRels);
	                        }
	
	                        //if the org does not exists, than add it
	                        
	                        if(!orgExistsForThisGroup)
	                        {
	                            Orgs org = HibernateUtil.getDAOFactory().getOrgsDAO().findById(orgSysId);
	                            
	                            if(org != null)
	                            {
	                                OrgGroupRel rel = new OrgGroupRel();
	                                rel.setGroup(group);
	                                rel.setOrg(org);
	                                
	                                HibernateUtil.getDAOFactory().getOrgGroupRelDAO().persist(rel);
	                            }
	                        }
	                    }//end of for
	                }//end of if

            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }
    
    private static void validatePassword (String password) {
    	if (!isPasswordValid(password)) {
			throw new IllegalArgumentException("Password minimum requirements are not met.");
    	}
    }

    private static boolean isPasswordValid(String password) {
		return password != null && password.matches(PAS_SWORD_RULES);
    }
    
    @Deprecated
    public static boolean isOrgAccessible(String orgId, String username)
    {
        return isOrgAccessible(orgId, username, false, false);
    }
    
    private static UsersVO getUserVOFromCache(String username, boolean orgsOnly) {
    	Users cachedUser = null;
    	UsersVO usersVO = null;
        
        Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, username);
        if (obj != null)
        {
            cachedUser = (Users) obj;
            Log.log.trace(String.format(FOUND_CACHED_DB_OBJECT_FOR_USER_LOG, username));
            
            if ((cachedUser.getUserGroupRels() == null || cachedUser.getUserGroupRels().isEmpty()) &&
            	!OrgsUtil.getAllOrgVOs().isEmpty())
            {
            	/*
                 * Remove the Users object in DB user cache which is corrupted by Groups.doGetVO() resetting UserGroupRels to null
                 * to avoid cyclic references.
                 * 
                 * Commented out as now Groups.doGetVO() removes the original DB user cached Users object before resetting
                 * UserGroupRels to null and setting Users object into Group and then restores back the original Users 
                 * object back into DB user cache.
                 *
                 * HP TODO
                 *  
                 *  Need to think about locking on Hibernate cache used for DB caching.
                 */
            	
            	Log.log.warn(String.format(FOUND_CACHED_DB_OBJECT_FOR_USER_NO_GROUPS_WITH_ORGS_LOG, username));
                HibernateUtil.clearCachedDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, username);
                Log.log.trace(String.format(CLEARED_CACHE_DB_OBJECT_FOR_USER_LOG, username));
                cachedUser = null;
            }
        }
        
        if (cachedUser != null)
        {
        	if (orgsOnly)
        	{
        		usersVO = convertModelUsersToVO(cachedUser, null, null, null, false, false, false, true);
        	}
        	else
        	{
        		usersVO = convertModelUsersToVO(cachedUser, null, null, null, true);
        	}
        }
        else
        {
            Users tmpUser = findUserWithAllReferencesNoCache(null, username);

        	if (orgsOnly)
        	{
        		usersVO = convertModelUsersToVO(tmpUser, null, null, null, false, false, false, true);
        	}
        	else
        	{
        		usersVO = convertModelUsersToVO(tmpUser, null, null, null, true);
        	}
            
            if (tmpUser != null && 
            	((tmpUser.getUserGroupRels() != null && !tmpUser.getUserGroupRels().isEmpty()) ||
            	 ((tmpUser.getUserGroupRels() == null || tmpUser.getUserGroupRels().isEmpty()) &&
            	  OrgsUtil.getAllOrgVOs().isEmpty())))
            {
                HibernateUtil.cacheDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, 
                                            new String[] { "com.resolve.persistence.model.Users", 
                                                           "com.resolve.persistence.model.UserRoleRel", 
                                                           "com.resolve.persistence.model.Roles", 
                                                           "com.resolve.persistence.model.UserGroupRel", 
                                                           "com.resolve.persistence.model.Groups", 
                                                           "com.resolve.persistence.model.GroupRoleRel", 
                                                           "com.resolve.persistence.model.UserPreferences", 
                                                           "com.resolve.persistence.model.Organization", 
                                                           "com.resolve.persistence.model.Orgs", 
                                                           "com.resolve.persistence.model.OrgGroupRel" }, 
                                            tmpUser.getUUserName(), tmpUser);
                
                Log.log.trace(String.format(CACHED_USER_IN_CACCHED_DB_OBJECT_LOG, username));
            }
            else
            {
            	Log.log.warn(String.format(USER_NOT_CACHED_IN_CACHED_DB_OBJECT_LOG, username, username));
            }
        }
        
        return usersVO;
    }
    
    @SuppressWarnings("unchecked")
	public static boolean isOrgAccessible(String orgId, String username, boolean unrestrictedAdminAccess, boolean unrestrictedSystemAccess)
    {
    	long start = System.nanoTime();
        if (StringUtils.isBlank(orgId) || StringUtils.isBlank(username))
        {
            throw new RuntimeException("Passed invalid Org Id and/or username.");
        }
                
        if (isSuperUser(username))
        {
        	Log.log.trace(String.format(SUPER_USER_ORG_ACCESS_LOG, username, orgId, (System.nanoTime() - start)));
            return true;
        }

        boolean result = false;
        boolean done = false;
        Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.IS_ORG_ACCESSIBLE_CACHE_REGION, username);
        if (obj != null)
        {
        	Pair<Boolean, Set<String>> userOrgAccessPair = (Pair<Boolean, Set<String>>)obj;
        	
        	if (CollectionUtils.isNotEmpty(userOrgAccessPair.getRight())) {
        		result = userOrgAccessPair.getRight().contains(orgId);
        	}
        	
        	if (Log.log.isTraceEnabled()) {
        		Log.log.trace(String.format(USER_HAS_ORG_ACCESS_LOG, username, orgId, (System.nanoTime() - start)));
        	}
        	
            return result;
        }

        UsersVO usersVO = getUserVOFromCache(username, true);
        
        if (CollectionUtils.isEmpty(usersVO.getAccessibleOrgIds()))
        {
            Log.log.warn(String.format(HAS_NO_ORGS_LOG, username));
            result = false;
            done = true;
        }
        
        if (!done && usersVO.isOrgIdAccessible(orgId))
        {
        	Log.log.trace(String.format(USER_HAS_ORG_ACCESS_LOG, username, orgId, (System.nanoTime() - start)));
            result = true;
            done = true;
        }                 
        
        if (Log.log.isTraceEnabled()) {
	        if (result) {
	        	Log.log.trace(String.format(USER_HAS_ORG_ACCESS_LOG, username, orgId, (System.nanoTime() - start)));
	        } else {
	        	Log.log.trace(String.format(USER_DOES_NOT_HAVE_ORG_ACCESS_LOG, username, orgId, (System.nanoTime() - start)));
	        }
        }
                
        HibernateUtil.cacheDBObject(DBCacheRegionConstants.IS_ORG_ACCESSIBLE_CACHE_REGION, 
                new String[] { "com.resolve.persistence.model.Users", 
                               "com.resolve.persistence.model.UserRoleRel", 
                               "com.resolve.persistence.model.Roles", 
                               "com.resolve.persistence.model.UserGroupRel", 
                               "com.resolve.persistence.model.Groups", 
                               "com.resolve.persistence.model.GroupRoleRel", 
                               "com.resolve.persistence.model.UserPreferences", 
                               "com.resolve.persistence.model.Organization", 
                               "com.resolve.persistence.model.Orgs", 
                               "com.resolve.persistence.model.OrgGroupRel" }, 
                username, Pair.of(Boolean.valueOf(usersVO.getHasNoOrgAccess()), usersVO.getAccessibleOrgIds()));
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
	public static boolean isNoOrgAccessible(String username)
    {
    	long start = System.nanoTime();
    	
        boolean hasNoOrgAccess = isSuperUser(username);
        
        if (!hasNoOrgAccess)
        {
        	Boolean noOrgAcess = Boolean.FALSE;
            
            Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.IS_ORG_ACCESSIBLE_CACHE_REGION, username);
            
            if (obj != null)
            {
            	Pair<Boolean, Set<String>> userOrgAccessPair = (Pair<Boolean, Set<String>>)obj;
            	
            	if (userOrgAccessPair.getLeft() != null) {
            		noOrgAcess = userOrgAccessPair.getLeft();
            	}
            	
            	if (Log.log.isTraceEnabled()) {
                	Log.log.trace(String.format(USER_NO_ORG_ACCESS_LOG, username, (noOrgAcess ? HAS : DOES_NOT_HAVE), 
                								(System.nanoTime() - start)));
                }
            	
                return noOrgAcess;
            }
        	
        	UsersVO usersVO = getUserVOFromCache(username, true);
        	
        	if (usersVO != null) {
        		hasNoOrgAccess = usersVO.getHasNoOrgAccess();
        		
                HibernateUtil.cacheDBObject(DBCacheRegionConstants.IS_ORG_ACCESSIBLE_CACHE_REGION, 
                        new String[] { "com.resolve.persistence.model.Users", 
                                       "com.resolve.persistence.model.UserRoleRel", 
                                       "com.resolve.persistence.model.Roles", 
                                       "com.resolve.persistence.model.UserGroupRel", 
                                       "com.resolve.persistence.model.Groups", 
                                       "com.resolve.persistence.model.GroupRoleRel", 
                                       "com.resolve.persistence.model.UserPreferences", 
                                       "com.resolve.persistence.model.Organization", 
                                       "com.resolve.persistence.model.Orgs", 
                                       "com.resolve.persistence.model.OrgGroupRel" }, 
                        username, Pair.of(Boolean.valueOf(hasNoOrgAccess), usersVO.getAccessibleOrgIds()));
        	}
        }
        
        if (Log.log.isTraceEnabled()) {
        	Log.log.trace(String.format(USER_NO_ORG_ACCESS_LOG, username, (hasNoOrgAccess ? HAS : DOES_NOT_HAVE), 
        								(System.nanoTime() - start)));
        }
        
        return hasNoOrgAccess;
    }
    
    public static String getDefaultOrgIdForUser(String username)
    {
        String defaultOrgsId = null;
        
        String intUserName = username;
        
        if (StringUtils.isNotBlank(intUserName) && 
            (intUserName.equalsIgnoreCase("system") ||
             intUserName.equalsIgnoreCase(Constants.RESOLVE_MAINT)))
        {
            intUserName = "admin";
        }
        
        if (StringUtils.isBlank(intUserName))
        {
            return defaultOrgsId;
        }
        
        UsersVO usersVO = UserUtils.getUser(intUserName);
        
        if (usersVO != null && StringUtils.isNotBlank(usersVO.getSysOrg()))
        {
            defaultOrgsId = usersVO.getSysOrg();
        }
        
        return defaultOrgsId;
    }

    public static String encrypt(String in) {
    	try {
    		if (StringUtils.isNotEmpty(in)) {
    			return CryptUtils.encrypt(in);
    		}
		} catch (Exception e) {
			Log.log.error("Unable to encrypt user data");
		}
    	
    	return StringUtils.EMPTY;
    }

    public static String decrypt(String in) {
    	try {
    		if (StringUtils.isNotEmpty(in)) {
    			return CryptUtils.decrypt(in);
    		}
		} catch (Exception e) {
			Log.log.error("Unable to decrypt user data");
		}
    	
    	return StringUtils.EMPTY;
    }
    
	@SuppressWarnings({ "deprecation", "rawtypes" })
	public static void encryptUserData() {
		try {
			HibernateProxy.execute(() -> {
				Query q = HibernateUtil.createQuery("from Users");
				
				@SuppressWarnings("unchecked")
				List<Users> users = q.list();
	
				int encryptedCount = 0;
				
				for (Users user : users) {
					if (isUserDataNeedEncryption(user)) {
						if (StringUtils.isNotEmpty(user.getUQuestion1()) && !CryptUtils.isEncrypted(user.getUQuestion1())) {
							user.setUQuestion1Enc(encrypt(user.getUQuestion1()));
							user.setUQuestion1(null);
						}
	
						if (StringUtils.isNotEmpty(user.getUQuestion2()) && !CryptUtils.isEncrypted(user.getUQuestion2())) {
							user.setUQuestion2Enc(encrypt(user.getUQuestion2()));
							user.setUQuestion2(null);
						}
	
						if (StringUtils.isNotEmpty(user.getUQuestion3()) && !CryptUtils.isEncrypted(user.getUQuestion3())) {
							user.setUQuestion3Enc(encrypt(user.getUQuestion3()));
							user.setUQuestion3(null);
						}
	
						if (StringUtils.isNotEmpty(user.getUAnswer1()) && !CryptUtils.isEncrypted(user.getUAnswer1())) {
							user.setUAnswer1Enc(encrypt(user.getUAnswer1()));
							user.setUAnswer1(null);
						}
	
						if (StringUtils.isNotEmpty(user.getUAnswer2()) && !CryptUtils.isEncrypted(user.getUAnswer2())) {
							user.setUAnswer2Enc(encrypt(user.getUAnswer2()));
							user.setUAnswer2(null);
						}
	
						if (StringUtils.isNotEmpty(user.getUAnswer3()) && !CryptUtils.isEncrypted(user.getUAnswer3())) {
							user.setUAnswer3Enc(encrypt(user.getUAnswer3()));
							user.setUAnswer3(null);
						}
	
		                HibernateUtil.getDAOFactory().getUsersDAO().update(user);
	
						encryptedCount++;
					}
				}
	
				if (encryptedCount > 0) {
					Log.log.info(String.format("Encrypted data for %d user(s)", encryptedCount));
				}

			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
	}

	@SuppressWarnings("deprecation")
	private static boolean isUserDataNeedEncryption(Users user) {
		return (StringUtils.isNotEmpty(user.getUQuestion1()) && !CryptUtils.isEncrypted(user.getUQuestion1()))
				|| (StringUtils.isNotEmpty(user.getUQuestion2()) && !CryptUtils.isEncrypted(user.getUQuestion2()))
				|| (StringUtils.isNotEmpty(user.getUQuestion3()) && !CryptUtils.isEncrypted(user.getUQuestion3()))
				|| (StringUtils.isNotEmpty(user.getUAnswer1()) && !CryptUtils.isEncrypted(user.getUAnswer1()))
				|| (StringUtils.isNotEmpty(user.getUAnswer2()) && !CryptUtils.isEncrypted(user.getUAnswer2()))
				|| (StringUtils.isNotEmpty(user.getUAnswer3()) && !CryptUtils.isEncrypted(user.getUAnswer3()));
	}

    private static void hideProtectedData(UsersVO result) {
		if (StringUtils.isNotEmpty(result.getUQuestion1())) {
			result.setUQuestion1(VO.STARS);
		}

		if (StringUtils.isNotEmpty(result.getUQuestion2())) {
			result.setUQuestion2(VO.STARS);
		}

		if (StringUtils.isNotEmpty(result.getUQuestion3())) {
			result.setUQuestion3(VO.STARS);
		}

		if (StringUtils.isNotEmpty(result.getUAnswer1())) {
			result.setUAnswer1(VO.STARS);
		}

		if (StringUtils.isNotEmpty(result.getUAnswer2())) {
			result.setUAnswer2(VO.STARS);
		}

		if (StringUtils.isNotEmpty(result.getUAnswer3())) {
			result.setUAnswer3(VO.STARS);
		}
    }
    
    public static boolean isNoOrgAccessibleByOrg(String orgId)
    {
        boolean hasNoOrgAccess = false;                
        Set<OrgsVO> aggrOrgsInHierarchy = new HashSet<OrgsVO>();
        
        OrgsVO orgsVO = getOrg(orgId, null);
        
        if (orgsVO != null)
        {
            aggrOrgsInHierarchy.add(orgsVO);
            
            hasNoOrgAccess = orgsVO.getUHasNoOrgAccess().booleanValue();
        }
        
        if (!hasNoOrgAccess)
        {
            Set<OrgsVO> orgsInHierarchy = UserUtils.getAllOrgsInHierarchy(orgsVO.getSys_id(), orgsVO.getUName());
            aggrOrgsInHierarchy.addAll(orgsInHierarchy);
            
            for (OrgsVO userOrgsVO : aggrOrgsInHierarchy)
            {
                hasNoOrgAccess = userOrgsVO.getUHasNoOrgAccess().booleanValue();
                
                if (hasNoOrgAccess)
                {
                    break;
                }
            }
        }
        
        return hasNoOrgAccess;
    }
    
    public static boolean isOrgAccessibleByOrgName(String orgName, String username, boolean unrestrictedAdminAccess, boolean unrestrictedSystemAccess)
    {
        boolean isOrgAccessible = false;
        
        OrgsVO orgsVO = getOrg(null, orgName);
        
        if (orgsVO != null)
        {
            isOrgAccessible = isOrgAccessible(orgsVO.getSys_id(), username, false, false);
        }
        
        return isOrgAccessible;
    }
    
    public static boolean isSuperUser(String username)
    {
        if (StringUtils.isNotBlank(username))
        {
            if (username.equalsIgnoreCase(SYSTEM) || username.equalsIgnoreCase(RESOLVE_MAINT))
            {
                return true;
            }
            
            return hasSuperUserRole(username);
        }
        
        return false;
    }
    
    public static boolean hasSuperUserRole(String username)
    {
        /*
         * For time being treat users with admin role as super user.
         * Once a separate super user role is created replace the 
         * below method to check whether user has super user role.
         */
        
        Set<String> userRoles = getUserRoles(username);
        
        if (userRoles.contains(ADMIN))
        {
            return true;
        }
        
        return false;
    }

	public static boolean isOrgAccessible2(String orgId, String username)
    {
        if (StringUtils.isBlank(username))
        {
            throw new RuntimeException("Passed invalid username.");
        }
        
        if (StringUtils.isNotBlank(orgId))
        {
            return isOrgAccessible(orgId, username, false, false);
        }
        else
        {
            return isNoOrgAccessible(username);
        }
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map<String, Object> getUsersV2(QueryDTO queryDTO, String username) 
    {
        Map<String, Object> resultMap = new HashMap<>();
        List<UserDTO> result = new ArrayList<>();
        
        String sql = "select " + 
                        "       su.user_name UserID, " + 
                        "       sur.name RoleName, " + 
                        "       grps.u_name GroupName, " + 
                        "       grpssur.name GroupRoleName, su.first_name, su.last_name, su.phone, su.title, su.mobile_phone, su.email, su.last_login, su.locked_out, su.last_login_device, su.sys_id, " +
                        "su.sys_updated_on, su.sys_updated_by, su.sys_created_on, su.sys_created_by " + 
                        "from sys_user su " + 
                        "left join user_role_rel urr on su.sys_id = urr.u_user_sys_id " + 
                        "left join sys_user_role sur on urr.u_role_sys_id = sur.sys_id " + 
                        "left join user_group_rel ugr on su.sys_id = ugr.u_user_sys_id " + 
                        "left join groups grps on ugr.u_group_sys_id = grps.sys_id " + 
                        "left join group_role_rel grr on grps.sys_id = grr.u_group_sys_id " + 
                        "left join sys_user_role grpssur on grr.u_role_sys_id = grpssur.sys_id ";
                        
        
        List<Object> users = null;
        queryDTO.setPrefixTableAlias(null);
        
        if (StringUtils.isNotBlank (queryDTO.getFilterWhereClause()))
        {
            String where = transformColumns(queryDTO.getFilterWhereClause());
            sql = sql + (" where (" + where + ") ");
        }
        
        if(StringUtils.isNotEmpty(username) && !username.equalsIgnoreCase("resolve.maint"))
        {
            if (sql.contains(" where "))
            {
                sql = sql + (" and (su.user_name not in ('system')) ");
            }
            else
            {
                sql = sql + (" where (su.user_name not in ('system')) ");
            }
        }
        
        if (StringUtils.isNotBlank(queryDTO.getOrderBy()))
        {
            sql = sql + " order by " + transformColumns(queryDTO.getOrderBy());
        }
        else if (StringUtils.isNotBlank(queryDTO.getSort()))
        {
            String sort = transformColumns(queryDTO.getSort());
            queryDTO.setSort(sort);
            
            String sortOrder = "";
            for ( QuerySort querySort : queryDTO.getSortItems())
            {
                sortOrder = sortOrder + querySort.getProperty() + " " + querySort.getDirection() + ", ";
            }
            
            if (StringUtils.isNotBlank(sortOrder))
            {
                sortOrder = sortOrder.substring(0, sortOrder.length() - 2);
                sql = sql + " order by " + sortOrder;
            }
        }
        else    
        {
            sql = sql + " order by su.sys_updated_by";
        }
        
        //queryDTO.getOrderBy()
        try
        {
         
        	String sqlFinal = sql;
        	
          HibernateProxy.setCurrentUser("admin");
        	users = (List<Object>) HibernateProxy.execute(() -> {
        		   Query query = HibernateUtil.createSQLQuery(sqlFinal);
                   
                   for (QueryParameter param : queryDTO.getWhereParameters())
                   {
                       if (sqlFinal.contains(param.getName()))
                       {
                           query.setParameter(param.getName(), param.getValue());
                       }
                   }
                   
                   return query.list();
                
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error:" , e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        Map<String, UserDTO> userDTOMap = new LinkedHashMap<>(); // LinkedMap to maintain the returned order
        Map<String,Set<String>> userGroupsMap = new HashMap<>();
        Map<String,Set<String>> userRolesMap = new HashMap<>();
        
        if (users != null && users.size() > 0)
        {
            for (Object obj : users)
            {
                Object[] elems = (Object[])obj;
                
                if (!userDTOMap.containsKey(elems[0].toString()))
                {
                    userDTOMap.put(elems[0].toString(), convertUsersVO(elems));
                }
                
                if (userRolesMap.containsKey(elems[0].toString()))
                {
                    if (elems[1] != null) // User role
                    {
                        userRolesMap.get(elems[0].toString()).add(elems[1].toString());
                    }
                    if (elems[3] != null) // User roles from group
                    {
                        userRolesMap.get(elems[0].toString()).add(elems[3].toString());
                    }
                }
                else
                {
                    Set<String> roles = new HashSet<>();
                    userRolesMap.put(elems[0].toString(), roles);
                    if (elems[1] != null) // User Role
                    {
                        userRolesMap.get(elems[0].toString()).add(elems[1].toString());
                    }
                    if (elems[3] != null) // User roles from group
                    {
                        userRolesMap.get(elems[0].toString()).add(elems[3].toString());
                    }
                }
                
                if (userGroupsMap.containsKey(elems[0].toString()))
                {
                    if (elems[2] != null) // User Group
                    {
                        userGroupsMap.get(elems[0].toString()).add(elems[2].toString());
                    }
                }
                else
                {
                    Set<String> groups = new HashSet<>();
                    userGroupsMap.put(elems[0].toString(), groups);
                    if (elems[2] != null) // User Group
                    {
                        userGroupsMap.get(elems[0].toString()).add(elems[2].toString());
                    }
                }
            }
            
            for (String userName : userDTOMap.keySet())
            {
                UserDTO dto = userDTOMap.get(userName);
                
                if (userRolesMap.containsKey(userName))
                {
                    Set<String> userRoles = userRolesMap.get(userName);
                    if (userRoles != null && userRoles.size() > 0)
                    {
                        dto.setRoles(userRoles.toArray(new String[0]));
                    }
                }
                
                if (userGroupsMap.containsKey(userName))
                {
                    Set<String> userGroups = userGroupsMap.get(userName);
                    if (userGroups != null && userGroups.size() > 0)
                    {
                        dto.setGroups(userGroups.toArray(new String[0]));
                    }
                }
                
                result.add(dto);
            }
        }

        
        int page = queryDTO.getPage();
        int limit = queryDTO.getLimit();
        
        if (limit != -1)
        {
            int to = (page * limit) - 1;
            int from = (page - 1) * limit;
            
            if (result.size() <= to)
            {
                to = result.size();
            }
            
            resultMap.put("USERS", result.subList(from, to));
        }
        else
        {
            resultMap.put("USERS", result);
        }
        
        resultMap.put("COUNT", result.size());
        return resultMap;
    }
    
    private static UserDTO convertUsersVO(Object[] elements)
    {
        UserDTO vo = new UserDTO();
        
        /*
         * su.first_name, 4
         * su.last_name, 5
         * su.phone, 6
         * su.title, 7
         * su.mobile_phone, 8
         * su.email, 9
         * su.last_login, 10
         * su.locked_out, 11
         * su.last_login_device 12
         * su.sys_id 13
         * su.sys_updated_on, 14
         * su.sys_updated_by, 15
         * su.sys_created_on, 16
         * su.sys_created_by 17
         */
        String fullName = null;
        
        fullName = elements[4] != null ? elements[4].toString() : "" + (elements[5] != null ? elements[5].toString() : "");
        vo.setFullName(fullName);
        vo.setUUserName(elements[0] != null ? elements[0].toString() : null);
        vo.setUFirstName(elements[4] != null ? elements[4].toString() : null);
        vo.setULastName(elements[5] != null ? elements[5].toString() : null);
        vo.setUPhone(elements[6] != null ? elements[6].toString() : null);
        vo.setUTitle(elements[7] != null ? elements[7].toString() : null);
        vo.setUMobilePhone(elements[8] != null ? elements[8].toString() : null);
        vo.setUEmail(elements[9] != null ? elements[9].toString() : null);
//        vo.setULastLogin(elements[10] != null ? (Date)elements[10] : null);
        
        Boolean locked = null;
        if (elements[11] != null)
        {
            if (elements[11] instanceof BigDecimal)
                locked = (((BigDecimal)elements[11]).intValue() == 0 ? false : true);
            else
                locked = (boolean)elements[11];
        }
        
        vo.setULockedOut(locked);
        vo.setULastLoginDevice(elements[12] != null ? elements[12].toString() : null);
        vo.setSys_id(elements[13] != null ? elements[13].toString() : null);
        vo.setSysUpdatedOn(elements[14] != null ? (Date)elements[14] : null);
        vo.setSysUpdatedBy(elements[15] != null ? elements[15].toString() : null);
        vo.setSysCreatedOn(elements[16] != null ? (Date)elements[16] : null);
        vo.setSysCreatedBy(elements[17] != null ? elements[17].toString() : null);
        vo.setId(elements[13] != null ? elements[13].toString() : null);
        
        return vo;
    }
    
    private static String transformColumns(String whereCaluse)
    {
        if (StringUtils.isNotBlank(whereCaluse))
        {
            whereCaluse = whereCaluse.replaceFirst("UUserName", "su.user_name");
            whereCaluse = whereCaluse.replaceFirst("UFirstName", "su.first_name");
            whereCaluse = whereCaluse.replaceFirst("ULastName", "su.last_name");
            whereCaluse = whereCaluse.replaceFirst("UPhone", "su.phone");
            whereCaluse = whereCaluse.replaceFirst("UTitle", "su.title");
            whereCaluse = whereCaluse.replaceFirst("UMobilePhone", "su.mobile_phone");
            whereCaluse = whereCaluse.replaceFirst("UEmail", "su.email");
            whereCaluse = whereCaluse.replaceFirst("ULastLogin", "su.last_login");
            whereCaluse = whereCaluse.replaceFirst("ULockedOut", "su.locked_out");
            whereCaluse = whereCaluse.replaceFirst("ULastLoginDevice", "su.last_login_device");
            whereCaluse = whereCaluse.replaceFirst("sys_id", "su.sys_id");
            whereCaluse = whereCaluse.replaceFirst("sysUpdatedOn", "su.sys_updated_on");
            whereCaluse = whereCaluse.replaceFirst("sysUpdatedBy", "su.sys_updated_by");
            whereCaluse = whereCaluse.replaceFirst("sysCreatedOn", "su.sys_created_on");
            whereCaluse = whereCaluse.replaceFirst("sysCreatedBy", "su.sys_created_by");
        }
        
        return whereCaluse;
    }

    public static String saveUserSettings(String settingType, String settingValue, String userName) throws Exception {
	Users model = getUserPrivate(null, userName);
	if (settingType.equalsIgnoreCase(Constants.ANALYTIC_SETTINGS)) {
	    model.setAnalyticSettings(settingValue);
	} else if (settingType.equalsIgnoreCase(Constants.REFRESH_RATE_SETTINGS)) {
	    model.setRefreshRate(Integer.valueOf(settingValue));
	} else if (settingType.equalsIgnoreCase(Constants.COLOR_THEME_SETTINGS)) {
	    EnumSet<ColorTheme> colorThemes = EnumSet.allOf(ColorTheme.class);
	    
	    try {
		ColorTheme colorTheme = ColorTheme.valueOf(settingValue);
		if (colorThemes.contains(colorTheme)) {
		    model.setColorTheme(colorTheme);
		}
	    } catch (IllegalArgumentException e) {
		throw new ColorThemeException();
	    }

	}
	
	model = SaveUtil.saveUser(model, userName);
	
	return settingValue;
    }
    
    public static int getNamedUserCount() {
    	Integer result = VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        try
        {
        	StringBuilder sb = new StringBuilder();
        	sb.append("select count(user.sys_id) from Users user where user.UUserName not in ('")
        	.append(ADMIN).append("', '").append(MetricUtil.REMOTE_AUTO_DEPLOY_USERNAME).append("', '")
        	.append(SYSTEM).append("', '").append(PUBLIC).append("', '").append(USER)
        	.append("') and (user.sysIsDeleted is null or user.sysIsDeleted = false)");
        	
        	
            Long longResult = (Long) HibernateProxy.execute(()-> {
            	return HibernateUtil.createQuery(sb.toString()).uniqueResult();
            });
            result = longResult != null ? Integer.valueOf(longResult.intValue()) : VO.NON_NEGATIVE_INTEGER_DEFAULT;
            
        } catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        
        return result;
    }
    
    private static void v2LicenseUserEnforcement(License effectiveLic) throws Exception {
	    License currLic = effectiveLic != null ? effectiveLic : MainBase.main.getLicenseService().getCurrentLicense();
	    
	    if (currLic == null) {
	    	Log.alert(ERR.E10026.getCode(), ERR.E10026.getMessage() + " Shutting Down!!!", 
	    			  Constants.ALERT_TYPE_LICENSE);
			Log.log.fatal(ERR.E10026.getMessage() + " Shutting Down!!!");
			System.exit(1);
		}
	    
	    // V2 license max # of users enforcement
		
		if (currLic.isV2()) {
			if ((currLic.getEndUserCount() > 0) &&
			    (currLic.getEndUserCount() <= getNamedUserCount())) {
				Log.alert(ERR.E10022.getCode(), ERR.E10022.getMessage(), Constants.ALERT_TYPE_LICENSE);
				throw new Exception(ERR.E10022.getMessage());
			}
			
			// V2 expired license NON-PROD environments/instance types enforcement
			
			if (currLic.isExpired() && 
				!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(currLic.getEnv()) &&
				!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(
																		MainBase.main.configGeneral.getEnv())) {
				Log.alert(ERR.E10023.getCode(), ERR.E10023.getMessage(), Constants.ALERT_TYPE_LICENSE);
				throw new Exception(ERR.E10023.getMessage());
			}
			
			// V2 license HA entitlement violation check
			
			if (!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(currLic.getEnv()) &&
				!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(
																		MainBase.main.configGeneral.getEnv()) &&
				MainBase.main.getLicenseService().getIsHAViolated()) {
				Log.alert(ERR.E10023.getCode(), ERR.E10023.getMessage(), Constants.ALERT_TYPE_LICENSE);
				throw new Exception(ERR.E10023.getMessage());
			}
		}
    }
}
