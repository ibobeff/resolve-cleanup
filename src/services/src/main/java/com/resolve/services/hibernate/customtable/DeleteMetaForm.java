/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaControl;
import com.resolve.persistence.model.MetaControlItem;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaFormAction;
import com.resolve.persistence.model.MetaFormTab;
import com.resolve.persistence.model.MetaFormTabField;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.MetaSource;
import com.resolve.persistence.model.MetaxFieldDependency;
import com.resolve.persistence.model.MetaxFormViewPanel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.customform.CustomFormIndexAPI;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class DeleteMetaForm
{
    private Set<String> customTablesToBeDeleted = new HashSet<String>();
    
    public Set<String> deleteViewsForTable(String customTableName) throws Throwable
    {
        if (StringUtils.isEmpty(customTableName))
        {
            return customTablesToBeDeleted;
        }

        List<String> listIds = getFormViewIdsToDelete(customTableName);
        delete(listIds, "admin");
        
        return customTablesToBeDeleted;
    }// deleteViewsForTable

    public void delete(List<String> ids, String username) throws Throwable
    {
        if (ids != null && ids.size() > 0)
        {
            for (String id : ids)
            {
                delete(id, username);
            }// end of for
            CustomFormIndexAPI.deleteCustomForms(ids, username);
        }// end of if
    }// delete
    
    public void deleteByName(String viewName, String username) throws Throwable
    {
        if(StringUtils.isNotBlank(viewName))
        {
            MetaFormView example = new MetaFormView();
            example.setUViewName(viewName);
            
            MetaFormView  result = CustomFormUtil.findMetaFormView(null, viewName, username);
            if(result != null)
            {
                delete(result.getSys_id(), username);
            }
        }
    }

    private void delete(String id, String username) throws Throwable
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	            MetaFormView metaFormView = HibernateUtil.getDAOFactory().getMetaFormViewDAO().findById(id);
	            if (metaFormView != null)
	            {
	                // delete access rights
	                deleteMetaAccessRights(metaFormView.getMetaAccessRights());
	                
	                //delete panels
	                deleteMetaxFormViewPanel(metaFormView.getMetaxFormViewPanels());
	
	                // delete tabs
	                deleteMetaFormTab(metaFormView.getMetaFormTabs());
	
	                // delete sources
	                deleteMetaSources(metaFormView.getMetaSources());
	
	                // delete metacontrol
	                deleteMetaControl(metaFormView.getMetaControl());
	
	                // delete the view
	                HibernateUtil.getDAOFactory().getMetaFormViewDAO().delete(metaFormView);
	            }// end of if

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
//        System.out.println("------------- HibernateUtil.getTransactionCount() -->" + HibernateUtil.getTransactionCount());
//        
//        if(customTablesToBeDeleted.size() > 0)
//        {
//            for(String customTable : customTablesToBeDeleted)
//            {
//                try
//                {
//                    new DeleteCustomTable(customTable, null, "admin").delete();
//                }
//                catch(Throwable t)
//                {
//                    Log.log.error("error in deleting the table:" + customTable, t);
//                }
//            }
//        }

    }// delete

    // ///////////////
    // private methods
    // ///////////////

    public static void deleteMetaAccessRights(MetaAccessRights metaAccessRights)
    {
        if (metaAccessRights != null)
        {
            HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().delete(metaAccessRights);
        }

    }// deleteMetaAccessRights
    
    private void deleteMetaxFormViewPanel(Collection<MetaxFormViewPanel> metaxFormViewPanels)
    {
        if (metaxFormViewPanels != null && metaxFormViewPanels.size() > 0)
        {
            for (MetaxFormViewPanel metaxFormViewPanel : metaxFormViewPanels)
            {
                // delete form tab fields
                deleteMetaFormTab(metaxFormViewPanel.getMetaFormTabs());

                // delete tab
                HibernateUtil.getDAOFactory().getMetaxFormViewPanelDAO().delete(metaxFormViewPanel);

            }// end of for
        }// end of if
        
    }

    private void deleteMetaFormTab(Collection<MetaFormTab> metaFormTabs)
    {
        if (metaFormTabs != null && metaFormTabs.size() > 0)
        {
            for (MetaFormTab metaFormTab : metaFormTabs)
            {
                // delete form tab fields
                deleteMetaFormTabField(metaFormTab.getMetaFormTabFields());
                
                //delete dependencies
                deleteDependencies(metaFormTab.getMetaxFieldDependencys());

                // delete tab
                HibernateUtil.getDAOFactory().getMetaFormTabDAO().delete(metaFormTab);

            }// end of for
        }// end of if

    }// deleteMetaFormTab

    private void deleteMetaSources(Collection<MetaSource> metaSources)
    {
        if (metaSources != null && metaSources.size() > 0)
        {
            for (MetaSource metaSource : metaSources)
            {
                deleteMetaSource(metaSource);
                
            }// end of for loop
        }// end of if
    }// deleteMetaSource

    private void deleteMetaSource(MetaSource metaSource)
    {
        if (metaSource != null)
        {
            // delete props
            deleteMetaFieldProperties(metaSource.getMetaFieldProperties(), this.customTablesToBeDeleted);

            // delete
            HibernateUtil.getDAOFactory().getMetaSourceDAO().delete(metaSource);
        }// end of if
    }// deleteMetaSource

    private void deleteMetaControl(MetaControl metaControl)
    {
        if (metaControl != null)
        {
            // delete control items
            deleteMetaControlItems(metaControl.getMetaControlItems());

            // delete control
            HibernateUtil.getDAOFactory().getMetaControlDAO().delete(metaControl);

        }
    }// deleteMetaControl

    private void deleteMetaFormTabField(Collection<MetaFormTabField> metaFormTabFields)
    {
        if (metaFormTabFields != null && metaFormTabFields.size() > 0)
        {
            for (MetaFormTabField metaFormTabField : metaFormTabFields)
            {
                // delete sources
                deleteMetaSource(metaFormTabField.getMetaSource());
                
                // delete metasytle
//                deleteMetaStyle(metaFormTabField.getMetaStyle());
                
                //delete meta properties and its dependencies
                deleteMetaFieldProperties(metaFormTabField.getMetaFieldFormProperties(), this.customTablesToBeDeleted);

                // delete tab field
                HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().delete(metaFormTabField);

            }// end of for loop
        }// end of if

    }// deleteMetaFormTabField

//    private void deleteMetaStyle(MetaStyle metaStyle)
//    {
//        if (metaStyle != null)
//        {
//            HibernateUtil.getDAOFactory().getMetaStyleDAO().delete(metaStyle);
//        }
//
//    }// deleteMetaStyle

//    private void deleteMetaFieldProperties(MetaFieldProperties metaFieldProperties)
//    {
//        if (metaFieldProperties != null)
//        {
//            deleteDependencies(metaFieldProperties.getMetaxFieldDependencys());
//            
//            HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().delete(metaFieldProperties);
//        }
//
//    }// deleteMetaFieldProperties

    private void deleteMetaControlItems(Collection<MetaControlItem> metaControlItems)
    {
        if (metaControlItems != null && metaControlItems.size() > 0)
        {
            for (MetaControlItem metaControlItem : metaControlItems)
            {
                deleteDependencies(metaControlItem.getMetaxFieldDependencys());
                
                deleteMetaFormAction(metaControlItem.getMetaFormActions());

                HibernateUtil.getDAOFactory().getMetaControlItemDAO().delete(metaControlItem);
            }
        }// end of if
    }// deleteMetaControlItems

    private void deleteMetaFormAction(Collection<MetaFormAction> metaFormActions)
    {
        if (metaFormActions != null && metaFormActions.size() > 0)
        {
            for (MetaFormAction metaFormAction : metaFormActions)
            {
                HibernateUtil.getDAOFactory().getMetaFormActionDAO().delete(metaFormAction);
            }
        }

    }// deleteMetaFormAction
    
    private void deleteDependencies(Collection<MetaxFieldDependency> dependencies)
    {
        if(dependencies != null && dependencies.size() > 0)
        {
            for(MetaxFieldDependency dependency : dependencies)
            {
                HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().delete(dependency);
            }  
        }
    }

    private List<String> getFormViewIdsToDelete(String customTableName)
    {
        List<String> listIds = new ArrayList<String>();
        //String sql = "from MetaFormView where LOWER(UTableName) = '" + customTableName.toLowerCase().trim() + "'" ;
        String sql = "from MetaFormView where LOWER(UTableName) = :UTableName" ;
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UTableName", customTableName.trim().toLowerCase());
        
        
        try
        {
            List<? extends Object> list  = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
            if (list != null && list.size() > 0)
            {
                for (Object obj : list)
                {
                    MetaFormView view = (MetaFormView) obj;
                    listIds.add(view.getSys_id());
                }// end of for
            }// end of if

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return listIds;
    }// getFormViewIdsToDelete
    
    /////////////////////////// DELETE META FORM TAB FIELDS //////////////////////////////////////////

    
    public static void deleteMetaFieldProperties(MetaFieldProperties prop, Set<String> customTablesToBeDeleted)
    {
        if(prop != null)
        {
            if(prop.getUUIType().equalsIgnoreCase(CustomFormUIType.FileUploadField.name()) && customTablesToBeDeleted != null)
            {
                String tablename = prop.getUFileUploadTableName();
                customTablesToBeDeleted.add(tablename);
                
//                try
//                {
//                    CustomTableUtil.deleteSQLTable(tablename);
//                }
//                catch(Throwable t)
//                {
//                    Log.log.error("error deleting table " + tablename, t);
//                }
            }
            
            deleteMetaxFieldDependencys(prop.getMetaxFieldDependencys());
            
            HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().delete(prop);
        }
    }
    
    private static void deleteMetaxFieldDependencys(Collection<MetaxFieldDependency> metaxFieldDependencys)
    {
        if(metaxFieldDependencys != null && metaxFieldDependencys.size() > 0)
        {
            for(MetaxFieldDependency dependency : metaxFieldDependencys)
            {
                HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().delete(dependency);
            }
        }
    }
    
    


}
