/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.beanutils.PropertyUtils;

import com.bmc.thirdparty.org.apache.commons.collections.CollectionUtils;
import com.resolve.persistence.model.ResolveWikiLookup;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveWikiLookupVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WikiLookupUtil
{
	private static final String U_ORDER = "UOrder";
	
    public static List<ResolveWikiLookupVO> getResolveWikiLookup(QueryDTO query, String username)
    {
        List<ResolveWikiLookupVO> result = new ArrayList<ResolveWikiLookupVO>();
        
        //manipualate the qry types for lookup 
        manipuateQueryDTOForWikiLookupGrid(query);
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveWikiLookup instance = new ResolveWikiLookup();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ResolveWikiLookup instance = (ResolveWikiLookup) o;
                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    
    public static ResolveWikiLookupVO findResolveWikiLookupById(String sys_id, String username)
    {
        ResolveWikiLookup model = null;
        ResolveWikiLookupVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findResolveWikiLookupModelById(sys_id, null, username);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = convertModelToVO(model, null);
        }
        
        return result;
    }
    
    public static ResolveWikiLookupVO saveResolveWikiLookup(ResolveWikiLookupVO vo, String username) throws Exception
    {
        if(vo != null)
        {
            ResolveWikiLookup model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                model = findResolveWikiLookupModelById(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("ResolveWikiLookup with sysId " + vo.getSys_id() + " does not exist.");
                }
                
                //check if the title is same or not, if not, than validate if the title is unique
                //this will throw exception if there are more than 1 recs with the same title
                ResolveWikiLookup duplicate = findResolveWikiLookupModelById(null, vo.getURegex(), username);
                if(duplicate != null && !duplicate.getSys_id().equalsIgnoreCase(vo.getSys_id()))
                {
                    throw new Exception("ResolveWikiLookup with regex " + vo.getURegex() + " already exist");
                }                
                
            }
            else
            {
                model = findResolveWikiLookupModelById(null, vo.getURegex(), username);
                if(model != null)
                {
                    throw new Exception("ResolveWikiLookup with regex " + vo.getURegex() + " already exist");
                }
                
                model = new ResolveWikiLookup();
            }
            model.applyVOToModel(vo);
            model.setChecksum(calculateChecksum(vo));
            model = SaveUtil.saveResolveWikiLookup(model, username);
            vo = convertModelToVO(model, null);
        }
        
        return vo;
    }

    public static void deleteResolveWikiLookupByIds(String[] sysIds, String username) throws Exception
    {
        if(sysIds != null)
        {
            for(String sysId : sysIds)
            {
                deleteResolveWikiLookupById(sysId, username);
            }
        }
    }
    
    //private apis
    private static ResolveWikiLookupVO convertModelToVO(ResolveWikiLookup model, Map<String, String> mapOfOrganization)
    {
        ResolveWikiLookupVO vo = null;
        
        if(model != null)
        {
            if(mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }
            
            vo = model.doGetVO();
            if(StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }              
        }
        
        return vo;
    }
    
    private static ResolveWikiLookup findResolveWikiLookupModelById(String sys_id, String regex, String username)
    {
        ResolveWikiLookup result = null;
        
        if(StringUtils.isNotEmpty(sys_id))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	result = (ResolveWikiLookup) HibernateProxy.execute(() -> {
                
            		return HibernateUtil.getDAOFactory().getResolveWikiLookupDAO().findById(sys_id);
                
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(regex))
        {
            
            //String sql = "from ResolveWikiLookup where LOWER(URegex) = '" + regex.toLowerCase().trim() + "' ";
            String sql = "from ResolveWikiLookup where LOWER(URegex) = :URegex ";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("URegex", regex.toLowerCase().trim());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if (list != null && list.size() > 0)
                {
                    result = (ResolveWikiLookup) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }            
        }
        
        return result;
    }
    
    private static void deleteResolveWikiLookupById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	            
	            ResolveWikiLookup model = HibernateUtil.getDAOFactory().getResolveWikiLookupDAO().findById(sysId);
	            if(model != null)
	            {
	                HibernateUtil.getDAOFactory().getResolveWikiLookupDAO().delete(model);
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e); 
        }
    }
    
    private static void manipuateQueryDTOForWikiLookupGrid(QueryDTO query) {        
        
    	List<QueryFilter> filters = query.getFilters();
    	
        if(filters != null && filters.size() > 0)
        {
        	QueryFilter uOrderFilter = filters.stream().filter(f -> U_ORDER.equalsIgnoreCase(f.getField())).
        							   findAny().orElse(null);
        	
        	if (uOrderFilter != null && !QueryFilter.INT_TYPE.equals(uOrderFilter.getType())) {
        		uOrderFilter.setType(QueryFilter.INT_TYPE);
        	}
        }
    }
    
    public static Map<String, Object> getWikiLookupComponents(String sysId, String username) {
        Map<String, Object> result = new HashMap<>();
        ResolveWikiLookup lookup = findResolveWikiLookupModelById(sysId, null, username);
        if (lookup != null) {
            String wikiFullName = lookup.getUWiki();
            if (StringUtils.isNotBlank(wikiFullName)) {
                String wikiSysId = WikiUtils.getWikidocSysId(wikiFullName);
                if (StringUtils.isNotBlank(wikiSysId)) {
                    List<String> wikis = new ArrayList<>();
                    wikis.add(wikiFullName + ImpexEnum.COLON.getValue() + wikiSysId);
                    result.put(ImpexEnum.WIKI.getValue(), wikis);
                } else {
                    Log.log.error(String.format("No wiki found for the wiki lookup with regex '%s'", lookup.getURegex()));
                }
            }
        }
        return result;
    }
    /**
     * API to be invoked from update/upgrade script only!
     */
    public static void updateAllWikiLookupsForChecksum()
    {
        try {
            UpdateChecksumUtil.updateComponentChecksum("ResolveWikiLookup", WikiLookupUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
            
    public static Integer calculateChecksum(ResolveWikiLookupVO wikiLookupVo)
    {
        return Objects.hash(wikiLookupVo.getUOrder(), wikiLookupVo.getURegex(), wikiLookupVo.getUWiki());
    }
    
    public static Integer calculateChecksum(ResolveWikiLookup wikiLookup)
    {
        return calculateChecksum(wikiLookup.doGetVO());
    }
    
    public static Integer getLookupChecksum(String regex, String username)
    {
        Integer checksum = null;
        ResolveWikiLookup lookup = findResolveWikiLookupModelById(null, regex, username);
        if (lookup != null)
        {
            checksum = lookup.getChecksum();
        }
        return checksum;
    }
}
