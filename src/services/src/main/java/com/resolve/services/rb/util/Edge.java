/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import com.resolve.util.StringUtils;

public class Edge extends Common implements Comparable<Edge>
{
    private int source;
    private int target;
    private String edgeColor; //green or red
    private String style;
    
    public Edge()
    {
        super();
    }
    
    public Edge(int id, int source, int target, String edgeLabel, String description, String edgeColor)
    {
        super(id, edgeLabel, description);
        this.source = source;
        this.target = target;
        this.edgeColor = edgeColor;
        this.geometry = new Geometry();
        cell = new Cell(getStyle(), 1, source, target, 1, -1, geometry);
    }

    public Edge(int id, int source, int target, String edgeLabel, String description, String edgeColor, String style, Geometry geometry)
    {
        super(id, edgeLabel, description);
        this.source = source;
        this.target = target;
        this.edgeColor = edgeColor;
        this.style = style;
        this.geometry = geometry;
        cell = new Cell(getStyle(), 1, source, target, 1, -1, geometry);
    }

    public int getSource()
    {
        return source;
    }

    public void setSource(int source)
    {
        this.source = source;
        this.cell.setSource(source);
    }

    public int getTarget()
    {
        return target;
    }

    public void setTarget(int target)
    {
        this.target = target;
        this.cell.setTarget(target);
    }

    @Override
    String getStyle()
    {
        if(StringUtils.isNotBlank(this.style))
        {
            return style;
        }
        else if(StringUtils.isBlank(edgeColor))
        {
            return "straight;noEdgeStyle=1;noEdgeStyle=1";
        }
        else
        {
            return "straight;noEdgeStyle=1;noEdgeStyle=1;strokeColor=" + edgeColor;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        //this protection is for model's integrity, like edges going nowwhere.
        //the situation can happen when an edge is created for the future but subsequently 
        //other route take priority based on other logic. 
        if(source > 0 && target > 0)
        {
            sb.append("    <Edge ").append("label=\"").append(getNodeLabel()).append("\" description=\"").append(getDescription()).append("\" ").append("id=\"").append(getId()).append("\">\n");
            sb.append(cell.toString()).append("\n");
            sb.append("    </Edge>");
        }
        return sb.toString();
    }

    public int compareTo(Edge o)
    {
        return this.getId().compareTo(o.getId());
    }
}
