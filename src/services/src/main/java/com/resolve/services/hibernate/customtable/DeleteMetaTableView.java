/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class DeleteMetaTableView
{
    private MetaTableView metaTableView = null;
    private Set<String> sysIds = null;
    private String username = null;
    private Set<String> userRoles = new HashSet<String>();
    private Set<String> viewWithErrors = new HashSet<String>();
    
    //flag to delete the Default view. Is used when deleting the custom table
    private boolean deleteDefaultView = false;
    
    public DeleteMetaTableView(Set<String> sysIds, String username)
    {
        this.sysIds = sysIds;        
        this.username = username;
    }
    
    public DeleteMetaTableView(MetaTableView metaTableView, String username)
    {
        this.metaTableView = metaTableView;
        this.username = username;
    }
    
    public boolean isDeleteDefaultView()
    {
        return deleteDefaultView;
    }

    public void setDeleteDefaultView(boolean deleteDefaultView)
    {
        this.deleteDefaultView = deleteDefaultView;
    }

    public boolean delete() throws Exception
    {
        boolean success = true;
        
        //get user roles 
        userRoles.addAll(UserUtils.getUserRoles(username));
        
        if(this.sysIds != null && this.sysIds.size() > 0)
        {
            for(String sysId : sysIds)
            {
                delete(sysId);
            }
        }
        else if(metaTableView != null)
        {
            delete(metaTableView);
        }
        
        if(viewWithErrors.size() > 0)
        {
            throw new Exception("Error Deleting Following Views:" + StringUtils.convertCollectionToString(viewWithErrors.iterator(), ","));
        }
        
        return success;
    }
    
    private void delete(String sysId) 
    {
        if(StringUtils.isEmpty(sysId))
        {
            return;
        }
        
        MetaTableView metaTableView = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	metaTableView = (MetaTableView) HibernateProxy.execute(() -> {
            	MetaTableView metaTableViewResult = HibernateUtil.getDAOFactory().getMetaTableViewDAO().findById(sysId);
		        delete(metaTableViewResult);
		        
		        return metaTableViewResult;
            });

        }
        catch (Throwable t)
        {
           viewWithErrors.add(metaTableView.getUName());
           
           Log.log.error(t.getMessage(), t);
                     HibernateUtil.rethrowNestedTransaction(t);
        }
        
    }
    
    private void delete(MetaTableView metaTableView)
    {
        if(metaTableView != null)
        {
            String metaTableViewRoles = metaTableView.getMetaAccessRights().getUAdminAccess();
            boolean hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, metaTableViewRoles);
            if(hasRights)
            {
                String viewName = metaTableView.getUName();
                if("Default".equalsIgnoreCase(viewName))
                {
                    //if the flag is true, than only delete the 'Default' view. else don't allow the deletion
                    if(!this.deleteDefaultView)
                    {
                        Log.log.error("'Default' view can be deleted only when a Custom Table is deleted.");
                        viewWithErrors.add(metaTableView.getUName());
                        return;
                    }
                }
                    
                //continue the deletion
                if(metaTableView.getParentMetaTableView() != null)
                {
                    //this is self - so delete only this one
                    deleteSelf(metaTableView);
                }
                else
                {
                    //the is a shared one, so delete this and all the self ones related to this shared one
                    deleteShared(metaTableView);
                }
                
                //delete the final record
                HibernateUtil.getDAOFactory().getMetaTableViewDAO().delete(metaTableView);
            }
            else
            {
                Log.log.error("User " + username + " does not have right to delete view " + metaTableView.getUDisplayName());
                viewWithErrors.add(metaTableView.getUName());
            }
            
        }
    }
    
    
    private void deleteSelf(MetaTableView metaTableView)
    {
        deleteMetaTableViewField(metaTableView.getMetaTableViewFields());
    }
    
    private void deleteShared(MetaTableView metaTableView)
    {
        //delete the child views 
        Collection<MetaTableView> childMetaTableViews = metaTableView.getMetaTableViews();
        if(childMetaTableViews != null && childMetaTableViews.size() > 0)
        {
            for(MetaTableView childView : childMetaTableViews)
            {
                delete(childView);
            }
        }

        //delete the view fields        
        deleteMetaTableViewField(metaTableView.getMetaTableViewFields());
        
        //delete access rights
        HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().delete(metaTableView.getMetaAccessRights());
    }
    
    private void deleteMetaTableViewField(Collection<MetaTableViewField> metaTableViewFields)
    {
        if(metaTableViewFields != null)
        {
            for(MetaTableViewField tableField : metaTableViewFields)
            {
                if(tableField.getMetaFieldTableProperties() != null)
                {
                    HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().delete(tableField.getMetaFieldTableProperties());
                }
                
                if(tableField.getMetaViewField() != null)
                {
                    HibernateUtil.getDAOFactory().getMetaViewFieldDAO().delete(tableField.getMetaViewField());
                }
                
                HibernateUtil.getDAOFactory().getMetaTableViewFieldDAO().delete(tableField);
            }
        }
    }
    
    
}
