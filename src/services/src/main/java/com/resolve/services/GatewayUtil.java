/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.services;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.dom4j.Element;
import org.dom4j.Node;
import org.hibernate.Session;

import com.resolve.gateway.dto.GatewayDTO;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MInfo;
import com.resolve.rsbase.MainBase;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.DatabaseConnectionPoolVO;
import com.resolve.services.hibernate.vo.EWSAddressVO;
import com.resolve.services.hibernate.vo.EmailAddressVO;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayPropertiesVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.ResolveBlueprintVO;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.services.hibernate.vo.SSHPoolVO;
import com.resolve.services.hibernate.vo.TelnetPoolVO;
import com.resolve.services.hibernate.vo.XMPPAddressVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
/**
 * A utility class for resolve gateways.
 * 
 * @author bipul.dutta
 * 
 */
public class GatewayUtil {
	// this is a map where we store some abnormality with the
	// blueprint key (e.g., rsremote.receive.db.active) not same as the
	// sys app module (e.g., database). Normally all other existing
	// gateways are fine except database.
	public static Map<String, String> GATEWAY_EXCEPTION_MAPPING = new HashMap<String, String>();

	static Pattern gatewayActivePattern = Pattern.compile("(.+)[.]receive[.].+[.]active");

	private static String PRIMARY_PROPERTY_SUFFIX = "primary";
	private static String SECONDARY_PROPERTY_SUFFIX = "secondary";

	static {
		GATEWAY_EXCEPTION_MAPPING.put("database", "db");
		GATEWAY_EXCEPTION_MAPPING.put("remedy", "remedyx");
	}

	public static void synchronizeEmailGateway() {
		Map<String, Object> messageParams = new HashMap<String, Object>();
		messageParams.put("UPDATE_SOCIAL_POSTER", "TRUE");

		// send the message back to the requester who sent this message.
		Log.log.debug(
				"SENDING message to " + Constants.ESB_NAME_GATEWAY_TOPIC + " so Email and EWS gateway could sync up.");

		if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_GATEWAY_TOPIC, "MEmail.sendSyncRequest",
				messageParams) == false) {
			Log.log.warn("Failed to send synchronization message to " + Constants.ESB_NAME_GATEWAY_TOPIC
					+ " for Email gateway.");
		}
		if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_GATEWAY_TOPIC, "MEWS.sendSyncRequest",
				messageParams) == false) {
			Log.log.warn("Failed to send synchronization message to " + Constants.ESB_NAME_GATEWAY_TOPIC
					+ " for XMPP gateway, ignoring it because may be there is no RSRemotes.");
		}
	}

	public static void synchronizeXMPPGateway() {
		Map<String, Object> messageParams = new HashMap<String, Object>();
		messageParams.put("UPDATE_SOCIAL_POSTER", "TRUE");

		// send the message back to the requester who sent this message.
		Log.log.debug("SENDING message to " + Constants.ESB_NAME_GATEWAY_TOPIC + " so XMPP gateway could sync up.");

		if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_GATEWAY_TOPIC, "MXMPP.sendSyncRequest",
				messageParams) == false) {
			Log.log.warn("Failed to send synchronization message to " + Constants.ESB_NAME_GATEWAY_TOPIC
					+ " for XMPP gateway, ignoring it because may be there is no RSRemotes.");
		}
	}

	public static Set<String> findAllActiveGateways() {
		// LinkedHashSet is serializable
		Set<String> gateways = new TreeSet<String>();
		List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
		for (ResolveBlueprintVO blueprintVO : blueprints) {
			Properties properties = new Properties();
			try {
				properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));
			} catch (IOException e) {
				Log.log.warn(e.getMessage(), e);
			}

			// for example we want to check property like
			// rsremote.receive.email.active=true
			for (Object key : properties.keySet()) {
				String name = key.toString();
				Matcher gatewayActiveMatcher = gatewayActivePattern.matcher(name);
				if (gatewayActiveMatcher.matches() && !name.contains(".ldap.") && !name.contains(".ad.")) {
					String value = properties.getProperty(name);
					if ("true".equalsIgnoreCase(value)) {
						String rsremoteName = gatewayActiveMatcher.group(1);
						String[] parts = name.split("\\.");
						if (parts.length != 4)
							continue;
						String gatewayName = GATEWAY_EXCEPTION_MAPPING.containsKey(parts[2])
								? GATEWAY_EXCEPTION_MAPPING.get(parts[2])
								: parts[2];

						String queueProperty = rsremoteName + ".receive." + gatewayName + ".queue";
						String queue = properties.getProperty(queueProperty);
						if (!StringUtils.isBlank(queue)) {
							gateways.add(queue);
						}
					}
				}
			}
		}
		return gateways;
	}

	public static void synchronizeRoutingSchemas(List<String> gatewayQueues) throws Exception {
		Map<String, String> messageParams = new HashMap<String, String>();
		messageParams.put("RESET_ROUTING_SCHEMAS", "TRUE");

		// get all the active gateways, ignore the provided gatewayQueues
		// Set<String> activeGateways = findAllActiveGateways();
		TreeMap<String, Set<String>> activeQueuesGWNameMap = getActiveQueueListMap();

		Set<String> gateways = activeQueuesGWNameMap.keySet();

		if (gateways != null && !gateways.isEmpty()) {
			for (String gateway : gateways) {
				Set<String> activeQueues = activeQueuesGWNameMap.get(gateway);
				// we decided that we ask every gateway to synchronize to prevent
				// the case where gateway is silently deleted from schema.

				for (String gatewayQueueName : activeQueues) {
					// send the message to the gateway topic, let the default
					// processor process the method.
					Log.log.debug("SENDING message to " + gatewayQueueName.toUpperCase()
							+ "_TOPIC so the gateway could sync up routing schemas.");

					if (MainBase.esb.sendInternalMessage(gatewayQueueName.toUpperCase() + "_TOPIC", "DEFAULT",
							"synchronizeRoutingSchemas", messageParams) == false) {
						Log.log.warn("Failed to send synchronization message to " + gatewayQueueName
								+ " for the routing schemas, ignoring it because may be there is no RSRemotes.");
					}

					// send the message to worker, let the default
					// processor process the method.
					Log.log.debug("SENDING message to " + gatewayQueueName.toUpperCase()
							+ "_WORKER so the gateway could sync up routing schemas.");

					if (MainBase.esb.sendInternalMessage(gatewayQueueName.toUpperCase() + "_WORKER", "DEFAULT",
							"synchronizeRoutingSchemas", messageParams) == false) {
						Log.log.warn("Failed to send synchronization message to " + gatewayQueueName
								+ "  workers for the routing schemas, ignoring it because may be there is no RSRemotes.");
					}
				}

			}
		} else {
			Log.log.debug("No active gateway found so the gateway could sync up routing schemas.");
		}
	}

	public static TreeMap<String, Set<String>> getActiveQueueListMap() throws Exception {
		TreeMap<String, Set<String>> activeQueuesGWNameMap = new TreeMap<String, Set<String>>();

		List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
		for (ResolveBlueprintVO blueprintVO : blueprints) {
			Properties properties = new Properties();

			properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));

			// Get rsremote instance count

			if (properties.containsKey("rsremote.instance.count")) {
				int rmtInstCnt = 0;

				try {
					rmtInstCnt = Integer.parseInt(properties.getProperty("rsremote.instance.count", "0"));
				} catch (NumberFormatException nfe) {
					Log.log.warn("Blueprint property rsremote.instance.count value "
							+ properties.getProperty("rsremote.instance.count") + " is not a number!!!");
				}

				if (rmtInstCnt > 0) {
					for (int i = 1; i <= rmtInstCnt; i++) {
						if (properties.containsKey("rsremote.instance" + i + ".name")) {
							String rmtInstName = properties.getProperty("rsremote.instance" + i + ".name");

							String orgSuffix = "";

							if (properties.containsKey(rmtInstName + ".general.org")) {
								String orgNamePropVal = properties.getProperty(rmtInstName + ".general.org");

								if (StringUtils.isNotBlank(orgNamePropVal)) {
									orgSuffix = orgNamePropVal.replaceAll("[^0-9A-Za-z_]", "").toUpperCase();
								}
							}

							// Check property names like <rsremote instance name>.receive.<gateway name
							// starting with lower case alpha>.active

							for (Object key : properties.keySet()) {
								String name = key.toString();

								/*
								 * HP TO DO Need to filter out queue names for connections pools as connection
								 * pools are deployed to rsremotes and are shared resource used by gateways in
								 * that rsremote.
								 */

								if (name.toString().matches(rmtInstName + "[.]receive[.][a-z].*[.]active")
										&& !name.contains(".ldap.") && !name.contains(".ad.")
										&& !name.contains(".radius.") && !name.contains(".ssh.")
										&& !name.contains(".telnet.")) {
									String[] parts = name.split("\\.");
									if (parts.length != 4)
										continue;

									// Verify if primary/secondary is enabled

									String gatewayName = GatewayUtil.GATEWAY_EXCEPTION_MAPPING.containsKey(parts[2])
											? GatewayUtil.GATEWAY_EXCEPTION_MAPPING.get(parts[2])
											: parts[2];

									String value = properties.getProperty(name);

									if ("true".equalsIgnoreCase(value)) {
										if (!activeQueuesGWNameMap.containsKey(gatewayName)) {
											activeQueuesGWNameMap.put(gatewayName, new TreeSet<String>());
										}

										String queueProperty = parts[0] + ".receive." + gatewayName + ".queue";
										String queue = properties.getProperty(queueProperty);
										if (!StringUtils.isEmpty(queue))
											activeQueuesGWNameMap.get(gatewayName).add(queue + orgSuffix);
									}
								}
							}
						} else {
							Log.log.warn("RSRemote instance name property rsremote.instance" + i
									+ ".name is missing from blueprint!!!");
						}
					}
				}
			}
		}

		return activeQueuesGWNameMap;
	}
	
	@SuppressWarnings("unchecked")
	public static String getFilterModelVOMappedColumnName(String modelName) {
		String mappedColumn = null;
    	String fieldName = "UName";
    	Class cls = GatewayFilterVO.class;
    	
        if("EmailAddress".equals(modelName)) {
            fieldName = "UEmailAddress";
            cls = EmailAddressVO.class;
        } else if("EWSAddress".equals(modelName)) {
            fieldName = "UEWSAddress";
            cls = EWSAddressVO.class;
        } else if("DatabaseConnectionPool".equals(modelName)) {
            fieldName = "UPoolName";
            cls = DatabaseConnectionPoolVO.class;
        } else if("XMPPAddress".equals(modelName)) {
            fieldName = "UXMPPAddress";
            cls = XMPPAddressVO.class;
        } else if("TelnetPool".equals(modelName)) {
            fieldName = "USubnetMask";
            cls = TelnetPoolVO.class;
        } else if("SSHPool".equals(modelName)) {
            fieldName = "USubnetMask";
            cls = SSHPoolVO.class;
        }
        
        Method mthd = null;
        
        try {
			mthd = cls.getMethod(String.format("get%s", fieldName));
		} catch (NoSuchMethodException | SecurityException e) {
			Log.log.error(String.format("Error %sin getting method named get%s from Class %s for filter model name %s",
										(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
										fieldName, cls.getName(), modelName));
			return mappedColumn;
		}
        
        if (mthd == null) {
        	Log.log.error(String.format("No such method named get%s found in Class %s for filter model name %s",
										fieldName, cls.getName(), modelName));
        	return mappedColumn;
        }
        
        if (mthd.isAnnotationPresent(MappingAnnotation.class)) {
        	mappedColumn = ((MappingAnnotation)mthd.getAnnotation(MappingAnnotation.class)).columnName();
        } else {
        	Log.log.error(String.format("Method %s does not have expected annotation %s", mthd.getName(), 
        								MappingAnnotation.class.getName()));
        	return mappedColumn;
        }
        
        return mappedColumn;
    }
	
	@SuppressWarnings("unchecked")
    public static List<GatewayDTO> getGatewaysInfo(final List<ResolveRegistrationVO> rsrgVOs, String username, String type, 
    											   boolean activeOnly, boolean typeOnly, 
    											   boolean latestDeployedFilter)  throws Exception {
        final List<GatewayDTO> records = new CopyOnWriteArrayList<GatewayDTO>();
        final Set<String> gatewayTypes = new CopyOnWriteArraySet<String>();
        if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("GatewayUtil:getGatewaysInfo(%d,%s,%s,%b,%b,%b), UserUtils.getUserOrgHierarchyIdList " +
										"In DBPoolUsage [%s]", 
										rsrgVOs.size(), username, type, activeOnly, typeOnly, latestDeployedFilter,
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
        final Collection<String> accessibleOrgs = UserUtils.getUserOrgHierarchyIdList(username);
        if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("GatewayUtil:getGatewaysInfo(%d,%s,%s,%b,%b,%b) UserUtils.getUserOrgHierarchyIdList " +
										"Out, UserUtils.isNoOrgAccessible In DBPoolUsage [%s]", 
										rsrgVOs.size(), username, type, activeOnly, typeOnly, latestDeployedFilter,
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
    	final boolean isNoOrgAccessible = UserUtils.isNoOrgAccessible(username);
    	if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("GatewayUtil:getGatewaysInfo(%d,%s,%s,%b,%b,%b) UserUtils.isNoOrgAccessible " +
										"Out DBPoolUsage [%s]", 
										rsrgVOs.size(), username, type, activeOnly, typeOnly, latestDeployedFilter,
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
//        boolean isAdminUser = UserUtils.isAdminUser(username);
        rsrgVOs.parallelStream().forEach(rsrgVO -> {
            String org = rsrgVO.getUOrg();
            boolean isOrgPresent = StringUtils.isNotBlank(org);
            boolean isOrgAccessible = isOrgPresent ? accessibleOrgs.contains(org) : isNoOrgAccessible;
            
            if (isOrgAccessible) {
                String config = rsrgVO.getUConfig();
                
                /*
                 * Composite RSRemote Name = GUID<-NAME<-GROUP>>
                 */
                String compositeRSRemoteName = String.format("%s%s%s", rsrgVO.getUGuid(),
                        (StringUtils.isNotBlank(rsrgVO.getUName()) ? "-" + rsrgVO.getUName() : ""),
                        (StringUtils.isNotBlank(rsrgVO.getUGroup()) ? "-" + rsrgVO.getUGroup() : ""));
                try {
                    long updatedOn = rsrgVO.getSysUpdatedOn().getTime();
                    final XDoc doc = new XDoc(config);
                    long timeConsideredStale = System.currentTimeMillis() - 
                    						   (doc.getIntValue("./REGISTRATION/@INTERVAL", 5) * 60000 * 2); // Double the interval
                    String containerIp = doc.getStringValue("./ID/@LOCATION", "");
                    String containerName = doc.getStringValue("./ID/@NAME", "");
                    if (updatedOn < timeConsideredStale) {
                    	if (Log.log.isDebugEnabled()) {
                    		Log.log.debug(String.format("Registration information from DB is stale by %d minutes",
                    									(System.currentTimeMillis() - updatedOn) / 60000 ));
                    	}
                        return;
                    }
                    final List<Object> receives = doc.getNodes("./RECEIVE/*");
                    receives.parallelStream/*stream*/().forEach( o -> {
                    	try {
                        Element e = (Element) o;
                        String gwXpath = e.getUniquePath();
                        boolean isPrimary = doc.getBooleanValue(gwXpath + "/@PRIMARY", false);
                        boolean isSecondary = doc.getBooleanValue(gwXpath + "/@SECONDARY", false);
                        // Worker gateways won't be accounted for.
                        if (!isPrimary && !isSecondary) {
                            return;
                        }
                        boolean active = doc.getBooleanValue(gwXpath + "/@ACTIVE", false);
                        String gatewayType = e.getName();
                        if (gatewayType.equalsIgnoreCase("db")) {
                            gatewayType = "DATABASE";
	                        } else if (gatewayType.equalsIgnoreCase("ad") || gatewayType.equalsIgnoreCase("ldap") ||
	                        		   gatewayType.equalsIgnoreCase("radius")) {
	                            // ignore authentication gateways.
	                            return;
                        }
                        if (typeOnly) {
	                        	if (activeOnly && !active) {
	                        		return;
	                        	} 
                            gatewayTypes.add(gatewayType);
                            return;
                        }
                        /**
                         * Check wether if we need to filter on 
                         * 1. active gateway: a gateway is selected if it is active
                         * 2. gateway type: if a gateway type matches with the filter.
                         */
                        if ((active || !activeOnly) && (StringUtils.isBlank(type) || type.equals(gatewayType))) {
	                            
                            GatewayPropertiesVO gateway = new GatewayPropertiesVO(false);
                            
                            // Set Container (RSRemote) attributes org name and rsremote name
                            
                            if (isOrgPresent) {
                            	gateway.setUOrgName(org);
                            } else {
                            	gateway.setUOrgName(OrgsVO.NONE_ORG_NAME);
                            }
                            
                            gateway.setURSRemoteName(compositeRSRemoteName);
                            
                            gateway.setUActive(active);
                            gateway.setUGatewayName(doc.getStringValue(gwXpath + "/@QUEUE"));
	
                            gateway.setUPrimary(isPrimary);
                            gateway.setUSecondary(isSecondary);
	
                            gateway.setUPrimary(doc.getBooleanValue(gwXpath + "/@PRIMARY", false));
                            gateway.setUSecondary(doc.getBooleanValue(gwXpath + "/@SECONDARY", false));
	
	                        	GatewayDTO gatewayDTO = new GatewayDTO();
	                            
                            gatewayDTO.setType(gatewayType);
                            gatewayDTO.setGatewayProperties(gateway);
                            gatewayDTO.setContainerIp(containerIp);
                            gatewayDTO.setContainerName(containerName);
                            String originalPrimary = doc.getStringValue(gwXpath + "/@ORIGINALPRIMARY", null);
                            if (originalPrimary != null) {
                                gatewayDTO.setOriginalPrimary(Boolean.parseBoolean(originalPrimary));
                            }
	                            
	                            if (latestDeployedFilter) {
	                            	Pair<Date, String> pair = null;
	                            
		                            try {
		                            	pair = getLatestDeployedFilter(gatewayType, gateway.getUGatewayName(), username);
		                            	
			                            if (pair.getLeft() != null && StringUtils.isNotBlank(pair.getRight())) {
			                            	gatewayDTO.setUpdatedBy(pair.getRight());
				                            gatewayDTO.setUpdatedOn(pair.getLeft());
			                            }
		                            } catch (Throwable t) {
		                            	Log.log.warn(String.format("Error [%s]in getting latest deployed filter for gateway " +
		                            							   "type %s from queue %s by user %s",
		                            							   StringUtils.isNotBlank(t.getMessage()) ? 
		                            							   "[" + t.getMessage() + "] " : "",
		                            							   gatewayType, gateway.getUGatewayName(), username));
		                            }
	                            }
	                            
                            records.add(gatewayDTO);
                        }
                    	} catch (Throwable t) {
                        	Log.log.warn(String.format("Error [%s]in processing element [%s] from config.xml [%s]", 
     							   		 StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "",
     							   		 ((Node)o).asXML(), config));
                    	}
                    });
                } catch (Throwable t) {
                    Log.log.warn(String.format("Error [%s]in parsing rsremote config.xml [%s]",
                    						   StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "",
                    						   config));
                }
            }
        });
        if (typeOnly) {
            for(String gatewayType: gatewayTypes) {
                GatewayDTO gatewayDTO = new GatewayDTO();
                gatewayDTO.setType(gatewayType);
                records.add(gatewayDTO);
            }
        }
        
        if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("GatewayUtil:getGatewaysInfo(%d,%s,%s,%b,%b,%b) Out DBPoolUsage [%s]", 
										rsrgVOs.size(), username, type, activeOnly, typeOnly, latestDeployedFilter,
										StringUtils.mapToString(
														new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
        
	    return records;
	}

    public static String getGatewayFilterModelName(String gatewayTypeInput, boolean includePackageName) {
        String model = null;
        String gatewayType = gatewayTypeInput;
        if (gatewayType.equals("db")) {
            gatewayType = "DATABASE";
        }
        String gwType = ServiceGateway.getGatewayType(gatewayType);
        if (gwType == null || gwType.equalsIgnoreCase("none")) {
            model = HibernateUtil.getGatewayModelFilter(gatewayType);
            if (StringUtils.isNotBlank(model) && !includePackageName) {
                model = StringUtils.substringAfter(model, "com.resolve.persistence.model.");
            }
        } else {
            model = String.format(includePackageName? "com.resolve.persistence.model.%sGatewayFilter" : "%sGatewayFilter", gwType);
        }
        return model;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static Pair<Date, String> getLatestDeployedFilter(String type,   /* gateway type. Example HTTP, EWS, NETCOOL*/
            												 String queue,  /* name of gateway instance queue */
            												 String username) throws Exception {
    	Pair<Date, String> retLatestDeployedFilterDetails = Pair.of(null, null);
    	
    	if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("GatewayUtil:getLatestDeployedFilter(%s,%s,%s) In DBPoolUsage [%s]", type,  
										queue, username,
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
    	
        try {
            String model = getGatewayFilterModelName(type, true);
            
            if (StringUtils.isNotBlank(model)) {

            Class<?> modelClassName = Class.forName(model);
            
            retLatestDeployedFilterDetails = (Pair<Date, String>) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
            	Session session = HibernateUtil.getCurrentSession();
                CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
                CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(modelClassName);
                Root from = criteriaQuery.from(modelClassName);
		
                Predicate pred = criteriaBuilder.equal(from.get("UQueue"), queue);
                criteriaQuery.select(from).where(pred).orderBy(criteriaBuilder.desc(from.get("sysUpdatedOn")));
		            List<BaseModel> filters = session.createQuery(criteriaQuery).setMaxResults(1).list();
		            
		            if (CollectionUtils.isNotEmpty(filters)) {
		            	try {
		            		BaseModel filter = filters.get(0);
		            		return  Pair.of(filter.getSysUpdatedOn(), filter.getSysUpdatedBy());
		            	} catch (ClassCastException cce) {
		            		Log.log.error(String.format("Filter model %s for gateway type %s does not extends from " +
		            									"mandatory %s", modelClassName.getSimpleName(), type, 
		            									BaseModel.class.getSimpleName()));
		            		throw cce;
		            	}
		            }
		    
		        
		        return Pair.of(null, null);
            });
            }
        } catch (Exception e) {
            Log.log.warn("Error getting gateway deployment filter information", e);
                                            HibernateUtil.rethrowNestedTransaction(e);
        }
         catch (Throwable t) {
            Log.log.warn(String.format("Error [%s]in getting information about latest deployed gateway filter of type %s" +
            						   " on queue %s", 
            						   (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""), 
            						   type, queue));
            throw new Exception(t);
        }
        
        
        return retLatestDeployedFilterDetails;
    }
}
