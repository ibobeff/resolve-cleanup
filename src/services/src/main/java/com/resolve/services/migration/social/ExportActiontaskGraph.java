/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.social;

import java.util.List;

import org.neo4j.graphdb.Node;

import com.resolve.services.migration.neo4j.SocialRelationshipTypes;


public class ExportActiontaskGraph extends ExportComponentGraph
{
    public ExportActiontaskGraph(String sysId) throws Exception
    {
       super(sysId, SocialRelationshipTypes.ACTIONTASK);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode) throws Exception
    {
        //get all the tags for this actiontask node
        Iterable<Node> tagsForActiontask = ExportDocumentGraph.findTagsFor(compNode);
        if(tagsForActiontask != null)
        {
            for(Node anyNode : tagsForActiontask)
            {
                addRelationship(compNode, anyNode, null);
            }//end of for
        }
        
        
        return relationships;
    }
}
