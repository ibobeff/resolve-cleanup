/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.search.wiki.WikiIndexAPI;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WikiStatusUtil
{
    
    public static void updateWikiStatusFlagForNamespaces(Set<String> namespaces, WikiStatusEnum field, boolean value, boolean sendNotification, String username)  throws Exception
    {
        if(namespaces != null)
        {
            for(String namespace : namespaces)
            {
                Set<String> sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace, username);
                updateWikiStatusFlag(sysIds, field, value, sendNotification, username);
            }
        }
    }
    
    public static void updateWikiStatusFlag(Set<String> docSysIds,  WikiStatusEnum field, boolean value, boolean sendNotification, String username) throws Exception
    {
        if(docSysIds != null)
        {
            for(String docSysId : docSysIds)
            {
                try
                {
                    updateWikiStatusFlag(docSysId, null, field, value, sendNotification, username);
                }
                catch (Exception e)
                {
                   Log.log.error("error updateing the delete flag for document with sysId : " + docSysId, e);
                   throw e;
                }
            }
        }
    }
    
    public static void updateWikiStatusFlag(String sysId, String docFullName, WikiStatusEnum field, boolean value, boolean sendNotification, String username) throws Exception
    {
        if (StringUtils.isNotBlank(docFullName) || StringUtils.isNotBlank(sysId))
        {
            List<SubmitNotification> notifications = new ArrayList<SubmitNotification>();
            
            Set<String> userRoles = UserUtils.getUserRoles(username);
            WikiDocument doc = WikiUtils.getWikiDocumentModel(sysId, docFullName, username, false);
            if (doc != null)
            {
                String docAdminRoles = doc.getAccessRights().getUAdminAccess();
                boolean hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, docAdminRoles);
                if (hasRights)
                {
                    switch(field)
                    {
                        case ACTIVE :
                                        doc.setUIsActive(value);
                                        break;
                        case LOCKED :
                                        doc.setUIsLocked(value);
                                        doc.setULockedBy(username);
                                        
                                        //if it is in the memory
                                        DocUtils.unlockByAdmin(doc.getUFullname(), username);
                                        break;
                        case HIDDEN :
                                        doc.setUIsHidden(value);
                                        break;
                        case DELETED :
                                        if (value && 
                                            ((StringUtils.isNotBlank(doc.getUDisplayMode()) &&
                                              doc.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK)) ||
                                             (/*!doc.ugetUIsDeletable()*/ doc.ugetUSIRRefCount().longValue() > 0)))
                                        {
                                            throw new Exception("Document with " + (StringUtils.isNotBlank(docFullName) ? "name " + docFullName : "") +
                                                                (StringUtils.isNotBlank(sysId) ? "sys id " + sysId : "") +
                                                                " is either a playbook (template or associated with SIR) or is document referenced in playbook activity which cannot be deleted."); 
                                        }
                                        doc.setUIsDeleted(value);
                                        break;
                        default :
                            break;
                    }

                    //save/update the document 
                    updateDocument(doc, username);
                    
                    //notification after the doc has been committed
                    switch(field)
                    {
                        case ACTIVE :
                                        if (value)
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_ACTIVE, username, true, true));
                                        }
                                        else
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_INACTIVE, username, true, true));
                                        }
                                        
                                        //index
                                        WikiIndexAPI.setActive(doc.getSys_id(), username, value);
                                        
                                        break;
                        case LOCKED :
                                        if (value)
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_LOCKED, username, true, true));
                                        }
                                        else
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_UNLOCKED, username, true, true));
                                        }
                            
                                        //index
                                        WikiIndexAPI.setLocked(doc.getSys_id(), username, value);
                                        
                                        break;
                        case HIDDEN :
                                        if (value)
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_HIDE, username, true, true));
                                        }
                                        else
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_UNHIDE, username, true, true));
                                        }
                            
                                        //index
                                        WikiIndexAPI.setHidden(doc.getSys_id(), username, value);
                                        
                                        break;
                        case DELETED :
                                        if (value)
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_DELETE, username, true, true));
                                        }
                                        else
                                        {
                                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_UNDELETE, username, true, true));
                                        }
                                        
                                        //index
                                        WikiIndexAPI.setDeleted(doc.getSys_id(), username, value);
                                        
                                        break;
                        default :
                                        //index the doc
                                        break;
                    }
                    
                    //submit the notifications
                    if(sendNotification && notifications.size() > 0)
                    {
                        //Finally submit all the notifications we have gathered.
                        NotificationHelper.submitNotifications(notifications, true);
                    }
                    
                }
                else
                {
                    Log.log.error("User " + username + " does not have 'admin' role on the document " + doc.getUFullname() + " to Delete it.");
                }
            }
            else
            {
                throw new Exception("Document with name " + docFullName + " does not exist.");
            }
        }
    }
    
    
    private static void  updateDocument(WikiDocument doc, String username) throws Exception
    {
        SaveUtil.saveWikiDocument(doc, username);

        //update graph
        WikiUtils.updateSocialGraphDB(doc, username);

       
    }
    

}
