/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.export;

import java.util.Collection;
import java.util.List;

import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.vo.social.NodeType;

public class ExportTeamGraph extends ExportComponentGraph
{
    private boolean recurse = false;

    public ExportTeamGraph(String sysId, ImpexOptionsDTO options, boolean recurse) throws Exception
    {
        super(sysId, options, NodeType.TEAM);
        this.recurse = recurse;
    }

    @Override
    protected List<GraphRelationshipDTO> exportRelationships(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception
    {
        //get all the nodes that this process node is refering too
        exportTeamsForTeam(compNode, socialImpex);
        exportUsersForTeam(compNode, socialImpex);

        return relationships;
    }

    private void exportTeamsForTeam(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception
    {
        //get all the nodes that this team node is refering too
        Collection<ResolveNodeVO> nodesThatThisTeamContains = ServiceGraph.getMemberNodes(compNode.getSys_id(), null, null, null, "admin");
        if (nodesThatThisTeamContains != null)
        {
            for (ResolveNodeVO anyNode : nodesThatThisTeamContains)
            {
                NodeType type = anyNode.getUType();
                if (type == NodeType.TEAM)
                {
                    addRelationship(compNode, anyNode);
                    prepareReturnNode(anyNode, socialImpex);
                    exportUsersForTeam(anyNode, socialImpex);
                    //if the recurse is true, get the child teams
                    if (recurse)
                    {
                        //** RECURSIVE
                        exportTeamsForTeam(anyNode, socialImpex);
                    }
                }
            }
        }//end of if        

    }

    private void exportUsersForTeam(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception
    {
        Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(compNode.getSys_id(), null, null, null, "admin");
        if (nodesFollowingMe != null)
        {
            for (ResolveNodeVO nodeFollowingMe : nodesFollowingMe)
            {
                NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                //if this a USER node, ignore it 
                if (nodeFollowingMeType == NodeType.USER)
                {
                    addRelationship(compNode, nodeFollowingMe);
                    prepareReturnNode(nodeFollowingMe, socialImpex);
                }
            }
        }
    }

    private void prepareReturnNode(ResolveNodeVO anyNode, SocialImpexVO socialImpex) throws Exception
    {
        NodeType type = anyNode.getUType();

        if (type == NodeType.TEAM)
        {
            if (options != null && options.getProcessTeams())
            {
                socialImpex.addReturnTeam(new RSComponent(anyNode));
            }
        }
        else if (type == NodeType.USER)
        {
            if (options != null && options.getProcessUsers())
            {
                socialImpex.addReturnUser(new RSComponent(anyNode));
            }
        }
    }
}
