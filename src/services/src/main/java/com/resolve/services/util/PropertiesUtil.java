/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.resolve.persistence.model.Properties;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.util.Log;

/**
 * This class reads a config flat file and loads that into Properties table in
 * the DB Also, it has utility methods to access, update the properties
 * 
 * FIX - need to add local caching of properties
 * 
 * @author jeet.marwah
 * 
 */
public class PropertiesUtil
{
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Public methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void initProperties(String fileName) throws Exception
    {
        ServiceHibernate.initProperties(fileName);
    } // initProperties

    /**
     * This utility method is to update a value of the property (NOT A CREATE)
     * and whose type is String
     * 
     * @param name
     * @param value
     */
    public static void setPropertyString(String name, String value)
    {
        ServiceHibernate.setPropertyString(name, value);

    } // setPropertyString

    /**
     * This utility method is to get the property of name and whose type is
     * String
     * 
     * @param name
     * @return
     */
    public static String getPropertyString(String name)
    {
        return ServiceHibernate.getPropertyString(name);
    } // getPropertyString

    /**
     * This utility method is to update a value of the property (NOT A CREATE)
     * and whose type is boolean
     * 
     * @param name : String representing name of a property
     * @param boolValue : Boolean representing value to be set on that property
     */
    public static void setPropertyBoolean(String name, boolean boolValue)
    {
       ServiceHibernate.setPropertyBoolean(name, boolValue);
    } // setPropertyBoolean

    /**
     * This utility method is to get the property of name and whose type is
     * boolean
     * 
     * @param name
     * @return
     */
    public static boolean getPropertyBoolean(String name)
    {
        return ServiceHibernate.getPropertyBoolean(name);
    } // getPropertyBoolean

    /**
     * THis is the utility method for the properties that has type as List. This
     * is an UPDATE and not a CREATE.
     * 
     * @param name
     * @param values
     */
    public static void setPropertyList(String name, List<String> values)
    {
        ServiceHibernate.setPropertyList(name, values);
    } // setPropertyList

    /**
     * THis is the utility method for the properties that has Type as List.
     * 
     * @param name
     * @return
     */
    public static List<String> getPropertyList(String name)
    {
        return ServiceHibernate.getPropertyList(name);
    } // getPropertyList

    /**
     * Utility method to update the property that has Int as its type. Not a
     * CREATE.
     * 
     * @param name
     * @param value
     */
    public static void setPropertyLong(String name, long value)
    {
        ServiceHibernate.setPropertyLong(name, value);
    } // setPropertyLong

    /**
     * Utility method to get the property value that has INT as its type.
     * 
     * @param name
     * @return
     */
    public static long getPropertyLong(String name)
    {
        return ServiceHibernate.getPropertyLong(name);
    } // getPropertyLong

    /**
     * Method to retrieve a list of properties name values
     * 
     * @param names
     * @return
     */
    public static Map<String, String> getProperties(List<String> names)
    {
        Map<String, String> result = new HashMap<String, String>();

        for (String name : names)
        {
            String value = getPropertyString(name);
            if (value != null)
            {
                result.put(name, value);
            }
        }

        return result;
    } // getProperties

    /**
     * Method to set the name/values of properties
     * 
     * @param nameValues
     */
    public static void setProperties(Map<String, String> nameValues)
    {
        for (Map.Entry<String, String> entry : nameValues.entrySet())
        {
            setPropertyString(entry.getKey(), entry.getValue());
        }
    } // setProperties

    /**
     * Utility method to save list of properties obje
     * 
     * @param coll
     */
    public static void saveProperties(Collection<PropertiesVO> coll)
    {
        if (coll != null && coll.size() > 0)
        {
            for (PropertiesVO prop : coll)
            {
                saveProperties(prop);
            }
        }
    }

    /**
     * 
     * This will create Properties record if it does not exist, else update the
     * existing one.
     * 
     * @param props
     */
    public static PropertiesVO saveProperties(PropertiesVO props)
    {
        PropertiesVO prop = null;
        try
        {
            prop = ServiceHibernate.saveProperties(props, "system");
        }
        catch (Exception e)
        {
            Log.log.error("Error saving property", e);
        }
        
        return prop;
    }


    /**
     * THis method queries the properties table and does a wildcard search on
     * name
     * 
     * @param uname
     * @return
     */
    public static List<PropertiesVO> getPropertiesLikeName(String uname)
    {
        return ServiceHibernate.getPropertiesLikeName(uname);
    } // getPropertiesLikeName

    public static String getResolveURL()
    {
        return getPropertyString("system.resolve.url");
    } // getResolveURL

    public static String getRSClientURL()
    {
        return getPropertyString("system.resolve.url") + "/jsp/rsclient.jsp?url=";
    } // getResolveURL
    
    public static Integer calculateChecksum(Properties property)
    {
        Integer checksum = 0;
        if (property != null) {
            checksum = Objects.hash(property.getUName(), property.getUValue());
        }
        return checksum;
    }
    
    public static void updateAllPropertiesForChecksum() {
        try {
            UpdateChecksumUtil.updateComponentChecksum("Properties", PropertiesUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }

} // PropertiesUtil
