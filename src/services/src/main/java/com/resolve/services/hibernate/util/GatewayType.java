package com.resolve.services.hibernate.util;

public enum GatewayType
{
    PULL("PullGatewayFilter"),
    PUSH("PushGatewayFilter"),
    MSG("MSGGatewayFilter");

    private GatewayType(String entityFilterClass)
    {
        this.entityFilterClass = entityFilterClass;
    }

    private String entityFilterClass;

    public String getEntityFilterClass()
    {
        return entityFilterClass;
    }
}
