package com.resolve.services.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import com.resolve.util.StringUtils;

public class CSRFUtil {
    private static final String LOGIN_URL = "/resolve/service/login";
	private static final String FETCH_CSRF_TOKEN = "FETCH-CSRF-TOKEN";
	private static final String CSRF_SERVLET_URL = "/resolve/JavaScriptServlet";
	private static final String ROOT_URL = "/resolve/";
	private static final Pattern CSRF_SCRIPT = Pattern.compile("<script src=\"\\/resolve\\/JavaScriptServlet\\?(.+)\"><\\/script>");

	/**
	 * Initiates session with http server. On a first step connection is created
	 * and server http session is initiated. On a second step GET request is
	 * sent to CSRF service which leads to initialization of internal service structures.
	 * 
	 * @param serviceUrl http service url(e.g http://localhost:8080)
	 * @return http client for further operations
	 * @throws IOException
	 */
    public static HttpClient initSession(String serviceUrl) throws Exception {
		HttpClient httpClient = HttpClientBuilder.create().build();
		return initSession(httpClient, serviceUrl);
    }

	/**
	 * Initiates session with https server over SSL. On a first step connection is created
	 * and server https session is initiated. On a second step GET request is
	 * sent to CSRF service which leads to initialization of internal service structures.
	 * 
	 * @param serviceUrl https service url(e.g https://localhost:8443)
	 * @return http client for further operations
	 * @throws IOException
	 */
    public static HttpClient initSslSession(String serviceUrl) throws Exception {
		SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(new TrustSelfSignedStrategy()).build();
		SSLConnectionSocketFactory factory = new SSLConnectionSocketFactory(sslcontext, new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		});

		HttpClient httpClient = HttpClients.custom().setConnectionManagerShared(true).setSSLSocketFactory(factory)
				.build();        
        
		return initSession(httpClient, serviceUrl);
    }
    
    private static HttpClient initSession(HttpClient httpClient, String serviceUrl) throws Exception {
    	// Touch the app to create JSESSIONID
		HttpGet request = new HttpGet(new StringBuilder(serviceUrl).append(ROOT_URL).toString());
        HttpClientContext context = HttpClientContext.create();
        HttpResponse response = httpClient.execute(request, context);
        String data = EntityUtils.toString(response.getEntity());
        
        // Find CSRF servlet url and get it. This is required for server-side CSRF initialization 
        Matcher matcher = CSRF_SCRIPT.matcher(data);
		while (matcher.find()) {
			String token = matcher.group(1);
			request = new HttpGet(new StringBuilder(serviceUrl).append(CSRF_SERVLET_URL).append("?").append(token).toString());
			httpClient.execute(request, context);
		}

		return httpClient;
    }
    
    /**
     * Fetches initial CSRF token that allows to start communication with protected resources.
     * 
     * @param httpClient http client that already has the established session with service 
     * @param serviceUrl http service url(e.g http://localhost:8080)
     * @return CSRF token in a form of <TOKEN_NAME:TOKEN_VALUE>
     * @throws Exception
     */
	public static String fetchInitialToken(HttpClient httpClient, String serviceUrl) throws Exception {
		return fetchToken(httpClient, serviceUrl, null);
	}
	
	/**
	 * Fetches session CSRF token that allows to make AJAX calls to protected resources.
	 * 
	 * @param httpClient http client that already has the established session with service 
	 * @param serviceUrl http service url(e.g http://localhost:8080)
	 * @param initialToken initial CSRF token
	 * @return CSRF token in a form of <TOKEN_NAME:TOKEN_VALUE>
	 * @throws Exception
	 */
	public static String fetchToken(HttpClient httpClient, String serviceUrl, String initialToken) throws Exception {
		HttpPost httpPost = new HttpPost(new StringBuilder(serviceUrl).append(CSRF_SERVLET_URL).toString());
		httpPost.setHeader(FETCH_CSRF_TOKEN, "1");
		
		if (StringUtils.isNotEmpty(initialToken)) {
	        String[] tokens = parseCSRFToken(initialToken);
			httpPost.setHeader(tokens[0], tokens[1]);
		}
		HttpResponse response = httpClient.execute(httpPost);
		
		if (response == null || response.getStatusLine().getStatusCode() != HttpStatus.SC_OK || response.getEntity() == null) {
			throw new Exception("Unable to fetch CSRF token");
		}

		return EntityUtils.toString(response.getEntity());
	}

	/**
	 * Authenticates the user using provided username/password. As a http client will have cookie store with user cookies.
	 *  
	 * @param httpClient http client that already has the established session with service
	 * @param serviceUrl http service url(e.g http://localhost:8080)
	 * @param user user name 
	 * @param password
	 * @throws Exception
	 */
	public static void login(HttpClient httpClient, String serviceUrl, String user, String password) throws Exception {
		HttpPost httpPost = new HttpPost(new StringBuilder(serviceUrl).append(LOGIN_URL).toString());
        
        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("username", user));
        postParameters.add(new BasicNameValuePair("password", password));
        postParameters.add(new BasicNameValuePair("url", "/"));
        postParameters.add(new BasicNameValuePair("hashState", ""));
        postParameters.add(new BasicNameValuePair("iframeId", ""));

		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(postParameters);
        httpPost.setEntity(entity);
        HttpResponse response = httpClient.execute(httpPost);
		if (response == null || response.getStatusLine().getStatusCode() != HttpStatus.SC_MOVED_TEMPORARILY) {
			throw new Exception("Login failed");
		}
	}

	/**
	 * Validates and parses provided CSRF token.
	 * 
	 * @param token CSRF token in a form of <TOKEN_NAME:TOKEN_VALUE>
	 * @return array with token name and token value
	 * @throws Exception
	 */
	public static String[] parseCSRFToken(String token) throws Exception {
		if (StringUtils.isEmpty(token)) {
        	throw new Exception("Empty token provided");
		}
		
        String[] tokens = token.split(":");
        if (tokens.length != 2) {
        	throw new Exception("Wrong token provided");
        }

        return tokens; 
	}
}
