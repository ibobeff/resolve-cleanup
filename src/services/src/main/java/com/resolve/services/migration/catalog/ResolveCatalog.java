/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.catalog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.joda.time.DateTime;
import org.neo4j.graphdb.Node;

import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.util.DateManipulator;
import com.resolve.util.StringUtils;

public class ResolveCatalog implements Serializable
{
    public static final String DISPLAY_TYPE_WIKI = "wiki";
    public static final String DISPLAY_TYPE_LIST_OF_DOCUMENTS = "listOfDocuments";
    public static final String DISPLAY_TYPE_URL = "url";

    private static final long serialVersionUID = 3097707333544347344L;

    // common header parameters
    public static final String SYS_ID = "sys_id";
    public static final String DISPLAYNAME = "displayName";
    public static final String ROOT_NAME = "rootName";
    public static final String TYPE = "type";
    public static final String ORDER = "order";
    public static final String SYS_CREATED_ON = "sys_created_on";
    public static final String SYS_UPDATED_ON = "sys_updated_on";
    public static final String SYS_CREATED_BY = "sys_created_by";
    public static final String SYS_UPDATED_BY = "sys_updated_by";

    // access control
    public static final String ROLES = "roles";
    public static final String EDIT_ROLES = "editRoles";

    // static property parameters
    public static final String TITLE = "title";
    public static final String DESC = "description";
    public static final String IMAGE = "image";
    public static final String IMAGE_NAME = "image_name";
    public static final String TOOLTIP = "tooltip";
    public static final String WIKI = "wiki";
    public static final String LINK = "link";
    public static final String DISPLAYTYPE = "displayType"; //valid values are - wiki, listOfDocuments, url
    public static final String WIKISYSID = "wikidocSysID";
    public static final String MAX_IMAGE_WIDTH = "maxImageWidth";
    public static final String PATH = "path";
    public static final String OPEN_IN_NEW_TAB = "openInNewTab";
    public static final String SIDE_WIDTH = "sideWidth";

    public static final String IS_ROOT = "isRoot";
    public static final String IS_ROOT_REF = "isRootRef";
    public static final String CATALOG_TYPE_STRING = "catalogType";
    
    public static final String INTERNAL_NAME = "internalName";
    public static final String ICON = "icon";
    public static final String NAMESPACE = "namespace";

    private String id;
    private String name;
    private String type;
    protected String operation;

    private int order;
    private long sys_created_on;
    private long sys_updated_on;
    private String sys_created_by;
    private String sys_updated_by;

    private String roles = "";
    private String editRoles = "";

    private String title;
    private String description;
    private String image;
    private String imageName;
    private String tooltip;
    private String wiki;
    private String link;
    private String form;
    private String displayType;
    private int maxImageWidth;
    private String path;
    private boolean openInNewTab;
    private int sideWidth;

    private String catalogType;

    private String wikidocSysID;

    private boolean isRoot;
    private boolean isRootRef;
    
    private String internalName;
    private String icon;
    
    // Children are of type ResolveCatalog
    private List<ResolveCatalog> children = new ArrayList<ResolveCatalog>();
    
    // Tags are of type Tag - the picker is using Tag, so changing the ResolveTag to Tag
    private List<ResolveTagVO> tags = new ArrayList<ResolveTagVO>(); 

    //list of errors 
    private List<String> errors = new ArrayList<String>();
    
    //wiki namespace - for the root node. This will be applied to all the nodes if the namespace of the doc is not present
    private String namespace = null;
    
    
    public ResolveCatalog()
    {
    }

    public ResolveCatalog(String id, String name)
    {
        this.id = id;
        this.name = name;
    }

    //
    // ************** getting/setting for header parameters **********
    //
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        if (StringUtils.isNotEmpty(id))
        {
            this.id = id.trim();
        }
        else
        {
            this.id = id;
        }

        if (StringUtils.isEmpty(this.name))
        {
            this.name = id;
        }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * The metaType =
     * 
     * @return
     */
    public String getType()
    {
        return this.type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getOrder()
    {
        return this.order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }

    public long getSys_created_on()
    {
        return sys_created_on;
    }

    public void setSys_created_on(long sys_created_on)
    {
        this.sys_created_on = sys_created_on;
    }

    public long getSys_updated_on()
    {
        return sys_updated_on;
    }

    public void setSys_updated_on(long sys_updated_on)
    {
        this.sys_updated_on = sys_updated_on;
    }

    public String getSys_created_by()
    {
        return sys_created_by;
    }

    public void setSys_created_by(String sys_created_by)
    {
        this.sys_created_by = sys_created_by;
    }

    public String getSys_updated_by()
    {
        return sys_updated_by;
    }

    public void setSys_updated_by(String sys_updated_by)
    {
        this.sys_updated_by = sys_updated_by;
    }

    //
    // *************** getting/setting for roles **********************
    public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    //
    // *************** getting/setting for static properties ******************
    //
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getTooltip()
    {
        return tooltip;
    }

    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }

    public String getWiki()
    {
        return wiki;
    }

    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }

    public String getLink()
    {
        return link;
    }

    public void setLink(String link)
    {
        this.link = link;
    }

    public String getWikidocSysID()
    {
        return this.wikidocSysID;
    }

    public void setWikidocSysID(String wikidocSysID)
    {
        this.wikidocSysID = wikidocSysID;
    }

    public boolean isRoot()
    {
        return this.isRoot;
    }

    public void setIsRoot(boolean isRoot)
    {
        setRoot(isRoot);
    }
    
    public void setRoot(boolean isRoot)
    {
        // Added for Json compatiblity
        this.isRoot = isRoot;
    }

    public boolean isRootRef()
    {
        return this.isRootRef;
    }

    public void setIsRootRef(boolean isRootRef)
    {
        setRootRef(isRootRef);
    }
    
    public void setRootRef(boolean isRootRef)
    {
        // Added for Json compatiblity
        this.isRootRef = isRootRef;
    }
    
    public List<ResolveCatalog> getChildren()
    {
        return children;
    }

    public void setChildren(List<ResolveCatalog> children)
    {
        this.children = children;
    }

    public int getSideWidth()
    {
        return sideWidth;
    }

    public void setSideWidth(int sideWidth)
    {
        this.sideWidth = sideWidth;
    }


    public String getForm()
    {
        return form;
    }

    public void setForm(String form)
    {
        this.form = form;
    }

    public List<ResolveTagVO> getTags()
    {
        return tags;
    }

    public void setTags(List<ResolveTagVO> tags)
    {
        this.tags = tags;
    }

    public String getImageName()
    {
        return imageName;
    }

    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    public String getDisplayType()
    {
        return displayType;
    }

    public void setDisplayType(String displayType)
    {
        this.displayType = displayType;
    }

    public String getCatalogType()
    {
        return catalogType;
    }

    public void setCatalogType(String catalogType)
    {
        this.catalogType = catalogType;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }
    
    

//    @JsonIgnore
//    public boolean isValid()
//    {
//        return this.getErrors().size() == 0;
//    }

//    private List<String> getErrors()
//    {
//        // Check duplicate name
//        this.errors.clear();
//        try
//        {
//            if (StringUtils.isEmpty(this.id))
//            {
//                List<ResolveCatalog> catalogs = ServiceCatalog.getAllCatalogs(null);
//                for (ResolveCatalog catalog : catalogs)
//                {
//                    if (catalog.getName().equals(this.name))
//                    {
//                        this.errors.add("duplicateName");
//                    }
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            this.errors.add(e.getMessage());
//        }
//        return this.errors;
//    }

    @JsonIgnore
    public String getError()
    {
        StringBuilder sb = new StringBuilder();
        for (String error : this.errors)
        {
            if (sb.length() > 0) sb.append(", ");
            sb.append(error);
        }
        return sb.toString();
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public int getMaxImageWidth()
    {
        return maxImageWidth;
    }

    public void setMaxImageWidth(int maxImageWidth)
    {
        this.maxImageWidth = maxImageWidth;
    }

    public boolean isOpenInNewTab()
    {
        return openInNewTab;
    }

    public void setOpenInNewTab(boolean openInNewTab)
    {
        this.openInNewTab = openInNewTab;
    }

    public String getOperation()
    {
        return this.operation;
    }
    
    public void setOperation(String operation)
    {
        this.operation = operation;
    }
    
    public void applyPath(String oldRoot, String newRoot, boolean rename)
    {
        if (StringUtils.isNotEmpty(this.path))
        {
//            String originalPath = this.path;
            String newPath = this.path.replaceFirst(oldRoot,  newRoot);
            this.path = newPath;
        }

        for (Object child : this.children)
        {
            ((ResolveCatalog) child).applyPath(oldRoot, newRoot, rename);
        }
    }
    
    public String getInternalName()
    {
        return internalName;
    }

    public void setInternalName(String internalName)
    {
        this.internalName = internalName;
    }
    
    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    //
    // ******************** Object - Node convertion ********************
    //
    public void convertObjectToNode(Node node)
    {
        // sys_id
        if (StringUtils.isEmpty(id))
        {
            id = Long.toString(node.getId()) + "." + Long.toString(sys_updated_on);
            
            //make sure that values are current for the new recs
            sys_created_on = DateManipulator.GetUTCDateLong();
            sys_updated_on = DateManipulator.GetUTCDateLong();
        }
        node.setProperty(SYS_ID, id);
        
        // displayName
        if (StringUtils.isEmpty(name))
        {
            name = id;
        }
        node.setProperty(DISPLAYNAME, name);

        // displayName
        node.setProperty(TYPE, (StringUtils.isNotEmpty(type)) ? type : "");
        node.setProperty(ORDER, order);

        // sys_created_on
        if (sys_created_on == 0l)
        {
            sys_created_on = DateManipulator.GetUTCDateLong();
        }
        node.setProperty(SYS_CREATED_ON, sys_created_on);

        // sys_updated_on
        if (sys_updated_on == 0l)
        {
            sys_updated_on = DateManipulator.GetUTCDateLong();
        }
        node.setProperty(SYS_UPDATED_ON, sys_updated_on);

        // sys_created_by
        if (sys_created_by == null)
        {
            sys_created_by = "system";
        }
        node.setProperty(SYS_CREATED_BY, sys_created_by);

        // sys_updated_by
        if (sys_updated_by == null)
        {
            sys_updated_by = "system";
        }
        node.setProperty(SYS_UPDATED_BY, sys_updated_by);

        node.setProperty(ROLES, (StringUtils.isNotEmpty(roles)) ? roles : "");
        node.setProperty(EDIT_ROLES, (StringUtils.isNotEmpty(editRoles)) ? editRoles : "");

        node.setProperty(TITLE, (StringUtils.isNotEmpty(title)) ? title : "");
        node.setProperty(DESC, (StringUtils.isNotEmpty(description)) ? description : "");
        node.setProperty(IMAGE, (StringUtils.isNotEmpty(image)) ? image : "");
        node.setProperty(IMAGE_NAME, (StringUtils.isNotEmpty(imageName)) ? imageName : "");
        node.setProperty(TOOLTIP, (StringUtils.isNotEmpty(tooltip)) ? tooltip : "");
        node.setProperty(WIKI, (StringUtils.isNotEmpty(wiki)) ? wiki : "");
        node.setProperty(NAMESPACE,  (StringUtils.isNotEmpty(namespace)) ? namespace : "");
        node.setProperty(LINK, (StringUtils.isNotEmpty(link)) ? link : "");
        node.setProperty(DISPLAYTYPE, (StringUtils.isNotEmpty(displayType)) ? displayType : "");
        node.setProperty(WIKISYSID, (StringUtils.isNotEmpty(wikidocSysID)) ? wikidocSysID : "");

        node.setProperty(IS_ROOT, isRoot);

        if (isRoot)
        {
            node.setProperty(ROOT_NAME, name.toLowerCase());
        }

        node.setProperty(IS_ROOT_REF, isRootRef);
        node.setProperty(CATALOG_TYPE_STRING, catalogType != null ? catalogType : "");
        node.setProperty(MAX_IMAGE_WIDTH, maxImageWidth);
        node.setProperty(PATH, path != null ? path : "");
        node.setProperty(OPEN_IN_NEW_TAB, openInNewTab);
        node.setProperty(SIDE_WIDTH, sideWidth);
        
        node.setProperty(INTERNAL_NAME, internalName != null ? internalName : "");
        node.setProperty(ICON, icon != null ? icon : "");

    }

    public void convertNodeToObject(Node node)
    {
        id = (String) node.getProperty(SYS_ID, "");
        name = (String) node.getProperty(DISPLAYNAME, "");
        type = (String) node.getProperty(TYPE, "");
        order = (Integer) node.getProperty(ORDER, 0);
        sys_created_on = (Long) node.getProperty(SYS_CREATED_ON, DateTime.now().getMillis());
        sys_updated_on = (Long) node.getProperty(SYS_UPDATED_ON, DateTime.now().getMillis());
        sys_created_by = (String) node.getProperty(SYS_CREATED_BY, "");
        sys_updated_by = (String) node.getProperty(SYS_UPDATED_BY, "");

        roles = (String) node.getProperty(ROLES, "");
        editRoles = (String) node.getProperty(EDIT_ROLES, "");

        title = (String) node.getProperty(TITLE, "");
        description = (String) node.getProperty(DESC, "");
        image = (String) node.getProperty(IMAGE, "");
        imageName = (String) node.getProperty(IMAGE_NAME, "");
        tooltip = (String) node.getProperty(TOOLTIP, "");
        wiki = (String) node.getProperty(WIKI, "");
        namespace = (String) node.getProperty(NAMESPACE, "");
        link = (String) node.getProperty(LINK, "");
        displayType = (String) node.getProperty(DISPLAYTYPE, "");
        wikidocSysID = (String) node.getProperty(WIKISYSID, "");

        isRoot = (Boolean) node.getProperty(IS_ROOT, false);
        isRootRef = (Boolean) node.getProperty(IS_ROOT_REF, false);
        catalogType = (String) node.getProperty(CATALOG_TYPE_STRING, "");
        maxImageWidth = (Integer) node.getProperty(MAX_IMAGE_WIDTH, 0);
        path = (String) node.getProperty(PATH, "");
        openInNewTab = (Boolean) node.getProperty(OPEN_IN_NEW_TAB, false);
        sideWidth = (Integer)node.getProperty(SIDE_WIDTH, 225);
        
        internalName = (String) node.getProperty(INTERNAL_NAME, "");
        icon = (String) node.getProperty(ICON, "");
        
    }

    //
    // **************** define uniq of object ********************************
    @Override
    public int hashCode()
    {
        int hashcode = 1;
        hashcode = 31 * hashcode + ((id == null) ? 0 : id.hashCode());
        return hashcode;
    }

    public boolean equals(Object obj)
    {
        boolean result = true;

        if (this == obj)
        {
            result = true;
        }

        if (obj == null)
        {
            result = false;
        }

        if (getClass() != obj.getClass())
        {
            result = false;
        }

        ResolveCatalog catalogObj = (ResolveCatalog) obj;

        if (id == null)
        {
            if (catalogObj.getId() != null)
            {
                result = false;
            }
        }
        else if (!id.equals(catalogObj.getId()))
        {
            result = false;
        }

        return result;
    }

    public void purgeIds()
    {
        this.id = "";
        if (!"CatalogReference".equals(this.type) || this.isRoot)
        {
            for (Object child : this.children)
            {
                ((ResolveCatalog) child).purgeIds();
            }
        }
    }

    //changed the name from 'get..' to 'find..' as the json convertor executes all the 'get' apis when it serializes/deserializes
    public Set<String> findAllDocumentNames()
    {
        Set<String> result = new HashSet<String>();
        
        findAllWikiNames(result, this);
        
        return result;
    }
    
    public void updatePath()
    {
        updatePath(this, "");
    }
    
    public void updateWikiDocumentNames()
    {
        updateWikiDocumentNames(this, this.getNamespace());
    }
    
    public void validate() throws Exception
    {
        //validate the doc names first
        validateWikiDocumentNames();
        validatePaths();
    }
    
    public Set<String> findAllTagIds()
    {
        Set<String> result = new HashSet<String>();
        
        findAllTagIs(result, this);
        
        return result;
    }
    
    public ResolveCatalog findCatalogNodeWithPath(String path)
    {
        ResolveCatalog result = null;
        
        if(StringUtils.isNotBlank(path))
        {
            result = findCatalogNodeWithPath(this, path);
        }
        
        return result;
    }
    
    public Set<String> findAllCatalogNodeIds()
    {
        Set<String> result = new HashSet<String>();
        result.addAll(findAllCatalogNodeIds(this));
        return result;
    }
    
    
    //private apis
    private void findAllTagIs(Set<String> result, ResolveCatalog catalog)
    {
        if (catalog.getTags() != null && catalog.getTags().size() > 0)
        {
            List<ResolveTagVO> objects = catalog.getTags();
            for (ResolveTagVO tag : objects)
            {
                result.add(tag.getSys_id());
            }
        }
        
        List<ResolveCatalog> objects = catalog.getChildren();
        if (objects != null && objects.size() > 0)
        {
            for (ResolveCatalog localCatalog : objects)
            {
//                if (localCatalog.getChildren() != null && localCatalog.getChildren().size() > 0)
//                {
                    findAllTagIs(result, localCatalog);
//                }
//                else
//                {
//                    if (localCatalog.getTags() != null && localCatalog.getTags().size() > 0)
//                    {
//                        List<Tag> localObjs = catalog.getTags();
//                        for (Tag tag : localObjs)
//                        {
//                            result.add(tag.getId());
//                        }
//                    }
//                }
            }
        }
    }
    

    private void findAllWikiNames(Set<String> result, ResolveCatalog catalog)
    {
        //get docs only where the displayType = 'wiki'
        if (StringUtils.isNotBlank(catalog.getDisplayType()) 
                        && StringUtils.isNotBlank(catalog.getWiki()) 
                        && (catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_WIKI) || catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
        {
            String wiki = catalog.getWiki();
            List<String> wikiNames = StringUtils.convertStringToList(wiki, ",");
            result.addAll(wikiNames);
        }
        
        
        List<ResolveCatalog> objects = catalog.getChildren();
        if (objects != null && objects.size() > 0)
        {
            for (ResolveCatalog localCatalog : objects)
            {
                if (localCatalog.getChildren() != null && localCatalog.getChildren().size() > 0)
                {
                    //** RECURSIVE
                    findAllWikiNames(result, localCatalog);
                }
                else if (StringUtils.isNotEmpty(localCatalog.getDisplayType())
                                && StringUtils.isNotEmpty(localCatalog.getWiki()))
                {
                    if((localCatalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_WIKI)) 
                                    || localCatalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_LIST_OF_DOCUMENTS))
                    {
                        String wiki = localCatalog.getWiki();
                        List<String> wikiNames = StringUtils.convertStringToList(wiki, ",");
                        result.addAll(wikiNames);
                    }
                }
            }
        }
    }
    
    private ResolveCatalog findCatalogNodeWithPath(ResolveCatalog catalog, String path)
    {
        ResolveCatalog result = null;
        
        if(catalog != null && StringUtils.isNotBlank(path))
        {
            List<ResolveCatalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (ResolveCatalog localCatalog : objects)
                {
                    String localCatalogPath = localCatalog.getPath();
                    if(StringUtils.isNotBlank(localCatalogPath) && localCatalogPath.equalsIgnoreCase(path))
                    {
                        result = localCatalog;
                        break;
                    }
         
                    //if not - check deeper
                    //** RECURSIVE
                    result = findCatalogNodeWithPath(localCatalog, path);
                    if(result != null)
                    {
                        break;
                    }
                }//end of for loop
            }
        }
        
        return result;
    }
    
    private Set<String> findAllCatalogNodeIds(ResolveCatalog catalog)
    {
        Set<String> result = new HashSet<String>();
        
        if(catalog != null)
        {
            //add the id
            if (StringUtils.isNotBlank(catalog.getId()))
            {
                result.add(catalog.getId());
            }
            
            //add the child ids
            List<ResolveCatalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (ResolveCatalog localCatalog : objects)
                {
                  //** RECURSIVE
                    result.addAll(findAllCatalogNodeIds(localCatalog));
                }//end of for loop
            }
        }
        
        return result;
    }
    
    private void updatePath(ResolveCatalog catalog, String path)
    {
        if(catalog != null)
        {
            String catalogName = catalog.getName();
            if(StringUtils.isEmpty(catalogName))
            {
                throw new RuntimeException("Name is mandatory. Name for one of the node is empty. ");
            }
            
            String newPath = path + "/" + catalogName;
            catalog.setPath(newPath);
            
            //for the child elements
            List<ResolveCatalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (ResolveCatalog localCatalog : objects)
                {
                  //** RECURSIVE
                    updatePath(localCatalog, newPath);
                }//end of for loop
            }
        }
    }
    
    private void updateWikiDocumentNames(ResolveCatalog catalog, String rootNamespace)
    {
        if(catalog != null && StringUtils.isNotBlank(rootNamespace))
        {
            String catalogName = catalog.getName();
            if(StringUtils.isEmpty(catalogName))
            {
                throw new RuntimeException("Name is mandatory. Name for one of the node is empty. ");
            }
            
            //get docs only where the displayType = 'wiki'
            if (StringUtils.isNotBlank(catalog.getDisplayType()) 
                            && StringUtils.isNotBlank(catalog.getWiki()) 
                            && (catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_WIKI) || catalog.getDisplayType().equalsIgnoreCase(DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
            {
                String wiki = catalog.getWiki();
                wiki = updateNamespacesToWikiDocumentNames(rootNamespace, wiki);
                catalog.setWiki(wiki);
            }
  
            //for the child elements
            List<ResolveCatalog> objects = catalog.getChildren();
            if (objects != null && objects.size() > 0)
            {
                for (ResolveCatalog localCatalog : objects)
                {
                    //** RECURSIVE
                    updateWikiDocumentNames(localCatalog, rootNamespace);
                }//end of for loop
            }
        }
    }
    
    //wiki - maybe CSV of names or just 1 wiki name
    private String updateNamespacesToWikiDocumentNames(String rootNamespace, String wiki)
    {
        Set<String> wikiFullNames = new HashSet<String>();
        List<String> wikiNames = StringUtils.convertStringToList(wiki, ",");
        for(String wikiName : wikiNames)
        {
            if(StringUtils.isNotBlank(wikiName))
            {
                if( wikiName.indexOf('.') == -1)
                {
                    wikiFullNames.add(rootNamespace + "." + wikiName);    
                }
                else
                {
                    wikiFullNames.add(wikiName);
                }
            }
        }
        
        String fullname = StringUtils.convertCollectionToString(wikiFullNames.iterator(), ",");
        return fullname;
    }

    private void validateWikiDocumentNames() throws Exception
    {
        Set<String> docFullNames = findAllDocumentNames();
        Set<String> invalidDocNames = new HashSet<String>();

        if(docFullNames != null && docFullNames.size() > 0)
        {
            for(String docFullName : docFullNames)
            {
                try
                {
                    //validate if the name of document is correct
                    WikiUtils.validateWikiDocName(docFullName);
                }
                catch (Exception e)
                {
                    invalidDocNames.add(docFullName);
                }
            }//end of for loop
        
            if(invalidDocNames.size() > 0)
            {
                throw new Exception("Invalid documents :" + StringUtils.convertCollectionToString(invalidDocNames.iterator(), ","));
            }
        }
    }
    
    private void validatePaths() throws Exception
    {
        //set for comparing the uniqueness
        Set<String> paths = new HashSet<String>();
        paths.add(this.getPath());
        
        //list of children for the catalog
        List<ResolveCatalog> children = this.getChildren();
        
        //validate 
        validatePathUniqueness(children, paths);
    }
    
    private void validatePathUniqueness(List<ResolveCatalog> catalogs, Set<String> existingPaths) throws Exception
    {
        if (catalogs != null && catalogs.size() > 0)
        {
            for (ResolveCatalog localCatalog : catalogs)
            {
                String path = localCatalog.getPath();
                if (StringUtils.isBlank(path))
                {
                    throw new Exception("Path is not available for catalog " + localCatalog.getName());
                }
                else
                {
                    if(existingPaths.contains(path))
                    {
                        throw new Exception("Duplicate paths found for " + path);
                    }
                    else
                    {
                        existingPaths.add(path);
                    }
                }
                    
                //validate deeper
                //** RECURSIVE
                validatePathUniqueness(localCatalog.getChildren(), existingPaths);
            }//end of for loop
        }
    }

    
}
