package com.resolve.services.task.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ColumnStructuredParserGen extends AbstractParserGen
{
    
    public ColumnStructuredParserGen(String regex, String parseStart, String parseEnd, String parseType, HashMap<String,Integer> variables, Set<String> flowVariables, Set<String> wsdataVariables,  Set<String> outputVariables, HashMap<String, Boolean> options)
    {
        super(regex, parseStart, parseEnd, parseType, variables, flowVariables, wsdataVariables, outputVariables, options);
        
    }

    public String generateCode()
    {
        String template = getParserTemplate();
        if(regex == null || regex.equals(""))
        {
            regex = "\"\"";
        }
        else
        {
            regex = "/"+regex+"/";
        }
        
        if(parseStart == null || parseStart.equals(""))
        {
            parseStart = "\"\"";
        }
        else
        {
            parseStart = "/"+parseStart+"/";
        }
        
        if(parseEnd == null || parseEnd.equals(""))
        {
            parseEnd = "\"\"";
        }
        else
        {
            parseEnd = "/"+parseEnd+"/";
        }
        template = template.replace("<==REGEX==>", regex);
        template = template.replace("<==PARSE_START==>", parseStart);
        template = template.replace("<==PARSE_END==>", parseEnd);
        
        String rawTrimmingRegexExpression = "(?s)";
        if(options.get("boundaryStartIncluded"))
            rawTrimmingRegexExpression += "($parseStart";
        else 
            rawTrimmingRegexExpression += "$parseStart(";
        if( !parseEnd.equals("") && parseEnd != null && !parseEnd.equals("\"\"") && !parseEnd.contains("(?:\\n|\\r\\n)"))
            rawTrimmingRegexExpression += ".*?";
        else
            rawTrimmingRegexExpression += ".*";
        if(options.get("boundaryEndIncluded"))            
            rawTrimmingRegexExpression += "$parseEnd)";
        else 
            rawTrimmingRegexExpression += ")$parseEnd";
        
        template = template.replace("<==RAW_TEXT_TRIM_REGEX==>", rawTrimmingRegexExpression);
        
        //Trim always implemented in the groovy code currently(11-20-2014), code written to support functionality in the future
        Boolean trim = options.get("repeat");
        if(trim ==  null)
        {
            trim = false;
        }
        
        template = template.replace("<==TRIM==>", trim.toString());
        return template;
    }

    
    public String getParserTemplate()
    {
        String parserTemplate = "def rawTerminalText = RAW;\r\n" + 
                "def isTest = INPUTS[\"TEST\"];\r\n" + 
        		"def parsingContent = \"\";\r\n" + 
        		"\r\n" + 
        		"def regex = <==REGEX==>;\r\n" + 
        		"def parseStart = <==PARSE_START==>;\r\n" + 
        		"def parseEnd = <==PARSE_END==>;\r\n" + 
        		"\r\n" + 
        		"def splitRecords = []; \r\n" + 
        		"\r\n" + 
        		"if(rawTerminalText)\r\n" + 
        		"{\r\n" + 
        		"    //extract From parseStart to parseEnd\r\n" + 
        		"    def sampleTrimmedMatcher = rawTerminalText =~ /<==RAW_TEXT_TRIM_REGEX==>/;\r\n" + 
        		"    if(sampleTrimmedMatcher.find())\r\n" + 
        		"    {\r\n" + 
        		"        parsingContent = sampleTrimmedMatcher.group(1);\r\n" + 
        		"    }\r\n" + 
        		"}\r\n" + 
        		"\r\n" + 
                "def regexMatched = false\r\n" +
        		"parsingContent.eachLine()\r\n" + 
        		"{  curLine -> \r\n" + 
        		"\r\n" + 
        		"    //verify that the line is not blank\r\n" + 
        		"    curLine = curLine.trim();\r\n" +
        		"    if(!curLine.isEmpty())\r\n" + 
        		"    {\r\n" + 
                "        if(!regexMatched) regexMatched = true\r\n" + 
        		"        Collection splitLine = curLine.trim().split(regex);\r\n" +
                "        splitLine = splitLine*.trim();\r\n" +
        		"        splitRecords.add(splitLine);\r\n" + 
        		"    }\r\n" + 
        		"}\r\n" + 
        		"\r\n" +
        		"//find maxListSize\r\n" + 
        		"def maxListSize = Integer.MIN_VALUE;\r\n" + 
        		"splitRecords.each()\r\n" + 
        		"{ curList ->\r\n" + 
        		"    if(curList.size() > maxListSize)\r\n" + 
        		"    {\r\n" + 
        		"        maxListSize = curList.size();\r\n" + 
        		"    }\r\n" + 
        		"}\r\n" + 
        		"\r\n" + 
        		"//pad every list with empty values until 2d array is a perfect square\r\n" + 
        		"//required for transpose and referencing\r\n" + 
        		"splitRecords.each()\r\n" + 
        		"{ curList ->\r\n" + 
        		"    def sizeDifference = maxListSize - curList.size();\r\n" + 
        		"    if(sizeDifference!=0)\r\n" + 
        		"    {\r\n" + 
        		"        for(def i = 0; i < sizeDifference;i++)\r\n" + 
        		"        {\r\n" + 
        		"            if(curList instanceof Collection)\r\n" +
                "            {\r\n" + 
        		"                curList.add(\"\");\r\n" + 
                "            }\r\n" + 
        		"        }\r\n" + 
        		"    }\r\n" + 
        		"}\r\n"+
        		"splitRecords = GroovyCollections.transpose(splitRecords);\n" +
        		"DATA = [:];\n";
        
        //Assign variables to extracted data
        Iterator<String> keyIter = variables.keySet().iterator();
        while(keyIter.hasNext())
        {
            String curKey = keyIter.next();
            parserTemplate += "DATA[\""+curKey+"\"] = splitRecords["+(variables.get(curKey)-1)+"] ?: [];\n";
        }

        parserTemplate += "if(isTest)\r\n" +
        "{\r\n" +
            "   DATA[\"regex matched\"] = regexMatched\r\n" + 
            "   DATA[\"parsing content\"] = parsingContent\r\n" + 
        "}\r\n\r\n";
        
        //add WSDATA
        for(String key : wsdataVariables)
        {
            parserTemplate += "WSDATA[\""+key+"\"] = DATA[\""+key+"\"]\r\n"; 
        }
        //add FLOW
        for(String key : flowVariables)
        {
            parserTemplate += "FLOWS[\""+key+"\"] = DATA[\""+key+"\"]\r\n"; 
        }
        //add OUTPUT
        for(String key : outputVariables)
        {
            parserTemplate += "OUTPUTS[\""+key+"\"] = DATA[\""+key+"\"]\r\n"; 
        }

        parserTemplate += "\r\nDATA";
        
        return parserTemplate;
    }
}
