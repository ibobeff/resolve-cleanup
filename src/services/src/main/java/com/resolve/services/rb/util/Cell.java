/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

public class Cell
{
    private String style;
    private int parent=1; //have seen only value=1 all the time 
    private int source; //id of the source node 
    private int target; //id of the target node 
    private int edge; //have seen only value=1 all the time 
    private int vertex; //have seen only value=1 all the time 
    private Geometry geometry;

    public Cell()
    {
        
    }
    
    public Cell(String style, int parent, int source, int target, int edge, int vertex, Geometry geometry)
    {
        super();
        this.style = style;
        this.parent = parent;
        this.source = source;
        this.target = target;
        this.edge = edge;
        this.vertex = vertex;
        this.geometry = geometry;
    }
    
    public String getStyle()
    {
        return style;
    }
    public void setStyle(String style)
    {
        this.style = style;
    }
    public int getParent()
    {
        return parent;
    }
    public void setParent(int parent)
    {
        this.parent = parent;
    }
    public int getSource()
    {
        return source;
    }
    public void setSource(int source)
    {
        this.source = source;
    }
    public int getTarget()
    {
        return target;
    }
    public void setTarget(int target)
    {
        this.target = target;
    }
    public int getEdge()
    {
        return edge;
    }
    public void setEdge(int edge)
    {
        this.edge = edge;
    }
    public int getVertex()
    {
        return vertex;
    }
    public void setVertex(int vertex)
    {
        this.vertex = vertex;
    }
    public Geometry getGeometry()
    {
        return geometry;
    }
    public void setGeometry(Geometry geometry)
    {
        this.geometry = geometry;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("      <mxCell ").append("style=\"" + style + "\" ").append("parent=\"" + parent + "\"");
        if(vertex > 0)
        {
            sb.append(" vertex=\"" + vertex + "\"");
        }
        if(source > 0)
        {
            sb.append(" source=\"" + source + "\"");
        }
        if(target > 0)
        {
            sb.append(" target=\"" + target + "\"");
        }
        if(edge > 0)
        {
            sb.append(" edge=\"" + edge + "\"");
        }
        sb.append(">\n");
        if(geometry != null)
        {
            sb.append(geometry.toString());
        }
        sb.append("      </mxCell>");

        return sb.toString();
    }
}
