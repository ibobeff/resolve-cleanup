/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.social;

import java.util.Date;
import java.util.Map;

import com.resolve.services.interfaces.SocialDTO;

public class SocialRssDTO extends SocialDTO
{
    private static final long serialVersionUID = 5239217788511176670L;
    public final static String TABLE_NAME = "rss_subscription";
    
    private String u_rss_owner;
    private String u_url;
    private Date u_feed_updated;
    private String u_schedule;

    public SocialRssDTO() {}
    public SocialRssDTO(Map<String, Object> data)
    {
        super(data);
    }
    public String getU_rss_owner()
    {
        return u_rss_owner;
    }
    public void setU_rss_owner(String u_rss_owner)
    {
        this.u_rss_owner = u_rss_owner;
    }
    
    public String getU_url()
    {
        return u_url;
    }
    public void setU_url(String u_url)
    {
        this.u_url = u_url;
    }
    public Date getU_feed_updated()
    {
        return u_feed_updated;
    }
    public void setU_feed_updated(Date u_feed_updated)
    {
        this.u_feed_updated = u_feed_updated;
    }
    public String getU_schedule()
    {
        return u_schedule;
    }
    public void setU_schedule(String u_schedule)
    {
        this.u_schedule = u_schedule;
    }
    
    
    
}
