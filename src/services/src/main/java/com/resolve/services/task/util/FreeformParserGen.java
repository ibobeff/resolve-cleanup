package com.resolve.services.task.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class FreeformParserGen extends AbstractParserGen
{

    String repeatRegex = "";
    public FreeformParserGen(String regex, String parseStart, String parseEnd, String parseType, HashMap<String,Integer> variables, Set<String> flowVariables, Set<String> wsdataVariables, Set<String> outputVariables, HashMap<String, Boolean> options)
    {
        super(regex, parseStart, parseEnd, parseType, variables, flowVariables, wsdataVariables, outputVariables, options);
    }

    
    String generateCode()
    {
        String template = getParserTemplate();
        if(regex == null || regex.equals(""))
        {
            regex = "\"\"";
        }
        else
        {
            regex = "/"+regex+"/";
        }
        if(parseStart == null || parseStart.equals(""))
        {
            parseStart = "\"\"";
        }
        else
        {
            parseStart = "/"+parseStart+"/";
        }
        if(parseEnd == null || parseEnd.equals(""))
        {
            parseEnd = "\"\"";
        }
        else
        {
            parseEnd = "/"+parseEnd+"/";
        }
        template = template.replace("<==REGEX==>", regex);
        template = template.replace("<==PARSE_START==>", parseStart);
        template = template.replace("<==PARSE_END==>", parseEnd);
        
        String rawTrimmingRegexExpression = "(?s)";
        if(options.get("boundaryStartIncluded"))
            rawTrimmingRegexExpression += "($parseStart";
        else 
            rawTrimmingRegexExpression += "$parseStart(";
        if( !parseEnd.equals("") && parseEnd != null && !parseEnd.equals("\"\"") && !parseEnd.contains("(?:\\n|\\r\\n)"))
            rawTrimmingRegexExpression += ".*?";
        else
            rawTrimmingRegexExpression += ".*";
        if(options.get("boundaryEndIncluded"))            
            rawTrimmingRegexExpression += "$parseEnd)";
        else 
            rawTrimmingRegexExpression += ")$parseEnd";
        
        template = template.replace("<==RAW_TEXT_TRIM_REGEX==>", rawTrimmingRegexExpression);
        
        Boolean repeat = options.get("repeat");
        if(repeat ==  null)
        {
            repeat = false;
        }
        
        repeatRegex = regex.replace("(?s)","(?g)");
        
        template = template.replace("<==REPEAT==>", repeat.toString());
        
        
        return template;
    }

    String getParserTemplate()
    {
        String template = ""+
        "def rawTerminalText = RAW;\r\n" + 
        "def isTest = INPUTS[\"TEST\"];\r\n" + 
        "def parsingContent = \"\";\r\n" + 
		"\r\n" + 
		"def regex = <==REGEX==>;\r\n" + 
		"def parseStart = <==PARSE_START==>;\r\n" + 
		"def parseEnd = <==PARSE_END==>;\r\n" + 
		"def repeat = <==REPEAT==>;" +
		"\r\n" + 
		"def extractedGroups = [];\r\n" + 
		"\r\n" + 
        "def regexMatched = false\r\n" +
		"if(rawTerminalText)\r\n" + 
		"{\r\n" + 
		"    //extract From parseStart to parseEnd\r\n" + 
		"    def sampleTrimmedMatcher = rawTerminalText =~ /<==RAW_TEXT_TRIM_REGEX==>/\r\n" + 
		"    if(sampleTrimmedMatcher.find())\r\n" + 
		"    {\r\n" + 
		"        parsingContent = sampleTrimmedMatcher.group(1);\r\n" + 
		"    }\r\n" + 
		"}\r\n" + 
		"\r\n" + 
		"def matcher = parsingContent =~ regex\r\n" + 
		"while(matcher.find())\r\n" + 
		"{\r\n" + 
		"    if(!regexMatched) regexMatched = true\r\n" + 
		"    if(repeat)\r\n" + 
		"    {\r\n" + 
		"        for(def groupNum = 1; groupNum <= matcher.groupCount();groupNum++)\r\n" + 
		"        {\r\n" + 
		"            def curGroupList = [];\r\n" + 
		"            def groupListIndex = groupNum-1;\r\n" + 
		"            if(extractedGroups.size() > groupListIndex)\r\n" + 
		"            {\r\n" + 
		"                curGroupList = extractedGroups.get(groupListIndex);\r\n" + 
		"                curGroupList.add(matcher.group(groupNum).trim());\r\n" + 
		"                extractedGroups.set(groupListIndex,curGroupList);\r\n" + 
		"            }\r\n" + 
		"            else\r\n" + 
		"            {\r\n" + 
		"                curGroupList.add(matcher.group(groupNum).trim());\r\n" + 
		"                extractedGroups.add(curGroupList);\r\n" + 
		"            }\r\n" + 
		"        }\r\n" + 
		"    }\r\n" + 
		"    else if(!repeat)\r\n" + 
		"    {\r\n" + 
		"        for(def groupNum = 1; groupNum <= matcher.groupCount();groupNum++)\r\n" + 
		"        {\r\n" + 
		"            extractedGroups.add(matcher.group(groupNum).trim());\r\n" + 
		"        }\r\n" + 
		"        //want to break from while to only process first match in not repeat condition\r\n" + 
		"        break;\r\n" + 
		"    }\r\n" + 
		"}"+
		"\r\n" +
		"DATA = [:];\n";

        //Assign variables to extracted data        
        Iterator<String> keyIter = variables.keySet().iterator();
        while(keyIter.hasNext())
        {
            String curKey = keyIter.next();
            template += "DATA[\""+curKey+"\"] = extractedGroups["+(variables.get(curKey)-1)+"] ?: [];\n";
        }
        
        template += "if(isTest)\r\n" +
        "{\r\n" +
            "   DATA[\"regex matched\"] = regexMatched\r\n" + 
            "   DATA[\"parsing content\"] = parsingContent\r\n" + 
        "}\r\n\r\n";

        //add WSDATA
        for(String key : wsdataVariables)
        {
            template += "WSDATA[\""+key+"\"] = DATA[\""+key+"\"]\r\n"; 
        }
        //add FLOW
        for(String key : flowVariables)
        {
            template += "FLOWS[\""+key+"\"] = DATA[\""+key+"\"]\r\n"; 
        }
        //add OUTPUT
        for(String key : outputVariables)
        {
            template += "OUTPUTS[\""+key+"\"] = DATA[\""+key+"\"]\r\n"; 
        }

        template += "\r\nDATA\n";
        return template;
    }

}
