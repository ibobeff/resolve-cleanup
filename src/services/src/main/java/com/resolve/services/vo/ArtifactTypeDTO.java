package com.resolve.services.vo;

import java.util.Set;

public class ArtifactTypeDTO {
	private String UName;
	private Set<GenericDictionaryItemDTO> dictionaryItems;
	private Boolean UStandard;

	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	public Set<GenericDictionaryItemDTO> getDictionaryItems() {
		return dictionaryItems;
	}

	public void setDictionaryItems(Set<GenericDictionaryItemDTO> dictionaryItems) {
		this.dictionaryItems = dictionaryItems;
	}

	public Boolean getUStandard() {
		return UStandard;
	}

	public void setUStandard(Boolean uStandard) {
		UStandard = uStandard;
	}
}
