/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

public class ChartRatingDTO
{
    private String name;
    private int data1;
    
    public ChartRatingDTO(String name, int data1)
    {
        this.name = name;
        this.data1 = data1;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public int getData1()
    {
        return data1;
    }
    public void setData1(int data1)
    {
        this.data1 = data1;
    }
}
