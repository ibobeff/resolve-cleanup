/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.search.model.UserInfo;
import com.resolve.search.wiki.WikiIndexAPI;
import com.resolve.search.wiki.WikiSearchAPI;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.constants.WikiDocStatsCounterName;
import com.resolve.services.exception.MissingVersionException;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.ResolveNamespaceUtil;
import com.resolve.services.hibernate.util.UpdateWikiDocRelations;
import com.resolve.services.hibernate.util.WikiAttachmentUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.NamespaceVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikiArchiveVO;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocResolutionRatingVO;
import com.resolve.services.hibernate.vo.WikidocStatisticsVO;
import com.resolve.services.hibernate.wiki.AddRemoveRolesUtil;
import com.resolve.services.hibernate.wiki.AttachmentHelper;
import com.resolve.services.hibernate.wiki.DeleteHelper;
import com.resolve.services.hibernate.wiki.DocUtils;
import com.resolve.services.hibernate.wiki.NamespaceUtil;
import com.resolve.services.hibernate.wiki.RatingUtil;
import com.resolve.services.hibernate.wiki.SearchWikiAttachments;
import com.resolve.services.hibernate.wiki.WikiArchiveUtil;
import com.resolve.services.hibernate.wiki.WikiPageInfoHelper;
import com.resolve.services.hibernate.wiki.WikiStatusEnum;
import com.resolve.services.hibernate.wiki.WikiStatusUtil;
import com.resolve.services.hibernate.wiki.reports.DTExecutionReport;
import com.resolve.services.hibernate.wiki.reports.DTPathDetailReport;
import com.resolve.services.hibernate.wiki.reports.DTPathTrendReport;
import com.resolve.services.vo.FeedbackDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.WikiPageInfoDTO;
import com.resolve.services.vo.WikidocRatingDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.wiki.dto.AttachmentDTO;

import net.sf.json.JSONObject;

public class ServiceWiki
{
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Wiki CRUD operations
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // get list of users based on Query
    public static List<WikiDocumentVO> getWikiDocuments(QueryDTO query, NodeType type, String username) throws Exception
    {
        return WikiUtils.getWikiDocuments(query, type, username);
    }

    public static List<WikiDocumentVO> searchWikiDocsUsingHQL(String whereClause, String username) throws Exception
    {
        return WikiUtils.searchWikiDocsUsingHQL(whereClause, username); 
    }
    
    public static WikiDocumentVO getWikiDoc(String sysId, String fullname, String username)
    {
        return WikiUtils.getWikiDoc(sysId, fullname, username);
    }

    public static WikiDocumentVO getWikiDocWithGraphRelatedDataNoLock(String sysId, String fullname, String username)
    {
        return WikiUtils.getWikiDocWithGraphRelatedDataNoLock(sysId, fullname, username);
    }

    public static WikiDocumentVO getWikiDocWithGraphRelatedData(String sysId, String fullname, String username, String version) throws Exception
    {
        return WikiUtils.getWikiDocWithGraphRelatedData(sysId, fullname, username, version);
    }
    
    public static WikiDocumentVO getWikiDocWithGraphRelatedData(String sysId, String fullname, String username)
    {
        return WikiUtils.getWikiDocWithGraphRelatedData(sysId, fullname, username);
    }

    public static WikiDocumentVO getDefaultPropertiesForNewWiki(String fullname, String username)
    {
        return WikiUtils.getDefaultPropertiesForNewWiki(fullname, username);
    }

    public static WikiDocumentVO saveWikiDoc(WikiDocumentVO doc, String username) throws Exception
    {
        WikiDocumentVO wiki = WikiUtils.saveWikiDoc(doc, null, username);
        if (wiki.ugetUIsDeleted())
        {
            WikiIndexAPI.setDeleted(wiki.getId(), username, true);
        }
        return wiki;
    }
    
	public static WikiDocumentVO saveAndCommitWikiDoc(WikiDocumentVO doc, String comment, String username, boolean isSave)
			throws Exception {
		return saveAndCommitWikiDoc(doc, comment, username, false, isSave);
	}

	public static WikiDocumentVO saveAndCommitWikiDoc(WikiDocumentVO doc, String comment, String username,
			boolean useNamespaceEntity, boolean isSave) throws Exception {
		WikiDocumentVO wiki = WikiUtils.saveWikiDoc(doc, comment, username, useNamespaceEntity, isSave);
		if (wiki.ugetUIsDeleted()) {
			WikiIndexAPI.setDeleted(wiki.getId(), username, true);
		}
		return wiki;
	}

    public static List<String> getAllWikiDocuments(String wikiname, boolean isRunbook)
    {
        return WikiUtils.getAllWikiDocuments(wikiname, isRunbook);
    }

    public static boolean hasRightsToDocument(String documentFullName, String username, RightTypeEnum rightType)
    {
        return WikiUtils.hasRightsToDocument(documentFullName, username, rightType);
    }

    public static boolean isWikidocExist(String fullname, boolean caseSensitive)
    {
        return WikiUtils.isWikidocExist(fullname, caseSensitive);
    }
    
    /**
     *
     * @param fullname
     * @return
     */
    public static String getWikidocSysId(String fullname)
    {
        return WikiUtils.getWikidocSysId(fullname);
    }

    /**
     *
     * @param runbookNameList
     * @return
     */
    public static List<String> runbooksNotFound(List<String> runbookNameList)
    {
        return WikiUtils.runbooksNotFound(runbookNameList);
    }

    /**
     *
     * @param runbookNameList
     * @param user
     * @return
     */
    public static List<String> runbooksEditNotAvailable(List<String> runbookNameList, String user)
    {
        return WikiUtils.runbooksEditNotAvailable(runbookNameList, user);
    }

    /**
     *
     * @param docFullName
     * @param sectionString
     */
    public static void addSectionToWikiDoc(String docFullName, String sectionString)
    {
        WikiUtils.addSectionToWikiDoc(docFullName, sectionString);
    }


    /**
     * checks if the wiki is active or not
     *
     * @param wiki
     * @return
     * @throws Exception 
     */
    public static boolean isWikiDocumentExecutable(String wiki) throws Exception
    {
        return WikiUtils.isWikiDocumentExecutable(wiki);
    }
    
    @SuppressWarnings("unchecked")
    public static void updateWikiRelationships(Map<String, Object> params)
    {
        if (params != null)
        {
            // stats update
            WikiDocStatsCounterName wikiDocStatsCounterName = (WikiDocStatsCounterName) params.get(HibernateConstants.WIKIDOC_STATS_COUNTER_NAME_KEY);
            String docId = (String) params.get(ConstantValues.WIKI_DOCUMENT_SYS_ID);
            if (StringUtils.isNotEmpty(docId) && wikiDocStatsCounterName != null)
            {
                UpdateWikiDocRelations.updateWikiDocStatsCounter(wikiDocStatsCounterName, docId, (String)params.get("USERNAME"));
            }

            // if there is list of ids - usually for impex
            List<String> docSysIds = params.containsKey(ConstantValues.WIKI_DOCUMENT_SYS_IDS) ? (List<String>) params.get(ConstantValues.WIKI_DOCUMENT_SYS_IDS) : null;
            if (docSysIds != null && wikiDocStatsCounterName != null)
            {
                UpdateWikiDocRelations.updateWikiDocStatsCounter(wikiDocStatsCounterName, docSysIds, (String)params.get("USERNAME"));
            }

            // load the doc
            boolean updateContent = params.containsKey(ConstantValues.WIKI_DOCUMENT_CONTENT) ? (Boolean) params.get(ConstantValues.WIKI_DOCUMENT_CONTENT) : false;
            boolean updateMain = params.containsKey(ConstantValues.WIKI_DOCUMENT_MODEL) ? (Boolean) params.get(ConstantValues.WIKI_DOCUMENT_MODEL) : false;
            boolean updateException = params.containsKey(ConstantValues.WIKI_DOCUMENT_EXCEPTION) ? (Boolean) params.get(ConstantValues.WIKI_DOCUMENT_EXCEPTION) : false;
            boolean updateDTree = params.containsKey(ConstantValues.WIKI_DOCUMENT_DT) ? (Boolean) params.get(ConstantValues.WIKI_DOCUMENT_DT) : false;

            if (updateContent || updateMain || updateException || updateDTree)
            {
                if (StringUtils.isNotBlank(docId))
                {
                    UpdateWikiDocRelations.updateAllRelationForDocument(docId, updateContent, updateMain, updateException, updateDTree);
                }

                if (docSysIds != null)
                {
                    UpdateWikiDocRelations.updateAllRelationForDocument(docSysIds, updateContent, updateMain, updateException, updateDTree);
                }
            }// end of if

        }
    }
    
    public static WikidocStatisticsVO findWikidocStatisticsForWiki(String docSysId, String docFullName, String username) throws Exception
    {
        return WikiUtils.findWikidocStatisticsForWiki(docSysId, docFullName, username);
    }

        

    /**
     * /**
     *
     * API to create a wiki document
     *
     * List of keys that this API expects: MANDATORY USERNAME namespace -->
     * namespace of the wikidocument that will be created with docname --> wiki
     * document name content --> content for the wikidocument. This will get
     * generated based on the template OPTIONAL summary title tags mainXML
     * finalXML abortXML readRoles writeRoles adminRoles executeRoles
     *
     *
     * @param params
     * @param username
     * @throws Exception
     */
    public static WikiDocumentVO createWiki(Map<String, Object> params, String username) throws Exception
    {
        return WikiUtils.createWiki(params, username);
    }

    public static WikiDocumentVO updateWiki(Map<String, Object> params, String username) throws Exception
    {
        return WikiUtils.updateWiki(params, username);
    }

    public static List<WikiDocumentVO> getListOfHelpDocuments(String username) throws Exception
    {
        return WikiUtils.getListOfHelpDocuments(username);
    }

    public static void moveRenameDocuments(List<String> docSysIds, String newNamespace, boolean overwrite, String username) throws Exception
    {
        WikiUtils.moveRenameDocuments(docSysIds, newNamespace, overwrite, username);
    }

	public static void moveRenameDocuments(List<String> docSysIds, String newNamespace, boolean overwrite,
			String username, boolean useNamespaceEntity) throws Exception {
		WikiUtils.moveRenameDocuments(docSysIds, newNamespace, overwrite, username, useNamespaceEntity);
	}

    public static WikiDocumentVO moveRenameDocument(String fromDocument, String toDocument, boolean overwrite, boolean validateUser, String username) throws Exception
    {
        return WikiUtils.moveRenameDocument(fromDocument, toDocument, overwrite, validateUser, username);
    }

	public static WikiDocumentVO moveRenameDocument(String fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username, boolean useNamespaceEntity) throws Exception {
		return WikiUtils.moveRenameDocument(fromDocument, toDocument, overwrite, validateUser, username, useNamespaceEntity);
	}

    public static void moveRenameDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, boolean overwrite, String username) throws Exception
    {
        WikiUtils.moveRenameDocumentsForNamepaces(fromNamespaces, toNamespace, overwrite, username);
    }

	public static void moveRenameDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace,
			boolean overwrite, String username, boolean useNamespaceEntity) throws Exception {
		WikiUtils.moveRenameDocumentsForNamepaces(fromNamespaces, toNamespace, overwrite, username, useNamespaceEntity);
	}

    public static void moveRenameDocumentsForNamepace(String fromNamespace, String toNamespace, boolean overwrite, String username) throws Exception
    {
        WikiUtils.moveRenameDocumentsForNamepace(fromNamespace, toNamespace, overwrite, username);
    }

    public static void copyDocuments(List<String> docSysIds, String newNamespace, boolean overwrite, String username) throws Exception
    {
        WikiUtils.copyDocuments(docSysIds, newNamespace, overwrite, username);
    }

	public static void copyDocuments(List<String> docSysIds, String newNamespace, boolean overwrite, String username,
			boolean useNamespaceEntity) throws Exception {
		WikiUtils.copyDocuments(docSysIds, newNamespace, overwrite, username, useNamespaceEntity);
	}

    public static WikiDocumentVO copyDocument(String fromDocument, String toDocument, boolean overwrite, boolean validateUser, String username) throws Exception
    {
        return WikiUtils.copyDocument(fromDocument, toDocument, overwrite, validateUser, username);
    }

	public static WikiDocumentVO copyDocument(String fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username, boolean useNamespaceEntity) throws Exception {
		return WikiUtils.copyDocument(fromDocument, toDocument, overwrite, validateUser, username, useNamespaceEntity);
	}

	public static WikiDocumentVO copyDocument(WikiDocumentVO fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username, boolean useNamespaceEntity) throws Exception {
		return WikiUtils.copyDocument(fromDocument, toDocument, overwrite, validateUser, username, useNamespaceEntity);
	}

    public static void copyDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, boolean overwrite, String username) throws Exception
    {
        WikiUtils.copyDocumentsForNamepaces(fromNamespaces, toNamespace, overwrite, username);
    }

	public static void copyDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, boolean overwrite,
			String username, boolean useNamespaceEntity) throws Exception {
		WikiUtils.copyDocumentsForNamepaces(fromNamespaces, toNamespace, overwrite, username, useNamespaceEntity);
	}

    public static void copyDocumentsForNamepace(String fromNamespace, String toNamespace, boolean overwrite, String username) throws Exception
    {
        WikiUtils.copyDocumentsForNamepace(fromNamespace, toNamespace, overwrite, username);
    }

    public static void replaceDocumentsForNamepace(String fromNamespace, String toNamespace, String username) throws Exception
    {
        WikiUtils.replaceDocumentsForNamepace(fromNamespace, toNamespace, username);
    }

    public static void replaceDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, String username) throws Exception
    {
        WikiUtils.replaceDocumentsForNamepaces(fromNamespaces, toNamespace, username);
    }

	public static void replaceDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, String username,
			boolean useNamespaceEntity) throws Exception {
		WikiUtils.replaceDocumentsForNamepaces(fromNamespaces, toNamespace, username, useNamespaceEntity);
	}

    public static void replaceDocuments(List<String> docSysIds, String newNamespace, String username) throws Exception
    {
        WikiUtils.replaceDocuments(docSysIds, newNamespace, username);
    }

	public static void replaceDocuments(List<String> docSysIds, String newNamespace, String username,
			boolean useNamespaceEntity) throws Exception {
		WikiUtils.replaceDocuments(docSysIds, newNamespace, username, useNamespaceEntity);
	}

    public static WikiDocumentVO replaceDocument(String fromDocument, String toDocument, boolean validateUser, String username) throws Exception
    {
        return WikiUtils.replaceDocument(fromDocument, toDocument, validateUser, username);
    }

	public static WikiDocumentVO replaceDocument(String fromDocument, String toDocument, boolean validateUser,
			String username, boolean useNamespaceEntity) throws Exception {
		return WikiUtils.replaceDocument(fromDocument, toDocument, validateUser, username, useNamespaceEntity);
	}

    public static ResponseDTO<NamespaceVO> getDocumentNamespaces(QueryDTO query, String username)
    {
        return NamespaceUtil.getDocumentNamespaces(query, username);
    }

    public static void updateDeleteForWiki(Set<String> docSysIds, boolean deleted, String username) throws Exception
    {
        ServiceWiki.updateWikiStatusFlag(docSysIds, WikiStatusEnum.DELETED, deleted, true, username);
        // DeleteHelper.updateDeleteForWiki(docSysIds, deleted, username);
    }

    public static void updateDeleteForNamespace(Set<String> namespaces, boolean deleted, String username) throws Exception
    {
        ServiceWiki.updateWikiStatusFlagForNamespaces(namespaces, WikiStatusEnum.DELETED, deleted, false, username);
        // DeleteHelper.updateDeleteForNamespaces(namespaces, deleted,
        // username);
    }

    public static void deleteWikiDocument(String sysId, String docFullName, String username) throws Exception
    {
        DeleteHelper.deleteWikiDocument(sysId, docFullName, username);
    }

    public static void deleteWikiDocuments(Set<String> docSysIds, String username) throws Exception
    {
        DeleteHelper.deleteWikiDocuments(docSysIds, username);
    }

    public static void deleteWikiForNamespaces(Set<String> namespaces, String username) throws Exception
    {
        DeleteHelper.deleteWikiForNamespaces(namespaces, username);
    }
    
    public static void deleteAllWikiDocuments(String username) throws Exception
    {
        DeleteHelper.deleteAllWikiDocuments(username);
    }
    
    public static void setSoftLock(String docFullName, boolean value, String username)
    {
        if(value)
        {
            DocUtils.lock(username, docFullName);
        }
        else
        {
            DocUtils.unlock(docFullName, username);
        }
    }
    
    public static synchronized void requestSoftLock(String username, String docFullName)
    {
        DocUtils.requestSoftLock(username, docFullName);
    }

    public static void updateWikiStatusFlag(Set<String> docSysIds, WikiStatusEnum field, boolean value, boolean sendNotification, String username) throws Exception
    {
        WikiStatusUtil.updateWikiStatusFlag(docSysIds, field, value, sendNotification, username);
    }

    public static void updateWikiStatusFlagForNamespaces(Set<String> namespaces, WikiStatusEnum field, boolean value, boolean sendNotification, String username) throws Exception
    {
        WikiStatusUtil.updateWikiStatusFlagForNamespaces(namespaces, field, value, sendNotification, username);
    }

    public static void updateLastReviewedDateForWikiDocs(Set<String> docSysIds, String username) throws Exception
    {
        WikiUtils.updateDocumentsAsReviewed(docSysIds, username);
    }

    public static void updateLastReviewedDateForNamespace(Set<String> namespaces, String username) throws Exception
    {
        WikiUtils.updateLastReviewedDateForNamespace(namespaces, username);
    }

	public static void updateLastReviewedDateForNamespace(Set<String> namespaces, String username, boolean useNamespaceEntity)
			throws Exception {
		WikiUtils.updateLastReviewedDateForNamespace(namespaces, username, useNamespaceEntity);
	}

    public static void updateExpirationDateForWikiDocs(Set<String> docSysIds, Date expireDate, String username) throws Exception
    {
        WikiUtils.updateExpirationDateForWikiDocs(docSysIds, expireDate, username);
    }
    
    public static void updateExpirationDateForWikiDocs(Map<String, Date> params, String username) throws Exception
    {
        WikiUtils.updateExpirationDateForWikiDocs(params, username);
    }

    public static void updateExpirationDateForNamespace(Set<String> namespaces, Date expireDate, String username) throws Exception
    {
        WikiUtils.updateExpirationDateForNamespace(namespaces, expireDate, username);
    }

	public static void updateExpirationDateForNamespace(Set<String> namespaces, Date expireDate, String username,
			boolean useNamespaceEntity) throws Exception {
		WikiUtils.updateExpirationDateForNamespace(namespaces, expireDate, username, useNamespaceEntity);
	}

    public static void updateExpirationDateForNamespace(Map<String, Date> params, String username) throws Exception
    {
        WikiUtils.updateExpirationDateForNamespace(params, username);
    }

    public static WikidocResolutionRatingVO getResolutionRatingForDocument(String docSysId, String docFullName, String username) throws Exception
    {
        return RatingUtil.getResolutionRatingForDocument(docSysId, docFullName, username);
    }

    public static void updateResolutionRatingCountForNamespaces(Set<String> namespaces, WikidocResolutionRatingVO rating, String username) throws Exception
    {
        RatingUtil.updateResolutionRatingCountForNamespaces(namespaces, rating, username);
    }

    public static void updateResolutionRatingCount(Set<String> wikidocumentId, WikidocResolutionRatingVO rating, String username) throws WikiException
    {
        RatingUtil.updateResolutionRatingCount(wikidocumentId, rating, username);
    }

    public static void updateWeightForNamespaces(Set<String> namespaces, double weight, String username) throws WikiException
    {
        RatingUtil.updateWeightForNamespaces(namespaces, weight, username);
    }

    public static void updateWikidocsWeight(Set<String> wikidocumentId, double weight, String username) throws WikiException
    {
        RatingUtil.updateWikidocsWeight(wikidocumentId, weight, username);
    }
    
    public static void incrementRating(String docSysId, String docFullName, int rating, long multiplier, String username) throws Exception
    {
        RatingUtil.incrementRating(docSysId, docFullName, rating, multiplier, username);
    }
    
    public static void initRating(String docSysId, String docFullName, int total, int count, String username) throws Exception
    {
        RatingUtil.initRating(docSysId, docFullName, total, count, username);
    }

    public static void updateHomePageForRoles(String docSysId, Set<String> roleSysIds, String username) throws Exception
    {
        WikiUtils.updateHomePageForRoles(docSysId, roleSysIds, username);
    }

    public static void assignRoles(Set<String> docSysIds, AccessRightsVO roles, boolean defaultRights, String username) throws Exception
    {
        AddRemoveRolesUtil.setRolesToWikiDocuments(docSysIds, roles, defaultRights, username);
    }

    //this is action from the Namespace Admin --> Set Roles 
    public static void setRolesForNamespaces(Set<String> namespaces, AccessRightsVO roles, boolean defaultRights, String username) throws Exception
    {
        AddRemoveRolesUtil.setRolesForNamespaces(namespaces, roles, defaultRights, username);
    }

	public static void setRolesForNamespaces(Set<String> namespaces, AccessRightsVO roles, boolean defaultRights,
			String username, boolean useNamespaceEntity) throws Exception {
		AddRemoveRolesUtil.setRolesForNamespaces(namespaces, roles, defaultRights, username, useNamespaceEntity);
	}

    //this is action from the Namespace admin --> Set Namespace Default
    public static void setNamespaceDefaultRoles(Set<String> namespaces, AccessRightsVO roles, String username) throws Exception
    {
        AddRemoveRolesUtil.setNamespaceDefaultRoles(namespaces, roles, username);
    }

	public static void setNamespaceDefaultRoles(Set<String> namespaces, AccessRightsVO roles, String username,
			boolean useNamespaceEntity) throws Exception {
		AddRemoveRolesUtil.setNamespaceDefaultRoles(namespaces, roles, username, useNamespaceEntity);
	}

    public static void addTagsToWiki(Set<String> docSysIds, Set<String> tagSysIds, String username) throws Exception
    {
        WikiUtils.addTagsToWikis(docSysIds, tagSysIds, username);
    }

	public static void addTagsToWikiNamespaces(Set<String> namespaces, Set<String> tagSysIds, String username) throws Exception {
		addTagsToWikiNamespaces(namespaces, tagSysIds, username, false);
	}
	
	public static void addTagsToWikiNamespaces(Set<String> namespaces, Set<String> tagSysIds, String username,
			boolean useNamespaceEntity) throws Exception {
		if (CollectionUtils.isNotEmpty(namespaces) && CollectionUtils.isNotEmpty(tagSysIds)) {
			for (String namespace : namespaces) {
				try {
					Set<String> docSysIds = null;
					if (useNamespaceEntity) {
						docSysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace);
					} else {
						docSysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace, username);
					}
					addTagsToWiki(docSysIds, tagSysIds, username);
				} catch (Exception e) {
					Log.log.error("error assigning tags to namespace : " + namespace, e);
				}
			}
		}
	}

    public static void removeTagsFromWiki(Set<String> docSysIds, Set<String> tagSysIds, String username) throws Exception
    {
        WikiUtils.removeTagsFromWikis(docSysIds, tagSysIds, username);
    }

	public static void removeTagsFromWikiNamespaces(Set<String> namespaces, Set<String> tagSysIds, String username) throws Exception {
		removeTagsFromWikiNamespaces(namespaces, tagSysIds, username, false);
	}
	
	public static void removeTagsFromWikiNamespaces(Set<String> namespaces, Set<String> tagSysIds, String username,
			boolean useNamespaceEntity) throws Exception {
		if (CollectionUtils.isNotEmpty(namespaces) && CollectionUtils.isNotEmpty(tagSysIds)) {
			for (String namespace : namespaces) {
				try {
					Set<String> docSysIds = null;
					if (useNamespaceEntity) {
						docSysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace);
					} else {
						docSysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace, username);
					}
					removeTagsFromWiki(docSysIds, tagSysIds, username);
				} catch (Exception e) {
					Log.log.error("error removing tags to namespace : " + namespace, e);
				}
			}
		}
	}

    public static void indexWikiDocument(String sysId, String username)
    {
        if (StringUtils.isNotBlank(sysId))
        {
            WikiIndexAPI.indexWikiDocument(sysId, true, username);
        }
    }
    
    public static void indexWikiDocuments(Set<String> sysIds, boolean indexAttachment, String username)
    {
        if (sysIds != null && sysIds.size() > 0)
        {
            WikiIndexAPI.indexWikiDocuments(sysIds, indexAttachment, username);
        }
    }

    public static void indexWikiAttachments(String docSysId, String docFullName, Set<String> attachSysIds, String username) throws Exception
    {
        WikiDocumentVO doc = WikiUtils.validateUserForDocumentVO(docSysId, docFullName, RightTypeEnum.view, username);
        if (doc != null && attachSysIds != null && attachSysIds.size() > 0)
        {
            WikiIndexAPI.indexWikiAttachments(doc.getSys_id(), attachSysIds, username);
        }
    }

	public static void indexWikiNamespaces(Set<String> namespaces, boolean indexAttachment, String username) {
		indexWikiNamespaces(namespaces, indexAttachment, username, false);
	}
    
    public static void indexWikiNamespaces(Set<String> namespaces, boolean indexAttachment, String username, boolean useNamespaceEntity)
    {
        if (namespaces != null && namespaces.size() > 0)
        {
            for (String namespace : namespaces)
            {
                try
                {
					Set<String> docSysIds = null;
					if (useNamespaceEntity) {
						docSysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace);
					} else {
						docSysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace, username);
					}
                    indexWikiDocuments(docSysIds, indexAttachment, username);
                }
                catch (Exception e)
                {
                    Log.log.error("error indexing tags to namespace : " + namespace, e);
                }
            }
        }
    }

    public static void purgeWikiDocumentIndexes(Set<String> sysIds, String username)
    {
        if (sysIds != null && sysIds.size() > 0)
        {
            WikiIndexAPI.purgeWikiDocuments(sysIds, true, username);
        }
    }

	public static void purgeWikiNamespaces(Set<String> namespaces, String username) {
		purgeWikiNamespaces(namespaces, username, false);
	}
    
    public static void purgeWikiNamespaces(Set<String> namespaces, String username, boolean useNamespaceEntity)
    {
        if (namespaces != null && namespaces.size() > 0)
        {
            for (String namespace : namespaces)
            {
                try
                {
					Set<String> docSysIds = null;
					if (useNamespaceEntity) {
						docSysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace);
					} else {
						docSysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace, username);
					}
                    purgeWikiDocumentIndexes(docSysIds, username);
                }
                catch (Exception e)
                {
                    Log.log.error("error purging ns indexes: " + namespace, e);
                }
            }
        }
    }

    public static void purgeWikiAttachmentIndexes(Set<String> attachSysIds, String docId, String username) throws Exception
    {
        if (attachSysIds != null && attachSysIds.size() > 0)
        {
            WikiIndexAPI.purgeWikiAttachments(attachSysIds, docId, username);
        }
    }

    public static void indexAllWikiDocuments(boolean indexAttachment, String username)
    {
        // gather all the sysIds
        Set<String> sysIds = WikiUtils.getAllWikidocSysIds();
        WikiIndexAPI.indexWikiDocuments(sysIds, indexAttachment, username);
    }
    
    public static void purgeAllWikiDocumentIndexes(String username)
    {
        WikiIndexAPI.purgeAllWikiDocuments(username);
    }

    public static void archiveDocuments(Set<String> sysIds, String comment, String username) throws Exception
    {
        WikiUtils.archiveDocuments(sysIds, comment, username);
    }

    public static void archiveNamespaces(Set<String> namespaces, String comment, String username) throws Exception
    {
        WikiUtils.archiveNamespaces(namespaces, comment, username);
    }

    public static void resetWikidocStats(Set<String> sysIds, String username) throws Exception
    {
        WikiUtils.resetWikidocStats(sysIds, username);
    }

    public static void resetNamespaces(Set<String> namespaces, String username) throws Exception
    {
        WikiUtils.resetNamespaces(namespaces, username);
    }

	public static void resetNamespaces(Set<String> namespaces, String username, boolean useNamespaceEntity) throws Exception {
		WikiUtils.resetNamespaces(namespaces, username, useNamespaceEntity);
	}

    public static List<AccessRightsVO> getAccessRightsFor(Set<String> docSysIds, String username) throws Exception
    {
        return WikiUtils.getAccessRightsFor(docSysIds, username);
    }

    public static List<AccessRightsVO> getAccessRightsForNamespaces(Set<String> namespaces, String username) throws Exception
    {
        return WikiUtils.getAccessRightsForNamespaces(namespaces, username);
    }

    public static List<AccessRightsVO> getDefaultAccessRightsForNamespaces(Set<String> namespaces, String username) throws Exception
    {
        return WikiUtils.getDefaultAccessRightsForNamespaces(namespaces, username);
    }

    public static List<ResolveTagVO> getTagsForDocuments(Set<String> docSysIds, String username) throws Exception
    {
        return WikiUtils.getTagsForDocuments(docSysIds, username);
    }
    
    public static List<ResolveTagVO> getTagsForDocuments(List<String> docSysIds, String username) throws Exception
    {
        Set<String> docSysIdsSet = new HashSet<String>(docSysIds);
        return WikiUtils.getTagsForDocuments(docSysIdsSet, username);
    }

    public static List<ResolveTagVO> getTagsForNamespaces(Set<String> namespaces, String username) throws Exception
    {
        return WikiUtils.getTagsForNamespaces(namespaces, username);
    }
    
    public static List<ResolveTagVO> getTagsForNamespaces(List<String> namespaces, String username) throws Exception
    {
        Set<String> namespacesSet = new HashSet<String>(namespaces);
        return WikiUtils.getTagsForNamespaces(namespacesSet, username);
    }

    public static void submitRequest(String docSysId, String docFullName, boolean review, String comment, 
    		String username)  throws Exception
    {
        WikiUtils.submitRequest(docSysId, docFullName, review, comment, username);
    }
    
    public static void submitRequest(String docSysId, String docFullName, String type, boolean review,  
    		boolean submitCR, String username) throws Exception
    {
        WikiUtils.submitRequest(docSysId, docFullName, type, review, submitCR, username);
    }

    // wiki attachments api
    public static WikiAttachmentVO getAttachmentWithContent(String sysId, String username) throws Exception
    {
        return WikiAttachmentUtil.getAttachmentWithContent(sysId, username);
    }
    
    public static WikiAttachmentVO findWikiAttachmentVO(String docName, String attachmentSysId, String fileName, boolean content) throws Exception
    {
        return WikiAttachmentUtil.findWikiAttachmentVO(docName, attachmentSysId, fileName, content);
    }

    public static Set<String> getAllAttachmentIds(String docSysId, String docFullName, String username) throws Exception
    {
        Set<String> result = new TreeSet<String>();
        
        List<AttachmentDTO> attachmentDTOs = WikiAttachmentUtil.getAllAttachmentsFor(docSysId, docFullName, username);
        if(attachmentDTOs != null)
        {
            for(AttachmentDTO dto : attachmentDTOs)
            {
                result.add(dto.getFileSysId()); 
            }
        }
        return result;
    }
    
    public static List<AttachmentDTO> getAllAttachmentsFor(String docSysId, String docFullName, String username) throws Exception
    {
        return WikiAttachmentUtil.getAllAttachmentsFor(docSysId, docFullName, username);
    }

    public static Collection<WikiAttachmentVO> addAttachmentsToWiki(String docSysId, String docFullName, Collection<WikiAttachmentVO> attachments, String username) throws Exception
    {
        AttachmentHelper attachmentHelper = new AttachmentHelper(docSysId, docFullName, attachments, username);
        return attachmentHelper.attach();
    }

    // convenience api
    public static WikiAttachmentVO addAttachmentToWiki(String docSysId, String docFullName, WikiAttachmentVO attachment, String username) throws Exception
    {
        WikiAttachmentVO attachmentVO = null;
        
        if (attachment != null)
        {
            List<WikiAttachmentVO> attachments = new ArrayList<WikiAttachmentVO>();
            attachments.add(attachment);

            Collection<WikiAttachmentVO> attachmentVos = addAttachmentsToWiki(docSysId, docFullName, attachments, username);
            if(attachmentVos != null && attachmentVos.size() > 0)
            {
                attachmentVO = attachmentVos.iterator().next();
            }
        }
        
        return attachmentVO;
    }

    public static void deleteAttachments(String docSysId, String docFullName, Set<String> attachSysIds, String username) throws Exception
    {
        WikiAttachmentUtil.deleteAttachments(docSysId, docFullName, attachSysIds, username);
    }

    public static void renameAttachment(String docSysId, String docFullName, String attachmentSysId, String newName,  boolean overwrite, String username) throws Exception
    {
        WikiAttachmentUtil.renameAttachment(docSysId, docFullName, attachmentSysId, newName,  overwrite, username);
    }

    public static ResponseDTO<AttachmentDTO> getAttachmentsFor(QueryDTO query, String docSysId, String docFullname, String username) throws Exception
    {
        SearchWikiAttachments searchAttachments = new SearchWikiAttachments(query, docSysId, docFullname, username);
        return searchAttachments.getAttachments();
    }

    public static ResponseDTO<com.resolve.search.model.WikiDocument> searchWikiDocuments(QueryDTO queryDTO, UserInfo user)
    {
        return WikiSearchAPI.searchWikiDocuments(queryDTO, user);
    }

    public static ResponseDTO<com.resolve.search.model.WikiDocument> advancedSearch(QueryDTO queryDTO, UserInfo user)
    {
        return WikiSearchAPI.advancedSearch(queryDTO, user);
    }

    public static void addGlobalAttachmentsToWiki(String docSysId, String docFullname, Set<String> globalAttachSysIds, String username) throws Exception
    {
        WikiAttachmentUtil.addGlobalAttachmentsToWiki(docSysId, docFullname, globalAttachSysIds, username);
    }
    
    public static double getAverageResolutionRating(String docSysId, String docFullName, String username)  throws Exception
    {
        return WikiUtils.getAverageResolutionRating(docSysId, docFullName, username);
    }
    
    public static void submitSurvey(String docSysId, String docFullName, FeedbackDTO feedback,  String username) 
    		throws Exception
    {
        WikiUtils.submitSurvey(docSysId, docFullName, feedback, username);
    }
    
    public static WikidocRatingDTO getRatingFor(String docSysId, String docFullName, String username) throws Exception
    {
        return WikiUtils.getRatingFor(docSysId, docFullName, username);
    }
    
    public static WikiPageInfoDTO getWikiPageInfo(String docSysId, String docFullName) throws Exception
    {
        WikiPageInfoHelper helper = new WikiPageInfoHelper(docSysId, docFullName);
        return helper.getInfo();
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////   WIKI REVISIONS  ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<WikiArchiveVO> getAllWikiRevisionsFor(String fullDocName, String username) throws Exception
    {
        return WikiArchiveUtil.getAllWikiRevisionsFor(fullDocName, username);
    }
    
    public static List<WikiArchiveVO> getSpecificWikiRevisionsFor(String docSysId, String fullDocName, Integer revision, String username) throws Exception
    {
        return WikiArchiveUtil.getSpecificWikiRevisionsFor(docSysId, fullDocName, revision, username);
    } 
    
    public static void rollbackWiki(String docSysId, String fullDocName, Integer rollbackToRevision, String username) throws Exception
    {
        WikiArchiveUtil.rollbackWiki(docSysId, fullDocName, rollbackToRevision, username);
    }
    
    public static void resetWiki(String docSysId, String fullDocName, String username) throws Exception
    {
        WikiArchiveUtil.resetWiki(docSysId, fullDocName, username);
    }

    public static void cleanupWikiArchive(String wikidocId)
    {
        try
        {
            new WikiArchiveUtil().cleanupWikiArchive(wikidocId);
        }
        catch (Throwable t)
        {
            Log.log.error("Error in cleanupWikiArchive:", t);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////   WIKI REPORTS  ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * api for 
     *  Top Viewed DT Reports
     *  Bottom Viewed DT Reports
     * 
     * 
     * @param query
     * @param username
     * @return
     * @throws Exception
     */
    public static ResponseDTO<Map<String, Object>> getDTExecutionReportData(QueryDTO query, String username) throws Exception
    {
        return new DTExecutionReport(query, username).getData();
    }
    
    /**
     * api for 
     *      Total Path Count Trend Reports
     *      Abort Path Count Trend Reports
     *      Path Duration Trend Reports
     * 
     * 
     * @param query
     * @param username
     * @return
     * @throws Exception
     */
    public static ResponseDTO<Map<String, Object>> getDTPathTrendReport(QueryDTO query, String username) throws Exception
    {
        return new DTPathTrendReport(query, username).getData();
    }    
    
    /**
     * 
     *  API for 
     *      Path Completed Detail Reports
     *      Path Aborted Detail Reports
     * 
     * @param query
     * @param username
     * @return
     * @throws Exception
     */
    public static ResponseDTO<Map<String, Object>> getDTPathDetailReport(QueryDTO query, String username) throws Exception
    {
        return new DTPathDetailReport(query, username).getData();
    }  

    /**
     * API to be called from update script to clean all bad (dangling) references present in Wiki content and in main, abort and DT models.
     * 
     */
    public static void cleanWikidocBadReferences()
    {
        WikiUtils.cleanWikidocBadReferences();
    }
    
    public static ResponseDTO<Map<String, Map<String,List<String>>>> convertDocxToWiki(HttpServletRequest request,
    		String resolveHome) {
        return WikiUtils.convertDocxToWiki(request, resolveHome);
    }
    
    @SuppressWarnings("rawtypes")
    public static ResponseDTO encryptParams(JSONObject jsonProperty)
    {
        return WikiUtils.encryptParams(jsonProperty);
    }
    
    public static List<AttachmentDTO> tokenizeAttachmentUrls(HttpServletRequest request, List<AttachmentDTO> dtos) {
    	if (dtos != null) {
    		for (AttachmentDTO dto : dtos) {
    			if (StringUtils.isNotEmpty(dto.getDownloadUrl())) {
        			String[] params = dto.getDownloadUrl().split("\\?");
        			String[] token = JspUtils.getCSRFTokenForPage(request, params[0]);
        			dto.setDownloadUrl(String.join("", params[0], "?", params[1], "&", token[0], "=", token[1]));
    			}
    		}
    	}
    	
    	return dtos;
    }
}
