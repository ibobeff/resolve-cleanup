package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Restrictions;

import com.resolve.persistence.model.ArtifactConfiguration;
import com.resolve.persistence.model.ArtifactType;
import com.resolve.persistence.model.CEFDictionaryItem;
import com.resolve.persistence.model.CustomDictionaryItem;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ArtifactConfigurationVO;
import com.resolve.services.hibernate.vo.CustomDictionaryItemVO;
import com.resolve.services.vo.CustomDictionaryItemDTO;
import com.resolve.util.Log;

public class CustomDictionaryUtil {
	private static final String ERROR_DUPLICATE_NAME = "CEF dictionary already has an item with the name = %s";
	private static final String ERROR_NOT_FOUND = "Custom dictionary entry not found";
	private static final String ERROR_INCORRECT_NAME = "Custom dictionary entry should have correct name";
	protected static final String U_SHORT_NAME = "UShortName";

	public static List<CustomDictionaryItemVO> getAllCustomDictionary() {
		List<CustomDictionaryItemVO> cVOs = new ArrayList<CustomDictionaryItemVO>();
		try {
			HibernateProxy.execute(() -> {
				List<CustomDictionaryItem> result = HibernateUtil.getDAOFactory().getCustomDictionaryDAO().findAll();
				for (CustomDictionaryItem cdi : result) {
					cVOs.add(convertToCustomDictionaryVO(cdi));
				}
			});		
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return cVOs;
	}

	public static CustomDictionaryItemVO getCustomDictionaryByShortName(String shortName) {
		
		try {
			if (StringUtils.isEmpty(shortName)) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			return (CustomDictionaryItemVO) HibernateProxy.execute(() -> {
				
				CustomDictionaryItemVO cVO = null;
				
				CustomDictionaryItem result = (CustomDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CustomDictionaryItem.class)
						.add(Restrictions.eq(U_SHORT_NAME, shortName).ignoreCase()).uniqueResult();
				if (result != null) {
					cVO =	convertToCustomDictionaryVO(result);
				}
				
				return cVO;
			});

		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
			return null;
		}
		
	}

	public static CustomDictionaryItemVO saveCustomDictionary(CustomDictionaryItemDTO dto) {
		CustomDictionaryItemVO cdiVO = null;
		try {
			if (StringUtils.isEmpty(dto.getUShortName())) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}
			
			cdiVO = (CustomDictionaryItemVO) HibernateProxy.execute(() -> {
				// First check if same item is present in CEF
				CEFDictionaryItem cef = (CEFDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CEFDictionaryItem.class)
						.add(Restrictions.eq(U_SHORT_NAME, dto.getUShortName()).ignoreCase()).uniqueResult();
				
				if (cef != null) {
					Log.log.error(String.format(ERROR_DUPLICATE_NAME, dto.getUShortName()));
					throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
				}
				
				CustomDictionaryItem oldCdi = (CustomDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CustomDictionaryItem.class)
						.add(Restrictions.eq(U_SHORT_NAME, dto.getUShortName()).ignoreCase()).uniqueResult();

				if (oldCdi == null) {
					oldCdi = new CustomDictionaryItem();
				}
				
				oldCdi.setUShortName(dto.getUShortName());
				oldCdi.setUFullName(dto.getUFullName());
				oldCdi.setUDescription(dto.getUDescription());
			oldCdi.setChecksum(Objects.hash(oldCdi.getUShortName()));
				
				CustomDictionaryItem result = HibernateUtil.getDAOFactory().getCustomDictionaryDAO().insertOrUpdate(oldCdi);
				return convertToCustomDictionaryVO(result);
			});			

		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return cdiVO;
	}

	public static boolean deleteCustomDictionary(String shortName) {
		try {
			if (StringUtils.isEmpty(shortName)) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			HibernateProxy.execute(() -> {

				CustomDictionaryItem oldCdi = (CustomDictionaryItem) HibernateUtil.getCurrentSession()
						.createCriteria(CustomDictionaryItem.class)
						.add(Restrictions.eq(U_SHORT_NAME, shortName.toLowerCase()).ignoreCase()).uniqueResult();

				if (oldCdi == null) {
					throw new IllegalArgumentException(ERROR_NOT_FOUND);
				}

				HibernateUtil.getDAOFactory().getCustomDictionaryDAO().delete(oldCdi);
			});

			return true;
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		return false;
	}
	
	public static Set<ArtifactConfigurationVO> getCustomItemArtifactConfigurations(String shortName) {
		Set<ArtifactConfigurationVO> configVOs = new HashSet<>();
		try {
			HibernateProxy.execute(() -> {
				CustomDictionaryItem result = (CustomDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CustomDictionaryItem.class)
						.add(Restrictions.eq(U_SHORT_NAME, shortName).ignoreCase()).uniqueResult();
				if (result != null) {
					ArtifactType artifactType = result.getArtifactType();
					if (artifactType != null) {
						Set<ArtifactConfiguration> configs = artifactType.getArtifactConfigurations();
						if (configs != null) {
							for (ArtifactConfiguration config : configs) {
								configVOs.add(config.doGetVO());
							}
						}
					}
				}
			});		
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return configVOs;
	}

	protected static CustomDictionaryItemVO convertToCustomDictionaryVO(CustomDictionaryItem cdi) {
		CustomDictionaryItemVO cVO = new CustomDictionaryItemVO();
		cVO.setUShortName(cdi.getUShortName());
		cVO.setUFullName(cdi.getUFullName());
		cVO.setUDescription(cdi.getUDescription());
		cVO.setSys_id(cdi.getSys_id());
		cVO.setSysCreatedBy(cdi.getSysCreatedBy());
		cVO.setSysCreatedOn(cdi.getSysCreatedOn());
		cVO.setSysIsDeleted(cdi.getSysIsDeleted());
		cVO.setSysModCount(cdi.getSysModCount());
		cVO.setSysOrg(cdi.getSysOrg());
		cVO.setSysUpdatedBy(cdi.getSysUpdatedBy());
		cVO.setSysUpdatedOn(cdi.getSysUpdatedOn());
		return cVO;
	}
}
