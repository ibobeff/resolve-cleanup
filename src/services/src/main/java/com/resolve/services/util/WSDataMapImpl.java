/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;

public class WSDataMapImpl implements WSDataMap
{
    private static String EXPIRATION_TIME = "_resolve_wsdatamap_expiration_";
    private static String WSDATA_ENTRY_VALUE = "_wsdata_entry_value_";

    private final String problemId;
    private final Map<String,Object> data;
    
    public WSDataMapImpl(String problemId, Map<String,Object> data)
    {
        this.problemId = problemId;
        this.data = data ;
    }
    
    /**
     * This method retrieves data from the data storage (e.g., ES) on demand. The on demand
     * strategy is suitable due to replication latency of ES because query may not return
     * data due to replication delay but GET will always do.
     */
    public Object get(final String key)
    {
        Object val = data.get(key);
        if (val==null)
        {
            //this makes it On Demand
            val = ServiceWorksheet.getWorksheetWSDATAProperty(problemId, key, "system");
            if(val != null)
            {
                data.put(key, val);
            }
        }
        //check for expiration of a WSDATA key
        if (val != null && val instanceof Map)
        {
            Map<String,Object> valueMap = (Map<String,Object>) val;
            Object expiration = valueMap.get(EXPIRATION_TIME);
            if (expiration != null) 
            {
                long expirationTime = (Long)expiration;
                long now = System.currentTimeMillis();
                if (now > expirationTime)
                {
                    val = null; // value expired
                }
                {
                    val = valueMap.get(WSDATA_ENTRY_VALUE); // value not expired 
                }
            }
        }
        return val;
    }

    /**
     * The returned Map is not meant to manipulate the content of the data and that
     * is why it returns an "unmodifiable" Map.

     * @return
     */
    public Map<String, Object> getDataMap()
    {
        return Collections.unmodifiableMap(data);
    }

    public void put(final String key, final Object value)
    {
        if (value!=null)
        {
            data.put(key, value);
            
            //store it on the data storage instantly. this may prevent data loss in
            //case something happens during task result assessment.
            ServiceWorksheet.setWorksheetWSDATAProperty(problemId, key, value, "system");
        }
    }

    public void put(final String key, final Object value, final long duration)
    {
        if (value!=null)
        {
            long now = System.currentTimeMillis();
            now += duration * 1000;
            Map<String,Object> durableData = new HashMap<String,Object>();
            durableData.put(EXPIRATION_TIME, now);
            durableData.put(WSDATA_ENTRY_VALUE, value);
            put(key, durableData);
        }
    }
    
    public void putAll(final Map<String, Object> map)
    {
        if (map != null)
        {
            for(String key : map.keySet())
            {
                put(key, map.get(key));
            }
        }
    }
    
    /*
     * To support WSDATA["KEY"] = "VALUE"; from a groovy script
     */
    public void set(final String key, final Object value)
    {
        put(key, value);
    }
}
