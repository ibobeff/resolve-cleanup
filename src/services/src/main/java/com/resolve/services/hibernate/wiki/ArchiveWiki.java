/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import static com.resolve.persistence.model.WikiDocument.COL_MODEL_PROCESS;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.WikiArchive;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.VersionLimitReachedException;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ArchiveWiki
{
    private static final String AUTO_COMMIT_COMMENT = "Saved Draft";

    private WikiDocument doc = null;
    private String comment = AUTO_COMMIT_COMMENT;
    private String username = null;
    private boolean isUserArchive = false;
    
    private Set<String> docSysIds = null;
    private int incrementBy = 0;
    
    //for notification
    private List<SubmitNotification> submitNotifications = new ArrayList<SubmitNotification>();

    public ArchiveWiki(WikiDocument doc, String comment, String username)
    {
        if (doc == null)
        {
            throw new RuntimeException("Document to archive cannot be null");
        }

        this.doc = doc;
        this.username = StringUtils.isNotEmpty(username) ? username : "system";
        if (StringUtils.isNotEmpty(comment))
        {
            this.comment = comment;
            isUserArchive = true;
        }
    }
    
    public ArchiveWiki(Set<String> docSysIds, String comment, String username)
    {
        if(docSysIds == null)
        {
            throw new RuntimeException("List of doc sysIds is null");
        }
        
        this.docSysIds = docSysIds;
        this.username = StringUtils.isNotEmpty(username) ? username : "system";
        if (StringUtils.isNotEmpty(comment))
        {
            this.comment = comment;
            isUserArchive = true;
            incrementBy = 1;
        }
    }

    public void archive() throws Exception
    {
        if(doc != null)
        {
            try
            {
                archiveDocument(doc);
            }
            catch (Exception e)
            {
               Log.log.error("Error archiving document " + doc.getUFullname(), e);
            }
        }
        else if(docSysIds != null)
        {
            for(String sysId : docSysIds)
            {
                WikiDocument doc = WikiUtils.getWikiDocumentModel(sysId, null,  username, false);
                if(doc != null)
                {
                    try
                    {
                        archiveDocument(doc);
                    }
                    catch (Exception e)
                    {
                       Log.log.error("Error archiving document " + doc.getUFullname(), e);
                    }
                }
            }
        }
        
        //send the notifications
        if(submitNotifications.size() > 0)
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(ConstantValues.SOCIAL_NOTIFICATION_LIST, submitNotifications);
            
            SocialUtil.submitESBForUpdateSocialForWiki(params);
        }
    }
    
	private void archiveDocument(WikiDocument doc) throws Exception {
        Map<String, String> acols = doc.UgetArchievableColumns();
        Set<String> keys = acols.keySet();
        int revisionNumber = doc.getUVersion() != null ?  doc.getUVersion() + incrementBy : 1;

		try {

			HibernateProxy.execute(() -> {
				validateVersionCount(doc.getSys_id());

				for (String colName : keys) {
	            String methodName = acols.get(colName);
					Method m = doc.getClass().getMethod(methodName, (Class<?>[]) null);
	                String value = (String) m.invoke(doc, (Object[]) null);
					if (StringUtils.isEmpty(value)) {
	                    value = "";
	                }

					doc.setUVersion(revisionNumber);

	                WikiArchive archObj = new WikiArchive();
	                archObj.setUTableId(doc.getSys_id());
	                archObj.setUTableName(doc.UgetTableName());
	                archObj.setUTableColumn(colName);
	                archObj.setUPatch(value);
	                archObj.setUComment(comment);
	                archObj.setUUserArchive(isUserArchive);
	                archObj.setUVersion(revisionNumber);
	                archObj.setUIsStable(doc.getUIsStable());
					archObj.setUVersion(revisionNumber);

					if (StringUtils.isEmpty(username)) {
						username = "system";
	            }

					if (StringUtils.isNotBlank(archObj.getSys_id())) {
						HibernateUtil.getDAOFactory().getWikiArchiveDAO().update(archObj);
					} else {
						archObj.setSys_id(null);
						HibernateUtil.getDAOFactory().getWikiArchiveDAO().persist(archObj);
	            }

	        }
	        
			});
		} catch (VersionLimitReachedException e) {
			Log.log.error("Failed to persist WikiArchive: " + e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			Log.log.error("Failed to persist WikiArchive: " + e.getMessage(), e);
			HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    private void addArchive(WikiDocument doc, WikiArchive archive, int revisionNumber) throws Exception
    {
        doc.setUVersion(revisionNumber);
        archive.setUVersion(revisionNumber);
        
        try
        {
           HibernateProxy.setCurrentUser(username);
           HibernateProxy.execute(() -> {
        	   SaveUtil.saveWikiArchive(archive, username);
           });
            
            
            /*
             *  HP Since Original wikidoc is not updated, skip updating 
             *  as this will get called for content, process model, 
             *  abort model, and decision tree. 
             */
            //SaveUtil.saveWikiDocument(doc, username);

			if (this.isUserArchive) {
				submitNotifications.add(NotificationHelper.getDocumentNotification(doc,
						UserGlobalNotificationContainerType.DOCUMENT_COMMIT, username, false, true));
	        }
		}
        catch (Throwable e)

        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }

	@SuppressWarnings("unchecked")
	private static void validateVersionCount(String wikiDocId) {
		long versionCount = (long) HibernateUtil.getSessionFactory().getCurrentSession().createQuery(
				"SELECT count(sys_id) FROM WikiArchive WHERE UTableId = :UTableId AND UTableColumn = :UTableColumn")
				.setParameter("UTableId", wikiDocId)
				.setParameter("UTableColumn", COL_MODEL_PROCESS) // the actual version 'archive' record holding the raw XML content
				.uniqueResultOptional().orElse(0);

		long versionLimit = PropertiesUtil.getPropertyLong("global.version.limit.count");
		
		if (versionCount >= versionLimit) {
			throw new VersionLimitReachedException(
					String.format("The version limit of %s was reached, count: %s: ", versionLimit, versionCount));	
		}
	}

}
