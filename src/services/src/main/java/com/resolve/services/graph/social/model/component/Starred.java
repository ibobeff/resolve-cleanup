/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.vo.social.NodeType;

/**
 * 
 * @author jeet.marwah
 *
 */
public class Starred extends RSComponent
{
    private static final long serialVersionUID = -8367858587757863094L;
    
//    public static final String TYPE = "starred";

    public Starred()
    {
        super(NodeType.STARRED);
    }

}
