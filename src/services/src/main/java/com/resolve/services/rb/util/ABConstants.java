/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/
package com.resolve.services.rb.util;

import java.util.HashMap;
import java.util.Map;

public class ABConstants
{
    public static final String BUILDER = "builder";
    public static final String LOOP = "loop";
    public static final String CONNECTION = "connection";
    public static final String IF = "if";
    public static final String CONDITION = "condition";
    
    public static final String DEFAULT_NAMESPACE = "resolve";
    public static final Map<String, String> CONNECT_TASKS = new HashMap<String, String>();
    public static final Map<String, String> DISCONNECT_TASKS = new HashMap<String, String>();
    static
    {
        CONNECT_TASKS.put("ssh", "sshConnect");
        CONNECT_TASKS.put("http", "httpConnect");
        CONNECT_TASKS.put("telnet", "telnetConnect");

        DISCONNECT_TASKS.put("ssh", "sshDisconnect");
        DISCONNECT_TASKS.put("http", "httpDisconnect");
        DISCONNECT_TASKS.put("telnet", "telnetDisconnect");
    }
    
    //store in case someone decides to change the usage like UI send FLOW but we need as FLOWS.
    public static final Map<String, String> SOURCE = new HashMap<String, String>();
    
    static
    {
        SOURCE.put("OUTPUT", "OUTPUT");
        SOURCE.put("FLOW", "FLOW");
        SOURCE.put("PARAM", "PARAM");
        SOURCE.put("WSDATA", "WSDATA");
        SOURCE.put("PROPERTY", "PROPERTY");
    }

    //store in case someone decides to change the usage like UI send FLOW but we need as FLOWS.
    public static final Map<String, String> SOURCE_MAP = new HashMap<String, String>();
    
    static
    {
        SOURCE_MAP.put("OUTPUT", "OUTPUTS");
        SOURCE_MAP.put("FLOW", "FLOWS");
        SOURCE_MAP.put("PARAM", "PARAMS");
        SOURCE_MAP.put("WSDATA", "WSDATA");
        SOURCE_MAP.put("PROPERTY", "PROPERTIES");
    }

    public static final Map<String, String> URLOPERATORS = new HashMap<String, String>();
    
    static
    {
        URLOPERATORS.put("eq", "==");
        URLOPERATORS.put("neq", "!=");
        URLOPERATORS.put("gt", "&gt;");
        URLOPERATORS.put("gte", "&gt;=");
        URLOPERATORS.put("lt", "&lt;");
        URLOPERATORS.put("lte", "&lt;=");
        URLOPERATORS.put("contains" , "contains");
    }

    public static final Map<String, String> OPERATORS = new HashMap<String, String>();
    
    static
    {
        OPERATORS.put("eq", "==");
        OPERATORS.put("neq", "!=");
        OPERATORS.put("gt", ">");
        OPERATORS.put("gte", ">=");
        OPERATORS.put("lt", "<");
        OPERATORS.put("lte", "<=");
        OPERATORS.put("contains" , "contains");
        OPERATORS.put("notContains" , "notContains");
        OPERATORS.put("startsWith" , "startsWith");
        OPERATORS.put("endsWith" , "endsWith");
    }

    public static final Map<String, String> DELIMITERS = new HashMap<String, String>();
    
    static
    {
        DELIMITERS.put("comma", ",");
        DELIMITERS.put("space", " ");
    }
   
}
