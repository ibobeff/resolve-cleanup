/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.LockMode;

import com.resolve.persistence.dao.ContentWikidocActiontaskRelDAO;
import com.resolve.persistence.dao.ContentWikidocWikidocRelDAO;
import com.resolve.persistence.dao.DTWikidocWikidocRelDAO;
import com.resolve.persistence.dao.ExceptionWikidocActiontaskRelDAO;
import com.resolve.persistence.dao.ExceptionWikidocWikidocRelDAO;
import com.resolve.persistence.dao.FinalWikidocActiontaskRelDAO;
import com.resolve.persistence.dao.FinalWikidocWikidocRelDAO;
import com.resolve.persistence.dao.MainWikidocActiontaskRelDAO;
import com.resolve.persistence.dao.MainWikidocWikidocRelDAO;
import com.resolve.persistence.dao.ResolveCompRelDAO;
import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.model.ContentWikidocActiontaskRel;
import com.resolve.persistence.model.ContentWikidocWikidocRel;
import com.resolve.persistence.model.DTWikidocWikidocRel;
import com.resolve.persistence.model.ExceptionWikidocActiontaskRel;
import com.resolve.persistence.model.ExceptionWikidocWikidocRel;
import com.resolve.persistence.model.FinalWikidocActiontaskRel;
import com.resolve.persistence.model.FinalWikidocWikidocRel;
import com.resolve.persistence.model.MainWikidocActiontaskRel;
import com.resolve.persistence.model.MainWikidocWikidocRel;
import com.resolve.persistence.model.ResolveCompRel;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocStatistics;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.wiki.WikiIndexAPI;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiDocStatsCounterName;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.DecisionTreeHelper;
import com.resolve.services.util.ParseUtil;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class has implementation to update the relationship tables related to wikidoc.
 * The idea is to decouple the updates to this table from the main save of the wikidoc so that even if there is
 * a failure in one of these updates, it does not affect the save transaction.
 * 
 * These methods are called by the executor threads.
 * 
 * @author jeet.marwah
 *
 */

public class UpdateWikiDocRelations
{
    private static ConcurrentHashMap<String, AtomicInteger> wikiDocStatisticsViewCounter = new ConcurrentHashMap<String, AtomicInteger>();
    private static ConcurrentHashMap<String, AtomicInteger> wikiDocStatisticsEditCounter = new ConcurrentHashMap<String, AtomicInteger>();
    private static ConcurrentHashMap<String, AtomicInteger> wikiDocStatisticsExecuteCounter = new ConcurrentHashMap<String, AtomicInteger>();
    
    static {
        ScheduledExecutor.getInstance().executeDelayedRepeat(UpdateWikiDocRelations.class, "updateWikiStatistics", 180, 120, TimeUnit.SECONDS);
    }
    
    @SuppressWarnings("unchecked")
	public static void updateWikiStatistics()    {
        Log.log.debug(String.format("WikiDoc Statistics: View Counter Size = %d, Edit Counter Size = %d" + 
        							", Execute Counter Size = %d", wikiDocStatisticsViewCounter.size(), 
        							wikiDocStatisticsEditCounter.size(), wikiDocStatisticsExecuteCounter.size()));

          Set<String> viewSet = wikiDocStatisticsViewCounter.keySet();
          Set<String> editSet = wikiDocStatisticsEditCounter.keySet();
          Set<String> executeSet = wikiDocStatisticsExecuteCounter.keySet();

          for (String docId : viewSet)
          {
              Log.log.debug(String.format("Processing Wiki Doc Statistics for View for Wiki Doc with id %s", docId));
              
              try {
				HibernateProxy.executeNoCacheHibernateInitLocked( () -> {

				      try
				      {
				    	  WikidocStatistics ws = null;
				    	  List<WikidocStatistics> wss = HibernateUtil
														.createQuery("from WikidocStatistics w where w.wikidoc.id = :docId " +
																	 "order by w.sysUpdatedOn desc")
														.setParameter("docId", docId).list();

				    	  if (CollectionUtils.isNotEmpty(wss)) {
				    		  ws = wss.get(0);
				    	  }
				    	  
				          if (ws == null)
				          {
				              ws = new WikidocStatistics();
				              
				              WikiDocument wd = WikiUtils.getWikiDocumentModel(docId, null, "system", false);
				              
				              if (wd == null)
				              {
				                  wd = new WikiDocument();
				                  wd.setSys_id(docId);
				              }
				              
				              ws.setWikidoc(wd);
				              ws.setUViewCount(0);
				              ws.setUEditCount(0);
				              ws.setUExecuteCount(0);
				              ws.setUReviewCount(0);
				              ws.setUUsefulNoCount(0);
				              ws.setUUsefulYesCount(0);
				              HibernateUtil.getCurrentSession().save(ws);
				          }

				          Log.log.debug(String.format("View Wiki Doc Statistics Before Update %s", ws));
				          
				          Log.log.trace(String.format("Trying to acquire " + LockMode.UPGRADE + 
				        		  					  " lock on wiki doc statistics for wiki doc with sys id %s ...", 
				        		  					  ws.getWikidoc().getSys_id()));
				          
				          HibernateUtil.getCurrentSession().lock(ws, LockMode.UPGRADE);
				          
				          Log.log.trace(String.format("Acquired " + LockMode.UPGRADE + 
				                        			  " lock on wiki doc statistics for wiki doc with sys id %s !!!", 
				                        			  ws.getWikidoc().getSys_id()));
				          
				          AtomicInteger a = wikiDocStatisticsViewCounter.remove(docId);
				          
				          if (a != null) {
				        	  ws.setUViewCount(ws.getUViewCount() + a.get());
				          } else {
				        	  Log.log.debug(String.format("wikiDocStatisticsViewCounter is null for docId: %s, wiki = %s",
				        			  					  docId, ws.getWikidoc().getUName()));
				          }
				          
				          Log.log.debug(String.format("View Wiki Doc Statistics After Update %s", ws));

				          Log.log.trace(String.format("Released " + LockMode.UPGRADE + 
				                        			  " lock on wiki doc statistics for wiki doc with sys id %s ***",
				                        			  ws.getWikidoc().getSys_id()));
				          
				          //update the view count in ES
				          if(ws.getUViewCount() != null)
				          {
				              WikiIndexAPI.setClickCount(docId, new Long(ws.getUViewCount()), "system");
				          }
				          
				          Log.log.debug("Updated View Count in ES");
				      
				          }
				          catch (Throwable ex)
				          {
				              Log.log.error(ex.getMessage(), ex);
				          }
				  });
				
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin updating wikidoc statistics view counter for wikidoc with sys id %s", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											docId), t);
                HibernateUtil.rethrowNestedTransaction(t);
                throw new RuntimeException(t);
			}

          }

          for (String docIdUser : editSet)
          {
              String[] tokens = docIdUser.split(ConstantValues.GIBBERISH_TOKEN);
              String docId = tokens[0];
              Log.log.debug(String.format("Processing Wiki Doc Statistics for Edit for Wiki Doc with id %s", docId));
              try
              {
            	  HibernateProxy.executeNoCacheHibernateInitLocked( () -> {
	            	  WikidocStatistics ws = null;
	            	  List<WikidocStatistics> wss = HibernateUtil
		  											.createQuery("from WikidocStatistics w where w.wikidoc.id = :docId " +
		  														 "order by w.sysUpdatedOn desc")
		  											.setParameter("docId", docId).list();
	
	            	  if (CollectionUtils.isNotEmpty(wss)) {
	            		  ws = wss.get(0);
	            	  }
	
	                  if (ws == null)
	                  {
	                      ws = new WikidocStatistics();
	
	                      WikiDocument wd = WikiUtils.getWikiDocumentModel(docId, null, "system", false);
	                      
	                      if (wd == null)
	                      {
	                          wd = new WikiDocument();
	                          wd.setSys_id(docId);
	                      }
	                      
	                      ws.setWikidoc(wd);
	                      ws.setUViewCount(0);
	                      ws.setUEditCount(0);
	                      ws.setUExecuteCount(0);
	                      ws.setUReviewCount(0);
	                      ws.setUUsefulYesCount(0);
	                      ws.setUUsefulNoCount(0);
	                      HibernateUtil.getCurrentSession().save(ws);
	                  }
	
	                  Log.log.debug(String.format("Edit Wiki Doc Statistics Before Update %s", ws));
	
	                  Log.log.trace(String.format("Trying to acquire " + LockMode.UPGRADE + 
	                                			  " lock on wiki doc statistics for wiki doc with sys id %s ...", 
	                                			  ws.getWikidoc().getSys_id()));
	                  
	                  HibernateUtil.getCurrentSession().lock(ws, LockMode.UPGRADE);
	                  
	                  Log.log.trace(String.format("Acquired " + LockMode.UPGRADE + 
	                                			  " lock on wiki doc statistics for wiki doc with sys id %s !!!", 
	                                			  ws.getWikidoc().getSys_id()));
	                  
	                  AtomicInteger a = wikiDocStatisticsEditCounter.remove(docIdUser);
	                  if (a!=null)
	                  {
	                	  ws.setUEditCount(ws.getUEditCount() + a.get());
	                  }
	                  else
	                  {
	                	  Log.log.debug(String.format("wikiDocStatisticsEditCounter is null for docIdUser: %s, wiki = %s", 
	                			  					  docIdUser, ws.getWikidoc().getUName()));
	                  }
	                  
	                  Log.log.debug(String.format("Edit Wiki Doc Statistics After Update %s", ws));
	                  
	                  Log.log.trace(String.format("Released %s lock on wiki doc statistics for wiki doc with sys id %s ***", 
	              			  LockMode.UPGRADE.name(), 
	              			  ws.getWikidoc().getSys_id()));
	                  
	                  //update the edit count in ES
	                  if(ws.getUEditCount() != null)
	                  {
	                      WikiIndexAPI.setEditCount(docId, new Long(ws.getUEditCount()), "system");
	                  }
	                  
	                  Log.log.debug("Updated Edit Count in ES");
            	  });
              }
              catch (Throwable t)
              {
            	  Log.log.error(String.format("Error %sin updating wikidoc statistics update counter for wikidoc with sys " +
            			  					  "id %s", 
											  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											  docId), t);
                  HibernateUtil.rethrowNestedTransaction(t);
                  throw new RuntimeException(t);
              }
          }

          for (String docId : executeSet)
          {
              Log.log.debug(String.format("Processing Wiki Doc Statistics for Execute for Wiki Doc with id %s", docId));
              
              try
              {
            	  HibernateProxy.executeNoCacheHibernateInitLocked(() -> {

                	  WikidocStatistics ws = null;
                	  List<WikidocStatistics> wss = HibernateUtil
                			  						.createQuery("from WikidocStatistics w where w.wikidoc.id = :docId " +
                			  									 "order by w.sysUpdatedOn desc")
                			  						.setParameter("docId", docId).list();
                      
                	  if (CollectionUtils.isNotEmpty(wss)) {
                		  ws = wss.get(0);
                	  }
                	  
                      if (ws == null)
                      {
                          ws = new WikidocStatistics();

                          WikiDocument wd = WikiUtils.getWikiDocumentModel(docId, null, "system", false);
                          
                          if (wd == null)
                          {
                              wd = new WikiDocument();
                              wd.setSys_id(docId);
                          }
                          
                          ws.setWikidoc(wd);
                          ws.setUViewCount(0);
                          ws.setUEditCount(0);
                          ws.setUExecuteCount(0);
                          HibernateUtil.getCurrentSession().save(ws);
                      }
                      
                      Log.log.debug(String.format("Eexecute Wiki Doc Statistics Before Update %s", ws));

                      Log.log.trace(String.format("Trying to acquire " + LockMode.UPGRADE + 
                                    			  " lock on wiki doc statistics for wiki doc with sys id %s ...",
                                    			  ws.getWikidoc().getSys_id()));
                      
                      HibernateUtil.getCurrentSession().lock(ws, LockMode.UPGRADE);
                      
                      Log.log.trace(String.format("Acquired %s lock on wiki doc statistics for wiki doc with sys id %s !!!", 
                                    			  LockMode.UPGRADE.name(),
                                    			  ws.getWikidoc().getSys_id()));
                      
                      AtomicInteger a = wikiDocStatisticsExecuteCounter.remove(docId);
                      
                      if (a!=null) {
                    	  ws.setUExecuteCount(ws.getUExecuteCount() + a.get());
                      } else {
                    	  Log.log.debug(String.format("wikiDocStatisticsExecuteCounter is null for docId: %s, wiki = %s",
                    			  					  docId, ws.getWikidoc().getUName()));
                      }
                      
                      Log.log.debug("Eexecute Wiki Doc Statistics After Update " + ws);

                      Log.log.trace(String.format("Released %s lock on wiki doc statistics for wiki doc with sys id %s ***",
                			  LockMode.UPGRADE.name(),
                			  ws.getWikidoc().getSys_id()));
                      
                      //update the execute count in ES
                      if(ws.getUExecuteCount() != null)
                      {
                          WikiIndexAPI.setExecuteCount(docId, new Long(ws.getUExecuteCount()), "system");
                      }
                      
                      Log.log.debug("Updated Execute Count in ES");                  
            	  });
              }
              catch (Throwable t)
              {
            	  Log.log.error(String.format("Error %sin updating wikidoc statistics execute counter for wikidoc with sys " +
	  					  					  "id %s", 
	  					  					  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
	  					  					  docId), t);
                  HibernateUtil.rethrowNestedTransaction(t);
                  throw new RuntimeException(t);
              }
          }
    }

    public static void updateWikiDocStatsCounter(WikiDocStatsCounterName wikiDocStatsCounterName, List<String> docSysIds, String username)
    {
        if(docSysIds != null)
        {
            for(String docId : docSysIds)
            {
                updateWikiDocStatsCounter(wikiDocStatsCounterName, docId, username);
            }
        }
        
    }
    
    public static void updateWikiDocStatsCounter(WikiDocStatsCounterName wikiDocStatsCounterName, String docId, String username)
    {
        if (StringUtils.isNotEmpty(docId))
        {
            //
            // GVo Note
            // Updating stats is asynchronously done by a thread and can potentially be done by different users.
            // To ensure properly recording who is updating, the docId and the username are composed as the key.
            // When the thread updating stats runs, it will decompose the docId and the username. This way, different
            // users will be recorded correctly.
            //
            String docIdUser = docId + ConstantValues.GIBBERISH_TOKEN + username;
            if (wikiDocStatsCounterName == WikiDocStatsCounterName.VIEWED)
            {
                AtomicInteger at = wikiDocStatisticsViewCounter.get(docId);
                if (at == null)
                {
                    wikiDocStatisticsViewCounter.putIfAbsent(docId, new AtomicInteger());
                }
                wikiDocStatisticsViewCounter.get(docId).incrementAndGet();
            }
            if (wikiDocStatsCounterName == WikiDocStatsCounterName.EDITED)
            {
                AtomicInteger at = wikiDocStatisticsEditCounter.get(docIdUser);
                if (at == null)
                {
                    wikiDocStatisticsEditCounter.putIfAbsent(docIdUser, new AtomicInteger());
                }
                wikiDocStatisticsEditCounter.get(docIdUser).incrementAndGet();

            }
            if (wikiDocStatsCounterName == WikiDocStatsCounterName.EXECUTED)
            {
                AtomicInteger at = wikiDocStatisticsExecuteCounter.get(docId);
                if (at == null)
                {
                    wikiDocStatisticsExecuteCounter.putIfAbsent(docId, new AtomicInteger());
                }
                wikiDocStatisticsExecuteCounter.get(docId).incrementAndGet();
            }
            
        }
    }
    
    public static void updateAllRelationForDocument(String docId, boolean updateContent, boolean updateMain, boolean updateException, boolean updateDTree)
    {
        if(StringUtils.isNotBlank(docId))
        {
            if(updateContent || updateMain || updateException || updateDTree)
            {
                WikiDocumentVO wikiDocument = ServiceWiki.getWikiDoc(docId, null, "system");
    
                //update the relationships
                if(updateContent) updateContentWikiDocRelationship(wikiDocument);
                if(updateMain) updateMainWikiDocRelationship(wikiDocument);
                if(updateException) updateExceptionWikiDocRelationship(wikiDocument);
                if(updateDTree) updateDTWikiDocRelationship(wikiDocument);
            }
        }
    }
    
    public static  void updateAllRelationForDocument(List<String> docSysIds, boolean updateContent, boolean updateMain, boolean updateException, boolean updateDTree)
    {
        if(docSysIds != null)
        {
            for(String docId : docSysIds)
            {
                updateAllRelationForDocument(docId, updateContent, updateMain, updateException, updateDTree);
            }
        }
    }

    public static  void updateFinalWikiDocRelationship(WikiDocumentVO wikiDocument)
    {
        if (wikiDocument != null)
        {
            try
            {
                updateFinalWikidocWikidocRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateFinalWikidocWikidocRel:", e);
            }

            try
            {
                updateFinalWikidocActiontaskRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateFinalWikidocActiontaskRel:", e);
            }
        }
    }

    public  static void updateExceptionWikiDocRelationship(WikiDocumentVO wikiDocument)
    {
        if (wikiDocument != null)
        {
            try
            {
                updateExceptionWikidocWikidocRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateExceptionWikidocWikidocRel:", e);
            }

            try
            {
                updateExceptionWikidocActiontaskRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateExceptionWikidocActiontaskRel:", e);
            }
        }
    }

    public static  void updateMainWikiDocRelationship(WikiDocumentVO wikiDocument)
    {
        if (wikiDocument != null)
        {
            try
            {
                updateMainWikidocWikidocRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateMainWikidocWikidocRel:", e);
            }

            try
            {
                updateMainWikidocActiontaskRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateMainWikidocActiontaskRel:", e);
            }
        }
    }

    public static void updateContentWikiDocRelationship(WikiDocumentVO wikiDocument)
    {
        if (wikiDocument != null)
        {
            try
            {
                updateContentWikidocWikidocRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateContentWikidocWikidocRel:", e);
            }

            try
            {
                updateContentWikidocActiontaskRel(wikiDocument);
            }
            catch (Exception e)
            {
                Log.log.error("Error in updateContentWikidocActiontaskRel:", e);
            }
        }
        else
        {
            Log.log.error("No Wiki Document to Update");
        }
    }//updateContentWikiDocRelationship
    
    public static  void updateDTWikiDocRelationship(WikiDocumentVO wikiDocument)
    {
        if (wikiDocument != null)
        {
            try
            {
                updateDTWikidocWikidocRel(wikiDocument);
                updateResolveCompRelations(wikiDocument);
            }
            catch (Throwable e)
            {
                Log.log.error("Error in updateDTWikidocWikidocRel:", e);
            }
        }
    }//updateDTWikiDocRelationship
    
    public static void resetWikidocStats(String docSysId, String docFullName, String username) throws Exception
    {
        WikiDocument doc = WikiUtils.getWikiDocumentModel(docSysId, docFullName, username, true);
        if(doc != null)
        {
            Collection<WikidocStatistics> stats = doc.getWikidocStatistics();
            if(stats != null && stats.size() > 0)
            {
                for(WikidocStatistics stat : stats)
                {
                    stat.setUViewCount(0);
                    stat.setUEditCount(0);
                    stat.setUExecuteCount(0);
                    stat.setUReviewCount(0);
                    stat.setUUsefulNoCount(0);
                    stat.setUUsefulYesCount(0);
                    
                    SaveUtil.saveWikidocStatistics(stat, username);
                    
                }//end of for loop                
            }
        }
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static  void updateFinalWikidocWikidocRel(WikiDocumentVO vo) throws WikiException
    {
        //      String deleteSql = "delete from FinalWikidocWikidocRel where finalWikidoc = :UFinalWikidocId";
        Set<String> listOfReferencedDocs = ParseUtil.getListOfWikiDocumentReferencedByXML(vo.getUModelFinal());
        List<FinalWikidocWikidocRel> listOfRefObjs = new ArrayList<FinalWikidocWikidocRel>();

        try
        {
            //prepare the ref obj list
            for (String refDocName : listOfReferencedDocs)
            {
                //create the obj
                FinalWikidocWikidocRel cwwr = new FinalWikidocWikidocRel();
                cwwr.setUWikidocFullname(refDocName);

                listOfRefObjs.add(cwwr);

            }//end of for loop

            //delete and insert it
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            WikiDocument doc = dao.findById(vo.getSys_id());
	
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("FinalWikidocWikidocRel", "finalWikidoc ='" + vo.getSys_id() + "'");
	            HibernateUtil.deleteQuery("FinalWikidocWikidocRel", "finalWikidoc", vo.getSys_id());
	
	            //insert the elements in the table
	            FinalWikidocWikidocRelDAO daoRel = HibernateUtil.getDAOFactory().getFinalWikidocWikidocRelDAO();
	            for (FinalWikidocWikidocRel obj : listOfRefObjs)
	            {
	                obj.setFinalWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

    }//updateFinalWikidocWikidocRel

    private  static void updateFinalWikidocActiontaskRel(WikiDocumentVO vo) throws WikiException
    {
        //      String deleteSql = "delete from FinalWikidocActiontaskRel where finalWikidoc = :UFinalWikidocId";
        Set<String> listOfReferencedActionTask = ParseUtil.getListOfActionTasksReferencedByXML(vo.getUModelFinal());
        List<FinalWikidocActiontaskRel> listOfRefObjs = new ArrayList<FinalWikidocActiontaskRel>();

        try
        {
            //prepare the ref obj list
            for (String refTaskName : listOfReferencedActionTask)
            {
                //create the obj
                FinalWikidocActiontaskRel cwwr = new FinalWikidocActiontaskRel();
                cwwr.setUActiontaskFullname(refTaskName);

                listOfRefObjs.add(cwwr);

            }

            //delete and insert it
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(vo.getSys_id());
	
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("FinalWikidocActiontaskRel", "finalWikidoc ='" + vo.getSys_id() + "'");
	            HibernateUtil.deleteQuery("FinalWikidocActiontaskRel", "finalWikidoc", vo.getSys_id());
	
	            //insert the elements in the table
	            FinalWikidocActiontaskRelDAO daoRel = HibernateUtil.getDAOFactory().getFinalWikidocActiontaskRelDAO();
	            for (FinalWikidocActiontaskRel obj : listOfRefObjs)
	            {
	                obj.setFinalWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

    }//updateFinalWikidocActiontaskRel

    private  static void updateExceptionWikidocWikidocRel(WikiDocumentVO vo) throws WikiException
    {
        //      String deleteSql = "delete from ExceptionWikidocWikidocRel where exceptionWikidoc = :UExceptionWikidocId";
        Set<String> listOfReferencedDocs = ParseUtil.getListOfWikiDocumentReferencedByXML(vo.getUModelException());
        List<ExceptionWikidocWikidocRel> listOfRefObjs = new ArrayList<ExceptionWikidocWikidocRel>();

        try
        {
            //prepare the ref obj list
            for (String refDocName : listOfReferencedDocs)
            {
                //create the obj
                ExceptionWikidocWikidocRel cwwr = new ExceptionWikidocWikidocRel();
                cwwr.setUWikidocFullname(refDocName);

                listOfRefObjs.add(cwwr);

            }

            //delete and insert
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
	
	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            WikiDocument doc = dao.findById(vo.getSys_id());
	
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("ExceptionWikidocWikidocRel", "exceptionWikidoc ='" + vo.getSys_id() + "'");
	            HibernateUtil.deleteQuery("ExceptionWikidocWikidocRel", "exceptionWikidoc", vo.getSys_id());
	
	            //insert the elements in the table
	            ExceptionWikidocWikidocRelDAO daoRel = HibernateUtil.getDAOFactory().getExceptionWikidocWikidocRelDAO();
	            for (ExceptionWikidocWikidocRel obj : listOfRefObjs)
	            {
	                obj.setExceptionWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

    }//updateExceptionWikidocWikidocRel

    private static  void updateExceptionWikidocActiontaskRel(WikiDocumentVO vo) throws WikiException
    {
        //      String deleteSql = "delete from ExceptionWikidocActiontaskRel where exceptionWikidoc = :UExceptionWikidocId";
        Set<String> listOfReferencedActionTask = ParseUtil.getListOfActionTasksReferencedByXML(vo.getUModelException());
        List<ExceptionWikidocActiontaskRel> listOfRefObjs = new ArrayList<ExceptionWikidocActiontaskRel>();

        try
        {
            //prepare the ref obj list
            for (String refTaskName : listOfReferencedActionTask)
            {
                //create the obj
                ExceptionWikidocActiontaskRel cwwr = new ExceptionWikidocActiontaskRel();
                cwwr.setUActiontaskFullname(refTaskName);

                listOfRefObjs.add(cwwr);

            }

            //delete and insert
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(vo.getSys_id());
	
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("ExceptionWikidocActiontaskRel", "exceptionWikidoc ='" + vo.getSys_id() + "'");
	            HibernateUtil.deleteQuery("ExceptionWikidocActiontaskRel", "exceptionWikidoc", vo.getSys_id());
	
	            //insert the elements in the table
	            ExceptionWikidocActiontaskRelDAO daoRel = HibernateUtil.getDAOFactory().getExceptionWikidocActiontaskRelDAO();
	            for (ExceptionWikidocActiontaskRel obj : listOfRefObjs)
	            {
	                obj.setExceptionWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

    }//updateExceptionWikidocActiontaskRel
    
    private static  void updateMainWikidocWikidocRel(WikiDocumentVO vo) throws WikiException
    {
//      String deleteSql = "delete from MainWikidocWikidocRel where mainWikidoc = :UMainWikidocId";
        Set<String> listOfReferencedDocs = ParseUtil.getListOfWikiDocumentReferencedByXML(vo.getUModelProcess());
        List<MainWikidocWikidocRel> listOfRefObjs = new ArrayList<MainWikidocWikidocRel>();

        try
        {
            //prepare the ref obj list
            for (String refDocName : listOfReferencedDocs)
            {
                //create the obj
                MainWikidocWikidocRel cwwr = new MainWikidocWikidocRel();
                cwwr.setUWikidocFullname(refDocName);
                listOfRefObjs.add(cwwr);

            }

          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            WikiDocument doc = dao.findById(vo.getSys_id());
	
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("MainWikidocWikidocRel", "mainWikidoc ='" + vo.getSys_id() + "'");
	            HibernateUtil.deleteQuery("MainWikidocWikidocRel", "mainWikidoc", vo.getSys_id());
	
	            //insert the elements in the table
	            MainWikidocWikidocRelDAO daoRel = HibernateUtil.getDAOFactory().getMainWikidocWikidocRelDAO();
	            for (MainWikidocWikidocRel obj : listOfRefObjs)
	            {
	                obj.setMainWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

    }//updateMainWikidocWikidocRel
    
    private  static void updateMainWikidocActiontaskRel(WikiDocumentVO vo) throws WikiException
    {
//      String deleteSql = "delete from MainWikidocActiontaskRel where mainWikidoc = :UMainWikidocId";
        Set<String> listOfReferencedActionTask = ParseUtil.getListOfActionTasksReferencedByXML(vo.getUModelProcess());
        List<MainWikidocActiontaskRel> listOfRefObjs = new ArrayList<MainWikidocActiontaskRel>();

        try
        {
            //prepare the ref obj list
            for (String refTaskName : listOfReferencedActionTask)
            {
                //create the obj
                MainWikidocActiontaskRel cwwr = new MainWikidocActiontaskRel();
                cwwr.setUActiontaskFullname(refTaskName);

                listOfRefObjs.add(cwwr);
            }

          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(vo.getSys_id());
	
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("MainWikidocActiontaskRel", "mainWikidoc ='" + vo.getSys_id() + "'");
	            HibernateUtil.deleteQuery("MainWikidocActiontaskRel", "mainWikidoc", vo.getSys_id());
	
	            //insert the elements in the table
	            MainWikidocActiontaskRelDAO daoRel = HibernateUtil.getDAOFactory().getMainWikidocActiontaskRelDAO();
	            for (MainWikidocActiontaskRel obj : listOfRefObjs)
	            {
	                obj.setMainWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }//updateMainWikidocActiontaskRel

    private static void updateContentWikidocWikidocRel(WikiDocumentVO vo) throws WikiException
    {
//      String deleteSql = "delete from ContentWikidocWikidocRel where contentWikidoc = :UContentWikidocId";
        Set<String> listOfReferencedDocs = null;
        try
        {
            listOfReferencedDocs = ParseUtil.getListOfWikiDocumentReferencedBy(vo);
        }
        catch(Exception e)
        {
            Log.log.error("Error in updateContentWikidocWikidocRel for doc <" + vo.getUFullname() + ">", e);
            throw new WikiException(e);
        }
        
        
        List<ContentWikidocWikidocRel> listOfRefObjs = new ArrayList<ContentWikidocWikidocRel>();

        try
        {
            //prepare the ref obj list
            for (String refDocName : listOfReferencedDocs)
            {
                //create the obj
                ContentWikidocWikidocRel cwwr = new ContentWikidocWikidocRel();
                cwwr.setUWikidocFullname(refDocName);

                listOfRefObjs.add(cwwr);
            }

          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(vo.getSys_id());
	            
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("ContentWikidocWikidocRel", "contentWikidoc ='" + vo.getSys_id() + "'");
	            HibernateUtil.deleteQuery("ContentWikidocWikidocRel", "contentWikidoc", vo.getSys_id());
	
	            //insert the elements in the table
	            ContentWikidocWikidocRelDAO daoRel = HibernateUtil.getDAOFactory().getContentWikidocWikidocRelDAO();
	            for (ContentWikidocWikidocRel obj : listOfRefObjs)
	            {
	                obj.setContentWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
           
        }

    }//updateContentWikidocWikidocRel
    
    private static  void updateContentWikidocActiontaskRel(WikiDocumentVO wikiDocument) throws WikiException
    {
//      String deleteSql = "delete from ContentWikidocActiontaskRel where contentWikidoc = :UContentWikidocId";
        Set<String> listOfReferencedActionTask = ParseUtil.getListOfActionTaskReferencedBy(wikiDocument.getUContent());
        List<ContentWikidocActiontaskRel> listOfRefObjs = new ArrayList<ContentWikidocActiontaskRel>();

        try
        {
            //prepare the ref obj list
            for (String refTaskName : listOfReferencedActionTask)
            {
                if (refTaskName.indexOf("#") == -1)
                {
                    refTaskName = refTaskName + "#" + wikiDocument.getUNamespace();
/*                    ResolveActionTaskVO taskVO = ActionTaskUtil.getResolveActionTask(null, refTaskName, "system");
                    if(taskVO != null)
                    {
                        refTaskName = taskVO.getUFullName();
                    } */
                }

                if (StringUtils.isNotBlank(refTaskName) && refTaskName.indexOf('#') > -1)
                {
                    //create the obj
                    ContentWikidocActiontaskRel cwwr = new ContentWikidocActiontaskRel();
                    cwwr.setUActiontaskFullname(refTaskName);
                    listOfRefObjs.add(cwwr);
                }

            }

          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(wikiDocument.getSys_id());
	            
	            //delete all the elements from the table
	            //HibernateUtil.deleteQuery("ContentWikidocActiontaskRel", "contentWikidoc ='" + wikiDocument.getSys_id() + "'");
	            HibernateUtil.deleteQuery("ContentWikidocActiontaskRel", "contentWikidoc", wikiDocument.getSys_id());
	
	            //insert the elements in the table
	            ContentWikidocActiontaskRelDAO daoRel = HibernateUtil.getDAOFactory().getContentWikidocActiontaskRelDAO();
	            for (ContentWikidocActiontaskRel obj : listOfRefObjs)
	            {
	                obj.setContentWikidoc(doc);
	                daoRel.persist(obj);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }

    }//updateContentWikidocActiontaskRel
    
    private static  void updateDTWikidocWikidocRel(WikiDocumentVO vo) throws WikiException
    {
        Set<String> listOfReferencedDocs = ParseUtil.getListOfWikiDocumentReferencedByDT(vo.getUDecisionTree());
//        listOfReferencedDocs.add(wikiDocument.getUFullname() + "_Decision");
        List<DTWikidocWikidocRel> listOfRefObjs = new ArrayList<DTWikidocWikidocRel>();

        try
        {
            //prepare the ref obj list
            for (String refDocName : listOfReferencedDocs)
            {
                DTWikidocWikidocRel cwwr = new DTWikidocWikidocRel();
                cwwr.setUWikidocFullname(refDocName);

                listOfRefObjs.add(cwwr);
            }

            
            HibernateProxy.execute(() -> {
            	  WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
                  WikiDocument doc = dao.findById(vo.getSys_id());

                  //delete all the elements from the table
                  //HibernateUtil.deleteQuery("DTWikidocWikidocRel", "dtWikidoc ='" + vo.getSys_id() + "'");

                  HibernateUtil.deleteQuery("DTWikidocWikidocRel", "dtWikidoc", vo.getSys_id());

                  //insert the elements in the table
                  DTWikidocWikidocRelDAO daoRel = HibernateUtil.getDAOFactory().getDTWikidocWikidocRelDAO();
                  for (DTWikidocWikidocRel obj : listOfRefObjs)
                  {
                      obj.setDtWikidoc(doc);
                      daoRel.persist(obj);
                  }
            });

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

    }//updateDTWikidocWikidocRel
    
    @SuppressWarnings("unchecked")
	private static  void updateResolveCompRelations(WikiDocumentVO vo)
    {
    	if (vo != null)
    	{
    		WikiDocument doc = new WikiDocument(vo);
    		Set<String> compNames = DecisionTreeHelper.getAllDTComponents(doc, "Document");
        	compNames.addAll(DecisionTreeHelper.getAllDTComponents(doc, "Subprocess"));
        	
        	Set<String> tasks = DecisionTreeHelper.getAllDTComponents(doc, "Task");
        	
        	if (compNames.size() > 0)
        	{
    			try
    			{
    				HibernateProxy.execute(() -> {
    					String query = "from ResolveCompRel where parentId =:parentId";
    		             
        				List<Object> objs = HibernateUtil.getCurrentSession().createQuery(query).
                                     setParameter("parentId", vo.getSys_id()).list();
        				
        				HibernateUtil.deleteObjs("ResolveCompRel", objs);
        				
        				ResolveCompRelDAO resolveCompRelDao = HibernateUtil.getDAOFactory().getResolveCompRelDAO();
    				
    				
    				
	    				
	    				for (String compName : compNames)
	            		{
	    					String childId = WikiUtils.getWikidocSysId(compName);
	    					if (StringUtils.isBlank(childId))
	                        {
	                            Log.log.error("Wiki/Runbook: " + compName + " could not be found in DT: " + vo.getUFullname() + ". If this is an import operation, try saving this DT again.");
	                            continue;
	                        }
	    					ResolveCompRel resolveCompRel = new ResolveCompRel();
	    					resolveCompRel.setParentId(vo.getSys_id());
	    					resolveCompRel.setParentType("DT");
	    					resolveCompRel.setChildId(childId);
	    					resolveCompRel.setChildType("Document");
	    					resolveCompRelDao.persist(resolveCompRel);
	            		}
    				
	    				for (String task : tasks)
	    				{
	    					String taskId = ActionTaskUtil.getActionIdFromFullname(task, "admin");
	    					if (StringUtils.isBlank(taskId))
	                        {
	                            Log.log.error("ActionTask: " + task + " could not be found in DT: " + vo.getUFullname() + ". If this is an import operation, try saving this DT again.");
	                            continue;
	                        }
	    					ResolveCompRel resolveCompRel = new ResolveCompRel();
	    					resolveCompRel.setParentId(vo.getSys_id());
	    					resolveCompRel.setParentType("DT");
	    					resolveCompRel.setChildId(taskId);
	    					resolveCompRel.setChildType("Task");
	    					resolveCompRelDao.persist(resolveCompRel);
	    				}
    				});
    			}
    			catch (Throwable e)
    	        {
    	            Log.log.error(e.getMessage(), e);
                                HibernateUtil.rethrowNestedTransaction(e);
    	        }
        	}
    	}
    }
    
    /**
     * API to be called from update script to clean all bad (dangling) references present in Wiki content and in main, abort and DT models.
     * 
     * @param docId : String respresenting WikiDoc sysId.
     */
    public static  void cleanWikidocBadReferences(String docId)
    {
        WikiDocumentVO wikiDocument = ServiceWiki.getWikiDoc(docId, null, "system");
        if (wikiDocument != null)
        {
            try
            {
                updateContentWikidocWikidocRel(wikiDocument);
            }
            catch(Exception e)
            {
                Log.log.error("Error while cleaning Wiki content of '" + wikiDocument.getUFullname()+ "'. Message: " + e.getMessage(), e);
            }
            
            try
            {
                updateMainWikidocWikidocRel(wikiDocument);
            }
            catch(Exception e)
            {
                Log.log.error("Error while cleaning Wiki main model of '" + wikiDocument.getUFullname()+ "'. Message: " + e.getMessage(), e);
            }
            try
            {
                updateExceptionWikidocWikidocRel(wikiDocument);
            }
            catch(Exception e)
            {  
                Log.log.error("Error while cleaning Wiki abort model of '" + wikiDocument.getUFullname()+ "'. Message: " + e.getMessage(), e);
            }
            
            updateDTWikiDocRelationship(wikiDocument);
        }
    }
    
    public static void updateAndCommitWikiDocStatsCounter(WikiDocStatsCounterName wikiDocStatsCounterName, String docId, String username)
    {        
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	WikidocStatistics ws = (WikidocStatistics) HibernateProxy.execute(() -> {
	            WikidocStatistics wikiDocStat = (WikidocStatistics) HibernateUtil.
	            					   createQuery("from WikidocStatistics w where w.wikidoc.id = :docId").
	            					   setParameter("docId", docId).uniqueResult();
	            
	            
	            if (wikiDocStat == null)
	            {
	                wikiDocStat = new WikidocStatistics();
	                
	                WikiDocument wd = WikiUtils.getWikiDocumentModel(docId, null, username, false);
	                
	                if (wd == null)
	                {
	                    wd = new WikiDocument();
	                    wd.setSys_id(docId);
	                }
	                
	                wikiDocStat.setWikidoc(wd);
	                wikiDocStat.setUViewCount(0);
	                wikiDocStat.setUEditCount(0);
	                wikiDocStat.setUExecuteCount(0);
	                wikiDocStat.setUReviewCount(0);
	                wikiDocStat.setUUsefulNoCount(0);
	                wikiDocStat.setUUsefulYesCount(0);
	                HibernateUtil.getCurrentSession().save(wikiDocStat);
	            }
	
	            Log.log.debug(String.format("Wiki Doc with sys_id = %s Statistics Before Update %s",
	            							wikiDocStat.getWikidoc().getSys_id(), wikiDocStat));
	            
	            Log.log.trace(String.format("Trying to acquire " + LockMode.UPGRADE + 
	                         			    " lock on wiki doc statistics for wiki doc with sys id %s ...", 
	                         			    wikiDocStat.getWikidoc().getSys_id()));
	            
	            HibernateUtil.getCurrentSession().lock(wikiDocStat, LockMode.UPGRADE);
	            
	            Log.log.trace(String.format("Acquired " + LockMode.UPGRADE + 
	                            		    " lock on wiki doc statistics for wiki doc with sys id %s !!!",
	                            		    wikiDocStat.getWikidoc().getSys_id()));
	            
	            updateWikiDocStatsCounter(wikiDocStatsCounterName, docId, username);
	            
	            if (wikiDocStatsCounterName == WikiDocStatsCounterName.VIEWED &&
	                wikiDocStatisticsViewCounter.keySet().contains(docId))
	            {
	                AtomicInteger a = wikiDocStatisticsViewCounter.remove(docId);
	                wikiDocStat.setUViewCount(wikiDocStat.getUViewCount() + a.get());
	            }
	            
	            if (wikiDocStatsCounterName == WikiDocStatsCounterName.EDITED && 
	                wikiDocStatisticsEditCounter.keySet().contains(docId))
	            {
	                AtomicInteger a = wikiDocStatisticsEditCounter.remove(docId);
	                wikiDocStat.setUEditCount(wikiDocStat.getUEditCount() + a.get());
	            }
	            
	            if (wikiDocStatsCounterName == WikiDocStatsCounterName.EXECUTED && 
	                wikiDocStatisticsExecuteCounter.keySet().contains(docId))
	            {
	                AtomicInteger a = wikiDocStatisticsExecuteCounter.remove(docId);
	                wikiDocStat.setUExecuteCount(wikiDocStat.getUExecuteCount() + a.get());
	            }
	            
	            Log.log.debug(String.format("Wiki Doc with sys_id = %s Statistics After Update %s",
	            							wikiDocStat.getWikidoc().getSys_id(), wikiDocStat));
	            return wikiDocStat;
            
        	});
            
            Log.log.trace(String.format("Released " + LockMode.UPGRADE + 
                         			    " lock on wiki doc statistics for wiki doc with sys id %s ***", 
                         			    ws.getWikidoc().getSys_id()));
            
            //update the view count in ES
            if(wikiDocStatsCounterName == WikiDocStatsCounterName.VIEWED && ws.getUViewCount() != null)
            {
                WikiIndexAPI.setClickCount(docId, new Long(ws.getUViewCount()), username);
            }
            
            //update the edit count in ES
            if(wikiDocStatsCounterName == WikiDocStatsCounterName.EDITED && ws.getUEditCount() != null)
            {
                WikiIndexAPI.setEditCount(docId, new Long(ws.getUEditCount()), username);
            }
            
            //update the execute count in ES
            if(wikiDocStatsCounterName == WikiDocStatsCounterName.EXECUTED && ws.getUExecuteCount() != null)
            {
                WikiIndexAPI.setExecuteCount(docId, new Long(ws.getUExecuteCount()), username);
            }
        }
        catch (Throwable ex)
        {
            Log.log.error(ex.getMessage(), ex);
                      HibernateUtil.rethrowNestedTransaction(ex);
        }
    }    
}
