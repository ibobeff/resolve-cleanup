/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.entities;

import java.util.ArrayList;
import java.util.Collection;

import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

public class MigrateNamespace extends Entities
{
    private String namespace;
    
    public MigrateNamespace(String namespace)
    {
        this.namespace = namespace;
    }
    

    private ResolveNodeVO migrateNamespace() throws Exception
    {
        ResolveNodeVO node = new ResolveNodeVO();
        node.setUCompName(namespace);
        node.setUCompSysId(namespace);
        node.setUType(NodeType.NAMESPACE);
        node.setUMarkDeleted(false);
        node.setULock(false);
        node.setUPinned(false);

        //persist
        ResolveNodeVO pnode = persistNode(node);
        Log.log.trace(Thread.currentThread().getName() + " : Migrated Namespace:" + namespace);
        
        return pnode;
    }
    
//    @Override
    public void run()
    {
        try
        {
            migrateNamespace();
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Namespace during the migration script. Please ignore this error as this will not affect the migration process. The namespace is " + namespace, e);
        }
        
    }
    
    @Override
    public Collection<String> call() throws Exception
    {
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        try
        {
            ResolveNodeVO pnode = migrateNamespace();
            
            if(pnode != null)
            {
                pCompSysIds.add(pnode.getUCompSysId());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Namespace during the migration script. Please ignore this error as this will not affect the migration process. The namespace is " + namespace, e);
            throw e;
        }
        
        return pCompSysIds;
    }
}
