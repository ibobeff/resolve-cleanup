package com.resolve.services.rr.util;

import java.util.LinkedList;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.util.Log;

import net.sf.json.JSONObject;

public class BulkTaskStatusRecorder
{
    private ObjectMapper mapper;
    private LinkedList<JSONObject> messages;
    private static final String STATE = "state";
    private static final String DETAIL = "detail";
    private static final String GLOBAL = "global";
    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private static final String RID = "rid";
    private static final String SCHEMA = "schema";
    private static final String LINE_NO = "line_no";
    private static final String LINE = "line";
    private static final String ID = "id";
    private static final String FINISHED = "finished";

    public BulkTaskStatusRecorder() {
        this.mapper = new ObjectMapper();
        this.messages = new LinkedList<JSONObject>();
    }
    public synchronized LinkedList<JSONObject> getMessages() throws Exception
    {
        return this.messages;
    }
    private synchronized void addMessage(JSONObject msg) {
        this.messages.add(msg);
        Log.log.info(msg.toString());
        if(this.messages.size()>30)
            this.messages.poll();
    }
    public void globalError(String detail) {
        JSONObject globalError = new JSONObject();
        globalError.put(STATE,GLOBAL);
        globalError.put(DETAIL, detail);
        this.addMessage(globalError);
    }
    
    public void exportSuccess(String id,String rid,String schema,String detail) {
        JSONObject mappingOpMsg = new JSONObject();
        mappingOpMsg.put(STATE, SUCCESS);
        mappingOpMsg.put(ID,id);
        mappingOpMsg.put(RID, rid);
        mappingOpMsg.put(SCHEMA, schema);
        this.addMessage(mappingOpMsg);
    }
    
    public void importSuccess(int num,String line) {
        JSONObject mappingOpMsg = new JSONObject();
        mappingOpMsg.put(STATE, SUCCESS);
        mappingOpMsg.put(LINE_NO,num);
        mappingOpMsg.put(LINE, line);
        this.addMessage(mappingOpMsg);
    }
    public void exportFailure(String id,String rid,String schema,String detail) {
        JSONObject mappingOpMsg = new JSONObject();
        mappingOpMsg.put(STATE, FAILURE);
        mappingOpMsg.put(ID, id);
        mappingOpMsg.put(RID, rid);
        mappingOpMsg.put(SCHEMA, schema);
        this.addMessage(mappingOpMsg);
    }
    public void importFailure(int num,String line,String detail) {
        JSONObject mappingOpMsg = new JSONObject();
        mappingOpMsg.put(STATE, FAILURE);
        mappingOpMsg.put(LINE_NO, num);
        mappingOpMsg.put(LINE, line);
        mappingOpMsg.put(DETAIL, detail);
        this.addMessage(mappingOpMsg);
    }
    public void finish() {
        JSONObject mappingOpMsg = new JSONObject();
        mappingOpMsg.put(FINISHED, "finished.");
        this.addMessage(mappingOpMsg);
    }
}
