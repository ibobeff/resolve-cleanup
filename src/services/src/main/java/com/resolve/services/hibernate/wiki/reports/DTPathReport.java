/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.services.jdbc.util.GenericJDBCHandler;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public abstract class DTPathReport
{
    protected DTReportStatusType status = null;// Possible values ==> COMPLETED, ABORTED
    protected ReportDurationType type = ReportDurationType.day; //Possible values ==> day, week, month, year
    protected String dtFullNames; //comma seperated DT full names
    protected String namespaces = null; //comma separted values of namespaces
    protected String authors = null; //comma seperated values of authors

    //date range for the query
    protected Long startDate = null;
    protected Long endDate = null;
    
    //pagination
    protected int start = -1;
    protected int limit = -1;
    protected int page = 1;
    
    public DTPathReport(QueryDTO query, String username) throws Exception
    {
        if(query == null)
            throw new Exception("QueryDTO is mandatory");
        
        evaluateQueryDTO(query);
    }
    
    public abstract ResponseDTO<Map<String, Object>> getData() throws Exception;
    
    protected List<Map<String, Object>> executeSql() throws Exception
    {
        List<Map<String, Object>> rawData = new ArrayList<Map<String, Object>>();
        String sql = getSql();
        
        try
        {
           rawData = GenericJDBCHandler.executeSQLSelect(sql, -1, -1, false);
        }
        catch (Exception e)
        {
            Log.log.error("Error in execting sql: " + sql);
            throw e;
        }
        
        return rawData;
    }
    
    protected Collection<DTGroupByPathIdDTO> getRecordsByPathId() throws Exception
    {
        Collection<DTGroupByPathIdDTO> result = new ArrayList<DTGroupByPathIdDTO>();
        
        List<Map<String, Object>> data = executeSql();
        if(data != null)
        {
            DTGroupByPathIdDTO dTGroupByPathIdDTO = null;
            
            for (Map<String, Object> record : data)
            {
                DTRecord dtRecord = new DTRecord(record);
                String recPathId = dtRecord.getPathId();
                
                //for the first time, it will be null
                if(dTGroupByPathIdDTO == null)
                {
                    dTGroupByPathIdDTO = new DTGroupByPathIdDTO();
                    dTGroupByPathIdDTO.setPathId(dtRecord.getPathId());
                }
                

                if(recPathId.equalsIgnoreCase(dTGroupByPathIdDTO.getPathId()))
                {
                    dTGroupByPathIdDTO.getRecords().add(dtRecord);
                }
                else
                {
                    //add it to collection
                    result.add(dTGroupByPathIdDTO);
                    
                    //reset it
                    dTGroupByPathIdDTO = new DTGroupByPathIdDTO();
                    dTGroupByPathIdDTO.setPathId(dtRecord.getPathId());
                    dTGroupByPathIdDTO.getRecords().add(dtRecord);
                }
            }//end of for loop   
            
            //add the last record to the collection
            if(dTGroupByPathIdDTO != null)
            {
                result.add(dTGroupByPathIdDTO);
            }
            
        }
        
        return result;
    }
    
    protected String getSql()
    {
        StringBuilder sql = new StringBuilder("SELECT lookup.u_dt_root_doc, dt.dtnamespace, dt.pathid, dt.nodeid, dt.seqid, dt.userid, dt.ts, dt.ts_datetime, dt.node_cnt, dt.tot_cnt, dt.tot_time, dt.status ");
        sql.append(" FROM tmetric_lookup lookup ");//.append("\n");
        sql.append("      JOIN ");
        if(this.type == ReportDurationType.day || this.type == ReportDurationType.week)
        {
            sql.append(" tmetric_hr_dt dt ");
        }
        else
        {
            sql.append(" tmetric_day_dt dt ");
        }
        sql.append(" ON lookup.u_path_id = dt.pathid ");//.append("\n");
        
        
        sql.append(" WHERE 1=1 ");//.append("\n");

        Map<Integer, Object> queryParams = new HashMap<Integer, Object>();
        
        sql.append(filterOnStatus(queryParams));
        sql.append(filterTimeRange());
        sql.append(filterDTFullNames(queryParams));
        sql.append(filterNamespaces(queryParams));
        sql.append(filterAuthors(queryParams));

        sql.append(" ORDER BY dt.ts_datetime, dt.pathid, dt.seqid");//.append("\n");
        
        Log.log.debug("Sql:" + sql.toString());

        return sql.toString();
    }
    
    private void evaluateQueryDTO(QueryDTO query) throws Exception
    {
        /**
            filter:[{"field":"uisActive","type":"bool","condition":"equals","value":true},{"field":"uhasActiveModel","type":"bool","condition":"equals","value":true},{"field":"uisRoot","type":"bool","condition":"equals","value":true}]
            page:1
            start:0
            limit:50
            group:[{"property":"unamespace","direction":"ASC"}]
            sort:[{"property":"unamespace","direction":"ASC"},{"property":"sysUpdatedOn","direction":"DESC"}]         
            
        */

        if (query != null)
        {
            List<QueryFilter> filters = query.getFilterItems();
            if (filters != null && filters.size() > 0)
            {
                for (QueryFilter filter : filters)
                {
                    setFilter(filter);
                }
            }
            
            if(StringUtils.isNotBlank(query.getInterval()))
            {
                this.type = ReportDurationType.valueOf(query.getInterval());
            }
            else
            {
                Log.log.debug("Interval is not available for this query.");
            }
            
            
//            if(startDate == null || endDate == null)
//            {
//                throw new Exception("StartDate and Enddate range is mandatory.");
//            }

            this.page = query.getPage();
            this.start = query.getStart();
            this.limit = query.getLimit();

        }//end of if
    }
    
    private void setFilter(QueryFilter filter)
    {
        if(filter.isValid())
        {
            String field = filter.getField();
            if (field.equalsIgnoreCase("status"))
            {
                this.status = DTReportStatusType.valueOf(filter.getValue().toUpperCase());
            }
//            else if (field.equalsIgnoreCase("interval")) // used at the QUeryDTO level...kept incase we use this
//            {
//                this.type = ReportDurationType.valueOf(filter.getValue());
//            }
            else if (field.equalsIgnoreCase("dtFullNames"))
            {
                this.dtFullNames = filter.getValue();
            }
            else if (field.equalsIgnoreCase("namespaces"))
            {
                this.namespaces = filter.getValue();
            }
            else if (field.equalsIgnoreCase("authors"))
            {
                this.authors = filter.getValue();
            }
            else if (field.equalsIgnoreCase("sysCreatedOn"))
            {
                String startValue = filter.getStartValue();
                this.startDate = DateUtils.convertStringToTimesInMillis(startValue, "yyyy-MM-dd'T'HH:mm:ssZ");
                
                String endValue = filter.getEndValue();
                this.endDate = DateUtils.convertStringToTimesInMillis(endValue, "yyyy-MM-dd'T'HH:mm:ssZ");
            }
        }
    }

    private String filterOnStatus(Map<Integer, Object> queryParams)
    {
        StringBuilder whereClause = new StringBuilder("");

        if (this.status != null )
        {
            //whereClause.append(" and dt.status = '").append(this.status.name()).append("'");//.append(" \n");
            whereClause.append(" and dt.status = ?");//.append(" \n");
            queryParams.put(new Integer(queryParams.size() + 1), this.status.name());
        }

        return whereClause.toString();
    }
    
    private String filterTimeRange()
    {
        StringBuilder whereClause = new StringBuilder("");

        if (this.startDate > -1 && this.endDate > -1)
        {
            whereClause.append(" and dt.ts >= ").append(this.startDate).append(" and dt.ts <= ").append(this.endDate);//.append(" \n");
        }

        return whereClause.toString();
    }
    
    private String filterDTFullNames(Map<Integer, Object> queryParams)
    {
        StringBuilder whereClause = new StringBuilder("");

        if (StringUtils.isNotBlank(dtFullNames))
        {
            //to make it case-insensitive
            dtFullNames = dtFullNames.toLowerCase().trim();
            
            Set<String> dtFullNamesSet = new HashSet<String>(StringUtils.convertStringToList(dtFullNames, ","));
            //whereClause.append(" and  LOWER(lookup.u_dt_root_doc) IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(ns)) + ")");//.append("\n");
            whereClause.append(" and  LOWER(lookup.u_dt_root_doc) IN (" + SQLUtils.prepareParametrerizedQueryString(new ArrayList<String>(dtFullNamesSet)) + ")");//.append("\n");
            
            for (String dtFullName : dtFullNamesSet)
            {
                queryParams.put(new Integer(queryParams.size() + 1), dtFullName);
            }
        }

        return whereClause.toString();
    }
    
    private String filterNamespaces(Map<Integer, Object> queryParams)
    {
        StringBuilder whereClause = new StringBuilder("");

        if (StringUtils.isNotBlank(namespaces))
        {
            //to make it case-insensitive
            namespaces = namespaces.toLowerCase().trim();
            
            Set<String> nameSpacesSet = new HashSet<String>(StringUtils.convertStringToList(namespaces, ","));
            //whereClause.append(" and  LOWER(dt.dtnamespace) IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(nameSpacesSet)) + ")");//.append("\n");
            whereClause.append(" and  LOWER(dt.dtnamespace) IN (" + SQLUtils.prepareParametrerizedQueryString(new ArrayList<String>(nameSpacesSet)) + ")");//.append("\n");
            
            for (String nameSpace : nameSpacesSet)
            {
                queryParams.put(new Integer(queryParams.size() + 1), nameSpace);
            }
        }

        return whereClause.toString();
    }

    private String filterAuthors(Map<Integer, Object> queryParams)
    {
        StringBuilder whereClause = new StringBuilder("");

        if (StringUtils.isNotBlank(authors))
        {
            //to make it case-insensitive
            authors = authors.toLowerCase().trim();
            
            Set<String> authorsSet = new HashSet<String>(StringUtils.convertStringToList(authors, ","));
            //whereClause.append(" and  LOWER(dt.userid) IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(authorsSet)) + ")");//.append("\n");
            whereClause.append(" and  LOWER(dt.userid) IN (" + SQLUtils.prepareParametrerizedQueryString(new ArrayList<String>(authorsSet)) + ")");//.append("\n");
            
            for (String author : authorsSet)
            {
                queryParams.put(new Integer(queryParams.size() + 1), author);
            }
        }

        return whereClause.toString();
    }    
}
