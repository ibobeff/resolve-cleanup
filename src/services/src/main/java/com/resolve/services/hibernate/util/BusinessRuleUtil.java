/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.ResolveBusinessRule;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveBusinessRuleVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class BusinessRuleUtil
{
	private static final String U_ORDER = "UOrder";
	
    public static List<ResolveBusinessRuleVO> getResolveBusinessRules(QueryDTO query, String username)
    {
        List<ResolveBusinessRuleVO> result = new ArrayList<ResolveBusinessRuleVO>();

        //manipulate the qry
        manipuateQueryDTOForBusniessRuleGrid(query);
        
        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if (data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveBusinessRule instance = new ResolveBusinessRule();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveBusinessRule instance = (ResolveBusinessRule) o;
                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    public static ResolveBusinessRuleVO findBusinessRuleByIdOrName(String sys_id, String brName, String username)
    {
        ResolveBusinessRule model = null;
        ResolveBusinessRuleVO result = null;

        try
        {
            model = findBusinessRuleModelByIdOrName(sys_id, brName, username);
            if (model != null)
            {
                result = convertModelToVO(model, null);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }


        return result;
    }

    public static ResolveBusinessRuleVO saveBusinessRule(ResolveBusinessRuleVO vo, String username) throws Exception
    {
        if (vo != null && vo.validate())
        {
            ResolveBusinessRule model = null;
            if (StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                //UPDATE
                //first check if the BR with the same name already exist
                model = findBusinessRuleModelByIdOrName(null, vo.getUName(), username);
                if(model != null && !vo.getSys_id().equalsIgnoreCase(model.getSys_id()))
                {
                    throw new Exception("Business Rule with name " + vo.getUName() + " already exist");
                }
                
                model = findBusinessRuleModelByIdOrName(vo.getSys_id(), null, username);
            }
            else
            {
                //insert
                model = findBusinessRuleModelByIdOrName(null, vo.getUName(), username);
                if(model != null)
                {
                    throw new RuntimeException("'" + vo.getUName() + "' already exist. Please try again.");
                }
                
                model = new ResolveBusinessRule();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveResolveBusinessRule(model, username);
            vo = convertModelToVO(model, null);
        }

        return vo;
    }

    public static void deleteBusinessRulesByIds(String[] sysIds, String username) throws Exception
    {
        if (sysIds != null)
        {
            for (String sysId : sysIds)
            {
                deleteBusinessRuleById(sysId, username);
            }
        }
    }

    private static void deleteBusinessRuleById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
	
	            ResolveBusinessRule model = HibernateUtil.getDAOFactory().getResolveBusinessRuleDAO().findById(sysId);
	            if (model != null)
	            {
	                HibernateUtil.getDAOFactory().getResolveBusinessRuleDAO().delete(model);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    @SuppressWarnings("rawtypes")
    private static ResolveBusinessRule findBusinessRuleModelByIdOrName(String sys_id, String brName, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (ResolveBusinessRule) HibernateProxy.execute(() -> {
        		ResolveBusinessRule result = null;
	            if (StringUtils.isNotEmpty(sys_id))
	            {
	                result = HibernateUtil.getDAOFactory().getResolveBusinessRuleDAO().findById(sys_id);
	            }
	            else if (StringUtils.isNotEmpty(brName))
	            {
	                String hql = " from ResolveBusinessRule where upper(UName) = '" + brName.toUpperCase() + "'";
	                Query query = HibernateUtil.createQuery(hql);
	                List<?> temp = query.list();
	                if (temp.size() > 0)
	                {
	                    result = (ResolveBusinessRule) temp.get(0);
	                }
	                else
	                {
	                    result = null;
	                }
	            }
	            return result;
        	});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return null;
    }

    private static ResolveBusinessRuleVO convertModelToVO(ResolveBusinessRule model, Map<String, String> mapOfOrganization)
    {
        ResolveBusinessRuleVO vo = null;

        if (model != null)
        {
            if (mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }

            vo = model.doGetVO();
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }
        }

        return vo;
    }
    
    private static void manipuateQueryDTOForBusniessRuleGrid(QueryDTO query) {
    	
        List<QueryFilter> filters = query.getFilters();
        
        if(filters != null && filters.size() > 0) {
        	QueryFilter uOrderFilter = filters.stream().filter(f -> U_ORDER.equalsIgnoreCase(f.getField())).
					   findAny().orElse(null);

        	if (uOrderFilter != null && !QueryFilter.INT_TYPE.equals(uOrderFilter.getType())) {
        		uOrderFilter.setType(QueryFilter.INT_TYPE);
        	}
        }
    }

}
