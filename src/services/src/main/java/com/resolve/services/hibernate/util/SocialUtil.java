/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.persistence.model.UserPreferences;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialUtil
{
    public static String convertRolesToSocialRolesString(String csvRoles)
    {
        String result = "";
        
        if(StringUtils.isNotEmpty(csvRoles))
        {
            Set<String> roles = new HashSet<String>(StringUtils.convertStringToList(csvRoles, ","));
            result = StringUtils.convertCollectionToString(roles.iterator(), " ");
        }
        
        return result;
    }
    

    public static String convertSocialRolesStringToRoles(String socialRoles)
    {
        return (StringUtils.isNotEmpty(socialRoles) ? socialRoles.replaceAll(" ", ","): "");
    }

    @Deprecated
    public static RSComponent createWorksheetNode(String sysId, String username) throws Exception
    {
        return createWorksheetNode(sysId, username, null, null);
    }
    
    public static RSComponent createWorksheetNode(String sysId, String username, String orgId, String orgName) throws Exception
    {
        RSComponent result = null;
        
        if(StringUtils.isNotBlank(sysId))
        {
            com.resolve.search.model.Worksheet data = WorksheetUtil.getModelByID(sysId, "admin", "PST", orgId, orgName);
            if(data != null && StringUtils.isNotBlank(data.getSysId()))
            {
                ResolveNodeVO nodeVO = ServiceGraph.findNode(null, data.getSysId(), null, NodeType.WORKSHEET, username);
                if(nodeVO == null)
                {
                    nodeVO = new ResolveNodeVO();
                    nodeVO.setUCompName(data.getNumber());
                    nodeVO.setUCompSysId(data.getSysId());
                    nodeVO.setUType(NodeType.WORKSHEET);
                    nodeVO.setUMarkDeleted(false);
                    nodeVO.setULock(false);
                    nodeVO.setUPinned(false);
                    nodeVO.setUReadRoles("resolve_process resolve_dev admin postmrole");
                    
                    if (StringUtils.isNotBlank(orgId) || StringUtils.isNotBlank(orgName))
                    {
                        String intOrgId = orgId;
                        
                        if (StringUtils.isBlank(intOrgId))
                        {
                            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                            
                            if (orgsVO != null)
                            {
                                intOrgId = orgsVO.getSys_id();
                            }
                        }
                        
                        if (StringUtils.isNotBlank(intOrgId))
                        {
                            nodeVO.setSysOrg(intOrgId);
                        }
                    }
                    
                    //persist the node
                    ServiceGraph.persistNode(nodeVO, username);
                }
                
                //prepare the comp obj
                result = new Worksheet(nodeVO);
            }
        }
        
        return result;
    }
    
    
    public static Map<String, String> getUserSocialPreferences(String username)
    {
        Map<String, String> prefMap = new HashMap<String, String>();

        // prepare the map
        if (StringUtils.isNotEmpty(username))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
                	
                	Users user = new Users();
                    user.setUUserName(username);
                	
                    user = HibernateUtil.getDAOFactory().getUsersDAO().findFirst(user);
                    if (user != null)
                    {
                        Collection<UserPreferences> prefList = user.getSocialPreferences();
                        if (prefList != null && prefList.size() > 0)
                        {
                            for (UserPreferences sp : prefList)
                            {
                                prefMap.put(sp.getUPrefKey(), sp.getUPrefValue());
                            }
                        }
                    }

                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return prefMap;
    }
    
    public static Map<String, Object> findTeamByIdOrName(String sysId, String name)
    {
        Map<String, Object> record = null;
        
        if(StringUtils.isNotEmpty(sysId))
        {
            record = CustomTableUtil.getCustomTableRecord(SocialTeamDTO.TABLE_NAME, sysId);
        }
        else if(StringUtils.isNotEmpty(name))
        {
            Map<String, Object> example = new HashMap<String, Object>();
            example.put("u_display_name", name.trim() );
            
            record = CustomTableUtil.findFirstCustomTableRecord(SocialTeamDTO.TABLE_NAME, example);                
        }
        
        return record;
    }
    
    public static void setFollowStreams(User user, List<String> ids, boolean follow) throws Exception
    {
        ResolveNodeVO userNode = ServiceGraph.findNode(null, user.getId(), null, NodeType.USER, user.getName());
        if(userNode != null)
        {
            if(ids != null && ids.size() > 0)
            {
                Collection<ResolveNodeVO> dstComponents = new ArrayList<ResolveNodeVO>();
                for(String id : ids)
                {
                	ResolveNodeVO dstComponent = null;
                	try {
                        dstComponent = ServiceGraph.findNode(null, id, null, null, user.getName());
                	} catch (Exception e) {
                		Log.log.warn("Node not found for userId " + user.getId());
                	}

                    if(dstComponent != null)
                    {
                        dstComponents.add(dstComponent);
                    }
                    else
                    {
                        Log.log.debug("Comp with Id '" + id + "' does not exist.");
                    }
                }//end of for
                
                if(dstComponents.size() > 0)
                {
                    if(follow)
                    {
                        ServiceGraph.follow(userNode.getSys_id(), null, null, null, dstComponents, null, user.getName());
                    }
                    else
                    {
                        ServiceGraph.unfollow(userNode.getSys_id(), null, null, null, dstComponents, user.getName());
                    }
                    
                    //post Follow operations
                    postFollowOperations(user, dstComponents, follow);
                }
            }
        }
        else
        {
            Log.log.error("This user not available in GraphDB:" + user);
            throw new Exception();
        }
    }
    
    public static void removeRssCronJob(String sysId, String rssName)
    {
        Map<String, String> content = new HashMap<String, String>();
        content.put(Constants.RSS_NAME, rssName);
        content.put(Constants.RSS_SYS_ID, sysId);
        
        MainBase.getESB().sendMessage("RSCONTROLS", "MSocial.removeRSS", content);
        
    }
    
    
    public static List<User> getAllUsersFollowingThisStream(String streamId, String username) throws Exception
    {
        List<User> users = new ArrayList<User>();
        
        if(StringUtils.isNotBlank(streamId))
        {
            Collection<ResolveNodeVO> nodes = ServiceGraph.getNodesFollowingMe(null, streamId, null, null, username);
            if(nodes != null && nodes.size() > 0)
            {
                for(ResolveNodeVO node : nodes)
                {
                    if(node.getUType() == NodeType.USER)
                    {
                        users.add(new User(node));
                    }
                }
            }
        }
        
        return users;
    }

    public static List<User> getListOfUsersThisUserIsFollowing(User user)  throws Exception
    {
        List<User> users = new ArrayList<User>();
        
        if(user != null && StringUtils.isNotBlank(user.getSys_id()))
        {
            Collection<ResolveNodeVO> nodes = ServiceGraph.getNodesFollowedBy(null, user.getSys_id(), null, NodeType.USER, "system");
            if(nodes != null && nodes.size() > 0)
            {
                for(ResolveNodeVO node : nodes)
                {
                    if(node.getUType() == NodeType.USER)
                    {
                        users.add(new User(node));
                    }
                }
            }
        }
        
        return users;
    }
    
    public static void submitNotifications(List<SubmitNotification> notifications, boolean isAsync)
    {
        if (isAsync)
        {
            submitBulkNotificationESB(notifications);
        }
        else
        {
            for (SubmitNotification notification : notifications)
            {
                submitNotification(notification, isAsync);
            }
        }
    }
    
	public static void submitNotification(SubmitNotification submitNotification, boolean isAsync)
    {
        if (isAsync)
        {
            submitNotificationESB(submitNotification);
        }
        else
        {
        	try
            {
                submitNotification(submitNotification);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    private static void submitNotificationESB(SubmitNotification submitNotification)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("submitNotification", submitNotification);

        MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.submitNotification", params);
    }
    
    private static void submitBulkNotificationESB(List<SubmitNotification> notifications)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bulkNotifications", notifications);

        MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.submitBulkNotifications", params);
    }
    
    private static void loadRss(ResolveNodeVO rssNode)
    {
        if(rssNode != null)
        {
            try
            {
                Collection<ResolveNodeVO> usersFollowingRSS = ServiceGraph.getNodesFollowingMe(null, rssNode.getUCompSysId(), null, NodeType.RSS, "system");
                if(usersFollowingRSS != null && usersFollowingRSS.size() > 0)
                {
                    Map<String, String> content = new HashMap<String, String>();
                    content.put(Constants.RSS_NAME, rssNode.getUCompName());
                    content.put(Constants.RSS_SYS_ID, rssNode.getUCompSysId());

                    MainBase.getESB().sendMessage("RSCONTROL", "MSocial.loadRSS", content);
                }
                else
                {
                    removeRssCronJob(rssNode.getUCompSysId(), rssNode.getUCompName());
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error in loadRss", e);
            }
        }
    }//loadRSS
    
    private static void removeRss(ResolveNodeVO rssNode)
    {
        if (rssNode != null)
        {
            try
            {
                Collection<ResolveNodeVO> usersFollowingRSS = ServiceGraph.getNodesFollowingMe(null, rssNode.getUCompSysId(), null, NodeType.RSS, "system");
                if (usersFollowingRSS == null || usersFollowingRSS.size() == 0)
                {
                    removeRssCronJob(rssNode.getUCompSysId(), rssNode.getUCompName());
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error in loadRss", e);
            }
        }
    }
    
    private static void postFollowOperations(final User user, final Collection<ResolveNodeVO> dstComponents, 
    		final boolean follow) throws Exception
    {
        if (CollectionUtils.isEmpty(dstComponents)) {
        	return;
        }
        
		dstComponents.stream().filter(dstComponent -> dstComponent.getUType() == NodeType.RSS)
				.forEach(dstComponent -> {
					if (follow) {
						loadRss(dstComponent);
					} else {
						removeRss(dstComponent);
					}
				});
    }
    
    public static List<RSComponent> additionalFilterCriteria(List<RSComponent> comps)
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        if(comps != null)
        {
            result.addAll(comps);
        }
        
        return result;
    }
    
    public static void submitESBForUpdateSocialForWiki(Map<String, Object> params)
    {
        MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.updateSocialForWiki", params);
    }
    
    @SuppressWarnings("unused")
    private static SubmitNotification prepareNotificationForUserFollowingComp(User user, RSComponent comp, boolean follow)
    {
        SubmitNotification submitNotification = null;
        
        return submitNotification;
    }
    
   private static void submitNotification(SubmitNotification socialNotification) throws Exception
   {
       if (socialNotification == null)
       {
           Log.log.error("SubmitNotification cannot be null.");
           throw new Exception("SubmitNotification cannot be null.");
       }
       else if (socialNotification.getCompSysId() == null || socialNotification.getEventType() == null 
    		   || socialNotification.getUsername() == null)
       {
           Log.log.error("Eventtype, sysId and username cannot be null.");
           throw new Exception("Eventtype, sysId and username cannot be null.");
       }
       
       if (StringUtils.isBlank(socialNotification.getCompSysId()) || socialNotification.getEventType() == null) {
    	   return;
       }
       
       List<String> targetSysIds = new LinkedList<>();
       
       try
       {
           List<RSComponent> users = ServiceSocial.findUsersRegisteredToReceiveThisNotificationForComp(
        		   socialNotification.getCompSysId(), socialNotification.getEventType());
           if(users != null)
           {
               for(RSComponent user : users)
               {
                   targetSysIds.add(user.getSys_id());
               }
           }
           
           //post the notification only if there are any users associated with it.
           if(targetSysIds.size() > 0)
           {
               ServiceSocial.postSystemMessage(socialNotification.getSubject(), socialNotification.getContent(), 
            		   socialNotification.getUsername(), targetSysIds, MainBase.viewResolveUrl);
           }
           else
           {
               Log.log.info("Ignoring system notification " + socialNotification.getEventType() + " for component " + socialNotification.getCompDisplayName());
           }
       }
       catch (Exception e)
       {
           Log.log.info("Ignoring system notification " + socialNotification.getEventType() + " for component " + socialNotification.getCompDisplayName());
       }
   }
}
