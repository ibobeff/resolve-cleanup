/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.follow;

import java.util.ArrayList;
import java.util.Collection;

import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.StringUtils;

/**
 * This is similar to TeamFollow except for it keeps the heirarchy structure 
 * 
 * @author jeet.marwah
 *
 */
public class TeamTreeGraph
{
    private boolean ignoreUsers = false; // if its true, then the result will filter out Users and return only Teams
    private boolean recurse = false;
    private String username = null;
    private ResolveNodeVO rootTeamNode = null;
    
    private Team team = null;
    
    public TeamTreeGraph(String teamID, boolean recurse, String username) throws Exception
    {
        if(StringUtils.isEmpty(teamID))
        {
            throw new Exception("Team Id is mandatory.");
        }
        
        rootTeamNode = ServiceGraph.findNode(null, teamID, null, NodeType.TEAM, username);
        if(rootTeamNode == null)
        {
            throw new Exception("Team with team Id " + teamID + " does not exist");
        }
        
        this.recurse = recurse;
        this.username = username;
        
        team = new Team(rootTeamNode);
        team.setTeams(new ArrayList<Team>());
        team.setUsers(new ArrayList<User>());
        team.setDirectFollow(true);
    }
    
    //only used internally
    private TeamTreeGraph(String teamID, boolean recurse, boolean isDirectFollow, String username) throws Exception
    {
        this(teamID, recurse, username);
        team.setDirectFollow(isDirectFollow);
    }
    
    public Team getTeam()  throws Exception
    {
        Collection<ResolveNodeVO> nodesThatThisTeamContains = ServiceGraph.getNodesFollowedBy(rootTeamNode.getSys_id(), null, null, null, username);
        if(nodesThatThisTeamContains != null && nodesThatThisTeamContains.size() > 0)
        {
            
            for(ResolveNodeVO node : nodesThatThisTeamContains)
            {
                NodeType type = node.getUType();
                if(type == NodeType.TEAM)
                {
                    if(recurse)
                    {
                        //**RECURSIVE
                        team.getTeams().add(new TeamTreeGraph(node.getUCompSysId(), true, false, username).getTeam());
                    }
                    else
                    {
                        team.getTeams().add(new Team(node));
                    }
                }
            }
        }
        
        if(!isIgnoreUsers())
        {
            Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(rootTeamNode.getSys_id(), null, null, null, username);
            if (nodesFollowingMe != null)
            {
                for (ResolveNodeVO nodeFollowingMe : nodesFollowingMe)
                {
                    NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                    if (nodeFollowingMeType == NodeType.USER)
                    {
                        team.getUsers().add(new User(nodeFollowingMe));
                    }
                }
            }
        }
        
        
        
        return team;
    }
    
    public boolean isIgnoreUsers()
    {
        return ignoreUsers;
    }

    public void setIgnoreUsers(boolean ignoreUsers)
    {
        this.ignoreUsers = ignoreUsers;
    }

}
