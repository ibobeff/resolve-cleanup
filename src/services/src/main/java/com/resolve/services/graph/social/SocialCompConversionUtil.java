/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ServiceSocialConstants;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.SocialAdminUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

/**
 * util to transform object for social 
 * 
 * @author jeet.marwah
 *
 */
public class SocialCompConversionUtil
{

    
    ////////////////////////////// User ///////////////////////////////////////////////
    
    public static User getSocialUser(String username)
    {
        //roles were not getting populated if we are not using the No Cache
        UsersVO user = UserUtils.getUserWithRolesOnly(username);
        return convertUserToSocialUser(user, null);
    }// getSocialUser
    
    public static User getSocialUserById(String userSysId)
    {
        UsersVO user = null;
        if (StringUtils.isNotEmpty(userSysId))
        {
            user = UserUtils.findUserWithRolesById(userSysId);
        }
        return convertUserToSocialUser(user, null);
    }// getSocialUserById
    
    public static User convertUserToSocialUser(UsersVO user, User socialUser)
    {
        if (user != null)
        {
            
            String displayName = "";
            if(StringUtils.isNotEmpty(user.getUFirstName()))
            {
                displayName = user.getUFirstName();
            }
            
            if(StringUtils.isNotBlank(user.getULastName()))
            {
                if(StringUtils.isNotBlank(displayName))
                {
                    displayName = displayName + " " + user.getULastName();
                }
                else
                {
                    displayName = user.getULastName();
                }
            }
            
            //if its blank, use the username
            if(StringUtils.isBlank(displayName))
            {
                displayName = user.getUUserName();
            }
            
            
            String sysOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;

            if(socialUser == null)
            {
                socialUser = new User();
            }
            
            socialUser.setSys_id(user.getSys_id());
            socialUser.setId(user.getSys_id());
            socialUser.setDisplayName(displayName);
            socialUser.setName(user.getUUserName());
            socialUser.setLock(user.getULockedOut() != null ? user.getULockedOut() : false);
            socialUser.setRoles(StringUtils.isNotEmpty(user.getRoles()) ? SocialUtil.convertRolesToSocialRolesString(user.getRoles()) : "");
        }

        return socialUser;
    }

    public static User convertUserToSocialUser(Users user)
    {
        return convertUserToSocialUser(user.doGetVO(), null);
    }// convertUsersToSocialUser
    
    
    ////////////////////////////// Worksheet ///////////////////////////////////////////////

    
    
//    public static Worksheet createWorksheet(String sysId)
//    {
//        Worksheet result = null;
//        
//        if(StringUtils.isNotBlank(sysId))
//        {
//            com.resolve.search.model.Worksheet data = WorkSheetService.getInstance().getModelByID(sysId, "admin", "PST");
//            if(data != null && StringUtils.isNotBlank(data.getSysId()))
//            {
//                result = createWorksheet(data);
//            }
//        }
//
//        return result;
//    }
    
//    private static Worksheet createWorksheet(com.resolve.search.model.Worksheet data)
//    {
//        Worksheet result = null;
//
//        if (data != null)
//        {
//            result = new Worksheet();
//            result.setSys_id(data.getSysId());
//            result.setId(data.getSysId());
//            result.setDisplayName(data.getNumber());
//            result.setName(data.getNumber());
//            
//            result.setRoles("resolve_process resolve_dev postmrole");
//            result.setLock(false);
//        }
//
//        return result;
//    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /////////////////////////////////// OLD APIS
    
    ////////////////////////////// ActionTask ///////////////////////////////////////////////
    
    public static ActionTask createActionTask(String sysId)
    {
        ResolveActionTaskVO task = ServiceHibernate.getResolveActionTask(sysId, null, "system");
        ActionTask target = convertResolveActionTaskVOToActionTask(task, null);

        return target;
    }// createActionTask
    
    public static ActionTask createActionTaskByName(String actiontaskName)
    {
        ResolveActionTaskVO task = ServiceHibernate.getResolveActionTask(null, actiontaskName, "system");
        ActionTask target = convertResolveActionTaskVOToActionTask(task, null);

        return target;
    }// createActionTask
    
    public static ActionTask convertResolveActionTaskVOToActionTask(ResolveActionTaskVO vo, ActionTask result)
    {
        if(vo != null)
        {
            if(result == null)
            {
                result = new ActionTask();
            }
            
            result.setSys_id(vo.getSys_id());
            result.setId(vo.getSys_id());
//            result.setSys_org(vo.getSysOrg());
            result.setDisplayName(vo.getUFullName());
            
            AccessRightsVO ar = vo.getAccessRights();
            if(ar != null)
            {
                result.setRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                result.setEditRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUWriteAccess() + "," + ar.getUAdminAccess()));
                result.setPostRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
            }
        }
        
        return result;
    }  
    
    public static List<ActionTask> getActiontaskModelsFromNames(List<String> atFullName)
    {
        List<ActionTask> listOfATs = new ArrayList<ActionTask>();
        
        if (atFullName != null && atFullName.size() > 0)
        {
            //String sqlSysIds = SQLUtils.prepareQueryString(atFullName);
            //String sql = "select a.sys_id, a.sysOrg, a.UFullName, a.accessRights from ResolveActionTask a where a.UFullName IN  (" + sqlSysIds + ")";
            String sql = "select a.sys_id, a.sysOrg, a.UFullName, a.accessRights from ResolveActionTask a where a.UFullName IN  (:UFullName)";
            
            Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
            
            queryInParams.put("UFullName", new ArrayList<Object>(atFullName));
            
            try
            {
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectInOnly(sql, queryInParams);
                if (data != null && data.size() > 0)
                {
                    String selectedColumns = "sys_id,sysOrg,UFullName,accessRights";
                    if (StringUtils.isNotBlank(selectedColumns))
                    {
                        String[] columns = selectedColumns.split(",");

                        //this will be the array of objects
                        for (Object o : data)
                        {
                            Object[] record = (Object[]) o;
                            ResolveActionTask instance = new ResolveActionTask();

                            //set the attributes in the object
                            for (int x = 0; x < columns.length; x++)
                            {
                                String column = columns[x].trim();
                                Object value = record[x];

                                PropertyUtils.setProperty(instance, column, value);
                            }

                            listOfATs.add(convertResolveActionTaskVOToActionTask(instance.doGetVO(), null));
                        }//end of for loop
                    }
                }//end of if
            }
            catch (Exception e)
            {
                Log.log.error("Error while querying Actiontask with sql: " + sql, e);
            }
        }
        return listOfATs;
    }

    ////////////////////////////// Document ///////////////////////////////////////////////

    public static Document createDocument(String sysId)
    {
        WikiDocumentVO doc = ServiceWiki.getWikiDoc(sysId, null, "system");
        Document target = convertWikiDocumentVOToDocument(doc, null);

        return target;
    }// createDocument
    
    public static Document createDocumentByName(String docName)
    {
        WikiDocumentVO doc = ServiceWiki.getWikiDoc(null, docName, "system");
        Document target = convertWikiDocumentVOToDocument(doc, null);

        return target;
    }// createDocument
    
    
    public static Document convertWikiDocumentToDocument(WikiDocument doc, Document result)
    {
        return convertWikiDocumentVOToDocument(doc.doGetVO(), result);
    }
    
    public static Document convertWikiDocumentVOToDocument(WikiDocumentVO vo, Document result)
    {
        if(vo != null)
        {
            if(result == null)
            {
                result = new Document();
            }
            
            result.setSys_id(vo.getSys_id());
            result.setId(vo.getSys_id());
//            result.setSys_org(vo.getSysOrg());
            result.setDisplayName(vo.getUFullname());
            result.setLock(vo.ugetUIsLocked());
            result.setMarkDeleted(vo.ugetUIsDeleted());
            result.setRunbook(vo.ugetUHasActiveModel());
            result.setDecisionTree(vo.ugetUIsRoot());
            
            AccessRightsVO ar = vo.getAccessRights();
            if(ar != null)
            {
                result.setRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                result.setEditRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                result.setPostRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
            }
        }
        
        return result;
    }  
    
    public static List<Document> getDocumentModelsForSysIds(List<String> docSysIds)
    {
        List<Document> listOfDocuments = new ArrayList<Document>();
        
        if (docSysIds != null && docSysIds.size() > 0)
        {
            //String sqlSysIds = SQLUtils.prepareQueryString(docSysIds);
            //String sql = "select a.sys_id, a.sysOrg, a.UFullname, a.UIsLocked, a.UIsDeleted, a.UHasActiveModel, a.UIsRoot, a.accessRights from WikiDocument a where a.sys_id IN  (" + sqlSysIds + ")";
            String sql = "select a.sys_id, a.sysOrg, a.UFullname, a.UIsLocked, a.UIsDeleted, a.UHasActiveModel, a.UIsRoot, a.accessRights from WikiDocument a where a.sys_id IN  (:sys_id)";
            
            Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
            
            queryInParams.put("sys_id", new ArrayList<Object>(docSysIds));
            
            try
            {
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectInOnly(sql, queryInParams);
                if (data != null && data.size() > 0)
                {
                    String selectedColumns = "sys_id,sysOrg,UFullname,UIsLocked,UIsDeleted,UHasActiveModel,UIsRoot,accessRights";
                    if (StringUtils.isNotBlank(selectedColumns))
                    {
                        String[] columns = selectedColumns.split(",");

                        //this will be the array of objects
                        for (Object o : data)
                        {
                            Object[] record = (Object[]) o;
                            WikiDocument instance = new WikiDocument();

                            //set the attributes in the object
                            for (int x = 0; x < columns.length; x++)
                            {
                                String column = columns[x].trim();
                                Object value = record[x];

                                PropertyUtils.setProperty(instance, column, value);
                            }

                            listOfDocuments.add(convertWikiDocumentToDocument(instance, null));
                        }//end of for loop
                    }
                }//end of if
            }
            catch (Exception e)
            {
                Log.log.error("Error while querying the Document with sql: " + sql, e);
            }
        }
        return listOfDocuments;
    }
    
    ////////////////////////////// Inbox ///////////////////////////////////////////////

    ////////////////////////////// Namespace ///////////////////////////////////////////////

    ////////////////////////////// Outbox ///////////////////////////////////////////////

    ////////////////////////////// Rss ///////////////////////////////////////////////
    public static Rss createRss(String sysId)
    {
        // set the roles of the Process by querying the DB
        Map<String, Object> record = ServiceHibernate.findById(SocialRssDTO.TABLE_NAME, sysId);
        SocialRssDTO dto = new SocialRssDTO(record);
        Rss target =  convertSocialRssDTOToRss(dto, null);

        return target;
    }// createRss
    
    public static Rss convertSocialRssDTOToRss(SocialRssDTO rss, Rss result)
    {
        if(rss != null)
        {
            if(result == null)
            {
                result = new Rss();
            }
            
            result.setSys_id(rss.getSys_id());
            result.setId(rss.getSys_id());
//            result.setSys_org(rss.getSys_org());
            result.setDisplayName(rss.getU_display_name());
            result.setRoles(SocialUtil.convertRolesToSocialRolesString(rss.getU_read_roles() + "," + rss.getU_edit_roles()));
            result.setEditRoles(SocialUtil.convertRolesToSocialRolesString(rss.getU_edit_roles()));
            result.setPostRoles(SocialUtil.convertRolesToSocialRolesString(rss.getU_post_roles() + "," + rss.getU_edit_roles()));
            result.setLock(rss.getU_is_locked() != null ? rss.getU_is_locked() : false);
        }
        
        return result;
    }    
    
    public static Rss createRssByName(String name)
    {
        Map<String, Object> example = new HashMap<String, Object>();
        example.put(ServiceSocialConstants.U_DISPLAY_NAME, name.trim() );
        
        Map<String, Object> record = ServiceHibernate.findFirstCustomTableRecord(SocialRssDTO.TABLE_NAME, example);  
        SocialRssDTO dto = new SocialRssDTO(record);
        Rss target =  convertSocialRssDTOToRss(dto, null);

        return target;
    }

    ////////////////////////////// Forum ///////////////////////////////////////////////

    public static Forum createForum(String sysId)
    {
        // set the roles of the Forum by querying the DB
        Map<String, Object> record = ServiceHibernate.findById(SocialForumDTO.TABLE_NAME, sysId);
        SocialForumDTO dto = new SocialForumDTO(record);
        Forum target = convertSocialForumDTOToForum(dto, null);

        return target;
    }// createForum
    
    public static Forum convertSocialForumDTOToForum(SocialForumDTO forum, Forum result)
    {
        if(forum != null)
        {
            if(result == null)
            {
                result = new Forum();
            }
            
            result.setSys_id(forum.getSys_id());
            result.setId(forum.getSys_id());
//            result.setSys_org(forum.getSys_org());
            result.setDisplayName(forum.getU_display_name());
            result.setRoles(SocialUtil.convertRolesToSocialRolesString(forum.getU_read_roles() + "," + forum.getU_edit_roles()));
            result.setEditRoles(SocialUtil.convertRolesToSocialRolesString(forum.getU_edit_roles()));
            result.setPostRoles(SocialUtil.convertRolesToSocialRolesString(forum.getU_post_roles() + "," + forum.getU_edit_roles()));
            result.setLock(forum.getU_is_locked() != null ? forum.getU_is_locked() : false);
        }
        
        return result;
    }   
    
    public static Forum createForumByName(String name)
    {
        Map<String, Object> example = new HashMap<String, Object>();
        example.put(ServiceSocialConstants.U_DISPLAY_NAME, name.trim() );
        
        Map<String, Object> record = ServiceHibernate.findFirstCustomTableRecord(SocialForumDTO.TABLE_NAME, example);  
        SocialForumDTO dto = new SocialForumDTO(record);
        Forum target = convertSocialForumDTOToForum(dto, null);

        return target;
    }

    ////////////////////////////// Process ///////////////////////////////////////////////
    public static Process createProcess(String sysId)
    {
     // set the roles of the Forum by querying the DB
        Map<String, Object> record = ServiceHibernate.findById(SocialProcessDTO.TABLE_NAME, sysId);
        SocialProcessDTO process = new SocialProcessDTO(record);
        Process target = convertSocialProcessDTOToProcess(process, null);

        return target;
    }// createProcess
    
    public static Process convertSocialProcessDTOToProcess(SocialProcessDTO process,  Process socialProcess)
    {
        if(process != null)
        {
            if(socialProcess == null)
            {
                socialProcess = new Process();
            }
            
            socialProcess.setSys_id(process.getSys_id());
            socialProcess.setId(process.getSys_id());
            socialProcess.setDisplayName(process.getU_display_name());
//            socialProcess.setSys_org(process.getSys_org());
            socialProcess.setRoles(SocialUtil.convertRolesToSocialRolesString(process.getU_read_roles() + "," + process.getU_edit_roles()));
            socialProcess.setEditRoles(SocialUtil.convertRolesToSocialRolesString(process.getU_edit_roles()));
            socialProcess.setPostRoles(SocialUtil.convertRolesToSocialRolesString(process.getU_post_roles() + "," + process.getU_edit_roles()));
            socialProcess.setLock(process.getU_is_locked() != null ? process.getU_is_locked() : false);            
        }
        
        return socialProcess;
    }
    
    public static Process createProcessByName(String name)
    {
        Map<String, Object> example = new HashMap<String, Object>();
        example.put(ServiceSocialConstants.U_DISPLAY_NAME, name.trim() );
        
        Map<String, Object> record = ServiceHibernate.findFirstCustomTableRecord(SocialProcessDTO.TABLE_NAME, example); 
        SocialProcessDTO process = new SocialProcessDTO(record);
        Process target = convertSocialProcessDTOToProcess(process, null);

        return target;
    }
    
    ////////////////////////////// Starred ///////////////////////////////////////////////

    ////////////////////////////// Team ///////////////////////////////////////////////
    public static Team createTeam(String sysId)
    {
        // set the roles of the Team by querying the DB
        Map<String, Object> record = ServiceHibernate.findById(SocialTeamDTO.TABLE_NAME, sysId);
        SocialTeamDTO team = new SocialTeamDTO(record);
        Team target = convertSocialTeamDTOToTeam(team, null);

        return target;
    }// createTeam
    
    public static Team convertSocialTeamDTOToTeam(SocialTeamDTO team, Team result)
    {
        if(team != null)
        {
            if(result == null)
            {
                result = new Team();
            }
            result.setSys_id(team.getSys_id());
            result.setId(team.getSys_id());
            result.setDisplayName(team.getU_display_name());
//            result.setSys_org(team.getSys_org());
            result.setRoles(SocialUtil.convertRolesToSocialRolesString(team.getU_read_roles() + "," + team.getU_edit_roles()));
            result.setEditRoles(SocialUtil.convertRolesToSocialRolesString(team.getU_edit_roles()));
            result.setPostRoles(SocialUtil.convertRolesToSocialRolesString(team.getU_post_roles() + "," + team.getU_edit_roles()));
            result.setLock(team.getU_is_locked() != null ? team.getU_is_locked() : false);
        }
        
        return result;
    }
    
    public static Team createTeamByName(String name)
    {
        Team target = null;
        
        /*
        Map<String, Object> example = new HashMap<String, Object>();
        example.put(ServiceSocialConstants.U_DISPLAY_NAME, name.trim() );
        
        Map<String, Object> record = ServiceHibernate.findFirstCustomTableRecord(SocialTeamDTO.TABLE_NAME, example); 
        if(record != null)
        {
            SocialTeamDTO team = new SocialTeamDTO(record);
            target = convertSocialTeamDTOToTeam(team, null);
        }*/
        
        SocialTeamDTO team = (SocialTeamDTO) SocialAdminUtil.getSocialDTO(AdvanceTree.TEAMS, null, name, "admin");

        if (team != null)
        {
            target = convertSocialTeamDTOToTeam(team, null);
        }
        
        return target;
    }

    
    
    
    
//    public static Runbook createRunbook(String sysId)
//    {
//        WikiDocumentVO doc = ServiceWiki.getWikiDoc(sysId, null, "system");
//        Runbook target = convertWikiDocumentVOToRunbook(doc, null);
//
//        return target;
//    }// createRunbook
//    
//    public static Runbook convertWikiDocumentVOToRunbook(WikiDocumentVO vo,  Runbook result)
//    {
//        if(vo != null)
//        {
//            if(result == null)
//            {
//                result = new Runbook();
//            }
//            
//            result.setSys_id(vo.getSys_id());
//            result.setSys_org(vo.getSysOrg());
//            result.setDisplayName(vo.getUFullname());
//            result.setRoles(SocialCompConversionUtil.convertRolesToSocialRolesString(vo.getUReadRoles()));
//            result.setEditRoles(SocialCompConversionUtil.convertRolesToSocialRolesString(vo.getUWriteRoles()));
//            result.setPostRoles(SocialCompConversionUtil.convertRolesToSocialRolesString(vo.getUWriteRoles()));
//            result.setLock(vo.getUIsLocked() != null ? vo.getUIsLocked() : false);
//        }
//        
//        return result;
//    }  
//    
//    public static DecisionTree createDecisionTree(String sysId)
//    {
//        WikiDocumentVO doc = ServiceWiki.getWikiDoc(sysId, null, "system");
//        DecisionTree target = convertWikiDocumentVOToDecisionTree(doc, null);
//
//        return target;
//    }// createRunbook
//    
//    public static DecisionTree convertWikiDocumentVOToDecisionTree(WikiDocumentVO vo, DecisionTree result)
//    {
//        if(vo != null)
//        {
//            if(result == null)
//            {
//                result = new DecisionTree();
//            }
//            
//            result.setSys_id(vo.getSys_id());
//            result.setSys_org(vo.getSysOrg());
//            result.setDisplayName(vo.getUFullname());
//            result.setRoles(SocialCompConversionUtil.convertRolesToSocialRolesString(vo.getUReadRoles()));
//            result.setEditRoles(SocialCompConversionUtil.convertRolesToSocialRolesString(vo.getUWriteRoles()));
//            result.setPostRoles(SocialCompConversionUtil.convertRolesToSocialRolesString(vo.getUWriteRoles()));
//            result.setLock(vo.getUIsLocked() != null ? vo.getUIsLocked() : false);
//
//        }
//        
//        return result;
//    }   
    
    
    
   
    
    ////////////////////////////// Additional APIs ///////////////////////////////////////////////
   

//    public static String convertRolesToSocialRolesString(String csvRoles)
//    {
//        String result = "";
//        
//        if(StringUtils.isNotEmpty(csvRoles))
//        {
//            Set<String> roles = new HashSet<String>(StringUtils.convertStringToList(csvRoles, ","));
//            result = StringUtils.convertCollectionToString(roles.iterator(), " ");
////            result = result.replaceAll("&", "_");
//        }
//        
//        return result;
//    }

}
