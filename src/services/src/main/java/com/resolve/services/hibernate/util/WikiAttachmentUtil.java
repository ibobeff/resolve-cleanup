/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.dao.WikiAttachmentDAO;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceWiki;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.services.hibernate.wiki.SearchWikiAttachments;
import com.resolve.services.util.DateManipulator;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.dto.AttachmentDTO;

public class WikiAttachmentUtil
{
    private static final String GLOBAL_ATTACHMENT_WIKI_NAME = "System.Attachment";

    public static List<AttachmentDTO> getAllAttachmentsFor(String docSysId, String docFullName, String username) throws Exception
    {
        List<AttachmentDTO> listOfAttachments = new ArrayList<AttachmentDTO>();
        if (StringUtils.isNotEmpty(docFullName) || StringUtils.isNotEmpty(docSysId))
        {
            SearchWikiAttachments searchAttachments = new SearchWikiAttachments(null, docSysId, docFullName, username);
            ResponseDTO<AttachmentDTO> response = searchAttachments.getAttachments();
            if(response.isSuccess())
            {
                listOfAttachments.addAll(response.getRecords());
            }
        }

        return listOfAttachments;
    }

    //utility api
    public static AttachmentDTO convertWikiAttachmentToDTO(WikiAttachment attachModel, WikidocAttachmentRel wikiAttachmentRel, String docFullname)
    {
        AttachmentDTO dto = null;

        if (attachModel != null && StringUtils.isNotBlank(docFullname))
        {
            //this is imp as all the date conversions take place here
            WikiAttachmentVO attach = attachModel.doGetVO();
            
            String downloadUrl = "/resolve/service/wiki/download/" + docFullname.replace('.', '/') + "?attach=";
            boolean global =false;
            String fileName = attach.getUFilename();
            if(wikiAttachmentRel != null)
            {
                String docSysId = wikiAttachmentRel.getWikidoc().getSys_id();
                String parentDocSysId = wikiAttachmentRel.getParentWikidoc().getSys_id();
                if(!docSysId.equalsIgnoreCase(parentDocSysId))
                {
                    //this is a global attachment
                    //fileName = fileName + " <b>(Global Attachment)</b>";
                    global=true;
                }
            }

            dto = new AttachmentDTO();
            dto.setFileName(fileName);
            dto.setGlobal(global);
            dto.setFileSysId(attach.getSys_id());
            dto.setSize(attach.getUSize());
            dto.setUploadedBy(attach.getSysUpdatedBy());
            dto.setUploadedOn(DateManipulator.getDateInUTCFormat(attach.getSysCreatedOn()));
            dto.setTableName("wikiattachmentcontent");
            dto.setDownloadUrl(downloadUrl + attach.getSys_id());

            //update the sys cols 
            dto.setSys_id(attach.getSys_id());
            dto.setSys_created_by(attach.getSysCreatedBy());
            dto.setSys_created_on(attach.getSysCreatedOn());
            dto.setSys_org(attach.getSysOrg());
            dto.setSys_updated_by(attach.getSysUpdatedBy());
            dto.setSys_updated_on(attach.getSysUpdatedOn());
        }

        return dto;
    }

    public static WikiAttachmentVO getAttachmentWithContent(String sysId, String username) throws Exception
    {
        WikiAttachment model = null;
        WikiAttachmentVO result = null;

        if (StringUtils.isNotEmpty(sysId))
        {
            try
            {
                //get user roles 
                StringBuilder roles = new StringBuilder();
                Set<String> userRoles = UserUtils.getUserRoles(username);

              HibernateProxy.setCurrentUser(username);
                model = (WikiAttachment) HibernateProxy.execute(() -> {
                    WikiAttachment m = HibernateUtil.getDAOFactory().getWikiAttachmentDAO().findById(sysId);
                    if (m != null)
                    {
                        Collection<WikidocAttachmentRel> rels = m.getWikidocAttachmentRels();
                        if (rels != null)
                        {
                            for (WikidocAttachmentRel rel : rels)
                            {
                                WikiDocument doc = rel.getWikidoc();
                                if (doc != null)
                                {
                                    AccessRights rights = doc.getAccessRights();
                                    if (rights != null)
                                    {
                                        if (roles.length() > 0)
                                        {
                                            roles.append("," + rights.getUReadAccess() + "," + rights.getUAdminAccess());
                                        }
                                        else
                                        {
                                            roles.append(rights.getUReadAccess() + "," + rights.getUAdminAccess());
                                        }
                                    }
                                }
                            }//end of for loop

                            //validate if the user has rights to any document that this attachment belongs to.
                            boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(username, userRoles, roles.toString());
                            //userCanEdit = false;
                            if (!userCanEdit)
                            {
                                throw new Exception("User does not have Read/Admin rights for downloading this attachment");
                            }
                        }
                        else
                        {
                            throw new Exception("Bad Attachment: This file does not belong to any document");
                        }

                        //populate the content of this attachment
                        WikiAttachmentUtil.populateAttachmentContent(m);
                    }

                    return m;
                    
                });
                
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                throw new Exception(t);
            }

            if (model != null)
            {
                result = model.doGetVO();
            }
        }

        return result;
    }

    public static void deleteAttachments(String docSysId, String docFullName, Set<String> attachSysIds, String username) throws Exception
    {
        WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, docFullName, RightTypeEnum.edit, username);

        //prepare to delete
        String idsToDeleteContent = getListToDeleteContent(doc, attachSysIds);
        //String listOfids = SQLUtils.prepareQueryString(new ArrayList<String>(attachSysIds));
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            //only the parent doc can delete the attachments ...for others, only references must be deleted
	            if (idsToDeleteContent != null && idsToDeleteContent.trim().length() > 0)
	            {
	                HibernateUtil.delete("WikiAttachmentContent", new ArrayList<String>(attachSysIds));
	                HibernateUtil.delete("WikiAttachment", new ArrayList<String>(attachSysIds));
	                HibernateUtil.delete("WikidocAttachmentRel", new ArrayList<String>(attachSysIds));
	            }
	
	            //HibernateUtil.deleteQuery("WikidocAttachmentRel", "wikiAttachment in (" + listOfids.trim() + ") and wikidoc = '" + doc.getSys_id() + "' ");
	            HibernateUtil.deleteQuery("WikidocAttachmentRel", "wikiAttachment", attachSysIds, "wikidoc", doc.getSys_id());

            });

            //delete the attachment indexes
//            WikiUtils.indexWikiDocument(doc, true);
            if(StringUtils.isNotBlank(idsToDeleteContent))
            {
                Set<String> attachmentsDeleted = SQLUtils.convertQueryStringToSet(idsToDeleteContent);
                ServiceWiki.purgeWikiAttachmentIndexes(attachmentsDeleted, doc.getSys_id(), username);
            }

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    public static WikiAttachmentVO findWikiAttachmentVO(String docName, String attachmentSysId, String fileName, boolean content) throws Exception
    {
        WikiAttachmentVO result = null;
        
        WikiAttachment attach = findWikiAttachmentFor(docName, attachmentSysId, fileName, content);
        if(attach != null)
        {
            result = attach.doGetVO();
        }
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static WikiAttachment findWikiAttachmentFor(String docName, String attachmentSysId, String fileName, boolean content) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	return (WikiAttachment) HibernateProxy.execute(() -> {
        		WikiAttachment file = null;
            
	            if(StringUtils.isNotBlank(attachmentSysId))
	            {
	                file = HibernateUtil.getDAOFactory().getWikiAttachmentDAO().findById(attachmentSysId);
	            }
	            else if(StringUtils.isNotBlank(fileName))
	            {
	                //String sql = "select c from WikiDocument a, WikidocAttachmentRel b, WikiAttachment c where LOWER(a.UFullname) = '" + docName.trim().toLowerCase() + "' and " + " a.sys_id = b.wikidoc and b.wikiAttachment = c.sys_id and LOWER(c.UFilename) = '" + fileName.trim().toLowerCase() + "'";
	                //Query q = HibernateUtil.createQuery(sql);
	                //List<WikiAttachment> list = q.list();
	                
	                String hql = "select c from WikiDocument a, WikidocAttachmentRel b, WikiAttachment c where LOWER(a.UFullname) = :docName and "
	                                + " a.sys_id = b.wikidoc and b.wikiAttachment = c.sys_id and LOWER(c.UFilename) = :UFilename";
	                Map<String, Object> queryParams = new HashMap<String, Object>();
	                queryParams.put("UFilename", fileName.toLowerCase());
	                queryParams.put("docName", docName.trim().toLowerCase());
	                
	                List<WikiAttachment> list = (List<WikiAttachment>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
	                if (list != null && list.size() > 0)
	                {
	                    file = list.get(0);
	                }
	            }
	            
	            if(file != null)
	            {
	                //populate the content if its true
	                if (content)
	                {
	                    populateAttachmentContent(file);
	                }
	                
	                file.getWikidocAttachmentRels().size();
	            }
	            
	            return file;
        	});

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }

    }//findWikiAttachmentFor

    @SuppressWarnings("unchecked")
    public static void populateAttachmentContent(WikiAttachment att)
    {
        try
        {
            HibernateProxy.execute(() -> {
            	String sql = " from WikiAttachmentContent where wikiAttachment = '" + att.getSys_id() + "'";
                WikiAttachmentContent wac = null;
            	 List<WikiAttachmentContent> list = HibernateUtil.createQuery(sql).list();
                 if (list != null && list.size() > 0)
                 {
                     wac = list.get(0);
                 }

                 att.usetWikiAttachmentContent(wac);
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }

    public static void renameAttachment(String docSysId, String docFullName, String attachmentSysId, String newName,  boolean overwrite, String username) throws Exception
    {
        if(StringUtils.isBlank(docSysId) && StringUtils.isBlank(docFullName))
        {
            throw new Exception("Doc sysId or Name is mandatory for renaming the attachment");
        }
        
        if(StringUtils.isBlank(attachmentSysId))
        {
            throw new Exception("Attachment sysId is mandatory to know which attachment is to be renamed");
        }
        
        if(StringUtils.isBlank(newName))
        {
            throw new Exception("Attachment cannot be updated with an empty name. Please provide the updated name");
        }
        
        
        //validate the name first
        validateAttachmentName(newName);
        
        WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, docFullName, RightTypeEnum.edit, username);

        //get the old attachment and rename it 
        WikiAttachment oldAttachment = findWikiAttachmentFor(doc.getUFullname(), attachmentSysId, null, false);
        if (oldAttachment == null)
        {
            throw new Exception("Attachment with id " + attachmentSysId + " name does not exist.");
        }
        
        //if the oldname and newname are same, throw an exception
        if(oldAttachment.getUFilename().trim().equalsIgnoreCase(newName.trim()))
        {
            throw new Exception("You are trying to update the attachment with same name. Please give a new name.");
        }

        //validate if the new doc name exist or not
        WikiAttachment newAttachment = findWikiAttachmentFor(doc.getUFullname(), null, newName, false);
        if (newAttachment != null)
        {
            if(overwrite)
            {
                //delete this attachment as the overwrite flag is set to true
                Set<String> attachSysIds = new HashSet<String>();
                attachSysIds.add(newAttachment.getSys_id());
                
                deleteAttachments(docSysId, docFullName, attachSysIds, username);
            }
            else
            {
                throw new Exception("Attachment with " + newName + " name already exist.");
            }
        }

        
        //update the name and save it
        oldAttachment.setUFilename(newName);

        try
        {
            //persist it
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	HibernateUtil.getDAOFactory().getWikiAttachmentDAO().persist(oldAttachment);
            });

            //index the attachment
            Set<String> attachSysIds = new HashSet<String>();
            attachSysIds.add(oldAttachment.getSys_id());
            ServiceWiki.indexWikiAttachments(doc.getSys_id(), null, attachSysIds, username);

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    public static Collection<WikiAttachment> prepareAttachment(String docFullName, String username)
    {
        List<WikiAttachment> attachments = new ArrayList<WikiAttachment>();

        try
        {

          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                WikiDocument doc = WikiUtils.getWikiDocumentModel(null, docFullName, username, true);
                if (doc.getWikidocAttachmentRels() != null && doc.getWikidocAttachmentRels().size() > 0)
                {
                    
                    WikiAttachment attachment = null;
                    for (WikidocAttachmentRel rel : doc.getWikidocAttachmentRels())
                    {
                        //get the attachment handle
                        attachment = rel.getWikiAttachment();

                        //populate the content
                        populateAttachmentContent(attachment);

                        //add it to the list
                        attachments.add(attachment);
                    }//end of for loop
                }//end of if

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        //prepare for the new attachments.
        if (attachments != null && attachments.size() > 0)
        {
            for (WikiAttachment attachment : attachments)
            {
                attachment.setSys_id(null);
                attachment.setSysCreatedBy(null);
                attachment.setSysCreatedOn(null);
                attachment.setSysUpdatedBy(null);
                attachment.setSysUpdatedOn(null);
                attachment.setSysModCount(null);
                attachment.setWikidocAttachmentRels(null);

                WikiAttachmentContent wac = attachment.ugetWikiAttachmentContent();
                if (wac != null)
                {
                    wac.setSys_id(null);
                    wac.setSysCreatedBy(null);
                    wac.setSysCreatedOn(null);
                    wac.setSysUpdatedBy(null);
                    wac.setSysUpdatedOn(null);
                    wac.setSysModCount(null);
                    wac.setWikiAttachment(null);
                }

            }
        }//end of if

        return attachments;
    }//prepareAttachmentRels
    
    public static void addGlobalAttachmentsToWiki(String docSysId, String docFullname, Set<String> globalAttachSysIds, String username) throws Exception
    {
        WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, docFullname, RightTypeEnum.edit, username);
        WikiDocument globalDocument = WikiUtils.validateUserForDocument(null, GLOBAL_ATTACHMENT_WIKI_NAME, RightTypeEnum.view, "system");
        
        if(globalAttachSysIds != null && globalAttachSysIds.size() > 0)
        {
            for(String globalAttachmentSysId : globalAttachSysIds)
            {
                addGlobalAttachmentToWiki(doc, globalDocument, globalAttachmentSysId, username);
            }
        }
    }
    /**
     * filter out the ids for which the content can be deleted. So if the document owns a attachment, it can delete the content, rest of the docs can
     * only refer it. For that there is a rec in the WikidocAttachmentRel table.
     * 
     * @param UParentWikidocId
     * @param listOfids
     * @return
     */
    private static String getListToDeleteContent(WikiDocument doc, Set<String> listOfids)
    {
        String ids = "";
        Collection<WikidocAttachmentRel> list = doc.getWikidocAttachmentRels();

        //prepare the qry string 
        if (list != null && list.size() > 0)
        {
            for (WikidocAttachmentRel rel : list)
            {
                if (doc.getSys_id().equals(rel.getParentWikidoc().getSys_id()))
                {
                    //if there is any other ParentSys_Id other then the one specified, in that case, the content must not be deleted as
                    //it is referenced by other documents.
                    boolean isAttachmentContentReferencedByAnyOtherWikidoc = isAttachmentContentReferencedByAnyOtherWikidoc(rel.getWikiAttachment().getSys_id(), doc.getSys_id());
                    if (!isAttachmentContentReferencedByAnyOtherWikidoc)
                    {
                        if (listOfids.contains(rel.getWikiAttachment().getSys_id()))
                        {
                            if (ids.trim().length() > 0)
                            {
                                ids = ids + ",'" + rel.getWikiAttachment().getSys_id() + "'";
                            }
                            else
                            {
                                ids = "'" + rel.getWikiAttachment().getSys_id() + "'";
                            }
                        }
                    }
                }
            }
        }

        return ids;
    }//getListToDeleteContent    

    @SuppressWarnings("unchecked")
	private static boolean isAttachmentContentReferencedByAnyOtherWikidoc(String wikiAttachmentId, String UParentWikidocId)
    {
        boolean isAttachmentContentReferencedByAnyOtherWikidoc = false;
        Collection<WikidocAttachmentRel> wikidocAttachmentRels = null;

        try
        {
        	wikidocAttachmentRels = (Collection<WikidocAttachmentRel>) HibernateProxy.execute(() -> {
            	WikiAttachmentDAO dao = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();

            	WikiAttachment attachment = dao.findById(wikiAttachmentId);
                Collection<WikidocAttachmentRel> result = attachment.getWikidocAttachmentRels();
                if (result != null)
                {
                	result.size();//this is to load it in memory
                }
                
                return result;
            });
            
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        if (wikidocAttachmentRels != null && wikidocAttachmentRels.size() > 0)
        {
            //if there is any other ParentSys_Id other then the one specified, in that case, the content must not be deleted as
            //it is referenced by other documents.
            for (WikidocAttachmentRel rel : wikidocAttachmentRels)
            {
                String parentSys_Id = rel.getParentWikidoc().getSys_id();
                if (!parentSys_Id.equals(UParentWikidocId))
                {
                    isAttachmentContentReferencedByAnyOtherWikidoc = true;
                    break;
                }
            }
        }

        return isAttachmentContentReferencedByAnyOtherWikidoc;

    }//isAttachmentContentReferencedByAnyOtherWikidoc
    
    private static void addGlobalAttachmentToWiki(WikiDocument doc, WikiDocument globalDocument, String globalAttachmentSysId, String username)  throws Exception
    {
        //verify if the attachSysId is a attachment of global document
        WikiAttachment globalAttachment = findWikiAttachmentForDocument(globalDocument.getSys_id(), globalAttachmentSysId);
        if(globalAttachment != null)
        {
            //verify if the attachment with the name already exist for this document
            WikiAttachment docAttachmentWithSameName = findWikiAttachmentFor(doc.getUFullname(), null, globalAttachment.getUFilename(), false);
            if(docAttachmentWithSameName != null)
            {
                throw new Exception("Attachment with " + globalAttachment.getUFilename() + " name already exist for the document " + doc.getUFullname());
            }
            
            //create the relationship 
            WikidocAttachmentRel rel = new WikidocAttachmentRel();
            rel.setWikidoc(doc);
            rel.setWikiAttachment(globalAttachment);
            rel.setParentWikidoc(globalDocument);
            rel.setSysCreatedBy(username);
            rel.setSysCreatedOn(GMTDate.getDate());
            
            //persist 
            SaveUtil.saveWikidocAttachmentRel(rel, username);
            
            //TODO: index - not sure if need to do that or not as its a global attach and that attachment is already indexed by now 
        }
        else
        {
            Log.log.error("Attachment with sysId " + globalAttachmentSysId + " does not belong to global attachment document");
        }
    }
    
    private static WikiAttachment findWikiAttachmentForDocument(String docSysId, String attachmentSysId)
    {
        WikiAttachment result = null;
        
        if(StringUtils.isNotBlank(docSysId) && StringUtils.isNotBlank(attachmentSysId))
        {
            StringBuilder hql = new StringBuilder("select wa from WikiAttachment as wa, WikidocAttachmentRel as rel ");
            //hql.append(" where wa.sys_id = rel.wikiAttachment and rel.wikidoc = '" + docSysId + "' ");
            hql.append(" where wa.sys_id = rel.wikiAttachment.sys_id and rel.wikidoc.sys_id = :wikidoc ");
            //hql.append(" and wa.sys_id = '" + attachmentSysId + "' ");
            hql.append(" and wa.sys_id = :sys_id ");
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("wikidoc", docSysId);
            queryParams.put("sys_id", attachmentSysId);
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (WikiAttachment) list.get(0);
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
            
        }

        return result;
    }
    

    private static void validateAttachmentName(String name) throws Exception
    {
        if(StringUtils.isNotBlank(name))
        {
            boolean valid = name.matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH_SPACE);
            if(!valid)
            {
                throw new RuntimeException("Name can have alphanumeric, space or _. Please update with correct values.");
            }
        }
    }

}
