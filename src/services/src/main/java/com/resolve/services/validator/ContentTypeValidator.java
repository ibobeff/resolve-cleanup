package com.resolve.services.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.resolve.services.constants.annotation.ContentTypeContraint;
import com.resolve.services.vo.ContentType;
import com.resolve.util.StringUtils;

public class ContentTypeValidator implements ConstraintValidator<ContentTypeContraint, String> {

	@Override
	public boolean isValid(String contentType, ConstraintValidatorContext arg1) {
		try {
			if (StringUtils.isNotBlank(contentType)) {
				ContentType.valueOf(contentType.toUpperCase());
			}
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}

}
