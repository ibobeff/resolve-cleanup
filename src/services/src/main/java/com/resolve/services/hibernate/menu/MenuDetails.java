/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.menu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringEscapeUtils;

import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;

//THIS COMPLETE CLASS NEEDS TO BE REFACTORED. Best will be to prepare this on the UI as all its doing is creating a json 
//for the automation menu

/*
 * This class support to build the popup menu.
 */
public class MenuDetails 
{	
	private boolean lastIsItem = false;
	private static MenuDetails onlyInstance;
	private Map<String, String> popupMenuMap = new ConcurrentHashMap();
	private Map<String, String> dtMenuMap = new ConcurrentHashMap();
	private static final String POPUP_MENU = "popupMenu";
	private static final String DT_MENU = "dtMenu";
	
//	public static final String AUTOMATION_SELECT_TASK = "automation.select.task";
	
	/*
	 * Only one instance
	 */
	private MenuDetails()
	{
	}
	
	/**
	 * 
	 * @return
	 */
	public static MenuDetails getInstance()
	{
		if(onlyInstance == null)
		{
			synchronized(MenuDetails.class)
			{
				if(onlyInstance == null)
				{
					onlyInstance = new MenuDetails();
				}
			}
		}
		
		return onlyInstance;
	} //getInstance
	
	/**
	 * This method return the popup menu.
	 * 
	 * @return
	 */
	public String getPopupMenu(String userName)
	{
		String popupMenu = "";
		
		if(popupMenuMap.get(POPUP_MENU) == null)
		{
			refreshCache(userName);
		}
		
		popupMenu = popupMenuMap.get(POPUP_MENU);
		
		return popupMenu;
	} //getPopupMenu
	
	/**
     * This method returns the dt popup menu.
     * 
     * @return
     */
    public String getDTMenu(String userName)
    {
        String dtMenu = "";
        
        if(dtMenuMap.get(DT_MENU) == null)
        {
            refreshCache(userName);
        }
        
        dtMenu = dtMenuMap.get(DT_MENU);
        
        return dtMenu;
    } //getDTMenu
    
	/**
	 * This method refresh cache.
	 */
	public synchronized void refreshCache(String userName)
	{
	    //just for 3.3.1 --- the menulist is for admin only
	    String usernameLocal = "admin";
	    
		String popupMenu = buildPopupMenu(usernameLocal);
		
		if(popupMenu != null && !popupMenu.equals(""))
		{
			popupMenuMap.remove(POPUP_MENU);
			popupMenuMap.put(POPUP_MENU, popupMenu);
		}
		
		String dtMenu = buildDTMenu(usernameLocal);
		
		if (StringUtils.isNotEmpty(dtMenu))
		{
		    dtMenuMap.remove(DT_MENU);
		    dtMenuMap.put(DT_MENU, dtMenu);
		}
	} //refreshCache
	
	public void resetDTMenuMap()
	{
	    dtMenuMap.clear();
	}
	
	public void resetPopupMenuMap()
    {
	    popupMenuMap.clear();
    }
	
	/**
	 * This method build the popup menu.
	 * 
	 * @return
	 */
	public String buildPopupMenu(String userName)
	{
		String popupMenu = "";
		Map root = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        Map defaultMap = new TreeMap();
        
		try 
		{
    HibernateProxy.setCurrentUser(userName);
			popupMenu = (String) HibernateProxy.execute(() -> {
				
				boolean selectTaskFlag = PropertiesUtil.getPropertyBoolean("automation.select.task");

				if(selectTaskFlag)
				{
					List<ResolveActionTask> actionTaskList = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findAll();
				    
	    			for(ResolveActionTask actionTask: actionTaskList)
	    			{
	    				String menuPath = actionTask.getUMenuPath();
	    				
	    				if((menuPath == null || menuPath.equals("")) || menuPath.equals("/"))
	    				{
	    					defaultMap.put(actionTask.getSys_id(), actionTask);
	    				}
	    				else
	    				{
	    					Map currentMap = root;					
	    					String[] paths = actionTask.getUMenuPath().split("/");
	    					
	    					for(int idx = 0; idx<paths.length; idx++)
	    					{
	    						String path = paths[idx];
	    						
	    						if(path != null && !path.equals(""))
	    						{
	    							Map pathMap = (Map) currentMap.get(path);
	    							
	    							if(pathMap == null)
	    							{
	    								pathMap = new TreeMap();
	    								currentMap.put(path, pathMap);
	    							}
	    							
	    							currentMap = pathMap;
	    						}
	    					}
	    		
	    					currentMap.put(actionTask.getUName() + "/" + actionTask.getSys_id(), actionTask);
	    				}
	    			}	
				}
	    			
	    			return buildJSPopupMenu(root, defaultMap);
	    			 
				//}//end of if
				
				});
			
		} 
		catch (Throwable t) 
		{
			Log.log.info(t.getMessage(), t);
       HibernateUtil.rethrowNestedTransaction(t);
		}
		
		return popupMenu;
	} //buildPopupMenu

	/**
     * This method build the DT popup menu.
     * 
     * @return
     */
    public String buildDTMenu(String userName)
    {
        String dtMenu = "";
                
        dtMenu = buildJSDTMenu();
        
        return dtMenu;
    } //buildDTMenu
    
	/**
	 * This method build the javascript popup menu.
	 * 
	 * @param root
	 * @param defaultMap
	 * @return
	 */
	protected String buildJSPopupMenu(Map root, Map defaultMap)
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append(" var selected = !graph.isSelectionEmpty(); " +
					   " var cell = graph.getSelectionCell(); " +
					   " var cellName = cell.value.nodeName.toLowerCase(); " + 
					   " var labelName = null; " + 
					   " var labelIdNull = (cell != null && cell.getAttribute('label') == null)?true:false;" + 
					   " var isTaskOrImage = (cell.value.nodeName.toLowerCase() == ('task'))?true:false;" + 
					   " var isRunbook = (cell.value.nodeName.toLowerCase() == ('subprocess'))?true:false;" +
					   " var isText = (cell.value.nodeName.toLowerCase() == ('text'))?true:false;" +
					   //" alert('------ the selected is ' + selected ); alert('------ the isRunbook is ' + isRunbook );" + 
					   " if( cell != null ) " +
					   " { labelName = cell.getAttribute('label'); } " +
					   //" this.menu = new Ext.menu.Menu(");
		               "var thisMenu = new Ext.menu.Menu(");
		                
		sBuffer.append(" {" );  
		sBuffer.append(" id: 'feeds-ctx', "); 
		sBuffer.append(" items: [");
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		
		String actionTaskMenu = this.buildActionTaskMenu(root, defaultMap);
		
		sBuffer.append(actionTaskMenu);
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(this.buildDependencyMenu());
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(this.buildEditMenu());
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(this.buildFormatMenu());
		sBuffer.append("\n");
		
		//end part of popupMenu
		sBuffer.append("]");
		sBuffer.append("}");
		
		sBuffer.append(");"); 
		
		//sBuffer.append(" thisMenu.shadow = false; ");
		//sBuffer.append(" thisMenu.on("+"\'"+"hide"+"\'"+", this.onContextHide, this); "); 
		//sBuffer.append(" thisMenu.showAt([e.clientX, e.clientY]);");
		
		sBuffer.append(" menuLocal = thisMenu;");
		
		return sBuffer.toString();
		
	} //buildJSPopupMenu
	
	/**
     * This method build the javascript DT popup menu.
     * 
     * @param root
     * @param defaultMap
     * @return
     */
    protected String buildJSDTMenu()
    {
        StringBuffer sBuffer = new StringBuffer();
        
        sBuffer.append(" var selected = !graph.isSelectionEmpty(); " + 
                       " var cell = graph.getSelectionCell(); " +
                       " var cellName = cell.value.nodeName.toLowerCase(); " + 
                       " var labelName = null; " +
                       " var labelIdNull = (cell != null && cell.getAttribute('label') == null)?true:false;" + 
                       " var isQuestion = (cell.value.nodeName.toLowerCase() == ('question'))?true:false;" + 
                       " var isDocument = (cell.value.nodeName.toLowerCase() == ('document'))?true:false;" +
                       " var isEdge = (cell.value.nodeName.toLowerCase() == ('edge'))?true:false;" +
                       " var isRoot = (cell.value.nodeName.toLowerCase() == ('root'))?true:false;" +
                       " var isText = false;" +
                       //" alert('------ the selected is ' + selected ); alert('------ the isDocument is ' + isDocument );" + 
//                     " alert('------- isQuestion ' + isQuestion );" +
                       " if( cell != null ) " +
                       " { labelName = cell.getAttribute('label'); } " +
                       //" this.menu = new Ext.menu.Menu(");
                       " thisMenu = new Ext.menu.Menu(");
                        
        sBuffer.append(" {" );  
        sBuffer.append(" id: 'feeds-ctx', "); 
        sBuffer.append(" items: [");
        sBuffer.append("\n");
        
        sBuffer.append("\n");
        sBuffer.append(this.buildSelectTypeMenu());
        sBuffer.append("\n");
        
        sBuffer.append("\n");
        sBuffer.append(this.buildSearchDTMenu());
        sBuffer.append("\n");
        
		sBuffer.append("\n");
		sBuffer.append(this.buildEditDTMenu());
		sBuffer.append("\n");
        
        sBuffer.append("\n");
        sBuffer.append(this.buildFormatMenu());
        sBuffer.append("\n");
        
        //end part of popupMenu
        sBuffer.append("]");
        sBuffer.append("}");
        
        sBuffer.append(");"); 
        
        sBuffer.append(" thisMenu.shadow = false; ");
        sBuffer.append(" thisMenu.on("+"\'"+"hide"+"\'"+", this.onContextHide, this); "); 
        sBuffer.append(" thisMenu.showAt([e.clientX, e.clientY]);");
        
        sBuffer.append(" menuLocal = thisMenu;");
        
        return sBuffer.toString();
        
    } //buildJSDTMenu
    
	/**
	 * This method build the action task menu.
	 * 
	 * @param root
	 * @param defaultMap
	 * @return
	 */
	protected String buildActionTaskMenu(Map root, Map defaultMap)
	{
		StringBuffer sBuffer = new StringBuffer();
		
		boolean selectTaskFlag = PropertiesUtil.getPropertyBoolean("automation.select.task");
		
		if(selectTaskFlag)
		{
    		sBuffer.append("{ "); 
    		sBuffer.append(" id:'Selected Task', ");
    		sBuffer.append(" text:'Select Task', ");
    		sBuffer.append(" disabled: (labelIdNull || !isTaskOrImage || isText), ");
    		sBuffer.append(" handler: function() { }, ");
    		sBuffer.append(" menu: ");
    		sBuffer.append(" { ");
    			sBuffer.append("\n");
    			sBuffer.append(" items: [ ");
    		
    				sBuffer.append(getMenuDefinition(root));
    				sBuffer.append(getMenuDefinition(defaultMap));
    				
    					String lastChar = sBuffer.substring(sBuffer.length()-1, sBuffer.length());
                	
    					if(lastChar.equals(","))
    					{
    						sBuffer.replace(sBuffer.length()-1, sBuffer.length(), "");
    					}
    				
    					sBuffer.append("]");
    					sBuffer.append("\n");
    		sBuffer.append("}");
    		sBuffer.append("},");
		}
		// end actiontasks
		
		//start Edit Task
        sBuffer.append("{");
        sBuffer.append(" id:'Search Task', ");
        sBuffer.append(" text:'Search Task', ");
        sBuffer.append(" disabled: (labelName == null || !selected || isRunbook || isText || labelName == 'Precondition' || cellName == 'subprocess'), ");
        sBuffer.append(" handler: function() { searchTask(); } ");
        sBuffer.append(" },");
        sBuffer.append("{");
        sBuffer.append(" id:'Execute Task', ");
        sBuffer.append(" text:'Execute Task', ");
        sBuffer.append(" disabled: (labelIdNull || !isTaskOrImage), ");
        sBuffer.append(" handler: function()");
        sBuffer.append(" { ");
            
            sBuffer.append(" executeTask(); ");
        
        sBuffer.append(" } ");
        sBuffer.append(" },"); // end Execute Task
        sBuffer.append("{");
        sBuffer.append(" id:'Edit Task', ");
        sBuffer.append(" text:'Edit Task', ");
        sBuffer.append(" disabled: (labelName == null || !isTaskOrImage || isText || !selected || labelName == 'Precondition' || cellName == 'subprocess'), ");
        sBuffer.append(" handler: function() { editTask(); } ");
        sBuffer.append(" },'-',");
        
  
        sBuffer.append("{");
        sBuffer.append(" id:'Search Runbook', ");
        sBuffer.append(" text:'Search Runbook', ");
        sBuffer.append(" disabled: (labelName == null || !selected || !isRunbook || isText || labelName == 'Precondition'), ");
        sBuffer.append(" handler: function() { searchRunbook(); } ");
        sBuffer.append(" },");
        
        sBuffer.append("{");
        sBuffer.append(" id:'Edit Runbook', ");
        sBuffer.append(" text:'Edit Runbook', ");
        sBuffer.append(" disabled: (labelName == null || !isRunbook || !selected || isText || labelName == 'Precondition' || cellName == 'task'), ");
        sBuffer.append(" handler: function() { editSubprocess(); } ");
        sBuffer.append(" },'-',");
		
		return sBuffer.toString();
		
	} //buildActionTaskMenu
	
	/**
	 * this method build the execute menu.
	 * 
	 * @return
	 */
	protected String buildExecute()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("\n");
		sBuffer.append("{");
		sBuffer.append(" id:'Execute Task', ");
		sBuffer.append(" text:'Execute Task', ");
		sBuffer.append(" disabled: (labelIdNull || isText ||  !isTaskOrImage), ");
		sBuffer.append(" handler: function()");
		sBuffer.append(" { ");
			
			sBuffer.append(" executeTask(); ");
		
		sBuffer.append(" } ");
		sBuffer.append(" },'-',"); // end Execute Task
		sBuffer.append("\n");
		
		return sBuffer.toString();
	} //buildExecute
	
	/**
	 * This method build the format select menu.
	 * 
	 * @return
	 */
	protected String buildFormatSelect()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("{");
		sBuffer.append(" text:'Select Vertices', ");
		sBuffer.append(" disabled: false, ");
		sBuffer.append(" scope: this, ");
		sBuffer.append(" preventDefault: true, ");
		sBuffer.append(" handler: function() { graph.selectVertices(); } ");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Select Edges', ");
		sBuffer.append(" disabled: false, ");
		sBuffer.append(" scope: this, ");
		sBuffer.append(" preventDefault: true, ");
		sBuffer.append(" handler: function() { graph.selectEdges(); } ");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Select ALL   ', ");
		sBuffer.append(" disabled: false, ");
		sBuffer.append(" scope: this, ");
		sBuffer.append(" preventDefault: true, ");
		sBuffer.append(" handler: function() { graph.selectAll(); } ");
		sBuffer.append(" }");
		
		return sBuffer.toString();
		
	} //buildFormatSelect
	
	/**
	 * This method build the edit menu.
	 * 
	 * @return
	 */
	protected String buildEditMenu()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("{");
		sBuffer.append(" id:'Edit Inputs', ");
		sBuffer.append(" text:'Edit Inputs', ");
		sBuffer.append(" disabled: (labelName == null || !selected || isText || labelName == 'Precondition' || cellName == 'subprocess'), ");
		sBuffer.append(" handler: function() { editInputs(); } ");
		sBuffer.append(" },");
		
		sBuffer.append("{");
		sBuffer.append(" id:'Edit Outputs', ");
		sBuffer.append(" text:'Edit Outputs', ");
		sBuffer.append(" disabled: (labelName == null || !selected || isText || labelName == 'Precondition' || cellName == 'subprocess'), ");
		sBuffer.append(" handler: function() { editOutput(); } ");
		sBuffer.append(" },");
		/*
		sBuffer.append("{");
		sBuffer.append(" id:'Edit Properties', ");
        sBuffer.append(" text:'Edit Properties', ");
        sBuffer.append(" disabled: !selected, ");
        sBuffer.append(" handler: function() ");
        sBuffer.append(" { ");
            
            sBuffer.append(" var cell = graph.getSelectionCell(); ");
            sBuffer.append(" if(cell != null) ");
            sBuffer.append(" { ");
                        sBuffer.append("  showProperties(graph, cell); ");
            sBuffer.append(" } ");  
        sBuffer.append(" } ");
        sBuffer.append(" },");
		*/
		sBuffer.append("'-',");
		/*
		sBuffer.append("{");
		sBuffer.append(" id:'Edit Task', ");
		sBuffer.append(" text:'Edit Task', ");
		sBuffer.append(" disabled: (labelName == null || !isTaskOrImage || !selected || labelName == 'Precondition' || cellName == 'subprocess'), ");
		sBuffer.append(" handler: function() { editTask(); } ");
		sBuffer.append(" },");
		*/
		/*
		sBuffer.append("{");
		sBuffer.append(" id:'Edit Runbook', ");
		sBuffer.append(" text:'Edit Runbook', ");
		sBuffer.append(" disabled: (labelName == null || !isRunbook || !selected || labelName == 'Precondition' || cellName == 'task'), ");
		sBuffer.append(" handler: function() { editSubprocess(); } ");
		sBuffer.append(" },");
		*/
		
//		sBuffer.append("{");
//		sBuffer.append(" text:'Edit Properties', ");
//		sBuffer.append(" disabled: !selected, ");
//		sBuffer.append(" handler: function() ");
//		sBuffer.append(" { ");
//			
//			sBuffer.append(" var cell = graph.getSelectionCell(); ");
//			sBuffer.append(" if(cell != null) ");
//			sBuffer.append(" { ");
//						sBuffer.append("  showProperties(graph, cell); ");
//			sBuffer.append(" } ");	
//		sBuffer.append(" } ");
//		sBuffer.append(" },");
		
		//sBuffer.append("'-',");
		// // end Edit
		
		return sBuffer.toString();
		
	} //buildEditMenu
	
	/**
	 * This method build the Font menu.
	 * @return
	 */
	protected String buildFontMenu()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		//start Font
		sBuffer.append("{");
		sBuffer.append(" text:'Font', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" menu: ");
		sBuffer.append(" [ ");
		sBuffer.append("{");
		sBuffer.append(" text:'Font Size', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Font Family', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" },");
		sBuffer.append("{");
		
		sBuffer.append(" text:'Font Color', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" iconCls:'fontcolor-icon', ");
		sBuffer.append(" menu: fontColorMenu, ");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Font Background', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu: labelBackgroundMenu, ");
		sBuffer.append(" }, '-',");
		sBuffer.append("{");
		sBuffer.append(" text:'Bold', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" iconCls:'bold-icon', ");
		sBuffer.append(" scope:this, ");
		sBuffer.append(" handler: function(){ graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_BOLD); }, ");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Italic', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" iconCls:'italic-icon', ");
		sBuffer.append(" scope:this, ");
		sBuffer.append(" handler: function() { graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_ITALIC); }, ");
		sBuffer.append(" }");
		
		sBuffer.append(" ] ");
		sBuffer.append(" },'-',"); // end font
		
		return sBuffer.toString();
	} //buildFontMenu
	
	/**
	 * This method build the format menu.
	 * 
	 * @return
	 */
	protected String buildFormatMenu()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		//start Format
		sBuffer.append("{");
		sBuffer.append(" text:'Format', ");
		sBuffer.append(" disabled: isText,  ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" menu: ");
		sBuffer.append(" [ ");
		
		sBuffer.append("\n");
		sBuffer.append(buildFormatBackground());
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(buildFormatLabel());
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(buildFormatLine());
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(buildFormatLinestart());
		sBuffer.append("\n");
	
		sBuffer.append("\n");
		sBuffer.append(buildFormatLineend());
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(buildFormatConnection());
		sBuffer.append("\n");
		
		sBuffer.append("\n");
		sBuffer.append(buildFormatShape());
		sBuffer.append("\n");
		
		
		sBuffer.append("\n");
		sBuffer.append(buildFormatSelect());
		sBuffer.append("\n");
		
		//start 
		sBuffer.append(" ] ");
		sBuffer.append(" },"); // end Shape
		
		sBuffer.append("{");
        sBuffer.append(" id:'Edit Properties', ");
        sBuffer.append(" text:'Edit Properties', ");
        sBuffer.append(" disabled: isText || !selected, ");
        sBuffer.append(" handler: function() ");
        sBuffer.append(" { ");
            
            sBuffer.append(" var cell = graph.getSelectionCell(); ");
            sBuffer.append(" if(cell != null) ");
            sBuffer.append(" { ");
                        sBuffer.append("  showProperties(graph, cell); ");
            sBuffer.append(" } ");  
        sBuffer.append(" } ");
        sBuffer.append(" }");
		//end Format
		
		return sBuffer.toString();
	} //buildFormatMenu
	
	/**
	 * This method build the Format background menu.
	 * 
	 * @return
	 */
	public String buildFormatBackground()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("{");
		sBuffer.append(" text:'Background', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu: ");
			sBuffer.append(" [ ");
				sBuffer.append("{");
				sBuffer.append(" text:'Fill Color', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" menu: fillColorMenu ");
				sBuffer.append(" },");
				
				sBuffer.append("{");
				sBuffer.append(" text:'Gradient', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" menu: gradientColorMenu ");
				sBuffer.append(" },'-',");
						
				sBuffer.append("{");
				sBuffer.append(" text:'Image', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" handler: function() ");
				sBuffer.append(" { ");
				sBuffer.append(" var value = '';");
				sBuffer.append(" var state = graph.getView().getState(graph.getSelectionCell());");
				sBuffer.append(" if (state != null) ");
				sBuffer.append(" { ");
				sBuffer.append(" value = state.style[mxConstants.STYLE_IMAGE] || value; ");
				sBuffer.append(" } ");
				sBuffer.append(" value = mxUtils.prompt('Enter Image URL', value); ");
				sBuffer.append(" if (value != null) ");
				sBuffer.append(" { ");
				sBuffer.append(" graph.setCellStyles(mxConstants.STYLE_IMAGE, value); ");
				sBuffer.append(" } ");
				sBuffer.append(" } ");
				sBuffer.append(" },");
				
				sBuffer.append("{");
				sBuffer.append(" text:'Rounded', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() { graph.toggleCellStyles(mxConstants.STYLE_ROUNDED); } ");
				sBuffer.append(" },");
				
				sBuffer.append("{");
				sBuffer.append(" text:'Shadow', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() { graph.toggleCellStyles(mxConstants.STYLE_SHADOW); } ");
				sBuffer.append(" },'-',");
						
				sBuffer.append("{");
				sBuffer.append(" text:'Opacity', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() ");
					sBuffer.append(" { ");
					sBuffer.append(" var value = mxUtils.prompt('Enter Opacity (0-100%)', '100'); ");
					sBuffer.append(" if (value != null) ");
					sBuffer.append(" { ");
					sBuffer.append(" graph.setCellStyles(mxConstants.STYLE_OPACITY, value); ");
					sBuffer.append("  } ");
					sBuffer.append("  } ");
				sBuffer.append(" }");		
						
			sBuffer.append(" ] ");
		sBuffer.append(" },'-',"); // end Background
		
		return sBuffer.toString();
	} //buildFormatBackground
	
	/**
	 * This method build the format label menu.
	 * 
	 * @return
	 */
	public String buildFormatLabel()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("{");
		sBuffer.append(" text:'Label', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu: ");
		sBuffer.append(" [ ");
			sBuffer.append("{");
			sBuffer.append(" text:'Fontcolor', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" iconCls:'fontcolor-icon', ");
			sBuffer.append(" menu: fontColorMenu ");
			sBuffer.append(" },'-',");
			sBuffer.append("{");
			sBuffer.append(" text:'Label Fill', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" menu: labelBackgroundMenu ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Label Border', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" menu: labelBorderMenu ");
			sBuffer.append(" },'-',");
			sBuffer.append("{");
			sBuffer.append(" text:'Rotate Label', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" scope:this, ");
			sBuffer.append(" handler: function() { ");
			sBuffer.append("  graph.toggleCellStyles(mxConstants.STYLE_HORIZONTAL, true); }");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Text opacity', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" scope:this, ");
			sBuffer.append(" handler: function() { ");
			sBuffer.append("  var value = mxUtils.prompt('Enter text opacity (0-100%)', '100'); ");
			sBuffer.append("  if (value != null) { ");
			sBuffer.append("  graph.setCellStyles(mxConstants.STYLE_TEXT_OPACITY, value); } ");
			sBuffer.append(	"}");
			sBuffer.append(" },'-',");
			sBuffer.append("{");
			sBuffer.append(" text:'Position', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" handler: function() { }, ");
			sBuffer.append(" menu: ");
			sBuffer.append("  [ ");
				sBuffer.append("{");
				sBuffer.append(" text:'Top', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() {  graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_TOP); graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_BOTTOM);}");
				sBuffer.append("},");
				sBuffer.append("{");
				sBuffer.append(" text:'Middle', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() {  graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_MIDDLE); graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);} ");
				sBuffer.append("},");
				sBuffer.append("{");
				sBuffer.append(" text:'Bottom', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() {  graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_BOTTOM); graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP);}");
				sBuffer.append("},'-',");
				sBuffer.append("{");
				sBuffer.append(" text:'Left', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() {  graph.setCellStyles(mxConstants.STYLE_LABEL_POSITION, mxConstants.ALIGN_LEFT); graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_RIGHT);} ");
				sBuffer.append("},");
				sBuffer.append("{");
				sBuffer.append(" text:'Center', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() {  graph.setCellStyles(mxConstants.STYLE_LABEL_POSITION, mxConstants.ALIGN_CENTER); graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);} ");
				sBuffer.append("},");
				sBuffer.append("{");
				sBuffer.append(" text:'Right', ");
				sBuffer.append(" disabled: !selected, ");
				sBuffer.append(" scope:this, ");
				sBuffer.append(" handler: function() {  graph.setCellStyles(mxConstants.STYLE_LABEL_POSITION, mxConstants.ALIGN_RIGHT); graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_LEFT);} ");
				sBuffer.append("}");
				
			sBuffer.append("  ] ");
			sBuffer.append(" }");
		sBuffer.append(" ] ");
		sBuffer.append(" },"); //end Label
		
		return sBuffer.toString();
	} //buildFormatLabel
	
	/**
	 * This method build the format shape menu.
	 * 
	 * @return
	 */
	public String buildFormatShape()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("{");
		sBuffer.append(" text:'Align', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu:");
		sBuffer.append(" [");
			sBuffer.append("{");
			sBuffer.append(" text:'Left', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/alignleft.gif',  ");
			sBuffer.append(" scope:this,  ");
			sBuffer.append(" handler: function() { graph.alignCells(mxConstants.ALIGN_LEFT); }   ");
			sBuffer.append("},");
			sBuffer.append("{");
			sBuffer.append(" text:'Center', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/aligncenter.gif',  ");
			sBuffer.append(" scope:this,  ");
			sBuffer.append(" handler: function() { graph.alignCells(mxConstants.ALIGN_CENTER); }   ");
			sBuffer.append("},");
			sBuffer.append("{");
			sBuffer.append(" text:'Right', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/alignright.gif',  ");
			sBuffer.append(" scope:this,  ");
			sBuffer.append(" handler: function() { graph.alignCells(mxConstants.ALIGN_RIGHT); }   ");
			sBuffer.append("},");
			sBuffer.append("{");
			sBuffer.append(" text:'Top', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/aligntop.gif',  ");
			sBuffer.append(" scope:this,  ");
			sBuffer.append(" handler: function() { graph.alignCells(mxConstants.ALIGN_TOP); }   ");
			sBuffer.append("},");
			sBuffer.append("{");
			sBuffer.append(" text:'Middle', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/alignmiddle.gif',  ");
			sBuffer.append(" scope:this,  ");
			sBuffer.append(" handler: function() { graph.alignCells(mxConstants.ALIGN_MIDDLE); }   ");
			sBuffer.append("},");
			sBuffer.append("{");
			sBuffer.append(" text:'Bottom', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/alignbottom.gif',  ");
			sBuffer.append(" scope:this,  ");
			sBuffer.append(" handler: function() { graph.alignCells(mxConstants.ALIGN_BOTTOM); }   ");
			sBuffer.append("}");
		sBuffer.append(" ]");
		sBuffer.append("},");
		sBuffer.append("{");
		sBuffer.append(" text:'To Back', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" icon: '/resolve/jsp/model/images/toback.gif',  ");
		sBuffer.append(" scope:this,  ");
		sBuffer.append(" handler: function() { graph.orderCells(true); }   ");
		sBuffer.append("},");
		sBuffer.append("{");
		sBuffer.append(" text:'To Front', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" icon: '/resolve/jsp/model/images/tofront.gif',  ");
		sBuffer.append(" scope:this,  ");
		sBuffer.append(" handler: function() { graph.orderCells(false); }   ");
		sBuffer.append("},'-', ");
		
		return sBuffer.toString();
	} //buildFormatShape
	
	/**
	 * This method build the format connection menu.
	 * 
	 * @return
	 */
	public String buildFormatConnection()
	{
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("{");
		sBuffer.append(" text:'Connector', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu:  ");
		sBuffer.append(" [  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Straight', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/straight.gif', ");
			sBuffer.append(" handler: function() { graph.setCellStyle('straight'); } ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Horizontal', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/connect.gif', ");
			sBuffer.append(" handler: function() { graph.setCellStyle(null); } ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Vertical', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/vertical.gif', ");
			sBuffer.append(" handler: function() { graph.setCellStyle('vertical'); } ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Entity Relation', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/entity.gif', ");
			sBuffer.append(" handler: function() { graph.setCellStyle('edgeStyle=entityRelationEdgeStyle'); } ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Arrow', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/arrow.gif', ");
			sBuffer.append(" handler: function() { graph.setCellStyle('arrow'); } ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Plain', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" handler: function() { graph.toggleCellStyles(mxConstants.STYLE_NOEDGESTYLE, false); }");
			sBuffer.append("}");
		sBuffer.append(" ] ");
		sBuffer.append(" },'-',  ");
		
		return sBuffer.toString();
	} //buildFormatConnection
	
	/**
	 * This method build the format line end menu.
	 * 
	 * @return
	 */
	public String buildFormatLineend()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("{");
		sBuffer.append(" text:'Lineend', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu:  ");
		sBuffer.append(" [  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Open', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/open_end.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_OPEN); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Classic', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/classic_end.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_CLASSIC); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Block', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/block_end.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_BLOCK); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Diamond', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/diamond_end.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_DIAMOND); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Oval', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/oval_end.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_OVAL); }   ");
			sBuffer.append(" },'-',");
			sBuffer.append("{");
			sBuffer.append(" text:'None', ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_ENDARROW, mxConstants.NONE); }   ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Size', ");
			sBuffer.append(" handler: function() { var size = mxUtils.prompt('Enter Size', mxConstants.DEFAULT_MARKERSIZE); ");
			sBuffer.append(" if (size != null) ");
			sBuffer.append("{ graph.setCellStyles(mxConstants.STYLE_ENDSIZE, size); }");
			sBuffer.append("} ");
			sBuffer.append(" }");
			
		sBuffer.append(" ]  ");
		sBuffer.append(" },  ");
		
		return sBuffer.toString();
	} //buildFormatLineend
	
	/**
	 * This method build the format line start menu.
	 * 
	 * @return
	 */
	public String buildFormatLinestart()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		sBuffer.append("{");
		sBuffer.append(" text:'Linestart', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu:  ");
		sBuffer.append(" [  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Open', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/open_start.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_STARTARROW, mxConstants.ARROW_OPEN); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Classic', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/classic_start.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_STARTARROW, mxConstants.ARROW_CLASSIC); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Block', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/block_start.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_STARTARROW, mxConstants.ARROW_BLOCK); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Diamond', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/diamond_start.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_STARTARROW, mxConstants.ARROW_DIAMOND); }   ");
			sBuffer.append(" },  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Oval', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" icon: '/resolve/jsp/model/images/oval_start.gif',  ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_STARTARROW, mxConstants.ARROW_OVAL); }   ");
			sBuffer.append(" },'-',");
			sBuffer.append("{");
			sBuffer.append(" text:'None', ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_STARTARROW, mxConstants.NONE); }   ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Size', ");
			sBuffer.append(" handler: function() { var size = mxUtils.prompt('Enter Size', mxConstants.DEFAULT_MARKERSIZE); ");
			sBuffer.append(" if (size != null) ");
			sBuffer.append("{ graph.setCellStyles(mxConstants.STYLE_STARTSIZE, size); }");
			sBuffer.append("} ");
			sBuffer.append(" }");
			
		sBuffer.append(" ]  ");
		sBuffer.append(" },  ");
		
		return sBuffer.toString();
	} //buildFormatLinestart
	
	/**
	 * This method build the format line menu.
	 * 
	 * @return
	 */
	public String buildFormatLine()
	{
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("{");
		sBuffer.append(" text:'Line', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" menu:  ");
		sBuffer.append(" [  ");
			sBuffer.append("{");
			sBuffer.append(" text:'Linecolor', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" iconCls:'linecolor-icon', ");
			sBuffer.append(" menu: lineColorMenu ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Dashed', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" scope:this, ");
			sBuffer.append(" handler: function() {  graph.toggleCellStyles(mxConstants.STYLE_DASHED); } ");
			sBuffer.append(" },");
			sBuffer.append("{");
			sBuffer.append(" text:'Linewidth', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" handler: function()");
				sBuffer.append("{ ");
				sBuffer.append(" var value = '0';");
				sBuffer.append(" var state = graph.getView().getState(graph.getSelectionCell()); ");
				sBuffer.append(" if (state != null) ");
				sBuffer.append(" { ");
				sBuffer.append(" value = state.style[mxConstants.STYLE_STROKEWIDTH] || 1; ");
				sBuffer.append(" } ");
				sBuffer.append(" value = mxUtils.prompt('Enter Linewidth (Pixels)', value); ");
				sBuffer.append(" if (value != null) ");
				sBuffer.append(" { ");
				sBuffer.append(" graph.setCellStyles(mxConstants.STYLE_STROKEWIDTH, value); ");
				sBuffer.append(" } ");
				sBuffer.append(" } ");
			sBuffer.append(" }");
			
		sBuffer.append(" ]  ");
		sBuffer.append(" },");
		
		return sBuffer.toString();
		
	} //buildFormatLine
	
	/**
	 * This method build the shape menu.
	 * 
	 * @return
	 */
	protected String buildShapeMenu()
	{
		StringBuffer sBuffer = new StringBuffer();
		
		//start Shape
		sBuffer.append("{");
		sBuffer.append(" text:'Shape', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" menu: ");
		sBuffer.append(" [ ");
		sBuffer.append("{");
		sBuffer.append(" text:'Ungroup', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" icon: '/resolve/jsp/model/images/ungroup.gif', ");
		sBuffer.append(" scope:this, ");
		sBuffer.append(" handler: function() { graph.setSelectionCells(graph.ungroupCells()); }, ");
		sBuffer.append(" }, '-',");
		sBuffer.append("{");
		sBuffer.append(" text:'Bring to front', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" icon: '/resolve/jsp/model/images/tofront.gif', ");
		sBuffer.append(" scope:this, ");
		sBuffer.append(" handler: function() { graph.orderCells(false); }, ");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Send to back', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" icon: '/resolve/jsp/model/images/toback.gif', ");
		sBuffer.append(" scope:this, ");
		sBuffer.append(" handler: function() { graph.orderCells(true); }, ");
		sBuffer.append(" }");
		
		sBuffer.append(" ] ");
		sBuffer.append(" },"); // end Shape
		
		return sBuffer.toString();
	} //buildShapeMenu
	
	/**
	 * This method build the dependence menu.
	 * 
	 * @return
	 */
	protected String buildDependencyMenu()
	{
		StringBuffer sBuffer = new StringBuffer();
		//start Dependence
		sBuffer.append("{ "); 
		sBuffer.append(" id:'Dependency', ");
		sBuffer.append(" text:'Dependency', ");
		sBuffer.append(" disabled: (!selected || isText || isRunbook), ");
		sBuffer.append(" handler: function() { }, ");
		
		sBuffer.append(" menu: ");
		sBuffer.append(" { ");
		sBuffer.append(" items: [ ");
		
		sBuffer.append("{");
		sBuffer.append(" id:'Execution', ");
		sBuffer.append(" text:'Execution', ");
		sBuffer.append(" disabled: (labelName != null || isText || !selected ), ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" menu: [ ");
			sBuffer.append("{");
			sBuffer.append(" text:'Required', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_DASHED); } ");
			sBuffer.append(" },");
			
			sBuffer.append("{");
			sBuffer.append(" text:'Not Required', ");
			sBuffer.append(" disabled: !selected, ");
			sBuffer.append(" handler: function() { graph.setCellStyles(mxConstants.STYLE_DASHED, true); } ");
			sBuffer.append(" }");
			
		sBuffer.append("]"); 
		sBuffer.append("},"); //end Execution
		
		sBuffer.append("{");
		sBuffer.append(" id:'Condition', ");
		sBuffer.append(" text:'Condition', ");
		sBuffer.append(" disabled: (labelName != null || !selected ), ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" menu: [");
		sBuffer.append("{");
		sBuffer.append(" text:'Success/Good', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() { graph.setCellStyles('strokeColor', 'green'); }");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Failed/Bad', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() { graph.setCellStyles('strokeColor', 'red'); } ");
		sBuffer.append(" },");
		sBuffer.append("{");
		sBuffer.append(" text:'Not required', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() { graph.setCellStyles('strokeColor', 'black'); } ");
		sBuffer.append(" }");	
		sBuffer.append("]"); 
		sBuffer.append("},"); //Condition

		sBuffer.append("{");
		sBuffer.append(" id:'Merge', ");
		sBuffer.append(" text:'Merge', ");
		sBuffer.append(" disabled: (labelName == null || !selected ), ");
		sBuffer.append(" handler: function() { }, ");
		sBuffer.append(" menu: [");
		sBuffer.append("{");
		sBuffer.append(" text:'ALL (wait for all tasks)', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() ");
			sBuffer.append("{ ");
			sBuffer.append(" var cell = graph.getSelectionCell();");
	
			//==== start remove dashed line if existed ======== 
				
				sBuffer.append("  var styleVar = cell.style; ");
				
				sBuffer.append("  thisModified = false; ");
				sBuffer.append("  var mergeSelect = cell.getValue().getAttribute('merge'); ");
				sBuffer.append("  if(mergeSelect == null || (mergeSelect != null && mergeSelect != 'merge = ALL')) ");
				sBuffer.append("  { thisModified = true; } ");
				
				sBuffer.append("  var styleVarUpdate = ''; ");
				sBuffer.append("  if(styleVar.indexOf('dashed=1')>0) ");
				sBuffer.append("  { ");
				sBuffer.append("     styleVarUpdate = styleVar.substring(0, styleVar.indexOf('dashed=1')); ");
				sBuffer.append("     styleVarUpdate += styleVar.substring(styleVar.indexOf('dashed=1')+'dashed=1'.length, styleVar.length); ");
				sBuffer.append("      ");
				
				sBuffer.append("     if(styleVarUpdate != '' && styleVarUpdate.length>0) ");
				sBuffer.append("     {");
				sBuffer.append("     	cell.setStyle(styleVarUpdate);");
				sBuffer.append("     	graph.refresh();");
				sBuffer.append("     }");
				sBuffer.append("  } ");
		
			//==== end remove dashed line if existed ===.	
		 
			sBuffer.append("  cell.setAttribute('merge', 'merge = ALL');");
			sBuffer.append( "} ");
		sBuffer.append(" },");
		
		sBuffer.append("{");
		sBuffer.append(" text:'ANY (wait for one task)', ");
		sBuffer.append(" disabled: !selected, ");
		sBuffer.append(" handler: function() ");
			sBuffer.append("{ ");
			sBuffer.append("  var cell = graph.getSelectionCell(); ");
			
			//==== start add dashed line if not existed ===. 
			
			sBuffer.append("  var styleVar = cell.style; ");
			
            sBuffer.append("  thisModified = false; ");
            sBuffer.append("  var mergeSelect = cell.getValue().getAttribute('merge'); ");
            sBuffer.append("  if(mergeSelect == null || (mergeSelect != null && mergeSelect != 'merge = ANY')) ");
            sBuffer.append("  { thisModified = true; } ");
			
			sBuffer.append("  var styleVarUpdate = ''; ");
			sBuffer.append("  if(styleVar.indexOf('dashed=1')<0) ");
			sBuffer.append("  { ");
			sBuffer.append("     var lastChar = styleVar.substring(styleVar.length-1, styleVar.length); ");
			sBuffer.append("     if(lastChar != ';') ");
			sBuffer.append("     { ");
			sBuffer.append("     	styleVarUpdate = styleVar + ';dashed=1'; ");
			sBuffer.append("     } ");
			sBuffer.append("     else { ");
			sBuffer.append("     	styleVarUpdate = styleVar + 'dashed=1'; ");		
			sBuffer.append("     } ");
			sBuffer.append("     if(styleVarUpdate != '' && styleVarUpdate.length>0) ");
			sBuffer.append("     {");
			sBuffer.append("     	cell.setStyle(styleVarUpdate);");
			sBuffer.append("     	graph.refresh();");
			sBuffer.append("     }");
			sBuffer.append("  } ");
	
		   //==== end add dashed line if not existed ===.	
			
			
			sBuffer.append("  cell.setAttribute('merge', 'merge = ANY'); ");
			sBuffer.append("} ");
		sBuffer.append(" },");
		
		//for Merge Targets
		sBuffer.append("{");
        sBuffer.append(" text:'TARGETS (wait for all instances)', ");
        sBuffer.append(" disabled: !selected, ");
        sBuffer.append(" handler: function() ");
            sBuffer.append("{ ");
            sBuffer.append(" var cell = graph.getSelectionCell();");
            
            //==== start remove dashed line if existed ===. 
                
                sBuffer.append("  var styleVar = cell.style; ");
                
                sBuffer.append("  thisModified = false; ");
                sBuffer.append("  var mergeSelect = cell.getValue().getAttribute('merge'); ");
                sBuffer.append("  if(mergeSelect == null || (mergeSelect != null && mergeSelect != 'merge = TARGETS')) ");
                sBuffer.append("  { thisModified = true; } ");
                
                sBuffer.append("  var styleVarUpdate = ''; ");
                sBuffer.append("  if(styleVar.indexOf('dashed=1')>0) ");
                sBuffer.append("  { ");
                sBuffer.append("     styleVarUpdate = styleVar.substring(0, styleVar.indexOf('dashed=1')); ");
                sBuffer.append("     styleVarUpdate += styleVar.substring(styleVar.indexOf('dashed=1')+'dashed=1'.length, styleVar.length); ");
                sBuffer.append("      ");
                
                sBuffer.append("     if(styleVarUpdate != '' && styleVarUpdate.length>0) ");
                sBuffer.append("     {");
                sBuffer.append("        cell.setStyle(styleVarUpdate);");
                sBuffer.append("        graph.refresh();");
                sBuffer.append("     }");
                sBuffer.append("  } ");
        
            //==== end remove dashed line if existed ===.   
         
            sBuffer.append("  cell.setAttribute('merge', 'merge = TARGETS');");
            sBuffer.append( "} ");
        sBuffer.append(" }");
        //end merge targets
		
		sBuffer.append("]"); 
		sBuffer.append("}"); //Merge
		
		sBuffer.append("]");
		sBuffer.append("}");
		
		sBuffer.append("},"); // end Dependence
		
		return sBuffer.toString();
	} //buildDependencyMenu
	
	/**
     * This method build the parameters menu.
     * 
     * @return
     */
    protected String buildEditDTMenu()
    {
        StringBuffer sBuffer = new StringBuffer();
        
        //start Edit Task
//        sBuffer.append("{");
//        sBuffer.append(" id:'Edit Parameters', ");
//        sBuffer.append(" text:'Edit Parameters', ");
//        sBuffer.append(" disabled: (labelName == null || !selected || labelName == 'Root'), ");
//        sBuffer.append(" handler: function() { editParams(); } ");
//        //TODO: automation.js check editInputs() also check getMenuDefinition and getInputsAndOutputs
//        sBuffer.append(" },");
//        
        sBuffer.append("{");
        sBuffer.append(" id:'View Document', ");
        sBuffer.append(" text:'View Document', ");
        sBuffer.append(" disabled: (labelName == null || !isDocument || !selected), ");
        sBuffer.append(" handler: function() { viewDTModel(); } ");
        sBuffer.append(" },");
        
        sBuffer.append("'-',");
        
        sBuffer.append("{");
        sBuffer.append(" id:'Edit Decision Tree', ");
        sBuffer.append(" text:'Edit Decision Tree', ");
        sBuffer.append(" disabled: (labelName == null || !isDocument || !selected), ");
        sBuffer.append(" handler: function() { editDTModel(); } ");
        sBuffer.append(" },");
        
        sBuffer.append("'-',");
        
        /*
        sBuffer.append("{");
        sBuffer.append(" text:'Edit Properties', ");
        sBuffer.append(" disabled: !selected, ");
        sBuffer.append(" handler: function() ");
        sBuffer.append(" { ");
            
            sBuffer.append(" var cell = graph.getSelectionCell(); ");
            sBuffer.append(" if(cell != null) ");
            sBuffer.append(" { ");
                        sBuffer.append("  showProperties(graph, cell); ");
            sBuffer.append(" } ");  
        sBuffer.append(" } ");
        sBuffer.append(" },");
        
        sBuffer.append("'-',");
        */
        // // end Edit
        
        return sBuffer.toString();
        
    } //buildEditDTMenu
    
    /**
     * This method build the DT Select Type menu.
     * 
     * @return
     */
    protected String buildSelectTypeMenu()
    {
        StringBuffer sBuffer = new StringBuffer();
        //start Select Type
        sBuffer.append("{ "); 
        sBuffer.append(" id:'Select Type', ");
        sBuffer.append(" text:'Select Type', ");
        sBuffer.append(" disabled: (labelName == null || !selected || !isQuestion), ");
        sBuffer.append(" handler: function() { }, ");
        sBuffer.append(" menu: [");
        sBuffer.append("{");
        sBuffer.append(" text:'Radio', ");
        sBuffer.append(" disabled: !selected, ");
        sBuffer.append(" handler: function() ");
            sBuffer.append("{ ");
            sBuffer.append(" var cell = graph.getSelectionCell();");
            
            //==== start remove dashed line if existed ===. 
                
                sBuffer.append("  var styleVar = cell.style; ");
                sBuffer.append("  var styleVarUpdate = ''; ");
                sBuffer.append("  if(styleVar.indexOf('dashed=1')>0) ");
                sBuffer.append("  { ");
                sBuffer.append("     styleVarUpdate = styleVar.substring(0, styleVar.indexOf('dashed=1')); ");
                sBuffer.append("     styleVarUpdate += styleVar.substring(styleVar.indexOf('dashed=1')+'dashed=1'.length, styleVar.length); ");
                sBuffer.append("     thisModified = true; ");
                sBuffer.append("      ");
                
                sBuffer.append("     if(styleVarUpdate != '' && styleVarUpdate.length>0) ");
                sBuffer.append("     {");
                sBuffer.append("        cell.setStyle(styleVarUpdate);");
                sBuffer.append("        graph.refresh();");
                sBuffer.append("     }");
                sBuffer.append("  } ");
        
            //==== end remove dashed line if existed ===.   
         
            sBuffer.append("  cell.setAttribute('select', 'RADIO');");
            sBuffer.append( "} ");
        sBuffer.append(" },");
        
        sBuffer.append("{");
        sBuffer.append(" text:'Drop Down', ");
        sBuffer.append(" disabled: !selected, ");
        sBuffer.append(" handler: function() ");
            sBuffer.append("{ ");
            sBuffer.append("  var cell = graph.getSelectionCell(); ");
            
            //==== start add dashed line if not existed ===. 
            
            sBuffer.append("  var styleVar = cell.style; ");
            sBuffer.append("  var styleVarUpdate = ''; ");
            sBuffer.append("  if(styleVar.indexOf('dashed=1')<0) ");
            sBuffer.append("  { ");
            sBuffer.append("     var lastChar = styleVar.substring(styleVar.length-1, styleVar.length); ");
            sBuffer.append("     if(lastChar != ';') ");
            sBuffer.append("     { ");
            sBuffer.append("        styleVarUpdate = styleVar + ';dashed=1'; ");
            sBuffer.append("     } ");
            sBuffer.append("     else { ");
            sBuffer.append("        styleVarUpdate = styleVar + 'dashed=1'; ");     
            sBuffer.append("     } ");
            sBuffer.append("     thisModified = true; ");
            sBuffer.append("     if(styleVarUpdate != '' && styleVarUpdate.length>0) ");
            sBuffer.append("     {");
            sBuffer.append("        cell.setStyle(styleVarUpdate);");
            sBuffer.append("        graph.refresh();");
            sBuffer.append("     }");
            sBuffer.append("  } ");
    
           //==== end add dashed line if not existed ===.   
            
            
            sBuffer.append("  cell.setAttribute('select', 'DROPDOWN'); ");
            sBuffer.append("} ");
        sBuffer.append(" }");
        
        sBuffer.append("]");
        
        sBuffer.append("},'-',"); // end Dependence
        
        return sBuffer.toString();
    } //buildDependencyMenu
    
    /**
     * This method build the DT Select Type menu.
     * 
     * @return
     */
    protected String buildSearchDTMenu()
    {
        StringBuffer sBuffer = new StringBuffer();
        //start Select Type
        sBuffer.append("{ "); 
        sBuffer.append(" id:'Search Decision Tree', ");
        sBuffer.append(" text:'Search Decision Tree', ");
        sBuffer.append(" disabled: (labelName == null || !selected ||isQuestion), ");
        sBuffer.append(" handler: function() {searchDT(); } ");
        sBuffer.append("},'-',"); // end Dependence
        
        return sBuffer.toString();
    } //buildSearchDTMenu
    
	/**
	 * This method build the menu definition menu.
	 * 
	 * @param currentMap
	 * @return
	 */
	protected String getMenuDefinition(Map currentMap)
	{
		StringBuffer sBuffer = new StringBuffer();
		StringBuffer updatedBuffer = new StringBuffer();
		
        if (currentMap != null)
        {
            for (Iterator i=currentMap.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry)i.next();
                String name = (String)entry.getKey();
                Object obj = entry.getValue();
                
                // insert entry into currentMap
                if (obj instanceof ResolveActionTask)
                {
                	this.lastIsItem = true;
                	ResolveActionTask actiontask = (ResolveActionTask)obj;
                	
                	String uDesc = (actiontask.getUDescription() == null)?(""):(actiontask.getUDescription());
                	uDesc = filter(uDesc);
                	uDesc = StringEscapeUtils.escapeHtml(uDesc);
                	
                	String uName = actiontask.getUName();
                	
                	uName = filter(uName);
                	uName = StringEscapeUtils.escapeHtml(uName);
                	
                	String uNameSpace = actiontask.getUNamespace();
                	uNameSpace = filter(uNameSpace);
                	uNameSpace = StringEscapeUtils.escapeHtml(uNameSpace);
                	              	
                	ResolveActionInvoc invocation = actiontask.getResolveActionInvoc();
                	
                	String inputOutputDesc = getInputsOrOutputs(invocation);
                	inputOutputDesc = filter(inputOutputDesc);
                	inputOutputDesc = StringEscapeUtils.unescapeHtml(inputOutputDesc);
                	
                	if(uName != null && uNameSpace != null)
                	{                	
	                	sBuffer.append("\n");
	                	sBuffer.append(" {");
	                	sBuffer.append("text:'"+uName+"',");
	                	sBuffer.append(" disabled: !selected, ");
	                	sBuffer.append(" scope: this, "); 
	                	sBuffer.append(" handler: function() ");
	                	sBuffer.append(" { " );
	       
	                	sBuffer.append(" var cell = graph.getSelectionCell(); ");
	                	
	                	sBuffer.append(" var labelDisplayNameBefore = ''; ");
	                	sBuffer.append(" var labelDisplayNameAfter = ''; ");
	            	
	                	sBuffer.append(" if(cell != null) ");
	                	sBuffer.append(" { ");
	            	
			                	sBuffer.append(" var userObject = cell.getValue(); ");
			                	sBuffer.append(" labelDisplayNameBefore = userObject.getAttribute('label'); ");
			                	
			                	sBuffer.append(" userObject.setAttribute('label', '"+ uName +"'); ");
			                	sBuffer.append(" userObject.setAttribute('description', '" + uName +"#" + uNameSpace +"'); ");
			                	sBuffer.append(" userObject.setAttribute('tooltip', '"+ inputOutputDesc +"'); ");
			                	sBuffer.append(" userObject.setAttribute('href', '" + uName +"#" + uNameSpace +"'); ");
			            	
			                	if(!uDesc.equals(""))
			                	{
			                		sBuffer.append(" userObject.setAttribute('detail', '" + uDesc+"'); ");
			                	}
	                	
	                	sBuffer.append(" cell.setValue(userObject); ");
	                	
	                	sBuffer.append(" labelDisplayNameAfter = userObject.getAttribute('label'); ");
	                	
	                	sBuffer.append("if(labelDisplayNameBefore != labelDisplayNameAfter)");
	                	sBuffer.append("{ thisModified = true; updateTitle();}");
	                	
	                	sBuffer.append(" graph.setAutoSizeCells = true; ");
	                	sBuffer.append(" var styleName = cell.getStyle(); ");
	                	
	                	sBuffer.append(" if(styleName.indexOf('image') == 0) ");
	                	sBuffer.append(" { ");
	                		sBuffer.append(" graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_BOTTOM); ");
	                		sBuffer.append(" graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP); ");
	                	sBuffer.append(" } ");
	                	sBuffer.append(" else { ");
	                		sBuffer.append(" graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_MIDDLE); ");
	                		sBuffer.append(" graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE); ");
	                	sBuffer.append(" } ");
	                	
	                	sBuffer.append(" graph.getView().refresh(); ");
	                	sBuffer.append(" } ");
	                	sBuffer.append(" } ");
	                	sBuffer.append(" },");
                	}
                }
                else
                {
                    // create new menu and update currentMap
                	lastIsItem = false;
                	sBuffer.append("{"); 
                	sBuffer.append(" text:'" +name+"\',");   
                	sBuffer.append(" disabled: !selected, "); 
                	sBuffer.append(" handler:function() {}, "); 
                	sBuffer.append(" menu: ");
                	sBuffer.append(" { "); 
                	sBuffer.append(" items: [ ");
                	
                	sBuffer.append("\n");
                	
                	sBuffer.append(getMenuDefinition((Map)obj));
                    
                    if(lastIsItem)
                    {
                    	String lastChar = sBuffer.substring(sBuffer.length()-1, sBuffer.length());
                    	
                    	if(lastChar.equals(","))
                    	{
                    		sBuffer.replace(sBuffer.length()-1, sBuffer.length(), "");
                    	}
                    	
                    	sBuffer.append("]");
                    	sBuffer.append("}");
                    	sBuffer.append("},");
                    }   
                }
            }
        }
        
        return sBuffer.toString();
	} //getMenuDefinition 
	
	
	/**
	 * This method return the inputs or outputs.
	 * 
	 * @param invocation
	 * @return
	 */
	private String getInputsOrOutputs(ResolveActionInvoc invocation)
	{
		JSONObject jsonObject = new JSONObject();
		
		List<String> inputList = new ArrayList();
		List<String> outList = new ArrayList();
		
		if (invocation != null)
        { 
			List<ResolveActionParameter> actionParams = (List<ResolveActionParameter>)invocation.getResolveActionParameters();
            String paramName = null;
            String description = null;
            
            for (ResolveActionParameter actionParam : actionParams)
            {
            	String uType = actionParam.getUType();
            	paramName = actionParam.getUName();
        		description = actionParam.getUDescription();
        		description = filter(description);
        		description = StringEscapeUtils.escapeHtml(description);
            	
            	if(uType == null || uType.equals(HibernateConstantsEnum.INPUT.getTagName()))
            	{
            		inputList.add(paramName + "&" + description);
            	}
            	else if(uType.equals(HibernateConstantsEnum.OUTPUT.getTagName()))
            	{
            		outList.add(paramName + "&" + description);
            	}
            }
        }
		
		jsonObject.put(HibernateConstantsEnum.INPUT_DESC.getTagName(), inputList);
		
		jsonObject.put(HibernateConstantsEnum.OUTPUT_DESC.getTagName(), outList);
		
		String jsonObjectStr = jsonObject.toString();
		jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
		
		return jsonObjectStr;
	} //getInputsOrOutputs
	
	/**
	 * This method filter out special chars.
	 * 
	 * @param source
	 * @return
	 */
	public String filter(String source)
	{
		if(source != null)
		{
			source = source.replace("\n", "\\n");
			source = source.replace("\r", "\\r");
			source = source.replace("'", "\\\'");
			source = source.replace("\"", "\\\"");
		}
		
		return source;
	}
	
	/**
	 * This method build the popup menu string.
	 * 
	 * @return
	 */
	public String buildPopupMenuString()
	{
		File testFile = FileUtils.getFile("C:\\tmp\\popup.txt");
		//File testFile = FileUtils.getFile("C:\\tmp\\testXML.txt");
		String contents = getContents(testFile);
		//String modifiedXML = modifyXML(contents);
	
		return contents;
	} //buildPopupMenuString
	
	/**
	 * This method build XML.
	 * 
	 * @return
	 */
	public String buildXML()
	{
		File testFile = FileUtils.getFile("C:\\tmp\\testJPMC1.txt");
		//File testFile = FileUtils.getFile("C:\\tmp\\testXML.txt");
		String contents = getContents(testFile);
		String modifiedXML = modifyXML(contents);
	
		return modifiedXML;
	} // buildXML
	
	/**
	 *
	 * @param contents
	 * @return
	 */
	public static String modifyXML(String contents)
	{
		
		String startEdgeTag = "<Edge";
		String endEdgeTag = "</Edge>";
		
		StringBuffer edgeStringBuffer = new StringBuffer();
		String edgeString = "";
		String xml = "";
		
		try
		{
			if(contents != null && !contents.equals(""))
			{
				while(contents.contains(startEdgeTag))
				{
					edgeString = contents.substring(contents.indexOf(startEdgeTag), contents.indexOf(endEdgeTag) + endEdgeTag.length());
					edgeStringBuffer.append(edgeString);
					contents = contents.replace(edgeString, "");
				}
				
				int lastIndex = contents.lastIndexOf("</root>");
				
				xml = contents.substring(0, lastIndex) +
					  ((edgeStringBuffer != null)? (edgeStringBuffer.toString()):("")) +
					  contents.substring(lastIndex, contents.length());
				
				xml = xml.replace("xwiki", "resolve/jsp");
				
			}		
		}
		catch(Exception exp)
		{
			Log.log.info(exp.getMessage(), exp);
		}
	
		return xml;
	} //modifyXML
	
	/**
	 * 
	 * @param aFile
	 * @return
	 */
	public String getContents(File aFile) 
	{
		
	    StringBuilder contents = new StringBuilder();
	    
	    try 
	    {
	      BufferedReader input =  new BufferedReader(new FileReader(aFile));
	      try {
	        String line = null; 
	        
	        while (( line = input.readLine()) != null)
	        {
	          contents.append(line); 
	        }
	      }
	      finally 
	      {
	        input.close();
	      }
	    }
	    catch (IOException ex)
	    {
	    	Log.log.info(ex.getMessage(), ex);
	    }
	    
	    return contents.toString();
	} //getContents

}
