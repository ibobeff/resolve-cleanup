/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.Collection;
import java.util.Map;

import com.resolve.persistence.model.ResolveEdge;
import com.resolve.persistence.model.ResolveNode;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.migration.social.NonNeo4jRelationType;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;



public class ServiceGraph
{
    
    public static ResolveNodeVO findNode(String sysId, String compSysId, String compName, NodeType type, String username) throws Exception
    {
        return GraphUtil.findNode(sysId, compSysId, compName, type, username);
    }
    
    public static ResponseDTO<ResolveNodeVO> findNodes(String compName, NodeType type, int start, int limit, String username) throws Exception
    {
        return GraphUtil.findNodes(compName, type, start, limit, username);
    }
    public static ResponseDTO<ResolveNodeVO> findNamespaceNodes(String compName, NodeType type, int start, int limit, String username) throws Exception
    {
        return GraphUtil.findNamespaceNodes(compName, type, start, limit, username);
    }
    
    
    public static ResolveNodeVO persistNode(ResolveNodeVO node, String username) throws Exception
    {
        return GraphUtil.persistNode(node, username);
    }
    
    public static void deleteNode(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        //compType is mandatory
        //will delete the node and its edges too
        GraphUtil.deleteNode(sysId, compSysId, compName, compType, username);
    }
    
    //give me all the nodes that I am following
    public static Collection<ResolveNodeVO> getNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getNodesFollowedBy(sysId, compSysId, compName, compType, username);
    }
    
    //give me all the nodes that are following me
    public static Collection<ResolveNodeVO> getNodesFollowingMe(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getNodesFollowingMe(sysId, compSysId, compName, compType, username);
    }
    
//    public static void follow(String compSysId, String compName, NodeType compType, String followSysId, String followCompName, NodeType followType, Map<String, String> edgeProperties) throws Exception
//    {
//        throw new Exception("Not Implemented Yet");
//    }
    
    public static void follow(String sysId, String compSysId, String compName, NodeType compType, Collection<ResolveNodeVO> followComponents, Map<String, String> edgeProperties, String username) throws Exception
    {
        GraphUtil.follow(sysId, compSysId, compName, compType, followComponents, edgeProperties, username);
        
    }
    
//    public static void unfollow(String compSysId, String compName, NodeType compType, String followSysId, NodeType followCompName, String followType) throws Exception
//    {
//        throw new Exception("Not Implemented Yet");
//    }
    
    public static void unfollow(String sysId, String compSysId, String compName, NodeType compType, Collection<ResolveNodeVO> unfollowComponents, String username) throws Exception
    {
        GraphUtil.unfollow(sysId, compSysId, compName, compType, unfollowComponents, username);
    }

    //TODO
    public static void importGraph(SocialImpexVO socialImpex) throws Throwable
    {
        Log.log.warn("SOCIAL RELATIONSHIP IMPORT IS NOT YET IMPLEMENTED", new Exception("SOCIAL RELATIONSHIP IMPORT IS NOT YET IMPLEMENTED"));
    }
    
    public static void exportGraph(SocialImpexVO socialImpex) throws Throwable
    {
        Log.log.warn("SOCIAL RELATIONSHIP EXPORT IS NOT YET IMPLEMENTED", new Exception("SOCIAL RELATIONSHIP EXPORT IS NOT YET IMPLEMENTED"));
    }
    
    public static void addRelation(String srcSysId, String srcCompSysId, String srcCompName, NodeType srcCompType, Collection<ResolveNodeVO> destComponents, Map<String, String> edgeProperties, String username, String relType) throws Exception
    {
        GraphUtil.addEdge(srcSysId, srcCompSysId, srcCompName, srcCompType, destComponents, edgeProperties, username, relType);
    }
    
    public static void removeRelation(String srcSysId, String srcCompSysId, String srcCompName, NodeType srcCompType, Collection<ResolveNodeVO> destComponents, String username, String relType) throws Exception
    {
        GraphUtil.removeEdge(srcSysId, srcCompSysId, srcCompName, srcCompType, destComponents, username, relType);
    }
    
    //return all group nodes that given node is member of e.g. return forum/process/team user is member of
    public static Collection<ResolveNodeVO> getGroupNodes(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getNodesFollowedBy(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.MEMBER.name());
    }
    
    //return all source member nodes for given destination node group of e.g. user and/or process contains given node as member
    public static Collection<ResolveNodeVO> getMemberNodes(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getNodesFollowingMe(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.MEMBER.name());
    }
    
    //give me all PROCESS and TEAM Type nodes that are following me
    public static Collection<ResolveNodeVO> getProcessAndTeamNodesFollowingMe(String sysId, String username) throws Exception
    {
        return GraphUtil.getProcessAndTeamNodesFollowingMe(sysId, username);
    }
    
    public static ResolveEdge createEdge(String srcSysId, String destSysId, Map<String, String> edgeProperties, String username, String relType, Map<String, ResolveNode> nodesInRelationCache) throws Exception
    {
        return GraphUtil.createEdge(srcSysId, destSysId, edgeProperties, username, relType, nodesInRelationCache);
    }
    
    //return all nodes except Document and ActionTask nodes, followed by given node
    public static Collection<ResolveNodeVO> getNodesFollowedByExceptDocAndAT(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getNodesFollowedByExceptDocAndAT(sysId, compSysId, compName, compType, username);
    }
    
    //return all process nodes, followed by given node 
    public static Collection<ResolveNodeVO> getProcessNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getProcessNodesFollowedBy(sysId, compSysId, compName, compType, username);
    }
    
    //return all Process group nodes that given node is member of e.g. return process(es) user is member of
    public static Collection<ResolveNodeVO> getProcessGroupNodes(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getProcessNodesFollowedBy(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.MEMBER.name());
    }
    
    //return all PROCESS nodes that are following given node (USER)
    public static Collection<ResolveNodeVO> getProcessNodesFollowingMe(String sysId, String username) throws Exception
    {
        return GraphUtil.getProcessNodesFollowingMe(sysId, username);
    }
    
    //return all Document source member nodes for given destination node group (only Process can contain DOcument as MEMBER)
    public static Collection<ResolveNodeVO> getDocumentMemberNodes(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getDocumentNodesFollowingMe(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.MEMBER.name(), 0, 0);
    }
    
    //return all DOCUMENT nodes that are following given node (PROCESS)
    public static Collection<ResolveNodeVO> getDocumentNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, int start, int limit) throws Exception
    {
        return GraphUtil.getDocumentNodesFollowedBy(sysId, compSysId, compName, compType, username, start, limit);
    }
    
    // Check if src node is in FOLLOWER rlationship (directly following) with destination node
    public static boolean isDirectlyFollowedBy(String srcNodeSysId, String destNodeSysId, String username) throws Exception
    {
        return GraphUtil.isSrcInDirectRelationshipToDest(srcNodeSysId, destNodeSysId, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
    //return all Action Task source member nodes for given destination node group (only Process can contain Action Task as MEMBER)
    public static Collection<ResolveNodeVO> getActionTaskMemberNodes(String sysId, String compSysId, String compName, NodeType compType, String username) throws Exception
    {
        return GraphUtil.getActionTaskNodesFollowingMe(sysId, compSysId, compName, compType, username, NonNeo4jRelationType.MEMBER.name(), 0, 0);
    }
    
    //return all ACTIONTASK nodes that are following given node (PROCESS)
    public static Collection<ResolveNodeVO> getActionTaskNodesFollowedBy(String sysId, String compSysId, String compName, NodeType compType, String username, int start, int limit) throws Exception
    {
        return GraphUtil.getActionTaskNodesFollowedBy(sysId, compSysId, compName, compType, username, start, limit);
    }
}
