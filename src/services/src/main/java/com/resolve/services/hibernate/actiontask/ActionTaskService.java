package com.resolve.services.hibernate.actiontask;

import java.security.AccessControlException;

import org.springframework.util.Assert;

import com.resolve.persistence.dao.AccessRightsDAO;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.util.UserUtils;

public enum ActionTaskService {

	INSTANCE;

	private ActionTaskRepository actionTaskRepository = ActionTaskRepository.INSTANCE;
//	private AccessRightsRepository accessRightsRepository = AccessRightsRepository.INSTANCE;

	public String getActionTaskName(String id, String username) throws Exception {
		try {
        HibernateProxy.setCurrentUser(username);
			String name = (String) HibernateProxy.execute(() -> {
				return actionTaskRepository.findActionTaskName(id);
			});
			return name;
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings("incomplete-switch")
	public void validateActionTaskUserPermissions(String docId, String username, RightTypeEnum permission) throws Exception {

		Assert.notNull(permission, "Cannot validate user permissions for missing permission parameter");
		ResolveActionTask actionTask = actionTaskRepository.findOne(docId);
		validateActionTask(actionTask, docId, username);
		createAdminAccessRightsIfMissing(actionTask, username);

		StringBuilder roles = new StringBuilder(actionTask.getAccessRights().getUAdminAccess());
		switch (permission) {
		case view:
			roles.append(",").append(actionTask.getAccessRights().getUReadAccess());
		case edit:
			roles.append(",").append(actionTask.getAccessRights().getUWriteAccess());
		case execute:
			roles.append(",").append(actionTask.getAccessRights().getUExecuteAccess());
		}

		validateActionTaskUserRoles(username, actionTask, roles);
	}

	private void validateActionTask(ResolveActionTask actionTask, String docId, String username) {
		if (actionTask == null) {
			throw new IllegalStateException(String.format("Document not found. ID: '%s', user: '%s'", docId, username));
		} else if (actionTask.getAccessRights() == null) {
			throw new IllegalStateException(
					String.format("Missing access rights for document with ID: '%s'. User: '%s'", docId, username));
		}
	}

	private void createAdminAccessRightsIfMissing(ResolveActionTask actionTask, String username) throws Exception {
		if (actionTask.getAccessRights() != null) {
			return;
		}

		String userAdmin = "admin";
		AccessRights ar = new AccessRights();
		ar.setUResourceType(ResolveActionTask.RESOURCE_TYPE);
		ar.setUResourceName(actionTask.getUFullName());
		ar.setUResourceId(actionTask.getSys_id());
		ar.setUAdminAccess(userAdmin);
		ar.setUExecuteAccess(userAdmin);
		ar.setUReadAccess(userAdmin);
		ar.setUWriteAccess(userAdmin);
		
		HibernateProxy.execute(() -> {
			return HibernateUtil.getDAOFactory().getAccessRightsDAO().save(ar);
		});
		
		actionTask.setAccessRights(ar);
	}

	private void validateActionTaskUserRoles(String username, ResolveActionTask actionTask, StringBuilder roles) {
		if (!UserUtils.hasRole(username, UserUtils.getUserRoles(username), roles.toString())) {
			throw new AccessControlException(
					String.format("Insufficient user permissions for document. User: '%s', document: '%s'", username,
							actionTask.getUFullName()));
		}
	}

}
