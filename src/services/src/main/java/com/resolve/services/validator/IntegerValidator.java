package com.resolve.services.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.resolve.services.constants.annotation.IntegerContraint;
import com.resolve.util.StringUtils;

public class IntegerValidator implements ConstraintValidator<IntegerContraint, String> {

	@Override
	public boolean isValid(String filed, ConstraintValidatorContext arg1) {
		if (StringUtils.isInteger(filed)) {
			return true;
		}

		return false;
	}

}
