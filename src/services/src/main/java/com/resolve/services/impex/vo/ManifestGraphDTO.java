package com.resolve.services.impex.vo;

import java.util.ArrayList;
import java.util.List;

public class ManifestGraphDTO
{
    String name;
    String type;
    String filterModel; // only used during GW export
    String id;
    boolean exported;
    int checksum;
    List<ManifestGraphDTO> children = new ArrayList<>();
    List<String> ids = new ArrayList<>();
    
    public ManifestGraphDTO()
    {
        this.exported = true;
    }
    
    public ManifestGraphDTO(String name, boolean exported, int checksum)
    {
        this.name = name;
        this.exported = exported;
        this.checksum = checksum;
    }
    
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    
    public String getFilterModel() {
        return filterModel;
    }
    public void setFilterModel(String model) {
        this.filterModel = model;
    }

    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public boolean isExported()
    {
        return exported;
    }
    public void setExported(boolean exported)
    {
        this.exported = exported;
    }
    
    public int getChecksum()
    {
        return checksum;
    }
    public void setChecksum(int checksum)
    {
        this.checksum = checksum;
    }
    
    public List<ManifestGraphDTO> getChildren()
    {
        return children;
    }
    public void setChildren(List<ManifestGraphDTO> children)
    {
        this.children = children;
    }

    public List<String> getIds()
    {
        return ids;
    }
    public void setIds(List<String> ids)
    {
        this.ids = ids;
    }
    
}
