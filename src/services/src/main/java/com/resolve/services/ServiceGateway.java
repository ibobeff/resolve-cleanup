/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import static com.resolve.util.Log.log;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.dom4j.Node;
import org.dom4j.tree.DefaultAttribute;
import org.hibernate.ReplicationMode;
import com.resolve.gateway.Gateway;
import com.resolve.gateway.MGateway;
import com.resolve.gateway.dto.GatewayDTO;
import com.resolve.persistence.model.DatabaseConnectionPool;
import com.resolve.persistence.model.EWSAddress;
import com.resolve.persistence.model.EmailAddress;
import com.resolve.persistence.model.ExchangeserverFilter;
import com.resolve.persistence.model.GatewayFilter;
import com.resolve.persistence.model.GatewayHealthCheck;
import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.model.RemedyxForm;
import com.resolve.persistence.model.ResolveRegistration;
import com.resolve.persistence.model.SSHPool;
import com.resolve.persistence.model.TelnetPool;
import com.resolve.persistence.model.XMPPAddress;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MInfo;
import com.resolve.rsbase.MainBase;
import com.resolve.services.hibernate.util.DatabaseGatewayUtil;
import com.resolve.services.hibernate.util.EWSGatewayUtil;
import com.resolve.services.hibernate.util.EmailGatewayUtil;
import com.resolve.services.hibernate.util.ExchangeGatewayUtil;
import com.resolve.services.hibernate.util.GatewayUtil;
import com.resolve.services.hibernate.util.MSGGatewayFilterUtil;
import com.resolve.services.hibernate.util.OrgsUtil;
import com.resolve.services.hibernate.util.PullGatewayFilterUtil;
import com.resolve.services.hibernate.util.PushGatewayFilterUtil;
import com.resolve.services.hibernate.util.RemedyGatewayUtil;
import com.resolve.services.hibernate.util.ResolveRegistrationUtil;
import com.resolve.services.hibernate.util.SSHUtil;
import com.resolve.services.hibernate.util.TelnetUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.util.XMPPGatewayUtil;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayHealthCheckVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.NamePropertyVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.PullGatewayFilterVO;
import com.resolve.services.hibernate.vo.PushGatewayFilterVO;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ServiceGateway {
	public static final String PARAM_MAINBASE_KEY = "MainBase";
	public static final String PARAM_USERNAME_KEY = "username";
	public static final Map<String, String> primaryModelName2FCMName = new HashMap<String, String>();
	
	private static final String MREGISTER_MESSAGE_HANDLER_NAMES_FOR_QNAMES_METHOD = 
																				"MRegister.getQueueName2MsgHandlerNames";
	private static final String MREGISTER_FILTER_COMPANION_MODEL_DETAILS_METHOD = 
																				"MRegister.getFilterCompanionModelDetails";
	
	private static ConcurrentMap<String, String> gatewayName2Type = new ConcurrentHashMap<String, String>();
	private static final String HEALTH_CHECK_RESULT_TIMESTAMP_KEY = "HEALTH_CHECK_RESULT_TIMESTAMP";
	/*
     * HP TO DO All the info below should be coming up from gateway catalog info
     * Remove following once gateway catalog info, its registration and, 
     * catalog info repository is in place.
     */
	private static ConcurrentMap<String, String> qName2MsgHandlerNames = new ConcurrentHashMap<String, String>();
	private static ConcurrentMap<String, Pair<String, Map<String, String>>> 
						fcmName2Details = new ConcurrentHashMap<String, Pair<String, Map<String, String>>>();
		
	
	static {
		primaryModelName2FCMName.put("DatabaseFilter", "DatabaseConnectionPool");
		primaryModelName2FCMName.put("EmailFilter", "EmailAddress");
		primaryModelName2FCMName.put("EWSFilter", "EWSAddress");
		primaryModelName2FCMName.put("RemedyxFilter", "RemedyxForm");
		primaryModelName2FCMName.put("XMPPFilter", "XMPPAddress");
	}
	
	public static Set<String> getAllQueue(String modelName) throws Exception {
		Set<String> result = new TreeSet<String>();

		try {
			result = GatewayUtil.getInstance().getAllQueue(modelName);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static GatewayFilterVO getFilterById(String gatewayName, String id) throws Exception {

		String type = getGatewayType(gatewayName);
		if (StringUtils.isBlank(type) || type.equals("None"))
			throw new Exception("Gateway type is not available.");

		if (type.equals("Push"))
			return PushGatewayFilterUtil.getPushFilterById(id);

		if (type.equals("Pull"))
			return PullGatewayFilterUtil.getPullFilter(id);

		if (type.equals("MSG"))
			return MSGGatewayFilterUtil.getMSGFilter(id);

		return null;
	}

	public static boolean isNewSDKGateway(String modelName) throws Exception {

		boolean isNewSDK = false;

		if (StringUtils.isBlank(modelName))
			throw new Exception("Model name is not available.");

		int index = modelName.indexOf("Filter");
		if (index == -1)
			return false;

		// Push, Pull, MSG filter model names are PushGatewayFilter, PullGatewayFilter , and MSGGatewayFilter
		/*
		 * HP TO DO Too much hard coding, need to use annotations to get types for filter objects or gateway catalog
		 */
		if (modelName.equals("PushGatewayFilter") || modelName.equals("PullGatewayFilter") || 
			modelName.equals("MSGGatewayFilter")) {
			return true;
		}
		
		String gatewayName = modelName.substring(0, index).toLowerCase();

		String type = getGatewayType(gatewayName);

		
		if (StringUtils.isNotBlank(type) && 
			(type.equals("Push") || type.equals("Pull") || type.equals("MSG"))) {
			isNewSDK = true;
		}
		
		return isNewSDK;

	}

	public static String getGatewayType(String gatewayName) {

		String type = gatewayName2Type.get(gatewayName.toLowerCase());

		if (StringUtils.isNotBlank(type))
			return type;

		try {
			type = GatewayUtil.getInstance().getGatewayType(gatewayName);

			if (StringUtils.isNotBlank(type)) {
				if (type.equals("Push") || type.equals("Pull") || type.equals("MSG")) {
					gatewayName2Type.putIfAbsent(gatewayName.toLowerCase(), type);
				} else { 
    				gatewayName2Type.putIfAbsent(gatewayName.toLowerCase(), "None");
			}
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
		return type;
	}

	public static String getSDKGatewayType(String modelName) throws Exception {

		String sdkGatewayType = "";

		try {
			sdkGatewayType = GatewayUtil.getInstance().getSDKGatewayType(modelName);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return sdkGatewayType;
	}

	public static List<GatewayVO> getAllFilter(QueryDTO query, String username) throws Exception {
		return getAllFilter(query, username, false);
	}

	public static List<GatewayVO> getAllFilter(QueryDTO query, String username, boolean secured) throws Exception {
		List<GatewayVO> result = new ArrayList<GatewayVO>();

		try {
			result = GatewayUtil.getInstance().getAllFilter(query, username, secured);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static List<GatewayVO> getAllFilters(String modelName, String queueName, String username, boolean secured)
			throws Exception {
		return GatewayUtil.getInstance().getAllFilters(modelName, queueName, username, secured);
	}

	public static List<GatewayVO> getAllFilters(String modelName, String queueName, String username) throws Exception {
		return GatewayUtil.getInstance().getAllFilters(modelName, queueName, username);
	}

	public static GatewayVO getFilter(String modelName, String id, String username) throws Exception {
		return getFilter(modelName, id, username, false);
	}

	public static GatewayVO getFilter(String modelName, String id, String username, boolean secured) throws Exception {
		GatewayVO result = null;

		try {
			result = GatewayUtil.getInstance().getFilter(modelName, id, username, secured);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static GatewayVO getFilterByName(String modelName, String queueName, String name) throws Exception {
		return getFilterByName(modelName, queueName, name, false);
	}

	public static GatewayVO getFilterByName(String modelName, String queueName, String name, boolean secured)
			throws Exception {
		GatewayVO result = null;

		try {
			result = GatewayUtil.getInstance().getFilterByName(modelName, queueName, name, secured);
			
			/*
			 * If named filter is not found check if (primary) filter model has companion filter model 
			 * and if so get filter from companion model
			 */
			if (result == null && primaryModelName2FCMName.containsKey(modelName) && 
				StringUtils.isNotBlank(primaryModelName2FCMName.get(modelName))) {
				result = GatewayUtil.getInstance().getFilterByName(primaryModelName2FCMName.get(modelName), queueName, name, secured);
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static String getFilterDeployed(String modelName, String gatewayName, String id) throws Exception {
		return GatewayUtil.getInstance().getFilterDeployed(modelName, gatewayName, id);
	}

	public static void setFilters(String modelName, Map<String, String> filters) throws Exception {
		try {
			if (!modelName.equals("PushFilter") && !modelName.equals("PullFilter"))
				GatewayUtil.getInstance().setFilters(modelName, filters);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}

	public static String getModelName(String gatewayName) throws Exception {

		String modelName = null;

		String type = getGatewayType(gatewayName);

		if (type != null) {
			if (type.equals("Push"))
				modelName = "PushGatewayFilter";

			else if (type.equals("Pull"))
				modelName = "PullGatewayFilter";

			else if (type.equals("MSG"))
				modelName = "MSGGatewayFilter";
		}

		return modelName;
	}

	public static boolean deleteFilter(String modelName, String[] ids) throws Exception {
		boolean result = false;

		try {
			result = GatewayUtil.getInstance().deleteFilter(modelName, ids);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static Object saveFilter(String modelName, GatewayVO filterVo, String username) throws Exception {
		Object result = false;

		try {
			result = GatewayUtil.getInstance().saveFilter(modelName, filterVo, username);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static List<GatewayVO> listFilters(String gatewayName, String queueName, Boolean deployed) throws Exception {

		String type = getGatewayType(gatewayName);
		if (StringUtils.isBlank(type) || type.equals("None"))
			throw new Exception("Gateway type is not available.");

		if (type.equals("Push"))
			return PushGatewayFilterUtil.listPushFilters(gatewayName, queueName, deployed);

		if (type.equals("Pull"))
			return PullGatewayFilterUtil.listPullFilters(gatewayName, queueName, deployed);

		if (type.equals("MSG"))
			return MSGGatewayFilterUtil.listMSGFilters(gatewayName, queueName, deployed);

		return null;
	}

	// Database gateway related
	public static void setDatabasePools(Map pools) {
		DatabaseGatewayUtil.getInstance().setDatabasePools(pools);
	}

	public static List<Map<String, String>> getDatabasePools(String queueName) {
		return DatabaseGatewayUtil.getDatabasePools(queueName);
	}

	// Database gateway related
	public static void setEmailAddresses(Map emailAddresses) {
		EmailGatewayUtil.getInstance().setEmailAddresses(emailAddresses);

	}

	public static List<Map<String, String>> getEmailAddresses(String queueName, boolean isSocialPoster) {
		return EmailGatewayUtil.getInstance().getEmailAddresses(queueName, isSocialPoster);
	}

	// EWS gateway related
	public static void setEWSAddresses(Map emailAddresses) {
		EWSGatewayUtil.getInstance().setEWSAddresses(emailAddresses);
	}

	public static List<Map<String, String>> getEWSAddresses(String queueName, boolean isSocialPoster) {
		return EWSGatewayUtil.getInstance().getEWSAddresses(queueName, isSocialPoster);
	}

	// Exchange gateway
	public static void setExchangeGatewayFilters(Map filters) {
		// TODO setFilter is a bit weird for this, until we normalize it this is fine.
		ExchangeGatewayUtil.getInstance().setFilters(ExchangeserverFilter.class.getSimpleName(), filters);
	}

	// Remedyx gateway
	public static void setRemedyForms(Map forms) {
	    RemedyGatewayUtil.getInstance().setRemedyForms(forms);
	}

	public static List<Map<String, String>> getRemedyForms(String queueName) {
		return RemedyGatewayUtil.getRemedyForms(queueName);
	}

	// XMPP gateway
	public static void setMXMPPAddresses(Map addresses) {
		XMPPGatewayUtil.getInstance().setXMPPAddresses(addresses);
	}

	public static List<Map<String, String>> getMXMPPAddresses(String queueName, boolean isSocialPoster) {
		return XMPPGatewayUtil.getInstance().getXMPPAddresses(queueName, isSocialPoster);
	}

	public static void setMSSHPools(Map<String, String> pools) {
		SSHUtil.setMSSHPools(pools);
	}

	public static void setTelnetPools(Map<String, String> pools) {
		TelnetUtil.setTelnetPools(pools);
	}

	public static List<NamePropertyVO> getAllNameProperties(QueryDTO query) throws Exception {
		List<NamePropertyVO> result = new ArrayList<NamePropertyVO>();

		try {
			result = GatewayUtil.getInstance().getAllNameProperties(query);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static void setNameProperties(String modelName, Map<String, Object> nameProperties) throws Exception {
		try {
			GatewayUtil.getInstance().setNameProperties(modelName, nameProperties);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}

	public static boolean deleteNameProperty(String modelName, String[] ids) throws Exception {
		boolean result = false;

		try {
			result = GatewayUtil.getInstance().deleteModel(modelName, ids);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	public static boolean saveNameProperty(String modelName, Object namePropertyVO, String username) throws Exception {
		boolean result = false;

		try {
			result = GatewayUtil.getInstance().saveNameProperty(modelName, namePropertyVO, username);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, String> getCustomaGatewayNames(final String resolveHome) throws Exception {
		Map<String, String> resultMap = new HashMap<String, String>();

		Map<String, Object> customFieldsMap = getCustomSDKFields(null, resolveHome);
		if (customFieldsMap != null) {
			List<Map<String, String>> listOfMaps = (List<Map<String, String>>) customFieldsMap.get("records");
			if (listOfMaps != null) {
				for (Map<String, String> map : listOfMaps) {
					if (map.get("MENUTITLE") != null) {
						resultMap.put(map.get("MENUTITLE"), map.get("IMPLPREFIXES"));
					}
				}
			}
		}

		return resultMap;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getCustomSDKFields(final String gatewayName, final String resolveHome)
			throws Exception {
		Map<String, Object> listOfMaps = new HashMap<String, Object>();
		File configFile = FileUtils.getFile(resolveHome + "tomcat/webapps/resolve/WEB-INF/config.xml");
		XDoc configDoc = new XDoc(configFile);
		String xPath = null;
		if (StringUtils.isBlank(gatewayName)) {
			xPath = "//CUSTOMSDKFIELDS//*";
		} else {
			xPath = "//CUSTOMSDKFIELDS/" + gatewayName.toUpperCase() + "/*";
			Node node = configDoc.getNode(xPath);
			if (node != null) {
				Map<String, String> attribMap = new HashMap<String, String>();
				Iterator<DefaultAttribute> i = node.getParent().attributeIterator();
				while (i.hasNext()) {
					DefaultAttribute da = i.next();
					attribMap.put(da.getName(), da.getValue());
				}

				listOfMaps.put("config", attribMap);
			}

		}

		listOfMaps.put("records", configDoc.getListMapValue(xPath));

		return listOfMaps;
	}

	public static int getDefaultInterval(final String gatewayName, final String resolveHome) throws Exception {
		int interval = 10;

		File configFile = FileUtils.getFile(resolveHome + "tomcat/webapps/resolve/WEB-INF/config.xml");
		XDoc configDoc = new XDoc(configFile);

		interval = configDoc.getIntValue("./DEFAULTINTERVAL/" + gatewayName.toUpperCase() + "/@interval", 10);

		return interval;
	}

	public static String getHttpAuthType(final String gatewayName, final String resolveHome) throws Exception {

		File configFile = FileUtils.getFile(resolveHome + "tomcat/webapps/resolve/WEB-INF/config.xml");
		XDoc configDoc = new XDoc(configFile);
		String authType = configDoc.getStringValue("//FILTERAUTHTYPE/" + gatewayName.toUpperCase() + "/@type", "");

		return authType;
	}

	private static Map<String, PushGatewayFilterVO> convertToPushFilter(Map<String, String> filters) {

		Map<String, PushGatewayFilterVO> pushFilters = new HashMap<String, PushGatewayFilterVO>();

		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			String filterName = it.next();
			String value = "|" + filters.get(filterName) + "|";

			if (filterName.equals("QUEUE") || StringUtils.isBlank(value))
				continue;

			PushGatewayFilterVO filter = new PushGatewayFilterVO();

			String[] attrList = value.split("&");
			int index = -1;

			for (int j = 0; j < attrList.length; j++) {
				index = attrList[j].indexOf("=");
				if (index == -1)
					continue;

				String attrKey = attrList[j].substring(1, index - 1);
				String attrValue = attrList[j].substring(index + 2, attrList[j].length() - 1);

				switch (attrKey) {
				case "ACTIVE":
					try {
						Boolean active = new Boolean(attrValue);
						filter.setUActive(active);
					} catch (Exception e) {
						Log.log.error(e.getMessage(), e);
					}
					break;
				case "DEPLOYED":
					try {
						Boolean deployed = new Boolean(attrValue);
						filter.setUDeployed(deployed);
					} catch (Exception e) {
						Log.log.error(e.getMessage(), e);
					}
					break;
				case "ORDER":
					try {
						Integer order = new Integer(attrValue);
						filter.setUOrder(order);
					} catch (Exception e) {
						Log.log.warn(e.getMessage(), e);
					}
					break;
				case "RUNBOOK":
					filter.setURunbook(attrValue);
					break;
				case "QUEUE":
					filter.setUQueue(attrValue);
					break;
				case "INTERVAL":
					try {
						Integer interval = new Integer(attrValue);
						filter.setUInterval(interval);
					} catch (Exception e) {
						Log.log.error(e.getMessage(), e);
					}
					break;
				case "URI":
					filter.setUUri(attrValue);
					break;
				case "SSL":
					try {
						Boolean ssl = new Boolean(attrValue);
						filter.setUSsl(ssl);
					} catch (Exception e) {
						Log.log.error(e.getMessage(), e);
					}
					break;
				case "SCRIPT":
					filter.setUScript(attrValue);
					break;
				case "EVENT_EVENTID":
					filter.setUEventEventId(attrValue);
					break;
				case "ID":
					filter.setUName(attrValue);
					break;
				case "GATEWAY_NAME":
					filter.setUGatewayName(attrValue);
					break;
				case "PORT":
					try {
						Integer port = new Integer(attrValue);
						filter.setUPort(port);
					} catch (Exception e) {
						Log.log.warn(e.getMessage(), e);
					}
					break;
				case "BLOCKING":
					filter.setUBlocking(attrValue);
					break;
				default:
					break;
				}
			}

			pushFilters.put(filterName, filter);
		}

		return pushFilters;
	}

	private static Map<String, PullGatewayFilterVO> convertToPullFilter(Map<String, String> filters) {

		Map<String, PullGatewayFilterVO> pullFilters = new HashMap<String, PullGatewayFilterVO>();

		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			String filterName = it.next();
			String value = "\\|" + filters.get(filterName);

			if (filterName.equals("QUEUE") || StringUtils.isBlank(value))
				continue;

			PullGatewayFilterVO filter = new PullGatewayFilterVO();

			String[] attrList = value.split("&");
			int index = -1;

			for (int j = 0; j < attrList.length; j++) {
				index = attrList[j].indexOf("=");
				if (index == -1)
					continue;

				String attrKey = attrList[j].substring(1, index - 1);
				String attrValue = attrList[j].substring(index + 2);

				switch (attrKey) {
				case "ACTIVE":
					try {
						Boolean active = new Boolean(attrValue);
						filter.setUActive(active);
					} catch (Exception e) {
						Log.log.error(e.getMessage(), e);
					}
					break;
				case "DEPLOYED":
					try {
						Boolean deployed = new Boolean(attrValue);
						filter.setUDeployed(deployed);
					} catch (Exception e) {
						Log.log.error(e.getMessage(), e);
					}
					break;
				case "RUNBOOK":
					filter.setURunbook(attrValue);
					break;
				case "QUEUE":
					filter.setUQueue(attrValue);
					break;
				case "INTERVAL":
					try {
						Integer interval = new Integer(attrValue);
						filter.setUInterval(interval);
					} catch (Exception e) {
						Log.log.error(e.getMessage(), e);
					}
					break;
				case "QUERY":
					filter.setUQuery(attrValue);
					break;
				case "TIME_RANGE":
					filter.setUTimeRange(attrValue);
					break;
				case "LAST_ID":
					filter.setULastId(attrValue);
					break;
				case "EVENT_EVENTID":
					filter.setUEventEventId(attrValue);
					break;
				case "ID":
					filter.setUName(attrValue);
					break;
				case "GATEWAY_NAME":
					filter.setUGatewayName(attrValue);
					break;
				case "LAST_TIMESTAMP":
					filter.setULastTimestamp(attrValue);
					break;
				default:
					break;
				}
			}

			pullFilters.put(filterName, filter);
		}

		return pullFilters;
	}
	
	@SuppressWarnings("unchecked")
	public static List<GatewayVO> getAllSDK2Filters(String modelName, String gatewayName, 
													String queueName) throws Exception {
		List<GatewayVO> result = Collections.emptyList();
		
		String type = ServiceGateway.getGatewayType(gatewayName);
        
        if(StringUtils.isBlank(type) || type.equals("None"))
            throw new Exception(String.format("Gateway %s type is not SDK2.", gatewayName));
        
        if(type.equals("Push")) {
            result = (List<GatewayVO>) PushGatewayFilterUtil.getPushFiltersForQueue(gatewayName, queueName);
        } else if(type.equals("Pull")) {
            result = (List<GatewayVO>) PullGatewayFilterUtil.getPullFiltersForQueue(gatewayName, queueName);
		} else if(type.equals("MSG")) {
            result = (List<GatewayVO>) MSGGatewayFilterUtil.getMSGFilterForQueue(gatewayName, queueName);
		}
        
        return result;
	}

    public static List<ResolveRegistrationVO> getActiveRSRemoteRegistrations(String username) throws Exception {
        QueryDTO query = new QueryDTO(-1, -1);
        query.setModelName(ResolveRegistration.class.getSimpleName());
        query.setWhereClause("UType='RSREMOTE' and UStatus='ACTIVE'");
        return ResolveRegistrationUtil.getResolveRegistration(query, username);
    }
 
    public static List<GatewayDTO> getGatewaysInfo(String username, String type, boolean activeOnly, 
    											   boolean typeOnly) throws Exception {
    	return ServiceGateway.getGatewaysInfo(username, type, activeOnly, typeOnly, true);
    }
    
    public static List<GatewayDTO> getGatewaysInfo(String username, String type, boolean activeOnly, boolean typeOnly,
    											   boolean latestDeployedFilter) throws Exception {
    	if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("ServiceGateway:getGatewaysInfo(%s,%s,%b,%b,%b) In DBPoolUsage [%s]", username, type,  
										activeOnly, typeOnly, latestDeployedFilter,
										StringUtils.mapToString(
														new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
    	
        List<ResolveRegistrationVO> rsrgVOs = getActiveRSRemoteRegistrations(username);
        List<GatewayDTO> records = com.resolve.services.GatewayUtil.getGatewaysInfo(rsrgVOs, username, type, activeOnly, 
        																			typeOnly, latestDeployedFilter);
        
        if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("ServiceGateway:getGatewaysInfo(%s,%s,%b,%b,%b) Out DBPoolUsage [%s]", username, type,  
										activeOnly, typeOnly, latestDeployedFilter,
										StringUtils.mapToString(
														new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
        
        return records;
    }
    
    @SuppressWarnings({ "static-access", "unchecked" })
	public static String getMessageHandlerNameForQName(MainBase comp, String qName) {
    	String messageHandlerName = null;
    	
    	if (qName2MsgHandlerNames.containsKey(qName.toUpperCase())) {
    		messageHandlerName = qName2MsgHandlerNames.get(qName.toUpperCase());
    	} else {
    		try {
    			long startTime = System.currentTimeMillis();
				Map<String, String> tQName2MsgHandlerNames = 
										comp.esb.call(qName.toUpperCase(), 
													  MREGISTER_MESSAGE_HANDLER_NAMES_FOR_QNAMES_METHOD, 
						  							  new HashMap<String, String>(), 5000);
				
				if (MapUtils.isNotEmpty(tQName2MsgHandlerNames)) {
					if (Log.log.isDebugEnabled()) {
						Log.log.debug(String.format("Queue names to message handler names from %s in %d msec: [%s]", 
													qName.toUpperCase(), (System.currentTimeMillis() - startTime),
													StringUtils.mapToString(tQName2MsgHandlerNames, "=", ", ")));
					}
					
					tQName2MsgHandlerNames.keySet().stream().forEach(rcvdQName -> {
						qName2MsgHandlerNames.putIfAbsent(rcvdQName.toUpperCase(), tQName2MsgHandlerNames.get(rcvdQName));
					});
					
					messageHandlerName = qName2MsgHandlerNames.get(qName.toUpperCase());
				}
			} catch (Exception e) {
				Log.log.error(String.format("Error %sin getting message handler names from queue %s", 
											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
											qName.toUpperCase()));
			}
    	} 
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Message handler name for queue name %s is %s", qName.toUpperCase(), 
    									messageHandlerName));
    	}
    	
    	return messageHandlerName;
    }
    
    private static String getMessageHandlerName(GatewayDTO activeGatewayInfo, MainBase comp) {
    	String msgHandlerName = null;    	
    	String tQName = null;
    	
    	if (StringUtils.isNotBlank(activeGatewayInfo.getGatewayProperties().getUGatewayName())) {
    		tQName = activeGatewayInfo.getGatewayProperties().getUGatewayName().toUpperCase();
        	
        	if (StringUtils.isNotBlank(activeGatewayInfo.getGatewayProperties().getUOrgName()) &&
            	!activeGatewayInfo.getGatewayProperties().getUOrgName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME)) {
        		tQName = (activeGatewayInfo.getGatewayProperties().getUGatewayName() +
        				  activeGatewayInfo.getGatewayProperties().getUOrgName().replaceAll("[^0-9A-Za-z_]", ""))
        				  .toUpperCase();
        	}
        	
        	msgHandlerName = getMessageHandlerNameForQName(comp, tQName);
    	}
    	
    	return msgHandlerName;
    }
    
    @SuppressWarnings({ "unchecked", "static-access" })
	private static Map<String, String> gatewayHealthCheck(GatewayDTO activeGatewayInfo, MainBase comp) {
    	Map<String, String> gatewayHealthReport = new HashMap<String, String>();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	
    	String messageHandlerName = getMessageHandlerName(activeGatewayInfo, comp);
    	
    	if (StringUtils.isBlank(messageHandlerName)) {
    		gatewayHealthReport.put(Gateway.HealthStatusCode.class.getSimpleName(), 
									Gateway.HealthStatusCode.FAILED_TO_CONDUCT.toString());
    		gatewayHealthReport.put(MGateway.HEALTH_CHECK_RESULT_MAP_DETAIL_KEY, 
									String.format("Failed to get message handler name for active gateway info of type " +
												  "%s, queue name %s", activeGatewayInfo.getType(), 
												  activeGatewayInfo.getGatewayProperties().getUGatewayName().toUpperCase()));
    		gatewayHealthReport.put(HEALTH_CHECK_RESULT_TIMESTAMP_KEY, sdf.format(new Date()));
    		return gatewayHealthReport;
    	}
    	
    	String tQName = activeGatewayInfo.getGatewayProperties().getUGatewayName().toUpperCase();
    	
    	if (StringUtils.isNotBlank(activeGatewayInfo.getGatewayProperties().getUOrgName()) && 
    		!activeGatewayInfo.getGatewayProperties().getUOrgName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME)) {
    		tQName = (activeGatewayInfo.getGatewayProperties().getUGatewayName() +
    				  activeGatewayInfo.getGatewayProperties().getUOrgName().replaceAll("[^0-9A-Za-z_]", "")).toUpperCase();
    	}
    	
    	long startTime = System.currentTimeMillis();
    	
    	try {
    		/*
    		 * HP TODO When gateway catalog info repository is available, it should have time (in minutes) gateways
    		 * take to conduct health check.
    		 * 
    		 * Hard coding gateway health check time to 2 seconds for now.
    		 */
    		
			gatewayHealthReport = comp.esb.call(tQName, String.format("%s.healthCheck", messageHandlerName), 
					  							new HashMap<String, Object>(), 30 * 1000);
			
			if (gatewayHealthReport == null) {
				gatewayHealthReport = new HashMap<String, String>();
			}
			
			if (MapUtils.isEmpty(gatewayHealthReport)) {
				gatewayHealthReport.put(Gateway.HealthStatusCode.class.getSimpleName(), 
										Gateway.HealthStatusCode.FAILED_TO_REPORT_IN_TIME.toString());
				gatewayHealthReport.put(MGateway.HEALTH_CHECK_RESULT_MAP_DETAIL_KEY, 
										String.format("Gateway failed to report health check status back in expected " +
													  "%d second(s)", 30));
			}
			
			gatewayHealthReport.put(HEALTH_CHECK_RESULT_TIMESTAMP_KEY, sdf.format(new Date()));
		} catch (Exception e) {
			String errMsg = String.format("Error %sin conducting health check on gateway/queue %s", 
					  					  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
					  					  tQName);    		
			Log.log.error(errMsg, e);
			gatewayHealthReport.put(Gateway.HealthStatusCode.class.getSimpleName(), 
									Gateway.HealthStatusCode.FAILED_TO_CONDUCT.toString());
			gatewayHealthReport.put(MGateway.HEALTH_CHECK_RESULT_MAP_DETAIL_KEY, 
									String.format("Error Message = [%s], Stack Trace = [%s]", errMsg, 
												  ExceptionUtils.getStackTrace(e)));
			gatewayHealthReport.put(HEALTH_CHECK_RESULT_TIMESTAMP_KEY, sdf.format(new Date()));
		}
    	
    	if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("%s gateway/queue healthCheck returned [%s] in %d msec", tQName,
											(MapUtils.isNotEmpty(gatewayHealthReport) ? 
											 StringUtils.mapToString(gatewayHealthReport, "=", ", ") : ""),
											System.currentTimeMillis() - startTime));
    	}
    	
    	return gatewayHealthReport;
    }
    
    public static void conductGatewayHealthChecks(Map<String, Object> params) {
    	Log.log.info(String.format("conductGatewayHealthChecks In DBPoolUsage [%s]", 
				StringUtils.mapToString(
								new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
    	
    	try {
	    	if (MapUtils.isEmpty(params)) {
	    		throw new IllegalArgumentException("Missing required params");
	    	}
	    	
	    	if (!params.containsKey(PARAM_MAINBASE_KEY) || params.get(PARAM_MAINBASE_KEY) == null || 
	    		!(params.get(PARAM_MAINBASE_KEY) instanceof MainBase)) {
	    		throw new IllegalArgumentException(String.format("Missing required key %s or value for %s is null or " +
	    														 "value is not of expected type %s",
	    														 PARAM_MAINBASE_KEY, PARAM_MAINBASE_KEY, 
	    														 MainBase.class.getSimpleName()));
	    	}
	    	
	    	MainBase comp = (MainBase) params.get(PARAM_MAINBASE_KEY);
	    	
	    	if (!params.containsKey(PARAM_USERNAME_KEY) || !(params.get(PARAM_USERNAME_KEY) instanceof String) ||
	    		StringUtils.isBlank((String)params.get(PARAM_USERNAME_KEY))) {
	        		throw new IllegalArgumentException(String.format("Missing required key %s or value is not of " +
	        														 "expected type %s or value for %s is null or blank",
	        														 PARAM_USERNAME_KEY, String.class.getSimpleName(), 
	        														 PARAM_USERNAME_KEY));
	        }
	    	
	    	String username = (String) params.get(PARAM_USERNAME_KEY);
	    	
	    	final ConcurrentMap<GatewayDTO, Map<String, String>> gatewayHealthReports = new ConcurrentHashMap<GatewayDTO, Map<String, String>>();
	    	
	    	long startTime = System.currentTimeMillis();
	    	long tStartTime = startTime;
	    	long tmp;
	    	
	    	// Get gateway info of active gateways
	    	final List<GatewayDTO> activeGatewayInfos = getGatewaysInfo(username, null, true, false, false);
	    	
	    	if (Log.log.isDebugEnabled()) {
	    		tmp = System.currentTimeMillis();
	    		Log.log.debug(String.format("Get info of all active gateways took %d msec and returned %d records [%s]", 
	    									(tmp - tStartTime), 
	    									(CollectionUtils.isNotEmpty(activeGatewayInfos) ? 
	    									 activeGatewayInfos.size() : 0),
	    									(CollectionUtils.isNotEmpty(activeGatewayInfos) ? 
	    									 StringUtils.collectionOfObjectsToString(activeGatewayInfos, ", ") : "")));
	    		tStartTime = tmp;
	    	}
	    	
	    	// Perform health check on active gateways
	    	if (CollectionUtils.isNotEmpty(activeGatewayInfos)) {
	    		activeGatewayInfos.parallelStream().forEach(activeGatewayInfo -> {
	    			gatewayHealthReports.put(activeGatewayInfo, gatewayHealthCheck(activeGatewayInfo, comp));
	    		});
	    		
	    		if (Log.log.isDebugEnabled()) {
	        		tmp = System.currentTimeMillis();
	        		Log.log.debug(String.format("Conducting health check on %d active gateways took %d msec", 
	        									activeGatewayInfos.size(), (tmp - tStartTime)));
	        		tStartTime = tmp;
	        	}
	    	}
	    	
	    	// Persist (currently no history so upsert) gateway health check report
	    	if (MapUtils.isNotEmpty(gatewayHealthReports)) {
	    		
	    		if (Log.log.isDebugEnabled()) {
	    			gatewayHealthReports.keySet().stream().forEach(activeGatewayInfo -> {
					Log.log.debug(String.format("%s, %s, %s, %s  (%d) : [%s]", 
												activeGatewayInfo.getGatewayProperties().getUGatewayName(),
												activeGatewayInfo.getType(), 
												activeGatewayInfo.getGatewayProperties().getUOrgName(),
												activeGatewayInfo.getGatewayProperties().getURSRemoteName(),
												activeGatewayInfo.hashCode(),
												StringUtils.mapToString(gatewayHealthReports.get(activeGatewayInfo), 
																		"=", ", ")));
	    			});
	    		}
	    			
	    		if (Log.log.isDebugEnabled()) {
	    			Log.log.debug(String.format("persistGatewayHealthReports(%d) In DBPoolUsage [%s]", 
	    										gatewayHealthReports.size(),
	    										StringUtils.mapToString(new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
	    		}
	    		
	    		persistGatewayHealthReports(gatewayHealthReports, username);
	    		
	    		if (Log.log.isDebugEnabled()) {
	    			Log.log.debug(String.format("persistGatewayHealthReports(%d) Out DBPoolUsage [%s]", 
	    										gatewayHealthReports.size(),
	    										StringUtils.mapToString(
	    												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
	    		}
	    		
	    		if (Log.log.isDebugEnabled()) {
	        		tmp = System.currentTimeMillis();
	        		Log.log.debug(String.format("Persisting %d health check reports on %d active gateways took %d msec", 
	        									gatewayHealthReports.size(), activeGatewayInfos.size(), (tmp - tStartTime)));
	        	}
	    	}
	    	
	    	if (Log.log.isDebugEnabled()) {
	    		Log.log.debug(String.format("Conducting health check on all active gateways took total %d msec",
	    									(System.currentTimeMillis() - startTime)));
	    	}
    	} catch (Throwable t) {
    		Log.log.error(String.format("Error %sin conducting gateway health check on all active gateways", 
    									(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), t);
    	}
    	
    	Log.log.info(String.format("conductGatewayHealthChecks Out DBPoolUsage [%s]", 
								   StringUtils.mapToString(new MInfo().dbpoolUsage(new HashMap<String, Object>()), 
										   										   "=", ", ")));
    }
    
    private static GatewayHealthCheck setupGatewayHealthCheck(String orgName, String remoteName, String type, 
															  String queueName, String username) {
    	GatewayHealthCheck gatewayHealthCheck = new GatewayHealthCheck();
    	
    	if (StringUtils.isNotBlank(username)) {
    	gatewayHealthCheck.setSysCreatedBy(username);
    	gatewayHealthCheck.setSysUpdatedBy(username);
    	}
    	
    	if (StringUtils.isNotBlank(orgName)  && !orgName.equals(OrgsVO.NONE_ORG_NAME)) {
    		Orgs orgs = OrgsUtil.findOrgModelByName(orgName);
    		
    		if (orgs != null) {
    			gatewayHealthCheck.setSysOrg(orgs.getSys_id());
    		}
    	}
    	
    	gatewayHealthCheck.setOrgName(orgName);
    	gatewayHealthCheck.setRemoteName(remoteName);
    	gatewayHealthCheck.setGtwType(type);
    	gatewayHealthCheck.setQueueName(queueName);
    	
    	return gatewayHealthCheck;
    }
    
    public static GatewayHealthCheck findGatewayHealthCheck(final String orgName, final String remoteName, final String type, 
    														final String queueName, final String username) {
    	GatewayHealthCheck gatewayHealthCheck = null;
    	GatewayHealthCheck eGHC = setupGatewayHealthCheck(orgName, remoteName, type, queueName, null);
    	
    	try {
    		HibernateProxy.setCurrentUser(username);
            gatewayHealthCheck = (GatewayHealthCheck) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
            	return HibernateUtil.getDAOFactory().getGatewayHealthCheckDAO().findFirst(eGHC);
            });
		} catch (Throwable t) {
            log.error(String.format("Error %sin getting Gateway Health Check Report based on query %s", 
            						(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
            						eGHC), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new RuntimeException(t);
        }
    	
    	return gatewayHealthCheck;
    }
    
    public static void persistGatewayHealthReports(Map<GatewayDTO, Map<String, String>> gatewayHealthReports, 
    												String username) {		
    	if (MapUtils.isNotEmpty(gatewayHealthReports)) {
    		gatewayHealthReports.keySet().parallelStream().forEach(activeGatewayInfo -> {
    			if (Log.log.isDebugEnabled()) {
    				Log.log.debug(String.format("Processing Active Gateway Health Check (%d) Org Name: %s, Remote Name: %s, " +
    											"Type: %s, Queue Name: %s", activeGatewayInfo.hashCode(),
    											activeGatewayInfo.getGatewayProperties().getUOrgName(),
												activeGatewayInfo.getGatewayProperties().getURSRemoteName(),
												activeGatewayInfo.getType(),
												activeGatewayInfo.getGatewayProperties().getUGatewayName()));
    											
    			}
    			
    			try {
    				GatewayHealthCheck dbGatewayHealthCheck = 
    										findGatewayHealthCheck(activeGatewayInfo.getGatewayProperties().getUOrgName(),
    														   	   activeGatewayInfo.getGatewayProperties().getURSRemoteName(),
    														   	   activeGatewayInfo.getType(),
    														   	   activeGatewayInfo.getGatewayProperties().getUGatewayName(),
    														   	   username);
    			
    				boolean found = false;
    				
    				if (dbGatewayHealthCheck != null) {
    					found = true;
    					if (Log.log.isDebugEnabled()) {
    						Log.log.debug(String.format("Found existing Gateway Health Check in DB (%d) %s matching Org Name: " +
    													"%s, Remote Name: %s, Type: %s, Queue Name: %s",     													
    													activeGatewayInfo.hashCode(),
    													dbGatewayHealthCheck,
    													activeGatewayInfo.getGatewayProperties().getUOrgName(),
    													activeGatewayInfo.getGatewayProperties().getURSRemoteName(),
    													activeGatewayInfo.getType(),
    													activeGatewayInfo.getGatewayProperties().getUGatewayName()));
    					}
    				
    					dbGatewayHealthCheck.mapReport(gatewayHealthReports.get(activeGatewayInfo));
    					dbGatewayHealthCheck.setSysModCount(Integer.valueOf(dbGatewayHealthCheck.getSysModCount().intValue() + 
    																		1));
    					dbGatewayHealthCheck.setSysUpdatedBy(username);
    					dbGatewayHealthCheck.setSysUpdatedOn(new Date());
    				} else {
    					dbGatewayHealthCheck = setupGatewayHealthCheck(
    												activeGatewayInfo.getGatewayProperties().getUOrgName(),
    												activeGatewayInfo.getGatewayProperties().getURSRemoteName(),
    												activeGatewayInfo.getType(),
    												activeGatewayInfo.getGatewayProperties().getUGatewayName(),
    												username);
    					dbGatewayHealthCheck.setSysModCount(VO.NON_NEGATIVE_INTEGER_DEFAULT);
    					dbGatewayHealthCheck.mapReport(gatewayHealthReports.get(activeGatewayInfo));
    					
    					dbGatewayHealthCheck.setSysCreatedOn(new Date());
    					dbGatewayHealthCheck.setSysUpdatedOn(dbGatewayHealthCheck.getSysCreatedOn());
    				}
    			
    				// Persist entry into DB
    				
    				GatewayHealthCheck newGatewayHealthCheck;
    				GatewayHealthCheck dbGatewayHealthCheckFinal = dbGatewayHealthCheck;
    				boolean foundFinal = found;
    				
    				try {
    			
    					HibernateProxy.setCurrentUser(username);
    					newGatewayHealthCheck = (GatewayHealthCheck) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
    						
    						 GatewayHealthCheck result = dbGatewayHealthCheckFinal;
    						
    						if (!foundFinal) {
        						result = HibernateUtil.getDAOFactory().getGatewayHealthCheckDAO().
        	            								insertOrUpdate(dbGatewayHealthCheckFinal);
        					} else {
    	    					/*
    	    					 * Resolve's upsert Hibernate interceptor overwrites the sysUpdatedOn on updating entity
    	    					 * which messes up the sysUpdatedOn logic used by Gateway Health Check Report to 
    	    					 * indicate consumers of report whether the it is stale or current based on requested time,
    	    					 * hence update the entity by replicating avoiding update interceptor.
    	    					 */
        						HibernateUtil.getCurrentSession().replicate(GatewayHealthCheck.class.getName(), 
        								dbGatewayHealthCheckFinal, ReplicationMode.OVERWRITE);	
        					}
    						
    						return result;
    					});
    				} catch (Throwable t) {
    					newGatewayHealthCheck = null;
    					log.error(String.format("Error %sin saving Gateway Health Check Report %s", 
    											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
    											dbGatewayHealthCheckFinal), t);
                        HibernateUtil.rethrowNestedTransaction(t);
                        throw new RuntimeException(t);
    				}
    			
    				if (Log.log.isDebugEnabled() && newGatewayHealthCheck != null) {
    					Log.log.debug(String.format("Inserted new / Updated existing Gateway Health Check in DB %s for " +
    												"Org Name: %s, Remote Name: %s, Type: %s, Queue Name: %s", 
    												newGatewayHealthCheck,
    												activeGatewayInfo.getGatewayProperties().getUOrgName(),
    												activeGatewayInfo.getGatewayProperties().getURSRemoteName(),
    												activeGatewayInfo.getType(),
    												activeGatewayInfo.getGatewayProperties().getUGatewayName()));
    				}
    			} catch (Throwable t) {
    				Log.log.error(String.format("Error %sin persisting gateway health check report [%s] for " +
    											"Org Name: %s, Remote Name: %s, Type: %s, Queue Name: %s", 
    											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
    											StringUtils.mapToString(gatewayHealthReports.get(activeGatewayInfo),
																		"=", ", "),
    											activeGatewayInfo.getGatewayProperties().getUOrgName(),
												activeGatewayInfo.getGatewayProperties().getURSRemoteName(),
												activeGatewayInfo.getType(),
												activeGatewayInfo.getGatewayProperties().getUGatewayName()), t);
    			}
    		});
    	}
    }
    
    @SuppressWarnings("unchecked")
	public static List<GatewayHealthCheckVO> getGatewayHealthCheckReports(String username, 
    																	  int healthCheckIntrvlInMin) throws Exception {
    	final List<GatewayHealthCheckVO> gtwHealthCheckVOs = new CopyOnWriteArrayList<GatewayHealthCheckVO>();
    	final List<GatewayHealthCheck> fGtwHealthChecks = new ArrayList<GatewayHealthCheck>();
    	List<GatewayHealthCheck> gtwHealthChecks = null;
    	
    	final Collection<String> accessibleOrgs = UserUtils.getUserOrgHierarchyIdList(username);
    	final boolean isNoOrgAccessible = UserUtils.isNoOrgAccessible(username);
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
   	 	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
   	 	final long currentTimeInMillis = System.currentTimeMillis();
   	 	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Get Gateway Health Check Reports - User Name: %s, Health Check Interval " +
    									"(min): %d, Current Time: %s, Accessible Orgs [%s], Noe Org Accessible: %b", 
    									username, healthCheckIntrvlInMin, sdf.format(new Date(currentTimeInMillis)),
    									StringUtils.collectionToString(accessibleOrgs, ", "), isNoOrgAccessible));
    	}
    	
    	try {
    		HibernateProxy.setCurrentUser(username);
            gtwHealthChecks = (List<GatewayHealthCheck>) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
            	return HibernateUtil.getDAOFactory().getGatewayHealthCheckDAO().findAll();
            });
		} catch (Throwable t) {
            log.error(String.format("Error %sin getting Gateway Health Check Reports", 
            						(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new Exception(t);
        }
    	
    	fGtwHealthChecks.parallelStream().filter(gtwHCR -> {
	    		if (StringUtils.isNotBlank(gtwHCR.getSysOrg()) && accessibleOrgs.contains(gtwHCR.getSysOrg())) {
	    			return true;
	    		}
	    		
	    		if (StringUtils.isBlank(gtwHCR.getSysOrg()) && isNoOrgAccessible) {
	    			return true;
	    		}
	    		
	    		return false;
    	}).forEach(gtwHCR -> {
    		if (gtwHCR != null && StringUtils.isNotBlank(gtwHCR.getSys_id())) {
    			GatewayHealthCheckVO gatewayHealthCheckVO = gtwHCR.doGetVO();
	    	
    			if (gatewayHealthCheckVO != null && StringUtils.isNotBlank(gatewayHealthCheckVO.getSys_id())) {
		    		gatewayHealthCheckVO.markStale(healthCheckIntrvlInMin, currentTimeInMillis);
		    		gtwHealthCheckVOs.add(gatewayHealthCheckVO);
    			} else {
    				Log.log.error(String.format("Null %s or %s without sys_id converted to VO from %s", 
							GatewayHealthCheckVO.class.getSimpleName(),
							GatewayHealthCheckVO.class.getSimpleName(), gtwHCR));
	    	}
    		} else {
    			Log.log.error(String.format("Received null %s or %s without sys_id from db", 
    										GatewayHealthCheck.class.getSimpleName(),
    										GatewayHealthCheck.class.getSimpleName()));
    	}
    	});
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Returning %d Gateway Health Check Reports: {%s}", 
    									gtwHealthCheckVOs.size(), 
    									StringUtils.collectionOfObjectsToString(gtwHealthCheckVOs, ", ")));
    	}
    	    	
    	return gtwHealthCheckVOs;
    }
    
    public static Map<String, Object> getFilterDeployStatus(List<String> filters, List<String> queueNames, String gatewayType, String username) {
        Map<String, Object> statusResultMap = new HashMap<>();
        
        String type = getGatewayType(gatewayType);
        if (StringUtils.isNotBlank(type)) {
            String filterModelClass = null;
            switch (type.toLowerCase()) {
                case "none" : { // legacy gateway
                    filterModelClass = HibernateUtil.getGatewayModelFilter(gatewayType);
                    statusResultMap = checkDeploymentStatus(filterModelClass, filters, queueNames, username);
                    break;
                } case "push" : {
                    statusResultMap = checkDeploymentStatus("PushGatewayFilter", filters, queueNames, username);
                    break;
                } case "pull" : {
                    statusResultMap = checkDeploymentStatus("PullGatewayFilter", filters, queueNames, username);
                    break;
                } case "msg" : {
                    statusResultMap = checkDeploymentStatus("MSGGatewayFilter", filters, queueNames, username);
                    break;
                } default : {
                    // hmmm... something is not right
                    Log.log.error(String.format("Error: Unrecognized gateway type: %s", gatewayType));
                }
            }
        } else {
            statusResultMap.put("Error", String.format("Unrecognized gateway type: %s", gatewayType));
        }
        return statusResultMap;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static Map<String, Object> checkDeploymentStatus(String filterModelClass, List<String> filters, List<String> queueNames, String username) {
        Map<String, Object> statusResultMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(filters) && CollectionUtils.isNotEmpty(queueNames)) {
            queueNames.parallelStream().forEach(queueName -> {
                // prepare specified filters for sql 'IN' criteria
                List<String> localFilters = filters.stream().map(filter -> "'" + filter + "'").collect(Collectors.toList());
                String filterString = StringUtils.join(localFilters, ",").toLowerCase();
                StringBuilder hql = new StringBuilder("from ");
                hql.append(filterModelClass).append (" where lower (UName) in (").append(filterString).append(") and lower (UQueue) = :queueName");
                List<Object> result = null;
                try {
                      HibernateProxy.setCurrentUser(username);
                	result = (List<Object>) HibernateProxy.execute(() -> {
                		return HibernateUtil.createHQLQuery(hql.toString()).setParameter("queueName", queueName.toLowerCase()).list();
                	});
                } catch (Throwable t) {
                    Log.log.error("Error", t);
                                                            HibernateUtil.rethrowNestedTransaction(t);
                }
                // collect all the filters deployed for a specified queue
                Map<String, Object> filterQueueDeployStatus = new HashMap<>();
                if (CollectionUtils.isNotEmpty(result)) {
                    result.stream().map(object -> ((GatewayFilter)object).getUName()).forEach(filterName -> {
                        if (filters.contains(filterName)) { // check whether we need to get status of this filter
                            filterQueueDeployStatus.put(filterName, true); // this filter is deployed.
                        }
                    });
                    /*
                     * We have collected all deployed filters. Now check which filters are not deployed yet
                     * by iterating through the original filters list. If filter name is not found in filterQueueDeployStatus,
                     * it's not deployed, so add it to filterQueueDeployStatus as 'false'.
                     */
                    filters.stream().forEach(filter -> {
                        if (!filterQueueDeployStatus.containsKey(filter)) {
                            filterQueueDeployStatus.put(filter, false);
                        }
                    });
                    // for the specified queue, bundle the status of all filters.
                    statusResultMap.put(queueName, filterQueueDeployStatus);
                } else {
                    // hmmm... didn't find any record(s) with this queue.
                    filters.stream().forEach(filter -> { // mark all the filters as not deployed for that queue
                        filterQueueDeployStatus.put(filter, false);
                    });
                    statusResultMap.put(queueName, filterQueueDeployStatus);
                }
            });
        }
        return statusResultMap;
    }
    
    @SuppressWarnings({ "static-access", "unchecked" })
	public static Pair<String, Map<String, String>> getFilterCompanionModelDetails(MainBase comp, 
																				   String fcmName, 
																				   String qName) {
    	Pair<String, Map<String, String>> fcmDetail = null;
    	
    	if (fcmName2Details.containsKey(fcmName)) {
    		fcmDetail = fcmName2Details.get(fcmName);
    	} else {
    		try {
    			long startTime = System.currentTimeMillis();
				Map<String, Pair<String, Map<String, String>>> 
								fcmDetails = comp.esb.call(qName.toUpperCase(), 
														   MREGISTER_FILTER_COMPANION_MODEL_DETAILS_METHOD, 
						  								   new HashMap<String, String>(), 5000);
				
				if (MapUtils.isNotEmpty(fcmDetails)) {
					Log.log.debug(String.format("Received %d Filter Companion Models Details for model [%s] in %d msec " +
												"form queue %s", fcmDetails.size(), 
												StringUtils.setToString(fcmDetails.keySet(), ", "),
												(System.currentTimeMillis() - startTime), qName.toUpperCase()));
					
					fcmDetails.keySet().forEach(fltCompModelName -> {
						Log.log.debug(String.format("Filter Companion Model %s -> (Main Model %s, Method Names [%s])",
													fltCompModelName, fcmDetails.get(fltCompModelName).getLeft(),
													StringUtils.mapToString(fcmDetails.get(fltCompModelName).getRight(), 
																			"=", ", ")));
					});
					
					fcmDetails.keySet().parallelStream().forEach(rcvdFCMModelName -> {
						fcmName2Details.putIfAbsent(rcvdFCMModelName, fcmDetails.get(rcvdFCMModelName));
					});
					
					fcmDetail = fcmName2Details.get(fcmName);
				}
			} catch (Exception e) {
				Log.log.error(String.format("Error %sin getting filter companion model details from queue %s", 
											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
											qName.toUpperCase()));
			}
    	} 
    	
    	if (fcmDetail != null && Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Filter Companion Model Details For %s -> (Main Model %s, Method Names [%s])",
    									fcmName, fcmDetail.getLeft(), 
    									StringUtils.mapToString(fcmDetail.getRight(), "=", ", ")));
    	}
    	return fcmDetail;
    }

    public static List<Map<String, String>> getGatewayListForExport (QueryDTO query, String username) throws Exception {
        List<Map<String, String>> result = new ArrayList<>();
        List<GatewayDTO> gateways = getGatewaysInfo("admin", null, false, true);
        String sort = CollectionUtils.isEmpty(query.getSortItems()) ? null :
            query.getSortItems().get(0).getDirection().name();
        String search = CollectionUtils.isEmpty(query.getFilters()) ? null :
            query.getFilters().get(0).getValue();

        if (CollectionUtils.isNotEmpty(gateways)) {
            Stream<GatewayDTO> stream = gateways.stream();
            if (StringUtils.isBlank(sort) || sort.equals("ASC"))
                stream = stream.sorted((dto1, dto2) -> dto1.getType().compareTo(dto2.getType()));
            else
                stream = stream.sorted((dto2, dto1) -> dto1.getType().compareTo(dto2.getType()));
            if (StringUtils.isNotBlank(search))
                stream = stream.filter(dto -> dto.getType().toLowerCase().startsWith(search.toLowerCase()));
            stream.forEach(dto -> {
                Map<String, String> gatewayMap = new HashMap<>();
                String gatewayType = dto.getType();
                String model = null;
                try {
                    model = getModelName(gatewayType);
                    if (StringUtils.isBlank(model))
                        model = HibernateUtil.getGatewayModelFilter(gatewayType);
                    if (model.contains(".")) {
                        model = model.replace("com.resolve.persistence.model.", "");
                    }
                }
                catch (Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
                gatewayMap.put("id", model);
                gatewayMap.put("sys_id", gatewayType);
                gatewayMap.put("subType", gatewayType);
                result.add(gatewayMap);
            });
        }
        return result;
    }

    public static Map<String, Object> getFiltersToExport(String modelName, String type, String username) throws Exception {
        List<String> list = new ArrayList<>();
        List<GatewayVO> filters = null;
        String gatewayType = getGatewayType(type);
        if (StringUtils.isBlank(gatewayType) || gatewayType.equalsIgnoreCase("none")) {
            if (type.equals(ImpexEnum.SSH.getValue())) {
                List<SSHPool> sshPools = getAllSSHPoolFilters();
                if (CollectionUtils.isNotEmpty(sshPools)) {
                    list = sshPools.stream()
                                    .sorted((p1, p2) -> p1.getUSubnetMask().compareTo(p2.getUSubnetMask())) // sort it in ascending order by subnetmask
                                    .map(pool -> String.format("%s:%s:%s", pool.getUSubnetMask(), pool.getSys_id(), modelName))
                                    .collect(Collectors.toList());
                }
            } else if (type.equals(ImpexEnum.TELNET.getValue())) {
                List<TelnetPool> telnetPools = getAllTelnetPoolFilters();
                if (CollectionUtils.isNotEmpty(telnetPools)) {
                    list = telnetPools.stream()
                                    .sorted((p1, p2) -> p1.getUSubnetMask().compareTo(p2.getUSubnetMask())) // sort it in ascending order by subnetmask
                                    .map(pool -> String.format("%s:%s:%s", pool.getUSubnetMask(), pool.getSys_id(), modelName))
                                    .collect(Collectors.toList());
                }
            } else {
                filters = getAllFilters(modelName, ImpexEnum.default_queue.getValue(), username);
            }
        }
        else
            filters = getAllSDK2Filters(modelName, type, ImpexEnum.default_queue.getValue());
        if (!type.equals(ImpexEnum.SSH.getValue())
                        && !type.equals(ImpexEnum.TELNET.getValue())
                        && CollectionUtils.isNotEmpty(filters)) {
            list = filters.stream()
                            .map(filter -> (GatewayFilterVO)filter) // cast the returned GatewayVO to GatewayFilterVO
                            .sorted((f1, f2) -> f1.getUName().compareTo(f2.getUName())) // Sort it in ascending order by name
                            .map(filterVO -> String.format("%s:%s:%s", filterVO.getUName(), filterVO.getSys_id(), modelName))
                            .collect(Collectors.toList());
        }
        Map<String, Object> result = new HashMap<>();
        List<String> companionList = getCompanionList(modelName);
        result.put(ImpexEnum.filters.getValue(), list);
        if (CollectionUtils.isNotEmpty(companionList)) {
            result.put(ImpexEnum.companion.getValue(), companionList);
        }
        return result;
    }

    public static Map<String, Object> getFilterDetails(String modelName, String sysId, String username) throws Exception {
        Map<String, Object> result = new HashMap<>();
        if (StringUtils.isNotBlank(modelName)
                        && !modelName.equals(ImpexEnum.SSHPool.getValue())
                        && !modelName.equals(ImpexEnum.TelnetPool.getValue()))
        {
            GatewayFilterVO filterVO = (GatewayFilterVO)getFilter(modelName, sysId, username);
            result.put(ImpexEnum.RUNBOOK.getValue(), StringUtils.isBlank(filterVO.getURunbook())? Arrays.asList() : 
                    Arrays.asList(String.format("%s:%s", filterVO.getURunbook(), WikiUtils.getWikidocSysId(filterVO.getURunbook()))));
        }
        return result;
    }

    public static List<DatabaseConnectionPool> getAllDBPools() {
        List<DatabaseConnectionPool> poolList = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                DatabaseConnectionPool pool = new DatabaseConnectionPool();
                pool.setUQueue(ImpexEnum.default_queue.getValue());
                poolList.addAll(HibernateUtil.getDAOFactory().getDatabaseConnectionPoolDAO().find(pool));
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return poolList;
    }

    public static List<EmailAddress> getAllEmailAddresses() {
        List<EmailAddress> addressList = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                EmailAddress address = new EmailAddress();
                address.setUQueue(ImpexEnum.default_queue.getValue());
                addressList.addAll(HibernateUtil.getDAOFactory().getEmailAddressDAO().find(address));
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return addressList;
    }

    public static List<EWSAddress> getAllEWSAddresses() {
        List<EWSAddress> addressList = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                EWSAddress address = new EWSAddress();
                address.setUQueue(ImpexEnum.default_queue.getValue());
                addressList.addAll(HibernateUtil.getDAOFactory().getEWSAddressDAO().find(address));
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return addressList;
    }

    public static List<RemedyxForm> getAllRemedyxForms() {
        List<RemedyxForm> forms = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                RemedyxForm form = new RemedyxForm();
                form.setUQueue(ImpexEnum.default_queue.getValue());
                forms.addAll(HibernateUtil.getDAOFactory().getRemedyFormDAO().find(form));
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return forms;
    }

    public static List<XMPPAddress> getAllXMPPAddresses() {
        List<XMPPAddress> addressList = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                XMPPAddress address = new XMPPAddress();
                address.setUQueue(ImpexEnum.default_queue.getValue());
                addressList.addAll(HibernateUtil.getDAOFactory().getXMPPAddressDAO().find(address));
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return addressList;
    }

    public static List<SSHPool> getAllSSHPoolFilters() {
        List<SSHPool> pools = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                SSHPool pool = new SSHPool();
                pool.setUQueue(ImpexEnum.default_queue.getValue());
                pools.addAll(HibernateUtil.getDAOFactory().getSSHConnectionPoolDAO().find(pool));
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return pools;
    }
    
    public static List<TelnetPool> getAllTelnetPoolFilters() {
        List<TelnetPool> pools = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                TelnetPool pool = new TelnetPool();
                pool.setUQueue(ImpexEnum.default_queue.getValue());
                pools.addAll(HibernateUtil.getDAOFactory().getTelnetConnectionPoolDAO().find(pool));
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return pools;
    }

    private static List<String> getCompanionList(String modelName) {
        List<String> list = new ArrayList<>();
        String companionModel = ServiceGateway.primaryModelName2FCMName.get(modelName);
        if (EnumUtils.isValidEnum(ImpexEnum.class, companionModel)) {
            ImpexEnum modelEnum = ImpexEnum.valueOf(companionModel);
            switch (modelEnum) {
                case DatabaseConnectionPool : {
                    List<DatabaseConnectionPool> poolList = getAllDBPools();
                    if (CollectionUtils.isNotEmpty(poolList)) {
                        list = poolList.stream()
                                .sorted((p1, p2) -> p1.getUPoolName().compareTo(p2.getUPoolName()))
                                .map(pool -> String.format("%s:%s", pool.getUPoolName(), pool.getSys_id()))
                                .collect(Collectors.toList());
                    }
                    break;
                }
                case EmailAddress : {
                    List<EmailAddress> addresses = getAllEmailAddresses();
                    if (CollectionUtils.isNotEmpty(addresses)) {
                        list = addresses.stream()
                                        .sorted((a1, a2) -> a1.getUEmailAddress().compareTo(a2.getUEmailAddress()))
                                        .map(address -> String.format("%s:%s", address.getUEmailAddress(), address.getSys_id()))
                                        .collect(Collectors.toList());
                    }
                    break;
                }
                case EWSAddress : {
                    List<EWSAddress> addresses = getAllEWSAddresses();
                    if (CollectionUtils.isNotEmpty(addresses)) {
                        list = addresses.stream()
                                        .sorted((a1, a2) -> a1.getUEWSAddress().compareTo(a2.getUEWSAddress()))
                                        .map(address -> String.format("%s:%s", address.getUEWSAddress(), address.getSys_id()))
                                        .collect(Collectors.toList());
                    }
                    break;
                }
                case RemedyxForm : {
                    List<RemedyxForm> forms = getAllRemedyxForms();
                    if (CollectionUtils.isNotEmpty(forms)) {
                        list = forms.stream()
                                        .sorted((r1, r2) -> r1.getUName().compareTo(r2.getUName()))
                                        .map(form -> String.format("%s:%s", form.getUName(), form.getSys_id()))
                                        .collect(Collectors.toList());
                    }
                    break;
                }
                case XMPPAddress : {
                    List<XMPPAddress> addresses = getAllXMPPAddresses();
                    if (CollectionUtils.isNotEmpty(addresses)) {
                        list = addresses.stream()
                                        .sorted((a1, a2) -> a1.getUXMPPAddress().compareTo(a2.getUXMPPAddress()))
                                        .map(address -> String.format("%s:%s", address.getUXMPPAddress(), address.getSys_id()))
                                        .collect(Collectors.toList());
                    }
                    break;
                }
                default : {
                    // Do nothing here
                    break;
                }
            }
        }
        return list;
    }
    
    @SuppressWarnings("unchecked")
    public static Integer getFilterChecksum(String name, String filterModel, String sysId) {
        Integer checksum = null;
        List<Object> objectList = new ArrayList<>();
        String hql = String.format("from %s where sys_id = :id", filterModel);
        try {
            HibernateUtil.action(util -> {
                objectList.addAll(HibernateUtil.getCurrentSession().createQuery(hql).setParameter("id", sysId).list());
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        if (CollectionUtils.isNotEmpty(objectList)) {
            Object filterObject = objectList.get(0);
            try {
                checksum = (Integer)PropertyUtils.getSimpleProperty(filterObject, "checksum");
            }
            catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        return checksum;
    }

    /**
     * API to be invoked from update script ONLY.
     */
    public static void updateGatewayFilterChecksum () {
        /*
         * List of all legacy (along with companion), SDK1 and SDK2 gateway filters.
         */
        List<String> filterModels = Arrays.asList("SSHPool", "TelnetPool", "XMPPFilter", "XMPPAddress", "HTTPFilter", "AmqpFilter", "EWSFilter", "EWSAddress", "ArcSightFilter",
                        "SNMPFilter", "EmailFilter", "EmailAddress", "NetcoolFilter", "HPSMFilter", "TSRMFilter", "SalesforceFilter", "RemedyxFilter", "RemedyxForm",
                        "CASpectrumFilter", "HPOMFilter", "HPOMXFilter", "TIBCOBespokeFilter", "ExchangeserverFilter", "TCPFilter", "PullGatewayFilter",
                        "PushGatewayFilter", "MSGGatewayFilter","ServiceNowFilter", "DatabaseFilter", "DatabaseConnectionPool", "MoogSoftFilter", "IBMmqFilter",
                        "ExchangeFilter", "GroovyFilter", "JIRAFilter", "JIRASERVICEDESKFilter", "KafkaFilter", "McAfeeFilter", "MonolithFilter", "QRadarFilter",
                        "QRadarPullFilter", "RADIUSFilter", "SCOMFilter", "ServiceDeskFilter", "SolarWindsFilter", "SplunkFilter", "SplunkPullFilter", "WeblogicJMSFilter");
        filterModels.parallelStream().forEach(model -> {
            try {
                UpdateChecksumUtil.updateComponentChecksum(model, com.resolve.services.ServiceGateway.class);
            }
            catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        });
    }
} // class ServiceGateway
