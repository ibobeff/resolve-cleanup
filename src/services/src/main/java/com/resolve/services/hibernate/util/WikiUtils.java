/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.File;
import java.io.IOException;
import java.security.AccessControlException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.util.StringUtil;
import org.hibernate.query.Query;
import org.hibernate.type.IntegerType;
import org.hibernate.type.TimestampType;

import com.resolve.persistence.dao.MetaFormViewDAO;
import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.dao.WikidocQualityRatingDAO;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.MainWikidocActiontaskRel;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.PlaybookActivities;
import com.resolve.persistence.model.ResolveCatalogNodeWikidocRel;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.RoleWikidocHomepageRel;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.WikiArchive;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.model.WikidocQualityRating;
import com.resolve.persistence.model.WikidocResolutionRating;
import com.resolve.persistence.model.WikidocResolveTagRel;
import com.resolve.persistence.model.WikidocStatistics;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.search.wiki.WikiIndexAPI;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceSocial;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.constants.WikiDocStatsCounterName;
import com.resolve.services.exception.MissingVersionException;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.hibernate.customtable.CustomFormUtil;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveNamespaceVO;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocStatisticsVO;
import com.resolve.services.hibernate.wiki.ArchiveWiki;
import com.resolve.services.hibernate.wiki.CleanBadRefsFromWiki;
import com.resolve.services.hibernate.wiki.CopyHelper;
import com.resolve.services.hibernate.wiki.DocUtils;
import com.resolve.services.hibernate.wiki.MoveRenameHelper;
import com.resolve.services.hibernate.wiki.NamespaceUtil;
import com.resolve.services.hibernate.wiki.ReplaceHelper;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.hibernate.wiki.SearchWiki;
import com.resolve.services.hibernate.wiki.WikiDocLock;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.VO;
import com.resolve.services.rb.util.ABConstants;
import com.resolve.services.rb.util.ResolutionBuilderUtils;
import com.resolve.services.util.DateManipulator;
import com.resolve.services.util.DocxAPI;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.ChartRatingDTO;
import com.resolve.services.vo.FeedbackDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.RatingDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.WikiDocumentDTO;
import com.resolve.services.vo.WikidocRatingDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.DateUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;

import net.sf.json.JSONObject;

public class WikiUtils {

	private static final String REGEX_DOT_DELIMITER = "\\.";
	private final static String INVALID_WIKI_DOC_NAME_MSG = "Invalid Wiki Document Name.";
	private final static String WIKI_DOC_NAME_SPL_CHAR_MSG = "Special character's are not allowed in wiki document name except '-', '_' and '.'.";

	public static WikiDocumentVO getWikiDoc(String sysId, String fullname, String username) {
		WikiDocumentVO result = null;
		WikiDocument doc = getWikiDocumentModel(sysId, fullname, username, false);

		if (doc != null) {
			result = doc.doGetVO();
		}

		return result;
	}// getWikiDoc

	public static WikiDocumentVO getWikiDocWithGraphRelatedDataNoLock(String sysId, String fullname, String username,
			String version) throws Exception {
		WikiDocumentVO result = null;

		WikiDocument doc = getWikiDocModelWithGraphRelatedData(sysId, fullname, username);

		if (doc != null) {
			result = doc.doGetVO();
		}

		if (version != null) {
			result = getWikiVersion(result, doc.getSys_id(), version);
		}

		return result;
	}

	public static WikiDocumentVO getWikiDocWithGraphRelatedDataNoLock(String sysId, String fullname, String username) {
		try {
			return getWikiDocWithGraphRelatedDataNoLock(sysId, fullname, username, null);
			// should not be typically thrown as no version is requested
		} catch (Exception e) {
			Log.log.warn(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public static WikiDocumentVO getWikiDocWithGraphRelatedData(String sysId, String fullname, String username,
			String version) throws Exception {
		WikiDocumentVO result = getWikiDocWithGraphRelatedDataNoLock(sysId, fullname, username, version);

		if (result != null) {
			// Check if there is a UI lock
			WikiDocLock lock = DocUtils.getWikiDocLockNonBlocking(result.getUFullname());
			if (lock != null) {
				result.setSoftLock(true);
				result.setLockedByUsername(lock.getUsername());
			} else {
				result.setSoftLock(false);
				result.setLockedByUsername(null);
			}
		}

		return result;
	}

	public static WikiDocumentVO getWikiDocWithGraphRelatedData(String sysId, String fullname, String username) {
		try {
			return getWikiDocWithGraphRelatedData(sysId, fullname, username, null);
		} catch (Exception e) {
			Log.log.warn(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	} // getWikiDoc

	public static WikiDocument getWikiDocModelWithGraphRelatedData(String sysId, String fullname, String username) {
		long t1 = System.currentTimeMillis();
		WikiDocument doc = getWikiDocumentModel(sysId, fullname, username, false);
		Log.log.debug("getWikiDocumentModel(" + (sysId != null ? sysId : "null") + ", "
				+ (fullname != null ? fullname : "null") + ", " + username + ", " + "false) took "
				+ (System.currentTimeMillis() - t1) + "msec");

		if (doc != null) {
			// for tags, catalog
			t1 = System.currentTimeMillis();
			populateGraphRelatedDataForDocument(doc, username);
			Log.log.debug("populateGraphRelatedDataForDocument(" + doc.getUFullname() + ", " + username + ") took "
					+ (System.currentTimeMillis() - t1) + "msec");
		}

		return doc;
	}// getWikiDoc
	   
	public static WikiDocumentVO getDefaultPropertiesForNewWiki(String fullname, String username) {
		if (StringUtils.isBlank(fullname) || fullname.indexOf('.') <= 0) {
			throw new RuntimeException("Fullname is mandatory and must be in '<Namespace>.<Doc name>' format.");
		}

		WikiDocumentVO result = new WikiDocumentVO();
		String[] arr = fullname.split(REGEX_DOT_DELIMITER);

		result.setUNamespace(arr[0]);
		result.setUName(arr[1]);
		result.setUFullname(fullname);

		// content
		String content = GenericSectionUtil.validateSectionStr("", null);
		result.setUContent(content);

		// roles
		result.setUIsDefaultRole(true);

		// add access rights to doc
		AccessRightsVO rights = getDefaultRoles(arr[0]);
		rights.setUResourceName(fullname);
		rights.setUResourceType(WikiDocument.RESOURCE_TYPE);
		result.setAccessRights(rights);

		return result;
	}

	/**
	 * specifically for catalog viewer
	 * 
	 * @param comps {@link Set} of {@link RSComponent}
	 * @return List of {@link WikiDocumentDTO}
	 * @throws Exception
	 */
	public static List<WikiDocumentDTO> getWikiDocumentDTOForDocumentComps(Set<RSComponent> comps) throws Exception {
		List<WikiDocumentDTO> result = new ArrayList<WikiDocumentDTO>();
		
		try {

        HibernateProxy.setCurrentUser("system");
			HibernateProxy.executeNoCache(() -> {
				
				WikiDocument doc = null;
				
				for (RSComponent comp : comps) {
					String type = comp.getType().name();
					if (type.equalsIgnoreCase(NodeType.DOCUMENT.name())) {
						doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(comp.getSys_id());
						if (doc != null && doc.ugetUIsActive() && !doc.ugetUIsHidden() && !doc.ugetUIsLocked()
								&& !doc.ugetUIsDeleted()) {
							WikiDocumentDTO dto = new WikiDocumentDTO();
							dto.setId(doc.getSys_id());
							dto.setUName(doc.getUName());
							dto.setUFullname(doc.getUFullname());
							dto.setUTitle(doc.getUTitle());
							dto.setUSummary(doc.getUSummary());
							dto.setUContent(doc.getUContent());

							for (WikidocAttachmentRel attachRel : doc.getWikidocAttachmentRels()) {
								WikiAttachment attach = attachRel.getWikiAttachment();
								if (attach.getUFilename().contains("logo")) {
									dto.getAttachments().add(attach.getSys_id());
								}
							}

							result.add(dto);

						} // end of if

					}
				}

			});

		} catch (Throwable t) {
			Log.log.info("Unable to get the wikidocs for catalog viewer", t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		return result;
	}

	public static WikiDocumentVO saveWikiDoc(WikiDocumentVO doc, String comment, String username) throws Exception {
		SaveHelper saveHelper = new SaveHelper(doc, username);
		saveHelper.setComment(comment);
		doc = saveHelper.save();

		return doc;
	}

	public static WikiDocumentVO saveWikiDoc(WikiDocumentVO doc, String comment, String username,
			boolean useNamespaceEntity, boolean isSave) throws Exception {
		if (useNamespaceEntity) {
			String toNamespace = doc.getUNamespace();
			applyAccessRights(doc, toNamespace);
		}
		SaveHelper saveHelper = new SaveHelper(doc, username);
		saveHelper.setComment(StringUtil.isBlank(comment) ? buildDefaultComment(isSave) : comment);
		doc = saveHelper.save();

		return doc;
	}

	private static String buildDefaultComment(boolean isSave) {
		return isSave ? "Saved Draft" : "Committed Version";
	}

	public static void saveListOfWikiDocs(Collection<WikiDocument> list, String username) {
		try {
			for (WikiDocument doc : list) {
				SaveUtil.saveWikiDocument(doc, username);
			}
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
		}
	}

	public static WikiDocument getWikiDocumentModel(String sysId, String fullname, String username,
			boolean loadReferences) {
		return getWikiDocumentModel(sysId, fullname, username, loadReferences, false);
	}

		
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static WikiDocument getWikiDocumentModel(String sysId, String fullname, String username, boolean loadReferences, boolean loadPbActivities)
    {

		try {
        HibernateProxy.setCurrentUser(username);
			return (WikiDocument) HibernateProxy.executeNoCache(() -> {
				
				WikiDocument doc = null;
				AccessRights ar = null;
				
				if (StringUtils.isNotBlank(sysId)) {
					doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(sysId);
				} else if (StringUtils.isNotBlank(fullname)) {
					// fetch the info based on the search criteria
					// StringBuffer sql = new StringBuffer("select wd").append("
					// from WikiDocument as wd ").append(" where upper(wd.UFullname)
					// = '").append(fullname.trim().toUpperCase() + "'");
					StringBuffer sql = new StringBuffer(
							"select  wd from WikiDocument as wd where upper(wd.UFullname) = :wikiFullName");

					Query query = HibernateUtil.createQuery(sql.toString());
					query.setParameter("wikiFullName", fullname.toUpperCase());

					List<Object> wikiDocs = query.list();
					if (wikiDocs != null && wikiDocs.size() > 0) {
						doc = (WikiDocument) wikiDocs.get(0);
					}
				}

				if (doc != null) {
					// populate the access rights
					if (doc.getAccessRights() != null) {
						ar = doc.getAccessRights();
					} else {
						Log.log.warn("" + doc + " has no access rigts!!! Creating access rights for admin user...");
						// SHOULD NEVER BE THE CASE
						ar = new AccessRights();
						ar.setUResourceType(WikiDocument.RESOURCE_TYPE);
						ar.setUResourceName(doc.getUFullname());
						ar.setUResourceId(doc.getSys_id());
						ar.setUAdminAccess("admin");
						ar.setUExecuteAccess("admin");
						ar.setUReadAccess("admin");
						ar.setUWriteAccess("admin");

						HibernateUtil.getDAOFactory().getAccessRightsDAO().persist(ar);
						doc.setAccessRights(ar);
					}

					doc.setUReadRoles(ar.getUReadAccess());
					doc.setUWriteRoles(ar.getUWriteAccess());
					doc.setUAdminRoles(ar.getUAdminAccess());
					doc.setUExecuteRoles(ar.getUExecuteAccess());

					// tags
					Collection<WikidocResolveTagRel> wikidocResolveTagRels = doc.getWikidocResolveTagRels();
					if (wikidocResolveTagRels != null)
						wikidocResolveTagRels.size();

					// catalogs
					Collection<ResolveCatalogNodeWikidocRel> catalogNodeWikidocRels = doc.getCatalogNodeWikidocRels();
					if (catalogNodeWikidocRels != null)
						catalogNodeWikidocRels.size();

					if (loadReferences) {
						Collection<WikidocAttachmentRel> wikidocAttachmentRels = doc.getWikidocAttachmentRels();
						if (wikidocAttachmentRels != null)
							wikidocAttachmentRels.size();

						Collection<WikidocStatistics> wikidocStatistics = doc.getWikidocStatistics();
						if (wikidocStatistics != null)
							wikidocStatistics.size();

						Collection<RoleWikidocHomepageRel> roleWikidocHomepageRels = doc.getRoleWikidocHomepageRels();
						if (roleWikidocHomepageRels != null)
							roleWikidocHomepageRels.size();

						Collection<WikidocResolutionRating> wikidocResolutionRating = doc.getWikidocRatingResolution();
						if (wikidocResolutionRating != null)
							wikidocResolutionRating.size();

						Collection<WikidocQualityRating> wikidocQualityRating = doc.getWikidocQualityRating();
						if (wikidocQualityRating != null)
							wikidocQualityRating.size();

					}
					if (loadPbActivities) {
						Set<PlaybookActivities> pbActivities = doc.getPbActivities();
						if (pbActivities != null)
							pbActivities.size();
					}
				}
				
				// update the graph related info
				if (loadReferences) {
					populateGraphRelatedDataForDocument(doc, username);
				}

				return doc;
				
			});

			
		} catch (Throwable t) {
			Log.log.error(String.format("Error %sin getting Wiki Document model for %s",
					(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
					StringUtils.isNotBlank(sysId) ? sysId : fullname), t);
                             HibernateUtil.rethrowNestedTransaction(t);
			return null;
		}
		// }

	}

	public static WikiDocumentVO getWikiVersion(WikiDocumentVO doc, String sysId, String version)
			throws Exception {

		try {

			return (WikiDocumentVO) HibernateProxy.execute(() -> {
				CriteriaBuilder queryBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<WikiArchive> queryCriteria = queryBuilder.createQuery(WikiArchive.class);
				Root<WikiArchive> wikiArchiveRoot = queryCriteria.from(WikiArchive.class);
				List<WikiArchive> versionList = null;

				boolean versionRequested = false;
				// latest stable
				if (com.resolve.services.util.Constants.VERSION_LATEST_STABLE.equals(version)) {
					queryCriteria.where(queryBuilder.equal(wikiArchiveRoot.get("UIsStable"), true),
							queryBuilder.equal(wikiArchiveRoot.get("UTableId"), sysId));
					queryCriteria.orderBy(queryBuilder.desc(wikiArchiveRoot.get("UVersion")));
					final int numVersionComponents = 4;
					Query<WikiArchive> query = HibernateUtil.getCurrentSession().createQuery(queryCriteria)
							.setMaxResults(numVersionComponents);
					versionList = query.list();
					versionRequested = true;
					// concrete version
				} else if (version != null && !com.resolve.services.util.Constants.VERSION_LATEST.equals(version)) {
					queryCriteria.where(queryBuilder.equal(wikiArchiveRoot.get("UTableId"), sysId),
							queryBuilder.equal(wikiArchiveRoot.get("UVersion"), version));
					Query<WikiArchive> query = HibernateUtil.getCurrentSession().createQuery(queryCriteria);
					versionList = query.list();
					versionRequested = true;
				}

				final int numVersionComponents = 4;
				if (versionList != null && versionList.size() == numVersionComponents) {
					WikiArchive first = versionList.get(0);
					Integer archiveVersion = first.getUVersion();
					Boolean isStable = first.getUIsStable();
					doc.setUVersion(archiveVersion);
					doc.setUIsStable(isStable);

					for (WikiArchive wikiArchive : versionList) {
						switch (wikiArchive.getUTableColumn()) {
						case WikiDocument.COL_CONTENT:
							doc.setUContent(wikiArchive.getUPatch());
							break;
						case WikiDocument.COL_MODEL_PROCESS:
							doc.setUModelProcess(wikiArchive.getUPatch());
							break;
						case WikiDocument.COL_MODEL_EXCEPTION:
							doc.setUModelException(wikiArchive.getUPatch());
							break;
						case WikiDocument.COL_DECISION_TREE:
							doc.setUDecisionTree(wikiArchive.getUPatch());
							break;
						default:
							break;
						}
					}
				} else if (versionRequested) {
					throw new MissingVersionException(
							String.format("Missing version: %s for wiki document with id: %s", version, sysId));
				}
				
				return doc;
			});
		} catch (Exception ex) {
			Log.log.info("Unable to retrieve wikidoc version: " + ex.getMessage());
			throw ex;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean isWikidocExist(String fullname, boolean caseSensitive) {
		boolean exist = false;
		caseSensitive = false;// making sure that the qry will always be case
								// sensitive

		boolean caseSensitiveFianl = caseSensitive;

		if (StringUtils.isNotEmpty(fullname)) {
			try {
				exist = (boolean) HibernateProxy.execute(() -> {

					// fetch the info based on the search criteria
					StringBuffer sql = null;
					String name = null;
					if (caseSensitiveFianl) {
						sql = new StringBuffer("select  wd.UFullname").append(" from WikiDocument as wd ")
								.append(" where wd.UFullname = :wikiFullName");
					} else {
						name = fullname.trim().toUpperCase();
						sql = new StringBuffer("select  wd.UFullname").append(" from WikiDocument as wd ")
								.append(" where upper(wd.UFullname) = :wikiFullName");
					}

					Query query = HibernateUtil.createQuery(sql.toString());
					query.setParameter("wikiFullName", name);
					List<Object> wikiDocs = query.list();
					if (wikiDocs != null && wikiDocs.size() > 0) {
						return true;
					}

					return false;
				});
			} catch (Throwable t) {
				Log.log.info("Unable to Determine Whether Wiki Docs Exist", t);
                               HibernateUtil.rethrowNestedTransaction(t);
			}
		}

		return exist;
	}// isWikidocExist

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getWikidocSysId(String fullname) {
		String sysId = null;
		if (StringUtils.isNotEmpty(fullname)) {
			try {
				// StringBuilder sql = new StringBuilder("select wd.sys_id").append(" from
				// WikiDocument as wd ").append(" where upper(wd.UFullname) =
				// '").append(fullname.trim().toUpperCase() + "'");

				Map<String, Object> queryParams = new HashMap<String, Object>();

				StringBuilder sql = new StringBuilder("select  wd.sys_id").append(" from WikiDocument as wd ")
						.append(" where upper(wd.UFullname) = :UFullname");
				queryParams.put("UFullname", fullname.trim().toUpperCase());

				sysId = (String) HibernateProxy.execute(() -> {
					Query query = HibernateUtil.createQuery(sql.toString());

					if (queryParams != null && !queryParams.isEmpty()) {
						for (String paramName : queryParams.keySet()) {
							query.setParameter(paramName, queryParams.get(paramName));
						}
					}

					List<Object> wikiDocs = query.list();
					if (wikiDocs != null && wikiDocs.size() > 0) {
						return wikiDocs.get(0).toString();
					}
					return null;
				});
				
			} catch (Throwable t) {
				Log.log.info("Unable to Determine Whether Wiki Docs Exist", t);
                               HibernateUtil.rethrowNestedTransaction(t);
			}
		}

		return sysId;
	}// isWikidocExist

	/**
	 * Get the WikiDoc Fullname if given the Wiki sys_id.
	 * 
	 * @param wikiSysId
	 *            : String representing wikidoc sysId.
	 * @return String representing wikidoc full name, null if no wiki present with
	 *         the given sysId.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getWikidocFullName(String wikiSysId) {
		String wikiFullName = null;
		if (StringUtils.isNotEmpty(wikiSysId)) {
			try {
				wikiFullName = (String) HibernateProxy.execute(() -> {
					// fetch the info based on the search criteria
					StringBuffer sql = new StringBuffer("select  wd.UFullname ").append("from WikiDocument as wd ")
							.append("where wd.sys_id = ?");

					Query query = HibernateUtil.createQuery(sql.toString());
					query.setParameter(0, wikiSysId);
					List<Object> wikiDocs = query.list();
					if (wikiDocs != null && wikiDocs.size() > 0) {
						return wikiDocs.get(0).toString();
					}
					return null;
				});
			} catch (Throwable t) {
				Log.log.info("Unable to Determine Whether Wiki Docs Exist", t);
                               HibernateUtil.rethrowNestedTransaction(t);
			}
		}

		return wikiFullName;
	}// getWikidocFullName

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Set<String> getAllWikidocSysIds() {
		Set<String> sysIds = new TreeSet<String>();
		try {
			HibernateProxy.execute(() -> {
				// fetch the info based on the search criteria
				String sql = "select wd.sys_id from WikiDocument as wd";

				Query query = HibernateUtil.createQuery(sql);
				List<Object> wikiDocs = query.list();
				if (wikiDocs != null && wikiDocs.size() > 0) {
					for (Object sysId : wikiDocs) {
						sysIds.add(sysId.toString());
					}
				}
			});
			
		} catch (Throwable t) {
			Log.log.info(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		return sysIds;
	}// getAllWikidocSysIds

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<String> runbooksNotFound(List<String> runbookNameList) {
		List<String> runbooksNotFound = new ArrayList<String>();

		if (runbookNameList == null || runbookNameList.isEmpty()) {
			Log.log.warn("No Runbooks Passed to Check If Exist");
		} else {
			while (runbookNameList.size() > 1000) {
				List<String> subList = runbookNameList.subList(0, 1000);
				runbooksNotFound.addAll(runbooksNotFound(subList));
				subList.clear();
			}

			StringBuffer runbookList = null;
			for (String runbookName : runbookNameList) {
				if (!runbooksNotFound.contains(runbookName)) {
					runbooksNotFound.add(runbookName);
					if (runbookList == null) {
						runbookList = new StringBuffer("'" + runbookName + "'");
					} else {
						runbookList.append(",'" + runbookName + "'");
					}
				}
			}
			if (runbookList != null) {
				try {
					StringBuffer rl = runbookList;
					HibernateProxy.execute(() -> {

						StringBuffer sql = new StringBuffer("select  wd.UFullname").append(" from WikiDocument as wd ")
								.append(" where lower(wd.UFullname) in (").append(rl.toString().toLowerCase() + ")");

						Query query = HibernateUtil.createQuery(sql.toString());
						List<Object> page = query.list();
						for (Object obj : page) {
							String fullName = (String) obj;

							runbooksNotFound.remove(fullName);
						}

					});

					// fetch the info based on the search criteria
				} catch (Throwable t) {
					Log.log.info("Unable to Determine Whether Wiki Docs Exist", t);
                                 HibernateUtil.rethrowNestedTransaction(t);
				}
			}
		}

		return runbooksNotFound;
	} // runbooksNotFound

	public static List<String> runbooksEditNotAvailable(List<String> runbookNameList, String user) {
		List<String> runbooksEditNotAvailable = new ArrayList<String>();

		if (runbookNameList == null || runbookNameList.isEmpty()) {
			Log.log.warn("No Runbooks Passed to Check Edit Permissions");
		} else {
			while (runbookNameList.size() > 1000) {
				List<String> subList = runbookNameList.subList(0, 1000);
				runbooksEditNotAvailable.addAll(runbooksEditNotAvailable(subList, user));
				subList.clear();
			}

			StringBuffer runbookList = null;
			for (String runbookName : runbookNameList) {
				if (!runbooksEditNotAvailable.contains(runbookName)) {
					runbooksEditNotAvailable.add(runbookName);
					if (runbookList == null) {
						runbookList = new StringBuffer("'" + runbookName + "'");
					} else {
						runbookList.append(",'" + runbookName + "'");
					}
				}
			}
			runbooksEditNotAvailable.clear();

			if (runbookList != null) {
				List<? extends Object> page = null;

				try {
					// fetch the info based on the search criteria
					// StringBuffer sql = new StringBuffer("select wd.sys_id, wd.UFullname,
					// ar.UWriteAccess, wd.UIsDeleted , wd.UIsLocked").append(" from AccessRights as
					// ar, ").append(" WikiDocument as wd ").append(" where ar.UResourceId =
					// wd.sys_id ").append(" and ar.UResourceType
					// ='").append(WikiDocument.RESOURCE_TYPE).append("' and wd.UFullname in
					// (").append(runbookList.toString() + ")");
					StringBuffer sql = new StringBuffer(
							"select  wd.sys_id, wd.UFullname, ar.UWriteAccess, wd.UIsDeleted , wd.UIsLocked")
									.append(" from AccessRights as ar, ").append(" WikiDocument as wd ")
									.append(" where ar.UResourceId = wd.sys_id ").append(" and ar.UResourceType ='")
									.append(WikiDocument.RESOURCE_TYPE).append("' and wd.UFullname in (:UFullname)");

					Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();

					queryInParams.put("UFullname", new ArrayList<Object>(runbookNameList));

					page = GeneralHibernateUtil.executeHQLSelectInOnly(sql.toString(), queryInParams);
					if (page != null && page.size() > 0) {
						for (Object obj : page) {
							Object[] arr = (Object[]) obj;
							// String id = (String) arr[0];
							String UFullName = (String) arr[1];
							String UWriteAccess = (String) arr[2];
							Boolean UIsDeleted = (Boolean) arr[3];
							Boolean UIsLocked = (Boolean) arr[4];
							if (UIsDeleted) {
								Log.log.error("Unable to Edit Document " + UFullName + ": Is Deleted");
								runbooksEditNotAvailable.add(UFullName);
							}
							if (UIsLocked) {
								Log.log.error("Unable to Edit Document " + UFullName + ": Is Locked");
								runbooksEditNotAvailable.add(UFullName);
							}
							boolean hasEditWrite = UserUtils.hasRole(user, UWriteAccess);
							if (!hasEditWrite) {
								Log.log.error("Unable to Edit Document " + UFullName);
								runbooksEditNotAvailable.add(UFullName);
							}
						}

					} // end of if
				} catch (Throwable t) {
					Log.log.info("Unable to Determine Edit Permissions for Affected Wikis", t);
				}

			}
		}

		return runbooksEditNotAvailable;
	} // runbooksNotEditable

	public static void addSectionToWikiDoc(String docFullName, String sectionString) {
		Log.log.debug("Adding section to " + docFullName);
		Log.log.trace("Section Content: " + sectionString);
		try {
			WikiDocumentVO doc = getWikiDoc(null, docFullName, "system");
			if (doc != null) {
				String content = doc.getUContent();
				content = content + "\n" + sectionString;
				doc.setUContent(content);

				SaveUtil.saveWikiDocument(new WikiDocument(doc), "system");
			} else {
				Log.log.error("Cannot Find Wiki Document to Edit");
			}

		} catch (Throwable t) {
			Log.log.error("Unable to Edit Wiki Doc " + docFullName, t);
		}
	} // addSectionToWikiDoc

	public static boolean isWikiDocumentExecutable(String wiki) throws Exception {
		boolean result = true;

		WikiDocumentVO doc = getWikiDoc(null, wiki, "system");

		if (doc != null) {
			result = doc.ugetUIsActive() && !doc.ugetUIsLocked() // is not
																	// locked
					&& !doc.ugetSysIsDeleted() // is not marked deleted
					&& !doc.ugetUIsHidden() // is not hidden
			;

		} else {
		    throw new Exception("Missing WIKI document: " + wiki);
		}

		return result;
	} // isWikiDocumentActive

	public static boolean isWikiDocumentMarkDeleted(String sysId, String documentFullName) throws Exception {
		boolean result = false;

		WikiDocumentVO doc = getWikiDoc(sysId, documentFullName, "system");

		if (doc != null) {
			result = doc.ugetUIsDeleted();
		} else {
			throw new Exception("Missing WIKI document: sysId:" + sysId + ", name:" + documentFullName);
		}

		return result;
	} // isWikiDocumentActive

	@SuppressWarnings("unchecked")
	public static List<String> getAllWikiDocuments(String wikiname, boolean isRunbook) {
		List<String> result = new ArrayList<String>();

		StringBuffer sql = new StringBuffer("select wd.UFullname ").append(" from AccessRights as ar, ")
				.append(" WikiDocument as wd ").append(" where ar.UResourceId = wd.sys_id and ar.UResourceType ='")
				.append(WikiDocument.RESOURCE_TYPE).append("'")
				.append(" and wd.UIsDeleted = false and wd.UIsLocked = false and wd.UIsHidden = false ")
				.append(" and wd.UHasActiveModel = ").append(isRunbook);

		Map<String, Object> queryParams = new HashMap<String, Object>();

		if (StringUtils.isNotEmpty(wikiname)) {
			// sql.append(" and wd.UFullname = '").append(wikiname).append("'");
			sql.append(" and wd.UFullname = :UFullname");
			queryParams.put("UFullname", wikiname);
		}

		try {
			List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql.toString(), queryParams);
			if (list != null && list.size() > 0) {
				result.addAll((List<String>) list);
				Collections.sort(result);
			}

		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
		}

		return result;
	}

	public static boolean hasRightsToDocument(String documentFullName, String username, RightTypeEnum rightType) {
		boolean hasRights = false;

		if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(documentFullName)) {
			// get user roles
			Set<String> userRoles = UserUtils.getUserRoles(username);

			// get the actiontask rights
			WikiDocumentVO doc = getWikiDoc(null, documentFullName, username);
			if (doc != null) {
				AccessRightsVO accessRights = doc.getAccessRights();
				String rights = accessRights.getUAdminAccess();
				if (rightType == RightTypeEnum.edit) {
					rights = rights + "," + accessRights.getUWriteAccess();
				} else if (rightType == RightTypeEnum.execute) {
					rights = rights + "," + accessRights.getUExecuteAccess();
				} else {
					rights = rights + "," + accessRights.getUReadAccess();
				}
				hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, rights);
			} else {
				hasRights = true;
			}
		}

		return hasRights;
	}

	//////////////////////////// New implementation
	public static List<WikiDocumentVO> getWikiDocuments(QueryDTO query, NodeType type, String username)
			throws Exception {
		return new SearchWiki(query, type, username).execute();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<WikiDocumentVO> searchWikiDocsUsingHQL(String whereClause, String username) throws Exception {
		List<WikiDocumentVO> list = new ArrayList<WikiDocumentVO>();
		List<Object> queryResults = null;
		// get the user roles
		Set<String> userRoles = null;
		try {
			userRoles = UserUtils.getUserRoles(username);
		} catch (Throwable e) {
			return list;
		}

		String sql = " select doc, ar from WikiDocument as doc, AccessRights as ar "
				+ " where ar.UResourceId = doc.sys_id and ar.UResourceType = '" + WikiDocument.RESOURCE_TYPE
				+ "' and doc.UIsDeleted = false " + " and doc.UIsLocked = false and doc.UIsHidden = false ";

		if (whereClause != null && whereClause.trim().length() > 0) {
			sql = sql + " and " + whereClause;
		}

		try {
			final String q = sql;
			queryResults = (List<Object>) HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(q);
				return query.list();
			});

		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (queryResults != null && queryResults.size() > 0) {
			for (Object o : queryResults) {
				Object[] arr = (Object[]) o;
				WikiDocument wd = (WikiDocument) arr[0];
				AccessRights ar = (AccessRights) arr[1];

				if (StoreUtility.hasUserAccessRight(username, userRoles, ar.getUReadAccess())) {
					list.add(wd.doGetVO());
				}

			}
		}

		return list;
	}

	// utility apis

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<WikiDocumentVO> batchSearchWikiDocsUsingHQL(String whereClause, String username)
			throws Exception {
		List<WikiDocumentVO> list = new ArrayList<WikiDocumentVO>();
		List<Object> queryResults = null;

		try {
			queryResults = (List<Object>) HibernateProxy.execute(() -> {
				String sql = " select doc from WikiDocument as doc";

				if (whereClause != null && whereClause.trim().length() > 0) {
					sql = sql + " where  " + whereClause;
				}
				Query query = HibernateUtil.createQuery(sql);
				return query.list();
			});

			if (queryResults != null && queryResults.size() > 0) {
				for (Object o : queryResults) {
					WikiDocument wd = (WikiDocument) o;
					// WikiDocument wd = (WikiDocument) arr[0];
					Collection<WikidocStatisticsVO> collection = null;
					if (wd.getWikidocStatistics() != null) {
						wd.getWikidocStatistics().size();
						if (wd.getWikidocStatistics().size() > 0) {
							collection = new ArrayList<WikidocStatisticsVO>();
							collection.add(wd.getWikidocStatistics().iterator().next().doGetVO());
						}
					}

					WikiDocumentVO wikiVO = wd.doGetVO();
					wikiVO.setWikidocStatistics(collection);
					list.add(wikiVO);
				}
			}

		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return list;
	}

	public static void validateWikiDocName(String docName) throws Exception {
		boolean result = true;

		if (docName != null) {
			result = docName.matches(HibernateConstants.REGEX_WIKI_DOCUMENT_FULLNAME);

			if (result) {
				if (docName.matches("[^\\*\\^!~`&@#$<>?/()+=,\":;{}\\[\\]|]*")) {
					CheckRule dot = new CheckRule();
					dot.ch = '.';
					dot.maxCount = 1;

					CheckRule space = new CheckRule();
					space.ch = ' ';

					Map<Character, CheckRule> checker = new HashMap<Character, CheckRule>();
					checker.put(dot.ch, dot);
					checker.put(space.ch, space);

					Character prv = null;
					for (int i = 0; i < docName.length(); i++) {
						Character ch = docName.charAt(i);

						if (checker.containsKey(ch)) {
							CheckRule cr = checker.get(ch);
							if (cr.checkContinuous && ch.equals(prv)) {
								result = false;
								break;
							}
							cr.count += 1;
							if (cr.maxCount > -1 && cr.count > cr.maxCount) {
								result = false;
								break;
							}

							checker.put(ch, cr);
						}

						prv = ch;
					}
				} else {
					throw new Exception(WIKI_DOC_NAME_SPL_CHAR_MSG);
				}
			}
		}

		if (!result) {
			throw new Exception(INVALID_WIKI_DOC_NAME_MSG);
		}

	}

	public static void updateExecuteCountForWikiDocument(String docFullName) {
		if (!StringUtils.isEmpty(docFullName)) {
			try {
				WikiDocumentVO wikiDoc = ServiceWiki.getWikiDoc(null, docFullName, "system");// StoreUtility.find(docFullName);

				if (wikiDoc != null) {
					// increment the VIEW counter for this document
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(ConstantValues.WIKI_DOCUMENT_SYS_ID, wikiDoc.getSys_id());
					params.put(HibernateConstants.WIKIDOC_STATS_COUNTER_NAME_KEY, WikiDocStatsCounterName.EXECUTED);
					sendESBForUpdatingWikiRelations(params);
				}
			} catch (Exception e) {
				Log.log.error(e.getMessage(), e);
			}
		}
	}

	public static void sendESBForUpdatingWikiRelations(Map<String, Object> params) {
		MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MWiki.updateWikiRelationships", params);
	}

	public static WikiDocumentVO createWiki(Map<String, Object> params, String username) throws Exception {
		WikiDocumentVO doc = null;
		String namespace = (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY);
		String docname = (String) params.get(ConstantValues.WIKI_DOCNAME_KEY);
		String sectionName = getSectionNameFromDocName(docname);
		docname = getDocumentNameFrom(docname);

		params.put(ConstantValues.WIKI_DOC_FULLNAME_KEY, namespace + "." + docname);
		params.put(ConstantValues.WIKI_SECTION_KEY, sectionName);

		if (params.get(ConstantValues.DEFAULT_ROLE) == null) {
			params.put(ConstantValues.DEFAULT_ROLE, "true");
		}
		params.put(ConstantValues.WIKI_ACTION_KEY, WikiAction.save);
		try {
			doc = updateWikidocumentFromMap(null, params);
			boolean reviewed = params.containsKey(ConstantValues.REVIEWED_CHECKBOX_VALUE)
					? (Boolean) params.get(ConstantValues.REVIEWED_CHECKBOX_VALUE)
					: true;

			SaveHelper wikiSave = new SaveHelper(doc, username);
			wikiSave.setReviewed(reviewed);
			doc = wikiSave.save();

			Log.log.info("Created " + namespace + "." + docname + " document via ESB");
		} catch (Exception e) {
			if (StringUtils.isBlank(e.getMessage())
					|| !e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX)
					|| !(e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX)
							|| e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX))) {
				Log.log.error("Failed to Save Decision Document", e);
			}
			throw e;
		}

		return doc;

	}// create

	public static WikiDocumentVO updateWiki(Map<String, Object> params, String username) throws Exception {
		WikiDocumentVO doc = null;
		String namespace = (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY);
		String docname = (String) params.get(ConstantValues.WIKI_DOCNAME_KEY);
		String sectionName = getSectionNameFromDocName(docname);
		docname = getDocumentNameFrom(docname);
		String fullname = namespace + "." + docname;

		params.put(ConstantValues.WIKI_DOC_FULLNAME_KEY, fullname);
		params.put(ConstantValues.WIKI_SECTION_KEY, sectionName);
		params.put(ConstantValues.WIKI_ACTION_KEY, WikiAction.save);
		params.put(ConstantValues.ESB_UPDATE_KEY, true);
		try {
			doc = getWikiDoc(null, fullname, username);
			if (doc == null) {
				throw new Exception("Document with name " + fullname + " does not exist. Aborting Update of Wiki");
			}

			doc = updateWikidocumentFromMap(doc, params);
			boolean reviewed = params.containsKey(ConstantValues.REVIEWED_CHECKBOX_VALUE)
					? (Boolean) params.get(ConstantValues.REVIEWED_CHECKBOX_VALUE)
					: true;

			SaveHelper wikiSave = new SaveHelper(doc, username);
			wikiSave.setReviewed(reviewed);
			doc = wikiSave.save();

			Log.log.info("Updated " + namespace + "." + docname + " document via ESB");
		} catch (Exception e) {
			Log.log.error("Error in updating doc :" + namespace + "." + docname, e);
			throw e;
		}

		return doc;

	}

	public static List<WikiDocumentVO> getListOfHelpDocuments(String username) throws Exception {
		List<WikiDocumentVO> result = new ArrayList<WikiDocumentVO>();

		QueryDTO query = new QueryDTO();
		query.setSelectColumns("UFullname,UHasActiveModel,sys_id");
		query.setWhereClause("UFullname like 'Doc.Wiki Help%' ");
		// [{"property":"unamespace","direction":"ASC"},{"property":"ufullname","direction":"ASC"}]
		query.setSort("[{\"property\":\"ufullname\",\"direction\":\"ASC\"}]");

		result = new SearchWiki(query, NodeType.DOCUMENT, username).execute();

		return result;
	}

	public static void moveRenameDocuments(List<String> docSysIds, String newNamespace, boolean overwrite,
			String username) throws Exception {
		moveRenameDocuments(docSysIds, newNamespace, overwrite, username, false);
	}

	public static void moveRenameDocuments(List<String> docSysIds, String newNamespace, boolean overwrite,
			String username, boolean useNamespaceEntity) throws Exception {
		if (docSysIds != null && StringUtils.isNotBlank(newNamespace) && StringUtils.isNotBlank(username)) {
			docSysIds.parallelStream()
					.forEach(sysId -> moveRenameDocument(sysId, newNamespace, overwrite, username, useNamespaceEntity));
		}
	}

	private static void moveRenameDocument(String sysId, String newNamespace, boolean overwrite, String username,
			boolean useNamespaceEntity) {
		WikiDocumentVO doc = getWikiDoc(sysId, null, username);
		if (doc != null) {
			String fromDocument = doc.getUFullname();
			String toDocument = newNamespace + "." + doc.getUName();

			try {
				moveRenameDocument(fromDocument, toDocument, overwrite, false, username, useNamespaceEntity);
			} catch (Exception e) {
				Log.log.error("Error renaming doc " + fromDocument + " to " + toDocument, e);
			}
		}
	}

	public static WikiDocumentVO moveRenameDocument(String fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username) throws Exception {
		return moveRenameDocument(fromDocument, toDocument, overwrite, validateUser, username, false);
	}

	public static WikiDocumentVO moveRenameDocument(String fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username, boolean useNamespaceEntity) throws Exception {
		MoveRenameHelper moveRename = new MoveRenameHelper(fromDocument, toDocument, username);
		moveRename.setOverwrite(overwrite);
		moveRename.setValidateUser(validateUser);
		WikiDocumentVO newWiki = moveRename.move();

		if (useNamespaceEntity) {
			String[] toDocNames = toDocument.split(REGEX_DOT_DELIMITER);
			String toNamespace = toDocNames.length == 2 ? toDocNames[0] : null;

			String[] fromDocNames = fromDocument.split(REGEX_DOT_DELIMITER);
			String fromNamespace = fromDocNames.length == 2 ? fromDocNames[0] : null;

			applyAccessRights(newWiki, fromNamespace, toNamespace);
			SaveHelper saveHelper = new SaveHelper(newWiki, username);
			newWiki = saveHelper.save();
		}

		return newWiki;
	}

	private static void applyAccessRights(WikiDocumentVO newWiki, String fromNamespace, String toNamespace)
			throws Exception {
		ResolveNamespaceVO toNS = ResolveNamespaceUtil.getNamespaceByFullName(toNamespace);
		if (toNS == null) {
			ResolveNamespaceVO fromNS = ResolveNamespaceUtil.getNamespaceByFullName(fromNamespace);
			ResolveNamespaceVO newNS = new ResolveNamespaceVO();
			newNS.setUName(toNamespace);

			AccessRights ar = new AccessRights();
			ar.applyVOToModel(fromNS.getAccessRights());
			newNS.setAccessRights(ar.doGetVO());

			ResolveNamespaceVO savedNS = ResolveNamespaceUtil.saveNamespace(newNS, null);
			newWiki.setNamespace(savedNS);
		} else {
			newWiki.setNamespace(toNS);
		}
	}

	private static void applyAccessRights(WikiDocumentVO doc, String toNamespace) throws Exception {
		ResolveNamespaceVO toNS = ResolveNamespaceUtil.getNamespaceByFullName(toNamespace);
		if (toNS == null) {
			ResolveNamespaceVO newNS = new ResolveNamespaceVO();
			newNS.setUName(toNamespace);

			AccessRights ar = new AccessRights();
			ar.applyVOToModel(doc.getAccessRights());
			newNS.setAccessRights(ar.doGetVO());

			ResolveNamespaceVO savedNS = ResolveNamespaceUtil.saveNamespace(newNS, null);
			doc.setNamespace(savedNS);
		} else {
			doc.setNamespace(toNS);
		}
	}

	public static void moveRenameDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace,
			boolean overwrite, String username) throws Exception {
		moveRenameDocumentsForNamepaces(fromNamespaces, toNamespace, overwrite, username, false);
	}

	public static void moveRenameDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace,
			boolean overwrite, String username, boolean useNamespaceEntity) throws Exception {
		if (fromNamespaces != null && StringUtils.isNotBlank(toNamespace)) {
			for (String namespace : fromNamespaces) {
				try {
					moveRenameDocumentsForNamepace(namespace, toNamespace, overwrite, username, useNamespaceEntity);
				} catch (Exception e) {
					Log.log.error("error moving the namespace " + namespace + " to " + toNamespace, e);
				}
			}
		}
	}

	public static void moveRenameDocumentsForNamepace(String fromNamespace, String toNamespace, boolean overwrite,
			String username) throws Exception {
		moveRenameDocumentsForNamepace(fromNamespace, toNamespace, overwrite, username, false);
	}

	public static void moveRenameDocumentsForNamepace(String fromNamespace, String toNamespace, boolean overwrite,
			String username, boolean useNamespaceEntity) throws Exception {
		if (StringUtils.isNotBlank(fromNamespace) && StringUtils.isNotBlank(toNamespace)) {
			Set<String> docFullnames = null;
			if (useNamespaceEntity) {
				docFullnames = ResolveNamespaceUtil.findAllDocumentsNamesForNamespace(fromNamespace);
			} else {
				docFullnames = NamespaceUtil.findAllDocumentsForNamespace(fromNamespace, username);
			}

			if (docFullnames != null) {
				for (String fromDocument : docFullnames) {
					String name = fromDocument.split(REGEX_DOT_DELIMITER)[1];
					String toDocument = toNamespace + "." + name;

					try {
						// this will be an admin user...so switching off the
						// validation
						WikiDocumentVO wikiVO = moveRenameDocument(fromDocument, toDocument, overwrite, false,
								username);

						if (useNamespaceEntity) {
							applyAccessRights(wikiVO, fromNamespace, toNamespace);
							SaveHelper saveHelper = new SaveHelper(wikiVO, username);
							wikiVO = saveHelper.save();
						}
					} catch (Exception e) {
						Log.log.error("Error renaming doc " + fromDocument + " to " + toDocument, e);
					}
				}

				if (useNamespaceEntity) {
					Set<String> remainingDocs = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(fromNamespace);
					if (CollectionUtils.isEmpty(remainingDocs)) {
						ResolveNamespaceUtil.deleteNamespaceByName(fromNamespace);
					}
				}
			}
		}
	}

	public static void copyDocuments(List<String> docSysIds, String newNamespace, boolean overwrite, String username)
			throws Exception {
		copyDocuments(docSysIds, newNamespace, overwrite, username, false);
	}

	public static void copyDocuments(List<String> docSysIds, String newNamespace, boolean overwrite, String username,
			boolean useNamespaceEntity) throws Exception {
		if (docSysIds != null && StringUtils.isNotBlank(newNamespace) && StringUtils.isNotBlank(username)) {
			docSysIds.parallelStream()
					.forEach(sysId -> copyDocument(sysId, newNamespace, overwrite, username, useNamespaceEntity));
		}
	}

	private static void copyDocument(String sysId, String newNamespace, boolean overwrite, String username,
			boolean useNamespaceEntity) {
		WikiDocumentVO doc = getWikiDoc(sysId, null, username);
		if (doc != null) {
			String fromDocument = doc.getUFullname();
			String toDocument = newNamespace + "." + doc.getUName();

			try {
				copyDocument(fromDocument, toDocument, overwrite, false, username, useNamespaceEntity);
			} catch (Exception e) {
				Log.log.error("Error renaming doc " + fromDocument + " to " + toDocument, e);
			}
		}
	}

	public static WikiDocumentVO copyDocument(String fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username) throws Exception {
		return copyDocument(fromDocument, toDocument, overwrite, validateUser, username, false);
	}

	public static WikiDocumentVO copyDocument(String fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username, boolean useNamespaceEntity) throws Exception {
		CopyHelper copyHelper = new CopyHelper(fromDocument, toDocument, username);

		return copyDocument(copyHelper, toDocument, overwrite, validateUser, username, useNamespaceEntity);
	}

	public static WikiDocumentVO copyDocument(WikiDocumentVO fromDocument, String toDocument, boolean overwrite,
			boolean validateUser, String username, boolean useNamespaceEntity) throws Exception {
		WikiDocument fromDocModel = new WikiDocument(fromDocument);
		CopyHelper copyHelper = new CopyHelper(fromDocModel, toDocument, username);

		return copyDocument(copyHelper, toDocument, overwrite, validateUser, username, useNamespaceEntity);
	}

	private static WikiDocumentVO copyDocument(CopyHelper copyHelper, String toDocument, boolean overwrite,
			boolean validateUser, String username, boolean useNamespaceEntity) throws Exception {
		copyHelper.setOverwrite(overwrite);
		copyHelper.setValidateUser(validateUser);

		WikiDocumentVO doc = copyHelper.copy();

		if (useNamespaceEntity) {
			String[] toDocNames = toDocument.split(REGEX_DOT_DELIMITER);
			String toNamespace = toDocNames.length == 2 ? toDocNames[0] : null;
			updateDocNamespace(doc, toNamespace, username);
		}

		// if from doc has Automation Builder then do its copying as well
		if (StringUtils.isNotBlank(copyHelper.getFromDocResolutionBuilderId())) {
			ResolutionBuilderUtils.copy(copyHelper.getFromDocResolutionBuilderId(), doc.getSys_id(), doc.getUName(),
					doc.getUNamespace(), username);
		}

		return doc;
	}

	public static void copyDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, boolean overwrite,
			String username) throws Exception {
		copyDocumentsForNamepaces(fromNamespaces, toNamespace, overwrite, username, false);
	}

	public static void copyDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, boolean overwrite,
			String username, boolean useNamespaceEntity) throws Exception {
		if (fromNamespaces != null && StringUtils.isNotBlank(toNamespace)) {
			for (String namespace : fromNamespaces) {
				try {
					copyDocumentsForNamepace(namespace, toNamespace, overwrite, username, useNamespaceEntity);
				} catch (Exception e) {
					Log.log.error("error copying the namespace " + namespace + " to " + toNamespace, e);
				}
			}
		}
	}

	public static void copyDocumentsForNamepace(String fromNamespace, String toNamespace, boolean overwrite,
			String username) throws Exception {
		copyDocumentsForNamepace(fromNamespace, toNamespace, overwrite, username, false);
	}

	public static void copyDocumentsForNamepace(String fromNamespace, String toNamespace, boolean overwrite,
			String username, boolean useNamespaceEntity) throws Exception {
		if (StringUtils.isNotBlank(fromNamespace) && StringUtils.isNotBlank(toNamespace)) {
			Set<String> docFullnames = null;
			if (useNamespaceEntity) {
				docFullnames = ResolveNamespaceUtil.findAllDocumentsNamesForNamespace(fromNamespace);
			} else {
				docFullnames = NamespaceUtil.findAllDocumentsForNamespace(fromNamespace, username);
			}

			if (docFullnames != null) {
				for (String fromDocument : docFullnames) {
					String name = fromDocument.split(REGEX_DOT_DELIMITER)[1];
					String toDocument = toNamespace + "." + name;

					try {
						// this will be an admin user...so switching off the
						// validation
						WikiDocumentVO copiedVO = copyDocument(fromDocument, toDocument, overwrite, false, username);
						if (useNamespaceEntity) {
							updateDocNamespace(copiedVO, toNamespace, username);
						}
					} catch (Exception e) {
						Log.log.error("Error copying doc " + fromDocument + " to " + toDocument, e);
					}
				}
			}
		}
	}

	private static WikiDocumentVO updateDocNamespace(WikiDocumentVO vo, String toNamespace, String username)
			throws Exception {
		applyAccessRights(vo, toNamespace);
		SaveHelper saveHelper = new SaveHelper(vo, username);
		return saveHelper.save();
	}

	public static WikiDocumentVO replaceDocument(String fromDocument, String toDocument, boolean validateUser,
			String username) throws Exception {
		return replaceDocument(fromDocument, toDocument, validateUser, username, false);
	}

	public static WikiDocumentVO replaceDocument(String fromDocument, String toDocument, boolean validateUser,
			String username, boolean useNamespaceEntity) throws Exception {
		ReplaceHelper copyHelper = new ReplaceHelper(fromDocument, toDocument, username);
		copyHelper.setValidateUser(validateUser);

		WikiDocumentVO doc = copyHelper.replace();

		if (useNamespaceEntity) {
			String[] toDocNames = toDocument.split(REGEX_DOT_DELIMITER);
			String toNamespace = toDocNames.length == 2 ? toDocNames[0] : null;
			updateDocNamespace(doc, toNamespace, username);
		}

		return doc;
	}

	public static void replaceDocuments(List<String> docSysIds, String newNamespace, String username) throws Exception {
		replaceDocuments(docSysIds, newNamespace, username, true);
	}

	public static void replaceDocuments(List<String> docSysIds, String newNamespace, String username,
			boolean useNamespaceEntity) throws Exception {
		if (docSysIds != null && StringUtils.isNotBlank(newNamespace) && StringUtils.isNotBlank(username)) {
			docSysIds.parallelStream()
					.forEach(sysId -> replaceDocument(sysId, newNamespace, username, useNamespaceEntity));
		}
	}

	private static void replaceDocument(String sysId, String newNamespace, String username,
			boolean useNamespaceEntity) {
		WikiDocumentVO doc = getWikiDoc(sysId, null, username);
		if (doc != null) {
			String fromDocument = doc.getUFullname();
			String toDocument = newNamespace + "." + doc.getUName();

			try {
				replaceDocument(fromDocument, toDocument, false, username, useNamespaceEntity);
			} catch (Exception e) {
				Log.log.error("Error replacing doc " + fromDocument + " to " + toDocument, e);
			}
		}
	}

	public static void replaceDocumentsForNamepace(String fromNamespace, String toNamespace, String username)
			throws Exception {
		replaceDocumentsForNamepace(fromNamespace, toNamespace, username, false);
	}

	public static void replaceDocumentsForNamepace(String fromNamespace, String toNamespace, String username,
			boolean useNamespaceEntity) throws Exception {
		if (StringUtils.isNotBlank(fromNamespace) && StringUtils.isNotBlank(toNamespace)) {
			Set<String> docFullnames = null;
			if (useNamespaceEntity) {
				docFullnames = ResolveNamespaceUtil.findAllDocumentsNamesForNamespace(fromNamespace);
			} else {
				docFullnames = NamespaceUtil.findAllDocumentsForNamespace(fromNamespace, username);
			}

			if (docFullnames != null) {
				for (String fromDocument : docFullnames) {
					String name = fromDocument.split(REGEX_DOT_DELIMITER)[1];
					String toDocument = toNamespace + "." + name;

					try {
						// this will be an admin user...so switching off the
						// validation
						WikiDocumentVO replacedVO = replaceDocument(fromDocument, toDocument, false, username);
						if (useNamespaceEntity) {
							updateDocNamespace(replacedVO, toNamespace, username);
						}
					} catch (Exception e) {
						Log.log.error("Error replacing doc " + fromDocument + " to " + toDocument, e);
					}
				}
			}
		}
	}

	public static void replaceDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, String username)
			throws Exception {
		replaceDocumentsForNamepaces(fromNamespaces, toNamespace, username, false);
	}

	public static void replaceDocumentsForNamepaces(List<String> fromNamespaces, String toNamespace, String username,
			boolean useNamespaceEntity) throws Exception {
		if (fromNamespaces != null && StringUtils.isNotBlank(toNamespace)) {
			for (String namespace : fromNamespaces) {
				try {
					replaceDocumentsForNamepace(namespace, toNamespace, username, useNamespaceEntity);
				} catch (Exception e) {
					Log.log.error("error replacing the namespace " + namespace + " to " + toNamespace, e);
				}
			}
		}
	}

	public static void updateLastReviewedDateForNamespace(Set<String> namespaces, String username) throws Exception {
		updateLastReviewedDateForNamespace(namespaces, username, false);
	}

	public static void updateLastReviewedDateForNamespace(Set<String> namespaces, String username,
			boolean useNamespaceEntity) throws Exception {
		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					if (useNamespaceEntity) {
						updateLastReviewedDateForNamespace(ns);
					} else {
						updateLastReviewedDateForNamespace(ns, username);
					}
				} catch (Exception e) {
					Log.log.error("error setting the review for ns", e);
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public static void updateLastReviewedDateForNamespace(String namespace, String username) throws Exception {
		if (StringUtils.isNotEmpty(namespace)) {
			String sql = "update WikiDocument set ULastReviewedOn = :ULastReviewedOn , "
					+ " UReqestSubmissionOn = :UReqestSubmissionOn ," + " UIsRequestSubmission = :UIsRequestSubmission "
					+ " where UNamespace = :UNamespace";

			if (StringUtils.isEmpty(username)) {
				username = "system";
			}

			try {
				Date date = GMTDate.getDate();

         HibernateProxy.setCurrentUser(username);
				HibernateProxy.execute(() -> {

					Query query = HibernateUtil.createQuery(sql);
					query.setParameter("ULastReviewedOn", date);
					query.setParameter("UReqestSubmissionOn", null, TimestampType.INSTANCE);
					query.setParameter("UIsRequestSubmission", Boolean.FALSE);
					query.setParameter("UNamespace", namespace);
					query.executeUpdate();

				});
			} catch (Throwable e) {
				Log.log.error(e.getMessage(), e);				
				throw new Exception(e);
			}
		}
	}

	public static void updateLastReviewedDateForNamespace(String namespace) throws Exception {
		try {
			Date date = GMTDate.getDate();
        
			HibernateProxy.execute(() -> {

				ResolveNamespaceVO ns = ResolveNamespaceUtil.getNamespaceByFullName(namespace);

				if (ns != null) {

					CriteriaBuilder cb = HibernateUtil.getCurrentSession().getCriteriaBuilder();
					CriteriaUpdate<WikiDocument> update = cb.createCriteriaUpdate(WikiDocument.class);
					Root<WikiDocument> fromUpdate = update.from(WikiDocument.class);

					update.set(fromUpdate.get("ULastReviewedOn"), date)
							.set(fromUpdate.get("UReqestSubmissionOn"), (Date) null)
							.set(fromUpdate.get("UIsRequestSubmission"), false)
							.where(cb.equal(fromUpdate.get("namespace").get("sys_id"), ns.getSys_id()));

					HibernateUtil.getCurrentSession().createQuery(update).executeUpdate();

				}

			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}

	public static void updateExpirationDateForWikiDocs(Map<String, Date> params, String username) throws Exception {
		if (StringUtils.isEmpty(username)) {
			username = "system";
		}

		if (params != null && params.size() > 0) {
			Iterator<String> it = params.keySet().iterator();
			while (it.hasNext()) {
				String docFullName = it.next();
				try {
					String docSysId = getWikidocSysId(docFullName);
					if (StringUtils.isNotBlank(docSysId)) {
						Set<String> docSysIds = new HashSet<String>();
						docSysIds.add(docSysId);

						Date expireDate = params.get(docFullName);

						updateExpirationDateForWikiDocs(docSysIds, expireDate, username);

					}
				} catch (Exception e) {
					Log.log.error("Error in updating the expiry date for " + docFullName, e);
				}
			}
		}
	}

	public static void updateExpirationDateForWikiDocs(Set<String> docSysIds, Date expireDate, String username)
			throws Exception {
		if (StringUtils.isEmpty(username)) {
			username = "system";
		}

		if (docSysIds != null && docSysIds.size() > 0) {
			Iterator<String> it = docSysIds.iterator();
			while (it.hasNext()) {
				String sysId = it.next();
				updateExpirationDateForWikiDoc(sysId, expireDate, username);
			}
		}
	}

	public static void updateExpirationDateForNamespace(Set<String> namespaces, Date expireDate, String username)
			throws Exception {
		updateExpirationDateForNamespace(namespaces, expireDate, username, false);
	}

	public static void updateExpirationDateForNamespace(Set<String> namespaces, Date expireDate, String username,
			boolean useNamespaceEntity) throws Exception {
		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					if (useNamespaceEntity) {
						updateExpirationDateForNamespace(ns, expireDate);
					} else {
						updateExpirationDateForNamespace(ns, expireDate, username);
					}
				} catch (Exception e) {
					Log.log.error("error setting the expiration date for ns", e);
				}
			}
		}
	}

	public static void updateExpirationDateForNamespace(Map<String, Date> params, String username) throws Exception {
		if (StringUtils.isEmpty(username)) {
			username = "system";
		}

		if (params != null && params.size() > 0) {
			Iterator<String> it = params.keySet().iterator();
			while (it.hasNext()) {
				String namespace = it.next();
				Object date = params.get(namespace);
				if (date instanceof Date || date == null) {
					try {
						updateExpirationDateForNamespace(namespace, (Date) date, "system");
					} catch (Exception e) {
						Log.log.error("Some error happened in update the expire date for namespace " + namespace, e);
					}
				}
			}
		}

	}

	@SuppressWarnings("rawtypes")
	public static void updateExpirationDateForNamespace(String namespace, Date expireDate, String username)
			throws Exception {
		if (StringUtils.isNotEmpty(namespace)) {
			String sql = "update WikiDocument set UExpireOn = :UExpireOn where UNamespace = :UNamespace";

			try {
         HibernateProxy.setCurrentUser(username);
				HibernateProxy.execute(() -> {

					Query query = HibernateUtil.createQuery(sql);
					query.setParameter("UExpireOn", expireDate, TimestampType.INSTANCE);
					query.setParameter("UNamespace", namespace);
					query.executeUpdate();

				});
			} catch (Throwable e) {
				Log.log.error(e.getMessage(), e);				
				throw new Exception(e);
			}
		}
	}

	public static void updateExpirationDateForNamespace(String namespace, Date expireDate) throws Exception {
		try {        
			HibernateProxy.execute(() -> {
				ResolveNamespaceVO ns = ResolveNamespaceUtil.getNamespaceByFullName(namespace);

				if (ns != null) {                      
					HibernateProxy.execute(() -> {
						CriteriaBuilder cb = HibernateUtil.getCurrentSession().getCriteriaBuilder();
						CriteriaUpdate<WikiDocument> update = cb.createCriteriaUpdate(WikiDocument.class);
						Root<WikiDocument> fromUpdate = update.from(WikiDocument.class);

						update.set(fromUpdate.get("UExpireOn"), expireDate)
								.where(cb.equal(fromUpdate.get("namespace").get("sys_id"), ns.getSys_id()));

						HibernateUtil.getCurrentSession().createQuery(update).executeUpdate();
					});

				}
			});

		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	public static void updateHomePageForRoles(String docSysId, Set<String> roleSysIds, String username)
			throws Exception {
		if (StringUtils.isNotBlank(docSysId) && roleSysIds != null && roleSysIds.size() > 0) {
			WikiDocument wikidoc = new WikiDocument();
			wikidoc.setSys_id(docSysId);

			try {
         HibernateProxy.setCurrentUser(username);
				HibernateProxy.execute(() -> {

					// delete all the recs that this doc was mapped with and roles
					Query query = HibernateUtil.createQuery(
							"delete from RoleWikidocHomepageRel where wikidoc.sys_id = :docSysId OR role.sys_id IN (:roleSysIds)");
					query.setParameter("docSysId", docSysId);
					query.setParameter("roleSysIds", roleSysIds);
					query.executeUpdate();
	
					// add the new ones
					for (String roleId : roleSysIds) {
						Roles role = new Roles();
						role.setSys_id(roleId);
	
						RoleWikidocHomepageRel rel = new RoleWikidocHomepageRel();
						rel.setRole(role);
						rel.setWikidoc(wikidoc);
	
						HibernateUtil.getDAOFactory().getRoleWikidocHomepageRelDAO().persist(rel);
					}

				});
			} catch (Throwable e) {
				Log.log.error(e.getMessage(), e);
				throw new Exception(e);
			}
		}
	}

	public static void updateSocialGraphDB(WikiDocument doc, String username) throws Exception {
		NodeType nodeType = NodeType.DOCUMENT;

		if (StringUtils.isNotBlank(doc.getUDisplayMode())
				&& doc.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK)) {
			if (doc.ugetUIsTemplate()) {
				nodeType = NodeType.PLAYBOOK_TEMPLATE;
			} else {
				nodeType = NodeType.SIR_PLAYBOOK;
			}
		}

		ResolveNodeVO docNode = ServiceGraph.findNode(null, doc.getSys_id(), null, nodeType, username);
		if (docNode == null) {
			docNode = new ResolveNodeVO();
			docNode.setUCompSysId(doc.getSys_id());
			docNode.setUType(nodeType);
			docNode.setUPinned(false);
		}

		Set<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
		props.add(createProp(Document.IS_DT, doc.ugetUIsRoot() + ""));
		props.add(createProp(Document.IS_RUNBOOK, doc.ugetUHasActiveModel() + ""));
		props.add(createProp(Document.DISPLAY_MODE, doc.getUDisplayMode()));
		props.add(createProp(Document.IS_TEMPLATE, doc.ugetUIsTemplate() + ""));
		// props.add(createProp(Document.IS_DELETABLE, doc.ugetUIsDeletable() + ""));
		props.add(createProp(Document.SIR_REF_COUNT, doc.ugetUSIRRefCount().toString() + ""));

		docNode.setUCompName(doc.getUFullname());
		docNode.setUMarkDeleted(doc.getUIsDeleted());
		docNode.setULock(doc.getUIsLocked());
		docNode.setProperties(props);

		// access rights
		AccessRights ar = doc.getAccessRights();
		if (ar != null) {
			docNode.setUReadRoles(
					SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
			docNode.setUEditRoles(
					SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
			docNode.setUPostRoles(
					SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
		}

		// persist the doc node
		try {
			ServiceGraph.persistNode(docNode, username);
		} catch (Exception e) {
			Log.log.error(e.getMessage());
		}

		// for updating the Namespace in the graph
		String namespace = doc.getUNamespace();
		ResolveNodeVO namespaceNode = ServiceGraph.findNode(null, null, namespace, NodeType.NAMESPACE, username);
		if (namespaceNode == null) {
			// if this not there, create one
			namespaceNode = new ResolveNodeVO();
			namespaceNode.setUCompName(namespace);
			namespaceNode.setUCompSysId(namespace);
			namespaceNode.setUType(NodeType.NAMESPACE);

			ServiceGraph.persistNode(namespaceNode, username);
		}
	}

	public static void archiveDocuments(Set<String> sysIds, String comment, String username) throws Exception {
		new ArchiveWiki(sysIds, comment, username).archive();
	}

	public static void archiveNamespaces(Set<String> namespaces, String comment, String username) throws Exception {
		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					Set<String> sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(ns, username);
					archiveDocuments(sysIds, comment, username);
				} catch (Exception e) {
					Log.log.error("error in archiving ns : " + ns, e);
				}
			}
		}
	}

	public static void resetWikidocStats(Set<String> sysIds, String username) throws Exception {
		if (sysIds != null) {
			for (String sysId : sysIds) {
				try {
					UpdateWikiDocRelations.resetWikidocStats(sysId, null, username);
				} catch (Exception e) {
					Log.log.error("error in reseting the stats for " + sysId, e);
				}
			}
		}
	}

	public static void resetNamespaces(Set<String> namespaces, String username) throws Exception {
		resetNamespaces(namespaces, username, false);
	}

	public static void resetNamespaces(Set<String> namespaces, String username, boolean useNamespaceEntity)
			throws Exception {
		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					Set<String> sysIds = null;
					if (useNamespaceEntity) {
						sysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(ns);
					} else {
						sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(ns, username);
					}
					resetWikidocStats(sysIds, username);
				} catch (Exception e) {
					Log.log.error("error in reseting stats for ns : " + ns, e);
				}
			}
		}
	}

	public static List<AccessRightsVO> getAccessRightsForNamespaces(Set<String> namespaces, String username)
			throws Exception {
		List<AccessRightsVO> result = new ArrayList<AccessRightsVO>();

		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					Set<String> sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(ns, username);
					result.addAll(getAccessRightsFor(sysIds, username));
				} catch (Exception e) {
					Log.log.error("error in getting access rights for ns: " + ns, e);
				}
			}
		}

		return result;
	}

	public static List<AccessRightsVO> getDefaultAccessRightsForNamespaces(Set<String> namespaces, String username)
			throws Exception {
		List<AccessRightsVO> result = new ArrayList<AccessRightsVO>();
		List<PropertiesVO> lProps = PropertiesUtil.getPropertiesLikeName(ConstantValues.PROP_WIKI_RIGHTS);

		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					AccessRightsVO vo = getAccessRightsForNamespace(ns, lProps);
					if (vo != null) {
						result.add(vo);
					}
				} catch (Throwable e) {
					Log.log.error("error in getting access rights for ns: " + ns, e);
				}
			}
		}

		return result;
	}

	public static List<AccessRightsVO> getAccessRightsFor(Set<String> docSysIds, String username) throws Exception {
		List<AccessRightsVO> result = new ArrayList<AccessRightsVO>();
		// String sqlDocSysIds = SQLUtils.prepareQueryString(new
		// ArrayList<String>(docSysIds));
		// String sql = "from AccessRights where UResourceId IN (" +
		// sqlDocSysIds + ") and UResourceType = '" + WikiDocument.RESOURCE_TYPE
		// + "'";
		List<String> listOfSysIds = new ArrayList<String>(docSysIds);

		if (listOfSysIds.size() > ConstantValues.BATCH_FOR_SQL_IN_CLAUSE) {
			int totalSize = listOfSysIds.size();

			int toIndex = 0;
			int fromIndex = 0;
			List<String> batch500 = null;

			while (toIndex < totalSize) {
				toIndex = fromIndex + ConstantValues.BATCH_FOR_SQL_IN_CLAUSE;
				if (toIndex > totalSize) {
					toIndex = totalSize;
				}

				batch500 = listOfSysIds.subList(fromIndex, toIndex);

				// process it
				result.addAll(getAccessRightsOfDocumentPrivate(batch500, username));

				// set the values
				fromIndex = toIndex;
			} // end of while loop
		} else {
			result.addAll(getAccessRightsOfDocumentPrivate(listOfSysIds, username));
		}

		return result;
	}

	public static List<ResolveTagVO> getTagsForNamespaces(Set<String> namespaces, String username) throws Exception {
		List<ResolveTagVO> result = new ArrayList<ResolveTagVO>();

		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					Set<String> sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(ns, username);
					result.addAll(getTagsForDocuments(sysIds, username));
				} catch (Exception e) {
					Log.log.error("error in getting tags for ns: " + ns, e);
				}
			}
		}

		return result;
	}

	public static List<ResolveTagVO> getTagsForDocuments(Set<String> docSysIds, String username) throws Exception {
		List<ResolveTagVO> result = new ArrayList<ResolveTagVO>();

		if (docSysIds != null) {
			for (String docSysId : docSysIds) {
				result.addAll(getTagsForDocument(docSysId, username));
			}
		}

		return result;
	}

	public static WikiDocumentVO validateUserForDocumentVO(String docSysId, String docFullName, RightTypeEnum right,
			String username) throws Exception {
		return validateUserForDocument(docSysId, docFullName, right, username).doGetVO();
	}

	/**
	 * this will validate the doc with the user based on the rights and will return
	 * the doc object if he has rights to it
	 * 
	 * @param docSysId
	 * @param docFullName
	 * @param right
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public static WikiDocument validateUserForDocument(String docSysId, String docFullName, RightTypeEnum right,
			String username) throws Exception {
		WikiDocument doc = getWikiDocumentModel(docSysId, docFullName, username, true);
		if (doc == null) {
			throw new Exception("Document " + docFullName + " , " + docSysId + " does not exist");
		}
		if (doc.getAccessRights() == null) {
			throw new Exception(
					"Document " + docFullName + " , " + docSysId + " has No access rights record in the DB.");
		}

		// prepare the roles based on the right type
		String roles = doc.getAccessRights().getUAdminAccess();
		if (right == RightTypeEnum.view) {
			roles = roles + "," + doc.getAccessRights().getUReadAccess();
		} else if (right == RightTypeEnum.edit) {
			roles = roles + "," + doc.getAccessRights().getUWriteAccess();
		} else if (right == RightTypeEnum.execute) {
			roles = roles + "," + doc.getAccessRights().getUExecuteAccess();
		}

		// get user roles and validate
		Set<String> userRoles = UserUtils.getUserRoles(username);
		boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(username, userRoles, roles);
		if (!userCanEdit) {
			throw new Exception("User does not have rights for the document " + doc.getUFullname());
		}

		return doc;
	}

	public static WikiDocument validateUserAccessRights(String docSysId, String docFullName, RightTypeEnum right,
			String username) {

		WikiDocument doc = getWikiDocumentModel(docSysId, docFullName, username, true);
		if (doc == null) {
			throw new IllegalStateException(String.format("Document not found. ID: '%s', name: %s, user: '%s'",
					docSysId, docFullName, username));
		} else if (doc.getAccessRights() == null) {
			throw new IllegalStateException(
					String.format("Missing access rights for document: '%s'. User: '%s'", docFullName, username));
		}

		// prepare the roles based on the right type
		String roles = doc.getAccessRights().getUAdminAccess();
		if (right == RightTypeEnum.view) {
			roles = roles + "," + doc.getAccessRights().getUReadAccess();
		} else if (right == RightTypeEnum.edit) {
			roles = roles + "," + doc.getAccessRights().getUWriteAccess();
		} else if (right == RightTypeEnum.execute) {
			roles = roles + "," + doc.getAccessRights().getUExecuteAccess();
		}

		if (!UserUtils.hasRole(username, UserUtils.getUserRoles(username), roles)) {
			throw new AccessControlException(String.format(
					"Insufficient user permissions for document. User: '%s', document: '%s'", username, docFullName));
		}

		return doc;
	}

	public static void submitRequest(String docSysId, String docFullName, boolean review, String comment,
			String username) throws Exception {
		WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, docFullName, RightTypeEnum.view, username);
		if (doc != null) {
			submitRequest(doc.getSys_id(), Constants.CR_TYPE_REVIEW, review, true, comment, username);
		}
	}

	public static void submitRequest(String docSysId, String docFullName, String type, boolean review, boolean submitCR,
			String username) throws Exception {
		WikiDocument doc = WikiUtils.validateUserForDocument(docSysId, docFullName, RightTypeEnum.view, username);
		if (doc != null) {
			submitRequest(doc.getSys_id(), type, review, submitCR, null, username);
		}
	}

	public static void updateDocumentsAsReviewed(Set<String> docSysIds, String username) throws Exception {
		if (StringUtils.isEmpty(username)) {
			username = "system";
		}

		if (docSysIds != null && docSysIds.size() > 0) {
			for (String sysId : docSysIds) {
				submitRequest(sysId, Constants.CR_TYPE_REVIEW, false, false, null, username);
			}
			// update the index so that review data is updated
			WikiIndexAPI.indexWikiDocuments(docSysIds, false, username);
		}
	}

	public static double getAverageResolutionRating(String docSysId, String docFullName, String username)
			throws Exception {
		WikiDocument doc = getWikiDocumentModel(docSysId, docFullName, username, false);
		if (doc == null) {
			throw new Exception("Document " + docFullName + " , " + docSysId + " does not exist");
		}

		return getAverageResolutionRating(doc.getSys_id());
	}// getAverageRating

	public static void submitSurvey(String docSysId, String docFullName, FeedbackDTO feedback, String username)
			throws Exception {
		WikiDocumentVO doc = ServiceWiki.getWikiDoc(docSysId, docFullName, username);
		if (doc != null) {
			// update the feedback
			int qualityRating = feedback.getRating();
			String comment = feedback.getComment();
			updateWikidocQualityRating(doc.getSys_id(), qualityRating, comment, username);

			// increase the count of yes or no for this document
			boolean incrementYesFlag = feedback.isUseful();
			boolean incrementNoFlag = !feedback.isUseful();
			updateWikidocStats(doc.getSys_id(), incrementYesFlag, incrementNoFlag, username);

			// post the feedback to doc stream
			if (StringUtils.isNotBlank(comment)) {
				List<String> ids = new ArrayList<String>();
				ids.add(doc.getSys_id());
				ServiceSocial.post("", comment, username, ids);
				WikiIndexAPI.indexWikiDocument(doc.getSys_id(), false, username);
			}
		}
	}

	public static WikidocRatingDTO getRatingFor(String docSysId, String docFullName, String username) throws Exception {
		

		try {

        HibernateProxy.setCurrentUser(username);
			return (WikidocRatingDTO) HibernateProxy.execute(() -> {
				
				WikidocRatingDTO dto = null;
				double avg = 0;

				WikiDocument wikiDocument = getWikiDocumentModel(docSysId, docFullName, username, false);
				if (wikiDocument == null) {
					if (StringUtils.isNotBlank(docSysId)) {
						throw new Exception("Document " + (docFullName != null ? docFullName : "") + " , "
								+ (docSysId != null ? docSysId : "") + " does not exist");
					} else {
						Log.log.warn("No ratings found for Document " + (docFullName != null ? docFullName : "") + " , "
								+ (docSysId != null ? docSysId : ""));
						return dto;
					}
				}

				// prepare the object
				dto = new WikidocRatingDTO();
				List<RatingDTO> ratingDtos = new ArrayList<RatingDTO>();
				List<ChartRatingDTO> chartRatingDtos = new ArrayList<ChartRatingDTO>();
				
				// reload the doc
				wikiDocument = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(wikiDocument.getSys_id());

				Collection<WikidocQualityRating> qualityRatings = wikiDocument.getWikidocQualityRating();
				if (qualityRatings != null) {
					for (WikidocQualityRating qualityRating : qualityRatings) {
						ratingDtos.add(new RatingDTO(qualityRating.getUstarCount(), qualityRating.getUfeedback(),
								DateManipulator.getDateInUTCFormat(qualityRating.getSysCreatedOn())));
					}
				}

				// get the instance of resolution obj
				WikidocResolutionRating wikiResolution = null;
				Collection<WikidocResolutionRating> wikidocResolutions = wikiDocument.getWikidocRatingResolution();
				if (wikidocResolutions != null && wikidocResolutions.size() == 1) {
					wikiResolution = wikidocResolutions.iterator().next();

					chartRatingDtos.add(new ChartRatingDTO("1 Star",
							wikiResolution.getU1StarCount() != null ? wikiResolution.getU1StarCount().intValue() : 0));
					chartRatingDtos.add(new ChartRatingDTO("2 Stars",
							wikiResolution.getU2StarCount() != null ? wikiResolution.getU2StarCount().intValue() : 0));
					chartRatingDtos.add(new ChartRatingDTO("3 Stars",
							wikiResolution.getU3StarCount() != null ? wikiResolution.getU3StarCount().intValue() : 0));
					chartRatingDtos.add(new ChartRatingDTO("4 Stars",
							wikiResolution.getU4StarCount() != null ? wikiResolution.getU4StarCount().intValue() : 0));
					chartRatingDtos.add(new ChartRatingDTO("5 Stars",
							wikiResolution.getU5StarCount() != null ? wikiResolution.getU5StarCount().intValue() : 0));
				}

				avg = getAverageResolutionRating(wikiDocument.getSys_id());

				// add it to the dto
				dto.setChartRatingDtos(chartRatingDtos);
				dto.setRatingDtos(ratingDtos);
				dto.setDocFullName(wikiDocument.getUFullname());
				dto.setDocSysId(wikiDocument.getSys_id());
				dto.setRating(avg);

				return dto;
				
			});

			
		} catch (Exception e) {
			Log.log.debug(e.getMessage(), e);
			throw e;
		}

	}

	public static AccessRightsVO getDefaultRoles(String namespace) {
		AccessRightsVO rights = new AccessRightsVO();

		List<PropertiesVO> lProps = PropertiesUtil.getPropertiesLikeName(ConstantValues.PROP_WIKI_RIGHTS);
		Set<String> viewRoles = StoreUtility.getRolesFor(RightTypeEnum.view, lProps, namespace);
		Set<String> editRoles = StoreUtility.getRolesFor(RightTypeEnum.edit, lProps, namespace);
		Set<String> executeRoles = StoreUtility.getRolesFor(RightTypeEnum.execute, lProps, namespace);
		Set<String> adminRoles = StoreUtility.getRolesFor(RightTypeEnum.admin, lProps, namespace);

		// add admin to all incase its missed in the conf properties
		viewRoles.add("admin");
		editRoles.add("admin");
		executeRoles.add("admin");
		adminRoles.add("admin");

		rights.setUReadAccess(StringUtils.convertCollectionToString(viewRoles.iterator(), ","));
		rights.setUWriteAccess(StringUtils.convertCollectionToString(editRoles.iterator(), ","));
		rights.setUExecuteAccess((StringUtils.convertCollectionToString(executeRoles.iterator(), ",")));
		rights.setUAdminAccess((StringUtils.convertCollectionToString(adminRoles.iterator(), ",")));

		return rights;
	}

	public static WikidocStatisticsVO findWikidocStatisticsForWiki(String docSysId, String docFullName, String username)
			throws Exception {
		WikidocStatisticsVO result = null;

		WikiDocument doc = validateUserForDocument(docSysId, docFullName, RightTypeEnum.view, username);
		if (doc != null) {
			if (doc.getWikidocStatistics() != null && doc.getWikidocStatistics().size() > 0) {
				WikidocStatistics stats = doc.getWikidocStatistics().iterator().next();
				if (stats != null) {
					result = stats.doGetVO();
				}
			}
		}

		return result;
	}

	public static WikiDocumentVO updateWikidocumentFromMap(WikiDocumentVO doc, Map<String, Object> params) {
		if (doc == null)
			doc = new WikiDocumentVO();
		if (params != null) {
			String namespace = params.containsKey(ConstantValues.WIKI_NAMESPACE_KEY)
					? (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY)
					: null;
			String name = params.containsKey(ConstantValues.WIKI_DOCNAME_KEY)
					? (String) params.get(ConstantValues.WIKI_DOCNAME_KEY)
					: null;
			String fullname = null;
			if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(namespace)) {
				fullname = namespace + "." + name;
			}

			doc.setUFullname(StringUtils.isNotBlank(fullname) ? fullname : doc.getUFullname());
			doc.setUNamespace(StringUtils.isNotBlank(namespace) ? namespace : doc.getUNamespace());
			doc.setUName(StringUtils.isNotBlank(name) ? name : doc.getUName());
			doc.setUContent(params.containsKey(ConstantValues.WIKI_CONTENT_KEY)
					? (String) params.get(ConstantValues.WIKI_CONTENT_KEY)
					: doc.getUContent());

			doc.setUTitle(params.containsKey(ConstantValues.WIKI_DOC_TITLE_KEY)
					? (String) params.get(ConstantValues.WIKI_DOC_TITLE_KEY)
					: doc.getUTitle());
			doc.setUSummary(params.containsKey(ConstantValues.WIKI_SUMMARY_KEY)
					? (String) params.get(ConstantValues.WIKI_SUMMARY_KEY)
					: doc.getUSummary());
			doc.setUTag(
					params.containsKey(ConstantValues.WIKI_TAGS_KEY) ? (String) params.get(ConstantValues.WIKI_TAGS_KEY)
							: doc.getUTag());

			doc.setUModelProcess(
					params.containsKey(ConstantValues.MAIN_XML_KEY) ? (String) params.get(ConstantValues.MAIN_XML_KEY)
							: doc.getUModelProcess());
			doc.setUModelFinal(
					params.containsKey(ConstantValues.FINAL_XML_KEY) ? (String) params.get(ConstantValues.FINAL_XML_KEY)
							: doc.getUModelFinal());
			doc.setUModelException(
					params.containsKey(ConstantValues.ABORT_XML_KEY) ? (String) params.get(ConstantValues.ABORT_XML_KEY)
							: doc.getUModelException());
			doc.setUDecisionTree(
					params.containsKey(ConstantValues.DTREE_XML_KEY) ? (String) params.get(ConstantValues.DTREE_XML_KEY)
							: doc.getUDecisionTree());

			doc.setUDecisionTree(params.containsKey(ConstantValues.DTREE_OPTIONS_KEY)
					? (String) params.get(ConstantValues.DTREE_OPTIONS_KEY)
					: doc.getUDTOptions());

			doc.setUDisplayMode(WikiDocumentVO.WIKI);

			boolean defaultRole = params.containsKey(ConstantValues.DEFAULT_ROLE)
					? new Boolean((String) params.get(ConstantValues.DEFAULT_ROLE))
					: true;
			if (defaultRole) {
				// use this only if the user wants to explicitly set the default
				// roles or if the access rights are not available for this
				// document
				if (params.containsKey(ConstantValues.DEFAULT_ROLE) || doc.getAccessRights() == null) {
					doc.setUIsDefaultRole(true);
				}
			} else {
				AccessRightsVO ar = doc.getAccessRights();
				if (ar == null) {
					ar = new AccessRightsVO();
				}

				ar.setUReadAccess(params.containsKey(ConstantValues.READ_ROLES_KEY)
						? (String) params.get(ConstantValues.READ_ROLES_KEY)
						: ar.getUReadAccess());
				ar.setUWriteAccess(params.containsKey(ConstantValues.WRITE_ROLES_KEY)
						? (String) params.get(ConstantValues.WRITE_ROLES_KEY)
						: ar.getUWriteAccess());
				ar.setUExecuteAccess(params.containsKey(ConstantValues.EXECUTE_ROLES_KEY)
						? (String) params.get(ConstantValues.EXECUTE_ROLES_KEY)
						: ar.getUExecuteAccess());
				ar.setUAdminAccess(params.containsKey(ConstantValues.ADMIN_ROLES_KEY)
						? (String) params.get(ConstantValues.ADMIN_ROLES_KEY)
						: ar.getUAdminAccess());

				doc.setAccessRights(ar);
			}
		}

		return doc;
	}

	public static void addTagsToWikis(Set<String> docSysIds, Set<String> tagSysIds, String username) throws Exception {
		if (docSysIds != null && docSysIds.size() > 0 && tagSysIds != null && tagSysIds.size() > 0) {
			for (String docSysId : docSysIds) {
				addOrRemoveTagsToWiki(docSysId, tagSysIds, true, username);
			}
		}
	}

	public static void removeTagsFromWikis(Set<String> docSysIds, Set<String> tagSysIds, String username)
			throws Exception {
		if (docSysIds != null && docSysIds.size() > 0 && tagSysIds != null && tagSysIds.size() > 0) {
			for (String docSysId : docSysIds) {
				addOrRemoveTagsToWiki(docSysId, tagSysIds, false, username);
			}
		}
	}

	private static WikiDocument submitRequest(String sysId, String type, boolean reviewNow, boolean submitCR,
			String comment, String username) throws Exception {
		WikiDocument result = null;
		if (StringUtils.isNotEmpty(sysId)) {
			try {				

         HibernateProxy.setCurrentUser(username);
				result = (WikiDocument) HibernateProxy.execute(() -> {
					String componentType = Constants.CR_COMPONENT_TYPE_DOCUMENT;
					WikiDocument doc = null;
					doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(sysId);
					if (doc != null) {
						if (reviewNow) {
							doc.setUReqestSubmissionOn(DateUtils.getCurrentDateInGMT(GMTDate.getDefaultTimezone(), true));
							doc.setUIsRequestSubmission(true);
							doc.setULastReviewedOn(null);
						} else {
							doc.setUReqestSubmissionOn(null);
							doc.setUIsRequestSubmission(false);
							doc.setULastReviewedOn(GMTDate.getDate());
							doc.setULastReviewedBy(username);
	
							Collection<WikidocStatistics> stats = doc.getWikidocStatistics();
							if (stats != null && stats.size() > 0) {
								// there can be only 1 rec - its a collection for
								// lazy loading
								WikidocStatistics stat = stats.iterator().next();
								int currReviewCount = stat.getUReviewCount();
								stat.setUReviewCount(currReviewCount + 1);
							}
						}
	
						// create content request
						if (submitCR) {
							if (StringUtils.isNotEmpty(doc.getUModelProcess())) {
								componentType = Constants.CR_COMPONENT_TYPE_RUNBOOK;
							} else if (StringUtils.isNotEmpty(doc.getUDecisionTree())) {
								componentType = Constants.CR_COMPONENT_TYPE_DECISIONTREE;
							}
						}
	
						HibernateUtil.getDAOFactory().getWikiDocumentDAO().persist(doc);
					}

				

					if (doc != null && reviewNow) {
						// async invocation to request
						Map<String, String> params = new HashMap<String, String>();
						params.put(ConstantValues.WIKI_DOCUMENT_NAME, doc.getUFullname());
						params.put(ConstantValues.CR_TYPE, type);
						params.put(ConstantValues.COMPONENT_TYPE, componentType);
						params.put(Constants.HTTP_REQUEST_USERNAME, username);
						if (StringUtils.isNotBlank(comment)) {
							params.put(ConstantValues.CR_DETAIL, comment);
	
							// also, post the comment if the user has entered it
							List<String> ids = new ArrayList<String>();
							ids.add(doc.getSys_id());
							ServiceSocial.post("", comment, username, ids);
						}
	
						MainBase.getESB().sendMessage(Constants.ESB_NAME_RSVIEW, "MAction.submitRequest", params);
					}
					return doc;
				});
			} catch (Throwable e) {
				Log.log.error(e.getMessage(), e);				
				throw new Exception(e);
			}
		}

		return result;
	}

	private static void updateExpirationDateForWikiDoc(String sysId, Date expireDate, String username)
			throws Exception {
		if (StringUtils.isNotEmpty(sysId)) {
			try {
         HibernateProxy.setCurrentUser(username);
				HibernateProxy.execute(() -> {

					WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(sysId);
					if (doc != null) {
						doc.setUExpireOn(expireDate);
						HibernateUtil.getDAOFactory().getWikiDocumentDAO().persist(doc);
					}

				});
			} catch (Throwable e) {
				Log.log.error(e.getMessage(), e);
				throw new Exception(e);
			}
		}
	}

	private static void populateGraphRelatedDataForDocument(WikiDocument doc, String username) {
		if (doc != null) {
			try {
				ResolveNodeVO docNode = ServiceGraph.findNode(null, doc.getSys_id(), doc.getUFullname(),
						NodeType.DOCUMENT, username);

				if (docNode == null) {
					// Find by sys id only if for some reason resolve node by name returns null

					docNode = ServiceGraph.findNode(null, doc.getSys_id(), null, NodeType.DOCUMENT, username);
				}

				Document docComp = new Document(docNode);

				doc.setUIsDocumentStreamLocked(docComp.isLock());
				doc.setUIsCurrentUserFollowing(false);

				Collection<ResolveNodeVO> nodesFollowingThisDoc = ServiceGraph.getNodesFollowingMe(docNode.getSys_id(),
						null, null, NodeType.DOCUMENT, username);
				if (nodesFollowingThisDoc != null && nodesFollowingThisDoc.size() > 0) {
					for (ResolveNodeVO node : nodesFollowingThisDoc) {
						if (node.getUType() == NodeType.USER) {
							if (node.getUCompName().equalsIgnoreCase(username)) {
								doc.setUIsCurrentUserFollowing(true);
								break;
							}
						}
					}
				}
			} catch (Exception e) {
				Log.log.trace("Error in getting document node from Node tables: " + doc.getUFullname(), e);
			}
		}
	}

	private static List<ResolveTagVO> getTagsForDocument(String docSysId, String username) {
		List<ResolveTagVO> result = new ArrayList<ResolveTagVO>();

		if (StringUtils.isNotEmpty(docSysId)) {
			try {

         HibernateProxy.setCurrentUser(username);
				HibernateProxy.execute(() -> {
					WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(docSysId);
					if (doc != null) {
						Collection<WikidocResolveTagRel> rels = doc.getWikidocResolveTagRels();
						if (rels != null && rels.size() > 0) {
							for (WikidocResolveTagRel rel : rels) {
								if (rel.getTag() != null) {
									result.add(rel.getTag().doGetVO());
								}
							} // end of for loop
						}
					}

				});
			} catch (Exception e) {
				Log.log.error("error in getting tags for document:" + docSysId, e);
                               HibernateUtil.rethrowNestedTransaction(e);
			}
		}

		return result;
	}

	private static List<AccessRightsVO> getAccessRightsOfDocumentPrivate(List<String> docSysIds, String username)
			throws Exception {
		List<AccessRightsVO> result = new ArrayList<AccessRightsVO>();
		String sqlDocSysIds = SQLUtils.prepareQueryString(new ArrayList<String>(docSysIds));

		try {
			QueryDTO query = new QueryDTO();
			query.setModelName("AccessRights");
			query.setWhereClause(
					"UResourceId IN (" + sqlDocSysIds + ") and UResourceType = '" + WikiDocument.RESOURCE_TYPE + "'");

			List<VO> data = GeneralHibernateUtil.executeSelectHQL(query, -1, -1, false);
			for (VO vo : data) {
				result.add((AccessRightsVO) vo);
			}
		} catch (Exception e) {
			Log.log.error("error getting the access rights for " + sqlDocSysIds);
			throw e;
		}

		return result;
	}

	private static AccessRightsVO getAccessRightsForNamespace(String ns, List<PropertiesVO> lProps) {
		AccessRightsVO result = null;

		if (StringUtils.isNotBlank(ns)) {
			for (PropertiesVO prop : lProps) {
				String propName = prop.getUName();
				String[] propNameValues = propName.split(REGEX_DOT_DELIMITER);
				String rightType = propNameValues[2];
				String propOfNamespace = propNameValues[3];

				if (propOfNamespace.equalsIgnoreCase(ns)) {
					if (result == null) {
						result = new AccessRightsVO();
					}

					if (rightType.equalsIgnoreCase("view")) {
						result.setUReadAccess(prop.getUValue());
					} else if (rightType.equalsIgnoreCase("edit")) {
						result.setUWriteAccess(prop.getUValue());
					} else if (rightType.equalsIgnoreCase("admin")) {
						result.setUAdminAccess(prop.getUValue());
					} else if (rightType.equalsIgnoreCase("execute")) {
						result.setUExecuteAccess(prop.getUValue());
					}
				}
			}
		}

		return result;
	}

	@SuppressWarnings("unused")
	private static String convertTagsToCSV(List<ResolveTagVO> tags) {
		StringBuilder result = new StringBuilder();

		if (tags != null) {
			for (ResolveTagVO tag : tags) {
				if (tag != null) {
					result.append(tag.getUName() + ",");
				}
			}

			if (result.length() > 1) {
				result.setLength(result.length() - 1);// remove the last comma
			}
		}

		return result.toString();
	}

	public static Set<WikiDocument> getWikiDocRefs(String docId) {
		Set<WikiDocument> refDocs = new HashSet<WikiDocument>();
		List<String> listOfALLDocsForAvoidingRecursion = new ArrayList<String>();

		// WikiDoc refs from content
		String sql = "select wd from ContentWikidocWikidocRel cwwr, WikiDocument wd where cwwr.contentWikidoc = '"
				+ docId.trim() + "' and cwwr.UWikidocFullname = wd.UFullname";
		populateReferencedWikiDocs(refDocs, listOfALLDocsForAvoidingRecursion, docId, sql);

		// WikiDoc refs from Main
		sql = "select wd from MainWikidocWikidocRel cwwr, WikiDocument wd where cwwr.mainWikidoc = '" + docId.trim()
				+ "' and cwwr.UWikidocFullname = wd.UFullname";
		populateReferencedWikiDocs(refDocs, listOfALLDocsForAvoidingRecursion, docId, sql);

		// Wikidocs refs from Exception
		sql = "select wd from ExceptionWikidocWikidocRel cwwr, WikiDocument wd where cwwr.exceptionWikidoc = '"
				+ docId.trim() + "' and cwwr.UWikidocFullname = wd.UFullname";
		populateReferencedWikiDocs(refDocs, listOfALLDocsForAvoidingRecursion, docId, sql);

		return refDocs;
	}

	public static Set<WikiDocument> getWikiDocDTRefs(String docId) {
		Set<WikiDocument> refDocs = new HashSet<WikiDocument>();
		List<String> listOfALLDocsForAvoidingRecursion = new ArrayList<String>();

		// Wikidoc ref from DT
		String sql = "select wd from DTWikidocWikidocRel dtwwr, WikiDocument wd where dtwwr.dtWikidoc = '"
				+ docId.trim() + "' and dtwwr.UWikidocFullname = wd.UFullname";
		populateReferencedWikiDocs(refDocs, listOfALLDocsForAvoidingRecursion, docId, sql);

		return refDocs;
	}

	// ***NOTE - THIS API NEEDS TO BE CALLED WITHIN A TRANSACTION AND ONLY IN
	// THE SERVICES LAYER, CANNOT BE EXPOSED *********
	public static WikiDocumentDTO prepareWikiDTO(WikiDocument doc) {
		WikiDocumentDTO dto = new WikiDocumentDTO();
		dto.setId(doc.getSys_id());
		dto.setUName(doc.getUName());
		dto.setUFullname(doc.getUFullname());
		dto.setUTitle(doc.getUTitle());
		dto.setUSummary(doc.getUSummary());
		dto.setUContent(doc.getUContent());

		for (WikidocAttachmentRel attachRel : doc.getWikidocAttachmentRels()) {
			WikiAttachment attach = attachRel.getWikiAttachment();
			if (attach.getUFilename().contains("logo")) {
				dto.getAttachments().add(attach.getSys_id());
			}
		}

		return dto;
	}

	public static void updateWikiDocCatalogRef(String newCatalogSysId, String oldCatalogSysId) throws Exception {
		QueryDTO query = new QueryDTO();
		query.setModelName("WikiDocument");
		query.setSelectColumns("sys_id");
		query.setWhereClause(" UCatalogId = '" + oldCatalogSysId + "'");

		List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, -1, -1, false);
		if (data != null) {
			for (Object sysId : data) {
				String q = " update WikiDocument set UCatalogId = '" + newCatalogSysId + "' where sys_id = '"
						+ sysId.toString() + "'";
				GeneralHibernateUtil.executeHQLUpdate(q, "admin");
			}
		}
	}

	private static void populateReferencedWikiDocs(Set<WikiDocument> refDocs,
			List<String> listOfALLDocsForAvoidingRecursion, String docId, String sql) {
		List<WikiDocument> listRefDocs = getReferencedWikiDocs(sql);

		for (WikiDocument doc : listRefDocs) {
			if (listOfALLDocsForAvoidingRecursion.contains(doc.getUFullname())) {
				return;
			}
			// add it to the list
			if (!doc.getSys_id().equals(docId))
				refDocs.add(doc);

			// add it to the recursive list
			listOfALLDocsForAvoidingRecursion.add(doc.getUFullname());

			// recurse it
			populateReferencedWikiDocs(refDocs, listOfALLDocsForAvoidingRecursion, doc.getSys_id(), sql);

		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static List<WikiDocument> getReferencedWikiDocs(String sql) {
		List<WikiDocument> list = new ArrayList<WikiDocument>();

		try {
			
			list = (List<WikiDocument>) HibernateProxy.execute(() -> {
				Query q = HibernateUtil.createQuery(sql);
				return q.list();
			});		

		} catch (Throwable e) {
			// making sure its not null
			list = new ArrayList<WikiDocument>();

			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return list;
	}

	private static double getAverageResolutionRating(String docSysId) {
		double avgRating = 0.0;
		NumberFormat formatter = new DecimalFormat("#.###");
		try {
			avgRating = (double) HibernateProxy.execute(() -> {
				WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
				WikiDocument wikiDocument = dao.findById(docSysId);

				Collection<WikidocResolutionRating> wikidocResolutions = wikiDocument.getWikidocRatingResolution();
				if (wikidocResolutions != null && wikidocResolutions.size() == 1) {
					WikidocResolutionRating wikiResolution = wikidocResolutions.iterator().next();

					double total = wikiResolution.getU1StarCount() * 1 + wikiResolution.getU2StarCount() * 2
							+ wikiResolution.getU3StarCount() * 3 + wikiResolution.getU4StarCount() * 4
							+ wikiResolution.getU5StarCount() * 5 + wikiResolution.getUinitTotal();

					long count = wikiResolution.getU1StarCount() + wikiResolution.getU2StarCount()
							+ wikiResolution.getU3StarCount() + wikiResolution.getU4StarCount()
							+ wikiResolution.getU5StarCount() + wikiResolution.getUinitCount();

					// Avoid divide by 0 and avgRating becoming NaN
					if (count > 0) {
						// calculate the avg
						return total / count;
					}

				} // end of if
				return 0.0;
			});

		} catch (Throwable e) {
			Log.log.debug(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		Log.log.debug("avgRating = " + avgRating);

		// don't let the avg be -ve
		if (avgRating < 0.0) {
			avgRating = 0.0;
		} else {
			avgRating = Double.parseDouble(formatter.format(avgRating));
		}

		return avgRating;
	}// getAverageRating

	private static void updateWikidocQualityRating(String wikidocumentId, int qualityRating, String feedback,
			String username) {
		try {
        HibernateProxy.setCurrentUser(username);
			WikiDocument wikiDoc = (WikiDocument) HibernateProxy.execute(() -> {

				// get the handle of wikidocument
				WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
				WikiDocument wikiDocument = dao.findById(wikidocumentId);
	
				WikidocQualityRatingDAO daoWR = HibernateUtil.getDAOFactory().getWikidocQualityRatingDAO();
	
				// create the object
				WikidocQualityRating wikiQuality = new WikidocQualityRating();
				wikiQuality.setWikidoc(wikiDocument);
				wikiQuality.setUstarCount(qualityRating);
				wikiQuality.setUfeedback(feedback);
	
				// persist
				daoWR.persist(wikiQuality);
	
				// get the instance of resolution obj
				WikidocResolutionRating wikiResolution = null;
				Collection<WikidocResolutionRating> wikidocResolutions = wikiDocument.getWikidocRatingResolution();
				if (wikidocResolutions != null && wikidocResolutions.size() == 1) {
					wikiResolution = wikidocResolutions.iterator().next();
				} else {
					wikiResolution = new WikidocResolutionRating();
					wikiResolution.setWikidoc(wikiDocument);
				}
	
				if (qualityRating == 1) {
					wikiResolution.setU1StarCount(
							wikiResolution.getU1StarCount() != null ? wikiResolution.getU1StarCount() + 1 : 1);
				} else if (qualityRating == 2) {
					wikiResolution.setU2StarCount(
							wikiResolution.getU2StarCount() != null ? wikiResolution.getU2StarCount() + 1 : 1);
				} else if (qualityRating == 3) {
					wikiResolution.setU3StarCount(
							wikiResolution.getU3StarCount() != null ? wikiResolution.getU3StarCount() + 1 : 1);
				} else if (qualityRating == 4) {
					wikiResolution.setU4StarCount(
							wikiResolution.getU4StarCount() != null ? wikiResolution.getU4StarCount() + 1 : 1);
				} else if (qualityRating == 5) {
					wikiResolution.setU5StarCount(
							wikiResolution.getU5StarCount() != null ? wikiResolution.getU5StarCount() + 1 : 1);
				}
	
				HibernateUtil.getDAOFactory().getWikidocResolutionRatingDAO().persist(wikiResolution);
	
				// update the rating boost for the document.
				double rating = getAverageResolutionRating(wikiDocument.getSys_id());
				wikiDocument.setURatingBoost(rating);
				return wikiDocument;
			});

			// index it
			if (wikiDoc != null) {
				// index the rating info to Elastic Search
				WikiIndexAPI.indexWikiDocument(wikiDoc.getSys_id(), false, username);
			}
		} catch (Throwable e) {
			Log.log.debug(e.getMessage(), e);			
                             HibernateUtil.rethrowNestedTransaction(e);
		}
	}// updateWikidocQualityRating

	private static void updateWikidocStats(String docSysId, boolean incrementYesFlag, boolean incrementNoFlag,
			String username) {
		if (StringUtils.isNotEmpty(docSysId)) {
			try {
         HibernateProxy.setCurrentUser(username);
				HibernateProxy.execute(() -> {

				WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(docSysId);
					if (doc != null) {
	
						Collection<WikidocStatistics> wikidocStatistics = doc.getWikidocStatistics();
						WikidocStatistics stats = null;
						if (wikidocStatistics != null && wikidocStatistics.size() > 0) {
							stats = (new ArrayList<WikidocStatistics>(wikidocStatistics)).get(0);
						} else {
							stats = new WikidocStatistics();
						}
	
						int currCounter = 0;
	
						if (incrementYesFlag) {
							currCounter = stats.getUUsefulYesCount();
							currCounter++;
							stats.setUUsefulYesCount(currCounter);
						}
	
						if (incrementNoFlag) {
							currCounter = stats.getUUsefulNoCount();
							currCounter++;
							stats.setUUsefulNoCount(currCounter);
						}
	
						// persist it
						HibernateUtil.getDAOFactory().getWikidocStatisticsDAO().persist(stats);
	
					}
				});
			} catch (Throwable t) {
				Log.log.error(t.getMessage(), t);
                               HibernateUtil.rethrowNestedTransaction(t);
			}
		}
	}

	private static void addOrRemoveTagsToWiki(String docSysId, Set<String> tagSysIds, boolean add, String username) {
		if (StringUtils.isNotBlank(docSysId) && tagSysIds != null && tagSysIds.size() > 0) {
			try {
         HibernateProxy.setCurrentUser(username);
				HibernateProxy.execute(() -> {

					WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(docSysId);
					if (doc != null) {
						Collection<WikidocResolveTagRel> tagRels = doc.getWikidocResolveTagRels();
	
						// delete the existing tags
						if (add) {
							for (String tagSysId : tagSysIds) {
								ResolveTag tagModel = HibernateUtil.getDAOFactory().getResolveTagDAO().findById(tagSysId);
								if (tagModel != null) {
									// so tag is available
									// check if it already is assigned to this
									// document
									boolean insert = true;
									if (tagRels != null && tagRels.size() > 0) {
										for (WikidocResolveTagRel rel : tagRels) {
											if (rel.getTag() != null) {
												if (rel.getTag().getUName().equalsIgnoreCase(tagModel.getUName())) {
													insert = false;
													break;
												}
											}
										} // end of for loop
									}
	
									if (insert) {
										WikidocResolveTagRel rel = new WikidocResolveTagRel();
										rel.setWikidoc(doc);
										rel.setTag(tagModel);
	
										HibernateUtil.getDAOFactory().getWikidocResolveTagRelDAO().persist(rel);
									}
								} // end of if
							} // end of for loop
						} else {
							// for delete
							if (tagRels != null && tagRels.size() > 0) {
								for (WikidocResolveTagRel rel : tagRels) {
									// if the sysId is available in the collection,
									// then delete that relationship
									if (tagSysIds.contains(rel.getTag().getSys_id())) {
										HibernateUtil.getDAOFactory().getWikidocResolveTagRelDAO().delete(rel);
									}
								} // end of for loop
							}
						}
	
					} // end of if
					else {
						throw new Exception("Document with sysId '" + docSysId + "' does not exist.");
					}

				});
			} catch (Exception e) {
				Log.log.error("error while assigning tags to document " + docSysId, e);
                               HibernateUtil.rethrowNestedTransaction(e);

			}

		}

	}

	protected static ResolveNodePropertiesVO createProp(String key, String value) {
		ResolveNodePropertiesVO prop = new ResolveNodePropertiesVO();
		prop.setUKey(key);
		prop.setUValue(value);

		return prop;
	}

	static class CheckRule {
		public Character ch;
		public int maxCount = -1;
		public int count;
		public boolean checkContinuous = true;
	}

	/**
	 * API to be called from update script to clean all bad (dangling) references
	 * present in Wiki content and in main, abort and DT models.
	 * 
	 */
	public static void cleanWikidocBadReferences() {
		final int FIXED_POOL_THREAD_COUNT = 40;
		final int MAX_WIKIDOC_BATCH_SIZE = 500;

		ExecutorService service = Executors.newFixedThreadPool(FIXED_POOL_THREAD_COUNT);

		Log.log.info("*** CLEANING WIKIDOC BAD REFERENCES START ***");
		List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsForWithTimeout("WikiDocument",
				"UNamespace not in ('Doc3', 'Doc')", "system", 500));
		if (sysIds != null && sysIds.size() > 0) {
			Log.log.info("Total # of Wikis To Be Checked For Bad References: " + sysIds.size());
			int batchCount = 0;
			int batchSize = 1;

			if (sysIds.size() > FIXED_POOL_THREAD_COUNT)
				batchSize = sysIds.size() / FIXED_POOL_THREAD_COUNT;

			if (batchSize > MAX_WIKIDOC_BATCH_SIZE)
				batchSize = MAX_WIKIDOC_BATCH_SIZE;

			List<List<String>> batchSysIds = JavaUtils.chopped(sysIds, batchSize);
			Collection<Callable<Integer>> cleanWikiDocTasks = new ArrayList<Callable<Integer>>();
			for (List<String> batchSysId : batchSysIds) {
				cleanWikiDocTasks.add(new CleanBadRefsFromWiki(new HashSet<String>(batchSysId)));
				Log.log.info("Prepared Batch # " + (++batchCount) + " of " + batchSysId.size());
			}

			int completedCount = 0;
			try {
				List<Future<Integer>> futures = service.invokeAll(cleanWikiDocTasks);

				if (futures != null && !futures.isEmpty()) {
					for (Future<Integer> future : futures) {
						completedCount += (Integer) future.get();
						Log.log.info(completedCount + " of " + sysIds.size() + " Wikidocs are cleaned");
					}
				}

				Log.log.info("*** CLEANING WIKIDOC BAD REFERENCES END ***");
			} catch (InterruptedException e) {
				Log.log.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				Log.log.error(e.getMessage(), e);
			} catch (Exception e) {
				Log.log.error(e.getMessage(), e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static ResponseDTO<Map<String, Map<String, List<String>>>> convertDocxToWiki(HttpServletRequest request,
			String resolveHome) {
		ResponseDTO<Map<String, Map<String, List<String>>>> result = new ResponseDTO<>();

		// Get wiki info from the request
		String docxSourceDirectory = resolveHome + "tmp/";
		String docxSourceFile = "";
		HashMap<String, String> inputs = new HashMap<String, String>();

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		String errorMessage = "";

		try {

			// Pull out the file item list from the request
			List<FileItem> fileItemList = upload.parseRequest(request);

			// System.out.println("File item list: " + fileItemList.toString());

			// Parse the uploaded file items
			Iterator<FileItem> iter = fileItemList.iterator();

			inputs.put("wikiID", "No wiki ID given.");
			inputs.put("wikiName", "No wiki name given.");
			inputs.put("userName", "No username given.");
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();

				if (item.isFormField()) {

					// Add form field parameter into inputs map

					inputs.put(item.getFieldName(), item.getString());
					// System.out.println("wikiInputs: " + item.getFieldName() + ": " +
					// item.getString());
				} else {

					// upload the file
					docxSourceFile = processUploadedFile(item, docxSourceDirectory);
				}
			}

		} catch (Exception e) {
			errorMessage = "Error while converting Docx to Wiki.";
			Log.log.error(e.getMessage(), e);
		}

		// System.out.println("Docx source file: " + docxSourceFile);

		// Now that the docx file is uploaded, convert the file, but only if the upload
		// went through

		if (docxSourceFile == null || docxSourceFile.isEmpty()) {
			result.setSuccess(false);
			result.setMessage("An error occurred when trying to upload the docx file to the server. \n" + errorMessage);
		} else {

			String docxSourcePath = docxSourceDirectory + docxSourceFile;
			// System.out.println("Docx source path: " + docxSourcePath);

			try {

				// Convert the docx file.
				// System.out.println("Inputs: " + inputs.toString());
				String docxContent = DocxAPI.convertDocxFileToWiki(docxSourcePath, inputs.get("wikiID"),
						inputs.get("wikiName"), inputs.get("userName"));

				// format the docxContent
				docxContent.replaceAll("\\r\\n", "");
				docxContent.replaceAll("\\\"", "\"");

				// Put the html content into a properly formatted response

				HashMap<String, Map<String, List<String>>> content = new HashMap<String, Map<String, List<String>>>();
				HashMap<String, List<String>> contentMap = new HashMap<String, List<String>>();
				ArrayList<String> docxText = new ArrayList<String>();
				docxText.add(docxContent);
				contentMap.put("htmlContent", docxText);
				content.put("docxContent", contentMap);

				// Create response

				result.setSuccess(true);
				result.setMessage("The docx file at " + docxSourceFile + " has been successfully converted.");
				result.setData(content);

			} catch (Exception e) {
				result.setSuccess(false);
				result.setMessage("An error occurred when trying to upload the docx file to the server.");
				Log.log.error("An error occurred when trying to upload the docx file to the server.", e);
			}

		}

		return result;
	}

	private static String processUploadedFile(FileItem item, String location) throws Exception {
		// few attributes for debuging purpose...may be removed/commented out
		// String fieldName = item.getFieldName();
		String fileName = item.getName();// C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
		fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);

		// String contentType = item.getContentType();//text/plain
		// boolean isInMemory = item.isInMemory();

		// long sizeInBytes = item.getSize();

		File file = FileUtils.getFile(location, fileName);
		item.write(file);

		return fileName;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResponseDTO encryptParams(JSONObject jsonProperty) {
		ResponseDTO result = new ResponseDTO();
		StringBuilder sb = new StringBuilder();
		sb.append(jsonProperty.get("connectionType") + "Connect").append("#").append(ABConstants.DEFAULT_NAMESPACE);
		String connectTaskFullname = sb.toString();
		HashMap<String, String> params = null;
		try {
			params = new ObjectMapper().readValue(jsonProperty.get("params").toString(), HashMap.class);
		} catch (IOException e) {
			Log.log.error(e.getMessage(), e);
		}
		ResolveActionTaskVO connectActionTaskVO = ActionTaskUtil.getActionTaskWithReferences(null, connectTaskFullname,
				"admin");
		if (connectActionTaskVO == null) {
			result.setMessage("Missing ActionTask " + connectTaskFullname);
			return result;
		}
		ResolveActionInvocVO connectActionInvokeVO = connectActionTaskVO.getResolveActionInvoc();
		if (connectActionInvokeVO != null) {
			Collection<ResolveActionParameterVO> resolveActionParameterVOs = connectActionInvokeVO
					.getResolveActionParameters();

			if (resolveActionParameterVOs != null && !resolveActionParameterVOs.isEmpty()) {
				for (ResolveActionParameterVO resolveActionParameterVO : resolveActionParameterVOs) {
					String paramName = resolveActionParameterVO.getUName();
					if (resolveActionParameterVO.getUEncrypt() && params.containsKey(paramName)) {
						String value = params.get(paramName);
						if (!value.startsWith("$")) {
							try {
								value = CryptUtils.encrypt(value);
							} catch (Exception e) {
								Log.log.error(e.getMessage(), e);
							}
						}
						params.put(paramName, value);
					}
				}
			}
		}
		result.setData(params);
		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String getDTModel(String wikiFullName, String userName) {
		String dtModel = null;

		try {
        HibernateProxy.setCurrentUser(userName);
			dtModel = (String) HibernateProxy.execute(() -> {

				StringBuffer sql = new StringBuffer(
						"select wd.UDecisionTree from WikiDocument as wd where upper(wd.UFullname) = :wikiFullName");

				Query query = HibernateUtil.createQuery(sql.toString());
				query.setParameter("wikiFullName", wikiFullName.toUpperCase());

				List<Object> objects = query.list();
				if (objects != null && objects.size() > 0) {
					return objects.get(0);
				}
				
				return null;
			});


			
		} catch (Exception e) {
			Log.log.error("Error while reading DT Model for Wiki: " + wikiFullName, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return dtModel;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Set<String> getModelTasksMockList(String docSysId, String username) {
		Set<String> mockNameSet = new HashSet<String>();
		WikiDocument wikiDoc = getWikiDocumentModel(docSysId, null, username, false);

		MainWikidocActiontaskRel rel = new MainWikidocActiontaskRel();
		rel.setMainWikidoc(wikiDoc);

		List<MainWikidocActiontaskRel> list = null;

		String sql = " from MainWikidocActiontaskRel where mainWikidoc.sys_id = :wikiSysId";

		try {
			list = (List<MainWikidocActiontaskRel>) HibernateProxy.execute(() -> {
				Query q = HibernateUtil.createQuery(sql);
				q.setParameter("wikiSysId", docSysId);
				return q.list();
			});

			
		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (list != null) {
			for (MainWikidocActiontaskRel relation : list) {
				String taskName = relation.getUActiontaskFullname();
				mockNameSet.addAll(ActionTaskUtil.getTaskMockList(taskName, username));
			}
		}

		return mockNameSet;
	}

	public static void updateResultMacro(String wikiFullname) {
		List<Object> contentList = getWikiContentList(wikiFullname);

		if (contentList != null) {
			for (Object obj : contentList) {
				Object[] objArray = (Object[]) obj;
				String sysId = (String) objArray[0];
				String content = (String) objArray[1];

				if (StringUtils.isNotBlank(content)) {
					boolean updateNeeded = false;
					String[] resultMacros = StringUtils.substringsBetween(content, "{result2:", "{result2}");
					if (resultMacros != null && resultMacros.length > 0) {
						for (String resultMacro : resultMacros) {
							if (resultMacro.contains("|selectAll=")) {
								continue;
							} else {
								updateNeeded = true;
							}
							resultMacro = "{result2:" + resultMacro + "{result2}";
							String updatedMacro = null;
							if (resultMacro.trim().contains("namespace=")) {
								// OK, the result macro contains actiontask(s)
								updatedMacro = resultMacro.replaceFirst("}", "|selectAll=false}");
							} else {
								updatedMacro = resultMacro.replaceFirst("}", "|selectAll=true}");
							}

							content = StringUtils.replace(content, resultMacro, updatedMacro);
						}
					}

					if (updateNeeded) {
						updateNeeded = false;
						// update the records
						updateWikiContent(content, sysId);
					}
				}
			}
			HibernateUtil.clearDBCacheRegion(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, false);
		}
	}

	public static void uploadAutomationImage(String imageName, FileItem fileItem, String resolveHome) throws Exception {
		String fileName = fileItem.getName();
		String ext = FilenameUtils.getExtension(fileName);
		String destination = resolveHome + "tomcat/webapps/resolve/jsp/model/images/resolve/custom/";
		File distFolder = new File(destination);
		if (!distFolder.exists()) {
			distFolder.mkdir();
		}
		File file = FileUtils.getFile(destination, imageName + "." + ext);
		fileItem.write(file);
	}

	public static List<String> listAutomationImages(final String resolveHome) {
		List<String> imageNameList = new ArrayList<String>();

		File dir = FileUtils.getFile(resolveHome + "tomcat/webapps/resolve/jsp/model/images/resolve/custom/");
		if (dir.exists()) {
			WildcardFileFilter filter = new WildcardFileFilter("*.*");
			Collection<File> fileCollection = FileUtils.listFiles(dir, filter, TrueFileFilter.INSTANCE);
			if (fileCollection != null) {
				for (File file : fileCollection) {
					imageNameList.add(file.getName());
				}
			}
		}

		return imageNameList;
	}

	public static void deleteAutomationImage(final String imageName, final String resolveHome) {
		String fileLocation = resolveHome + "tomcat/webapps/resolve/jsp/model/images/resolve/custom/" + imageName;
		File file = FileUtils.getFile(fileLocation);

		if (file.exists()) {
			file.delete();
		}
	}

	/*
	 * In older version (Resolve 5.3 and earlier), user had to type '\n' in a wiki
	 * to get a new line character. We are running away from that notion. Now, user
	 * will see what they type in the wiki content. So, to support '\n' as a new
	 * char, we had to migrate all the old wiki's in which, all the '\n's will be
	 * replace by </br>.
	 */
	public static void updateCarriageReturn(String wikiFullname) {
		List<Object> contentList = getWikiContentList(wikiFullname);

		if (contentList != null) {
			for (Object obj : contentList) {
				Object[] objArray = (Object[]) obj;
				String sysId = (String) objArray[0];
				String content = (String) objArray[1];

				if (StringUtils.isNotBlank(content)) {
					boolean updateNeeded = false;
					if (content.contains("\\n")) {
						updateNeeded = true;
						content = content.replace("\\n", "</br>");
					}

					if (updateNeeded) {
						updateNeeded = false;
						updateWikiContent(content, sysId);
					}
				}
			}
			HibernateUtil.clearDBCacheRegion(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, false);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Object> getWikiContentList(String wikiFullname) {
		
		List<Object> contentList = null;
		try {
			contentList =(List<Object>) HibernateProxy.execute(() -> {
				
				String sql = "select sys_id, UContent from WikiDocument";
				if (StringUtils.isNotBlank(wikiFullname)) {
					sql = sql + " where lower(UFullname) = :wikiFullname";
				}
				
				Query q = HibernateUtil.createQuery(sql);
				if (StringUtils.isNotBlank(wikiFullname)) {
					q.setParameter("wikiFullname", wikiFullname.toLowerCase());
				}
				return q.list();
			});
			
		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return contentList;
	}

	public static List<String> getReferredForms(String wikiSysId) {
		Set<String> formNames = new HashSet<>();
		List<String> result = new ArrayList<>();
		String wikiFullname = getWikidocFullName(wikiSysId);
		if (StringUtils.isBlank(wikiFullname)) {
			return result;
		}
		List<Object> contentList = getWikiContentList(wikiFullname);
		if (contentList != null) {
			for (Object obj : contentList) {
				Object[] objArray = (Object[]) obj;
				String content = (String) objArray[1];

				if (StringUtils.isNotBlank(content)) {
					formNames.addAll(ParseUtil.getListOfReferredForms(content));
				}
			}
		}
		// construct AB form name
		String formName = "ABF_" + wikiFullname.replace(" ", "_").replace("-", "_").replace(".", "_").toUpperCase();

		MetaFormView formViewVO = null;
		try {
			formViewVO = CustomFormUtil.findMetaFormView(null, formName, "admin");
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
		if (formViewVO != null) {
			formNames.add(formViewVO.getUFormName());
		}

		if (CollectionUtils.isNotEmpty(formNames)) {
			formNames.stream().forEach(f -> {
				MetaFormView formView = findMetaFormViewByName(f.trim());
				if (formView != null) {
					result.add(f + ImpexEnum.COLON.getValue() + formView.getSys_id());
				}
			});
		}
		return result;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Object> getWikiContentList(String wikiFullname, Integer version) {
		try {
			return (List<Object>) HibernateProxy.execute(() -> {
				
				List<Object> contentList = null;
				
				String sql = "select sys_id, UContent, UVersion from WikiDocument";
				if (StringUtils.isNotBlank(wikiFullname)) {
					sql = sql + " where lower(UFullname) = :wikiFullname";
				}
				
				Query q = HibernateUtil.createQuery(sql);

				if (StringUtils.isNotBlank(wikiFullname)) {
					q.setParameter("wikiFullname", wikiFullname.toLowerCase());
				}

				List<Object> objs = q.list();

				if (objs != null && !objs.isEmpty() && objs.size() == 1) {
					if (version != null && version.intValue() > 0) {
						Object[] objArray = (Object[]) objs.get(0);

						Integer wikiDocVer = (Integer) objArray[2];

						if (wikiDocVer != null && wikiDocVer.intValue() == version.intValue()) {
							contentList = objs;
						} else {
							sql = "select sys_id, UPatch, UVersion from WikiArchive where UTableName = 'wikidoc' and UTableColumn = 'CONTENT' and UTableId = :UTableId and UVersion = :UVersion";

							q = HibernateUtil.createQuery(sql);

							q.setParameter("UTableId", (String) objArray[0]);
							q.setParameter("UVersion", version, IntegerType.INSTANCE);

							contentList = q.list();
						}
					} else {
						contentList = objs;
					}
				}
				
				return contentList;
			});
		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
		
		return null;

	}

	@SuppressWarnings("rawtypes")
	public static void updateWikiContent(String content, String sysId) {
		String updateSQL = "update WikiDocument set UContent = :content " + " where sys_id = :sysId";

		try {
        HibernateProxy.setCurrentUser("admin");
			HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(updateSQL);
				query.setParameter("content", content);
				query.setParameter("sysId", sysId);
				query.executeUpdate();
			});

		} catch (Throwable e) {
			Log.log.error("Could not update result macro for Wiki sysId: " + sysId, e);			
                             HibernateUtil.rethrowNestedTransaction(e);
		}
	}

	/**
	 * An API to update only the model of a wiki with given content. modelName could
	 * be UContent, UModelProcess, UModelException or UDecisionTree
	 */
	@SuppressWarnings("rawtypes")
	public static void updateWikiModel(String modelName, String content, String sysId) {
		String updateSQL = "update WikiDocument set " + modelName + " = :content " + " where sys_id = :sysId";

		try {
        HibernateProxy.setCurrentUser("admin");
			HibernateProxy.execute(() -> {
			
				Query query = HibernateUtil.createQuery(updateSQL);
				query.setParameter("content", content);
				query.setParameter("sysId", sysId);
				query.executeUpdate();
			});			
		} catch (Throwable e) {
			Log.log.error("Could not update result macro for Wiki sysId: " + sysId, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void updateWikiArchiveContent(String content, String wiki_sys_id, Integer version) {
		String updateSQL = "update WikiArchive set UPatch = :content " + "where  UTableId = :wiki_sys_id "
				+ "and    UVersion = :version " + "and    lower(UTableName) = 'wikidoc' "
				+ "and    UTableColumn = 'CONTENT'";

		try {
        HibernateProxy.setCurrentUser("admin");
			HibernateProxy.execute(() -> {

				Query query = HibernateUtil.createQuery(updateSQL);
				query.setParameter("content", content);
				query.setParameter("wiki_sys_id", wiki_sys_id);
				query.setParameter("version", version, IntegerType.INSTANCE);
				query.executeUpdate();
			});
		} catch (Throwable e) {
			Log.log.error(
					"Could not update wikiarchive content for Wiki sysId: " + wiki_sys_id + ", version: " + version, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
	}

	/**
	 * API (to be called from Resolve update) to reset the falsely set
	 * Wiki-Automation Builder flag It reads all the Wiki SysIds, reads their
	 * associated Resolution (Automation) Builder ids and check whether resolution
	 * builder has any meta data (tasks) associated with it. If no, reset the flag.
	 * 
	 * ExecutorService is used with the thread pool of 50. Every thread will check
	 * for 500 tasks.
	 */
	public static void fixWikiABAssociation() {
		long startTime = System.currentTimeMillis();
		// resetWikiABAssociation();

		ExecutorService service = Executors.newFixedThreadPool(50);

		List<String> sysIds = new ArrayList<String>(
				GeneralHibernateUtil.getSysIdsForWithTimeout("WikiDocument", null, "system", 480));

		List<List<String>> batchSysIds = JavaUtils.chopped(sysIds, 500);

		Collection<Callable<Collection<String>>> tasks = new ArrayList<Callable<Collection<String>>>();

		for (List<String> ids : batchSysIds) {
			tasks.add(new WikiUtils().new CheckWikiABAssociation(ids));
		}

		try {
			service.invokeAll(tasks);
		} catch (Throwable t) {
			Log.log.error("Could not submit task to fix WikiABAssociation", t);
		}

		try {
			service.shutdown();
		} catch (Throwable t) {
			Log.log.error("Could not shutdown executor service to fix WikiABAssociation", t);
		}

		Log.log.info("Total time for fixing Wiki AB Association for " + sysIds.size() + " docs: "
				+ ((System.currentTimeMillis() - startTime) / 1000d));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getCorrectCaseWikiName(String wikiName, String username) {
		String correctCaseWikiName = null;

		StringBuffer sql = new StringBuffer("select UFullname from WikiDocument ").append("where lower(UFullname) = ?");
		try {
        HibernateProxy.setCurrentUser(username);
			correctCaseWikiName = (String) HibernateProxy.execute(() -> {
				
				String result = null;
				
				Query query = HibernateUtil.createQuery(sql.toString());
				query.setParameter(0, wikiName.toLowerCase());
				List<String> wikiDocNames = query.list();
				if (wikiDocNames != null && wikiDocNames.size() > 0) {
					result = wikiDocNames.get(0);
				}
				
				return result;
			});

			

		} catch (Exception e) {
			Log.log.error("Could not get correct case wiki name", e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return correctCaseWikiName;
	}

	public class CheckWikiABAssociation implements Runnable, Callable<Collection<String>> {
		List<String> wikiSysIds = null;

		public CheckWikiABAssociation(List<String> sysIds) {
			this.wikiSysIds = sysIds;
		}

		@Override
		public void run() {
			try {
				for (String wikiSysId : wikiSysIds) {
					String generalSysId = getGeneralSysId(wikiSysId);
					if (StringUtils.isNotBlank(generalSysId)) {
						if (!checkABTaskAssociation(generalSysId)) {
							updateWikiABAssociation(wikiSysId);
						}
					}
				}
			} catch (Exception e) {
				Log.log.error("Could not run a thread to update WikiABAssociation.", e);
			}
		}

		@Override
		public Collection<String> call() throws Exception {
			run();
			return new ArrayList<String>();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String getGeneralSysId(String wikiSysId) {
		String id = null;

		String sql = "select UResolutionBuilderId from WikiDocument where sys_id = :sysId";
		List<String> tasks = null;
		try {
			
        HibernateProxy.setCurrentUser("admin");
			tasks = (List<String>) HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(sql);
				query.setParameter("sysId", wikiSysId);
				return  query.list();
			});
			

		} catch (Throwable e) {
			Log.log.error("Could not find RBGeneral sysId from WikiSysId: " + wikiSysId, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (tasks != null) {
			id = tasks.get(0);
		}

		return id;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private boolean checkABTaskAssociation(String rbGeneralId) {
		boolean result = false;
		String sql = "select sys_id from RBTask where generalId = :generalId";
		List<String> tasks = null;
		try {
        HibernateProxy.setCurrentUser("admin");
			tasks = (List<String>) HibernateProxy.execute(() -> {
	
				Query query = HibernateUtil.createQuery(sql);
				query.setParameter("generalId", rbGeneralId);
				return query.list();

			});
		} catch (Throwable e) {
			Log.log.error("Could not find RBTasks for GeneralId: " + rbGeneralId, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (tasks != null && tasks.size() > 0) {
			result = true;
		}

		return result;
	}

	@SuppressWarnings("rawtypes")
	private void updateWikiABAssociation(String wikiId) {
		String sql = "update WikiDocument set UHasResolutionBuilder = false where sys_id = :sysId";
		try {
        HibernateProxy.setCurrentUser("admin");
			HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(sql);
				query.setParameter("sysId", wikiId);
				query.executeUpdate();
			});
		} catch (Throwable e) {
			Log.log.error("Could not update UHasResolutionBuilder field of Wiki:" + wikiId, e);			
                             HibernateUtil.rethrowNestedTransaction(e);
		}
	}

	private static String getSectionNameFromDocName(String docname) {
		String section = "";
		if (docname.indexOf("#") > -1) {
			section = docname.substring(docname.indexOf("#") + 1);
		}

		return section;
	}

	private static String getDocumentNameFrom(String docname) {
		String name = docname;
		if (docname.indexOf("#") > -1) {
			name = docname.substring(0, docname.indexOf("#"));
		}

		return name;

	}

	/**
	 * API to migrate SIR 1.0 playbook template to SIR 2.0. Should be called from
	 * migration script ONLY.
	 **/
	@SuppressWarnings("unchecked")
	public static void migrateSirTemplate() {
		String hql = "from WikiDocument where u_content not like '%\"metaPhaseId\":%' and u_display_mode = 'playbook'";
		List<WikiDocument> docList = null;
		try {
        HibernateProxy.setCurrentUser("admin");
			docList = (List<WikiDocument>) HibernateProxy.execute(() -> {
				return HibernateUtil.createQuery(hql).list();
			});
		} catch (Exception e) {
			Log.log.error("Error while reading old templates for migration.", e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (docList != null && !docList.isEmpty()) {
			docList.stream().map(s -> s.doGetVO(false)).forEach(s -> {
				try {
					ServiceWiki.saveAndCommitWikiDoc(s, "", s.getSysUpdatedBy(), true);
				} catch (Exception e) {
					Log.log.error("Error while saving old template, " + s.getUFullname() + ", while migration.", e);
				}
			});
		}
	}

	private static final String SYSID = "sysId";
	private static final String CHECKSUM = "checksum";
	private static final String UPDATE_CHECKSUM_SQL = "update WikiDocument set checksum = :" + CHECKSUM
			+ " where sys_id = :" + SYSID;
	private static final String ERROR_WIKI_CHECKSUM_UPDATE = "Error: Could not update checksum for Wiki: %s";
    
    public static Integer calculateChecksum(WikiDocumentVO wikiVO)
    {
		/**
		 * ********** IMPORTANT ***********
		 *
		 * This approach is not as accurate as it should be. If user changes model
		 * layout, say by changing a task icon a bit, we will get different checksum,
		 * which is not right because logically, the Runbook execution flow is not
		 * changed.
		 * 
		 * Ideally, we should construct a model dependency graph and that should be used
		 * to calculate checksum.
		 * 
		 * This logic will be changed in the future when we start constructing model
		 * dependency graph and save it in the DB at the time of Wiki save.
		 */
		int hash = 0;

		String content = wikiVO.getUContent();
		String mainModel = wikiVO.getUModelProcess();
		String abortModel = wikiVO.getUModelException();
		String decisionTreeModel = wikiVO.getUDecisionTree();

		hash = Objects.hash(wikiVO.getUFullname(), content, mainModel, abortModel, decisionTreeModel);

		return hash;
	}
    
    /*
     * API to be invoked by UpdateChecksumUtil.
     */
    public static Integer calculateChecksum(WikiDocument wiki)
    {
        return calculateChecksum(wiki.doGetVO());
    }
    
    public static void updateWikiChecksum(WikiDocumentVO wikiVO, String username)
    {
        Integer checksum = calculateChecksum(wikiVO);
		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {
				HibernateUtil.createQuery(UPDATE_CHECKSUM_SQL).setParameter(SYSID, wikiVO.getSys_id()).setParameter(CHECKSUM, checksum).executeUpdate();
			});
		} catch (Exception e) {
			Log.log.error(String.format(ERROR_WIKI_CHECKSUM_UPDATE, wikiVO.getUFullname()), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
	}
    
    private static final String FROM_WIKI_SQL = "from WikiDocument where checksum is null order by sysCreatedOn";
    private static final String ERROR_READING_WIKIS_FOR_CHECKSUM_UPDATE = "Error: Could not read Wikis for checksum update.";
    private static final String ERROR_UPDATING_WIKIS_WITH_CHECKSUM = "Error: Could not update Wiki checksums in batch operation";
    
    
    private static int calculateWikiChecksum(WikiDocumentVO wikiVO) {

		/**
		 * ********** IMPORTANT ***********
		 *
         * This approach is not as accurate as it should be. If user changes model layout,
         * say by changing a task icon a bit, we will get different checksum, which is not
         * right because logically, the Runbook execution flow is not changed.
		 * This approach is not as accurate as it should be. If user changes model
		 * layout, say by changing a task icon a bit, we will get different checksum,
		 * which is not right because logically, the Runbook execution flow is not
		 * changed.
		 * 
		 * Ideally, we should construct a model dependency graph and that should be used
		 * to calculate checksum.
		 * 
		 * This logic will be changed in the future when we start constructing model
         *  dependency graph and save it in the DB at the time of Wiki save. 
		 * dependency graph and save it in the DB at the time of Wiki save.
		 */
		int hash = 0;
        

		String content = wikiVO.getUContent();
		String mainModel = wikiVO.getUModelProcess();
		String abortModel = wikiVO.getUModelException();
		String decisionTreeModel = wikiVO.getUDecisionTree();
        

		hash = Objects.hash(wikiVO.getUFullname(), content, mainModel, abortModel, decisionTreeModel);
        

		return hash;
	}

	/**
	 * API to be invoked from update/upgrade script only!
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void updateAllWikisForChecksum() throws Exception {
                                                                   HibernateProxy.setCurrentUser("admin");
		HibernateProxy.execute(() -> {
			int returnedRecords = -1;
			List<WikiDocument> wikiList = null;
			do {
				try {
					
					// read 50 tasks at a time
					wikiList = HibernateUtil.createQuery(FROM_WIKI_SQL).setMaxResults(100).setFirstResult(0).list();				
				} catch (Exception e) {					
					Log.log.error(ERROR_READING_WIKIS_FOR_CHECKSUM_UPDATE, e);
				}
	
				if (wikiList.size() > 0) {
					try {
						// batch update all returned records.
						Query query = HibernateUtil.createQuery(UPDATE_CHECKSUM_SQL);
						wikiList.stream().map(list -> list.doGetVO(false)).forEach(wikiVO -> {
							int checksum = calculateWikiChecksum(wikiVO);
							query.setParameter(SYSID, wikiVO.getSys_id()).setParameter(CHECKSUM, checksum).executeUpdate();
						});
					} catch (Exception e) {
						Log.log.error(ERROR_UPDATING_WIKIS_WITH_CHECKSUM, e);
					}
				}
				// stop after returned records are less than 50.
			} while (returnedRecords == 100);
		});
	}

	private static MetaFormView findMetaFormViewByName(String formName) {
		try {
        HibernateProxy.setCurrentUser("system");
			return (MetaFormView) HibernateProxy.execute(() -> {
				MetaFormView obj = null;
				if (StringUtils.isNotEmpty(formName)) {
					obj = new MetaFormView();
					obj.setUFormName(formName);
					
					MetaFormViewDAO daoMetaFormView = HibernateUtil.getDAOFactory().getMetaFormViewDAO();
					obj = daoMetaFormView.findFirst(obj);
				}

				return obj;
			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		return null;
	}

	public static Integer getWikichecksum(String wikiFullname, String username) {
		Integer checksum = null;
		WikiDocumentVO wikiVO = getWikiDoc(null, wikiFullname, username);
		if (wikiVO != null) {
			checksum = wikiVO.getChecksum();
		}
		return checksum;
	}

	/*
	 * API to be called from udate script only. It will clean Wiki-Wiki-Content
	 * relation and Wiki-Task content relation table.
	 */
	@SuppressWarnings("unchecked")
	public static void cleanWikiContentRelations() throws Exception {
		List<String> sqlList = new ArrayList<>();
		sqlList.add("select distinct contentWikidoc.sys_id from ContentWikidocWikidocRel");
		sqlList.add("select distinct contentWikidoc.sys_id from ContentWikidocActiontaskRel");
		sqlList.stream().forEach(sql -> {
			int returnedRecords = -1;
			List<String> wikiIdList = null;
			int page = 1, limit = 50;

			do {
				try {
					int start = (page - 1) * limit;
          HibernateProxy.setCurrentUser("admin");
					wikiIdList = (List<String>) HibernateProxy.execute(() -> {
						// read 50 tasks at a time
						return HibernateUtil.createHQLQuery(sql).setMaxResults(limit).setFirstResult(start).list();
					});
					page++;
				} catch (Exception e) {
					
					Log.log.error("Error while reading wiki list from ContentWikidocWikidocRel", e);
                                 HibernateUtil.rethrowNestedTransaction(e);
				}

				if (CollectionUtils.isNotEmpty(wikiIdList)) {
					try {
						wikiIdList.stream().map(wikiId -> getWikiDoc(wikiId, null, "admin"))
								.forEach(wikiDoc -> UpdateWikiDocRelations.updateContentWikiDocRelationship(wikiDoc));
					} catch (Exception e) {
						Log.log.error("Error while leaning wiki wiki content relation for DT.", e);
					}
				}
				returnedRecords = wikiIdList.size();

				// stop after returned records are less than 50.
			} while (returnedRecords == 50);
		});
	}

}
