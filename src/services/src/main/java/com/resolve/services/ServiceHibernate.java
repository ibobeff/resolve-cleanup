/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.query.Query;

import com.resolve.esb.ESB;
import com.resolve.persistence.model.ResolveEvent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.EntityUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigCAS;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.search.customform.CustomFormIndexAPI;
import com.resolve.search.property.PropertyIndexAPI;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.actiontask.VersionPersistAction;
import com.resolve.services.hibernate.customtable.CreateCustomTable;
import com.resolve.services.hibernate.customtable.CustomFormUtil;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.services.hibernate.customtable.DeleteMetaFilter;
import com.resolve.services.hibernate.customtable.DeleteMetaForm;
import com.resolve.services.hibernate.customtable.DeleteMetaTableView;
import com.resolve.services.hibernate.customtable.ExtMetaFormLookup;
import com.resolve.services.hibernate.customtable.ExtSaveMetaFormView;
import com.resolve.services.hibernate.menu.MenuDefinitionUtil;
import com.resolve.services.hibernate.menu.MenuServiceUtil;
import com.resolve.services.hibernate.util.ActionTaskPropertiesUtil;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.AppsUtil;
import com.resolve.services.hibernate.util.ArchiveUtil;
import com.resolve.services.hibernate.util.AssessorUtil;
import com.resolve.services.hibernate.util.BusinessRuleUtil;
import com.resolve.services.hibernate.util.ConfigActiveDirectoryUtil;
import com.resolve.services.hibernate.util.ConfigLDAPUtil;
import com.resolve.services.hibernate.util.ConfigRADIUSUtil;
import com.resolve.services.hibernate.util.CronUtil;
import com.resolve.services.hibernate.util.ExecuteSysScriptUtil;
import com.resolve.services.hibernate.util.ExtFormSubmit;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.GenericDataServiceUtil;
import com.resolve.services.hibernate.util.GenericUserUtils;
import com.resolve.services.hibernate.util.GroovyUtil;
import com.resolve.services.hibernate.util.LogUtil;
import com.resolve.services.hibernate.util.MetricUtil;
import com.resolve.services.hibernate.util.OrganizationUtil;
import com.resolve.services.hibernate.util.ParserUtil;
import com.resolve.services.hibernate.util.PreprocessorUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.RSControlUtil;
import com.resolve.services.hibernate.util.RSSReader;
import com.resolve.services.hibernate.util.ResolveArchiveUtils;
import com.resolve.services.hibernate.util.ResolveBlueprintUtil;
import com.resolve.services.hibernate.util.ResolveControllerUtil;
import com.resolve.services.hibernate.util.ResolveEventUtil;
import com.resolve.services.hibernate.util.ResolveRegistrationUtil;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.util.SearchUtil;
import com.resolve.services.hibernate.util.SocialAdminUtil;
import com.resolve.services.hibernate.util.SocialAttachmentUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.util.StoreUtility;
import com.resolve.services.hibernate.util.SystemScriptUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.VariableUtil;
import com.resolve.services.hibernate.util.WikiLookupUtil;
import com.resolve.services.hibernate.util.WikiTemplateUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.hibernate.vo.AlertLogVO;
import com.resolve.services.hibernate.vo.ArchiveWorksheetVO;
import com.resolve.services.hibernate.vo.AuditLogVO;
import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.hibernate.vo.ConfigLDAPVO;
import com.resolve.services.hibernate.vo.ConfigRADIUSVO;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.GatewayLogVO;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.MetaAccessRightsVO;
import com.resolve.services.hibernate.vo.MetaFieldPropertiesVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.hibernate.vo.NamespaceVO;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskMockDataVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveAppsVO;
import com.resolve.services.hibernate.vo.ResolveArchiveVO;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.hibernate.vo.ResolveBlueprintVO;
import com.resolve.services.hibernate.vo.ResolveBusinessRuleVO;
import com.resolve.services.hibernate.vo.ResolveControllersVO;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.services.hibernate.vo.ResolveEventHandlerVO;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.hibernate.vo.ResolveImpexLogVO;
import com.resolve.services.hibernate.vo.ResolveMCPLoginVO;
import com.resolve.services.hibernate.vo.ResolveParserTemplateVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.hibernate.vo.ResolvePreprocessVO;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveRegistrationPropertyVO;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.services.hibernate.vo.ResolveSessionVO;
import com.resolve.services.hibernate.vo.ResolveSysScriptVO;
import com.resolve.services.hibernate.vo.ResolveWikiLookupVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.SocialPostAttachmentVO;
import com.resolve.services.hibernate.vo.SysAppApplicationVO;
import com.resolve.services.hibernate.vo.SysAppModuleVO;
import com.resolve.services.hibernate.vo.UserPreferencesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.NamespaceUtil;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.jdbc.util.JdbcCrudUtil;
import com.resolve.services.util.ResolveSessionUtil;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.vo.ActionTaskTreeDTO;
import com.resolve.services.vo.DefaultDataDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.SearchAttributesDTO;
import com.resolve.services.vo.SearchRecordDTO;
import com.resolve.services.vo.WikiUser;
import com.resolve.services.vo.form.FormData;
import com.resolve.services.vo.form.FormSubmitDTO;
import com.resolve.services.vo.form.RsCustomTableDTO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.services.vo.menu.MenuDTO;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.MetricThresholdType;
import com.resolve.util.ResultCode;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.workflow.form.dto.CustomFormDTO;

import groovy.lang.Binding;

/**
 * All Apis related to HIbernate goes here. Please document these apis well as this is the entry point to the persisted layer.
 *
 * Also, keep the implementation of these as 1 liner by invoking another class in the package so that this class will
 * already be huge due to number of apis in it.
 *
 *  If we document the api here, there may be no need to document in the supporting classes , except code documentation.
 *
 * NOTE: If there is any API that is referencing the MODEL in the persistence package in this Class, there is something NOT CORRECT. Make sure that this class
 * uses only the VO objects then you pattern is correct.
 *
 * always CONVERT THE MODEL TO VO OUTSIDE OF THE TRANSACTION - refer UserUtils.getUserPreferences() for example
 *
 * @author jeet.marwah
 *
 */


public class ServiceHibernate
{

    /**************************************/
    /**
     * THESE API TO BE USED ONLY IN EXCEPTION CASES. CONSULT WITH DUKE BEFORE USING THIS TRANSACTION APIS
     *
     * @param username
     */


    public static void rethrowNestedTransaction(Throwable t)
    {
        GeneralHibernateUtil.rethrowNestedTransaction(t);
    }
    /********* END OF TRANSACTION APIS *****************************/

    /**
     * this logs the message in the audit log table
     *
     * @param source
     * @param message
     */
    public static void auditLog(String source, String message)
    {
        GeneralHibernateUtil.log(source, message);
    }

    /**
     * convert the string to Hibernate Clob object
     *
     * @param value
     * @return
     */
    public static Clob getClobFromString(String value)
    {
        return GeneralHibernateUtil.getClobFromString(value);
    }

    /**
     * Convert the Clob object to String
     *
     * @param obj
     * @return
     */
    public static String getStringFromClob(Object obj)
    {
        return GeneralHibernateUtil.getStringFromClob(obj);
    }
    
    public static void uploadAutomationImage(String imageName, FileItem fileItem, String resolveHome) throws Exception
    {
    	WikiUtils.uploadAutomationImage(imageName, fileItem, resolveHome);;
    }
    
    public static List<String> listAutomationImages(final String resolveHome)
    {
    	return WikiUtils.listAutomationImages(resolveHome);
    }
    
    public static void deleteAutomationImage(final String imageName, final String resolveHome)
    {
    	WikiUtils.deleteAutomationImage(imageName, resolveHome);
    }
    
    /**
     * return all namespace of wikidoc and actiontask
     *
     * @return
     */
    public static List<NamespaceVO> getAllNamespaces(QueryDTO query)
    {
        return NamespaceUtil.getAllNamespaces(query);
    }

    /**
     * return all namespace of wikidoc and actiontask 
     *
     * @return
     */
    public static List<NamespaceVO> getAllNamespaces(QueryDTO query, boolean exclude)
    {
        return NamespaceUtil.getAllNamespaces(query, exclude);
    }

    /**
     * initialize the properties from the flat file
     *
     * @param fileName
     * @throws Exception
     */
    public static void initProperties(String fileName) throws Exception
    {
        PropertiesUtil.initProperties(fileName);
    }


    public static void setPropertyString(String name, String value)
    {
        PropertiesUtil.setPropertyString(name, value);
    }

    /**
     * This utility method is to get the property of name and whose type is
     * String
     *
     * @param name
     * @return
     */
    public static String getPropertyString(String name)
    {
        return PropertiesUtil.getPropertyString(name);
    }

    /**
     * This utility method is to update a value of the property (NOT A CREATE)
     * and whose type is boolean
     *
     * @param name
     * @param value
     */
    public static void setPropertyBoolean(String name, boolean boolValue)
    {
       PropertiesUtil.setPropertyBoolean(name, boolValue);
    }

    /**
     * This utility method is to get the property of name and whose type is
     * boolean
     *
     * @param name
     * @return
     */
    public static boolean getPropertyBoolean(String name)
    {
        return PropertiesUtil.getPropertyBoolean(name);
    }

    /**
     * THis is the utility method for the properties that has type as List. This
     * is an UPDATE and not a CREATE.
     *
     * @param name
     * @param values
     */
    public static void setPropertyList(String name, List<String> values)
    {
        PropertiesUtil.setPropertyList(name, values);
    }

    /**
     * THis is the utility method for the properties that has Type as List.
     *
     * @param name
     * @return
     */
    public static List<String> getPropertyList(String name)
    {
        return PropertiesUtil.getPropertyList(name);
    }

    /**
     * Utility method to update the property that has Int as its type. Not a
     * CREATE.
     *
     * @param name
     * @param value
     */
    public static void setPropertyLong(String name, long value)
    {
        PropertiesUtil.setPropertyLong(name, value);
    }

    /**
     * Utility method to get the property value that has INT as its type.
     *
     * @param name
     * @return
     */
    public static long getPropertyLong(String name)
    {
        return PropertiesUtil.getPropertyLong(name);
    }

    /**
     *
     * This will create Properties record if it does not exist, else update the
     * existing one.
     *
     * @param props
     */
    public static PropertiesVO saveProperties(PropertiesVO props, String username)  throws Exception
    {
        return PropertiesUtil.saveProperties(props, username);
    }

    /**
     * THis method queries the properties table and does a wildcard search on
     * name
     *
     * @param uname
     * @return
     */
    public static List<PropertiesVO> getPropertiesLikeName(String uname)
    {
        return PropertiesUtil.getPropertiesLikeName(uname);
    }

    /**
     * where clause to get the list of properties
     *
     * @param whereQuery
     * @param username
     * @return
     */
    public static List<PropertiesVO> getPropertiesUsingWhereClause(String whereQuery, String username)
    {
        return PropertiesUtil.getPropertiesUsingWhereClause(whereQuery, username);
    }

    /**
     * delete the social notification namespace property from the Properties table
     *
     * @param namespace
     * @param userName
     */
    public static void deleteSocialNamespaceNotification(String namespace, String userName)
    {
        PropertiesUtil.deleteSocialNamespaceNotification(namespace, userName);
    }

    /**
     * saving a new social namespace property in the properties table
     *
     * @param namespace
     * @param userName
     */
    public static void saveNamespaceNotifications(String namespace, String userName)
    {
        PropertiesUtil.saveNamespaceNotifications(namespace, userName);
    }

    /**
     * delete the list of property rows based on sysIds
     *
     * @param propertiesIDS
     * @return
     */
//    public static String deletePropertiesByIDS(List<String> propertiesIDS)
//    {
//        return PropertiesUtil.deletePropertiesByIDS(propertiesIDS);
//    }

    /**
     * find property by sysid
     *
     * @param sysId
     * @return
     */
    public static PropertiesVO findPropertyById(String sysId)
    {
        return PropertiesUtil.findPropertyById(sysId);
    }


    /**
     * find property by name
     *
     * @param name
     * @return
     */
    public static PropertiesVO findPropertyByName(String name)
    {
        return PropertiesUtil.findPropertyByName(name);
    }

    public static void deletePropertiesById(String[] sysIds, String username) throws Exception
    {
        PropertiesUtil.deletePropertiesById(sysIds, username);
    }



    /**
     * facade for getting the next seq integer
     *
     * @param sequenceName
     * @return
     */
    public static Integer getNextSequence(String sequenceName)
    {
        return GeneralHibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_INIT);
    }
    
    public static void archive(String modelClass, String attributeName, String sysId, String username) throws Exception
    {
        ResolveArchiveUtils.archive(modelClass, attributeName, sysId, username);
    }
    
    public static void purgeArchive(String modelClass, String sysIds, String username) throws Exception
    {
        ResolveArchiveUtils.purgeArchive(modelClass, sysIds, username);
    }
    
    public static ResponseDTO<ResolveArchiveVO> findArchivesFor(String modelClassName, String sysId, int start, int limit, String username) throws Exception
    {
        return ResolveArchiveUtils.findArchivesFor(modelClassName, sysId, start, limit, username);
    }

    /**
     * Get Users object.
     *
     * @param username
     * @return Users object. Null if the username does not exist.
     */
    public static UsersVO getUser(String username)
    {
        return GenericUserUtils.getUser(username);
    }
    
    public static boolean isValidUser(String username)
    {
        boolean valid = false;
        
        if(StringUtils.isNotBlank(username))
        {
            UsersVO user = getUser(username);
            if(user != null)
            {
                valid = true;
            }
        }
        
        return valid;
    }
    
    public static boolean isAdminUser(String username)
    {
        return GenericUserUtils.isAdminUser(username);
    }
    
    public static UsersVO getUserNoCache(String username)
    {
        return UserUtils.getUserNoCache(username);
    }

    /**
     * Return true if the organization that the user belongs to exists and is active
     *
     * @param username
     * @return
     */
    public static boolean isUserOrgEnabled(String username)
    {
        return UserUtils.isUserOrgEnabled(username);
    }

    /**
     * get role based on sysId or rolename
     *
     * @param sysId
     * @param roleName
     * @return
     */
    public static RolesVO getRole(String sysId, String roleName)
    {
        return UserUtils.getRole(sysId, roleName, "system");
    }

    public static List<RolesVO> getRolesForGroup(String groupSysId, String groupName)
    {
        return UserUtils.getRolesForGroup(groupSysId, groupName);
    }

    public static List<UsersVO> getUsersForGroup(String groupSysId, String groupName)
    {
        return UserUtils.getUsersForGroup(groupSysId, groupName);
    }



    public static void addRolesToGroup(String groupSysId, String groupName, List<String> roles, boolean deleteRolesNotInList, String username)  throws Exception
    {
        UserUtils.addRolesToGroup(groupSysId, groupName, roles, deleteRolesNotInList, username);
    }

    public static void addRolesAndGroupsToUser(String userSysId, String userName, List<String> roles, List<String> groups, String username)  throws Exception
    {
        UserUtils.addRolesAndGroupsToUser(userSysId, userName, roles, groups, username);
    }

    public static void addRolesToUser(String userSysId, String userName, List<String> roles, boolean deleteRolesNotInList, String username)  throws Exception
    {
        UserUtils.addRolesToUser(userSysId, userName, roles, deleteRolesNotInList, username);
    }

    public static void addGroupsToUser(String userSysId, String userName, List<String> groups, String username)  throws Exception
    {
        UserUtils.addGroupsToUser(userSysId, userName, groups, username);
    }

    public static void removeUserFromGroups(String userSysId, String userName, List<String> groups, String username)  throws Exception
    {
        UserUtils.removeUserFromGroups(userSysId, userName, groups, username);
    }
    
    public static Collection<String> removeUsersFromGroups(Set<String> userSysIds, Set<String> groups, String username) throws Exception
    {
        return UserUtils.removeUsersFromGroups(userSysIds, groups, username);
    }

    public static void removeUserFromRoles(String userSysId, String userName, List<String> roles, String username) throws Exception
    {
        UserUtils.removeUserFromRoles(userSysId, userName, roles, username);
    }

    public static Collection<String> removeUsersFromRoles(Set<String> userSysId, Set<String> roles, String username) throws Exception
    {
        return UserUtils.removeUsersFromRoles(userSysId, roles, username);
    }
    
    /**
     * deleting list of roles based on ids
     *
     * @param userRolesIDS
     * @param username
     */
    public static Collection<String> deleteUserRolesByIDS(List<String> userRolesIDS, String username)  throws Exception
    {
        return UserUtils.deleteUserRolesByIDS(userRolesIDS, username);
    }

    public static Collection<String> deleteUserGroupByIDS(List<String> userGroupIDS, String username)  throws Exception
    {
        return UserUtils.deleteUserGroupByIDS(userGroupIDS, username);
    }

    public static void deleteUsersByIDS(List<String> userSysIds, String username) throws Exception
    {
        UserUtils.deleteUsersByIDS(userSysIds, username);
    }

    public static boolean copyTemplateUser(String username, String domain) throws Exception
    {
        return UserUtils.copyTemplateUser(username, domain);
    }


    /**
     * get a group by name or sysId
     *
     * @param groupName
     * @return
     */
    public static GroupsVO getGroup(String sysId, String groupName)
    {
        return UserUtils.getGroup(sysId, groupName, "system");
    }

    /**
     * get the users with roles
     *
     * @param username
     * @return
     */
    public static UsersVO getUserWithRoles(String username)
    {
       return UserUtils.getUserWithRoles(username);
    }

    /**
     * user with preferences
     *
     * @param username
     * @return
     */
    public static UsersVO getUserWithPreferences(String username)
    {
        return UserUtils.getUserWithPreferences(username);
    }

    /**
     * return pref for a user
     *
     * @param username
     * @param preferencesKeys
     * @return
     */
    public static Set<UserPreferencesVO> getUserPreferences(String username, Set<String> preferencesKeys)
    {
        return UserUtils.getUserPreferences(username, preferencesKeys);
    }

    
    public static String getUserNameSimple(String sysId)
    {
        return UserUtils.getUserNameSimple(sysId);
    }
    
    
    /**
     * user by id
     *
     * @param sys_id
     * @return
     */
    public static UsersVO findUserById(String sys_id)
    {
        return UserUtils.findUserById(sys_id);
    }

    /**
     * user with roles by id
     *
     * @param sys_id
     * @return
     */
    public static UsersVO findUserWithRolesById(String sys_id)
    {
        return UserUtils.findUserWithRolesById(sys_id);
    }

    /**
     * This method returns a Set of roles that a user belongs to.
     *
     * @param userName
     * @return
     */
    public static Set<String> getUserRoles(String userName)
    {
        return UserUtils.getUserRoles(userName);
    }

    public static List<RolesVO> getUserRolesOnly(String username)
    {
        return UserUtils.getUserRolesOnly(username);
    }

    /**
     * Returns all the users with matching roles.
     *
     * @param <code>String</code> roles. Comma seperated <code>String</code> of
     *        role wraped in a single quote.
     * @param <code>String</code> whereClause.
     */
    public static List<UsersVO> getUsersWithRole(String roles, String whereClause)
    {
        return UserUtils.getUsersWithRole(roles, whereClause);
    }

    public static Set<String> getAllRolesWithAnd()
    {
        return UserUtils.getAllRolesWithAnd();
    }

    public static Set<String> getAllRoles()
    {
        return UserUtils.getAllRoles();
    }

    /**
     * Returns all group names on the current system
     *
     * @return Set containing all group names
     */
    public static Set<String> getAllGroups()
    {
        return UserUtils.getAllGroups();
    }

    /**
     * This method returns a Set of groups that a user belongs to.
     *
     * @param userName
     * @return
     */
    public static Set<String> getUserGroups(String userName)
    {
        return UserUtils.getUserGroups(userName);
    }

    public static List<GroupsVO> getUserGroupObjects(String username)
    {
        return UserUtils.getUserGroupObjects(username);
    }

    /**
     * This method retuns the User Names of all users belonging to a group.
     *
     * @param groupName
     * @return
     */
    public static Set<String> getUsersForGroup(String groupName)
    {
        return UserUtils.getUsersForGroup(groupName);
    }

    /**
     * This method retuns the User Names of all users belonging to a group.
     *
     * @param groupName
     * @return
     */
    public static Set<String> getUsersForGroup(String[] groups)
    {
        return UserUtils.getUsersForGroup(groups);
    }

    /**
     * This method returns all roles that make up a group
     *
     * @param groupName
     * @return
     */
    public static Set<String> getRolesForGroup(String groupName)
    {
        return UserUtils.getRolesForGroup(groupName);
    }

    /**
     * Check if the current user has the required roles
     *
     * @param task_sid
     *            - sys_id of the ActionTask
     * @throws Exception
     *             - If user does not have permissions, ActionTask is
     *             deactivated or missing
     */
    public static boolean checkTaskPermission(String task_sid, String username)
    {
        return UserUtils.checkTaskPermission(task_sid, username);
    }



    /**
     *
     * @param username
     * @return
     */
    public static boolean isLockedOut(String username)
    {
        return UserUtils.isLockedOut(username);
    }

    /**
     *
     * @param username
     * @param password
     * @return
     */
    public static boolean checkPassword(String username, String password)
    {
        return UserUtils.checkPassword(username, password);
    }

    /**
     *
     * @param username
     * @param password
     * @throws Exception
     */
    public static void updatePassword(String username, String password) throws Exception
    {
        UserUtils.updatePassword(username, password);
    }

    /**
     *
     * @param user
     */
    public static UsersVO saveUser(UsersVO user, String cloneOfUser, String username) throws Exception
    {
       return UserUtils.saveUser(user, cloneOfUser, username);
    }

    public static boolean isUserExist(String username)
    {
        return UserUtils.isUserExist(username);
    }

    /**
     * Get all user with roles.
     *
     * @param isLocked
     *            specify is want locked or unlocked.
     * @return
     */
    public static List<UsersVO> getAllUsers(boolean isLocked)
    {
        return UserUtils.getAllUsers(isLocked);
    }

    /**
     * Get All users with custom where clause.
     *
     * @param whereClause
     * @return
     */
    public static List<UsersVO> getAllUsers(String whereClause)
    {
        return UserUtils.getAllUsers(whereClause);
    }


    public static List<UsersVO> getUsers(String whereClause, String sortField, boolean asc, int limit, int offset)
    {
        return UserUtils.getUsers(whereClause, sortField, asc, limit, offset);
    }
    
    public static Map<String,Object> getUsersV2(QueryDTO query, String username)
    {
        return UserUtils.getUsersV2(query, username);
    }

    /**
     * Get All Roles with custom where clause.
     *
     * @param whereClause
     * @return
     */
    public static List<RolesVO> getAllRoles(String whereClause)
    {
        return UserUtils.getAllRoles(whereClause);
    }

    /**
     * Get All Groups with custom where clause.
     *
     * @param whereClause
     * @return
     */
    public static List<GroupsVO> getAllGroups(String whereClause)
    {
        return UserUtils.getAllGroups(whereClause);
    }

    /**
     *
     * @param isLocked
     * @return
     */
    public static Set<String> getAllUsernames(boolean isLocked)
    {
        return UserUtils.getAllUsernames(isLocked);
    }


    /**
     *
     * @param email
     * @return
     */
    public static UsersVO getUserByEmail(String email)
    {
        return UserUtils.getUserByEmail(email);
    }

    /**
     *
     * @param im
     * @return
     */
    public static UsersVO getUserByIM(String im)
    {
        return UserUtils.getUserByIM(im);
    }

    /**
     *
     * @param name
     * @return
     */
    public static String getProperty(String name)
    {
        return VariableUtil.getProperty(name);
    }

    /**
     *
     * @param name
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static String getProperty(String name, Map resultAttr)
    {
        return VariableUtil.getProperty(name, resultAttr);
    }

    /**
     *
     * @param name
     * @param value
     * @param module
     * @param encrypt
     */
    public static void setProperty(String name, String value, String module, boolean encrypt)
    {
        VariableUtil.setProperty(name, value, module, encrypt);
    }

    /**
     *
     * @param name
     */
    public static void removeProperty(String name)
    {
        VariableUtil.removeProperty(name);
    }

    /**
     *
     * @param userid
     * @param problemid
     * @return
     * @throws Exception 
     */
    @Deprecated
    public static String setActiveWorksheet(String userid, String problemid) throws Exception
    {
        return WorksheetUtil.setActiveWorksheet(userid, problemid, null, null);
    }
    
    /**
    *
    * @param userid
    * @param problemid
    * @param orgId
    * @param orgName
    * @return
    * @throws Exception 
    */
    public static String setActiveWorksheet(String userid, String problemid, String orgId, String orgName) throws Exception
    {
        return WorksheetUtil.setActiveWorksheet(userid, problemid, orgId, orgName);
    }
   
    /**
     *
     * @param userid
     * @return
     */
    @Deprecated
    public static String getActiveWorksheet(String userid)
    {
        return getActiveWorksheet(userid, null, null);
    }
    
    /**
    *
    * @param userid
    * @return
    */
    public static Collection<String> getAllActiveWorksheetIds(String userid)
    {
        return WorksheetUtil.getAllActiveWorksheetIds(userid);
    }
   
    /**
    *
    * @param userid
    * @param orgId
    * @param orgName
    * @return
    */
    public static String getActiveWorksheet(String userid, String orgId, String orgName)
    {
        return WorksheetUtil.getActiveWorksheet(userid, orgId, orgName);
    }
   
    /**
     *
     * @param userid
     * @return
     */
    @Deprecated
    public static boolean isActiveArchiveWorksheet(String userid)
    {
        return WorksheetUtil.isActiveArchiveWorksheet(userid, null, null);
    }

    /**
     * save list of wikidoc objects
     *
     * @param list
     * @param username
     */
    public static void saveListOfWikiDocs(Collection<WikiDocumentVO> list, String username)
    {
        StoreUtility.saveListOfWikiDocs(list, username);
    }

//    /**
//     * get the actiontask vo based on id
//     *
//     * @param task_sid
//     * @return
//     */
//    public static ResolveActionTaskVO getResolveActionTaskById(String task_sid)
//    {
//        return ResolveActionTaskUtil.getResolveActionTaskById(task_sid);
//    }

    /**
     *
     * @return
     */
    public static List<ResolveBlueprintVO> findAllResolveBlueprint()
    {
        return ResolveBlueprintUtil.findAllResolveBlueprint();
    }

    /**
     * get the list of parser based on method type
     *
     * @param methodType
     * @return
     */
    public static List<ResolveParserVO> getAllParserOfMethod(String methodType)
    {
        return ParserUtil.getAllParserOfMethod(methodType);
    }
    
    public static ResolveParserVO findResolveParser(String sysId, String name, String username)
    {
        return ParserUtil.findResolveParser(sysId, name, username);
    }

    public static ResolveParserVO findResolveParserWithoutRefs(String sysId, String name, String username)
    {
        return ParserUtil.findResolveParserWithoutRefs(sysId, name, username);
    }
    
    public static ResolveParserVO saveResolveParser(ResolveParserVO vo, String username) throws Exception
    {
        return ParserUtil.saveResolveParser(vo, username);
    }

    public static Set<String> findActiontaskReferencesForParser(Set<String> parserSysIds, String username)   throws Exception
    {
        return ParserUtil.findActiontaskReferencesForParser(parserSysIds, username);
    }
    
    public static void deleteResolveParserByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {
        ParserUtil.deleteResolveParserByIds(sysIds, deleteAll, username);
    }

    public static List<ResolvePreprocessVO> getResolvePreprocess(QueryDTO query, String username)
    {
        return PreprocessorUtil.getResolvePreprocess(query, username);
    }

    public static ResolvePreprocessVO findResolvePreprocess(String sysId, String name, String username)
    {
        return PreprocessorUtil.findResolvePreprocess(sysId, name, username);
    }
    
    public static Set<String> findActiontaskReferencesForPreprocessors(Set<String> preprocessorSysIds, String username)   throws Exception
    {
        return PreprocessorUtil.findActiontaskReferencesForPreprocessors(preprocessorSysIds, username);
    }

    public static void deleteResolvePreprocessByIds(String[] sysIds,  boolean deleteAll, String username) throws Exception
    {
        PreprocessorUtil.deleteResolvePreprocessByIds(sysIds, deleteAll, username);
    }

    public static ResolvePreprocessVO saveResolvePreprocess(ResolvePreprocessVO vo, String username)  throws Exception
    {
        return PreprocessorUtil.saveResolvePreprocess(vo, username);
    }

    /**
     * return list of parser template for a parser and its type
     *
     * @param parserSysId
     * @param parserName
     * @param type
     * @return
     */
    public static List<ResolveParserTemplateVO> getTemplatesForParser(String parserSysId, String parserName, String type)
    {
        return ParserUtil.getTemplatesForParser(parserSysId, parserName, type);
    }

    public static List<ResolveParserVO> getResolveParser(QueryDTO query, String username)
    {
        return ParserUtil.getResolveParser(query, username);
    }

    /**
     * executes the HQL  - update/insert/delete
     *
     * @param hql
     * @param username
     * @throws Exception
     */
    public static void executeHQLUpdate(String hql, String username) throws Exception
    {
        GeneralHibernateUtil.executeHQLUpdate(hql, username);
    }

    /**
     * executes a select and returns a list of objects
     *
     * @param hql
     * @return
     * @throws Exception
     */
    @Deprecated
    public static List<? extends Object> executeHQLSelect(String hql) throws Exception
    {
        return GeneralHibernateUtil.executeHQLSelect(hql, new HashMap<String, Object>());
    }
    
    /**
     * executes a select using specified paramters and returns a list of objects
     *
     * @param hql
     * @param queryParams
     * @return
     * @throws Exception
     */
    public static List<? extends Object> executeHQLSelect(String hql, Map<String, Object> queryParams) throws Exception
    {
        return GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
    }
    
    public static List<? extends Object> executeHQLSelect(String hql, int start, int limit) throws Exception
    {
        return GeneralHibernateUtil.executeHQLSelect(hql, start, limit);
    }

    public static List<Map<String, Object>> executeHQLSelect(QueryDTO queryDTO, int start, int limit) throws Exception
    {
        return GeneralHibernateUtil.executeHQLSelect(queryDTO, start, limit, false);
    }

    public static List<Map<String, Object>> executeHQLSelectForUI(QueryDTO queryDTO, int start, int limit) throws Exception
    {
        return GeneralHibernateUtil.executeHQLSelect(queryDTO, start, limit, true);
    }
    
    public static List<? extends Object> executeHQLSelectVO(QueryDTO queryDTO, int start, int limit, boolean forUI) throws Exception
    {
        return GeneralHibernateUtil.executeHQLSelectModel(queryDTO, start, limit, forUI);
    }

    /**
     * elects the master when the system comes up
     *
     * @param electionInterval
     * @param guid
     * @return
     */
    public static boolean electMaster(int electionInterval, String guid)
    {
        return GeneralHibernateUtil.electMaster(electionInterval, guid);
    }

    
    /**
     * elects the master when the system comes up for any component
     *
     * @param electionInterval
     * @param guid
     * @param component name
     * @return
     */
    public static boolean genericElectMaster(int electionInterval, String guid, String component)
    {
        return GeneralHibernateUtil.genericElectMaster(electionInterval, guid, component);
    }
    
    /**
     *
     * @param name
     * @param type
     * @return
     */
    public static ResolveEventHandlerVO getResolveEventHandler(String name, String type)
    {
        return RSControlUtil.getResolveEventHandler(name, type);
    }
    
    public static void insertResolveEvent(ResolveEventVO eventVO) throws Exception
    {
        ResolveEventUtil.insertResolveEvent(eventVO);
    }
    
    public static void clearAllResolveEventByValue(String eventValue) throws Exception
    {
        ResolveEventUtil.clearAllResolveEventByValue(eventValue);
    }
    
    public static ResolveEventVO getLatestEventByValue(String operation) throws Exception
    {
        return ResolveEventUtil.getLatestEventByValue(operation);
    }
    
    public static ResolveEventVO getLatestImpexEventByValue() throws Exception
    {
        return ResolveEventUtil.getLatestImpexEventByValue();
    }
    
    public static ResolveEventVO getLatestImpexEventByValue(long waitForSecs) throws Exception
    {
        return ResolveEventUtil.getLatestImpexEventByValue(waitForSecs);
    }
    
    public static Pair<ResolveEventVO, Boolean> getAndSetImpexEventValue(String operation, long waitForSecs, String moduleName,
    																	  String userName) throws Exception {
    	return ResolveEventUtil.getAndSetImpexEventValue(operation, waitForSecs, moduleName, userName);
    }
    
    /**
     * clears the cache
     *
     * @param region
     * @param sendMsg
     */
    public static void clearDBCacheRegion(DBCacheRegionConstants region, boolean sendMsg)
    {
        GeneralHibernateUtil.clearDBCacheRegion(region, sendMsg);
    }


    /**
     * get object from cache region
     *
     *  ONLY RSCONTROL USES THIS API
     *
     * @param region
     * @param accessId
     * @return
     */
    public static Object getCachedWikiDocumentForRSControl(DBCacheRegionConstants region, String accessId)
    {
        return GeneralHibernateUtil.getCachedWikiDocumentForRSControl(region, accessId);
    }

    /**
     * caches the object to a specific region
     *
     * @param region
     * @param entities
     * @param acessId
     * @param value
     */
    public static void cacheWikiDocumentForRSControl(String name)
    {
    	if (StringUtils.isNotBlank(name))
        {
            WikiDocument doc = WikiUtils.getWikiDocumentModel(null, name, "system", false);

            if (doc != null)
            {
				HibernateUtil.cacheDBObject(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION,
						new String[] { "com.resolve.persistence.model.WikiDocument" }, name, doc);
            }
        }
    }
    
    /**
     * save the registration
     *
     * @param vo
     * @param username
     * @return
     */
    public static ResolveRegistrationVO saveResolveRegistration(ResolveRegistrationVO vo, String username)
    {
        return ResolveRegistrationUtil.saveResolveRegistration(vo, username);
    }

    /**
     * find reg by status
     *
     * @param status
     * @return
     */
    public static List<ResolveRegistrationVO> findResolveRegistrationByStatus(String status)
    {
        return ResolveRegistrationUtil.findResolveRegistrationByStatus(status);
    }

    /**
     *
     *
     * @param esb
     * @param id
     */
    public static void setEsb(ESB esb, String id)
    {
        GeneralHibernateUtil.setEsb(esb, id);
    }

    /**
     * log an event
     *
     * @param msg
     * @param username
     */
    public static void logEvent(String msg, String username)
    {
    	if (StringUtils.isNotEmpty(msg))
        {
            ResolveEvent event = new ResolveEvent();
            event.setUValue(msg);

            SaveUtil.saveResolveEvent(event, username);
        }
    }

    /**
     * shutdown hibernate
     */
    public static void shutdownHibernate()
    {
        GeneralHibernateUtil.shutdownHibernate();
    }

    /**
     * initialize the persistence
     *
     * @param configSQL
     * @param configCAS
     * @param customMappingFileLocation
     * @param hibernateCfgLocation
     */
    public static void initPersistence(ConfigSQL configSQL, ConfigCAS configCAS, String customMappingFileLocation, String hibernateCfgLocation, boolean updateSchema)
    {
        GeneralHibernateUtil.initPersistence(configSQL, configCAS, customMappingFileLocation, hibernateCfgLocation, updateSchema);
    }

    public static void initPersistence(ConfigSQL configSQL, ConfigCAS configCAS, String customMappingFileLocation, String hibernateCfgLocation)
    {
        GeneralHibernateUtil.initPersistence(configSQL, configCAS, customMappingFileLocation, hibernateCfgLocation);
    }

    /**
     * update reg properties
     *
     * @param guid
     * @param properties
     * @throws Exception
     */
    public static void updateRegistrationProperties(String guid, Map<String, String> properties) throws Exception
    {
        ResolveRegistrationUtil.updateRegistrationProperties(guid, properties);
    }

    /**
     * find a reg property
     *
     * @param propertyName
     * @param guid
     * @return
     */
    public static ResolveRegistrationPropertyVO findResolveRegistrationProperty(String propertyName, String guid)
    {
        return ResolveRegistrationUtil.findResolveRegistrationProperty(propertyName, guid);
    }

    /**
     * find reg by GUID
     *
     * @param guid
     * @return
     */
    public static ResolveRegistrationVO findResolveRegistrationByGuid(String guid)
    {
        return ResolveRegistrationUtil.findResolveRegistrationByGuid(guid);
    }

    /**
     * find a row data in a table
     *
     * @param entityName
     * @param sysId
     * @return
     */
    public static Map<String, Object> findById(String entityName, String sysId)
    {
        return GeneralHibernateUtil.findById(entityName, sysId);
    }

    public static Map<String, Object> findFirstCustomTableRecord(String tableName, Map<String, Object> example)
    {
        return GeneralHibernateUtil.findFirstCustomTableRecord(tableName, example);
    }

    public static List<Map<String, Object>> getAllData(String tableName, int limit, int offset)
    {
        return GeneralHibernateUtil.getAllData(tableName, limit, offset);
    }

    public static List<Map<String, Object>> getAllData(String tableName)
    {
        return GeneralHibernateUtil.getAllData(tableName);
    }

    /**
     * exe a groovy script
     *
     * @param script
     * @param args
     * @param binding
     * @param rc
     * @param cacheable
     * @param isException
     * @return
     */
    public static Map<String, Object> executeGroovy(String script, String scriptName, Object[] args, Binding binding, ResultCode rc, boolean cacheable, boolean isException)
    {
        return GroovyUtil.executeGroovy(script, scriptName, args, binding, rc, cacheable, isException);
    }

    /**
     * get an instance of a connection
     *
     * @return
     * @throws Exception
     */
    public static Connection getConnection() throws Exception
    {
        return GeneralHibernateUtil.getConnection();
    }

    public static long getTotalRecordsIn(String model, String whereClause, List<Object> paramValueList)
    {
        return GeneralHibernateUtil.getTotalRecordsIn(model, whereClause, paramValueList);
    }

    public static List<ResolvePropertiesVO> getResolveProperties(QueryDTO query, String username)  throws Exception
    {
        return ActionTaskPropertiesUtil.getResolveProperties(query, username);
    }

    public static Set<String> getAllAvailablePropertyModules() throws Exception
    {
        return ActionTaskPropertiesUtil.getAllAvailablePropertyModules();
    }
    
    public static ResolvePropertiesVO findResolvePropertiesById(String sys_id, String module, String name, String username) throws Exception
    {
        return ActionTaskPropertiesUtil.findResolveProperties(sys_id, module, name, username);
    }
    
    public static ResolvePropertiesVO saveResolveProperties(ResolvePropertiesVO vo, String username) throws Exception
    {
        return ActionTaskPropertiesUtil.saveResolveProperties(vo, username);
    }

    public static void deleteResolvePropertiesByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {
        ActionTaskPropertiesUtil.deleteResolvePropertiesByIds(sysIds, deleteAll, username);
    }

    public static GatewayLogVO getGatewayLog(String sysId)
    {
        return LogUtil.getGatewayLog(sysId);
    }

    public static List<GatewayLogVO> getGatewayLogs(QueryDTO query)
    {
        return LogUtil.getGatewayLogs(query);
    }

    public static AlertLogVO getAlertLog(String sysId)
    {
        return LogUtil.getAlertLog(sysId);
    }

    public static List<AlertLogVO> getAlertLogs(QueryDTO query)
    {
        return LogUtil.getAlertLogs(query);
    }

    public static AuditLogVO getAuditLog(String sysId)
    {
        return LogUtil.getAuditLog(sysId);
    }

    public static List<AuditLogVO> getAuditLogs(QueryDTO query)
    {
        return LogUtil.getAuditLogs(query);
    }

    public static ResolveImpexLogVO getResolveImpexLog(String sysId)
    {
        return LogUtil.getResolveImpexLog(sysId);
    }

    public static List<ResolveImpexLogVO> getResolveImpexLogs(QueryDTO query)
    {
        return LogUtil.getResolveImpexLogs(query);
    }

    public static void deleteResolveImpexLogBySysId(List<String> sysIds, String username)
    {
        LogUtil.deleteResolveImpexLogBySysId(sysIds, username);
    }

    public static void deleteAuditLogBySysId(List<String> sysIds, String username)
    {
        LogUtil.deleteAuditLogBySysId(sysIds, username);
    }

    public static void deleteAlertLogBySysId(List<String> sysIds, String username)
    {
        LogUtil.deleteAlertLogBySysId(sysIds, username);
    }

    public static void deleteGatewayLogBySysId(List<String> sysIds, String username)
    {
        LogUtil.deleteGatewayLogBySysId(sysIds, username);
    }

    public static boolean isThisWorksheetInArchive(String problemId)
    {
        return WorksheetUtil.isThisWorksheetInArchive(problemId);
    }

    public static List<String> getActionTaskids(String[] actionNamespaceArr, String[] actionNameArr, String problemId, boolean isArchive)
    {
        return WorksheetUtil.getActionTaskids(actionNamespaceArr, actionNameArr, problemId, isArchive);
    }

//    public static String getActionTaskSys_Id(String actionTaskNamespace, String actionTaskName)
//    {
//        return ActionTaskUtil.getActionTaskSys_Id(actionTaskNamespace, actionTaskName);
//    }

    public static String getActionResultDetailFromArchive(String problem_id, String task_id, String node_id, String separator, Integer max, Integer offset)
    {
        return WorksheetUtil.getActionResultDetailFromArchive(problem_id, task_id, node_id, separator, max, offset);
    }

    public static boolean isProcessCompleted(String problemId, String wiki)
    {
        return WorksheetUtil.isProcessCompleted(problemId, wiki);
    }

    public static boolean isProcessCompletedForTask(String problemId, String[] actionNamespaceArr, String[] actionNameArr, 
    		ConfigSQL configSQL)
    {
        return WorksheetUtil.isProcessCompletedForTask(problemId, actionNamespaceArr, actionNameArr, configSQL);
    }

    public static ResolveSessionVO getResolveSession(String userid)
    {
        return ResolveSessionUtil.getResolveSession(userid);
    }

    public static int getTotalHqlCount(QueryDTO queryDTO) throws Exception
    {
        return GeneralHibernateUtil.getTotalHqlCount(queryDTO);
    }
    
    @SuppressWarnings("rawtypes")
    public static int getTotalHqlCount(Query query) throws Exception
    {
        return GeneralHibernateUtil.getTotalHqlCount(query);
    }

    public static int getTotalHqlCount(String sql) throws Exception
    {
        return GeneralHibernateUtil.getTotalHqlCount(sql);
    }

    public static Map<String, String> getHQLMetaData(String hql) throws Exception
    {
        return GeneralHibernateUtil.getHQLMetaData(hql);
    }

    public static Collection<String> getAllColumnsByTable(String tableModelName)
    {
        return GeneralHibernateUtil.getAllColumnsByTable(tableModelName);
    }

    public static String getColumnTypeString(String tableModelName, String colName)
    {
        return GeneralHibernateUtil.getColumnTypeString(tableModelName, colName);
    }


    public static List<String> getAllResolveBusinessRulesNames(String whereClause)
    {
        return GeneralHibernateUtil.getAllResolveBusinessRulesNames(whereClause);
    }

    public static List<String> getAllSystemScriptsName(String whereClause)
    {
        return GeneralHibernateUtil.getAllSystemScriptsName(whereClause);
    }


    public static String table2Class(String tableName)
    {
        return GeneralHibernateUtil.table2Class(tableName);
    }

    public static void purgeDBTable(String modelName, String username) throws Exception
    {
        GeneralHibernateUtil.purgeDBTable(modelName, username);
    }
    
    public static Map<String, String> deleteDBRow(String username, String modelClass, List<String> listId, boolean isCustomTable) throws Exception
    {
        return processDeleteDBRow(username, modelClass, listId, isCustomTable);
    }

    public static Map<String, String> deleteDBRow(String username, String modelClass, String sysId, boolean isCustomTable)
    {
        return processDeleteDBRow(username, modelClass, sysId, isCustomTable);
    }

    /*
    @Deprecated
    public static void deleteQuery(String modelClassName, String whereClause, String username) throws Exception
    {
        GeneralHibernateUtil.deleteQuery(modelClassName, whereClause, username);
    }*/
    
    public static void deleteQuery(String modelClassName, String paramName, String paramValue, String username) throws Exception
    {
        GeneralHibernateUtil.deleteQuery(modelClassName, paramName, paramValue, username);
    }
    
    public static void deleteAllQuery(String modelClassName, String paramName, String paramValue, String username) throws Exception
    {
        GeneralHibernateUtil.deleteAllQuery(modelClassName, paramName, paramValue, username);
    }
    
    public static void deleteQuery(String modelClassName, String inParamName, Collection<String> inParamValues, String username) throws Exception
    {
        GeneralHibernateUtil.deleteQuery(modelClassName, inParamName, inParamValues, username);
    }
    
    public static Map<String, String> updateDBRow(String username, String modelClass, Map<String, Object> persistedRowData, boolean isCustomTable)
    {
        return GeneralHibernateUtil.updateDBRow(username, modelClass, persistedRowData, isCustomTable);
    }
    
    @Deprecated
    public static List<Map<String, Object>> queryCustomTable(String tableName, String whereClause)
    {
        return GeneralHibernateUtil.queryCustomTable(tableName, whereClause, null);
    }

    public static List<Map<String, Object>> queryCustomTable(String tableName, String whereClause, Map<String, Object> whereClauseParams)
    {
        return GeneralHibernateUtil.queryCustomTable(tableName, whereClause, whereClauseParams);
    }
    
    public static List<Map<String, Object>> queryCustomTable(String tableName, String whereClause, int limit, int offset, boolean forUI) throws Exception
    {
        return GeneralHibernateUtil.queryCustomTable(tableName, whereClause, limit, offset, forUI);
    }

    
    public static Map<String, Object> insertOrUpdateCustomTable(String username, String tableName, Map<String, Object> data) throws Exception
    {
        return GeneralHibernateUtil.insertOrUpdateCustomTable(username, tableName, data);
    }

    public static byte[] getBytesFromBlob(Blob blob)
    {
        return  GeneralHibernateUtil.getBytesFromBlob(blob);
    }

    public static Blob getBlobFromBytes(byte[] content)
    {
        return GeneralHibernateUtil.getBlobFromBytes(content);
    }

    public static List<DefaultDataDTO> getCustomFormDefaultValues(String id, String viewName, String tableName, String userId, RightTypeEnum rightType, String recordSysId) throws Exception
    {
        return new ExtMetaFormLookup(id, viewName, tableName, userId, rightType, recordSysId).getDefaultValues();
    }

    public static RsFormDTO getMetaFormView(String id, String viewName, String tableName, String userId, RightTypeEnum rightType, String recordSysId, boolean formDefinitionOnly) throws Exception
    {
        ExtMetaFormLookup lookup = new ExtMetaFormLookup(id, viewName, tableName, userId, rightType, recordSysId);
        lookup.setFormDefinitionOnly(formDefinitionOnly);
        
        return lookup.getMetaFormViewX();
    }

    public static RsUIField convertMetaFieldToRsUIField(MetaAccessRightsVO accessRights, MetaFieldVO metaField, MetaFieldPropertiesVO metaFieldFormProperties, RightTypeEnum rightType, String userId, Set<String> userRoles, String recordSysId)
    {
        return ExtMetaFormLookup.convertMetaFieldToRsUIField(accessRights, metaField, metaFieldFormProperties, rightType, userId, userRoles, recordSysId);
    }

    
    public static CustomTableVO createCustomTable(RsCustomTableDTO customTable, String username, String timezone) throws Throwable
    {
        return new CreateCustomTable(customTable, username).create();
    }

    public static RsFormDTO saveMetaFormView(RsFormDTO view, String userid)  throws Exception
    {
        return new ExtSaveMetaFormView(view, userid).save();
    }

    public static Map<String, String> findMetaTableViewForUser(String tableName, String username, RightTypeEnum rightType) throws Exception
    {
        return CustomTableUtil.findMetaTableViewForUser(tableName, username, rightType);
    }

    public static String deleteFilters(String sysIds, String username)
    {
        return new DeleteMetaFilter(username).deleteFilters(sysIds);
    }

    public static boolean deleteMetaTableView(Set<String> setOfSysIds, String username)  throws Exception
    {
        return new DeleteMetaTableView(setOfSysIds, username).delete();
    }

    
    public static void deleteMetaForm(List<String> ids, String username) throws Throwable
    {
        new DeleteMetaForm().delete(ids, username);
    }

    public static List<MetaFormViewVO> getMetaFormViews(QueryDTO query, String username) throws Exception
    {
        return CustomFormUtil.getMetaFormViews(query, username);
    }

    public static List<MetaFormViewVO> getAllMetaForms()
    {
        return CustomFormUtil.getAllMetaForms();
    }

    public static void deleteMetaFormByName(String formViewName) throws Throwable
    {
        new DeleteMetaForm().deleteByName(formViewName, "admin");
    }

    public static Map<String, String> getUserSocialPreferences(String username)
    {
        return SocialUtil.getUserSocialPreferences(username);
    }


    public static String executeScript(String name, Map<String, Object> params)
    {
        return ExecuteSysScriptUtil.executeScript(name, params);
    }

    public static String getArchiveContentForTaskDetail(String executeid, String taskresultid)
    {
        return WorksheetUtil.getArchiveContentForTaskDetail(executeid, taskresultid);
    }

    public static String getRawArchiveContentForTaskDetail(String executeid, String executeresultid)
    {
        return WorksheetUtil.getRawArchiveContentForTaskDetail(executeid, executeresultid);
    }


    public static ArchiveWorksheetVO findByExample(ArchiveWorksheetVO example)
    {
        return ArchiveUtil.findByExample(example);
    }

    public static WikiUser getUserRolesAndGroups(String userName)
    {
        return UserUtils.getUserRolesAndGroups(userName);
    }

    public static boolean isCustomTable(String tableName)
    {
        return StoreUtility.isCustomTable(tableName);
    }

    public static String evaluateViewLookup(String lookupString, String username)  throws Exception
    {
        return StoreUtility.evaluateViewLookup(lookupString, username);
    }

    public static Map<String, Object> findTeamByIdOrName(String sysId, String name)
    {
        return SocialUtil.findTeamByIdOrName(sysId, name);
    }

    public static String getActionResultDetail(String problem_id, String task_id, String node_id)
    {
        return StoreUtility.getActionResultDetail(problem_id, task_id, node_id);

    } // getActionResultDetail

    public static String getActionResultDetail(String problem_id, String task_id, String node_id, String separator, Integer max, Integer offset)
    {
        return StoreUtility.getActionResultDetail(problem_id, task_id, node_id, separator, max, offset);
    }



//    public static void executeSystemExecutor(String className, String apiName, Map<String, Object> params)
//    {
//        GeneralHibernateUtil.executeSystemExecutor(className, apiName, params);
//    }

    /**
     *
     * Mandatory values are
     *
     * BUTTON_NAME
     * FORM_VIEW_NAME
     * USERNAME
     * LIST OF NAME VALUE PAIRS THAT MAPS TO <colname>:<value> for the custom table mapped to this form
     *
     * @param params
     */
    public static Map<String, Object> submitForm(Map<String, Object> params)
    {
        //mandatory params
        String controlItemName = (String) params.get(HibernateConstants.EXT_FORM_BUTTON_NAME_KEY);
        String viewName = (String) params.get(HibernateConstants.EXT_FORM_VIEW_NAME_KEY);
        String timezone = GMTDate.getDefaultTimezone();
        String username = (String)params.get(Constants.HTTP_REQUEST_USERNAME);
        FormData dbRowData = prepareFormData(params);

        //prepare the DTO
        FormSubmitDTO formSubmitDTO = new FormSubmitDTO();
        formSubmitDTO.setControlItemName(controlItemName);
        formSubmitDTO.setViewName(viewName);
        formSubmitDTO.setTimezone(timezone);
        formSubmitDTO.setUserId(username);
        formSubmitDTO.setDbRowData(dbRowData);
        Map<String, Object> updatedData = null;
        
        try
        {
            updatedData = ServiceHibernate.executeFormAndGetRowData(formSubmitDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error("error in executing form ", e);
        }//formSubmit.getDBRowData();

        return (updatedData!=null) ? updatedData : new HashMap<String, Object>();
    }

    public static FormData prepareFormData(Map<String, Object> params)
    {
        Map<String, Object> dbValues = new HashMap<String, Object>();
        dbValues.putAll(params);
        dbValues.remove(HibernateConstants.EXT_FORM_BUTTON_NAME_KEY);
        dbValues.remove(HibernateConstants.EXT_FORM_VIEW_NAME_KEY);
        dbValues.remove(Constants.HTTP_REQUEST_USERNAME);

        FormData data = new FormData();
        data.setData(dbValues);
        return data;
    }

    public static Map<String, String> executeForm(FormSubmitDTO formSubmitDTO, String userId) throws Exception
    {
        ExtFormSubmit form = new ExtFormSubmit(formSubmitDTO, userId);
        Map<String, String> result = form.executeFormProcessSequence();

        return result;
    }

    public static Map<String, Object> executeFormAndGetRowData(FormSubmitDTO formSubmitDTO, String userId) throws Exception
    {
        ExtFormSubmit form = new ExtFormSubmit(formSubmitDTO, userId);
        form.executeFormProcessSequence();

        Map<String, Object> result = form.getDBRowData();

        return result;
    }

    public static List<PropertiesVO> getProperties(QueryDTO query, String username)
    {
        return PropertiesUtil.getProperties(query, username);
    }

    public static Map<String, Object> getUIEditableMetricThresholds(QueryDTO query)
    {
        return MetricUtil.getUIEditableMetricThresholds(query);
    }

    public static List<MetricThresholdVO> getMetricThreshold(QueryDTO query)
    {
        return MetricUtil.getMetricThreshold(query);
    }

    public static List<MetricThresholdVO> getMetricThreshold(QueryDTO query, String[] sysIds)
    {
        return MetricUtil.getMetricThreshold(query, sysIds);
    }

    public static List<MetricThresholdVO> getListOfDeployedMetricServers(QueryDTO query)
    {
        return MetricUtil.getListOfDeployedMetricServers(query);
    }

    public static MetricThresholdVO findMetricThresholdById(String sys_id)
    {
        return MetricUtil.findMetricThresholdById(sys_id);
    }
    
    public static Map<String, Object> findMetricThresholdByIdAndHighestEditableVersion(String sys_id)
    {
        return MetricUtil.findMetricThresholdByIdAndHighestEditableVersion(sys_id);
    }
    
    public static Map<String, Object> getRevisionWithVersionCount(String module, String name)
    {
    	return MetricUtil.getRevisionWithVersionCount(module, name);
    }

    public static MetricThresholdVO findMetricThreshold(String module, String name, Integer version, boolean includeMCP)
    {
        return MetricUtil.findMetricThreshold(module, name, version, includeMCP);
    }
    
    public static ResolveMCPLoginVO findMCPLogin(String group, String cluster)
    {
        return MetricUtil.findMCPLoginVO(group, cluster);
    }
    
    public static List<String> getRegisteredClusters(String group)
    {
        return MetricUtil.getRegisteredClusters(group);
    }
    
    public static MetricThresholdVO saveMetricThreshold(MetricThresholdVO vo, String username)
    {
        return MetricUtil.saveMetricThreshold(vo, username);
    }

    public static ResolveMCPLoginVO saveMCPLogin(ResolveMCPLoginVO vo, String username)
    {
        return MetricUtil.saveMCPLogin(vo, username);
    }

    public static MetricThresholdVO saveVersionMetricThreshold(MetricThresholdVO vo, String username)
    {
        return MetricUtil.saveVersionMetricThreshold(vo, username);
    }

    public static MetricThresholdVO copyMetricThreshold(MetricThresholdVO vo, String username)
    {
        return MetricUtil.copyMetricThreshold(vo, username);
    }

    public static void mcpDeployMetricThreshold(String id, String deployData)
    {
        MetricUtil.mcpDeployMetricThreshold(id, deployData);
    }
    
    public static void mcpUndeployMetricThreshold(String id, String deployData)
    {
        MetricUtil.mcpUndeployMetricThreshold(id, deployData);
    }
    
    public static void mcpAllUndeployMetricThreshold(String id, String deployData)
    {
        MetricUtil.mcpAllUndeployMetricThreshold(id, deployData);
    }
    
    public static int findHighestVersionNumber(String namespace, String ruleName, boolean includeMCP)
    {
        return MetricUtil.findHighestVersionNumber(namespace, ruleName, includeMCP);
    }
    public static void deleteMetricThresholdByIdsFromUI(String[] sysIds, String username) throws Exception
    {
       MetricUtil.deleteMetricThresholdByIds(sysIds, username, false);
    }

    public static List<MetricThresholdVO> getListOfMetricServers(QueryDTO query)
    {
        return MetricUtil.getListOfMetricServers(query);
    }

    /**
     * This method loads the default metric units threshold values from the
     * threshold.properties file into the metric_threshold database
     *
     * @param fileName
     *            threshold.properties
     * @throws Exception
     */
    public static void initMetricThresholds(String fileName) throws Exception
    {
        MetricUtil.initMetricThresholds(fileName);
    } // initMetricThresholds

    public static Map<String, Map<String, Object>> deployMetricThreshold(String[] guids, String[] sys_ids, String[] highs, String[] lows, String username)
    {
        return MetricUtil.deployMetricThreshold(guids, sys_ids, highs, lows, username);
    } //deployMetricThreshold

    public static boolean addAndDeployMetricThreshold(String guid, String metricname, String metricgroup, String metrichigh, String metriclow, String metrictype, String username, String uActive, String uAction, String uSource, String uRuleModule, String uRuleName)
    {
        return MetricUtil.addAndDeployMetricThreshold(guid, metricname, metricgroup, metrichigh, metriclow, metrictype, username, uActive, uAction, uSource, uRuleModule, uRuleName);
    } //addAndDeployMetricThreshold

    public static void processThresholdValuesInMap(MetricThresholdVO metric, Map<String, MetricThresholdType> thresholdProperties)
    {
       MetricUtil.processValuesInMap(metric, thresholdProperties);
    }


//    public static ConfigActiveDirectoryVO findConfigActiveDirectoryByDomainName(String domainName)
//    {
//        return ConfigActiveDirectoryUtil.findConfigActiveDirectoryByDomainName(domainName);
//    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Active Directory CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static List<ConfigActiveDirectoryVO> getConfigActiveDirectory(QueryDTO query, String username)
    {
        return ConfigActiveDirectoryUtil.getConfigActiveDirectory(query, username);
    }

    public static ConfigActiveDirectoryVO findConfigActiveDirectoryById(String sys_id, String username)
    {
        return ConfigActiveDirectoryUtil.findConfigActiveDirectoryById(sys_id, username);
    }

    public static ConfigActiveDirectoryVO saveConfigActiveDirectory(ConfigActiveDirectoryVO vo, String username)
    {
        return ConfigActiveDirectoryUtil.saveConfigActiveDirectory(vo, username);
    }

    public static void deleteConfigActiveDirectoryByIds(String[] sysIds, String username) throws Exception
    {
        ConfigActiveDirectoryUtil.deleteConfigActiveDirectoryByIds(sysIds, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Organization CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static List<OrganizationVO> getOrganization(QueryDTO query)
    {
        return OrganizationUtil.getOrganization(query);
    }

    public static OrganizationVO findOrganizationById(String sys_id)
    {
        return OrganizationUtil.findOrganizationById(sys_id);
    }

    public static OrganizationVO saveOrganization(OrganizationVO vo, String username)
    {
        return OrganizationUtil.saveOrganization(vo, username);
    }

    public static void disableEnableOrganizationByIds(String[] sysIds, boolean disable, String username) throws Exception
    {
        OrganizationUtil.disableEnableOrganizationByIds(sysIds, disable, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //LDAP Config CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<ConfigLDAPVO> getConfigLDAP(QueryDTO query, String username)
    {
        return ConfigLDAPUtil.getConfigLDAP(query, username);
    }

    public static ConfigLDAPVO findConfigLDAPById(String sys_id, String username)
    {
        return ConfigLDAPUtil.findConfigLDAPById(sys_id, username);
    }

    public static ConfigLDAPVO saveConfigLDAP(ConfigLDAPVO vo, String username)
    {
        return ConfigLDAPUtil.saveConfigLDAP(vo, username);
    }

    public static void deleteConfigLDAPByIds(String[] sysIds, String username) throws Exception
    {
        ConfigLDAPUtil.deleteConfigLDAPByIds(sysIds, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Social Process CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get the list of process
//    public static List<SocialProcessDTO> getAllSocialProcesses(QueryDTO query, String username) throws Exception
//    {
//        return SocialAdminUtil.getAllSocialProcesses(query, username);
//    }

    public static Map<String, Object> getSocialComponents(QueryDTO query, String username) throws Exception
    {
        return SocialAdminUtil.getSocialComponents(query, username);
    }

    //save/update for a process
    public static SocialProcessDTO saveProcess(SocialProcessDTO process, String username, Boolean validate) throws Exception
    {
        return SocialAdminUtil.saveProcess(process, username, validate);
    }

    //get details of 1 process
    public static SocialProcessDTO getProcess(String processSysId, String username)
    {
        return SocialAdminUtil.getProcess(processSysId, username);
    }
    
    public static SocialProcessDTO getProcessByName(String processName, String userName)
    {
        return SocialAdminUtil.getProcessByName(processName, userName);
    }

    //delete the process with these sysIds
    public static void deleteProcesses(String[] processSysIds, String username) throws Exception
    {
        SocialAdminUtil.deleteProcesses(processSysIds, username);
    }

    public static SocialTeamDTO getTeam(String teamSysId, String username)
    {
        return SocialAdminUtil.getTeam(teamSysId, username);
    }
    
    public static SocialTeamDTO getTeamByName(String teamname, String username)
    {
        return SocialAdminUtil.getTeamByName(teamname, username);
    }

    public static Set<SocialDTO> getContainerComponents (String compSysId, String compTYpe, String userName) throws Exception
    {
        return SocialAdminUtil.getContainerComponents(compSysId, compTYpe, userName);
    }

    public static String removeContainerComponents (String containerType, String containerId, List<SocialDTO> components, String userName)  throws Exception
    {
        return SocialAdminUtil.removeContainerComponents (containerType, containerId, components, userName);
    }

    @SuppressWarnings("rawtypes")
    public static ResponseDTO getListOfComponentsToBeAdded(QueryDTO query, String parentName, String parentId, String userName)
    {
        return SocialAdminUtil.getListOfComponentsToBeAdded(query, parentName, parentId, userName);
    }

    public static List<String> addComponentToContainer (String containerType, String containerId, List<SocialDTO> components, String userName,
                    Boolean addUsersToGroup, Boolean validate) throws Exception
    {
        return SocialAdminUtil.addComponentToContainer (containerType, containerId, components, userName, addUsersToGroup, validate);
    }

    public static SocialTeamDTO saveTeam(SocialTeamDTO team, String username, Boolean validate) throws Exception
    {
        return SocialAdminUtil.saveTeam(team, username, validate);
    }

    public static void deleteTeam(String[] teamSysIds, String username) throws Exception
    {
        SocialAdminUtil.deleteTeam(teamSysIds, username);
    }


    public static SocialForumDTO getForum(String forumSysId, String username)
    {
        return SocialAdminUtil.getForum(forumSysId, username);
    }
    
    public static SocialForumDTO getForumByName(String forumName, String userName)
    {
        return SocialAdminUtil.getForumByName(forumName, userName);
    }

    public static SocialRssDTO getRss(String rssSysId, String username)
    {
        return SocialAdminUtil.getRss(rssSysId, username);
    }

    public static SocialForumDTO saveForum(SocialForumDTO forum, String username, Boolean validate) throws Exception
    {
        return SocialAdminUtil.saveForum(forum, username, validate);
    }

    public static void deleteForum(String[] forumSysIds, String username)  throws Exception
    {
        SocialAdminUtil.deleteForum(forumSysIds, username);
    }

    public static SocialRssDTO saveRss(SocialRssDTO rss, String username, Boolean validate) throws Exception
    {
        return SocialAdminUtil.saveRss(rss, username, validate);
    }
    
    public static SocialRssDTO getRssByName(String rssName, String userName)
    {
        return SocialAdminUtil.getRssByName(rssName, userName);
    }

    public static void updateFeedUpdatedDateForRss(SocialRssDTO vo, String username)
    {
        SocialAdminUtil.updateFeedUpdatedDateForRss(vo, username);
    }

    public static void deleteRss(String[] rssSysIds, String username)  throws Exception
    {
        SocialAdminUtil.deleteRss(rssSysIds, username);
    }

    public static SocialDTO getDefaultAccessRights (String compType)
    {
        return SocialAdminUtil.getdefaultAccessRights(compType);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Business Rule CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get the list
    public static List<ResolveBusinessRuleVO> getResolveBusinessRules(QueryDTO query, String username)
    {
        return BusinessRuleUtil.getResolveBusinessRules(query, username);
    }

    //save/update
    public static ResolveBusinessRuleVO saveBusinessRule(ResolveBusinessRuleVO vo, String username) throws Exception
    {
        return BusinessRuleUtil.saveBusinessRule(vo, username);
    }

    //get details of 1 record
    public static ResolveBusinessRuleVO findBusinessRuleByIdOrName(String sysId, String brName, String username)
    {
        return BusinessRuleUtil.findBusinessRuleByIdOrName(sysId, brName, username);
    }

    //delete records with these sysIds
    public static void deleteBusinessRulesByIds(String[] sysIds, String username)  throws Exception
    {
        BusinessRuleUtil.deleteBusinessRulesByIds(sysIds, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // System Script CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get the list
    public static List<ResolveSysScriptVO> getResolveSysScripts(QueryDTO query, String username)
    {
        return SystemScriptUtil.getResolveSysScripts(query, username);
    }

    //save/update
    public static ResolveSysScriptVO saveResolveSysScript(ResolveSysScriptVO vo, String username) throws Exception
    {
        return SystemScriptUtil.saveResolveSysScript(vo, username);
    }

    //get details of 1 record
    public static ResolveSysScriptVO findResolveSysScriptByIdOrName(String sysId, String ssName, String username)
    {
        return SystemScriptUtil.findResolveSysScriptByIdOrName(sysId, ssName,  username);
    }

    //delete records with these sysIds
    public static void deleteResolveSysScriptByIds(String[] sysIds, String username)  throws Exception
    {
        SystemScriptUtil.deleteResolveSysScriptByIds(sysIds, username);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Wiki Lookup CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get the list
    public static List<ResolveWikiLookupVO> getResolveWikiLookup(QueryDTO query, String username)
    {
        return WikiLookupUtil.getResolveWikiLookup(query, username);
    }

    //get details of 1 record
    public static ResolveWikiLookupVO findResolveWikiLookupById(String sys_id, String username)
    {
        return WikiLookupUtil.findResolveWikiLookupById(sys_id, username);
    }

    //save/update
    public static ResolveWikiLookupVO saveResolveWikiLookup(ResolveWikiLookupVO vo, String username)  throws Exception
    {
        return WikiLookupUtil.saveResolveWikiLookup(vo, username);
    }

    //delete records with these sysIds
    public static void deleteResolveWikiLookupByIds(String[] sysIds, String username) throws Exception
    {
        WikiLookupUtil.deleteResolveWikiLookupByIds(sysIds, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Wiki Template CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get the list
    public static List<WikiDocumentMetaFormRelVO> getWikiTemplate(QueryDTO query, String username)
    {
        return WikiTemplateUtil.getWikiTemplate(query, username);
    }

    //get details of 1 record
    public static WikiDocumentMetaFormRelVO findWikiTemplateById(String sys_id, String username)
    {
        return WikiTemplateUtil.findWikiTemplateByIdOrByName(sys_id, null, username);
    }

    //save/update
    public static WikiDocumentMetaFormRelVO saveWikiTemplate(WikiDocumentMetaFormRelVO vo, String username) throws Exception
    {
        return WikiTemplateUtil.saveWikiTemplate(vo, username);
    }

    //delete records with these sysIds
    public static void deleteWikiTemplateByIds(String[] sysIds, String username) throws Exception
    {
        WikiTemplateUtil.deleteWikiTemplateByIds(sysIds, username);
    }

    //copy of the template
    public static WikiDocumentMetaFormRelVO copyTemplate(String fromTemplate, String toTemplate, String username) throws Exception
    {
        return WikiTemplateUtil.copyTemplate(fromTemplate, toTemplate, username);
    }

    //rename wiki template
    public static WikiDocumentMetaFormRelVO renameTemplate(String fromTemplate, String toTemplate, String username) throws Exception
    {
        return WikiTemplateUtil.renameTemplate(fromTemplate, toTemplate, username);
    }
    
    //create wiki with template
    public static void createWikiUsingTemplate(Map<String, Object> params, String username) throws Exception
    {
        WikiTemplateUtil.createWikiUsingTemplate(params, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Users/Roles/Groups CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get list of users based on Query
    public static List<UsersVO> getUsers(QueryDTO query, String username)
    {
        return UserUtils.getUsers(query, username);
    }

    public static UsersVO getUserById(String sysId)
    {
       return UserUtils.getUserById(sysId);
    }
    
    public static UsersVO getUserByNameNoCache(String username)
    {
       return UserUtils.getUserByNameNoCache(username);
    }

    public static UsersVO getUserByNameNoReference(String username)
    {
       return UserUtils.getUserByNameNoReference(username);
    }
    
    public static UsersVO getUser(String sysId, String username)
    {
       return UserUtils.getUser(sysId, username);
    }

    public static Collection<String> deleteUsersByIdsAndQuery(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        return UserUtils.deleteUsersByIdsAndQuery(sysIds, query, username);
    }

    //roles
    public static List<RolesVO> getRoles(QueryDTO query, String username)
    {
        return UserUtils.getRoles(query, username);
    }

    public static RolesVO getRoleById(String sysId, String username)
    {
       return UserUtils.getRole(sysId, null, username);
    }

    public static Collection<String> deleteRolesByIdsAndQuery(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        return UserUtils.deleteRolesByIdsAndQuery(sysIds, query, username);
    }
    
    public static RolesVO saveRole(String roleName, String description, String username) throws Exception
    {
        RolesVO vo = getRole(null, roleName);
        if (vo == null)
        {
            vo = new RolesVO();
            vo.setUName(roleName);
        }
        vo.setUDescription(description);
        
        vo = saveRole(vo, username);
        
        return vo;
    }


    public static RolesVO saveRole(RolesVO vo, String username) throws Exception
    {
        return UserUtils.saveRole(vo, username);
    }

    //groups
    public static List<GroupsVO> getGroups(QueryDTO query, String username)
    {
        return UserUtils.getGroups(query, username, true);
    }

    public static GroupsVO getGroupById(String sysId, String username)
    {
        return UserUtils.getGroupNoCache(sysId, null, username);
    }

    public static Collection<String> deleteGroupsByIdsAndQuery(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        return UserUtils.deleteGroupsByIdsAndQuery(sysIds, query, username);
    }

    public static GroupsVO saveGroup(GroupsVO vo, String username)  throws Exception
    {
        return UserUtils.saveGroup(vo, username);
    }

    public static Collection<String> addRolesToUsers(Set<String> userSysIds, Set<String> roleSysIds, String username)  throws Exception
    {
        return UserUtils.addRolesToUsers(userSysIds, roleSysIds, username);
    }

    public static Collection<String> addGroupsToUsers(Set<String> userSysIds, Set<String> groupSysIds, String username) throws Exception
    { 
        return UserUtils.addGroupsToUsers(userSysIds, groupSysIds, username);
    }

    public static void addUsersToGroup(String groupSysId, String groupName, List<String> usernames, String username) throws Exception
    {
        UserUtils.addUsersToGroup(groupSysId, groupName, usernames, username);
    }

    public static List<ComboboxModelDTO> getAllUsersForMentions(String typeAheadString)
    {
        return UserUtils.getAllUsersForMentions(typeAheadString);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Apps CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get list of users based on Query
    public static List<ResolveAppsVO> getResolveApps(QueryDTO query, String username)
    {
        return AppsUtil.getResolveApps(query, username);
    }

    public static ResolveAppsVO findResolveAppsById(String sys_id, String username)
    {
        return AppsUtil.findResolveAppsById(sys_id, username);
    }

    public static void deleteResolveAppsByIds(String[] sysIds, QueryDTO query, String username, boolean purge) throws Exception
    {
        AppsUtil.deleteResolveAppsByIds(sysIds, query, username, purge);
    }

    public static ResolveAppsVO saveResolveApps(ResolveAppsVO vo, String username) throws Exception
    {
        return AppsUtil.saveResolveApps(vo, username);
    }

    public static boolean doesUserHaveRightsForController(String controllerName, String username)
    {
        return AppsUtil.doesUserHaveRightsForController(controllerName, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // List Resolve Registration CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get list of users based on Query
    public static List<ResolveRegistrationVO> getResolveRegistration(QueryDTO query, String username)
    {
        return ResolveRegistrationUtil.getResolveRegistration(query, username);
    }

    public static ResolveRegistrationVO findResolveRegistrationById(String sys_id, String username)
    {
        return ResolveRegistrationUtil.findResolveRegistrationById(sys_id, username);
    }

    public static void deleteResolveRegistrationByIds(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        ResolveRegistrationUtil.deleteResolveRegistrationByIds(sysIds, query, username);
    }
    
    public static Map<String, String> getHostInfo(String hostName, String username) throws Exception
    {
        return ResolveRegistrationUtil.getHostInfo(hostName, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Job Scheduler CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get list of users based on Query
    public static List<ResolveCronVO> getResolveCronJobs(QueryDTO query, String username)
    {
        return CronUtil.getResolveCronJobs(query, username);
    }

    public static ResolveCronVO findCronJob(String sysId, String name, String username)
    {
        return CronUtil.findCronJob(sysId, name, username);
    }

    public static void deleteCronJobsById(String[] sysIds, QueryDTO query, String username)
    {
        CronUtil.deleteCronJobsById(sysIds, query, username);
    }

    public static ResolveCronVO saveResolveCron(ResolveCronVO vo, String username)  throws Exception
    {
        return CronUtil.saveResolveCron(vo, username);
    }

    public static void activateDeactivateCronJob(String[] ids, boolean activateJob, String username)
    {
        CronUtil.activateDeactivateCronJob(ids, activateJob, username);
    }

    public static List<ResolveCronVO> findAllActiveCronJobs(boolean onlyActiveJobs)
    {
        return CronUtil.findAllActiveCronJobs(onlyActiveJobs);
    }

    public static void deleteCronByName(String name, String username)
    {
        CronUtil.deleteCronByName(name, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Menu Definition CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get list of users based on Query
    public static List<SysAppApplicationVO> getMenuDefinitions(QueryDTO query, String username)
    {
        return MenuDefinitionUtil.getMenuDefinitions(query, username);
    }

    public static SysAppApplicationVO findMenuDefinition(String sys_id, String appName, String username)
    {
        return MenuDefinitionUtil.findMenuDefinition(sys_id, appName, username);
    }

    public static void deleteMenuDefinition(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        MenuDefinitionUtil.deleteMenuDefinition(sysIds, query, username);
    }

    public static void setMenuDefinitionSequence(String[] menuDefinitionSysIds, String username)
    {
        MenuDefinitionUtil.setMenuDefinitionSequence(menuDefinitionSysIds, username);
    }

    public static SysAppApplicationVO saveSysAppApplication(SysAppApplicationVO vo, String username) throws Exception
    {
        return MenuDefinitionUtil.saveSysAppApplication(vo, username);
    }

    public static MenuDTO getMenu(String username) throws Exception
    {
        return MenuServiceUtil.getMenu(username);
    }

    public static List<SysAppModuleVO> getMenuItems(QueryDTO query, String userName)
    {
        return MenuServiceUtil.getMenuItems(query, userName);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ResolveController CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //get list of users based on Query
    public static List<ResolveControllersVO> getResolveControllers(QueryDTO query, String username)
    {
        return ResolveControllerUtil.getResolveControllers(query, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Social DB operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static List<SocialPostAttachmentVO> getAllPostAttachments(String username) throws Exception
    {
        return SocialAttachmentUtil.getAllPostAttachments(username);
    }
    
    public static SocialPostAttachmentVO findSocialPostAttachmentById(String sys_id, String username)
    {
        return SocialAttachmentUtil.findSocialPostAttachmentById(sys_id, username);
    }

    public static void updateSocialPostAttachmentActiveFlag(String postid, boolean active, String username) throws Exception
    {
        SocialAttachmentUtil.updateSocialPostAttachmentActiveFlag(postid, active, username);
    }

    public static SocialPostAttachmentVO saveSocialPostAttachment(SocialPostAttachmentVO vo, String username)
    {
        return SocialAttachmentUtil.saveSocialPostAttachment(vo, username);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // RSS update operation
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void updateRssWithId(String sysId, String username)
    {
        RSSReader.update(sysId, username);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Action Task operation
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ResponseDTO<ResolveActionTaskVO> getResolveActionTasks(QueryDTO query, String username) throws Exception
    {
        return ActionTaskUtil.getResolveActionTasks(query, username);
    }
    
    public static ResolveActionTaskVO getActionTaskFromIdWithReferences(String sysId, String fullName, String username, boolean validate) throws Exception
    {
        return ActionTaskUtil.getActionTaskWithReferences(sysId, fullName, username, validate);
    }
    
    public static ResolveActionTaskVO getActionTaskFromIdWithReferences(String sysId, String fullName, String username, boolean validate, String version) throws Exception
    {
        return ActionTaskUtil.getActionTaskWithReferences(sysId, fullName, username, validate, version);
    }
    
    public static ResolveActionTaskVO getActionTaskFromIdWithReferences(String sysId, String fullName, String username)
    {
        return ActionTaskUtil.getActionTaskWithReferences(sysId, fullName, username);
    }

    public static ResolveActionTaskVO getActionTaskWithReferencesExceptGraph(String sysId, String fullName, String username)
    {
        return ActionTaskUtil.getActionTaskWithReferencesExceptGraph(sysId, fullName, username);
    }
    
    public static ResolvePropertiesVO getPropertyFromIdWithReferences(String sysId, String fullName, String username) throws Exception
    {
        return ActionTaskPropertiesUtil.getPropertyWithReferences(sysId, fullName, username);
    }
    
    public static MetaFormViewVO getCustomFormFromIdWithReferences(String sysId, String fullName, String username) throws Exception
    {
        return CustomFormUtil.getCustomFormWithReferences(sysId, fullName, username);
    }
    
    public static List<ActionTaskTreeDTO> getActionTaskTree(QueryDTO queryDTO, String username) throws Exception
    {
        return ActionTaskUtil.getActionTaskTree(queryDTO, username);
    }

    public static ResolveActionTaskVO getResolveActionTask(String task_sid, String atName, String username)
    {
        return ActionTaskUtil.getResolveActionTask(task_sid, atName, username);
    }
    
    public static boolean hasRightsToActionTask(String actiontaskSysId, String actiontaskFullName, String username, RightTypeEnum rightType)
    {
        return ActionTaskUtil.hasRightsToActionTask(actiontaskSysId, actiontaskFullName, username, rightType);
    }
    
    public static String getActionIdFromFullname(String fullname, String username)
    {
        return ActionTaskUtil.getActionIdFromFullname(fullname, username);
    }
    
    public static String getActionTaskNameFromId(String sysId, String username)
    {
        return ActionTaskUtil.getActionTaskNameFromId(sysId, username);
    }
    
    public static Map<String, Object> findNamespaceForImpex(QueryDTO queryDTO, String username)
    {
        return ActionTaskUtil.findNamespaceForImpex(queryDTO, username);
    }

    public static boolean isActionTaskExist(String sysId, String fullname)
    {
        return ActionTaskUtil.isActionTaskExist(sysId, fullname);
    }
    
    public static Map<String, Object> getAllActionTasks(QueryDTO queryDTO)
    {
        return ActionTaskUtil.getAllActionTasks(queryDTO);
    } 

    public static ResolveActionTaskMockDataVO getActionTaskMockDataVO(String actionTaskId, String mockName)
    {
        return ActionTaskUtil.getActionTaskMockDataVO(actionTaskId, mockName);
    }
    
    public static List<String> getAllActionTaskNames(/*String whereClause*/)
    {
        return ActionTaskUtil.getAllActionTaskNames(/*whereClause*/);
    }
    
    public static Collection<ResolveActionParameterVO> getParamsForActionTaskSysId(String taskSysId, String username)
    {
        return ActionTaskUtil.getParamsForActionTaskSysId(taskSysId, username);
    }
    
    public static ResolveActionTaskVO saveResolveActionTask(ResolveActionTaskVO vo, String username) throws Exception
    {
        return ActionTaskUtil.saveResolveActionTask(vo, null, username, null,  VersionPersistAction.UPDATE);
    }
    
    public static ResolveActionTaskVO saveResolveActionTask(ResolveActionTaskVO vo, String rawContent, String username, String comment, Boolean isSave) throws Exception
    {
        VersionPersistAction versionPersistAction;
        if(isSave == false) {
            versionPersistAction = VersionPersistAction.COMMIT;
        } else {
            versionPersistAction = VersionPersistAction.SAVE;
    }
    
        return ActionTaskUtil.saveResolveActionTask(vo, rawContent, username, comment, versionPersistAction);
    }
    
    public static Set<String> deleteResolveActionTaskByIds(List<String> sysIds, boolean validate, String username) throws Exception
    {
        return ActionTaskUtil.deleteResolveActionTaskByIds(sysIds, validate, username);
    }
    
    public static Set<String> findAllResolveActionTaskModuleNames(String username)
    {
        return ActionTaskUtil.findAllResolveActionTaskModuleNames(username);
    }
    
    public static List<String> moveOrRenameActionTask(List<String> ids, String name, String moduleName, String userName)  throws Exception
    {
        return ActionTaskUtil.moveOrRenameActionTask(ids, name, moduleName, userName);
    }
    
    public static List<String> copyActionTasks(List<String> ids, String name, String moduleName, boolean overwrite, String userName) throws Exception
    {
        return ActionTaskUtil.copyActionTasks(ids, name, moduleName, overwrite, userName);
    }
    
    public static boolean getLogRawResultFlag()
    {
        return ActionTaskUtil.getLogRawResultFlag();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Assessor operation
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static ResolveAssessVO findResolveAssess(String sysId, String name, String username)
    {
        return AssessorUtil.findResolveAssess(sysId, name, username);
    }

    public static ResolveAssessVO findResolveAssessWithoutRefs(String sysId, String name, String username)
    {
        return AssessorUtil.findResolveAssessWithoutRefs(sysId, name, username);
    }
    
    
    public static ResolveAssessVO getDefaultResolveAssess()
    {
        return AssessorUtil.getDefaultResolveAssess();
    }

    public static List<ResolveAssessVO> getResolveAssess(QueryDTO query, String username)
    {
        return AssessorUtil.getResolveAssess(query, username);
    }
    
    public static String getAssessorScript(String name, String username) throws Exception
    {
        return AssessorUtil.getAssessorScript(name, username);
    }
    
    public static String getPreprocessorScript(String name, String username) throws Exception
    {
        return PreprocessorUtil.getPreprocessorScript(name, username);
    }
    
    public static String getParserScript(String name, String username) throws Exception
    {
        return ParserUtil.getParserScript(name, username);
    }
    
    public static Set<String> findActiontaskReferencesForAssessors(Set<String> assessorSysIds, String username)   throws Exception
    {
        return AssessorUtil.findActiontaskReferencesForAssessors(assessorSysIds, username);
    }
    
    public static void deleteResolveAssessByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {
        AssessorUtil.deleteResolveAssessByIds(sysIds, deleteAll, username);
    }
    
    public static ResolveAssessVO saveResolveAssess(ResolveAssessVO vo, String username)  throws Exception
    {
        return AssessorUtil.saveResolveAssess(vo, username);
    }

    /**
     * This api is use to insert or update records in a table. If 'sys_id' is available, then it acts as an UPDATE, else it acts as an INSERT. It returns the list of all the records
     * that were updated or inserted.
     *
     * @param username
     * @param tableName
     * @param records
     * @return
     * @throws Exception
     */
    public static List<Map<String, Object>> insertOrUpdateUsingJDBC(String username, String tableName, 
    		List<Map<String, Object>> records, String dbType) throws Exception
    {
        if (StringUtils.isEmpty(tableName) || CollectionUtils.isEmpty(records)) {
        	throw new Exception("Tablename and records are mandatory");
        }
    	
        return JdbcCrudUtil.save(username, tableName, records, dbType);
    }

    /**
     * gets the list of all the Resolve tables
     *
     * @return
     */
    public static Set<String> getListOfResolveTables()
    {
        return GeneralHibernateUtil.getListOfResolveTables();
    }

    /**
     * reads the controllers.properties file on the startup and updates the Apps and Controller tables
     *
     * @param fileName
     * @throws Exception 
     */
    public static void initControllerProperties(String fileName) throws Exception
    {
        AppsUtil.initControllerProperties(fileName);
    }

    /**
     * This method returns a list of the LDAP/AD/RADIUS gateway (Queue) that is
     * configured to be used for Resolve authentication. Make sure the blueprint
     * properties are configured properly. For example here is a typical configuration
     * that has LDAP as as active which is also used for user authentication
     *
     * rsremote.receive.ldap.active=true
     * rsremote.receive.ldap.queue=LDAP
     * rsremote.receive.ldap.userauth=true
     *
     * rsremote.receive.ad.active=false
     * rsremote.receive.ad.queue=AD
     * rsremote.receive.ad.userauth=false
     * 
     * rsremote.receive.radius.active=false
     * rsremote.receive.radius.queue=RADIUS
     * rsremote.receive.radius.userauth=false
     * @param gatewayType either ad or ldap.
     * @return
     */
    public static List<String> getUserAuthenticationGateway(String gatewayType)
    {
        return ResolveBlueprintUtil.getUserAuthenticationGateway(gatewayType);
    }
    
    public static VO findVOById(String modelName, String id) throws Exception
    {
        return GeneralHibernateUtil.findVOById(modelName, id);
    }
    
    public static void indexAllActionTasks(String username)
    {
        Set<String> sysIds = ActionTaskUtil.getAllActionTaskSysIds();
        ActionTaskIndexAPI.indexActionTasks(sysIds, username);
    }
    
    public static void indexAllResolveProperties(String username)
    {
        // gather all the sysIds
        Set<String> sysIds = ActionTaskPropertiesUtil.getAllResolvePropertySysIds();
        PropertyIndexAPI.indexProperties(sysIds, username);
    }

    public static void indexAllCustomForms(String username)
    {
        // gather all the sysIds
        Set<String> sysIds = CustomFormUtil.getAllCustomFormSysIds();
        CustomFormIndexAPI.indexCustomForms(sysIds, username);
    }
    
    /**
     * Look for a previously saved Java object from Resolve's SQL database  
     *
     * @param objectName Unique object name or id for Java object stored in SQL
     * @return Java object found from SQL database locked for update or null if not found
     */
    public static Object findSharedJavaObject(String objectName)
    {
        return GeneralHibernateUtil.findSharedJavaObject(objectName);
    }

    /**
     * Look for a previously saved Java object from Resolve's SQL database
     * without locking object for update.  
     *
     * @param objectName Unique object name or id for Java object stored in SQL
     * @return Java object found from SQL database or null if not found
     */
    public static Object findSharedJavaObjectNonBlocking(String objectName)
    {
        return GeneralHibernateUtil.findSharedJavaObjectNonBlocking(objectName);
    }
    
    /**
     * Store a Java object to Resolve's SQL database  
     *
     * @param objectName Unique object name or id given to Java object stored in SQL database
     * @param obj Target Java object to be stored in SQL database
     * @return sysId of SQL row
     */
    public static Object saveSharedJavaObject(String objectName, Object obj)
    {
        return GeneralHibernateUtil.saveSharedJavaObject(objectName, obj);
    }
    
    public static String getContentAutoGenCode(String connectionType, String username) throws Exception
    {
    	return ActionTaskUtil.getContentAutoGenCode(connectionType, username);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // RADIUS Config CRUD operations
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static List<ConfigRADIUSVO> getConfigRADIUS(QueryDTO query, String username)
    {
        return ConfigRADIUSUtil.getConfigRADIUS(query, username);
    }
    
    public static ConfigRADIUSVO findConfigRADIUSById(String sys_id, String username)
    {
        return ConfigRADIUSUtil.findConfigRADIUSById(sys_id, username);
    }
    
    public static ConfigRADIUSVO saveConfigRADIUS(ConfigRADIUSVO vo, String username)
    {
        return ConfigRADIUSUtil.saveConfigRADIUS(vo, username);
    }
    
    public static void deleteConfigRADIUSByIds(String[] sysIds, String username) throws Exception
    {
        ConfigRADIUSUtil.deleteConfigRADIUSByIds(sysIds, username);
    }
    
//    public static void syncTagsESAndGraph()
//    {
//        SyncTagsESAndGraph.sync();
//    }
    
    //orgs
    public static List<OrgsVO> getOrgs(QueryDTO query, String username)
    {
        return UserUtils.getOrgs(query, username);
    }
    
    public static OrgsVO saveOrg(OrgsVO vo, String username)  throws Exception
    {
        return UserUtils.saveOrg(vo, username);
    }
    
    public static OrgsVO getOrgById(String sysId)
    {
        return UserUtils.getOrgNoCache(sysId, null);
    }
    
    public static Set<OrgsVO> getValidParentOrgs(String childOrgId, String childOrgName)
    {
        return UserUtils.getValidParentOrgs(childOrgId, childOrgName);
    }
    
    public static List<OrgsVO> getParentOrgsInHierarchy(String childOrgId, String childOrgName)
    {
        return UserUtils.getParentOrgsInHierarchy(childOrgId, childOrgName);
    }
    
    /**
    *
    * @param userid
    * @return
     * @throws Exception 
    */
    public static Map<String, Pair<String, String>> getMapOfActiveWorksheets(String userid) throws Exception
    {
        return WorksheetUtil.getMapOfActiveWorksheets(userid);
    }
    
    /**
    *
    * @param userid
    * @param orgId
    * @param orgName
    * @return
    */
   public static boolean isActiveArchiveWorksheet(String userid, String orgId, String orgName)
   {
       return WorksheetUtil.isActiveArchiveWorksheet(userid, orgId, orgName);
   }
   
   public static Set<OrgsVO> getAllOrgsInHierarchy(String rootOrgId, String rootOrgName)
   {
       return UserUtils.getAllOrgsInHierarchy(rootOrgId, rootOrgName);
   }
   
   public static List<CustomFormDTO> getCustomFormsForUI(QueryDTO query, String username) throws Exception
   {
       return CustomFormUtil.getCustomFormsForUI(query, username);
   }
   
   public static Map<String, String> processDeleteDBRow(String username, String modelClass, List<String> listId, boolean isCustomTable) throws Exception
   {
       HashMap<String, String> result = new HashMap<String, String>();
       result.put(HibernateConstantsEnum.MODELCLASS.getTagName(), modelClass);

       if (Log.log.isTraceEnabled())
       {
           Log.log.trace("Deleting record for table :" + modelClass);
           Log.log.trace("Id to delete :" + StringUtils.listToString(listId));
       }

       try
       {

           if (!modelClass.equals("custom_table_parent_example") && isCustomTable)
           {
               try
               {
                 HibernateProxy.setCurrentUser(username);
                   HibernateProxy.execute(() -> {
                	// its a custom table
                       EntityUtil.delete(modelClass, listId);
                   });
               }
               catch (Exception e)
               {
                   throw e;
               }
           }
           else
           {
               // its a system table
               GenericDataServiceUtil.deleteByIds(listId, result);
           }

           result.put(HibernateConstantsEnum.DB.getTagName(), "Record Deleted successfully.");

       }
       catch (Throwable t)
       {
           Log.log.error("Error:", t);
           result.put(HibernateConstants.ERROR_EXCEPTION_KEY, "DB Update Failed:" + t.getMessage());
           throw new Exception(t);
       }

       return result;
   }// deleteDBRow()

   public static Map<String, String> processDeleteDBRow(String username, String modelClass, String sysId, boolean isCustomTable)
   {
       HashMap<String, String> result = new HashMap<String, String>();
       result.put(HibernateConstantsEnum.MODELCLASS.getTagName(), modelClass);

       if (Log.log.isTraceEnabled())
       {
           Log.log.trace("Deleting record for table :" + modelClass);
           Log.log.trace("Id to delete :" + sysId);
       }

       try
       {

           if (!modelClass.equals("custom_table_parent_example") && isCustomTable)
           {
               try
               {

                 HibernateProxy.setCurrentUser(username);
            	   HibernateProxy.execute(() -> {
                       // its a custom table
                       EntityUtil.delete(modelClass, sysId);

            	   });
               }
               catch (Exception e)
               {
                   throw e;
               }
           }
           else
           {
               List<String> listId = new ArrayList<String>();
               listId.add(sysId);

               // its a system table
               GenericDataServiceUtil.deleteByIds(listId, result);
           }

           result.put(HibernateConstantsEnum.DB.getTagName(), "Record Deleted successfully.");

       }
       catch (Throwable t)
       {
           Log.log.error("Error:", t);
           result.put(HibernateConstants.ERROR_EXCEPTION_KEY, "DB Update Failed:" + t.getMessage());
       }

       return result;
   }// deleteDBRow()

   public static String saveUserSettings(String settingType, String settingValue, String userName) throws Exception {
       return UserUtils.saveUserSettings(settingType, settingValue, userName);
   }
   
   public static SearchRecordDTO saveRecentlyOpenedItem(SearchRecordDTO searchRecordDTO, String username) throws Exception {
	   return SearchUtil.saveRecentlyOpenedItem(searchRecordDTO, username);
   }
   
   public static List<SearchRecordDTO> listRecentlyOpenedItems(String username) throws Exception {
	   return SearchUtil.listRecentlyOpenedItems(username);
   }
   
   public static SearchAttributesDTO saveRecentSearch(SearchAttributesDTO searchDTO, String username) throws Exception {
	   return SearchUtil.saveRecentSearch(searchDTO, username);
   }
   
   public static List<SearchAttributesDTO> listRecentSearches(String username) throws Exception {
	   return SearchUtil.listRecentSearches(username);
   }
   
   public static int getTotalHqlSysIdCount(QueryDTO queryDTO) throws Exception {
       return GeneralHibernateUtil.getTotalHqlSysIdCount(queryDTO);
   }
   
   public static int getTotalHqlDistinctSysIdCount(QueryDTO queryDTO) throws Exception {
       return GeneralHibernateUtil.getTotalHqlDistinctSysIdCount(queryDTO);
   }
   
   /**
    * caches the wiki document VO to cache region named WIKI_DOCUMENT_CACHE_REGION
    *
    * @param wikiDoc	Wiki document to cache
    */

    public static void cacheWikiDocumentForRSControl(WikiDocument wikiDoc) {
    	if (wikiDoc != null) {
    		HibernateUtil.cacheDBObject(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION,
										new String[] { "com.resolve.persistence.model.WikiDocument" }, 
										wikiDoc.getUFullname(), wikiDoc);
    	}
    }
}
