/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import com.resolve.services.util.CacheUtils;
import com.resolve.services.vo.UserWorksheetData;

public class ServiceCache
{
    public static UserWorksheetData getWorksheetDataFromCache(String problemId)
    {
        return CacheUtils.getWorksheetDataFromCache(problemId);
    }
    
    public static void putWorksheetDataFromCache(String problemId, UserWorksheetData userInfo)
    {
        CacheUtils.putWorksheetDataFromCache(problemId, userInfo);
    }
    
    public static boolean hasProblemIdInWorksheetDataCache(String problemId)
    {
        return CacheUtils.hasProblemIdInWorksheetDataCache(problemId);
    }
    
    public static void removeProblemIdFromWorksheetDataCacahe(String problemId)
    {
        CacheUtils.removeProblemIdFromWorksheetDataCacahe(problemId);
    }
    
    public static void cleanupWorksheetDataCache()
    {
        CacheUtils.cleanupWorksheetDataCache();
    }
    
//    public static Map<String, FieldMetaData> getFieldNamesForModel(String clazzName)
//    {
//        return CacheUtils.getFieldNamesForModel(clazzName);
//    }
}
