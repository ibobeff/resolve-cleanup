/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util.vo;

import java.util.List;
import java.util.Map;

public class MenuDefinitionModelVO
{
    private String title;
    private Boolean active;
    private String roles;
    private String name;
    private String sys_ID;
    private String sequence;
    private List<Map<String, String>> menuDefinitionRoles;
    private List<Map<String, String>> availableRoles;
    private List<MenuItemModelVO> menuItemModel;
    
    public MenuDefinitionModelVO() 
    {
    }

    public MenuDefinitionModelVO(String title, Boolean active, String roles, String name, String sys_id)
    {   
        this.setTitle(title);
        this.setActive(active);
        this.setRoles(roles);
        this.setName(name);
        this.setSys_ID(sys_id);
    }
    
    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    public String getRoles()
    {
        return roles;
    }
    public void setRoles(String roles)
    {
        this.roles = roles;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getSys_ID()
    {
        return sys_ID;
    }
    public void setSys_ID(String sys_ID)
    {
        this.sys_ID = sys_ID;
    }
    public String getSequence()
    {
        return sequence;
    }
    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }
    public List<Map<String, String>> getMenuDefinitionRoles()
    {
        return menuDefinitionRoles;
    }
    public void setMenuDefinitionRoles(List<Map<String, String>> menuDefinitionRoles)
    {
        this.menuDefinitionRoles = menuDefinitionRoles;
    }
    public List<Map<String, String>> getAvailableRoles()
    {
        return availableRoles;
    }
    public void setAvailableRoles(List<Map<String, String>> availableRoles)
    {
        this.availableRoles = availableRoles;
    }

    public List<MenuItemModelVO> getMenuItemModel()
    {
        return menuItemModel;
    }

    public void setMenuItemModel(List<MenuItemModelVO> menuItemModel)
    {
        this.menuItemModel = menuItemModel;
    }
    

}
