/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.archive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.hibernate.query.Query;

import com.resolve.persistence.model.ArchiveActionResult;
import com.resolve.persistence.model.ArchiveWorksheet;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.vo.ActionTaskInfoVO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class ArchiveTable
{
	public static AtomicBoolean isArchiving = new AtomicBoolean(false);
	
    public static void archive(String dbType, boolean force, boolean isSIRWSArchive) {
    	boolean isAlreadyRunning = isArchiving.getAndSet(true);
    	
    	if (isAlreadyRunning) {
    		Log.log.info(String.format("Previous archive (non-SIR/SIR Worksheet) run for DB Type %s is still running. " +
    								   "Skipping current %sarchive request...", dbType, isSIRWSArchive ? "SIR " : ""));
    		return;
    	}

        long startTime = System.currentTimeMillis();
        
    	try {
	        BaseArchiveTable instance = ArchiveTableFactory.getTableArchiver(dbType);
	
	        Log.log.info(String.format("Force=%b, Starting MAIN Archive: %d, Is SIR Worksheet Archive: %b", force, startTime,
	        						   isSIRWSArchive));
	
	        // archive
	        instance.archive(force, isSIRWSArchive);
	
	        Log.log.info("MAIN Archive completed after " + (System.currentTimeMillis() - startTime) / 1000 + " seconds");
    	}
    	catch (Throwable e)
    	{
    		Log.log.error("MAIN archive aborted after " + (System.currentTimeMillis() - startTime) / 1000 + " seconds", e );
    	}
    	finally
    	{
	        Log.log.info("MAIN Archive finished after " + (System.currentTimeMillis() - startTime) / 1000 + " seconds");
	        isArchiving.getAndSet(false);
    	}
    }
	
    @Deprecated
    @SuppressWarnings({ "unchecked", "unused", "rawtypes" })
    public static List<ActionTaskInfoVO> getListForResultMacroArchiveWorksheet(String problemId, String[] actionNamespaceArr, String[] actionNameArr, String[] nodeIdArr, String[] actionWikiArr, String[] descriptionArr, boolean isOrderByAsc, List<String> actionTaskids)
    {
        List<ActionTaskInfoVO> result = new ArrayList<ActionTaskInfoVO>();
        
        //prepare it for the query
        String actiontaskids = SQLUtils.prepareQueryString(actionTaskids);
        
        if(StringUtils.isNotEmpty(actiontaskids) && StringUtils.isNotEmpty(problemId))
        {
            String sql =    " SELECT r FROM ArchiveActionResult r " +
                            " JOIN r.problem as p with p.sys_id = '" + problemId + "' ";
            
            if(StringUtils.isNotEmpty(actiontaskids))
            {
                sql = sql + " JOIN r.task as t with t.sys_id IN (" + actiontaskids + ") ";
            }
                            
            sql = sql + " order by r.UTimestamp " + (isOrderByAsc ? "asc" : "desc");
            
            try
            {
            	String sqlFinal = sql;
              HibernateProxy.setCurrentUser("system");
            	HibernateProxy.execute(() -> {
                    // init problem
                    ArchiveWorksheet problem = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO().findById(problemId);
                    if(problem != null)
                    {
                        List<ArchiveActionResult> lRAR = null;
                        
                        Query q = HibernateUtil.createQuery(sqlFinal);
                        lRAR = q.list();
                        
                        if(lRAR != null && lRAR.size() > 0)
                        {
                            if(actionNameArr != null && actionNameArr.length > 0)
                            {
                                
                                for (int count=0; count < actionNameArr.length; count++)
                                {
                                    String atName = actionNameArr[count];
                                    String atNamespace = actionNamespaceArr[count];
                                    String atDescription = descriptionArr[count];
                                    String atActionWiki = actionWikiArr[count];
                                    String atNodeId = nodeIdArr[count];
                                    
                                    for(ArchiveActionResult rar : lRAR)
                                    {
                                        ResolveActionTask task = rar.getTask();
                                        
                                        String taskName = task.getUName();
                                        String taskNamespace = task.getUNamespace();
                                        String selectNodeId = rar.getExecuteRequest() != null ? rar.getExecuteRequest().getUNodeId() : "";
                                        String resultResultId = rar.getSys_id();
                                        String resultSummary = (rar.getActionResultLob() == null ? "" : rar.getActionResultLob().getUSummary());
                                        String resultSeverity = rar.getUSeverity();
                                        String executeId = rar.getExecuteRequest() != null ? rar.getExecuteRequest().getSys_id() : "";
                                        
                                        String key = task.getUFullName() + "|" + selectNodeId; //name#ns|nodeid
                                        String keyName = task.getUName();
                                        String keyNameNamespace = task.getUFullName();
                                        
                                        if(StringUtils.isNotEmpty(atName) && taskName.equalsIgnoreCase(atName))
                                        {
                                            //continue as the namespace are not equal. if the namespace is empty/null, 
                                            //then we assume its the actiontask and use that description
                                            if(StringUtils.isNotEmpty(atNamespace) && !taskNamespace.equalsIgnoreCase(atNamespace))
                                            {
                                                continue;
                                            }
                                            
                                            //compare the nodeId
                                            if(StringUtils.isNotEmpty(atNodeId) && !selectNodeId.equalsIgnoreCase(atNodeId))
                                            {
                                                continue;
                                            }
                                            
                                            if (StringUtils.isBlank(atDescription))
                                            {
                                                atDescription =  task.getUSummary();
                                                if (StringUtils.isBlank(atDescription))
                                                {
                                                    atDescription = task.getUName();
                                                }
                                            }
                            
                                            Map<String, String> rowInfo = new HashMap<String, String>();
                                            rowInfo.put("DESCRIPTION", atDescription);
                                            rowInfo.put("SUMMARY", resultSummary);
                                            rowInfo.put("RESULT", resultSeverity);
                                            
//                                            if (StringUtils.isNotBlank(atActionWiki))
//                                            {
//                                                rowInfo.put("LINK:DESCRIPTION", "/resolve/service/wiki/view/" + atActionWiki.replace('.', '/'));
//                                            }
                            
//                                            rowInfo.put("LINK:RESULT", "/resolve/service/result/task/detail?"+Constants.HTTP_REQUEST_TASKRESULTID+"="+resultResultId);
                            
//                                          result.add(rowInfo);
                                            
//                                          String key = task.getUFullName() + "|" + selectNodeId; //name#ns|nodeid

                                            ActionTaskInfoVO vo = new ActionTaskInfoVO();
                                            vo.setSysId(resultResultId);
                                            vo.setActionTaskFullName(task.getUFullName());
                                            vo.setRowInfo(rowInfo);
                                            vo.setKey(key);
                                            vo.setKeyName(keyName);
                                            vo.setKeyNameNamespace(keyNameNamespace);

                                            result.add(vo);

                                        }
                                            
                                    }
                                    
                                }
                            }
                            else
                            {
                                //everything
                                for(ArchiveActionResult rar : lRAR)
                                {
                                    ResolveActionTask task = rar.getTask();
                                    String resultResultId = rar.getSys_id();
                                    String resultSummary = (rar.getActionResultLob() == null ? "" : rar.getActionResultLob().getUSummary());
                                    String resultSeverity = rar.getUSeverity();
                                    String executeId = rar.getExecuteRequest() != null ? rar.getExecuteRequest().getSys_id() : "";
                                    String selectNodeId = rar.getExecuteRequest() != null ? rar.getExecuteRequest().getUNodeId() : "";
                                    String actionWiki = "";
                                    String description = "";

                                    String key = task.getUFullName() + "|" + selectNodeId; //name#ns|nodeid
                                    String keyName = task.getUName();
                                    String keyNameNamespace = task.getUFullName();

                                    if (StringUtils.isEmpty(description))
                                    {
                                        description =  task.getUSummary();
                                        if (StringUtils.isEmpty(description))
                                        {
                                            description = task.getUName();
                                        }
                                    }
                    
                                    Map<String, String> rowInfo = new HashMap<String, String>();
                                    rowInfo.put("DESCRIPTION", description);
                                    rowInfo.put("SUMMARY", resultSummary);
                                    rowInfo.put("RESULT", resultSeverity);
                                    
//                                    if (StringUtils.isNotEmpty(actionWiki))
//                                    {
//                                        rowInfo.put("LINK:DESCRIPTION", "/resolve/service/wiki/view/" + actionWiki.replace('.', '/'));
//                                    }
                    
//                                    rowInfo.put("LINK:RESULT", "/resolve/service/result/task/detail?"+Constants.HTTP_REQUEST_TASKRESULTID+"="+resultResultId);
                    
//                                    result.add(rowInfo);
//                                    String key = task.getUFullName() + "|" + selectNodeId; //name#ns|nodeid

                                    ActionTaskInfoVO vo = new ActionTaskInfoVO();
                                    vo.setSysId(resultResultId);
                                    vo.setActionTaskFullName(task.getUFullName());
                                    vo.setRowInfo(rowInfo);
                                    vo.setKey(key);
                                    vo.setKeyName(keyName);
                                    vo.setKeyNameNamespace(keyNameNamespace);

                                    result.add(vo);

                                    
                                }
                            }
                        }//end of if 
                    }
                    
                    
            	});
                
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);

                Map<String, String> rowInfo = new HashMap<String, String>();
                rowInfo.put("Exception", e.getMessage());

                ActionTaskInfoVO vo = new ActionTaskInfoVO();
                vo.setRowInfo(rowInfo);

                // result.add(rowInfo);
                result.add(vo);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }//end of it
        
        return result;
    } // getListForResultMacroArchiveWorksheet
    
    

}
