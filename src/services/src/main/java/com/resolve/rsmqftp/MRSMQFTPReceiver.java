/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmqftp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;

import org.apache.commons.lang3.StringUtils;

import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.esb.amqp.MAmqpListener;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

/**
 * RSMQ FTP Message Handler for receiving side.
 * 
 * @author HemantPhanasgaonkar
 * @version 1.0
 * @since Post 6.3
 */
public final class MRSMQFTPReceiver {
	private static final String RELATIVE_PATH_PREFIX = ".";
	
	private static MainBase mainBase = MainBase.main;
	private static ConcurrentMap<String, Map<String, Object>> txfersInProgress = 
																	new ConcurrentHashMap<String, Map<String, Object>>();
	
	private static String instance(Map<String, Object> params) {
		return String.format("%s: %s", MRSMQFTPReceiver.class.getSimpleName(), ChunkKey.txferMsgMapLogger(params));
	}
	
	private static void validateReceiverPath(Map<String, Object> params) throws RSMQFTPException {
		File dout = null;
		
		String rcvrPath = (String)params.get(ChunkKey.RCVRPATH.name());
		String absRcvrPath = rcvrPath;
		
		if (StringUtils.isNotBlank(rcvrPath) && rcvrPath.startsWith(RELATIVE_PATH_PREFIX)) {
			absRcvrPath = mainBase.configGeneral.home + File.separator + rcvrPath;
		}
		
		params.put(ChunkKey.ABSRCVRPATH.name(), absRcvrPath);
		
		long fileSize = ((Long)params.get(ChunkKey.FILESIZE.name())).longValue();
		
		try {
			dout = new File(absRcvrPath);
		} catch (NullPointerException npe) {
			RSMQFTPException rsmqftpe = 
					new RSMQFTPException(instance(params), "validateReceiverPath", 
										 String.format("Receiver Path: %s", 
												 	   StringUtils.isNotBlank(absRcvrPath) ? absRcvrPath : ""), npe);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		if (!dout.exists()) {
			try {
				dout.mkdirs();
			} catch (SecurityException se) {
				RSMQFTPException rsmqftpe = 
						new RSMQFTPException(instance(params), "validateReceiverPath", 
							   				 String.format("Failed to create all missing directories in %s", absRcvrPath), 
							   				 se);
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				throw rsmqftpe;
			}
		}
		
		if (dout == null || !dout.isDirectory() || (dout.getUsableSpace() < fileSize)) {
			RSMQFTPException rsmqftpe = 
								new RSMQFTPException(instance(params), "validateReceiverPath", 
									   				 String.format("%s does not exists or is not directory or usable space " +
									   						 	   "%d is less than file size %d", 
									   						 	   absRcvrPath, dout.getUsableSpace(), fileSize));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
	}
	
	@SuppressWarnings("resource")
	private static void createNewFile(Map<String, Object> params) throws RSMQFTPException {
		File fout = null;
		
		String absRcvrPath = (String)params.get(ChunkKey.ABSRCVRPATH.name());
		String rcvrFileName = (String)params.get(ChunkKey.RCVRFILENAME.name());
		
		try {
			fout = new File(absRcvrPath, rcvrFileName);
			
			if (fout.isDirectory()) {
				RSMQFTPException rsmqftpe = 
						new RSMQFTPException(instance(params), "createNewFile", 
							   				 String.format("File Is Directory: %s%s%s", absRcvrPath, File.separator, 
							   						 	   rcvrFileName));
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				throw rsmqftpe;
			}
			
			if (fout.exists()) {
				String txferId = (String)params.get(ChunkKey.TXFERID.name());
				String userId = (String)params.get(ChunkKey.USERID.name());
				
				fout = new File(absRcvrPath, String.format("%s-%s-%s", txferId, userId, rcvrFileName));
			}
			
			boolean isNewFile = fout.createNewFile();
			
			if (!isNewFile) {
				RSMQFTPException rsmqftpe = 
						new RSMQFTPException(instance(params), "createNewFile", 
							   				 String.format("File Already Exists: %s%s%s", absRcvrPath, File.separator, 
							   						 	   rcvrFileName));
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				throw rsmqftpe;
			}
		} catch (IOException | SecurityException e) {
			RSMQFTPException rsmqftpe = 
					new RSMQFTPException(instance(params), "createNewFile", 
						   				 String.format("New File: %s%s%s", absRcvrPath, File.separator, rcvrFileName), e);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		FileOutputStream fos = null;
		
		try {
			fos = new FileOutputStream(fout);
		} catch (FileNotFoundException | SecurityException e) {
			try {
				fout.delete();
			} catch (SecurityException se) {
				Log.log.error(String.format("Error %sin deleting file %s", 
										 	StringUtils.isNotBlank(se.getMessage()) ? "[" + se.getMessage() + "] " : "",
										 	fout.getPath()),
							  se);
			}
			
			RSMQFTPException rsmqftpe = 
					new RSMQFTPException(instance(params), "createNewFile", 
						   				 String.format("File Output Stream: %s%s%s", absRcvrPath, File.separator, 
						   						 	   rcvrFileName), e);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		CheckedOutputStream cos = new CheckedOutputStream(fos, new CRC32());
		
		params.put(ChunkKey.RCVRFILEHANDLE.name(), fout);
		params.put(ChunkKey.ABSRCVRFILEPATH.name(), fout.getAbsolutePath());
		params.put(ChunkKey.RCVRFILEOUTPUTSTREAM.name(), fos);
		params.put(ChunkKey.RCVRCHKEDOUTPUTSTREAM.name(), cos);
	}
	
	private static void createReceiverChunkListener(Map<String, Object> params) throws RSMQFTPException {
		MListener receiverChunkListener  = null;
		
		try {
			receiverChunkListener = 
				mainBase.mServer.createListener(String.format("%s_%s", MRSMQFTPReceiver.class.getSimpleName().toUpperCase(),
															  ((String)params.get(ChunkKey.TXFERID.name())).toUpperCase()),
												MRSMQFTPReceiver.class.getPackage().getName());
			receiverChunkListener.init(false);
		} catch (ESBException esbe) {
			RSMQFTPException rsmqftpe = 
					new RSMQFTPException(instance(params), "createReceiverChunkListener", 
										 String.format("%s_%s", MRSMQFTPReceiver.class.getSimpleName().toUpperCase(),
												 	   ((String)params.get(ChunkKey.TXFERID.name())).toUpperCase()), 
										 esbe);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		params.put(ChunkKey.RECEIVERCHUNKLISTENER.name(), receiverChunkListener);
	}
	
	public static Map<String, Object> startReceiver(Map<String, Object> params) {
		Map<String, Object> resp = new HashMap<String, Object>(params);
		
		try {
			validateReceiverPath(params);
			createNewFile(params);
			createReceiverChunkListener(params);
			Map<String, Object> prevTxferInProgress = 
									txfersInProgress.putIfAbsent((String)params.get(ChunkKey.TXFERID.name()), params);
			
			if (prevTxferInProgress != null) {
				Log.log.warn(String.format("Received startReceiver for Txfer Id %s which already had Previous transfer " +
										   "[%s] in progress, overwriting previous transfer in progress!!!", 
										   ChunkKey.txferMsgMapLogger(prevTxferInProgress)));
				txfersInProgress.put((String)params.get(ChunkKey.TXFERID.name()), params);
			}
			
			if (Log.log.isDebugEnabled()) {
				Log.log.debug(String.format("Added [%s, %s] to Txfers In Progress, Total Txfers In Progress %d",
											(String)params.get(ChunkKey.TXFERID.name()), instance(params),
											txfersInProgress.size()));
			}
			
			resp = params;
			resp.put(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name(), Integer.valueOf(txfersInProgress.size()));
		} catch (Throwable t) {
			if (t instanceof RSMQFTPException) {
				Log.log.error(t.getMessage(), t);
				resp.put(ChunkKey.RSMQFTPEXCEPTION.name(), t);
				resp.put(ChunkKey.RSMQFTPEXCEPTIONINSTANT.name(), Instant.now());
			} else {
				RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "startReceiver", null, t);
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				resp.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
				resp.put(ChunkKey.RSMQFTPEXCEPTIONINSTANT.name(), Instant.now());
			}
		}
		
		return ChunkKey.getSerializableMap(resp);
	}
	
	private static void validateTxferId(Map<String, Object> params) throws RSMQFTPException {
		if (StringUtils.isNotBlank((String)params.get(ChunkKey.TXFERID.name())) && 
			!txfersInProgress.containsKey((String)params.get(ChunkKey.TXFERID.name()))) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "validateTxferId", 
										 					 String.format("Missing file transfer in progress with " +
										 							 	   "TxferId %s", 
										 							 	   (String)params.get(ChunkKey.TXFERID.name())));
			Log.log.warn(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		} else {
			params.put(ChunkKey.CHUNKRCVDFROMESBTIME.name(), System.currentTimeMillis());
		}
	}
	
	private static void validateChunkSequence(Map<String, Object> params) throws RSMQFTPException {
		if (!params.containsKey(ChunkKey.CURRENTCHUNK.name())) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "validateChunkSequence", 
					 										 String.format("Missing %s [%s] in received chunk", 
					 												 	   ChunkKey.CURRENTCHUNK.name(),
					 												 	   ChunkKey.CURRENTCHUNK.getDisplayName()));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		int currentChunk = ((Integer)params.get(ChunkKey.CURRENTCHUNK.name())).intValue();
		
		int prevChunk = txfersInProgress.get((String)params.get(ChunkKey.TXFERID.name()))
						.containsKey(ChunkKey.CURRENTCHUNK.name()) ? 
						((Integer)txfersInProgress.get((String)params.get(ChunkKey.TXFERID.name()))
								  .get(ChunkKey.CURRENTCHUNK.name())).intValue() : 0;
		
		int totalChunks = ((Integer)params.get(ChunkKey.TOTALCHUNKS.name())).intValue();
		
		if ((currentChunk - prevChunk) != 1 || currentChunk > totalChunks) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "validateChunkSequence", 
					 										 String.format("Received chunk number out of order or " +
					 												 	   "received more chunks than expected. Current " +
					 												 	   "Chunk #: %d, Previous Received Chunk #: %d, " +
					 												 	   "Total Chunks: %d", currentChunk, prevChunk,
					 												 	   totalChunks));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		// CHUNKSIZE of 0 or -ve indicates poison pill 
		if (((Integer)params.get(ChunkKey.CHUNKSIZE.name())).intValue() <= 0) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "validateChunkSequence", 
					 										 String.format("Received STOP TRANSFER request for txferId " +
					 												 	   "%s. Current Chunk #: %d, " +
					 												 	   "Previous Received Chunk #: %d, " +
					 												 	   "Total Chunks: %d", 
					 												 	   (String)params.get(ChunkKey.TXFERID.name()), 
					 												 	   currentChunk, prevChunk, totalChunks));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
	}
	
	private static void writeChunkToCheckedOutputStream(Map<String, Object> params) throws RSMQFTPException {
		CheckedOutputStream cos = (CheckedOutputStream) txfersInProgress.get((String)params.get(ChunkKey.TXFERID.name()))
								  						.get(ChunkKey.RCVRCHKEDOUTPUTSTREAM.name());
		
		try {
			cos.write((byte[])params.get(ChunkKey.CHUNKDATA.name()), 0, 
					  ((Integer)params.get(ChunkKey.CHUNKSIZE.name())).intValue());
			cos.flush();
		} catch (IOException ioe) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "writeChunkToCheckedOutputStream", null, ioe);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		long receiverCheckSum = cos.getChecksum().getValue();
		
		if (receiverCheckSum != ((Long)params.get(ChunkKey.RUNNINGCHECKSUM.name())).longValue()) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "writeChunkToCheckedOutputStream", 
															 String.format("Receiver's checksum %d does not match " +
																	 	   "sender's checksum %d", receiverCheckSum,
																	 	  ((Long)params.get(ChunkKey.RUNNINGCHECKSUM.name()))
																	 	  .longValue()));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		params.remove(ChunkKey.CHUNKDATA.name());
		params.put(ChunkKey.CHUNKWRITTENTOFILETIME.name(), Long.valueOf(System.currentTimeMillis()));
		params.put(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name(), Integer.valueOf(txfersInProgress.size()));
		
		txfersInProgress.get((String)params.get(ChunkKey.TXFERID.name())).putAll(params);
	}
	
	private static void writeChunkToFile(Map<String, Object> params) throws RSMQFTPException {
		validateChunkSequence(params);
		writeChunkToCheckedOutputStream(params);
	}
	
	private static void writeChunkPostProcess(Map<String, Object> params) {
		if (params.containsKey(ChunkKey.RSMQFTPEXCEPTION.name()) || 
			(((Integer)params.get(ChunkKey.TOTALCHUNKS.name())).intValue() == 
			 ((Integer)params.get(ChunkKey.CURRENTCHUNK.name())).intValue())) {
			try {
				((CheckedOutputStream)txfersInProgress.get((String)params.get(ChunkKey.TXFERID.name()))
								 	  .get(ChunkKey.RCVRCHKEDOUTPUTSTREAM.name())).close();
			} catch (IOException ioe) {
				Log.log.error(String.format("writeChunkPostProcess[%s]: Error %sin closing receiver's checked output " +
											"stream", instance(params), 
											StringUtils.isNotBlank(ioe.getMessage()) ? "[" + ioe.getMessage() + "] " : ""), 
							  ioe);
			}
			
			MAmqpListener rcvrChunkListener = 
								((MAmqpListener)txfersInProgress.get((String)params.get(ChunkKey.TXFERID.name()))
												.get(ChunkKey.RECEIVERCHUNKLISTENER.name()));
				
			rcvrChunkListener.deleteQueue(String.format("%s_%s", MRSMQFTPReceiver.class.getSimpleName().toUpperCase(),
													   ((String)params.get(ChunkKey.TXFERID.name())).toUpperCase()));
				
			rcvrChunkListener.close();
			
			if (params.containsKey(ChunkKey.RSMQFTPEXCEPTION.name())) {
				boolean deleted = false;
				File fout = (File)txfersInProgress.get((String)params.get(ChunkKey.TXFERID.name()))
						 		   .get(ChunkKey.RCVRFILEHANDLE.name());
				try {
					deleted = fout.delete();
				} catch (SecurityException se) {
					Log.log.error(String.format("writeChunkPostProcess[%s]: Error %sin deleting file %s", instance(params), 
												StringUtils.isNotBlank(se.getMessage()) ? "[" + se.getMessage() + "] " : "",
												fout.getPath()), 
								  se);
				}
			}
			
			if (params.containsKey(ChunkKey.RSMQFTPEXCEPTION.name()) || 
					(((Integer)params.get(ChunkKey.TOTALCHUNKS.name())).intValue() == 
					 ((Integer)params.get(ChunkKey.CURRENTCHUNK.name())).intValue())) {
				txfersInProgress.remove((String)params.get(ChunkKey.TXFERID.name()));
				params.put(ChunkKey.TXFERCOMPLETEINSTANT.name(), Instant.now());
			}
		}
	}
	
	public static Map<String, Object> writeChunk(Map<String, Object> params) {
		Map<String, Object> resp = new HashMap<String, Object>(params);
		
		try {
			validateTxferId(params);
			
			Options.ChunkTxferMode chunkTxferMode = (Options.ChunkTxferMode)params.get(ChunkKey.CHUNKTXFERMODE.name());
			
			if (chunkTxferMode.equals(Options.ChunkTxferMode.SYNC)) {
				writeChunkToFile(params);
				resp = new HashMap<String, Object>(params);
			} else if (chunkTxferMode.equals(Options.ChunkTxferMode.BUFFERED)) {
				RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "writeChunk", 
																 String.format("Chunk Transfer Mode %s is currently " +
																		 	   "not supported", chunkTxferMode.name()));
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				resp.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
			}
		} catch (Throwable t) {
			if (t instanceof RSMQFTPException) {
				Log.log.error(t.getMessage(), t);
				resp.put(ChunkKey.RSMQFTPEXCEPTION.name(), t);
			} else {
				RSMQFTPException rsmqftpe = new RSMQFTPException(instance(params), "writeChunk", null, t);
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				resp.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
			}
		}
		
		// Perform post processing after writing chunk
		
		writeChunkPostProcess(resp);
		
		return ChunkKey.getSerializableMap(resp);
	}
}