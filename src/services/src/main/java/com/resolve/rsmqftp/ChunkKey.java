/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmqftp;

import java.io.File;
import java.io.FileOutputStream;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.CheckedOutputStream;

import org.apache.commons.collections.MapUtils;

import com.resolve.esb.MListener;
import com.resolve.rsmqftp.Options.ChunkTxferMode;

/**
 * Chunk key enum.
 * 
 * @author HemantPhanasgaonkar
 * @version 1.0
 * @since Post 6.3
 */
public enum ChunkKey {
	TXFERID("Transfer Id", String.class, true), 
	TOTALCHUNKS("Total Chunks", Integer.class, true), 
	CURRENTCHUNK("Current Chunk", Integer.class, true),
	CHUNKSIZE("Chunk Size (in bytes)", Integer.class, true), 
	CHUNKDATA("Chunk Data", byte[].class, true), 
	RUNNINGCHECKSUM("Stream Checksum", Long.class, true), 
	CHUNKCREATETIME("Chunk Create Time (msec since epoch)", Long.class, true), 
	USERID("User Id", String.class, true), 
	RCVRPATH("Receiver Directory", String.class, true), 
	RCVRFILENAME("Receiver File Name", String.class, true),
	FILESIZE("File Size (in bytes)", Long.class, true), 
	RSMQFTPEXCEPTION("RSMQ FTP Exception", RSMQFTPException.class, true),
	RCVRFILEHANDLE("Receiver File Handle", File.class, true),
	RCVRFILEOUTPUTSTREAM("Receiver File Output Stream", FileOutputStream.class, false),
	RECEIVERCHUNKLISTENER("Receiver Chunk Listener", MListener.class, false),
	CHUNKSENTTOESBTIME("Chunk Sent To ESB Time (msec since epoch)", Long.class, true),
	CHUNKRCVDFROMESBTIME("Chunk Received From ESB Time (msec since epoch)", Long.class, true),
	CHUNKTXFERMODE("Chunk Txfer Mode (SYNC, BUFFERED)", Options.ChunkTxferMode.class, true),
	RCVRCHKEDOUTPUTSTREAM("Receiver Checked (CRC32) Output Stream", CheckedOutputStream.class, false),
	CHUNKWRITTENTOFILETIME("Chunk Written To File Time (msec since epoch)", Long.class, true),
	SESSIONCREATEINSTANT("Session Create Instant", Instant.class, true),
	RCVRTXFERSINPROGRESSCOUNT("Receiver Txfers In Progress Count", Integer.class, true),
	RSMQFTPEXCEPTIONINSTANT("RSMQ FTP Exception Instant", Instant.class, true),
	TXFERCOMPLETEINSTANT("Txfer Complerte Instant", Instant.class, true),
	ABSRCVRPATH("Absolute Receiver Directory", String.class, true),
	ABSRCVRFILEPATH("Absolute Receiver File Path", String.class, true);
	
	private final String displayName;
	private final Class<?> type;
	private final boolean isSerializable;
	
	ChunkKey(String displayName, Class<?> type, boolean isSerializable) {
		this.displayName = displayName;
		this.type = type;
		this.isSerializable = isSerializable;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	public boolean getIsSerializable() {
		return isSerializable;
	}
	
	private static void appendParamNameValue(StringBuilder strBldr, ChunkKey chunkKey, Map<String, Object> params) {
		if (params.containsKey(chunkKey.name())) {
			if (strBldr.length() > 0) {
				strBldr.append(", ");
			}
			
			switch (chunkKey) {
				case CHUNKDATA:
					strBldr.append(String.format("Chunk Data Length: %d",
							   					 ((byte[])params.get(chunkKey.name())).length));
					break;
					
				case CHUNKTXFERMODE:
					strBldr.append(String.format("Chunk Txfer Mode: %s", 
												 ((Options.ChunkTxferMode)params.get(chunkKey.name())).name()));
					break;
				
				default: 
					strBldr.append(String.format("%s: %s", chunkKey.getDisplayName(), params.get(chunkKey.name())));
					break;
			}
		}
	}
	
	public static String txferMsgMapLogger(Map<String, Object> params) {
		StringBuilder paramsStrBldr = new StringBuilder();
		
		appendParamNameValue(paramsStrBldr, TXFERID, params);		
		appendParamNameValue(paramsStrBldr, TOTALCHUNKS, params);
		appendParamNameValue(paramsStrBldr, CURRENTCHUNK, params);
		appendParamNameValue(paramsStrBldr, CHUNKSIZE, params);
		appendParamNameValue(paramsStrBldr, CHUNKDATA, params);
		appendParamNameValue(paramsStrBldr, RUNNINGCHECKSUM, params);
		appendParamNameValue(paramsStrBldr, CHUNKCREATETIME, params);
		appendParamNameValue(paramsStrBldr, USERID, params);
		appendParamNameValue(paramsStrBldr, RCVRPATH, params);
		appendParamNameValue(paramsStrBldr, RCVRFILENAME, params);
		appendParamNameValue(paramsStrBldr, FILESIZE, params);
		appendParamNameValue(paramsStrBldr, CHUNKSENTTOESBTIME, params);
		appendParamNameValue(paramsStrBldr, CHUNKRCVDFROMESBTIME, params);
		appendParamNameValue(paramsStrBldr, CHUNKTXFERMODE, params);
		appendParamNameValue(paramsStrBldr, CHUNKWRITTENTOFILETIME, params);
		appendParamNameValue(paramsStrBldr, SESSIONCREATEINSTANT, params);
		appendParamNameValue(paramsStrBldr, RCVRTXFERSINPROGRESSCOUNT, params);
		appendParamNameValue(paramsStrBldr, RSMQFTPEXCEPTIONINSTANT, params);
		appendParamNameValue(paramsStrBldr, TXFERCOMPLETEINSTANT, params);
		appendParamNameValue(paramsStrBldr, ABSRCVRPATH, params);
		appendParamNameValue(paramsStrBldr, ABSRCVRFILEPATH, params);
		
		return paramsStrBldr.toString();
	}
	
	public static Map<String, Object> getSerializableMap(Map<String, Object> source) {
		Map<String, Object> seralizableMap = new HashMap<String, Object>(source);
		
		seralizableMap.remove(RCVRFILEOUTPUTSTREAM.name());
		seralizableMap.remove(RECEIVERCHUNKLISTENER.name());
		seralizableMap.remove(RCVRCHKEDOUTPUTSTREAM.name());
		
		return seralizableMap;
	}
	
	public static Map<String, String> convertToDTOMap(Map<String, Object> params) {
		final Map<String, String> dtoMap = new HashMap<String, String>();
		
		if (params != null && MapUtils.isNotEmpty(params)) {
			params.keySet().stream().forEach(key -> {
				switch (ChunkKey.valueOf(key)) {
					case CHUNKDATA:
					case RCVRFILEOUTPUTSTREAM:
					case RECEIVERCHUNKLISTENER:
					case RCVRCHKEDOUTPUTSTREAM:
						break;
					case CHUNKTXFERMODE:
						dtoMap.put(key, ((ChunkTxferMode)params.get(key)).name());
						break;
					default:
						dtoMap.put(key, params.get(key).toString());
						break;
				}
			});
		}
		
		return dtoMap;
	}
}