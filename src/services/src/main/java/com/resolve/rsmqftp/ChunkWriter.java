/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmqftp;


import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.collections.MapUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.rsmqftp.Options.ChunkTxferMode;
import com.resolve.util.Log;

/**
 * Reads chunk details from blocking queue and sends them to 
 * <code>MRSMQFTPRECEIVER_<Txfer Id></code> ESB queue.
 * <p> 
 * Message handler {@link MRSMQFTPReceiver} processes the chunk details messages
 * sent to it.
 * 
 * @author HemantPhanasgaonkar
 * @version 1.0
 * @since Post 6.3
 */
public final class ChunkWriter {
	public static final String WRITE_CHUNK_METHOD_NAME = "writeChunk";
	public static final long SYNC_CHUNK_TXFER_MODE_TIMEOUT = 30000; // 30 secs
	
	private static final String MRSMQFTPRECEIVER_CLASS_NAME = MRSMQFTPReceiver.class.getSimpleName();
	private static final String MRSMQFTPRECEIVER_CLASS_UPPERCASE_NAME = MRSMQFTPRECEIVER_CLASS_NAME.toUpperCase();
		
	private String txferId;	
	private int totalChunks;
	private int chunksSent;
	private int chunksAcked;
	private Options options;
	private BlockingQueue<Map<String, Object>> bChunkQueue;
	private Map<String, Object> txferInProgressStatus;
	private int rcvrTxfersInProgressCount;
	
	public ChunkWriter(String txferId, int totalChunks, Options options, 
					   BlockingQueue<Map<String, Object>> bChunkQueue,
					   Map<String, Object> txferInProgressStatus,
					   int rcvrTxfersInProgressCount) throws RSMQFTPException {
		this.txferId = txferId;
		this.totalChunks = totalChunks;
		this.options = options;
		this.bChunkQueue = bChunkQueue;
		this.txferInProgressStatus = txferInProgressStatus;
		this.rcvrTxfersInProgressCount = rcvrTxfersInProgressCount;
		
		chunksSent = 0;
		chunksAcked = 0;		
	}
	
	private boolean canSendChunkToESBQueue() {
		if (options.getChunkTxferMode().equals(ChunkTxferMode.SYNC)) {
			return chunksSent == chunksAcked;
		} else if (options.getChunkTxferMode().equals(ChunkTxferMode.BUFFERED)) {
			return (chunksSent - chunksAcked) < options.getBlockingQueueCapacity();
		} else {
			return false;
		}
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public void syncWriteToESBQueue(Map<String, Object> chunk) throws RSMQFTPException {
		Map<String, Object> chunkTxferResp = null;
		
		try {
			chunk.put(ChunkKey.CHUNKTXFERMODE.name(), Options.ChunkTxferMode.SYNC);
			/*
			 * For every additional receiver txfer in progress, increase receive response timeout from receiver by 5 secs.
			 * Assumption is that with every additional txfer in progress on receiver, its time to synchronously
			 * send the response increases fraction of total response time and this increase is linear.
			 * 
			 * Note: Above assumption fails miserably especially even with single sender sending to itself a same file 
			 *       of 50-100mb size with only two sessions simultaneously. Sync call times out for one of the session. 
			 *       Please note even increasing timeout n times the # of txfers in progress at receivers end, fails to 
			 *       respond within timeout.
			 *       
			 *       Preliminary analysis indicates that main reason is single channel used for sending messages to 
			 *       ESB queues/topics. 
			 *       
			 *       Possible solution is to create channel pool in ESB which can be used to send messages.
			 *       
			 *       Once above is done, logic to increase response time based on # of simultaneous txfers in progress
			 *       on receiver end should be removed.
			 *       
			 *       Asynchronous chunk transfer will help at the cost of reduced transfer speeds but will make trasnfer
			 *       most stable as it will be able to withstand loss of RSMQ connectivity between components.
			 *       
			 *       Channel pool along with asynchronous chunk transfer(BUFFERED txfer mode) will not only 
			 *       speed up single large file transfers to receiver and make it stable but simultaneous large 
			 *       file transfers to same or multiple receivers will not affect the txfer speed drastically. 
			 */
			chunkTxferResp = MainBase.main.esb.call(String.format("%s_%s", MRSMQFTPRECEIVER_CLASS_UPPERCASE_NAME,
														 		  txferId.toUpperCase()), 
										   			String.format("%s.%s", MRSMQFTPRECEIVER_CLASS_NAME, 
										   						  WRITE_CHUNK_METHOD_NAME),
										   			chunk, SYNC_CHUNK_TXFER_MODE_TIMEOUT + 
										   				   (rcvrTxfersInProgressCount > 1 ? 
										   					(SYNC_CHUNK_TXFER_MODE_TIMEOUT / 6) * 
										   					(rcvrTxfersInProgressCount - 1) : 0));
		} catch (Exception e) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "syncWriteToESBQueue", 
															 String.format("%s-%s.esb.call", MainBase.main.release.hostName, 
																	 	   MainBase.main.release.serviceName), e);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			txferInProgressStatus.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
			throw rsmqftpe;
		}
		
		chunksSent++;
		
		if (chunkTxferResp == null || MapUtils.isEmpty(chunkTxferResp)) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "syncWriteToESBQueue", 
			 		 										 String.format("Received no response for %s.%s from " +
			 		 												 	   "receiver component listening to queue " +
			 		 												 	   "%s_%s for chunk [%s]", 
			 		 												 	   MRSMQFTPRECEIVER_CLASS_NAME, 
			 		 												 	   WRITE_CHUNK_METHOD_NAME, 
			 		 												 	   MRSMQFTPRECEIVER_CLASS_UPPERCASE_NAME,
			 		 												 	   txferId.toUpperCase(),
			 		 												 	   ChunkKey.txferMsgMapLogger(chunk)));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			txferInProgressStatus.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
			throw rsmqftpe;
		}
		
		if (MapUtils.isNotEmpty(chunkTxferResp) && chunkTxferResp.containsKey(ChunkKey.RSMQFTPEXCEPTION.name())) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "syncWriteToESBQueue", 
			 		 										 String.format("Received chunk response [%s] " +
		 		 												 	   	   "for chunk [%s] has error [%s]",
		 		 												 	   	   ChunkKey.txferMsgMapLogger(chunkTxferResp),
			 		 												 	   ChunkKey.txferMsgMapLogger(chunk),
			 		 										 			   ((RSMQFTPException)(chunkTxferResp
			 		 										 			     .get(ChunkKey.RSMQFTPEXCEPTION.name())))
			 		 										 			   .getMessage()));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			txferInProgressStatus.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
			throw rsmqftpe;
		}
		
		chunksAcked++;
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("[%s] : Received chunk response [%s] for chunk [%s]", this.toString(), 
										ChunkKey.txferMsgMapLogger(chunkTxferResp),
										ChunkKey.txferMsgMapLogger(chunk)));
		}
		
		// Update receiver txfers in progress count based on the receiver response
		if (chunkTxferResp.containsKey(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name())) {
			/*
			 * Please see note above.
			 */
			if (rcvrTxfersInProgressCount < 
				((Integer)chunkTxferResp.get(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name())).intValue()) {
				rcvrTxfersInProgressCount = 
					((Integer)chunkTxferResp.get(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name())).intValue();	
			}
//			rcvrTxfersInProgressCount = ((Integer)chunkTxferResp.get(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name())).intValue();
		}
		
		txferInProgressStatus.putAll(chunkTxferResp);
	}
	
	public void readChunkAndWriteToESBQueue() throws RSMQFTPException {
		while (chunksSent < totalChunks && canSendChunkToESBQueue()) {
			Map<String, Object> chunk = null;
			
			try {
				chunk = bChunkQueue.take();
			} catch (InterruptedException ie) {
				RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "readChunkAndWriteToESBQueue", 
				"take() from blocking queue", ie);
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				txferInProgressStatus.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
				throw rsmqftpe;
			}
			
			chunk.put(ChunkKey.CHUNKSENTTOESBTIME.name(), Long.valueOf(System.currentTimeMillis()));
			
			if (options.getChunkTxferMode().equals(ChunkTxferMode.SYNC)) {
				syncWriteToESBQueue(chunk);
			} else if (options.getChunkTxferMode().equals(ChunkTxferMode.BUFFERED)) {
				RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "readChunkAndWriteToESBQueue", 
															 	 String.format("Chunk Transfer Mode %s is currently " +
																	 	   	   "not supported", 
																	 	   	   options.getChunkTxferMode().name()));
				Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
				txferInProgressStatus.put(ChunkKey.RSMQFTPEXCEPTION.name(), rsmqftpe);
				throw rsmqftpe;
			}				
		}
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("%s: Finished", this.toString()));
		}
	}
	
	@Override
	public String toString() {
		return String.format("%s Transfer Id: %s, Total Chunks: %d, Blocking Queue Size: %d, Chunks Sent: %d, " + 
							 "Chunks Acked: %d", 
							 ChunkWriter.class.getSimpleName(), txferId, totalChunks, bChunkQueue.size(), chunksSent,
							 chunksAcked);
	}
}