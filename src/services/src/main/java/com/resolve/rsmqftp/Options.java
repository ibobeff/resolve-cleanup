/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmqftp;

import java.time.Duration;

/**
 * FTP over ESB options.
 * 
 * @author HemantPhanasgaonkar
 * @version 1.0
 * @since Post 6.3
 */
public final class Options {	
	private static final int DEFAULT_CHUNK_SIZE = 262144; // 256 KB 
	private static final int MAX_CHUNK_SIZE = 1048576; // 1 MB
	private int chunkSize = DEFAULT_CHUNK_SIZE;			
	
	private static final int DEFAULT_BLOCKING_QUEUE_CAPACITY = 8;
	private static final int MAX_BLOCKING_QUEUE_CAPACITY = 16;
	private int blockingQueueCapacity = DEFAULT_BLOCKING_QUEUE_CAPACITY;
	
	private static final long DEFAULT_START_RECEIVER_TIME_OUT = 2000; // 2 secs
	private static final long MAX_START_RECEIVER_TIME_OUT = 10000; // 10 secs 
	private long startReceiverTimeOut = DEFAULT_START_RECEIVER_TIME_OUT;
	
	private static final Duration DEFAULT_SENDER_SESSION_KEEP_ALIVE_DURATION = Duration.ofMinutes(2); // 2 minute
	private static final Duration MAX_SENDER_SESSION_KEEP_ALIVE_DURATION = Duration.ofMinutes(10); // 10 minute
	// Duration to keep alive session without any txfer in progress
	private Duration sessionKeepAliveDur = DEFAULT_SENDER_SESSION_KEEP_ALIVE_DURATION;
	
	private static final Duration DEFAULT_COMPLETED_OR_ERRORED_TXFERS_IN_PROGRESS_KEEP_ALIVE_DURATION = 
																				Duration.ofMinutes(5); // 5 minutes
	private static final Duration MAX_COMPLETED_OR_ERRORED_TXFERS_IN_PROGRESS_KEEP_ALIVE_DURATION = 
																				Duration.ofMinutes(15); // 15 minutes
	// Duration to keep alive txfers in progress which have either finished or errored out
	private Duration txfersInProgressKeepAliveDur = DEFAULT_COMPLETED_OR_ERRORED_TXFERS_IN_PROGRESS_KEEP_ALIVE_DURATION;
		
	/**
	 * <code>SYNC</code>
	 * <p>
	 * Chunk is sent to ESB queue using synchronous call which waits for the response from
	 * receiver for 30 secs before sending next chunk.
	 * <p>
	 * If the response is not received within 30 secs the txfer is aborted.
	 * <p>
	 * May not be optimal if ESB connection between sender and receiver is unreliable.
	 * <p>
	 * Busy ESB broker or busy receiver may not be able to process the received data within 30 secs
	 * if receiver is busy with multiple txfers.
	 * <p>
	 * <p>
	 * <code>BUFFERED</code>
	 * <p>
	 * Chunks is sent to ESB queue asynchronously.
	 * <p>
	 * Receiving component will write chunks in proper order.
	 * <p>
	 * Acknowledgement of chunk processed from remote is received asynchronously.
	 * <p> 
	 * Number of unacknowledged chunks in queue will never be more than <code>blockingQueueCapacity</code>
	 * at any time thus not overloading ESB and receiving component which could be busy.
	 * <p>
	 * Unreliable ESB connection between sender and receiver does not affect transfer.
	 */
	public enum ChunkTxferMode {SYNC, BUFFERED}
	
	private ChunkTxferMode chunkTxferMode = ChunkTxferMode.SYNC;
	
	public int getChunkSize() {
		return chunkSize;
	}
	
	public void setChunkSize(int chunkSize) {
		if (chunkSize < DEFAULT_CHUNK_SIZE) {
			this.chunkSize = DEFAULT_CHUNK_SIZE;
		} else if (chunkSize > MAX_CHUNK_SIZE) {
			this.chunkSize = MAX_CHUNK_SIZE;
		} else {
			this.chunkSize = chunkSize;
		}
	}
	
	public int getBlockingQueueCapacity() {
		return blockingQueueCapacity;
	}
	
	public void setBlokcingQueueCapacity(int blockingQueueCapacity) {
		if (blockingQueueCapacity < DEFAULT_BLOCKING_QUEUE_CAPACITY) {
			this.blockingQueueCapacity = DEFAULT_BLOCKING_QUEUE_CAPACITY;
		} else if (blockingQueueCapacity > MAX_BLOCKING_QUEUE_CAPACITY) {
			this.blockingQueueCapacity = MAX_BLOCKING_QUEUE_CAPACITY;
		} else {
			this.blockingQueueCapacity = blockingQueueCapacity;
		}
	}
	
	public long getStartReceiverTimeOut() {
		return startReceiverTimeOut;
	}
	
	public void setStartReceiverTimeOut(long startReceiverTimeOut) {
		if (startReceiverTimeOut < DEFAULT_START_RECEIVER_TIME_OUT) {
			this.startReceiverTimeOut = DEFAULT_START_RECEIVER_TIME_OUT;
		} else if (startReceiverTimeOut > MAX_START_RECEIVER_TIME_OUT) {
			this.startReceiverTimeOut = MAX_START_RECEIVER_TIME_OUT;
		} else {
			this.startReceiverTimeOut = startReceiverTimeOut;
		}
	}
	
	public Duration getSessionKeepAliveDur() {
		return sessionKeepAliveDur;
	}
	
	public void setSessionKeepAliveDur(Duration sessionKeepAliveDur) {
		if (sessionKeepAliveDur != null) {
			if (sessionKeepAliveDur.compareTo(DEFAULT_SENDER_SESSION_KEEP_ALIVE_DURATION) < 0) {
				this.sessionKeepAliveDur = DEFAULT_SENDER_SESSION_KEEP_ALIVE_DURATION;
			} else if (sessionKeepAliveDur.compareTo(MAX_SENDER_SESSION_KEEP_ALIVE_DURATION) > 0) {
				this.sessionKeepAliveDur = MAX_SENDER_SESSION_KEEP_ALIVE_DURATION;
			} else if (sessionKeepAliveDur.compareTo(this.sessionKeepAliveDur) != 0) {
				this.sessionKeepAliveDur = sessionKeepAliveDur;
			}
		}
	}
	
	public Duration getTxfersInProgressKeepAliveDur() {
		return txfersInProgressKeepAliveDur;
	}
	
	public void setTxfersInProgressKeepAliveDur(Duration txfersInProgressKeepAliveDur) {
		if (txfersInProgressKeepAliveDur != null) {
			if (txfersInProgressKeepAliveDur
				.compareTo(DEFAULT_COMPLETED_OR_ERRORED_TXFERS_IN_PROGRESS_KEEP_ALIVE_DURATION) < 0) {
				this.txfersInProgressKeepAliveDur = DEFAULT_COMPLETED_OR_ERRORED_TXFERS_IN_PROGRESS_KEEP_ALIVE_DURATION;
			} else if (txfersInProgressKeepAliveDur
					   .compareTo(MAX_COMPLETED_OR_ERRORED_TXFERS_IN_PROGRESS_KEEP_ALIVE_DURATION) > 0) {
				this.txfersInProgressKeepAliveDur = MAX_COMPLETED_OR_ERRORED_TXFERS_IN_PROGRESS_KEEP_ALIVE_DURATION;
			} else if (txfersInProgressKeepAliveDur.compareTo(this.txfersInProgressKeepAliveDur) != 0) {
				this.txfersInProgressKeepAliveDur = txfersInProgressKeepAliveDur;
			}
		}
	}
	
	public ChunkTxferMode getChunkTxferMode() {
		return chunkTxferMode;
	}
	
	public void setChunkTxferMode(ChunkTxferMode chunkTxferMode) {
		if (chunkTxferMode != null) {
			this.chunkTxferMode = chunkTxferMode;
		}
	}
	
	@Override
	public String toString() {
		return String.format("%s Chunk Size: %d bytes, Blokcing Queue Capacity: %d, Start Receiver Time Out: %d msecs, " +
							 "Session Keep Alive Duration: %s, " +
							 "Txfers In Progress (Completed/Errored) Keep Alive Duration: %s", 
							 Options.class.getSimpleName(), chunkSize, blockingQueueCapacity, startReceiverTimeOut,
							 sessionKeepAliveDur, txfersInProgressKeepAliveDur);
	}
}