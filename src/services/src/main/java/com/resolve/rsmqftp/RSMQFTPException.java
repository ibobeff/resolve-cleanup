/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmqftp;

import org.apache.commons.lang3.StringUtils;

/**
 * Exception in FTP over ESB.
 * 
 * @author HemantPhanasgaonkar
 * @version 1.0
 * @since Post 6.3
 */
public final class RSMQFTPException extends Exception {
	private static final String UNKNOWN = "Unknown";
	
	public String instanceLog;
	public String methodName;
	public String errMsg;
	
	public RSMQFTPException(String instanceLog, String methodName, String errMsg) {
		super(String.format("Instance: [%s], Method: %s, Error Message: [%s]", 
							StringUtils.isNotBlank(instanceLog) ? instanceLog : UNKNOWN, 
							StringUtils.isNotBlank(methodName) ? methodName : UNKNOWN, 
							StringUtils.isNotBlank(errMsg) ? errMsg : UNKNOWN));
	}
	
	public RSMQFTPException(String instanceLog, String methodName, String errMsg, Throwable cause) {
		super(String.format("Instance: [%s], Method: %s, Error Message: [%s], Cause Error Message: [%s]", 
							StringUtils.isNotBlank(instanceLog) ? instanceLog : UNKNOWN, 
							StringUtils.isNotBlank(methodName) ? methodName : UNKNOWN, 
							StringUtils.isNotBlank(errMsg) ? errMsg : UNKNOWN, 
							StringUtils.isNotBlank(cause.getMessage()) ? cause.getMessage() : UNKNOWN), cause);
	}
}