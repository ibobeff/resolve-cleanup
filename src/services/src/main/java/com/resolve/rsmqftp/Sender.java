/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmqftp;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.rsbase.MainBase;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Reads and sends file to ESB broker.
 * <p>
 * File is read in chunks and chunks are sent to <code>ESB</code> broker. Various options such as chunk size, 
 * transfer modes for chunk <code>SYNC</code>, <code>ASYNC</code> or ESB modes <code>P2P</code>, 
 * <code>Publish/Subscribe</code> etc can be controlled using options. 
 * <p>
 * Number of file transfers per component and other restrictions should be implemented by 
 * extending classes for example BluePrintSender. BluePrint sender may wish to limit sending
 * blueprint.properties file from cluster to single <code>RSRemote</code> and may wish to limit
 * only single transfer across cluster..  
 * 
 * @author HemantPhanasgaonkar
 * @version 1.0
 * @since Post 6.3
 */
public class Sender {
	private static final String START_RECEIVER_METHOD_NAME = "startReceiver";
	private static final String CHUNK_READER_READ_CHUNK_WRITE_TO_QUEUE = "readChunkAndWriteToQueue";
	private static final String CHUNK_WRITER_READ_CHUNK_WRITE_TO_ESB_QUEUE = "readChunkAndWriteToESBQueue";
	
	private static ConcurrentMap<String, Sender> senders = new ConcurrentHashMap<String, Sender>();
	private static ConcurrentMap<String, Map<String, Object>> txfersInProgress = 
																	new ConcurrentHashMap<String, Map<String, Object>>();
	
	private String txferId;										// Txfer id used to uniquely identify this transfer
	private Instant sessCreateInstant;							// Session create instant
	private String userId;
	
	private String senderPath;
	private String senderFileName;
	private String receiverPath;								// Can be relative to receiver component home if starts with .
	private String receiverFileName;							// Optional, defaults to sender file name if not specified
	
	private Options options = new Options();
	// Holds Chunks produced by ChunkReader and consumed by ChunWriter
	private BlockingQueue<Map<String, Object>> bChunkQueue;
	private String chunkReaderJobId;							// chunk reader executor job id in case needed to stop txfer	
	private String rcvrCompId;									// Receiver component Id (GUID), 
																// defaults to current component id (GUID) 
	private String chunkWriterJobId;							// chunk writer executor job id in case needed to stop txfer
	private ChunkWriter chunkWriterInstance;					// chunk writer instance in case needed to send 
	
	static {
		// Clear out Txfers In Progress and Senders Thread, runs every 1 minute
		new Thread() {
			public void run()
    		{    		
				while (true) {
					try {
						
						
						if (MapUtils.isNotEmpty(txfersInProgress)) {
							// Clear out txfers in progress which have completed or errored out
							final List<String> txferIdsInProgress = new ArrayList<String>(txfersInProgress.keySet());
							
							List<String> removeTxfersInProgressIds = 
											txferIdsInProgress.stream()
											.filter(txferIdInProgress -> {
												/* Remove if stale
												 * Stale = Either Exception or Tafer Is Complete and 
												 * Instant of either of above + keep alive time is before now
												 */
												return ((txfersInProgress.get(txferIdInProgress)
													     .containsKey(ChunkKey.RSMQFTPEXCEPTIONINSTANT.name())) &&
													    ((((Instant)txfersInProgress.get(txferIdInProgress)
													       .get(ChunkKey.RSMQFTPEXCEPTIONINSTANT.name()))
													      .plusSeconds(senders.get(txferIdInProgress).getOptions()
															 	       .getTxfersInProgressKeepAliveDur().toMinutes() * 60))
													     .isBefore(Instant.now()))) ||
														((txfersInProgress.get(txferIdInProgress)
														  .containsKey(ChunkKey.TXFERCOMPLETEINSTANT.name())) &&
														 ((((Instant)txfersInProgress.get(txferIdInProgress)
															.get(ChunkKey.TXFERCOMPLETEINSTANT.name()))
														   .plusSeconds(senders.get(txferIdInProgress).getOptions()
																	 	.getTxfersInProgressKeepAliveDur().toMinutes() * 60))
														  .isBefore(Instant.now())));
											}).collect(Collectors.toList());
							
							if (CollectionUtils.isNotEmpty(removeTxfersInProgressIds)) {
								removeTxfersInProgressIds.stream().forEach(removeTxfersInProgressId -> {
									Map<String, Object> txferInProgressDetails = 
															txfersInProgress.remove(removeTxfersInProgressId);
									Log.log.info(String.format("Cleared Txfer In Progress [%s]", 
															   ChunkKey.txferMsgMapLogger(txferInProgressDetails)));
								});
							} else {
								Log.log.debug("No Txfer In Progresses to clear");
							}
						}
						
						if (MapUtils.isNotEmpty(senders)) {
							// Clear out senders with txferIds which do not have any txfers in progress
							final List<String> sessionTxferIds = new ArrayList<String>(senders.keySet());
							
							List<String> removeTxferIds = sessionTxferIds.stream()
														  .filter(sessTxferId -> {
															  return (!txfersInProgress.containsKey(sessTxferId)) &&
																   	 (senders.get(sessTxferId).getSessCreateInstant()
																   	  .plusSeconds(senders.get(sessTxferId).getOptions()
																   				   .getSessionKeepAliveDur()
																   				   .toMinutes() * 60)
																   	  .isBefore(Instant.now()));
														  }).collect(Collectors.toList());
							
							if (CollectionUtils.isNotEmpty(removeTxferIds)) {
								removeTxferIds.stream().forEach(sessTxferIdToRemove -> {
									Sender removedSender = senders.remove(sessTxferIdToRemove);
									Log.log.info(String.format("Cleared %s [%s]", Sender.class.getSimpleName(), removedSender));
								});
							} else {
								Log.log.debug(String.format("No %ss to clear", Sender.class.getSimpleName()));
							}
						}
						
						Thread.sleep(60*1000);
					} catch (Throwable t) {
						Log.log.error(String.format("Error %sin %s cleanup thread", 
													(StringUtils.isNotBlank(t.getMessage()) ? 
																		    "[" + t.getMessage() + "] " : ""),
									  Sender.class.getSimpleName()), t);
					}
				}
    		}
		}.start();
	}
	
	public Sender() {
		txferId = Guid.getGuid();
		sessCreateInstant = Instant.now();
		senders.put(txferId, this);
	}
	
	public Sender (String userId, String senderPath, String senderFileName, String receiverPath, 
				   String receiverFileName) {
		if (StringUtils.isBlank(userId)) throw new IllegalArgumentException("User id is required");
		
		if (StringUtils.isBlank(senderPath)) throw new IllegalArgumentException("Sender file path is required");
		
		if (StringUtils.isBlank(senderFileName)) throw new IllegalArgumentException("Sender file name is required");
		
		if (StringUtils.isBlank(receiverPath)) throw new IllegalArgumentException("Receiver file path is required");
		
		txferId = Guid.getGuid();
		sessCreateInstant = Instant.now();
		this.userId = userId;
		this.senderPath = senderPath;
		this.senderFileName = senderFileName;
		this.receiverPath = receiverPath;
		this.receiverFileName = receiverFileName;
		
		if (StringUtils.isBlank(this.receiverFileName)) {
			this.receiverFileName = this.senderFileName;
		}
		
		senders.put(txferId, this);
	}
	
	public String getTxferId() {
		return txferId;
	}
	
	public Instant getSessCreateInstant() {
		return sessCreateInstant;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getSenderPath() {
		return senderPath;
	}
	
	public void setSenderPath(String senderPath) {
		this.senderPath = senderPath;
	}
	
	public String getSenderFileName() {
		return senderFileName;
	}
	
	public void setSenderFileName(String senderFileName) {
		this.senderFileName = senderFileName;
	}
	
	public String getReceiverPath() {
		return receiverPath;
	}
	
	public void setReceiverPath(String receiverPath) {
		this.receiverPath = receiverPath;
	}
	
	public String getReceiverFileName() {
		return StringUtils.isNotBlank(receiverFileName) ? receiverFileName : senderFileName;
	}
	
	public void setReceiverFileName(String receiverFileName) {
		if (StringUtils.isNotBlank(receiverFileName)) {
			this.receiverFileName= receiverFileName;
		}
	}
	
	public Options getOptions() {
		if (options == null) {
			options = new Options();
		}
		return options;
	}
	
	public void setOptions(Options options) {
		this.options = options;
	}
	
	public String getRcvrCompId() {
		return rcvrCompId;
	}
	
	public void setRcvrCompId(String rcvrCompId) {
		if (StringUtils.isBlank(rcvrCompId)) {
			this.rcvrCompId = MainBase.main.configId.guid;
		} else {
			this.rcvrCompId = rcvrCompId;
		}
	}
	
	@SuppressWarnings("static-access")
	private void validate() throws RSMQFTPException {
		if (MainBase.main.esb == null) throw new RSMQFTPException(this.toString(), "validate", 
														 String.format("%s does not have required ESB component",
																 	   MainBase.main.getClass().getSimpleName()),
														 new IllegalArgumentException("User id is required"));
		
		if (StringUtils.isBlank(userId)) throw new RSMQFTPException(this.toString(), "validate", null,
																	new IllegalArgumentException("User id is required"));
		
		if (StringUtils.isBlank(senderPath)) throw new RSMQFTPException(
															this.toString(), "validate", null,
															new IllegalArgumentException("Sender file path is required"));
		
		if (StringUtils.isBlank(senderFileName)) throw new RSMQFTPException(
															this.toString(), "validate", null, 
															new IllegalArgumentException("Sender file name is required"));
		
		if (StringUtils.isBlank(receiverPath)) throw new RSMQFTPException(
															this.toString(), "validate", null, 
															new IllegalArgumentException("Receiver file path is required"));
	}
	
	private Map<String, Object> buildStartReceiverRequest(long fileSize) {
		Map<String, Object> startReceiverReq = new HashMap<String, Object>();
		
		startReceiverReq.put(ChunkKey.TXFERID.name(), txferId);
		startReceiverReq.put(ChunkKey.USERID.name(), userId);
		startReceiverReq.put(ChunkKey.RCVRPATH.name(), receiverPath);
		startReceiverReq.put(ChunkKey.RCVRFILENAME.name(), receiverFileName);
		startReceiverReq.put(ChunkKey.FILESIZE.name(), Long.valueOf(fileSize));
		startReceiverReq.put(ChunkKey.SESSIONCREATEINSTANT.name(), sessCreateInstant);
		
		return startReceiverReq;
	}
	
	@SuppressWarnings("unchecked")
	private int initiateReceiver(long fileSize) throws RSMQFTPException {
		Map<String, Object> startReceiverReq = buildStartReceiverRequest(fileSize);
		Map<String, Object> startReceiverResp = null;
		
		try {
			startReceiverResp = MainBase.esb.call(String.format("RSMQFTP_%s", getRcvrCompId()), 
												  String.format("%s.%s", MRSMQFTPReceiver.class.getSimpleName(),
															    START_RECEIVER_METHOD_NAME),
												  ChunkKey.getSerializableMap(startReceiverReq), 
												  getOptions().getStartReceiverTimeOut());
		} catch (Exception e) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "initiateReceiver", 
					 										 String.format("Host Name: %s, Service: %s, File Size: %d", 
					 												 	   MainBase.main.release.hostName, 
					 												 	   MainBase.main.release.serviceName,
					 												 	   fileSize), 
					 										 e);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		if (startReceiverResp == null || MapUtils.isEmpty(startReceiverResp)) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "initiateReceiver", 
			 		 										 String.format("Received no response for %s.%s from Receiver " +
			 		 												 	   "component listening to queue RSMQFTP_%s", 
			 		 												 	   MRSMQFTPReceiver.class.getSimpleName(), 
			 		 												 	   START_RECEIVER_METHOD_NAME, getRcvrCompId()));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		if (MapUtils.isNotEmpty(startReceiverResp) && startReceiverResp.containsKey(ChunkKey.RSMQFTPEXCEPTION.name())) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "initiateReceiver", 
					 								 		 String.format("%s.%s failed to start Receiver", 
					 								 				 	   MRSMQFTPReceiver.class.getSimpleName(), 
					 								 				 	   START_RECEIVER_METHOD_NAME),
					 								 		 (RSMQFTPException)startReceiverResp
					 								 				   		   .get(ChunkKey.RSMQFTPEXCEPTION.name()));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		startReceiverReq.putAll(startReceiverResp);
		txfersInProgress.put(txferId, startReceiverReq);
		
		return startReceiverResp.containsKey(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name()) ? 
			   ((Integer)startReceiverResp.get(ChunkKey.RCVRTXFERSINPROGRESSCOUNT.name())).intValue() : 1;
	}
	
	private Pair<ChunkReader, ChunkWriter> setupSenderForSend() throws RSMQFTPException {
		validate();
		bChunkQueue = new ArrayBlockingQueue<Map<String, Object>>(getOptions().getBlockingQueueCapacity());
		ChunkReader chunkReader = new ChunkReader(getTxferId(), getSenderPath(), getSenderFileName(), getOptions(),
												  bChunkQueue);
		int rcvrTxfersInProgressCount = initiateReceiver(chunkReader.getLength());
		ChunkWriter chunkWriter = new ChunkWriter(getTxferId(), chunkReader.getTotalChunks(), getOptions(), 
												  bChunkQueue, txfersInProgress.get(txferId), rcvrTxfersInProgressCount);
		chunkWriterInstance = chunkWriter;
		return Pair.of(chunkReader, chunkWriter);
	}
	
	private void startSenderSend(Pair<ChunkReader, ChunkWriter> chunkReaderWriterPair) {
		chunkWriterJobId = TaskExecutor.execute(chunkReaderWriterPair.getRight(), CHUNK_WRITER_READ_CHUNK_WRITE_TO_ESB_QUEUE);
		chunkReaderJobId = TaskExecutor.execute(chunkReaderWriterPair.getLeft(), CHUNK_READER_READ_CHUNK_WRITE_TO_QUEUE);
	}
	
	/**
	 * Sends file chunks to ESB broker synchronously.
	 * <p>
	 * Sender after successful setup of file transfer, starts send in current thread. 
	 * If current thread exits before transfer is complete, file transfer stops immediately.
	 * <p>
	 * This method is suitable for small file transfers avoiding overhead of separate thread
	 * to check on completion.
	 * <p> 
	 * {@link ChunkReader} reads chunk of data from stream and puts chunk data 
	 * along with other details into capacity restricted blocking queue.
	 * <p>  
	 * {@link ChunkWriter} takes chunks from queue and writes them to specified queue/topic in ESB broker.
	 * <p>
	 * Both {@link ChunkReader} and {@link ChunkWriter} perform their operations using {@link TaskExecutor}.
	 * 
	 * @return						Pair containing Txfer id to asynchronously query status and absolute receiver file path
	 * @throws RSMQFTPException		If any error occurs such as file not readable or is not a file or 
	 * 								file does not exists or any error while reading data and sending to
	 * 								ESB broker.
	 */
	public Pair<String, String> syncSend() throws RSMQFTPException {
		if (txfersInProgress.containsKey(txferId)) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "syncSend", 
			 		 										 "Previous transfer is already in progress");
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		Pair<ChunkReader, ChunkWriter> chunkReaderWriterPair = setupSenderForSend();
		String absRcvrPath = (String) txfersInProgress.get(txferId).get(ChunkKey.ABSRCVRFILEPATH.name());
		startSenderSend(chunkReaderWriterPair);
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(this);
		}
		
		return Pair.of(txferId, absRcvrPath);
	}
	
	private static class AsyncStartSenderSend {
		Sender sender;
		Pair<ChunkReader, ChunkWriter> chunkReaderWriterPair;
		
		AsyncStartSenderSend(Sender sender, Pair<ChunkReader, ChunkWriter> chunkReaderWriterPair) {
			this.sender = sender;
			this.chunkReaderWriterPair = chunkReaderWriterPair;
		}
		
		@SuppressWarnings("unused")
		public void startSenderSend() {
			sender.startSenderSend(chunkReaderWriterPair);
		}
	}
	
	/**
	 * Sends file chunks to ESB broker asynchronously.
	 * <p>
	 * Sender after successful setup of file transfer, starts send using <code>ForkJoinExecutor</code>
	 * in a separate thread. This allows transfer to continue even if the thread this method 
	 * is invoked from exits.
	 * <p>
	 * This method is suitable for large file transfers allowing separate thread
	 * to check on transfer status.
	 * <p>
	 * {@link ChunkReader} reads chunk of data from stream and puts chunk data 
	 * along with other details into capacity restricted blocking queue.
	 * <p>  
	 * {@link ChunkWriter} takes chunks from queue and writes them to specified queue/topic in ESB broker.
	 * <p>
	 * Both {@link ChunkReader} and {@link ChunkWriter} perform their operations using {@link TaskExecutor}.
	 * 
	 * @param	txferId						TxferId of the Sender to start asynchronous transfer.
	 * @return								Absolute receiver file path
	 * @throws	IllegalArgumentException	If txferId is null or no Sender with specified txferId is not found.
	 * @throws	RSMQFTPException			If any error occurs while setting up sender for transfer.
	 */
	public static String asyncSend(String txferId) throws RSMQFTPException {
		if (StringUtils.isBlank(txferId)) throw new IllegalArgumentException("Txfer id is required");
		
		if (!senders.containsKey(txferId)) {
			throw new IllegalArgumentException(String.format("No %s with specified txfer id %s found", 
															 Sender.class.getSimpleName(), txferId));
		}
		
		Sender sender = senders.get(txferId);
		
		if (sender == null) {
			RSMQFTPException rsmqftpe = new RSMQFTPException( null, "asyncSend", 
						 									  String.format("Failed to find %s for specified txferId %s",
						 											  		Sender.class.getSimpleName(), txferId));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		if (txfersInProgress.containsKey(txferId)) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(sender.toString(), "asyncSend", 
			 		 										 "Previous transfer is already in progress");
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		Pair<ChunkReader, ChunkWriter> chunkReaderWriter = sender.setupSenderForSend();		
		TaskExecutor.execute(new AsyncStartSenderSend(sender, chunkReaderWriter), "startSenderSend");
		
		return (String) txfersInProgress.get(txferId).get(ChunkKey.ABSRCVRFILEPATH.name());
	}
	
	/**
	 * Get Txfer Status
	 * 
	 * @param	txferId		Txfer id of transfer to get status for.
	 * @return				Txfer status.
	 * 						Null if no status is found for specified txferId.
	 */
	public static Map<String, Object> getStatus(String txferId) {
		if (StringUtils.isBlank(txferId)) throw new IllegalArgumentException("Txfer id is required");
		
		return txfersInProgress.get(txferId);
	}
	
	private static Pair<Sender, Map<String, Object>> getTxferInProgress(String txferId) throws RSMQFTPException {
		if (StringUtils.isBlank(txferId)) throw new IllegalArgumentException("Txfer id is required");
		
		Sender sender = senders.get(txferId);
		
		if (sender == null) {
			RSMQFTPException rsmqftpe = new RSMQFTPException( null, "getTxferInProgress", 
						 									  String.format("Failed to find %s for specified txferId %s",
						 											  		Sender.class.getSimpleName(), txferId));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		if (!txfersInProgress.containsKey(txferId)) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(sender.toString(), "getTxferInProgress", 
			 		 										 String.format("Failed to find any txfer in progress for " +
			 		 												 	   "specified txferId %s", txferId));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		Map<String, Object> txferInProgressDetails = txfersInProgress.get(txferId);
		
		if (txfersInProgress.containsKey(ChunkKey.RSMQFTPEXCEPTIONINSTANT.name()) ||
			txfersInProgress.containsKey(ChunkKey.TXFERCOMPLETEINSTANT.name())) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(ChunkKey.txferMsgMapLogger(txferInProgressDetails), 
															 "getTxferInProgress", 
															 String.format("Txfer In Progress for specified Txfer Id %s " +
																	 	   "is already complete or errored out", txferId));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		return Pair.of(sender, txferInProgressDetails);
	}
	
	
	@Override
	public String toString() {
		return String.format("%s Txfer Id: %s, Session Create Instant: %s, User Id: %s, " +
							 "Sender Path: %s, Sender File Name: %s, Receiver Pah: %s, Receiver File Name: %s, " +
							 "Blocking Queue Size: %d, Chunk Writer ForkJoinExecutor Job Id: %s, " +
							 "Chunk Reader ForkJoinExecutor Job Id: %s, Receiver ESB Queue Name: %s",
							 Sender.class.getSimpleName(), txferId, sessCreateInstant, userId, senderPath, senderFileName, 
							 receiverPath, getReceiverFileName(), bChunkQueue != null ? bChunkQueue.size() : 0, 
							 StringUtils.isNotBlank(chunkWriterJobId) ? chunkWriterJobId : "",
							 StringUtils.isNotBlank(chunkReaderJobId) ? chunkReaderJobId : "", 
							 StringUtils.isNotBlank(rcvrCompId) ? rcvrCompId : "");
	}
}