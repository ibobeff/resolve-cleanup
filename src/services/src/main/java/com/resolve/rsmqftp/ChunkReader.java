/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmqftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.util.Log;

/**
 * Reads chunks from input stream, computes checksum and writes chunk with
 * chunk details such as checksum, current chunk number, total number of chunks
 * to blocking queue.
 * 
 * @author HemantPhanasgaonkar
 * @version 1.0
 * @since Post 6.3
 */
public final class ChunkReader {
	private String txferId;
	private File fin;
	private FileInputStream fis;
	private Options options;
	private BlockingQueue<Map<String, Object>> bChunkQueue;
	private long length;
	private int totalChunks;
	private int chunksRead;
	private int chunksWritten;
	private Checksum runningCheckSum;	// CRC32
	
	public ChunkReader(String txferId, String localPath, String localFileName, Options options, 
					   BlockingQueue<Map<String, Object>> bChunkQueue) throws RSMQFTPException {
		this.txferId = txferId;
		this.options = options;
		this.bChunkQueue = bChunkQueue;
		
		fin = new File(localPath, localFileName);
		
		if (fin == null || !fin.exists() || !fin.isFile() || !fin.canRead()) {
			RSMQFTPException rsmqftpe = 
								new RSMQFTPException(ChunkReader.class.getSimpleName(), "constructor", 
									   				 String.format("%s%s%s does not exists or is directory or cannot be read",
											   		 localPath, File.separator, localFileName));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		try {
			fis = new FileInputStream(fin);
		} catch (FileNotFoundException fnfe) {
			RSMQFTPException rsmqftpe = 
								new RSMQFTPException(ChunkReader.class.getSimpleName(), "constructor", 
													 "Failed to get input stream", fnfe);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		length = fin.length();
		
		if (length <= 0) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(ChunkReader.class.getSimpleName(), "constructor", 
										 					 "File is empty");
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		totalChunks = (int)(length / options.getChunkSize()) + ((length % options.getChunkSize()) > 0 ? 1 : 0);
		chunksRead = 0;
		chunksWritten = 0;
		runningCheckSum = new CRC32();
	}
	
	public long getLength() {
		return length;
	}
	
	public int getTotalChunks() {
		return totalChunks;
	}
	
	private Pair<Integer, byte[]> readChunk(int chunkIndex) throws RSMQFTPException {
		int chunkSize = ((chunkIndex + 1) == totalChunks) ? 
						((int)(length - ((long)(chunksRead * options.getChunkSize())))) : options.getChunkSize();
		byte[] chunkBytes = new byte[chunkSize];

		int chunkReadSize = 0;

		try {
			chunkReadSize = fis.read(chunkBytes, 0, chunkSize);
		} catch (IOException e) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "readChunkAndWriteToQueue", 
	 					 									 String.format("Error reading %d bytes from input stream", 
	 					 											 	   chunkSize), e);
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		chunksRead++;
		
		if (chunkReadSize != chunkSize) {
			RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "readChunkAndWriteToQueue", 
						 									 String.format("Expected to read %d bytes from input " +
						 											 	   "stream does not match actual %d bytes read", 
						 											 	   chunkSize, chunkReadSize));
			Log.log.error(rsmqftpe.getMessage(), rsmqftpe);
			throw rsmqftpe;
		}
		
		return Pair.of(Integer.valueOf(chunkReadSize), chunkBytes);
	}
	
	private Map<String, Object> buildChunk(int chunkIndex, long checkSumValue, Pair<Integer, byte[]> chunkDetails) {
		Map<String, Object> chunk = new HashMap<String, Object>();
		
		chunk.put(ChunkKey.TXFERID.name(), txferId);
		chunk.put(ChunkKey.TOTALCHUNKS.name(), Integer.valueOf(totalChunks));
		chunk.put(ChunkKey.CURRENTCHUNK.name(), Integer.valueOf(chunkIndex+1));
		chunk.put(ChunkKey.CHUNKSIZE.name(), chunkDetails.getLeft());
		chunk.put(ChunkKey.CHUNKDATA.name(), chunkDetails.getRight());
		chunk.put(ChunkKey.RUNNINGCHECKSUM.name(), Long.valueOf(checkSumValue));
		chunk.put(ChunkKey.CHUNKCREATETIME.name(), Long.valueOf(System.currentTimeMillis()));
		
		return chunk;
	}

	public void readChunkAndWriteToQueue() throws RSMQFTPException {
		for (int i = 0; i < totalChunks; i++) {
			Pair<Integer, byte[]> chunkDetails = readChunk(i);
			
			if (chunkDetails != null) {
				runningCheckSum.update(chunkDetails.getRight(), 0, chunkDetails.getLeft());
				Map<String, Object> chunk = buildChunk(i, runningCheckSum.getValue(), chunkDetails);
				try {
					bChunkQueue.put(chunk);
				} catch (InterruptedException ie) {
					RSMQFTPException rsmqftpe = new RSMQFTPException(this.toString(), "readChunkAndWriteToQueue", 
							 										 "Interrupted while waiting to write chunk " +
							 										 "read from input stream to blocking queue", 
									 	   							 ie);
				}
				chunksWritten++;
			}
		}
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("%s: Finished", this.toString()));
		}
	}
	
	@Override
	public String toString() {
		return String.format("%s Transfer Id: %s, Blocking Queue Size: %d, File Size (in bytes): %d, Total Chunks: %d, " + 
							 "Chunks Read From File: %d, Chunks Written To Queue: %d, CheckSum: %d", 
							 ChunkReader.class.getSimpleName(), txferId, bChunkQueue.size(), length, totalChunks,
							 chunksRead, chunksWritten, runningCheckSum.getValue());
	}
}