/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
//import org.elasticsearch.action.WriteConsistencyLevel;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.model.query.Query;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class everything about all the indexing functionality in resolve that
 * uses ElasticSearch. This is for indexed document insert, update, and delete.
 */
public class IndexAPI
{
    private final String indexName;
    private final String documentType;
    private String indexAlias;
    private boolean isIndexAlias = false;
    private boolean isDynamicIndex = false;
    
    private final SearchService instance;

    IndexAPI() {
    	this.indexName = null;
    	this.documentType = null;
    	
    	instance = new SearchService();
    }
    
    IndexAPI(String indexName, String documentType)
    {
        this.indexName = indexName;
        this.documentType = documentType;
        
        instance = new SearchService();
    }

    IndexAPI(String indexName, String documentType, String indexAlias, boolean isDynamicIndex)
    {
        this(indexName, documentType);
        this.indexAlias = indexAlias;
        isIndexAlias = StringUtils.isNotBlank(indexAlias);
        this.isDynamicIndex = isDynamicIndex;
    }

    public String getIndexName()
    {
        return indexName;
    }

    public String getDocumentType()
    {
        return documentType;
    }

    public long getTotalCount()
    {
        long result = instance.getTotalCount(indexName, documentType);
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Total number of " + documentType + ":" + result);
        }
        return result;
    }

    public void indexSuggester(Collection<Query> searchQueries, String username) throws SearchException
    {
        instance.indexSuggester(indexName, documentType, searchQueries, username);
    }

    public String indexDocument(Serializable indexModel, boolean doRefresh, String username) throws SearchException
    {
        return indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, indexModel, doRefresh, username);
    }
    
    public void deleteArtifacts(final Collection<String> ids, String username) throws SearchException
    {
        QueryBuilder query = QueryBuilders.termsQuery("sysId", ids);
        deleteByQueryBuilder(query, false, username);
    }

    public String indexDocument(String idField, String createdOnField, Serializable indexModel, boolean doRefresh, String username) throws SearchException
    {
        return instance.index(indexName, documentType, idField, createdOnField, indexModel, doRefresh, false, username);
    }

    public String indexDocument(String idField, String createdOnField, Serializable indexModel, boolean doRefresh, ActiveShardCount writeConsistencyLevel, String username) throws SearchException
    {
        return instance.index(indexName, documentType, idField, createdOnField, indexModel, doRefresh, false, writeConsistencyLevel, username);
    }

    public void indexDocument(Serializable indexModel, String username) throws SearchException
    {
        instance.index(indexName, documentType, SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, indexModel, false, isDynamicIndex, username);
    }

    public void indexDocument(Serializable indexModel, boolean doRefresh, ActiveShardCount writeConsistencyLevel, String username) throws SearchException
    {
        indexDocument(indexModel, null, doRefresh, writeConsistencyLevel, username);
    }

    public void indexDocument(Serializable indexModel, String routingKeyField, boolean doRefresh, ActiveShardCount writeConsistencyLevel, String username) throws SearchException
    {
        instance.index(indexName, documentType, SearchConstants.SYS_ID, routingKeyField, SearchConstants.SYS_CREATED_ON, indexModel, doRefresh,  isDynamicIndex, writeConsistencyLevel, username);
    }

    public void indexDocuments(String idField, String createdOnField, Collection<? extends Serializable> indexModels, boolean doRefresh, String username) throws SearchException
    {
        instance.index(indexName, documentType, idField, createdOnField, indexModels, doRefresh, false, username);
    }

    public List<String> indexChildDocuments(String parentIdField, String idField, String createdOnField, Collection<? extends Serializable> indexModels, String username, boolean isRefresh) throws SearchException
    {
        return instance.indexChildren(indexName, documentType, parentIdField, idField, createdOnField, indexModels, username, isRefresh);
    }

    public void deleteDocumentById(final String id, String username) throws SearchException
    {
        deleteDocumentById(id, null, username);
    }

    public void deleteDocumentById(final String id, String username, boolean isSync) throws SearchException
    {
        deleteDocumentById(id, null, username, isSync);
    }
    
    public void deleteDocumentById(final String id, String routingKey, String username) throws SearchException
    {
        deleteDocumentById(id, routingKey, username, false);
    }

    public void deleteDocumentById(final String id, String routingKey, String username, boolean isSync) throws SearchException
    {
        if(isIndexAlias)
        {
            Collection<String> sysIds = new ArrayList<String>();
            sysIds.add(id);
            if (isSync)
            {
                deleteDocumentByIds(sysIds, true, username);
            }
            else
            {
                deleteDocumentByIds(sysIds, username);
            }
        }
        else
        {
            instance.deleteById(indexName, documentType, id, routingKey);
        }
    }
    
    public void deleteDocumentByIds(final Collection<String> ids, String username) throws SearchException
    {
        deleteDocumentByIds(ids, false, username);
    }

    public Collection<String> deleteDocumentByIds(final Collection<String> ids, boolean isSync, String username) throws SearchException
    {
        QueryBuilder query = QueryBuilders.termsQuery("sysId", ids);
        return deleteByQueryBuilder(query, isSync, username);
    }

    public void deleteByQueryBuilder(final QueryBuilder query, final String username) throws SearchException
    {
        deleteByQueryBuilder(query, false, username);
    }

    public Collection<String> deleteByQueryBuilder(final QueryBuilder query, final boolean isSync, final String username) throws SearchException
    {
        if(isIndexAlias)
        {
            return instance.deleteByQueryBuilder(indexAlias, documentType, query, isSync, username);
        }
        else
        {
            return instance.deleteByQueryBuilder(indexName, documentType, query, isSync, username);
        }
    }

    public long getLastIndexTime(final String timeStampedFieldName) throws SearchException
    {
        if(isIndexAlias)
        {
            return instance.getLastIndexTime(indexAlias, documentType, timeStampedFieldName);
        }
        else
        {
            return instance.getLastIndexTime(indexName, documentType, timeStampedFieldName);
        }
    }
    
    public void moveData(final String sourceIndex, final String targetIndex, final String documentType, final int batchSize, final String username) throws SearchException
    {
        instance.moveData(sourceIndex, targetIndex, documentType, batchSize, username);
    }
    
    public void bulkDeleteByIds(Map<Pair<String, String>, List<String>> bulkReq, boolean fromDR) throws SearchException {
    	instance.bulkDeleteByIds(bulkReq, fromDR);
    }
}
