/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;
import java.util.TreeSet;

@StripHTML
public class WikiAttachment extends BaseIndexModel implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String wikiDocumentId;
    private String filename;
    //Base64.encodeBytes("testing text".getBytes("UTF8")))
    private String content;
    private int size;
    
    private TreeSet<String> readRoles = new TreeSet<String>(); // TreeSet to keep in order

    public WikiAttachment()
    {
        super();
    }

    public WikiAttachment(String sysId, String wikiDocumentId, String filename, String content, TreeSet<String> readRoles)
    {
        this();
        setSysId(sysId);
        this.wikiDocumentId = wikiDocumentId;
        this.filename = filename;
        this.content = content;
        //this.content = org.elasticsearch.common.Base64.encodeBytes(content.getBytes());

        if(readRoles != null)
        {
            readRoles.addAll(readRoles);
        }
    }

    public String getWikiDocumentId()
    {
        return wikiDocumentId;
    }

    public void setWikiDocumentId(String wikiDocumentId)
    {
        this.wikiDocumentId = wikiDocumentId;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public TreeSet<String> getReadRoles()
    {
        return readRoles;
    }

    public void setReadRoles(TreeSet<String> readRoles)
    {
        this.readRoles = readRoles;
    }
}
