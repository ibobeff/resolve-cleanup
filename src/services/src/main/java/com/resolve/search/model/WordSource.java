/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class WordSource implements Serializable
{
    private static final long serialVersionUID = -1153277037243899319L;
	//document, automation, social, dictionary
    private String source;
    private int frequency;
    private int selectCount;
    //in days
    private int updatedOn;
    
    public WordSource()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public WordSource(String source)
    {
        this();
        this.source = source;
    }

    public WordSource(String source, int frequency, int selectCount, int updatedOn)
    {
        this();
        this.source = source;
        this.frequency = frequency;
        this.selectCount = selectCount;
        this.updatedOn = updatedOn;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public int getFrequency()
    {
        return frequency;
    }

    public void setFrequency(int frequency)
    {
        this.frequency = frequency;
    }

    public void addFrequency(int frequency)
    {
        this.frequency += frequency;
    }

    public void substractFrequency(int frequency)
    {
        this.frequency -= frequency;
    }

    public int getSelectCount()
    {
        return selectCount;
    }

    public void setSelectCount(int selectCount)
    {
        this.selectCount = selectCount;
    }

    public void addSelectCount(int selectCount)
    {
        this.selectCount += selectCount;
    }

    public void subtractSelectCount(int selectCount)
    {
        this.selectCount -= selectCount;
    }

    public int getUpdatedOn()
    {
        return updatedOn;
    }

    public void setUpdatedOn(int updatedOn)
    {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        WordSource other = (WordSource) obj;
        if (source == null)
        {
            if (other.source != null) return false;
        }
        else if (!source.equals(other.source)) return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WordSource [source=" + source + ", frequency=" + frequency + ", selectCount=" + selectCount + ", updatedOn=" + updatedOn + "]";
    }
}
