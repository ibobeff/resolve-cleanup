/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;



/**
 * Interface that is implemented by content indexers.
 *
 * WikiIndexer is one of the implementer.
 */
public interface Indexer<T>
{

    /**
     * Caller simply wants to drop the content and move on.
     *
     * @param objects could be sysId of some documents of the document itself or other object
     * that the index method understands.
     * @return True if the operation succeeds, false otherwise
     */
    boolean enqueue(T objects);

    /**
     * Actual indexing happen here.
     *
     * @param objects could be sysId of some documents of the document itself.
     * @return True if the operation succeeds, false otherwise
     * @throws SearchException
     */
    boolean index(T objects) throws SearchException;
}
