/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class TaskResult extends BaseIndexModel
{
    private static final long serialVersionUID = -1404982376384798693L;
	private String address;
    private String completion;
    private String condition;
    private Integer duration;
    private String esbaddr;
    private String severity;
    private Long timestamp;
    private String executeRequestId;
    private String executeRequestNumber;
    private String executeResultId;
    private String executeResultCommand;
    private Integer executeResultReturncode;
    private String problemId;
    private String problemNumber;
    private String processId;
    private String processNumber;
    private String targetGUID;
    private String taskId;
    private String taskName;
    private String taskFullName;
    private String taskNamespace;
    private String taskSummary;
    private TreeSet<String> taskRoles = new TreeSet<String>();
    private TreeSet<String> taskTags = new TreeSet<String>();

    private String detail;
    private String summary;
    private String raw;
    private String wiki;
    private String nodeId;
    private Boolean isHidden = false;
    private String activityId;
    private Boolean modified;
    
    public TaskResult()
    {
        super();
        // blank constructor, required for JSON serialization.
    }

    public TaskResult(String sysId, String sysOrg, String username)
    {
        super(sysId, sysOrg, username);
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getCompletion()
    {
        return completion;
    }

    public void setCompletion(String completion)
    {
        this.completion = completion;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public Integer getDuration()
    {
        return duration;
    }

    public void setDuration(Integer duration)
    {
        this.duration = duration;
    }

    public String getEsbaddr()
    {
        return esbaddr;
    }

    public void setEsbaddr(String esbaddr)
    {
        this.esbaddr = esbaddr;
    }

    public String getSeverity()
    {
        return severity;
    }

    public void setSeverity(String severity)
    {
        this.severity = severity;
    }

    public Long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Long timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getExecuteRequestId()
    {
        return executeRequestId;
    }

    public void setExecuteRequestId(String executeRequestId)
    {
        this.executeRequestId = executeRequestId;
    }

    public String getExecuteRequestNumber()
    {
        return executeRequestNumber;
    }

    public void setExecuteRequestNumber(String executeRequestNumber)
    {
        this.executeRequestNumber = executeRequestNumber;
    }

    public String getExecuteResultId()
    {
        return executeResultId;
    }

    public void setExecuteResultId(String executeResultId)
    {
        this.executeResultId = executeResultId;
    }

    public String getExecuteResultCommand()
    {
        return executeResultCommand;
    }

    public void setExecuteResultCommand(String executeResultCommand)
    {
        this.executeResultCommand = executeResultCommand;
    }

    public Integer getExecuteResultReturncode()
    {
        return executeResultReturncode;
    }

    public void setExecuteResultReturncode(Integer executeResultReturncode)
    {
        this.executeResultReturncode = executeResultReturncode;
    }

    public String getProblemId()
    {
        return problemId;
    }

    public void setProblemId(String problemId)
    {
        this.problemId = problemId;
    }

    public String getProblemNumber()
    {
        return problemNumber;
    }

    public void setProblemNumber(String problemNumber)
    {
        this.problemNumber = problemNumber;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setProcessId(String processId)
    {
        this.processId = processId;
    }

    public String getProcessNumber()
    {
        return processNumber;
    }

    public void setProcessNumber(String processNumber)
    {
        this.processNumber = processNumber;
    }

    public String getTargetGUID()
    {
        return targetGUID;
    }

    public void setTargetGUID(String target)
    {
        this.targetGUID = target;
    }

    public String getTaskId()
    {
        return taskId;
    }

    public void setTaskId(String actionTask)
    {
        this.taskId = actionTask;
    }

    public String getTaskName()
    {
        return taskName;
    }

    public void setTaskName(String taskName)
    {
        this.taskName = taskName;
    }

    public String getTaskFullName()
    {
        return taskFullName;
    }

    public void setTaskFullName(String taskFullName)
    {
        //actually never used. clean up later
        this.taskFullName = taskFullName;
    }

    public String getTaskNamespace()
    {
        return taskNamespace;
    }

    public void setTaskNamespace(String taskNamespace)
    {
        this.taskNamespace = taskNamespace;
    }

    public String getTaskSummary()
    {
        return taskSummary;
    }

    public void setTaskSummary(String taskSummary)
    {
        this.taskSummary = taskSummary;
    }
    
    public TreeSet<String> getTaskRoles()
    {
        return taskRoles;
    }

    public void setTaskRoles(TreeSet<String> taskRoles)
    {
        this.taskRoles = taskRoles;
    }

    public TreeSet<String> getTaskTags()
    {
        return taskTags;
    }

    public void setTaskTags(TreeSet<String> taskTags)
    {
        this.taskTags = taskTags;
    }

    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getRaw()
    {
        return raw;
    }

    public void setRaw(String raw)
    {
        this.raw = raw;
    }

    public String getWiki()
    {
        return wiki;
    }

    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }

    public String getNodeId()
    {
        return nodeId;
    }

    public void setNodeId(String nodeId)
    {
        this.nodeId = nodeId;
    }

    public Boolean isHidden()
    {
        return isHidden;
    }

    public void setHidden(Boolean isHidden)
    {
        this.isHidden = isHidden;
    }

    public String getActivityId()
	{
		return activityId;
	}
	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}
	
	public Boolean getModified()
    {
        return modified;
    }
    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }

	public Map<String, Object> asMap()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        result.putAll(super.asMap());

        result.put("u_address", getAddress());
        result.put("u_completion", getCompletion());
        result.put("u_condition", getCondition());
        result.put("u_duration", getDuration());
        result.put("u_esbaddr", getEsbaddr());
        result.put("u_severity", getSeverity());
        result.put("u_timestamp", getTimestamp());
        result.put("u_execute_request_id", getExecuteRequestId());
        result.put("u_execute_result_id", getExecuteResultId());
        result.put("u_problem", getProblemId());
        result.put("u_problem_num", getProblemId());
        result.put("u_process", getProcessId());
        result.put("u_process_num", getProcessNumber());
        result.put("u_target_guid", getTargetGUID());
        result.put("u_actiontask", getTaskId());
        result.put("u_actiontask_name", getTaskName());
        result.put("u_actiontask_full_name", getTaskFullName());
        result.put("u_task_namespace", getTaskNamespace());
        result.put("u_task_summary", getTaskSummary());
        result.put("u_task_hidden", isHidden());
        result.put("u_detail", getDetail());
        result.put("u_summary", getSummary());
        result.put("u_raw", getRaw());
        result.put("u_wiki", getWiki());
        result.put("u_nodeid", getNodeId());
        result.put("activityId", getActivityId());

        return result;
    }
}
