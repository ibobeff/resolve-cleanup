/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.dictionary;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.model.Word;
import com.resolve.search.model.WordSource;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class DictionaryIndexAPI
{
    private static final Indexer<IndexData<Word>> indexer = new DictionaryIndexer(SearchAdminAPI.getSearchConfig(), SearchConstants.USER_QUERY_TYPE_DICTIONARY);

    public static void indexAllWords(String username)
    {
        Log.log.warn("DictionaryIndexAPI.indexAllWords() : indexing all words from the dictionary requested by " + username);

        // this is a dictionary with words we're going to insert
		InputStream stream = DictionaryIndexAPI.class
				.getResourceAsStream("/fulldictionary.txt");
        String dictionaryText = StringUtils.toString(stream, "UTF-8");
        List<String> wordList = StringUtils.readLines(dictionaryText);
        List<Word> words = new ArrayList<Word>();
        WordSource wordSource = new WordSource(SearchConstants.USER_QUERY_TYPE_DICTIONARY, 1, 0, DateUtils.getDaysFromUTCDate().intValue());
        for(String word : wordList)
        {
            Word dictionaryWord = new Word(word);
            dictionaryWord.addSource(wordSource);
            words.add(dictionaryWord);
        }

        IndexData<Word> indexData = new IndexData<Word>(words, OPERATION.INDEX, username);
        indexer.enqueue(indexData);
    }

    public static void purgeAllWords(String username)
    {
        IndexData<Word> indexData = new IndexData<Word>(Collections.EMPTY_LIST, OPERATION.PURGE, username);
        indexer.enqueue(indexData);
    }
}
