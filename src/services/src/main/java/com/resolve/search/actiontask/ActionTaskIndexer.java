/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.actiontask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.RateIndexer;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.Server;
import com.resolve.search.model.ActionTask;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class ActionTaskIndexer implements Indexer<IndexData<String>>
{
    private static volatile ActionTaskIndexer instance = null;
    private final IndexAPI indexAPI;
    private final RateIndexer<IndexData<String>> queryIndexer;

    private final ConfigSearch config;
    private long indexThreads;

    private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

    public static ActionTaskIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("ActionTaskIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static ActionTaskIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new ActionTaskIndexer(config);
        }
        return instance;
    }

    private ActionTaskIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexThreads = this.config.getIndexthreads();
        this.indexAPI = APIFactory.getActionTaskIndexAPI();

        queryIndexer = APIFactory.getAutomationQueryIndexer();
    }

    public void init()
    {
        Log.log.debug("Initializing ActionTaskIndexer.");
        QueueListener<IndexData<String>> actionTaskIndexListener = new IndexListener<IndexData<String>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(actionTaskIndexListener);
        Log.log.debug("ActionTaskIndexer initialized.");
    }

    public boolean enqueue(IndexData<String> indexData)
    {
        return dataQueue.offer(indexData);
    }

    @Override
    public boolean index(IndexData<String> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> sysIds = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexActionTasks(sysIds, indexData.getUsername());
                    break;
                case DELETE:
                    setDeleteActionTasks(sysIds, true, indexData.getUsername());
                    break;
                case PURGE:
                    purgeActionTasks(sysIds, indexData.getUsername());
                    break;
                case MIGRATE:
                	updateIndexMapping();
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    private void updateIndexMapping() {
    	SearchAdminAPI.updateIndexMapping(SearchConstants.INDEX_NAME_ACTION_TASK, SearchConstants.DOCUMENT_TYPE_ACTION_TASK, Server.ES_ACTION_TASKS_MAPPING_FILE);
	}

	/**
     * Indexes action tasks.
     *
     * TODO need to improve based on observed performance. BulkProcessor?
     *
     * @param sysIds
     * @throws Exception
     */
    private void indexActionTasks(Collection<String> sysIds, String username) throws Exception
    {
        Long lastIndexTime = indexAPI.getLastIndexTime(SearchConstants.SYS_UPDATED_ON);

        int start = 0;
        int limit = 100; //100 a batch
        
        Collection<ActionTask> actionTasks = new ArrayList<ActionTask>();
        
        for (String sysId : sysIds)
        {
            if(start >= limit)
            {
            	 indexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, actionTasks, false, username);
                 for(ActionTask task : actionTasks)
                 {
                     Collection<String> queries = Arrays.asList(task.getSummary(), task.getInvocationContent());
                     queryIndexer.indexQueries(task.getSysId(), queries, task.getSysUpdatedOn(), false, username);
                 }
                 actionTasks = new ArrayList<ActionTask>();
                 start = 0;
                 Thread.sleep(500L);
            }
            
            ResolveActionTaskVO actionTaskVO = ServiceHibernate.getActionTaskFromIdWithReferences(sysId, null, username);
            ActionTask actionTask = new ActionTask(actionTaskVO);
            actionTasks.add(actionTask);
            start++;
        }
        
        if(actionTasks.size() > 0) {
        	indexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, actionTasks, false, username);
            for(ActionTask task : actionTasks)
            {
                Collection<String> queries = Arrays.asList(task.getSummary(), task.getInvocationContent());
                queryIndexer.indexQueries(task.getSysId(), queries, task.getSysUpdatedOn(), false, username);
            }
        }
        
        //once all the action tasks are indexed, update the search queries with their ratings
        IndexData<String> indexData = new IndexData<String>(Arrays.asList(lastIndexTime.toString()), OPERATION.RATE, username);
        queryIndexer.enqueue(indexData);
    }

    private void setDeleteActionTasks(final Collection<String> sysIds, final boolean isDeleted, final String username) throws SearchException
    {
        if (sysIds == null || sysIds.isEmpty())
        {
            throw new SearchException("The sysIds list is null/empty, check the calling code why is it like that.");
        }
        else
        {
            // wikis are not really deleted but marked as deleted.
            for (String sysId : sysIds)
            {
                ActionTask actionTask = ActionTaskSearchAPI.findActionTaskById(sysId, username);
                if (actionTask != null)
                {
                    actionTask.setDeleted(isDeleted);
                    indexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, actionTask, false, username);
                }
            }
        }
    }
    
    private void purgeActionTasks(final Collection<String> sysIds, final String username) throws SearchException
    {
        if(sysIds == null)
        {
            //purging all action tasks
            Log.log.info("Purging all Action Tasks requested by \"" + username + "\"");
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            indexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
        else
        {
            indexAPI.deleteDocumentByIds(sysIds, username);
        }
    }
}