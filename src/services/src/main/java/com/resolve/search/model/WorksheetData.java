/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;

/**
 * This class is for WSDATA. The special thing about this is the sysId of this object is
 * a actually worksheetId+propertyKey. This mechanism makes it very efficient in ES to handle
 * various updates that will happen with a particular propertyKey.
 * 
 */
public class WorksheetData extends BaseIndexModel implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String worksheetId;
    private String propertyName;
    private byte[] propertyValue;

    public WorksheetData()
    {
        super();
    }

    public WorksheetData(String worksheetId, String propertyName, byte[] propertyValue, String createdBy)
    {
        //this mechanism of sysId makes the WSDATA mgmt quite efficient
        super(worksheetId+propertyName, null, createdBy);
        this.worksheetId = worksheetId;
        this.propertyName = propertyName;
        this.propertyValue = propertyValue;
    }

    public String getWorksheetId()
    {
        return worksheetId;
    }

    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }

    public String getPropertyName()
    {
        return propertyName;
    }

    public void setPropertyName(String propertyName)
    {
        this.propertyName = propertyName;
    }

    public byte[] getPropertyValue()
    {
        return propertyValue;
    }

    public void setPropertyValue(byte[] propertyValue)
    {
        this.propertyValue = propertyValue;
    }

}
