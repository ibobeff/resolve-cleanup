/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.actiontask;

import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ActionTask;
import com.resolve.search.model.UserInfo;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ActionTaskSearchAPI
{
    private static final SearchAPI<ActionTask> searchAPI = APIFactory.getActionTaskSearchAPI();
    
    //the fieldName^int (summary^3) provides the boosting score to the records so they comes up on top 
    private static final String[] searchFields = new String[] {"name", "fullName^3", "summary^2", "namespace", "type", "description", "invocationContent"};
    private static final String[] highlightedFields = new String[] {"fullName", "summary"};

    public static ActionTask findActionTaskById(String sysId, String username) throws SearchException
    {
        return searchAPI.findDocumentById(sysId, username);
    }

    public static ResponseDTO<ActionTask> searchActionTasks(QueryDTO queryDTO, UserInfo userInfo)
    {
        ResponseDTO<ActionTask> result = new ResponseDTO<ActionTask>();

        try
        {
            QueryBuilder queryBuilder = null;
            QueryBuilder filterBuilder1 = QueryBuilders.termsQuery("readRoles", userInfo.getRoles());

            if(StringUtils.isNotBlank(queryDTO.getSearchTerm()))
            {
                MultiMatchQueryBuilder multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), searchFields);
                multiMatchQueryBuilder.fuzziness(".5");
                multiMatchQueryBuilder.maxExpansions(12);

                queryBuilder = QueryBuilders.functionScoreQuery(multiMatchQueryBuilder).boostMode(CombineFunction.REPLACE);

                result = searchAPI.searchByTerm(SearchConstants.USER_QUERY_TYPE_AUTOMATION, queryDTO, queryBuilder, new QueryBuilder[] {filterBuilder1}, highlightedFields, userInfo);
            }
            else
            {
                queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
                result = searchAPI.searchByQuery(queryDTO, queryBuilder, null, new QueryBuilder[] {filterBuilder1}, highlightedFields, userInfo.getUsername());
            }

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage(e.getMessage());
        }

        return result;
    }
}
