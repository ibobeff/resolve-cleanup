/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;


@SuppressWarnings("serial")
public class MetricTimer extends BaseIndexModel
{
    private static final long serialVersionUID = -2069932800023940930L;
	private String src;
    private String id1;
    private String id2;
    private Long value;

    public MetricTimer()
    {
        super();
    }

    public MetricTimer(String username)
    {
        super(null, null, username);
    }

    public String getSrc()
    {
        return src;
    }

    public void setSrc(String src)
    {
        this.src = src;
    }

    public String getId1()
    {
        return id1;
    }

    public void setId1(String id1)
    {
        this.id1 = id1;
    }

    public String getId2()
    {
        return id2;
    }

    public void setId2(String id2)
    {
        this.id2 = id2;
    }

    public Long getValue()
    {
        return value;
    }

    public void setValue(Long value)
    {
        this.value = value;
    }
    
    @Override
    public String toString()
    {
        return "MetricTimer [src=" + src + ", id1=" + id1 + ", id2=" + id2 + ", value=" + value + "]";
    }
}
