/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.query;

import java.util.Collections;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.QueryAPI;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.UserInfo;
import com.resolve.search.model.query.GlobalQuery;
import com.resolve.search.model.query.UserQuery;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class UserQueryIndexAPI
{
    private static final IndexAPI globalQueryIndexAPI = APIFactory.getGlobalQueryIndexAPI();
    private static final SearchAPI<GlobalQuery> globalQuerySearchAPI = APIFactory.getGlobalQuerySearchAPI();
    private static final IndexAPI userQueryIndexAPI = APIFactory.getUserQueryIndexAPI();
    private static final SearchAPI<UserQuery> userQuerySearchAPI = APIFactory.getUserQuerySearchAPI();
    
    /**
     * 
     * @param query
     * @param type
     * @param totalCount
     * @param userInfo
     * @throws SearchException
     */
    public static void indexUserQuery(String query, String type, int totalCount, final UserInfo userInfo) throws SearchException
    {
        Log.log.debug("indexing phrases entered by user: " + query);
        if(StringUtils.isNotBlank(query))
        {
            //the formula is that the recent query must be on top
            long currentTime = DateUtils.GetUTCDateLong();
            
            //just for weightage purpose, keeping it as a small number
            long oldInMonth = currentTime - SearchConstants.INCEPTION_DAY;
            oldInMonth = ((((oldInMonth / 1000) / 60) / 60) / 24) / 30;

            //let's deal with the global query first
            GlobalQuery globalQuery = QueryAPI.findGlobalQuery(query, userInfo.getUsername());
            if(globalQuery == null)
            {
                globalQuery = SearchUtils.prepareGlobalQuery(query, totalCount, userInfo);
                globalQuery.setSelectCount(1);
                globalQueryIndexAPI.indexDocument(globalQuery, false, userInfo.getUsername());
            }
            else
            {
                globalQuery.setWeight((totalCount * (int) oldInMonth));
                globalQuery.setSelectCount(globalQuery.getSelectCount() + 1);
                globalQuery.setSysUpdatedOn(currentTime);
                globalQueryIndexAPI.indexDocument(globalQuery, userInfo.getUsername());
            }
            
            UserQuery userQuery = UserQuerySearchAPI.findByQueryAndUsername(query, type, userInfo.getUsername());
            if(userQuery == null)
            {
                userQuery = SearchUtils.prepareUserQuery(query, type, totalCount, userInfo);
                userQuery.setSelectCount(1);
                userQueryIndexAPI.indexDocument(userQuery, false, userInfo.getUsername());
            }
            else
            {
                userQuery.setWeight((totalCount * (int) oldInMonth));
                userQuery.setSelectCount(userQuery.getSelectCount() + 1);
                userQuery.setSysUpdatedOn(currentTime);
                userQueryIndexAPI.indexDocument(userQuery, userInfo.getUsername());
            }
        }
    }
    
    public static void updateAllUserQueryRating(String username)
    {
        Log.log.debug("setting rating for user search queries");

        //IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.RATE, username);
        //userQueryIndexer.enqueue(indexData);
    }
    
    public static void purgeAllUserQueries(String username)
    {
        Log.log.info("Purging all user queries requested by " + username);
        IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.PURGE, username);
        //userQueryIndexer.enqueue(indexData);
    }
}
