/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.List;
import java.util.TreeSet;

import com.resolve.search.SearchConstants;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@StripHTML
public class ActionTask extends BaseIndexModel {
	private static final long serialVersionUID = 1L;

	private String name;
	private String fullName;
	private String namespace;
	private String menupath;
	private String summary;
	@StripHTML(value = false)
	private String rawSummary;
	private String description;

	private Double rating;
	private Long clickCount;

	private boolean isDeleted = false;
	private boolean isActive = false;
	private boolean isHidden = false;
	// helps with automation query
	private boolean isAutomation = true;

	private String invocationContent;

	private TreeSet<String> readRoles = new TreeSet<String>();
	private TreeSet<String> tags = new TreeSet<String>();

	private AccessRights accessRights;
	
	public ActionTask() {
		super();
		// blank constructor, required for JSON serialization.
	}

	public ActionTask(String sysId, String sysOrg, String name, String namespace, String title, String summary,
			String description, TreeSet<String> readRoles) {
		super(sysId, sysOrg);
		this.name = name;
		this.namespace = namespace;
		this.fullName = namespace + "." + name;
		this.summary = summary;
		this.rawSummary = summary;
		this.description = description;
		if (readRoles != null) {
			this.readRoles.addAll(readRoles);
		}
	}

	/**
	 * This constroctor creates a ActionTask from the ResolveActionTaskVO.
	 *
	 * @param actionTaskVO
	 */
	public ActionTask(ResolveActionTaskVO actionTaskVO) {
		setSysId(actionTaskVO.getSys_id());
		setSysOrg(actionTaskVO.getSysOrg());
		setSysCreatedBy(actionTaskVO.getSysCreatedBy());
		if (actionTaskVO.getSysCreatedOn() == null) {
			// to bad using current UTC time
			setSysCreatedOn(DateUtils.GetUTCDateLong());
		} else {
			setSysCreatedOn(actionTaskVO.getSysCreatedOn().getTime());
		}
		setSysUpdatedBy(actionTaskVO.getSysUpdatedBy());
		if (actionTaskVO.getSysUpdatedOn() == null) {
			// to bad using current UTC time
			setSysUpdatedOn(getSysCreatedOn());
		} else {
			setSysUpdatedOn(actionTaskVO.getSysUpdatedOn().getTime());
		}
		setName(actionTaskVO.getUName());
		setNamespace(actionTaskVO.getUNamespace());
		setMenupath(actionTaskVO.getUMenuPath());
		setFullName(actionTaskVO.getUFullName());
		setSummary(actionTaskVO.getUSummary());
		setRawSummary(actionTaskVO.getUSummary());
		setDescription(actionTaskVO.getUDescription());
		setDeleted(actionTaskVO.getSysIsDeleted() == null ? false : actionTaskVO.getSysIsDeleted());
		// this is not necessary for search. it is for task result only
		// setHidden(actionTaskVO.getUIsHidden() == null ? false :
		// actionTaskVO.getUIsHidden());
		setActive(actionTaskVO.getUActive() == null ? false : actionTaskVO.getUActive());
		if (actionTaskVO.getResolveActionInvoc() != null
				&& actionTaskVO.getResolveActionInvoc().ugetIndexContent() != null) {
			setInvocationContent(actionTaskVO.getResolveActionInvoc().ugetIndexContent());
		}

		if (actionTaskVO.getTags() != null) {
			this.tags.addAll(actionTaskVO.getTags());
		}

		// Roles, wikiDocumentVO makes sure that the roles are all comma
		// separated.
		if (actionTaskVO.getAccessRights() == null) {
			Log.log.error(actionTaskVO.getUFullName() + " action task do not have read access rights, why?");
		} else {
			AccessRightsVO accessRightsVO = actionTaskVO.getAccessRights();
			// it's guranteed by the API that it's a comma separated list
			List<String> readRoleList = StringUtils.convertStringToList(accessRightsVO.getUReadAccess(),
					",");
			for (String readRole : readRoleList) {
				this.readRoles.add(readRole);
			}
			
			AccessRights accessRights = new AccessRights();
			accessRights.setReadAccess(accessRightsVO.getUReadAccess());
			accessRights.setWriteAccess(accessRightsVO.getUWriteAccess());
			accessRights.setAdminAccess(accessRightsVO.getUAdminAccess());
			accessRights.setExecuteAccess(accessRightsVO.getUExecuteAccess());
			setAccessRights(accessRights);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return SearchConstants.SEARCH_TYPE_ACTION_TASK;
	}

	public void setType(String type) {
		// useless
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getMenupath() {
		return menupath;
	}

	public void setMenupath(String menupath) {
		this.menupath = menupath;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getRawSummary() {
		return rawSummary;
	}

	public void setRawSummary(String rawSummary) {
		this.rawSummary = rawSummary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isHidden() {
		return isHidden;
	}

	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}

	public boolean isAutomation() {
		return isAutomation;
	}

	public void setAutomation(boolean isAutomation) {
		this.isAutomation = isAutomation;
	}

	public String getInvocationContent() {
		return invocationContent;
	}

	public void setInvocationContent(String invocationContent) {
		this.invocationContent = invocationContent;
	}

	public TreeSet<String> getReadRoles() {
		return readRoles;
	}

	public void setReadRoles(TreeSet<String> readRoles) {
		this.readRoles = readRoles;
	}

	public TreeSet<String> getTags() {
		return tags;
	}

	public void setTags(TreeSet<String> tags) {
		this.tags = tags;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public void addRating(Double rating) {
		if (this.rating == null) {
			this.rating = rating;
		} else {
			this.rating += rating;
		}
	}

	public Long getClickCount() {
		return clickCount;
	}

	public void setClickCount(Long clickCount) {
		this.clickCount = clickCount;
	}

	public void addClickCount() {
		if (this.clickCount == null) {
			this.clickCount = 1L;
		} else {
			this.clickCount += 1L;
		}
	}

	public AccessRights getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(AccessRights accessRights) {
		this.accessRights = accessRights;
	}
}