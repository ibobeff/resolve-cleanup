/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.TreeSet;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.resolve.search.SearchConstants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class represents a social post that gets stored in ElasticSearch (ES).
 * Although there is reference to SocialComment they don't get stored in ES.
 * SocialComment has its own index for convenience of independently querying and
 * managing.
 *
 */
public class SocialPost extends BaseIndexModel
{
    private static final long serialVersionUID = 1L;

    private String title;
    private String content;
    //type is like "system", "rss", "post"
    private String type = "post";
    private String uri; //an identifier (e.g., RSS or other identifier for future.
    private String authorSysId; //user's sys id from the database
    private String authorUsername; //user's login name from the database
    private String authorDisplayName; //user's display name from the database

    //these field are included in the indexed document itself
    //for efficient querying of some statictical data (e.g., UserStats)
    private long numberOfLikes;
    private long numberOfReads;
    private long numberOfStarred;
    private long numberOfComments;

    private Double rating;
    private Long clickCount;

    private boolean isLocked = false;
    private boolean isDeleted = false;

    private TreeSet<String> readUsernames = new TreeSet<String>();
    private TreeSet<String> likedUsernames = new TreeSet<String>();
    private TreeSet<String> starredUsernames = new TreeSet<String>();

    private TreeSet<SocialTarget> targets = new TreeSet<SocialTarget>();
    private TreeSet<String> readRoles = new TreeSet<String>(); // TreeSet to keep in order
    private TreeSet<String> postRoles = new TreeSet<String>(); // TreeSet to keep in order
    private TreeSet<String> editRoles = new TreeSet<String>(); // TreeSet to keep in order

    private TreeSet<String> mentions = new TreeSet<String>();;
    private TreeSet<String> tags = new TreeSet<String>();;

    public SocialPost()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public SocialPost(String sysId, String sysOrg, TreeSet<SocialTarget> targets, String title, String content, String type, String uri, String authorSysId, String authorUsername)
    {
        super(sysId, sysOrg);
        this.title = title;
        this.content = content;
        this.type = type;
        this.uri = uri;
        this.authorSysId = authorSysId;
        this.authorUsername = authorUsername;
        this.targets = targets;

        if(targets != null)
        {
            //roles are nothing but aggregation of all the roles in each target.
            for(SocialTarget target : targets)
            {
                readRoles.addAll(target.getReadRoles());
                postRoles.addAll(target.getReadRoles());
                editRoles.addAll(target.getReadRoles());
            }
        }
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public String getAuthorSysId()
    {
        return authorSysId;
    }

    public void setAuthorSysId(String authorSysId)
    {
        this.authorSysId = authorSysId;
    }

    public String getAuthorUsername()
    {
        return authorUsername;
    }

    public void setAuthorUsername(String author)
    {
        this.authorUsername = author;
    }

    public String getAuthorDisplayName()
    {
        return authorDisplayName;
    }

    public void setAuthorDisplayName(String authorDisplayName)
    {
        this.authorDisplayName = authorDisplayName;
    }

    public boolean isLocked()
    {
        return isLocked;
    }

    public void setLocked(boolean isLocked)
    {
        this.isLocked = isLocked;
    }

    public boolean isDeleted()
    {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public Double getRating()
    {
        return (rating == null ? 0.0 : rating);
    }

    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    public void addRating(Double rating)
    {
        if(this.rating == null)
        {
            this.rating = rating;
        }
        else
        {
            this.rating += rating;
        }
    }

    public Long getClickCount()
    {
        return (clickCount == null ? 0 : clickCount);
    }

    public void setClickCount(Long clickCount)
    {
        this.clickCount = clickCount;
    }

    public void addClickCount()
    {
        if(this.clickCount == null)
        {
            this.clickCount = 1L;
        }
        else
        {
            this.clickCount += 1L;
        }
    }

    public TreeSet<SocialTarget> getTargets()
    {
        return targets;
    }

    public void setTargets(TreeSet<SocialTarget> targets)
    {
        this.targets = targets;
        if(targets != null)
        {
            //roles are nothing but aggregation of all the roles in each target.
            for(SocialTarget target : targets)
            {
                if(target == null)
                {
                    Log.log.debug("Why the target is null?");
                }
                else
                {
                    if(target.getReadRoles() != null)
                    {
                        readRoles.addAll(target.getReadRoles());
                    }
                    if(target.getPostRoles() != null)
                    {
                        postRoles.addAll(target.getPostRoles());
                    }
                    if(target.getEditRoles() != null)
                    {
                        editRoles.addAll(target.getEditRoles());
                    }
                }
            }
        }
    }

    public TreeSet<String> getReadUsernames()
    {
        return readUsernames;
    }

    public void setReadUsernames(TreeSet<String> readUsernames)
    {
        this.readUsernames = readUsernames;
    }

    public void addReadUsername(String username)
    {
        this.readUsernames.add(username);
    }

    public void removeReadUsername(String username)
    {
        this.readUsernames.remove(username);
    }

    public TreeSet<String> getLikedUsernames()
    {
        return likedUsernames;
    }

    public void setLikedUsernames(TreeSet<String> likedUsernames)
    {
        this.likedUsernames = likedUsernames;
    }

    public void addLikedUsername(String username)
    {
        this.likedUsernames.add(username);
    }

    public void removeLikedUsername(String username)
    {
        this.likedUsernames.remove(username);
    }

    public TreeSet<String> getStarredUsernames()
    {
        return starredUsernames;
    }

    public void setStarredUsernames(TreeSet<String> starredUsernames)
    {
        this.starredUsernames = starredUsernames;
    }

    public void addStarredUsername(String username)
    {
        this.starredUsernames.add(username);
    }

    public void removeStarredUsername(String username)
    {
        this.starredUsernames.remove(username);
    }

    public long getNumberOfLikes()
    {
        return numberOfLikes;
    }

    public void setNumberOfLikes(long numberOfLikes)
    {
        this.numberOfLikes = numberOfLikes;
    }

    public long getNumberOfReads()
    {
        return numberOfReads;
    }

    public void setNumberOfReads(long numberOfReads)
    {
        this.numberOfReads = numberOfReads;
    }

    public long getNumberOfStarred()
    {
        return numberOfStarred;
    }

    public void setNumberOfStarred(long numberOfStarred)
    {
        this.numberOfStarred = numberOfStarred;
    }

    public long getNumberOfComments()
    {
        return numberOfComments;
    }

    public void setNumberOfComments(long numberOfComments)
    {
        this.numberOfComments = numberOfComments;
    }

    public TreeSet<String> getMentions()
    {
        return mentions;
    }

    public void setMentions(TreeSet<String> mentions)
    {
        this.mentions = mentions;
    }

    public void addMention(String mention)
    {
        if (StringUtils.isNotBlank(mention))
        {
            this.mentions.add(mention);
        }
    }

    public TreeSet<String> getTags()
    {
        return tags;
    }

    public void setTags(TreeSet<String> tags)
    {
        this.tags = tags;
    }

    public TreeSet<String> getReadRoles()
    {
        return readRoles;
    }

    public void setReadRoles(TreeSet<String> readRoles)
    {
        this.readRoles = readRoles;
    }

    public void addReadRole(String role)
    {
        if (StringUtils.isNotBlank(role))
        {
            this.readRoles.add(role);
        }
    }

    public TreeSet<String> getPostRoles()
    {
        return postRoles;
    }

    public void setPostRoles(TreeSet<String> postRoles)
    {
        this.postRoles = postRoles;
    }

    public TreeSet<String> getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(TreeSet<String> editRoles)
    {
        this.editRoles = editRoles;
    }

    @JsonIgnore
    public boolean isInbox(String userSysId)
    {
        boolean result = false;
        
        if(StringUtils.isNotBlank(userSysId))
        {
            if(!userSysId.equals(getAuthorSysId()))
            {
                for(SocialTarget target : getTargets())
                {
                    if(userSysId.equals(target.getSysId()))
                    {
                        result = true;
                        break;
                    }
                }
            }
        }

        return result;
    }

    @JsonIgnore
    public boolean isSystemMessage()
    {
        return SearchConstants.SOCIALPOST_TYPE_SYSTEM.equalsIgnoreCase(getType()) 
                        || SearchConstants.SOCIALPOST_TYPE_MANUAL.equalsIgnoreCase(getType());
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getSysId() == null) ? 0 : getSysId().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        SocialPost other = (SocialPost) obj;
        if (getSysId() == null)
        {
            if (other.getSysId() != null) return false;
        }
        else if (!getSysId().equals(other.getSysId())) return false;
        return true;
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();

        toString.append("SocialPost [" + super.toString() + ", title=" + title + ", content=" + content + ", authorUsername=" + authorUsername + ", isLocked=" + isLocked + ", isDeleted=" + isDeleted + "]\n");

        if(getTargets() != null)
        {
            for(SocialTarget target : getTargets())
            {
                toString.append(toString.append(target.toString()));
            }
        }
        toString.append("\n");
        if (getReadRoles() != null)
        {
            toString.append("  --> Roles: ");
            for (String role : getReadRoles())
            {
                toString.append(role.toString() + ",");
            }
        }
        toString.append("\n");
        toString.append("  --> Read By:");
        for (String username : getReadUsernames())
        {
            toString.append(username + ",");
        }
        toString.append("\n");
        toString.append("  --> Liked By:");
        for (String username : getLikedUsernames())
        {
            toString.append(username + ",");
        }
        toString.append("\n");
        toString.append("  --> Starred By:");
        for (String username : getStarredUsernames())
        {
            toString.append(username + ",");
        }
        return toString.toString();
    }
}
