package com.resolve.search.model;

import java.io.Serializable;

public class MetricData implements Serializable
{
    private static final long serialVersionUID = 2748177842798164451L;
	private String id;
    private String src;
    private long ts;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getSrc()
    {
        return src;
    }
    
    public void setSrc(String s)
    {
        src = s;
    }
    
    public long getTs()
    {
        return ts;
    }
    
    public void setTs(long t)
    {
        ts = t;
    }
}
