/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.util.Collection;
import java.util.LinkedList;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class IndexData<T>
{
    public static enum OPERATION {INDEX, DELETE, PURGE, RATE, MIGRATE, CUSTOM};

    //may be wiki doc id for attachment, post id for a comment etc.
    private String parentId;

    private final Collection<T> objects;
    private final OPERATION operation;
    private boolean bulkProcess;
    private String username;
    private String sourceId;
    private Long sysUpdatedOn;
    
    public IndexData(T object, OPERATION operation, String username)
    {
        Collection<T> newObjects = new LinkedList<T>();
        newObjects.add(object);
        this.objects = newObjects;
        this.operation = operation;
        
        if (StringUtils.isBlank(username))
        {
            Log.log.warn("IndexData constructor was called with blank username.");
            this.username = "system";
        }
        else
        {
            this.username = username;
        }
    }

    public IndexData(Collection<T> objects, OPERATION operation, String username)
    {
        this.objects = objects;
        this.operation = operation;

        if (StringUtils.isBlank(username))
        {
            Log.log.warn("IndexData constructor was called with blank username.");
            this.username = "system";
        }
        else
        {
            this.username = username;
        }
    }

    public IndexData(Collection<T> objects, OPERATION operation, boolean bulkProcess, String username)
    {
        this.objects = objects;
        this.operation = operation;
        this.bulkProcess = bulkProcess;

        if (StringUtils.isBlank(username))
        {
            Log.log.warn("IndexData constructor was called with blank username.");
            this.username = "system";
        }
        else
        {
            this.username = username;
        }
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public Collection<T> getObjects()
    {
        return objects;
    }

    public OPERATION getOperation()
    {
        return operation;
    }

    public boolean isBulkProcess()
    {
        return bulkProcess;
    }

    public void setBulkProcess(boolean bulkProcess)
    {
        this.bulkProcess = bulkProcess;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getSourceId()
    {
        return sourceId;
    }

    public void setSourceId(String sourceId)
    {
        this.sourceId = sourceId;
    }

    public Long getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Long sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }
    
    @Override
    public String toString()
    {
        return "Objects : [" + StringUtils.collectionOfObjectsToString((Collection<? extends Object>) objects, ", ") + "], OPERATION : " + operation + ", username : " + username;
    }
}
