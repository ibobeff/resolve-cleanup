/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.util.DateUtils;

@StripHTML
public class Property extends BaseIndexModel {

	private static final long serialVersionUID = 2344863994538404115L;

	private String name;

	private String value;

	private String namespace;

	private String type;

	private boolean deleted;

	private AccessRights accessRights;

	public Property() {
	}

	public Property(ResolvePropertiesVO resolvePropertiesVO) {

		setSysId(resolvePropertiesVO.getSys_id());
		setSysOrg(resolvePropertiesVO.getSysOrg());
		setSysCreatedBy(resolvePropertiesVO.getSysCreatedBy());
		if (resolvePropertiesVO.getSysCreatedOn() == null) {
			// to bad using current UTC time
			setSysCreatedOn(DateUtils.GetUTCDateLong());
		} else {
			setSysCreatedOn(resolvePropertiesVO.getSysCreatedOn().getTime());
		}
		setSysUpdatedBy(resolvePropertiesVO.getSysUpdatedBy());
		if (resolvePropertiesVO.getSysUpdatedOn() == null) {
			// to bad using current UTC time
			setSysUpdatedOn(getSysCreatedOn());
		} else {
			setSysUpdatedOn(resolvePropertiesVO.getSysUpdatedOn().getTime());
		}

		this.name = resolvePropertiesVO.getUName();
		this.value = resolvePropertiesVO.getUValue();
		this.namespace = resolvePropertiesVO.getUModule();
		this.type = resolvePropertiesVO.getUType();
		this.deleted = resolvePropertiesVO.getSysIsDeleted();

		// When RBAC is implemented, the real access rights must be set here
		this.accessRights = new AccessRights();
		this.accessRights.setReadAccess("all");
		this.accessRights.setWriteAccess("all");
		this.accessRights.setExecuteAccess("all");
		this.accessRights.setAdminAccess("all");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public AccessRights getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(AccessRights accessRights) {
		this.accessRights = accessRights;
	}

}
