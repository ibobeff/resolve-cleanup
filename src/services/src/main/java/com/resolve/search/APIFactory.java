/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import com.resolve.search.model.ActionTask;
import com.resolve.search.model.AutomationSearchResponse;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.MetricTimer;
import com.resolve.search.model.PBActivity;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBAuditLog;
import com.resolve.search.model.PBNotes;
import com.resolve.search.model.SecurityIncident;
import com.resolve.search.model.SyncLog;
import com.resolve.search.model.Tag;
import com.resolve.search.model.WikiAttachment;
import com.resolve.search.model.WikiDocument;
import com.resolve.search.model.Word;
import com.resolve.search.model.WorksheetData;
import com.resolve.search.model.query.AutomationQuery;
import com.resolve.search.model.query.DocumentQuery;
import com.resolve.search.model.query.GlobalQuery;
import com.resolve.search.model.query.Query;
import com.resolve.search.model.query.SocialQuery;
import com.resolve.search.model.query.UserQuery;
import com.resolve.search.model.query.WikiQuery;
import com.resolve.search.query.QueryIndexer;

public class APIFactory {
	public static IndexAPI getActionTaskIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_ACTION_TASK, SearchConstants.DOCUMENT_TYPE_ACTION_TASK);
	}

	public static IndexAPI getPropertyIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_PROPERTY, SearchConstants.DOCUMENT_TYPE_PROPERTY);
	}

	public static IndexAPI getCustomFormIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_CUSTOM_FORM, SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM);
	}

	public static SearchAPI<AutomationSearchResponse> getAutomationSearchAPI() {
		return new SearchAPI<AutomationSearchResponse>(
				new String[] { SearchConstants.INDEX_NAME_ACTION_TASK, SearchConstants.INDEX_NAME_WIKI_DOCUMENT },
				new String[] { SearchConstants.DOCUMENT_TYPE_ACTION_TASK, SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT },
				AutomationSearchResponse.class);
	}

	public static IndexAPI getMetricJVMIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_JVM, SearchConstants.DOCUMENT_TYPE_METRIC_JVM);
	}

	public static IndexAPI getMetricRunbookIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_RUNBOOK, SearchConstants.DOCUMENT_TYPE_METRIC_RUNBOOK);
	}

	public static IndexAPI getMetricTransactionIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_TRANSACTION,
				SearchConstants.DOCUMENT_TYPE_METRIC_TRANSACTION);
	}

	public static IndexAPI getMetricCPUIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_CPU, SearchConstants.DOCUMENT_TYPE_METRIC_CPU);
	}

	public static IndexAPI getMetricDBIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_DB, SearchConstants.DOCUMENT_TYPE_METRIC_DB);
	}

	public static IndexAPI getMetricJMSQueueIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_JMS_QUEUE,
				SearchConstants.DOCUMENT_TYPE_METRIC_JMS_QUEUE);
	}

	public static IndexAPI getMetricUsersIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_USERS, SearchConstants.DOCUMENT_TYPE_METRIC_USERS);
	}

	public static IndexAPI getMetricLatencyIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_LATENCY, SearchConstants.DOCUMENT_TYPE_METRIC_LATENCY);
	}

	public static IndexAPI getMetricActionTaskIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_ACTIONTASK,
				SearchConstants.DOCUMENT_TYPE_METRIC_ACTIONTASK);
	}

	public static IndexAPI getWorksheetIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_WORKSHEET, SearchConstants.DOCUMENT_TYPE_WORKSHEET,
				SearchConstants.INDEX_NAME_WORKSHEET_ALIAS, true);
	}

	public static IndexAPI getExecutionSummaryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY,
				SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY, SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS,
				true);
	}

	public static IndexAPI getProcessRequestIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_PROCESSREQUEST, SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST,
				SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS, true);
	}

	public static IndexAPI getTaskResultIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_TASKRESULT, SearchConstants.DOCUMENT_TYPE_TASKRESULT,
				SearchConstants.INDEX_NAME_TASKRESULT_ALIAS, true);
	}

	public static IndexAPI getExecuteStateIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_EXECUTE_STATE, SearchConstants.DOCUMENT_TYPE_EXECUTE_STATE);
	}

	public static IndexAPI getMetricTimerIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_METRIC_TIMER, SearchConstants.DOCUMENT_TYPE_METRIC_TIMER);
	}

	public static IndexAPI getWorksheetDataIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_WORKSHEET_DATA, SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA);
	}
	
	@Deprecated
	public static IndexAPI getPBActivityIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_PB_ACTIVITY, SearchConstants.DOCUMENT_TYPE_PB_ACTIVITY);
	}

	public static IndexAPI getPBNotesIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_PB_NOTES, SearchConstants.DOCUMENT_TYPE_PB_NOTES);
	}

	public static IndexAPI getPBArtifactsIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_PB_ARTIFACTS, SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS);
	}

	public static IndexAPI getPBAttachmentIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_PB_ATTACHMENT, SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT);
	}

	public static IndexAPI getPBAuditLogIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_PB_AUDIT_LOG, SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG);
	}
	
	@Deprecated
	public static IndexAPI getExecutionSummaryArchiveIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ARCHIVE,
				SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY_ARCHIVE);
	}

	@Deprecated
	public static SearchAPI<PBActivity> getPBActivitySearchAPI() {
		return new SearchAPI<PBActivity>(SearchConstants.INDEX_NAME_PB_ACTIVITY,
				SearchConstants.DOCUMENT_TYPE_PB_ACTIVITY, PBActivity.class);
	}

	public static SearchAPI<PBNotes> getPBNotesSearchAPI() {
		return new SearchAPI<PBNotes>(SearchConstants.INDEX_NAME_PB_NOTES, SearchConstants.DOCUMENT_TYPE_PB_NOTES,
				PBNotes.class);
	}

	public static SearchAPI<PBArtifacts> getPBArtifactsSearchAPI() {
		return new SearchAPI<PBArtifacts>(SearchConstants.INDEX_NAME_PB_ARTIFACTS,
				SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS, PBArtifacts.class);
	}

	public static SearchAPI<PBAttachments> getPBAttachmentSearchAPI() {
		return new SearchAPI<PBAttachments>(SearchConstants.INDEX_NAME_PB_ATTACHMENT,
				SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT, PBAttachments.class);
	}

	public static SearchAPI<PBAuditLog> getPBAuditLogSearchAPI() {
		return new SearchAPI<PBAuditLog>(SearchConstants.INDEX_NAME_PB_AUDIT_LOG,
				SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG, PBAuditLog.class);
	}

	public static <T> SearchAPI<T> getWorksheetSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_WORKSHEET, SearchConstants.DOCUMENT_TYPE_WORKSHEET,
				SearchConstants.INDEX_NAME_WORKSHEET_ALIAS, true, clazz);
	}

	public static <T> SearchAPI<T> getProcessRequestSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_PROCESSREQUEST, SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST,
				SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS, true, clazz);
	}

	public static <T> SearchAPI<T> getTaskResultSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_TASKRESULT, SearchConstants.DOCUMENT_TYPE_TASKRESULT,
				SearchConstants.INDEX_NAME_TASKRESULT_ALIAS, true, clazz);
	}

	public static SearchAPI<ExecuteState> getExecuteStateSearchAPI() {
		return new SearchAPI<ExecuteState>(SearchConstants.INDEX_NAME_EXECUTE_STATE,
				SearchConstants.DOCUMENT_TYPE_EXECUTE_STATE, ExecuteState.class);
	}

	public static SearchAPI<MetricTimer> getMetricTimerSearchAPI() {
		return new SearchAPI<MetricTimer>(SearchConstants.INDEX_NAME_METRIC_TIMER,
				SearchConstants.DOCUMENT_TYPE_METRIC_TIMER, MetricTimer.class);
	}

	public static SearchAPI<WorksheetData> getWorksheetDataSearchAPI() {
		return new SearchAPI<WorksheetData>(SearchConstants.INDEX_NAME_WORKSHEET_DATA,
				SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA, WorksheetData.class);
	}

	public static IndexAPI getTagIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_TAG, SearchConstants.DOCUMENT_TYPE_TAG);
	}

	public static SearchAPI<Tag> getTagSearchAPI() {
		return new SearchAPI<Tag>(SearchConstants.INDEX_NAME_TAG, SearchConstants.DOCUMENT_TYPE_TAG, Tag.class);
	}

	public static IndexAPI getWikiIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_WIKI_DOCUMENT, SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
	}

	public static IndexAPI getWikiAttachmentIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_WIKI_DOCUMENT, SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT);
	}

	public static SearchAPI<WikiDocument> getWikiSearchAPI() {
		return new SearchAPI<WikiDocument>(SearchConstants.INDEX_NAME_WIKI_DOCUMENT,
				SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT, WikiDocument.class);
	}

	public static SearchAPI<WikiAttachment> getWikiAttachmentSearchAPI() {
		return new SearchAPI<WikiAttachment>(SearchConstants.INDEX_NAME_WIKI_DOCUMENT,
				SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, WikiAttachment.class);
	}

	public static IndexAPI getSocialPostIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_POST);
	}

	public static IndexAPI getSocialCommentIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT);
	}

	public static IndexAPI getSystemMessageIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SYSTEM_POST, SearchConstants.DOCUMENT_TYPE_SYSTEM_POST);
	}

	public static <T> SearchAPI<T> getSystemMessageSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_SYSTEM_POST, SearchConstants.DOCUMENT_TYPE_SYSTEM_POST,
				clazz);
	}

	public static IndexAPI getSystemMessageCommentIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SYSTEM_POST, SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT);
	}

	public static <T> SearchAPI<T> getSystemMessageCommentSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_SYSTEM_POST, SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT,
				clazz);
	}

	public static <T> SearchAPI<T> getSocialPostSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_POST,
				clazz);
	}

	public static <T> SearchAPI<T> getSocialCommentSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT,
				clazz);
	}

	public static IndexAPI getDocumentQueryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SEARCH_QUERY, SearchConstants.DOCUMENT_TYPE_DOCUMENT_QUERY);
	}

	public static IndexAPI getSocialQueryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SEARCH_QUERY, SearchConstants.DOCUMENT_TYPE_SOCIAL_QUERY);
	}

	public static IndexAPI getAutomationQueryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SEARCH_QUERY, SearchConstants.DOCUMENT_TYPE_AUTOMATION_QUERY);
	}

	public static SearchAPI<DocumentQuery> getDocumentQuerySearchAPI() {
		return new SearchAPI<DocumentQuery>(SearchConstants.INDEX_NAME_SEARCH_QUERY,
				SearchConstants.DOCUMENT_TYPE_DOCUMENT_QUERY, DocumentQuery.class);
	}

	public static SearchAPI<SocialQuery> getSocialQuerySearchAPI() {
		return new SearchAPI<SocialQuery>(SearchConstants.INDEX_NAME_SEARCH_QUERY,
				SearchConstants.DOCUMENT_TYPE_SOCIAL_QUERY, SocialQuery.class);
	}

	public static SearchAPI<AutomationQuery> getAutomationQuerySearchAPI() {
		return new SearchAPI<AutomationQuery>(SearchConstants.INDEX_NAME_SEARCH_QUERY,
				SearchConstants.DOCUMENT_TYPE_AUTOMATION_QUERY, AutomationQuery.class);
	}

	/**
	 * Used for WIKI name suggestion.
	 * 
	 * @return
	 */
	public static IndexAPI getWikiQueryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_SEARCH_QUERY, SearchConstants.DOCUMENT_TYPE_WIKI_QUERY);
	}

	public static SearchAPI<WikiQuery> getWikiQuerySearchAPI() {
		return new SearchAPI<WikiQuery>(SearchConstants.INDEX_NAME_SEARCH_QUERY,
				SearchConstants.DOCUMENT_TYPE_WIKI_QUERY, WikiQuery.class);
	}

	public static IndexAPI getDictionaryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_DICTIONARY, SearchConstants.DOCUMENT_TYPE_DICTIONARY);
	}

	public static SearchAPI<Word> getDictionarySearchAPI() {
		return new SearchAPI<Word>(SearchConstants.INDEX_NAME_DICTIONARY, SearchConstants.DOCUMENT_TYPE_DICTIONARY,
				Word.class);
	}

	public static IndexAPI getGlobalQueryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_GLOBAL_QUERY, SearchConstants.DOCUMENT_TYPE_GLOBAL_QUERY);
	}

	public static SearchAPI<GlobalQuery> getGlobalQuerySearchAPI() {
		return new SearchAPI<GlobalQuery>(SearchConstants.INDEX_NAME_GLOBAL_QUERY,
				SearchConstants.DOCUMENT_TYPE_GLOBAL_QUERY, GlobalQuery.class);
	}

	public static IndexAPI getUserQueryIndexAPI() {
		return new IndexAPI(SearchConstants.INDEX_NAME_USER_QUERY, SearchConstants.DOCUMENT_TYPE_USER_QUERY);
	}

	public static SearchAPI<UserQuery> getUserQuerySearchAPI() {
		return new SearchAPI<UserQuery>(SearchConstants.INDEX_NAME_USER_QUERY, SearchConstants.DOCUMENT_TYPE_USER_QUERY,
				UserQuery.class);
	}

	public static SearchAPI<ActionTask> getActionTaskSearchAPI() {
		return new SearchAPI<ActionTask>(SearchConstants.INDEX_NAME_ACTION_TASK,
				SearchConstants.DOCUMENT_TYPE_ACTION_TASK, ActionTask.class);
	}

	public static SearchAPI<SyncLog> getSyncLogAPI(String indexName) {
		return new SearchAPI<SyncLog>(indexName, SearchConstants.DOCUMENT_TYPE_SYNC_LOG, SyncLog.class);
	}

	public static SearchAPI<SecurityIncident> getSecurityIncidentSearchAPI() {
		return new SearchAPI<SecurityIncident>(SearchConstants.INDEX_NAME_SECURITY_INCIDENT,
				SearchConstants.DOCUMENT_TYPE_SECURITY_INCIDENT, SecurityIncident.class);
	}

	public static QueryIndexer getWikiQueryIndexer() {
		IndexAPI indexAPI = getWikiQueryIndexAPI();
		SearchAPI<? extends Query> searchAPI = APIFactory.getWikiQuerySearchAPI();
		return new QueryIndexer(SearchAdminAPI.getSearchConfig(), indexAPI, searchAPI,
				SearchConstants.USER_QUERY_TYPE_WIKI, true);
	}

	public static QueryIndexer getDocumentQueryIndexer() {
		IndexAPI indexAPI = getDocumentQueryIndexAPI();
		SearchAPI<? extends Query> searchAPI = APIFactory.getDocumentQuerySearchAPI();
		return new QueryIndexer(SearchAdminAPI.getSearchConfig(), indexAPI, searchAPI,
				SearchConstants.USER_QUERY_TYPE_DOCUMENT, true);
	}

	public static QueryIndexer getSocialQueryIndexer() {
		IndexAPI indexAPI = getSocialQueryIndexAPI();
		SearchAPI<? extends Query> searchAPI = APIFactory.getSocialQuerySearchAPI();
		return new QueryIndexer(SearchAdminAPI.getSearchConfig(), indexAPI, searchAPI,
				SearchConstants.USER_QUERY_TYPE_SOCIAL, true);
	}

	public static QueryIndexer getAutomationQueryIndexer() {
		IndexAPI indexAPI = getAutomationQueryIndexAPI();
		SearchAPI<? extends Query> searchAPI = APIFactory.getAutomationQuerySearchAPI();
		return new QueryIndexer(SearchAdminAPI.getSearchConfig(), indexAPI, searchAPI,
				SearchConstants.USER_QUERY_TYPE_AUTOMATION, true);
	}

	public static IndexAPI getDocumentIndexAPI() {
		return new IndexAPI(SearchConstants.SEARCH_TYPE_DOCUMENT, SearchConstants.SEARCH_TYPE_DOCUMENT);
	}

	public static IndexAPI getAutomationIndexAPI() {
		return new IndexAPI(SearchConstants.SEARCH_TYPE_AUTOMATION, SearchConstants.SEARCH_TYPE_AUTOMATION);
	}

	public static IndexAPI getDecisiontreeIndexAPI() {
		return new IndexAPI(SearchConstants.SEARCH_TYPE_DECISION_TREE, SearchConstants.SEARCH_TYPE_DECISION_TREE);
	}

	public static IndexAPI getPlaybookIndexAPI() {
		return new IndexAPI(SearchConstants.SEARCH_TYPE_PLAYBOOK, SearchConstants.SEARCH_TYPE_PLAYBOOK);
	}
	
	public static <T> SearchAPI<T> getExecutionSummarySearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY, SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY,
								SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS, true, clazz);
	}
	
	public static IndexAPI getGenericIndexAPI() {
		return new IndexAPI();
	}
	
	public static <T> SearchAPI<T> getPBArtifactsSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_PB_ARTIFACTS, SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS, clazz);
	}
	
	public static <T> SearchAPI<T> getPBAttachmentsSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_PB_ATTACHMENT, SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT, clazz);
	}
	
	public static <T> SearchAPI<T> getPBAuditLogSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_PB_AUDIT_LOG, SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG, clazz);
	}
	
	public static <T> SearchAPI<T> getPBNotesSearchAPI(Class<T> clazz) {
		return new SearchAPI<T>(SearchConstants.INDEX_NAME_PB_NOTES, SearchConstants.DOCUMENT_TYPE_PB_NOTES, clazz);
	}
}
