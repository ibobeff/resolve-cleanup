package com.resolve.search.model;

public class MetricJVM extends MetricData
{
    private static final long serialVersionUID = 4971545942667406590L;
	private long threadCount;
    private long freeMem;
    
    
    public long getThreadCount()
    {
        return threadCount;
    }
    
    public void setThreadCount(long c)
    {
        threadCount = c;
    }
    
    public long getFreeMem()
    {
        return freeMem;
    }
    
    public void setFreeMem(long f)
    {
        freeMem = f;
    }
    
}
