/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.worksheet;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.SyncData;
import com.resolve.search.SyncService;
import com.resolve.search.model.TaskResult;
import com.resolve.util.BeanUtil;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

/**
 * Currently this is being used during migration from 3.4.x to 5.1.
 * 
 * @author dutta
 *
 */
public class TaskResultIndexer<T> implements Indexer<IndexData<T>>
{
    private static volatile TaskResultIndexer instance = null;
    private static final IndexAPI worksheetIndexAPI = APIFactory.getWorksheetIndexAPI();
    
    private final IndexAPI indexAPI;

    private final ConfigSearch config;
    private long indexThreads;
    private final BulkProcessor bulkProcessor;

    private ResolveConcurrentLinkedQueue<IndexData<T>> dataQueue = null;

    public static TaskResultIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("TaskResultIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static TaskResultIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new TaskResultIndexer(config);
        }
        return instance;
    }

    private TaskResultIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexAPI = APIFactory.getTaskResultIndexAPI();
        this.indexThreads = this.config.getIndexthreads();
        this.bulkProcessor = initBulkProcessor(SearchAdminAPI.getClient(), config);
    }

    public void init()
    {
        Log.log.debug("Initializing TaskResultIndexer.");
        QueueListener<IndexData<T>> wikiIndexListener = new IndexListener<IndexData<T>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiIndexListener);
        Log.log.debug("TaskResultIndexer initialized.");
    }
    
    @Override
    public boolean enqueue(IndexData<T> indexData)
    {
        boolean result = false;

        if(indexData != null && indexData.getObjects().size() > 0)
        {
            result = dataQueue.offer(indexData);
        }
        return result;
    }

    @Override
    public boolean index(IndexData<T> indexData)
    {
        OPERATION operation = indexData.getOperation();
        try
        {
            switch (operation)
            {
                case INDEX:
                    bulkProcessTaskResults(indexData);
                    break;
                case DELETE:
                	Collection<String> sysIds = (Collection<String>) indexData.getObjects();
                    deleteTaskResults(sysIds, indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

	private void bulkProcessTaskResults(IndexData<T> indexData) throws SearchException {
		TaskResult taskResult = (TaskResult) indexData.getObjects().iterator().next();
		Long sysCreatedOn = null;

		try {
			sysCreatedOn = BeanUtil.getLongProperty(SearchConstants.INDEX_NAME_TASKRESULT,
					SearchConstants.SYS_CREATED_ON);
		} catch (Exception ex) {
		}

		if (sysCreatedOn == null) {
			sysCreatedOn = DateUtils.getCurrentTimeInMillis();
		}

		String dynamicIndexName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_TASKRESULT,
				DateUtils.getISODate(sysCreatedOn));

		IndexRequest indexRequest = new IndexRequest(dynamicIndexName, SearchConstants.DOCUMENT_TYPE_TASKRESULT,
				taskResult.getSysId()).source(SearchUtils.createJson(taskResult), XContentType.JSON);

		this.bulkProcessor.add(indexRequest);
		
		// send to synchronization service
		if (config.isSync() && !config.getSyncExcludeSet().contains(SearchConstants.INDEX_NAME_TASKRESULT)
				&& !"synclog".equals(SearchConstants.DOCUMENT_TYPE_TASKRESULT)) {
			String routingKey = null;
			if (StringUtils.isNotBlank(SearchConstants.SYS_ID)) {
				try {
					routingKey = BeanUtil.getStringProperty(taskResult, SearchConstants.SYS_ID);
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					Log.log.error(e.getMessage(), e);
				}
			}
			
			SyncData<Serializable> syncData = new SyncData<Serializable>(
					SearchUtils.getSyncLogIndexName(SearchConstants.INDEX_NAME_TASKRESULT,
							SearchConstants.DOCUMENT_TYPE_TASKRESULT),
					dynamicIndexName, SearchConstants.DOCUMENT_TYPE_TASKRESULT,
					SearchUtils.SYNC_OPERATION.INDEX, null, routingKey, taskResult.getSysId(),
					DateUtils.getCurrentTimeInMillis(), taskResult, indexData.getUsername());
			SyncService.getInstance().enqueue(syncData);
		}
	}

	private void deleteTaskResults(final Collection<String> sysIds, final String username) throws SearchException {
		indexAPI.deleteDocumentByIds(sysIds, username);
	}

	private BulkProcessor initBulkProcessor(Client client, ConfigSearch config) {
		return BulkProcessor.builder(client, new BulkProcessor.Listener() {

			@Override
			public void beforeBulk(long executionId, BulkRequest request) {
			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
				System.out.println("Bulk execution failed [" + executionId + "].\n" + failure.toString());
			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
				Log.log.info("Bulk execution completed [" + executionId + "].\n" + "Took (ms): "
						+ response.getTookInMillis() + "\n" + "Count: " + response.getItems().length + "\n"
						+ "Failure message: " + response.buildFailureMessage());
			}
		}).setConcurrentRequests(config.getConcurrentRequests()).setBulkActions(config.getBulkActions())
				.setFlushInterval(TimeValue.timeValueMillis(config.getFlushInterval()))
				.setBulkSize(new ByteSizeValue(config.getBulkSize(), ByteSizeUnit.MB)).build();
	}
}
