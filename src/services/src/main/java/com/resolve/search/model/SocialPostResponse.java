/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a response object for the social post queries. This doesn't
 * extends {@link SocialPost} but is an aggregation as we do not need some of the properties
 * from {@link SocialPost} for the response and it adds some more useful properties.
 */
@SuppressWarnings("serial")
public class SocialPostResponse extends SocialPost
{
    private static final long serialVersionUID = 1765394678951748293L;

	//username of the user who is getting this post
    private String username;

    // true if liked by the user who is getting this response
    private boolean isLiked;
    private boolean isRead;
    private boolean isStarred;
    private boolean hasDeleteRights;

    //this flag turns on if a comment is marked as the "answer"
    private boolean isSolved;

    private List<SocialCommentResponse> comments = new ArrayList<SocialCommentResponse>();

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public boolean isLiked()
    {
        return isLiked;
    }

    public void setLiked(boolean isLiked)
    {
        this.isLiked = isLiked;
    }

    public boolean isRead()
    {
        return isRead;
    }

    public void setRead(boolean isRead)
    {
        this.isRead = isRead;
    }

    public boolean isStarred()
    {
        return isStarred;
    }

    public void setStarred(boolean isStarred)
    {
        this.isStarred = isStarred;
    }

    public List<SocialCommentResponse> getComments()
    {
        return comments;
    }

    public void setComments(List<SocialCommentResponse> comments)
    {
        this.comments = comments;
    }

    public void addComments(SocialCommentResponse comment)
    {
        getComments().add(comment);
    }

    public boolean isHasDeleteRights()
    {
        return hasDeleteRights;
    }

    public void setHasDeleteRights(boolean hasDeleteRights)
    {
        this.hasDeleteRights = hasDeleteRights;
    }

    public boolean isSolved()
    {
        return isSolved;
    }

    public void setSolved(boolean isSolved)
    {
        this.isSolved = isSolved;
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();

        toString.append("SocialPost [" + super.toString() + ", title=" + getTitle() + ", content=" + getContent() + ", authorUsername=" + getAuthorUsername() + ", isLocked=" + isLocked() + ", isDeleted=" + isDeleted() + "]\n");

        if(getTargets() != null)
        {
            for(SocialTarget target : getTargets())
            {
                toString.append(toString.append(target.toString()));
            }
        }
        toString.append("\n");
        toString.append("  --> Read By:");
        for (String username : getReadUsernames())
        {
            toString.append(username + ",");
        }
        toString.append("\n");
        toString.append("  --> Liked By:");
        for (String username : getLikedUsernames())
        {
            toString.append(username + ",");
        }
        toString.append("\n");
        toString.append("  --> Starred By:");
        for (String username : getStarredUsernames())
        {
            toString.append(username + ",");
        }
        toString.append("\n");
        toString.append("  --> Comments:");
        for (SocialComment comment : getComments())
        {
            toString.append("  --> ");
            toString.append(comment.toString());
            toString.append("\n");
        }

        return toString.toString();
    }
}
