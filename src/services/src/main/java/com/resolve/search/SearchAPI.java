/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import static org.elasticsearch.search.aggregations.AggregationBuilders.dateHistogram;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram.Bucket;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalHistogram;
import org.elasticsearch.search.aggregations.metrics.stats.StatsAggregationBuilder;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion.Entry.Option;

import com.resolve.search.model.BaseIndexModel;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.UserInfo;
import com.resolve.search.model.Worksheet;
import com.resolve.search.query.UserQueryIndexAPI;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is everything about search functionality in resolve that uses
 * ElasticSearch. Every search could be different so this API shall synthesize
 * the query before requesting the data from the tier below it.
 */
public class SearchAPI<T>
{
	private static final String UNDER_SCORE = "_";
	private static final String WILD_CARD = "*";
	
    private final String[] indexNames;
    private final String[] documentTypes;
    //like worksheet,taskresult whichever has alias in it.
    private String indexNameSeed;
    private String indexAlias;
    private boolean isDynamicIndex = false;

    private final Class<T> clazz;

    private boolean isIndexAlias = false;
    
    // private final String[] childDocumentType;

    private final SearchService instance;

    SearchAPI(String[] indexNames, String[] documentTypes, Class<T> clazz)
    {
        this.indexNames = indexNames;
        this.documentTypes = documentTypes;
        this.clazz = clazz;

        instance = new SearchService();
    }

    SearchAPI(String indexName, String documentType, Class<T> clazz)
    {
        this.indexNames = new String[] {indexName};
        this.documentTypes = new String[] {documentType};
        this.clazz = clazz;

        instance = new SearchService();
    }

    SearchAPI(String indexName, String documentType, String indexAlias, boolean isDynamicIndex, Class<T> clazz)
    {
        this.indexNames = new String[] {indexName};
        this.documentTypes = new String[] {documentType};
        //this is just a short cut at this point
        this.indexNameSeed = indexName;
        this.indexAlias = indexAlias;
        this.isDynamicIndex = isDynamicIndex;
        this.clazz = clazz;

        isIndexAlias = StringUtils.isNotBlank(indexAlias);
        
        instance = new SearchService();
    }

    /* Count methods */

    public long getCountByQuery(String username, QueryBuilder... filterBuilder) throws SearchException
    {
        return getCountByQuery(documentTypes, null, filterBuilder);
    }

    public long getCountByQuery(QueryBuilder queryBuilder) throws SearchException
    {
        return getCountByQuery(documentTypes, queryBuilder);
    }

    public long getCountByQuery(QueryDTO queryDTO, QueryBuilder... filterBuilders) throws SearchException
    {
        QueryBuilder queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
        return getCountByQuery(queryBuilder, filterBuilders);
    }

    public long getCountByQuery(QueryBuilder queryBuilder, QueryBuilder... filterBuilder) throws SearchException
    {
        return getCountByQuery(documentTypes, queryBuilder, filterBuilder);
    }

    public long getCountByQuery(String[] documentTypes, QueryBuilder queryBuilder, QueryBuilder... filterBuilders) throws SearchException
    {
        long result = 0;
        if(isIndexAlias)
        {
            result = instance.getCountByQueryBuilder(new String[] {indexAlias}, documentTypes, queryBuilder, filterBuilders);
        }
        else
        {
            result = instance.getCountByQueryBuilder(indexNames, documentTypes, queryBuilder, filterBuilders);
        }
        return result;
    }
    
    public long getNonSuperUserProcReqCountByQuery(String username, QueryBuilder... filterBuilder) throws SearchException
    {
        return getNonSuperUserProcReqCountByQuery(username, documentTypes, null, filterBuilder);
    }
    
    public long getNonSuperUserProcReqCountByQuery(String username, String[] documentTypes, QueryBuilder queryBuilder, QueryBuilder... filterBuilders) throws SearchException
    {
        long result = 0;
        if(isIndexAlias)
        {
            result = instance.getNonSuperUserProcReqCountByQueryBuilder(username, new String[] {indexAlias}, documentTypes, queryBuilder, filterBuilders);
        }
        else
        {
            result = instance.getNonSuperUserProcReqCountByQueryBuilder(username, indexNames, documentTypes, queryBuilder, filterBuilders);
        }
        return result;
    }
    
    public List<String> getSuggestions(String fieldName, QueryDTO queryDTO, String username) throws SearchException
    {
        List<String> result = new LinkedList<String>();
        int limit = queryDTO.getLimit();
        if (limit < 1)
        {
            // default is 5
            limit = 5;
        }
        //SuggestResponse response = null;
        // this will give fuzzyness up to 2 laters
        //response = SearchAdminAPI.getClient().prepareSuggest(indexNames).addSuggestion(new CompletionSuggestionFuzzyBuilder("fuzzyBuilder").field(fieldName).setFuzzyEditDistance(1).text(queryDTO.getSearchTerm()).size(limit)).execute().actionGet();
        
        SearchResponse response = SearchAdminAPI.getClient().prepareSearch(indexNames).suggest(new SuggestBuilder().addSuggestion("fuzzyBuilder", SuggestBuilders.completionSuggestion(fieldName).text(queryDTO.getSearchTerm()).size(limit))).get();    

        if(response != null)
        {
            try
            {
                Iterator<? extends org.elasticsearch.search.suggest.Suggest.Suggestion.Entry.Option> iterator = response.getSuggest().iterator().next().getEntries().get(0).iterator();
    
                if (iterator != null)
                {
                    while (iterator.hasNext())
                    {
                        Option obj = (Option) iterator.next();
                        result.add(obj.getText().toString());
                    }
                }
            }
            catch (NoSuchElementException e)
            {
                // Ignore it didn't find any suggestion
            }
        }
        return result;
    }

    public T findDocumentById(String sysId, String username, boolean ignoreESInMemCache) throws SearchException {
        return findDocumentById(sysId, null, username, ignoreESInMemCache);
    }
    
    public T findDocumentById(String sysId, String username) throws SearchException
    {
        return findDocumentById(sysId, null, username);
    }

    public T findDocumentById(String sysId, String routingKey, String username, 
    						  boolean ignoreESInMemCache) throws SearchException {
        return findDocumentById(sysId, routingKey, false, username, ignoreESInMemCache);
    }
    
    public T findDocumentById(String sysId, String routingKey, String username) throws SearchException
    {
        return findDocumentById(sysId, routingKey, false, username);
    }
    
    public T findDocumentById(String sysId, String routingKey, boolean fromPrimary, String username, 
    						  boolean ignoreESInMemCache) throws SearchException {
        T document = null;
        long startTime = System.currentTimeMillis();
        
        if(isDynamicIndex) {
            //we'll do a little prediction and see if we find the document in today's
            //index, this way we will never miss any data
            String tmpIndexName = SearchUtils.getDynamicIndexName(indexNameSeed, DateUtils.GetUTCDate());
            
            final List<String> indexNames = new ArrayList<String>();
            
            indexNames.add(tmpIndexName);
            indexNames.add(indexAlias);
            
            final Map<String, T> foundDocs = new HashMap<String, T>();
            final Map<String, SearchException> searchExceptions = new HashMap<String, SearchException>();
            
            String docFoundIndexName = indexNames.stream()
            		   				   .filter(indxName -> {
            		   					   T lDocument = null;
            		   					   long tStartTime = System.currentTimeMillis();
            		   					   
            		   					   try {
	            		   					   if (indxName.contains(UNDER_SCORE)) {
	            		   					       //pragmatic approach
	            		   						   
												   lDocument = instance.findById(indxName, documentTypes[0], clazz, sysId, 
												    		   				     null, routingKey, true, ignoreESInMemCache);
	            		   					   } else {
	
	            		   						   lDocument = instance.findByQuery(indexAlias, documentTypes[0], clazz, 
	            		   								   							sysId);
	            		   					   }
            		   					   } catch (SearchException e) {
        		   							   searchExceptions.put(indxName, e);
        		   						   }
            		   					   
            		   					   if (Log.log.isDebugEnabled()) {
            		   						   if (!searchExceptions.containsKey(indxName)) {
            		   							   Log.log.debug(String.format("Time taken to find Document by Id %s, " +
         		   									   					   	   "routing key %s, from Primary %b, and " +
         		   									   					   	   "for user %s in Index %s: %d msec", sysId, 
         		   									   					   	   routingKey, fromPrimary, username, indxName, 
         		   									   					   	   (System.currentTimeMillis() - tStartTime)));
            		   						   }
            		   					   }
            		   					   
            		   					   if (lDocument != null && !searchExceptions.containsKey(indxName)) {
            		   						   foundDocs.put(indxName, lDocument);
            		   					   }
            		   					
            		   					   return lDocument != null && !searchExceptions.containsKey(indxName);
            		   				   })
            		   				   .findFirst()
            		   				   .orElse(null);
            
            if (StringUtils.isNotBlank(docFoundIndexName)) {
            	document = foundDocs.get(docFoundIndexName);
            } else  if (StringUtils.isBlank(docFoundIndexName) && MapUtils.isNotEmpty(searchExceptions)) {
            	searchExceptions.keySet().stream().forEach(indexName -> {
            		SearchException se = searchExceptions.get(indexName);
            		Log.log.error(String.format("Error %sin finding %s with sys id %s from index %s" , 
            									(StringUtils.isNotBlank(se.getMessage()) ? 
            									 "[" + se.getMessage() + "] " : ""),
            									clazz.getName(), sysId, indexName), se);
            	});
            	throw searchExceptions.get(searchExceptions.keySet().toArray(new String[0])[0]);
            }
        } else {
            document = instance.findById(indexNames[0], documentTypes[0], clazz, sysId, null, routingKey, 
            							 true, ignoreESInMemCache);
        }
        
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("Time taken to find Document by Id %s, routing key %s, from Primary %b, and for " +
                            "user %s: %d msec", sysId, routingKey, fromPrimary, username,
                            (System.currentTimeMillis() - startTime)));
        }
        
        if (document != null && document instanceof Worksheet) {
            if (StringUtils.isNotBlank(((Worksheet)document).getSysOrg())) {
                if (!UserUtils.isOrgAccessible(((Worksheet)document).getSysOrg(), username, false, false)) {
                    throw new SearchException("User " + username + " does not belong to Worksheet's Org.");
                }
            } else {
                if (!UserUtils.isNoOrgAccessible(username)) {
                    throw new SearchException("User " + username + " does not have access to No/None Org.");
                }
            }
        } else if (document != null && (document instanceof ProcessRequest || document instanceof TaskResult)) {
            String worksheetId = null;
            
            if (document instanceof ProcessRequest) {
                worksheetId = ((ProcessRequest)document).getProblem();
            } else if (document instanceof TaskResult) {
                worksheetId = ((TaskResult)document).getProblemId();
            }
            
            if (StringUtils.isNotBlank(worksheetId)) {
                BaseIndexModel baseModel = (BaseIndexModel) findDocumentById(worksheetId, username);
                
                if (baseModel != null &&
                    StringUtils.isNotBlank(baseModel.getSysOrg())) {
                    // Txfer Worksheets sysOrg to Model's sysOrg
                    
                    ((BaseIndexModel)document).setSysOrg(baseModel.getSysOrg());
                }
            }
        }
        
        return document;
    }
    
    public T findDocumentById(String sysId, String routingKey, boolean fromPrimary, String username) throws SearchException
    {
    	return findDocumentById(sysId, routingKey, fromPrimary, username, false);
    }

    
    /**
     * This is for document that has parent (e.g., social comment, wiki attachment)
     * 
     * @param sysId
     * @param parentId
     * @param username
     * @return
     * @throws SearchException
     */
    public T findDocumentById(String sysId, String parentId, String routingKey, String username) throws SearchException
    {
        T document = null;
        
        if(isDynamicIndex)
        {
            //we'll do a little prediction and see if we find the document in its current month's 
            //index, this way we will never miss any data
            String tmpIndexName = SearchUtils.getDynamicIndexName(indexNameSeed, DateUtils.GetUTCDate());
            //pragmatic approach
            document = instance.findById(tmpIndexName, documentTypes[0], clazz, sysId, parentId, routingKey);
            //if it wasn't found may be it's older, query based is fine
            if(document == null)
            {
                document = instance.findByQuery(indexAlias, documentTypes[0], clazz, sysId);
            }
        }
        else
        {
            document = instance.findById(indexNames[0], documentTypes[0], clazz, sysId, parentId, routingKey);
        }
        
        return document;
    }

    public List<T> findDocumentByIds(List<String> sysIds, String username) throws SearchException
    {
        List<T> result = null;
        
        if(isIndexAlias)
        {
            result = instance.findByQuery(indexAlias, documentTypes[0], clazz, sysIds);
        }
        else
        {
            result = instance.findByIds(indexNames[0], documentTypes[0], clazz, sysIds);
        }

        return result; 
    }
    
    public Map<String, Long> getStatistics(QueryBuilder queryBuilder, StatsAggregationBuilder statsBuilder, String username) throws SearchException
    {
        return instance.getStatistics(indexNames, documentTypes, queryBuilder, statsBuilder, username);
    }

    /* Search methods */

    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, String username) throws SearchException
    {
    	return searchByQuery(queryDTO, username, null, false);
    }
    
    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, String username, boolean enforceOrg) throws SearchException
    {
    		return searchByQuery(queryDTO,username, null, enforceOrg);
    }

    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, String username, String[] indexNames) throws SearchException
    {
    		return searchByQuery(queryDTO,username, indexNames, false);
    }
    
    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, String username, String[] indexNames, boolean enforceOrg) throws SearchException
    {
        ResponseDTO<T> result = new ResponseDTO<T>();
        try
        {
            QueryBuilder queryBuilder = getQueryBuilder(queryDTO, enforceOrg);
           
            if(indexNames != null && indexNames.length > 0) {
            	result = instance.searchByQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, 
            									null, null, null, username);
            }
            else {
                result = searchByQuery(queryDTO, queryBuilder, null, null, null, username);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public QueryBuilder getQueryBuilder(QueryDTO queryDTO, boolean enforceOrg) {
		return StringUtils.isBlank(queryDTO.getSearchTerm())
				? SearchUtils.createBoolQueryBuilder(queryDTO, clazz.getName(), enforceOrg)
				: SearchUtils.createTermQueryBuilder(queryDTO.getSearchTerm());
    }
    
    public QueryBuilder getQueryBuilder(QueryDTO queryDTO) {
		return getQueryBuilder(queryDTO, false);
    }
    
    public ResponseDTO<T> searchSirTaskResults(QueryDTO queryDTO, String[] fields, String username) throws SearchException
    {
        ResponseDTO<T> result = new ResponseDTO<T>();
        try
        {
        	if (queryDTO != null && StringUtils.isBlank(queryDTO.getSearchTerm()))
        	{
        		result = searchByQuery(queryDTO, username);
        	}
        	else
        	{
        		BoolQueryBuilder booleanQueryBuilder = (BoolQueryBuilder)SearchUtils.createBoolQueryBuilder(queryDTO, clazz.getName());
        		String[] searchTermArray = queryDTO.getSearchTerm().trim().split(" ");
        		StringBuilder searchTermBuilder = new StringBuilder();
        		for (String searchTerm : searchTermArray)
        		{
        		    searchTermBuilder.append(searchTerm.trim()).append("* |");
        		}
                SimpleQueryStringBuilder simpleQueryStringBuilder = QueryBuilders.simpleQueryStringQuery(searchTermBuilder.toString());
                simpleQueryStringBuilder.defaultOperator(Operator.AND).analyzeWildcard(true);
                
                if (fields == null || fields.length == 0)
                {
                	fields = new String[] {"*.*"};
                }
                
                for (String field : fields)
            		simpleQueryStringBuilder.field(field);
                
                booleanQueryBuilder.must(simpleQueryStringBuilder);
        		result = searchByQuery(queryDTO, booleanQueryBuilder, null, null, fields, username);
        	}
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, String username) throws SearchException
    {
        return searchByQuery(queryDTO, queryBuilder, null, null, null, username);
    }
    
    @Deprecated
    public ResponseDTO<T> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, QueryBuilder[] filterBuilders, String username) throws SearchException
    {
        return searchByFilteredQuery(queryDTO, queryBuilder, null, filterBuilders, null, username);
    }
    
    @Deprecated
    public ResponseDTO<T> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, String[] indexNames, QueryBuilder[] filterBuilders, String username) throws SearchException
    {
        return searchByFilteredQuery(queryDTO, queryBuilder, indexNames, null, filterBuilders, null, username);
    }
    
    @Deprecated
    public ResponseDTO<T> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, StatsAggregationBuilder statsBuilder, QueryBuilder[] filterBuilders, String[] highlightedFields, String username) throws SearchException
    {
        if(isIndexAlias)
        {
            return instance.searchByFilteredQuery(clazz, new String[] {indexAlias}, documentTypes, queryDTO, queryBuilder, statsBuilder, filterBuilders, highlightedFields, username);
        }
        else
        {
            return instance.searchByFilteredQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, statsBuilder, filterBuilders, highlightedFields, username);
        }
    }
    
    @Deprecated
    public ResponseDTO<T> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, String[] indexNames, StatsAggregationBuilder statsBuilder, QueryBuilder[] filterBuilders, String[] highlightedFields, String username) throws SearchException
    {
    	return instance.searchByFilteredQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, statsBuilder, filterBuilders, highlightedFields, username);
    }

    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, QueryBuilder[] filterBuilders, String username) throws SearchException
    {
        return searchByQuery(queryDTO, queryBuilder, null, filterBuilders, null, username);
    }

    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, StatsAggregationBuilder statsBuilder, QueryBuilder[] filterBuilders, String[] highlightedFields, String username) throws SearchException
    {
        if(isIndexAlias)
        {
            return instance.searchByQuery(clazz, new String[] {indexAlias}, documentTypes, queryDTO, queryBuilder, statsBuilder, filterBuilders, highlightedFields, username);
        }
        else
        {
            return instance.searchByQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, statsBuilder, filterBuilders, highlightedFields, username);
        }
    }
    
    public ResponseDTO<T> searchByQuery(String[] documentTypes, QueryDTO queryDTO, BoolQueryBuilder queryBuilder, String username) throws SearchException
    {
        if(isIndexAlias)
        {
            return instance.searchByQuery(clazz, new String[] {indexAlias}, documentTypes, queryDTO, queryBuilder, null, null, null, username);
        }
        else
        {
            return instance.searchByQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, null, null, null, username);
        }
    }

    /**
     * This method uses the search term from the QueryDTO and does a Fuzzy query
     * on the fields provided through searchFields array.
     * 
     * @param queryType
     * @param queryDTO
     * @param queryBuilder
     * @param filterBuilders
     * @param highlightedFields
     * @param userInfo
     * @return
     * @throws SearchException
     */
    public ResponseDTO<T> searchByTerm(String queryType, QueryDTO queryDTO, QueryBuilder queryBuilder, QueryBuilder[] filterBuilders, String[] highlightedFields, UserInfo userInfo) throws SearchException
    {
        return searchByTerm(documentTypes, queryType, queryDTO, queryBuilder, filterBuilders, highlightedFields, userInfo);
    }
    
    public ResponseDTO<T> searchByTerm(String[] documentTypes, String queryType, QueryDTO queryDTO, QueryBuilder queryBuilder, QueryBuilder[] filterBuilders, String[] highlightedFields, UserInfo userInfo) throws SearchException
    {
        ResponseDTO<T> result = new ResponseDTO<T>();

        if(isIndexAlias)
        {
            result = instance.searchByQuery(clazz, new String[] {indexAlias}, documentTypes, queryDTO, queryBuilder, null, filterBuilders, highlightedFields, userInfo.getUsername());
        }
        else
        {
            result = instance.searchByQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, null, filterBuilders, highlightedFields, userInfo.getUsername());
        }
        // if the search term yielded some result then register it as user's query
        if (result.getRecords() != null && result.getRecords().size() > 0 && StringUtils.isNotBlank(queryDTO.getSearchTerm()))
        {
            UserQueryIndexAPI.indexUserQuery(queryDTO.getSearchTerm(), queryType, result.getRecords().size(), userInfo);
            
            //if this is the phrase selected from one of the suggesters then update ratings on that.
            QueryAPI.addSelectCount(queryType, queryDTO.getSearchTerm(), userInfo.getUsername());
        }

        return result;
    }
    
    public ResponseDTO<T> searchByTerm(String[] indexNames, String[] documentTypes, String queryType, QueryDTO queryDTO, QueryBuilder queryBuilder, QueryBuilder[] filterBuilders, String[] highlightedFields, UserInfo userInfo) throws SearchException
    {
        ResponseDTO<T> result = instance.searchByQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, null, filterBuilders, highlightedFields, userInfo.getUsername());

        // if the search term yielded some result then register it as user's query
        if (result.getRecords() != null && result.getRecords().size() > 0)
        {
            UserQueryIndexAPI.indexUserQuery(queryDTO.getSearchTerm(), queryType, result.getRecords().size(), userInfo);
            
            //if this is the phrase selected from one of the suggesters then update ratings on that.
            QueryAPI.addSelectCount(queryType, queryDTO.getSearchTerm(), userInfo.getUsername());
        }

        return result;
    }

    public ResponseDTO<T> scrollByQuery(QueryBuilder queryBuilder, int batchSize, long timeoutInMillis) throws SearchException
    {
        return instance.scrollByQuery(indexNames, documentTypes, indexAlias, clazz, timeoutInMillis, batchSize, queryBuilder);
    }

    public void processDataByScrolling(QueryBuilder queryBuilder, int batchSize, long timeoutInMillis, ScrollCallback callback) throws SearchException
    {
        instance.processDataByScrolling(indexNames, documentTypes, indexAlias, clazz, timeoutInMillis, batchSize, queryBuilder, callback);
    }
    
    /**
     * This method provides data that can be aggregated based on date and interval.
     * 
     * Example of a final JSON query against ES:
     *  {
     *      "query": {
     *          "match": {
     *              "sysCreatedBy": "johnuser"
     *          }
     *      },
     *      "aggs": {
     *          "wikidocument": {
     *              "date_histogram": {
     *                  "field": "sysUpdatedDt",
     *                  "interval": "day"
     *              }
     *          }
     *      }
     *  }
     * 
     * QueryDTO will have modelName="wikidocument", selectColumns=sysUpdatedDt, interval="day" and filterItem with sysCreatedBy="johnuser"
     * 
     * @param queryDTO available "interval" is second,minute,hour,day,week,month,quarter,year or other expression like 1.5h etc.
     * 
     * @return key is the unix timestamp and the value is document count. 
     * @throws ExecutionException 
     * @throws InterruptedException 
     */
    public Map<Long, Long> getDateHistogramData(QueryDTO queryDTO) throws InterruptedException, ExecutionException
    {
        Map<Long, Long> result = new TreeMap<Long, Long>();
        
        QueryBuilder query = SearchUtils.createBoolQueryBuilder(queryDTO);

        DateHistogramInterval interval = new DateHistogramInterval(queryDTO.getInterval());
        //dateHistogram(queryDTO.getModelName()).field(queryDTO.getSelectColumns()).interval(interval);
        /*
        DateHistogramBuilder histogramBuilder = new DateHistogramBuilder(queryDTO.getModelName());
        histogramBuilder.field(queryDTO.getSelectColumns());
        Interval interval = new Interval(queryDTO.getInterval());
        histogramBuilder.interval(interval);
        */
        
        SearchRequestBuilder request = instance.getClient().prepareSearch(indexNames).setTypes(documentTypes).setQuery(query).addAggregation(dateHistogram(queryDTO.getModelName()).field(queryDTO.getSelectColumns()).dateHistogramInterval(interval));
        SearchResponse response = request.execute().actionGet();
        //Aggregations aggrs = response.getAggregations().get(queryDTO.getModelName());

        InternalHistogram histogram = response.getAggregations().get(queryDTO.getModelName());
        Collection<InternalHistogram.Bucket> buckets = histogram.getBuckets();
        for(Bucket bucket : buckets)
        {
            result.put(((Number)bucket.getKey()).longValue(), bucket.getDocCount());
        }
     
        return result;
    }

    /**
     * Search ElasticSearch using the json query. This is the raw version of query api. In general
     * we use QueryDTO but this api allows you to run any DSL query you want to run and get data
     * back as array of json string.
     * 
     * @param indexName actual index name or alias
     * @param documentType
     * @param query ElasticSearch json query example { "match_all" : { } }
     * @param start
     * @param limit
     * @return
     */
    public static ResponseDTO<String> searchByQuery(String indexName, String documentType, String query, int start, int limit)
    {
        return searchByQuery(new String[] {indexName}, new String[] {documentType}, query, start, limit);
    }
    
    /**
     * Search ElasticSearch using the json query. This is the raw version of query api. In general
     * we use QueryDTO but this api allows you to run any DSL query you want to run and get data
     * back as array of json string.
     * 
     * @param indexNames actual index names or aliases
     * @param documentTypes
     * @param query ElasticSearch json query example { "match_all" : { } }
     * @param start
     * @param limit
     * @return
     */
    public static ResponseDTO<String> searchByQuery(String[] indexNames, String[] documentTypes, String query, int start, int limit)
    {
        ResponseDTO<String> result = null; 
        SearchService searchService = new SearchService();
        try
        {
            result = searchService.searchByQuery(indexNames, documentTypes, query, start, limit);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }
    
    public ResponseDTO<T> searchByQuery(QueryDTO queryDTO, QueryBuilder[] filters, String username) throws SearchException
    {
        ResponseDTO<T> result = new ResponseDTO<T>();
        try
        {
            QueryBuilder queryBuilder = null;
            if (StringUtils.isBlank(queryDTO.getSearchTerm()))
            {
                queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
            }
            else
            {
                queryBuilder = SearchUtils.createTermQueryBuilder(queryDTO.getSearchTerm());
            }
            result = searchByQuery(queryDTO, queryBuilder, null, filters, null, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    public ResponseDTO<List<String>> getAllIndexNames()
    {
    	ResponseDTO<List<String>> resp = new ResponseDTO<List<String>>();
    	final ResponseDTO<List<String>> getAllIndxNamesResp = instance.getAllIndexNames(indexNames[0] + 
    																					(isDynamicIndex ? 
    																					 UNDER_SCORE + WILD_CARD : ""));
    	
    	if (getAllIndxNamesResp != null && getAllIndxNamesResp.getData() != null) {
    		resp.setSuccess(true).setData(getAllIndxNamesResp.getData());
    	} else {
    		resp.setSuccess(false).setMessage(StringUtils.isNotBlank(getAllIndxNamesResp.getMessage()) ? 
					 												 getAllIndxNamesResp.getMessage() : "");
    	}
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("getAllIndexNames(%s) returning %d index names", indexNames[0],
    									resp.getData().size()));
    	}
    	
    	return resp;
    }
    
    public ResponseDTO<String> getAllIndexNames(Instant onOrBefore) {
    	ResponseDTO<String> resp = new ResponseDTO<String>();
    	
    	if (onOrBefore == null) {
    		ResponseDTO<List<String>> getAllIndxNamesResp = getAllIndexNames();
    		
    		if (getAllIndxNamesResp != null && getAllIndxNamesResp.isSuccess() &&
    			getAllIndxNamesResp.getData() != null) {
    			resp.setRecords(getAllIndxNamesResp.getData())
    				.setTotal(getAllIndxNamesResp.getData().size())
    				.setSuccess(true);
    		} else {
    			resp.setSuccess(false).setMessage(StringUtils.isNotBlank(getAllIndxNamesResp.getMessage()) ? 
    																	 getAllIndxNamesResp.getMessage() : "");
    		}
    	} else {
	    	if (isDynamicIndex) {
	    		final ResponseDTO<List<String>> getAllIndxNamesResp = getAllIndexNames();
	    		
	    		if (getAllIndxNamesResp != null && getAllIndxNamesResp.isSuccess() &&
	    			getAllIndxNamesResp.getData() != null) {
	    			List<String> filteredIndxNames = 
	    							getAllIndxNamesResp
	    							.getData()
	    							.parallelStream()
	    							.filter(indexName -> {
										String yearMonthDay = StringUtils.substringAfter(indexName, UNDER_SCORE);
										 
										if (StringUtils.isBlank(yearMonthDay) || yearMonthDay.length() != 8) {
											return false;
										}
										 
										String year = yearMonthDay.substring(0, 4).trim();
										String month = yearMonthDay.substring(4, 6).trim();
										String day = yearMonthDay.substring(6, 8).trim();
										 
										if (StringUtils.isBlank(year) || year.length() != 4 ||
											StringUtils.isBlank(month) || month.length() != 2 ||
											StringUtils.isBlank(day) || day.length() != 2) {
											return false;
										}
										 
										return onOrBefore.compareTo(Instant
												 					.parse(String.format("%s-%s-%sT00:00:00Z", 
												 										 year, month, day))) >= 0;
									})
									.collect(Collectors.toList());
	    			
	    			filteredIndxNames.sort((o1, o2) -> {
	    				String yearMonthDay1 = StringUtils.substringAfter(o1, UNDER_SCORE);
	    				
	    				String year1 = yearMonthDay1.substring(0, 4).trim();
						String month1 = yearMonthDay1.substring(4, 6).trim();
						String day1 = yearMonthDay1.substring(6, 8).trim();
						
						String yearMonthDay2 = StringUtils.substringAfter(o2, UNDER_SCORE);
	    				
	    				String year2 = yearMonthDay2.substring(0, 4).trim();
						String month2 = yearMonthDay2.substring(4, 6).trim();
						String day2 = yearMonthDay2.substring(6, 8).trim();
						
						return Instant.parse(String.format("%s-%s-%sT00:00:00Z", year1, month1, day1))
							   .compareTo(Instant.parse(String.format("%s-%s-%sT00:00:00Z", year2, month2, day2)));
	    			});
	    			
	    			resp.setSuccess(true).setRecords(filteredIndxNames).setTotal(filteredIndxNames.size());
	    		} else {
	    			resp.setSuccess(false).setMessage(StringUtils.isNotBlank(getAllIndxNamesResp.getMessage()) ? 
							 												 getAllIndxNamesResp.getMessage() : "");
	    		}
	    	} else {
	    		resp.setSuccess(false).setMessage(String.format("Index %s is not a daily index", indexNames[0]));
	    	}
    	}
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("getAllIndexNames(%s) %sreturning %d index names", indexNames[0],
    									(onOrBefore != null ? "on or before " + onOrBefore + " " : ""),
    									resp.getTotal()));
    	}
    	
    	return resp;
    }
    
    public ResponseDTO<List<String>> getAllEmptyIndexNames()
    {
    	ResponseDTO<List<String>> resp = new ResponseDTO<List<String>>();
    	final ResponseDTO<List<String>> getAllEmptyIndxNamesResp = instance.getAllEmptyIndexNames(indexNames[0] + 
    																							  (isDynamicIndex ? 
    																							   UNDER_SCORE + WILD_CARD : 
    																							   ""));
    	
    	if (getAllEmptyIndxNamesResp != null && getAllEmptyIndxNamesResp.getData() != null) {
    		resp.setSuccess(true).setData(getAllEmptyIndxNamesResp.getData());
    	} else {
    		resp.setSuccess(false).setMessage(StringUtils.isNotBlank(getAllEmptyIndxNamesResp.getMessage()) ? 
    																 getAllEmptyIndxNamesResp.getMessage() : "");
    	}
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("getAllEmptyIndexNames(%s) returning %d index names", indexNames[0],
    									resp.getData().size()));
    	}
    	
    	return resp;
    }
    
    // Both from and to are inclusive
    public ResponseDTO<String> getAllEmptyIndexNames(Instant from, Instant to) {
    	ResponseDTO<String> resp = new ResponseDTO<String>();
    	
    	if (!isDynamicIndex) {
    		resp.setSuccess(false).setMessage(String.format("Index %s is not a daily index", indexNames[0]));
    	}
    	
    	if (to == null) {
    		to = Instant.now().truncatedTo(ChronoUnit.DAYS).minus(1l, ChronoUnit.DAYS);
    	}
    	
    	final Instant finalTo = to;
    	
    	final ResponseDTO<List<String>> getAllIndxNamesResp = getAllEmptyIndexNames();
		
		if (getAllIndxNamesResp != null && getAllIndxNamesResp.isSuccess() &&
			getAllIndxNamesResp.getData() != null) {
			List<String> filteredIndxNames = 
							getAllIndxNamesResp
							.getData()
							.parallelStream()
							.filter(indexName -> {
								boolean onOrAfter = from != null ? false : true;
								
								String yearMonthDay = StringUtils.substringAfter(indexName, UNDER_SCORE);
								 
								if (StringUtils.isBlank(yearMonthDay) || yearMonthDay.length() != 8) {
									return false;
								}
								 
								String year = yearMonthDay.substring(0, 4).trim();
								String month = yearMonthDay.substring(4, 6).trim();
								String day = yearMonthDay.substring(6, 8).trim();
								 
								if (StringUtils.isBlank(year) || year.length() != 4 ||
									StringUtils.isBlank(month) || month.length() != 2 ||
									StringUtils.isBlank(day) || day.length() != 2) {
									return false;
								}
								
								Instant indexInstant = Instant.parse(String.format("%s-%s-%sT00:00:00Z", 
			 																		year, month, day));
								if (!onOrAfter) {
									onOrAfter = from.compareTo(indexInstant) <= 0;
								}
								
								if (!onOrAfter) {
									return onOrAfter;
								}
								
								return finalTo.compareTo(indexInstant) >= 0;
							})
							.collect(Collectors.toList());
			
			filteredIndxNames.sort((o1, o2) -> {
				String yearMonthDay1 = StringUtils.substringAfter(o1, UNDER_SCORE);
				
				String year1 = yearMonthDay1.substring(0, 4).trim();
				String month1 = yearMonthDay1.substring(4, 6).trim();
				String day1 = yearMonthDay1.substring(6, 8).trim();
				
				String yearMonthDay2 = StringUtils.substringAfter(o2, UNDER_SCORE);
				
				String year2 = yearMonthDay2.substring(0, 4).trim();
				String month2 = yearMonthDay2.substring(4, 6).trim();
				String day2 = yearMonthDay2.substring(6, 8).trim();
				
				return Instant.parse(String.format("%s-%s-%sT00:00:00Z", year1, month1, day1))
					   .compareTo(Instant.parse(String.format("%s-%s-%sT00:00:00Z", year2, month2, day2)));
			});
			
			resp.setSuccess(true).setRecords(filteredIndxNames).setTotal(filteredIndxNames.size());
		} else {
			resp.setSuccess(false).setMessage(StringUtils.isNotBlank(getAllIndxNamesResp.getMessage()) ? 
					 												 getAllIndxNamesResp.getMessage() : "");
		}
		
    	if (Log.log.isDebugEnabled()) {
    		String inclusiveTxt = " both inclusive ";
    		
    		if (from == null && to == null) {
    			inclusiveTxt = " ";
    		} else if (from == null || to == null) {
    			inclusiveTxt = " inclusive ";
    		}
    		
    		Log.log.debug(String.format("getAllEmptyIndexNames(%s)%s%s%sreturning %d index names", indexNames[0],
    									(from != null ? " from " + from + " " : ""),(to != null ? " to " + to + " " : ""),
    									inclusiveTxt, resp.getTotal()));
    	}
    	
    	return resp;
    }
    
    public Pair<ResponseDTO<T>, String> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, 
    														  QueryBuilder[] filterBuilders, String username,
    														  String scrollId, 
    														  TimeValue scrollTimeOut) throws SearchException {
        return searchByFilteredQuery(queryDTO, queryBuilder, null, filterBuilders, null, username, scrollId,
        							 scrollTimeOut);
    }
    
    public Pair<ResponseDTO<T>, String> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, 
    														  StatsAggregationBuilder statsBuilder, 
    														  QueryBuilder[] filterBuilders, String[] highlightedFields, 
    														  String username, String scrollId, 
    														  TimeValue scrollTimeOut) throws SearchException {
        if(isIndexAlias) {
            return instance.searchByFilteredQuery(clazz, new String[] {indexAlias}, documentTypes, queryDTO, queryBuilder, 
            									  statsBuilder, filterBuilders, highlightedFields, username, scrollId,
            									  scrollTimeOut);
        } else {
            return instance.searchByFilteredQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, statsBuilder, 
            									  filterBuilders, highlightedFields, username, scrollId, scrollTimeOut);
        }
    }
    
    public Pair<ResponseDTO<T>, String> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, 
    														  String[] indexNames, QueryBuilder[] filterBuilders, 
    														  String username, String scrollId,
    														  TimeValue scrollTimeOut) throws SearchException {
        return searchByFilteredQuery(queryDTO, queryBuilder, indexNames, null, filterBuilders, null, username, scrollId,
        							 scrollTimeOut);
    }
    
    public Pair<ResponseDTO<T>, String> searchByFilteredQuery(QueryDTO queryDTO, QueryBuilder queryBuilder, 
    														  String[] indexNames, StatsAggregationBuilder statsBuilder, 
    														  QueryBuilder[] filterBuilders, String[] highlightedFields, 
    														  String username, String scrollId, 
    														  TimeValue scrollTimeOut) throws SearchException {
    	return instance.searchByFilteredQuery(clazz, indexNames, documentTypes, queryDTO, queryBuilder, statsBuilder, 
    										  filterBuilders, highlightedFields, username, scrollId, scrollTimeOut);
    }
    
    public boolean clearScroll(String scrollId) {
    	return instance.clearScroll(scrollId);
    }
    
    public long getScrollCtxKeepAliveDur() {
		return instance.getScrollCtxKeepAliveDur();
	}
	
	public Duration getScrollCtxKeepAliveDurInMinutes() {
		return instance.getScrollCtxKeepAliveDurInMinutes();
	}
}