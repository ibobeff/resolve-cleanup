/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
public class ExecuteState extends BaseIndexModel
{
    private static final long serialVersionUID = 8900020261496074396L;
	private String debug;
    private String executeId;
    private String flows;
    private String inputs;
    private String params;
    private String problemId;
    private String processId;
    private String reference;
    private String targetAddress;
    private Long processStartTime;
    private String processState;
    private String componentId;

    public ExecuteState()
    {
        super();
    }

    public ExecuteState(String sysId, String sysOrg, String username)
    {
        super(sysId, sysOrg, username);
    }

    public String getDebug()
    {
        return debug;
    }

    public void setDebug(String debug)
    {
        this.debug = debug;
    }

    public String getExecuteId()
    {
        return executeId;
    }

    public void setExecuteId(String executeId)
    {
        this.executeId = executeId;
    }

    public String getFlows()
    {
        return flows;
    }

    public void setFlows(String flows)
    {
        this.flows = flows;
    }

    public String getInputs()
    {
        return inputs;
    }

    public void setInputs(String inputs)
    {
        this.inputs = inputs;
    }

    public String getParams()
    {
        return params;
    }

    public void setParams(String params)
    {
        this.params = params;
    }

    public String getProblemId()
    {
        return problemId;
    }

    public void setProblemId(String problemId)
    {
        this.problemId = problemId;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setProcessId(String processId)
    {
        this.processId = processId;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getTargetAddress()
    {
        return targetAddress;
    }

    public void setTargetAddress(String targetAddress)
    {
        this.targetAddress = targetAddress;
    }

    public Long getProcessStartTime()
    {
        return processStartTime;
    }

    public void setProcessStartTime(Long processStartTime)
    {
        this.processStartTime = processStartTime;
    }

    public String getProcessState()
    {
        return processState;
    }

    public void setProcessState(String processState)
    {
        this.processState = processState;
    }
    
    public String getComponentId()
    {
        return componentId;
    }
    
    public void setComponentId(String componentId)
    {
        this.componentId = componentId;
    }
    
    @Override
    public String toString()
    {
        return "ExecuteState [debug=" + debug + ", executeId=" + executeId + ", flows=" + flows + ", inputs=" + inputs + ", params=" + params + ", problemId=" + problemId + ", processId=" + processId + ", reference=" + reference + ", targetAddress=" + targetAddress + ", processStartTime=" + processStartTime + ", processState=" + processState + 
               (StringUtils.isNotBlank(componentId) ? ", componentId=" + componentId : "") + "]";
    }
}
