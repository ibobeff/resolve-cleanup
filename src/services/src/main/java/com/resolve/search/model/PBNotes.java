/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;

public class PBNotes extends BaseIndexModel implements Serializable
{
	private static final long serialVersionUID = 5324998757822615599L;
	
	public PBNotes()
	{
		super();
	}
	
	public PBNotes(String sysId, String sysOrg)
	{
		super(sysId, sysOrg);
	}
	
	public PBNotes(String sysId, String sysOrg, String createdBy)
	{
		super(sysId, sysOrg, createdBy);
	}
	
	public PBNotes(PBNotes note) 
	{
		super(note.getSysId(), note.getSysOrg(), note.getSysCreatedBy());
		this.setWorksheetId(note.getWorksheetId());
		this.setIncidentId(note.getIncidentId());
		this.setNote(note.getNote());
		this.setActivityName(note.getActivityName());
		this.setCategory(note.getCategory());
		this.setPostedBy(note.getPostedBy());
		this.setComponentId(note.getComponentId());
		this.setActivityId(note.getActivityId());
		this.setModified(note.isModified());
		this.setSource(note.getSource());
		this.setSourceValue(note.getSourceValue());
		this.setTitle(note.getTitle());
		this.setSourceAndValue(note.getSourceAndValue());
		this.setSir(note.getSir());
	}
	
	private String worksheetId;
	private String incidentId;
	private String note;
	private String activityName;
	private String category;
	private String postedBy;
	private String componentId;
	private String activityId;
	private String auditMessage;
	private Boolean modified;
	private String source;
	private String sourceValue;
	private String title;
	private String sourceAndValue;
	private String sir;
	
	public String getWorksheetId()
	{
		return worksheetId;
	}
	public void setWorksheetId(String worksheetId)
	{
		this.worksheetId = worksheetId;
	}
	
	public String getIncidentId()
	{
		return incidentId;
	}
	public void setIncidentId(String incidentId)
	{
		this.incidentId = incidentId;
	}
	
	public String getNote()
	{
		return note;
	}
	public void setNote(String note)
	{
		this.note = note;
	}
	
	public String getActivityName()
	{
		return activityName;
	}
	public void setActivityName(String activityName)
	{
		this.activityName = activityName;
	}
	
	public String getCategory()
	{
		return category;
	}
	public void setCategory(String category)
	{
		this.category = category;
	}

	public String getPostedBy()
	{
		return postedBy;
	}
	public void setPostedBy(String postedBy)
	{
		this.postedBy = postedBy;
	}
	
	public String getComponentId()
    {
        return componentId;
    }
    public void setComponentId(String componentId)
    {
        this.componentId = componentId;
    }
    
    public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    
    public String getAuditMessage()
    {
        return auditMessage;
    }
    public void setAuditMessage(String auditMessage)
    {
        this.auditMessage = auditMessage;
    }

    public Boolean isModified()
    {
        return modified;
    }
    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }

    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }

    public String getSourceValue()
    {
        return sourceValue;
    }
    public void setSourceValue(String sourceValue)
    {
        this.sourceValue = sourceValue;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSourceAndValue()
    {
        return sourceAndValue;
    }
    public void setSourceAndValue(String sourceAndValue)
    {
        this.sourceAndValue = sourceAndValue;
    }
	
	public String getSir()
    {
        return sir;
    }
    public void setSir(String sir)
    {
        this.sir = sir;
    }
}
