/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

import java.io.Serializable;
import java.util.TreeSet;

public interface Query extends Serializable
{
    Suggest getSuggest();

    void setSuggest(Suggest suggest);

    void addSuggest(String suggest, int weight);

    String getSuggestion();

    int getSelectCount();

    void setSelectCount(int selectCount);

    void addSelectCount(int selectCount);

    //weight from the suggest
    int getWeight();

    long getSysUpdatedOn();

    void setSysUpdatedOn(long sysUpdatedOn);

    TreeSet<String> getSourceIds();

    void setSourceIds(TreeSet<String> sourceIds);

    Query addSourceId(String sourceId);
    
    String getSuggestFieldName();
}
