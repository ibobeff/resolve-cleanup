package com.resolve.search.model;

import com.resolve.util.StringUtils;

public class ExecutionSummary extends BaseIndexModel
{
    private static final long serialVersionUID = -8513782994085350970L;
    
    public static final String RBC_UNBILLABLE = "/$Unbillable";
    
    private String worksheetId;
    private String rbc;
    private String runbook;
    private String runbookParams;
    private String reference;
    private String hash;
    private String rbcSource;
    
    public ExecutionSummary()
    {
        super();
    }
    
    public ExecutionSummary(String sysId, String sysOrg)
    {
        super(sysId, StringUtils.isNotBlank(sysOrg) ? sysOrg : null);
    }
    
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    
    public String getRbc()
    {
        return rbc;
    }
    public void setRbc(String rbc)
    {
        this.rbc = rbc;
    }
    
    public String getRunbook()
    {
        return runbook;
    }
    public void setRunbook(String runbook)
    {
        this.runbook = runbook;
    }

    public String getRunbookParams()
    {
        return runbookParams;
    }
    public void setRunbookParams(String runbookParams)
    {
        this.runbookParams = runbookParams;
    }

    public String getReference()
    {
        return reference;
    }
    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getHash()
    {
        return hash;
    }
    public void setHash(String hash)
    {
        this.hash = hash;
    }
    
    public String getRbcSource()
    {
        return rbcSource;
    }
    public void setRbcSource(String rbcSource)
    {
        this.rbcSource = rbcSource;
    }
}
