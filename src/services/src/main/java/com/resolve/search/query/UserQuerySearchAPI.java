/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.query;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchException;
import com.resolve.search.model.query.AbstractQuery;
import com.resolve.search.model.query.UserQuery;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class UserQuerySearchAPI
{
    private static final SearchAPI<UserQuery> searchAPI = APIFactory.getUserQuerySearchAPI();

    /**
     * Find the user's query that the user
     * 
     * @param query
     * @param username
     * @return
     * @throws SearchException
     */
    public static UserQuery findByQueryAndUsername(String query, String type, String username) throws SearchException
    {
        UserQuery result = null;
        
        //at this point we do not care about the type "document", "automation", or "social" query
        QueryBuilder queryBuilder = QueryBuilders.boolQuery()
                        .must(QueryBuilders.matchQuery("query", query))
                        //.must(QueryBuilders.matchQuery("type", type))
                        .must(QueryBuilders.matchQuery("username", username));

                        
        QueryDTO queryDTO = new QueryDTO(0, 1);
        ResponseDTO<UserQuery> response = searchAPI.searchByQuery(queryDTO, queryBuilder, username);
        if(response.getRecords() != null && response.getRecords().size() > 0)
        {
            result = response.getRecords().get(0);
        }
        return result;
    }
    
    public ResponseDTO<AbstractQuery> getSearchQuerySuggestions(String indexName, String documentType, String fieldName, QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<AbstractQuery> responseDTO = new ResponseDTO<AbstractQuery>();

        if(StringUtils.isBlank(queryDTO.getSearchTerm()))
        {
            Log.log.warn("QueryDTO do not have searchTerm set, avoid calling empty!");
        }
        else
        {
            List<AbstractQuery> records = new ArrayList<AbstractQuery>();
            List<String> suggestions = searchAPI.getSuggestions(fieldName, queryDTO, username);
            for(String suggestion : suggestions)
            {
                //records.add(new AbstractQuery(suggestion, 1));
            }
            responseDTO.setRecords(records).setSuccess(true);
        }
        return responseDTO;
    }
}
