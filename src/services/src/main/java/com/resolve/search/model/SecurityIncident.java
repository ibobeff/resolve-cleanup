package com.resolve.search.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;

public class SecurityIncident implements Comparable<SecurityIncident>, Serializable {
	private static final long serialVersionUID = 1983098940963751780L;

	@JsonProperty("sys_id")
	private String sysId;

	@JsonProperty("sys_org")
	private String sysOrg;

	@JsonProperty("sys_updated_by")
	private String sysUpdatedBy;

	@JsonProperty("sys_updated_on")
	private Date sysUpdatedOn;

	@JsonProperty("sys_created_on")
	private Date sysCreatedOn;

	@JsonProperty("due_by")
	private Date dueBy;

	@JsonProperty("source_system")
	private String origin;

	@JsonProperty("closed_by")
	private String sysClosedBy;

	@JsonProperty("closed_on")
	private Date sysClosedOn;

	@JsonProperty("data_compromised")
	private String dataCompromised;

	@JsonProperty("sir_type")
	private String investigationType;

	private String sir;
	private String title;
	private String description;
	private String playbook;
	private String severity;
	private String owner;
	private String members;
	private String status;
	private Long sla;

	@JsonProperty("pbartifacts")
    private List<ArtifactLite> artifacts = new ArrayList<>(); 

	@JsonProperty("pbnotes")
    private List<NoteLite> notes = new ArrayList<>(); 

	@JsonProperty("automation_results")
	private List<AutomationResultLite> automationResults = new ArrayList<>(); 

	@JsonProperty("pbattachments")
    private List<AttachmentLite> attachments = new ArrayList<>(); 

	private Map<String, String> highlightFields = new HashMap<>();

    static class ArtifactLite implements Comparable<ArtifactLite>, Serializable {
		private static final long serialVersionUID = 6772616623147691720L;

		@JsonProperty("sys_id")
		private String sysId;

		@JsonProperty("sys_created_by")
		private String sysCreatedBy;

		@JsonProperty("sys_updated_by")
		private String sysUpdatedBy;

		private String name;
        private String value;
        private String source;
        private String sourceValue;
        
		public ArtifactLite() {
			super();
		}

        public String getSysId() {
			return sysId;
		}

		public void setSysId(String sysId) {
			this.sysId = sysId;
		}

		public String getSysCreatedBy() {
			return sysCreatedBy;
		}

		public void setSysCreatedBy(String sysCreatedBy) {
			this.sysCreatedBy = sysCreatedBy;
		}

		public String getSysUpdatedBy() {
			return sysUpdatedBy;
		}

		public void setSysUpdatedBy(String sysUpdatedBy) {
			this.sysUpdatedBy = sysUpdatedBy;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getSourceValue() {
			return sourceValue;
		}

		public void setSourceValue(String sourceValue) {
			this.sourceValue = sourceValue;
		}

		@Override
		public int compareTo(ArtifactLite o) {
			return this.equals(o) ? 0 : this.sysId.compareTo(o.sysId);
		}
    }
    
    static class NoteLite implements Comparable<NoteLite>, Serializable {
		private static final long serialVersionUID = -4452373955170570912L;

		@JsonProperty("sys_id")
		private String sysId;

		@JsonProperty("sys_created_by")
		private String sysCreatedBy;

		private String note;
        private String source;

        @JsonProperty("source_value")
        private String sourceValue;

        public NoteLite() {
			super();
		}

        public String getSysId() {
			return sysId;
		}

		public void setSysId(String sysId) {
			this.sysId = sysId;
		}

		public String getSysCreatedBy() {
			return sysCreatedBy;
		}

		public void setSysCreatedBy(String sysCreatedBy) {
			this.sysCreatedBy = sysCreatedBy;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getSourceValue() {
			return sourceValue;
		}

		public void setSourceValue(String sourceValue) {
			this.sourceValue = sourceValue;
		}

		@Override
		public int compareTo(NoteLite o) {
			return this.equals(o) ? 0 : this.sysId.compareTo(o.sysId);
		}
    }
    
    static class AutomationResultLite implements Comparable<AutomationResultLite>, Serializable {
		private static final long serialVersionUID = -5139574189312047522L;

		@JsonProperty("sys_id")
		private String sysId;

		@JsonProperty("sys_created_by")
		private String sysCreatedBy;

		private String summary;
        private String detail;
        private String target;
        private String esbaddr;
        private String completion;
        private String severity;
        private String condition;

		public AutomationResultLite() {
			super();
		}

        public String getSysId() {
			return sysId;
		}

		public void setSysId(String sysId) {
			this.sysId = sysId;
		}

		public String getSysCreatedBy() {
			return sysCreatedBy;
		}

		public void setSysCreatedBy(String sysCreatedBy) {
			this.sysCreatedBy = sysCreatedBy;
		}

		public String getSummary() {
			return summary;
		}

		public void setSummary(String summary) {
			this.summary = summary;
		}

		public String getDetail() {
			return detail;
		}

		public void setDetail(String detail) {
			this.detail = detail;
		}

		public String getTarget() {
			return target;
		}

		public void setTarget(String target) {
			this.target = target;
		}

		public String getEsbaddr() {
			return esbaddr;
		}

		public void setEsbaddr(String esbaddr) {
			this.esbaddr = esbaddr;
		}

		public String getCompletion() {
			return completion;
		}

		public void setCompletion(String completion) {
			this.completion = completion;
		}

		public String getSeverity() {
			return severity;
		}

		public void setSeverity(String severity) {
			this.severity = severity;
		}

		public String getCondition() {
			return condition;
		}

		public void setCondition(String condition) {
			this.condition = condition;
		}

		@Override
		public int compareTo(AutomationResultLite o) {
			return this.equals(o) ? 0 : this.sysId.compareTo(o.sysId);
		}
    }
    
    static class AttachmentLite implements Comparable<AttachmentLite>, Serializable {
		private static final long serialVersionUID = -6009935922559392088L;

		@JsonProperty("sys_id")
		private String sysId;

		@JsonProperty("sys_created_by")
		private String sysCreatedBy;

		@JsonProperty("malicious")
    	private Boolean isMalicious = false;

		private String description;
		private String name;
    	private String source;

    	@JsonProperty("source_value")
        private String sourceValue;

		public AttachmentLite() {
			super();
		}

		public String getSysId() {
			return sysId;
		}

		public void setSysId(String sysId) {
			this.sysId = sysId;
		}

		public String getSysCreatedBy() {
			return sysCreatedBy;
		}

		public void setSysCreatedBy(String sysCreatedBy) {
			this.sysCreatedBy = sysCreatedBy;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Boolean getIsMalicious() {
			return isMalicious;
		}

		public void setIsMalicious(Boolean isMalicious) {
			this.isMalicious = isMalicious;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getSourceValue() {
			return sourceValue;
		}

		public void setSourceValue(String sourceValue) {
			this.sourceValue = sourceValue;
		}

		@Override
		public int compareTo(AttachmentLite o) {
			return this.equals(o) ? 0 : this.sysId.compareTo(o.sysId);
		}
    }

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getSysOrg() {
		return sysOrg;
	}

	public void setSysOrg(String sysOrg) {
		this.sysOrg = sysOrg;
	}

	public String getSysUpdatedBy() {
		return sysUpdatedBy;
	}

	public void setSysUpdatedBy(String sysUpdatedBy) {
		this.sysUpdatedBy = sysUpdatedBy;
	}

	public Date getSysUpdatedOn() {
		return sysUpdatedOn;
	}

	public void setSysUpdatedOn(Date sysUpdatedOn) {
		this.sysUpdatedOn = sysUpdatedOn;
	}

	public Date getSysCreatedOn() {
		return sysCreatedOn;
	}

	public void setSysCreatedOn(Date sysCreateOn) {
		this.sysCreatedOn = sysCreateOn;
	}

	public String getSir() {
		return sir;
	}

	public void setSir(String sir) {
		this.sir = sir;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlaybook() {
		return playbook;
	}

	public void setPlaybook(String playbook) {
		this.playbook = playbook;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public Date getDueBy() {
		return dueBy;
	}

	public void setDueBy(Date dueBy) {
		this.dueBy = dueBy;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getMembers() {
		return members;
	}

	public void setMembers(String members) {
		this.members = members;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getSla() {
		return sla;
	}

	public void setSla(Long sla) {
		this.sla = sla;
	}

	public String getSysClosedBy() {
		return sysClosedBy;
	}

	public void setSysClosedBy(String sysClosedBy) {
		this.sysClosedBy = sysClosedBy;
	}

	public Date getSysClosedOn() {
		return sysClosedOn;
	}

	public void setSysClosedOn(Date sysClosedOn) {
		this.sysClosedOn = sysClosedOn;
	}

	public String getDataCompromised() {
		return dataCompromised;
	}

	public void setDataCompromised(String dataCompromised) {
		this.dataCompromised = dataCompromised;
	}

	public List<ArtifactLite> getArtifacts() {
		return artifacts;
	}

	public void setArtifacts(List<ArtifactLite> artifacts) {
		this.artifacts = artifacts;
	}

	public List<NoteLite> getNotes() {
		return notes;
	}

	public void setNotes(List<NoteLite> notes) {
		this.notes = notes;
	}

	public List<AutomationResultLite> getAutomationResults() {
		return automationResults;
	}

	public void setAutomationResults(List<AutomationResultLite> automationResults) {
		this.automationResults = automationResults;
	}

	public List<AttachmentLite> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<AttachmentLite> attachments) {
		this.attachments = attachments;
	}

	public String getInvestigationType() {
		return investigationType;
	}

	public void setInvestigationType(String investigationType) {
		this.investigationType = investigationType;
	}

	public Map<String, String> getHighlightFields() {
		return highlightFields;
	}

	public void setHighlightFields(Map<String, String> highlightFields) {
		this.highlightFields = highlightFields;
	}

	@Override
	public int compareTo(SecurityIncident o) {
		return this.equals(o) ? 0 : this.sysId.compareTo(o.sysId);
	}
}
