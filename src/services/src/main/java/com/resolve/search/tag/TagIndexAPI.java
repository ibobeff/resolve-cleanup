/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.tag;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.Tag;
import com.resolve.search.social.SocialIndexAPI;
import com.resolve.util.DateUtils;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class TagIndexAPI
{
    private static IndexAPI indexAPI = APIFactory.getTagIndexAPI();
    
    private static final Indexer<IndexData<Tag>> indexer = TagIndexer.getInstance();

    /**
     * Called by internal Resolve component to synchronize Tags.
     * Tag objects must have sysId in it otherwise those tags
     * will be rejected.
     *
     * @param tags : List of {@link Tag} needs to be synchronized
     * @param username : String representing user name who is invoking this API.
     * 
     * @throws SearchException
     */
    public static void syncTags(final List<Tag> tags, final String username) throws SearchException
    {
        if (tags == null || tags.size() == 0)
        {
            Log.log.warn("TagIndexAPI.syncTags() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            StringBuilder message = new StringBuilder();
            for(Tag tag : tags)
            {
                if(StringUtils.isBlank(tag.getSysId()))
                {
                    message.append(tag.getName());
                    message.append(",");
                }
            }
            if(message.length() > 0)
            {
                throw new SearchException("Following tag has no sysId configured, set them and call syncTags again.");
            }
            else
            {
                IndexData<Tag> indexData = new IndexData<Tag>(tags, OPERATION.INDEX, username);
                indexer.enqueue(indexData);
            }
        }
    }

    /**
     * Indexes or updates an existing tag.
     *
     * @param tag
     * @throws SearchException
     */
    public static Tag indexTag(final Tag tag, final String username) throws SearchException
    {
        Tag result = null;

        // verify if the tag already exists
        Tag existingTag = TagSearchAPI.findTagByName(tag.getName(), username);
        if (existingTag != null && !existingTag.getSysId().equals(tag.getSysId()))
        {
            // throw new SearchException("Tag already exists: " + tag.getName());
            Log.log.debug("Tag '" + tag.getName() + "' already exist.");
            tag.setSysId(existingTag.getSysId());
        }

        String sysId = null;
        
        String tagName = tag.getName();
        if(tagName != null && tagName.startsWith("#") && tagName.length() > 2)
        {
            tagName = tagName.substring(1);
        }
        
        if (StringUtils.isBlank(tag.getSysId()))
        {
            // new
            Tag newTag = tag;
            sysId = Guid.getGuid();
            newTag = SearchUtils.prepareTag(tag.getSysOrg(), tagName, username);
            newTag.setSysId(sysId);
            newTag.setDescription(tag.getDescription());
            newTag.repalceSpacialChars();
            indexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, newTag, true, username);
        }
        else
        {
            // Tag tmpTag = tag;
            sysId = tag.getSysId();
            Tag tmpTag = TagSearchAPI.findTagById(tag.getSysId(), username);
            if(tmpTag == null)
            {
                Log.log.info("Didn't find Tag by sysId:" + sysId);
            }
            else
            {
                String oldName = tmpTag.getName();
                String newName = tag.getName();
                int weight = tmpTag.getSuggest().getWeight();
                tmpTag.setName(tagName);
                tmpTag.addWeight(weight);
                tmpTag.setDescription(tag.getDescription());
                tmpTag.setSysUpdatedBy(username);
                tmpTag.setSysUpdatedOn(DateUtils.GetUTCDateLong());
                tmpTag.addWeight(1);
                indexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, tmpTag, true, username);
    
                // we modify only the Hashtags in social post/comments
                if (newName.startsWith("#") && !tagName.equalsIgnoreCase(oldName))
                {
                    SocialIndexAPI.replaceTagName(oldName, newName, username);
                }
            }
        }

        result = TagSearchAPI.findTagById(sysId, username);

        return result;
    }

    public static void indexTags(final Collection<Tag> tags, final String username) throws SearchException
    {
        if (tags == null || tags.isEmpty())
        {
            Log.log.debug("The tags must be provided, check the calling code why is it like that.");
        }
        else
        {
            for (Tag tag : tags)
            {
                indexTag(tag, username);
            }
        }
    }

    public static void deleteTag(final String sysId, String username) throws SearchException
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.debug("The sysId must be provided, check the calling code why is it like that.");
        }
        else
        {
            indexAPI.deleteDocumentById(sysId, username);
        }
    }

    public static void deleteTags(final Collection<String> sysIds, String username) throws SearchException
    {
        if (sysIds == null || sysIds.isEmpty())
        {
            throw new SearchException("The sysIds list is null/empty, check the calling code why is it like that.");
        }
        else
        {
            indexAPI.deleteDocumentByIds(sysIds, username);
        }
    }

    public static void verifyTag(String contents, String username) throws SearchException
    {
        Set<String> tags = SearchUtils.findTags(contents);
        for (String tagName : tags)
        {
            Tag tag = TagSearchAPI.findTagByName(tagName, username);
            if (tag == null)
            {
                // we need to create this tag.
                tag = SearchUtils.prepareTag(null, tagName, username);
            }
            else
            {
                //add the total appearance for better suggestion
                tag.addWeight(1);
            }
            indexTag(tag, username);
        }
    }
}
