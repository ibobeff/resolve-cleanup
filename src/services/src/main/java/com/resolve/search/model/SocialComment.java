/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;
import java.util.TreeSet;

import com.resolve.util.StringUtils;

/**
 * This class represents a comments posted to a social post. It has no value
 * without the postId.
 */
public class SocialComment extends BaseIndexModel implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String postId;
    // helps counting for stats.
    private String postAuthorUsername;
    // comment's author username
    private String commentAuthorSysId;
    private String commentAuthorUsername;
    private String commentAuthorDisplayName;
    private String content;
    private boolean isAnswer = false;

    private long numberOfLikes;

    private TreeSet<String> likedUsernames = new TreeSet<String>();

    // TreeSet to keep in order
    private TreeSet<String> readRoles = new TreeSet<String>(); 
    private TreeSet<String> postRoles = new TreeSet<String>();
    private TreeSet<String> editRoles = new TreeSet<String>();

    public SocialComment()
    {
        super();
        // blank constructor, required for JSON serialization.
    }

    public SocialComment(String postId, String postAuthorUsername, String commentAuthorUsername, String content)
    {
        this();
        this.postId = postId;
        this.postAuthorUsername = postAuthorUsername;
        this.commentAuthorUsername = commentAuthorUsername;
        this.content = content;
    }

    public String getPostId()
    {
        return postId;
    }

    public void setPostId(String postId)
    {
        this.postId = postId;
    }

    public String getPostAuthorUsername()
    {
        return postAuthorUsername;
    }

    public void setPostAuthorUsername(String postAuthorUsername)
    {
        this.postAuthorUsername = postAuthorUsername;
    }

    public String getCommentAuthorSysId()
    {
        return commentAuthorSysId;
    }

    public void setCommentAuthorSysId(String commentAuthorSysId)
    {
        this.commentAuthorSysId = commentAuthorSysId;
    }

    public String getCommentAuthorUsername()
    {
        return commentAuthorUsername;
    }

    public void setCommentAuthorUsername(String commentAuthorUsername)
    {
        this.commentAuthorUsername = commentAuthorUsername;
    }

    public String getCommentAuthorDisplayName()
    {
        return commentAuthorDisplayName;
    }

    public void setCommentAuthorDisplayName(String commentAuthorDisplayName)
    {
        this.commentAuthorDisplayName = commentAuthorDisplayName;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public boolean isAnswer()
    {
        return isAnswer;
    }

    public void setAnswer(boolean isAnswer)
    {
        this.isAnswer = isAnswer;
    }

    public TreeSet<String> getLikedUsernames()
    {
        return likedUsernames;
    }

    public void setLikedUsernames(TreeSet<String> likedUsernames)
    {
        this.likedUsernames = likedUsernames;
    }

    public void addLikedUsername(String username)
    {
        this.likedUsernames.add(username);
    }

    public void removeLikedUsername(String username)
    {
        this.likedUsernames.remove(username);
    }

    public long getNumberOfLikes()
    {
        return numberOfLikes;
    }

    public void setNumberOfLikes(long numberOfLikes)
    {
        this.numberOfLikes = numberOfLikes;
    }

    public TreeSet<String> getReadRoles()
    {
        return readRoles;
    }

    public void setReadRoles(TreeSet<String> readRoles)
    {
        this.readRoles = readRoles;
    }

    public void addReadRole(String role)
    {
        if (StringUtils.isNotBlank(role))
        {
            this.readRoles.add(role);
        }
    }

    public TreeSet<String> getPostRoles()
    {
        return postRoles;
    }

    public void setPostRoles(TreeSet<String> postRoles)
    {
        this.postRoles = postRoles;
    }

    public TreeSet<String> getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(TreeSet<String> editRoles)
    {
        this.editRoles = editRoles;
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();

        toString.append("SocialComment [postId=" + postId + ", postAuthorUsername=" + postAuthorUsername + ", commentAuthorUsername=" + commentAuthorUsername + ", content=" + content + "]\n");
        toString.append("\n");
        toString.append("  --> Liked By:");
        for (String username : getLikedUsernames())
        {
            toString.append(username + ",");
        }
        return toString.toString();
    }
}
