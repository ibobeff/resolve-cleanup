/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.TreeSet;

import com.resolve.search.SearchUtils;

/**
 * This class hold various user related information for indexing and searching.
 * Object of this class should have at least sysId and username to call it a
 * proper UserInfo object. If one of these properties are missing there may be
 * some unpredictable runtime error.
 */
public class UserInfo
{
    private final String sysId;
    private final String sysOrg;
    private final String username;
    private boolean isAdmin;

    private TreeSet<String> roles = new TreeSet<String>();

    /*public UserInfo()
    {
        super();
    }*/

    public UserInfo(String sysId, String sysOrg, String username)
    {
        //this();
        this.sysId = sysId;
        this.sysOrg = sysOrg;
        this.username = username;
    }

    public UserInfo(String sysId, String sysOrg, String username, String readRoles, boolean isAdmin)
    {
        this(sysId, sysOrg, username);
        this.isAdmin = isAdmin;
        //sometime (graphdb) roles are stored with space delimited
        //however in database they're stored comma separated
        if(readRoles != null)
        {
            if(readRoles.contains(","))
            {
                this.roles = SearchUtils.parseRoles(readRoles, ",");
            }
            else
            {
                this.roles = SearchUtils.parseRoles(readRoles, " ");
            }
        }
    }

    public String getSysId()
    {
        return sysId;
    }

    public String getSysOrg()
    {
        return sysOrg;
    }

    public String getUsername()
    {
        return username;
    }

    public boolean isAdmin()
    {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public TreeSet<String> getRoles()
    {
        return roles;
    }

    public void setRoles(TreeSet<String> roles)
    {
        this.roles = roles;
    }
}
