package com.resolve.search;

import java.util.Map;

public class MetricAggregationAPI
{
    private static SearchService instance;

    MetricAggregationAPI()
    {
        init();
    }
    
    private static void init()
    {
        if (instance==null)
        {
            synchronized (MetricAggregationAPI.class)
            {
                if (instance==null)
                {
                    instance = new SearchService();
                }
            }
        }
    }

    public static Map<String, Double> jvmThreadCountHistorgram(String[] fieldNames, String[] values, String intervalType, int intervalValue)
    {
        return jvmThreadCountHistorgram(fieldNames, values, intervalType, intervalValue, null, null);
    }
    
    public static Map<String, Double> jvmThreadCountHistorgram(String[] fieldNames, String[] values, String intervalType, int intervalValue, Long tsMin, Long tsMax)
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_JVM, SearchConstants.INDEX_NAME_METRIC_JVM, fieldNames, values, intervalType, intervalValue, "threadCount", tsMin, tsMax);
    }
    
    public static Map<String, Double> jvmFreeMemoryHistorgram(String[] fieldNames, String[] values, String intervalType, int intervalValue, Long tsMin, Long tsMax)
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_JVM, SearchConstants.INDEX_NAME_METRIC_JVM, fieldNames, values, intervalType, intervalValue, "freeMem", tsMin, tsMax);
    }

    public static Map<String, Double> runbookMetricHistorgram(String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricFieldName, Long tsMin, Long tsMax )
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_RUNBOOK, SearchConstants.INDEX_NAME_METRIC_RUNBOOK, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricFieldName, tsMin, tsMax);
    }
    
    public static Map<String, Double> transactionMetricHistorgram(String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricFieldName, Long tsMin, Long tsMax )
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_TRANSACTION, SearchConstants.INDEX_NAME_METRIC_TRANSACTION, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricFieldName, tsMin, tsMax);
    }

    public static Map<String, Double> cpuMetricHistorgram(String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricFieldName, Long tsMin, Long tsMax )
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_CPU, SearchConstants.INDEX_NAME_METRIC_CPU, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricFieldName, tsMin, tsMax);
    }

    public static Map<String, Double> jmsQueueMetricHistorgram(String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricFieldName, Long tsMin, Long tsMax )
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_JMS_QUEUE, SearchConstants.INDEX_NAME_METRIC_JMS_QUEUE, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricFieldName, tsMin, tsMax);
    }

    public static Map<String, Double> usersMetricHistorgram(String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricFieldName, Long tsMin, Long tsMax )
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_USERS, SearchConstants.INDEX_NAME_METRIC_USERS, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricFieldName, tsMin, tsMax);
    }

    public static Map<String, Double> latencyMetricHistorgram(String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricFieldName, Long tsMin, Long tsMax )
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_LATENCY, SearchConstants.INDEX_NAME_METRIC_LATENCY, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricFieldName, tsMin, tsMax);
    }

    public static Map<String, Double> actionTaskMetricHistorgram(String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricFieldName, Long tsMin, Long tsMax )
    {
        return metricAggregation(SearchConstants.INDEX_NAME_METRIC_ACTIONTASK, SearchConstants.INDEX_NAME_METRIC_ACTIONTASK, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricFieldName, tsMin, tsMax);
    }

    public static Map<String, Double> metricAggregation(String index, String idxType, String[] fieldNames, String[] values, String intervalType, int intervalValue, String metricField, Long tsMin, Long tsMax)
    {
        init();
        return instance.metricAggregation(index, idxType, fieldNames, values, intervalType, intervalValue, metricField, tsMin, tsMax);
    }
    
    public static Map<String, Double> metricAggregation(String index, String idxType, String[] fieldNames, String[] values, String intervalType, int intervalValue, String metricField)
    {
        return instance.metricAggregation(index, idxType, fieldNames, values, intervalType, intervalValue, metricField, null, null);
    }
    
    

}
