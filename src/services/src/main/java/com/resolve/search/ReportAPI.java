/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.    
 *
 ******************************************************************************/
package com.resolve.search;

import static org.elasticsearch.search.aggregations.AggregationBuilders.avg;
import static org.elasticsearch.search.aggregations.AggregationBuilders.dateHistogram;
import static org.elasticsearch.search.aggregations.AggregationBuilders.histogram;
import static org.elasticsearch.search.aggregations.AggregationBuilders.sum;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.global.Global;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.ExtendedBounds;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalDateHistogram;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalHistogram;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Order;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.avg.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.InternalAvg;
import org.elasticsearch.search.aggregations.metrics.sum.InternalSum;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.hibernate.query.Query;
import org.joda.time.DateTime;

import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.model.UserInfo;
import com.resolve.search.model.WikiDocument;
import com.resolve.services.util.Constants;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

/**
 * This class provides data for various Resolve reports such as KM reports.
 *
 */
public class ReportAPI
{
    private static Set<String> NAMESPACE_EXCLUSION_SET = new HashSet<String>();
    private static Map<String, DateHistogramInterval> jvmIntervalMap = new HashMap<String, DateHistogramInterval>();
    
    private static final SimpleDateFormat SDF = new SimpleDateFormat ("yyyy-MM-dd'T'hh:mm:ss");

    static
    {
        if(SearchAdminAPI.getSearchConfig() != null)
        {
            String excludedNamespaces = SearchAdminAPI.getSearchConfig().getProperties().get("dashboard.namespace.exclude");
            if(StringUtils.isNotBlank(excludedNamespaces))
            {
                Log.log.debug("Found following namespaces to be excluded: " + excludedNamespaces);
                excludedNamespaces = excludedNamespaces.toLowerCase();
                String[] namespaces = StringUtils.stringToArray(excludedNamespaces.toLowerCase(), ",");
                for(String namespace : namespaces)
                {
                    NAMESPACE_EXCLUSION_SET.add(namespace);
                }
            }
        }
        
        jvmIntervalMap.put("5m", DateHistogramInterval.minutes(5));
        jvmIntervalMap.put("1h", DateHistogramInterval.hours(1));
        jvmIntervalMap.put("1d", DateHistogramInterval.hours(24));
    }

    private static ArrayList<String> getAggregation(final String aggregationField, final SearchResponse response, final int size)
    {
        ArrayList<String> result = new ArrayList<String>(); 
        if(size > 0)
        {
            StringTerms aggregationTerms = response.getAggregations().get(aggregationField);
            if(aggregationTerms != null)
            {
                for(Bucket aggregationBucket : aggregationTerms.getBuckets())
                {
                    if(StringUtils.isNotBlank(aggregationBucket.getKeyAsString()))
                    {
                        result.add(aggregationBucket.getKeyAsString());
                    }
                }
            }
        }
        return result;
    }

    /**
     * This API provides data for the following reports.
     * 
     * 1. Created Age Report
     * 2. Updated Age Report
     * 3. Reviewed Age Report
     *  
     * @param queryDTO
     * @param userField
     * @param dateField
     * @param userInfo
     * @return
     */
    public static ResponseDTO<Map<Object, Object>> wikiAgeAnalysis(final QueryDTO queryDTO, final String userField, final String dateField, final UserInfo userInfo)
    {
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object,Object>>();
        
        try
        {
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            queryBuilder.must(QueryBuilders.termQuery("hidden", false));
            queryBuilder.must(QueryBuilders.termQuery("deleted", false));
            if(NAMESPACE_EXCLUSION_SET != null)
            {
                queryBuilder.mustNot(QueryBuilders.termsQuery("namespace.exact", NAMESPACE_EXCLUSION_SET));
            }

            //List<FilterBuilder> filterBuilders = new LinkedList<FilterBuilder>();
            //StringBuilder nullCheckScript = new StringBuilder();
            //nullCheckScript.append("_source.").append(dateField).append(" != null && _source.").append(dateField).append(" > 0");
            //FilterBuilder nullCheckScriptFilter = FilterBuilders.scriptFilter(nullCheckScript.toString());
            //filterBuilders.add(nullCheckScriptFilter);

            QueryBuilder existsFilter = new ExistsQueryBuilder(dateField);
            QueryBuilder greaterThanFilter = QueryBuilders.rangeQuery(dateField).gt(0L);
            QueryBuilder nullCheckScriptFilter = new BoolQueryBuilder().filter(existsFilter).filter(greaterThanFilter); 
            //FilterBuilder nullCheckScriptFilter = FilterBuilders.andFilter(existsFilter, greaterThanFilter);

            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
            int limit = queryDTO.getLimit();
            int start = queryDTO.getStart();
            searchRequestBuilder.setFrom(start);
            if (limit > 0)
            {
                searchRequestBuilder.setSize(limit);
            }

            //add global aggregations
            searchRequestBuilder.addAggregation(terms("type").field("type") )
                .addAggregation(terms("ns").field("namespace.raw") .order(Order.term(true)))
                .addAggregation(terms("user").field(userField) .order(Order.term(true)));

            //searchRequestBuilder.setFetchSource(true);
            String selectFields = StringUtils.isBlank(queryDTO.getSelectColumns()) ? "*" : queryDTO.getSelectColumns();
            String excludeFields = StringUtils.isBlank(queryDTO.getExcludeColumns()) ? "" : queryDTO.getExcludeColumns();
            searchRequestBuilder.setFetchSource(selectFields, excludeFields);
            //add the computed field
            Map<String, Object> scriptParams = new LinkedHashMap<String, Object>();
            long currentTimestamp = DateUtils.GetUTCDateLong();
            scriptParams.put("currentTimestamp", currentTimestamp);
            
            //old non sandboxed script groovy, which is disabled by default.
            //String script = "((((currentTimestamp - _source." + dateField + ")/1000)/60)/60)/24";
            //searchRequestBuilder.addScriptField("age", script, scriptParams);
            
            //this is the new "expression" sandboxed script which is available by default
            String script = "((((currentTimestamp - doc['" + dateField + "'].value)/1000)/60)/60)/24";
            searchRequestBuilder.addScriptField("age",  new Script(ScriptType.INLINE, "expression", script, scriptParams));  //script) addScriptField("age", "expression", script, scriptParams);
            
            searchRequestBuilder.setPostFilter(nullCheckScriptFilter);
            searchRequestBuilder.setQuery(queryBuilder);
            
            for(QuerySort querySort : queryDTO.getSortItems())
            {
                SortOrder sortOrder = SortOrder.ASC;
                boolean asc = querySort.getDirection() == QuerySort.SortOrder.ASC;
                if (!asc)
                {
                    sortOrder = SortOrder.DESC;
                }
                searchRequestBuilder.addSort(querySort.getProperty(), sortOrder);
            }

            if(Log.log.isDebugEnabled() && searchRequestBuilder != null)
            {
                System.out.println(searchRequestBuilder.toString());
                Log.log.debug("Query: " + searchRequestBuilder.toString());
            }
            
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());
            
            if(Log.log.isDebugEnabled() && response != null)
            {
                Log.log.debug("Response: " + response.toString());
            }

            List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
            for (SearchHit hit : response.getHits())
            {
                try
                {
                    Map<String, Object> record = new TreeMap<String, Object>();
                    for(SearchHitField field : hit.getFields().values())
                    {
                        record.put(field.getName(),  field.getValue());
                    }
                    record.putAll(hit.getSource());

                    records.add(record);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
            }

            Map<Object, Object> data = new HashMap<Object, Object>();
            data.put("data", records);
            data.put("types", getAggregation("type", response, records.size()));
            data.put("namespaces", getAggregation("ns", response, records.size()));
            data.put("users", getAggregation("user", response, records.size()));
            result.setData(data);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }

        return result;
    }

    /**
     * Provides data for the following KM reports.
     * 
     * 1. Created Reports
     * 2. Updated Reports
     * 3. Reviewed Reports
     *   
     * @param queryDTO set interval with one of second, minute, hour, day, month, quarter, year
     * @param userField example: sysCreatedBy, sysUpdatedBy, lastReviewedBy
     * @param dateField example: sysCreatedDt, sysUpdatedDt, lastReviewedDt
     * @param userInfo
     * @return 
     */
    public static ResponseDTO<Map<Object, Object>> wikiDateAnalysis(final QueryDTO queryDTO, final String userField, final String dateField, final String lowerBound, final String upperBound, final UserInfo userInfo)
    {
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object,Object>>();
        
        try
        {
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            queryBuilder.must(QueryBuilders.termQuery("hidden", false));
            queryBuilder.must(QueryBuilders.termQuery("deleted", false));
            if(NAMESPACE_EXCLUSION_SET != null)
            {
                queryBuilder.mustNot(QueryBuilders.termsQuery("namespace.exact", NAMESPACE_EXCLUSION_SET));
            }

            //List<FilterBuilder> filterBuilders = new LinkedList<FilterBuilder>();
            //StringBuilder nullCheckScript = new StringBuilder();
            //nullCheckScript.append("_source.").append(dateField).append(" != null && _source.").append(dateField).append(" > 0");
            //FilterBuilder nullCheckScriptFilter = FilterBuilders.scriptFilter(nullCheckScript.toString());
            //filterBuilders.add(nullCheckScriptFilter);
            //FilterBuilder andFilter = FilterBuilders.andFilter(filterBuilders.toArray(new FilterBuilder[filterBuilders.size()]));
          
            QueryBuilder existsFilter = new ExistsQueryBuilder(dateField);
            QueryBuilder greaterThanFilter = QueryBuilders.rangeQuery(dateField).gt(0L);
            QueryBuilder nullCheckScriptFilter = new BoolQueryBuilder().filter(existsFilter).filter(greaterThanFilter);
            queryBuilder.filter(nullCheckScriptFilter);
            
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
            //no need to return document hence the type is count
//            searchRequestBuilder.setSearchType(SearchType.COUNT);
            searchRequestBuilder.setSize(0);
            
            searchRequestBuilder.setQuery(queryBuilder);
//            searchRequestBuilder.setPostFilter(nullCheckScriptFilter);
            
            //add global aggregations
            searchRequestBuilder.addAggregation(terms("type").field("type") )
                .addAggregation(terms("ns").field("namespace.raw") .order(Order.term(true)))
                .addAggregation(terms("user").field(userField) .order(Order.term(true)));

            //main aggregation
            DateHistogramInterval interval = new DateHistogramInterval(queryDTO.getInterval());
            searchRequestBuilder.addAggregation(dateHistogram("dateAnalysis").field(dateField).dateHistogramInterval(interval).minDocCount(0).extendedBounds(new ExtendedBounds(lowerBound, upperBound)));
            
            if(Log.log.isDebugEnabled() && searchRequestBuilder != null)
            {
                Log.log.debug("Query: " + searchRequestBuilder.toString());
            }

            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());

            if(Log.log.isDebugEnabled() && response != null)
            {
                Log.log.debug("Response: " + response.toString());
            }

            int totalRecords = 0;
            Map<Long, Long> records = new TreeMap<Long, Long>();
            InternalDateHistogram histogram = response.getAggregations().get("dateAnalysis");
            if(histogram != null)
            {
                List<InternalDateHistogram.Bucket> buckets = histogram.getBuckets();
                totalRecords = buckets.size();
                if(buckets != null)
                {
                    //int start = queryDTO.getStart();
                    //int end = start + (queryDTO.getLimit() - 1);
                    //int runningTotal = 0;
                    for(org.elasticsearch.search.aggregations.bucket.histogram.Histogram.Bucket bucket : buckets)
                    {
//                        if(runningTotal >= start && runningTotal <= end)
//                        {
//                            records.put(bucket.getKeyAsNumber().longValue(), bucket.getDocCount());
//                        }
//                        runningTotal++;
                        records.put(((DateTime) bucket.getKey()).getMillis(), bucket.getDocCount());
                    }
                }
            }         
            
            Map<Object, Object> data = new HashMap<Object, Object>();
            data.put("data", records);
            data.put("types", getAggregation("type", response, records.size()));
            data.put("namespaces", getAggregation("ns", response, records.size()));
            data.put("users", getAggregation("user", response, records.size()));
            
            result.setTotal(totalRecords);
            
            result.setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }

        return result;
    }
    
    /**
     * Provides data for following KM reports.
     * 
     * 1. Created by Author Reports:
     *      aggregationField=sysCreatedBy
     *      subAggregationField=namespace
     *      userField=sysCreatedBy
     *   
     * 2. Created by Namespace Reports:
     *      aggregationField=namespace
     *      subAggregationField=sysCreatedBy
     *      userField=sysCreatedBy
     *   
     * 3. Updated by Author Reports:
     *      aggregationField=sysUpdatedBy
     *      subAggregationField=namespace
     *      userField=sysUpdatedBy
     *   
     * 4. Updated by Namespace Reports:
     *      aggregationField=namespace
     *      subAggregationField=sysUpdatedBy
     *      userField=sysUpdatedBy
     *   
     * 5. Reviewed by Author Reports:
     *      aggregationField=lastReviewedBy
     *      subAggregationField=namespace
     *      userField=lastReviewedBy
     *   
     * 6. Reviewed by Namespace Reports:
     *      aggregationField=namespace
     *      subAggregationField=sysCreatedBy
     *      userField=sysCreatedBy
     *   
     * @param queryDTO
     * @param aggregationField example: sysCreatedBy, sysUpdatedBy, lastReviewedBy
     * @param subAggregationField example: namespace
     * @param userInfo
     * @return 
     */
    public static ResponseDTO<Map<Object, Object>> wikiCountAnalysis(final QueryDTO queryDTO, final String aggregationField, final String subAggregationField, final String userField, final UserInfo userInfo)
    {
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object,Object>>();
        
        try
        {
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            queryBuilder.must(QueryBuilders.termQuery("hidden", false));
            queryBuilder.must(QueryBuilders.termQuery("deleted", false));
            if(NAMESPACE_EXCLUSION_SET != null)
            {
                queryBuilder.mustNot(QueryBuilders.termsQuery("namespace.exact", NAMESPACE_EXCLUSION_SET));
            }
            
            List<QueryBuilder> filterBuilders = new LinkedList<QueryBuilder>();
            
            if(StringUtils.isNotBlank(aggregationField))
            {
                //StringBuilder nullCheckScript = new StringBuilder();
                //nullCheckScript.append("_source.").append(aggregationField).append(" != null");
                //FilterBuilder nullCheckScriptFilter = FilterBuilders.scriptFilter(nullCheckScript.toString());
                
                QueryBuilder nullCheckScriptFilter = new ExistsQueryBuilder(aggregationField);
                filterBuilders.add(nullCheckScriptFilter);
            }

            if(StringUtils.isNotBlank(subAggregationField))
            {
                //StringBuilder nullCheckScript = new StringBuilder();
                //nullCheckScript.append("_source.").append(subAggregationField).append(" != null");
                //FilterBuilder nullCheckScriptFilter = FilterBuilders.scriptFilter(nullCheckScript.toString());
                
                QueryBuilder nullCheckScriptFilter = new ExistsQueryBuilder(subAggregationField);
                filterBuilders.add(nullCheckScriptFilter);
            }
            
//            QueryBuilder andFilter = QueryBuilders.andFilter(filterBuilders.toArray(new FilterBuilder[filterBuilders.size()]));
            BoolQueryBuilder andFilter = new BoolQueryBuilder();
            for (QueryBuilder qb : filterBuilders)
            {
            	andFilter.filter(qb);
            }
            
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
            //no need to return document hence the type is count
//            searchRequestBuilder.setSearchType(SearchType.COUNT);
            searchRequestBuilder.setSize(0);
          
//            searchRequestBuilder.setPostFilter(andFilter);
            queryBuilder.filter(andFilter);
            searchRequestBuilder.setQuery(queryBuilder);

            //add global aggregations
            searchRequestBuilder.addAggregation(terms("type").field("type") )
                .addAggregation(terms("ns").field("namespace.raw") .order(Order.term(true)))
                .addAggregation(terms("user").field(userField) .order(Order.term(true)));

            //main aggregation
            TermsAggregationBuilder termsAggregation = terms(aggregationField).field(aggregationField) ;
            if(StringUtils.isNotBlank(subAggregationField))
            {
                termsAggregation.subAggregation(terms(subAggregationField).field(subAggregationField) );
            }
            
            searchRequestBuilder.addAggregation(termsAggregation);

            if(Log.log.isDebugEnabled() && searchRequestBuilder != null)
            {
                Log.log.debug("Query: " + searchRequestBuilder.toString());
            }

            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());

            if(Log.log.isDebugEnabled() && response != null)
            {
                Log.log.debug("Response: " + response.toString());
            }

            Map<String, Object> records = new TreeMap<String, Object>();
            
            StringTerms mainAggregation = response.getAggregations().get(aggregationField);
            if(mainAggregation != null)
            {
                for(Bucket mainBucket : mainAggregation.getBuckets())
                {
                    //System.out.println(bucket.);
                    StringTerms subAggregation = mainBucket.getAggregations().get(subAggregationField);
                    Map<String, Long> subRecord = new LinkedHashMap<String, Long>();
                    if(subAggregation != null)
                    {
                        for(Bucket subBucket : subAggregation.getBuckets())
                        {
                            subRecord.put(subBucket.getKeyAsString(), subBucket.getDocCount());
                        }
                    }
                    records.put(mainBucket.getKeyAsString(), subRecord);
                }
            }
            
            Map<Object, Object> data = new HashMap<Object, Object>();
            data.put("data", records);
            data.put("types", getAggregation("type", response, records.size()));
            data.put("namespaces", getAggregation("ns", response, records.size()));
            data.put("users", getAggregation("user", response, records.size()));
            result.setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }

        return result;
    }
    
    /**
     * Used for the following KM reports.
     * 
     * 1. Top Rated report
     *   - send a sort item with the field="rating" and order=ASC. start=0 and limit=10 defines N (0-10 for top 10).
     * 2. Bottom Rated report
     *   - send a sort item with the field="rating" and order=DESC. start=0 and limit=10 defines N (0-10 for bottom 10)
     * 
     * 3. Top Viewed report
     *   - send a sort item with the field="clickCount" and order=ASC. start=0 and limit=10 defines N (0-10 for top 10).
     * 4. Bottom Viewed report
     *   - send a sort item with the field="clickCount" and order=DESC. start=0 and limit=10 defines N (0-10 for bottom 10)
     * 
     * @param queryDTO
     * @param userField
     * @param userInfo
     * @return
     */
    public static ResponseDTO<Map<String, Object>> wikiTopAnalysis(final QueryDTO queryDTO, final UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        try
        {
            BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
            queryBuilder.must(QueryBuilders.termQuery("hidden", false));
            queryBuilder.must(QueryBuilders.termQuery("deleted", false));
            if(NAMESPACE_EXCLUSION_SET != null)
            {
                queryBuilder.mustNot(QueryBuilders.termsQuery("namespace.exact", NAMESPACE_EXCLUSION_SET));
            }
            
            QueryBuilder queryBuilder1 = SearchUtils.createBoolQueryBuilder(queryDTO);
            queryBuilder.must(queryBuilder1);

            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
            int limit = queryDTO.getLimit();
            int start = queryDTO.getStart();
            searchRequestBuilder.setFrom(start);
            if (limit > 0)
            {
                searchRequestBuilder.setSize(limit);
            }
     
            //searchRequestBuilder.setPostFilter(andFilter);
            searchRequestBuilder.setQuery(queryBuilder);
            
            long count = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.INDEX_NAME_WIKI_DOCUMENT)
                            .setSize(0).get().getHits().getTotalHits();

            //add global aggregations
            searchRequestBuilder.addAggregation(terms("type").field("type"))
                .addAggregation(terms("namespace").field("namespace.raw").size((int)count).order(Order.term(true)))
                .addAggregation(terms("author").field("sysCreatedBy").size((int)count).order(Order.term(true)));

            for(QuerySort querySort : queryDTO.getSortItems())
            {
                SortOrder sortOrder = SortOrder.ASC;
                boolean asc = querySort.getDirection() == QuerySort.SortOrder.ASC;
                if (!asc)
                {
                    sortOrder = SortOrder.DESC;
                }
                searchRequestBuilder.addSort(querySort.getProperty(), sortOrder);
            }

            if(Log.log.isDebugEnabled() && searchRequestBuilder != null)
            {
                Log.log.debug("Query: " + searchRequestBuilder.toString());
            }

            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());

            if(Log.log.isDebugEnabled() && response != null)
            {
                Log.log.debug("Response: " + response.toString());
            }

            ObjectMapper MAPPER = new ObjectMapper();
            List<WikiDocument> records = new ArrayList<WikiDocument>();
            for (SearchHit hit : response.getHits())
            {
                try
                {
                    WikiDocument obj = MAPPER.readValue(hit.getSourceAsString(), WikiDocument.class);
                    records.add(obj);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
            }

            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", records);
            data.put("Type", getAggregation("type", response, records.size()));
            data.put("Namespace", getAggregation("namespace", response, records.size()));
            data.put("Author", getAggregation("author", response, records.size()));
            result.setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }
    
    /**
     * Used for the following KM report.
     * 
     * 1. Feedback Rating Report - Summary
     * 
     * @param queryDTO
     * @param userField
     * @param userInfo
     * @return
     */
    public static ResponseDTO<Map<String, Object>> wikiTopSummaryAnalysis(final QueryDTO queryDTO, final UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        try
        {
            /*
             * This filter is not needed as we check rating greater than 0 in the result.
             */
            //queryDTO.addFilterItem(new QueryFilter(QueryFilter.NUMERIC_TYPE, "0", "rating", QueryFilter.GREATER_THAN));
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            queryBuilder.must(QueryBuilders.termQuery("hidden", false));
            queryBuilder.must(QueryBuilders.termQuery("deleted", false));
            if(NAMESPACE_EXCLUSION_SET != null)
            {
                queryBuilder.mustNot(QueryBuilders.termsQuery("namespace.exact", NAMESPACE_EXCLUSION_SET));
            }

            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
            //no need to return document hence the type is count
//            searchRequestBuilder.setSearchType(SearchType.COUNT);
            searchRequestBuilder.setQuery(queryBuilder);
            searchRequestBuilder.setSize(0);
            
            long count = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.INDEX_NAME_WIKI_DOCUMENT)
                            .setSize(0).get().getHits().getTotalHits();
            
            //add global aggregations
            searchRequestBuilder.addAggregation(terms("Type").field("type").order(Order.term(true)))
                .addAggregation(terms("Namespace").field("namespace.raw").size((int)count).order(Order.term(true)))
                .addAggregation(terms("Author").field("sysCreatedBy").size((int)count).order(Order.term(true)));

            //add rating agg along with top 3 wiki with that rating
            searchRequestBuilder.addAggregation(histogram("rating").field("rating").interval(1).subAggregation(terms("full_name").field("fullName.raw").size(3)));

            if(Log.log.isDebugEnabled() && searchRequestBuilder != null)
            {
                //System.out.println(searchRequestBuilder.toString());
                Log.log.debug("Query: " + searchRequestBuilder.toString());
            }

            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());

            if(Log.log.isDebugEnabled() && response != null)
            {
                Log.log.debug("Response: " + response.toString());
            }
            
            ArrayList<Object> records = new ArrayList<Object>();
            InternalHistogram histogram = response.getAggregations().get("rating");
            if(histogram != null)
            {
                Collection<org.elasticsearch.search.aggregations.bucket.histogram.InternalHistogram.Bucket> buckets = histogram.getBuckets();
                if(buckets != null)
                {
                    for(org.elasticsearch.search.aggregations.bucket.histogram.Histogram.Bucket ratingBucket : buckets)
                    {   
                        Map<String, Object> recordEntry = new TreeMap<String, Object>();
                        if(((Number)ratingBucket.getKey()).longValue() > 0)
                        {
                            recordEntry.put("Rating", ((Number)ratingBucket.getKey()).longValue());
                            recordEntry.put("Count", ratingBucket.getDocCount());
                            ArrayList<String> nameArray = new ArrayList<String>();
                            StringTerms nameBuckets = ratingBucket.getAggregations().get("full_name");
                            for(Bucket bucket : nameBuckets.getBuckets()) {
                                nameArray.add(bucket.getKeyAsString());
                            }
                            if (nameArray.size() > 0)
                            {
                                recordEntry.put("Wiki_name", nameArray);
                                records.add(recordEntry);
                            }
                        }
                    }
                }
            }
            
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", records);
            data.put("Type", getAggregation("Type", response, records.size()));
            data.put("Namespace", getAggregation("Namespace", response, records.size()));
            data.put("Author", getAggregation("Author", response, records.size()));
            result.setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }

        return result;
    }
    
    /**
     * Provides data for following KM reports.
     * 
     * 1. Summary by Author Reports:
     *      aggregationField=sysCreatedBy
     *   
     * 2. Summary by Namespace Reports:
     *      aggregationField=namespace
     *   
     * 3. Summary by Type Reports:
     *      aggregationField=type
     *   
     * @param queryDTO
     * @param aggregationField example: sysCreatedBy, namespace, type
     * @param userInfo
     * @return 
     */
    public static ResponseDTO<Map<String, Object>> wikiSummaryAnalysis(final QueryDTO queryDTO, final String aggregationType, final UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        
        try
        {
            if(StringUtils.isNotBlank(aggregationType))
            {
                BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
                queryBuilder.must(QueryBuilders.termQuery("hidden", false));
                queryBuilder.must(QueryBuilders.termQuery("deleted", false));
   
                if(NAMESPACE_EXCLUSION_SET != null)
                {
                    queryBuilder.mustNot(QueryBuilders.termsQuery("namespace.exact", NAMESPACE_EXCLUSION_SET));
                }

                SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
                searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
                //no need to return document hence the type is count
//                searchRequestBuilder.setSearchType(SearchType.COUNT);
                searchRequestBuilder.setQuery(queryBuilder);
                searchRequestBuilder.setSize(0);
                
                //Setting up aggregations
                String subAggsName = "";
                String subAggsField = "";
                
                if(aggregationType.equalsIgnoreCase("NAMESPACE")) {
                    subAggsName = "Namespace";
                    subAggsField = "namespace.raw";
                }
                else if(aggregationType.equalsIgnoreCase("TYPE")) {
                    subAggsName = "Type";
                    subAggsField = "type";
                }
                else if(aggregationType.equalsIgnoreCase("AUTHOR")) {
                    subAggsName = "Author";
                    subAggsField = "sysCreatedBy";
                }
                
                //Terms aggregation
                TermsAggregationBuilder subAggs = terms(subAggsName) .field(subAggsField);
                
//                //Group by interval
//                Calendar calendar = GregorianCalendar.getInstance();
//                TimeZone timeZone = calendar.getTimeZone();
//                int offsetTimeZone = - timeZone.getOffset( System.currentTimeMillis() ) / 3600000; //Convert to hour
//                String offsetString = offsetTimeZone + "h";
//                DateHistogramBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).interval(new Interval(interval)).minDocCount(0).subAggregation(subAggs).offset(offsetString);
                
                searchRequestBuilder.addAggregation(subAggs);
                SearchResponse response = searchRequestBuilder.execute().actionGet();
                result.setTotal(response.getHits().getTotalHits());
                
                //Handling return records
                ArrayList<Object> records = new ArrayList<Object>();
                StringTerms docTermBucket = response.getAggregations().get(subAggsName);
                for(Bucket docTerm : docTermBucket.getBuckets()) {
                    String termName = docTerm.getKeyAsString();
                    long totalCount = docTerm.getDocCount();
                    Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                    recordsEntry.put("name", termName);
                    recordsEntry.put("count", totalCount);
                    records.add(recordsEntry);
                }
                Map<String, Object> data = new HashMap<String, Object>();
                data.put("data", records);
                result.setData(data);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }

        return result;
    }
    /** 
     *This API is used for following reports
     * All wiki related reports. This is used to get filter for each reports.
     *   
     * @param queryDTO
     * @param userInfo
     * @return 
     */
     public static ResponseDTO<Map<String, Object>> wikiFilter(QueryDTO queryDTO, UserInfo userInfo)
     {
         ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
         try {
            
             BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
             queryBuilder.must(QueryBuilders.termQuery("hidden", false));
             queryBuilder.must(QueryBuilders.termQuery("deleted", false));
             if(NAMESPACE_EXCLUSION_SET != null)
             {
                 queryBuilder.mustNot(QueryBuilders.termsQuery("namespace.exact", NAMESPACE_EXCLUSION_SET));
             }
             
             SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
             searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
//             searchRequestBuilder.setSearchType(SearchType.COUNT);
             searchRequestBuilder.setSize(0);
             searchRequestBuilder.setQuery(queryBuilder);
            
             long count = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_WIKI_DOCUMENT).setTypes(SearchConstants.INDEX_NAME_WIKI_DOCUMENT)
                             .setSize(0).get().getHits().getTotalHits();
             //Aggregations
             String namespaceAggsName = "Namespace";
             String namespaceField = "namespace.raw";
             TermsAggregationBuilder namespaceAggs = terms(namespaceAggsName).field(namespaceField).order(Order.term(true)).size((int)count);
             
             String authorAggsName = "Author";
             String authorField = "sysCreatedBy";
             TermsAggregationBuilder authorAggs = terms(authorAggsName).field(authorField).order(Order.term(true)).size((int)count);
            
             searchRequestBuilder.addAggregation(namespaceAggs).addAggregation(authorAggs);
             SearchResponse response = searchRequestBuilder.execute().actionGet();
             result.setTotal(response.getHits().getTotalHits());
             
             //Handling return records
             ArrayList<Object> namespaceRecords = new ArrayList<Object>();
             StringTerms namespaceBucket = response.getAggregations().get(namespaceAggsName);
             for(Bucket docName : namespaceBucket.getBuckets()) {
                 String namespace = docName.getKeyAsString();
                 namespaceRecords.add(namespace);
             }
             
             //Handling return records
             ArrayList<Object> authorRecords = new ArrayList<Object>();
             StringTerms authorBucket = response.getAggregations().get(authorAggsName);
             for(Bucket docName : authorBucket.getBuckets()) {
                 String author = docName.getKeyAsString();
                 authorRecords.add(author);
             }
             
             Map<String, Object> data = new HashMap<String, Object>();
             data.put("Namespace", namespaceRecords);
             data.put("Author", authorRecords);
             result.setData(data);
         }
         catch (Exception e)
         {
             Log.log.error(e.getMessage(), e);
             result.setMessage(e.getMessage());
         }
         return result;
     }
     
    /** 
    *This API is used for following reports
    * All runbook related reports
    *   
    * @param queryDTO
    * @param userInfo
    * @return 
    */
    public static ResponseDTO<Map<String, Object>> runbookAnalysis(QueryDTO queryDTO, final String aggregationType, final String interval, final String timezone, final String lowerBound, final String upperBound, UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
            if(StringUtils.isNotBlank(interval) && StringUtils.isNotBlank(aggregationType)){
                BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
                SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
                searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
                searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
                searchRequestBuilder.setSize(0);
                searchRequestBuilder.setQuery(queryBuilder);
                
                //Setting up aggregations
                String runbookNameAggsName = "Doc_name";
                String runbookNameField = "wiki.raw";
                String intervalAggsName = "Interval";
                String intervalAggsField = "timestamp";
                String subAggsName = "";
                String subAggsField = "";
          
                if(aggregationType.equalsIgnoreCase("DURATION")) {
                    subAggsName = "Duration";
                    subAggsField = "duration";
                    //Create sum aggregation
                    SumAggregationBuilder sumAggs = sum(subAggsName).field(subAggsField);
                    //Group by runbook name.
                    TermsAggregationBuilder runbookNameAggs = terms(runbookNameAggsName).field(runbookNameField).subAggregation(sumAggs);
                    //Group by interval
//                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).timeZone(DateTimeZone.forID(timezone)).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    searchRequestBuilder.addAggregation(dateHistogramAgg);
                }
                else if(aggregationType.equalsIgnoreCase("EXECUTION")) {
                 
                    //Group by runbook name.
                    TermsAggregationBuilder runbookNameAggs = terms(runbookNameAggsName).field(runbookNameField);
                    //Group by interval
//                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).timeZone(DateTimeZone.forID(timezone)).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    searchRequestBuilder.addAggregation(dateHistogramAgg);
                }
                
                SearchResponse response = searchRequestBuilder.execute().actionGet();
                
                //Handling return records
                ArrayList<Object> records = new ArrayList<Object>();
                InternalDateHistogram agg = response.getAggregations().get(intervalAggsName);
                for(InternalDateHistogram.Bucket timestamp : agg.getBuckets()) {
                    String ts = ((DateTime)timestamp.getKey()).getMillis()+"";
                    long docCount = timestamp.getDocCount();
                    if(docCount > 0) {
                        StringTerms docNameBucket = timestamp.getAggregations().get(runbookNameAggsName);
                        for(Bucket docName : docNameBucket.getBuckets()) {
                            String runbookName = docName.getKeyAsString();
                            
                            //DURATION type
                            if(aggregationType.equalsIgnoreCase("DURATION")) {
                                Sum totalDuration = docName.getAggregations().get(subAggsName);
                                double duration = totalDuration.getValue();
                                long totalCount = docName.getDocCount();
                                Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                                recordsEntry.put("timestamp", ts);
                                recordsEntry.put("runbook_name", runbookName);
                                recordsEntry.put("duration", duration);
                                recordsEntry.put("count", totalCount);
                                records.add(recordsEntry);
                            }
                            
                           
                            //EXECUTION type
                            else if(aggregationType.equalsIgnoreCase("EXECUTION")) {
                                double count = docName.getDocCount();
                                Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                                recordsEntry.put("timestamp", ts);
                                recordsEntry.put("runbook_name", runbookName);
                                recordsEntry.put("count", count);
                                records.add(recordsEntry);
                            }
                        }
                    }
                    else{
                        //Still return empty data for this timestamp to over interval jump
                        Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                        recordsEntry.put("timestamp", ts);
                        records.add(recordsEntry);
                    }
                }
                Map<String, Object> data = new HashMap<String, Object>();
                data.put("data", records);
                result.setData(data);
                result.setTotal(records.size());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }
    
    public static ResponseDTO<Map<String, Object>> runbookExecutionAnalysis(QueryDTO queryDTO, final String aggregationType, final String interval, final String timezone, final String lowerBound, final String upperBound, UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
            if(StringUtils.isNotBlank(interval) && StringUtils.isNotBlank(aggregationType)){
                BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
                SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
                searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
                searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
                searchRequestBuilder.setSize(0);
                searchRequestBuilder.setQuery(queryBuilder);
                
                //Setting up aggregations
                String runbookNameAggsName = "Doc_name";
                String runbookNameField = "wiki.raw";
                String intervalAggsName = "Interval";
                String intervalAggsField = "timestamp";
                String subAggsName = "";
                String subAggsField = "";
          
               if(aggregationType.equalsIgnoreCase("CONDITION")) {
                    
                    subAggsName = "Condition";
                    subAggsField = "condition";
                    //Create term aggregation
                    TermsAggregationBuilder termAggs = terms(subAggsName).field(subAggsField);
                    //Group by runbook name.
                    TermsAggregationBuilder runbookNameAggs = terms(runbookNameAggsName) .field(runbookNameField).subAggregation(termAggs);
                    //Group by interval
//                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).timeZone(DateTimeZone.forID(timezone)).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    searchRequestBuilder.addAggregation(dateHistogramAgg);
                    
                    //Global aggregation to get all condition type
                    searchRequestBuilder.addAggregation(AggregationBuilders.global("globalAgg").subAggregation(terms("typeName").field("condition")));
                }
                else if(aggregationType.equalsIgnoreCase("SEVERITY")) {
                    subAggsName = "Severity";
                    subAggsField = "severity";
                    //Create term aggregation
                    TermsAggregationBuilder termAggs = terms(subAggsName).field(subAggsField);
                    //Group by runbook name.
                    TermsAggregationBuilder runbookNameAggs = terms(runbookNameAggsName) .field(runbookNameField).subAggregation(termAggs);
                    //Group by interval
//                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).timeZone(DateTimeZone.forID(timezone)).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(runbookNameAggs).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
                    searchRequestBuilder.addAggregation(dateHistogramAgg);
                    
                    //Global aggregation to get all condition type
                    searchRequestBuilder.addAggregation(AggregationBuilders.global("globalAgg").subAggregation(terms("typeName").field("severity")));
                }
               
                
                SearchResponse response = searchRequestBuilder.execute().actionGet();
                result.setTotal(response.getHits().getTotalHits());
                
                //Handling return records
                //Asign each type an empty array.
                Map<String, ArrayList<Object>> recordsForEachType = new TreeMap<String, ArrayList<Object>>();
                Global globalAgg = response.getAggregations().get("globalAgg");
                StringTerms typeNameAggs = globalAgg.getAggregations().get("typeName");
                for(Bucket typeNameBucket : typeNameAggs.getBuckets()) {
                    recordsForEachType.put(typeNameBucket.getKeyAsString(), new ArrayList<Object>());
                }
                InternalDateHistogram agg = response.getAggregations().get(intervalAggsName);
                for(InternalDateHistogram.Bucket timestamp : agg.getBuckets()) {
                    String ts = ((DateTime)timestamp.getKey()).getMillis()+"";
                    long docCount = timestamp.getDocCount();
                    if(docCount > 0) {
                        StringTerms docNameBucket = timestamp.getAggregations().get(runbookNameAggsName);
                        for(Bucket docName : docNameBucket.getBuckets()) {
                            String runbookName = docName.getKeyAsString();
                            StringTerms typeBucket = docName.getAggregations().get(subAggsName);
                            for(Bucket type : typeBucket.getBuckets()) {
                                String typeName  = type.getKeyAsString();
                                //Get array contain this type
                                ArrayList<Object> container = recordsForEachType.get(typeName);
                                double count = type.getDocCount();
                                Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                                recordsEntry.put("timestamp", ts);
                                recordsEntry.put("runbook_name", runbookName);
                                recordsEntry.put("count", count);
                                container.add(recordsEntry);
                            }
                        }
                    }
                    else{
                        //Still return empty data for this timestamp to over interval jump
                        Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                        recordsEntry.put("timestamp", ts);
                        for(String k : recordsForEachType.keySet()) {
                            ArrayList<Object> container = recordsForEachType.get(k);
                            container.add(recordsEntry);
                        }
                    }
                }
                Map<String, Object> data = new HashMap<String, Object>();
                for(String k : recordsForEachType.keySet()) {
                    ArrayList<Object> container = recordsForEachType.get(k);
                    data.put(k , container);
                }
                result.setData(data);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }
    
    public static ResponseDTO<Map<String, Object>> runbookName(QueryDTO queryDTO, UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
           
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);
            
            //Aggregations
            String runbookNameAggsName = "Doc_name";
            String runbookNameField = "wiki.raw";
           
            long count = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT)
                            .setSize(0).get().getHits().getTotalHits();
            //Group by runbook name.
            TermsAggregationBuilder runbookNameAggs = terms(runbookNameAggsName).field(runbookNameField).order(Order.term(true)).size((int) count);
           
            searchRequestBuilder.addAggregation(runbookNameAggs);
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            
            //Handling return records
            ArrayList<Object> records = new ArrayList<Object>();
            StringTerms docNameBucket = response.getAggregations().get(runbookNameAggsName);
            for(Bucket docName : docNameBucket.getBuckets()) {
                String runbookName = docName.getKeyAsString();
                Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                recordsEntry.put("runbook_name", runbookName);
                records.add(recordsEntry);
            }
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", records);
            result.setData(data);
            result.setTotal(records.size());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    public static ResponseDTO<Map<String, Object>>  taskAnalysis(QueryDTO queryDTO, String interval, String timezone, String lowerBound, String upperBound, UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
           
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);
            
            //Aggregations
            String taskNameAggsName = "Task_name";
            String taskNameField = "taskName";
            String intervalAggsName = "Interval";
            String intervalAggsField = "timestamp";
            String sumAggsName = "Duration";
            String sumAggsField = "duration";
            
            //Create sum aggregation
            SumAggregationBuilder sumAggs = sum(sumAggsName).field(sumAggsField);
            
            //Group by timer name.
            TermsAggregationBuilder timerNameAggs = terms(taskNameAggsName) .field(taskNameField).subAggregation(sumAggs);
            
            //Group by interval
//            DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(timerNameAggs).timeZone(DateTimeZone.forID(timezone)).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
            DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(timerNameAggs).extendedBounds(new ExtendedBounds(lowerBound, upperBound));

            searchRequestBuilder.addAggregation(dateHistogramAgg);
            
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());
            
            //Handling return records
            ArrayList<Object> records = new ArrayList<Object>();
            InternalDateHistogram agg = response.getAggregations().get(intervalAggsName);
            for(InternalDateHistogram.Bucket timestamp : agg.getBuckets()) {
                String ts = ((DateTime)timestamp.getKey()).getMillis()+"";
                long docCount = timestamp.getDocCount();
                if(docCount > 0) {
                    StringTerms taskNameAgg = timestamp.getAggregations().get(taskNameAggsName);
                    for(Bucket taskNameBucket : taskNameAgg.getBuckets()) {
                        String taskName = taskNameBucket.getKeyAsString();
                        Sum totalDuration = taskNameBucket.getAggregations().get(sumAggsName);
                        double duration = totalDuration.getValue();
                        long totalCount = taskNameBucket.getDocCount();
                        Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                        recordsEntry.put("timestamp", ts);
                        recordsEntry.put("task_name", taskName);
                        recordsEntry.put("duration", duration);
                        recordsEntry.put("count", totalCount);
                        records.add(recordsEntry);
                    }
                }
                else {
                    Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                    recordsEntry.put("timestamp", ts);
                    records.add(recordsEntry);
                }
            }
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", records);
            result.setData(data);
                      
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }
    
    public static ResponseDTO<Map<String, Object>> timerAnalysis(QueryDTO queryDTO, String interval, String timezone, String lowerBound, String upperBound, UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
           
        	List<QueryFilter>filters = queryDTO.getFilters();
        	for (QueryFilter f : filters)
        	{
        		if ("sysCreatedOn".equalsIgnoreCase(f.getField()))
        		{
        			f.setParamName(Constants.RANGE_QUERY_FILTER_NO_TIMEZONE);
        		}
        	}
        	
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_METRICTIMER).setTypes(SearchConstants.DOCUMENT_TYPE_METRICTIMER);
            searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);
            
            //Aggregations
            String timerNameAggsName = "Timer_name";
            String timerNameField = "src.raw";
            String intervalAggsName = "Interval";
            String intervalAggsField = "sysCreatedOn";
            String sumAggsName = "Duration";
            String sumAggsField = "value";
            
            //Create sum aggregation
            SumAggregationBuilder sumAggs = sum(sumAggsName).field(sumAggsField);
            
            //Group by timer name.
            TermsAggregationBuilder timerNameAggs = terms(timerNameAggsName) .field(timerNameField).subAggregation(sumAggs);
            
            //Group by interval
//            DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(timerNameAggs).timeZone(DateTimeZone.forID(timezone)).extendedBounds(new ExtendedBounds(lowerBound, upperBound));
            DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram(intervalAggsName).field(intervalAggsField).dateHistogramInterval(new DateHistogramInterval(interval)).minDocCount(0).subAggregation(timerNameAggs).extendedBounds(new ExtendedBounds(lowerBound, upperBound));

            searchRequestBuilder.addAggregation(dateHistogramAgg);
            
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());
            
            //Handling return records
            ArrayList<Object> records = new ArrayList<Object>();
            InternalDateHistogram agg = response.getAggregations().get(intervalAggsName);
            for(InternalDateHistogram.Bucket timestamp : agg.getBuckets()) {
                String ts = ((DateTime)timestamp.getKey()).getMillis()+"";
                long docCount = timestamp.getDocCount();
                if(docCount > 0) {
                    StringTerms timerNameAgg = timestamp.getAggregations().get(timerNameAggsName);
                    for(Bucket timerNameBucket : timerNameAgg.getBuckets()) {
                        String timerName = timerNameBucket.getKeyAsString();
                        Sum totalDuration = timerNameBucket.getAggregations().get(sumAggsName);
                        double duration = totalDuration.getValue();
                        long totalCount = timerNameBucket.getDocCount();
                        Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                        recordsEntry.put("timestamp", ts);
                        recordsEntry.put("timer_name", timerName);
                        recordsEntry.put("duration", duration);
                        recordsEntry.put("count", totalCount);
                        records.add(recordsEntry);
                    }
                }
                else{
                    //Still return empty data for this timestamp to over interval jump
                    Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                    recordsEntry.put("timestamp", ts);
                    records.add(recordsEntry);
                }
            }
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", records);
            result.setData(data);
                      
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    public static ResponseDTO<Map<String, Object>> topTaskAnalysis(QueryDTO queryDTO,final String type, UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
           
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            //Exluclude all #resolve task name
            queryBuilder.mustNot(QueryBuilders.wildcardQuery("taskFullName.raw", "*#resolve*"));
            
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);
            
            //Aggregations
            String taskAggsName = "task_name";
            String taskAggsField = "taskFullName.raw";
            String durationAggsName = "avg_duration";
            String durationAggsField = "duration";
            int displayLimit = queryDTO.getLimit();
           
            //Group by task Name
            TermsAggregationBuilder taskNameAggs = terms(taskAggsName).size(displayLimit).field(taskAggsField);
            if(type.equalsIgnoreCase("DURATION"))
            {
                AvgAggregationBuilder durationAvg = avg(durationAggsName).field(durationAggsField);
                //Return top avg duration
                taskNameAggs.subAggregation(durationAvg).order(Order.aggregation(durationAggsName, false));
            }
            searchRequestBuilder.addAggregation(taskNameAggs);
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            result.setTotal(response.getHits().getTotalHits());
            
            //Handling return records
            ArrayList<Object> records = new ArrayList<Object>();
            StringTerms taskNameAgg = response.getAggregations().get(taskAggsName);
            for(Bucket taskNameBucket : taskNameAgg.getBuckets()) {
                Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                recordsEntry.put("task_name", taskNameBucket.getKey());
                if(type.equalsIgnoreCase("EXECUTION")){
                    recordsEntry.put("count" , taskNameBucket.getDocCount());
                }
                else if(type.equalsIgnoreCase("DURATION")){
                    Avg avg_duration = taskNameBucket.getAggregations().get(durationAggsName);
                    recordsEntry.put("avg_duration" , avg_duration.getValue());
                }
                records.add(recordsEntry);
            }
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", records);
            result.setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    public static ResponseDTO<Map<String, Object>> taskName(QueryDTO queryDTO, UserInfo userInfo)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
           
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            //Exluclude all #resolve task name
            queryBuilder.mustNot(QueryBuilders.wildcardQuery("taskFullName", "*#resolve*"));
            
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);
            
            //Aggregations
            String taskAggsName = "task_name";
            String taskAggsField = "taskFullName.raw";
           
            long count = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT)
                            .setSize(0).get().getHits().getTotalHits();
            //Group by task Name
            TermsAggregationBuilder taskNameAggs = terms(taskAggsName) .field(taskAggsField).order(Order.term(true)).size((int)count);
            searchRequestBuilder.addAggregation(taskNameAggs);
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            
            //Handling return records
            ArrayList<Object> records = new ArrayList<Object>();
            StringTerms taskNameAgg = response.getAggregations().get(taskAggsName);
            for(Bucket taskNameBucket : taskNameAgg.getBuckets()) {
                Map<String, Object> recordsEntry = new TreeMap<String, Object>();
                recordsEntry.put("task_name", taskNameBucket.getKey());
                records.add(recordsEntry);
            }
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", records);
            result.setData(data);
            result.setTotal(records.size());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }
    
    @SuppressWarnings("rawtypes")
	public static ResponseDTO adminReports(QueryDTO query, String interval, String type, String reportType) throws Exception
    {
    	switch(reportType)
    	{
    	    case "runbook" : return adminRunbook(query, interval, type);
    		case "jvm" : return adminJvm(query, interval, type);
    		case "cpu" : return adminCpu(query, interval, type);
    		case "database" : return adminDatabase(query, interval, type);
    		case "page" : return adminPage(query, interval, type);
    		case "user" : return adminUser(query, interval, type);    		
    		default : throw new Exception("Invalid Report Type.");
    	}
    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO adminRunbook(QueryDTO query, String interval, String type) throws ParseException
    {
        ResponseDTO result = new ResponseDTO();
        if (interval.equals("5m"))
        {
            query.setModelName("metric_min_transaction");
        }
        else if (interval.equals("1h"))
        {
            query.setModelName("metric_hr_transaction");
        }
        else
        {
            query.setModelName("metric_day_transaction");
        }
        
        String dataField = null;
        String sql = null;
        String esField = null;
        if (type.equals("latency"))
        {
            dataField = "avg_latency";
            sql = "select ts, src, (TOT_LATENCY / CNT_LATENCY) as avg_latency from ";
            esField = "latency";
        }     
        
        sql = sql + query.getModelName() ;
        
        String dateRange = null;
        if (query.getFilterItems() != null)
        {
            for (QueryFilter filter : query.getFilterItems())
            {
				dateRange = " where ts between " + SDF.parse(filter.getStartValue().toUpperCase()).getTime() + " and "
						+ SDF.parse(filter.getEndValue().toUpperCase()).getTime();
                sql = sql + dateRange;
            }
        }
        
        List sqlList = null;
        sqlList = readSQLReport(sql, sqlList);
        
        // Retrieve distinct sources
        sql = "select distinct src from " + query.getModelName() + dateRange;
        List<String> srcList = null;
        srcList = readSQLReport(sql, srcList);
        
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        
        if (sqlList != null)
        {
            for (Object obj : sqlList)
            {
                Object[] objArray = (Object[]) obj;
                long ts = Long.parseLong(objArray[0].toString());
                String src = objArray[1].toString();
                double avg = Double.parseDouble(objArray[2].toString());
                Map<String, Object> resultMap = new HashMap<String, Object>();
                resultMap.put("ts", ts);
                resultMap.put("src", src);
                resultMap.put(dataField, avg);
                resultList.add(resultMap);
            }
        }
        
        adminReportGenericESQuery(query, "metrictransaction", interval, "avg", esField, dataField, resultList, srcList, null);
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("src", srcList);
        resultMap.put("data", resultList);
        
        result.setData(resultList);
        result.setRecords(srcList);
        
        return result;
    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static ResponseDTO adminJvm(QueryDTO query, String interval, String type) throws ParseException
    {
    	ResponseDTO result = new ResponseDTO();
    	if (interval.equals("5m"))
    	{
    		query.setModelName("metric_min_jvm");
    	}
    	else if (interval.equals("1h"))
    	{
    		query.setModelName("metric_hr_jvm");
    	}
    	else
    	{
    		query.setModelName("metric_day_jvm");
    	}
    	
    	String dataField = null;
    	String sql = null;
    	String esField = null;
    	if (type.equals("thread"))
    	{
    		dataField = "avg_thread_count";
    		sql = "select ts, src, (TOT_THREAD_COUNT / CNT_THREAD_COUNT) as avg_thread_count from ";
    		esField = "threadCount";
    	}
    	else
    	{
    		// memory count
    		dataField = "avg_mem_free";
    		sql = "select ts, src, (TOT_MEM_FREE / CNT_MEM_FREE) as avg_mem_free from ";
    		esField = "freeMem";
    	}
    	
    	sql = sql + query.getModelName() ;
    	
    	String dateRange = null;
    	if (query.getFilterItems() != null)
    	{
    		for (QueryFilter filter : query.getFilterItems())
    		{
				dateRange = " where ts between " + SDF.parse(filter.getStartValue().toUpperCase()).getTime() + " and "
						+ SDF.parse(filter.getEndValue().toUpperCase()).getTime();
    			sql = sql + dateRange;
    		}
    	}
    	
    	List sqlList = null;
    	sqlList = readSQLReport(sql, sqlList);
    	
    	// Retrieve distinct sources
    	sql = "select distinct src from " + query.getModelName() + dateRange;
    	List<String> srcList = null;
    	srcList = readSQLReport(sql, srcList);
    	
    	List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
    	
    	if (sqlList != null)
    	{
    		for (Object obj : sqlList)
    		{
    			Object[] objArray = (Object[]) obj;
    			long ts = Long.parseLong(objArray[0].toString());
    			String src = objArray[1].toString();
    			double avg = Double.parseDouble(objArray[2].toString());
    			Map<String, Object> resultMap = new HashMap<String, Object>();
				resultMap.put("ts", ts);
				resultMap.put("src", src);
				resultMap.put(dataField, avg);
				resultList.add(resultMap);
    		}
    	}
    	
    	adminReportGenericESQuery(query, "metricjvm", interval, "avg", esField, dataField, resultList, srcList, null);
    	
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	resultMap.put("src", srcList);
    	resultMap.put("data", resultList);
    	
    	result.setData(resultList);
    	result.setRecords(srcList);
    	
    	return result;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResponseDTO adminCpu(QueryDTO query, String interval, String type) throws ParseException
    {
    	ResponseDTO result = new ResponseDTO();
    	
    	if (interval.equals("5m"))
    	{
    		query.setModelName("metric_min_server");
    	}
    	else if (interval.equals("1h"))
    	{
    		query.setModelName("metric_hr_server");
    	}
    	else
    	{
    		query.setModelName("metric_day_server");
    	}
    	
    	String dataField = null;
    	String sql = null;
    	String esField = null;
    	String esAggregationType = null;
    	
    	if (type.equals("1m"))
    	{
    		dataField = "avg_load1";
    		sql = "select ts, src, (TOT_LOAD1 / CNT_LOAD1)/100 as avg_load1 from ";
    		esAggregationType = "avg";
    		esField = "load1";
    	}
    	else if (type.equals("5m"))
    	{
    		dataField = "avg_load5";
    		sql = "select ts, src, (TOT_LOAD5 / CNT_LOAD5)/100 as avg_load5 from ";
    		esAggregationType = "avg";
    		esField = "load5";
    	}
    	else
    	{
    		dataField = "avg_load15";
    		sql = "select ts, src, (TOT_LOAD15 / CNT_LOAD15)/100 as avg_load15 from ";
    		esAggregationType = "avg";
    		esField = "load15";
    	}
    	
    	sql = sql + query.getModelName() ;
    	
    	String dateRange = null;
    	if (query.getFilterItems() != null)
    	{
    		for (QueryFilter filter : query.getFilterItems())
    		{
				dateRange = " where ts between " + SDF.parse(filter.getStartValue().toUpperCase()).getTime() + " and "
						+ SDF.parse(filter.getEndValue().toUpperCase()).getTime();
    			sql = sql + dateRange;
    		}
    	}
    	List sqlList = null;
    	sqlList = readSQLReport(sql, sqlList);
    	
    	// Retrieve distinct sources
    	sql = "select distinct src from " + query.getModelName() + dateRange;
    	List<String> srcList = null;
    	srcList = readSQLReport(sql, srcList);
    	
    	List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
    	
    	if (sqlList != null)
    	{
    		for (Object obj : sqlList)
    		{
    			Object[] objArray = (Object[]) obj;
    			long ts = Long.parseLong(objArray[0].toString());
    			String src = objArray[1].toString();
    			double avg = Double.parseDouble(objArray[2].toString());
    			Map<String, Object> resultMap = new HashMap<String, Object>();
				resultMap.put("ts", ts);
				resultMap.put("src", src);
				resultMap.put(dataField, avg);
				resultList.add(resultMap);
    		}
    	}
    	
    	adminReportGenericESQuery(query, "metriccpu", interval, esAggregationType, esField, dataField, resultList, srcList, new Integer(100));
    	
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	resultMap.put("src", srcList);
    	resultMap.put("data", resultList);
    	
    	result.setData(resultList);
        result.setRecords(srcList);
    	
    	return result;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResponseDTO adminDatabase(QueryDTO query, String interval, String type) throws ParseException
    {
    	ResponseDTO result = new ResponseDTO();
    	
    	if (interval.equals("5m"))
    	{
    		query.setModelName("metric_min_database");
    	}
    	else if (interval.equals("1h"))
    	{
    		query.setModelName("metric_hr_database");
    	}
    	else
    	{
    		query.setModelName("metric_day_database");
    	}
    	
    	String dataField = null;
    	String sql = null;
    	String esField = null;
    	String esAggregationType = null;
    	
    	if (type.equals("dbSize"))
    	{
    		dataField = "avg_size";
    		sql = "select ts, src, (TOT_SIZE / CNT_SIZE) as avg_size from ";
    		esAggregationType = "avg";
    		esField = "size";
    	}
    	else if (type.equals("resTime"))
    	{
    		dataField = "avg_response_time";
    		sql = "select ts, src, (TOT_RESPONSE_TIME / CNT_RESPONSE_TIME) as avg_response_time from ";
    		esAggregationType = "avg";
    		esField = "responseTime";
    	}
    	else
    	{
    		dataField = "avg_percentage_wait";
    		sql = "select ts, src, (TOT_PERCENTAGE_WAIT / CNT_PERCENTAGE_WAIT) as avg_percentage_wait from ";
    		esAggregationType = "avg";
    		esField = "percentWait";
    	}
    	
    	sql = sql + query.getModelName() ;
    	
    	String dateRange = null;
    	if (query.getFilterItems() != null)
    	{
    		for (QueryFilter filter : query.getFilterItems())
    		{
				dateRange = " where ts between " + SDF.parse(filter.getStartValue().toUpperCase()).getTime() + " and "
						+ SDF.parse(filter.getEndValue().toUpperCase()).getTime();
    			sql = sql + dateRange;
    		}
    	}
    	
    	// Retrieve data
    	List sqlList = null;
    	sqlList = readSQLReport(sql, sqlList);
    	
    	// Retrieve distinct sources
    	sql = "select distinct src from " + query.getModelName() + dateRange;
    	List<String> srcList = null;
    	srcList = readSQLReport(sql, srcList);
    	
    	List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
    	if (sqlList != null)
    	{
    		for (Object obj : sqlList)
    		{
    			Object[] objArray = (Object[]) obj;
    			long ts = Long.parseLong(objArray[0].toString());
    			String src = objArray[1].toString();
    			double avg = Double.parseDouble(objArray[2].toString());
    			Map<String, Object> resultMap = new HashMap<String, Object>();
				resultMap.put("ts", ts);
				resultMap.put("src", src);
				resultMap.put(dataField, avg);
				resultList.add(resultMap);
    		}
    	}
    	
    	adminReportGenericESQuery(query, "metricdb", interval, esAggregationType, esField, dataField, resultList, srcList, null);
    	
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	resultMap.put("src", srcList);
    	resultMap.put("data", resultList);
    	
    	result.setData(resultList);
    	result.setRecords(srcList);
    	
    	return result;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResponseDTO adminPage(QueryDTO query, String interval, String type) throws ParseException
    {
    	ResponseDTO result = new ResponseDTO();
    	
    	if (interval.equals("5m"))
    	{
    		query.setModelName("metric_min_latency");
    	}
    	else if (interval.equals("1h"))
    	{
    		query.setModelName("metric_hr_latency");
    	}
    	else
    	{
    		query.setModelName("metric_day_latency");
    	}
    	
    	String dataField = null;
    	String sql = null;
    	String esField = null;
    	String esAggregationType = null;
    	
    	if (type.equals("wiki"))
    	{
    		dataField = "tot_wiki";
    		sql = "select ts, src, tot_wiki from ";
    		esAggregationType = "sum";
    		esField = "wiki";
    	}
    	else
    	{
    		dataField = "avg_wikiresponsetime";
    		sql = "select ts, src, TOT_WIKIRESPONSETIME, CNT_WIKIRESPONSETIME from ";
    		esAggregationType = "avg";
    		esField = "wikiResponseTime";
    	}
    	
    	sql = sql + query.getModelName() ;
    	
    	String dateRange = null;
    	if (query.getFilterItems() != null)
    	{
    		for (QueryFilter filter : query.getFilterItems())
    		{
				dateRange = " where ts between " + SDF.parse(filter.getStartValue().toUpperCase()).getTime() + " and "
						+ SDF.parse(filter.getEndValue().toUpperCase()).getTime();
    			sql = sql + dateRange;
    		}
    	}
    	
    	List sqlList = null;
    	sqlList = readSQLReport(sql, sqlList);
    	
    	// Retrieve distinct sources
    	sql = "select distinct src from " + query.getModelName() + dateRange;
    	List<String> srcList = null;
    	srcList = readSQLReport(sql, srcList);
    	
    	List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
    	
    	if (sqlList != null)
    	{
    		for (Object obj : sqlList)
    		{
    			Object[] objArray = (Object[]) obj;
    			long ts = Long.parseLong(objArray[0].toString());
    			String src = objArray[1].toString();
    			double avg = 0;
    			if (type.equals("wiki"))
    			{
    				avg = Double.parseDouble(objArray[2].toString());
    			}
    			else
    			{
	    			long totalWikiResTime = Long.parseLong(objArray[2].toString());
	    			long cntWikiResTome = Long.parseLong(objArray[3].toString());
	    			if (cntWikiResTome != 0)
	    			{
	    				avg = (totalWikiResTime / cntWikiResTome) / 1000;
	    			}
    			}
    			Map<String, Object> resultMap = new HashMap<String, Object>();
				resultMap.put("ts", ts);
				resultMap.put("src", src);
				resultMap.put(dataField, avg);
				resultList.add(resultMap);
    		}
    	}
    	
    	adminReportGenericESQuery(query, "metriclatency", interval, esAggregationType ,esField, dataField, resultList, srcList, null);
    	
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	resultMap.put("src", srcList);
    	resultMap.put("data", resultList);
    	
    	result.setData(resultList);
        result.setRecords(srcList);
    	
    	return result;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO adminUser(QueryDTO query, String interval, String type) throws ParseException
    {        
        ResponseDTO result = new ResponseDTO();
        if (interval.equals("5m"))
        {
            query.setModelName("metric_min_users");
        }
        else if (interval.equals("1h"))
        {
            query.setModelName("metric_hr_users");
        }
        else
        {
            query.setModelName("metric_day_users");
        }
        
        String dataField = null;
        String sql = null;
        String esField = null;
        
        dataField = "avg_user_active";
        sql = "select ts, src, (TOT_ACTIVE / CNT_ACTIVE) as avg_user_count from ";
        esField = "active";
        
        sql = sql + query.getModelName() ;
        
        SimpleDateFormat  sdf = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        
        String dateRange = null;
        if (query.getFilterItems() != null)
        {
            for (QueryFilter filter : query.getFilterItems())
            {
                Date startDate = sdf.parse(filter.getStartValue());
                Date endDate = sdf.parse(filter.getEndValue());
                
                dateRange = " where ts between " + startDate.getTime() + " and " + endDate.getTime();
                sql = sql + dateRange;
            }
        }
        
        Log.log.debug("user report SQL Data Query : " + sql);
        
        List sqlList = null;
        sqlList = readSQLReport(sql, sqlList);
        
        // Retrieve distinct sources
        sql = "select distinct src from " + query.getModelName() + dateRange;
        
        Log.log.debug("user report SQL Distinct Label Query : " + sql);
        
        List<String> srcList = null;
        srcList = readSQLReport(sql, srcList);
        
        Set<String> distinctSrcs = new HashSet<String>();
        
        if (srcList != null && !srcList.isEmpty())
        {
            for (String src : srcList)
            {
                if (StringUtils.isNotBlank(src))
                {
                    distinctSrcs.add(src);
                }
            }
        }
        List<String> dsrcList = new ArrayList<String>(distinctSrcs);
        
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        
        if (sqlList != null)
        {
            for (Object obj : sqlList)
            {
                Object[] objArray = (Object[]) obj;
                long ts = Long.parseLong(objArray[0].toString());
                String src = objArray[1].toString();
                double avg = Double.parseDouble(objArray[2].toString());
                Map<String, Object> resultMap = new HashMap<String, Object>();
                resultMap.put("ts", ts);
                resultMap.put("src", src);
                resultMap.put(dataField, avg);
                resultList.add(resultMap);
            }
        }
        
        adminReportGenericESQuery(query, "metricusers", interval, "avg", esField, dataField, resultList, dsrcList, null);
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("src", dsrcList);
        resultMap.put("data", resultList);
        
        result.setData(resultList);
        result.setRecords(dsrcList);
        
        return result;
    }
    
    private static void adminReportGenericESQuery(QueryDTO query, String indexName, String interval,String esAggregationType, String esField, String dataField, 
    		List<Map<String, Object>> resultList, List<String> srcList, Integer avgDenominator)
    {
    	SearchRequestBuilder  srb = SearchAdminAPI.getClient().prepareSearch(indexName);
        srb.setTypes(indexName);
        srb.setSize(0);
        
        BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(query);
        srb.setQuery(queryBuilder);
        
    	DateHistogramAggregationBuilder dhb = AggregationBuilders.dateHistogram("avghistogram").field("ts");
    	dhb.dateHistogramInterval(jvmIntervalMap.get(interval));
    	if(esAggregationType.equals("sum")) 
    	{
    	     dhb.subAggregation(AggregationBuilders.terms("srcName").field("src").subAggregation(AggregationBuilders.sum("reportAvg").field(esField)));
    	}
    	else
    	{
            dhb.subAggregation(AggregationBuilders.terms("srcName").field("src").subAggregation(AggregationBuilders.avg("reportAvg").field(esField)));
    	}
    	    
    	srb.addAggregation(dhb);
    	
    	SearchResponse response = srb.execute().actionGet();
    	
    	InternalDateHistogram dateHistrogram = response.getAggregations().get("avghistogram");
    	for(InternalDateHistogram.Bucket avgHistogram : dateHistrogram.getBuckets())
    	{
    		
            Object key = avgHistogram.getKey();
            Number ts = 0;
            if (key instanceof DateTime)
            {
                ts = ((DateTime) key).getMillis();
            }
            else
            {
                ts = (Number) key;
            }
    		
    		StringTerms terms = avgHistogram.getAggregations().get("srcName");
    		List<Bucket> buckets = terms.getBuckets();
    		if (buckets != null)
    		{
    			for (Bucket bucket : buckets)
    			{	    				
    				String src = bucket.getKeyAsString();
    				double value = 0;
    				if(esAggregationType.equals("sum"))
    				{
    				    InternalSum aggTerm = bucket.getAggregations().get("reportAvg");
    				    value = aggTerm.getValue();
    				}   
    				else
    				{
    				    InternalAvg aggTerm = bucket.getAggregations().get("reportAvg");
    				    value = aggTerm.getValue();
    				    if (avgDenominator != null)
                        {
    				        value = value / avgDenominator.intValue();
                        }
    				}    	    				
    				Map<String, Object> resultMap = new HashMap<String, Object>();
    				resultMap.put("ts", ts);
    				resultMap.put("src", src);
    				resultMap.put(dataField, value + "");
    				resultList.add(resultMap);
    			}
    		}
    	}
    	
    	// Find distinct source from ES
    	srb = SearchAdminAPI.getClient().prepareSearch(indexName);
        srb.setTypes(indexName);
        srb.setSize(0);
        srb.setQuery(queryBuilder);
    	srb.addAggregation(AggregationBuilders.terms("srcName").field("src").size(9999));
    	
    	response = srb.execute().actionGet();
    	
    	StringTerms terms = response.getAggregations().get("srcName");
    	List<Bucket> buckets = terms.getBuckets();
    	for (Bucket bucket : buckets)
    	{
    		if (!srcList.contains(bucket.getKey()))
    		{
    			srcList.add(bucket.getKeyAsString());
    		}
    	}
    }
    
    @SuppressWarnings("rawtypes")
	private static List readSQLReport(String sql, List resultList)
    {
    	try
    	{
       HibernateProxy.setCurrentUser("system");
    		resultList = (List) HibernateProxy.execute(() -> {
    			Query sqlQuery = HibernateUtil.createSQLQuery(sql);
        		return sqlQuery.list();
    		});
    		
    	}
    	catch(Exception e)
    	{
    		HibernateUtil.rethrowNestedTransaction(e);
    	}
    	return resultList;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResponseDTO<Map<String, Object>> getExecutionStatus(QueryDTO queryDTO)
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String,Object>>();
        try {
            //Get all manual duration and cost for each runbook in runbook properties.
            String sql = "select u_runbook, u_manual_duration, u_hourly_cost" +
                         " from dash_runbook_properties" + 
                         " where SYS_CREATED_ON in (select min(SYS_CREATED_ON) from dash_runbook_properties group by UPPER(u_runbook))";
            List sqlData = null;
            sqlData = readSQLReport(sql, sqlData);
            Map<String, Object> runbookInfoMap = new HashMap<String, Object>();
            if (sqlData != null)
            {
                for (Object obj : sqlData)
                {
                    Object[] objArray = (Object[]) obj;
                    String name = objArray[0].toString();
                    //Convert to second since duration in runbook_properties is in minute.
                    double duration = Double.parseDouble(objArray[1].toString()) * 60;
                    double cost = Double.parseDouble(objArray[2].toString());
                    Map<String, Double> runbookInfo = new HashMap<String, Double>();
                    runbookInfo.put("duration", duration);
                    runbookInfo.put("cost", cost);
                    runbookInfoMap.put(name.toLowerCase(), runbookInfo);
                }
            }
            
            //CALCULATE COST AND TIME SAVED ON COMPLETED RUNBOOK.
            BoolQueryBuilder queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            QueryBuilder termQuery = QueryBuilders.termQuery("taskName","end");
            queryBuilder.must(termQuery);            
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);   
            //Aggregations           
            String runbookNameAggsName = "Runbook_name";
            String runbookNameField = "wiki.raw";
            String totalDurationAggsName = "Total_duration";
            String totalDurationField = "duration";
            //Group by Name
            TermsAggregationBuilder runbookNameAggs = terms(runbookNameAggsName) .field(runbookNameField);
            //Total Execution Duration for each runbook
            SumAggregationBuilder totalDurationAggs = sum(totalDurationAggsName).field(totalDurationField);            
            runbookNameAggs.subAggregation(totalDurationAggs);            
            searchRequestBuilder.addAggregation(runbookNameAggs);            
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            
            StringTerms runbookNameAggsResult = response.getAggregations().get(runbookNameAggsName);
            int totalTimeSaved = 0;
            double totalCostSaved = 0.0;
            for(Bucket runbookBucket : runbookNameAggsResult.getBuckets()) {
                String runbookName = runbookBucket.getKeyAsString();
                if(runbookInfoMap.containsKey(runbookName.toLowerCase())) {
                    Map<String, Double> runbookInfo = (Map<String, Double>) runbookInfoMap.get(runbookName.toLowerCase());
                    //Total Time Save
                    int totalExecutions = (int) runbookBucket.getDocCount();
                    int totalManualDuration = (int) (runbookInfo.get("duration") * totalExecutions);
                    Sum totalDurationBucket = runbookBucket.getAggregations().get(totalDurationAggsName);
                    int totalExecutionDuration = (int) totalDurationBucket.getValue();
                    if(totalManualDuration > totalExecutionDuration) {
                        int timeSavedForCurrentRunbook = totalManualDuration - totalExecutionDuration;
                        totalTimeSaved += timeSavedForCurrentRunbook;
                        //Total Cost Save
                        totalCostSaved += (int) ( timeSavedForCurrentRunbook * runbookInfo.get("cost")  / 3600);
                    }
                }
            }
            
            //GET STATUS OF RUNBOOK
            queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST);
            searchRequestBuilder.setSearchType(SearchType.QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder); 
            //Aggregations
            String runbookStatusAggsName = "Process_status";
            String runbookStatusField = "status.keyword"; 
            //Group by Status.
            TermsAggregationBuilder runbookStatusAggs = terms(runbookStatusAggsName) .field(runbookStatusField);
            searchRequestBuilder.addAggregation(runbookStatusAggs);            
            response = searchRequestBuilder.execute().actionGet(); 
            //Handling return records
            Map<String, Long> runbookStatusList = new TreeMap<String, Long>();       
            StringTerms runbookStatusResult = response.getAggregations().get(runbookStatusAggsName);
            for(Bucket runbookStatusBucket : runbookStatusResult.getBuckets()) {
                String runbookStatus = runbookStatusBucket.getKeyAsString().toLowerCase();
                long totalRunbookWithStatus = runbookStatusBucket.getDocCount();
                runbookStatusList.put(runbookStatus, totalRunbookWithStatus);
            }
            
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("status", runbookStatusList);
            data.put("time_saved", totalTimeSaved);
            data.put("cost_saved", totalCostSaved);
            result.setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResponseDTO businessReportsMetaData(QueryDTO query, String reportType)
    {
        String sql = null;
        String tableName = null;    
        String fieldName = null;
        ResponseDTO result = new ResponseDTO();
        if (reportType.equals("runbook"))
        {
            tableName = "dash_runbook_properties";
            fieldName = "u_runbook";
        }
        else if (reportType.equals("actiontask"))
        {
            tableName = "dash_task_properties";
            fieldName = "u_name";
        }
        else
        {
            tableName ="dash_timer_properties";
            fieldName = "u_name";
        }
        String whereClause = " where SYS_CREATED_ON in (select min(SYS_CREATED_ON)from " + tableName + " group by UPPER(" + fieldName + "))";
        String sortClause = " order by " + fieldName;
        sql = "select " + fieldName + ", u_manual_duration, u_hourly_cost, u_display_name from " + tableName + whereClause + sortClause;
        
        Log.log.debug("user report SQL Data Query : " + sql);
        
        List sqlList = null;
        sqlList = readSQLReport(sql, sqlList);        
       
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        
        if (sqlList != null)
        {
            for (Object obj : sqlList)
            {
                Object[] objArray = (Object[]) obj;
                String u_name = objArray[0].toString();
                long u_manual_duration = Long.parseLong(objArray[1].toString());
                long u_hourly_cost = Long.parseLong(objArray[2].toString());
                String u_display_name = u_name;
                if (objArray[3]!=null)
                {
                	u_display_name = objArray[3].toString();
                }
                Map<String, Object> resultMap = new HashMap<String, Object>();
                resultMap.put("u_name", u_name);
                resultMap.put("u_manual_duration", u_manual_duration);  
                resultMap.put("u_hourly_cost", u_hourly_cost);
                resultMap.put("u_display_name", u_display_name);  
                resultList.add(resultMap);
            }
        }
        result.setTotal(resultList.size());
        result.setRecords(resultList);      
        
        return result;
    }

    @SuppressWarnings("unchecked")
    public static ResponseDTO<Map<String, Object>>  getDTReport(
                    HttpServletRequest request,
                    JSONObject payload,
                    HttpServletResponse response,
                    String type
                    ) throws ServletException, IOException
    {
        abstract class DTReport {
//            private int start;
//            private int page;
//            private int limit;
            private String filters;
            protected String from;
            protected String to;
            protected  String filterQuery;
            protected String query;
            protected String params;
            protected List<String> values;
            DTReport(String filters, String from, String to, int page, int start, int limit) {
//                this.page = page;
//                this.start = start;
//                this.limit = limit;
                this.filters = filters;
                this.from = from;
                this.to = to;
                this.createParamsValues();

            }
            public void setFilters(String filters) {
                this.filters = filters;
            }
            public void createParamsValues() {
                params = "";
                if (filters != null && !filters.isEmpty()) {
                    values = new LinkedList<String>();
                    String[] tokens = filters.split(",");
                    params += "(";
                    for(int i=0; i<tokens.length; i++) {
                        values.add(tokens[i]);
                        params += "?";
                        if (i < tokens.length-1) {
                            params += ",";
                        }
                    }
                    params += ")";
                }
            }
            public String getTableName() {
                return "tmetric_hr_dt";
            }

            public String getPaging() {
//                Integer start = this.start + page;
//                Integer nItems = limit;
//                return " LIMIT " + start.toString() + "," + nItems.toString();
                return "";
            }
            @SuppressWarnings("unused")
            public String getDataQuery() {
                return query;
            }
            public  String getFilterQuery() {
                return filterQuery;
            }
            public void parseFiltersFromResult(List<?> sqlList, ResponseDTO<Map<String, Object>> result) {
                List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
                for (Object obj : sqlList)
                {
                    Map<String, Object> resultMap = new HashMap<String, Object>();
                    resultMap.put("dtname", (String)obj);
                    records.add(resultMap);
                }
                result.setRecords(records).setSuccess(true).setTotal(records.size());
            }


            protected PreparedStatement createParameterizedQuery(String hqlQueryPrefix, String hqlQuerySuffix) {
                String query = hqlQueryPrefix + (values != null && !values.isEmpty() ? " IN " + params : "") + hqlQuerySuffix;
                
                Log.log.debug("createParameterizedQuery(" + hqlQueryPrefix + ", " + hqlQuerySuffix + ") query = " + query);
                
                PreparedStatement stmt = null;
                try
                {
                    stmt = HibernateUtil.getConnection().prepareStatement(query);
                    
                    if (values != null && !values.isEmpty())
                    {
                        for (int i=0; i<values.size(); i++) {
                            
                            String value = values.get(i);
                            
                            if (value != null && value.length() > 2 && value.startsWith("'") && value.endsWith("'")) {
                                value = value.substring(1, value.length() - 1);
                            }
                            
                            stmt.setString(i+1, value);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
                return stmt;
            }

            public PreparedStatement getPreparedStatement() {
                return null;
            }
            public void parseDataFromResult(ResultSet res, ResponseDTO<Map<String, Object>> result) {}
        }
        class DTReportNodeDuration extends DTReport {
            DTReportNodeDuration(String type, String filters, String from, String to, int page, int start, int limit) {
                super(filters, from, to, page, start, limit);
                filterQuery =  "SELECT DISTINCT nodeid FROM "
                                + this.getTableName()
                                + " WHERE LOWER(nodeid) NOT LIKE '%_decision%' AND tot_time > 0 AND ts < "
                                + to
                                + " AND ts > "
                                + from
                                + " ORDER BY nodeid "
                                + this.getPaging();
            }
            public PreparedStatement getPreparedStatement() {
                String hqlQueryPrefix = "SELECT avg(tot_time) as avg_duration, nodeid as node_name FROM "
                                + this.getTableName()
                                + " WHERE LOWER(nodeid) NOT LIKE '%_decision%' " +
                                (values != null && !values.isEmpty() ? "AND nodeid " : "");
                String hqlQuerySuffix = " AND ts < "
                                + to
                                + " AND ts > "
                                + from
                                + " GROUP BY nodeid HAVING avg(tot_time) > 0 ORDER BY avg(tot_time) DESC "
                                + this.getPaging();
                return createParameterizedQuery(hqlQueryPrefix, hqlQuerySuffix);
            }

            @Override
            public void parseDataFromResult(ResultSet rs, ResponseDTO<Map<String, Object>> result) {
                List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
                if (rs != null)
                {
                    try
                    {
                        while(rs.next())
                        {
                            String name = rs.getString("node_name");
                            long duration = rs.getLong("avg_duration");
                            Map<String, Object> resultMap = new HashMap<String, Object>();
                            resultMap.put("avg_duration", duration);
                            resultMap.put("node_name", name);
                            records.add(resultMap);
                        }
                    }
                    catch (SQLException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                result.setRecords(records).setSuccess(true).setTotal(records.size());
            }

            @Override
            public void parseFiltersFromResult(List<?> sqlList, ResponseDTO<Map<String, Object>> result) {
                List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
                for (Object obj : sqlList)
                {
                    Map<String, Object> resultMap = new HashMap<String, Object>();
                    resultMap.put("node_name", (String)obj);
                    records.add(resultMap);
                }
                result.setRecords(records).setSuccess(true).setTotal(records.size());
            }
        }

        class DTReportExecutionPath extends DTReport {
            DTReportExecutionPath(String type, String filters, String from, String to, int page, int start, int limit) {
                super(filters, from, to, page, start, limit);
                filterQuery = "SELECT DISTINCT nodeid FROM "
                                + this.getTableName()
                                + " WHERE seqid = 1 AND tot_time > 0 AND ts < "
                                + to
                                + " AND ts > "
                                + from
                                + " ORDER BY nodeid "
                                + this.getPaging();
            }
        }

        class DTReportExecutionTime extends DTReport {
            DTReportExecutionTime(String type, String filters, String from, String to, int page, int start, int limit) {
                super(filters, from, to, page, start, limit);
                filterQuery = "SELECT DISTINCT nodeid FROM "
                                + this.getTableName()
                                + " WHERE seqid = 1 AND ts < " + to +" AND ts > " + from +" ORDER BY nodeid "
                                + this.getPaging();
            }

            public PreparedStatement getPreparedStatement() {
                String hqlQueryPrefix = "SELECT ts, u_dt_root_doc as dtname, sum(tot_time)/tot_cnt as duration FROM "
                                + this.getTableName()
                                + " LEFT JOIN tmetric_lookup ON tmetric_hr_dt.pathid = tmetric_lookup.u_path_id"
                                + " WHERE " + 
                                (values != null && !values.isEmpty() ? "nodeid " : "");
                String hqlQuerySuffix = (values != null && !values.isEmpty() ? " AND " : "") + "ts < "
                                + to
                                +" AND ts > "
                                + from
                                + " GROUP BY ts, pathid, u_dt_root_doc, tot_cnt ORDER BY ts, u_dt_root_doc "
                                + this.getPaging();
                return createParameterizedQuery(hqlQueryPrefix, hqlQuerySuffix);
            }

            @Override
            public void parseDataFromResult(ResultSet rs, ResponseDTO<Map<String, Object>> result) {
                List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
                if (rs != null)
                {
                    try
                    {
                        while(rs.next())
                        {
                            long duration = rs.getLong("duration");
                            long ts = rs.getLong("ts");
                            String name = rs.getString("dtname");
                            Map<String, Object> resultMap = new HashMap<String, Object>();
                            resultMap.put("ts", ts);
                            resultMap.put("dtname", name);
                            resultMap.put("duration", duration);
                            records.add(resultMap);
                        }
                    }
                    catch (SQLException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                result.setRecords(records).setSuccess(true).setTotal(records.size());
            }
        }

        class DTReportExecutionCount extends DTReport {
            DTReportExecutionCount(String type, String filters, String from, String to, int page, int start, int limit) {
                super(filters, from, to, page, start, limit);
                filterQuery = "SELECT DISTINCT nodeid FROM "
                                + this.getTableName()
                                + " WHERE seqid = 1 AND ts < " + to +" AND ts > " + from +" ORDER BY nodeid "
                                + this.getPaging();
            }

            @Override
            public PreparedStatement getPreparedStatement()
            {
                String hqlQueryPrefix = "SELECT sum(tot_cnt) as count, ts, nodeid FROM " 
                                + this.getTableName()
                                + " WHERE seqid = 1 " +
                                (values != null && !values.isEmpty() ? "AND nodeid " : "");
                String hqlQuerySuffix = " AND ts < "
                                + to
                                + " AND ts > "
                                + from
                                + " GROUP BY ts, nodeid ORDER BY ts "
                                + this.getPaging();
                return createParameterizedQuery(hqlQueryPrefix, hqlQuerySuffix);
            }

            @Override
            public void parseDataFromResult(ResultSet rs, ResponseDTO<Map<String, Object>> result) {
                List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
                if (rs != null)
                {
                    try
                    {
                        while(rs.next())
                        {
                            long count = rs.getLong("count");
                            long ts = rs.getLong("ts");
                            String name = rs.getString("nodeid");
                            Map<String, Object> resultMap = new HashMap<String, Object>();
                            resultMap.put("count", count);
                            resultMap.put("ts", ts);
                            resultMap.put("dtname", name);
                            records.add(resultMap);
                        }
                    }
                    catch (SQLException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                result.setRecords(records).setSuccess(true).setTotal(records.size());
            }
        }
        class DTReportTop extends DTReport {
            private String secondFilter;
            DTReportTop(String type, String filters, String filters1, String from, String to, int page, int start, int limit) {
                super(filters, from, to, page, start, limit);
                secondFilter = filters1; 
                filterQuery = "SELECT u_namespace, wikidoc.sys_created_by FROM "
                                + this.getTableName()
                                + " LEFT JOIN tmetric_lookup ON pathid = u_path_id LEFT JOIN wikidoc ON nodeid = u_fullname "
                                + " WHERE seqid = 1 AND ts < "
                                + to 
                                + " AND ts > "
                                + from 
                                + " AND u_namespace IS NOT NULL"
                                + " ORDER BY nodeid ";
            }

            @Override
            public PreparedStatement getPreparedStatement()
            {
                this.createParamsValues();
                String params1 = this.params;
                List<String> values1 = this.values;
                this.setFilters(secondFilter);
                this.createParamsValues();
                String param2 = this.params;
                
                if (StringUtils.isBlank(params1) || StringUtils.isBlank(param2))
                {
                    return null;
                }
                List<String> values2 = this.values;
                String queryStr = "SELECT sum(tot_cnt) as count, nodeid as dtname, wikidoc.sys_created_by as author FROM "
                                + this.getTableName()
                                + " LEFT JOIN tmetric_lookup ON pathid = u_path_id LEFT JOIN wikidoc ON u_dt_root_doc = u_fullname "
                                + " WHERE seqid = 1 AND u_namespace IN "
                                + params1
                                + " AND wikidoc.sys_created_by IN "
                                + param2
                                + " AND ts < "
                                + to
                                + " AND ts > "
                                + from 
                                + " GROUP BY nodeid, wikidoc.sys_created_by ORDER BY sum(tot_cnt)";
                PreparedStatement stmt = null;
                try
                {
                    stmt = HibernateUtil.getConnection().prepareStatement(queryStr);
                    int i;
                    for (i=0; i<values1.size(); i++) {
                        stmt.setString(i+1, values1.get(i));
                    }
                    for (int j=0; j<values2.size(); j++) {
                        stmt.setString(i+j+1, values2.get(j));
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
                return stmt;
            }

            @Override
            public void parseDataFromResult(ResultSet rs, ResponseDTO<Map<String, Object>> result) {
                List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
                if (rs != null)
                {
                    try
                    {
                        while(rs.next())
                        {
                            long count = rs.getLong("count");
                            String author = rs.getString("author");
                            String name = rs.getString("dtname");
                            Map<String, Object> resultMap = new HashMap<String, Object>();
                            resultMap.put("count", count);
                            resultMap.put("dtname", name);
                            resultMap.put("author", author);
                            records.add(resultMap);
                        }
                    }
                    catch (SQLException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                result.setRecords(records).setSuccess(true).setTotal(records.size());
            }

            @Override
            public void parseFiltersFromResult(List<?> sqlList, ResponseDTO<Map<String, Object>> result) {
                List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
                for (Object obj : sqlList)
                {
                    Object[] objArray = (Object[]) obj;
                    String name = (String)objArray[0];
                    String author = (String)objArray[1];
                    Map<String, Object> resultMap = new HashMap<String, Object>();
                    resultMap.put("namespace", name);
                    resultMap.put("author", author);
                    records.add(resultMap);
                }
                result.setRecords(records).setSuccess(true).setTotal(records.size());
            }
        }
        class DTReportFactory {
            public final DTReport create(Map<String, Object> paramsPayload) {
                String type = (String)paramsPayload.get("reportType");
                String filters = (String)paramsPayload.get("filterName");
                String filters1 = (String)paramsPayload.get("filterName1");
                String from = paramsPayload.get("from").toString();
                String to = paramsPayload.get("to").toString();
                int page = (int)paramsPayload.get("page");
                int start = (int)paramsPayload.get("start");
                int limit = (int)paramsPayload.get("limit");

                if (type.equalsIgnoreCase("Execution Count")) {
                    return new DTReportExecutionCount(type, filters, from, to, page, start, limit);
                } else if (type.equalsIgnoreCase("Execution Time")) {
                    return new DTReportExecutionTime(type, filters, from, to, page, start, limit);
                } else if (type.equalsIgnoreCase("Execution Path")) {
                    return new DTReportExecutionPath(type, filters, from, to, page, start, limit);
                } else if (type.equalsIgnoreCase("Node Duration")) {
                    return new DTReportNodeDuration(type, filters, from, to, page, start, limit);
                } else if(type.equalsIgnoreCase("Top")) {
                    return new DTReportTop(type, filters, filters1, from, to, page, start, limit);
                }
                else {
                    return null;
                }
            }
        }

        String json = payload.toString();
        ObjectMapper mapper = new ObjectMapper();
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        try
        {
            Map<String, Object> paramsPayload = mapper.readValue(json, Map.class);
            DTReport dtReport = new DTReportFactory().create(paramsPayload);                    
            
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	List<?> sqlList = null;    
            	 if (type.equalsIgnoreCase("data")) {
                     PreparedStatement query = dtReport.getPreparedStatement();
                     if (query != null)
                     {
                         ResultSet res = query.executeQuery();
                         dtReport.parseDataFromResult(res, result);
                     }
                 } else {
                     sqlList = readSQLReport(dtReport.getFilterQuery(), sqlList);
                     dtReport.parseFiltersFromResult(sqlList, result);
                 }
            });
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    }
}
