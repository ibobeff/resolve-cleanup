/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.social;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexConfiguration;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.RateIndexer;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.SocialComment;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.SocialTarget;
import com.resolve.search.model.UserInfo;
import com.resolve.search.model.query.Query;
import com.resolve.search.query.QueryIndexer;
import com.resolve.search.tag.TagIndexAPI;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialIndexAPI
{
    private static final Indexer<IndexData<Map<String, Object>>> indexer = SocialIndexer.getInstance();

    private static final IndexAPI postIndexAPI = APIFactory.getSocialPostIndexAPI();
    private static final IndexAPI postCommentIndexAPI = APIFactory.getSocialCommentIndexAPI();
    private static final SearchAPI<SocialPost> postSearchAPI = APIFactory.getSocialPostSearchAPI(SocialPost.class);
    private static final SearchAPI<SocialComment> postCommentSearchAPI = APIFactory.getSocialCommentSearchAPI(SocialComment.class);

    private static final IndexAPI systemIndexAPI = APIFactory.getSystemMessageIndexAPI();
    private static final IndexAPI systemCommentIndexAPI = APIFactory.getSystemMessageCommentIndexAPI();
    private static final SearchAPI<SocialPost> systemSearchAPI = APIFactory.getSystemMessageSearchAPI(SocialPost.class);
    private static final SearchAPI<SocialComment> systemCommentSearchAPI = APIFactory.getSystemMessageCommentSearchAPI(SocialComment.class);

    private static final IndexAPI socialQueryIndexAPI = APIFactory.getSocialQueryIndexAPI();
    private static final SearchAPI<? extends Query> socialQuerySearchAPI = APIFactory.getSocialQuerySearchAPI();
    private static final RateIndexer<IndexData<String>> queryIndexer = new QueryIndexer(SearchAdminAPI.getSearchConfig(), socialQueryIndexAPI, socialQuerySearchAPI, SearchConstants.USER_QUERY_TYPE_SOCIAL, true);

    public static void moveSocialPost(String postId, TreeSet<SocialTarget> newTargets, String username) throws SearchException
    {
        SocialPost socialPost = SocialSearchAPI.findSocialPostById(postId, username);
        if (socialPost != null)
        {
            indexSocialPost(socialPost, newTargets, username, null, false);
        }
    }

    public static synchronized void indexSocialPost(SocialPost socialPost, TreeSet<SocialTarget> targets, String username, String postType, boolean updateQuery) throws SearchException
    {
        if (targets == null)
        {
            throw new SearchException("post doesn't belong to any target/stream, check the code so it sends target/stream RSComponent sys id.");
        }
        else
        {
            setPostTypesToTargets(postType, socialPost, targets);
            // we are ready to index
            indexSocialPost(socialPost, username, postType, updateQuery);
        }
    }

    public static void addNewTargets(final String postId, final TreeSet<SocialTarget> targets, final String username) throws SearchException
    {
        SocialPost socialPost = SocialSearchAPI.findSocialPostById(postId, username);
        if (socialPost == null)
        {
            Log.log.warn("addNewTargets failed, invalid post id: " + postId);
        }
        else
        {
            for(SocialTarget target : targets)
            {
                socialPost.getTargets().add(target);
            }
            
            indexSocialPost(socialPost, username, socialPost.getType(), false);
        }
    }

    /**
     * This method replaces old tag with new tag in the content of a post or
     * comment. Since changing tag name is a rare event this mechanism is better
     * than replacing content on the fly with the latest Tag name everytime
     * someone requests the content. ElasticSearch team has been working on a
     * new functionality where it'll let us replace the tag by query itself
     * passing a script. When that becomes available we will not have to make
     * query, then replace and then update the index. It'll be taken care off by
     * ES itself.
     * 
     * @param oldTagName
     * @param newTagName
     * @param username
     * @throws SearchException
     */
    public static void replaceTagName(String oldTagName, String newTagName, String username) throws SearchException
    {
        QueryDTO queryDTO = new QueryDTO(0, 1);

        QueryBuilder queryBuilder = QueryBuilders.simpleQueryStringQuery(oldTagName).field("content");

        // find posts
        long total = postSearchAPI.getCountByQuery(queryBuilder);
        int start = 0;
        int limit = 100;

        if (total > 0)
        {
            for (int i = start; i <= total; i += limit)
            {
                queryDTO = new QueryDTO(i, limit);
                ResponseDTO<SocialPost> response = postSearchAPI.searchByQuery(queryDTO, queryBuilder, username);
                Collection<SocialPost> posts = new ArrayList<SocialPost>();
                for (SocialPost post : response.getRecords())
                {
                    post.setContent(post.getContent().replaceAll(oldTagName, newTagName));
                    posts.add(post);
                }
                // do a bulk update
                // verifies and create Tag is not available
                verifyTagInPost(posts, username);
                postIndexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, posts, false, username);
            }
        }

        // now check comments
        total = postSearchAPI.getCountByQuery(queryBuilder);
        start = 0;
        limit = 100;

        if (total > 0)
        {
            for (int i = start; i <= total; i += limit)
            {
                queryDTO = new QueryDTO(i, limit);
                ResponseDTO<SocialComment> response = postCommentSearchAPI.searchByQuery(queryDTO, queryBuilder, username);
                Collection<SocialComment> comments = new ArrayList<SocialComment>();
                for (SocialComment comment : response.getRecords())
                {
                    comment.setContent(comment.getContent().replaceAll(oldTagName, newTagName));
                    comments.add(comment);
                }
                // do a bulk update
                indexSocialComment(comments, false, username, SearchConstants.SOCIALPOST_TYPE_POST);
            }
        }
    }

    /**
     * Index or updates an existing comment.
     * 
     * @param comments
     * @param username
     * @return the sysId of the comment.
     * @throws SearchException
     */
    public static List<String> indexSocialComment(final Collection<SocialComment> comments, final boolean updateCount, final String username, final String postType) throws SearchException
    {
        List<String> result = null;

        if (comments != null)
        {
            if (SearchConstants.SOCIALPOST_TYPE_SYSTEM.equalsIgnoreCase(postType) || SearchConstants.SOCIALPOST_TYPE_MANUAL.equalsIgnoreCase(postType))
            {
                result = systemCommentIndexAPI.indexChildDocuments("postId", SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, comments, username, false);
            }
            else
            {
                result = postCommentIndexAPI.indexChildDocuments("postId", SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, comments, username, false);
                for (SocialComment socialComment : comments)
                {
                    // simply update the post so the post's sysUpdatedOn is
                    // refreshed
                    SocialPost socialPost = SocialSearchAPI.findSocialPostById(socialComment.getPostId(), username);
                    if (socialPost != null)
                    {
                        if(socialComment.getSysUpdatedOn() == null)
                        {
                            socialPost.setSysUpdatedOn(DateUtils.GetUTCDateLong());
                        }
                        else
                        {
                            if(socialComment.getSysUpdatedOn() > socialPost.getSysUpdatedOn())
                            {
                                socialPost.setSysUpdatedOn(socialComment.getSysUpdatedOn());
                            }
                        }
                        // since there is a new comment we are making the post
                        // unread for all people
                        // so they can see the post as unread.
                        socialPost.getReadUsernames().clear();
                        socialPost.getReadUsernames().add(username);
                        socialPost.setNumberOfReads(0);
                        if (updateCount)
                        {
                            socialPost.setNumberOfComments(socialPost.getNumberOfComments() + 1);
                        }
                        indexSocialPost(socialPost, username, postType, false);
                    }
                }
            }
        }

        return result;
    }

    public static void indexSocialComment(final SocialComment comment, final String username, final String postType) throws SearchException
    {
        indexSocialComment(comment, true, username, postType);
    }

    public static void indexSocialComment(final SocialComment comment, final boolean updateCount, final String username, final String postType) throws SearchException
    {
        if (StringUtils.isBlank(comment.getPostId()))
        {
            throw new SearchException("SocialComment does not have its post ID: " + comment.toString());
        }
        else
        {
            indexSocialComment(Arrays.asList(comment), updateCount, username, postType);
        }
    }

    public static String indexSocialPost(final SocialPost post, final String username, final String postType, final boolean updateQuery) throws SearchException
    {
        String result = null;

        SocialPost tmpPost = post;

        if (SearchConstants.SOCIALPOST_TYPE_SYSTEM.equalsIgnoreCase(postType) || SearchConstants.SOCIALPOST_TYPE_MANUAL.equalsIgnoreCase(postType))
        {
            ResponseDTO<SocialPost> systemPost = new ResponseDTO<SocialPost>();

            if (SearchConstants.SOCIALPOST_TYPE_SYSTEM.equalsIgnoreCase(postType))
            {
                // if this is a system generated post then find the content if it already exists
                // if it does then simply update the created/modified date. the field content.exact
                // is a non-analized field so it provides exact comparison.
                QueryDTO queryDTO = new QueryDTO(0, 1);
                queryDTO.addFilterItem(new QueryFilter("content.exact", QueryFilter.EQUALS, post.getContent()));
                systemPost = systemSearchAPI.searchByQuery(queryDTO, username);
            }

            // there is already a system post with this title, so we'll simply
            // update it
            if (systemPost.getRecords() != null && systemPost.getRecords().size() >= 1)
            {
                tmpPost = systemPost.getRecords().get(0);
                post.setSysId(tmpPost.getSysId());
                // we do not want any fields to strip html
                result = systemIndexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, post, true, username);
            }
            else
            {
                //save the type, it helps during search filter
                tmpPost.setType(postType.toLowerCase());
                // system messages are locked from adding comment etc.
                tmpPost.setLocked(true);
                // we do not want any fileds to strip html
                result = systemIndexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, tmpPost, true, username);
            }
        }
        else
        {
            if (SearchConstants.SOCIALPOST_TYPE_RSS.equals(post.getType()))
            {
                result = postIndexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, tmpPost, true, username);
            }
            else
            {
                if(tmpPost.getType() == null)
                {
                    tmpPost.setType(SearchConstants.SEARCH_TYPE_POST);
                }
                
                setPostTypesToTargets(postType, tmpPost, tmpPost.getTargets());

                result = postIndexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, tmpPost, true, username);
                // not going to accept RSS for query suggestion, could get ugly
                // pretty quick
                if (updateQuery)
                {
                    Collection<String> queries = Arrays.asList(post.getTitle(), post.getContent());
                    queryIndexer.indexQueries(post.getSysId(), queries, post.getSysUpdatedOn(), true, username);
                }
            }
        }

        return result;
    }

    private static void setPostTypesToTargets(final String postType, SocialPost socialPost, TreeSet<SocialTarget> targets) throws SearchException
    {
        TreeSet<SocialTarget> socialTargets = new TreeSet<SocialTarget>();
        for (SocialTarget target : targets)
        {
            if (target == null || StringUtils.isBlank(target.getSysId()))
            {
                throw new SearchException("This target is either null or does not have sys id, check it out.");
            }
            if(target.getSysId().equals(socialPost.getAuthorSysId()))
            {
                target.setPostType(SearchConstants.SOCIALPOST_TYPE_BLOG);
            }
            else if("USER".equalsIgnoreCase(target.getType()))
            {
                target.setPostType(SearchConstants.SOCIALPOST_TYPE_INBOX);
            }
            else
            {
                target.setPostType(postType);
            }
            socialTargets.add(target);
        }
        // add targets to the post
        socialPost.setTargets(socialTargets);
    }

    public static void deletePostById(final String postId, final String username) throws SearchException
    {
        if (StringUtils.isBlank(postId))
        {
            Log.log.debug("The postId must be provided, check the calling code why is it like that.");
        }
        else
        {
            // first delete all social comments by postId.
            QueryBuilder queryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("postId", postId));
            postCommentIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            // now delete the post
            postIndexAPI.deleteDocumentById(postId, username);
        }
    }

    public static void deleteSocialCommentById(final String commentId, final String username) throws SearchException
    {
        if (StringUtils.isBlank(commentId))
        {
            Log.log.debug("The commentId must be provided, check the calling code why is it like that.");
        }
        else
        {
            // delete the social comment.
            QueryBuilder queryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("sysId", commentId));
            postCommentIndexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
    }

    public static void setRead(final String postId, final String username, final boolean isRead) throws SearchException
    {
        SocialPost socialPost = SocialSearchAPI.findSocialPostById(postId, username);
        if (socialPost == null)
        {
            Log.log.warn("setRead failed, invalid post id: " + postId);
        }
        else
        {
            setRead(socialPost, username, isRead);
        }
    }

    public static void setRead(final SocialPost socialPost, final String username, final boolean isRead) throws SearchException
    {
        if (socialPost == null)
        {
            Log.log.warn("setRead failed, invalid post");
        }
        else
        {
            long totalCount = socialPost.getReadUsernames().size();
            if (isRead)
            {
                socialPost.addReadUsername(username);
                totalCount += 1;
            }
            else
            {
                socialPost.removeReadUsername(username);
                totalCount -= 1;
            }
            // don't count self read.
            /*
             * if
             * (socialPost.getReadUsernames().contains(socialPost.getAuthorUsername
             * ())) { totalCount -= 1; }
             */
            socialPost.setNumberOfReads(totalCount);
            indexSocialPost(socialPost, username, socialPost.getType(), false);
        }
    }

    public static void setLikedPost(final String id, final String username, final boolean isLiked) throws SearchException
    {
        SocialPost socialPost = SocialSearchAPI.findSocialPostById(id, username);
        if (socialPost == null)
        {
            Log.log.warn("likePost failed, invalid post id: " + id);
        }
        else
        {
            long totalCount = socialPost.getLikedUsernames().size();
            if (isLiked)
            {
                socialPost.addLikedUsername(username);
                totalCount += 1;
            }
            else
            {
                socialPost.removeLikedUsername(username);
                totalCount -= 1;
            }
            // don't count self read. we are letting user likes own posts
            // if
            // (socialPost.getLikedUsernames().contains(socialPost.getAuthorUsername()))
            // {
            // totalCount -= 1;
            // }
            socialPost.setNumberOfLikes(totalCount);
            indexSocialPost(socialPost, username, socialPost.getType(), false);
        }
    }

    public static void setLikedComment(final String id, final String parentId, final String username, final boolean isLiked) throws SearchException
    {
        SocialComment socialComment = SocialSearchAPI.findSocialCommentById(id, username);
        if (socialComment == null)
        {
            Log.log.warn("likeComment failed, invalid comment id: " + id);
        }
        else
        {
            long totalCount = socialComment.getLikedUsernames().size();
            if (isLiked)
            {
                socialComment.addLikedUsername(username);
                totalCount += 1;
            }
            else
            {
                socialComment.removeLikedUsername(username);
                totalCount -= 1;
            }
            // make sure the author don't start liking his/her own comments to
            // increase influence
            /*
             * if (socialComment.getLikedUsernames().contains(socialComment.
             * getCommentAuthorUsername())) { totalCount -= 1; }
             */
            socialComment.setNumberOfLikes(totalCount);
            indexSocialComment(socialComment, false, username, SearchConstants.SOCIALPOST_TYPE_POST);
        }
    }

    public static void setStarred(final String postId, final String username, final boolean isStarred) throws SearchException
    {
        SocialPost socialPost = SocialSearchAPI.findSocialPostById(postId, username);
        if (socialPost == null)
        {
            Log.log.warn("setStarred failed, invalid post id: " + postId);
        }
        else
        {
            long totalCount = socialPost.getStarredUsernames().size();
            if (isStarred)
            {
                socialPost.addStarredUsername(username);
                totalCount += 1;
            }
            else
            {
                socialPost.removeStarredUsername(username);
                totalCount -= 1;
            }
            /*
             * if (socialPost.getStarredUsernames().contains(socialPost.
             * getAuthorUsername())) { totalCount -= 1; }
             */
            socialPost.setNumberOfStarred(totalCount);
            indexSocialPost(socialPost, username, socialPost.getType(), false);
        }
    }

    public static void setAnswer(final String id, final String username, final boolean isAnswer) throws SearchException
    {
        SocialComment socialComment = SocialSearchAPI.findSocialCommentById(id, username);
        if (socialComment == null)
        {
            Log.log.warn("setAnswer failed, invalid comment id: " + id);
        }
        else
        {
            socialComment.setAnswer(isAnswer);
            indexSocialComment(socialComment, false, username, SearchConstants.SOCIALPOST_TYPE_POST);
        }
    }

    public static void setLocked(final String postId, final String username, final boolean isLocked) throws SearchException
    {
        if (StringUtils.isBlank(postId))
        {
            Log.log.warn("setLocked failed, invalid post id: " + postId);
        }
        else
        {
            SocialPost socialPost = SocialSearchAPI.findSocialPostById(postId, username);
            if(socialPost != null)
            {
                socialPost.setLocked(isLocked);;
                postIndexAPI.indexDocument(socialPost, username);
            }
        }
    }

    public static void setDeleted(final String postId, final String username, final boolean isDeleted) throws SearchException
    {
        if (StringUtils.isBlank(postId))
        {
            Log.log.warn("setDeleted failed, invalid post id: " + postId);
        }
        else
        {
            SocialPost socialPost = SocialSearchAPI.findSocialPostById(postId, username);
            if(socialPost != null)
            {
                socialPost.setDeleted(isDeleted);;
                postIndexAPI.indexDocument(socialPost, username);
            }
        }
    }

    private static void verifyTagInPost(Collection<SocialPost> posts, String username) throws SearchException
    {
        for (SocialPost post : posts)
        {
            TagIndexAPI.verifyTag(post.getContent(), username);
        }
    }

    private static void verifyTagInComment(Collection<SocialComment> comments, String username) throws SearchException
    {
        for (SocialComment comment : comments)
        {
            TagIndexAPI.verifyTag(comment.getContent(), username);
        }
    }

    /**
     * This is going to reindex the whole social queries (suggester) by reading
     * the contents from the the posts.
     * 
     * @param username
     * @throws SearchException
     */
    public static void updateAllQueryRating(String username) throws SearchException
    {
        Collection<String> data = new LinkedList<String>();
        data.add("0");
        IndexData<String> indexData = new IndexData<String>(data, OPERATION.RATE, username);
        queryIndexer.enqueue(indexData);
    }

    public static void markPostAsRead(QueryDTO query, String streamId, String streamType, TreeSet<String> targetIds, UserInfo user) throws Exception
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("OPERATION", "SET_READ");
        params.put("QUERY", query);
        params.put("STREAM_ID", streamId);
        params.put("STREAM_TYPE", streamType);
        params.put("TARGET_IDS", targetIds);
        params.put("USER", user);
        params.put("IS_READ", Boolean.TRUE);

        IndexData<Map<String, Object>> indexData = new IndexData<Map<String, Object>>(Arrays.asList(params), OPERATION.CUSTOM, true, user.getUsername());
        indexer.enqueue(indexData);
    }
    
    
    public static boolean setRating(final String sysId, final String username, final double rating) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("setRating failed, invalid post sys id: " + sysId);
        }
        else
        {
            SocialPost socialPost = SocialSearchAPI.findSocialPostById(sysId, username);
            if(socialPost != null)
            {
                socialPost.setRating(socialPost.getRating() == null ? rating : socialPost.getRating() + rating);
                postIndexAPI.indexDocument(socialPost, username);
                result = true;
            }
        }
        return result;
    }

    public static boolean increaseClickCount(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("increaseClickCount failed, invalid post sys id: " + sysId);
        }
        else
        {
            SocialPost socialPost = SocialSearchAPI.findSocialPostById(sysId, username);
            if(socialPost != null)
            {
                socialPost.setClickCount(socialPost.getClickCount() == null ? 1 : socialPost.getClickCount() + 1);
                postIndexAPI.indexDocument(socialPost, username);
                result = true;
            }
        }
        return result;
    }

    public static boolean resetRating(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("resetRating failed, invalid post sys id: " + sysId);
        }
        else
        {
            SocialPost socialPost = SocialSearchAPI.findSocialPostById(sysId, username);
            if(socialPost != null)
            {
                socialPost.setRating(null);
                postIndexAPI.indexDocument(socialPost, username);
                result = true;
            }
        }
        return result;
    }

    public static boolean resetClickCount(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("resetClickCount failed, invalid post sys id: " + sysId);
        }
        else
        {
            SocialPost socialPost = SocialSearchAPI.findSocialPostById(sysId, username);
            if(socialPost != null)
            {
                socialPost.setClickCount(null);
                postIndexAPI.indexDocument(socialPost, username);
                result = true;
            }
        }
        return result;
    }
    

    /**
     * This method is used to update existing social index with new mapping etc.
     * While running this there should not be any transaction happenning to the index
     * because this method will copy the data to a new one and drop the index and 
     * recreate it with new mapping and then copy the data over.
     *   
     * @param indexName
     * @param mappingName
     */
    public static void updateIndex(String indexName, int batchSize, String username)
    {
        List<IndexConfiguration> defaultIndexConfigurations = new ArrayList<IndexConfiguration>();
        
        String commonSettingsJson = SearchUtils.readJson("common_settings.json");

        Map<String, String> mappings = new HashMap<String, String>();
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, SearchUtils.readJson("social_posts_mappings.json"));
        mappings.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, SearchUtils.readJson("social_comments_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_SOCIAL_POST, commonSettingsJson, mappings));

        // system messages social posts
        Map<String, String> systemMappings = new HashMap<String, String>();
        systemMappings.put(SearchConstants.DOCUMENT_TYPE_SYSTEM_POST, SearchUtils.readJson("system_posts_mappings.json"));
        systemMappings.put(SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT, SearchUtils.readJson("system_comments_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_SYSTEM_POST, commonSettingsJson, systemMappings));

        try
        {
            long currentTime = System.currentTimeMillis();
            
            if(StringUtils.isBlank(indexName))
            {
                indexName = "all";
            }
            if("all".equalsIgnoreCase(indexName) || SearchConstants.INDEX_NAME_SOCIAL_POST.equalsIgnoreCase(indexName))
            {
                Log.log.info("Updating " + SearchConstants.INDEX_NAME_SOCIAL_POST + " started");
                String tmpPostIndex = SearchConstants.INDEX_NAME_SOCIAL_POST+currentTime;
                SearchAdminAPI.createIndex(tmpPostIndex, commonSettingsJson, mappings, 1, 0);
                postIndexAPI.moveData(SearchConstants.INDEX_NAME_SOCIAL_POST, tmpPostIndex, SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, batchSize, username);
                postCommentIndexAPI.moveData(SearchConstants.INDEX_NAME_SOCIAL_POST, tmpPostIndex, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, batchSize, username);
                //remove existing index
                Log.log.info("Deleting index " + SearchConstants.INDEX_NAME_SOCIAL_POST);
                SearchAdminAPI.removeIndex(SearchConstants.INDEX_NAME_SOCIAL_POST);
                //recreate
                Log.log.info("Recreating index " + SearchConstants.INDEX_NAME_SOCIAL_POST + " with latest mappings");
                SearchAdminAPI.createIndex(SearchConstants.INDEX_NAME_SOCIAL_POST, commonSettingsJson, mappings);
                //move the data back
                Log.log.info("Moving back data for " + SearchConstants.DOCUMENT_TYPE_SOCIAL_POST);
                postIndexAPI.moveData(tmpPostIndex, SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, batchSize, username);
                Log.log.info("Moving back data for " + SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT);
                postCommentIndexAPI.moveData(tmpPostIndex, SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, batchSize, username);
                //remove temporary indexes
                SearchAdminAPI.removeIndex(tmpPostIndex);
                Log.log.info("Finished updating " + SearchConstants.INDEX_NAME_SOCIAL_POST);
            }
            
            if("all".equalsIgnoreCase(indexName) || SearchConstants.INDEX_NAME_SYSTEM_POST.equalsIgnoreCase(indexName))
            {
                Log.log.info("Updating " + SearchConstants.INDEX_NAME_SYSTEM_POST + " started");
                String tmpSystemIndex = SearchConstants.INDEX_NAME_SYSTEM_POST+currentTime;
                SearchAdminAPI.createIndex(tmpSystemIndex, commonSettingsJson, systemMappings, 1, 0);
                systemIndexAPI.moveData(SearchConstants.INDEX_NAME_SYSTEM_POST, tmpSystemIndex, SearchConstants.DOCUMENT_TYPE_SYSTEM_POST, batchSize, username);
                systemCommentIndexAPI.moveData(SearchConstants.INDEX_NAME_SYSTEM_POST, tmpSystemIndex, SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT, batchSize, username);
                //remove existing index
                Log.log.info("Deleting index " + SearchConstants.INDEX_NAME_SYSTEM_POST);
                SearchAdminAPI.removeIndex(SearchConstants.INDEX_NAME_SYSTEM_POST);
                //recreate
                Log.log.info("Recreating index " + SearchConstants.INDEX_NAME_SYSTEM_POST + " with latest mappings");
                SearchAdminAPI.createIndex(SearchConstants.INDEX_NAME_SYSTEM_POST, commonSettingsJson, systemMappings);
                //move the data back
                Log.log.info("Moving back data for " + SearchConstants.DOCUMENT_TYPE_SYSTEM_POST);
                systemIndexAPI.moveData(tmpSystemIndex, SearchConstants.INDEX_NAME_SYSTEM_POST, SearchConstants.DOCUMENT_TYPE_SYSTEM_POST, batchSize, username);
                Log.log.info("Moving back data for " + SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT);
                systemCommentIndexAPI.moveData(tmpSystemIndex, SearchConstants.INDEX_NAME_SYSTEM_POST, SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT, batchSize, username);
                //remove temporary indexes
                SearchAdminAPI.removeIndex(tmpSystemIndex);
                Log.log.info("Finished updating " + SearchConstants.INDEX_NAME_SYSTEM_POST);
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void massageSocialPost(int batchSize, String username) throws SearchException
    {
        QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
        
        SearchResponse scrollResponse = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_SOCIAL_POST)
                        .setTypes(SearchConstants.DOCUMENT_TYPE_SOCIAL_POST)
//                        .setSearchType(SearchType.SCAN)
                        .addSort("_doc", SortOrder.ASC)
                        .setScroll(new TimeValue(1l, TimeUnit.MINUTES))
                        .setQuery(queryBuilder)
                        .setSize(batchSize).execute().actionGet(); //1000 hits per shard will be returned for each scroll
        Log.log.debug("Total records found :" + scrollResponse.getHits().getTotalHits());
        //Scroll until no hits are returned
        while (true) 
        {
            scrollResponse = SearchAdminAPI.getClient().prepareSearchScroll(scrollResponse.getScrollId())
            				 .setScroll(new TimeValue(1l, TimeUnit.MINUTES)).execute().actionGet();
            for (SearchHit hit : scrollResponse.getHits()) 
            {
                Log.log.debug("Massaging record with Id : " + hit.getId());
                SocialPost socialPost = SocialSearchAPI.findSocialPostById(hit.getId(), username);
                indexSocialPost(socialPost, username, socialPost.getType(), false);
                Log.log.debug("Massaged Target with postType");
                for(SocialTarget target : socialPost.getTargets())
                {
                    Log.log.debug("Massaged Target: " + target.toString());
                }
            }
              
            //Break condition: No hits are returned
            if (scrollResponse.getHits().getHits().length == 0) 
            {
                break;
            }
        }
    }
}
