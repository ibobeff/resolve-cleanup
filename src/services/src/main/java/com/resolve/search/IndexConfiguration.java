/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to send index configurations from various component.
 * The configurations are some JSON content that defines index settings
 * and mappings for a particular index. Refer to worksheets_settings.json
 * and worksheets_mappings.json in com.resolve.search.resources package.
 * This a way to define index declaratively.
 */
public class IndexConfiguration
{
    private String indexName;
    private String settings;
    //documenttype (socialpost, socialcoment) and mappings json
    private Map<String, String> mappings = new HashMap<String, String>();

    public IndexConfiguration(final String indexName, final String settings, final Map<String, String> mappings)
    {
        this.indexName = indexName;
        this.settings = settings;
        this.mappings = mappings;
    }
    public String getIndexName()
    {
        return indexName;
    }

    public void setIndexName(String indexName)
    {
        this.indexName = indexName;
    }

    public String getSettings()
    {
        return settings;
    }

    public void setSettings(String settings)
    {
        this.settings = settings;
    }

    public Map<String, String> getMappings()
    {
        return mappings;
    }

    public void setMappings(Map<String, String> mappings)
    {
        this.mappings = mappings;
    }
}
