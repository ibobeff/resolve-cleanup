/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.worksheet;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.BaseIndexModel;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.ExecutionSummary;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.model.WorksheetData;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.ActivityVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;

public class WorksheetSearchAPI
{
	public static final String START_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	
    public static String PROCESSREQUEST_STATUS = "PROCESSREQUEST_STATUS";
    public static String PROCESSREQUEST_UPDATEDON = "PROCESSREQUEST_UPDATEDON";
    private static final SearchAPI<Worksheet> worksheetSearchApi = APIFactory.getWorksheetSearchAPI(Worksheet.class);
    private static final SearchAPI<WorksheetData> worksheetDataSearchApi = APIFactory.getWorksheetDataSearchAPI();
    private static final SearchAPI<ProcessRequest> processRequestSearchAPI = APIFactory.getProcessRequestSearchAPI(ProcessRequest.class);
    private static final SearchAPI<TaskResult> taskResultSearchAPI = APIFactory.getTaskResultSearchAPI(TaskResult.class);
    private static final SearchAPI<ExecuteState> executeStateSearchAPI = APIFactory.getExecuteStateSearchAPI();
    private static final SearchAPI<ExecutionSummary> executionSummarySearchAPI = 
    														APIFactory.getExecutionSummarySearchAPI(ExecutionSummary.class);

    private static QueryBuilder buildQueryWithoutOrgFilter(QueryDTO queryDTO) {
    	QueryBuilder queryBuilderWithoutOrg = null;
        
        List<QueryFilter> queryFiltersWithoutOrgFilter = null;
        		
        if (CollectionUtils.isNotEmpty(queryDTO.getFilters())) {
        	queryFiltersWithoutOrgFilter = queryDTO.getFilters().stream()
        								   .filter(qryFilter -> !qryFilter.getField().equals("sysOrg"))
        								   .collect(Collectors.toList());
        }
        
        if (CollectionUtils.isNotEmpty(queryFiltersWithoutOrgFilter)) {
        	QueryDTO queryDTOWithoutOrg = new QueryDTO(queryDTO);
        	queryDTOWithoutOrg.setFilters(queryFiltersWithoutOrgFilter);
        	queryBuilderWithoutOrg = worksheetSearchApi.getQueryBuilder(queryDTOWithoutOrg, false);
        }
        
        if (Log.log.isTraceEnabled()) {
        	Log.log.trace(String.format("buildQueryWithoutOrgFilters(%s) = %s", queryDTO, queryBuilderWithoutOrg));
        }
        
        return queryBuilderWithoutOrg;
    }
    
    private static QueryBuilder buildQueryForNoneOrgFilter(QueryDTO queryDTO, String username) throws Exception {
    	QueryBuilder queryBuilderForNoneOrg = null;
    	
    	if (!UserUtils.isNoOrgAccessible(username)) {
            throw new Exception(String.format("User %s does not have access to %s", username, OrgsVO.NONE_ORG_NAME));
        }
    	
    	List<QueryFilter> qryFilters = new ArrayList<QueryFilter>();        
        qryFilters.add(new QueryFilter("sysOrg", QueryFilter.NOT_EXISTS));
        QueryDTO noOrgQueryDTO = new QueryDTO(queryDTO);
        noOrgQueryDTO.setFilters(qryFilters);
        queryBuilderForNoneOrg = worksheetSearchApi.getQueryBuilder(noOrgQueryDTO, true);
        
        if (Log.log.isTraceEnabled()) {
        	Log.log.trace(String.format("buildQueryForNoneOrgFilter(%s, %s) = %s", queryDTO, username, 
        								queryBuilderForNoneOrg));
        }
        
    	return queryBuilderForNoneOrg;
    }
    
    private static boolean collectOrgsInHierarchy(Set<OrgsVO> orgVOs, StringBuilder orgInHierarchyIdsCs,
    											  Set<String> orgsInHierarchyIds) {
    	boolean hasNoOrgAccess = false;
    	
    	for (OrgsVO orgsVO : orgVOs) {
	    	if (!hasNoOrgAccess && orgsVO.getUHasNoOrgAccess().booleanValue()) {
	            hasNoOrgAccess = true;
	        }
	        
	        if (orgInHierarchyIdsCs.length() > 0) {
	            orgInHierarchyIdsCs.append(",");
	        }
	        
	        orgInHierarchyIdsCs.append(orgsVO.getSys_id());
	        orgsInHierarchyIds.add(orgsVO.getSys_id());
    	}
        
    	return hasNoOrgAccess;
    }
    
    private static QueryBuilder buildQueryForOrgs(QueryFilter sysOrgTermsQuery,
    											  StringBuilder orgInHierarchyIdsCs, 
    											  Set<String> orgsInHierarchyIds,
    											  String username)  throws SearchException {
    	QueryBuilder queryBuilderForOrgs = null;
    	
    	if (sysOrgTermsQuery == null) {
        	QueryFilter sysOrgQueryFilter = new QueryFilter(QueryFilter.TERMS_TYPE, orgInHierarchyIdsCs.toString(), 
        													"sysOrg", QueryFilter.EQUALS);
        	queryBuilderForOrgs = SearchUtils.createTermsQueryBuilder(sysOrgQueryFilter);
        } else {
            Set<String> orgsInTermsQuery = StringUtils.isNotBlank(sysOrgTermsQuery.getValue()) ? 
                                           StringUtils.stringToSet(sysOrgTermsQuery.getValue(), ",") : null;
            
            Set<String> nonNilOrgsInTermsQuery = orgsInTermsQuery.stream()
            									 .filter(org -> StringUtils.isNotBlank(org) && 
            											   		!org.equalsIgnoreCase(Constants.NIL_STRING))
            									 .collect(Collectors.toSet());
            
            if (CollectionUtils.isNotEmpty(nonNilOrgsInTermsQuery))
            {
                if (orgsInHierarchyIds.containsAll(nonNilOrgsInTermsQuery))
                {
                	queryBuilderForOrgs = SearchUtils.createTermsQueryBuilder(sysOrgTermsQuery);
                }
                else
                {
                    throw new SearchException(String.format("User %s does not have access to one or more " +
                    										"specified Orgs with sys_ids %s", 
                    										username, sysOrgTermsQuery.getValue()));
                }
            }
        }
    	
    	if (Log.log.isTraceEnabled()) {
        	Log.log.trace(String.format("buildQueryForOrgs(%s, %s, %s, %s) = %s", 
        								(sysOrgTermsQuery != null ? sysOrgTermsQuery : "null"), 
        								orgInHierarchyIdsCs.toString(), 
        								StringUtils.setToString(orgsInHierarchyIds, ", "), username,
        								queryBuilderForOrgs));
        }
    	
    	return queryBuilderForOrgs;
    }
    
    private static BoolQueryBuilder populateMustsAndMustNots(QueryBuilder queryBuilderWithoutOrgFilter) {
    	final BoolQueryBuilder boolQueryBuilder = boolQuery();
		
		List<QueryBuilder> musts = ((BoolQueryBuilder)queryBuilderWithoutOrgFilter).must();
		
		if (CollectionUtils.isNotEmpty(musts)) {
			musts.stream().forEach(mustQryBldr -> ((BoolQueryBuilder)boolQueryBuilder).must(mustQryBldr));
		}
		
		List<QueryBuilder> mustNots = ((BoolQueryBuilder)queryBuilderWithoutOrgFilter).mustNot();
		
		if (CollectionUtils.isNotEmpty(mustNots)) {
			mustNots.stream().forEach(mustNotQryBldr -> ((BoolQueryBuilder)boolQueryBuilder)
														.mustNot(mustNotQryBldr));
		}
		
		return boolQueryBuilder;
    }
    
    public static ResponseDTO<Worksheet> searchWorksheets(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<Worksheet> result = new ResponseDTO<>();

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            
            /*
             * Restrict non-super users access to worksheets belonging 
             * to specified Org (and Orgs in specified Org hierarchy) or Orgs 
             * (and all hierarchical Orgs in accessible Orgs) user has access to.
             */
            
            // User specified Org other than None Org
            QueryFilter sysOrgTermsQuery = null;
            
            if (CollectionUtils.isNotEmpty(queryDTO.getFilters())) {
            	sysOrgTermsQuery = queryDTO.getFilters().stream()
            					   .filter(qryFilter -> qryFilter.isTermsType() && 
            							   qryFilter.getField().equals("sysOrg") && 
            							   qryFilter.getCondition().equalsIgnoreCase(QueryFilter.EQUALS) &&
            							   StringUtils.isNotBlank(qryFilter.getValue()))
            						.findFirst()
            						.orElse(null);
            }
                        
            // Get all Orgs user has access to including None Org
            
            boolean hasNoOrgAccess = false;                
            UsersVO usersVO = UserUtils.getUserNoCache(username, true);                
            Set<OrgsVO> aggrOrgsInHierarchy = new HashSet<OrgsVO>();
            StringBuilder orgInHierarchyIdsCs = new StringBuilder();
            Set<String> orgsInHierarchyIds = new HashSet<String>();
            
            if (usersVO != null && usersVO.getUserOrgs() != null && !usersVO.getUserOrgs().isEmpty()) {
                Set<OrgsVO> userOrgVOs = usersVO.getUserOrgs();
                
                for (OrgsVO userOrgsVO : userOrgVOs) {
                    if (!userOrgsVO.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME)) {
                        Set<OrgsVO> orgsInHierarchy = UserUtils.getAllOrgsInHierarchy(userOrgsVO.getSys_id(), 
                        															  userOrgsVO.getUName());
                        aggrOrgsInHierarchy.addAll(orgsInHierarchy);
                    } else {
                        hasNoOrgAccess = true;
                    }
                }
                
                // Avoid getting all Orgs user has access to if query contains specific Org (Not None)
                
                if (CollectionUtils.isNotEmpty(aggrOrgsInHierarchy) && sysOrgTermsQuery == null) {
                    if (hasNoOrgAccess == false ) { // check whether any other org has access to none org
                        hasNoOrgAccess = collectOrgsInHierarchy(aggrOrgsInHierarchy, orgInHierarchyIdsCs, orgsInHierarchyIds);
                    } else { // populate orgInHierarchyIdsCs and orgsInHierarchyIds anyway and ignore the return value.
                        collectOrgsInHierarchy(aggrOrgsInHierarchy, orgInHierarchyIdsCs, orgsInHierarchyIds);
                    }
                }
            }
            
            // Handle User user specified Org (if any) in query
            
            if (sysOrgTermsQuery != null && StringUtils.isNotBlank(sysOrgTermsQuery.getValue()) &&
                !sysOrgTermsQuery.getValue().equalsIgnoreCase(Constants.NIL_STRING)) {
            	
                /*
                 * User specified filter by specific Org (Not None) so reset user has access to None Org flag and 
                 * check if specified Org or Orgs in hierarchy has access to None Org 
                 */
                
            	hasNoOrgAccess = false;
                orgInHierarchyIdsCs = new StringBuilder();
                orgsInHierarchyIds.clear();
                
                // First check if user has access to passed in Org
                
                if (!UserUtils.isOrgAccessible(sysOrgTermsQuery.getValue(), username, false, false)) {
                    throw new Exception(String.format("User %s does not have access to Org with sys_id %s",
                    								  username, sysOrgTermsQuery.getValue()));
                }
                
                // Check if passed in Org or one of its children Orgs has access to 'None' Org 
                
                Set<OrgsVO> orgVOs = UserUtils.getAllOrgsInHierarchy(sysOrgTermsQuery.getValue(), null);
                
                if (orgVOs != null && !orgVOs.isEmpty()) {
                	hasNoOrgAccess = collectOrgsInHierarchy(orgVOs,  orgInHierarchyIdsCs, orgsInHierarchyIds);
                    sysOrgTermsQuery.setValue(orgInHierarchyIdsCs.toString());
                }
            } else if (sysOrgTermsQuery != null && StringUtils.isNotBlank(sysOrgTermsQuery.getValue()) &&
                       sysOrgTermsQuery.getValue().equalsIgnoreCase(Constants.NIL_STRING)) {
            	/*
                 * Query contains filter by None Org
                 * 
                 * Please note check for user's access to None Org is already done later on 
                 * so setting user has access to None Org to true without checking
                 */            	
                hasNoOrgAccess = true;                
            }
            
            // Build query using all specified filters (if any) except Org filter (if any)
            
            QueryBuilder queryBuilderWithoutOrgFilter = buildQueryWithoutOrgFilter(queryDTO);
            
            QueryBuilder noOrgQueryBuilder = null;
            
            /*
             * Build None Org query if user has access to None Org or 
             * specified Org and/or Orgs in specified Org hierarchy has 
             * access to None Org.  
             */
            
            if (hasNoOrgAccess) {
            	noOrgQueryBuilder = buildQueryForNoneOrgFilter(queryDTO, username);
            }
            
            QueryBuilder orgQueryBuilder = null;
            
            // If User has access to any Org(s) (excluding None) then proceed to building Org terms query
            
            if (!aggrOrgsInHierarchy.isEmpty()) {
            	orgQueryBuilder = buildQueryForOrgs(sysOrgTermsQuery, orgInHierarchyIdsCs, orgsInHierarchyIds, username);
            }
            
            QueryBuilder aggrQueryBuilder = null;
            
            if (orgQueryBuilder != null && noOrgQueryBuilder != null) {           
            	aggrQueryBuilder = boolQuery();
            	
            	if (queryBuilderWithoutOrgFilter != null) {
            		/*
            		 * Query for 
            		 * 
            		 * Specified non Org query filter(s)
            		 * 
            		 * AND
            		 * 
            		 * Specified Org(s) and Org(s) 
            		 * in Org hierarchy in specified Org(s) worksheets
            		 */
            		BoolQueryBuilder boolQryBldr1 = populateMustsAndMustNots(queryBuilderWithoutOrgFilter);
            		boolQryBldr1.must(orgQueryBuilder);
            		
            		/*
            		 * Query for 
            		 * 
            		 * Specified query filter(s)
            		 * 
            		 * AND
            		 * 
            		 * None Org worksheets
            		 */
            		BoolQueryBuilder boolQryBldr2 = populateMustsAndMustNots(queryBuilderWithoutOrgFilter);
            		
            		List<QueryBuilder> filters = ((BoolQueryBuilder)noOrgQueryBuilder).filter();
        			
        			if (CollectionUtils.isNotEmpty(filters)) {
        				boolQryBldr2.filter(filters.get(0));
        			}
        			
        			// Union of above two queries
        			
        			((BoolQueryBuilder)aggrQueryBuilder).should(boolQryBldr1);
        			((BoolQueryBuilder)aggrQueryBuilder).should(boolQryBldr2);
            	} else {
            		/*
            		 * Union of specified Org(s) and Org(s) 
            		 * in Org hierarchy in specified Org(s) terms query
            		 * and None Org worksheets
            		 */
            		((BoolQueryBuilder)aggrQueryBuilder).should(orgQueryBuilder);
            	
            		List<QueryBuilder> filters = ((BoolQueryBuilder)noOrgQueryBuilder).filter();
            	
            		if (CollectionUtils.isNotEmpty(filters) && filters.size() == 1) {
            			((BoolQueryBuilder) aggrQueryBuilder).should(filters.get(0));
            		}
            	}
            } else {
            	if (noOrgQueryBuilder != null) {
            		if (queryBuilderWithoutOrgFilter != null) {
            			/*
            			 * Query for 
            			 * 
            			 * Specified non Org query filter(s)
            			 * 
            			 * AND
            			 * 
            			 * None Org worksheets
            			 */
            			aggrQueryBuilder = populateMustsAndMustNots(queryBuilderWithoutOrgFilter);
            			
            			List<QueryBuilder> filters = ((BoolQueryBuilder)noOrgQueryBuilder).filter();
            			
            			if (CollectionUtils.isNotEmpty(filters)) {
            				((BoolQueryBuilder)aggrQueryBuilder).filter(filters.get(0));
            			}
            		} else {
            			// Query for None Org
            			aggrQueryBuilder = noOrgQueryBuilder;
            		}
            	} else if (orgQueryBuilder != null) {
            		if (queryBuilderWithoutOrgFilter != null) {
            			aggrQueryBuilder = populateMustsAndMustNots(queryBuilderWithoutOrgFilter);
            			((BoolQueryBuilder)aggrQueryBuilder).must(orgQueryBuilder);
            		} else {
            			aggrQueryBuilder = orgQueryBuilder;
            		}
            	}
            }
            
            if (/*CollectionUtils.isNotEmpty(aggrQueryBuilder.should())*/aggrQueryBuilder != null) {
            	if (Log.log.isTraceEnabled()) {
                	Log.log.trace(String.format("aggrQueryBuilder = %s", aggrQueryBuilder));
            	}
            	
            	result = worksheetSearchApi.searchByQuery(queryDTO, aggrQueryBuilder, null, null, null, username);
            }
        }
        catch (Exception e)
        {
            if (e.getLocalizedMessage().contains("does not have access to"))
            {
                Log.log.info(e.getLocalizedMessage());
            }
            else
            {
                Log.log.error(e.getMessage(), e);
            }
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }

    public static ResponseDTO<Worksheet> searchWorksheetsByFiltersOnly(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<Worksheet> result = null;
        
        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            
            if (!UserUtils.isAdminUser(username))
            {
                /*
                 * Restrict worksheet access of non-admin role users to worksheets belonging 
                 * to orgs in hierarchy.
                 */
                
                QueryFilter sysOrgTermsQuery = null;
                
                for (QueryFilter qryFltr : queryDTO.getFilters())
                {
                    if (qryFltr.isTermsType() && qryFltr.getField().equals("sysOrg") && 
                        StringUtils.isNotBlank(qryFltr.getValue()) && 
                        qryFltr.getCondition().equalsIgnoreCase(QueryFilter.EQUALS))
                    {
                        sysOrgTermsQuery = qryFltr;
                        break;
                    }
                }
                
                ResponseDTO<Worksheet> noOrgWorksheetsResult = new ResponseDTO<Worksheet>();
                
                boolean hasNoOrgAccess = false;                
                UsersVO usersVO = UserUtils.getUserNoCache(username);                
                Set<OrgsVO> aggrOrgsInHierarchy = new HashSet<OrgsVO>();
                StringBuilder orgInHierarchyIdsCs = new StringBuilder();
                Set<String> orgsInHierarchyIds = new HashSet<String>();
                
                if (usersVO != null && usersVO.getUserOrgs() != null && !usersVO.getUserOrgs().isEmpty())
                {
                    Set<OrgsVO> userOrgVOs = usersVO.getUserOrgs();
                    
                    for (OrgsVO userOrgsVO : userOrgVOs)
                    {
                        if (!userOrgsVO.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                        {
                            Set<OrgsVO> orgsInHierarchy = UserUtils.getAllOrgsInHierarchy(userOrgsVO.getSys_id(), userOrgsVO.getUName());
                            aggrOrgsInHierarchy.addAll(orgsInHierarchy);
                        }
                        else
                        {
                            hasNoOrgAccess = true;
                        }
                    }
                    
                    if (!aggrOrgsInHierarchy.isEmpty())
                    {
                        for (OrgsVO userOrgsVO : aggrOrgsInHierarchy)
                        {
                            if (!hasNoOrgAccess)
                            {
                                if (userOrgsVO.getUHasNoOrgAccess().booleanValue())
                                {
                                    hasNoOrgAccess = true;
                                }
                            }
                            
                            if (orgInHierarchyIdsCs.length() > 0)
                            {
                                orgInHierarchyIdsCs.append(",");
                            }
                            
                            orgInHierarchyIdsCs.append(userOrgsVO.getSys_id());
                            orgsInHierarchyIds.add(userOrgsVO.getSys_id());
                        }
                    }
                }
                
                if (sysOrgTermsQuery != null)
                {
                    /*
                     * Query contains filter by Org so reset the user hasNoOrgAccess flag and check filter Org
                     * Note Query will never contain Org None as there is no such Org
                     */
                    
                    hasNoOrgAccess = false;
                    
                    // Check if passed in Org or one of its children Orgs has access to 'None' Org 
                    
                    Set<OrgsVO> orgVOs = UserUtils.getAllOrgsInHierarchy(sysOrgTermsQuery.getValue(), null);
                    
                    if (orgVOs != null && !orgVOs.isEmpty())
                    {
                        for (OrgsVO orgsVO : orgVOs)
                        {
                            if (orgsVO.getUHasNoOrgAccess().booleanValue())
                            {
                                // Check if user has access to Org having No Org access
                                
                                if (UserUtils.isOrgAccessible(orgsVO.getSys_id(), username, false, false))
                                {
                                    hasNoOrgAccess = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                
                if (hasNoOrgAccess)
                {
                    QueryDTO noOrgQueryDTO = new QueryDTO(queryDTO);
                    
                    noOrgQueryDTO.addFilterItem(new QueryFilter("sysOrg", QueryFilter.NOT_EXISTS));
            
                    noOrgWorksheetsResult = worksheetSearchApi.searchByQuery(noOrgQueryDTO, username);
                }
                else
                {
                    List<Worksheet> emptyWorksheets = Collections.emptyList();
                    noOrgWorksheetsResult.setRecords(emptyWorksheets);
                    noOrgWorksheetsResult.setTotal(0l);
                }
                
                ResponseDTO<Worksheet> orgWorksheetsResult = null;
                
                QueryDTO orgQueryDTO = new QueryDTO(queryDTO);
                
                if (queryDTO.getLimit() > 0)
                {
                    if (noOrgWorksheetsResult.getRecords().size() == queryDTO.getLimit())
                    {
                        orgQueryDTO.setLimit(1);
                        orgQueryDTO.setStart(0);
                    }
                    else if (noOrgWorksheetsResult.getRecords().size() < queryDTO.getLimit())
                    {
                        orgQueryDTO.setLimit(queryDTO.getLimit() - noOrgWorksheetsResult.getRecords().size());
                        
                        if (queryDTO.getStart() >= noOrgWorksheetsResult.getTotal())
                        {
                            orgQueryDTO.setStart(queryDTO.getStart() - (int)noOrgWorksheetsResult.getTotal());
                        }
                        else if (queryDTO.getStart() + noOrgWorksheetsResult.getRecords().size() <= 
                                 noOrgWorksheetsResult.getTotal())
                        {
                            orgQueryDTO.setStart(0);
                        }
                    }
                }
                
                if (!aggrOrgsInHierarchy.isEmpty())
                {
                    if (sysOrgTermsQuery == null)
                    {
                        orgQueryDTO.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, orgInHierarchyIdsCs.toString(), "sysOrg", QueryFilter.EQUALS));
                        orgWorksheetsResult = worksheetSearchApi.searchByQuery(orgQueryDTO, username);
                    }
                    else
                    {
                        Set<String> orgsInTermsQuery = StringUtils.stringToSet(sysOrgTermsQuery.getValue(), ",");
                        
                        if (orgsInHierarchyIds.containsAll(orgsInTermsQuery))
                        {
                            orgWorksheetsResult = worksheetSearchApi.searchByQuery(orgQueryDTO, username);
                        }
                        else
                        {
                            throw new SearchException("User does not belong to specified Org.");
                        }
                    }
                }
                
                List<Worksheet> aggrWorksheets = new ArrayList<Worksheet>();
                long aggrWorksheetsTotal = 0l;
                
                if (noOrgWorksheetsResult != null && noOrgWorksheetsResult.getRecords() != null &&
                    noOrgWorksheetsResult.getTotal() > 0)
                {
                    aggrWorksheetsTotal += noOrgWorksheetsResult.getTotal();
                    
                    aggrWorksheets.addAll(noOrgWorksheetsResult.getRecords());
                }
                
                if (orgWorksheetsResult != null && orgWorksheetsResult.getRecords() != null &&
                    !orgWorksheetsResult.getRecords().isEmpty())
                {
                    if (queryDTO.getLimit() > 0)
                    {
                        if (aggrWorksheets.size() < queryDTO.getLimit())
                        {
                            aggrWorksheets.addAll(orgWorksheetsResult.getRecords());
                        }
                    }
                    else
                    {
                        aggrWorksheets.addAll(orgWorksheetsResult.getRecords());
                    }
                    
                    aggrWorksheetsTotal += orgWorksheetsResult.getTotal();
                }
                
                if (!aggrWorksheets.isEmpty() && aggrWorksheetsTotal > 0l)
                {
                    result = new ResponseDTO<Worksheet>();
                    result.setRecords(aggrWorksheets);
                    result.setTotal(aggrWorksheetsTotal);
                }
            }
            else
            {
                List<QueryFilter> filters = queryDTO.getFilterItems();
                QueryBuilder[] tfbs = new QueryBuilder[filters.size()]; 
                int i = 0;
                for (QueryFilter qf : filters)
                {
                    // Admin role should have access to all Orgs including No Org
                    
                    if (!(qf.isTermsType() && qf.getField().equals("sysOrg") && 
                          StringUtils.isNotBlank(qf.getValue()) && 
                          qf.getCondition().equalsIgnoreCase(QueryFilter.EQUALS)))
                    {
                        String field = qf.getField();
                        String value = qf.getValue();
                        TermQueryBuilder tfb = new TermQueryBuilder(field, value);
                        tfbs[i] = tfb;
                        i++;
                    }
                }
                
                QueryBuilder[] filteredtfbs = new QueryBuilder[i];
                System.arraycopy(tfbs, 0, filteredtfbs, 0, i);
                
                result = worksheetSearchApi.searchByFilteredQuery(queryDTO, new MatchAllQueryBuilder(), filteredtfbs, username);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<ProcessRequest> searchProcessRequestsByFiltersOnly(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<ProcessRequest> result = null;
        List<QueryFilter> filters = queryDTO.getFilterItems();
        QueryBuilder[] tfbs = new QueryBuilder[filters.size()]; 
        int i = 0;
        for (QueryFilter qf : filters)
        {
            String field = qf.getField();
            String value = qf.getValue();
            TermQueryBuilder tfb = new TermQueryBuilder(field, value);
            tfbs[i] = tfb;
            i++;
        }

        try
        {
//            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.DIRECTION_DESC);
            result = processRequestSearchAPI.searchByFilteredQuery(queryDTO, new MatchAllQueryBuilder(), tfbs, username);
//            result = processRequestSearchAPI.searchByQuery(queryDTO, username);
            filterByAccessibleOrgs(result, username);
            
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    public static ResponseDTO<ProcessRequest> searchProcessRequests(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<ProcessRequest> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            result = processRequestSearchAPI.searchByQuery(queryDTO, username);
            
            if (result != null && result.isSuccess() && 
                result.getRecords() != null && !result.getRecords().isEmpty())
            {
                filterByAccessibleOrgs(result, username);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
   
    public static ResponseDTO<TaskResult> searchTaskResults(QueryDTO queryDTO, String username) throws SearchException
    {
    	return searchTaskResults(queryDTO, null, username);
    }
    
    public static ResponseDTO<TaskResult> searchTaskResults(QueryDTO queryDTO, String[] indexNames, String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = null;

        try
        {
            queryDTO.setModelName(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            if(indexNames!= null && indexNames.length > 0)
            	result = taskResultSearchAPI.searchByQuery(queryDTO, username,indexNames);
            else
            	result = taskResultSearchAPI.searchByQuery(queryDTO, username);
            
            if (result != null && result.isSuccess() && 
                result.getRecords() != null && !result.getRecords().isEmpty() && !UserUtils.isAdminUser(username))
            {
                filterByAccessibleOrgs(result, username);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    } //searchPbTaskResults
    
    
    /**
     * This API is similar to 'searchTaskResults', the only difference is, it uses queryDTO.getSearchTerm() to perform
     * free form text search within the columns specified. It also highlights the search term.
     * <br>
     * @param queryDTO : {@link QueryDTO}, a generic query specifier.
     * @param columns : String array specifying which columns to perform a search in.
     * @param username : Logged in user name.
     * @return
     * @throws SearchException
     */
    public static ResponseDTO<TaskResult> searchSirTaskResults(QueryDTO queryDTO, String[] columns, String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = null;

        try
        {
            queryDTO.setModelName(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            result = taskResultSearchAPI.searchSirTaskResults(queryDTO, columns, username);
            
            if (result != null && result.isSuccess() && 
                result.getRecords() != null && !result.getRecords().isEmpty() && !UserUtils.isAdminUser(username))
            {
                filterByAccessibleOrgs(result, username);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<TaskResult> serveResultMacro(final QueryDTO queryDTO, final String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = null;

        try
        {
            queryDTO.setModelName(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
//            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.DIRECTION_DESC);
            
            QueryBuilder problemIdQueryBuilder = null;
            QueryBuilder taskNameQueryBuilder = null;
            QueryBuilder taskFullNameQueryBuilder = null;
            List<QueryFilter> filters = new ArrayList<QueryFilter>();
            for( QueryFilter queryFilter : queryDTO.getFilters())
            {
                if("taskName".equalsIgnoreCase(queryFilter.getField()))
                {
                    taskNameQueryBuilder = SearchUtils.createTermsQueryBuilder(queryFilter);
                }
                else if("taskFullName".equalsIgnoreCase(queryFilter.getField()))
                {
                    taskFullNameQueryBuilder = SearchUtils.createTermsQueryBuilder(queryFilter);
                }
                else if("problemId".equalsIgnoreCase(queryFilter.getField()))
                {
                    problemIdQueryBuilder = SearchUtils.createTermQueryBuilder(queryFilter);
                }
                else
                {
                    filters.add(queryFilter);
                }
            }
            BoolQueryBuilder boolQueryBuilder = null;
            if(filters.size() > 0)
            {
                queryDTO.setFilters(filters);
                boolQueryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
            }
            else
            {
                boolQueryBuilder = QueryBuilders.boolQuery();
            }
            
            if(problemIdQueryBuilder != null)
            {
                boolQueryBuilder.must(problemIdQueryBuilder);
            }
            if(taskNameQueryBuilder != null)
            {
                boolQueryBuilder.should(taskNameQueryBuilder);
            }
            if(taskFullNameQueryBuilder != null)
            {
                boolQueryBuilder.should(taskFullNameQueryBuilder);
            }
            if(taskNameQueryBuilder != null || taskFullNameQueryBuilder != null)
            {
                boolQueryBuilder.minimumShouldMatch("1");
            }
            result = taskResultSearchAPI.searchByQuery(queryDTO, boolQueryBuilder, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<TaskResult> serveResultMacroByFilters(final QueryDTO queryDTO, final String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = null;

        try
        {
            queryDTO.setModelName(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            
            TermQueryBuilder activityIdQueryBuilder = null;
            TermQueryBuilder problemIdQueryBuilder = null;
            TermsQueryBuilder taskNameQueryBuilder = null;
            TermsQueryBuilder taskFullNameQueryBuilder = null;
            List<QueryFilter> filters = new ArrayList<QueryFilter>();
			for (QueryFilter queryFilter : queryDTO.getFilters())
            {
                if("taskName".equalsIgnoreCase(queryFilter.getField()))
                {
                    taskNameQueryBuilder = SearchUtils.createTermsFilterBuilder(queryFilter);
                }
                else if("taskFullName".equalsIgnoreCase(queryFilter.getField()))
                {
                    taskFullNameQueryBuilder = SearchUtils.createTermsFilterBuilder(queryFilter);
                }
                else if("problemId".equalsIgnoreCase(queryFilter.getField()))
                {
                    problemIdQueryBuilder = SearchUtils.createTermFilterBuilder(queryFilter);
                }
                else if("activityId".equalsIgnoreCase(queryFilter.getField()))
                {
                	activityIdQueryBuilder = SearchUtils.createTermFilterBuilder(queryFilter);
                }
                else
                {
                    filters.add(queryFilter);
                }
            }
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//            if(filters.size() > 0)
//            {
//                queryDTO.setFilters(filters);
//                boolQueryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
//            }
//            else
//            {
//                boolQueryBuilder = QueryBuilders.boolQuery();
//            }
            if (activityIdQueryBuilder != null)
            {
            	boolQueryBuilder.must(activityIdQueryBuilder);
            }
            if(problemIdQueryBuilder != null)
            {
                boolQueryBuilder.must(problemIdQueryBuilder);
            }

            if(taskNameQueryBuilder != null && taskFullNameQueryBuilder != null)
            {
                boolQueryBuilder.should(taskNameQueryBuilder).should(taskFullNameQueryBuilder);
            }
            else if(taskNameQueryBuilder != null)
            {
                boolQueryBuilder.should(taskNameQueryBuilder);
            }
            else if(taskFullNameQueryBuilder != null)
            {
                boolQueryBuilder.should(taskFullNameQueryBuilder);
            }
//            if(taskNameQueryBuilder != null || taskFullNameQueryBuilder != null)
//            {
//                boolQueryBuilder.minimumShouldMatch("1");
//            }
            result = taskResultSearchAPI.searchByFilteredQuery(queryDTO, new MatchAllQueryBuilder(), new QueryBuilder[] {boolQueryBuilder}, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static long getTaskResultsCount(QueryDTO queryDTO, String username) throws SearchException
    {
        long result = -1;

        try
        {
            queryDTO.setModelName(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            result = taskResultSearchAPI.getCountByQuery(queryDTO, new QueryBuilder[0]);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<ExecuteState> searchExecuteStates(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<ExecuteState> result = null;

        try
        {
            result = executeStateSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<Worksheet> findWorksheetById(String sysId, String username) throws SearchException
    {
        ResponseDTO<Worksheet> result = null;

        try
        {
            if (StringUtils.isBlank(sysId))
            {
                throw new SearchException("sysId must not be blank");
            }
            result = new ResponseDTO<Worksheet>();
            
            Worksheet worksheet = worksheetSearchApi.findDocumentById(sysId, username);
            
            if (worksheet != null)
            {
                if (!UserUtils.isAdminUser(username))
                {
                    if (StringUtils.isNotBlank(worksheet.getSysOrg()) && !worksheet.getSysOrg().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                    {
                        /*
                         * Restrict worksheet access of non-admin role users to worksheets belonging 
                         * to orgs in hierarchy only.
                         */
                        
                        if (!UserUtils.isOrgAccessible(worksheet.getSysOrg(), username, false, false))
                        {
                            throw new SearchException("User does not belongs to Org of worksheet with id " + sysId + ".");
                        }
                    }
                    else
                    {
                        if (!UserUtils.isNoOrgAccessible(username))
                        {
                            throw new SearchException("User does not belongs to No Org (None) of worksheet with id " + sysId + ".");
                        }
                    }
                }
                
                result.setData(worksheet);
                result.setSuccess(true);
            }
            else
            {
                throw new SearchException("Can not find worksheet with id " + sysId);
            }
        }
        catch (Exception e)
        {
            //Log.log.trace(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }

    public static ResponseDTO<Worksheet> findWorksheetByNumber(String number, String username) throws SearchException
    {
        ResponseDTO<Worksheet> result = null;

        try
        {
            if (StringUtils.isBlank(number))
            {
                throw new SearchException("problem number must not be blank");
            }
            result = new ResponseDTO<Worksheet>();

            QueryDTO queryDTO = new QueryDTO(0, 1);
            queryDTO.addFilterItem(new QueryFilter("number", QueryFilter.EQUALS, number));
            ResponseDTO<Worksheet> response = worksheetSearchApi.searchByQuery(queryDTO, username);
            
            if (response != null && response.getRecords() != null && !response.getRecords().isEmpty())
            {
                if (!UserUtils.isAdminUser(username))
                {
                    if (StringUtils.isNotBlank(response.getRecords().get(0).getSysOrg()) && 
                        !response.getRecords().get(0).getSysOrg().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                    {
                        /*
                         * Restrict worksheet access of non-admin role users to worksheets belonging 
                         * to orgs in hierarchy only.
                         */
                        
                        if (!UserUtils.isOrgAccessible(response.getRecords().get(0).getSysOrg(), username, false, false))
                        {
                            throw new SearchException("User does not belongs to Org of worksheet with number " + number + ".");
                        }
                    }
                    else
                    {
                        if (!UserUtils.isNoOrgAccessible(username))
                        {
                            throw new SearchException("User does not belongs to No Org 'None' of worksheet with number " + number + ".");
                        }
                    }
                }
                
                result.setData(response.getRecords().get(0));
                result.setSuccess(true);
            }
            else
            {
                throw new SearchException("Can not find worksheet with number " + number); 
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<Worksheet> findWorksheetByIds(List<String> sysIds, String username) throws SearchException
    {
        ResponseDTO<Worksheet> result = null;

        try
        {
            if (sysIds == null || sysIds.size() <= 0)
            {
                throw new SearchException("sysIds must not be empty");
            }
            result = new ResponseDTO<Worksheet>();
                        
            List<Worksheet> worksheets = worksheetSearchApi.findDocumentByIds(sysIds, username);
            
            if (worksheets != null && !worksheets.isEmpty())
            {
                if (!UserUtils.isAdminUser(username))
                {
                    /*
                     * Restrict worksheet access of non-admin role users to worksheets belonging 
                     * to orgs in hierarchy only.
                     */
                    
                    Set<String> aggrOrgIdsInHierarchy = new HashSet<String>();
                    UsersVO usersVO = UserUtils.getUserNoCache(username);
                    
                    if (usersVO != null && usersVO.getUserOrgs() != null && !usersVO.getUserOrgs().isEmpty())
                    {
                        Set<OrgsVO> userOrgVOs = usersVO.getUserOrgs();
                        
                        for (OrgsVO userOrgsVO : userOrgVOs)
                        {
                            Set<OrgsVO> orgsInHierarchy = UserUtils.getAllOrgsInHierarchy(userOrgsVO.getSys_id(), userOrgsVO.getUName());
                            
                            if (orgsInHierarchy != null && !orgsInHierarchy.isEmpty())
                            {
                                for (OrgsVO orgInHierarchyVO : orgsInHierarchy)
                                {
                                    aggrOrgIdsInHierarchy.add(orgInHierarchyVO.getSys_id());
                                }
                            }
                        }
                    }
                    
                    boolean hasNoOrgAccess = UserUtils.isNoOrgAccessible(username);
                    
                    List<Worksheet> accessDenied = new ArrayList<Worksheet>();
                    
                    for (Worksheet worksheet : worksheets)
                    {
                        if (StringUtils.isNotBlank(worksheet.getSysOrg()) && !worksheet.getSysOrg().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                        {
                            if (!aggrOrgIdsInHierarchy.contains(worksheet.getSysOrg()))
                            {
                                accessDenied.add(worksheet);
                            }
                        }
                        else
                        {
                            if (!hasNoOrgAccess)
                            {
                                accessDenied.add(worksheet);
                            }
                        }
                    }
                    
                    worksheets.removeAll(accessDenied);
                }
                
                result.setRecords(worksheets);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
    
    public static ResponseDTO<ProcessRequest> findProcessRequestsById(String sysId, String username, 
    																  boolean ignoreESInMemCache) throws SearchException {
        ResponseDTO<ProcessRequest> result = null;

        try {
            if (StringUtils.isBlank(sysId)) {
                throw new SearchException("sysId must not be blank");
            }
            
            ProcessRequest processRequest = processRequestSearchAPI.findDocumentById(sysId, username, ignoreESInMemCache);
            
            result = new ResponseDTO<ProcessRequest>();
            result.setSuccess(true);
            result.setData(processRequest);
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    public static ResponseDTO<ProcessRequest> findProcessRequestsById(String sysId, String username) throws SearchException
    {
    	return WorksheetSearchAPI.findProcessRequestsById(sysId, username, false);
    }

    public static ResponseDTO<ProcessRequest> findProcessRequestsByIds(List<String> sysIds, String username) throws SearchException
    {
        ResponseDTO<ProcessRequest> result = null;

        try
        {
            if (sysIds == null || sysIds.size() <= 0)
            {
                throw new SearchException("sysIds must not be empty");
            }
            result = new ResponseDTO<ProcessRequest>();
            result.setRecords(processRequestSearchAPI.findDocumentByIds(sysIds, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<ProcessRequest> findProcessRequestByWorksheetId(String worksheetId, String username) throws SearchException
    {
        ResponseDTO<ProcessRequest> result = new ResponseDTO<ProcessRequest>();

        try
        {
            if (StringUtils.isBlank(worksheetId))
            {
                throw new SearchException("worksheetId must be provided.");
            }
            
            QueryDTO queryDTO = new QueryDTO();
            
            //prepare the query for the problem id.
            QueryBuilder queryBuilder = QueryBuilders.termQuery("problem", worksheetId);
            
            result = processRequestSearchAPI.searchByQuery(queryDTO, queryBuilder, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<TaskResult> findTaskResultById(String sysId, String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = null;

        try
        {
            if (StringUtils.isBlank(sysId))
            {
                throw new SearchException("sysId must not be blank");
            }
            result = new ResponseDTO<TaskResult>();
            
            TaskResult taskResult = taskResultSearchAPI.findDocumentById(sysId, username);
            
            result.setData(taskResult);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<TaskResult> findTaskResultByIds(List<String> sysIds, String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = null;

        try
        {
            if (sysIds == null || sysIds.size() <= 0)
            {
                throw new SearchException("sysIds must not be empty");
            }
            result = new ResponseDTO<TaskResult>();
            result.setRecords(taskResultSearchAPI.findDocumentByIds(sysIds, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<TaskResult> findTaskResultByWorksheetId(String worksheetId, List<String> actionTaskIds, String username) throws SearchException
    {
        return findTaskResultByWorksheetId(worksheetId, actionTaskIds, true, username);
    }
    
    public static ResponseDTO<TaskResult> findTaskResultByWorksheetId(String worksheetId, List<String> actionTaskIds, boolean isOrderByAsc, String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<TaskResult>();

        try
        {
            if (StringUtils.isBlank(worksheetId))
            {
                throw new SearchException("worksheetId must be provided.");
            }
            
            QueryDTO queryDTO = new QueryDTO();
            if(isOrderByAsc)
            {
                queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));
            }
            else
            {
                queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.DESC));
            }
            
            //prepare the query for the problem id.
            QueryBuilder queryBuilder = QueryBuilders.termQuery("problemId", worksheetId);
            
            //build the filter for the task ids if any.
            QueryBuilder taskIdFilterBuilder = null;
            if(actionTaskIds != null)
            {
                taskIdFilterBuilder = QueryBuilders.termsQuery("taskId", actionTaskIds);
            }
            
            result = taskResultSearchAPI.searchByQuery(queryDTO, queryBuilder, new QueryBuilder[] { taskIdFilterBuilder }, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<TaskResult> findTaskResultByProcessId(String processId, String username) throws SearchException
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<TaskResult>();

        try
        {
            if (StringUtils.isBlank(processId))
            {
                throw new SearchException("processId must be provided.");
            }
            
            QueryDTO query = new QueryDTO();
            query.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processId));
            result = searchTaskResults(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static TaskResult findTaskResultByExecuteRequestId(String executeRequestId, String username) throws SearchException
    {
        TaskResult result = null;

        try
        {
            if (StringUtils.isBlank(executeRequestId))
            {
                throw new SearchException("executeRequestId must be provided.");
            }
            
            QueryDTO query = new QueryDTO();
            query.addFilterItem(new QueryFilter("executeRequestId", QueryFilter.EQUALS, executeRequestId));
            ResponseDTO<TaskResult> response = searchTaskResults(query, username);
            if(response.getRecords().size() > 0)
            {
                result = response.getRecords().get(0);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static TaskResult findTaskResultByExecuteResultId(String executeResultId, String username) throws SearchException
    {
        TaskResult result = null;

        try
        {
            if (StringUtils.isBlank(executeResultId))
            {
                throw new SearchException("executeResultId must be provided.");
            }
            
            QueryDTO query = new QueryDTO();
            query.addFilterItem(new QueryFilter("executeResultId", QueryFilter.EQUALS, executeResultId));
            ResponseDTO<TaskResult> response = searchTaskResults(query, username);
            if(response.getRecords().size() > 0)
            {
                result = response.getRecords().get(0);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static List<Worksheet> searchByFilter(QueryBuilder[] filters, String username) throws SearchException
    {
        List<Worksheet> result = new ArrayList<Worksheet>();
        ResponseDTO<Worksheet> response = null;
        
        try
        {
            QueryDTO queryDTO = new QueryDTO(0, 100);
            
            if (!UserUtils.isAdminUser(username))
            {
                /*
                 * Restrict worksheet access of non-admin role users to worksheets belonging 
                 * to orgs in hierarchy only.
                 */
                
                QueryFilter sysOrgTermsQuery = null;
                
                for (QueryFilter qryFltr : queryDTO.getFilters())
                {
                    if (qryFltr.isTermsType() && qryFltr.getField().equals("sysOrg") && 
                        StringUtils.isNotBlank(qryFltr.getValue()) && 
                        qryFltr.getCondition().equalsIgnoreCase(QueryFilter.EQUALS))
                    {
                        sysOrgTermsQuery = qryFltr;
                        break;
                    }
                }
                
                ResponseDTO<Worksheet> noOrgWorksheetsResult = new ResponseDTO<Worksheet>();
                
                boolean hasNoOrgAccess = false;                
                UsersVO usersVO = UserUtils.getUserNoCache(username);                
                Set<OrgsVO> aggrOrgsInHierarchy = new HashSet<OrgsVO>();
                StringBuilder orgInHierarchyIdsCs = new StringBuilder();
                Set<String> orgsInHierarchyIds = new HashSet<String>();
                
                if (usersVO != null && usersVO.getUserOrgs() != null && !usersVO.getUserOrgs().isEmpty())
                {
                    Set<OrgsVO> userOrgVOs = usersVO.getUserOrgs();
                    
                    for (OrgsVO userOrgsVO : userOrgVOs)
                    {
                        if (!userOrgsVO.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                        {
                            Set<OrgsVO> orgsInHierarchy = UserUtils.getAllOrgsInHierarchy(userOrgsVO.getSys_id(), userOrgsVO.getUName());
                            aggrOrgsInHierarchy.addAll(orgsInHierarchy);
                        }
                        else
                        {
                            hasNoOrgAccess = true;
                        }
                    }
                    
                    if (!aggrOrgsInHierarchy.isEmpty())
                    {
                        for (OrgsVO userOrgsVO : aggrOrgsInHierarchy)
                        {
                            if (!hasNoOrgAccess)
                            {
                                if (userOrgsVO.getUHasNoOrgAccess().booleanValue())
                                {
                                    hasNoOrgAccess = true;
                                }
                            }
                            
                            if (orgInHierarchyIdsCs.length() > 0)
                            {
                                orgInHierarchyIdsCs.append(",");
                            }
                            
                            orgInHierarchyIdsCs.append(userOrgsVO.getSys_id());
                            orgsInHierarchyIds.add(userOrgsVO.getSys_id());
                        }
                    }
                }
                
                if (sysOrgTermsQuery != null)
                {
                    /*
                     * Query contains filter by Org so reset the user hasNoOrgAccess flag and check filter Org
                     * Note Query will never contain Org None as there is no such Org
                     */
                    
                    hasNoOrgAccess = false;
                    
                    // Check if passed in Org or one of its children Orgs has access to 'None' Org 
                    
                    Set<OrgsVO> orgVOs = UserUtils.getAllOrgsInHierarchy(sysOrgTermsQuery.getValue(), null);
                    
                    if (orgVOs != null && !orgVOs.isEmpty())
                    {
                        for (OrgsVO orgsVO : orgVOs)
                        {
                            if (orgsVO.getUHasNoOrgAccess().booleanValue())
                            {
                                // Check if user has access to Org having No Org access
                                
                                if (UserUtils.isOrgAccessible(orgsVO.getSys_id(), username, false, false))
                                {
                                    hasNoOrgAccess = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                
                if (hasNoOrgAccess)
                {
                    QueryDTO noOrgQueryDTO = new QueryDTO(queryDTO);
                    
                    noOrgQueryDTO.addFilterItem(new QueryFilter("sysOrg", QueryFilter.NOT_EXISTS));
            
                    noOrgWorksheetsResult = worksheetSearchApi.searchByQuery(noOrgQueryDTO, filters, username);
                }
                else
                {
                    List<Worksheet> emptyWorksheets = Collections.emptyList();
                    noOrgWorksheetsResult.setRecords(emptyWorksheets);
                    noOrgWorksheetsResult.setTotal(0l);
                }
                
                ResponseDTO<Worksheet> orgWorksheetsResult = null;
                
                QueryDTO orgQueryDTO = new QueryDTO(queryDTO);
                
                if (queryDTO.getLimit() > 0)
                {
                    if (noOrgWorksheetsResult.getRecords().size() == queryDTO.getLimit())
                    {
                        orgQueryDTO.setLimit(1);
                        orgQueryDTO.setStart(0);
                    }
                    else if (noOrgWorksheetsResult.getRecords().size() < queryDTO.getLimit())
                    {
                        orgQueryDTO.setLimit(queryDTO.getLimit() - noOrgWorksheetsResult.getRecords().size());
                        
                        if (queryDTO.getStart() >= noOrgWorksheetsResult.getTotal())
                        {
                            orgQueryDTO.setStart(queryDTO.getStart() - (int)noOrgWorksheetsResult.getTotal());
                        }
                        else if (queryDTO.getStart() + noOrgWorksheetsResult.getRecords().size() <= 
                                 noOrgWorksheetsResult.getTotal())
                        {
                            orgQueryDTO.setStart(0);
                        }
                    }
                }
                
                if (!aggrOrgsInHierarchy.isEmpty())
                {
                    if (sysOrgTermsQuery == null)
                    {
                        orgQueryDTO.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, orgInHierarchyIdsCs.toString(), "sysOrg", QueryFilter.EQUALS));
                        orgWorksheetsResult = worksheetSearchApi.searchByQuery(orgQueryDTO, filters, username);
                    }
                    else
                    {
                        Set<String> orgsInTermsQuery = StringUtils.stringToSet(sysOrgTermsQuery.getValue(), ",");
                        
                        if (orgsInHierarchyIds.containsAll(orgsInTermsQuery))
                        {
                            orgWorksheetsResult = worksheetSearchApi.searchByQuery(orgQueryDTO, filters, username);
                        }
                        else
                        {
                            throw new SearchException("User does not belong to specified Org.");
                        }
                    }
                }
                
                List<Worksheet> aggrWorksheets = new ArrayList<Worksheet>();
                long aggrWorksheetsTotal = 0l;
                
                if (noOrgWorksheetsResult != null && noOrgWorksheetsResult.getRecords() != null &&
                    noOrgWorksheetsResult.getTotal() > 0)
                {
                    aggrWorksheetsTotal += noOrgWorksheetsResult.getTotal();
                    
                    aggrWorksheets.addAll(noOrgWorksheetsResult.getRecords());
                }
                
                if (orgWorksheetsResult != null && orgWorksheetsResult.getRecords() != null &&
                    !orgWorksheetsResult.getRecords().isEmpty())
                {
                    if (queryDTO.getLimit() > 0)
                    {
                        if (aggrWorksheets.size() < queryDTO.getLimit())
                        {
                            aggrWorksheets.addAll(orgWorksheetsResult.getRecords());
                        }
                    }
                    else
                    {
                        aggrWorksheets.addAll(orgWorksheetsResult.getRecords());
                    }
                    
                    aggrWorksheetsTotal += orgWorksheetsResult.getTotal();
                }
                
                if (!aggrWorksheets.isEmpty() && aggrWorksheetsTotal > 0l)
                {
                    response = new ResponseDTO<Worksheet>();
                    response.setRecords(aggrWorksheets);
                    response.setTotal(aggrWorksheetsTotal);
                }
            }
            else
            {
                // Admin role should have access to all Orgs including No Org
                
                QueryDTO adminRoleQueryDTO = new QueryDTO(queryDTO);
                
                List<QueryFilter> newQueryFilters = new ArrayList<QueryFilter>();
                
                for (QueryFilter qryFltr : queryDTO.getFilters())
                {
                    if (!(qryFltr.isTermsType() && qryFltr.getField().equals("sysOrg") && 
                          StringUtils.isNotBlank(qryFltr.getValue()) && 
                          qryFltr.getCondition().equalsIgnoreCase(QueryFilter.EQUALS)))
                    {
                        newQueryFilters.add(qryFltr);
                    }
                }
                
                adminRoleQueryDTO.setFilterItems(newQueryFilters);
                
                response = worksheetSearchApi.searchByFilteredQuery(adminRoleQueryDTO, null, filters, username);
            }
            
            if (response != null && response.getRecords() != null && !response.getRecords().isEmpty())
            {
                result = response.getRecords();
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    /**
     * API to be used ONLY by archiving process. It's filtering any worksheet associated with Security Incident.
     * <br>
     * 
     * @param startTime
     * @param endTime
     * @param username
     * @return list of worksheets with just the sysId and sirId
     * @throws SearchException
     */
    public static List<Worksheet> findArchiveWorksheetIdsByUpdatedOn(long startTime, long endTime, String username,
    																 boolean isSIRWSArchive) throws SearchException
    {
    	QueryDTO queryDTO = new QueryDTO();
    	queryDTO.setSelectColumns("sysId");
    	return findArchiveWorksheetsByUpdatedOn(startTime,endTime, username,queryDTO, isSIRWSArchive);
    }
    
    /**
     * API to be used ONLY by archiving process. It's filtering any worksheet associated with Security Incident.
     * <br>
     * 
     * @param startTime
     * @param endTime
     * @param blockSize
     * @param username
     * @return list of worksheets with just the sysId and sirId
     * @throws SearchException
     */
    public static List<Worksheet> findArchiveWorksheetsByUpdatedOn(long startTime, long endTime, int blockSize, 
    															   String username, int start, 
    															   boolean isSIRWSArchive) throws SearchException {
    	return findArchiveWorksheetsByUpdatedOn(startTime,endTime,username,new QueryDTO(start,blockSize), isSIRWSArchive);
    }

    public static List<Worksheet> findArchiveWorksheetsByUpdatedOn(long startTime, long endTime,String username, 
    															   QueryDTO queryDTO, 
    															   boolean isSIRWSArchive) throws SearchException {
    	return findArchiveWorksheetsByUpdatedOn(startTime,endTime,0,username,queryDTO, null, null, isSIRWSArchive);
    }
    /**
     * API to be used ONLY by archiving process. It filters out worksheets associated with Security Incident.
     * <br>
     * 
     * @param startTime
     * @param endTime
     * @param username
     * @param queryDto
     * @return list of worksheets with just the sysId and sirId
     * @throws SearchException
     */
    public static List<Worksheet> findArchiveWorksheetsByUpdatedOn(long startTime, long expiryTime, long cleanupTime, 
    															   String username, QueryDTO queryDTO,Set<String> ignoreIds, 
    															   String[] index, 
    															   boolean isSIRWSArchive) throws SearchException {
        List<Worksheet> result = new ArrayList<Worksheet>();
        ResponseDTO<Worksheet> response = null;
        QueryBuilder finalfilter = null;
        RangeQueryBuilder createdOnFilter = startTime == 0l ? QueryBuilders.rangeQuery("sysCreatedOn").gte(startTime) : 
											QueryBuilders.rangeQuery("sysCreatedOn").from(startTime)
											.to(startTime + Duration.ofDays(1l).toMillis() - 1);
        RangeQueryBuilder expiryFilter = QueryBuilders.rangeQuery("sysUpdatedOn").from(startTime).to(expiryTime);
        QueryBuilder sirNotExistsOrBlank = (!isSIRWSArchive) ? 
				   						   QueryBuilders.boolQuery()
				   						   .must(QueryBuilders.existsQuery("sirId"))
				   						   .mustNot(QueryBuilders.termQuery("sirId", "")) : 
				   						   null;
        
        if(CollectionUtils.isNotEmpty(ignoreIds)) {
	        QueryBuilder ignorefilter = QueryBuilders.termsQuery("sysId", ignoreIds);
	        
	        if (isSIRWSArchive) {
	        	finalfilter = QueryBuilders.boolQuery().must(QueryBuilders.wildcardQuery("sirId", "SIR-*"))
      			  	  		  .should(createdOnFilter).should(expiryFilter).mustNot(ignorefilter);
	        } else {
	        	finalfilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.constantScoreQuery(sirNotExistsOrBlank))
      			  	  		  .should(createdOnFilter).should(expiryFilter).mustNot(ignorefilter);
	        }
        } else {
        	if (isSIRWSArchive) {
        		finalfilter = QueryBuilders.boolQuery().must(QueryBuilders.wildcardQuery("sirId", "SIR-*"))
        				  	  .should(createdOnFilter).should(expiryFilter);
        	} else {
        		finalfilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.constantScoreQuery(sirNotExistsOrBlank))
			  	  		  	  .should(createdOnFilter).should(expiryFilter);
        	}
        }
        
        try {            
            if (!UserUtils.isAdminUser(username)) {
                /*
                 * Restrict worksheet access of non-admin role users to worksheets belonging 
                 * to orgs in hierarchy only.
                 */
                
                QueryFilter sysOrgTermsQuery = null;
                
                for (QueryFilter qryFltr : queryDTO.getFilters())
                {
                    if (qryFltr.isTermsType() && qryFltr.getField().equals("sysOrg") && 
                        StringUtils.isNotBlank(qryFltr.getValue()) && 
                        qryFltr.getCondition().equalsIgnoreCase(QueryFilter.EQUALS))
                    {
                        sysOrgTermsQuery = qryFltr;
                        break;
                    }
                }
                
                ResponseDTO<Worksheet> noOrgWorksheetsResult = new ResponseDTO<Worksheet>();
                
                boolean hasNoOrgAccess = false;                
                UsersVO usersVO = UserUtils.getUserNoCache(username);                
                Set<OrgsVO> aggrOrgsInHierarchy = new HashSet<OrgsVO>();
                StringBuilder orgInHierarchyIdsCs = new StringBuilder();
                Set<String> orgsInHierarchyIds = new HashSet<String>();
                
                if (usersVO != null && usersVO.getUserOrgs() != null && !usersVO.getUserOrgs().isEmpty())
                {
                    Set<OrgsVO> userOrgVOs = usersVO.getUserOrgs();
                    
                    for (OrgsVO userOrgsVO : userOrgVOs)
                    {
                        if (!userOrgsVO.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                        {
                            Set<OrgsVO> orgsInHierarchy = UserUtils.getAllOrgsInHierarchy(userOrgsVO.getSys_id(), userOrgsVO.getUName());
                            aggrOrgsInHierarchy.addAll(orgsInHierarchy);
                        }
                        else
                        {
                            hasNoOrgAccess = true;
                        }
                    }
                    
                    if (!aggrOrgsInHierarchy.isEmpty())
                    {
                        for (OrgsVO userOrgsVO : aggrOrgsInHierarchy)
                        {
                            if (!hasNoOrgAccess)
                            {
                                if (userOrgsVO.getUHasNoOrgAccess().booleanValue())
                                {
                                    hasNoOrgAccess = true;
                                }
                            }
                            
                            if (orgInHierarchyIdsCs.length() > 0)
                            {
                                orgInHierarchyIdsCs.append(",");
                            }
                            
                            orgInHierarchyIdsCs.append(userOrgsVO.getSys_id());
                            orgsInHierarchyIds.add(userOrgsVO.getSys_id());
                        }
                    }
                }
                
                if (sysOrgTermsQuery != null)
                {
                    /*
                     * Query contains filter by Org so reset the user hasNoOrgAccess flag and check filter Org
                     * Note Query will never contain Org None as there is no such Org
                     */
                    
                    hasNoOrgAccess = false;
                    
                    // Check if passed in Org or one of its children Orgs has access to 'None' Org 
                    
                    Set<OrgsVO> orgVOs = UserUtils.getAllOrgsInHierarchy(sysOrgTermsQuery.getValue(), null);
                    
                    if (orgVOs != null && !orgVOs.isEmpty())
                    {
                        for (OrgsVO orgsVO : orgVOs)
                        {
                            if (orgsVO.getUHasNoOrgAccess().booleanValue())
                            {
                                // Check if user has access to Org having No Org access
                                
                                if (UserUtils.isOrgAccessible(orgsVO.getSys_id(), username, false, false))
                                {
                                    hasNoOrgAccess = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                
                if (hasNoOrgAccess)
                {
                    QueryDTO noOrgQueryDTO = new QueryDTO(queryDTO);
                    
                    noOrgQueryDTO.addFilterItem(new QueryFilter("sysOrg", QueryFilter.NOT_EXISTS));
            
                    noOrgWorksheetsResult = worksheetSearchApi.searchByFilteredQuery(
                                                noOrgQueryDTO, 
                                                SearchUtils.createBoolQueryBuilder(noOrgQueryDTO), 
                                                new QueryBuilder[] { finalfilter }, username);
                }
                else
                {
                    List<Worksheet> emptyWorksheets = Collections.emptyList();
                    noOrgWorksheetsResult.setRecords(emptyWorksheets);
                    noOrgWorksheetsResult.setTotal(0l);
                }
                
                ResponseDTO<Worksheet> orgWorksheetsResult = null;
                
                QueryDTO orgQueryDTO = new QueryDTO(queryDTO);
                
                if (queryDTO.getLimit() > 0)
                {
                    if (noOrgWorksheetsResult.getRecords().size() == queryDTO.getLimit())
                    {
                        orgQueryDTO.setLimit(1);
                        orgQueryDTO.setStart(0);
                    }
                    else if (noOrgWorksheetsResult.getRecords().size() < queryDTO.getLimit())
                    {
                        orgQueryDTO.setLimit(queryDTO.getLimit() - noOrgWorksheetsResult.getRecords().size());
                        
                        if (queryDTO.getStart() >= noOrgWorksheetsResult.getTotal())
                        {
                            orgQueryDTO.setStart(queryDTO.getStart() - (int)noOrgWorksheetsResult.getTotal());
                        }
                        else if (queryDTO.getStart() + noOrgWorksheetsResult.getRecords().size() <= 
                                 noOrgWorksheetsResult.getTotal())
                        {
                            orgQueryDTO.setStart(0);
                        }
                    }
                }
                
                if (!aggrOrgsInHierarchy.isEmpty())
                {
                    if (sysOrgTermsQuery == null)
                    {
                        orgQueryDTO.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, orgInHierarchyIdsCs.toString(), "sysOrg", QueryFilter.EQUALS));
                        orgWorksheetsResult = worksheetSearchApi.searchByFilteredQuery(
                                                  orgQueryDTO, 
                                                  SearchUtils.createBoolQueryBuilder(orgQueryDTO), 
                                                  new QueryBuilder[] { finalfilter }, 
                                                  username);
                    }
                    else
                    {
                        Set<String> orgsInTermsQuery = StringUtils.stringToSet(sysOrgTermsQuery.getValue(), ",");
                        
                        if (orgsInHierarchyIds.containsAll(orgsInTermsQuery))
                        {
                            orgWorksheetsResult = worksheetSearchApi.searchByFilteredQuery(
                                                      orgQueryDTO, 
                                                      SearchUtils.createBoolQueryBuilder(orgQueryDTO), 
                                                      new QueryBuilder[] { finalfilter }, 
                                                      username);
                        }
                        else
                        {
                            throw new SearchException("User does not belong to specified Org.");
                        }
                    }
                }
                
                List<Worksheet> aggrWorksheets = new ArrayList<Worksheet>();
                long aggrWorksheetsTotal = 0l;
                
                if (noOrgWorksheetsResult != null && noOrgWorksheetsResult.getRecords() != null &&
                    noOrgWorksheetsResult.getTotal() > 0)
                {
                    aggrWorksheetsTotal += noOrgWorksheetsResult.getTotal();
                    
                    aggrWorksheets.addAll(noOrgWorksheetsResult.getRecords());
                }
                
                if (orgWorksheetsResult != null && orgWorksheetsResult.getRecords() != null &&
                    !orgWorksheetsResult.getRecords().isEmpty())
                {
                    if (queryDTO.getLimit() > 0)
                    {
                        if (aggrWorksheets.size() < queryDTO.getLimit())
                        {
                            aggrWorksheets.addAll(orgWorksheetsResult.getRecords());
                        }
                    }
                    else
                    {
                        aggrWorksheets.addAll(orgWorksheetsResult.getRecords());
                    }
                    
                    aggrWorksheetsTotal += orgWorksheetsResult.getTotal();
                }
                
                if (!aggrWorksheets.isEmpty() && aggrWorksheetsTotal > 0l)
                {
                    response = new ResponseDTO<Worksheet>();
                    response.setRecords(aggrWorksheets);
                    response.setTotal(aggrWorksheetsTotal);
                    
                    result = response.getRecords();
                }
            } else {
                // Admin role should have access to all Orgs including No Org
                
            	String scrollId = null;
            	
            	try {
            		TimeValue scrollTimeOut = new TimeValue(worksheetSearchApi.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);
            		
            		QueryDTO adminRoleQueryDTO = new QueryDTO(queryDTO);
                    
                    List<QueryFilter> newQueryFilters = new ArrayList<QueryFilter>();
                    
                    for (QueryFilter qryFltr : queryDTO.getFilters()) {
                        if (!(qryFltr.isTermsType() && qryFltr.getField().equals("sysOrg") && 
                              StringUtils.isNotBlank(qryFltr.getValue()) && 
                              qryFltr.getCondition().equalsIgnoreCase(QueryFilter.EQUALS))) {
                            newQueryFilters.add(qryFltr);
                        }
                    }
                    
                    adminRoleQueryDTO.setFilterItems(newQueryFilters);
            		
                    Pair<ResponseDTO<Worksheet>, String> scrollWorksheets;
                    
                    if (index != null) {								  
                    	scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(adminRoleQueryDTO, null, index, 
                    															    new QueryBuilder[] { finalfilter }, 
                    															    username, scrollId, scrollTimeOut);
                    } else {
                    	scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(adminRoleQueryDTO, null,
                    																new QueryBuilder[] { finalfilter }, 
                    																username, scrollId, scrollTimeOut);
                    }
                    
        			while(scrollWorksheets != null && scrollWorksheets.getLeft() != null &&
                		  CollectionUtils.isNotEmpty(scrollWorksheets.getLeft().getRecords())) {
        				scrollId = StringUtils.isNotBlank(scrollWorksheets.getRight()) ? scrollWorksheets.getRight() : null;
        				
        				result.addAll(scrollWorksheets.getLeft().getRecords());
        				scrollWorksheets = null;
    					
        				if (StringUtils.isNotBlank(scrollId)) {
	    					if (index != null) {								  
	                        	scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(adminRoleQueryDTO, null, index, 
	                        															    new QueryBuilder[] { finalfilter }, 
	                        															    username, scrollId, scrollTimeOut);
	                        } else {
	                        	scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(adminRoleQueryDTO, null,
	                        																new QueryBuilder[] { finalfilter }, 
	                        																username, scrollId, scrollTimeOut);
	                        }
        				}
        			}
            			
        			if (scrollWorksheets != null) {
        				scrollId = StringUtils.isNotBlank(scrollWorksheets.getRight()) ? scrollWorksheets.getRight() : null;
        				
        				if (StringUtils.isNotBlank(scrollId)) {
                			WorksheetSearchAPI.clearWorksheetScroll(scrollId);
                			scrollId = null;
                		}
        			}
            	} catch (Throwable t) {
            		SimpleDateFormat sdf = new SimpleDateFormat(START_DATE_FORMAT);
                	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
                	
        			Log.log.error(String.format("Error %sin getting %s worksheets from indices [%s] which were " +
        										"created between %s and %s (both inclusive) and updated between %s and " +
        										"%s (both inclusive)", 
        										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
        										(isSIRWSArchive ? "SIR" : "Non-SIR"),
        										StringUtils.listToString(Arrays.asList(index), ", "),
        										sdf.format(new Date(startTime)), 
        										sdf.format(new Date(startTime + Duration.ofDays(1l).toMillis() - 1)),
        										sdf.format(new Date(startTime)), sdf.format(new Date(expiryTime))), t);
            	} finally {
            		if (StringUtils.isNotBlank(scrollId)) {
            			WorksheetSearchAPI.clearWorksheetScroll(scrollId);
            		}
            	}
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    public static List<TaskResult> findArchiveTaskResultIdsByUpdatedOn(QueryDTO queryDTO, Set<String> ignoreIds, long startTime, long expiryTime, long cleanupTime, String username, String[] indexNames) throws SearchException
    {
    	List<TaskResult> result = new ArrayList<TaskResult>();
    	
    	String scrollId = null;
    	
    	try {
    		TimeValue scrollTimeOut = new TimeValue(taskResultSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);
    		
    		QueryBuilder finalfilter = null;
            RangeQueryBuilder createdOnFilter = startTime == 0l ? QueryBuilders.rangeQuery("sysCreatedOn").gte(startTime) : 
            									QueryBuilders.rangeQuery("sysCreatedOn").from(startTime)
            									.to(startTime + Duration.ofDays(1l).toMillis() - 1);
            RangeQueryBuilder expiryFilter = QueryBuilders.rangeQuery("sysUpdatedOn").from(startTime).to(expiryTime);
            
            if(CollectionUtils.isNotEmpty(ignoreIds)) {
    	        QueryBuilder ignorefilter = QueryBuilders.termsQuery("problemId", ignoreIds);
    	        finalfilter = QueryBuilders.boolQuery().should(createdOnFilter).should(expiryFilter).mustNot(ignorefilter);
            }
            else {
            	finalfilter = QueryBuilders.boolQuery().should(createdOnFilter).should(expiryFilter);
            }
            
            QueryBuilder[] filterBuilders = new QueryBuilder[] { finalfilter };
            
            Pair<ResponseDTO<TaskResult>, String> scrollTaskResults;
            
            if(indexNames != null) {
            	scrollTaskResults =  taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, indexNames, filterBuilders, 
            																   username, scrollId, scrollTimeOut);
            } else {
            	scrollTaskResults = taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, filterBuilders, username,
            																  scrollId, scrollTimeOut);
            }
            
            while(scrollTaskResults != null && scrollTaskResults.getLeft() != null &&
          		  CollectionUtils.isNotEmpty(scrollTaskResults.getLeft().getRecords())) {
            	scrollId = StringUtils.isNotBlank(scrollTaskResults.getRight()) ? scrollTaskResults.getRight() : null;
            	
            	result.addAll(scrollTaskResults.getLeft().getRecords());
            	scrollTaskResults = null;
            	
            	if (StringUtils.isNotBlank(scrollId)) {
	            	if(indexNames != null) {
	                	scrollTaskResults =  taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, indexNames, 
	                																   filterBuilders, username, scrollId, 
	                																   scrollTimeOut);
	                } else {
	                	scrollTaskResults = taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, filterBuilders, username,
	                																  scrollId, scrollTimeOut);
	                }
            	}
            }
            
            if (scrollTaskResults != null) {
            	scrollId = StringUtils.isNotBlank(scrollTaskResults.getRight()) ? scrollTaskResults.getRight() : null;
            	
            	if (StringUtils.isNotBlank(scrollId)) {
        			WorksheetSearchAPI.clearTaskResultScroll(scrollId);
        			scrollId = null;
        		}
			}
    	}  catch (Throwable t) {
    		SimpleDateFormat sdf = new SimpleDateFormat(START_DATE_FORMAT);
        	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
        	
			Log.log.error(String.format("Error %sin getting non-SIR task results from indices [%s] which were " +
										"created between %s and %s (both inclusive) and updated between %s and " +
										"%s (both inclusive)", 
										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										StringUtils.listToString(Arrays.asList(indexNames), ", "),
										sdf.format(new Date(startTime)), 
										sdf.format(new Date(startTime + Duration.ofDays(1l).toMillis() - 1)),
										sdf.format(new Date(startTime)), sdf.format(new Date(expiryTime))), t);
    	} finally {
    		if (StringUtils.isNotBlank(scrollId)) {
    			WorksheetSearchAPI.clearTaskResultScroll(scrollId);
    		}
    	}
    	
    	return result;
    }
    
    @Deprecated
    public static ResponseDTO<ProcessRequest> findProcessRequestsForArchive(QueryDTO queryDTO, long expiryTime, long cleanupTime, String username) throws SearchException
    {
        return findProcessRequestsForArchive(queryDTO,expiryTime,cleanupTime,0,username);
    }
    
    @Deprecated
    public static ResponseDTO<ProcessRequest> findProcessRequestsForArchive(QueryDTO queryDTO, long expiryTime, long cleanupTime, long startTime, String username) throws SearchException
    {
    	return findProcessRequestsForArchive(queryDTO,null,expiryTime,cleanupTime,startTime,null,username);
    }
    
    /**
     * This method return records with the condition similar to following:
     * 
     * SELECT * FROM resolve_process_request WHERE (u_status <> 'OPEN' AND
     * sys_updated_on < 1386119566011) OR (sys_updated_on < 1386119566011L);
     * 
     * @param queryDTO
     * @param expiryTime
     * @param cleanupTime
     * @param username
     * @return
     * @throws SearchException
     */
    @Deprecated
    public static ResponseDTO<ProcessRequest> findProcessRequestsForArchive(QueryDTO queryDTO,String[] indexNames, long expiryTime, long cleanupTime, long startTime, Set<String> ignoreIds,String username) throws SearchException
    {
    	QueryBuilder finalFilter = null;
    	QueryBuilder termsFilterBuilder = QueryBuilders.boolQuery().mustNot(QueryBuilders.queryStringQuery(Constants.ASSESS_STATUS_OPEN.toLowerCase() + " OR " + Constants.ASSESS_STATUS_WAITING.toLowerCase()).defaultField("status"));
        RangeQueryBuilder expiryDateFilterBuilder = QueryBuilders.rangeQuery("sysUpdatedOn");
        expiryDateFilterBuilder.from(startTime);
        expiryDateFilterBuilder.to(expiryTime);
        QueryBuilder expiryFilter = QueryBuilders.boolQuery().must(expiryDateFilterBuilder).must(termsFilterBuilder);
        RangeQueryBuilder cleanupFilterBuilder = QueryBuilders.rangeQuery("sysUpdatedOn").from(startTime).to(cleanupTime);
        if(CollectionUtils.isNotEmpty(ignoreIds)) {
        	QueryBuilder ignoreFilter = QueryBuilders.termsQuery("problem", ignoreIds);
        	finalFilter = QueryBuilders.boolQuery().should(expiryFilter).should(cleanupFilterBuilder).mustNot(ignoreFilter);
        }
        else
        	finalFilter = QueryBuilders.boolQuery().should(expiryFilter).should(cleanupFilterBuilder);

        QueryBuilder[] filterBuilders = new QueryBuilder[] { finalFilter };
        if(indexNames != null)
        	return processRequestSearchAPI.searchByFilteredQuery(queryDTO, null, indexNames, filterBuilders, username);
        else
        	return processRequestSearchAPI.searchByFilteredQuery(queryDTO, null, filterBuilders, username);
    }
    
    private static Set<String> filterOutWorksheetsUpdatedAfterExpiry(Long expiryTime, Set<String> batch) throws SearchException {
    	Set<String> wssUpdatedAfterExpiry = new HashSet<String>();
    	List<String> wsIdsToCheck = Arrays.asList(batch.toArray(new String[1]));
    	int start = 0;
    	int limit = 10000;
    	boolean contQuery = true;
    	
    	while(contQuery) {
    		SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), 
				 															 	 SearchAction.INSTANCE);
    	
    		searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
    		searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS);
        
    		QueryDTO queryDTO = new QueryDTO();
        
    		queryDTO.addFilterItem(new QueryFilter("numeric", Long.toString(expiryTime), "sysUpdatedOn",
        									   	   QueryFilter.GREATER_THAN));
    		
    		BoolQueryBuilder qb =(BoolQueryBuilder)SearchUtils.createBoolQueryBuilder(queryDTO);
    		
    		TermsQueryBuilder terms = new TermsQueryBuilder("sysId", 
    														wsIdsToCheck.subList(start, 
    																			 ((start + limit) <  wsIdsToCheck.size() ? 
    																			  (start + limit) : wsIdsToCheck.size())));
    		qb.filter(terms);
    		searchRequestBuilder.setQuery(qb);
    		searchRequestBuilder.setFetchSource("sysId", null);
    		searchRequestBuilder.setSize(limit);
    		searchRequestBuilder.setFrom(start);
    		
    		SearchResponse response = searchRequestBuilder.execute().actionGet();
    		
    		start += limit;
    		contQuery = response.getHits().getTotalHits() == limit;
    		
    		for (SearchHit hit : response.getHits()) {
    			if (!batch.contains((String)hit.getSource().get("sysId"))) {
    				wssUpdatedAfterExpiry.add((String)hit.getSource().get("sysId"));
    			}
    		}
    	}
        
        return wssUpdatedAfterExpiry;
    }
    
    public static Set<String> getRecentlyUsedWorksheets(Long expiryTime) throws SearchException
    {
    	Set<String> result = new HashSet<String>();
    	Set<String> batch = new HashSet<String>();
    	int start = 0;
    	int limit = 10000;
    	boolean contQuery = true;
    	while(contQuery) {
    		batch.clear();
	    	QueryDTO queryDTO = new QueryDTO();
	        queryDTO.setSelectColumns("problemId");
	        SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), 
	        																	 SearchAction.INSTANCE);
	        searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS);
	        queryDTO.addFilterItem(new QueryFilter("numeric",Long.toString(expiryTime), "sysUpdatedOn",
	        									   QueryFilter.GREATER_THAN));
	        BoolQueryBuilder qb = (BoolQueryBuilder)SearchUtils.createBoolQueryBuilder(queryDTO);
	        searchRequestBuilder.setQuery(qb);
	        TermsAggregationBuilder problemIdAggs = terms("Doc_name").field("problemId").size(limit);
	        searchRequestBuilder.addAggregation(problemIdAggs);
	        searchRequestBuilder.setFetchSource("problemId",null);
	        searchRequestBuilder.setSize(limit);
	        searchRequestBuilder.setFrom(start);
	        SearchResponse response = searchRequestBuilder.execute().actionGet();
	        
	        start += limit;
	        contQuery = response.getHits().getTotalHits() == limit;
	        StringTerms docNameBucket = response.getAggregations().get("Doc_name");
	        for(Bucket docName : docNameBucket.getBuckets()) {
	        	batch.add(docName.getKeyAsString());
	        }
	        
	        if(CollectionUtils.isNotEmpty(batch)) {
	        	Set<String> worksheetsWithATsAfterExpiry = filterOutWorksheetsUpdatedAfterExpiry(expiryTime, batch);
	        	
	        	if (CollectionUtils.isNotEmpty(worksheetsWithATsAfterExpiry)) {
	        		batch.removeAll(worksheetsWithATsAfterExpiry);
	        	}
	        }
	        
	        result.addAll(batch);
    	}
    	
        return result; 
    }
    
    public static Set<String> getRecentlyUsedWSDATAWorksheets(Long expiryTime) throws SearchException {
    	Set<String> result = new HashSet<String>();
    	Set<String> batch = new HashSet<String>();
    	int start = 0;
    	int limit = 10000;
    	boolean contQuery = true;
    	while(contQuery) {
    		batch.clear();
    		QueryDTO queryDTO = new QueryDTO();
	        queryDTO.setSelectColumns("worksheetId");
	        SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), 
					 															 SearchAction.INSTANCE);
	        searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WORKSHEET_DATA);
	        queryDTO.addFilterItem(new QueryFilter("numeric", Long.toString(expiryTime), "sysUpdatedOn",
					   							   QueryFilter.GREATER_THAN));
	        BoolQueryBuilder qb = (BoolQueryBuilder)SearchUtils.createBoolQueryBuilder(queryDTO);
	        searchRequestBuilder.setQuery(qb);	        
	        searchRequestBuilder.setFetchSource("worksheetId",null);
	        searchRequestBuilder.setSize(limit);
	        searchRequestBuilder.setFrom(start);
	        SearchResponse response = searchRequestBuilder.execute().actionGet();
	        
	        start += limit;
	        contQuery = response.getHits().getTotalHits() == limit;
	        
	        for (SearchHit hit : response.getHits()) {
	        	if (hit.getSource().containsKey("worksheetId")) {
	        		batch.add((String)hit.getSource().get("worksheetId"));
	        	}
	        }
	        
	        if (CollectionUtils.isNotEmpty(batch)) {
	        	Set<String> worksheetsWithWSDATAAfterExpiry = filterOutWorksheetsUpdatedAfterExpiry(expiryTime, batch);
	        	
	        	if (CollectionUtils.isNotEmpty(worksheetsWithWSDATAAfterExpiry)) {
	        		batch.removeAll(worksheetsWithWSDATAAfterExpiry);
	        	}
	        }
	        
	        result.addAll(batch);
    	}
    	
    	return result;
    }
    
    public static long getWorksheetResultCount(String worksheetId, String username) throws SearchException
    {
        QueryBuilder worksheetFilter = QueryBuilders.termQuery("problemId", worksheetId);
        return taskResultSearchAPI.getCountByQuery(username, worksheetFilter);
    }
    
    public static Set<String> getOpenProcessRequests(Long expiryTime, Long cleanupTime)
    {
    	final Set<String> result = new HashSet<String>();
    	String scrollId = null;
    	
    	try {
    		TimeValue scrollTimeOut = new TimeValue(processRequestSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);
    		
    		QueryDTO queryDTO = new QueryDTO();
    		
    		queryDTO.setSelectColumns("problem");
    		queryDTO.addSortItem(new QuerySort("sysUpdatedOn",QuerySort.SortOrder.ASC));
    		
    		QueryBuilder[] filterQBs = new QueryBuilder[2];
    		
    		filterQBs[0] = new TermsQueryBuilder("status",Constants.ASSESS_STATUS_OPEN.toLowerCase());
    		
    		RangeQueryBuilder range = new RangeQueryBuilder("sysCreatedOn");
    		range.gt(cleanupTime);
    		range.lte(expiryTime);
    		
    		filterQBs[1] = range;
    		
    		QueryBuilder matchAllQB = new MatchAllQueryBuilder();
    		
    		List<String> procReqIds = new CopyOnWriteArrayList<String>();
    		
    		Pair<ResponseDTO<ProcessRequest>, String> scrollProcReqs = 
    														processRequestSearchAPI.searchByFilteredQuery(
    																queryDTO, matchAllQB, filterQBs, "system",
    																scrollId, scrollTimeOut);
    		
			while(scrollProcReqs != null && scrollProcReqs.getLeft() != null &&
	    		  CollectionUtils.isNotEmpty(scrollProcReqs.getLeft().getRecords())) {
				scrollId = StringUtils.isNotBlank(scrollProcReqs.getRight()) ? scrollProcReqs.getRight() : null;
				
				scrollProcReqs.getLeft().getRecords().parallelStream()
				.forEach(procReq -> procReqIds.add(procReq.getProblem()));
				
				result.addAll(procReqIds);
				
				procReqIds.clear();
				scrollProcReqs = null;
				
				if (StringUtils.isNotBlank(scrollId)) {
					scrollProcReqs = processRequestSearchAPI.searchByFilteredQuery(queryDTO, matchAllQB, filterQBs, "system",
																			   	   scrollId, scrollTimeOut);
				}
			}
			
			if (scrollProcReqs != null) {
				scrollId = StringUtils.isNotBlank(scrollProcReqs.getRight()) ? scrollProcReqs.getRight() : null;
				
				if (StringUtils.isNotBlank(scrollId)) {
	    			WorksheetSearchAPI.clearProcessRequestScroll(scrollId);
	    			scrollId = null;
	    		}
			}
    	} catch (Throwable t) {
    		SimpleDateFormat sdf = new SimpleDateFormat(START_DATE_FORMAT);
        	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
        	
			Log.log.error(String.format("Error %sin getting process requests which were created after %s and on or " +
										"before %s and which are still in open state", 
										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										sdf.format(new Date(cleanupTime)), sdf.format(new Date(expiryTime))), t);
    	} finally {
    		if (StringUtils.isNotBlank(scrollId)) {
    			WorksheetSearchAPI.clearProcessRequestScroll(scrollId);
    		}
    	}
    	
    	return result;
    }
    
    public static long getOpenProcessRequestsCount(String worksheetId, String wiki, String username) throws SearchException
    {
        return (Long)getOpenProcessRequestsCountObject(worksheetId, wiki, username).get(PROCESSREQUEST_STATUS);
    }
    
    public static Map<String, Object> getOpenProcessRequestsCountObject(String worksheetId, String wiki, String username) throws SearchException
    {
        Map<String, Object> result = new HashMap<String, Object>();
        QueryDTO queryDTO = new QueryDTO(0,  1);
        queryDTO.addFilterItem(new QueryFilter("problem", QueryFilter.EQUALS, worksheetId));
        queryDTO.setSelectColumns("status,sysUpdatedOn");
        List<ProcessRequest> prs = WorksheetSearchAPI.searchProcessRequestsByFiltersOnly(queryDTO, username).getRecords();
        long openStatus = 1;
        long updatedOn = 0;
        
        if (prs==null || prs.isEmpty())
        {
            openStatus = 1; // open because no info is available
            updatedOn = System.currentTimeMillis();
        }
        else
        {
            openStatus = 0;
            for (ProcessRequest pr : prs)
            {
                String status = pr.getStatus();
                updatedOn = (updatedOn < pr.getSysUpdatedOn()) ? pr.getSysUpdatedOn() : updatedOn ;
                if (Constants.ASSESS_STATUS_OPEN.equals(status))
                {
                    openStatus = 1;
                    break;
                }
            }
        }

        result.put(PROCESSREQUEST_STATUS, openStatus);
        result.put(PROCESSREQUEST_UPDATEDON, updatedOn);
        
        return result;
    }

    public static long getOpenProcessRequestsCount(Worksheet ws, String wiki, String username) throws SearchException
    {
        return (Long)getOpenProcessRequestsCountObject(ws, wiki, username,null).get(PROCESSREQUEST_STATUS);
    }
    public static long getOpenProcessRequestsCount(Worksheet ws, String wiki, String username,long cleanupTime) throws SearchException
    {
        return (Long)getOpenProcessRequestsCountObject(ws, wiki, username,cleanupTime).get(PROCESSREQUEST_STATUS);
    }
    public static Map<String, Object> getOpenProcessRequestsCountObject( Worksheet ws, String wiki, String username,Long cleanupTime) throws SearchException
    {
    	String worksheetId = ws.getSysId();
        Map<String, Object> result = new HashMap<String, Object>();
        QueryDTO queryDTO = new QueryDTO(0,  1);
        queryDTO.addFilterItem(new QueryFilter("problem", QueryFilter.EQUALS, worksheetId));
        if(cleanupTime != null)
        {
        	QueryFilter cleanupFilter = new QueryFilter("sysUpdatedOn",QueryFilter.GREATER_THAN_OR_EQUAL,cleanupTime.toString());
            queryDTO.addFilterItem(cleanupFilter);
        }
        queryDTO.setSelectColumns("status,sysUpdatedOn");
        List<ProcessRequest> prs = WorksheetSearchAPI.searchProcessRequestsByFiltersOnly(queryDTO, "system").getRecords();
        long openStatus = 1;
        long updatedOn = 0;
        
        if (prs==null || prs.isEmpty())
        {
        	long now = System.currentTimeMillis();
        	if (now - ws.getSysUpdatedOn().longValue() > 10*60*1000)  // 10 minutes is too long for open process to start
        	{
        		openStatus = 0;
        	}
        	else
        	{
	            openStatus = 1; // open because no info is available
	            updatedOn = System.currentTimeMillis();
        	}
        }
        else
        {
            openStatus = 0;
            for (ProcessRequest pr : prs)
            {
                String status = pr.getStatus();
                updatedOn = (updatedOn < pr.getSysUpdatedOn()) ? pr.getSysUpdatedOn() : updatedOn ;
                if (Constants.ASSESS_STATUS_OPEN.equals(status))
                {
                    openStatus = 1;
                    break;
                }
            }
        }

        result.put(PROCESSREQUEST_STATUS, openStatus);
        result.put(PROCESSREQUEST_UPDATEDON, updatedOn);
        
        return result;
    }
    
    public static long getWaitProcessRequestsCount(String worksheetId, String wiki, String username) throws SearchException
    {
        long waitingCount = 0;
        
        QueryBuilder worksheetFilter = QueryBuilders.termQuery("problem", worksheetId);
        QueryBuilder wikiFilter = null;
        if (StringUtils.isNotBlank(wiki))
        {
            wikiFilter = QueryBuilders.termQuery("wiki", wiki.toLowerCase());
        }
        QueryBuilder openProcessFilterBuilder = QueryBuilders.termQuery("status", Constants.ASSESS_STATUS_WAITING.toLowerCase());
        
        if (UserUtils.isSuperUser(username))
        {
            waitingCount = processRequestSearchAPI.getCountByQuery(username, worksheetFilter, wikiFilter, openProcessFilterBuilder);
        }
        else
        {
            waitingCount = processRequestSearchAPI.getNonSuperUserProcReqCountByQuery(username, worksheetFilter, wikiFilter, openProcessFilterBuilder);
        }
        
        return waitingCount;
    }
    
    public static Map<String, Object> getWorksheetData(final String worksheetId, final List<String> propertyNames, 
    												   final String username, 
    												   boolean ignoreESInMemCache) throws SearchException {
        Map<String, Object> result =  new HashMap<String, Object>();

        if (Log.log.isDebugEnabled() && StringUtils.isBlank(worksheetId)) {
            Log.log.debug("Blank sysId provided, check the calling code");
        } else {
            try {
                if(propertyNames == null || propertyNames.size() == 0) {
                    // Check if user has access to Worksheet's Org
                    
                    findWorksheetById(worksheetId, username);
                    
                    QueryBuilder queryBuilder = QueryBuilders.termQuery("worksheetId", worksheetId);
                    ResponseDTO<WorksheetData> response = worksheetDataSearchApi.scrollByQuery(queryBuilder, 1000, 60000);
                    if(response.getRecords() != null && response.getRecords().size() > 0) {
                        for (WorksheetData worksheetData : response.getRecords()) {
                            result.put(worksheetData.getPropertyName(), ObjectProperties.deserialize(worksheetData.getPropertyValue()));
                        }
                    }
                } else {
                    if(propertyNames != null && propertyNames.size() > 0) {
                        for (String propertyKey: propertyNames) {
                            Object propertyValue = getWorksheetData(worksheetId, propertyKey, username);
                            result.put(propertyKey, propertyValue);
                        }
                    }
                }
            } catch (SearchException e) {
                Log.log.error(e.getMessage(), e);
                throw e;
            }

        }
        return result;
    }
    
    public static Map<String, Object> getWorksheetData(final String worksheetId, final List<String> propertyNames, 
    												   final String username) throws SearchException {
    	return getWorksheetData(worksheetId, propertyNames, username, false);
    }
    
    public static Object getWorksheetData(final String worksheetId, final String propertyName, 
    									  final String username, boolean ignoreESInMemCache) throws SearchException {
    	Object result = null;

        if (Log.log.isDebugEnabled() && (StringUtils.isBlank(worksheetId) || StringUtils.isBlank(propertyName))) {
            Log.log.debug("Blank worksheetId or propertyName provided, check the calling code");
        } else {
            try {
                WorksheetData worksheetData = worksheetDataSearchApi.findDocumentById(worksheetId+propertyName, worksheetId, 
                																	  username, ignoreESInMemCache);
                
                if(worksheetData != null) {
                    byte[] data = worksheetData.getPropertyValue();
                    result = ObjectProperties.deserialize(data);
                }
            } catch (SearchException e) {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;
    }
    
    public static Object getWorksheetData(final String worksheetId, final String propertyName, final String username) throws SearchException
    {
    	return getWorksheetData(worksheetId, propertyName, username, false);
    }
    
    public static ResponseDTO<ExecuteState> findExecuteStateById(String sysId, String username) throws SearchException
    {
        ResponseDTO<ExecuteState> result = null;

        try
        {
            if (StringUtils.isBlank(sysId))
            {
                throw new SearchException("sysId must not be blank");
            }
            result = new ResponseDTO<ExecuteState>();
            result.setData(executeStateSearchAPI.findDocumentById(sysId, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    private static void filterByAccessibleOrgs(ResponseDTO<? extends BaseIndexModel> result, String username)
    {
        Collection<BaseIndexModel> pbModelsToRemove = new ArrayList<BaseIndexModel>();
        Map<String,String> worksheetOrgs = new HashMap<String,String>();
        List<String> worksheetsToRemove = new ArrayList<String>();
        
        for (BaseIndexModel model : result.getRecords())
        {
            String worksheetId = null;
            
            if (model instanceof ProcessRequest)
            {
                worksheetId = ((ProcessRequest)model).getProblem();
            }
            else if (model instanceof TaskResult)
            {
                worksheetId = ((TaskResult)model).getProblemId();
            }
            
            if (StringUtils.isNotBlank(worksheetId))
            {
                if (worksheetOrgs.containsKey(worksheetId)) {
                    if (worksheetsToRemove.contains(worksheetId)) {
                        Log.log.debug("Removing Already Valdiated Worksheet : " + worksheetId);
                        pbModelsToRemove.add(model);
                    } else if (worksheetOrgs.get(worksheetId) != null){
                        model.setSysOrg(worksheetOrgs.get(worksheetId));
                    }
                } else {
	                try {
	                    ResponseDTO<Worksheet> worksheetResponse = findWorksheetById(worksheetId, username);
	                    
	                    if (worksheetResponse != null && worksheetResponse.isSuccess() && 
	                        worksheetResponse.getData() != null &&
	                        StringUtils.isNotBlank(worksheetResponse.getData().getSysOrg())) {
	                        // Txfer Worksheets sysOrg to Model's sysOrg
	                        String org = worksheetResponse.getData().getSysOrg();
	                        model.setSysOrg(org);
	                        worksheetOrgs.put(worksheetId, org);
	                    } else {
	                        worksheetOrgs.put(worksheetId, null);
	                    }
	                } catch (Exception e) {
	                  if(Log.log.isDebugEnabled()) {
  	                    Log.log.debug("Error " + e.getLocalizedMessage() + 
  	                        " in getting worksheet with id " + worksheetId + 
  	                        " associated with " + model.getClass().getSimpleName()); 
  	                    pbModelsToRemove.add(model);
  	                    worksheetsToRemove.add(worksheetId);
  	                    worksheetOrgs.put(worksheetId, null);
	                  }
	                }
                }
            }
        }
        
        if (!pbModelsToRemove.isEmpty())
        {
            result.getRecords().removeAll(pbModelsToRemove);
            result.setTotal(result.getRecords().size());
        }
    }
    
  public static String ACTIVITY_NAME = "activityName";
  
  /**
   * Gets the execution history of a SIR for by worksheet id, incident id and other parameters. It supports paging, simulated "free form" search by term, selected task/activity names etc.
   * 
   * @param problemId  sys_id of current SIR worksheet  
   * @param incidentId  incident id of  current SIR  
   * @param query  QueryDTO object to provide paging params ( "start", "limit"), and search term param ("searchTerm")  
   * @param columns  string array to define what columns are included in "free form" term search. If null/empty, then all columns will be searched for match. Support search column names are: "activityId", "activityName", "PROCESSID", "SEVERITY", "CONDITION", "SUMMARY"   
   * @param selectAll  corresponding to "ShowAll" option in UI. Basically, if set to true, UI will show all SIR task/activity in execution history   
   * @param activityNames  array of SIR activity/task names included in execution history search. If names are provided, they function as a filter on execution history results.    
   * @param sources  array of SIR source names included in execution history search. If names are provided, they function as a filter on execution history results.  
   * @return {@link ResponseDTO} - Reponse Data Transfer Object having object of type {@Map<String,String>} 
   * @throws Exception
   */
    public static ResponseDTO<Map<String, Object>> searchExecutionHistory(QueryDTO queryDTO, String[] columns, String username, String problemId, Boolean selectAll, String[] activityNames, String[] sources, String incidentId ) throws SearchException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        result.setRecords(new ArrayList<Map<String,Object>>()).setTotal(0);

        try
        {
            ResponseDTO<TaskResult> taskResult = null;
        	QueryDTO newDTO = new QueryDTO(queryDTO);
        	newDTO.setStart(0);
        	newDTO.setLimit(99999);
        	newDTO.setSearchTerm(null);
            newDTO.setModelName(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            newDTO.setSortItems(queryDTO.getSortItems());

            String tsQuery = "taskName:(end OR abort) "; 
            if (StringUtils.isNotBlank(problemId))
            {
            	newDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemId));
            	tsQuery += " problemId:(" + problemId + ")";
            }
            
            List<ActivityVO> activities = PlaybookUtils.listActivitiesByIncidentId(incidentId, username);
            Map<String, String> activityMap = new HashMap<String,String>();
            if (activities!=null && activities.size()>0)
            {
	            for (ActivityVO avo : activities)
	            {
	            	if (avo.getAltActivityId()!=null && avo.getActivityName()!=null)
	            	{
	            		activityMap.put(avo.getAltActivityId(), avo.getActivityName());
	            	}
	            }
            }
            else
            {
            	return result;
            }

            QueryStringQueryBuilder qsb = queryStringQuery(tsQuery).allowLeadingWildcard(true);
            qsb.escape(true).analyzeWildcard(true).defaultOperator(Operator.AND);
            
            SearchUtils.sanitizeQueryDTO(newDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            taskResult = taskResultSearchAPI.searchByQuery(newDTO, qsb, username);
            
            if (taskResult.getTotal()==0)
            	return result;
            
            Set<String> activitySet = new HashSet<String>();
            if (activityNames != null)
            {
            	for (String a : activityNames)
            	{
            		activitySet.add(a);
            	}
            }
            
            if (taskResult != null && taskResult.isSuccess() && 
                taskResult.getRecords() != null && !taskResult.getRecords().isEmpty())
            {
            	List<TaskResult> taskResults = taskResult.getRecords();
            	Map<String, Map<String, Object>> executionHistoryMap = new HashMap<String, Map<String, Object>>();
            	List<String> executionHistoryOrder = new ArrayList<String>();
            	
            	boolean wikiOnly = false;
            	boolean sirOnly = false;
            	if (sources!=null && sources.length>0 )
            	{
            		if ("wiki".equalsIgnoreCase(sources[0]))
            		{
            			wikiOnly = true;
            		}
            		else if ("sir".equalsIgnoreCase(sources[0]))
            		{
            			sirOnly = true;
            		}
            	}
            		
            	
            	for (TaskResult t : taskResults)
            	{
            		if (StringUtils.isBlank(t.getActivityId()))
            		{
            			if (sirOnly)
            			{	
            				continue;
            			}
            		}
            		else
            		{
            			if (wikiOnly)
            				continue;
            		}
            		
            		String key = t.getActivityId() + t.getProcessId();
            		if (executionHistoryMap.get(key)==null && ("end".equalsIgnoreCase(t.getTaskName()) || "abort".equalsIgnoreCase(t.getTaskName())))
            		{
            			executionHistoryMap.put(key, new HashMap<String, Object>());
	        			Map<String, Object> valMap = executionHistoryMap.get(key);
	        			valMap.put(Constants.ACTIVITY_ID, t.getActivityId() );
	        			valMap.put(ACTIVITY_NAME, activityMap.get(t.getActivityId()));

	        			valMap.put("processId", t.getProcessId() );
	        			valMap.put("severity", t.getSeverity());
	        			valMap.put("condition", t.getCondition());
	        			valMap.put("summary", t.getSummary());
	        			
	        			valMap.put(SearchConstants.SYS_CREATED_ON, t.getSysCreatedOn());
	        			valMap.put(SearchConstants.SYS_CREATED_BY, t.getSysCreatedBy());
	        			
	        			if (StringUtils.isBlank(t.getActivityId()))
	        			{
		        			valMap.put("source", "Wiki");
	        			}
	        			else
	        			{
	        				valMap.put("source", "SIR");
	        			}
	        			
	        			if (selectAll)
	        			{
	        				executionHistoryOrder.add(key);
	        			}
	        			else
	        			{
	        				String activityId = t.getActivityId();
	        				String activityName = activityMap.get(activityId);
	        				
	        				if (activitySet.contains(activityName))
	        				{
	        					executionHistoryOrder.add(key);
	        				}
	        				else
	        				{
	        					// This is not in selected activity, so removed it
	        					executionHistoryMap.remove(key);
	        				}
	        			}
	        			
            		}
            	}

            	// Search term matching
            	Set<String> mColSet = new HashSet<String>();
            	if (columns!=null)
            	{
            		for (String c : columns)
            		{
            			if (StringUtils.isNotBlank(c))
            			{
            				mColSet.add(c);
            			}
            		}
            	}
            	
            	String searchTerm = queryDTO.getSearchTerm();
            	if (StringUtils.isNotBlank(searchTerm))
            	{
            		Iterator<String> i = executionHistoryOrder.iterator();
            		while (i.hasNext())
            		{
            			String key = i.next();
            			Map<String, Object> valMap = executionHistoryMap.get(key);
            			boolean matching = false;
            			for (String k : valMap.keySet())
            			{
            				if (k!=null && valMap.get(k)!=null)
            				{	
	            				String v = valMap.get(k).toString().toLowerCase();
	            				if (!mColSet.isEmpty() && !mColSet.contains(k))
	            					continue;
	            				
	            				int pos = v.indexOf(searchTerm.toLowerCase());
	            				if (pos>=0)
	            				{
	            					matching = true;
	            					break;
	            				}
            				}
            			}
            			
            			// Not matching search term, so remove it
                		if (!matching)
                		{
                			i.remove();
                		}
            		}
            	}
            	
            	//paging
                int limit = queryDTO.getLimit();
                int start = queryDTO.getStart() != -1 ? queryDTO.getStart() : 0;
                
                int s = -1; int end = -1; 
                if (start < executionHistoryOrder.size())
                {
                	s = start;
                	
                	if (s+limit-1 < executionHistoryOrder.size())
                	{
                		end = s + limit;
                	}
                	else
                	{
                		end = executionHistoryOrder.size();
                	}
                }
                
                
                List<String> retEHOrder = new ArrayList<String>(); 
                if (s>=0 && end >=0)
                {
                	retEHOrder = executionHistoryOrder.subList(s, end);
                }
                	
                List<Map<String,Object>> records = new ArrayList<Map<String,Object>>();
                for (String i : retEHOrder)
                {
                	Map<String,Object> valMap = executionHistoryMap.get(i);
                	records.add(valMap);
                }
                
            	result.setRecords(records);
            	result.setTotal(executionHistoryOrder.size());
            	result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
    
    public static ResponseDTO<List<String>> getAllProcessRequestIndexNames() {
    	return processRequestSearchAPI.getAllIndexNames();
    }
    
    public static ResponseDTO<String> getAllProcessRequestIndexNames(long expiryTime) {
    	return processRequestSearchAPI.getAllIndexNames(Instant.ofEpochMilli(expiryTime).truncatedTo(ChronoUnit.DAYS));
    }
    
    public static ResponseDTO<List<String>> getAllWorksheetIndexNames()
    {
    	return worksheetSearchApi.getAllIndexNames();
    }
    
    public static ResponseDTO<String> getAllWorksheetIndexNames(long expiryTime) {
    	return worksheetSearchApi.getAllIndexNames(Instant.ofEpochMilli(expiryTime).truncatedTo(ChronoUnit.DAYS));
    }
    
    public static ResponseDTO<List<String>> getAllTaskResultIndexNames()
    {
    	return taskResultSearchAPI.getAllIndexNames();
    }
    
    public static ResponseDTO<String> getAllTaskResultIndexNames(long expiryTime) {
    	return taskResultSearchAPI.getAllIndexNames(Instant.ofEpochMilli(expiryTime).truncatedTo(ChronoUnit.DAYS));
    }
    
    public static ResponseDTO<WorksheetData> findWorksheetDataByWorksheetIds(List<String> worksheetIds) 
    												throws SearchException {
    	ResponseDTO<WorksheetData> result = null;

        try {
            if (CollectionUtils.isEmpty(worksheetIds)) {
                throw new SearchException("Worksheet Ids must not be empty");
            }
            
            result = new ResponseDTO<WorksheetData>();
            result.setSuccess(false);
            
            List<WorksheetData> worksheetDatas = new ArrayList<WorksheetData>();
                      
            QueryBuilder worksheetIdfilter = QueryBuilders.termsQuery("worksheetId", worksheetIds);
	        QueryBuilder finalfilter = QueryBuilders.boolQuery().must(worksheetIdfilter);
	        		
            String scrollId = null;
        	
        	try {
        		TimeValue scrollTimeOut = new TimeValue(worksheetDataSearchApi.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);
        		
                Pair<ResponseDTO<WorksheetData>, String> scrollWorksheetDatas;
                
                scrollWorksheetDatas = worksheetDataSearchApi.searchByFilteredQuery(null, null,
                																	new QueryBuilder[] { finalfilter }, 
                																	UserUtils.SYSTEM, scrollId, scrollTimeOut);
                
    			while(scrollWorksheetDatas != null && scrollWorksheetDatas.getLeft() != null &&
            		  CollectionUtils.isNotEmpty(scrollWorksheetDatas.getLeft().getRecords())) {
    				scrollId = StringUtils.isNotBlank(scrollWorksheetDatas.getRight()) ? 
    						   scrollWorksheetDatas.getRight() : null;
    				
    				worksheetDatas.addAll(scrollWorksheetDatas.getLeft().getRecords());
    				
    				scrollWorksheetDatas = null;
					
    				if (StringUtils.isNotBlank(scrollId)) {
    					scrollWorksheetDatas = worksheetDataSearchApi.searchByFilteredQuery(null, null,
                        																	new QueryBuilder[] { finalfilter }, 
                        																	UserUtils.SYSTEM, scrollId, 
                        																	scrollTimeOut);
    				}
    			}
        			
    			if (scrollWorksheetDatas != null) {
    				scrollId = StringUtils.isNotBlank(scrollWorksheetDatas.getRight()) ? 
    						   scrollWorksheetDatas.getRight() : null;
    				
    				if (StringUtils.isNotBlank(scrollId)) {
            			WorksheetSearchAPI.clearWorksheetScroll(scrollId);
            			scrollId = null;
            		}
    			}
    			
    			result.setRecords(worksheetDatas).setSuccess(true);
        	} catch (Throwable t) {
    			Log.log.error(String.format("Error %sin getting worksheet data for %d worksheet ids", 
    										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
    										worksheetIds.size()), t);
        	} finally {
        		if (StringUtils.isNotBlank(scrollId)) {
        			WorksheetSearchAPI.clearWorksheetScroll(scrollId);
        		}
        	}
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
    
    public static Pair<List<TaskResult>, String> findArchiveTaskResultIdsByUpdatedOn(
    													QueryDTO queryDTO, Set<String> ignoreIds, long startTime, 
    													long expiryTime, long cleanupTime, String username, 
    													String[] indexNames, String scrollId,
    													TimeValue scrollTimeOut) throws SearchException {
    	Pair<ResponseDTO<TaskResult>, String> response = Pair.of(new ResponseDTO<TaskResult>(), null);
    	
    	QueryBuilder finalfilter = null;
    	
    	if (StringUtils.isBlank(scrollId)) {        
	        RangeQueryBuilder createdOnFilter = startTime == 0l ? QueryBuilders.rangeQuery("sysCreatedOn").gte(startTime) : 
	        									QueryBuilders.rangeQuery("sysCreatedOn").from(startTime)
	        									.to(startTime + Duration.ofDays(1l).toMillis() - 1);
	        RangeQueryBuilder expiryFilter = QueryBuilders.rangeQuery("sysUpdatedOn").from(startTime).to(expiryTime);
	        
	        if(CollectionUtils.isNotEmpty(ignoreIds)) {
		        QueryBuilder ignorefilter = QueryBuilders.termsQuery("problemId", ignoreIds);
		        finalfilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.wildcardQuery("sirId", "SIR-*"))
		        			  .should(createdOnFilter).should(expiryFilter).mustNot(ignorefilter);
	        } else {
	        	finalfilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.wildcardQuery("sirId", "SIR-*"))
	        				  .should(createdOnFilter).should(expiryFilter);
	        }
    	}
    	
        QueryBuilder[] filterBuilders = new QueryBuilder[] { finalfilter };
        
        if(indexNames != null) {
        	response =  taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, indexNames, filterBuilders, username,
        														  scrollId, scrollTimeOut);
        } else {
        	response = taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, filterBuilders, username, 
        														 scrollId, scrollTimeOut);
        }
                
        return Pair.of(response.getLeft().getRecords(), response.getRight());
    }
    
    public static boolean clearTaskResultScroll(String scrollId) {
    	return taskResultSearchAPI.clearScroll(scrollId);
    }
    
    public static Pair<ResponseDTO<TaskResult>, String> searchTaskResults(QueryDTO queryDTO, QueryBuilder queryBuilder,
    																	  String[] indexNames,  String username, 
    																	  String scrollId,
    				    												  TimeValue scrollTimeOut) throws SearchException {
    	Pair<ResponseDTO<TaskResult>, String> response = Pair.of(new ResponseDTO<TaskResult>(), null);

        try
        {
            queryDTO.setModelName(SearchConstants.DOCUMENT_TYPE_TASKRESULT);
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.ASC);
            
            if(indexNames!= null && indexNames.length > 0) {
            	response =  taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, indexNames, 
            														  new QueryBuilder[] {queryBuilder}, username,
						  											  scrollId, scrollTimeOut);
            } else {
            	response = taskResultSearchAPI.searchByFilteredQuery(queryDTO, null, new QueryBuilder[] {queryBuilder}, 
            														 username, scrollId, scrollTimeOut);
            }
            
            if (response.getLeft().isSuccess() && response.getLeft().getRecords() != null && 
            	CollectionUtils.isNotEmpty(response.getLeft().getRecords()) && !UserUtils.isAdminUser(username)) {
                filterByAccessibleOrgs(response.getLeft(), username);
            }
        } catch (Throwable t) {
            Log.log.error(t.getMessage(), t);
            throw new SearchException(t.getMessage(), t);
        }
        
        return response;
    }
    
    public static ResponseDTO<List<String>> getAllExecutionSummaryIndexNames()
    {
    	return executionSummarySearchAPI.getAllIndexNames();
    }
    
    public static ResponseDTO<String> getAllExecutionSummaryIndexNames(long expiryTime) {
    	return executionSummarySearchAPI.getAllIndexNames(Instant.ofEpochMilli(expiryTime).truncatedTo(ChronoUnit.DAYS));
    }
    
    public static Pair<List<ProcessRequest>, String> findArchiveProcessRequestsByUpdatedOn(
    															QueryDTO queryDTO, 
    															Set<String> ignoreIds, long startTime,
    															long expiryTime, long cleanupTime, 
    															String username, String[] indexNames, 
    															String scrollId, 
    															TimeValue scrollTimeOut) throws SearchException {
    	Pair<ResponseDTO<ProcessRequest>, String> response = Pair.of(new ResponseDTO<ProcessRequest>(), null);
    	
    	QueryBuilder finalfilter = null;
    	
    	if (StringUtils.isBlank(scrollId)) {        
	        RangeQueryBuilder createdOnFilter = startTime == 0l ? QueryBuilders.rangeQuery("sysCreatedOn").gte(startTime) : 
	        									QueryBuilders.rangeQuery("sysCreatedOn").from(startTime)
	        									.to(startTime + Duration.ofDays(1l).toMillis() - 1);
	        RangeQueryBuilder expiryFilter = QueryBuilders.rangeQuery("sysUpdatedOn").from(startTime).to(expiryTime);
	        
	        if(CollectionUtils.isNotEmpty(ignoreIds)) {
		        QueryBuilder ignorefilter = QueryBuilders.termsQuery("problem", ignoreIds);
		        finalfilter = QueryBuilders.boolQuery().mustNot(ignorefilter).should(createdOnFilter).should(expiryFilter);
	        } else {
	        	finalfilter = QueryBuilders.boolQuery().should(createdOnFilter).should(expiryFilter);
	        }
    	}
    	
        QueryBuilder[] filterBuilders = new QueryBuilder[] { finalfilter };
        
        if(indexNames != null) {
        	response =  processRequestSearchAPI.searchByFilteredQuery(queryDTO, null, indexNames, filterBuilders, username,
        														  	  scrollId, scrollTimeOut);
        } else {
        	response = processRequestSearchAPI.searchByFilteredQuery(queryDTO, null, filterBuilders, username, 
        														 	 scrollId, scrollTimeOut);
        }
                
        return Pair.of(response.getLeft().getRecords(), response.getRight());
    }
    
    public static boolean clearProcessRequestScroll(String scrollId) {
    	return processRequestSearchAPI.clearScroll(scrollId);
    }
    
    public static boolean clearWorksheetScroll(String scrollId) {
    	return worksheetSearchApi.clearScroll(scrollId);
    }
    
    public static List<String> filterWorksheetIds(Set<String> worksheetIds, String[] indexNames, 
			   									  String username, boolean sir) throws SearchException {
    	final List<String> filteredWorksheetIds = new CopyOnWriteArrayList<String>();

    	if(CollectionUtils.isNotEmpty(worksheetIds)) {
    		QueryBuilder worksheetIdfilter = QueryBuilders.termsQuery("sysId", worksheetIds);
    		QueryBuilder sirNotExistsBlankOrIsSIR = (sir) ?
    												QueryBuilders.boolQuery()
    												.must(QueryBuilders.wildcardQuery("sirId", "SIR-*")) :
    										   	    QueryBuilders.boolQuery()
    										   	    .must(QueryBuilders.existsQuery("sirId"))
    										   	    .mustNot(QueryBuilders.termQuery("sirId", ""));
    										   
    		QueryBuilder qryBldr = (sir) ?
    							   QueryBuilders.boolQuery()
    							   .must(worksheetIdfilter)
    							   .must(QueryBuilders.constantScoreQuery(sirNotExistsBlankOrIsSIR)) :
    							   QueryBuilders.boolQuery()
    							   .should(worksheetIdfilter)
    							   .mustNot(QueryBuilders.constantScoreQuery(sirNotExistsBlankOrIsSIR));
    		
    		String scrollId = null;
    		QueryBuilder[] qryBldrs = {qryBldr};
    		try {
    			TimeValue scrollTimeOut = new TimeValue(worksheetSearchApi.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);

    			Pair<ResponseDTO<Worksheet>, String> scrollWorksheets;

    			if(indexNames != null) {
    				scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(null, null, indexNames, qryBldrs,
																				username, scrollId, scrollTimeOut);
    			} else {
    				scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(null, null, qryBldrs,
																				username, scrollId, scrollTimeOut);
    			}

    			while(scrollWorksheets != null && scrollWorksheets.getLeft() != null &&
    				  CollectionUtils.isNotEmpty(scrollWorksheets.getLeft().getRecords())) {
    				scrollId = StringUtils.isNotBlank(scrollWorksheets.getRight()) ? scrollWorksheets.getRight() : null;

    				scrollWorksheets.getLeft().getRecords().parallelStream()
    				.forEach(worksheet -> {
    					if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysId())) {
    						/*
    						 * For filtering Non-SIR worksheet ids from specified list of worksheet ids
    						 * ES query uses combination of  
    						 * 
    						 * SHOULD(worksheet id from specified list)
    						 * MUST_NOT (sirId field exists (Pre SIR there was no sirId field) AND
    						 *           sirId field is NOT blank/null)
    						 * 
    						 * Please note SHOULD is NOT a MUST so query may return worksheet id(s)
    						 * which satisfies MUST criteria but is NOT in list of specified worksheet
    						 * ids
    						 */
    						if (sir || worksheetIds.contains(worksheet.getSysId())) {
    							filteredWorksheetIds.add(worksheet.getSysId());
    						}
    					}
    				});

    				scrollWorksheets = null;

    				if (StringUtils.isNotBlank(scrollId)) {
    					if(indexNames != null) {
    						scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(null, null, indexNames, qryBldrs,
																						username, scrollId, scrollTimeOut);
    					} else {
    						scrollWorksheets = worksheetSearchApi.searchByFilteredQuery(null, null, qryBldrs,
																						username, scrollId, scrollTimeOut);
    					}
    				}
    			}

    			if (scrollWorksheets != null) {
    				scrollId = StringUtils.isNotBlank(scrollWorksheets.getRight()) ? scrollWorksheets.getRight() : null;

    				if (StringUtils.isNotBlank(scrollId)) {
    					WorksheetSearchAPI.clearWorksheetScroll(scrollId);
    					scrollId = null;
    				}
    			}
    		} catch (Throwable t) {
    			Log.log.error(String.format("Error %sin getting %sSIR worksheet Ids from %d worksheet ids", 
    										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
    										(sir ? "" : "Non-"), worksheetIds.size()), t);
    		} finally {
    			if (StringUtils.isNotBlank(scrollId)) {
    				WorksheetSearchAPI.clearWorksheetScroll(scrollId);
    			}
    		}
    	}

    	return filteredWorksheetIds;
    }
    
    public static long getScrollCtxKeepAliveDur() {
		return worksheetSearchApi.getScrollCtxKeepAliveDur();
	}
	
	public static Duration getScrollCtxKeepAliveDurInMinutes() {
		return worksheetSearchApi.getScrollCtxKeepAliveDurInMinutes();
	}
}
