/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.worksheet;

import java.util.Collection;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchException;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class ProcessRequestIndexer implements Indexer<IndexData<String>>
{
    private static volatile ProcessRequestIndexer instance = null;
    
    private final IndexAPI indexAPI;
    private final IndexAPI taskResultIndexAPI;

    private final ConfigSearch config;
    private long indexThreads;

    public static long counter = 0;

    private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

    public static ProcessRequestIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("ProcessRequestIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static ProcessRequestIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new ProcessRequestIndexer(config);
        }
        return instance;
    }

    private ProcessRequestIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexAPI = APIFactory.getProcessRequestIndexAPI();
        this.taskResultIndexAPI = APIFactory.getTaskResultIndexAPI();
        this.indexThreads = this.config.getIndexthreads();
    }

    public void init()
    {
        Log.log.debug("Initializing ProcessRequestIndexer.");
        QueueListener<IndexData<String>> wikiIndexListener = new IndexListener<IndexData<String>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiIndexListener);
        Log.log.debug("ProcessRequestIndexer initialized.");
    }

    public boolean enqueue(IndexData<String> indexData)
    {
        boolean result = false;

        if(indexData != null && indexData.getObjects().size() > 0)
        {
            result = dataQueue.offer(indexData);
        }
        return result;
    }

    @Override
    public boolean index(IndexData<String> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> sysIds = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexProcessRequests(indexData, indexData.getUsername());
                    break;
                case DELETE:
                    deleteProcessRequests(sysIds, indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    public void indexProcessRequests(final IndexData<String> indexData, final String username)
    {
/*        try
        {
            final Collection<String> sysIds = indexData.getObjects();
            
            if (sysIds != null && !sysIds.isEmpty())
            {
                List<String> sysIdList = (List<String>) sysIds;
                long startTime = 0L;
                long endTime = 0L;

                // got to throttle this or the indexer will be bombarded
                // let's try 500 at a time
                int count = sysIdList.size();
                int limit = 1000;
                int fromIndex = 0;
                int toIndex = limit;
                while (fromIndex < count)
                {
                    if (toIndex > count)
                    {
                        toIndex = count;
                    }
                    List<String> subList = sysIdList.subList(fromIndex, toIndex);
                    if (subList != null && subList.size() > 0)
                    {
                        startTime = System.currentTimeMillis();
                        Set<ResolveProcessRequestCAS> processRequests = ResolveProcessRequestDAO.getInstance().findByKeys(subList);
                        endTime = System.currentTimeMillis();
                        Log.log.debug("Time (millis) taken to get " + subList.size() + " process requests from Cassandra : " + (endTime - startTime));
                        if (processRequests != null && processRequests.size() > 0)
                        {
                            startTime = System.currentTimeMillis();
                            indexAPI.indexDocuments(processRequests, username);
                            endTime = System.currentTimeMillis();
                            Log.log.debug("Time (millis) taken to index " + subList.size() + " process requests in ES : " + (endTime - startTime));
                        }
                        // sleep for 5 seconds
                        if (count > limit)
                        {
                            //Thread.sleep() is not a good idea. Refer to a concept like Thread.sleep() with a lock held
                            Thread.sleep(5000L);
                        }
                    }
                    fromIndex = toIndex;
                    toIndex += limit;
                }
                counter += sysIds.size();
                Log.log.debug("Total process requests indexed since last start: " + counter);
            }
        }
        catch (Exception exp)
        {
            Log.log.info("indexProcessRequests cought exception: " + exp, exp);
        }
*/    }

    private void deleteProcessRequests(final Collection<String> sysIds, final String username) throws SearchException
    {
        // delete any action results belongs to this process
        QueryBuilder query = QueryBuilders.matchQuery("processId", sysIds);
        taskResultIndexAPI.deleteByQueryBuilder(query, username);

        // finally delete the process request
        indexAPI.deleteDocumentByIds(sysIds, username);
    }
}
