/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.HashMap;
import java.util.Map;

import com.resolve.services.interfaces.VO;

public class ProcessRequest extends BaseIndexModel
{
    private static final long serialVersionUID = 4940463378845138218L;
	private String number;
    private String problem;
    private String problemNumber; // transient
    private String status;
    private String wiki;
    private String timeout;
    private Integer duration;
    private Boolean modified;
    private Integer atExecCount; // Total count of ATs executed
    
    public ProcessRequest()
    {
        super();
    }

    public ProcessRequest(String sysId, String sysOrg, String username)
    {
        super(sysId, sysOrg, username);
    }

    public String getStatus()
    {
        return this.status;
    }

    public void setStatus(String UStatus)
    {
        this.status = UStatus;
    }

    public String getWiki()
    {
        return this.wiki;
    }

    public void setWiki(String UWiki)
    {
        this.wiki = UWiki;
    }

    public String getTimeout()
    {
        return this.timeout;
    }

    public void setTimeout(String UTimeout)
    {
        this.timeout = UTimeout;
    }

    public String getNumber()
    {
        return this.number;
    }

    public void setNumber(String UNumber)
    {
        this.number = UNumber;
    }

    public String getProblem()
    {
        return problem;
    }

    public void setProblem(String uProblem)
    {
        problem = uProblem;
    }

    public String getProblemNumber()
    {
        return problemNumber;
    }

    public void setProblemNumber(String problemNumber)
    {
        this.problemNumber = problemNumber;
    }

    public Integer getDuration()
    {
        return this.duration;
    }

    public void setDuration(Integer UDuration)
    {
        this.duration = UDuration;
    }
    
    public Boolean getModified()
    {
        return modified;
    }
    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }
    
    public Integer getAtExecCount() {
    	return atExecCount;
    }
    
    public Integer getuATExecCount() {
    	return atExecCount != null ? atExecCount : VO.NON_NEGATIVE_INTEGER_DEFAULT;
    }
    
    public void setAtExecCount(Integer atExecCount) {
    	this.atExecCount = atExecCount;
    }
    
    @Override
    public Map<String, Object> asMap()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        result.putAll(super.asMap());
        result.put("u_number", getNumber());
        result.put("u_problem", getProblem());
        result.put("u_status", getStatus());
        result.put("u_timeout", getTimeout());
        result.put("u_wiki", getWiki());
        result.put("u_duration", getDuration());
        result.put("u_atexeccount", getuATExecCount());
        return result;
    }

    @Override
    public String toString()
    {
        return String.format("ProcessRequest [number=%s, problem=%s, problemNumber=%s, status=%s, wiki=%s, timeout=%s, " +
        					 "duration=%d, Modified=%b, ATExecCount=%d]", number, problem, problemNumber, status, wiki, 
        					 timeout, duration.intValue(), modified.booleanValue(), getuATExecCount().intValue());
    }
}
