/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

import com.resolve.search.model.BaseIndexModel;


@SuppressWarnings("serial")
public class UserQuery extends BaseIndexModel
{
    private static final long serialVersionUID = -5821829281327269544L;
	private String query;
    private String type;
    private String userSysId;
    private String username;
    private int weight;
    private int totalCount;
    private int selectCount;
    
    public UserQuery()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getUserSysId()
    {
        return userSysId;
    }

    public void setUserSysId(String userSysId)
    {
        this.userSysId = userSysId;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public int getWeight()
    {
        return weight;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public int getSelectCount()
    {
        return selectCount;
    }

    public void setSelectCount(int selectCount)
    {
        this.selectCount = selectCount;
    }

    @Override
    public String toString()
    {
        return "UserQuery [query=" + query + ", selectCount=" + selectCount + ", type=" + type + ", userSysId=" + userSysId + ", username=" + username + ", weight=" + weight + ", totalCount=" + totalCount + ", toString()=" + super.toString() + "]";
    }
}
