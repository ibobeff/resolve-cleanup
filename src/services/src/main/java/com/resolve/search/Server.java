/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequestBuilder;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesResponse;
import org.elasticsearch.action.admin.indices.close.CloseIndexRequest;
import org.elasticsearch.action.admin.indices.close.CloseIndexResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.admin.indices.open.OpenIndexRequest;
import org.elasticsearch.action.admin.indices.open.OpenIndexResponse;
import org.elasticsearch.action.admin.indices.settings.get.GetSettingsResponse;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsRequestBuilder;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.Requests;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.Priority;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.settings.Settings.Builder;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.joda.time.DateTime;

import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import com.floragunn.searchguard.ssl.SearchGuardSSLPlugin;
import com.floragunn.searchguard.ssl.util.SSLConfigConstants;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.search.actiontask.ActionTaskIndexer;
import com.resolve.search.customform.CustomFormIndexAPI;
import com.resolve.search.customform.CustomFormIndexer;
import com.resolve.search.metric.MetricTimerIndexer;
import com.resolve.search.property.PropertyIndexAPI;
import com.resolve.search.property.PropertyIndexer;
import com.resolve.search.social.SocialIndexer;
import com.resolve.search.tag.TagIndexer;
import com.resolve.search.wiki.WikiAttachmentIndexer;
import com.resolve.search.wiki.WikiIndexer;
import com.resolve.search.worksheet.ExecuteStateIndexer;
import com.resolve.search.worksheet.ProcessRequestIndexer;
import com.resolve.search.worksheet.TaskResultIndexer;
import com.resolve.search.worksheet.WorksheetIndexer;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class represents the Resolve search service.
 */
public final class Server {
	public static final String ES_COMMON_SETTINGS_FILE = "common_settings.json";

	public static final String ES_WIKIDOC_MAPPING_FILE = "wiki_documents_mappings.json";

	public static final String ES_ACTION_TASKS_MAPPING_FILE = "action_tasks_mappings.json";

	public static final String ES_PROPERTIES_MAPPING_FILE = "properties_mappings.json";

	public static final String ES_CUSTOM_FORM_MAPPING_FILE = "custom_forms_mappings.json";
	
	public static final String ES_ATOMATIONS_MAPPING_FILE = "automations_mappings.json";
	public static final String ES_DOCUMENTS_MAPPING_FILE = "documents_mappings.json";
	public static final String ES_DECISION_TREES_MAPPING_FILE = "decision_trees_mappings.json";
	public static final String ES_PLAYBOOKS_MAPPING_FILE = "playbooks_mappings.json";

	private static final String ADMIN_USER = "admin";

	private boolean isRunning = false;
	// Singleton
	private static volatile Server instance = null;

	private Client client;

	private final ConfigSearch config;

	// private Map<String, List<DateTime>> indexDateTimeMap = new HashMap<String,
	// List<DateTime>>();

	public static Server getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new Server(config);
        }
        return (Server) instance;
    }

	/**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     * @throws IllegalStateException
     */
    public static Server getInstance()
    {
        if (instance == null)
        {
            Log.log.fatal("Search service is not initialized correctly..");
            throw new RuntimeException("Search service is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

	/**
     * Private constructor.
     *
     * @param config
     */
    private Server(ConfigSearch config)
    {
        this.config = config;

        client = createClient();
    } // ServiceService

	void init() throws SearchException
    {
        //start the thread to generate dynamic daily indices
        IndexCreator indexCreator = new IndexCreator(this);
        indexCreator.start();
        
        // Now initialize various indexers that listens to their respective queues
        WikiIndexer.getInstance(config).init();
        WikiAttachmentIndexer.getInstance(config).init();

        WorksheetIndexer.getInstance(config).init();
        ProcessRequestIndexer.getInstance(config).init();
        TaskResultIndexer.getInstance(config).init();
        ExecuteStateIndexer.getInstance(config).init();
        MetricTimerIndexer.getInstance(config).init();

        ActionTaskIndexer.getInstance(config).init();
        
        PropertyIndexer.getInstance(config).init();
        
        CustomFormIndexer.getInstance(config).init();
        
        TagIndexer.getInstance(config).init();

        SocialIndexer.getInstance(config).init();

        if(config.isSync())
        {
            SyncService.getInstance(config).init();
        }
        else
        {
            Log.log.info("Sit2Site replication is disabled.");
        }
        
        // finally set the running flag to true announcing that we're indeed up.
        isRunning = true;
    }

	/**
     * Components (RSCONTROL, RSVIEW etc.) calls this during startup.
     * 
     * @throws SearchException 
     */
    void configureIndex(String indices) throws SearchException
    {
        Log.log.info("Try to config indices. Wait for cluster health to become yellow");
        if(ensureYellow())
        {
            Log.log.info("ES cluster health is yellow now. Prepeare to create indices");
            
            if(StringUtils.isNotBlank(indices) && !"all".equalsIgnoreCase(indices))
            {
                //TODO we will deal with individual later.
            }
            else
            {
                configureAllIndices();
            }
        }
        else
        {
            Log.log.fatal("RSSearch cluster is not ready, cannot initialize search service.");
            throw new RuntimeException("RSSearch cluster is not ready, cannot initialize search service.");
        }
    }

	private void configureAllIndices() throws SearchException
    {
        Log.log.info("Config all indices starts");
        Map<String, IndicesAliasesRequestBuilder> indexAliases = new HashMap<String, IndicesAliasesRequestBuilder>();
        List<IndexConfiguration> defaultIndexConfigurations = new ArrayList<IndexConfiguration>();
        // some common clusterwide settings like shards/replica etc.
        String commonSettingsJson = readJson(ES_COMMON_SETTINGS_FILE);

        Map<String, String> mappings = new HashMap<String, String>();

        //create all the worksheet related indices
        configureDynamicIndices();
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA, readJson("worksheets_data_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_WORKSHEET_DATA, commonSettingsJson, mappings));

        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_EXECUTE_STATE, readJson("execute_states_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_EXECUTE_STATE, commonSettingsJson, mappings));

        //metrics
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_TIMER, readJson("metric_timer_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_TIMER, commonSettingsJson, mappings));

        // regular social posts
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, readJson("social_posts_mappings.json"));
        mappings.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, readJson("social_comments_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_SOCIAL_POST, commonSettingsJson, mappings));

        // system messages social posts
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_SYSTEM_POST, readJson("system_posts_mappings.json"));
        mappings.put(SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT, readJson("system_comments_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_SYSTEM_POST, commonSettingsJson, mappings));

        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_TAG, readJson("tags_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_TAG, commonSettingsJson, mappings));

        // wiki document
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT, readJson("wiki_documents_mappings.json"));
        mappings.put(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, readJson("wiki_attachments_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_WIKI_DOCUMENT, commonSettingsJson, mappings));

        // action task
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_ACTION_TASK, readJson(ES_ACTION_TASKS_MAPPING_FILE));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_ACTION_TASK, commonSettingsJson, mappings));

        // dictionary
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_DICTIONARY, readJson("dictionary_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_DICTIONARY, commonSettingsJson, mappings));

        //global query
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_GLOBAL_QUERY, readJson("global_query_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_GLOBAL_QUERY, commonSettingsJson, mappings));

        //user query
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_USER_QUERY, readJson("user_query_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_USER_QUERY, commonSettingsJson, mappings));

        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_JVM, readJson("metric_jvm_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_JVM, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_RUNBOOK, readJson("metric_runbook_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_RUNBOOK, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_TRANSACTION, readJson("metric_transaction_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_TRANSACTION, commonSettingsJson, mappings));

        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_CPU, readJson("metric_cpu_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_CPU, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_JMS_QUEUE, readJson("metric_jmsqueue_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_JMS_QUEUE, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_USERS, readJson("metric_users_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_USERS, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_LATENCY, readJson("metric_latency_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_LATENCY, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_DB, readJson("metric_database_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_METRIC_DB, commonSettingsJson, mappings));
        
        // all suggester index
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_DOCUMENT_QUERY, SearchUtils.suggestionMapping(SearchConstants.DOCUMENT_TYPE_DOCUMENT_QUERY, SearchConstants.SUGGEST_FIELD_NAME_DOCUMENT));
        mappings.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_QUERY, SearchUtils.suggestionMapping(SearchConstants.DOCUMENT_TYPE_SOCIAL_QUERY, SearchConstants.SUGGEST_FIELD_NAME_SOCIAL));
        mappings.put(SearchConstants.DOCUMENT_TYPE_AUTOMATION_QUERY, SearchUtils.suggestionMapping(SearchConstants.DOCUMENT_TYPE_AUTOMATION_QUERY, SearchConstants.SUGGEST_FIELD_NAME_AUTOMATION));
        mappings.put(SearchConstants.DOCUMENT_TYPE_WIKI_QUERY, SearchUtils.suggestionMapping(SearchConstants.DOCUMENT_TYPE_WIKI_QUERY, SearchConstants.SUGGEST_FIELD_NAME_WIKI));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_SEARCH_QUERY, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_PB_ACTIVITY, readJson("pb_activity_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_PB_ACTIVITY, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_PB_NOTES, readJson("pb_notes_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_PB_NOTES, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT, readJson("pb_attachment_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_PB_ATTACHMENT, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS, readJson("pb_artifacts_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_PB_ARTIFACTS, commonSettingsJson, mappings));
        
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG, readJson("pb_auditlog_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_PB_AUDIT_LOG, commonSettingsJson, mappings));
        
//        mappings = new HashMap<String, String>();
//        mappings.put(SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY_ARCHIVE, readJson("execution_summary_archive_mappings.json"));
//        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ARCHIVE, commonSettingsJson, mappings));

        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_SECURITY_INCIDENT, readJson("security_incident_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_SECURITY_INCIDENT, commonSettingsJson, mappings));
        
        //properties
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_PROPERTY, readJson("properties_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_PROPERTY, commonSettingsJson, mappings));
        
        // custom forms
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM, readJson("custom_forms_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.INDEX_NAME_CUSTOM_FORM, commonSettingsJson, mappings));

        // wikidocument type document
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.SEARCH_TYPE_DOCUMENT, readJson("documents_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.SEARCH_TYPE_DOCUMENT, commonSettingsJson, mappings));
        
        // wikidocument type decisiontree
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.SEARCH_TYPE_DECISION_TREE, readJson("decision_trees_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.SEARCH_TYPE_DECISION_TREE, commonSettingsJson, mappings));
        
        // wikidocument type automation
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.SEARCH_TYPE_AUTOMATION, readJson("automations_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.SEARCH_TYPE_AUTOMATION, commonSettingsJson, mappings));
        
        // wikidocument type playbook
        mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.SEARCH_TYPE_PLAYBOOK, readJson("playbooks_mappings.json"));
        defaultIndexConfigurations.add(new IndexConfiguration(SearchConstants.SEARCH_TYPE_PLAYBOOK, commonSettingsJson, mappings));
        
//        IndicesAliasesRequestBuilder executionSummaryAlias = client.admin().indices().prepareAliases();
//        executionSummaryAlias.addAlias("executionsummaryarchive", SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS);
//        indexAliases.put(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS, executionSummaryAlias);
        
        // call the common method to create indices.
        createIndex(defaultIndexConfigurations, indexAliases);
        
        ActionTaskIndexAPI.updateIndexMapping(ADMIN_USER);
        CustomFormIndexAPI.updateIndexMapping(ADMIN_USER);
        PropertyIndexAPI.updateIndexMapping(ADMIN_USER);
    }
	
	private Integer getIndexPriority(String indexName)
    {    
        if (!SearchAdminAPI.isIndexExists(indexName))
        {
            Log.log.info("Return null. Index doesn't exist: " + indexName);
            return null;
        }

        Integer priority = null;
        GetSettingsResponse response = client.admin().indices().prepareGetSettings(indexName).get();                           
        for (ObjectObjectCursor<String, Settings> cursor : response.getIndexToSettings()) { 
            Settings settings = cursor.value;                                               
            priority = settings.getAsInt("index.priority", null);             
            break;
        }
        return priority;
    }

	private void resetIndexPriorityByName(String indexName)
    {
        Log.log.info("Prepare to reset index priority for " + indexName);
        if (!SearchAdminAPI.isIndexExists(indexName))
        {
            Log.log.info("Index doesn't exist. Can't reset index priority for index: " + indexName);
            return;
        }
        
        Integer pri = getIndexPriority(indexName);
        if (pri==null || pri==0)
        {
            return;
        }
        try
        {
            if (SearchAdminAPI.isIndexExists(indexName))
            {
                IndicesAdminClient client = SearchAdminAPI.getClient().admin().indices();        
                ClusterHealthResponse response = SearchAdminAPI.getClient().admin().cluster().prepareHealth(indexName).setWaitForGreenStatus().get();
                ClusterHealthStatus status = response.getIndices().get(indexName).getStatus();
                
                int count = 0;
                while (!status.equals(ClusterHealthStatus.GREEN)) 
                {
                    Log.log.info("Index " + indexName + " status is " + status);
                    Thread.sleep(5000);
                    ++count;
                    if (count > 12 * 5)
                    {
                        Log.log.info("Stop reset priority for index " + indexName);
                        return;
                    }
                }
                
                Log.log.info("Index " + indexName + " status is " + status);
                
                CloseIndexResponse closeResp = SearchAdminAPI.getClient().admin().indices().close(new CloseIndexRequest(indexName)).get();
                Log.log.info("Index is closed: " + indexName + ", acknowledged: " + closeResp.isAcknowledged());
                
                UpdateSettingsResponse updateResp = client.prepareUpdateSettings(indexName)   
                .setSettings(Settings.builder().put("index.priority", 0))
                .get();
                Log.log.info("Index is updated: " + indexName + ", acknowledged: " + updateResp.isAcknowledged());
                
                OpenIndexResponse openResp = SearchAdminAPI.getClient().admin().indices().open(new OpenIndexRequest(indexName)).get();
                Log.log.info("Index is opened: " + indexName + ", acknowledged: " + openResp.isAcknowledged());
                
                Log.log.info("Reset index priority for index " + indexName);
            }
            else
            {
                Log.log.info("Index doesn't exist. Fail to reset index priority for index " + indexName);
            }
        }
        catch (Throwable ex)
        {
            Log.log.error(ex,ex);
        }
    }

	private void configureDynamicIndices() throws SearchException {
		try {
			openAllIndices();
			configureDynamicIndicesInternal();
		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
		} finally {
			openAllIndices();
		}
	}
	
	private boolean createIndexAndUpdateAliasesIfRequired(String newIndexName,
														  String indexAlias,
														  ResponseDTO<String> indxNamesResp,
														  List<IndexConfiguration> defaultIndexConfigurations,
														  IndicesAliasesRequestBuilder aliasReqBldr,
														  String newCommonSettingsJson,
														  Map<String, String> mappings) {
		boolean updatedAliases = false;
		
		if (indxNamesResp.isSuccess()) {
			if (CollectionUtils.isNotEmpty(indxNamesResp.getRecords())) {
				if (!indxNamesResp.getRecords().contains(newIndexName)) {
					updatedAliases = true;
					defaultIndexConfigurations
					.add(new IndexConfiguration(newIndexName, newCommonSettingsJson, mappings));
					aliasReqBldr.addAlias(newIndexName, indexAlias);
				}	
			} else {
				// Fresh install no existing daily indices
				updatedAliases = true;
				defaultIndexConfigurations
				.add(new IndexConfiguration(newIndexName, newCommonSettingsJson, mappings));
				aliasReqBldr.addAlias(newIndexName, indexAlias);
			}
		} else {
			// Fresh install no existing daily indices
			updatedAliases = true;
			defaultIndexConfigurations
			.add(new IndexConfiguration(newIndexName, newCommonSettingsJson, mappings));
			aliasReqBldr.addAlias(newIndexName, indexAlias);
		}
		
		return updatedAliases;
	}
	
	private void prepareWorksheetIndices(Map<String, IndicesAliasesRequestBuilder> indexAliases,
										 long msTwoDaysAhead,
										 List<IndexConfiguration> defaultIndexConfigurations,
										 String newCommonSettingsJson,
										 DateTime today, DateTime tomorrow) throws SearchException {
		boolean updatedWorksheetAliases = false;
		final ResponseDTO<String> wsIndxNamesResp = WorksheetSearchAPI.getAllWorksheetIndexNames(msTwoDaysAhead);
		
		if (wsIndxNamesResp != null && wsIndxNamesResp.isSuccess() && 
				CollectionUtils.isNotEmpty(wsIndxNamesResp.getRecords())) {
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("%d Daily Worksheet Indices: [%s]", wsIndxNamesResp.getRecords().size(),
											StringUtils.listToString(wsIndxNamesResp.getRecords(), ", ")));
			}
			
			Log.log.info(String.format("%d Daily worksheet indices, oldest & latest worksheet daily indices are %s, %s " +
									   "respectively", wsIndxNamesResp.getRecords().size(),
									   wsIndxNamesResp.getRecords().get(0),
									   wsIndxNamesResp.getRecords().get(wsIndxNamesResp.getRecords().size() - 1)));
			resetIndexPriorityByName(wsIndxNamesResp.getRecords().get(wsIndxNamesResp.getRecords().size() - 1));
			
			if (wsIndxNamesResp.getRecords().size() >= 2) {
				resetIndexPriorityByName(wsIndxNamesResp.getRecords().get(wsIndxNamesResp.getRecords().size() - 2));
			}
		}
		
		// Worksheet alias
		Map<String, String> mappings = new HashMap<String, String>();
		mappings.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET, readJson("worksheets_mappings.json"));
		IndicesAliasesRequestBuilder worksheetAlias = client.admin().indices().prepareAliases();
		
		final List<Boolean> boolList = new ArrayList<Boolean>(1);
		boolList.add(Boolean.FALSE);
		
		if (wsIndxNamesResp != null && wsIndxNamesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(wsIndxNamesResp.getRecords())) {
			wsIndxNamesResp.getRecords().stream()
			.forEach(wsIndxName -> {
				if (StringUtils.isNotBlank(wsIndxName) && wsIndxName.contains("_") && 
					!wsIndxName.contains(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS)) {
					worksheetAlias.addAlias(wsIndxName, SearchConstants.INDEX_NAME_WORKSHEET_ALIAS);
					boolList.set(0, Boolean.TRUE);
				}
			});
		}
		
		updatedWorksheetAliases = boolList.get(0).booleanValue();
		
		String wsIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_WORKSHEET, today);
		
		if (wsIndxNamesResp != null) {
			updatedWorksheetAliases = updatedWorksheetAliases | 
									  createIndexAndUpdateAliasesIfRequired(wsIndxName, 
											  								SearchConstants.INDEX_NAME_WORKSHEET_ALIAS,
											  								wsIndxNamesResp,
											  								defaultIndexConfigurations,
											  								worksheetAlias,
											  								newCommonSettingsJson,
											  								mappings);
		} else {
			throw new SearchException("Error getting list of existing daily worksheet indices");
		}
		
		wsIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_WORKSHEET, tomorrow);
		
		updatedWorksheetAliases = updatedWorksheetAliases | 
				  				  createIndexAndUpdateAliasesIfRequired(wsIndxName, 
						  												SearchConstants.INDEX_NAME_WORKSHEET_ALIAS,
						  												wsIndxNamesResp,
						  												defaultIndexConfigurations,
						  												worksheetAlias,
						  												newCommonSettingsJson,
						  												mappings);
		
		if (updatedWorksheetAliases) {
			indexAliases.put(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS, worksheetAlias);
		}
	}
	
	private void prepareExecutionSummaryIndices(Map<String, IndicesAliasesRequestBuilder> indexAliases,
			 									long msTwoDaysAhead,
			 									List<IndexConfiguration> defaultIndexConfigurations,
			 									String newCommonSettingsJson,
			 									DateTime today, DateTime tomorrow) throws SearchException {
		boolean updatedExecSummaryAliases = false;
		final ResponseDTO<String> execSummaryIndxNamesResp = 
										WorksheetSearchAPI.getAllExecutionSummaryIndexNames(msTwoDaysAhead);
		
		if (execSummaryIndxNamesResp != null && execSummaryIndxNamesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(execSummaryIndxNamesResp.getRecords())) {
			Log.log.info(String.format("%d Daily execution summary indices, oldest & latest execution summary daily indices " +
									   "are %s, %s respectively", execSummaryIndxNamesResp.getRecords().size(),
									   execSummaryIndxNamesResp.getRecords().get(0),
									   execSummaryIndxNamesResp.getRecords()
									   .get(execSummaryIndxNamesResp.getRecords().size() - 1)));
			resetIndexPriorityByName(execSummaryIndxNamesResp.getRecords()
									 .get(execSummaryIndxNamesResp.getRecords().size() - 1));
			
			if (execSummaryIndxNamesResp.getRecords().size() >= 2) {
				resetIndexPriorityByName(execSummaryIndxNamesResp.getRecords()
										 .get(execSummaryIndxNamesResp.getRecords().size() - 2));
			}
		}
		
		// Execution Summary alias
		Map<String, String >mappings = new HashMap<String, String>();
		mappings.put(SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY, readJson("execution_summary_mappings.json"));
		IndicesAliasesRequestBuilder executionSummaryAlias = client.admin().indices().prepareAliases();
		
		final List<Boolean> boolList = new ArrayList<Boolean>(1);
		boolList.add(Boolean.FALSE);
		
		if (execSummaryIndxNamesResp != null && execSummaryIndxNamesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(execSummaryIndxNamesResp.getRecords())) {
			updatedExecSummaryAliases = true;
			execSummaryIndxNamesResp.getRecords().stream()
			.forEach(execSummaryIndxName -> {
				if (StringUtils.isNotBlank(execSummaryIndxName) && execSummaryIndxName.contains("_") && 
					!execSummaryIndxName.contains(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS)) {
					executionSummaryAlias.addAlias(execSummaryIndxName, SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS);
					boolList.set(0, Boolean.TRUE);
				}
			});
		}
		
		updatedExecSummaryAliases = boolList.get(0).booleanValue();
		
		String execSummaryIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY, today);
		
		if (execSummaryIndxNamesResp != null) {
			updatedExecSummaryAliases = updatedExecSummaryAliases | 
					  					createIndexAndUpdateAliasesIfRequired(
					  							execSummaryIndxName, 
							  					SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS,
							  					execSummaryIndxNamesResp,
							  					defaultIndexConfigurations,
							  					executionSummaryAlias,
							  					newCommonSettingsJson,
							  					mappings);
		} else {
			throw new SearchException("Error getting list of existing daily execution summary indices");
		}
		
		execSummaryIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY, tomorrow);
		
		updatedExecSummaryAliases = updatedExecSummaryAliases | 
									createIndexAndUpdateAliasesIfRequired(execSummaryIndxName, 
																		  SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS,
																		  execSummaryIndxNamesResp,
																		  defaultIndexConfigurations,
																		  executionSummaryAlias,
																		  newCommonSettingsJson,
																		  mappings);
		
		if (updatedExecSummaryAliases) {
			indexAliases.put(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS, executionSummaryAlias);
		}
	}
	
	private void prepareProcessRequestIndices(Map<String, IndicesAliasesRequestBuilder> indexAliases,
											  long msTwoDaysAhead,
											  List<IndexConfiguration> defaultIndexConfigurations,
											  String newCommonSettingsJson,
											  DateTime today, DateTime tomorrow) throws SearchException {
		boolean updatedProcReqAliases = false;
		final ResponseDTO<String> procReqIndxNamesResp = 
										WorksheetSearchAPI.getAllProcessRequestIndexNames(msTwoDaysAhead);
		
		if (procReqIndxNamesResp != null && procReqIndxNamesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(procReqIndxNamesResp.getRecords())) {
			Log.log.info(String.format("%d Daily process request indices, oldest & latest process request daily indices " +
									   "are %s, %s respectively", procReqIndxNamesResp.getRecords().size(),
									   procReqIndxNamesResp.getRecords().get(0),
									   procReqIndxNamesResp.getRecords()
									   .get(procReqIndxNamesResp.getRecords().size() - 1)));
			resetIndexPriorityByName(procReqIndxNamesResp.getRecords()
									 .get(procReqIndxNamesResp.getRecords().size() - 1));
				
			if (procReqIndxNamesResp.getRecords().size() >= 2) {
				resetIndexPriorityByName(procReqIndxNamesResp.getRecords()
										 .get(procReqIndxNamesResp.getRecords().size() - 2));
			}
		}
		
		// Process Request indices and alias
		Map<String, String> mappings = new HashMap<String, String>();
		mappings.put(SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST, readJson("process_requests_mappings.json"));
		IndicesAliasesRequestBuilder processRequestAlias = client.admin().indices().prepareAliases();
		
		final List<Boolean> boolList = new ArrayList<Boolean>(1);
		boolList.add(Boolean.FALSE);
		
		if (procReqIndxNamesResp != null && procReqIndxNamesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(procReqIndxNamesResp.getRecords())) {
			procReqIndxNamesResp.getRecords().stream()
			.forEach(procReqIndxName -> {
				if (StringUtils.isNotBlank(procReqIndxName) && procReqIndxName.contains("_") && 
					!procReqIndxName.contains(SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS)) {
					processRequestAlias.addAlias(procReqIndxName, SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS);
					boolList.set(0, Boolean.TRUE);
				}
			});
		}
		
		updatedProcReqAliases = boolList.get(0).booleanValue();
		
		String procReqIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_PROCESSREQUEST, today);
		
		if (procReqIndxNamesResp != null) {
			updatedProcReqAliases = updatedProcReqAliases |
									createIndexAndUpdateAliasesIfRequired(procReqIndxName, 
																		  SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS,
																		  procReqIndxNamesResp,
																		  defaultIndexConfigurations,
																		  processRequestAlias,
																		  newCommonSettingsJson,
																		  mappings);
		} else {
			throw new SearchException("Error getting list of existing daily process request indices");
		}
		
		procReqIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_PROCESSREQUEST, tomorrow);
		
		updatedProcReqAliases = updatedProcReqAliases |
								createIndexAndUpdateAliasesIfRequired(procReqIndxName, 
													  				  SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS,
													  				  procReqIndxNamesResp,
													  				  defaultIndexConfigurations,
													  				  processRequestAlias,
													  				  newCommonSettingsJson,
													  				  mappings);
		
		if (updatedProcReqAliases) {
			indexAliases.put(SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS, processRequestAlias);
		}
	}
	
	private void prepareTaskResultIndices(Map<String, IndicesAliasesRequestBuilder> indexAliases,
			  								 	long msTwoDaysAhead,
			  								 	List<IndexConfiguration> defaultIndexConfigurations,
			  								 	String newCommonSettingsJson,
			  								 	DateTime today, DateTime tomorrow) throws SearchException {
		boolean updatedTaskResultAliases = false;
		final ResponseDTO<String> taskResultIndxNamesResp = 
										WorksheetSearchAPI.getAllTaskResultIndexNames(msTwoDaysAhead);
		
		if (taskResultIndxNamesResp != null && taskResultIndxNamesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(taskResultIndxNamesResp.getRecords())) {
			Log.log.info(String.format("%d Daily task result indices, oldest & latest task result daily indices " +
									   "are %s, %s respectively", taskResultIndxNamesResp.getRecords().size(),
									   taskResultIndxNamesResp.getRecords().get(0),
									   taskResultIndxNamesResp.getRecords()
									   .get(taskResultIndxNamesResp.getRecords().size() - 1)));
			resetIndexPriorityByName(taskResultIndxNamesResp.getRecords()
									 .get(taskResultIndxNamesResp.getRecords().size() - 1));
					
			if (taskResultIndxNamesResp.getRecords().size() >= 2) {
				resetIndexPriorityByName(taskResultIndxNamesResp.getRecords()
										 .get(taskResultIndxNamesResp.getRecords().size() - 2));
			}
		}
		
		// Task Result indices and alias
		Map<String, String> mappings = new HashMap<String, String>();
		mappings.put(SearchConstants.DOCUMENT_TYPE_TASKRESULT, readJson("task_results_mappings.json"));
		IndicesAliasesRequestBuilder taskResultAlias = client.admin().indices().prepareAliases();
		
		final List<Boolean> boolList = new ArrayList<Boolean>(1);
		boolList.add(Boolean.FALSE);
		
		if (taskResultIndxNamesResp != null && taskResultIndxNamesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(taskResultIndxNamesResp.getRecords())) {
			taskResultIndxNamesResp.getRecords().stream()
			.forEach(taskResultIndxName -> {
				if (StringUtils.isNotBlank(taskResultIndxName) && taskResultIndxName.contains("_") && 
					!taskResultIndxName.contains(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS)) {
					taskResultAlias.addAlias(taskResultIndxName, SearchConstants.INDEX_NAME_TASKRESULT_ALIAS);
					boolList.set(0, Boolean.TRUE);
				}
			});
		}
		
		updatedTaskResultAliases = boolList.get(0).booleanValue();
		
		String taskResultIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_TASKRESULT, today);
		
		if (taskResultIndxNamesResp != null) {
			updatedTaskResultAliases = updatedTaskResultAliases |
									   createIndexAndUpdateAliasesIfRequired(taskResultIndxName, 
											   								 SearchConstants.INDEX_NAME_TASKRESULT_ALIAS,
											   								 taskResultIndxNamesResp,
											   								 defaultIndexConfigurations,
											   								 taskResultAlias,
											   								 newCommonSettingsJson,
											   								 mappings);
		} else {
			throw new SearchException("Error getting list of existing daily task result indices");
		}
		
		taskResultIndxName = SearchUtils.getDynamicIndexName(SearchConstants.INDEX_NAME_TASKRESULT, tomorrow);
		
		updatedTaskResultAliases = updatedTaskResultAliases |
				   				   createIndexAndUpdateAliasesIfRequired(taskResultIndxName, 
						   								 				 SearchConstants.INDEX_NAME_TASKRESULT_ALIAS,
						   								 				 taskResultIndxNamesResp,
						   								 				 defaultIndexConfigurations,
						   								 				 taskResultAlias,
						   								 				 newCommonSettingsJson,
						   								 				 mappings);
		
		if (updatedTaskResultAliases) {
			indexAliases.put(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS, taskResultAlias);
		}
	}
	
	private void configureDynamicIndicesInternal() throws SearchException {
		Log.log.info("ES Status is atleast YELLOW");
		DateTime tomorrow = DateUtils.GetUTCDate().plusDays(1);
		DateTime today = DateUtils.GetUTCDate();
		Log.log.info(String.format("ConfigDynamicIndices starts. today=%s, tomorrow=%s", today, tomorrow));
		// Worksheet indices and alias
		Map<String, IndicesAliasesRequestBuilder> indexAliases = new HashMap<String, IndicesAliasesRequestBuilder>();
		long msTwoDaysAhead = (Instant.now().truncatedTo(ChronoUnit.DAYS).getEpochSecond() * 1000) + 
							  Duration.ofDays(2l).toMillis();
		List<IndexConfiguration> defaultIndexConfigurations = new ArrayList<IndexConfiguration>();
		// some common clusterwide settings like shards/replica etc.
		String commonSettingsJson = readJson("common_settings.json");
		String token = "priority\":  \"";
		int startIdx = commonSettingsJson.indexOf(token, 0);
		String newCommonSettingsJson = commonSettingsJson.substring(0, startIdx + token.length()) + "10\" }";
		
		Log.log.info("Prepare worksheet index");
		prepareWorksheetIndices(indexAliases, msTwoDaysAhead, defaultIndexConfigurations, newCommonSettingsJson, today,
								tomorrow);

		Log.log.info("Prepare execution summary index");
		prepareExecutionSummaryIndices(indexAliases, msTwoDaysAhead, defaultIndexConfigurations, newCommonSettingsJson, today,
									   tomorrow);

		Log.log.info("Prepare processrequest index");
		prepareProcessRequestIndices(indexAliases, msTwoDaysAhead, defaultIndexConfigurations, newCommonSettingsJson, today,
				   					 tomorrow);
		
		Log.log.info("Prepare taskResult index");
		prepareTaskResultIndices(indexAliases, msTwoDaysAhead, defaultIndexConfigurations, newCommonSettingsJson, today,
					 					tomorrow);

		Log.log.info("Try to create all required new indices");
		// call the common method to create indices.
		createIndex(defaultIndexConfigurations, indexAliases);
		
		Log.log.info("ConfigDynamicIndices ends.");
	}

	void createIndex(List<IndexConfiguration> indexConfigurations,
			Map<String, IndicesAliasesRequestBuilder> indexAliases) throws SearchException {
		if (indexConfigurations != null) {
			for (IndexConfiguration indexConfiguration : indexConfigurations) {
				createIndex(indexConfiguration.getIndexName(), indexConfiguration.getSettings(),
							indexConfiguration.getMappings());
			}
		}

		if (MapUtils.isNotEmpty(indexAliases)) {
			for (String aliasName : indexAliases.keySet()) {
				if (Log.log.isDebugEnabled()) {
					Log.log.debug(String.format("Building %s aliases...", aliasName));
				}
				IndicesAliasesRequestBuilder indexAlias = indexAliases.get(aliasName);
				// Timeout to wait for all nodes to acknowledge alias modifications
				indexAlias.setTimeout(TimeValue.timeValueMinutes(10));
				// Timeout to wait for master nodes to acknowledge alias modifications
				indexAlias.setMasterNodeTimeout(TimeValue.timeValueMinutes(4));
				IndicesAliasesResponse indicesAliasesResponse = indexAlias.execute().actionGet();
				if (indicesAliasesResponse.isAcknowledged()) {
					if (Log.log.isDebugEnabled()) {
						Log.log.debug(String.format("Successfully added daily indices to daily index alias: %s", aliasName));
					}
				}
			}
		}
	}

	public void openAllIndices() {
		Log.log.info("Open all indices for use");
		ImmutableOpenMap<String, IndexMetaData> indices = client.admin().cluster().prepareState().get().getState()
				.getMetaData().getIndices();
		Iterator<ObjectObjectCursor<String, IndexMetaData>> it = indices.iterator();

		int count = 0;
		while (it.hasNext()) {
			ObjectObjectCursor<String, IndexMetaData> ooc = it.next();
			String index = ooc.value.getIndex().getName();
			IndexMetaData m = ooc.value;
			if (m.getState() == null || !m.getState().equals(IndexMetaData.State.OPEN)) {
				try {
					SearchAdminAPI.getClient().admin().indices().open(new OpenIndexRequest(index)).get();
					count++;
					Log.log.info("Success: opened index " + index);
				} catch (Throwable ex) {
					Log.log.error(ex.getMessage(), ex);
				}
			}
		}
		Log.log.info("Total opened index count " + count);
	}

	public ConfigSearch getConfig() {
		return config;
	}

	/**
	 * Creates a client. ES client is already thread safe and automatically
	 * reconnects / discovers the cluster.
	 *
	 * @return
	 */
	private Client createClient() {
		Log.log.info("Creating RSSearch client.");
		Log.log.info("Cluster name: " + config.getCluster());
		// client will try 20 seconds before giving up
		Settings.Builder settingsBuilder = Settings.builder().put("cluster.name", config.getCluster())
				.put("client.transport.ping_timeout", TimeValue.timeValueSeconds(20)).put("transport.tcp.compress", config.isCompress());
		if (config.isSearchGuard()) {
			settingsBuilder.put(SSLConfigConstants.SEARCHGUARD_SSL_TRANSPORT_KEYSTORE_FILEPATH, config.getSgKeystore())
					.put(SSLConfigConstants.SEARCHGUARD_SSL_TRANSPORT_TRUSTSTORE_FILEPATH, config.getSgTruststore());
			if (StringUtils.isNotEmpty(config.getSgKeystorePwd())) {
				settingsBuilder.put(SSLConfigConstants.SEARCHGUARD_SSL_TRANSPORT_KEYSTORE_PASSWORD,
						config.getSgKeystorePwd());
			} else {
				settingsBuilder.put(SSLConfigConstants.SEARCHGUARD_SSL_TRANSPORT_KEYSTORE_PASSWORD, "changeit");
			}

			if (StringUtils.isNotEmpty(config.getSgTruststorePwd())) {
				settingsBuilder.put(SSLConfigConstants.SEARCHGUARD_SSL_TRANSPORT_TRUSTSTORE_PASSWORD,
						config.getSgTruststorePwd());
			} else {
				settingsBuilder.put(SSLConfigConstants.SEARCHGUARD_SSL_TRANSPORT_TRUSTSTORE_PASSWORD, "changeit");
			}

			settingsBuilder.put("path.home", ".");
		}

		Settings settings = settingsBuilder.build();

		// a Map with value as serverip:port
		Map<Integer, String> servers = config.getServers();
		String serverport = ((Integer) config.getServerport()).toString();
		TransportAddress[] transportAddresses = new InetSocketTransportAddress[servers.size()];
		int i = 0;
		try {
			for (Integer key : servers.keySet()) {
				String server = servers.get(key);
				String[] serverInfo = new String[] { server, serverport };
				String serverIp = serverInfo[0];
				Integer port = Integer.valueOf(serverInfo[1]);
				TransportAddress transportAddress = new InetSocketTransportAddress(InetAddress.getByName(serverIp),
						port);
				Log.log.info("Server: " + serverIp + ":" + port);

				transportAddresses[i++] = transportAddress;
			}
		} catch (Exception ex) {
			Log.log.error(ex, ex);
			throw new RuntimeException(ex);
		}

		if (config.isSearchGuard()) {
			client = new PreBuiltTransportClient(settings, SearchGuardSSLPlugin.class)
					.addTransportAddresses(transportAddresses);
		} else {
			client = new PreBuiltTransportClient(settings).addTransportAddresses(transportAddresses);
		}
		Log.log.info("Client: " + client);

		return client;
	}

	Client getClient() {
		return client;
	}
	
	public void shutdown() {
		if (client != null) {
			client.close();
		}
		isRunning = false;
	}

	public boolean isRunning() {
		return isRunning;
	}

	/**
	 * By design all the json files must be inside this package under resources
	 * folder.
	 *
	 * @param fileName
	 * @return
	 */
	public static String readJson(String fileName) {
		InputStream stream = Server.class.getResourceAsStream("/" + fileName);
		return StringUtils.toString(stream, "UTF-8");
	}

	/**
	 * Checks if the cluster health is at least yellow. Yellow state is where the
	 * cluster is ready with all the shards but may not have allocated the replicas
	 * yet which is fine to start using it.
	 * 
	 * In general either the state of GREEN or YELLOW is fine to moving forward.
	 * 
	 * Special Case: Worksheet and related indices need a quorum of shards to be
	 * available to start executing runbooks.
	 * 
	 * @return
	 */
	private boolean ensureYellow() {
		boolean result = false;
		for (int i = 0; i < 20; ++i) {
			try {
				Log.log.debug("Checking RSSearch status. Count: " + i);
				ClusterHealthResponse actionGet = getClient().admin().cluster().health(Requests.clusterHealthRequest()
						.waitForYellowStatus().waitForEvents(Priority.LANGUID).waitForNoRelocatingShards(true))
						.actionGet();
				if (actionGet.isTimedOut()) {
					Log.log.info("ensureYellow timed out, cluster state: "
							+ getClient().admin().cluster().prepareState().get().getState() + " \n "
							+ getClient().admin().cluster().preparePendingClusterTasks().get());
					Thread.sleep(10000L);
				} else if (ClusterHealthStatus.RED.equals(actionGet.getStatus())) {
					Log.log.info("RSSearch cluster status is RED, waiting to be yellow.");
					Thread.sleep(10000L);
				} else {
					result = actionGet.getStatus().equals(ClusterHealthStatus.GREEN)
							|| actionGet.getStatus().equals(ClusterHealthStatus.YELLOW);
					break;
				}
			} catch (Exception e) {
				Log.log.warn(e.getMessage(), e);
			}
		}
		return result;
	}

	/**
	 * Initializes a index with settings and mappings from json files.
	 * 
	 * @param indexName
	 * @param settingsJson
	 * @param mappings
	 * @throws SearchException
	 */
	void createIndex(String indexName, String settingsJson, Map<String, String> mappings) throws SearchException {
		createIndex(indexName, settingsJson, mappings, config.getShards(), config.getReplicas());
	}

	/**
	 * Initializes a index with settings and mappings from json files.
	 * 
	 * @param indexName
	 * @param settingsJson
	 * @param mappings
	 * @param numberOfShards
	 * @param numberOfReplicas
	 */
	void createIndex(String indexName, String settingsJson, Map<String, String> mappings, int numberOfShards,
			int numberOfReplicas) throws SearchException {
		Client client = getClient();
		Log.log.info("Try to create index " + indexName + ", numberOfShards=" + numberOfShards + ", numberOfReplicas="
				+ numberOfReplicas);
		try {
			// verify if the index already exists.
			if (!isExists(indexName)) {
				CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);

				Builder builder = Settings.builder().put("number_of_shards", numberOfShards)
						.put("number_of_replicas", numberOfReplicas).put("max_result_window", 5000000)
						.put("priority", 0);
				Settings indexSettings = null;

				if (StringUtils.isBlank(settingsJson)) {
					indexSettings = builder.build();
				} else {
					indexSettings = builder.loadFromSource(settingsJson).build();
				}

				if (mappings != null && mappings.size() > 0) {
					for (String documentType : mappings.keySet()) {
						if (StringUtils.isNotBlank(documentType)) {
							createIndexRequestBuilder.addMapping(documentType, mappings.get(documentType));
						} else {
							createIndexRequestBuilder.addMapping(indexName, mappings.get(documentType));
						}
					}
				}

				createIndexRequestBuilder.setSettings(indexSettings);
				CreateIndexResponse createResponse = createIndexRequestBuilder.execute().actionGet();
				if (createResponse.isAcknowledged()) {
					Log.log.info("Successfully created index " + indexName);
				}

				Log.log.info("Index created: " + indexName + ", number_of_shards: " + numberOfShards
						+ ", number_of_replicas: " + numberOfReplicas + ", other settings: " + settingsJson
						+ " and mappings: " + mappings);
				if (mappings != null && mappings.size() > 0) {
					for (String documentType : mappings.keySet()) {
						Log.log.info("Document type : " + documentType);
						Log.log.info("Mappings : " + mappings.get(documentType));
					}
				}
			} else {
				Log.log.info("Index " + indexName + " already exists, create request ignored.");
			}
		} catch (IllegalStateException e) {
			// this is an acceptable exception
			Log.log.info(e.getMessage(), e);
		}
	}

	/**
	 * Updates an index with settings and mappings from json files.
	 * 
	 * @param indexName
	 * @param settingsJson
	 * @param mappings
	 * @param numberOfShards
	 * @param numberOfReplicas
	 */
	void updateIndex(String indexName, String settingsJson, Map<String, String> mappings, int numberOfShards,
			int numberOfReplicas) throws SearchException {
		Client client = getClient();

		try {
			// verify if the index already exists.
			if (isExists(indexName)) {
				// to update we need to close the index
				CloseIndexResponse closeResponse = getClient().admin().indices().close(new CloseIndexRequest(indexName))
						.get();
				if (closeResponse.isAcknowledged()) {
					Log.log.debug("Index closed for settings/mappings update: " + indexName);

					// update call is required in case we add new settings or
					// mappings on an existing index.
					UpdateSettingsRequestBuilder updateRequestBuilder = client.admin().indices()
							.prepareUpdateSettings(indexName);

					if (StringUtils.isNotBlank(settingsJson)) {
						Settings indexSettings = Settings.builder().put("number_of_replicas", numberOfReplicas)
								.loadFromSource(settingsJson, XContentType.JSON).build();
						updateRequestBuilder.setSettings(indexSettings);
					}
					UpdateSettingsResponse updateSettingsResponse = updateRequestBuilder.execute().get();
					if (updateSettingsResponse.isAcknowledged()) {
						if (mappings != null && mappings.size() > 0) {
							for (String documentType : mappings.keySet()) {
								if (StringUtils.isNotBlank(documentType)) {
									client.admin().indices().preparePutMapping().setIndices(indexName)
											.setType(documentType).setSource(mappings.get(documentType)).execute();
								} else {
									client.admin().indices().preparePutMapping().setIndices(indexName)
											.setSource(mappings.get(documentType)).execute();
								}
							}
						}
					}

					// to update we need to close the index
					OpenIndexResponse openResponse = getClient().admin().indices().open(new OpenIndexRequest(indexName))
							.get();
					if (openResponse.isAcknowledged()) {
						Log.log.debug("Index opened successfully after update: " + indexName);
					}
					if (Log.log.isTraceEnabled()) {
						Log.log.trace("Index updated: " + indexName + ", number_of_shards: " + numberOfShards
								+ ", number_of_replicas: " + numberOfReplicas + ", other settings: " + settingsJson
								+ " and mappings: " + mappings);
						if (mappings != null && mappings.size() > 0) {
							for (String documentType : mappings.keySet()) {
								Log.log.trace("Document type : " + documentType);
								Log.log.trace("Mappings : " + mappings.get(documentType));
							}
						}
					}
				}
			} else {
				Log.log.debug("Index " + indexName + " does not exists, mapping update requests ignored.");
			}
		} catch (InterruptedException e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		} catch (ExecutionException e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		} catch (IllegalStateException e) {
			// this is an acceptable exception
			Log.log.info(e.getMessage(), e);
		}
	}

	boolean isExists(String indexName) {
		boolean result = false;

		try {
			IndicesExistsResponse response = getClient().admin().indices().exists(new IndicesExistsRequest(indexName))
					.get();
			if (response.isExists()) {
				result = true;
			}
		} catch (InterruptedException e) {
			Log.log.error(e.getMessage(), e);
		} catch (ExecutionException e) {
			Log.log.error(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * Removes an Index from the ElasticSearch server/cluster.
	 *
	 * @param indexName
	 *            to be deleted
	 */
	void removeIndex(String indexName) throws SearchException {
		try {
			DeleteIndexResponse response = getClient().admin().indices().delete(new DeleteIndexRequest(indexName))
					.actionGet();
			if (response.isAcknowledged()) {
				Log.log.info("Index " + indexName + " deleted successfully.");
			} else {
				throw new SearchException("Failed to delete index " + indexName);
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		}
	}

	String getExistingMappingVersion(String monthlyIndexName, String documentType) {
		String result = null;

		GetMappingsRequestBuilder request = SearchAdminAPI.getClient().admin().indices()
				.prepareGetMappings(monthlyIndexName);
		GetMappingsResponse response = request.get();
		if (response != null) {
			ImmutableOpenMap<String, MappingMetaData> mappingMap = response.mappings().get(monthlyIndexName);
			MappingMetaData mappingMetaData = mappingMap.get(documentType);
			try {
				String mappingJson = mappingMetaData.source().string();
				result = SearchUtils.readMappingVersion(mappingJson, documentType);
			} catch (IOException e) {
				Log.log.error(e.getMessage(), e);
			}
		}
		return result;
	}

	/**
	 * This thread creates the daily indices for worksheet, processrequest, and
	 * taskresult.
	 */
	class IndexCreator extends Thread {
		private final Server server;
		private volatile boolean active = true;

		public IndexCreator(Server server) {
			this.server = server;
		}

		public boolean isActive() {
			return active;
		}

		public void setActive(boolean active) {
			this.active = active;
		}

		@Override
		public void run() {
			while (isActive()) {
				try {
					// every 12 hours is good enough since we create current and next days index at
					// a time
					// sleep first to prevent dual creation on start up
					Log.log.info("Index creator goes to sleep. ");
					sleep(12 * 60 * 60 * 1000L + ThreadLocalRandom.current().nextLong(120000));
					// long t = 2*60*1000L + (new Double( Math.random()*100000).longValue()) ;
					// sleep(t);
					Log.log.info("Index creator wakes up. Try to config index.");
					server.configureDynamicIndices();
				} catch (InterruptedException e) {
					Log.log.error(e.getMessage(), e);
					break;
				} catch (SearchException e) {
					Log.log.error(e.getMessage(), e);
					break;
				} catch (Throwable e) {
					Log.log.error(e.getMessage(), e);
				}
			}
			Log.log.error("Nightly index creator stopped!");
		}// end run
	}
}
