/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

@SuppressWarnings("serial")
public class SearchException extends Exception
{
    private static final long serialVersionUID = -8750381609488168173L;
	private String errorNumber;

    public SearchException(String message)
    {
        super(message);
    }

    public SearchException(String message, Throwable t)
    {
        super(message, t);
    }
    
    public SearchException(String errorNumber, String message, Throwable t)
    {
        super(message, t);
        this.errorNumber = errorNumber;
    }
    
    public String getErrorNumber()
    {
        return errorNumber;
    }

    public void setErrorNumber(String errorNumber)
    {
        this.errorNumber = errorNumber;
    }
}
