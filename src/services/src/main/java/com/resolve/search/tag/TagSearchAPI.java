/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.tag;

import java.util.List;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.Tag;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;

public class TagSearchAPI
{
    private static final SearchAPI<Tag> searchAPI = APIFactory.getTagSearchAPI();

    public static Tag findTagById(final String sysId, final String username) throws SearchException
    {
        return searchAPI.findDocumentById(sysId, username);
    }

    /**
     * Supports all the available conditions exposed by QueryDTO.
     *
     * @param queryDTO
     *            could have a filter item as "name contains 'foo'"
     *
     * @return {@link ResponseDTO} with the {@link Tag} set to the data field.
     * @throws SearchException
     */
    public static ResponseDTO<Tag> searchTags(final QueryDTO queryDTO, final String username) throws SearchException
    {
        ResponseDTO<Tag> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            result = searchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Finds a tag by its name.
     *
     * @param tagName
     * @return Returns found {@link Tag}, null otherwise
     * @throws SearchException
     */
    public static Tag findTagByName(final String tagName, final String username) throws SearchException
    {
        Tag result = null;

        try
        {
            String tmpTagName = tagName;
            if(tagName.startsWith("#"))
            {
                tmpTagName = tagName.substring(1);
            }
            QueryDTO queryDTO = new QueryDTO(0, 1);
            queryDTO.addFilterItem(new QueryFilter("name", QueryFilter.EQUALS, tmpTagName));
            ResponseDTO<Tag> response = searchAPI.searchByQuery(queryDTO, username);
            List<? extends Object> records = response.getRecords();
            if (records != null && records.size() >= 1)
            {
                if (records.size() == 1)
                {
                    result = (Tag) records.get(0);
                }
                else
                {
                    throw new SearchException("Data consistency violated, found multiple tags with the same name");
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

}
