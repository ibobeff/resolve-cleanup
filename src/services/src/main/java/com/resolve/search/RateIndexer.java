/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.util.Collection;

import com.resolve.search.model.query.Query;



/**
 * Interface that is implemented by query indexer so we can update rating of query.
 *
 * QueryIndexer is one of the implementer.
 */
public interface RateIndexer<T> extends Indexer<T>
{
    /**
     * This method indexes queries.
     * 
     * @param sourceId is the sysId of the source that generated the queries (WikiDocument, SocialPost AT etc.)
     * @param indexData
     * @param updateRating
     * @throws SearchException
     */
    void indexQueries(IndexData<String> indexData, boolean updateRating) throws SearchException;

    /**
     * This method indexes queries.
     * 
     * @param sourceId
     * @param queries
     * @param sysUpdatedOn
     * @param updateRating
     * @param username
     * @throws SearchException
     */
    void indexQueries(String sourceId, Collection<String> queries, Long sysUpdatedOn, boolean updateRating, String username) throws SearchException;
    
    /**
     * This method updates rating of a query.
     * 
     * @param query
     * @param username
     * @throws SearchException
     */
    void updateRating(Query query, String username) throws SearchException;
}
