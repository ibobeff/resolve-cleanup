package com.resolve.search.content;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.lucene.search.join.ScoreMode;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.admin.indices.validate.query.ValidateQueryRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.sort.SortOrder;

import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.model.UserInfo;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.ContentType;
import com.resolve.services.vo.SearchAttributesDTO;
import com.resolve.services.vo.SearchRecordDTO;
import com.resolve.services.vo.SearchResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ContentSearchAPI {

	public static final String AGGREGATION_FIELD_NAMESPACE = "namespace";

	public static final String AGGREGATION_FIELD_MENUPATH = "menupath";

	private static final int MAX_AGGREGATION_BUCKETS = 10000;

	private static final String AGGREGATION_TYPE_COUNTS = "typeCounts";

	public static final String AGGREGATION_FIELD_AUTHOR = "sysCreatedBy,sysUpdatedBy";

	public static SearchResponseDTO searchAllDocuments(final SearchAttributesDTO searchDTO, final UserInfo userInfo) {
		return searchAllDocuments(searchDTO, userInfo, null);
	}

	/**
	 * If aggregation field is specified it must have a "raw" subfield that contains
	 * the normal field representation upon which an aggregation is to be performed.
	 * If no aggregation field is spevified normal search is performed based on
	 * search criteria.
	 */
	public static SearchResponseDTO searchAllDocuments(final SearchAttributesDTO searchDTO, final UserInfo userInfo,
			String aggregationField) {
		String username = userInfo.getUsername();

		SearchResponseDTO searchResult = new SearchResponseDTO();

		SearchRequestBuilder searchRequestBuilder = prepareSearchRequest(searchDTO, aggregationField, username);

		if (aggregationField == null) {

			boolean isRequestValid = true;
			// validate query first
			try {
				isRequestValid = validateRequest(searchRequestBuilder);
			} catch (InterruptedException | ExecutionException e) {
				Log.log.warn(e.getMessage());
				isRequestValid = false;
			}

			if (!isRequestValid) {
				throw new ElasticsearchException("Invalid search term.");
			}

		}

		SearchResponse searchResponse = searchRequestBuilder.get();

		ContentType searchType = searchDTO.getType();

		// count query to retrieve proper counts accross all types;
		searchDTO.setType(ContentType.ALL.name());
		searchDTO.setSize("0");
		searchRequestBuilder = prepareSearchRequest(searchDTO, aggregationField, username);
		SearchResponse countsSearchResponse = searchRequestBuilder.get();

		Set<SearchRecordDTO> searchRecords = new LinkedHashSet<>();
		if (aggregationField != null) {

			Terms aggregation = searchResponse.getAggregations().get(aggregationField);

			for (Bucket bucket : aggregation.getBuckets()) {
				String value = bucket.getKeyAsString();
				SearchRecordDTO searchRecord = new SearchRecordDTO();
				if (AGGREGATION_FIELD_NAMESPACE.equals(aggregationField)) {
					searchRecord.setNamespace(value);
				} else if (AGGREGATION_FIELD_MENUPATH.equals(aggregationField)) {
					searchRecord.setMenupath(value);
				}
				searchRecords.add(searchRecord);
			}

		} else {
			appendCounts(countsSearchResponse, searchResult);

			for (SearchHit hit : searchResponse.getHits()) {
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonNode node = mapper.readTree(hit.getSourceAsString());
					String jsonTypeNode = node.get("type").asText();
					SearchRecordDTO searchRecord = null;

					searchRecord = parseRecordFromJSON(node, jsonTypeNode, searchType);

					if (searchRecord != null) {
						// get only populated records and count all available types
						if (StringUtils.isNoneBlank(searchRecord.getSysId())) {
							searchRecords.add(searchRecord);
						}
					}

				} catch (IOException e) {
					Log.log.error(e.getMessage(), e);
				}

			}
		}

		List<SearchRecordDTO> records = new ArrayList<>();
		records.addAll(searchRecords);

		searchResult.setRecords(records);
		return searchResult;
	}

	public static List<String> getAuthors(final UserInfo userInfo) {
		String username = userInfo.getUsername();

		SearchRequestBuilder searchRequestBuilder = prepareSearchRequest(new SearchAttributesDTO(),
				AGGREGATION_FIELD_AUTHOR, username);
		SearchResponse searchResponse = searchRequestBuilder.get();

		String[] aggFields = AGGREGATION_FIELD_AUTHOR.split(",");
		Terms aggregation = null;
		Set<String> searchRecords = new HashSet<>();

		for (String field : aggFields) {
			aggregation = searchResponse.getAggregations().get(field);

			for (Bucket bucket : aggregation.getBuckets()) {
				String value = bucket.getKeyAsString();

				searchRecords.add(value);
			}
		}

		List<String> authors = new ArrayList<>();
		authors.addAll(searchRecords);

		return authors;
	}

	private static boolean validateRequest(SearchRequestBuilder searchRequestBuilder)
			throws InterruptedException, ExecutionException {
		final ValidateQueryRequest request = new ValidateQueryRequest();
		request.indices(SearchDefinition.ALL_INDEXES);
		request.query(searchRequestBuilder.request().source().query());
		return SearchAdminAPI.getClient().admin().indices().validateQuery(request).get().isValid();
	}

	private static void appendCounts(SearchResponse searchResponse, SearchResponseDTO searchResult) {
		Terms typeCountsAggregator = searchResponse.getAggregations().get(AGGREGATION_TYPE_COUNTS);
		Map<ContentType, Long> typeCounts = new HashMap<>();
		for (Bucket bucket : typeCountsAggregator.getBuckets()) {
			String value = bucket.getKeyAsString();
			long count = bucket.getDocCount();

			ContentType type = null;
			if ("plain".equals(value.trim().toLowerCase())) {
				type = ContentType.PROPERTY;
			} else {
				try {
					type = ContentType.valueOf(value.trim().toUpperCase());
				} catch (Exception ex) {
					// ignore unknown type
					Log.log.error(ex.getMessage());
				}
			}

			if (type != null) {
				Long currentCount = typeCounts.get(type);
				if (currentCount == null) {
					typeCounts.put(type, count);
				} else {
					typeCounts.put(type, currentCount + count);
				}
			}

		}

		long total = 0;
		for (Map.Entry<ContentType, Long> entry : typeCounts.entrySet()) {
			searchResult.setCount(entry.getKey(), entry.getValue());
			total += entry.getValue();
		}
		searchResult.setTotal(total);
	}

	private static SearchRecordDTO parseRecordFromJSON(JsonNode node, String jsonTypeNode, ContentType searchedType)
			throws JsonProcessingException, IOException {

		ContentType typeNode = null;
		if (jsonTypeNode != null && "plain".equals(jsonTypeNode.toLowerCase())) {
			typeNode = ContentType.PROPERTY;
		} else {
			try {
				typeNode = ContentType.valueOf(jsonTypeNode.trim().toUpperCase());
			} catch (Exception ex) {
				// ignore unknown type
				Log.log.error(ex.getMessage());
			}
		}

		SearchRecordDTO searchRecord = null;

		// parse only recorcds with the given type
		if (typeNode != null && searchedType == ContentType.ALL
				|| (searchedType != ContentType.ALL && searchedType == typeNode)) {
			JsonNode sysId = node.get("sysId");
			JsonNode name = node.get("name");
			JsonNode summary = node.get("summary");
			JsonNode namespaceNode = node.get("namespace");
			JsonNode menuPathNode = node.get("menupath");
			JsonNode sysCreatedOn = node.get("sysCreatedOn");
			JsonNode sysCreatedBy = node.get("sysCreatedBy");
			JsonNode sysUpdatedOn = node.get("sysUpdatedOn");
			JsonNode sysUpdatedBy = node.get("sysUpdatedBy");
			JsonNode accessRights = node.get("accessRights");
			JsonNode tags = node.get("tags");

			searchRecord = new SearchRecordDTO();
			searchRecord.setSysId(sysId != null ? sysId.asText() : null);
			searchRecord.setName(name != null ? name.asText() : null);
			searchRecord.setSummary(summary != null ? summary.asText() : null);
			searchRecord.setNamespace(namespaceNode != null ? namespaceNode.asText() : null);
			searchRecord.setMenupath(menuPathNode != null ? menuPathNode.asText() : null);
			searchRecord.setType(typeNode);
			searchRecord.setSysCreatedOn(sysCreatedOn != null ? sysCreatedOn.asLong() : null);
			searchRecord.setSysCreatedBy(sysCreatedBy != null ? sysCreatedBy.asText() : null);
			searchRecord.setSysUpdatedOn(sysUpdatedOn != null ? sysUpdatedOn.asLong() : null);
			searchRecord.setSysUpdatedBy(sysUpdatedBy != null ? sysUpdatedBy.asText() : null);

			if (tags != null && tags.size() != 0) {
				List<String> tagList = new ArrayList<>();
				for (JsonNode tag : tags) {
					tagList.add(tag.asText());
				}
				searchRecord.setTags(tagList);
			}

			if (accessRights != null) {
				JsonNode readAccess = accessRights.get("readAccess");
				JsonNode writeAccess = accessRights.get("writeAccess");
				JsonNode adminAccess = accessRights.get("adminAccess");
				JsonNode executeAccess = accessRights.get("executeAccess");

				searchRecord.setReadAccess((readAccess != null ? readAccess.asText() : null));
				searchRecord.setWriteAccess((writeAccess != null ? writeAccess.asText() : null));
				searchRecord.setAdminAccess((adminAccess != null ? adminAccess.asText() : null));
				searchRecord.setExecuteAccess((executeAccess != null ? executeAccess.asText() : null));
			}
		}
		return searchRecord;
	}

	/**
	 * Create index name, index type and document type by searched type
	 * 
	 * @param type
	 *            {@linkplain ContentType} enumerate types
	 * @return {@link SearchDefinition}
	 */
	private static SearchDefinition getSearchType(ContentType type) {
		SearchDefinition searchType = null;

		switch (type) {
		case DOCUMENT: {
			searchType = new SearchDefinition(SearchConstants.SEARCH_TYPE_DOCUMENT,
					SearchConstants.SEARCH_TYPE_DOCUMENT);
			break;
		}
		case AUTOMATION: {
			searchType = new SearchDefinition(SearchConstants.SEARCH_TYPE_AUTOMATION,
					SearchConstants.SEARCH_TYPE_AUTOMATION);
			break;
		}
		case DECISIONTREE: {
			searchType = new SearchDefinition(SearchConstants.SEARCH_TYPE_DECISION_TREE,
					SearchConstants.SEARCH_TYPE_DECISION_TREE);
			break;
		}
		case PLAYBOOK: {
			searchType = new SearchDefinition(SearchConstants.SEARCH_TYPE_PLAYBOOK,
					SearchConstants.SEARCH_TYPE_PLAYBOOK);
			break;
		}
		case ACTIONTASK: {
			searchType = new SearchDefinition(SearchConstants.INDEX_NAME_ACTION_TASK,
					SearchConstants.DOCUMENT_TYPE_ACTION_TASK);
			break;
		}
		case PROPERTY: {
			searchType = new SearchDefinition(SearchConstants.INDEX_NAME_PROPERTY,
					SearchConstants.DOCUMENT_TYPE_PROPERTY);
			break;
		}
		case CUSTOMFORM: {
			searchType = new SearchDefinition(SearchConstants.INDEX_NAME_CUSTOM_FORM,
					SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM);
			break;
		}
		default:
			break;
		}

		return searchType;
	}

	/**
	 * Create search request for every content type
	 * 
	 * @param searchDTO
	 * @param aggregationField
	 * @return
	 */
	private static SearchRequestBuilder prepareSearchRequest(SearchAttributesDTO searchDTO, String aggregationField,
			String username) {

		SearchDefinition searchDefinition = null;
		if (searchDTO.getType() != ContentType.ALL) {
			searchDefinition = getSearchType(searchDTO.getType());
		}

		SearchRequestBuilder requestBuilder = buildSearchRequest(searchDTO, searchDefinition, aggregationField,
				username);
		return requestBuilder;
	}

	/**
	 * Create search request for elastic search
	 * 
	 * @param searchDTO
	 *            attributes to filter by
	 * @param searchDefinition
	 *            index definitions
	 * @param aggregationField
	 *            field to aggregate by
	 * @return SearchRequestBuilder
	 */
	private static SearchRequestBuilder buildSearchRequest(SearchAttributesDTO searchDTO,
			SearchDefinition searchDefinition, String aggregationField, String username) {
		SearchRequestBuilder searchRequestBuilder = null;
		QueryBuilder termQuery = null;

		// if no term condition passed return all documents
		if (StringUtils.isNotEmpty(searchDTO.getTerm())) {
			termQuery = QueryBuilders.queryStringQuery(searchDTO.getTerm());
		} else {
			termQuery = QueryBuilders.matchAllQuery();
		}

		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery().must(termQuery)
				.must(QueryBuilders.termQuery("deleted", false)).mustNot(QueryBuilders.termQuery("type", "encrypt"));

		addFilters(boolQuery, searchDTO);
		addFiltersByAccessRights(boolQuery, username);

		if (searchDefinition != null) {
			searchRequestBuilder = SearchAdminAPI.getClient().prepareSearch(searchDefinition.getIndex())
					.setTypes(searchDefinition.getType()).setQuery(boolQuery);
		} else {
			searchRequestBuilder = SearchAdminAPI.getClient().prepareSearch(SearchDefinition.ALL_INDEXES)
					.setTypes(SearchDefinition.ALL_INDEX_TYPES).setQuery(boolQuery);
		}

		if (aggregationField != null) {
			String[] aggFields = aggregationField.split(",");
			AggregationBuilder termsAggregation = null;

			for (String field : aggFields) {
				termsAggregation = AggregationBuilders.terms(field).field(field + ".raw").size(MAX_AGGREGATION_BUCKETS)
						.order(Terms.Order.term(true));
				searchRequestBuilder.setFetchSource(new String[] { field }, new String[] {}) //
						.addAggregation(termsAggregation) //
						.setSize(0);
			}
		} else {
			if (searchDTO.getSortBy() != null) {
				SortOrder order = null;
				try {
					if (searchDTO.getSortOrder() != null) {
						order = SortOrder.fromString(searchDTO.getSortOrder());
					}
				} catch (Exception e) {
					Log.log.warn(e.getMessage());
				}

				String sortBy = searchDTO.getSortBy();
				if (!sortBy.equals("type") && !sortBy.equals("sysCreatedOn") && !sortBy.equals("sysUpdatedOn")) {
					sortBy += "." + sortBy;
				}
				searchRequestBuilder.addSort(sortBy, order != null ? order : SortOrder.ASC);
			}
			AggregationBuilder countBuilder = AggregationBuilders.terms(AGGREGATION_TYPE_COUNTS).field("type")
					.size(MAX_AGGREGATION_BUCKETS);
			searchRequestBuilder.addAggregation(countBuilder).setFrom(searchDTO.getFrom()).setSize(searchDTO.getSize());
		}

		return searchRequestBuilder;
	}

	/**
	 * Adding filters based on searched attributes
	 * 
	 * @param boolQuery
	 *            add filters to boolean query
	 * @param searchDTO
	 *            attributes to filter by
	 */
	private static void addFilters(BoolQueryBuilder boolQuery, SearchAttributesDTO searchDTO) {
		if (searchDTO.getAuthors() != null && !searchDTO.getAuthors().isEmpty()) {
			QueryBuilder createdByFilter = QueryBuilders.termsQuery("sysCreatedBy", searchDTO.getAuthors());
			QueryBuilder updatedByFilter = QueryBuilders.termsQuery("sysUpdatedBy", searchDTO.getAuthors());
			QueryBuilder authorsShouldQuery = QueryBuilders.boolQuery().should(createdByFilter).should(updatedByFilter);
			boolQuery.filter(authorsShouldQuery);
		}

		if (searchDTO.getTags() != null && !searchDTO.getTags().isEmpty()) {
			BoolQueryBuilder tagsFilter = QueryBuilders.boolQuery();
			searchDTO.getTags().forEach(tag -> {
				tagsFilter.should(QueryBuilders.termQuery("tags", tag.toLowerCase()));
			});
			boolQuery.filter(tagsFilter);
		}

		if (searchDTO.getNamespaces() != null && !searchDTO.getNamespaces().isEmpty()) {
			BoolQueryBuilder namespacesFilter = QueryBuilders.boolQuery();
			if (searchDTO.getNamespaces().get(0).equals("*")) {
				namespacesFilter.should(QueryBuilders.wildcardQuery("namespace.raw", "*"));
			} else {
				searchDTO.getNamespaces().forEach(namespace -> {
					namespacesFilter.should(QueryBuilders.termQuery("namespace.raw", namespace));
					namespacesFilter.should(QueryBuilders.wildcardQuery("namespace.raw", namespace + ".*"));
					namespacesFilter.should(QueryBuilders.termQuery("namespace.raw", ""));
				});
			}

			boolQuery.filter(namespacesFilter);
		}

		if (searchDTO.getMenupaths() != null && !searchDTO.getMenupaths().isEmpty()) {
			BoolQueryBuilder menuPathsFilter = QueryBuilders.boolQuery();
			if (searchDTO.getMenupaths().get(0).equals("*")) {
				menuPathsFilter.should(QueryBuilders.wildcardQuery("menupath.raw", "*"));
			} else {
				searchDTO.getMenupaths().forEach(menupath -> {
					menuPathsFilter.should(QueryBuilders.termQuery("menupath.raw", menupath));
					menuPathsFilter.should(QueryBuilders.wildcardQuery("menupath.raw", menupath + "/*"));
				});
			}

			boolQuery.filter(menuPathsFilter);
		}

		if (StringUtils.isNotBlank(searchDTO.getCreatedOn())) {

			LocalDate fromDate = LocalDate.parse(searchDTO.getCreatedOn(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			LocalDate toDate = fromDate.plusDays(1);
			long from = fromDate.atStartOfDay(ZoneOffset.UTC).toEpochSecond() * 1000;
			long to = toDate.atStartOfDay(ZoneOffset.UTC).toEpochSecond() * 1000;

			RangeQueryBuilder createdOnFilter = QueryBuilders.rangeQuery("sysCreatedOn").gte(from).lt(to);

			boolQuery.filter(createdOnFilter);
		}

		if (StringUtils.isNotBlank(searchDTO.getUpdatedOn())) {

			LocalDate fromDate = LocalDate.parse(searchDTO.getUpdatedOn(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			LocalDate toDate = fromDate.plusDays(1);
			long from = fromDate.atStartOfDay(ZoneOffset.UTC).toEpochSecond() * 1000;
			long to = toDate.atStartOfDay(ZoneOffset.UTC).toEpochSecond() * 1000;

			RangeQueryBuilder createdOnFilter = QueryBuilders.rangeQuery("sysUpdatedOn").gte(from).lt(to);

			boolQuery.filter(createdOnFilter);
		}
	}

	private static void addFiltersByAccessRights(BoolQueryBuilder boolQuery, String username) {
		Set<String> userRoles = UserUtils.getUserRoles(username);
		List<String> rolesList = new ArrayList<>();
		rolesList.addAll(userRoles);
		String roles = String.join(",", rolesList);

		BoolQueryBuilder rolesFilter = QueryBuilders.boolQuery();
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.readAccess", roles));
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.writeAccess", roles));
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.executeAccess", roles));
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.adminAccess", roles));

		// workaround for property index which doesn't have access restriction at the
		// moment
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.readAccess", "all"));
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.writeAccess", "all"));
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.executeAccess", "all"));
		rolesFilter.should(QueryBuilders.matchQuery("accessRights.adminAccess", "all"));

		NestedQueryBuilder accessRightsFilter = QueryBuilders.nestedQuery("accessRights", rolesFilter, ScoreMode.None);

		boolQuery.filter(accessRightsFilter);
	}
}
