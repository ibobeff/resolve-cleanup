/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.worksheet;

import java.util.Collection;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchException;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class ExecuteStateIndexer implements Indexer<IndexData<String>>
{
    private static volatile ExecuteStateIndexer instance = null;
    
    private final IndexAPI indexAPI;

    private final ConfigSearch config;
    private long indexThreads;

    private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

    public static ExecuteStateIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("ExecuteStateIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static ExecuteStateIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new ExecuteStateIndexer(config);
        }
        return instance;
    }

    private ExecuteStateIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexAPI = APIFactory.getExecuteStateIndexAPI();
        this.indexThreads = this.config.getIndexthreads();
    }

    public void init()
    {
        Log.log.debug("Initializing ExecuteStateIndexer.");
        QueueListener<IndexData<String>> wikiIndexListener = new IndexListener<IndexData<String>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiIndexListener);
        Log.log.debug("ExecuteStateIndexer initialized.");
    }

    public boolean enqueue(IndexData<String> indexData)
    {
        boolean result = false;

        if(indexData != null && indexData.getObjects().size() > 0)
        {
            result = dataQueue.offer(indexData);
        }
        return result;
    }

    @Override
    public boolean index(IndexData<String> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> sysIds = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case DELETE:
                    deleteExecuteStates(sysIds, indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    private void deleteExecuteStates(final Collection<String> sysIds, final String username) throws SearchException
    {
        indexAPI.deleteDocumentByIds(sysIds, username);
    }
}
