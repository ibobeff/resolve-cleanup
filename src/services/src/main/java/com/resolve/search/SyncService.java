/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.search;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexAction;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.action.update.UpdateAction;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.resolve.search.model.SyncLog;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public final class SyncService implements Indexer<SyncData<Serializable>>
{
    private static volatile SyncService instance = null;

    private final SearchService searchService;
    private final Client remoteClient;
    private final ConfigSearch config;

    private ResolveConcurrentLinkedQueue<SyncData<Serializable>> dataQueue = null;

    public static SyncService getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SyncService is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static SyncService getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new SyncService(config);
        }
        return instance;
    }

    private SyncService(ConfigSearch config)
    {
        if (SearchAdminAPI.getClient() == null || SearchAdminAPI.getSearchConfig() == null)
        {
            throw new IllegalStateException("Search service is not initialized");
        }
        this.config = config;
        searchService = new SearchService();
        this.remoteClient = createRemoteClient();
        Log.log.info("Remote client: " + remoteClient);
    }
    
    public void init()
    {
        //create sync log if it does not exists
        Server server = Server.getInstance();
        String mappingJson = Server.readJson("sync_log_mappings.json");
        Map<String, String> mappings = new HashMap<String, String>();
        mappings.put(SearchConstants.DOCUMENT_TYPE_SYNC_LOG, mappingJson);
        String commonSettingsJson = Server.readJson("common_settings.json");
		List<String> indexNames = StringUtils.stringToList(
				"synclog_misc,synclog_socialpost,synclog_systempost,synclog_tag,synclog_worksheet,synclog_processrequest,synclog_taskresult,"
				+ "synclog_pbnotes,synclog_pbauditlog,synclog_pbartifacts,synclog_pbattachment,synclog_pbactivity",
				",");
        
        for(String indexName : indexNames)
        {
            if(!server.isExists(indexName))
            {
                try
                {
                    server.createIndex(indexName, commonSettingsJson, mappings);
                }
                catch (SearchException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

        Log.log.debug("Initializing sync listener.");
        QueueListener<SyncData<Serializable>> indexListener = new IndexListener<SyncData<Serializable>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(indexListener);
        Log.log.debug("sync listener initialized.");
    }
    
    public Client getRemoteClient()
    {
        return remoteClient;
    }

    /**
     * Creates a client. ES client is already thread safe and automatically
     * reconnects / discovers the cluster.
     * 
     * @return
     */
    private Client createRemoteClient()
    {
        Log.log.info("Creating Remote RSSearch client.");
        Log.log.info("Remote cluster name: " + config.getSyncCluster());
        // client will try 60 seconds before giving up
        Settings settings = Settings.builder().put("cluster.name", config.getSyncCluster()).put("client.transport.ping_timeout", TimeValue.timeValueSeconds(60)).build();

        // a Map with value as serverip:port
        Map<Integer, String> syncServers = config.getSyncServers();
        String syncServerPort = ((Integer) config.getSyncServerport()).toString();
        TransportAddress[] transportAddresses = new InetSocketTransportAddress[syncServers.size()];
        int i = 0;
        try
        {
	        for (Integer key : syncServers.keySet())
	        {
	            String syncServer = syncServers.get(key);
	            String[] serverInfo = new String[] { syncServer, syncServerPort };
	            String syncServerIp = serverInfo[0];
	            Integer syncPort = Integer.valueOf(serverInfo[1]);
	            TransportAddress transportAddress = new InetSocketTransportAddress(InetAddress.getByName(syncServerIp), syncPort);
	            Log.log.info("Sync Server: " + syncServerIp + ":" + syncPort);
	            transportAddresses[i++] = transportAddress;
	        }
        }
        catch (Exception ex)
        {
        	Log.log.error(ex,ex);
        	throw new RuntimeException(ex); 
        }

        return new PreBuiltTransportClient(settings).addTransportAddresses(transportAddresses);
    }

    @Override
    public boolean enqueue(SyncData<Serializable> syncData)
    {
        if (!dataQueue.offer(syncData))
        {
            try
            {
                SyncLog syncLog = new SyncLog(syncData);
                searchService.index(syncData.getSyncLogIndexName(), SearchConstants.DOCUMENT_TYPE_SYNC_LOG, SearchConstants.SYS_ID, null, null, syncLog, false, false, ActiveShardCount.ONE, syncData.getUsername());
                Log.log.warn("Data queue cannot take any more requests, saving in synclog.");
            }
            catch (SearchException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return true;
    }

    @Override
    public boolean index(SyncData<Serializable> syncData) throws SearchException
    {
        SearchUtils.SYNC_OPERATION operation = syncData.getOperation();
        switch (operation)
        {
            case INDEX:
                indexDocument(syncData);
                break;
            case UPDATE_BY_SCRIPT:
                updateDocument(syncData);
                break;
            case DELETE:
                deleteDocument(syncData);
                break;
            case DELETE_BY_QUERY:
                deleteDocumentByQuery(syncData);
                break;
            case BULK_DELETE:
            	deleteDocumentsInBulk(syncData);
            	break;
            default:
                break;
        }
        return true;
    }

    void indexDocument(SyncData<Serializable> syncData)
    {
        boolean log = false;
        try
        {
            IndexRequestBuilder indexRequestBuilder = new IndexRequestBuilder(remoteClient, IndexAction.INSTANCE, syncData.getIndexName());
            indexRequestBuilder.setType(syncData.getDocumentType());
            indexRequestBuilder.setWaitForActiveShards(ActiveShardCount.ONE); //.setConsistencyLevel(WRITE_CONSISTENCY_LEVEL_ONE);
//            indexRequestBuilder.setConsistencyLevel(WRITE_CONSISTENCY_LEVEL_ONE);
            if (StringUtils.isNotBlank(syncData.getParentId()))
            {
                indexRequestBuilder.setParent(syncData.getParentId());
            }
            if (StringUtils.isNotBlank(syncData.getRoutingKey()))
            {
                indexRequestBuilder.setRouting(syncData.getRoutingKey());
            }

            // indexRequestBuilder.setRefresh(true);
            indexRequestBuilder.setSource(SearchUtils.createJson(syncData.getObject()), XContentType.JSON).setId(syncData.getId());
            IndexResponse response = indexRequestBuilder.setWaitForActiveShards(ActiveShardCount.ONE).execute().actionGet();
            if (response == null)
            {
                log = true;
            }
        }
        catch (Exception e)
        {
            log = true;
            Log.log.debug(e.getMessage());
        }
        
        if(log)
        {
            try
            {
                // leave it to the RSSync
                SyncLog syncLog = new SyncLog(syncData);
                searchService.index(syncData.getSyncLogIndexName(), SearchConstants.DOCUMENT_TYPE_SYNC_LOG, SearchConstants.SYS_ID, null, null, syncLog, false, false, ActiveShardCount.ONE, syncData.getUsername());
                Log.log.debug("Something wrong, didn't get any response for an index request from remote cluster. storing in SyncLog for RSSync to deal with it");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    void updateDocument(SyncData<Serializable> syncData)
    {
        boolean log = false;
        try
        {
            UpdateRequestBuilder updateRequestBuilder = new UpdateRequestBuilder(remoteClient, UpdateAction.INSTANCE, syncData.getIndexName(), syncData.getDocumentType(), syncData.getId());
            if (StringUtils.isNotBlank(syncData.getParentId()))
            {
                updateRequestBuilder.setParent(syncData.getParentId());
            }
            if (StringUtils.isNotBlank(syncData.getRoutingKey()))
            {
                updateRequestBuilder.setRouting(syncData.getRoutingKey());
            }
            updateRequestBuilder.setRetryOnConflict(3);
            updateRequestBuilder.setScript(new Script(ScriptType.INLINE, "mvel", syncData.getScript(), syncData.getScriptParams())); //ScriptType.INLINE).setScriptLang("mvel");
//            if (syncData.getScriptParams() != null)
//            {
//                for (String name : syncData.getScriptParams().keySet())
//                {
//                    updateRequestBuilder.addScriptParam(name, syncData.getScriptParams().get(name));
//                }
//            }
            UpdateResponse response = updateRequestBuilder.setWaitForActiveShards(ActiveShardCount.ONE).execute().actionGet();
            if (response == null || !response.getResult().equals(org.elasticsearch.action.DocWriteResponse.Result.CREATED))   //.getGetResult()..isCreated())
            {
                log = true;
            }
        }
        catch (Exception e)
        {
            log = true;
            Log.log.info(e.getMessage());
        }
        
        //if something went something wrong during sync with remote cluster then log it.
        if(log)
        {
            try
            {
                // leave it to the RSSync
                SyncLog syncLog = new SyncLog(syncData);
                searchService.index(syncData.getSyncLogIndexName(), SearchConstants.DOCUMENT_TYPE_SYNC_LOG, SearchConstants.SYS_ID, null, null, syncLog, false, false, ActiveShardCount.ONE, syncData.getUsername());
                Log.log.info("Something wrong, didn't get any response for an update request from remote cluster. storing in SyncLog for RSSync to deal with it");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    void deleteDocument(SyncData<Serializable> syncData)
    {
        boolean log = false;
        try
        {
            DeleteRequestBuilder deleteRequestBuilder = remoteClient.prepareDelete(syncData.getIndexName(), syncData.getDocumentType(), syncData.getId());
            if (StringUtils.isNotBlank(syncData.getParentId()))
            {
                deleteRequestBuilder.setParent(syncData.getParentId());
            }
            if (StringUtils.isNotBlank(syncData.getRoutingKey()))
            {
                deleteRequestBuilder.setRouting(syncData.getRoutingKey());
            }
    
            DeleteResponse response = deleteRequestBuilder.setRefreshPolicy(RefreshPolicy.IMMEDIATE).setWaitForActiveShards(ActiveShardCount.ONE).execute().actionGet();
            if (response == null || !response.getResult().equals(org.elasticsearch.action.DocWriteResponse.Result.DELETED));        //.isFound())
            {
                log = true;
            }
        }
        catch (Exception e)
        {
            log = true;
            Log.log.info(e.getMessage());
        }
        
        //if something went something wrong during sync with remote cluster then log it.
        if(log)
        {
            try
            {
                // leave it to the RSSync
                SyncLog syncLog = new SyncLog(syncData);
                searchService.index(syncData.getSyncLogIndexName(), SearchConstants.DOCUMENT_TYPE_SYNC_LOG, SearchConstants.SYS_ID, null, null, syncLog, false, false, ActiveShardCount.ONE, syncData.getUsername());
                Log.log.info("Something wrong, didn't get any response for a delete request from remote cluster. storing in SyncLog for RSSync to deal with it");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Deletes data by its sysId.
     * 
     * @param indexName
     * @param documentType
     * @param id
     * @throws SearchException
     */
    public void deleteById(String indexName, String documentType, String id) throws SearchException
    {
        deleteById(indexName, documentType, id, null);
    }

    /**
     * Deletes data by its sysId and routing key.
     * 
     * @param indexName
     * @param documentType
     * @param id
     * @param routingKey
     * @throws SearchException
     */
    void deleteById(String indexName, String documentType, String id, String routingKey) throws SearchException
    {
        Client client = getRemoteClient();
        DeleteRequestBuilder deleteRequestBuilder = client.prepareDelete(indexName, documentType, id);
        if (StringUtils.isNotBlank(routingKey))
        {
            deleteRequestBuilder.setRouting(routingKey);
        }

        // set the consistency level to QUORUM, unlike some other system quorum
        // in ES means replicas/2 + 1
        DeleteResponse response = deleteRequestBuilder.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE).setWaitForActiveShards(ActiveShardCount.ALL).execute().actionGet();
        if (response.getResult().equals(DocWriteResponse.Result.DELETED))
        {
            Log.log.trace("Index: " + indexName + ", Document: " + documentType + ", Delete successful: " + id);
            // send to synchronization service
            String syncLogIndexName = SearchUtils.getSyncLogIndexName(response.getIndex(), response.getType());
            if (config.isSync() && !config.getSyncExcludeSet().contains(response.getIndex()) && !"synclog".equals(response.getType()) && StringUtils.isNotEmpty(syncLogIndexName))
            {
                SyncData<Serializable> syncData = new SyncData<Serializable>(syncLogIndexName, response.getIndex(), response.getType(), SearchUtils.SYNC_OPERATION.DELETE, null, routingKey, response.getId(), DateUtils.getCurrentTimeInMillis(), null, "system");
                SyncService.getInstance().enqueue(syncData);
            }
        }
        else
        {
            Log.log.trace("Index: " + indexName + ", Document: " + documentType + ", Id not found for deletion: " + id);
        }
    }
    
    public Collection<String> searchIDsByQuery(final String[] indexNames, final String[] documentTypes, final QueryDTO query, final QueryBuilder queryBuilder) throws SearchException
    {
        List<String> result = new ArrayList<String>();

        Client client = getRemoteClient();

        QueryDTO tmpQuery = query;

        if (tmpQuery == null)
        {
            tmpQuery = new QueryDTO();
        }

        int start = tmpQuery.getStart() != -1 ? tmpQuery.getStart() : 0;

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        // comma separated list of fields
        String selectFields = StringUtils.isBlank(query.getSelectColumns()) ? "*" : query.getSelectColumns();
        String excludeFields = StringUtils.isBlank(query.getExcludeColumns()) ? "" : query.getExcludeColumns();
        if (StringUtils.isNotBlank(query.getExcludeColumns())  || StringUtils.isNotBlank(query.getSelectColumns()))
        {
            String[] selects = Arrays.asList(selectFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            String[] excludes = Arrays.asList(excludeFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            searchRequestBuilder.setFetchSource(selects, excludes);
        }

        // add the sorting information
//        addSortOrder(tmpQuery, searchRequestBuilder);

        searchRequestBuilder.setFrom(start);
        // this adds some overhead that's why it's in trace
        if (Log.log.isTraceEnabled())
        {
            searchRequestBuilder.setExplain(true);
        }

        if (queryBuilder != null)
        {
            searchRequestBuilder.setQuery(queryBuilder);
        }
        
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(searchRequestBuilder.toString());
        }

        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(resp.toString());
        }

        long total = resp.getHits().getTotalHits();
        int count = 0;
        while (true)
        {
            for (SearchHit hit : resp.getHits())
            {
                try
                {
                    String docId = hit.getId();
                    result.add(docId);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
                    count++;
            }
                
            //assumes default limit of 10, if ES changes this this function will need to be updated
            if (count < 10 || result.size() >= total)
            {
                break;
            }
            else
            {
                count = 0;
                start = start + 10;
                searchRequestBuilder.setFrom(start);
                resp = searchRequestBuilder.execute().actionGet();
            }
        }
        
        return result;
    }

    void deleteByQueryBuilder(String indexName, String documentType, QueryBuilder query) throws SearchException
    {
    	Collection<String> ids = searchIDsByQuery(new String[] {indexName}, new String[] {documentType}, null, query);
    	
    	for (String id : ids)
    	{
    		deleteById(indexName, documentType, id);
    	}

    }
    
    void deleteDocumentByQuery(SyncData<Serializable> syncData)
    {
        boolean log = false;
        try 
        {
        	deleteByQueryBuilder(syncData.getIndexName(), syncData.getDocumentType(), syncData.getQueryBuilder());
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        Log.log.debug("Delete index name:" + syncData.getIndexName() + ", delete query: " + syncData.getQueryBuilder());
        
//        try
//        {
//            DeleteByQueryResponse response = remoteClient.prepareDeleteByQuery(syncData.getIndexName()).setTypes(syncData.getDocumentType()).setQuery(syncData.getQueryBuilder()).execute().actionGet();
//    
//            if (response == null || !response.status().equals(RestStatus.OK))
//            {
//                log = true;
//            }
//        }
//        catch (Exception e)
//        {
//            log = true;
//            Log.log.info(e.getMessage());
//        }

        //if something went something wrong during sync with remote cluster then log it.
        if(log)
        {
            try
            {
                // leave it to the RSSync
                String sysId = SearchUtils.getNewRowKey();
                syncData.setId(sysId);
                SyncLog syncLog = new SyncLog(syncData);
                searchService.index(syncData.getSyncLogIndexName(), SearchConstants.DOCUMENT_TYPE_SYNC_LOG, SearchConstants.SYS_ID, null, null, syncLog, false, false, ActiveShardCount.ONE, syncData.getUsername());
                Log.log.info("Something wrong, didn't get any response for a delete request by query from remote cluster. storing in SyncLog for RSSync to deal with it");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }        
        }
    }
    
    void deleteDocumentsInBulk(SyncData<Serializable> syncData) {
    	if (MapUtils.isNotEmpty(syncData.getBulkReq())) {
        	try {
        		APIFactory.getGenericIndexAPI().bulkDeleteByIds(syncData.getBulkReq(), true);
        	} catch (SearchException e) {
        		Log.log.error(String.format("Error %sin bulk deleting documents by ids from DR ES",
        									StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), e);
        		throw new RuntimeException(e);
        	}
        }
    	
    	// add syncLog logging
    }
}
