/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.resolve.search.SearchUtils;
import com.resolve.util.Log;

/**
 * This class represents a social target/stream like Forum, Document etc.
 */
@SuppressWarnings("serial")
public class SocialTarget implements Comparable<SocialTarget>, Serializable
{
    private static final long serialVersionUID = -8310006291507389860L;
	private String sysId;
    private String type;
    private String postType; //differentiate INBOX and BLOG
    private String name;
    private String namespace;
    private boolean isLocked = false;
    private String displayName;

    public TreeSet<String> readRoles = new TreeSet<String>();
    public TreeSet<String> postRoles = new TreeSet<String>();
    public TreeSet<String> editRoles = new TreeSet<String>();

    public SocialTarget()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public SocialTarget(String sysId)
    {
        this();
        this.sysId = sysId;
    }

    public SocialTarget(String sysId, String type, String postType, String name, String displayName, String namespace, String readRoles, String postRoles, String editRoles)
    {
        this(sysId);
        this.type = type;
        this.postType = postType;
        this.name = name;
        
        this.displayName = StringUtils.isNotBlank(displayName) ? this.displayName : name;
        
        this.displayName = name;
        this.namespace = namespace;
        this.readRoles = SearchUtils.parseRoles(readRoles);
        this.postRoles = SearchUtils.parseRoles(postRoles);
        this.editRoles = SearchUtils.parseRoles(editRoles);
    }

    public String getSysId()
    {
        return sysId;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getPostType()
    {
        return postType;
    }

    public void setPostType(String postType)
    {
        this.postType = postType;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public boolean isLocked()
    {
        return isLocked;
    }

    public void setLocked(boolean isLocked)
    {
        this.isLocked = isLocked;
    }

    @JsonIgnore
    public TreeSet<String> getReadRoles()
    {
        return readRoles;
    }

    @JsonIgnore
    public void setReadRoles(TreeSet<String> readRoles)
    {
        this.readRoles = readRoles;
    }

    @JsonIgnore
    public TreeSet<String> getPostRoles()
    {
        return postRoles;
    }

    @JsonIgnore
    public void setPostRoles(TreeSet<String> postRoles)
    {
        this.postRoles = postRoles;
    }

    @JsonIgnore
    public TreeSet<String> getEditRoles()
    {
        return editRoles;
    }

    @JsonIgnore
    public void setEditRoles(TreeSet<String> editRoles)
    {
        this.editRoles = editRoles;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sysId == null) ? 0 : sysId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        SocialTarget other = (SocialTarget) obj;
        if (sysId == null)
        {
            if (other.sysId != null) return false;
        }
        else if (!sysId.equals(other.sysId)) return false;
        return true;
    }

    @Override
    public int compareTo(SocialTarget otherSocialTarget)
    {
        if (this.equals(otherSocialTarget))
        {
            return 0;
        }
        else
        {
            if(this.sysId == null || otherSocialTarget == null || otherSocialTarget.sysId == null)
            {
                Log.log.debug("Do not try to compare with null value");
            }
            return this.sysId.compareTo(otherSocialTarget.sysId);
        }
    }

    @Override
    public String toString()
    {
        return "SocialTarget [sysId=" + sysId + ", type=" + type + ", postType=" + postType + ", name=" + name + ", namespace=" + namespace + ", displayName=" + displayName + "]";
    }
}
