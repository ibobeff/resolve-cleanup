/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.search;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.ListenableActionFuture;
import org.elasticsearch.action.admin.cluster.repositories.put.PutRepositoryResponse;
import org.elasticsearch.action.admin.cluster.snapshots.create.CreateSnapshotResponse;
import org.elasticsearch.action.admin.cluster.snapshots.get.GetSnapshotsResponse;
import org.elasticsearch.action.admin.cluster.snapshots.restore.RestoreSnapshotResponse;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequestBuilder;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.admin.indices.stats.IndicesStatsResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequestBuilder;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexAction;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.index.reindex.ReindexAction;
import org.elasticsearch.index.reindex.ReindexRequestBuilder;
import org.elasticsearch.index.reindex.remote.RemoteInfo;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.ExtendedBounds;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram.Bucket;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.stats.InternalStats;
import org.elasticsearch.search.aggregations.metrics.stats.StatsAggregationBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.snapshots.SnapshotInfo;

import com.resolve.search.model.BaseIndexModel;
import com.resolve.search.model.ExecutionSummary;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBAuditLog;
import com.resolve.search.model.PBNotes;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.model.WorksheetData;
import com.resolve.search.model.query.Query;
import com.resolve.services.ServiceJDBC;
import com.resolve.services.archive.BaseArchiveTable;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.BeanUtil;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.RESTUtils;
import com.resolve.util.StringUtils;

/**
 * This class represents the Resolve search service that is capable of indexing
 * and searching entire contents managed by RSSearch.
 * 
 * This class is intentionally made non public to force the consumers use only
 * various index and search APIs for their respective operations.
 */
public final class SearchService
{
	// ObjectMapper is threadsafe so it's fine to be a static final.
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final long NO_NODE_AVAILABLE_SLEEPTIME = 2000L;
    private static final String HIGHLIGHT_FIELDS = "highlightFields";
	private static final Map<String, Object> EXECUTION_CACHE = new HashMap<>();
	private static final List<String> CACHED_INDICES = Arrays.asList(SearchConstants.DOCUMENT_TYPE_WORKSHEET,
			SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST);
    
    private final Client client;
    private final ConfigSearch config;
    private static boolean esCacheEnabled;

    public void reindex(String host, int port)
    {
    	Log.log.info("Migrating ES data starts...");
    	String url = "http://" + host + ":" + port + "/_cat/indices";
    	String indexList = null;
    	try 
    	{
    		indexList = RESTUtils.get(url, null, null);
    	}
    	catch (Exception ex)
    	{
    		Log.log.error(ex,ex);
    		throw new RuntimeException(ex);
    	}
    	
    	String indexLines[] = indexList.split("\\r?\\n");
    	List<String> oldIndexes = new ArrayList<String>();
    	for (String line : indexLines)
    	{	
    		String[] splited = line.split("\\s+");
    		if (StringUtils.isNotEmpty(splited[2]))
    		{
    			oldIndexes.add(splited[2]);
    		}
    	}
    	
    	for (String idxName : oldIndexes)
    	{
    		Log.log.info("Migrating data on index " + idxName + " from ES server: " + host + ", port: " + port);
    		try
    		{
	    		if (!SearchAdminAPI.isIndexExists(idxName))
	    		{
	    			createDynamicIndex(idxName);
	    		}
		    	ReindexRequestBuilder builder = ReindexAction.INSTANCE.newRequestBuilder(getClient()).source(new String[]{idxName}).destination(idxName);
		    	RemoteInfo ri = new RemoteInfo("http", host, port, new BytesArray("{\"match_all\":{}}"), null, null, new HashMap(), RemoteInfo.DEFAULT_SOCKET_TIMEOUT, RemoteInfo.DEFAULT_CONNECT_TIMEOUT );
		    	builder.setRemoteInfo(ri);
		    	builder.source().setScroll(new TimeValue(1200*1000));
		    	builder.get();
    		}
    		catch (Exception ex)
    		{
    			Log.log.error("Error migrating data from index " + idxName, ex);
    		}
    	}
    	
    	Log.log.info("Migrating ES data is done");
    }
        
    public boolean isDynamicIndexName(String idx)
    {
    	boolean result = false;
    	if (!SearchConstants.INDEX_NAME_WORKSHEET_DATA.equals(idx) && idx.startsWith(SearchConstants.INDEX_NAME_WORKSHEET))
    	{
    		result = true;
    	}
    	else if (idx.startsWith(SearchConstants.INDEX_NAME_PROCESSREQUEST))
    	{
    		result = true;
    	}
    	else if (idx.startsWith(SearchConstants.INDEX_NAME_TASKRESULT))
    	{
    		result = true;
    	}
    	else if (idx.startsWith(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY))
    	{
    		result = true;
    	}
    	
    	return result;
    }
    
    public void createDynamicIndex(String idx)
    {
    	Log.log.info("Prepare to create index: " + idx );
    	String mappingFile = null;
    	String aliasName = null;
    	String docType = null;
    	if (!SearchConstants.INDEX_NAME_WORKSHEET_DATA.equals(idx) && idx.startsWith(SearchConstants.INDEX_NAME_WORKSHEET))
    	{
    		mappingFile = "worksheets_mappings.json";
    		aliasName = SearchConstants.INDEX_NAME_WORKSHEET_ALIAS;
    		docType = SearchConstants.DOCUMENT_TYPE_WORKSHEET; 
    	}
    	else if (idx.startsWith(SearchConstants.INDEX_NAME_PROCESSREQUEST))
    	{
    		mappingFile = "process_requests_mappings.json";
    		aliasName = SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS;
    		docType = SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST; 
    	}
    	else if (idx.startsWith(SearchConstants.INDEX_NAME_TASKRESULT))
    	{
    		mappingFile = "task_results_mappings.json";
    		aliasName = SearchConstants.INDEX_NAME_TASKRESULT_ALIAS;
    		docType = SearchConstants.DOCUMENT_TYPE_TASKRESULT; 
    	}
    	else if (idx.startsWith(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY))
    	{
    		mappingFile = "execution_summary_mappings.json";
    		aliasName = SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS;
    		docType = SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY; 
    	}
    	
        Map<String, IndicesAliasesRequestBuilder> indexAliases = new HashMap<String, IndicesAliasesRequestBuilder>();
        List<IndexConfiguration> defaultIndexConfigurations = new ArrayList<IndexConfiguration>();
        // some common clusterwide settings like shards/replica etc.
        String commonSettingsJson = Server.getInstance().readJson("common_settings.json");
    
        //index alias
        Map<String, String> mappings = new HashMap<String, String>();
        mappings.put(docType, Server.getInstance().readJson(mappingFile));
        defaultIndexConfigurations.add(new IndexConfiguration(idx, commonSettingsJson, mappings));

        IndicesAliasesRequestBuilder idxAlias = client.admin().indices().prepareAliases();
        idxAlias.addAlias(idx, aliasName);
        Log.log.info("Reindex create new index " + idx);
        // call the common method to create indices.
        indexAliases.put(aliasName, idxAlias);
        try 
        {
        	Server.getInstance().createIndex(defaultIndexConfigurations, indexAliases);
        }
        catch (Exception ex)
        {
        	Log.log.error(ex,ex);
        	throw new RuntimeException(ex);
        }
    }
    
    SearchService()
    {
        if (SearchAdminAPI.getClient() == null || SearchAdminAPI.getSearchConfig() == null)
        {
            throw new IllegalStateException("Search service is not initialized");
        }
        this.client = SearchAdminAPI.getClient();
        this.config = SearchAdminAPI.getSearchConfig();

        MAPPER.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public Client getClient()
    {
        return client;
    }

    void index(String indexName, String documentType, String idField, String createdOnField, Collection<? extends Serializable> indexModels, boolean doRefresh, boolean dynamicIndex, String username) throws SearchException
    {
    	long startTime = System.currentTimeMillis();
        if (indexModels != null)
        {
            if (indexModels.size() == 1)
            {
                // no need of bulk processing, but since it came through the
                // bulk processor keep the write consistency to ONE
                index(indexName, documentType, idField, createdOnField, indexModels.iterator().next(), doRefresh, dynamicIndex, ActiveShardCount.ONE, username);
            }
            else
            {
                BulkRequestBuilder bulkRequest = getClient().prepareBulk();

                List<SyncData<Serializable>> syncDataList = null;
                String syncLogIndexName = SearchUtils.getSyncLogIndexName(indexName, documentType);
                if (config.isSync() && !config.getSyncExcludeSet().contains(indexName) && !"synclog".equals(documentType) && StringUtils.isNotEmpty(syncLogIndexName))
                {
                    syncDataList = new ArrayList<SyncData<Serializable>>();
                }

                for (Serializable indexModel : indexModels)
                {
                    IndexRequestBuilder indexRequestBuilder = getIndexRequestBuilder(indexName, documentType, idField, null, createdOnField, indexModel, doRefresh, dynamicIndex, username);
                    bulkRequest.add(indexRequestBuilder);

                    // send to synchronization service
                    if (syncDataList != null)
                    {
                        try
                        {
                            String sysId = BeanUtil.getStringProperty(indexModel, idField);
                            SyncData<Serializable> syncData = new SyncData<Serializable>(syncLogIndexName, indexName, documentType, SearchUtils.SYNC_OPERATION.INDEX, null, null, sysId, DateUtils.GetUTCDateLong(), indexModel, username);
                            syncDataList.add(syncData);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage(), e);
                        }
                    }
                }

                int count = 0;
                while (count < config.getIndexretries())
                {
                    try
                    {
                        // during bulk processing it's a good idea to have
                        // consistency level to just ONE so it is fast.
                        BulkResponse bulkResponse = bulkRequest.setWaitForActiveShards(ActiveShardCount.ONE).execute().actionGet();
                        if (bulkResponse.hasFailures())
                        {
                            for (BulkItemResponse item : bulkResponse)
                            {
                                if (item.isFailed())
                                {
                                    Log.log.warn("Indexing failed for id :" + item.getId() + ", error : " + item.getFailureMessage());
                                }
                            }
                        }

                        // send to synchronization service
                        if (syncDataList != null && syncDataList.size() > 0)
                        {
                            for (SyncData<Serializable> syncData : syncDataList)
                            {
                                SyncService.getInstance().enqueue(syncData);
                            }
                        }

                        // no problem, get out
                        break;
                    }
                    catch (NoNodeAvailableException e)
                    {
                        // it may be a network hickup, we're retrying even if
                        // the client and server are on the same box,
                        // it just means that they use the loopback interface,
                        // but its still network/sockets so it may still fail.
                        Log.log.warn("RSSearch node not available, retrying. There is no way to recover from this until the node becomes available.");
                        Log.log.warn(e.getMessage(), e);
                        Log.log.debug("RSSearch node recovery attempt: " + count + 1);
                        try
                        {
                            Thread.sleep(NO_NODE_AVAILABLE_SLEEPTIME);
                        }
                        catch (InterruptedException e1)
                        {
                            // ignore
                        }
                    }
                    count++;
                }
            }
        }
    }

    /**
     * Indexes child document like social comment, wiki attachment.
     * 
     * @param indexName
     * @param documentType
     * @param parentIdField
     * @param idField
     * @param createdOnField
     * @param indexModels
     * @param username
     * @param isRefresh
     * @return
     * @throws SearchException
     */
    List<String> indexChildren(String indexName, String documentType, String parentIdField, String idField, String createdOnField, Collection<? extends Serializable> indexModels, String username, boolean isRefresh) throws SearchException
    {
        List<String> result = new ArrayList<String>();

        if (indexModels != null)
        {
            BulkRequestBuilder bulkRequest = getClient().prepareBulk();

            List<SyncData<Serializable>> syncDataList = null;
            String syncLogIndexName = SearchUtils.getSyncLogIndexName(indexName, documentType);
            if (config.isSync() && !config.getSyncExcludeSet().contains(indexName) && !"synclog".equals(documentType) && StringUtils.isNotEmpty(syncLogIndexName))
            {
                syncDataList = new ArrayList<SyncData<Serializable>>();
            }

            for (Serializable indexModel : indexModels)
            {
                IndexRequestBuilder indexRequestBuilder = getIndexRequestBuilder(indexName, documentType, idField, null, createdOnField, indexModel, isRefresh, false, username);
                String parentId = null;
                if (StringUtils.isNotBlank(parentIdField))
                {
                    try
                    {
                        parentId = BeanUtil.getStringProperty(indexModel, parentIdField);
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                        throw new SearchException(e.getMessage(), e);
                    }
                    indexRequestBuilder.setParent(parentId);
                }
                bulkRequest.add(indexRequestBuilder);

                // send to synchronization service
                if (syncDataList != null)
                {
                    try
                    {
                        String sysId = BeanUtil.getStringProperty(indexModel, idField);
                        SyncData<Serializable> syncData = new SyncData<Serializable>(syncLogIndexName, indexName, documentType, SearchUtils.SYNC_OPERATION.INDEX, parentId, null, sysId, DateUtils.getCurrentTimeInMillis(), indexModel, username);
                        syncDataList.add(syncData);
                    }
                    catch (Exception e)
                    {
                        Log.log.warn(e.getMessage(), e);
                    }
                }
            }

            int count = 0;
            while (count < config.getIndexretries())
            {
                try
                {
                    BulkResponse bulkResponse = bulkRequest.execute().actionGet();
                    if (bulkResponse.hasFailures())
                    {
                        for (BulkItemResponse item : bulkResponse)
                        {
                            if (item.isFailed())
                            {
                                Log.log.warn("Indexing failed for id :" + item.getId() + ", error : " + item.getFailureMessage());
                            }
                        }
                    }
                    else
                    {
                        for (BulkItemResponse item : bulkResponse)
                        {
                            result.add(item.getId());
                        }

                        // send to synchronization service
                        if (config.isSync() && !config.getSyncExcludeSet().contains(indexName))
                        {
                            if (syncDataList != null && syncDataList.size() > 0)
                            {
                                for (SyncData<Serializable> syncData : syncDataList)
                                {
                                    SyncService.getInstance().enqueue(syncData);
                                }
                            }
                        }
                    }
                }
                catch (NoNodeAvailableException e)
                {
                    // it may be a network hickup, we're retrying
                    // even if the client and server are on the same box, it
                    // just means that they use the loopback interface, but its
                    // still network/sockets so it may still fail.
                    Log.log.warn("RSSearch node not available, retrying. There is no way to recover from this until the node becomes available.");
                    Log.log.warn(e.getMessage(), e);
                    Log.log.debug("RSSearch node recovery attempt: " + count + 1);
                    try
                    {
                        Thread.sleep(NO_NODE_AVAILABLE_SLEEPTIME);
                    }
                    catch (InterruptedException e1)
                    {
                        // ignore
                    }
                }
                count++;
            }
        }
        return result;
    }

    String index(String indexName, String documentType, String idField, String createdOnField, Serializable indexModel, boolean doRefresh, boolean isDynamicIndex, String username) throws SearchException
    {
        return index(indexName, documentType, idField, createdOnField, indexModel, doRefresh, isDynamicIndex, ActiveShardCount.DEFAULT, username);
    }
    
    String index(String indexName, String documentType, String idField, String routingKeyField, String createdOnField, Serializable indexModel, boolean doRefresh, boolean isDynamicIndex, String username) throws SearchException
    {
        return index(indexName, documentType, idField, routingKeyField, createdOnField, indexModel, doRefresh, isDynamicIndex, ActiveShardCount.DEFAULT, username);
    }

    String index(String indexName, String documentType, String idField, String createdOnField, Serializable indexModel, boolean doRefresh, boolean isDynamicIndex, ActiveShardCount writeConsistencyLevel, String username) throws SearchException
    {
        return index(indexName, documentType, idField,  null, createdOnField, indexModel, doRefresh, isDynamicIndex, writeConsistencyLevel, username);
    }
    
    String index(String indexName, String documentType, String idField, String routingKeyField, String createdOnField, Serializable indexModel, boolean doRefresh, boolean isDynamicIndex, ActiveShardCount writeConsistencyLevel, String username) throws SearchException
    {
        String result = null;
        if (indexModel != null)
        {
            String tmpIndexName = indexName; 
            IndexRequestBuilder indexRequestBuilder = getIndexRequestBuilder(tmpIndexName, documentType, idField, routingKeyField, createdOnField, indexModel, doRefresh, isDynamicIndex, username);
            int count = 0;
            while (count < config.getIndexretries())
            {
                try
                {
                	String sysId = null;
                	if (isEsCacheEnabled() && CACHED_INDICES.contains(documentType)) {
	                    ListenableActionFuture<IndexResponse> response = indexRequestBuilder.setWaitForActiveShards(writeConsistencyLevel).execute();
	                    if (response == null)
	                    {
	                        throw new SearchException("Something went wrong, didn't get any response for the index");
	                    }
	                    sysId = ((BaseIndexModel)indexModel).getSysId();
	                    
						// cache indexed document
						String key = documentType + sysId;
						EXECUTION_CACHE.put(key, indexModel);
					} else {
						IndexResponse response = indexRequestBuilder.setWaitForActiveShards(writeConsistencyLevel).execute().actionGet();
						sysId = response.getId();
					}
                	
                	// send to synchronization service
                	if (config.isSync() && !config.getSyncExcludeSet().contains(tmpIndexName) && !"synclog".equals(documentType))
                	{
                		String routingKey = null;
                		if (StringUtils.isNotBlank(routingKeyField))
                		{
                			routingKey = BeanUtil.getStringProperty(indexModel, routingKeyField);
                		}
                		
                		String dynamicIndex = tmpIndexName;
                		if (isDynamicIndex) {
                			dynamicIndex = createDynamicIndex(tmpIndexName, createdOnField, indexModel);
        				}
                		SyncData<Serializable> syncData = new SyncData<Serializable>(SearchUtils.getSyncLogIndexName(tmpIndexName, documentType), dynamicIndex, documentType, SearchUtils.SYNC_OPERATION.INDEX, null, routingKey, sysId, DateUtils.getCurrentTimeInMillis(), indexModel, username);
                		SyncService.getInstance().enqueue(syncData);
                	}
                	// everything fine, get out of here.

                	break;
                    
                }
                catch (IndexNotFoundException e)
                {
                    if(isDynamicIndex)
                    {
                        //this is a very very special situation between 5.2 and previous releases
                        //when there were monthly indices for worksheet related data. There might
                        //have been some old process stored along with the event state and after
                        //upgrade/update to 5.2 it woke up and tried to update it's status or something.
                        try
                        {
                            Long sysCreatedOn = BeanUtil.getLongProperty(indexModel, createdOnField);
                            if(sysCreatedOn != null)
                            {
                                Log.log.debug("Index " + tmpIndexName + " is missing, trying monthly.");
                                //this may be some old month based index. this happens in an upgraded
                                //environment for events that were sleeping (e.g., CR).
                                int monthOfYear = DateUtils.getMonthOfYear(sysCreatedOn);
                                tmpIndexName = indexName + "_" + monthOfYear;
                                Log.log.debug("Trying monthly index generated from " + createdOnField + " field " + tmpIndexName);
                                //this time we do not want the method to generate index name dynamically that's why dynamic index flag is false.
                                indexRequestBuilder = getIndexRequestBuilder(tmpIndexName, documentType, idField, routingKeyField, createdOnField, indexModel, doRefresh, false, username);
                                //limit retries as the index might have been physically removed.
                                count++; 
                                continue;
                            }
                        }
                        catch (Exception e1)
                        {
                            Log.log.error(e1.getMessage(), e1);
                        }
                    }
                    else
                    {
                        //for non dynamic index if this happen then it is a major problem and need to investigate ES.
                        Log.log.error("Something terrible happened because " + tmpIndexName + " index is not available. Was it manually deleted?", e);
                    }
                }
                catch (NoNodeAvailableException e)
                {
                    // it may be a network hickup, we're retrying
                    // even if the client and server are on the same box, it
                    // just means that they use the loopback interface, but its
                    // still network/sockets so it may still fail.
                    Log.log.warn("RSSearch node not available, retrying. There is no way to recover from this until the node becomes available.");
                    Log.log.warn(e.getMessage(), e);
                    Log.log.debug("RSSearch node recovery attempt: " + count + 1);
                    try
                    {
                        Thread.sleep(NO_NODE_AVAILABLE_SLEEPTIME);
                    }
                    catch (InterruptedException e1)
                    {
                        // ignore
                    }
                } catch (IllegalAccessException e) {
					Log.log.error(e.getMessage(), e);
				} catch (InvocationTargetException e) {
					Log.log.error(e.getMessage(), e);
				} catch (NoSuchMethodException e) {
					Log.log.error(e.getMessage(), e);
				}
                count++;
            }
        }
        
        return result;
    }

    private IndexRequestBuilder getIndexRequestBuilder(final String indexName, final String documentType, final String idField, final String routingKeyField, final String createdOnField, final Serializable indexModel, final boolean doRefresh, final boolean dynamicIndex, final String username) throws SearchException
    {
        IndexRequestBuilder indexRequestBuilder = null;

        String sysId = null;
        if (indexModel != null)
        {
            try
            {

                // some caller may provide its own ID to be used (e.g.,
                // wrksheets, processrequests etc.)
                if (StringUtils.isNotBlank(idField))
                {
                    try
                    {
                        sysId = BeanUtil.getStringProperty(indexModel, idField);
                    }
                    catch(java.lang.NoSuchMethodException ex)
                    {
                        //ignore, metric data don't have "sysId" field
                    }
                }
                if (StringUtils.isBlank(sysId))
                {
                    sysId = SearchUtils.getNewRowKey();
                }
                // if this is a index based on the dynamic rule (e.g., daily)
                // then generate the index name
                // from its sysCreatedOn value
                String tmpIndexName = indexName;
				if (dynamicIndex) {
					tmpIndexName = createDynamicIndex(tmpIndexName, createdOnField, indexModel);
				}

                indexRequestBuilder = client.prepareIndex(tmpIndexName, documentType, sysId);

                if (StringUtils.isNotBlank(routingKeyField))
                {
                    // some objects may declare routing key itself.
                    String routingKey = BeanUtil.getStringProperty(indexModel, routingKeyField);
                    if (StringUtils.isNotBlank(routingKey))
                    {
                        indexRequestBuilder.setRouting(routingKey);
                    }
                }

                String json = SearchUtils.createJson(indexModel);
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace("JSON to be indexed: " + json);
                }

//                indexRequestBuilder.setSource(XContentType.JSON, json);
                indexRequestBuilder.setSource(json, XContentType.JSON);
                if (doRefresh)
                {
                	indexRequestBuilder.setRefreshPolicy(RefreshPolicy.IMMEDIATE);
                }
//                indexRequestBuilder.setRefresh(doRefresh);
                // adding a timeout from the configuration
                indexRequestBuilder.setTimeout(TimeValue.timeValueSeconds(config.getIndextimeout()));
            }
            catch (Exception e)
            {
                throw new SearchException(e.getMessage(), e);
            }
        }
        return indexRequestBuilder;
    }

	private String createDynamicIndex(final String indexName, final String createdOnField,
			final Serializable indexModel) throws IllegalAccessException, InvocationTargetException {
		Long sysCreatedOn = null;
		try {
			sysCreatedOn = BeanUtil.getLongProperty(indexModel, createdOnField);
		} catch (java.lang.NoSuchMethodException ex) {
			// ignore
		}

		if (sysCreatedOn == null) {
			sysCreatedOn = DateUtils.getCurrentTimeInMillis();
		}
		// due to data problem in the other component, created on
		// may be wrong
		// in that case use the current date.
		// int monthOfYear = DateUtils.getMonthOfYear(sysCreatedOn);
		// tmpIndexName = tmpIndexName + "_" + monthOfYear;
		return SearchUtils.getDynamicIndexName(indexName, DateUtils.getISODate(sysCreatedOn));
	}
    
    <T> T findById(Class<T> clazz, String indexName, String documentType, String id, String[] returnFields) throws Exception
    {
        return findById(clazz, indexName, documentType, id, null, returnFields);
    }

    <T> T findById(Class<T> clazz, String indexName, String documentType, String id, String routingKey, String[] returnFields) throws Exception
    {
        Client client = getClient();

        // Get document
        GetRequestBuilder getRequestBuilder = client.prepareGet(indexName, documentType, id);
        if (StringUtils.isNotBlank(routingKey))
        {
            getRequestBuilder.setRouting(routingKey);
        }
        getRequestBuilder.setStoredFields(returnFields);
        GetResponse response = getRequestBuilder.execute().actionGet();
        T obj = MAPPER.readValue(response.getSourceAsString(), clazz);
        return obj;
    }

    <T> T findById(String indexName, String documentType, Class<T> clazz, String id) throws SearchException
    {
        return findById(indexName, documentType, clazz, id, null, null);
    }

    <T> T findById(String indexName, String documentType, Class<T> clazz, String id, String routingKey) throws SearchException
    {
        return findById(indexName, documentType, clazz, id, null, routingKey);
    }

    <T> T findById(String indexName, String documentType, Class<T> clazz, String id, String parentId, String routingKey) throws SearchException
    {
        return findById(indexName, documentType, clazz, id, parentId, routingKey, false);
    }

    @SuppressWarnings("unchecked")
	<T> T findById (String indexName, String documentType, Class<T> clazz, String id, String parentId, String routingKey, 
    				boolean fromPrimary, boolean ignoreESInMemCache) throws SearchException {
    	String key = null;
        T obj = null;
        
        if (!ignoreESInMemCache && isEsCacheEnabled() && CACHED_INDICES.contains(documentType)) {
        	key = documentType + id;
        	obj = (T) EXECUTION_CACHE.get(key);
        }
        
        if (obj == null) {
	        
			try {
	            Client client = getClient();
	
	            // Get document
	            GetRequestBuilder getRequestBuilder = client.prepareGet(indexName, documentType, id);
	            
	            if (StringUtils.isNotBlank(parentId)) {
	                getRequestBuilder.setParent(parentId);
	            }
	            
	            if (StringUtils.isNotBlank(routingKey)) {
	                getRequestBuilder.setRouting(routingKey);
	            }
	            
	            if (fromPrimary) {
	                getRequestBuilder.setPreference("_primary");
	            }
	            
	            GetResponse response = getRequestBuilder.execute().actionGet();
	
	            if (response.isExists()) {
	                obj = MAPPER.readValue(response.getSourceAsString(), clazz);
	            } else {
	                if (Log.log.isTraceEnabled()) {
	                    Log.log.trace(String.format("Data not found for the ID: %s, index name: %s, document type: %s", 
	                    							id, indexName, documentType));
	                    Log.log.trace(String.format("Response from ES: %s", response.getSourceAsString()));
	                }
	            }
	        } catch (IOException e) {
	            Log.log.error(e.getMessage(), e);
	            throw new SearchException(e.getMessage(), e);
	        }
        }
        
        if (!ignoreESInMemCache && isEsCacheEnabled() && obj != null && CACHED_INDICES.contains(documentType)) {
        	EXECUTION_CACHE.put(key, obj);
        }
        
        return obj;
    }
    
    <T> T findById(String indexName, String documentType, Class<T> clazz, String id, String parentId, String routingKey, 
    			   boolean fromPrimary) throws SearchException {
    	return findById(indexName, documentType, clazz, id, parentId, routingKey, fromPrimary, false);
    }

    public Map<String, Object> getById(String indexName, String documentType, String id, String parentId, String routingKey) throws SearchException
    {
    	long startTime = System.currentTimeMillis();
        Map<String, Object> result = null;

        // Get document
        GetRequestBuilder getRequestBuilder = client.prepareGet(indexName, documentType, id);
        if (StringUtils.isNotBlank(parentId))
        {
            getRequestBuilder.setParent(parentId);
        }
        if (StringUtils.isNotBlank(routingKey))
        {
            getRequestBuilder.setRouting(routingKey);
        }
        GetResponse response = getRequestBuilder.execute().actionGet();

        if (response.isExists())
        {
            result = response.getSource();
        }
        else
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Data not found for the ID: " + id + ", index name: " + indexName + ", document type: " + documentType);
                Log.log.trace("Response from ES: " + response.getSourceAsString());
            }
        }
        return result;
    }

    /**
     * This is another way to find a document by its ID. Used for indexes with
     * aliases (worksheet etc.)
     * 
     * @param indexName
     * @param documentType
     * @param clazz
     * @param id
     * @return
     * @throws SearchException
     */
    <T> T findByQuery(String indexName, String documentType, Class<T> clazz, String id) throws SearchException
    {
		String key = null;
        T obj = null;
        
        if (isEsCacheEnabled() && CACHED_INDICES.contains(documentType)) {
        	key = documentType + id;
        	obj = (T) EXECUTION_CACHE.get(key);
        }
        
        if (obj == null) {
        
        try
        {
            // Get document
            QueryBuilder query = QueryBuilders.termQuery(SearchConstants.SYS_ID, id);

            SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
            searchRequestBuilder.setIndices(indexName).setTypes(documentType);
            searchRequestBuilder.setFrom(0);
            searchRequestBuilder.setSize(1);
//            searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            searchRequestBuilder.setQuery(query);

            SearchResponse response = searchRequestBuilder.execute().actionGet();

            if (response.getHits().getTotalHits() == 1)
            {
                obj = MAPPER.readValue(response.getHits().getAt(0).getSourceAsString(), clazz);
            }
        }
        catch (JsonParseException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (JsonMappingException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        }

        if (isEsCacheEnabled() && obj != null && CACHED_INDICES.contains(documentType)) {
        	EXECUTION_CACHE.put(key, obj);
        }
        
        return obj;
    }

    <T> List<T> findByQuery(String indexName, String documentType, Class<T> clazz, List<String> ids) throws SearchException
    {
    	long startTime = System.currentTimeMillis();
        List<T> records = new ArrayList<T>();

        // Get document
        QueryBuilder query = QueryBuilders.termsQuery(SearchConstants.SYS_ID, ids);
        BoolQueryBuilder bqb = new BoolQueryBuilder();
        bqb = bqb.filter(query);
        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexName).setTypes(documentType);
        searchRequestBuilder.setFrom(0);
        searchRequestBuilder.setSize(ids.size());
        searchRequestBuilder.setQuery(bqb);

        SearchResponse response = searchRequestBuilder.execute().actionGet();

        if (response.getHits().getTotalHits() >= 0)
        {
            for (SearchHit hit : response.getHits())
            {
                try
                {
                    T obj = MAPPER.readValue(hit.getSourceAsString(), clazz);
                    records.add(obj);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
            }
        }
        return records;
    }

    <T> List<T> findByIds(String indexName, String documentType, Class<T> clazz, List<String> ids) throws SearchException
    {
        List<T> result = new ArrayList<T>();

        try
        {
            Client client = getClient();

            // Get document
            MultiGetRequestBuilder getRequestBuilder = client.prepareMultiGet(); // .prepareMultiGet()
                                                                                 // (indexName,
                                                                                 // documentType,
                                                                                 // ids);
            getRequestBuilder.add(indexName, documentType, ids);
            MultiGetResponse response = getRequestBuilder.execute().actionGet();

            if (response != null && response.getResponses() != null)
            {
                for (MultiGetItemResponse item : response.getResponses())
                {
                    result.add(MAPPER.readValue(item.getResponse().getSourceAsString(), clazz));
                }
            }
        }
        catch (JsonParseException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (JsonMappingException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }

    public <T> ResponseDTO<T> searchByQuery(final Class<T> clazz, final String[] indexNames, final String[] documentTypes, final QueryDTO query, final QueryBuilder queryBuilder, final StatsAggregationBuilder statsBuilder, final QueryBuilder[] filterBuilders, final String[] highlightedFields, final String username) throws SearchException
    {
        return searchByQuery(clazz, indexNames, documentTypes, query, queryBuilder, statsBuilder, filterBuilders, highlightedFields, username, false);
    }
    
    public <T> ResponseDTO<T> searchByQuery(final Class<T> clazz, final String[] indexNames, final String[] documentTypes, final QueryDTO query, final QueryBuilder queryBuilder, final StatsAggregationBuilder statsBuilder, final QueryBuilder[] filterBuilders, final String[] highlightedFields, final String username, boolean isCount) throws SearchException
    {
        ResponseDTO<T> result = new ResponseDTO<T>();

        List<T> records = new ArrayList<T>();

        Client client = getClient();

        QueryDTO tmpQuery = query;

        if (tmpQuery == null)
        {
            tmpQuery = new QueryDTO();
        }

        int limit = tmpQuery.getLimit();
        int start = tmpQuery.getStart() != -1 ? tmpQuery.getStart() : 0;

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        if (isCount)
        {
            searchRequestBuilder.setSize(0);
        }
        
        // comma separated list of fields
        String selectFields = StringUtils.isBlank(tmpQuery.getSelectColumns()) ? "*" : tmpQuery.getSelectColumns();
        String excludeFields = StringUtils.isBlank(tmpQuery.getExcludeColumns()) ? "" : tmpQuery.getExcludeColumns();
        if (StringUtils.isNotBlank(tmpQuery.getExcludeColumns())  || StringUtils.isNotBlank(tmpQuery.getSelectColumns()))
        {
            String[] selects = Arrays.asList(selectFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            String[] excludes = Arrays.asList(excludeFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            searchRequestBuilder.setFetchSource(selects, excludes);
        }

        addSortOrder(tmpQuery, searchRequestBuilder);

        searchRequestBuilder.setFrom(start);
        boolean limitQuery = false;
        if (limit > 0)
        {
            searchRequestBuilder.setSize(limit);
            limitQuery = true;
        }
        // this adds some overhead that's why it's in trace
        if (Log.log.isTraceEnabled())
        {
            searchRequestBuilder.setExplain(true);
        }

        if (statsBuilder != null)
        {
            searchRequestBuilder.addAggregation(statsBuilder);
        }
        if (queryBuilder != null)
        {
            searchRequestBuilder.setQuery(queryBuilder);
        }
        
        if (filterBuilders != null)
        {
        	BoolQueryBuilder bqb = new BoolQueryBuilder();
            int noOfFilterBuilders = 0;
            for (QueryBuilder filterBuilder : filterBuilders)
            {
                if (filterBuilder != null)
                {
                    bqb.filter(filterBuilder);
                    noOfFilterBuilders++;
                }
            }
            if (noOfFilterBuilders > 0)
            {
                searchRequestBuilder.setPostFilter(bqb);
            }
        }

        boolean hasHighlightedField = false;
        if (highlightedFields != null && highlightedFields.length > 0)
        {
            hasHighlightedField = true;
            HighlightBuilder hb = new HighlightBuilder();
            for (String highlightedField : highlightedFields)
            {
            	if ("*".equals(highlightedField) && (hb.requireFieldMatch() == null || hb.requireFieldMatch())) {
            		hb.field(highlightedField).requireFieldMatch(false).fragmentSize(200);
            	} else {
                	hb.field(highlightedField, 0, 0);
            	}
            }
            searchRequestBuilder.highlighter(hb);
        }

        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(searchRequestBuilder.toString());
        }

        //Sort the similar result based on sysUpdatedOn.
        if(!Constants.MACRO_TYPE.equals(tmpQuery.getOrderBy()))
        {                                
            searchRequestBuilder.addSort("_score", SortOrder.DESC);

        	if (!Arrays.asList(indexNames).contains(SearchConstants.INDEX_NAME_SECURITY_INCIDENT)) {
                FieldSortBuilder builder = new FieldSortBuilder("sysUpdatedOn");
                builder.unmappedType("long");
                builder.order(SortOrder.DESC);
                searchRequestBuilder.addSort(builder);
        	}
        }
        
        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(resp.toString());
        }
        
        result.setTotal(resp.getHits().getTotalHits());
        
        int count = 0;
        if (!isCount && result.getTotal() > 0)
        {    
            while (true)
            {
                for (SearchHit hit : ((Iterable<SearchHit>)resp.getHits()))
                {
                    T obj;
                    try
                    {
                        obj = MAPPER.readValue(hit.getSourceAsString(), clazz);
                        if (hasHighlightedField)
                        {
                            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                            if (hasHighlightFields(obj)) {
    							Map<String, String> highlightValues = highlightFields.entrySet().stream()
    									.filter(e -> e.getKey().indexOf(".keyword") < 0)
    									.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().fragments()[0].string()));
                            	BeanUtil.setProperty(obj, HIGHLIGHT_FIELDS, highlightValues);
                            }
                            for (String fieldName : highlightFields.keySet())
                            {
                                HighlightField field = highlightFields.get(fieldName);
                                String highlightedValue = field.fragments()[0].string();
                                BeanUtil.setProperty(obj, fieldName, highlightedValue);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                        throw new SearchException(e.getMessage(), e);
                    }
                    records.add(obj);
                    count++;
                }
                
                //assumes default limit of 10, if ES changes this this function will need to be updated
                if (limitQuery || count < 10 || records.size() >= result.getTotal())
                {
                    break;
                }
                else
                {
                    count = 0;
                    start = start + 10;
                    searchRequestBuilder.setFrom(start);
                    resp = searchRequestBuilder.execute().actionGet();
                }
            }
        }
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("Number of records is %d", records.size()));
        }
        result.setRecords(records);
        return result;
    }
    
    private <T> boolean hasHighlightFields(T obj) {
    	Class<?> clazz = obj.getClass();
    	try {
			clazz.getDeclaredField(HIGHLIGHT_FIELDS);
			return true;
		} catch (NoSuchFieldException | SecurityException e) {
			return false;
		}
    }

    @Deprecated
    public <T> ResponseDTO<T> searchByFilteredQuery(final Class<T> clazz, final String[] indexNames, final String[] documentTypes, final QueryDTO query, final QueryBuilder queryBuilder, final StatsAggregationBuilder statsBuilder, final QueryBuilder[] filterBuilders, final String[] highlightedFields, final String username) throws SearchException
    {
    	long startTime = System.currentTimeMillis();
        ResponseDTO<T> result = new ResponseDTO<T>();

        List<T> records = new ArrayList<T>();

        Client client = getClient();

        QueryDTO tmpQuery = query;

        if (tmpQuery == null)
        {
            tmpQuery = new QueryDTO();
        }

        // this is very interesting in ElasticSearch,
        // size is per shard, so if you want the limit to be 50 then 5 shards by
        // default and 5 items, so at a size of 1 I'd always get 5 back.
        // limit = config.getShards() % limit;
        int limit = tmpQuery.getLimit();
        int start = tmpQuery.getStart() != -1 ? tmpQuery.getStart() : 0;

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        // comma separated list of fields
        String selectFields = StringUtils.isBlank(query.getSelectColumns()) ? "*" : query.getSelectColumns();
        String excludeFields = StringUtils.isBlank(query.getExcludeColumns()) ? "" : query.getExcludeColumns();
        if (StringUtils.isNotBlank(query.getExcludeColumns()))
        {
            searchRequestBuilder.setFetchSource(selectFields, excludeFields);
        }

        // add the sorting information
        addSortOrder(tmpQuery, searchRequestBuilder);

        searchRequestBuilder.setFrom(start);
//        searchRequestBuilder.setSize(limit);
        boolean limitQuery = false;
        if (limit > 0)
        {
            searchRequestBuilder.setSize(limit);
            limitQuery = true;
        }
        // this adds some overhead that's why it's in trace
        if (Log.log.isTraceEnabled())
        {
            searchRequestBuilder.setExplain(true);
        }

//        searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        if (statsBuilder != null)
        {
            searchRequestBuilder.addAggregation(statsBuilder);
        }
//        if (queryBuilder != null)
//        {
//            searchRequestBuilder.setQuery(queryBuilder);
//        }

        if (filterBuilders != null)
        {
        	BoolQueryBuilder bqb = new BoolQueryBuilder();
//            AndFilterBuilder andFilterBuilder = FilterBuilders.andFilter();
            int noOfFilterBuilders = 0;
            for (QueryBuilder filterBuilder : filterBuilders)
            {
                if (filterBuilder != null)
                {
                    bqb.filter(filterBuilder);
                    noOfFilterBuilders++;
                }
            }
            if (noOfFilterBuilders > 0)
            {
                searchRequestBuilder.setQuery(bqb);
            }
        }
        
//        FilterBuilder filter = null;
//        if (filterBuilders!=null && filterBuilders.length>0)
//        {
//            filter = FilterBuilders.andFilter(filterBuilders);
//        }
//        
//        QueryBuilder qb = queryBuilder;
//        if (filter!=null && queryBuilder!=null)
//        {
//            qb = new FilteredQueryBuilder(queryBuilder, filter);
//        }
//        if (qb!=null)
//        {
//            searchRequestBuilder.setQuery(qb);
//        }

        
        
        boolean hasHighlightedField = false;
        if (highlightedFields != null && highlightedFields.length > 0)
        {
            hasHighlightedField = true;
            for (String highlightedField : highlightedFields)
            {
            	HighlightBuilder hb = new HighlightBuilder();
            	hb.field(highlightedField, 0, 0);
//                searchRequestBuilder.addHighlightedField(highlightedField, 0, 0);
            	searchRequestBuilder.highlighter(hb);
            }
        }
        
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(searchRequestBuilder.toString());
        }

//        //Sort the similar result based on sysUpdatedOn.
//        org.elasticsearch.search.sort.FieldSortBuilder builder = new org.elasticsearch.search.sort.FieldSortBuilder("sysUpdatedOn");
//        builder.unmappedType("long");
//        builder.order(SortOrder.DESC);
//        searchRequestBuilder.addSort("_score", SortOrder.DESC).addSort(builder);
        
        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(resp.toString());
        }

        result.setTotal(resp.getHits().getTotalHits());
        int count = 0;
        while (true)
        {
            for (SearchHit hit : resp.getHits())
            {
                T obj;
                try
                {
                    obj = MAPPER.readValue(hit.getSourceAsString(), clazz);
                    if (hasHighlightedField)
                    {
                        Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                        for (String fieldName : highlightFields.keySet())
                        {
                            HighlightField field = highlightFields.get(fieldName);
                            String highlightedValue = field.fragments()[0].string();
                            BeanUtil.setProperty(obj, fieldName, highlightedValue);
                            // Log.log.trace(highlightedValue);
                            // hit.getSource().put(fieldName, highlightedValue);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
                records.add(obj);
                    count++;
            }
                
            //assumes default limit of 10, if ES changes this this function will need to be updated
            if (limitQuery || count < 10 || records.size() >= result.getTotal())
            {
                break;
            }
            else
            {
                count = 0;
                start = start + 10;
                searchRequestBuilder.setFrom(start);
                resp = searchRequestBuilder.execute().actionGet();
            }
        }

        result.setRecords(records);
        
        return result;
    }

    <T> long getCountByQueryBuilder(String[] indexNames, String[] documentTypes, QueryBuilder queryBuilder, QueryBuilder... filterBuilders) throws SearchException
    {
        long result = 0;

        Client client = getClient();

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        searchRequestBuilder.setFrom(0);
        searchRequestBuilder.setSize(1);

        // search type COUNT is the best in this because it doesn't fetch (no
        // hammering the poor network :) the document from the index but
        // simply gets the count based on the query.
//        searchRequestBuilder.setSearchType(SearchType.COUNT);
        searchRequestBuilder.setSize(0);
        if (queryBuilder != null)
        {
            searchRequestBuilder.setQuery(queryBuilder);
        }

        if (filterBuilders != null)
        {
        	BoolQueryBuilder bqb = new BoolQueryBuilder();
//            AndFilterBuilder andFilterBuilder = FilterBuilders.andFilter();
            int noOfFilterBuilders = 0;
            for (QueryBuilder filterBuilder : filterBuilders)
            {
                if (filterBuilder != null)
                {
                    bqb.filter(filterBuilder);
                    noOfFilterBuilders++;
                }
            }
            if (noOfFilterBuilders > 0)
            {
                searchRequestBuilder.setQuery(bqb);
            }
        }
        
//        if (filterBuilders != null && filterBuilders.length > 0)
//        {
//            AndFilterBuilder andFilterBuilder = FilterBuilders.andFilter();
//            for (FilterBuilder filterBuilder : filterBuilders)
//            {
//                if (filterBuilder != null)
//                {
//                    andFilterBuilder.add(filterBuilder);
//                }
//            }
//            searchRequestBuilder.setPostFilter(andFilterBuilder);
//        }

        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        if (!resp.isTimedOut())
        {
            result = resp.getHits().getTotalHits();
        }
        return result;
    }

    /**
     * Gets statistical data by query and Statistical aggregation. For data such
     * as number of likes etc.
     * 
     * The returned {@link Map} will have following keys:
     * 
     * count : Doc count total : Sum of values
     * 
     * @param indexName
     * @param documentType
     * @param queryBuilder
     * @param statsBuilder
     * @return a {@link Map} with result from the query. Results like "sum":10,
     *         "min":0 etc.
     * @throws Exception
     */
    <T> Map<String, Long> getStatistics(String[] indexName, String[] documentType, QueryBuilder queryBuilder, StatsAggregationBuilder statsBuilder, String username) throws SearchException
    {
        Map<String, Long> result = new HashMap<String, Long>();

        Client client = getClient();

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexName).setTypes(documentType);

        searchRequestBuilder.setFrom(0);
        searchRequestBuilder.setSize(1);

//        searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        searchRequestBuilder.setQuery(queryBuilder);
        searchRequestBuilder.addAggregation(statsBuilder);

        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        if (!resp.isTimedOut())
        {
            List<Aggregation> aggregations = resp.getAggregations().asList();
            if (aggregations != null && aggregations.size() > 0)
            {
                InternalStats agg = (InternalStats) resp.getAggregations().asList().get(0);
                result.put("count", agg.getCount());
                result.put("total", (long) agg.getSum());
            }
        }
        return result;
    }

    private void addSortOrder(QueryDTO query, SearchRequestBuilder searchRequestBuilder)
    {
        List<QuerySort> sortItems = query.getSortItems();
        // as of now it's guranteed from UI there will be only
        // one field that the user can select for sort.
        if (sortItems != null && sortItems.size() > 0)
        {
            SortOrder sortOrder = SortOrder.ASC;
            boolean asc = sortItems.get(0).getDirection() == QuerySort.SortOrder.ASC;
            if (!asc)
            {
                sortOrder = SortOrder.DESC;
            }
            String sortField = sortItems.get(0).getProperty();
            searchRequestBuilder.addSort(sortField, sortOrder);
        }
    }

    /**
     * Deletes data by its sysId.
     * 
     * @param indexName
     * @param documentType
     * @param id
     * @throws SearchException
     */
    public void deleteById(String indexName, String documentType, String id) throws SearchException
    {
    	if (documentType.equalsIgnoreCase(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT)) {
            deleteById(indexName, documentType, id, SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
    	} else {
            deleteById(indexName, documentType, id, null);
    	}

    }

    /**
     * Deletes data by its sysId and routing key.
     * 
     * @param indexName
     * @param documentType
     * @param id
     * @param routingKey
     * @throws SearchException
     */
    void deleteById(String indexName, String documentType, String id, String routingKey) throws SearchException
    {
        Client client = getClient();
        DeleteRequestBuilder deleteRequestBuilder = client.prepareDelete(indexName, documentType, id);
        if (StringUtils.isNotBlank(routingKey))
        {
            deleteRequestBuilder.setRouting(routingKey);
        }

        // set the consistency level to QUORUM, unlike some other system quorum
        // in ES means replicas/2 + 1
        DeleteResponse response = deleteRequestBuilder.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE).setWaitForActiveShards(ActiveShardCount.ALL).execute().actionGet();
        if (response.getResult().equals(DocWriteResponse.Result.DELETED))
        {
            Log.log.trace("Index: " + indexName + ", Document: " + documentType + ", Delete successful: " + id);
            // send to synchronization service
            String syncLogIndexName = SearchUtils.getSyncLogIndexName(response.getIndex(), response.getType());
            if (config.isSync() && !config.getSyncExcludeSet().contains(response.getIndex()) && !"synclog".equals(response.getType()) && StringUtils.isNotEmpty(syncLogIndexName))
            {
                SyncData<Serializable> syncData = new SyncData<Serializable>(syncLogIndexName, response.getIndex(), response.getType(), SearchUtils.SYNC_OPERATION.DELETE, null, routingKey, response.getId(), DateUtils.getCurrentTimeInMillis(), null, "system");
                SyncService.getInstance().enqueue(syncData);
            }
        }
        else
        {
            Log.log.trace("Index: " + indexName + ", Document: " + documentType + ", Id not found for deletion: " + id);
        }
    }

    /**
     * Get last index time based on a date/time field.
     * 
     * @param indexName
     * @param documentType
     * @param timeStampedFieldName
     *            a field that's date/time (e.g., sys_created_by)
     * @return
     */
    long getLastIndexTime(String indexName, String documentType, String timeStampedFieldName) throws SearchException
    {
        long result = 0;

        try
        {
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();

            SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
            searchRequestBuilder.setIndices(indexName).setTypes(documentType);
            searchRequestBuilder.addStoredField(timeStampedFieldName);
            searchRequestBuilder.addSort(timeStampedFieldName, SortOrder.DESC);
            // we just need one record with the highest value on the field
            searchRequestBuilder.setFrom(0).setSize(1);
            searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            searchRequestBuilder.setQuery(queryBuilder);

            SearchResponse response = searchRequestBuilder.execute().actionGet();
            if (response != null && response.getHits() != null && response.getHits().getTotalHits() >= 0)
            {
                SearchHit[] searchHits = response.getHits().getHits();
                if (searchHits != null && searchHits.length > 0)
                {
                    SearchHit searchHit = searchHits[0];
                    SearchHitField searchHitField = searchHit.getField(timeStampedFieldName);
                    if (searchHitField!=null)
                    {
                    	result = (Long) searchHitField.getValue();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Get total number of documents in an index.
     * 
     * @param indexName
     * @param documentType
     * @return
     */
    long getTotalCount(String indexName, String documentType)
    {
        SearchResponse response = new SearchRequestBuilder(getClient(), SearchAction.INSTANCE).setSize(0).setIndices(indexName).setTypes(documentType).setQuery(QueryBuilders.matchAllQuery()).get();
        return response.getHits().totalHits;
    }

    /**
     * This method deletes data by query.
     * 
     * One use case is from UI's point of view where user selects certain
     * criteria and passes the QueryDTO to be executed for deletion.
     * 
     * @param indexName
     * @param documentType
     * @param query
     * @throws SearchException
     */
    Collection<String> deleteByQueryBuilder(String indexName, String documentType, QueryBuilder query, String username) throws SearchException
    {
        return deleteByQueryBuilder(indexName, documentType, query, false, username);
    }
    
    /*
     * HP TODO Add scroll to support sysid search criteria matching more than 10000 records
     * 
     * Note: Archival does not use this method anymore. It uses bulkDeleteByIds to delete all documents by sys ids.
     */
    public Collection<Map<String, String>> searchIDsByQuery(final String[] indexNames, final String[] documentTypes, final QueryDTO query, final QueryBuilder queryBuilder, String username) throws SearchException
    {
    	long startTime = System.currentTimeMillis();
        List<Map<String,String>> result = new ArrayList<Map<String, String>>();

        Client client = getClient();

        QueryDTO tmpQuery = query;

        if (tmpQuery == null)
        {
            tmpQuery = new QueryDTO();
        }

        int limit = tmpQuery.getLimit();
        int start = tmpQuery.getStart() != -1 ? tmpQuery.getStart() : 0;

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        // comma separated list of fields
        String selectFields = StringUtils.isBlank(tmpQuery.getSelectColumns()) ? "*" : tmpQuery.getSelectColumns();
        String excludeFields = StringUtils.isBlank(tmpQuery.getExcludeColumns()) ? "" : tmpQuery.getExcludeColumns();
        if (StringUtils.isNotBlank(tmpQuery.getExcludeColumns())  || StringUtils.isNotBlank(tmpQuery.getSelectColumns()))
        {
            String[] selects = Arrays.asList(selectFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            String[] excludes = Arrays.asList(excludeFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            searchRequestBuilder.setFetchSource(selects, excludes);
        }

        // add the sorting information
        addSortOrder(tmpQuery, searchRequestBuilder);

        searchRequestBuilder.setFrom(start);
        
        if (limit > 0)
        {
            searchRequestBuilder.setSize(limit);
        }
        
        // this adds some overhead that's why it's in trace
        if (Log.log.isTraceEnabled())
        {
            searchRequestBuilder.setExplain(true);
        }

        if (queryBuilder != null)
        {
            searchRequestBuilder.setQuery(queryBuilder);
        }
        
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(searchRequestBuilder.toString());
        }

        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(resp.toString());
        }

        /*
         *  Handle Org access by username for worksheet and worksheet related  
         *  objects like processrequest, taskresult, artifacts, notes etc.
         */

        boolean checkWorksheetOrgAccess = false;
        boolean hasNoOrgAccess = false;
        Set<String> aggrOrgIdsInHierarchy = new HashSet<String>();
        Map<String, String> indexNameToDocType = new HashMap<String, String>();
        
        if (documentTypes != null && indexNames.length >= 1 && documentTypes.length >= 1 &&
            indexNames.length == documentTypes.length &&
            CollectionUtils.containsAny(SearchConstants.WORKSHEET_ASSOCIATED_DOCUMENT_TYPES,
                                        Arrays.asList(documentTypes)))
        {
            checkWorksheetOrgAccess = true;
            hasNoOrgAccess = UserUtils.isNoOrgAccessible(username);
            
            UsersVO usersVO = UserUtils.getUser(username);
            
            if (usersVO != null)
            {
                aggrOrgIdsInHierarchy = usersVO.getAccessibleOrgIds();
            }
            
            for (int i = 0; i < indexNames.length; i++)
            {
                if (SearchConstants.WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.contains(documentTypes[i]))
                {
                    indexNameToDocType.put(indexNames[i].toLowerCase(), documentTypes[i].toLowerCase());
                }
            }
        }
                            
        long total = resp.getHits().getTotalHits();
        long processedCnt = 0l;
        int count = 0;
        while (true)
        {
            for (SearchHit hit : resp.getHits())
            {
                try
                {
                    String src = hit.getSourceAsString();
                    String docId = hit.getId();
                    String idxName = hit.getIndex();
                    
                    boolean add = true;
                    
                    if (checkWorksheetOrgAccess && indexNameToDocType.keySet().contains(idxName.toLowerCase()))
                    {
                        String docType = indexNameToDocType.get(idxName.toLowerCase());
                        String worksheetOrgId = null;
                        Worksheet worksheet = null;
                        ResolveSecurityIncidentVO resolveSecurityIncidentVO = null;
                        SearchAPI<Worksheet> worksheetSearch = APIFactory.getWorksheetSearchAPI(Worksheet.class);
                        switch (docType)
                        {
                            case SearchConstants.DOCUMENT_TYPE_WORKSHEET:
                                worksheet = findById(idxName.toLowerCase(), docType, Worksheet.class, docId);
                                
                                if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                {
                                    worksheetOrgId = worksheet.getSysOrg();
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA:
                                WorksheetData worksheetData = findById(idxName.toLowerCase(), docType, WorksheetData.class, docId);
                                
                                if (worksheetData != null && StringUtils.isNotBlank(worksheetData.getWorksheetId()))
                                {
                                    worksheet = worksheetSearch.findDocumentById(worksheetData.getWorksheetId(), username);
                                    
                                    if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                    {
                                        worksheetOrgId = worksheet.getSysOrg();
                                    }
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY:
                                ExecutionSummary executionSummary = findById(idxName.toLowerCase(), docType, ExecutionSummary.class, docId);
                                
                                if (executionSummary != null && StringUtils.isNotBlank(executionSummary.getWorksheetId()))
                                {
                                    worksheet = worksheetSearch.findDocumentById(executionSummary.getWorksheetId(), username);
                                    
                                    if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                    {
                                        worksheetOrgId = worksheet.getSysOrg();
                                    }
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST:
                                ProcessRequest processRequest = findById(idxName.toLowerCase(), docType, ProcessRequest.class, docId);
                                
                                if (processRequest != null && StringUtils.isNotBlank(processRequest.getProblem()))
                                {
                                    worksheet = worksheetSearch.findDocumentById(processRequest.getProblem(), username);
                                    
                                    if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                    {
                                        worksheetOrgId = worksheet.getSysOrg();
                                    }
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_TASKRESULT:
                                TaskResult taskResult = findById(idxName.toLowerCase(), docType, TaskResult.class, docId);
                                
                                if (taskResult != null && StringUtils.isNotBlank(taskResult.getProblemId()))
                                {
                                    worksheet = worksheetSearch.findDocumentById(taskResult.getProblemId(), username);
                                    
                                    if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                    {
                                        worksheetOrgId = worksheet.getSysOrg();
                                    }
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_PB_NOTES:
                                PBNotes pbNotes = findById(idxName.toLowerCase(), docType, PBNotes.class, docId);
                                
                                if (pbNotes != null && StringUtils.isNotBlank(pbNotes.getWorksheetId()))
                                {
                                    worksheet = worksheetSearch.findDocumentById(pbNotes.getWorksheetId(), username);
                                    
                                    if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                    {
                                        worksheetOrgId = worksheet.getSysOrg();
                                    }
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS:
                                PBArtifacts pbAtrifacts = findById(idxName.toLowerCase(), docType, PBArtifacts.class, docId);
                                
                                if (pbAtrifacts != null && StringUtils.isNotBlank(pbAtrifacts.getWorksheetId()))
                                {
                                    worksheet = worksheetSearch.findDocumentById(pbAtrifacts.getWorksheetId(), username);
                                    
                                    if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                    {
                                        worksheetOrgId = worksheet.getSysOrg();
                                    }
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT:
                                PBAttachments pbAttachments = findById(idxName.toLowerCase(), docType, PBAttachments.class, docId);
                                
                                if (pbAttachments != null && StringUtils.isNotBlank(pbAttachments.getWorksheetId()))
                                {
                                    worksheet = worksheetSearch.findDocumentById(pbAttachments.getWorksheetId(), username);
                                    
                                    if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                    {
                                        worksheetOrgId = worksheet.getSysOrg();
                                    }
                                }
                                
                                break;
                                
                            case SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG:
                                PBAuditLog pbAuditLog = findById(idxName.toLowerCase(), docType, PBAuditLog.class, docId);
                                
                                if (pbAuditLog != null && StringUtils.isNotBlank(pbAuditLog.getIncidentId()))
                                {
                                    try
                                    {
                                        resolveSecurityIncidentVO = PlaybookUtils.getSecurityIncident(pbAuditLog.getIncidentId(), null, username);
                                        
                                        worksheet = worksheetSearch.findDocumentById(resolveSecurityIncidentVO.getProblemId(), username);
                                        if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                                        {
                                            worksheetOrgId = worksheet.getSysOrg();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        add = false;
                                    }
                                }
                                
                                break;
                                
                            default:
                                add = false;
                        }
                        
                        if (add)
                        {
                            if (StringUtils.isNotBlank(worksheetOrgId))
                            {
                                add = aggrOrgIdsInHierarchy.contains(worksheetOrgId);
                            }
                            else
                            {
                                add = hasNoOrgAccess;
                            }
                        }
                    }
                    
                    if (add)
                    {
                        Map<String,String> m = new HashMap<String,String>();
                        m.put(docId, idxName);
                        result.add(m);
                        //result.add(docId);
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
                
                count++;
                processedCnt++;
            }
                
            //assumes default limit of 10, if ES changes this this function will need to be updated
            if (count < 10 || /*result.size()*/processedCnt >= total)
            {
                break;
            }
            else
            {
                count = 0;
                start = start + 10;
                searchRequestBuilder.setFrom(start);
                resp = searchRequestBuilder.execute().actionGet();
            }
        }
        
        return result;
    }
    
    /**
     * This method deletes data by query.
     * 
     * One use case is from UI's point of view where user selects certain
     * criteria and passes the QueryDTO to be executed for deletion.
     * 
     * @param indexName
     * @param documentType
     * @param query
     * @throws SearchException
     */
    Collection<String> deleteByQueryBuilder(String indexName, String documentType, QueryBuilder query, boolean isSync, String username) throws SearchException
    {
        final Collection<String> deletedIds = new CopyOnWriteArrayList<String>();
        
        final Collection<Map<String,String>> ids = new CopyOnWriteArrayList<Map<String,String>>();
        
        if (query != null && query instanceof TermsQueryBuilder && query.getName().equals("sysId") &&
        	username.equals(UserUtils.SYSTEM) && !indexName.endsWith("alias")) {
        	(((TermsQueryBuilder)query).values()).parallelStream().forEach(term -> {
        		HashMap<String, String> idMap = new HashMap<String, String>();
        		idMap.put((String)term, indexName);
        		ids.add(idMap);
        	});
        } else {
        	ids.addAll(searchIDsByQuery(new String[] {indexName}, new String[] {documentType}, null, query, username));
        }
    	
        ids.parallelStream().forEach(id -> {
        	id.keySet().stream().forEach(docId -> {
    			String idx = id.get(docId);
    			try {
					deleteById(idx, documentType, docId);
					deletedIds.add(docId);
				} catch (Throwable t) {
					Log.log.error(String.format("Error %sin deleting document with id %s from index %s",
												(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
												docId, idx));
				}
    		});
    	});
    	
//        // replicationtype=async deletes primary shards directly but replicas
//        // deletes async.
//        DeleteByQueryRequestBuilder request = client.prepareDeleteByQuery(indexName).setTypes(documentType);
//        if (isSync)
//        {
//            request.setReplicationType(ReplicationType.SYNC);
//        }
//        else
//        {
//            request.setReplicationType(ReplicationType.ASYNC);
//        }
//        DeleteByQueryResponse response = request.setQuery(query).execute().actionGet();
//
//        if (response.status().equals(RestStatus.OK))
//        {
            // send to synchronization service
    		String syncLogIndexName = SearchUtils.getSyncLogIndexName(indexName, documentType);
    		if (ids!=null && !ids.isEmpty() && config.isSync() && !config.getSyncExcludeSet().contains(indexName) && !"synclog".equals(documentType) && StringUtils.isNotEmpty(syncLogIndexName))
            {
                SyncData<Serializable> syncData = new SyncData<Serializable>(syncLogIndexName, indexName, documentType, SearchUtils.SYNC_OPERATION.DELETE_BY_QUERY, null, null, null, DateUtils.getCurrentTimeInMillis(), null, "system");
                syncData.setQueryBuilder(query);
                SyncService.getInstance().enqueue(syncData);
            }
//        }
            
        return deletedIds;
    }

    /**
     * Produces a JSON as follows: query : { input: ["hello world"], weight: 1
     * }, selectCount: 1, weight: 100, sysUpdatedOn: 123456789
     * 
     * @param indexName
     * @param documentType
     * @param queries
     * @param username
     * @throws SearchException
     */
    void indexSuggester(String indexName, String documentType, Collection<Query> queries, String username) throws SearchException
    {
        if (queries != null)
        {
            for (Query query : queries)
            {
                IndexRequestBuilder request = client.prepareIndex(indexName, documentType, query.getSuggestion());
                try
                {
                    request.setSource(MAPPER.writeValueAsString(query), XContentType.JSON).get();
                }
                catch (IOException e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * This method is used for updating the index for schema changes etc.
     * 
     * @param sourceIndex
     * @param targetIndex
     * @param documentType
     * @param batchSize
     * @param username
     * @throws SearchException
     */
    void moveData(String sourceIndex, String targetIndex, String documentType, int batchSize, String username) throws SearchException
    {
        if (StringUtils.isBlank(sourceIndex) || StringUtils.isBlank(targetIndex) || StringUtils.isBlank(documentType))
        {
            throw new SearchException("Parameter sourceIndex, targetIndex, documentType are mandatory");
        }

        QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();

        SearchResponse scrollResponse = client.prepareSearch(sourceIndex).setTypes(documentType).addSort("_doc", SortOrder.ASC).setScroll(new TimeValue(60000)).setQuery(queryBuilder).setSize(batchSize).execute().actionGet(); // 1000
                                                                                                                                                                                                                                 // returned
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Total records collected from " + sourceIndex + "#" + documentType + " :" + scrollResponse.getHits().getTotalHits());
        }                                                                                                                                                                                                                  // for
        // Scroll until no hits are returned
        while (true)
        {
            scrollResponse = client.prepareSearchScroll(scrollResponse.getScrollId()).setScroll(new TimeValue(600000)).execute().actionGet();

            BulkRequestBuilder bulkRequest = getClient().prepareBulk();
            for (SearchHit hit : scrollResponse.getHits())
            {
                IndexRequestBuilder indexRequestBuilder = new IndexRequestBuilder(client, IndexAction.INSTANCE, targetIndex);
                indexRequestBuilder.setType(documentType);
                indexRequestBuilder.setWaitForActiveShards(ActiveShardCount.ONE);
                indexRequestBuilder.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
                indexRequestBuilder.setSource(hit.getSource()).setId(hit.getId());
                bulkRequest.add(indexRequestBuilder);
            }

            // Break condition: No hits are returned
            if (scrollResponse.getHits().getHits().length == 0)
            {
                break;
            }
            else
            {
                BulkResponse bulkResponse = bulkRequest.setWaitForActiveShards(ActiveShardCount.ONE).setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE).execute().actionGet();
                if (bulkResponse == null || bulkResponse.hasFailures())
                {
                    throw new SearchException("Something went wrong, didn't get any response for the index");
                }
                else
                {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Bulk index took: " + bulkResponse.getTookInMillis());
                    }
                }
            }
        }
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Total records moved from " + sourceIndex + "#" + documentType + " to " + targetIndex + "#" + documentType);
        }
    }

    <T> ResponseDTO<T> scrollByQuery(String[] indexNames, String[] documentTypes, String indexAlias, Class<T> clazz, long timeoutInMillis, int batchSize, QueryBuilder queryBuilder) throws SearchException
    {
        ResponseDTO<T> result = new ResponseDTO<T>();

        try
        {
            List<T> records = new ArrayList<T>();
            SearchRequestBuilder searchRequestBuilder = null;

            if (StringUtils.isNotBlank(indexAlias))
            {
                searchRequestBuilder = SearchAdminAPI.getClient().prepareSearch(indexAlias);
            }
            else
            {
                searchRequestBuilder = SearchAdminAPI.getClient().prepareSearch(indexNames);
            }

            searchRequestBuilder.setTypes(documentTypes).addSort("_doc", SortOrder.ASC).setScroll(new TimeValue(timeoutInMillis)).setQuery(queryBuilder).setSize(batchSize); // e.g.,
                                                                                                                                                                             // 1000
                                                                                                                                                                             // hits
                                                                                                                                                                             // per
                                                                                                                                                                             // shard
                                                                                                                                                                             // will
                                                                                                                                                                             // be
                                                                                                                                                                             // returned
                                                                                                                                                                             // for
                                                                                                                                                                             // each
                                                                                                                                                                             // scroll

            SearchResponse scrollResponse = searchRequestBuilder.execute().actionGet();

            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Total records found :" + scrollResponse.getHits().getTotalHits());
            }

            while (true)
            {
                // Break condition: No hits are returned
                if (scrollResponse.getHits().getHits().length == 0)
                {
                    break;
                }
                for (SearchHit hit : scrollResponse.getHits())
                {
                    if (StringUtils.isNotBlank(hit.getSourceAsString()))
                    {
                        T obj = MAPPER.readValue(hit.getSourceAsString(), clazz);
                        records.add(obj);
                    }
                }
            	scrollResponse = SearchAdminAPI.getClient().prepareSearchScroll(scrollResponse.getScrollId()).setScroll(new TimeValue(timeoutInMillis)).execute().actionGet();

            }
            result.setRecords(records);
        }
        catch (JsonParseException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (JsonMappingException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    <T> void processDataByScrolling(String[] indexNames, String[] documentTypes, String indexAlias, Class<T> clazz, long timeoutInMillis, int batchSize, QueryBuilder queryBuilder, ScrollCallback callback) throws SearchException
    {
        try
        {
            if (callback == null)
            {
                throw new SearchException("Callback is null, nothing to process.");
            }
            else
            {
                SearchRequestBuilder searchRequestBuilder = null;

                if (StringUtils.isNotBlank(indexAlias))
                {
                    searchRequestBuilder = SearchAdminAPI.getClient().prepareSearch(indexAlias);
                }
                else
                {
                    searchRequestBuilder = SearchAdminAPI.getClient().prepareSearch(indexNames);
                }

                searchRequestBuilder.setTypes(documentTypes).addSort("_doc", SortOrder.ASC).setScroll(new TimeValue(timeoutInMillis)).setQuery(queryBuilder).setSize(batchSize); // e.g.,
                                                                                                                                                                                 // 1000
                                                                                                                                                                                 // hits
                                                                                                                                                                                 // per
                                                                                                                                                                                 // shard
                                                                                                                                                                                 // will
                                                                                                                                                                                 // be
                                                                                                                                                                                 // returned
                                                                                                                                                                                 // for
                                                                                                                                                                                 // each
                                                                                                                                                                                 // scroll

                SearchResponse scrollResponse = searchRequestBuilder.execute().actionGet();
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("Total records found :" + scrollResponse.getHits().getTotalHits());
                }

                while (true)
                {
                    scrollResponse = SearchAdminAPI.getClient().prepareSearchScroll(scrollResponse.getScrollId()).setScroll(new TimeValue(timeoutInMillis)).execute().actionGet();
                    for (SearchHit hit : scrollResponse.getHits())
                    {
                        if (StringUtils.isNotBlank(hit.getSourceAsString()))
                        {
                            T obj = MAPPER.readValue(hit.getSourceAsString(), clazz);
                            callback.process(obj);
                        }
                    }

                    // Break condition: No hits are returned
                    if (scrollResponse.getHits().getHits().length == 0)
                    {
                        break;
                    }
                }
            }
        }
        catch (JsonParseException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (JsonMappingException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
    }

    boolean configureSnapshot(String repositoryName, String type, String location, boolean compress)
    {
        boolean result = true;

        PutRepositoryResponse response = client.admin().cluster().preparePutRepository(repositoryName).setType(type).setSettings(Settings.builder().put("location", location).put("compress", compress)).get();

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Configure snapshot response");
            Log.log.debug(response.toString());
        }

        if (!response.isAcknowledged())
        {
            result = false;
        }

        return result;
    }

    boolean createSnapshot(String repositoryName, String snapshotName, String... indices)
    {
        boolean result = true;

        CreateSnapshotResponse response = client.admin().cluster().prepareCreateSnapshot(repositoryName, snapshotName).setWaitForCompletion(true).setIndices(indices).get();

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Create snapshot response");
            Log.log.debug(response.toString());
        }
        if (!response.status().equals(RestStatus.ACCEPTED))
        {
            result = false;
        }

        return result;
    }

    boolean restoreSnapshot(String repositoryName, String snapshotName, String... indices)
    {
        boolean result = true;

        RestoreSnapshotResponse response = client.admin().cluster().prepareRestoreSnapshot(repositoryName, snapshotName).setWaitForCompletion(true).setIndices(indices).get();

        Log.log.debug("Restore snapshot response");
        Log.log.debug(response.toString());
        if (!response.status().equals(RestStatus.ACCEPTED))
        {
            result = false;
        }

        return result;
    }

    Map<String, String> getAllSnapshots(String repositoryName)
    {
        Map<String, String> result = new HashMap<String, String>();

        GetSnapshotsResponse response = client.admin().cluster().prepareGetSnapshots(repositoryName).get();

        Log.log.debug("Snapshot information");
        for (SnapshotInfo snapshot : response.getSnapshots())
        {
            Log.log.debug("Snapshot Name: " + snapshot.snapshotId().getName() + ", Details: " + snapshot.toString());
            result.put(snapshot.snapshotId().getName(), snapshot.toString());
        }

        return result;
    }

    
    public Map<String, Double> metricAggregation(String index, String idxType, String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricField)
    {
        return metricAggregation(index, idxType, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricField, null, null); 
    }    

    
    public Map<String, Double> metricAggregation(String index, String idxType, String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricField, Long tsMin, Long tsMax )
    {
        return metricAggregation(index, idxType, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricField, tsMin, tsMax, null, 10000L); // Don't return more than 10k results
    }

    public Map<String, Double> sqlAggregation(String index, String idxType, String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricField, Long tsMin, Long tsMax, Long from, Long size )
    {
        String intervalTableType = null;
        if (Constants.METRIC_INTERVAL_MINUTE.equals(intervalType) && intervalValue==5)
        {
            intervalTableType = "min";
        }
        else if (Constants.METRIC_INTERVAL_HOUR.equals(intervalType) && intervalValue==1)
        {
            intervalTableType = "hr";
        }
        else if (Constants.METRIC_INTERVAL_DAY.equals(intervalType) && intervalValue==1)
        {
            intervalTableType = "day";
        }
        else
        {
            return null;
        }
     
        String tableSuffix = null;
        if (SearchConstants.INDEX_NAME_METRIC_JVM.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_JVM.equals(idxType))
        {
            tableSuffix="jvm";
        }
        else if (SearchConstants.INDEX_NAME_METRIC_CPU.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_CPU.equals(idxType))
        {
            tableSuffix="server";
        }
        else if (SearchConstants.INDEX_NAME_METRIC_RUNBOOK.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_RUNBOOK.equals(idxType))
        {
            tableSuffix="runbook";
        }
        else if (SearchConstants.INDEX_NAME_METRIC_TRANSACTION.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_TRANSACTION.equals(idxType))
        {
            tableSuffix="transaction";
        }
        else if (SearchConstants.INDEX_NAME_METRIC_USERS.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_USERS.equals(idxType))
        {
            tableSuffix="users";
        }
        else if (SearchConstants.INDEX_NAME_METRIC_JMS_QUEUE.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_JMS_QUEUE.equals(idxType))
        {
            tableSuffix="jms_queue";
        }
        else if (SearchConstants.INDEX_NAME_METRIC_LATENCY.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_LATENCY.equals(idxType))
        {
            tableSuffix="latency";
        }
        else
        {
            return null;
        }

        String sqlTable = "metric_" + intervalTableType + "_" + tableSuffix;
        String sqlQuery = " select * from " + sqlTable + " "; 
        Log.log.debug("Reports query sql query " + sqlQuery);
        
        List<Map<String, Object>> sqlRecs = null;
        try 
        {
            sqlRecs = ServiceJDBC.executeSQLSelect(sqlQuery, 0, -1);
        }
        catch(Exception ex)
        {
            Log.log.error(ex, ex);
            return null;
        }
        Log.log.debug("Reports metrics sql query results: " + sqlRecs);
        
        Map<String, Double> sqlResults =  null;
        if (SearchConstants.INDEX_NAME_METRIC_JVM.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_JVM.equals(idxType) && "freeMem".equals(metricField))
        {
            sqlResults = processJVMFreeMemRecs(sqlRecs);
        }
        else if (SearchConstants.INDEX_NAME_METRIC_JVM.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_JVM.equals(idxType) && "threadCount".equals(metricField))
        {
            sqlResults = processJVMThreadCountRecs(sqlRecs);
        }
        else if (SearchConstants.INDEX_NAME_METRIC_CPU.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_CPU.equals(idxType) && "load1".equals(metricField))
        {
            sqlResults = processCPULoad1Recs(sqlRecs);
        }
        else if (SearchConstants.INDEX_NAME_METRIC_CPU.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_CPU.equals(idxType) && "load5".equals(metricField))
        {
            sqlResults = processCPULoad5Recs(sqlRecs);
        }
        else if (SearchConstants.INDEX_NAME_METRIC_CPU.equals(index) && SearchConstants.DOCUMENT_TYPE_METRIC_CPU.equals(idxType) && "load15".equals(metricField))
        {
            sqlResults = processCPULoad15Recs(sqlRecs);
        }

        return sqlResults;

//        String scriptResult = null;
//        String scriptName = "Retrieve Reporting Data";
//        Map<String,Object> queryParams = new HashMap<String,Object>();
//        queryParams.put("tableName", sqlTable);
//        queryParams.put("NAME", scriptName);
//        queryParams.put("limit", "-1");
//        queryParams.put("page", "1");
//        queryParams.put("selectClause", "select *");
//        queryParams.put("start", "0");
//        queryParams.put("type", "table");
//
//        queryParams.put(Constants.EXECUTE_USERID, "system");
//
//        scriptResult = ServiceHibernate.executeScript(scriptName, queryParams);
//        Object[] sqlObjs =  StringUtils.stringToJSONArray(scriptResult).toArray();
//        Log.log.debug("Reports script execution results: " + sqlObjs);

    }

    private Map<String, Double> processCPULoad1Recs(List<Map<String, Object>> recs)
    {
        Map<String, Double> results = new HashMap<String, Double>();
        if (recs!=null)
        {
            for (Map<String,Object> rec : recs)
            {
                String ts = rec.get("ts").toString();
                int total = (Integer)rec.get("tot_load1");
                int count = (Integer)rec.get("cnt_load1");
                double avg = ((double)total)/count;
                results.put(ts, avg);
            }
        }
        return results;
    }
    
    private Map<String, Double> processCPULoad5Recs(List<Map<String, Object>> recs)
    {
        Map<String, Double> results = new HashMap<String, Double>();
        if (recs!=null)
        {
            for (Map<String,Object> rec : recs)
            {
                String ts = rec.get("ts").toString();
                int total = (Integer)rec.get("tot_load5");
                int count = (Integer)rec.get("cnt_load5");
                double avg = ((double)total)/count;
                results.put(ts, avg);
            }
        }
        return results;
    }
    
    private Map<String, Double> processCPULoad15Recs(List<Map<String, Object>> recs)
    {
        Map<String, Double> results = new HashMap<String, Double>();
        if (recs!=null)
        {
            for (Map<String,Object> rec : recs)
            {
                String ts = rec.get("ts").toString();
                int total = (Integer)rec.get("tot_load15");
                int count = (Integer)rec.get("cnt_load15");
                double avg = ((double)total)/count;
                results.put(ts, avg);
            }
        }
        return results;
    }
    
    private Map<String, Double> processJVMFreeMemRecs(List<Map<String, Object>> recs)
    {
        Map<String, Double> results = new HashMap<String, Double>();
        if (recs!=null)
        {
            for (Map<String,Object> rec : recs)
            {
                String ts = rec.get("ts").toString();
                int total = (Integer)rec.get("tot_mem_free");
                int count = (Integer)rec.get("cnt_mem_free");
                double avg = ((double)total)/count;
                results.put(ts, avg);
            }
        }
        return results;
    }

    private Map<String, Double> processJVMThreadCountRecs(List<Map<String, Object>> recs)
    {
        Map<String, Double> results = new HashMap<String, Double>();
        if (recs!=null)
        {
            for (Map<String,Object> rec : recs)
            {
                String ts = rec.get("ts").toString();
                int total = (Integer)rec.get("tot_thread_count");
                int count = (Integer)rec.get("cnt_thread_count");
                double avg = ((double)total)/count;
                results.put(ts, avg);
            }
        }
        return results;
    }
    
    public Map<String, Double> metricAggregation(String index, String idxType, String[] filterFieldNames, String[] filterFieldValues, String intervalType, int intervalValue, String metricField, Long tsMin, Long tsMax, Long from, Long size )
    {
    	BoolQueryBuilder qb = new BoolQueryBuilder();
    	int i = 0;
    	for (String field : filterFieldNames)
    	{
    		qb.filter(new TermQueryBuilder(field, filterFieldValues[i]));
    		i++;
    	}
    	
//        AndFilterBuilder af = new AndFilterBuilder();
//        for (int i=0; i< filterFieldNames.length; ++i)
//        {
//            TermFilterBuilder qb = new TermFilterBuilder(filterFieldNames[i], filterFieldValues[i]);
//            af.add(qb);
//        }
        
        SearchRequestBuilder  srb = getClient().prepareSearch(index);
        srb.setTypes(idxType);
        srb.setQuery(qb);
        
//        FilteredQueryBuilder fqb = new FilteredQueryBuilder(new MatchAllQueryBuilder(), af);
//        srb.setQuery(fqb);
        
        DateHistogramAggregationBuilder dhb = AggregationBuilders.dateHistogram("avghistogram").field("ts");
        
        if (tsMin!=null && tsMax!=null)
        {
            dhb.extendedBounds(new ExtendedBounds(tsMin, tsMax));
        }
        else if (tsMin!=null)
        {
            dhb.extendedBounds(new ExtendedBounds(tsMin, Long.MAX_VALUE));
        }
        else if (tsMax!=null)
        {
            dhb.extendedBounds(new ExtendedBounds(Long.MIN_VALUE, tsMax));
        }
        
        if (Constants.METRIC_INTERVAL_MINUTE.equalsIgnoreCase(intervalType))
        {
            dhb.dateHistogramInterval(DateHistogramInterval.minutes(intervalValue));
        }
        else if (Constants.METRIC_INTERVAL_HOUR.equalsIgnoreCase(intervalType))
        {
            dhb.dateHistogramInterval(DateHistogramInterval.hours(intervalValue));
        }
        else if (Constants.METRIC_INTERVAL_SECOND.equalsIgnoreCase(intervalType))
        {
            dhb.dateHistogramInterval(DateHistogramInterval.seconds(intervalValue));
        }
        else if (Constants.METRIC_INTERVAL_WEEK.equalsIgnoreCase(intervalType))
        {
            dhb.dateHistogramInterval(DateHistogramInterval.weeks(intervalValue));
        }
        else if (Constants.METRIC_INTERVAL_DAY.equalsIgnoreCase(intervalType))
        {
            dhb.dateHistogramInterval(DateHistogramInterval.days(intervalValue));
        }
        else
        {
            String err = "Unsupported metric interval type: " + intervalType;
            Log.log.error(err);
            throw new RuntimeException(err);
        }
        
        dhb.subAggregation(AggregationBuilders.avg("jvmavg").field(metricField));   
        srb.addAggregation(dhb);

        if (from!=null && from.longValue()>-1)
        {
            srb.setFrom(from.intValue());
        }

        if (size!=null && size.longValue()>0)
        {
            srb.setSize(size.intValue());
        }
        
        SearchResponse response = srb.execute().actionGet();
        Map<String, Double> result = new HashMap<String, Double>(); 
        Histogram dhg = response.getAggregations().get("avghistogram");
        
        for (Bucket b : dhg.getBuckets())
        {
           Avg agg =  (Avg)b.getAggregations().get("jvmavg");
           double avgVal = agg.getValue();
           String bKey = b.getKeyAsString();
           Object nKey = b.getKey(); // b.getKeyAsNumber();
           Log.log.debug("agg name = " + agg.getName() + ", bucket key = " + bKey + ", number key = " + nKey);
           result.put(nKey.toString(), avgVal);
        }
        
        //sqlAggregation(index, idxType, filterFieldNames, filterFieldValues, intervalType, intervalValue, metricField, tsMin, tsMax, from, size );
        return result;
    }

    public ResponseDTO<String> searchByQuery(final String[] indexNames, final String[] documentTypes, final String query, final int start, final int limit) throws SearchException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();

        if (indexNames == null || indexNames.length <= 0)
        {
            result.setMessage("Index or alias name(s) must be provided.");
            result.setSuccess(false);
        }
        else
        {
            Client client = getClient();

            SearchRequestBuilder searchRequestBuilder = client.prepareSearch(indexNames);
//            searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            searchRequestBuilder.setQuery(QueryBuilders.wrapperQuery(query));
            if (documentTypes != null && documentTypes.length > 0)
            {
                searchRequestBuilder.setTypes(documentTypes);
            }

            searchRequestBuilder.setFrom(start);

            if (limit > 0)
            {
                searchRequestBuilder.setSize(limit);
            }

            SearchResponse response = searchRequestBuilder.execute().actionGet();
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace(response.toString());
            }

            if (response.getHits().getTotalHits() >= 0)
            {
                List<String> records = new ArrayList<String>();
                for (SearchHit hit : response.getHits())
                {
                    try
                    {
                        records.add(hit.getSourceAsString());
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                        throw new SearchException(e.getMessage(), e);
                    }
                }
                result.setRecords(records);
            }
            result.setTotal(response.getHits().getTotalHits());
        }
        return result;
    }
    
    <T> long getNonSuperUserProcReqCountByQueryBuilder(String username, String[] indexNames, String[] documentTypes, QueryBuilder queryBuilder, QueryBuilder... filterBuilders) throws SearchException
    {
        long result = 0;

        Client client = getClient();

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        int start = 0;
        
        searchRequestBuilder.setFrom(start);

        // search type COUNT is the best in this because it doesn't fetch (no
        // hammering the poor network :) the document from the index but
        // simply gets the count based on the query.
//        searchRequestBuilder.setSearchType(SearchType.COUNT);
        if (queryBuilder != null)
        {
            searchRequestBuilder.setQuery(queryBuilder);
        }

        if (filterBuilders != null)
        {
            BoolQueryBuilder bqb = new BoolQueryBuilder();
//            AndFilterBuilder andFilterBuilder = FilterBuilders.andFilter();
            int noOfFilterBuilders = 0;
            for (QueryBuilder filterBuilder : filterBuilders)
            {
                if (filterBuilder != null)
                {
                    bqb.filter(filterBuilder);
                    noOfFilterBuilders++;
                }
            }
            if (noOfFilterBuilders > 0)
            {
                searchRequestBuilder.setQuery(bqb);
            }
        }
        
//        if (filterBuilders != null && filterBuilders.length > 0)
//        {
//            AndFilterBuilder andFilterBuilder = FilterBuilders.andFilter();
//            for (FilterBuilder filterBuilder : filterBuilders)
//            {
//                if (filterBuilder != null)
//                {
//                    andFilterBuilder.add(filterBuilder);
//                }
//            }
//            searchRequestBuilder.setPostFilter(andFilterBuilder);
//        }

        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        
        if (!resp.isTimedOut())
        {
            boolean hasNoOrgAccess = UserUtils.isNoOrgAccessible(username);
            Set<String> aggrOrgIdsInHierarchy = new HashSet<String>();
            
            UsersVO usersVO = UserUtils.getUser(username);
            
            if (usersVO != null)
            {
                aggrOrgIdsInHierarchy = usersVO.getAccessibleOrgIds();
            }
            
            long total = resp.getHits().getTotalHits();
            long processedCnt = 0l;
            int count = 0;
            
            while (true)
            {
                for (SearchHit hit : resp.getHits())
                {
                    try
                    {
                        boolean add = true;
                        
                        String worksheetOrgId = null;
                        Worksheet worksheet = null;
                            
                        ProcessRequest processRequest = MAPPER.readValue(hit.getSourceAsString(), ProcessRequest.class);
                        
                        if (processRequest != null && StringUtils.isNotBlank(processRequest.getProblem()))
                        {
                            worksheet = APIFactory.getWorksheetSearchAPI(Worksheet.class).findDocumentById(processRequest.getProblem(), username);
                            
                            if (worksheet != null && StringUtils.isNotBlank(worksheet.getSysOrg()))
                            {
                                worksheetOrgId = worksheet.getSysOrg();
                            }
                        }
                            
                        if (add)
                        {
                            if (StringUtils.isNotBlank(worksheetOrgId))
                            {
                                add = aggrOrgIdsInHierarchy.contains(worksheetOrgId);
                            }
                            else
                            {
                                add = hasNoOrgAccess;
                            }
                        }
                        
                        if (add)
                        {
                            result++;
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                        throw new SearchException(e.getMessage(), e);
                    }
                    
                    count++;
                    processedCnt++;
                }
                    
                //assumes default limit of 10, if ES changes this this function will need to be updated
                if (count < 10 || processedCnt >= total)
                {
                    break;
                }
                else
                {
                    count = 0;
                    start = start + 10;
                    searchRequestBuilder.setFrom(start);
                    resp = searchRequestBuilder.execute().actionGet();
                }
            }
        }
        
        return result;
    }
    
    public ResponseDTO<List<String>> getAllIndexNames(String indexNameToSearch)
    {
    	GetIndexRequest getIndxReq = new GetIndexRequest();
    	
    	if (StringUtils.isNotBlank(indexNameToSearch)) {
    		getIndxReq.indices(indexNameToSearch);
    	}
    	
    	String[] indices = client.admin().indices().getIndex(getIndxReq).actionGet().getIndices();
    	return new ResponseDTO<List<String>>().setData(Arrays.asList(indices));
    }
    
    public ResponseDTO<List<String>> getAllEmptyIndexNames(String indexNameToSearch)
    {
    	GetIndexRequest getIndxReq = new GetIndexRequest();
    	
    	if (StringUtils.isNotBlank(indexNameToSearch)) {
    		getIndxReq.indices(indexNameToSearch);
    	}
    	
    	String[] indices = client.admin().indices().getIndex(getIndxReq).actionGet().getIndices();
    	
    	List<String> indicesList = indices != null && indices.length > 0 ? Arrays.asList(indices) : new ArrayList<String>();
    	
    	List<String> emptyIndicesList = new ArrayList<String>();
    	
    	if (CollectionUtils.isNotEmpty(indicesList)) {
    		emptyIndicesList = indicesList.parallelStream()
    						   .filter(indxName -> {
    							   	IndicesStatsResponse indxStateResp = client.admin().indices().prepareStats(indxName).get();
    							   	return indxStateResp.getIndices().get(indxName).getTotal().docs.getCount() == 0l;
    						   })
    						   .collect(Collectors.toList());
    	}
    	
    	return new ResponseDTO<List<String>>().setData(emptyIndicesList);
    }
    
    private <T> Pair<ResponseDTO<T>, String> processSearchResponse(SearchResponse resp,
    															   final Class<T> clazz)  throws SearchException {
    	ResponseDTO<T> respDTO = new ResponseDTO<T>();
    	String nextScrollId = null;
    	List<T> records = new ArrayList<T>();
    	
    	if (resp != null) {
    		nextScrollId = resp.getScrollId();
    		
    		respDTO.setTotal(resp.getHits().getTotalHits());
    		
    		if (respDTO.getTotal() > 0) {
    			for (SearchHit hit : resp.getHits()) {
    				T obj;
    				
                    try {
                        obj = MAPPER.readValue(hit.getSourceAsString(), clazz);
                        
                        if (hit.getHighlightFields() != null && MapUtils.isNotEmpty(hit.getHighlightFields())) {
                            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                            
                            for (String fieldName : highlightFields.keySet()) {
                                HighlightField field = highlightFields.get(fieldName);
                                String highlightedValue = field.fragments()[0].string();
                                BeanUtil.setProperty(obj, fieldName, highlightedValue);
                            }
                        }
                    } catch (Exception e) {
                        Log.log.error(e.getMessage(), e);
                        throw new SearchException(e.getMessage(), e);
                    }
                    
                    records.add(obj);
    			}
    			
    			respDTO.setRecords(records);
    		}
    	}
    	
    	return Pair.of(respDTO, nextScrollId);
    }
    
    public <T> Pair<ResponseDTO<T>, String> searchByFilteredQuery(final Class<T> clazz, final String[] indexNames, 
    															  final String[] documentTypes, final QueryDTO query, 
    															  final QueryBuilder queryBuilder, 
    															  final StatsAggregationBuilder statsBuilder, 
    															  final QueryBuilder[] filterBuilders, 
    															  final String[] highlightedFields, 
    															  final String username, 
    															  final String scrollId,
    															  final TimeValue scrollTimeOut) throws SearchException {
    	SearchResponse scrollResp = null;
    	
        if (StringUtils.isNotBlank(scrollId)) {
        	scrollResp = getClient().prepareSearchScroll(scrollId).setScroll(scrollTimeOut).execute().actionGet();
        	
        	if (Log.log.isTraceEnabled()) {
                Log.log.trace(String.format("Search Response for Scroll Id %s: Scroll Id for next search [%s], Response [%s]", 
                							scrollId, scrollResp.getScrollId(), scrollResp));
            }
        } else {        
        	QueryDTO tmpQuery = query;

        	if (tmpQuery == null) {
        		tmpQuery = new QueryDTO();
            
        		try {
					if (clazz.newInstance() instanceof BaseIndexModel) {
						tmpQuery.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));
					}
				} catch (InstantiationException | IllegalAccessException e) {
					throw new SearchException(String.format("%s does not extends from required %s", clazz.getSimpleName(),
															BaseIndexModel.class.getSimpleName()));
				}
        	}

        	if (CollectionUtils.isEmpty(tmpQuery.getSortItems())) {
        		throw new SearchException("Scrolled search query must have at least one sort item");
        	}
        	
        	int limit = tmpQuery.getLimit() > 0 ? tmpQuery.getLimit() : 10000;
        	int start = tmpQuery.getStart() != -1 ? tmpQuery.getStart() : 0;

        	SearchRequestBuilder searchRequestBuilder = client.prepareSearch().setScroll(scrollTimeOut);
        	searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        	// comma separated list of fields
        	String selectFields = StringUtils.isBlank(tmpQuery.getSelectColumns()) ? "*" : tmpQuery.getSelectColumns();
        	String excludeFields = StringUtils.isBlank(tmpQuery.getExcludeColumns()) ? "" : tmpQuery.getExcludeColumns();
        	
        	if (StringUtils.isNotBlank(tmpQuery.getExcludeColumns()) || StringUtils.isNotBlank(tmpQuery.getSelectColumns())) {
        		searchRequestBuilder.setFetchSource(selectFields.split(","), excludeFields.split(","));
        	}

        	// add the sorting information
        	addSortOrder(tmpQuery, searchRequestBuilder);
        
        	searchRequestBuilder.setFrom(start);
        	searchRequestBuilder.setSize(limit);
        	
        	// this adds some overhead that's why it's in trace
        	if (Log.log.isTraceEnabled()) {
        		searchRequestBuilder.setExplain(true);
        	}

        	if (statsBuilder != null) {
        		searchRequestBuilder.addAggregation(statsBuilder);
        	}
        	
        	if (queryBuilder != null) {
        		searchRequestBuilder.setQuery(queryBuilder);
            }
        		
        	if (filterBuilders != null) {
        		BoolQueryBuilder bqb = new BoolQueryBuilder();
        		
        		int noOfFilterBuilders = 0;
        		
        		for (QueryBuilder filterBuilder : filterBuilders) {
        			if (filterBuilder != null) {
        				bqb.filter(filterBuilder);
        				noOfFilterBuilders++;
        			}
        		}
        		
        		if (noOfFilterBuilders > 0) {
        			searchRequestBuilder.setQuery(bqb);
        		}
        	}
        	
        	if (highlightedFields != null && highlightedFields.length > 0) {
        		for (String highlightedField : highlightedFields) {
        			HighlightBuilder hb = new HighlightBuilder();
        			hb.field(highlightedField, 0, 0);
        			searchRequestBuilder.highlighter(hb);
        		}
        	}
        
        	if (Log.log.isTraceEnabled()) {
        		Log.log.trace(searchRequestBuilder.toString());
        	}
        	
        	scrollResp = searchRequestBuilder.execute().actionGet();
        	
        	if (Log.log.isTraceEnabled()) {
        		Log.log.trace(scrollResp.toString());
        	}
        }
        
        return processSearchResponse(scrollResp, clazz);
    }
    
    public <T> boolean clearScroll(String scrollId) {
    	ClearScrollResponse clearScrollResp = getClient().prepareClearScroll().addScrollId(scrollId).execute().actionGet();
    	
    	if (!clearScrollResp.isSucceeded()) {
    		Log.log.warn(String.format("Failed to clear scroll search request with scroll id %s", scrollId));
    	}
    	
    	return clearScrollResp.isSucceeded();
    }
    
    private String getBulkResponseFailureDetailMessage(BulkResponse bulkResp, boolean ignoreIndexNotFoundError) {
    	final StringBuilder strBldr = new StringBuilder("");
    	
    	if (bulkResp != null) {
    		if (bulkResp.hasFailures()) {
    			Arrays.asList(bulkResp.getItems()).stream()
    			.forEach(bulkItemResp -> {
    				if (bulkItemResp.isFailed()) {
    					if (ignoreIndexNotFoundError && 
    						bulkItemResp.getFailureMessage().
    						startsWith(String.format("[%s] IndexNotFoundException", bulkItemResp.getIndex()))) {
    						return;
    					}
    					
    					strBldr.append(String.format(" | %s, %s, %s : [%s]", bulkItemResp.getIndex(), 
    												 bulkItemResp.getType(), bulkItemResp.getId(),
    												 bulkItemResp.getFailureMessage()));
    				}
    			});
    		}
    	}
    	
    	return strBldr.toString();
    }
    
    private String getBulkResponseDetailMessage(BulkResponse bulkResp) {
    	StringBuilder strBldr = new StringBuilder("");
    	
    	if (bulkResp != null) {
			Arrays.asList(bulkResp.getItems()).stream()
			.forEach(bulkItemResp -> {
					strBldr.append(String.format(" | %s, %s, %s%s", bulkItemResp.getIndex(), 
												 bulkItemResp.getType(), bulkItemResp.getId(),
												 (bulkItemResp.isFailed() ? 
												  " : [" + bulkItemResp.getFailureMessage() + "]" : "")));
			});
    	}
    	
    	return strBldr.toString();
    }
    
    /**
     * Bulk deletes documents by sysIds.
     * 
     * @param bulkReq	Bulk delete by sysIds request
     * 					Key = Pair of Index Name and Document Type
     * 					Value = List of sysIds
     * @param fromDR	True - Delete from DR ES 
     * 					False - Delete from main ES
     * @throws SearchException
     */
    public void bulkDeleteByIds(Map<Pair<String, String>, List<String>> bulkReq, boolean fromDR) throws SearchException
    {
    	long startTime = System.currentTimeMillis();
    	
    	if (MapUtils.isEmpty(bulkReq)) {
    		return;
    	}
    	
        Client client = getClient();
        BulkRequestBuilder bulkReqBldr = client.prepareBulk();
        
        bulkReqBldr.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE) 
        .setTimeout(TimeValue.timeValueMinutes(10)) // This is empirical
        .setWaitForActiveShards(ActiveShardCount.DEFAULT);
        
        final List<Boolean> requestAdded = new ArrayList<Boolean>(1);
        requestAdded.add(Boolean.FALSE);
        
        bulkReq.keySet().stream().forEach(indxNameDocType -> {
        	List<String> sysIds = bulkReq.get(indxNameDocType);
        	
        	if (CollectionUtils.isNotEmpty(sysIds)) {
        		/*
        		 * Handle index deleted from main ES or failed to 
        		 * delete index from main ES and ended up in 
        		 * deleting all individual documents form index leaving index
        		 * with 0 documents in main ES
        		 * 
        		 * For DR ES
        		 */
        		
        		if (fromDR) {
        			if (sysIds.contains(BaseArchiveTable.ALL_DOCUMENT_IDS_IN_INDEX_ID)) {
        				// Try to delete index from DR ES
        				
        				boolean deleteIndexResult = BaseArchiveTable.deleteIndex(indxNameDocType.getLeft());
        				
        				if (Log.log.isDebugEnabled()) {
        					Log.log.debug(String.format("Delete index %s from DR ES result is %b", 
        												indxNameDocType.getLeft(), deleteIndexResult));
        				}
        				
        				if (deleteIndexResult) {
        					sysIds.clear();
        					sysIds.add(BaseArchiveTable.ALL_DOCUMENT_IDS_IN_INDEX_ID);
        				}
        			}
        		}
        		
        		sysIds.stream().forEach(sysId -> {
        			if (sysId.equals(BaseArchiveTable.ALL_DOCUMENT_IDS_IN_INDEX_ID)) {
        				return;
        			}
        			
        			DeleteRequestBuilder delReqBldr = client.prepareDelete(indxNameDocType.getLeft(), 
        																   indxNameDocType.getRight(), sysId);
        			
        			/*
        			 * For worksheet data ES index id/sysId is composite key worksheet id + property Name but 
        			 * routing is based on hash(worksheet id) % # of shards i.e. routing key is different
        			 * than the id/sys_id and hence need to be set for worksheet data only
        			 */
        			if (indxNameDocType.getLeft().equals(SearchConstants.INDEX_NAME_WORKSHEET_DATA)) {
        				delReqBldr.setRouting(sysId.substring(0, 32));
        			}
        			
        			if (requestAdded.get(0).equals(Boolean.FALSE)) {
            			requestAdded.set(0, Boolean.TRUE);
            		}
        			
        			bulkReqBldr.add(delReqBldr);
        		});
        	}
        });
        
        long startTime1 = System.currentTimeMillis();
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("Building bulk request took %d ms", (startTime1 - startTime)));
        }
        
        startTime = startTime1;
        
        if (requestAdded.get(0).equals(Boolean.TRUE)) {
        	BulkResponse bulkResp = bulkReqBldr.execute().actionGet();
        	
        	startTime1 = System.currentTimeMillis();
	        Log.log.info(String.format("Bulk Delete By sysIds contining %d sysIds took %s (wall clock time %d ms) " +
	        						   "and has %serrors", bulkResp.getItems().length, bulkResp.getTook(),
	        						   (startTime1 - startTime), (bulkResp.hasFailures() ? "" : "no ")));
	        
	        if (Log.log.isTraceEnabled()) {
	        	Log.log.trace(getBulkResponseDetailMessage(bulkResp));
	        }
	        
	        if (Log.log.isDebugEnabled()) {
	        	if (bulkResp.hasFailures()) {
	        		Log.log.debug(getBulkResponseFailureDetailMessage(bulkResp, true));
	        	}
	        }
	        
	        if (!bulkResp.hasFailures()) {
	            Log.log.info(String.format("Bulk Delete By sysIds contining %d sysIds took %s (wall clock time %d ms)", 
	            						   bulkResp.getItems().length,bulkResp.getTook(), (startTime1 - startTime)));
	        } else {
	        	// Errors in bulk delete of type index not found are ignored.
	        	
	        	String errorDetails = getBulkResponseFailureDetailMessage(bulkResp, true);
	        	
	        	if (StringUtils.isNotBlank(errorDetails)) {
	        		String errMsg = String.format("Bulk Response Failure Details:%s", errorDetails);
	        		Log.log.error(errMsg);
	        		throw new SearchException(errMsg);
	        	} else {
	        		Log.log.info(String.format("Bulk Delete By sysIds containing %d sysIds took %s (wall clock time %d ms)" +
	        								   " and has no errors other than index not found errors which are ignored", 
	        								   bulkResp.getItems().length, bulkResp.getTook(), (startTime1 - startTime)));
	        	}
	        }
        }
	    
        if (MapUtils.isNotEmpty(bulkReq)) {
	        // send to synchronization service if request is from main ES and DR sync is configured
	        if (!fromDR && config.isSync()) {
                SyncData<Serializable> syncData = new SyncData<Serializable>(bulkReq, UserUtils.SYSTEM);
                SyncService.getInstance().enqueue(syncData);
            }
        }
    }

    
	public static void invalidateExecutionCache(String processRequestId, String worksheetId) {
		if (!StringUtils.isBlank(processRequestId)) {
			String key = SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST.concat(processRequestId);
			EXECUTION_CACHE.remove(key);
		}

		if (!StringUtils.isBlank(worksheetId)) {
			String key = SearchConstants.DOCUMENT_TYPE_WORKSHEET.concat(worksheetId);
			EXECUTION_CACHE.remove(key);
		}
	}

	public boolean isEsCacheEnabled() {
		return SearchService.esCacheEnabled;
	}

	public void setEsCacheEnabled(boolean esCacheEnabled) {
		SearchService.esCacheEnabled = esCacheEnabled;
	}
	
	public long getScrollCtxKeepAliveDur() {
		return config.getScrollCtxKeepAliveDur();
	}
	
	public Duration getScrollCtxKeepAliveDurInMinutes() {
		return config.getScrollCtxKeepAliveDurInMinutes();
	}
}