/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

import com.resolve.search.SearchConstants;
import com.resolve.util.DateUtils;

public class QueryFactory
{
    public static Query createQuery(String type, String phrase, int weight)
    {
        return createQuery(type, phrase, weight, DateUtils.GetUTCDateLong());
    }
    
    public static Query createQuery(String type, String phrase, int weight, long sysUpdatedOn)
    {
        Query query = null;
        
        if(SearchConstants.USER_QUERY_TYPE_DOCUMENT.endsWith(type))
        {
            query = new DocumentQuery(phrase, weight, sysUpdatedOn);
        }
        else if(SearchConstants.USER_QUERY_TYPE_SOCIAL.endsWith(type))
        {
            query = new SocialQuery(phrase, weight, sysUpdatedOn);
        }
        else if(SearchConstants.USER_QUERY_TYPE_AUTOMATION.endsWith(type))
        {
            query = new AutomationQuery(phrase, weight, sysUpdatedOn);
        }
        else if(SearchConstants.USER_QUERY_TYPE_WIKI.endsWith(type))
        {
            query = new WikiQuery(phrase, weight, sysUpdatedOn);
        }
        
        return query;
    }
}
