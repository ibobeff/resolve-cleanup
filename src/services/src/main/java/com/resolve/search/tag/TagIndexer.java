/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.tag;

import java.util.Collection;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.Tag;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class TagIndexer implements Indexer<IndexData<Tag>>
{
    private static volatile TagIndexer instance = null;
    private final IndexAPI indexAPI;

    private final ConfigSearch config;
    private long indexThreads;

    private ResolveConcurrentLinkedQueue<IndexData<Tag>> dataQueue = null;

    public static TagIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("TagIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static TagIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new TagIndexer(config);
        }
        return instance;
    }

    private TagIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexAPI = APIFactory.getTagIndexAPI();
        this.indexThreads = this.config.getIndexthreads();
    }

    public void init()
    {
        Log.log.debug("Initializing TagIndexer.");
        QueueListener<IndexData<Tag>> wikiIndexListener = new IndexListener<IndexData<Tag>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiIndexListener);
        Log.log.debug("TagIndexer initialized.");
    }

    public boolean enqueue(IndexData<Tag> indexData)
    {
        return dataQueue.offer(indexData);
    }

    @Override
    public boolean index(IndexData<Tag> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<Tag> tags = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexTags(tags, indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    private void indexTags(final Collection<Tag> tags, final String username)
    {
        try
        {
            indexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, tags, false, username);
        }
        catch (SearchException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }
}
