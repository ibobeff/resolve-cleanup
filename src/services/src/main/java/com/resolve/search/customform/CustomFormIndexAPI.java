/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.customform;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.CustomForm;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CustomFormIndexAPI
{
    private static IndexAPI indexAPI = APIFactory.getCustomFormIndexAPI();
    
    private static final Indexer<IndexData<String>> indexer = CustomFormIndexer.getInstance();
    
    public static long getLastIndexTime() throws SearchException
    {
        return indexAPI.getLastIndexTime(SearchConstants.SYS_UPDATED_ON);
    }

    public static void indexCustomForm(String sysId, String username)
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("CustomFormIndexAPI.indexCustomForm() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            if (StringUtils.isBlank(sysId))
            {
                Log.log.debug("Blank sysId provided, check the calling code");
            }
            else
            {
                try
                {
                    MetaFormViewVO customFormVO = ServiceHibernate.getCustomFormFromIdWithReferences(sysId, null, username);
                    CustomForm customForm = new CustomForm(customFormVO);
                    indexAPI.indexDocument(customForm, username);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }
    
	@SuppressWarnings("unchecked")
	public static void updateIndexMapping(String username) {
		IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.MIGRATE, username);
		indexer.enqueue(indexData);
	}
	
    public static void indexCustomForms(Collection<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("PropertyIndexAPI.indexCustomForms() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.INDEX, username);
            indexer.enqueue(indexData);
        }
    }

    public static void deleteProperties(Set<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("PropertyIndexAPI.deleteProperties() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
            indexer.enqueue(indexData);
        }
    }
    
    public static void deleteAllCustomForms(String username)
    {
        IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.DELETE, username);
        indexer.enqueue(indexData);
    }
    
    public static void deleteCustomForms(Collection<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("CustomFormIndexAPI.deleteCustomForms() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
            indexer.enqueue(indexData);
        }
    }
}
