/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

import com.resolve.search.model.BaseIndexModel;

@SuppressWarnings("serial")
public class GlobalQuery extends BaseIndexModel
{
    private static final long serialVersionUID = -6515332530864151605L;
	private String query;
    private int selectCount;
    private Integer weight;
    
    public GlobalQuery()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public int getSelectCount()
    {
        return selectCount;
    }

    public void setSelectCount(int selectCount)
    {
        this.selectCount = selectCount;
    }

    public Integer getWeight()
    {
        return weight;
    }

    public void setWeight(Integer weight)
    {
        this.weight = weight;
    }
}
