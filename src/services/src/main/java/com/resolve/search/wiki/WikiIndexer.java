/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.wiki;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.RateIndexer;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchException;
import com.resolve.search.Server;
import com.resolve.search.model.WikiDocument;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocStatisticsVO;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.vo.WikidocRatingDTO;
import com.resolve.util.Log;
import com.resolve.util.NumberUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

import static com.resolve.search.SearchConstants.INDEX_NAME_WIKI_DOCUMENT;
import static com.resolve.search.SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT;
import static com.resolve.search.SearchConstants.SYS_ID;
import static com.resolve.search.SearchConstants.SYS_CREATED_ON;
import static com.resolve.search.SearchConstants.SYS_UPDATED_ON;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_DOCUMENT;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_DECISION_TREE;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_AUTOMATION;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_PLAYBOOK;

public class WikiIndexer implements Indexer<IndexData<? extends Object>>
{
    private static volatile WikiIndexer instance = null;

    private final IndexAPI indexAPI;
    private final IndexAPI attachmentIndexAPI;
	private final IndexAPI documentIndexAPI;
	private final IndexAPI automationIndexAPI;
	private final IndexAPI decisionTreeIndexAPI;
	private final IndexAPI playbookIndexAPI;
    private final RateIndexer<IndexData<String>> queryIndexer;
    private final RateIndexer<IndexData<String>> wikiQueryIndexer;

    private final ConfigSearch config;
    private long indexThreads;

    private ResolveConcurrentLinkedQueue<IndexData<? extends Object>> dataQueue = null;

    private final Map<String, String> excludeNamespacePatternMap = new HashMap<String, String>();
    private final List<String> excludeDocumentList;

    public static WikiIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("WikiIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static WikiIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new WikiIndexer(config);
        }
        return instance;
    }

    private WikiIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexThreads = this.config.getIndexthreads();
        this.indexAPI = APIFactory.getWikiIndexAPI();
        this.attachmentIndexAPI = APIFactory.getWikiAttachmentIndexAPI();

        //exclusion, if ever need to run from RSRemote then make sure to parameterize this from Main.init() or somewhere.
        //String excludeNamespacePattern = PropertiesUtil.getPropertyString("search.exclude.namespace");
        String excludeNamespacePattern = config.getProperties().get("search.exclude.namespace");

        List<String> excludeNamespaceList = StringUtils.convertStringToList(excludeNamespacePattern, ",");
        if(excludeNamespaceList != null && excludeNamespaceList.size() > 0)
        {
            for(String key : excludeNamespaceList)
            {
                if(StringUtils.isNotBlank(key))
                {
                    excludeNamespacePatternMap.put(key, "");
                }
            }
        }
        
        //String excludeDocumentPattern = PropertiesUtil.getPropertyString("search.exclude.document.pattern");
        String excludeDocumentPattern = config.getProperties().get("search.exclude.document.pattern");
        
        excludeDocumentList = StringUtils.convertStringToList(excludeDocumentPattern, ",");

        //search term query indexer
        queryIndexer = APIFactory.getDocumentQueryIndexer();
        // this indexer is for WIKI name suggestion builder
        wikiQueryIndexer = APIFactory.getWikiQueryIndexer();
        
        documentIndexAPI = APIFactory.getDocumentIndexAPI();
    	automationIndexAPI = APIFactory.getAutomationIndexAPI();
    	decisionTreeIndexAPI = APIFactory.getDecisiontreeIndexAPI();
    	playbookIndexAPI = APIFactory.getPlaybookIndexAPI();
    }

    public void init()
    {
        Log.log.debug("Initializing WikiIndexer.");
        QueueListener<IndexData<? extends Object>> wikiIndexListener = new IndexListener<IndexData<? extends Object>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiIndexListener);
        Log.log.debug("WikiIndexer initialized.");
    }

    public boolean enqueue(IndexData<? extends Object> indexData)
    {
        Log.log.debug("Enqueing " + indexData + " to WikiIndexer...");
        return dataQueue.offer(indexData);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean index(IndexData<? extends Object> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> sysIds = (Collection<String>) indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexWikiDocuments(sysIds, indexData.getUsername());
                    break;
                case DELETE:
                    setDeleteWikiDocuments(sysIds, true, indexData.getUsername());
                    break;
                case PURGE:
                    purgeWikiDocuments(sysIds, indexData.getUsername());
                    break;
                case CUSTOM:
                    customIndex(indexData);
                    break;
                case MIGRATE:
                	updateIndexMapping();
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }
    
    private void updateIndexMapping() {
    	SearchAdminAPI.updateIndexMapping(INDEX_NAME_WIKI_DOCUMENT, DOCUMENT_TYPE_WIKI_DOCUMENT, Server.ES_WIKIDOC_MAPPING_FILE);
    	SearchAdminAPI.updateIndexMapping(SEARCH_TYPE_AUTOMATION, SEARCH_TYPE_AUTOMATION, Server.ES_ATOMATIONS_MAPPING_FILE);
    	SearchAdminAPI.updateIndexMapping(SEARCH_TYPE_DOCUMENT, SEARCH_TYPE_DOCUMENT, Server.ES_DOCUMENTS_MAPPING_FILE);
    	SearchAdminAPI.updateIndexMapping(SEARCH_TYPE_DECISION_TREE, SEARCH_TYPE_DECISION_TREE, Server.ES_DECISION_TREES_MAPPING_FILE);
    	SearchAdminAPI.updateIndexMapping(SEARCH_TYPE_PLAYBOOK, SEARCH_TYPE_PLAYBOOK, Server.ES_PLAYBOOKS_MAPPING_FILE);
	}
    
    public void indexWiki(final WikiDocument wikiDocument, final boolean updateQuery, String username)
    {
        try
        {
            if(wikiDocument != null && isWikiIndexable(wikiDocument))
            {
                Collection<WikiDocument> wikiDocuments = new LinkedList<WikiDocument>();
                wikiDocuments.add(wikiDocument);
                indexAPI.indexDocuments(SYS_ID, SYS_CREATED_ON, wikiDocuments, false, username);
                
                // index records in separate index by wiki document type 
                WikiIndexAPI.indexWikiDocumentByType(wikiDocument, username);
                
                //index attachments for this wiki
                Set<String> attachmentIds = ServiceWiki.getAllAttachmentIds(wikiDocument.getSysId(), wikiDocument.getFullName(), username);
                if(attachmentIds != null && attachmentIds.size() > 0)
                {
                    WikiIndexAPI.indexWikiAttachments(wikiDocument.getSysId(), attachmentIds, username);
                }
                
                if(updateQuery)
                {
                    // now update the search query with phrases found in the wiki document
                    Collection<String> queries = Arrays.asList(wikiDocument.getTitle(), wikiDocument.getSummary(), wikiDocument.getContent());
                    queryIndexer.indexQueries(wikiDocument.getSysId(), queries, wikiDocument.getSysUpdatedOn(), false, username);
        
                    // now update the wiki name suggester
                    Collection<String> wikiNames = Arrays.asList(wikiDocument.getFullName());
                    wikiQueryIndexer.indexQueries(wikiDocument.getSysId(), wikiNames, wikiDocument.getSysUpdatedOn(), false, username);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * Indexes wiki documents.
     * 
     * TODO need to improve based on observed performance. BulkProcessor?
     * 
     * @param sysIds
     * @throws Exception
     */
    private void indexWikiDocuments(Collection<String> sysIds, String username) throws Exception
    {
        int pauseCounter = 0;

        Long lastIndexTime = indexAPI.getLastIndexTime(SYS_UPDATED_ON);
        Log.log.info("Started Indexing: " + sysIds.size() + " WikiDocs");
        for (String sysId : sysIds)
        {
            WikiDocumentVO wikiVO = null;
            try
            {
                wikiVO = ServiceWiki.getWikiDocWithGraphRelatedDataNoLock(sysId, null, username);
                
                if (wikiVO != null)
                {
                    WikiDocument doc = new WikiDocument(wikiVO);
                    //add statistics data
                    WikidocStatisticsVO statsVO = ServiceWiki.findWikidocStatisticsForWiki(sysId, null, username);
                    
                    if(statsVO != null)
                    {
                        Log.log.debug("Wiki Document Statistics for " + wikiVO.getUFullname() + " are " + statsVO);
                        
                        doc.setClickCount(NumberUtils.createLong(statsVO.getUViewCount()));
                        doc.setEditCount(NumberUtils.createLong(statsVO.getUEditCount()));
                        doc.setExecuteCount(NumberUtils.createLong(statsVO.getUExecuteCount()));
                        doc.setReviewCount(NumberUtils.createLong(statsVO.getUReviewCount()));
                        doc.setUsefulNoCount(NumberUtils.createLong(statsVO.getUUsefulNoCount()));
                        doc.setUsefulYesCount(NumberUtils.createLong(statsVO.getUUsefulYesCount()));
                    }
                    else
                    {
                        Log.log.error("Could not find Wiki Document Statistics for " + wikiVO.getUFullname());
                    }
                    
                    //set the rating
                    WikidocRatingDTO wikiRating = ServiceWiki.getRatingFor(sysId, null, username);
                    
                    if(wikiRating != null)
                    {
                        Double rating = wikiRating.getRating(); 
                        doc.setRating(rating);
                    }
                    Log.log.debug("Indexing Wiki document: " + doc.getFullName());
                    indexWiki(doc, true, username);
                    
                    pauseCounter++;
                    if (pauseCounter%100 == 0)
                    {
                        Thread.sleep(1000L);
                    }
                    if (pauseCounter%500 == 0)
                    {
                        Log.log.info("Indexed: " + pauseCounter + " WikiDocs");
                    }
                }
                else
                {
                    Log.log.error("Could not find and hence index: " + wikiVO.getUFullname());
                }
            }
            catch(Exception e)
            {
                Log.log.error("Could not index: " + wikiVO.getUFullname(), e);
            }
        }
        
        Log.log.info("Finished Indexing: " + sysIds.size() + " WikiDocs");
        //once all the wiki documents are indexed, update the search queries with their ratings
        IndexData<String> indexData = new IndexData<String>(Arrays.asList(lastIndexTime.toString()), OPERATION.RATE, username);
        queryIndexer.enqueue(indexData);
    }

    private boolean isWikiIndexable(WikiDocument wikiDocument)
    {
        boolean result = true;

        if(excludeNamespacePatternMap.containsKey(wikiDocument.getNamespace()))
        {
            if(Log.log.isDebugEnabled())
            {
                Log.log.debug("Document is not indexed because of search.exclude.namespace system property. namespace: " + wikiDocument.getNamespace());
            }
            result = false;
        }
        else if(excludeDocumentList.size() > 0)
        {
            for(String pattern : excludeDocumentList)
            {
                if(wikiDocument.getName().contains(pattern))
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Document is not indexed because of search.exclude.document.pattern system property. pattern: " + pattern);
                    }
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    private void setDeleteWikiDocuments(final Collection<String> sysIds, final boolean isDeleted, final String username) throws SearchException
    {
        if (sysIds == null || sysIds.isEmpty())
        {
            throw new SearchException("The sysIds list is null/empty, check the calling code why is it like that.");
        }
        else
        {
            // wikis are not really deleted but marked as deleted.
            for (String sysId : sysIds)
            {
                WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
                wikiDocument.setDeleted(isDeleted);
                indexWiki(wikiDocument, false, username);
            }
        }
    }

    private void purgeWikiDocuments(final Collection<String> sysIds, final String username) throws SearchException
    {
        if (sysIds == null || sysIds.isEmpty())
        {
            // purging all documents
            Log.log.info(String.format("Purging all Wiki documents requested by \"%s\"", username));
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            Log.log.debug("Purging all Wiki attachments");
            attachmentIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            Log.log.debug("Purging all Wiki documents");
            indexAPI.deleteByQueryBuilder(queryBuilder, username);
            
            documentIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            automationIndexAPI.deleteByQueryBuilder(queryBuilder, username);
        	decisionTreeIndexAPI.deleteByQueryBuilder(queryBuilder, username);
        	playbookIndexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
        else
        {
            for (String wikiDocumentId : sysIds)
            {
                Log.log.debug(String.format("Purging Wiki attachments for wiki document id %s.", wikiDocumentId));
                QueryBuilder queryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("wikiDocumentId", wikiDocumentId));
                attachmentIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            }
            Log.log.debug(String.format("Purging Wiki documents for wiki document ids [%s].", 
            							StringUtils.collectionToString(sysIds, ", ")));
            indexAPI.deleteDocumentByIds(sysIds, username);
            documentIndexAPI.deleteDocumentByIds(sysIds, username);
            automationIndexAPI.deleteDocumentByIds(sysIds, username);
        	decisionTreeIndexAPI.deleteDocumentByIds(sysIds, username);
        	playbookIndexAPI.deleteDocumentByIds(sysIds, username);
        }
    }
    
    /**
     * Very specific indexing mainly for setting various counts.
     * 
     * Example:
     * 
     * params.put("WIKI_ID", sysId);
     * params.put("OPERATION", "SET_CLICK_COUNT");
     * params.put("COUNT", clickCount);
     * params.put("USERNAME", username);
     * 
     * @param indexData
     */
    private void customIndex(IndexData indexData)
    {
        IndexData<Map<String, Object>> customIndexData = (IndexData<Map<String, Object>>) indexData;
        Collection<Map<String, Object>> customData = customIndexData.getObjects();
        try
        {
            for(Map<String, Object> customDataMap : customData)
            {
                String sysId = (String) customDataMap.get("WIKI_ID");
                String username = (String) customDataMap.get("USERNAME");
                WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
                if(wikiDocument != null)
                {
                    boolean doIndex = false;
                    if("SET_CLICK_COUNT".equalsIgnoreCase((String)customDataMap.get("OPERATION")) && customDataMap.get("COUNT") != null)
                    {
                        Long count = (Long) customDataMap.get("COUNT");
                        wikiDocument.setClickCount(count);
                        doIndex = true;
                    }
                    else if("SET_EDIT_COUNT".equalsIgnoreCase((String)customDataMap.get("OPERATION")) && customDataMap.get("COUNT") != null)
                    {
                        Long count = (Long) customDataMap.get("COUNT");
                        wikiDocument.setEditCount(count);
                        doIndex = true;
                    }
                    else if("SET_EXECUTE_COUNT".equalsIgnoreCase((String)customDataMap.get("OPERATION")) && customDataMap.get("COUNT") != null)
                    {
                        Long count = (Long) customDataMap.get("COUNT");
                        wikiDocument.setExecuteCount(count);
                        doIndex = true;
                    }
                    if(doIndex)
                    {
                        indexAPI.indexDocument(wikiDocument, username);
                    }
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
}
