package com.resolve.search.content;

import static com.resolve.search.SearchConstants.DOCUMENT_TYPE_ACTION_TASK;
import static com.resolve.search.SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM;
import static com.resolve.search.SearchConstants.DOCUMENT_TYPE_PROPERTY;
import static com.resolve.search.SearchConstants.INDEX_NAME_ACTION_TASK;
import static com.resolve.search.SearchConstants.INDEX_NAME_CUSTOM_FORM;
import static com.resolve.search.SearchConstants.INDEX_NAME_PROPERTY;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_DOCUMENT;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_AUTOMATION;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_DECISION_TREE;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_PLAYBOOK;

class SearchDefinition {
	
	private String index;
	private String type;

	public static final String[] ALL_INDEXES = { SEARCH_TYPE_DOCUMENT, //
			INDEX_NAME_ACTION_TASK, //
			INDEX_NAME_PROPERTY, //
			INDEX_NAME_CUSTOM_FORM, //
			SEARCH_TYPE_AUTOMATION, //
			SEARCH_TYPE_DECISION_TREE, //
			SEARCH_TYPE_PLAYBOOK 
	};

	public static final String[] ALL_INDEX_TYPES = { SEARCH_TYPE_DOCUMENT, //
			DOCUMENT_TYPE_ACTION_TASK, //
			DOCUMENT_TYPE_PROPERTY, //
			DOCUMENT_TYPE_CUSTOM_FORM, //
			SEARCH_TYPE_AUTOMATION, //
			SEARCH_TYPE_DECISION_TREE, //
			SEARCH_TYPE_PLAYBOOK 
	};

	SearchDefinition(String index, String type) {
		this.index = index;
		this.type = type;
	}
	
	public String getIndex() {
		return index;
	}
	
	public String getType() {
		return type;
	}
	
}