package com.resolve.search.model;

public class MetricJMSQueue extends MetricData
{
    private static final long serialVersionUID = -4890654875799523390L;
	private long msgsCount;
    
    
    public long getMsgsCount()
    {
        return msgsCount;
    }
    
    public void setMsgsCount(long c)
    {
        msgsCount = c;
    }

}
