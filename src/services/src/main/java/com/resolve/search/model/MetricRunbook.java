package com.resolve.search.model;

public class MetricRunbook extends MetricData
{
    private static final long serialVersionUID = -6524460191347147792L;
	private long aborted;
    private long completed;
    private long gcondition;
    private long bcondition;
    private long ucondition;
    private long gseverity;
    private long cseverity;
    private long wseverity;
    private long sseverity;
    private long useverity;
    private long duration;
    
    
    public long getAborted()
    {
        return aborted;
    }
    
    public void setAborted(long a)
    {
        aborted = a;
    }
    
    public long getCompleted()
    {
        return completed;
    }
    
    public void setCompleted( long s)
    {
        completed = s;
    }

    public long getGcondition()
    {
        return gcondition;
    }
    
    public void setGcondition(long g)
    {
        gcondition = g;
    }
    
    public long getBcondition()
    {
        return bcondition;
    }
    
    public void setBcondition(long b)
    {
        bcondition = b;
    }
    
    public long getUcondition()
    {
        return ucondition;
    }
    
    public void setUcondition(long u)
    {
        ucondition = u;
    }
    
    public long getGseverity()
    {
        return gseverity;
    }
    
    public void setGseverity(long g)
    {
        gseverity = g;
    }
    
    public long getCseverity()
    {
        return cseverity;
    }
    
    public void setCseverity(long c)
    {
        cseverity = c;
    }
    
    public long getSseverity()
    {
        return sseverity;
    }
    
    public void setSseverity(long w)
    {
        sseverity = w;
    }
    
    public long getWseverity()
    {
        return wseverity;
    }
    
    public void setWseverity(long w)
    {
        wseverity = w;
    }
    
    public long getUseverity()
    {
        return useverity;
    }
    
    public void setUseverity(long u)
    {
        useverity = u;
    }
    
    public long getDuration()
    {
        return duration;
    }
    
    public void setDuration(long d)
    {
        duration = d;
    }
    
}
