/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.dictionary;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchException;
import com.resolve.search.model.Word;
import com.resolve.search.model.WordSource;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;

public class DictionarySearchAPI
{
    private static final SearchAPI<Word> searchAPI = APIFactory.getDictionarySearchAPI();

    public static ResponseDTO<Word> getSuggestions(QueryDTO queryDTO, String username) throws SearchException
    {

        QueryBuilder queryBuilder = QueryBuilders.boolQuery().minimumNumberShouldMatch(1).should(QueryBuilders.fuzzyQuery("content", queryDTO.getSearchTerm()).maxExpansions(1).prefixLength(queryDTO.getSearchTerm().length() - 1));
        queryDTO.setSearchTerm(null);
        return searchAPI.searchByQuery(queryDTO, queryBuilder, username);
        /*
         * QueryBuilder queryBuilder = QueryBuilders.prefixQuery("word",
         * queryDTO.getSearchTerm());
         * 
         * queryDTO.addFilterItem(new QueryFilter("string",
         * queryDTO.getSearchTerm(), "word",
         * QueryDTO.QUERY_COMPARISION.startsWith.name()));
         * queryDTO.setSearchTerm(null); // return
         * SearchAPI.searchByQueryDTO(Dictionary.class, username,
         * SearchConstants.INDEX_NAME_DICTIONARY,
         * SearchConstants.DOCUMENT_TYPE_DICTIONARY, queryDTO); return
         * SearchAPI.searchByQueryDTO(Dictionary.class, username, new String[] {
         * SearchConstants.INDEX_NAME_DICTIONARY }, new String[] {
         * SearchConstants.DOCUMENT_TYPE_DICTIONARY }, queryDTO, queryBuilder,
         * null);
         */
        /*
         * List<Dictionary> result = new ArrayList<Dictionary>(); result.add(new
         * Dictionary("abc", 0)); result.add(new Dictionary("abd", 0));
         * result.add(new Dictionary("abe", 0)); result.add(new
         * Dictionary("acd", 0)); result.add(new Dictionary("add", 0));
         */
        // return result;
    }

    public static Word findWordById(String sysId, String dictionarySource, String username) throws SearchException
    {
        Word result = null;

        QueryDTO queryDTO = new QueryDTO(0, 1);
        QueryBuilder queryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("word", sysId));
        NestedQueryBuilder filter = QueryBuilders.nestedQuery("source", QueryBuilders.boolQuery().must(QueryBuilders.termQuery("source", dictionarySource)), ScoreMode.None);

        ResponseDTO<Word> response = searchAPI.searchByQuery(queryDTO, queryBuilder, new QueryBuilder[] { filter }, username);
        //guranteed to be only one record
        if (response.getRecords() != null && response.getRecords().size() > 0)
        {
            result = response.getRecords().get(0);
        }

        return result;
    }

    public static Word findWordById(String sysId, String username) throws SearchException
    {
        return searchAPI.findDocumentById(sysId, username);
    }

    public static long getFrequency(String dictionarySource, String[] words, String username) throws SearchException
    {
        long result = 0L;
        for (String word : words)
        {
            //Word dictionaryWord = findWordById(word, dictionarySource, username);
            Word dictionaryWord = findWordById(word, username);
            if (dictionaryWord != null)
            {
                WordSource wordSource = dictionaryWord.findSource(dictionarySource);
                if (wordSource != null)
                {
                    result += wordSource.getFrequency();
                }
            }
        }
        return result;
    }
}
