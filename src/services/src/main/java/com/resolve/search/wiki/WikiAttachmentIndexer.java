/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.wiki;

import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeSet;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.WikiAttachment;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class WikiAttachmentIndexer implements Indexer<IndexData<String>>
{
    private static volatile WikiAttachmentIndexer instance = null;
    private final IndexAPI indexAPI;
    private final SearchAPI<WikiAttachment> searchAPI;

    private final ConfigSearch config;
    private long indexThreads;

    private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

    private int attachmentLimitInBytes = 500000; //by default it is 500KB
    
    public static WikiAttachmentIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("WikiIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static WikiAttachmentIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new WikiAttachmentIndexer(config);
        }
        return instance;
    }

    private WikiAttachmentIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexAPI = APIFactory.getWikiAttachmentIndexAPI();
        searchAPI = APIFactory.getWikiAttachmentSearchAPI();
        this.indexThreads = this.config.getIndexthreads();
        
        //String attachmentLimitProperty = PropertiesUtil.getPropertyString("search.attachment.limit");
        String attachmentLimitProperty = config.getProperties().get("search.attachment.limit");
        if (StringUtils.isNumeric(attachmentLimitProperty))
        {
            attachmentLimitInBytes = Integer.parseInt(attachmentLimitProperty);
        }
    }

    public void init()
    {
        Log.log.debug("Initializing WikiAttachmentIndexer.");
        QueueListener<IndexData<String>> wikiAttachmentIndexListener = new IndexListener<IndexData<String>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiAttachmentIndexListener);
        Log.log.debug("WikiAttachmentIndexer initialized.");
    }

    public boolean enqueue(IndexData<String> indexData)
    {
        return dataQueue.offer(indexData);
    }

    @Override
    public boolean index(IndexData<String> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> sysIds = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexWikiAttachment(indexData.getParentId(), sysIds, indexData.getUsername());
                    break;
                case PURGE:
                    purgeWikiAttachments(sysIds, indexData.getSourceId(), indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    /**
     * Indexes wiki attachments.
     *
     * TODO need to improve based on observed performance. bulk?
     *
     * @param sysIds
     * @throws Exception
     */
    private void indexWikiAttachment(String wikiDocId, Collection<String> sysIds, String username) throws Exception
    {
        int pauseCounter = 0;

        WikiDocumentVO wikiVO = ServiceWiki.getWikiDoc(wikiDocId, null, null);
        TreeSet<String> roles = SearchUtils.parseRoles(wikiVO.getAccessRights().getUReadAccess(), ",");
        for (String sysId : sysIds)
        {
            WikiAttachmentVO wikiAttachmentVO = ServiceWiki.findWikiAttachmentVO(wikiVO.getUName(), sysId, null, false);
            WikiAttachment wikiAttachment = searchAPI.findDocumentById(sysId, wikiDocId, null, username);

            //if the there is no change in file size we do not want to index again.
            if(wikiAttachmentVO != null && (wikiAttachment == null || wikiAttachment.getSize() != wikiAttachmentVO.getUSize()))
            {
                WikiAttachment doc = new WikiAttachment();
                doc.setSysId(wikiAttachmentVO.getSys_id());
                doc.setSysOrg(wikiAttachmentVO.getSysOrg());
                doc.setReadRoles(roles);
                doc.setWikiDocumentId(wikiDocId);
                doc.setFilename(wikiAttachmentVO.getUFilename());
                doc.setSize((wikiAttachmentVO.getUSize() == null ? 0 : wikiAttachmentVO.getUSize()));
                doc.setSysCreatedBy(wikiAttachmentVO.getSysCreatedBy());
                if(wikiAttachmentVO.getSysCreatedOn() != null)
                {
                    doc.setSysCreatedOn(wikiAttachmentVO.getSysCreatedOn().getTime());
                }
                doc.setSysUpdatedBy(wikiAttachmentVO.getSysUpdatedBy());
                if(wikiAttachmentVO.getSysUpdatedOn() != null)
                {
                    doc.setSysUpdatedOn(wikiAttachmentVO.getSysUpdatedOn().getTime());
                }

                Log.log.debug("Indexing Wiki attachment : " + doc.getFilename());

                //we will not index a file bigger that 500 KB.
                if(wikiAttachmentVO.getUSize() != null && wikiAttachmentVO.getUSize() <= attachmentLimitInBytes)
                {
                    wikiAttachmentVO = ServiceWiki.getAttachmentWithContent(sysId, username);
                    if(wikiAttachmentVO.ugetWikiAttachmentContent() != null)
                    {
                        if(Log.log.isDebugEnabled())
                        {
                            Log.log.debug("Attachment size is within the allowed size (allowed : " + attachmentLimitInBytes + ", actual : " + wikiAttachmentVO.getUSize());
                        }
                        byte[] bytes = wikiAttachmentVO.ugetWikiAttachmentContent().getUContent();
                        String contentString = AttachmentUtil.getContent(wikiAttachmentVO.getUFilename(), bytes);
                        doc.setContent(contentString);
                    }
                }
                
                indexWikiAttachment(doc, username);

                if(pauseCounter > 10)
                {
                    pauseCounter = 0;
                    Thread.sleep(1000L);
                }
                pauseCounter++;
            }
        }
    }

    private void indexWikiAttachment(final WikiAttachment wikiAttachment, String username) throws SearchException
    {
        Collection<WikiAttachment> wikis = new LinkedList<WikiAttachment>();
        wikis.add(wikiAttachment);
        indexWikiAttachment(wikis, username);
    }

    private void indexWikiAttachment(final Collection<WikiAttachment> wikiAttachments, final String username) throws SearchException
    {
        indexAPI.indexChildDocuments("wikiDocumentId", SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, wikiAttachments, username, false);
    }

    private void purgeWikiAttachments(Collection<String> attachSysIds, String routingId, String username)
    {
        Log.log.debug("Purging WIKI attachments");
        try
        {
            if (attachSysIds != null && attachSysIds.size() > 0)
            {
                for (String attachId : attachSysIds)
                {
                    indexAPI.deleteDocumentById(attachId, routingId, username);
                }
            }
            //indexAPI.deleteDocumentByIds(attachSysIds, username);
        }
        catch (SearchException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }
}
