/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;

import com.resolve.search.model.query.Suggest;
import com.resolve.util.constants.HibernateConstants;

public class Tag extends BaseIndexModel implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String name;
    protected Suggest suggest;
    private String description;
    private int weight;
    
    public Tag()
    {
        super();
        //blank constructor, required for JSON serialization. 
    }

    public Tag(String name, int weight)
    {
        this();
        setName(name);
        addWeight(weight);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
        this.suggest = new Suggest(name, 1);
    }

    public int getWeight()
    {
        return weight;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    public Suggest getSuggest()
    {
        return suggest;
    }

    public void setSuggest(Suggest suggest)
    {
        this.suggest = suggest;
    }

    public void addWeight(int weight)
    {
        this.weight += weight;
        this.suggest.addWeight(weight);
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        Tag other = (Tag) obj;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Tag [name=" + name + ", description=" + description + ", toString()=" + super.toString() + "]";
    }
    
    public void repalceSpacialChars()
    {
        setName(getName().replaceAll(HibernateConstants.REGEX_TAG_NAME, "_"));
    }
}
