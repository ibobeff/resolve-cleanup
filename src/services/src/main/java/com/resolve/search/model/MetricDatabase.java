package com.resolve.search.model;

public class MetricDatabase extends MetricData
{
	private static final long serialVersionUID = 8963458816680128291L;
	
	private long freeSpace;
    private long size;
    private long percentUsed;
    private long responseTime;
    private long queryCount;
    private long percentWait;
    
    
	public long getFreeSpace()
	{
		return freeSpace;
	}
	public void setFreeSpace(long freeSpace)
	{
		this.freeSpace = freeSpace;
	}
	public long getSize()
	{
		return size;
	}
	public void setSize(long size)
	{
		this.size = size;
	}
	public long getPercentUsed()
	{
		return percentUsed;
	}
	public void setPercentUsed(long percentUsed)
	{
		this.percentUsed = percentUsed;
	}
	public long getResponseTime()
	{
		return responseTime;
	}
	public void setResponseTime(long responseTime)
	{
		this.responseTime = responseTime;
	}
	public long getQueryCount()
	{
		return queryCount;
	}
	public void setQueryCount(long queryCount)
	{
		this.queryCount = queryCount;
	}
	public long getPercentWait()
	{
		return percentWait;
	}
	public void setPercentWait(long percentWait)
	{
		this.percentWait = percentWait;
	}
    
    
    

}
