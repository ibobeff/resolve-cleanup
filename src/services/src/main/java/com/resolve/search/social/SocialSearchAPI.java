/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.social;

import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.index.query.functionscore.FieldValueFactorFunctionBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder.FilterFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.stats.StatsAggregationBuilder;

import com.resolve.search.APIFactory;
import com.resolve.search.QueryAPI;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.SocialComment;
import com.resolve.search.model.SocialCommentResponse;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.SocialPostResponse;
import com.resolve.search.model.UserInfo;
import com.resolve.search.query.UserQueryIndexAPI;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.UserStatDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialSearchAPI
{
    private static final SearchAPI<SocialPost> postSearchAPI = APIFactory.getSocialPostSearchAPI(SocialPost.class);
    private static final SearchAPI<SocialPostResponse> postResponseSearchAPI = APIFactory.getSocialPostSearchAPI(SocialPostResponse.class);
    private static final SearchAPI<SocialPost> systemSearchAPI = APIFactory.getSystemMessageSearchAPI(SocialPost.class);
    private static final SearchAPI<SocialPostResponse> systemResponseSearchAPI = APIFactory.getSystemMessageSearchAPI(SocialPostResponse.class);

    private static final SearchAPI<SocialComment> commentSearchAPI = APIFactory.getSocialCommentSearchAPI(SocialComment.class);
    private static final SearchAPI<SocialCommentResponse> commentResponseSearchAPI = APIFactory.getSocialCommentSearchAPI(SocialCommentResponse.class);
    private static final SearchAPI<SocialComment> systemCommentSearchAPI = APIFactory.getSystemMessageCommentSearchAPI(SocialComment.class);
    private static final SearchAPI<SocialCommentResponse> systemCommentResponseSearchAPI = APIFactory.getSystemMessageCommentSearchAPI(SocialCommentResponse.class);

    private static final String[] postSearchFields = new String[] { "title^4", "content^3", "authorUsername", "authorDisplayName" };
    private static final String[] postHighlightedFields = new String[] { "title" };
    private static final String[] commentSearchFields = new String[] { "content^2", "commentAuthorUsername", "commentAuthorDisplayName" };
    private static final String[] advancedSearchFields = new String[] { "title", "content", "authorUsername", "authorDisplayName", "socialcomment.content", "socialcomment.commentAuthorUsername", "socialcomment.commentAuthorDisplayName" };

    private static final QueryBuilder undeletedFilter = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("deleted", false));

    public static ResponseDTO<SocialPostResponse> searchPosts(TreeSet<String> targetIds, QueryDTO queryDTO, QueryBuilder queryBuilder, String streamType, boolean includeComments, boolean unreadOnly, UserInfo userInfo) throws SearchException
    {
        return searchPosts(targetIds, queryDTO, queryBuilder, streamType, includeComments, unreadOnly, userInfo, null);
    }

    /**
     * Searches social post by {@link QueryDTO}.
     * 
     * @param targetIds
     * @param queryDTO
     * @param includeComments
     * @param userInfo
     * @param postType
     * @return
     * @throws SearchException
     */
    public static ResponseDTO<SocialPostResponse> searchPosts(TreeSet<String> targetIds, QueryDTO queryDTO, QueryBuilder tmpQueryBuilder, String streamType, boolean includeComments, boolean unreadOnly, UserInfo userInfo, String postType) throws SearchException
    {
        ResponseDTO<SocialPostResponse> result = null;

        try
        {
            //TreeSet<String> readRoles = userInfo.getRoles();

            if (StringUtils.isNotBlank(queryDTO.getSearchTerm()))
            {
                BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
                
                //this makes sure at least post or comment has the search term(s)
                queryBuilder.minimumShouldMatch(1);

                //QueryBuilder for the "post"
                //this script adds additional boosting to the original score (_score) that was calculated based on the
                //field level boosting (check postSearchFields above). sysUpdatedOn makes sure that the latest posts gets more weight
                //String script = "_score * doc['numberOfComments'].value * doc['numberOfLikes'].value * doc['numberOfReads'].value * doc['sysUpdatedOn'].value";
                //ScoreFunctionBuilder scoreFunctionBuilder = ScoreFunctionBuilders.scriptFunction(script);
                FieldValueFactorFunctionBuilder scoreFunctionBuilder1 = ScoreFunctionBuilders.fieldValueFactorFunction("numberOfComments");
                FieldValueFactorFunctionBuilder scoreFunctionBuilder2 = ScoreFunctionBuilders.fieldValueFactorFunction("numberOfLikes");
                FieldValueFactorFunctionBuilder scoreFunctionBuilder3 = ScoreFunctionBuilders.fieldValueFactorFunction("numberOfReads");

                FunctionScoreQueryBuilder.FilterFunctionBuilder ffb1 = new FilterFunctionBuilder(new MatchAllQueryBuilder() , scoreFunctionBuilder1); 
                FunctionScoreQueryBuilder.FilterFunctionBuilder ffb2 = new FilterFunctionBuilder(new MatchAllQueryBuilder() , scoreFunctionBuilder2); 
                FunctionScoreQueryBuilder.FilterFunctionBuilder ffb3 = new FilterFunctionBuilder(new MatchAllQueryBuilder() , scoreFunctionBuilder3); 
                
                QueryBuilder multiMatchQueryBuilder = null;
                if(SearchUtils.isSimpleQueryString(queryDTO.getSearchTerm()))
                {
                    SimpleQueryStringBuilder simpleQueryStringBuilder = QueryBuilders.simpleQueryStringQuery(queryDTO.getSearchTerm());
                    for(String field : postSearchFields)
                    {
                        simpleQueryStringBuilder.field(field);
                    }
                    multiMatchQueryBuilder = simpleQueryStringBuilder;
                }
                else
                {
                    multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), postSearchFields);
                }
                //postMultiMatchQueryBuilder.fuzziness(".5");
                //postMultiMatchQueryBuilder.maxExpansions(5);
 
                //QueryBuilder for the "comment"
                //String commentScript = "_score * doc['numberOfLikes'].value;";
                //ScoreFunctionBuilder commentScoreFunctionBuilder = ScoreFunctionBuilders.scriptFunction(commentScript);
                ScoreFunctionBuilder commentScoreFunctionBuilder = ScoreFunctionBuilders.fieldValueFactorFunction("numberOfLikes");
                FunctionScoreQueryBuilder.FilterFunctionBuilder ffb4 = new FilterFunctionBuilder(new MatchAllQueryBuilder() , commentScoreFunctionBuilder); 
                
                QueryBuilder commentMultiMatchQueryBuilder = null;
                if(SearchUtils.isSimpleQueryString(queryDTO.getSearchTerm()))
                {
                    SimpleQueryStringBuilder simpleQueryStringBuilder = QueryBuilders.simpleQueryStringQuery(queryDTO.getSearchTerm());
                    for(String field : commentSearchFields)
                    {
                        simpleQueryStringBuilder.field(field);
                    }
                    commentMultiMatchQueryBuilder = simpleQueryStringBuilder;
                }
                else
                {
                    commentMultiMatchQueryBuilder = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), commentSearchFields);
                    //commentMultiMatchQueryBuilder.fuzziness(".5");
                    //commentMultiMatchQueryBuilder.maxExpansions(5);
                }
                
                //now add "post" and "comment" query to the final QueryBuilder.
                //queryBuilder.should(QueryBuilders.functionScoreQuery(multiMatchQueryBuilder).boostMode(CombineFunction.REPLACE).add(scoreFunctionBuilder1).add(scoreFunctionBuilder2).add(scoreFunctionBuilder3));
                queryBuilder.should(new FunctionScoreQueryBuilder(multiMatchQueryBuilder, new FilterFunctionBuilder[] { ffb1, ffb2, ffb3 }).boostMode(CombineFunction.REPLACE)); 
                
                //if we don't want any scoring on the comment then use the simple query as follows
                //queryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, QueryBuilders.matchQuery("content", queryDTO.getSearchTerm())));
                queryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, new FunctionScoreQueryBuilder(multiMatchQueryBuilder, new FilterFunctionBuilder[] { ffb4 }).boostMode(CombineFunction.REPLACE), ScoreMode.None));//.add(commentScoreFunctionBuilder)));
//                queryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, QueryBuilders.functionScoreQuery(commentMultiMatchQueryBuilder).boostMode(CombineFunction.REPLACE).add(commentScoreFunctionBuilder)));

                //FilterBuilder filterBuilder1 = FilterBuilders.termsFilter("readRoles", readRoles);
                QueryBuilder filterBuilder2 = QueryBuilders.termsQuery("targets.sysId", targetIds);
                QueryBuilder filterBuilderReadUsernames = null;
                if (unreadOnly)
                {
                    filterBuilderReadUsernames = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("readUsernames", userInfo.getUsername()));
                }

                result = postResponseSearchAPI.searchByTerm(new String[] {SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT}, SearchConstants.USER_QUERY_TYPE_SOCIAL, queryDTO, queryBuilder, new QueryBuilder[] {filterBuilder2, filterBuilderReadUsernames, undeletedFilter}, postHighlightedFields, userInfo);
            }
            else
            {
                // make sure to not retrieve deleted posts.
                //queryDTO.addFilterItem(new QueryFilter("bool", "false", "deleted", QueryFilter.EQUALS));

                QueryBuilder queryBuilder = null;
                if(tmpQueryBuilder == null)
                {
                    queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
                }
                else
                {
                    queryBuilder = tmpQueryBuilder;
                }
                
                // Now add the special targets and roles query here.
                //FilterBuilder readRolesFilter = FilterBuilders.termsFilter("readRoles", readRoles);
                QueryBuilder targetsFilter = QueryBuilders.termsQuery("targets.sysId", targetIds);
                QueryBuilder inboxExclusionFilter = getInboxExclusionFilter(userInfo);
                
                QueryBuilder filterBuilderReadUsernames = null;
                if (unreadOnly)
                {
                    filterBuilderReadUsernames = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("readUsernames", userInfo.getUsername()));
                }
                
                QueryBuilder followedUserFilter = null;
                //this is a special case where a user clicks on a user's 
                //name that she was following.
                if("user".equalsIgnoreCase(streamType))
                {
                    //this is when user clicks on a user she is following we only want to see the 
                    //followed user's blog post only
                   followedUserFilter = QueryBuilders.boolQuery().must(QueryBuilders.termsQuery("authorSysId", targetIds));    
                }
                
                if (SearchConstants.SOCIALPOST_TYPE_SYSTEM.equalsIgnoreCase(postType) 
                                || SearchConstants.SOCIALPOST_TYPE_MANUAL.equalsIgnoreCase(postType))
                {
                    result = systemResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, new QueryBuilder[] {targetsFilter, filterBuilderReadUsernames, undeletedFilter }, userInfo.getUsername());
                }
                else
                {
                    result = postResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, new QueryBuilder[] {targetsFilter, inboxExclusionFilter, filterBuilderReadUsernames, followedUserFilter, undeletedFilter }, userInfo.getUsername());
                }
            }
            // result has the SocialPost object as records.
            result.setRecords(preparePostResponse(result.getRecords(), includeComments, userInfo, postType));
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }

        return result;
    }

    public static ResponseDTO<SocialPostResponse> advancedSearch(TreeSet<String> targetIds, QueryDTO queryDTO, UserInfo userInfo) throws SearchException
    {
        ResponseDTO<SocialPostResponse> result = null;

        try
        {
            QueryBuilder targetsFilter = QueryBuilders.termsQuery("targets.sysId", targetIds);
            QueryBuilder inboxExclusionFilter = getInboxExclusionFilter(userInfo);

            //FilterBuilder rolesFilter = FilterBuilders.termsFilter("readRoles", userInfo.getRoles());
            QueryBuilder typeFilter = null;
            QueryBuilder sysUpdatedOnFilter = null;
            QueryBuilder sysCreatedOnFilter = null;
            QueryBuilder authorUsernameFilter = null;
            QueryBuilder commentAuthorUsernameFilter = null;
          
            QueryBuilder multiMatchQueryBuilder = null;
            QueryBuilder commentMultiMatchQueryBuilder = null;

            boolean isAdvanced = false;

            if(StringUtils.isBlank(queryDTO.getSearchTerm()))
            {
                //this is advanced search
                if(queryDTO.getFilterItems() != null && queryDTO.getFilterItems().size() > 0)
                {
                    for(QueryFilter queryFilter : queryDTO.getFilterItems())
                    {
                        if("tag".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                StringBuilder tags = new StringBuilder();
                                for(String tag : StringUtils.stringToList(queryFilter.getValue(), ","))
                                {
                                    tags.append(" #").append(tag);
                                }
                                queryDTO.setSearchTerm(tags.toString());
                                multiMatchQueryBuilder = createQueryBuilder(new QueryFilter("content", QueryFilter.EQUALS, tags.toString()), new String[] {"content"}, false);
                                commentMultiMatchQueryBuilder = createQueryBuilder(new QueryFilter("socialcomment.content", QueryFilter.EQUALS, tags.toString()), new String[] {"socialcomment.content"}, true);
                                isAdvanced = true;
                            }
                        }
                        else if("type".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                typeFilter = QueryBuilders.termsQuery("type", new TreeSet<String>(StringUtils.stringToList(queryFilter.getValue(), ",")));
                            }
                        }
                        else if("targetId".equalsIgnoreCase(queryFilter.getField()))
                        {
                            targetsFilter = QueryBuilders.termsQuery("targets.sysId", targetIds);
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                TreeSet<String> tmpTargetIds = new TreeSet<String>();
                                List<String> targets = StringUtils.stringToList(queryFilter.getValue(), ",");
                                //make sure that the user only searches the targets they have access.
                                //this API caller could send wrong targets and try to get data.
                                for(String targetId : targets)
                                {
                                    if(targetIds.contains(targetId))
                                    {
                                        tmpTargetIds.add(targetId);
                                    }
                                    else
                                    {
                                        Log.log.warn("User " + userInfo.getUsername() + " doesn't have access to " + targetId + " ignoring");
                                    }
                                }
                                targetsFilter = QueryBuilders.termsQuery("targets.sysId", tmpTargetIds);
                                isAdvanced = true;
                            }
                        }
                        else if("authorUsername".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                //authorUsernameFilter = FilterBuilders.termFilter("authorUsername", queryFilter.getValue());
                                authorUsernameFilter = QueryBuilders.boolQuery().should(QueryBuilders.termQuery("authorUsername", queryFilter.getValue())).should(QueryBuilders.matchQuery("authorDisplayName", queryFilter.getValue()));
                                isAdvanced = true;
                            }
                        }
                        else if("socialcomment.commentAuthorUsername".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                //commentAuthorUsernameFilter = FilterBuilders.hasChildFilter(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, FilterBuilders.termFilter("commentAuthorUsername", queryFilter.getValue()));
                                QueryBuilder commentAuthorNameFilter = QueryBuilders.boolQuery().should(QueryBuilders.termQuery("commentAuthorUsername", queryFilter.getValue())).should(QueryBuilders.matchQuery("commentAuthorDisplayName", queryFilter.getValue()));
                                commentAuthorUsernameFilter = QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, commentAuthorNameFilter, ScoreMode.None);
                                isAdvanced = true;
                            }
                        }
                        else if("sysUpdatedOn".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                sysUpdatedOnFilter = SearchUtils.createDateRangeFilterBuilder(queryFilter);
                                isAdvanced = true;
                            }
                        }
                        else if("sysCreatedOn".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                sysCreatedOnFilter = SearchUtils.createDateRangeFilterBuilder(queryFilter);
                                isAdvanced = true;
                            }
                        }
                        else
                        {
                            String[] fields = null;
                            if(StringUtils.isBlank(queryFilter.getField()))
                            {
                                fields = advancedSearchFields;
                            }
                            else
                            {
                                fields = StringUtils.stringToArray(queryFilter.getField(), ",");
                            }
                            
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                multiMatchQueryBuilder = createQueryBuilder(queryFilter, fields, false);
                                commentMultiMatchQueryBuilder = createQueryBuilder(queryFilter, fields, true);
                            }
                            queryDTO.setSearchTerm(queryFilter.getValue());
                            isAdvanced = true;
                        }
                    }
                }
                
                if(StringUtils.isBlank(queryDTO.getSearchTerm()) && !isAdvanced)
                {
                    //this condition is when user clicks on the search icon without
                    //any input at all and dthere was nothing in special filter.
                    result = searchPosts(targetIds, queryDTO, null, null, true, false, userInfo);
                }
                else
                {
                    QueryBuilder queryBuilder = null;
                    
                    if(StringUtils.isBlank(queryDTO.getSearchTerm()))
                    {
                        queryBuilder = QueryBuilders.matchAllQuery();
                    }
                    else
                    {
                        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
                        if(multiMatchQueryBuilder != null && commentMultiMatchQueryBuilder != null)
                        {
                            boolQueryBuilder.minimumShouldMatch(1);
                            //now add "post" and "comment" query to the final QueryBuilder.
                            boolQueryBuilder.should(QueryBuilders.functionScoreQuery(multiMatchQueryBuilder));
                            //add the comment filter
                            boolQueryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, commentMultiMatchQueryBuilder, ScoreMode.None));
                        }
                        else if(multiMatchQueryBuilder != null)
                        {
                            //now add "post" and "comment" query to the final QueryBuilder.
                            boolQueryBuilder.must(QueryBuilders.functionScoreQuery(multiMatchQueryBuilder));
                        }
                        else if(commentMultiMatchQueryBuilder != null)
                        {
                            //add the comment filter
                            boolQueryBuilder.must(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, commentMultiMatchQueryBuilder, ScoreMode.None));
                        }
                        queryBuilder = boolQueryBuilder;
                    }
                    result = postResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, new QueryBuilder[] { undeletedFilter, targetsFilter, inboxExclusionFilter, typeFilter, authorUsernameFilter, commentAuthorUsernameFilter, sysUpdatedOnFilter, sysCreatedOnFilter }, userInfo.getUsername());
    
                    // if the search term yielded some result then register it as user's query
                    if (result.getRecords() != null && result.getRecords().size() > 0 && StringUtils.isNotBlank(queryDTO.getSearchTerm()))
                    {
                        UserQueryIndexAPI.indexUserQuery(queryDTO.getSearchTerm(), SearchConstants.USER_QUERY_TYPE_SOCIAL, result.getRecords().size(), userInfo);
                        
                        //if this is the phrase selected from one of the suggesters then update ratings on that.
                        QueryAPI.addSelectCount(SearchConstants.USER_QUERY_TYPE_SOCIAL, queryDTO.getSearchTerm(), userInfo.getUsername());
                    }
                    
                    // result has the SocialPost object as records.
                    result.setRecords(preparePostResponse(result.getRecords(), true, userInfo, SearchConstants.SOCIALPOST_TYPE_POST));
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage(e.getMessage());
            throw new SearchException(e.getMessage(), e);
        }

        return result;
    }

    private static QueryBuilder getInboxExclusionFilter(UserInfo userInfo)
    {
        QueryBuilder inboxExclusionFilter = 
        		QueryBuilders.boolQuery().should(QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("targets.type", "user")))
        		.should(QueryBuilders.boolQuery().must(QueryBuilders.termQuery("targets.postType", SearchConstants.SOCIALPOST_TYPE_INBOX)).must(QueryBuilders.termQuery("targets.sysId", userInfo.getSysId()))
                .should(QueryBuilders.termQuery("targets.postType", SearchConstants.SOCIALPOST_TYPE_BLOG)));
        return inboxExclusionFilter;
    }

    private static QueryBuilder createQueryBuilder(QueryFilter queryFilter, String[] searchFields, boolean isComment)
    {
        QueryBuilder result = null;
        String searchTerm = queryFilter.getValue();
        boolean isPostFieldSupplied = false;
        boolean isCommentFieldSupplied = false;
        if(SearchUtils.isSimpleQueryString(searchTerm))
        {
            SimpleQueryStringBuilder queryBuilder = QueryBuilders.simpleQueryStringQuery(searchTerm);
            for(String field : searchFields)
            {
                 if(isComment && field.toLowerCase().startsWith("socialcomment."))
                 {
                     if(field.toLowerCase().startsWith("socialcomment."))
                     {
                         queryBuilder.field(field.substring(14));
                         isCommentFieldSupplied = true;
                     }
                 }
                 else if(!isComment && !field.toLowerCase().startsWith("socialcomment."))
                 {
                     queryBuilder.field(field);
                     isPostFieldSupplied = true;
                 }
            }
            result = queryBuilder;
        }
        else
        {
            MultiMatchQueryBuilder queryBuilder = QueryBuilders.multiMatchQuery(searchTerm);
            for(String field : searchFields)
            {
                 if(isComment && field.toLowerCase().startsWith("socialcomment."))
                 {
                     queryBuilder.field(field.substring(14));
                     isCommentFieldSupplied = true;
                 }
                 else if(!isComment && !field.toLowerCase().startsWith("socialcomment."))
                 {
                     queryBuilder.field(field);
                     isPostFieldSupplied = true;
                 }
            }
            result = queryBuilder;
        }
        if((isComment && !isCommentFieldSupplied) || (!isComment && !isPostFieldSupplied))
        {
            result = null;
        }

        return result;
    }
    
    /**
     * This method will go through the List of socialPosts and add additional
     * data in SocialPostResponse object.
     * 
     * @param socialPosts
     * @return
     */
    public static List<SocialPostResponse> preparePostResponse(List<SocialPostResponse> socialPosts, boolean includeComments, UserInfo userInfo, String postType) throws Exception
    {
        List<SocialPostResponse> result = new ArrayList<SocialPostResponse>();

        if (socialPosts != null)
        {
            for (SocialPostResponse response : socialPosts)
            {
                result.add(setPost(response, includeComments, userInfo, postType));
            }
        }

        return result;
    }

    private static SocialPostResponse setPost(SocialPostResponse response, boolean includeComments, UserInfo userInfo, String postType) throws SearchException
    {
        response.setUsername(userInfo.getUsername());
        response.setHasDeleteRights(hasDeleteRights(response, userInfo));
        // now do aggregation.
        setLiked(response, userInfo.getUsername());
        setRead(response, userInfo.getUsername());
        setStarred(response, userInfo.getUsername());
        if (includeComments)
        {
            setComments(response, userInfo.getUsername(), postType);
        }

        return response;
    }

    private static void setComments(SocialPostResponse response, String username, String postType) throws SearchException
    {
        int limit = Long.valueOf(response.getNumberOfComments()).intValue();
        
        ResponseDTO<SocialCommentResponse> responseDTO = searchCommentsByPostId(response.getSysId(), 0, limit, username, postType);

        if (responseDTO.getRecords() != null)
        {
            response.getComments().clear();
            for (Object comment : responseDTO.getRecords())
            {
                SocialCommentResponse commentResponse = (SocialCommentResponse) comment;
                prepareCommentResponse(commentResponse, username);

                response.addComments(commentResponse);
            }
        }
    }

    private static void prepareCommentResponse(SocialCommentResponse response, String username)
    {
        boolean isLiked = response.getLikedUsernames().contains(username);

        int numberOfLikes = response.getLikedUsernames().size();
        response.setNumberOfLikes(numberOfLikes);
        response.setLiked(isLiked);
    }

    private static void setLiked(SocialPostResponse response, String username)
    {
        boolean isLiked = response.getLikedUsernames().contains(username);
        response.setLiked(isLiked);
    }

    private static void setRead(SocialPostResponse response, String username)
    {
        boolean isRead = response.getReadUsernames().contains(username);
        int numberOfReads = response.getReadUsernames().size();
        if (isRead)
        {
            // do not count self read?
            numberOfReads -= 1;
        }
        response.setNumberOfReads(numberOfReads);
        response.setRead(isRead);
    }

    private static void setStarred(SocialPostResponse response, String username)
    {
        boolean isStarred = response.getStarredUsernames().contains(username);
        int numberOfStarred = response.getStarredUsernames().size();
        if (isStarred)
        {
            // do not count self read?
            numberOfStarred -= 1;
        }
        response.setNumberOfStarred(numberOfStarred);
        response.setStarred(isStarred);
    }

    /**
     * Checks if the user has right to delete this post.
     * 
     * @param socialPostResponse
     * @param userInfo
     * @return
     */
    public static boolean hasDeleteRights(SocialPostResponse socialPostResponse, UserInfo userInfo)
    {
        Boolean result = false;

        if (userInfo.isAdmin() || socialPostResponse.getAuthorUsername().equalsIgnoreCase(userInfo.getUsername()))
        {
            result = true;
        }

        return result;
    }

    /**
     * Provides social interaction statistics for a particular user.
     * 
     * Example: a) Total number of posts. b) Total number of comments c) Total
     * number of comments received d) Total number of likes received
     * 
     * @param username
     * @return
     * @throws SearchException
     */
    public static UserStatDTO getUserStats(final String username) throws SearchException
    {
        UserStatDTO result = new UserStatDTO();
        result.setUsername(username);

        // Total number of posts
        // This query goes after the socialpost documents and find all the
        // documents where the user is the author.
        QueryDTO queryDTO = new QueryDTO(0, 1);
        queryDTO.addFilterItem(new QueryFilter("authorUsername", QueryFilter.EQUALS, username));
        long numberOfPostContributions = postResponseSearchAPI.getCountByQuery(queryDTO);
        result.setPostContributions(numberOfPostContributions);

        // Total number of comments
        // This query goes after the socialcomment documents and find the total
        // count where the user is the author.
        queryDTO = new QueryDTO(0, 1);
        queryDTO.addFilterItem(new QueryFilter("commentAuthorUsername", QueryFilter.EQUALS, username));
        long numberOfCommentContributions = postResponseSearchAPI.getCountByQuery(queryDTO);
        result.setCommentContributions(numberOfCommentContributions);

        // Total number of comments received
        // This query goes after the socialcomment documents and find the total
        // count by searching for the post's author username but excludes the
        // self comment.
        queryDTO = new QueryDTO(0, 1);
        queryDTO.addFilterItem(new QueryFilter("postAuthorUsername", QueryFilter.EQUALS, username))
            .addFilterItem(new QueryFilter("commentAuthorUsername", QueryFilter.NOT_EQUALS, username));
        // FilterBuilder filterBuilder =
        // FilterBuilders.boolFilter().mustNot(FilterBuilders.termFilter("commentAuthorUsername",
        // username));

        long numberOfCommentsReceived = postResponseSearchAPI.getCountByQuery(queryDTO);
        result.setCommentsReceived(numberOfCommentsReceived);

        // Total number of likes received for post
        // This query goes after the socialpost documents and find the total
        // likes count where the user is the author. Runs a facet on the query
        // to get sum of
        // the numberOfLikes field.
        queryDTO = new QueryDTO(0, 1);
        queryDTO.addFilterItem(new QueryFilter("authorUsername", QueryFilter.EQUALS, username));
        QueryBuilder queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
        //FacetBuilder facetBuilder = FacetBuilders.statisticalFacet("likes_received_facet_post").field("numberOfLikes");
        //List<FacetBuilder> facetBuilders = new LinkedList<FacetBuilder>();
        //facetBuilders.add(facetBuilder);
        StatsAggregationBuilder statsBuilder = AggregationBuilders.stats("likes_received_by_post").field("numberOfLikes");
        //List<StatsBuilder> statsBuilders = new LinkedList<StatsBuilder>();
        //statsBuilders.add(statsBuilder);

        Map<String, Long> statisticsForPost = postResponseSearchAPI.getStatistics(queryBuilder, statsBuilder, username);
        // the getStatistics() method gurantees that we'll get back the result
        // with "total"
        long numberOfLikesReceivedForPost = statisticsForPost.get("total");
        result.setLikesReceivedForPost(numberOfLikesReceivedForPost);

        // Total number of likes received for comment
        // This query goes after the socialcomment documents and find the total
        // likes count where the user is the author. Runs a facet on the query
        // to get sum of
        // the numberOfLikes field.
        queryDTO = new QueryDTO(0, 1);
        queryDTO.addFilterItem(new QueryFilter("commentAuthorUsername", QueryFilter.EQUALS, username));
        queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
        //facetBuilder = FacetBuilders.statisticalFacet("likes_received_facet_comment").field("numberOfLikes");
        //facetBuilders = new LinkedList<FacetBuilder>();
        //facetBuilders.add(facetBuilder);
        StatsAggregationBuilder statsBuilder1 = AggregationBuilders.stats("likes_received_by_comment").field("numberOfLikes");
        //List<StatsBuilder> statsBuilders = new LinkedList<StatsBuilder>();
        //statsBuilders.add(statsBuilder);

        Map<String, Long> statisticsForComment = postResponseSearchAPI.getStatistics(queryBuilder, statsBuilder1, username);
        // the getStatistics() method gurantees that we'll get back the result
        // with "total"
        long numberOfLikesReceivedForCOmment = statisticsForComment.get("total");
        result.setLikesReceivedForComment(numberOfLikesReceivedForCOmment);

        return result;
    }

    public static ResponseDTO<SocialPostResponse> searchPost(String streamId, String streamType, TreeSet<String> targetIds, QueryDTO queryDTO, boolean includeComments, boolean unreadOnly, Long startDateTime, Long endDateTime, UserInfo userInfo) throws Exception
    {
        ResponseDTO<SocialPostResponse> result = new ResponseDTO<SocialPostResponse>();
        String postType = null;

        BoolQueryBuilder queryBuilder = null;
        if(queryDTO != null && queryDTO.getFilterItems().size() > 0)
        {
            queryBuilder = (BoolQueryBuilder) SearchUtils.createBoolQueryBuilder(queryDTO);
        }
        else
        {
            queryBuilder = QueryBuilders.boolQuery();
        }
        
        queryBuilder.must(QueryBuilders.matchQuery("deleted", false));
        if(startDateTime != null && endDateTime != null)
        {
            queryBuilder.must(rangeQuery("sysUpdatedOn").from(startDateTime).to(endDateTime).includeLower(true).includeUpper(true));
        }
        if (unreadOnly)
        {
            queryBuilder.mustNot(QueryBuilders.termsQuery("readUsernames", userInfo.getUsername()));
        }

        if (SearchConstants.DISPLAY_CATEGORY_SYSTEM.equalsIgnoreCase(streamId))
        {
            queryBuilder.must(QueryBuilders.boolQuery().minimumShouldMatch("1")
                            .should(QueryBuilders.matchQuery("type", SearchConstants.SOCIALPOST_TYPE_MANUAL))
                            .should(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("type", SearchConstants.SOCIALPOST_TYPE_SYSTEM)).must(QueryBuilders.matchQuery("targets.sysId", userInfo.getSysId()))));
            result = systemResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, userInfo.getUsername());
            result.setRecords(SocialSearchAPI.preparePostResponse(result.getRecords(), includeComments, userInfo, postType));
        }
        else if (SearchConstants.SOCIALPOST_TYPE_MANUAL.equalsIgnoreCase(streamId))
        {
            //manual posts are a type of system notification posted by someone like
            //a system administrator and broadcast to everyone in the network.
            queryBuilder.must(QueryBuilders.boolQuery().minimumShouldMatch("1")
                            .should(QueryBuilders.matchQuery("type", SearchConstants.SOCIALPOST_TYPE_MANUAL)));
            result = systemResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, userInfo.getUsername());
            result.setRecords(SocialSearchAPI.preparePostResponse(result.getRecords(), includeComments, userInfo, postType));
        }
        else
        {
            if (SearchConstants.DISPLAY_CATEGORY_ALL.equalsIgnoreCase(streamId))
            {
                queryBuilder.mustNot(QueryBuilders.matchQuery("type", "rss"));
                result = searchPosts(targetIds, queryDTO, queryBuilder, null, includeComments, unreadOnly, userInfo);
            }
            else if (SearchConstants.DISPLAY_CATEGORY_ACTIVITY.equalsIgnoreCase(streamId))
            {
                queryBuilder.mustNot(QueryBuilders.matchQuery("type", "rss"))
                    .must(QueryBuilders.rangeQuery("numberOfComments").gt(0L))
                    .minimumShouldMatch(1)
                    .should(QueryBuilders.matchQuery("authorUsername", userInfo.getUsername()))
                    .should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, QueryBuilders.boolQuery()
                    .should(QueryBuilders.matchQuery("commentAuthorUsername", userInfo.getUsername())), ScoreMode.None));
                result = postResponseSearchAPI.searchByQuery(new String[] { SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT }, queryDTO, queryBuilder, userInfo.getUsername());
            }
            else if (SearchConstants.DISPLAY_CATEGORY_STARRED.equalsIgnoreCase(streamId))
            {
                queryBuilder.must(QueryBuilders.termQuery("socialpost.starredUsernames", userInfo.getUsername()));
                result = postResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, userInfo.getUsername());
            }
            else if (SearchConstants.DISPLAY_CATEGORY_INBOX.equalsIgnoreCase(streamId))
            {
                queryBuilder.must(QueryBuilders.matchQuery("targets.sysId", userInfo.getSysId())).must(QueryBuilders.matchQuery("targets.type", "USER")).mustNot(QueryBuilders.matchQuery("authorSysId", userInfo.getSysId()));
                result = postResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, userInfo.getUsername());
            }
            else if (SearchConstants.DISPLAY_CATEGORY_BLOG.equalsIgnoreCase(streamId))
            {
                queryBuilder.must(QueryBuilders.matchQuery("authorSysId", userInfo.getSysId())).must(QueryBuilders.matchQuery("targets.sysId", userInfo.getSysId()));
                result = postResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, userInfo.getUsername());
            }
            else if (SearchConstants.DISPLAY_CATEGORY_SENT.equalsIgnoreCase(streamId))
            {
                queryBuilder.must(QueryBuilders.matchQuery("authorSysId", userInfo.getSysId()));
                result = postResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, userInfo.getUsername());
            }
            else if (SearchConstants.DISPLAY_CATEGORY_NAMESPACE.equalsIgnoreCase(streamType))
            {
                queryBuilder.must(QueryBuilders.matchQuery("targets.namespace", streamId));
                result = postResponseSearchAPI.searchByQuery(queryDTO, queryBuilder, userInfo.getUsername());
            }

            result.setRecords(SocialSearchAPI.preparePostResponse(result.getRecords(), includeComments, userInfo, postType));
        }

        return result;
    }

    public static Set<String> getUserIdsWhoLikedThis(String id, String username)
    {
        Set<String> userSysId = new TreeSet<String>();
        try
        {
            SocialPost socialPost = findSocialPostById(id, username);
            if (socialPost == null)
            {
                // check in comment
                SocialComment socialComment = findSocialCommentById(id, username);
                userSysId.addAll(socialComment.getLikedUsernames());
            }
            else
            {
                userSysId.addAll(socialPost.getLikedUsernames());
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return userSysId;
    }

    public static Map<String, Long> getUnreadPostCount(QueryDTO queryDTO, TreeSet<String> streamIds, UserInfo userInfo) throws SearchException
    {
        Map<String, Long> result = new HashMap<String, Long>();

        if(queryDTO == null)
        {
            queryDTO = new QueryDTO(0, 1);
        }
        
        long count = 0;

        // these filters are mostly common to everything
        QueryBuilder filterBuilderNotDeleted = QueryBuilders.termQuery("deleted", false);
        //FilterBuilder filterBuilderReadRoles = FilterBuilders.termsFilter("readRoles", userInfo.getRoles());
        QueryBuilder filterBuilderTargetIds = QueryBuilders.termsQuery("targets.sysId", streamIds);
        QueryBuilder filterBuilderInboxExclusion = getInboxExclusionFilter(userInfo);        
        QueryBuilder filterBuilderReadUsernames = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("readUsernames", userInfo.getUsername()));

        QueryBuilder filterBuilderUpdatedAfter = null;
        
        if (queryDTO.getFilterItems() != null && !queryDTO.getFilterItems().isEmpty())
        {
            for (QueryFilter queryFilter : queryDTO.getFilterItems())
            {
                if (queryFilter.isDateType() && queryFilter.getCondition().equalsIgnoreCase(QueryFilter.AFTER) &&
                    queryFilter.getField().equals("sysUpdatedOn"))
                {
                    filterBuilderUpdatedAfter = SearchUtils.createDateRangeFilterBuilder(queryFilter);
                    break;
                }
            }
        }
        
        // get "system" posts' unread count
        
        BoolQueryBuilder queryBuilderx = QueryBuilders.boolQuery()
                        .must(QueryBuilders.matchQuery("deleted", false))
                        .minimumShouldMatch("1")
                        .should(QueryBuilders.matchQuery("type", SearchConstants.SOCIALPOST_TYPE_MANUAL))
                        .should(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("type", SearchConstants.SOCIALPOST_TYPE_SYSTEM)).must(QueryBuilders.matchQuery("targets.sysId", userInfo.getSysId())));
        
        if (filterBuilderUpdatedAfter != null)
        {
            count = systemSearchAPI.getCountByQuery(queryBuilderx, filterBuilderReadUsernames, filterBuilderUpdatedAfter);
        }
        else
        {
            count = systemSearchAPI.getCountByQuery(queryBuilderx, filterBuilderReadUsernames);
        }
        
        result.put(SearchConstants.DISPLAY_CATEGORY_SYSTEM, count);

        // get "all"
        QueryBuilder filterBuilderRSS = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("type", SearchConstants.SOCIALPOST_TYPE_RSS));
        if (filterBuilderUpdatedAfter != null)
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderTargetIds, filterBuilderInboxExclusion, filterBuilderReadUsernames, filterBuilderRSS, filterBuilderUpdatedAfter);
        }
        else
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderTargetIds, filterBuilderInboxExclusion, filterBuilderReadUsernames, filterBuilderRSS);
        }
        result.put(SearchConstants.DISPLAY_CATEGORY_ALL, count);

        // get "activity", this is a special one which is complex, hence the use
        // of QueryBuilder may revisit later to use FilterBuilder
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("deleted", false)).mustNot(QueryBuilders.matchQuery("type", "rss")).must(QueryBuilders.rangeQuery("numberOfComments").gt(0L)).minimumShouldMatch(1).should(QueryBuilders.matchQuery("authorUsername", userInfo.getUsername()))
                        .should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, QueryBuilders.boolQuery().should(QueryBuilders.matchQuery("commentAuthorUsername", userInfo.getUsername())), ScoreMode.None));
        queryBuilder.mustNot(QueryBuilders.termsQuery("readUsernames", userInfo.getUsername()));

        if (filterBuilderUpdatedAfter != null)
        {
            count = postResponseSearchAPI.getCountByQuery(new String[] { SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT }, queryBuilder, filterBuilderUpdatedAfter);
        }
        else
        {
            count = postResponseSearchAPI.getCountByQuery(new String[] { SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT }, queryBuilder, new QueryBuilder[0]);
        }
        result.put(SearchConstants.DISPLAY_CATEGORY_ACTIVITY, count);

        // get "starred"
        QueryBuilder filterBuilderStarred = QueryBuilders.termQuery("socialpost.starredUsernames", userInfo.getUsername());
        if (filterBuilderUpdatedAfter != null)
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderTargetIds, filterBuilderInboxExclusion, filterBuilderReadUsernames, filterBuilderStarred, filterBuilderUpdatedAfter);
        }
        else
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderTargetIds, filterBuilderInboxExclusion, filterBuilderReadUsernames, filterBuilderStarred);
        }
        result.put(SearchConstants.DISPLAY_CATEGORY_STARRED, count);

        // get "inbox", inbox is where target is USER but the author and
        // target's sysIds are different.
        QueryBuilder filterBuilderNotAuthorSysId = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("authorSysId", userInfo.getSysId()));
        QueryBuilder filterBuilderInbox = QueryBuilders.termQuery("targets.sysId", userInfo.getSysId());
        QueryBuilder filterBuilderUserPostType = QueryBuilders.termQuery("targets.type", "USER");
        if (filterBuilderUpdatedAfter != null)
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderNotAuthorSysId, filterBuilderInbox, filterBuilderReadUsernames, filterBuilderUserPostType, filterBuilderUpdatedAfter);
        }
        else
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderNotAuthorSysId, filterBuilderInbox, filterBuilderReadUsernames, filterBuilderUserPostType);
        }
        result.put(SearchConstants.DISPLAY_CATEGORY_INBOX, count);

        // get "blog", blog is where target and author's sysIds are same.
        QueryBuilder filterBuilderAuthorSysId = QueryBuilders.termQuery("authorSysId", userInfo.getSysId());
        QueryBuilder filterBuilderTargetSysId = QueryBuilders.termQuery("targets.sysId", userInfo.getSysId());
        if (filterBuilderUpdatedAfter != null)
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderReadUsernames, filterBuilderAuthorSysId, filterBuilderTargetSysId, filterBuilderUpdatedAfter);
        }
        else
        {
            count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderReadUsernames, filterBuilderAuthorSysId, filterBuilderTargetSysId);
        }
        result.put(SearchConstants.DISPLAY_CATEGORY_BLOG, count);

        return result;
    }

    public static long countUnreadPostByStreamId(String streamId, String streamType, UserInfo userInfo) throws SearchException
    {
        QueryDTO queryDTO = new QueryDTO(0, 1);

        // Retrieving these type of data (count) is extremely efficient by
        // FilterBuilder
        QueryBuilder filterBuilderNotDeleted = QueryBuilders.termQuery("deleted", false);
        //FilterBuilder filterBuilderReadRoles = FilterBuilders.termsFilter("readRoles", userInfo.getRoles());
        QueryBuilder filterBuilderTargetIds = null;
        QueryBuilder filterBuilderInboxExclusion = getInboxExclusionFilter(userInfo);

        QueryBuilder filterBuilderNamespace = null;
        
        QueryBuilder filterBuilderReadUsernames = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("readUsernames", userInfo.getUsername()));

        if(SearchConstants.DISPLAY_CATEGORY_NAMESPACE.equalsIgnoreCase(streamType))
        {
            filterBuilderNamespace = QueryBuilders.termsQuery("targets.namespace", streamId);
        }
        else
        {
            TreeSet<String> targetIds = new TreeSet<String>();
            targetIds.add(streamId);
            filterBuilderTargetIds = QueryBuilders.termsQuery("targets.sysId", targetIds);
        }
        
        long count = postResponseSearchAPI.getCountByQuery(queryDTO, filterBuilderNotDeleted, filterBuilderTargetIds, filterBuilderInboxExclusion, filterBuilderReadUsernames, filterBuilderNamespace);

        /*
         * the following are sample that we could have used to get count but
         * retriving data by query is a bit expensive because ES wants to do
         * scoring etc on the result, hence this approach is abandoned but left
         * for just reference. BoolQueryBuilder queryBuilder =
         * QueryBuilders.boolQuery();
         * queryBuilder.must(QueryBuilders.termsQuery("readRoles",
         * userInfo.getRoles())); TreeSet<String> targetIds = new
         * TreeSet<String>(); targetIds.add(streamId);
         * queryBuilder.must(QueryBuilders.termsQuery("targets.sysId",
         * targetIds));
         * queryBuilder.mustNot(QueryBuilders.termQuery("socialpost.readUsernames"
         * , userInfo.getUsername()));
         * 
         * long count =
         * SearchAPI.getCountByQuery(SearchConstants.INDEX_NAME_SOCIAL_POST,
         * SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, queryBuilder);
         */

        return count;
    }

    public static SocialComment findSocialCommentById(String id, String username) throws SearchException
    {
        SocialComment result = null;

        QueryDTO queryDTO = new QueryDTO();
        queryDTO.setStart(0);
        queryDTO.setLimit(1);
        queryDTO.addFilterItem(new QueryFilter("auto", id, "sysId", QueryFilter.EQUALS, true));
        // this will find the comment accross the social indexes.
        ResponseDTO<SocialComment> response = commentSearchAPI.searchByQuery(queryDTO, username);
        if (response != null && response.getRecords().size() == 1)
        {
            result = response.getRecords().get(0);
        }
        return result;
    }

    public static ResponseDTO<SocialCommentResponse> searchCommentsByPostId(String postId, int start, int limit, String username, String postType) throws SearchException
    {
        ResponseDTO<SocialCommentResponse> result = null;

        try
        {
            QueryDTO queryDTO = new QueryDTO(start, limit);
            queryDTO.addFilterItem(new QueryFilter("string", postId, "postId", QueryFilter.EQUALS, true));
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_CREATED_ON, QuerySort.SortOrder.ASC);

            result = searchComments(queryDTO, username, postType);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Searches social post by {@link QueryDTO}.
     * 
     * queryDTO.postId = <sysId of a post>
     * 
     * @param queryDTO
     * @return a {@link ResponseDTO} with the collection of
     *         {@link SocialComment} that matched based on the query.
     * @throws SearchException
     */
    public static ResponseDTO<SocialCommentResponse> searchComments(QueryDTO queryDTO, String username, String postType) throws SearchException
    {
        ResponseDTO<SocialCommentResponse> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_CREATED_ON, QuerySort.SortOrder.ASC);
            result = commentResponseSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static SocialPost findSocialPostById(String sysId, String username) throws SearchException
    {
        SocialPost socialPost = findSocialPostById(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, sysId, username);
        if (socialPost == null)
        {
            // search in system index
            socialPost = systemSearchAPI.findDocumentById(sysId, username);
        }
        return socialPost;
    }

    public static SocialPost findSocialPostById(String indexName, String documentType, String sysId, String username) throws SearchException
    {
        return postSearchAPI.findDocumentById(sysId, username);
    }

    public static ResponseDTO<SocialPostResponse> searchPostsByBucket(String streamId, String streamType, TreeSet<String> targetIds, QueryDTO queryDTO, boolean includeComments, boolean unreadOnly, Long startTimesInMillis, Long endTimesInMillis, UserInfo userInfo) throws SearchException
    {
        ResponseDTO<SocialPostResponse> result = null;
        try
        {
            result = searchPost(streamId, streamType, targetIds, queryDTO, includeComments, unreadOnly, startTimesInMillis, endTimesInMillis, userInfo);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }

        return result;
    }

    public static SocialPostResponse findSocialPostResponseById(String id, UserInfo userInfo) throws SearchException
    {
        SocialPostResponse response = postResponseSearchAPI.findDocumentById(id, userInfo.getUsername());
        if(response == null)
        {
            response = systemResponseSearchAPI.findDocumentById(id, userInfo.getUsername());
        }
        
        if(response != null)
        {
            response = setPost(response, true, userInfo, response.getType());
        }
        
        return response;
    }


    public static SocialPost findRSSByUri(String uri, String username) throws SearchException
    {

        SocialPost result = null;
        if (StringUtils.isNotBlank(uri))
        {
            QueryBuilder queryBuilder = QueryBuilders.matchQuery("uri", uri.trim());

            ResponseDTO<SocialPost> data = postSearchAPI.searchByQuery(new QueryDTO(0,  1), queryBuilder, username);
            if (data.getRecords() != null && data.getRecords().size() > 0)
            {
                result = data.getRecords().get(0);
            }
        }
        return result;
    }
}
