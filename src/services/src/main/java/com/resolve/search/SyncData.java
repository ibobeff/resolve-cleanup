/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.index.query.QueryBuilder;

public class SyncData<T> implements Serializable
{
    private static final long serialVersionUID = 1L;

    private final String syncLogIndexName;
    private final String indexName;
    private final String documentType;
    private final SearchUtils.SYNC_OPERATION operation;
    private String id;
    private String parentId;
    private String routingKey;
    private Long timestamp;
    private T object;
    private QueryBuilder queryBuilder;
    private String script;
    private Map<String, Object> scriptParams;
    private Map<Pair<String, String>, List<String>> bulkReq;

    private String username;

    public SyncData(String syncLogIndexName, String indexName, String documentType, SearchUtils.SYNC_OPERATION operation, String parentId, String routingKey, String id, long timestamp, T object, String username)
    {
        this.syncLogIndexName = syncLogIndexName;
        this.indexName = indexName;
        this.documentType = documentType;
        this.operation = operation;
        this.parentId = parentId;
        this.routingKey = routingKey;
        this.id = id;
        this.timestamp = timestamp;
        this.object = object;
        this.username = username;
    }

    public SyncData(String syncLogIndexName, String indexName, String documentType, String parentId, String routingKey, String id, long timestamp, String script, Map<String, Object> scriptParams, String username)
    {
        this.syncLogIndexName = syncLogIndexName;
        this.indexName = indexName;
        this.documentType = documentType;
        this.operation = SearchUtils.SYNC_OPERATION.UPDATE_BY_SCRIPT;
        this.parentId = parentId;
        this.routingKey = routingKey;
        this.id = id;
        this.timestamp = timestamp;
        this.script = script;
        this.scriptParams = scriptParams;
        this.username = username;
    }
    
    public SyncData(Map<Pair<String, String>, List<String>> bulkReq, String username) {
    	syncLogIndexName = "";
    	indexName = "";
    	documentType = "";
    	operation = SearchUtils.SYNC_OPERATION.BULK_DELETE;
    	this.bulkReq = bulkReq;
    	this.username = username;
    }
    
    public String getSyncLogIndexName()
    {
        return syncLogIndexName;
    }

    public String getIndexName()
    {
        return indexName;
    }

    public String getDocumentType()
    {
        return documentType;
    }

    public T getObject()
    {
        return object;
    }

    public void setObject(T object)
    {
        this.object = object;
    }

    public QueryBuilder getQueryBuilder()
    {
        return queryBuilder;
    }

    public void setQueryBuilder(QueryBuilder queryBuilder)
    {
        this.queryBuilder = queryBuilder;
    }

    public SearchUtils.SYNC_OPERATION getOperation()
    {
        return operation;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Long timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getScript()
    {
        return script;
    }

    public void setScript(String script)
    {
        this.script = script;
    }

    public Map<String, Object> getScriptParams()
    {
        return scriptParams;
    }

    public void setScriptParams(Map<String, Object> scriptParams)
    {
        this.scriptParams = scriptParams;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getRoutingKey()
    {
        return routingKey;
    }

    public void setRoutingKey(String routingKey)
    {
        this.routingKey = routingKey;
    }
    
    public Map<Pair<String, String>, List<String>> getBulkReq() {
    	return bulkReq;
    }
    
    public void setBulkReq(Map<Pair<String, String>, List<String>> bulkReq) {
    	this.bulkReq = bulkReq;
    }
    
    @Override
    public String toString()
    {
        return "SyncData [indexName=" + indexName + ", documentType=" + documentType + ", operation=" + operation + ", id=" + id + ", parentId=" + parentId + ", routingKey=" + routingKey + ", timestamp=" + timestamp + ", object=" + object + ", queryBuilder=" + queryBuilder + ", script=" + script + ", scriptParams=" + scriptParams + ", username=" + username + "]";
    }
}
