/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

import org.codehaus.jackson.annotate.JsonProperty;

import com.resolve.search.SearchConstants;


@SuppressWarnings("serial")
public class DocumentQuery extends AbstractQuery
{
    private static final long serialVersionUID = 5823287678015717245L;

	public DocumentQuery()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public DocumentQuery(String suggest, int weight)
    {
        super(suggest, weight);
    }

    public DocumentQuery(String suggest, int weight, Long sysUpdatedOn)
    {
        super(suggest, weight, sysUpdatedOn);
    }

    @JsonProperty(SearchConstants.SUGGEST_FIELD_NAME_DOCUMENT)
    @Override
    public Suggest getSuggest()
    {
        return suggest;
    }

    @Override
    public void setSuggest(Suggest suggest)
    {
        this.suggest = suggest;
    }

    @Override
    public String getSuggestFieldName()
    {
        return SearchConstants.SUGGEST_FIELD_NAME_DOCUMENT;
    }
}
