/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;
import java.util.Date;

import com.resolve.search.SyncData;
import com.resolve.util.DateUtils;

public class SyncLog implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String sysId;
    private String parentId;
    private String routingKey;
    private Date timestamp;
    private String indexName;
    private String documentType;
    private String transactionType; //e.g., delete, index, update_script
    private String query; //ES query JSON
    private boolean isProcessed = false;

    public SyncLog()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public SyncLog(String sysId, Date timestamp, String indexName, String documentType, String transactionType, boolean isProcessed)
    {
        super();
        this.sysId = sysId;
        this.timestamp = timestamp;
        this.indexName = indexName;
        this.documentType = documentType;
        this.transactionType = transactionType;
        this.isProcessed = isProcessed;
    }

    public SyncLog(SyncData<Serializable> syncData)
    {
        this(syncData.getId(), DateUtils.getDate(syncData.getTimestamp()), syncData.getIndexName(), syncData.getDocumentType(), syncData.getOperation().toString(), false);
        this.parentId = syncData.getParentId();
        this.routingKey = syncData.getRoutingKey();
        if(syncData.getQueryBuilder() != null)
        {
            this.query = syncData.getQueryBuilder().toString();
        }
    }

    public String getSysId()
    {
        return sysId;
    }

    public void setSysId(String sysId)
    {
        this.sysId = sysId;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getRoutingKey()
    {
        return routingKey;
    }

    public void setRoutingKey(String routingKey)
    {
        this.routingKey = routingKey;
    }

    public Date getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Date timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getIndexName()
    {
        return indexName;
    }

    public void setIndexName(String indexName)
    {
        this.indexName = indexName;
    }

    public String getDocumentType()
    {
        return documentType;
    }

    public void setDocumentType(String documentType)
    {
        this.documentType = documentType;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public boolean isProcessed()
    {
        return isProcessed;
    }

    public void setProcessed(boolean isProcessed)
    {
        this.isProcessed = isProcessed;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((documentType == null) ? 0 : documentType.hashCode());
        result = prime * result + ((indexName == null) ? 0 : indexName.hashCode());
        result = prime * result + (isProcessed ? 1231 : 1237);
        result = prime * result + ((query == null) ? 0 : query.hashCode());
        result = prime * result + ((routingKey == null) ? 0 : routingKey.hashCode());
        result = prime * result + ((sysId == null) ? 0 : sysId.hashCode());
        result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
        result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        SyncLog other = (SyncLog) obj;
        if (documentType == null)
        {
            if (other.documentType != null) return false;
        }
        else if (!documentType.equals(other.documentType)) return false;
        if (indexName == null)
        {
            if (other.indexName != null) return false;
        }
        else if (!indexName.equals(other.indexName)) return false;
        if (isProcessed != other.isProcessed) return false;
        if (query == null)
        {
            if (other.query != null) return false;
        }
        else if (!query.equals(other.query)) return false;
        if (routingKey == null)
        {
            if (other.routingKey != null) return false;
        }
        else if (!routingKey.equals(other.routingKey)) return false;
        if (sysId == null)
        {
            if (other.sysId != null) return false;
        }
        else if (!sysId.equals(other.sysId)) return false;
        if (timestamp == null)
        {
            if (other.timestamp != null) return false;
        }
        else if (!timestamp.equals(other.timestamp)) return false;
        if (transactionType == null)
        {
            if (other.transactionType != null) return false;
        }
        else if (!transactionType.equals(other.transactionType)) return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "SyncLog [sysId=" + sysId + ", routingKey=" + routingKey + ", timestamp=" + timestamp + ", indexName=" + indexName + ", documentType=" + documentType + ", transactionType=" + transactionType + ", query=" + query + ", isProcessed=" + isProcessed + "]";
    }
}
