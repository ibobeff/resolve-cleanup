/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.TreeSet;

public class AutomationSearchResponse extends BaseIndexModel
{
    private static final long serialVersionUID = 7791260003617225168L;
	private String name;
    private String fullName;
    private String type;
    private String namespace;
    private String summary;
    private String rawSummary;
    private Double rating;
    private Long clickCount;
    private Long numberOfReviews;

    private TreeSet<String> tags = new TreeSet<String>();

    public AutomationSearchResponse()
    {
        super();
        // blank constructor, required for JSON serialization.
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getRawSummary()
    {
        return rawSummary;
    }

    public void setRawSummary(String rawSummary)
    {
        this.rawSummary = rawSummary;
    }

    public Double getRating()
    {
        return (rating == null ? 0.0 : rating);
    }

    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    public Long getClickCount()
    {
        return (clickCount == null ? 0 : clickCount);
    }

    public void setClickCount(Long clickCount)
    {
        this.clickCount = clickCount;
    }

    public Long getNumberOfReviews()
    {
        return numberOfReviews;
    }

    public void setNumberOfReviews(Long numberOfReviews)
    {
        this.numberOfReviews = numberOfReviews;
    }

    public TreeSet<String> getTags()
    {
        return tags;
    }

    public void setTags(TreeSet<String> tags)
    {
        this.tags = tags;
    }

    @Override
    public String toString()
    {
        return "AutomationSearchResponse [fullName=" + fullName + ", type=" + type + ", summary=" + summary + ", rawSummary=" + rawSummary + ", tags=" + tags + ", toString()=" + super.toString() + "]";
    }
}
