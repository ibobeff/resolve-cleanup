/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;

public class PBArtifacts extends BaseIndexModel implements Serializable
{
	private static final long serialVersionUID = 7458671704659555331L;

	public PBArtifacts()
	{
		super();
	}
	
	public PBArtifacts(String sysId, String sysOrg)
	{
		super(sysId, sysOrg);
	}
	
	public PBArtifacts(String sysId, String sysOrg, String createdBy)
	{
		super(sysId, sysOrg, createdBy);
	}
	
	private String worksheetId;
	private String incidentId;
	private String name;
	private String value;
	private String description;
	private String sir;
	private String activityId;
	private String activityName;
	private String auditMessage;
	private Boolean modified;
	private String source;
    private String sourceValue;
    private String type;
    private String sourceAndValue;
    
	public String getWorksheetId()
	{
		return worksheetId;
	}
	public void setWorksheetId(String worksheetId)
	{
		this.worksheetId = worksheetId;
	}
	
	public String getIncidentId()
	{
		return incidentId;
	}
	public void setIncidentId(String incidentId)
	{
		this.incidentId = incidentId;
	}

	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}

	public String getValue()
	{
		return value;
	}
	public void setValue(String value)
	{
		this.value = value;
	}

	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getSir()
	{
		return sir;
	}
	public void setSir(String sir)
	{
		this.sir = sir;
	}

	public String getActivityId()
	{
		return activityId;
	}
	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}

	public String getActivityName()
	{
		return activityName;
	}
	public void setActivityName(String activityName)
	{
		this.activityName = activityName;
	}
	
	public String getAuditMessage()
    {
        return auditMessage;
    }
    public void setAuditMessage(String auditMessage)
    {
        this.auditMessage = auditMessage;
    }

    public Boolean isModified()
    {
        return modified;
    }

    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }

    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    
    public String getSourceValue()
    {
        return sourceValue;
    }
    public void setSourceValue(String sourceValue)
    {
        this.sourceValue = sourceValue;
    }

    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    
    public String getSourceAndValue()
    {
        return sourceAndValue;
    }
    public void setSourceAndValue(String sourceAndValue)
    {
        this.sourceAndValue = sourceAndValue;
    }
}
