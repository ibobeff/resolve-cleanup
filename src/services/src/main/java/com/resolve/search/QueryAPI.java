/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.model.Tag;
import com.resolve.search.model.query.AutomationQuery;
import com.resolve.search.model.query.DocumentQuery;
import com.resolve.search.model.query.GlobalQuery;
import com.resolve.search.model.query.Query;
import com.resolve.search.model.query.SocialQuery;
import com.resolve.search.model.query.UserQuery;
import com.resolve.search.model.query.WikiQuery;
import com.resolve.search.query.QueryIndexer;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This API is the gateway to the queries, suggestions etc. 
 *
 */
public class QueryAPI
{
    private static final SearchAPI<? extends Query> wikiQuerySearchAPI = APIFactory.getWikiQuerySearchAPI();
    private static final SearchAPI<? extends Query> documentQuerySearchAPI = APIFactory.getDocumentQuerySearchAPI();
    private static final SearchAPI<? extends Query> socialQuerySearchAPI = APIFactory.getSocialQuerySearchAPI();
    private static final SearchAPI<? extends Query> automationQuerySearchAPI = APIFactory.getAutomationQuerySearchAPI();
    private static final SearchAPI<UserQuery> userQuerySearchAPI = APIFactory.getUserQuerySearchAPI();
    private static final SearchAPI<GlobalQuery> globalQuerySearchAPI = APIFactory.getGlobalQuerySearchAPI();
    private static final SearchAPI<Tag> tagSearchAPI = APIFactory.getTagSearchAPI();

    public static ResponseDTO<Query> getWikiNameSuggestions(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<Query> responseDTO = new ResponseDTO<Query>();

        List<Query> records = new ArrayList<Query>();
        // "suggest" is the field that stored auto completion data for full name
        List<String> suggestions = wikiQuerySearchAPI.getSuggestions(SearchConstants.SUGGEST_FIELD_NAME_WIKI, queryDTO, username);
        for (String suggestion : suggestions)
        {
            records.add(new WikiQuery(suggestion, 1));
        }
        responseDTO.setRecords(records).setSuccess(true);

        return responseDTO;
    }

    public static ResponseDTO<Query> getDocumentQuerySuggestions(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<Query> responseDTO = new ResponseDTO<Query>();

        if (StringUtils.isBlank(queryDTO.getSearchTerm()))
        {
            Log.log.warn("QueryDTO do not have searchTerm set, avoid calling empty!");
        }
        else
        {
            List<Query> records = new ArrayList<Query>();
            List<String> suggestions = documentQuerySearchAPI.getSuggestions(SearchConstants.SUGGEST_FIELD_NAME_DOCUMENT, queryDTO, username);
            for (String suggestion : suggestions)
            {
                records.add(new DocumentQuery(suggestion, 1));
            }
            responseDTO.setRecords(records).setSuccess(true);
        }
        return responseDTO;
    }

    public static ResponseDTO<Query> getSocialQuerySuggestions(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<Query> responseDTO = new ResponseDTO<Query>();

        if (StringUtils.isBlank(queryDTO.getSearchTerm()))
        {
            Log.log.warn("QueryDTO do not have searchTerm set, avoid calling empty!");
        }
        else
        {
            List<Query> records = new ArrayList<Query>();
            if(queryDTO.getSearchTerm().startsWith("#"))
            {
                List<String> suggestions = tagSearchAPI.getSuggestions(SearchConstants.SUGGEST_FIELD_NAME_TAG, queryDTO, username);
                for (String suggestion : suggestions)
                {
                    records.add(new SocialQuery(suggestion, 1));
                }
            }
            else
            {
                List<String> suggestions = socialQuerySearchAPI.getSuggestions(SearchConstants.SUGGEST_FIELD_NAME_SOCIAL, queryDTO, username);
                for (String suggestion : suggestions)
                {
                    records.add(new SocialQuery(suggestion, 1));
                }
            }
            responseDTO.setRecords(records).setSuccess(true);
        }
        return responseDTO;
    }

    public static ResponseDTO<Query> getAutomationQuerySuggestions(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<Query> responseDTO = new ResponseDTO<Query>();

        if (StringUtils.isBlank(queryDTO.getSearchTerm()))
        {
            Log.log.warn("QueryDTO do not have searchTerm set, avoid calling empty!");
        }
        else
        {
            List<Query> records = new ArrayList<Query>();
            List<String> suggestions = automationQuerySearchAPI.getSuggestions(SearchConstants.SUGGEST_FIELD_NAME_AUTOMATION, queryDTO, username);
            for (String suggestion : suggestions)
            {
                records.add(new AutomationQuery(suggestion, 1));
            }
            responseDTO.setRecords(records).setSuccess(true);
        }
        return responseDTO;
    }
    
    public static ResponseDTO<Tag> getHashtagSuggestions(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<Tag> responseDTO = new ResponseDTO<Tag>();

        List<Tag> records = new ArrayList<Tag>();
        
        if(StringUtils.isBlank(queryDTO.getSearchTerm()))
        {
            queryDTO.addSortItem(new QuerySort("weight", QuerySort.SortOrder.DESC));
            responseDTO = tagSearchAPI.searchByQuery(queryDTO, username);
        }
        else
        {
            List<String> suggestions = tagSearchAPI.getSuggestions(SearchConstants.SUGGEST_FIELD_NAME_TAG, queryDTO, username);
            for (String suggestion : suggestions)
            {
                records.add(new Tag(suggestion, 1));
            }
            responseDTO.setRecords(records).setSuccess(true);
        }
        return responseDTO;
    }

    /**
     * Add select count and eventually update its weight.
     * 
     * @param type
     * @param searchTerm
     * @param username
     * @return
     * @throws SearchException
     */
    public static void addSelectCount(String type, String searchTerm, String username) throws SearchException
    {
        Query query = null;
        QueryIndexer queryIndexer = null;
        
        if(SearchConstants.USER_QUERY_TYPE_DOCUMENT.equals(type))
        {
            query = documentQuerySearchAPI.findDocumentById(searchTerm, username);
            queryIndexer = APIFactory.getDocumentQueryIndexer();
        }
        else if(SearchConstants.USER_QUERY_TYPE_SOCIAL.equals(type))
        {
            query = socialQuerySearchAPI.findDocumentById(searchTerm, username);
            queryIndexer = APIFactory.getSocialQueryIndexer();
        }
        else if(SearchConstants.USER_QUERY_TYPE_AUTOMATION.equals(type))
        {
            query = automationQuerySearchAPI.findDocumentById(searchTerm, username);
            queryIndexer = APIFactory.getAutomationQueryIndexer();
        }
        else if(SearchConstants.USER_QUERY_TYPE_WIKI.equals(type))
        {
            query = wikiQuerySearchAPI.findDocumentById(searchTerm, username);
            queryIndexer = APIFactory.getWikiQueryIndexer();
        }
        
        if(query != null && StringUtils.isNotBlank(query.getSuggestion()))
        {
            query.addSelectCount(1);
            queryIndexer.updateRating(query, username);
        }
    }
    
    public static ResponseDTO<UserQuery> getUserQueryHistory(QueryDTO queryDTO, String username) throws SearchException
    {
        try
        {
            queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.DESC));
            QueryBuilder queryBuilder = QueryBuilders.termQuery("username", username);
            return userQuerySearchAPI.searchByQuery(queryDTO, queryBuilder, username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public static ResponseDTO<GlobalQuery> getTopQuery(QueryDTO queryDTO, String username) throws SearchException
    {
        try
        {
            queryDTO.addSortItem(new QuerySort("selectCount", QuerySort.SortOrder.DESC));
            return globalQuerySearchAPI.searchByQuery(queryDTO, username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public static GlobalQuery findGlobalQuery(String query, String username) throws SearchException
    {
        GlobalQuery result = null;
        
        QueryDTO queryDTO = new QueryDTO(0, 1);
        queryDTO.addFilterItem(new QueryFilter("query", QueryFilter.EQUALS, query));
        ResponseDTO<GlobalQuery> response = globalQuerySearchAPI.searchByQuery(queryDTO, username);
        if(response.getRecords() != null && response.getRecords().size() > 0)
        {
            result = response.getRecords().get(0);
        }
        return result;
    }
}
