package com.resolve.search.model;

public class MetricCPU extends MetricData
{
    private static final long serialVersionUID = 7212552256728572117L;
	private long load1;
    private long load5;
    private long load15;
    
    
    public long getLoad1()
    {
        return load1;
    }
    
    public void setLoad1(long c)
    {
        load1 = c;
    }
    
    public long getLoad5()
    {
        return load5;
    }
    
    public void setLoad5(long f)
    {
        load5 = f;
    }

    public long getLoad15()
    {
        return load15;
    }
    
    public void setLoad15(long c)
    {
        load15 = c;
    }

}
