/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.automation;

import java.util.List;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.AutomationSearchResponse;
import com.resolve.search.model.UserInfo;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AutomationSearchAPI
{
    private static final SearchAPI<AutomationSearchResponse> searchAPI = APIFactory.getAutomationSearchAPI();

    private static final QueryBuilder hiddenDocumentFilter = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("hidden", false));
    private static final QueryBuilder undeletedFilter = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("deleted", false));
    private static QueryBuilder excludeNamespaceFilter = null;

    static
    {
        String excludeNamespacePattern = PropertiesUtil.getPropertyString("search.exclude.namespace");
        if(StringUtils.isNotBlank(excludeNamespacePattern))
        {
            //got to lower case it because the namespace is internally indexed lowercase.
            //do no worry if you see the data as camel case or whatever it is ES internal storage that
            //has them indexed lowercase..
            List<String> excludeNamespaceList = StringUtils.convertStringToList(excludeNamespacePattern.toLowerCase(), ",");
            if(excludeNamespaceList != null && excludeNamespaceList.size() > 0)
            {
                excludeNamespaceFilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.termsQuery("namespace.exact", excludeNamespaceList)); 
                		//QueryBuilders.notFilter(FilterBuilders.termsFilter("namespace.exact", excludeNamespaceList));
            }
        }
    }

    // the fieldName^int (summary^2) provides the boosting score to the records
    // so they comes up on top
    private static final String[] searchFields = new String[] { "name", "name.exact", "type", "fullName", "fullName.exact^3", "summary^2", "namespace", "namespace.exact", "content", "invocationContent", "processModel"};
    private static final String[] highlightedFields = new String[] { "summary" };

    public static ResponseDTO<AutomationSearchResponse> searchAutomations(QueryDTO queryDTO, UserInfo userInfo)
    {
        ResponseDTO<AutomationSearchResponse> result = new ResponseDTO<AutomationSearchResponse>();

        try
        {
            QueryBuilder queryBuilder = null;
            QueryBuilder rolesFilter = QueryBuilders.termsQuery("readRoles", userInfo.getRoles());
            //FilterBuilder processModelFilter = FilterBuilders.missingFilter("processModel").existence(true).nullValue(false);
            QueryBuilder automationFilter = QueryBuilders.termQuery("automation", Boolean.TRUE);
            QueryBuilder namespaceFilter = null;
            QueryBuilder tagFilter = null;
            QueryBuilder typeFilter = null;
            QueryBuilder sysUpdatedByFilter = null;
            QueryBuilder sysUpdatedOnFilter = null;
            QueryBuilder sysCreatedByFilter = null;
            QueryBuilder sysCreatedOnFilter = null;

            QueryBuilder multiMatchQueryBuilder = null;
            if(StringUtils.isBlank(queryDTO.getSearchTerm()))
            {
                //this is advanced search
                String[] userSearchFields = searchFields;
                if(queryDTO.getFilterItems() != null && queryDTO.getFilterItems().size() > 0)
                {
                    for(QueryFilter queryFilter : queryDTO.getFilterItems())
                    {
                        if("namespace".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                //check wiki_documents_mappings.json file for the namespace.exact
                                namespaceFilter = QueryBuilders.termsQuery("namespace.exact", StringUtils.stringToArray(queryFilter.getValue(), ","));
                            }
                        }
                        else if("tag".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                tagFilter = QueryBuilders.termsQuery("tags", SearchUtils.parseCSV(queryFilter.getValue(), ","));
                            }
                        }
                        else if("type".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                typeFilter = QueryBuilders.termsQuery("type", SearchUtils.parseCSV(queryFilter.getValue(), ","));
                            }
                        }
                        else if("sysUpdatedBy".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                sysUpdatedByFilter = QueryBuilders.termQuery("sysUpdatedBy", queryFilter.getValue());
                            }
                        }
                        else if("sysUpdatedOn".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                sysUpdatedOnFilter = SearchUtils.createDateRangeFilterBuilder(queryFilter);
                            }
                        }
                        else if("sysCreatedBy".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                sysCreatedByFilter = QueryBuilders.termQuery("sysCreatedBy", queryFilter.getValue());
                            }
                        }
                        else if("sysCreatedOn".equalsIgnoreCase(queryFilter.getField()))
                        {
                            if(StringUtils.isNotBlank(queryFilter.getValue()))
                            {
                                sysCreatedOnFilter = SearchUtils.createDateRangeFilterBuilder(queryFilter);
                            }
                        }
                        else
                        {
                            if(StringUtils.isNotBlank(queryFilter.getField()))
                            {
                                userSearchFields = StringUtils.stringToArray(queryFilter.getField(), ",");
                            }
                            queryDTO.setSearchTerm(queryFilter.getValue());
                        }
                    }
                }
                if(StringUtils.isBlank(queryDTO.getSearchTerm()))
                {
                    multiMatchQueryBuilder = QueryBuilders.matchAllQuery();
                }
                else
                {
                    if(SearchUtils.isSimpleQueryString(queryDTO.getSearchTerm()))
                    {
                        SimpleQueryStringBuilder simpleQueryStringBuilder = QueryBuilders.simpleQueryStringQuery(queryDTO.getSearchTerm());
                        for(String field : userSearchFields)
                        {
                            simpleQueryStringBuilder.field(field);
                        }
                        multiMatchQueryBuilder = simpleQueryStringBuilder;
                    }
                    else
                    {
                        multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), userSearchFields).fuzziness(".25").maxExpansions(5);
                    }
                }
            }
            else
            {
                if(SearchUtils.isSimpleQueryString(queryDTO.getSearchTerm()))
                {
                    SimpleQueryStringBuilder simpleQueryStringBuilder = QueryBuilders.simpleQueryStringQuery(queryDTO.getSearchTerm());
                    for(String field : searchFields)
                    {
                        simpleQueryStringBuilder.field(field);
                    }
                    multiMatchQueryBuilder = simpleQueryStringBuilder;
                }
                else
                {
                    multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), searchFields).fuzziness(".25").maxExpansions(5);
                }
            }

            queryBuilder = QueryBuilders.functionScoreQuery(multiMatchQueryBuilder);

            result = searchAPI.searchByTerm(SearchConstants.USER_QUERY_TYPE_AUTOMATION, queryDTO, queryBuilder, new QueryBuilder[] { rolesFilter, automationFilter, namespaceFilter, tagFilter, typeFilter, sysUpdatedByFilter, sysUpdatedOnFilter, sysCreatedByFilter, sysCreatedOnFilter, hiddenDocumentFilter, undeletedFilter, excludeNamespaceFilter }, highlightedFields, userInfo);

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage(e.getMessage());
        }

        return result;
    }
}
