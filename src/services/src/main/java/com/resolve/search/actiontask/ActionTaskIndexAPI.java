/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.actiontask;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.RateIndexer;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.ActionTask;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ActionTaskIndexAPI
{
    private static IndexAPI indexAPI = APIFactory.getActionTaskIndexAPI();
    
    private static final Indexer<IndexData<String>> indexer = ActionTaskIndexer.getInstance();
    private static final RateIndexer<IndexData<String>> queryIndexer = APIFactory.getAutomationQueryIndexer();
    
    private static long lastIndexTimeActionTask = 0;

    public static long getLastIndexTime() throws SearchException
    {
        return indexAPI.getLastIndexTime(SearchConstants.SYS_UPDATED_ON);
    }

    public static void indexActionTask(String sysId, String username)
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("ActionTaskIndexAPI.indexActionTask() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            if (StringUtils.isBlank(sysId))
            {
                Log.log.debug("Blank sysId provided, check the calling code");
            }
            else
            {
                ResolveActionTaskVO actionTaskVO = ServiceHibernate.getActionTaskFromIdWithReferences(sysId, null, username);
                ActionTask actionTask = new ActionTask(actionTaskVO);
                try
                {
                    Long lastIndexTime = indexAPI.getLastIndexTime(SearchConstants.SYS_UPDATED_ON);
                    long currentTime = System.currentTimeMillis();
                    if(lastIndexTimeActionTask == 0 || (currentTime - lastIndexTimeActionTask) > 500L)
                    {
                        indexAPI.indexDocument(actionTask, username);
                        IndexData<String> indexData = new IndexData<String>(Arrays.asList(lastIndexTime.toString()), OPERATION.RATE, username);
                        queryIndexer.enqueue(indexData);
                        lastIndexTime = currentTime;
                    }
                    else
                    {
                        IndexData<String> indexData = new IndexData<String>(Arrays.asList(sysId), OPERATION.INDEX, username);
                        indexer.enqueue(indexData);
                    }
                }
                catch (SearchException e)
                {
                    //TODO eventually we'll have to throw this outside.
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }

	@SuppressWarnings("unchecked")
	public static void updateIndexMapping(String username) {
		IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.MIGRATE, username);
		indexer.enqueue(indexData);
	}
    
    public static void indexActionTasks(Collection<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("ActionTaskIndexAPI.indexActionTasks() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.INDEX, username);
            indexer.enqueue(indexData);
        }
    }

    public static void purgeActionTasks(Set<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("ActionTaskIndexAPI.purgeActionTasks() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.PURGE, username);
            indexer.enqueue(indexData);
        }
    }

    public static void purgeAllActionTasks(String username)
    {
        IndexData<String> indexData = new IndexData<String>(Collections.<String> emptyList(), OPERATION.PURGE, username);
        indexer.enqueue(indexData);
    }

    public static void deleteActionTasks(Collection<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("ActionTaskIndexAPI.deleteActionTasks() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
            indexer.enqueue(indexData);
        }
    }

    public static void setActive(final String sysId, final String username, final boolean isActive) throws SearchException
    {
        ActionTask actionTask = ActionTaskSearchAPI.findActionTaskById(sysId, username);
        if (actionTask == null)
        {
            Log.log.warn("ActionTaskIndexAPI.setActive failed, invalid document sys id: " + sysId);
        }
        else
        {
            actionTask.setActive(isActive);
            indexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, actionTask, false, username);
        }
    }

    public static void setDeleted(final String sysId, final String username, final boolean isDeleted) throws SearchException
    {
        ActionTask actionTask = ActionTaskSearchAPI.findActionTaskById(sysId, username);
        if (actionTask == null)
        {
            Log.log.warn("ActionTaskIndexAPI.setDeleted failed, invalid document sys id: " + sysId);
        }
        else
        {
            actionTask.setDeleted(isDeleted);
            indexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, actionTask, false, username);
        }
    }
    
    public static boolean setRating(final String sysId, final String username, final double rating) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("setRating failed, invalid action task sys id: " + sysId);
        }
        else
        {
            ActionTask actionTask = ActionTaskSearchAPI.findActionTaskById(sysId, username);
            if(actionTask != null)
            {
                actionTask.setRating(actionTask.getRating() == null ? 1.0 : actionTask.getRating() + 1);
                indexAPI.indexDocument(actionTask, username);
            }
        }
        return result;
    }

    public static boolean increaseClickCount(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("increaseClickCount failed, invalid action task sys id: " + sysId);
        }
        else
        {
            ActionTask actionTask = ActionTaskSearchAPI.findActionTaskById(sysId, username);
            if(actionTask != null)
            {
                actionTask.setClickCount(actionTask.getClickCount() == null ? 1 : actionTask.getClickCount() + 1);
                indexAPI.indexDocument(actionTask, username);
            }
        }
        return result;
    }

    public static boolean resetRating(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("resetRating failed, invalid action task sys id: " + sysId);
        }
        else
        {
            ActionTask actionTask = ActionTaskSearchAPI.findActionTaskById(sysId, username);
            if(actionTask != null)
            {
                actionTask.setRating(null);
                indexAPI.indexDocument(actionTask, username);
            }
        }
        return result;
    }

    public static boolean resetClickCount(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("resetClickCount failed, invalid action task sys id: " + sysId);
        }
        else
        {
            ActionTask actionTask = ActionTaskSearchAPI.findActionTaskById(sysId, username);
            if(actionTask != null)
            {
                actionTask.setClickCount(null);
                indexAPI.indexDocument(actionTask, username);
            }
        }
        return result;
    }
}