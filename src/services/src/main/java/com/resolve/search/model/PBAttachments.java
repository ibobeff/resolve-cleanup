/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;

public class PBAttachments extends BaseIndexModel implements Serializable
{
	private static final long serialVersionUID = 6241851030800864136L;

	public PBAttachments()
	{
		super();
	}
	
	public PBAttachments(String sysId, String sysOrg)
	{
		super(sysId, sysOrg);
	}
	
	public PBAttachments(String sysId, String sysOrg, String createdBy)
	{
		super(sysId, sysOrg, createdBy);
	}
	
	public PBAttachments(String sysId, String sysOrg, String createdBy, Boolean isMalicious, String description)
    {
        super(sysId, sysOrg, createdBy);
        this.isMalicious = isMalicious;
        this.description = description; 
    }
	
	private String worksheetId;
	private String incidentId;
	private String activityId;
	private String name;
	private Integer size;
	private String auditMessage;
	private Boolean isMalicious = Boolean.FALSE;
	private String description;
	private Boolean modified;
	private String source;
    private String sourceValue;
    private String sourceAndValue;

	public String getWorksheetId()
	{
		return worksheetId;
	}
	public void setWorksheetId(String worksheetId)
	{
		this.worksheetId = worksheetId;
	}
	
	public String getIncidentId()
	{
		return incidentId;
	}
	public void setIncidentId(String incidentId)
	{
		this.incidentId = incidentId;
	}
	
	public String getActivityId()
	{
		return activityId;
	}
	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}

	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	
	public Integer getSize()
	{
		return size;
	}
	public void setSize(Integer size)
	{
		this.size = size;
	}
	
	public String getAuditMessage()
    {
        return auditMessage;
    }
    public void setAuditMessage(String auditMessage)
    {
        this.auditMessage = auditMessage;
    }
    
    public Boolean isMalicious()
    {
        return isMalicious;
    }
    public void setMalicious(Boolean isMalicious)
    {
        this.isMalicious = isMalicious;
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public Boolean isModified()
    {
        return modified;
    }

    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }

    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    
    public String getSourceValue()
    {
        return sourceValue;
    }
    public void setSourceValue(String sourceValue)
    {
        this.sourceValue = sourceValue;
    }
    
    public String getSourceAndValue()
    {
        return sourceAndValue;
    }
    public void setSourceAndValue(String sourceAndValue)
    {
        this.sourceAndValue = sourceAndValue;
    }
}
