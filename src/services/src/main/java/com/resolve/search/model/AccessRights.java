package com.resolve.search.model;

import java.io.Serializable;

public class AccessRights implements Serializable{

	private static final long serialVersionUID = 1L;

	private String readAccess;
	
	private String writeAccess;
	
	private String executeAccess;
	
	private String adminAccess;

	public AccessRights() {
	}

	public AccessRights(String readAccess, String writeAccess, String executeAccess, String adminAccess) {
		this.writeAccess = writeAccess;
		this.readAccess = readAccess;
		this.executeAccess = executeAccess;
		this.adminAccess = adminAccess;
	}
	
	public String getReadAccess() {
		return readAccess;
	}

	public void setReadAccess(String readAccess) {
		this.readAccess = readAccess;
	}

	public String getWriteAccess() {
		return writeAccess;
	}

	public void setWriteAccess(String writeAccess) {
		this.writeAccess = writeAccess;
	}

	public String getExecuteAccess() {
		return executeAccess;
	}

	public void setExecuteAccess(String executeAccess) {
		this.executeAccess = executeAccess;
	}

	public String getAdminAccess() {
		return adminAccess;
	}

	public void setAdminAccess(String adminAccess) {
		this.adminAccess = adminAccess;
	}
}
