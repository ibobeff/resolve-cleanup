/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import com.resolve.search.SearchConstants;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@StripHTML
public class WikiDocument extends BaseIndexModel
{
    private static final long serialVersionUID = 1L;

    private String name;
    private String fullName;
    private String type; //e.g., automation, decisiontree, or document

    private String namespace;
    private String title;
    private String summary;
    @StripHTML(value=false)
    private String rawSummary;
    private String content;
    @StripHTML(value=false)
    private String processModel;
    @StripHTML(value=false)
    private String abortModel;
    @StripHTML(value=false)
    private String finalModel;
    @StripHTML(value=false)
    private String decisionTree;

    private Double rating;
    private Long clickCount;
    private Long numberOfReviews;
    private Long editCount;
    private Long executeCount;
    private Long reviewCount;
    private Long usefulNoCount;
    private Long usefulYesCount;

    private boolean isLocked = false;
    private boolean isDeleted = false;
    private boolean isHidden = false;
    private boolean isActive = false;
    //helps with automation query
    private boolean isAutomation = true;
    
    private Boolean requestSubmission;
    private Long requestSubmissionOn;
    private Date requestSubmissionDt;
    private Long lastReviewedOn;   
    private Date lastReviewedDt;   
    private String lastReviewedBy;
    
    private Long expireOn; 
    private Date expireDt; 

    private TreeSet<String> readRoles = new TreeSet<String>(); // TreeSet to keep in order

    private TreeSet<String> tags = new TreeSet<String>();
    private TreeSet<String> catalogs = new TreeSet<String>();

    private AccessRights accessRights;
    
    public WikiDocument()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public WikiDocument(String sysId, String sysOrg, String name, String namespace, String title, String summary, String content, TreeSet<String> readRoles)
    {
        super(sysId, sysOrg);
        this.title = title;
        this.content = content;
        this.name = name;
        this.namespace = namespace;
        this.fullName = namespace + "." + name;
        this.summary = summary;
        this.content = content;
        if(readRoles != null)
        {
            this.readRoles.addAll(readRoles);
        }
    }

    /**
     * This constroctor creates a WikiDocument from the WikiDocumentVO.
     *
     * @param wikiDocumentVO
     */
    public WikiDocument(WikiDocumentVO wikiDocumentVO)
    {
        setSysId(wikiDocumentVO.getSys_id());
        setSysOrg(wikiDocumentVO.getSysOrg());
        setSysCreatedBy(wikiDocumentVO.getSysCreatedBy());
        if(wikiDocumentVO.getSysCreatedOn() == null)
        {
            //to bad using current UTC time
            setSysCreatedOn(DateUtils.GetUTCDateLong());
        }
        else
        {
            setSysCreatedOn(wikiDocumentVO.getSysCreatedOn().getTime());
        }
        setSysUpdatedBy(wikiDocumentVO.getSysUpdatedBy());
        if(wikiDocumentVO.getSysUpdatedOn() == null)
        {
            //to bad using current UTC time
            setSysUpdatedOn(getSysCreatedOn());
        }
        else
        {
            setSysUpdatedOn(wikiDocumentVO.getSysUpdatedOn().getTime());
        }
        setName(wikiDocumentVO.getUName());
        setNamespace(wikiDocumentVO.getUNamespace());
        setFullName(wikiDocumentVO.getUFullname());
        if(StringUtils.isBlank(wikiDocumentVO.getUTitle()))
        {
            setTitle(wikiDocumentVO.getUFullname());
        }
        else
        {
            setTitle(wikiDocumentVO.getUTitle());
        }
        setSummary(wikiDocumentVO.getUSummary());
        setRawSummary(wikiDocumentVO.getUSummary());
        setContent(wikiDocumentVO.getUContent());
        setProcessModel(wikiDocumentVO.getUModelProcess());
        setAbortModel(wikiDocumentVO.getUModelException());
        setFinalModel(wikiDocumentVO.getUModelFinal());
        setDecisionTree(wikiDocumentVO.getUDecisionTree());
        setDeleted(wikiDocumentVO.getSysIsDeleted() == null ? false : wikiDocumentVO.getSysIsDeleted());
        setLocked(wikiDocumentVO.getUIsLocked() == null ? false : wikiDocumentVO.getUIsLocked());
        setHidden(wikiDocumentVO.getUIsHidden() == null ? false : wikiDocumentVO.getUIsHidden());
        setActive(wikiDocumentVO.getUIsActive() == null ? false : wikiDocumentVO.getUIsActive());
        setRequestSubmission(wikiDocumentVO.getUIsRequestSubmission());
        if(wikiDocumentVO.getUReqestSubmissionOn() != null)
        {
            setRequestSubmissionOn(wikiDocumentVO.getUReqestSubmissionOn().getTime());
        }
        setLastReviewedBy(wikiDocumentVO.getULastReviewedBy());
        if(wikiDocumentVO.getULastReviewedOn() != null)
        {
            setLastReviewedOn(wikiDocumentVO.getULastReviewedOn().getTime());
        }
        if(wikiDocumentVO.getUExpireOn() != null)
        {
            setExpireOn(wikiDocumentVO.getUExpireOn().getTime());
        }
        //rating gets update directly through separate API
        //setRating(wikiDocumentVO.getURatingBoost());
        //if one of these are not empty then this is a runbook. automation takes precedence over DT meaning
        //if there is process model and DT in a document then it will be set as automation type.
        if(wikiDocumentVO.getUHasActiveModel() != null && wikiDocumentVO.getUHasActiveModel())
        {
            setAutomation(true);
            if(wikiDocumentVO.getUIsRoot() != null && wikiDocumentVO.getUIsRoot())
            {
                setType(SearchConstants.SEARCH_TYPE_AUTOMATION + ", " + SearchConstants.SEARCH_TYPE_DECISION_TREE);
            }
            else
            {
                setType(SearchConstants.SEARCH_TYPE_AUTOMATION);
            }
        }
        else if(wikiDocumentVO.getUIsRoot() != null && wikiDocumentVO.getUIsRoot())
        {
            setAutomation(false);
            setType(SearchConstants.SEARCH_TYPE_DECISION_TREE);
        }
        else if(wikiDocumentVO.getUDisplayMode() != null && wikiDocumentVO.getUDisplayMode().equalsIgnoreCase("playbook"))
        {
            setAutomation(false);
            setType(SearchConstants.SEARCH_TYPE_PLAYBOOK);
        }
        else
        {
            setAutomation(false);
            setType(SearchConstants.SEARCH_TYPE_DOCUMENT);
        }

        //Tags, wikiDocumentVO makes sure that the tags are all comma separated.
        if(StringUtils.isNotBlank(wikiDocumentVO.getUTag()))
        {
            List<String> tagList = StringUtils.convertStringToList(wikiDocumentVO.getUTag(), ",");
            for(String tag : tagList)
            {
                this.tags.add(tag);
            }
        }

        //Catalogs, wikiDocumentVO makes sure that the catalogs are all comma separated.
        if(StringUtils.isNotBlank(wikiDocumentVO.getUCatalog()))
        {
            List<String> catalogList = StringUtils.convertStringToList(wikiDocumentVO.getUCatalog(), ",");
            for(String catalog : catalogList)
            {
                this.catalogs.add(catalog);
            }
        }

        //Roles, wikiDocumentVO makes sure that the roles are all comma separated.
        if(wikiDocumentVO.getAccessRights() == null)
        {
            Log.log.error(wikiDocumentVO.getUFullname() + " document do not have read access rights, why?");
        }
        else
        {
            //it's guranteed by the API that it's a comma separated list
            List<String> readRoleList = StringUtils.convertStringToList(wikiDocumentVO.getAccessRights().getUReadAccess(), ",");
            for(String readRole : readRoleList)
            {
                this.readRoles.add(readRole);
            }
        }
        
        AccessRightsVO accessRightsVO = wikiDocumentVO.getAccessRights();
        if(accessRightsVO != null) {
    		AccessRights accessRights = new AccessRights();
    		accessRights.setReadAccess(accessRightsVO.getUReadAccess());
    		accessRights.setWriteAccess(accessRightsVO.getUWriteAccess());
    		accessRights.setAdminAccess(accessRightsVO.getUAdminAccess());
    		accessRights.setExecuteAccess(accessRightsVO.getUExecuteAccess());
    		setAccessRights(accessRights);
        }
    }
    
	private WikiDocument copyForType(String type) {
    	WikiDocument clone = new WikiDocument();
    	
    	// BaseIndexModel
    	clone.setSysId(this.getSysId());
    	clone.setSysOrg(this.getSysOrg());
    	clone.setSysUpdatedBy(this.getSysUpdatedBy());
    	clone.setSysUpdatedOn(this.getSysUpdatedOn());
    	clone.setSysUpdatedDt(this.getSysUpdatedDt());
    	clone.setSysCreatedBy(this.getSysCreatedBy());
    	clone.setSysCreatedOn(this.getSysCreatedOn());
    	clone.setSysCreatedDt(this.getSysCreatedDt());
    	
    	// WikiDOcument
    	
    	clone.setName(name);
    	clone.setFullName(fullName);
    	clone.setType(type);
    	
    	clone.setNamespace(namespace);
    	clone.setTitle(title);
    	clone.setSummary(summary);
    	clone.setRawSummary(rawSummary);
    	
    	switch (type) {
		
			case SearchConstants.SEARCH_TYPE_AUTOMATION:
				clone.setAbortModel(abortModel);
				clone.setFinalModel(finalModel);
				clone.setProcessModel(processModel);
				break;
		
			case SearchConstants.SEARCH_TYPE_DECISION_TREE:
				clone.setDecisionTree(decisionTree);
				break;
		
			case SearchConstants.SEARCH_TYPE_DOCUMENT:
			case SearchConstants.SEARCH_TYPE_PLAYBOOK:
				clone.setContent(content);
				break;
    	}
    	
    	clone.setRating(rating);
    	clone.setClickCount(clickCount);
    	clone.setNumberOfReviews(numberOfReviews);
    	clone.setEditCount(editCount);
    	clone.setExecuteCount(executeCount);
    	clone.setReviewCount(reviewCount);
    	clone.setUsefulNoCount(usefulNoCount);
    	clone.setUsefulYesCount(usefulYesCount);
    	
    	clone.setLocked(isLocked);
    	clone.setDeleted(isDeleted);
    	clone.setHidden(isHidden);
    	clone.setActive(isActive);
    	clone.setAutomation(isAutomation);
    	
    	clone.setRequestSubmission(requestSubmission);
    	clone.setRequestSubmissionOn(requestSubmissionOn);
    	clone.setRequestSubmissionDt(requestSubmissionDt);
    	clone.setLastReviewedOn(lastReviewedOn);
    	clone.setLastReviewedDt(lastReviewedDt);
    	clone.setLastReviewedBy(lastReviewedBy);
    	
    	clone.setExpireOn(expireOn);
    	clone.setExpireDt(expireDt);
    	
    	clone.setReadRoles(readRoles);
    	clone.setTags(tags);
    	// NOT CLONING CATALOGS
    	clone.setAccessRights(accessRights);
    	
    	return clone;
    }
    
    /**
     * This method clones WikiDocument for a specified type
     * from the WikiDocument.
     *
     * @param wikiDocument
     * @param type			
     */
    public WikiDocument cloneForType(String type) {
    	if (StringUtils.isBlank(type) || 
    		(!SearchConstants.SEARCH_TYPE_AUTOMATION.equals(type) &&
    		 !SearchConstants.SEARCH_TYPE_DECISION_TREE.equals(type) &&
    		 !SearchConstants.SEARCH_TYPE_DOCUMENT.equals(type) &&
    		 !SearchConstants.SEARCH_TYPE_PLAYBOOK.equals(type))) {
    		return this;
    	}
    	
    	return copyForType(type);
    }
	
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getRawSummary()
    {
        return rawSummary;
    }

    public void setRawSummary(String rawSummary)
    {
        this.rawSummary = rawSummary;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public boolean isLocked()
    {
        return isLocked;
    }

    public void setLocked(boolean isLocked)
    {
        this.isLocked = isLocked;
    }

    public boolean isDeleted()
    {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public boolean isHidden()
    {
        return isHidden;
    }

    public void setHidden(boolean isHidden)
    {
        this.isHidden = isHidden;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean isActive)
    {
        this.isActive = isActive;
    }

    public boolean isAutomation()
    {
        return isAutomation;
    }

    public void setAutomation(boolean isAutomation)
    {
        this.isAutomation = isAutomation;
    }

    public Boolean getRequestSubmission()
    {
        return requestSubmission;
    }

    public void setRequestSubmission(Boolean requestSubmission)
    {
        this.requestSubmission = requestSubmission;
    }

    public Date getRequestSubmissionDt()
    {
        return requestSubmissionDt;
    }

    public void setRequestSubmissionDt(Date requestSubmissionDt)
    {
        this.requestSubmissionDt = requestSubmissionDt;
    }

    public Long getRequestSubmissionOn()
    {
        return requestSubmissionOn;
    }

    public void setRequestSubmissionOn(Long requestSubmissionOn)
    {
        this.requestSubmissionOn = requestSubmissionOn;
        this.requestSubmissionDt = DateUtils.getDate(requestSubmissionOn);
    }

    public Date getLastReviewedDt()
    {
        return lastReviewedDt;
    }

    public void setLastReviewedDt(Date lastReviewedDt)
    {
        this.lastReviewedDt = lastReviewedDt;
    }

    public Long getLastReviewedOn()
    {
        return lastReviewedOn;
    }

    public void setLastReviewedOn(Long lastReviewedOn)
    {
        this.lastReviewedOn = lastReviewedOn;
        this.lastReviewedDt = DateUtils.getDate(lastReviewedOn);
    }
    public String getLastReviewedBy()
    {
        return lastReviewedBy;
    }

    public void setLastReviewedBy(String lastReviewedBy)
    {
        this.lastReviewedBy = lastReviewedBy;
    }

    public Date getExpireDt()
    {
        return expireDt;
    }

    public void setExpireDt(Date expireDt)
    {
        this.expireDt = expireDt;
    }

    public Long getExpireOn()
    {
        return expireOn;
    }

    public void setExpireOn(Long expireOn)
    {
        this.expireOn = expireOn;
        this.expireDt = DateUtils.getDate(expireOn);
    }

    public TreeSet<String> getReadRoles()
    {
        return readRoles;
    }
    
    public void setReadRoles(TreeSet<String> readRoles)
    {
        this.readRoles = readRoles;
    }

    public void addReadRole(String role)
    {
        if (StringUtils.isNotBlank(role))
        {
            this.readRoles.add(role);
        }
    }

    public TreeSet<String> getTags()
    {
        return tags;
    }

    public void setTags(TreeSet<String> tags)
    {
        this.tags = tags;
    }

    public TreeSet<String> getCatalogs()
    {
        return catalogs;
    }

    public void setCatalogs(TreeSet<String> catalogs)
    {
        this.catalogs = catalogs;
    }

    public String getProcessModel()
    {
        return processModel;
    }

    public void setProcessModel(String processModel)
    {
        this.processModel = processModel;
    }

    public String getAbortModel()
    {
        return abortModel;
    }

    public void setAbortModel(String abortModel)
    {
        this.abortModel = abortModel;
    }

    public String getFinalModel()
    {
        return finalModel;
    }

    public void setFinalModel(String finalModel)
    {
        this.finalModel = finalModel;
    }

    public String getDecisionTree()
    {
        return decisionTree;
    }

    public void setDecisionTree(String decisionTree)
    {
        this.decisionTree = decisionTree;
    }

    public Double getRating()
    {
        return (rating == null ? 0.0 : rating);
    }

    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    public void addRating(Double rating)
    {
        if(this.rating == null)
        {
            this.rating = rating;
        }
        else
        {
            this.rating += rating;
        }
    }

    public Long getClickCount()
    {
        return (clickCount == null ? 0 : clickCount);
    }

    public void setClickCount(Long clickCount)
    {
        this.clickCount = clickCount;
    }

    public void addClickCount()
    {
        if(this.clickCount == null)
        {
            this.clickCount = 1L;
        }
        else
        {
            this.clickCount += 1L;
        }
    }
    
    public Long getNumberOfReviews()
    {
        return (numberOfReviews == null ? 0 : numberOfReviews);
    }

    public void setNumberOfReviews(Long numberOfReviews)
    {
        this.numberOfReviews = numberOfReviews;
    }
    
    public void addNumberOfReviews()
    {
        if(this.numberOfReviews == null)
        {
            this.numberOfReviews = 1L;
        }
        else
        {
            this.numberOfReviews += 1L;
        }
    }

    public Long getEditCount()
    {
        return editCount;
    }

    public void setEditCount(Long editCount)
    {
        this.editCount = editCount;
    }

    public Long getExecuteCount()
    {
        return executeCount;
    }

    public void setExecuteCount(Long executeCount)
    {
        this.executeCount = executeCount;
    }

    public Long getReviewCount()
    {
        return reviewCount;
    }

    public void setReviewCount(Long reviewCount)
    {
        this.reviewCount = reviewCount;
    }

    public Long getUsefulNoCount()
    {
        return usefulNoCount;
    }

    public void setUsefulNoCount(Long usefulNoCount)
    {
        this.usefulNoCount = usefulNoCount;
    }

    public Long getUsefulYesCount()
    {
        return usefulYesCount;
    }

    public void setUsefulYesCount(Long usefulYesCount)
    {
        this.usefulYesCount = usefulYesCount;
    }

	public AccessRights getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(AccessRights accessRights) {
		this.accessRights = accessRights;
	}
}
