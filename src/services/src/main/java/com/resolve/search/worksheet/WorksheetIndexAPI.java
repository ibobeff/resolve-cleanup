/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.worksheet;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequestBuilder;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesResponse;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.min.Min;
import org.elasticsearch.search.sort.SortOrder;
import org.joda.time.DateTime;

import com.resolve.rsbase.MainBase;
import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.ExecutionSummary;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.model.WorksheetData;
import com.resolve.services.hibernate.util.ResolveEventUtil;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Guid;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.PerformanceDebug;
import com.resolve.util.StringUtils;

public class WorksheetIndexAPI
{
    private static final Indexer<IndexData<String>> worksheetIndexer = WorksheetIndexer.getInstance();
    private static final Indexer<IndexData<String>> processRequestIndexer = ProcessRequestIndexer.getInstance();
    private static final Indexer<IndexData<?>> taskResultIndexer = TaskResultIndexer.getInstance();
    private static final Indexer<IndexData<String>> executeStateIndexer = ExecuteStateIndexer.getInstance();

    private static final IndexAPI worksheetIndexAPI = APIFactory.getWorksheetIndexAPI();
    private static final IndexAPI executionSummaryIndexerAPI = APIFactory.getExecutionSummaryIndexAPI();
    private static final IndexAPI processRequestIndexAPI = APIFactory.getProcessRequestIndexAPI();
    private static final IndexAPI taskResultIndexAPI = APIFactory.getTaskResultIndexAPI();
    private static final IndexAPI executeStateIndexAPI = APIFactory.getExecuteStateIndexAPI();
    private static final IndexAPI worksheetDataIndexAPI = APIFactory.getWorksheetDataIndexAPI();

    public static synchronized void removeAllWorkSheetsRelatedIndex(String username) throws SearchException {
    	// HP TO DO This needs to be restricted to resolve.maint only, or to users with admin rights very least
        try {
            //remove all worksheet related data from the indexes
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            executeStateIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            taskResultIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            processRequestIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            worksheetIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            worksheetDataIndexAPI.deleteByQueryBuilder(queryBuilder, username);
        } catch (SearchException exp) {
            Log.log.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    /**
     * Index worksheet.
     * 
     * @param worksheet Worksheet to index
     * @param username
     */
    public static void indexWorksheet(final Worksheet worksheet, final String username)
    {
        try
        {
            // set the flag only if it's SIR worksheet.
            if (worksheet.getModified() == null && StringUtils.isNotBlank(worksheet.getSirId()))
            {
                worksheet.setModified(true);
            }
            
            worksheetIndexAPI.indexDocument(worksheet, username);
            
            /* async strategy
            long currentTimeInMillis = System.currentTimeMillis();

            //if there is a burst, go async.
            if(currentTimeInMillis - worksheetResultLastIndexTime > threshold)
            {
                worksheetResultLastIndexTime = currentTimeInMillis;
                WorksheetCAS worksheet = WorksheetDAO.getInstance().findById(sysId);
                worksheetIndexAPI.indexDocument(worksheet, username);
            }
            else
            {
                indexWorksheets(Arrays.asList(sysId), username);
            }
            */
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     * Index Execution Summary.
     * 
     * @param executionSummary
     * @param username
     */
    public static void indexExecutionSummary(final ExecutionSummary executionSummary, boolean archive, final String username)
    {
        long startTime = 0;
        if (PerformanceDebug.debugRSControl())
        {
            startTime = System.currentTimeMillis();
        }
    	
        if (executionSummary == null)
        {
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug("Null executionSummary object, check the calling code");
        	}
        }
        else
        {
            try
            {
                executionSummaryIndexerAPI.indexDocument(executionSummary, username);
            }
            catch (SearchException e)
            {
                //TODO eventually we'll have to throw this outside.
                Log.log.error(e.getMessage(), e);
            }
        }

        if (PerformanceDebug.debugRSControl())
        {
        	Log.log.info(String.format("indexExecutionSummary: executionSummary id=%s, worksheetid=%s, for archive=%b, " +
					   	 "total took %d msec", executionSummary.getSysId(), 
					   	 executionSummary.getWorksheetId(), archive, (System.currentTimeMillis()-startTime)));
        }
    }

    public static void indexWorksheets(final List<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.INDEX, true, username);
            worksheetIndexer.enqueue(indexData);
        }
    }

    /**
     * Index ProcessRequest.
     * 
     * @param sysId
     * @param username
     * @throws SearchException
     */
    public static void indexProcessRequest(final ProcessRequest processRequest, final String username)
    {
        try
        {
            Worksheet ws = WorksheetUtil.findWorksheetById(processRequest.getProblem(), username);
            if(processRequest.getModified() == null && ws != null && StringUtils.isNotBlank(ws.getSirId()))
            {
                // set the flag only if process request belongs to SIR worksheet.
                processRequest.setModified(true);
            }
            
            processRequestIndexAPI.indexDocument(processRequest, username);
            //if there is a burst, go async.
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void indexProcessRequests(final List<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.INDEX, true, username);
            processRequestIndexer.enqueue(indexData);
        }
    }

    public static void indexTaskResult(final TaskResult taskResult, final String username) {
    	indexTaskResult(taskResult, username, true);
    }
    
    /**
     * Index Task Result.
     * @param valueMap 
     * 
     * @param sysId
     * @param username
     * @throws SearchException
     */
    public static void indexTaskResult(final TaskResult taskResult, final String username, boolean updateSir)
    {
        if (taskResult == null)
        {
            Log.log.debug("Null taskResult object, check the calling code");
        }
        else
        {
            try
            {
                //going sync until we see performance issue
                if (taskResult.getModified() == null)
                {
                    Worksheet ws = WorksheetUtil.findWorksheetById(taskResult.getProblemId(), username);
                    if (ws != null && StringUtils.isNotBlank(ws.getSirId()))
                    {
                        // set the flag only if task result belongs to SIR worksheet.
                        taskResult.setModified(true);
                    }
                }
                
            	IndexData<TaskResult> indexData = new IndexData<>(taskResult, OPERATION.INDEX, username);
            	taskResultIndexer.index(indexData);

            	if(updateSir) {
                    String sirId = PlaybookUtils.getSIRId(username, taskResult.getProblemId());
                    if (sirId != null) {
                    	PlaybookUtils.touchSIR(username, sirId);
                    }
            	}
            }
            catch (Exception e)
            {
                //TODO eventually we'll have to throw this outside.
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    public static void indexTaskResults(final List<String> sysIds, final String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.INDEX, true, username);
            taskResultIndexer.enqueue(indexData);
        }
    }

    public static void deleteWorksheet(final String sysId, final String username) throws SearchException
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.debug("Blank sysId provided, check the calling code");
        }
        else
        {
            worksheetIndexAPI.deleteDocumentById(sysId, username);
        }
    }

    public static void deleteWorksheets(final List<String> sysIds, final String username) throws SearchException
    {
        deleteWorksheets(sysIds, false, username, true);
    }

    public static Collection<String> deleteWorksheets(List<String> sysIds, final boolean isSync, final String username, boolean filter) throws SearchException
    {
        Collection<String> deletedWorksheetIds = new HashSet<String>();
        
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
        else
        {
            Log.log.info(String.format("Delete %d worksheets, Worksheet ids: %s", sysIds.size(), 
            						   StringUtils.listToString(sysIds, ",")));

            if(isSync)
            {
                if (filter)
                {
                    sysIds = filterSirWorksheets(sysIds);
                }
                
                if (CollectionUtils.isEmpty(sysIds))
                {
                    return deletedWorksheetIds;
                }
                
                long start = System.currentTimeMillis();
                //this will immediately delete the worksheets
                QueryBuilder query1 = QueryBuilders.termsQuery("problemId", sysIds);
                int retry = 0;
                while (retry < 5)
                {
                    try 
                    {
                        taskResultIndexAPI.deleteByQueryBuilder(query1, true, username);
                        retry = 5;
                    }
                    catch (Throwable ex)
                    {
                        Log.log.info(String.format("retry # %d, Delete associated task results for %d worksheed ids",  
                        						   (retry + 1), sysIds.size()));
                        
                        if (retry >= 4) {
                        	Log.log.error(String.format("Error %sin deleting task results for %d worksheet ids after " +
                        								"trying five times", 
                        								(StringUtils.isNotBlank(ex.getMessage()) ? 
                        								 "[" + ex.getMessage() + "] " : ""), sysIds.size()), ex);
                        }
                        
                        try
                        {
                            Thread.sleep(5000);
                        }
                        catch (Exception e)
                        {
                            //ignore
                        }
                    }
                    retry++;
                }
                    
                // delete any process requests belongs to this worksheet
                //query = QueryBuilders.matchQuery("problem", worksheetId);
                QueryBuilder query2 = QueryBuilders.termsQuery("problem", sysIds);
                retry = 0;
                while (retry < 5)
                {
                    try 
                    {
                        processRequestIndexAPI.deleteByQueryBuilder(query2, true, username);
                        retry = 5;
                    }
                    catch (Throwable ex)
                    {
                        Log.log.info(String.format("retry # %d, Delete associated process requests for %d worksheed ids",  
     						   					   (retry + 1), sysIds.size()));
                        
                        if (retry >= 4) {
                        	Log.log.error(String.format("Error %sin deleting process requests for %d worksheet ids after " +
    													"trying five times", 
    													(StringUtils.isNotBlank(ex.getMessage()) ? 
    													 "[" + ex.getMessage() + "] " : ""), sysIds.size()), ex);
                        }
                        
                        try
                        {
                            Thread.sleep(5000);
                        }
                        catch (Exception e)
                        {
                            //ignore
                        }
                    }
                    retry++;
                } 
                
                // finally delete the worksheet
                retry = 0;
                while (retry < 5)
                {
                    try 
                    {
                        deletedWorksheetIds = worksheetIndexAPI.deleteDocumentByIds(sysIds, true, username);
                        retry = 5;
                    }
                    catch (Throwable ex)
                    {
                    	Log.log.info(String.format("retry # %d, Delete %d worksheets",  (retry + 1), sysIds.size()));
                    	
                    	if (retry >= 4) {
                        	Log.log.error(String.format("Error %sin deleting %d worksheets after trying five times", 
    													(StringUtils.isNotBlank(ex.getMessage()) ? 
    													 "[" + ex.getMessage() + "] " : ""), sysIds.size()), ex);
                        }
                    	
                        try
                        {
                            Thread.sleep(5000);
                        }
                        catch (Exception e)
                        {
                            //ignore
                        }
                    }
                    retry++;
                }
                 
//                if(Log.log.isDebugEnabled())
//                {
                    Log.log.info("Total time took to delete " + sysIds.size() + " worksheets: " + (System.currentTimeMillis() - start));
//                }
            }
            else
            {
                IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
                worksheetIndexer.enqueue(indexData);
            }
        }
        
        return deletedWorksheetIds;
    }
    
    private static List<String> filterSirWorksheets(List<String> ids)
    {
        List<String> result = new ArrayList<String>();
        QueryBuilder bqb = boolQuery().mustNot(QueryBuilders.wildcardQuery("sirId", "*-*")).must(QueryBuilders.termsQuery("sysId", ids));
        
        SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
        searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_WORKSHEET);
        searchRequestBuilder.setFetchSource("sysId", null);
        searchRequestBuilder.setSize(ids.size());
        searchRequestBuilder.setQuery(bqb);
        
        SearchResponse response = searchRequestBuilder.execute().actionGet();
        
        if (response.getHits().getTotalHits() > 0)
        {
            for (SearchHit searchHit : response.getHits().getHits())
            {
                result.add(searchHit.getId());
            }
        }
        
        return result;
    }

    public static void deleteWorksheetData(final String worksheetId, final List<String> properyKeys, final String username) throws SearchException
    {
        if (StringUtils.isBlank(worksheetId) || properyKeys == null || properyKeys.size() == 0)
        {
            Log.log.debug("Empty worksheetId or sysIds provided, check the calling code");
        }
        else
        {
            for(String propertyKey : properyKeys)
            {
                worksheetDataIndexAPI.deleteDocumentById(worksheetId+propertyKey, worksheetId, username);
                //worksheetDataIndexAPI.deleteDocumentById(worksheetId+propertyKey, username);
            }
        }
    }

    public static void deleteProcessRequest(final String sysId, final String username) throws SearchException
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.debug("Blank sysId provided, check the calling code");
        }
        else
        {
            processRequestIndexAPI.deleteDocumentById(sysId, username);
        }
    }

    public static void deleteProcessRequests(final List<String> sysIds, final boolean isSync, final String username) throws SearchException
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
        else
        {
            if(isSync)
            {
                long start = System.currentTimeMillis();
                // delete any action results belongs to this process
                QueryBuilder query = QueryBuilders.matchQuery("processId", sysIds);
                taskResultIndexAPI.deleteByQueryBuilder(query, username);
                // finally delete the process request
                processRequestIndexAPI.deleteDocumentByIds(sysIds, username);
                if(Log.log.isDebugEnabled())
                {
                    Log.log.debug("Total time took to delete " + sysIds.size() + " processrequests: " + (System.currentTimeMillis() - start));
                }
            }
            else
            {
                IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
                processRequestIndexer.enqueue(indexData);
            }
        }
    }
    
    public static void deleteAllProcessRequestRows(QueryBuilder queryBuilder, String status, final String username) throws SearchException
    {
        QueryBuilder query = QueryBuilders.matchQuery("status", status.toLowerCase());
        // delete any action results belongs to this process
        taskResultIndexAPI.deleteByQueryBuilder(query, username);
        // finally delete the process request
        processRequestIndexAPI.deleteByQueryBuilder(queryBuilder, username);
    }

    public static void deleteTaskResult(final String sysId, final String username) throws SearchException
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.debug("Blank sysId provided, check the calling code");
        }
        else
        {
            taskResultIndexAPI.deleteDocumentById(sysId, username);
        }
    }

    public static void deleteTaskResults(final List<String> sysIds, final boolean isSync, final String username) throws SearchException
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
        else
        {
            if(isSync)
            {
                long start = System.currentTimeMillis();
                taskResultIndexAPI.deleteDocumentByIds(sysIds, username);
                if(Log.log.isDebugEnabled())
                {
                    Log.log.debug("Total time took to delete " + sysIds.size() + " taskresults: " + (System.currentTimeMillis() - start));
                }
            }
            else
            {
                IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
                taskResultIndexer.enqueue(indexData);
            }
        }
    }

    public static void deleteExecuteStates(final List<String> sysIds, final String username) throws SearchException
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Empty sysIds provided, check the calling code");
            }
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
            executeStateIndexer.enqueue(indexData);
        }
    }

    /**
     * Index worksheet data.
     * 
     * @param worksheetId
     * @param data a Map with property keys and their values.
     * @param username
     */
    public static void indexWorksheetData(final String worksheetId, final Map<String, Object> data, String username)
    {
        if (Log.log.isDebugEnabled() && StringUtils.isBlank(worksheetId))
        {
            Log.log.debug("Blank worksheetId provided, check the calling code");
        }
        else
        {
            try
            {
                ResponseDTO<Worksheet> response = WorksheetSearchAPI.findWorksheetById(worksheetId, username);
                if(response == null || response.getData() == null)
                {
                    throw new SearchException("Worksheet with id " + worksheetId + " not found!");
                }
                else
                {
                    for(String propertyKey : data.keySet())
                    {
                        indexWorksheetData(worksheetId, propertyKey, data.get(propertyKey), username);
                    }
                }
                
            }
            catch (Exception e)
            {
                //TODO eventually we'll have to throw this outside.
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param worksheetId
     * @param username
     */
    public static void indexWorksheetData(final String worksheetId, final String propertyKey, final Object propertyValue, final String username) throws Exception
    {
        if (Log.log.isDebugEnabled() && StringUtils.isBlank(worksheetId))
        {
            Log.log.debug("Blank sysId provided, check the calling code");
        }
        else
        {
            try
            {
                if(propertyValue instanceof Serializable)
                {
                    byte[] data = ObjectProperties.serialize((Serializable)propertyValue);
                    WorksheetData worksheetData = new WorksheetData(worksheetId, propertyKey, data, username);
    
                    //worksheetId field has the routing key for WSDATA
                    //worksheetDataIndexAPI.indexDocument(worksheetData, "worksheetId", false, username);
                    //worksheetDataIndexAPI.indexDocument(worksheetData, false, WriteConsistencyLevel.ONE, username);
                    worksheetDataIndexAPI.indexDocument(worksheetData, "worksheetId", true, ActiveShardCount.ONE, username);
    
                }
                else
                {
                    throw new SearchException("Attempting to store invalid object, must be Serializable.");
                }
            }
            catch (SearchException e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
    }

    /**
     * Index Execute State.
     * 
     * @param sysId
     * @param username
     * @throws SearchException
     */
    public static void indexExecuteState(final ExecuteState executeState, final String username)
    {
        try
        {
            executeStateIndexAPI.indexDocument(executeState, true, ActiveShardCount.ONE, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
        
    }

    /**
     * This method is used to update existing index with new mapping etc.
     * While running this there should not be any transaction happenning to the index
     * because this method will copy the data to a new one and drop the index and 
     * recreate it with new mapping and then copy the data over.
     * 
     * @param indexName either worksheet, processrequest, or taskresult
     * @param batchSize how many records at a time
     * @param username
     * @throws SearchException
     */
    public static void updateIndex(String indexName, int batchSize, boolean force, String username) throws Exception
    {
        String commonSettingsJson = SearchUtils.readJson("common_settings.json");
        Map<String, String> mappings = new HashMap<String, String>();
        String documentType = null;
        IndexAPI indexAPI = null;
        String aliasName = null;
        
        String mappingVersion = null;
        if(SearchConstants.INDEX_NAME_TASKRESULT.equals(indexName))
        {
            String mappingJson = SearchUtils.readJson("task_results_mappings.json");
            mappings.put(SearchConstants.DOCUMENT_TYPE_TASKRESULT, mappingJson);
            documentType = SearchConstants.DOCUMENT_TYPE_TASKRESULT;
            indexAPI = taskResultIndexAPI;
            aliasName = SearchConstants.INDEX_NAME_TASKRESULT_ALIAS;
            mappingVersion = SearchUtils.readMappingVersion(mappingJson, documentType);
        }
        else if(SearchConstants.INDEX_NAME_PROCESSREQUEST.equals(indexName))
        {
            String mappingJson = SearchUtils.readJson("process_requests_mappings.json");
            mappings.put(SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST, mappingJson);
            documentType = SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST;
            indexAPI = processRequestIndexAPI;
            aliasName = SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS;
            mappingVersion = SearchUtils.readMappingVersion(mappingJson, documentType);
        }
        else if(SearchConstants.INDEX_NAME_WORKSHEET.equals(indexName))
        {
            String mappingJson = SearchUtils.readJson("worksheets_mappings.json");
            mappings.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET, mappingJson);
            documentType = SearchConstants.DOCUMENT_TYPE_WORKSHEET;
            indexAPI = worksheetIndexAPI;
            aliasName = SearchConstants.INDEX_NAME_WORKSHEET_ALIAS;
            mappingVersion = SearchUtils.readMappingVersion(mappingJson, documentType);
        }
        else
        {
            Log.log.warn("Invalid index name: " + indexName + " provided");
            throw new SearchException("Invalid index name: " + indexName + " provided");
        }
        
        try
        {
            //do the today right away, then the others
            
            DateTime dateTime = DateUtils.GetUTCDate();
            String dynamicIndexName = SearchUtils.getDynamicIndexName(indexName, dateTime);

            IndicesAliasesRequestBuilder alias = SearchAdminAPI.getClient().admin().indices().prepareAliases();

            //check if the mapping version is same. if yes, not need to update
            String existingMappingVersion = SearchAdminAPI.getExistingMappingVersion(dynamicIndexName, documentType);
            if(!mappingVersion.equalsIgnoreCase(existingMappingVersion) || force)
            {
                //current month start
                String tmpIndex = moveIndexToTemp(indexName, batchSize, username, commonSettingsJson, mappings, documentType, indexAPI, mappingVersion, dynamicIndexName);
                alias.addAlias(dynamicIndexName, aliasName);
                IndicesAliasesResponse indicesAliasesResponse = alias.execute().actionGet();
                if(indicesAliasesResponse.isAcknowledged())
                {
                    Log.log.debug("Successfully added index for today to alias: " + aliasName);
                    Log.log.debug("Resolve ready for Runbook execution...");
                }
                
                ResolveEventVO eventVO = new ResolveEventVO();
                //this value is very very important, before changing it talk to Justin
                //because update/upgrade script relies on this
                Log.log.info(dynamicIndexName + " index is ready to accept data.");
                eventVO.setUValue("ES Migration v2 Ready");
                ResolveEventUtil.insertResolveEvent(eventVO);
                
                moveIndexFromTemp(indexName, tmpIndex, batchSize, username, commonSettingsJson, mappings, documentType, indexAPI, mappingVersion, dynamicIndexName);
            }
            
            //this value is very very important, before changing it talk to Justin
            //because update/upgrade script relies on this
            ResolveEventVO eventVO1 = new ResolveEventVO();
            Log.log.info(dynamicIndexName + " index: ES Migration v2 Ready");
            eventVO1.setUValue("ES Migration v2 Ready");
            ResolveEventUtil.insertResolveEvent(eventVO1);

            //do the other daily indices
            dateTime = dateTime.minusDays(1);
            while(true)
            {
                dynamicIndexName = SearchUtils.getDynamicIndexName(indexName, dateTime);
                if(SearchAdminAPI.isIndexExists(dynamicIndexName))
                {
                    updateIndex(indexName, batchSize, force, username, commonSettingsJson, mappings, documentType, indexAPI, mappingVersion, dynamicIndexName);
                    alias.addAlias(dynamicIndexName, aliasName);
                }
                else
                {
                    break;
                }
                dateTime = dateTime.minusDays(1);
            }
            
            //if there are still monthly indices exists then update them as well
            for(int i=1; i<=12; i++)
            {
                dynamicIndexName = indexName + "_" + i;
                if(SearchAdminAPI.isIndexExists(dynamicIndexName))
                {
                    updateIndex(indexName, batchSize, force, username, commonSettingsJson, mappings, documentType, indexAPI, mappingVersion, dynamicIndexName);
                    alias.addAlias(dynamicIndexName, aliasName);
                }
            }
            
            IndicesAliasesResponse indicesAliasesResponse1 = alias.execute().actionGet();
            if(indicesAliasesResponse1.isAcknowledged())
            {
                Log.log.debug("Successfully added other indexes to alias : " + aliasName);
            }
            
            ResolveEventVO eventVO2 = new ResolveEventVO();
            Log.log.info("All indices updated and ES Migration v2 Ready");
            eventVO2.setUValue("ES Migration v2 Ready");
            ResolveEventUtil.insertResolveEvent(eventVO2);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    private static void updateIndex(String indexName, int batchSize, boolean force, String username, String commonSettingsJson, Map<String, String> mappings, String documentType, IndexAPI indexAPI, String mappingVersion, String dynamicIndexName) throws SearchException
    {
        //check if the mapping version is same. if yes, not need to update
        String existingMappingVersion = SearchAdminAPI.getExistingMappingVersion(dynamicIndexName, documentType);
        if(!mappingVersion.equalsIgnoreCase(existingMappingVersion) || force)
        {
            Log.log.info("Updating " + dynamicIndexName + " started");
            String tmpIndex = dynamicIndexName+ "_copy";
            SearchAdminAPI.createIndex(tmpIndex, commonSettingsJson, mappings, 1, 0);
            
            indexAPI.moveData(dynamicIndexName, tmpIndex, documentType, batchSize, username);
   
            //remove existing index
            Log.log.info("Deleting index " + dynamicIndexName);
            SearchAdminAPI.removeIndex(dynamicIndexName);
   
            //recreate with latest mappings
            Log.log.info("Recreating index " + dynamicIndexName + " with latest mappings");
            SearchAdminAPI.createIndex(dynamicIndexName, commonSettingsJson, mappings);
            
            //move the data back
            Log.log.info("Moving back data for " + dynamicIndexName);
            indexAPI.moveData(tmpIndex, dynamicIndexName, documentType, batchSize, username);
            
            //remove temporary indexes
            SearchAdminAPI.removeIndex(tmpIndex);
            Log.log.info("Finished updating " + indexName);
        }
        else
        {
            Log.log.info("Index mapping version are same, no need to update.");
        }
    }

    private static String moveIndexToTemp(String indexName, int batchSize, String username, String commonSettingsJson, Map<String, String> mappings, String documentType, IndexAPI indexAPI, String mappingVersion, String dynamicIndexName) throws SearchException
    {
        String tmpIndex = dynamicIndexName+ "_copy";
        Log.log.info("Updating " + dynamicIndexName + " started");
        SearchAdminAPI.createIndex(tmpIndex, commonSettingsJson, mappings, 1, 0);
            
        indexAPI.moveData(dynamicIndexName, tmpIndex, documentType, batchSize, username);
   
        //remove existing index
        Log.log.info("Deleting index " + dynamicIndexName);
        SearchAdminAPI.removeIndex(dynamicIndexName);
   
            //recreate with latest mappings
        Log.log.info("Recreating index " + dynamicIndexName + " with latest mappings");
        SearchAdminAPI.createIndex(dynamicIndexName, commonSettingsJson, mappings);
        
        return tmpIndex;
    }

    private static void moveIndexFromTemp(String indexName, String tmpIndex, int batchSize, String username, String commonSettingsJson, Map<String, String> mappings, String documentType, IndexAPI indexAPI, String mappingVersion, String dynamicIndexName) throws SearchException
    {
        //move the data back
        Log.log.info("Moving back data for " + dynamicIndexName);
        indexAPI.moveData(tmpIndex, dynamicIndexName, documentType, batchSize, username);
        
        //remove temporary indexes
        SearchAdminAPI.removeIndex(tmpIndex);
        Log.log.info("Finished updating " + indexName);
    }
   
    public static void truncateRunbookIndex(String indexName, String month)
    {
        try
        {
            String commonSettingsJson = SearchUtils.readJson("common_settings.json");
            Map<String, String> mappings = new HashMap<String, String>();
            String aliasName = null;
            
            boolean isMonthlyIndex = false;
            
            if(SearchConstants.INDEX_NAME_TASKRESULT.equalsIgnoreCase(indexName))
            {
                String mappingJson = SearchUtils.readJson("task_results_mappings.json");
                mappings.put(SearchConstants.DOCUMENT_TYPE_TASKRESULT, mappingJson);
                aliasName = SearchConstants.INDEX_NAME_TASKRESULT_ALIAS;
                isMonthlyIndex = true;
            }
            else if(SearchConstants.INDEX_NAME_PROCESSREQUEST.equalsIgnoreCase(indexName))
            {
                String mappingJson = SearchUtils.readJson("process_requests_mappings.json");
                mappings.put(SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST, mappingJson);
                aliasName = SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS;
                isMonthlyIndex = true;
            }
            else if(SearchConstants.INDEX_NAME_WORKSHEET.equalsIgnoreCase(indexName))
            {
                String mappingJson = SearchUtils.readJson("worksheets_mappings.json");
                mappings.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET, mappingJson);
                aliasName = SearchConstants.INDEX_NAME_WORKSHEET_ALIAS;
                isMonthlyIndex = true;
            }

            if(isMonthlyIndex)
            {
                IndicesAliasesRequestBuilder alias = SearchAdminAPI.getClient().admin().indices().prepareAliases();
                for(int i = 1; i <= 12; i++)
                {
                    String monthlyIndexName = indexName + "_" + i;
                    if(StringUtils.isBlank(month))
                    {
                        SearchAdminAPI.removeIndex(monthlyIndexName);
                        SearchAdminAPI.createIndex(monthlyIndexName, commonSettingsJson, mappings);
                        alias.addAlias(monthlyIndexName, aliasName);
                    }
                    else if(month.trim().equals(""+i))
                    {
                        SearchAdminAPI.removeIndex(monthlyIndexName);
                        SearchAdminAPI.createIndex(monthlyIndexName, commonSettingsJson, mappings);
                        alias.addAlias(monthlyIndexName, aliasName);
                        break;
                    }
                }
                IndicesAliasesResponse indicesAliasesResponse = alias.execute().actionGet();
                if(indicesAliasesResponse.isAcknowledged())
                {
                    Log.log.debug("Successfully created index alias : " + aliasName);
                }
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * This method will delete dynamically generated daily indices that falls between start end end date. 
     * If provided both start and end dates are inclusive.
     * 
     * @param indexName
     * @param start
     * @param end
     * @throws SearchException 
     */
    public static void deleteRunbookIndex(String indexNames, String start, String end) throws SearchException
    {
        try
        {
            Set<String> indices = StringUtils.stringToSet(indexNames, ",");
            
            DateTime startDate = null;
            if(StringUtils.isNotBlank(start))
            {
                startDate = DateTime.parse(start, SearchUtils.DATE_FORMAT);
            }
            DateTime endDate = DateTime.parse(end, SearchUtils.DATE_FORMAT);
            
            //we will not let the user delete today's index
            DateTime today = DateUtils.GetUTCDate().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
            if(endDate.compareTo(today) >= 0)
            {
                Log.log.debug("Day of year : " + endDate.getDayOfYear());
                throw new SearchException("End must be lest than today's date.");
            }
            else
            {
                for(String indexName : indices)
                {
                    Log.log.debug("Deleting indices for : " + indexName);
                    DateTime current = endDate;
                    if(startDate == null)
                    {
                        //this loop will continue until it does not find an index
                        while(true)
                        {
                            String dynamicIndexName = SearchUtils.getDynamicIndexName(indexName, current);
                            Log.log.debug("Deleting daily index : " + dynamicIndexName);
                            if(SearchAdminAPI.isIndexExists(dynamicIndexName))
                            {
                                SearchAdminAPI.removeIndex(dynamicIndexName);
                            }
                            else
                            {
                                Log.log.debug("Daily index : " + dynamicIndexName + " does not exist. Exiting.");
                                break;
                            }
                            current = current.minusDays(1);
                        }
                    }
                    else
                    {
                        //this loop will continue until it hits start date
                        while(current.compareTo(startDate) >= 0)
                        {
                            String dynamicIndexName = SearchUtils.getDynamicIndexName(indexName, current);
                            Log.log.debug("Deleting daily index : " + dynamicIndexName);
                            if(SearchAdminAPI.isIndexExists(dynamicIndexName))
                            {
                                SearchAdminAPI.removeIndex(dynamicIndexName);
                            }
                            else
                            {
                                Log.log.debug("Daily index : " + dynamicIndexName + " does not exist. Continuing to next index.");
                            }
                            current = current.minusDays(1);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
    }

    
    /**
     * API to be called from rsconsole to generate old dated RBC indices corresponding
     * to the worksheets starting from the given date with the given RBC code.
     * 
     * @param startDate : String representing date in the format MM/dd/yyyy. E.g. 01/15/2015
     * @param rbc : String representing rbc code. Accepted values are
     * 			 event/Event, ticket/Ticket, unclassified/Unclassified, unbillable/Unbillable or a slash, "/", for default.
     * @throws Exception
     */
    public static void generateRBCForOldWorksheets(String startDate, String rbc) throws Exception
    {
    	if (StringUtils.isBlank(startDate))
    	{
    		Log.log.error("StartDate cannot be empty and should be in the format: MM/dd/yyyy. E.g. 01/01/2015.");
    		throw new Exception ("StartDate cannot be empty and should be in the format: MM/dd/yyyy. E.g. 01/01/2015.");
    	}
    	
    	long startLong = GMTDate.getTime();
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date date = sdf.parse(startDate);
            startLong = date.getTime();
        }
        catch(Exception e)
        {
            Log.log.error("The date should be in the format: MM/dd/yyyy. E.g. 01/15/2015");
            throw new Exception("The date should be in the format: MM/dd/yyyy. E.g. 01/15/2015");
        }
    	
        List<String> rbcList = new ArrayList<String>();
        rbcList.add("Event");
        rbcList.add("Ticket");
        rbcList.add("Unclassified");
        rbcList.add("");
        rbcList.add("Unbillable");
        
        if (StringUtils.isNotBlank(rbc) && rbc.equals("/"))
        {
       	 rbc = "";
        }
        String rbcCode = StringUtils.capitalize(rbc);
        if (!rbcList.contains(rbcCode))
        {
        	Log.log.error("Unknown RBC code: " + rbc + ". Supported codes are: event/Event, ticket/Ticket, unclassified/Unclassified, unbillable/Unbillable or a slash, \"/\", for default.");
            throw new Exception("Unknown RBC code: " + rbc + ". Supported codes are: event/Event, ticket/Ticket, unclassified/Unclassified, unbillable/Unbillable or a slash, \"/\", for default.");
        }
        
        rbcCode = "/$" + rbcCode;
        
        // Get the earliest date until which we have executionsummary indexed.
    	SearchRequestBuilder searchRequestBuilder = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS);
//        searchRequestBuilder.setSearchType(SearchType.COUNT);
        searchRequestBuilder.setSize(0);
        searchRequestBuilder.setQuery(QueryBuilders.matchAllQuery());
        searchRequestBuilder.addAggregation(AggregationBuilders.min("agg").field("sysCreatedOn"));

        SearchResponse response1 = searchRequestBuilder.execute().actionGet();

        Min agg = response1.getAggregations().get("agg");
        
        // subtract 1 milisecond from the time so that this earliest index would not get counted.
        long endLong = (long)agg.getValue() - 1;
        if (endLong > GMTDate.getTime())
        {
        	endLong = GMTDate.getTime();
        }
    	
//    	SearchRequestBuilder srb = new SearchRequestBuilder(SearchAdminAPI.getClient());
//        srb.setIndices(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY);
//        //srb.setSearchType(SearchType.QUERY_AND_FETCH);
//        srb.addSort("sysCreatedOn", SortOrder.ASC);
//        srb.addField("sysCreatedOn");
//        srb.setSize(1);
//        SearchResponse response = srb.execute().actionGet();
//        SearchHit[] searchHits = response.getHits().getHits();
//        
//        // essd = execution summary start date
//        long essd = GMTDate.getTime();
//        
//        if (searchHits != null && searchHits.length > 0)
//        {
//        	SearchHit hit = searchHits[0];
//        	essd = (Long)hit.getSortValues()[0];
//        }
        
        
        long count = SearchAdminAPI.getClient().prepareSearch(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_WORKSHEET)
        .setSize(0).get().getHits().getTotalHits();
        
        SearchRequestBuilder srb = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
        srb.setIndices(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_WORKSHEET);
        srb.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        srb.setSize((int)count);
        srb.addSort("sysCreatedOn", SortOrder.ASC);
        
        BoolQueryBuilder boolQueryBuilder = boolQuery();
        RangeQueryBuilder rangeQueryBuilder = rangeQuery("sysCreatedOn").from(startLong).to(endLong);
        
        boolQueryBuilder.must(rangeQueryBuilder);
        boolQueryBuilder.must(QueryBuilders.wildcardQuery("summary", "runbook: *"));
        
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*resolve.*"));
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*devops.*"));
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*system.*"));
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*systemadmin.*"));
        
        srb.setQuery(boolQueryBuilder);
        
        SearchResponse response = srb.execute().actionGet();
        if (response != null)
        {
        	SearchHits hits = response.getHits();
        	if (hits != null && hits.hits() != null)
        	{
        		SearchHit[] searchHit = hits.hits();
        		if (searchHit.length > 0)
        		{
        			for (SearchHit hit : searchHit)
        			{
        				Map<String, Object> params = hit.getSource();
        				List<ProcessRequest> processRequests = null;
        				
        				try
        				{
        					String problemId = (String)params.get("sysId");
        					processRequests = WorksheetSearchAPI.findProcessRequestByWorksheetId(problemId, "system").getRecords();
        				}
        				catch(SearchException se)
        				{
        					Log.log.error("Could not get process request for problemId: " + (String)params.get("sysId"), se);
        				}
        				
        				if (processRequests != null && processRequests.size() > 0)
        				{
        					String status = processRequests.get(0).getStatus();
        					if (StringUtils.isNotBlank(status) && status.equalsIgnoreCase("COMPLETED"))
        					{
        						indexExecutionSummary(params, rbcCode);
        					}
        				}
        			}
        		}
        	}
        }
    }
    
    public static void indexExecutionSummary(Map<String, Object> params, String rbc)
    {
    	ExecutionSummary es = new ExecutionSummary(Guid.getGuid(true), null);
        String runbookParams = "";
        String username = params.get("sysCreatedBy").toString();
        
        es.setRbc(rbc);
        es.setWorksheetId(params.get("sysId").toString());
        es.setSysCreatedBy(username);
        es.setSysUpdatedBy(username);
        es.setRunbookParams(runbookParams);
        es.setReference(params.get("reference").toString());
        
        long now = (long)params.get("sysCreatedOn");
        es.setSysCreatedOn(now);
        es.setSysUpdatedOn(now);
        es.setSysCreatedDt(GMTDate.getDate(now));
        es.setSysUpdatedDt(es.getSysCreatedDt());
        
        String wikiName = params.get("summary").toString();
        if (StringUtils.isBlank(wikiName))
        {
        	wikiName = "";
        }
        else
        {
        	wikiName = StringUtils.substringAfter(wikiName, "Runbook: ");
        }
        es.setRunbook(wikiName);
        
        String hash = com.resolve.services.util.UserUtils.bcryptPasswdHash(rbc + wikiName.toLowerCase() + runbookParams);
        es.setHash(hash);
        
        Log.log.debug("Generating RBC code for problemId: " + es.getWorksheetId() + ", sysCreatedOn: " + es.getSysCreatedOn() + ", wikiName: " + wikiName);
        WorksheetIndexAPI.indexExecutionSummary(es, true, username);
    }
    
    @Deprecated
    public static void deleteWorksheetDatas(final List<String> sysIds, final boolean isSync, 
    										final String username) throws SearchException {
        if (CollectionUtils.isEmpty(sysIds)) {
            Log.log.debug("Empty sysIds provided, check the calling code");
        } else {
            long start = System.currentTimeMillis();
            worksheetDataIndexAPI.deleteDocumentByIds(sysIds, isSync, username);
            if(Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("Took %d ms to delete %d worksheet data documents", 
                							(System.currentTimeMillis() - start), sysIds.size()));
            }
        }
    }
}
