package com.resolve.search.model;

public class MetricActionTaskData extends MetricData
{
    private static final long serialVersionUID = -2678938831771860112L;
	private String name;
    private long value;
    private long count;
    private String group;
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String n)
    {
        name = n;
    }
    
    public long getValue()
    {
        return value;
    }
    
    public void setValue(long v)
    {
        value = v;
    }
    
    public long getCount()
    {
        return count;
    }
    
    public void setCount(long c)
    {
        count = c;
    }
    
    public String getGroup()
    {
        return group;
    }
    
    public void setGroup(String n)
    {
        group = n;
    }
    
}
