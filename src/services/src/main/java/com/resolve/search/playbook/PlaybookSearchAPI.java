/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.playbook;

import java.util.ArrayList;
import java.util.Collection;

import org.elasticsearch.index.query.QueryBuilder;

import com.resolve.exception.LimitExceededException;
import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.PBActivity;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBAuditLog;
import com.resolve.search.model.PBNotes;
import com.resolve.search.model.SecurityIncident;
import com.resolve.services.hibernate.util.RBACUtils;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class PlaybookSearchAPI
{
	private static final SearchAPI<PBActivity> pbActivitySearchAPI = APIFactory.getPBActivitySearchAPI();
	private static final SearchAPI<PBNotes> pbNotesSearchAPI = APIFactory.getPBNotesSearchAPI();
	private static final SearchAPI<PBArtifacts> pbArtifactsSearchAPI = APIFactory.getPBArtifactsSearchAPI();
	private static final SearchAPI<PBAttachments> pbAttachmentSearchAPI = APIFactory.getPBAttachmentSearchAPI();
	private static final SearchAPI<PBAuditLog> pbAuditLogSearchAPI = APIFactory.getPBAuditLogSearchAPI();
	private static final SearchAPI<SecurityIncident> securityIncidentSearchAPI = APIFactory.getSecurityIncidentSearchAPI();
	
	public static ResponseDTO<PBActivity> searchPBActivity(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<PBActivity> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            result = pbActivitySearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBActivity> searchPBActivity(String activityId, String incidentId, String username) throws SearchException
    {
        ResponseDTO<PBActivity> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        queryDTO.addFilterItem(new QueryFilter("auto ",activityId, "sysId", QueryFilter.EQUALS, true));
        queryDTO.addFilterItem(new QueryFilter("auto ",incidentId, "incidentId", QueryFilter.EQUALS, true));
        
        try
        {
            result = pbActivitySearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBNotes> searchPBNotes(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<PBNotes> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            massageTermsQuery(queryDTO);
			result = pbNotesSearchAPI.searchByQuery(queryDTO, username);
            
            // Check users access to SIR's Org this PBNotes is associated with
            
            if (result.isSuccess() && result.getRecords() != null && !result.getRecords().isEmpty())
            {
                Collection<PBNotes> pbPBNotesToRemove = new ArrayList<PBNotes>();
                
                for (PBNotes pbNote : result.getRecords())
                {
                    if (StringUtils.isNotBlank(pbNote.getIncidentId()))
                    {
                        try
                        {
                            PlaybookUtils.getSecurityIncident(pbNote.getIncidentId(), null, username);
                        }
                        catch (Exception e)
                        {
                            Log.log.debug("Error " + e.getLocalizedMessage() + 
                                          " in getting SIR associated with Playbook Note for Incident Id " +
                                          pbNote.getIncidentId() + ". This Playbook Note will be removed from list returned."); 
                            pbPBNotesToRemove.add(pbNote);
                        }
                    }
                }
                
                if (!pbPBNotesToRemove.isEmpty())
                {
                    result.getRecords().removeAll(pbPBNotesToRemove);
                    result.setTotal(result.getRecords().size());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBNotes> searchPBNotesByIncidentId(String incidentId, String username, int maxLimit) throws SearchException, LimitExceededException
    {
        ResponseDTO<PBNotes> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
		int limit = getItemsLimit(queryDTO.getLimit(), maxLimit);
        queryDTO.setLimit(limit);
		
        queryDTO.addFilterItem(new QueryFilter("incidentId", QueryFilter.EQUALS, incidentId));
        queryDTO.addFilterItem(new QueryFilter("componentId", QueryFilter.NOT_EXISTS));
        
        try
        {
            PlaybookUtils.getSecurityIncident(incidentId, null, username);
        }
        catch (Exception e)
        {
            throw new SearchException(e.getMessage());
        }
        
        try
        {
            result = pbNotesSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBNotes> searchPBNotesBySysId(String sysId, String username) throws SearchException
    {
        ResponseDTO<PBNotes> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        queryDTO.addFilterItem(new QueryFilter("sysId", QueryFilter.EQUALS, sysId));
        try
        {
            result = pbNotesSearchAPI.searchByQuery(queryDTO, username);
            
            // Check users access to SIR's Org this PBNotes is associated with
            
            if (result.isSuccess() && result.getRecords() != null && !result.getRecords().isEmpty())
            {
                Collection<PBNotes> pbNotesToRemove = new ArrayList<PBNotes>();
                
                for (PBNotes pbNote : result.getRecords())
                {
                    if (StringUtils.isNotBlank(pbNote.getIncidentId()))
                    {
                        try
                        {
                            PlaybookUtils.getSecurityIncident(pbNote.getIncidentId(), null, username);
                        }
                        catch (Exception e)
                        {
                            Log.log.debug("Error " + e.getLocalizedMessage() + 
                                          " in getting SIR associated with Playbook Notes for Activity named " +
                                          pbNote.getActivityName() + " and component id " + pbNote.getComponentId() +
                                          ". This Playbook Notes will be removed from list returned."); 
                            pbNotesToRemove.add(pbNote);
                        }
                    }
                }
                
                if (!pbNotesToRemove.isEmpty())
                {
                    result.getRecords().removeAll(pbNotesToRemove);
                    result.setTotal(result.getRecords().size());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBNotes> searchPBNote(String category, String activityName, String sir, String username) throws SearchException
    {
        ResponseDTO<PBNotes> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        queryDTO.addFilterItem(new QueryFilter("auto",category, "category", QueryFilter.EQUALS, true));
        queryDTO.addFilterItem(new QueryFilter("auto",activityName, "activityName", QueryFilter.EQUALS, true));
        queryDTO.addFilterItem(new QueryFilter("auto", sir, "incidentId", QueryFilter.EQUALS, true));
        queryDTO.addFilterItem(new QueryFilter("componentId", QueryFilter.NOT_EXISTS));
        queryDTO.addFilterItem(new QueryFilter("activityId", QueryFilter.NOT_EXISTS));
        
        try
        {
            result = pbNotesSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	// Artifacts
	public static ResponseDTO<PBArtifacts> searchPBArtifacts(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<PBArtifacts> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
			massageTermsQuery(queryDTO);
            result = pbArtifactsSearchAPI.searchByQuery(queryDTO, username);
            
            // Check users access to SIR's Org this PBArtifacts is associated with
            
            if (result.isSuccess() && result.getRecords() != null && !result.getRecords().isEmpty())
            {
                Collection<PBArtifacts> pbArtifactsToRemove = new ArrayList<PBArtifacts>();
                
                for (PBArtifacts pbArtifact : result.getRecords())
                {
                    if (StringUtils.isNotBlank(pbArtifact.getIncidentId()))
                    {
                        ResolveSecurityIncidentVO incidentVO = null;
                        
                        try
                        {
                            PlaybookUtils.getSecurityIncident(pbArtifact.getIncidentId(), null, username);
                        }
                        catch (Exception e)
                        {
                            Log.log.debug("Error " + e.getLocalizedMessage() + 
                                          " in getting SIR associated with Playbook Artifact for Artifact named " +
                                          pbArtifact.getName() + ". This Playbook Artifact will be removed from list returned."); 
                            pbArtifactsToRemove.add(pbArtifact);
                        }
                    }
                }
                
                if (!pbArtifactsToRemove.isEmpty())
                {
                    result.getRecords().removeAll(pbArtifactsToRemove);
                    result.setTotal(result.getRecords().size());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	/*
     * Massage the terms query by replacing any white space and a period, '.', by a comma, ','.
     * 
     * Terms query is fired on sourceAndValue property of note, artifact and attachment indices.
     * It's of type 'text', which tokenizes a string with a space. Back end populates this field
     * with words separated by a space, and ES tokenizes all of them.
     * 
     * QueryDTO interpretes terms query, by looking at the comma separated search string and convert them into
     * seperate terms to be searched in. E,g.
     * {"field":"sourceAndValue","type":"terms","condition":"equals","value":"first,second,third","caseSensitive":false}
     * will be converted into:
     *     "terms" : {
     *      "sourceAndValue" : [
     *        "first",
     *        "second",
     *        "third"
     *      ],
     *      "boost" : 1.0
     *    }
     * making sure all the words are searched.
     */
	
	private static void massageTermsQuery(QueryDTO queryDTO)
    {
        if (queryDTO != null && queryDTO.getFilterItems() != null && queryDTO.getFilterItems().size() > 0)
        {
            for (QueryFilter queryFiler : queryDTO.getFilterItems())
            {
                if (StringUtils.isNotBlank(queryFiler.getType()) && queryFiler.getType().equalsIgnoreCase("terms"))
                {
                    String value = queryFiler.getValue();
                    if (StringUtils.isNotBlank(value))
                    {
                        queryFiler.setValue(value.trim().replaceAll(" +", ",").replace(".", ","));
                    }
                }
            }
        }
    }
	
	public static ResponseDTO<PBArtifacts> searchPBArtifactsBySir(String sir, String username, Integer maxLimit) throws SearchException, LimitExceededException {
        ResponseDTO<PBArtifacts> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
		if(maxLimit != null) {
            int limit = getItemsLimit(queryDTO.getLimit(), maxLimit);
            queryDTO.setLimit(limit);
        }
		
		try {
			PlaybookUtils.getSecurityIncident(null, sir, username);

			queryDTO.addFilterItem(new QueryFilter("auto ", sir, "sir", QueryFilter.EQUALS, true));
			result = pbArtifactsSearchAPI.searchByQuery(queryDTO, username);
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		}

		return result;
	}

	public static ResponseDTO<PBArtifacts> searchPBArtifact(String sir, String name, String value, String username) throws SearchException
    {
        ResponseDTO<PBArtifacts> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        try
        {
            queryDTO.addFilterItem(new QueryFilter("auto ", sir, "sir", QueryFilter.EQUALS, true));
            queryDTO.addFilterItem(new QueryFilter("auto ", name, "name", QueryFilter.EQUALS, true));
            queryDTO.addFilterItem(new QueryFilter("auto ", value, "value", QueryFilter.EQUALS, true));
            result = pbArtifactsSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }

	public static ResponseDTO<PBArtifacts> searchPBArtifact(String sir, String name, String username) throws SearchException
    {
        ResponseDTO<PBArtifacts> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        try
        {
        	queryDTO.addFilterItem(new QueryFilter("auto ", sir, "sir", QueryFilter.EQUALS, true));
        	queryDTO.addFilterItem(new QueryFilter("auto ", name, "name", QueryFilter.EQUALS, true));
            result = pbArtifactsSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }

	public static ResponseDTO<PBArtifacts> searchAllArtifacts(String name, String value, String username) throws SearchException
    {
        ResponseDTO<PBArtifacts> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        try
        {
            queryDTO.addFilterItem(new QueryFilter("auto ", name, "name", QueryFilter.EQUALS, true));
            queryDTO.addFilterItem(new QueryFilter("auto ", value, "value", QueryFilter.EQUALS, true));
            result = pbArtifactsSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }

	public static ResponseDTO<PBArtifacts> getIncidentsFromArtifact(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<PBArtifacts> result = null;
        
        try
        {
            result = pbArtifactsSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	//Attachments
	public static ResponseDTO<PBAttachments> searchPBAttachments(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<PBAttachments> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
			massageTermsQuery(queryDTO);
            result = pbAttachmentSearchAPI.searchByQuery(queryDTO, username);
            
            // Check users access to SIR's Org this PBAttachments is associated with
            
            if (result.isSuccess() && result.getRecords() != null && !result.getRecords().isEmpty())
            {
                Collection<PBAttachments> pbAttachmentsToRemove = new ArrayList<PBAttachments>();
                
                for (PBAttachments pbAttachment : result.getRecords())
                {
                    if (StringUtils.isNotBlank(pbAttachment.getIncidentId()))
                    {
                        try
                        {
                            PlaybookUtils.getSecurityIncident(pbAttachment.getIncidentId(), null, username);
                        }
                        catch (Exception e)
                        {
                            Log.log.debug("Error " + e.getLocalizedMessage() + 
                                          " in getting SIR associated with Playbook Attachment for Attachment named " +
                                          pbAttachment.getName() + ". This Playbook Attachment will be removed from list returned."); 
                            pbAttachmentsToRemove.add(pbAttachment);
                        }
                    }
                }
                
                if (!pbAttachmentsToRemove.isEmpty())
                {
                    result.getRecords().removeAll(pbAttachmentsToRemove);
                    result.setTotal(result.getRecords().size());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBAttachments> searchPBAttachmentsByIncident(String incident, String username, Integer maxLimit) 
			throws SearchException, LimitExceededException
    {
        ResponseDTO<PBAttachments> result = null;
        QueryDTO queryDTO = new QueryDTO();
		
		if(maxLimit != null) {
            int limit = getItemsLimit(queryDTO.getLimit(), maxLimit);
            queryDTO.setLimit(limit);
        }
        
        try
        {
            PlaybookUtils.getSecurityIncident(incident, null, username);
        }
        catch (Exception e)
        {
            throw new SearchException(e.getMessage());
        }
        
        try
        {
        	queryDTO.addFilterItem(new QueryFilter("auto", incident, "incidentId", QueryFilter.EQUALS, true));
        	queryDTO.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));
            result = pbAttachmentSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBAttachments> searchPBAttachmentsByName(String sir, String name, String username) throws SearchException
    {
        ResponseDTO<PBAttachments> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        try
        {
        	queryDTO.addFilterItem(new QueryFilter("auto ", sir, "incidentId", QueryFilter.EQUALS, true));
        	queryDTO.addFilterItem(new QueryFilter("auto ", name, "name", QueryFilter.EQUALS, true));
            result = pbAttachmentSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
//	public static ResponseDTO<PBAttachments> searchPBAttachmentsByExt(String sir, String ext, String username) throws SearchException
//    {
//        ResponseDTO<PBAttachments> result = null;
//        QueryDTO queryDTO = new QueryDTO();
//        
//        try
//        {
//        	queryDTO.addFilterItem(new QueryFilter("auto ", sir, "incidentId", QueryFilter.EQUALS, true));
//        	queryDTO.addFilterItem(new QueryFilter("auto ", "*." + ext, "name", QueryFilter.EQUALS, true));
//            result = pbAttachmentSearchAPI.searchByQuery(queryDTO, username);
//        }
//        catch (Exception e)
//        {
//            Log.log.error(e.getMessage(), e);
//            throw new SearchException(e.getMessage(), e);
//        }
//        
//        return result;
//    }
	
	//AuditLog
	public static ResponseDTO<PBAuditLog> searchPBAuditLogs(QueryDTO queryDTO, String username) throws SearchException
    {
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ACTIVITY_TIMELINE_VIEW, null))
        {
            throw new SearchException(String.format("User %s does not have access to view timeline.", username));
        }
	    
        ResponseDTO<PBAuditLog> result = null;

        try
        {
            SearchUtils.sanitizeQueryDTO(queryDTO, SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC);
            boolean incidentFound = false;
            String incidentId = null;
            for (QueryFilter filter : queryDTO.getFilterItems())
            {
                if (filter != null && StringUtils.isNotBlank(filter.getField()) && filter.getField().equalsIgnoreCase("incidentid"))
                {
                    if (incidentFound)
                    {
                        // Only one filter with incidentId is allowed.
                        throw new SearchException("Only one filter with incidentId is allowed to view timeline.");
                    }
                    incidentFound = true;
                    
                    if (StringUtils.isNotBlank(filter.getType()) && filter.getType().equalsIgnoreCase("terms"))
                    {
                        throw new SearchException("terms query is not allowed while viewing timeline.");
                    }
                    
                    incidentId = filter.getValue();
                }
            }
            
            // call to check whether user has access to the org and has RBAC permission to view this incident.
            PlaybookUtils.getSecurityIncident(incidentId, null, username);
            
            result = pbAuditLogSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBAuditLog> searchPBAuditLogsBySIR(String sir, String username, int maxLimit) throws SearchException, LimitExceededException
    {
        ResponseDTO<PBAuditLog> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        try
        {
            int limit = getItemsLimit(queryDTO.getLimit(), maxLimit);
            queryDTO.setLimit(limit);
			PlaybookUtils.getSecurityIncident(sir, null, username);
        }
		catch (LimitExceededException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SearchException(e.getMessage());
        }
        
        try
        {
        	queryDTO.addFilterItem(new QueryFilter("auto ", sir, "incidentId", QueryFilter.EQUALS, true));
            result = pbAuditLogSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	//Actiity/Wiki Notes
	public static ResponseDTO<PBNotes> searchPBWikiNotesByIncidentId(String incidentId, String username, int maxLimit) throws SearchException, LimitExceededException
    {
        ResponseDTO<PBNotes> result = null;
        QueryDTO queryDTO = new QueryDTO();
		
		int limit = getItemsLimit(queryDTO.getLimit(), maxLimit);
        queryDTO.setLimit(limit);
        
        queryDTO.addFilterItem(new QueryFilter("incidentId", QueryFilter.EQUALS, incidentId));
        queryDTO.addFilterItem(new QueryFilter("activityId", QueryFilter.EXISTS));
        queryDTO.addFilterItem(new QueryFilter("componentId", QueryFilter.EXISTS));
        
        try
        {
            PlaybookUtils.getSecurityIncident(incidentId, null, username);
        }
        catch (Exception e)
        {
            throw new SearchException(e.getMessage());
        }
        
        try
        {
            result = pbNotesSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBNotes> searchPBWikiNotesByIncidentAndActivityId(String incidentId, String activityId, String username) throws SearchException
    {
        ResponseDTO<PBNotes> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        try
        {
            PlaybookUtils.getSecurityIncident(incidentId, null, username);
        }
        catch (Exception e)
        {
            throw new SearchException(e.getMessage());
        }
        
        queryDTO.addFilterItem(new QueryFilter("incidentId", QueryFilter.EQUALS, incidentId));
        queryDTO.addFilterItem(new QueryFilter("activityId", QueryFilter.EQUALS, activityId));
        queryDTO.addFilterItem(new QueryFilter("componentId", QueryFilter.EXISTS));
        
        try
        {
            result = pbNotesSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
    }
	
	public static ResponseDTO<PBNotes> searchPBWikiNotesByActivityAndComponentId(String activityId, String componentId, String username) throws SearchException
	{
	    ResponseDTO<PBNotes> result = null;
        QueryDTO queryDTO = new QueryDTO();
        
        queryDTO.addFilterItem(new QueryFilter("activityId", QueryFilter.EQUALS, activityId));
        queryDTO.addFilterItem(new QueryFilter("componentId", QueryFilter.EQUALS, componentId));
        
	    try
        {
            result = pbNotesSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        
        return result;
        
	    //return searchPBNotesBySysId(activityId + "-" + componentId, username);
	}

	public static ResponseDTO<SecurityIncident> searchSecurityIncident(QueryDTO queryDTO, String username)
			throws SearchException {
		try {
            String[] highlightedFields = new String[] {"*"};
            QueryBuilder queryBuilder = securityIncidentSearchAPI.getQueryBuilder(queryDTO);
			return securityIncidentSearchAPI.searchByQuery(queryDTO, queryBuilder, null, null, highlightedFields, username); //
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		}
	}
	
	public static int getItemsLimit(final int limit, final int maxLimit) throws LimitExceededException {
    	
    	if(limit <= 0) {
    		return maxLimit;
    	}
    	
    	if(limit > maxLimit) {
    		throw new LimitExceededException(String.format("Limit value: %s exceeds maximum limit: %s.", limit, maxLimit));
    	}
    	
    	return limit;
    }
}