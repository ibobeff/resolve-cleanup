/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.social;

import java.util.Collection;
import java.util.Map;
import java.util.TreeSet;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.SocialPostResponse;
import com.resolve.search.model.UserInfo;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class SocialIndexer implements Indexer<IndexData<Map<String, Object>>>
{
    private static volatile SocialIndexer instance = null;
    private final IndexAPI indexAPI;
    private final SearchAPI<SocialPost> searchAPI;

    private final ConfigSearch config;
    private long indexThreads;

    private ResolveConcurrentLinkedQueue<IndexData<Map<String, Object>>> dataQueue = null;

    public static SocialIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SocialIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static SocialIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new SocialIndexer(config);
        }
        return instance;
    }

    private SocialIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexAPI = APIFactory.getTagIndexAPI();
        this.indexThreads = this.config.getIndexthreads();
        this.searchAPI = APIFactory.getSocialPostSearchAPI(SocialPost.class);
    }

    public void init()
    {
        Log.log.debug("Initializing SocialIndexer.");
        QueueListener<IndexData<Map<String, Object>>> wikiIndexListener = new IndexListener<IndexData<Map<String, Object>>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiIndexListener);
        Log.log.debug("SocialIndexer initialized.");
    }

    public boolean enqueue(IndexData<Map<String, Object>> indexData)
    {
        return dataQueue.offer(indexData);
    }

    @Override
    public boolean index(IndexData<Map<String, Object>> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<Map<String, Object>> params = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case CUSTOM:
                    executeCustom(params);
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    /**
     * For usage check SocialIndexAPI.markPostAsRead method.
     * 
     * @param objects
     */
    private void executeCustom(Collection<Map<String, Object>> objects)
    {
        if(objects != null && objects.size() > 0)
        {
            Map<String, Object> params = objects.iterator().next();
            try
            {
                String operation = (String) params.get("OPERATION");
                if("SET_READ".equals(operation))
                {
                    setRead(params);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    private void setRead(Map<String, Object> params) throws SearchException
    {
        QueryDTO query = (QueryDTO) params.get("QUERY");
        String streamId = (String) params.get("STREAM_ID");
        String streamType = (String) params.get("STREAM_TYPE");
        TreeSet<String> targetIds = (TreeSet<String>) params.get("TARGET_IDS");
        UserInfo user = (UserInfo) params.get("USER");
        Boolean isRead = (Boolean) params.get("IS_READ");
        Log.log.debug("Marking bulk set of social posts read = " + isRead);

        int limit = 1000;
        query.setStart(0);
        query.setLimit(limit);
        query.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.DESC));
        
        ResponseDTO<SocialPostResponse> responses = search(query, streamId, streamType, targetIds, user);
        
        while(responses.getRecords() != null && responses.getRecords().size() > 0)
        {
            for(SocialPost socialPost : responses.getRecords())
            {
                SocialIndexAPI.setRead(socialPost, user.getUsername(), isRead);
            }
            query.setStart(query.getStart() + limit);
            responses = search(query, streamId, streamType, targetIds, user);
        }
    }
    
    private ResponseDTO<SocialPostResponse> search(QueryDTO query, String streamId, String streamType, TreeSet<String> targetIds, UserInfo user) throws SearchException
    {
        ResponseDTO<SocialPostResponse> responses = new ResponseDTO<SocialPostResponse>();
        
        if (SearchConstants.DISPLAY_CATEGORIES.containsKey(streamId))
        {
            responses = SocialSearchAPI.searchPostsByBucket(streamId, streamType, targetIds, query, false, true, null, null, user);
        }
        else if(StringUtils.isNotBlank(streamId))
        {
            responses = SocialSearchAPI.searchPosts(targetIds, query, null, null, false, true, user);
        }
        
        return responses;
    }
}
