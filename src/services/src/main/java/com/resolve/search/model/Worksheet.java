/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.util.HashMap;
import java.util.Map;

public class Worksheet extends BaseIndexModel
{
    private static final long serialVersionUID = 8377241557215824159L;
	private String alertId;
    private String assignedTo;
    private String assignedToName; //transient
    private String condition;
    private String correlationId;
    private String debug;
    private String description;
    private String number;
    private String reference;
    private String severity;
    private String summary;
    private String workNotes;
    private String worksheet;
    private String sirId;
    private Boolean modified;
    private String incidentId;

    private String worknotesHtml;
    private String worksheetXml;
    //private Map<String, Object> data;

    public Worksheet()
    {
        super();
    }

    public Worksheet(String sysId, String sysOrg, String createdBy)
    {
        super(sysId, sysOrg, createdBy);
    }

    public Worksheet(Map<String, Object> columns)
    {
        super(columns);
        if(columns != null)
        {
            for(String key : columns.keySet())
            {
                if(key.equals("u_alert_id")) alertId = (String) columns.get(key);
                if(key.equals("u_assigned_to")) assignedTo = (String) columns.get(key);
                if(key.equals("u_assigned_to_name")) assignedToName = (String) columns.get(key);
                if(key.equals("u_condition")) condition = (String) columns.get(key);
                if(key.equals("u_correlation_id")) correlationId = (String) columns.get(key);
                if(key.equals("u_debug")) debug = (String) columns.get(key);
                if(key.equals("u_description")) description = (String) columns.get(key);
                if(key.equals("u_number")) number = (String) columns.get(key);
                if(key.equals("u_reference")) reference = (String) columns.get(key);
                if(key.equals("u_severity")) severity = (String) columns.get(key);
                if(key.equals("u_summary")) summary = (String) columns.get(key);
                if(key.equals("u_work_notes")) workNotes = (String) columns.get(key);
                if(key.equals("u_worksheet")) worksheet = (String) columns.get(key);
                if(key.equals("u_sir_id")) sirId = (String) columns.get(key);
            }
        }
    }

    public String getAlertId()
    {
        return alertId;
    }

    public void setAlertId(String alertId)
    {
        this.alertId = alertId;
    }

    public String getAssignedTo()
    {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    public String getAssignedToName()
    {
        return assignedToName;
    }

    public void setAssignedToName(String assignedToName)
    {
        this.assignedToName = assignedToName;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public String getCorrelationId()
    {
        return correlationId;
    }

    public void setCorrelationId(String correlationId)
    {
        this.correlationId = correlationId;
    }

    public String getDebug()
    {
        return debug;
    }

    public void setDebug(String debug)
    {
        this.debug = debug;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getSeverity()
    {
        return severity;
    }

    public void setSeverity(String severity)
    {
        this.severity = severity;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getWorkNotes()
    {
        return workNotes;
    }

    public void setWorkNotes(String workNotes)
    {
        this.workNotes = workNotes;
    }

    public String getWorksheet()
    {
        return worksheet;
    }

    public void setWorksheet(String worksheet)
    {
        this.worksheet = worksheet;
    }

    public String getSirId()
	{
		return sirId;
	}

	public void setSirId(String sirId)
	{
		this.sirId = sirId;
	}

	public Boolean getModified()
    {
        return modified;
    }
    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }

    public String getWorknotesHtml()
    {
        return worknotesHtml;
    }

    public void setWorknotesHtml(String worknotesHtml)
    {
        this.worknotesHtml = worknotesHtml;
    }

    public String getWorksheetXml()
    {
        return worksheetXml;
    }

    public void setWorksheetXml(String worksheetXml)
    {
        this.worksheetXml = worksheetXml;
    }

    public String getIncidentId()
    {
        return incidentId;
    }
    
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
    /*
    public Map<String, Object> getData()
    {
        return data;
    }

    public void setData(Map<String, Object> data)
    {
        this.data = data;
    }
    */
    
    public Map<String, Object> asMap()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        result.putAll(super.asMap());

        result.put("u_alert_id", getAlertId());
        result.put("u_assigned_to", getAssignedTo());
        result.put("u_assigned_to_name", getAssignedToName());
        result.put("u_condition", getCondition());
        result.put("u_correlation_id", getCorrelationId());
        result.put("u_debug", getDebug());
        result.put("u_description", getDescription());
        result.put("u_number", getNumber());
        result.put("u_reference", getReference());
        result.put("u_severity", getSeverity());
        result.put("u_summary", getSummary());
        result.put("u_work_notes", getWorkNotes());
        result.put("u_worksheet", getWorksheet());
        result.put("u_sir_id", getSirId());
        //result.put("u_wsdata", getData());

        return result;
    }
    
    public Map<String, Object> asMap(String[] columnNames)
    {
        Map<String, Object> result = new HashMap<String, Object>();

        //get everything
        Map<String, Object> response = asMap();

        if(columnNames != null && columnNames.length > 0)
        {
            for(String columnName : columnNames)
            {
                if(response.containsKey(columnName))
                {
                    result.put(columnName, response.get(columnName));
                }
            }
        }
        return result;
    }
}
