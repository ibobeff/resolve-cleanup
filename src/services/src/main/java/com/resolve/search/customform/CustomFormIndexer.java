/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.customform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.Server;
import com.resolve.search.model.ActionTask;
import com.resolve.search.model.CustomForm;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class CustomFormIndexer implements Indexer<IndexData<String>> {
	private static volatile CustomFormIndexer instance = null;
	private final IndexAPI indexAPI;

	private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

	public static CustomFormIndexer getInstance() {
		if (instance == null) {
			throw new RuntimeException("CustomFormIndexer is not initialized correctly..");
		} else {
			return instance;
		}
	}

	public static CustomFormIndexer getInstance(ConfigSearch config) {
		if (instance == null) {
			instance = new CustomFormIndexer(config);
		}
		return instance;
	}

	private CustomFormIndexer(ConfigSearch config) {
		this.indexAPI = APIFactory.getCustomFormIndexAPI();
	}

	public void init() {
		Log.log.debug("Initializing CustomFormIndexer.");
		QueueListener<IndexData<String>> propertyIndexListener = new IndexListener<IndexData<String>>(this);
		dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(propertyIndexListener);
		Log.log.debug("CustomFormIndexer initialized.");
	}

	public boolean enqueue(IndexData<String> indexData) {
		return dataQueue.offer(indexData);
	}

	@Override
	public boolean index(IndexData<String> indexData) {
		OPERATION operation = indexData.getOperation();
		Collection<String> sysIds = indexData.getObjects();
		try {
			switch (operation) {
			case INDEX:
				indexCustomForms(sysIds, indexData.getUsername());
				break;
			case DELETE:
				deleteCustomForms(sysIds, indexData.getUsername());
				break;
			case MIGRATE:
				updateIndexMapping();
			default:
				break;
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
		return true;
	}

	private void updateIndexMapping() {
		SearchAdminAPI.updateIndexMapping(SearchConstants.INDEX_NAME_CUSTOM_FORM,
				SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM, Server.ES_CUSTOM_FORM_MAPPING_FILE);
	}

	private void indexCustomForms(Collection<String> sysIds, String username) throws Exception {
		int start = 0;
		int limit = 100; // 100 a batch

		Collection<CustomForm> customForms = new ArrayList<CustomForm>();

		for (String sysId : sysIds) {
			if (start >= limit) {
				indexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, customForms,
						false, username);

				customForms = new ArrayList<CustomForm>();
				start = 0;
				Thread.sleep(500L);
			}
			MetaFormViewVO customFormVO = ServiceHibernate.getCustomFormFromIdWithReferences(sysId, null, username);
			CustomForm customForm = new CustomForm(customFormVO);
			customForms.add(customForm);
			start++;
		}

		if (customForms.size() > 0) {
			indexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, customForms, false,
					username);
		}
	}
	
	private void deleteCustomForms(final Collection<String> sysIds, final String username) throws SearchException {
		if (sysIds == null) {
			Log.log.info("Deleting all custom forms requested by \"" + username + "\"");
			QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
			indexAPI.deleteByQueryBuilder(queryBuilder, username);
		} else {
			indexAPI.deleteDocumentByIds(sysIds, username);
		}
	}
}
