/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.wiki;

import java.util.List;
import java.util.Map;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder.FilterFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.UserInfo;
import com.resolve.search.model.WikiAttachment;
import com.resolve.search.model.WikiDocument;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WikiSearchAPI
{
    private static final SearchAPI<WikiDocument> searchAPI = APIFactory.getWikiSearchAPI();
    private static final SearchAPI<WikiAttachment> attachmentSearchAPI = APIFactory.getWikiAttachmentSearchAPI();

    //the fieldName^int (title^5) provides the boosting score to the records so they comes up on top
    private static final String[] wikiSearchFields = new String[] {"name", "name.exact", "title^5", "fullName", "fullName.exact^4", "summary^3", "content^2", "namespace", "namespace.exact", "processModel", "abortModel", "finalModel", "decisionTree"};
    private static final String[] wikiHighlightedFields = new String[] {"title", "summary"};
    private static final String[] wikiAttachmentSearchFields = new String[] {"filename.exact", "content"};
    
    //private static final String[] allWikiSearchFields = new String[] {"name", "title^5", "fullName^4", "summary^3", "content", "namespace", "processModel", "abortModel", "finalModel", "decisionTree", "attachmentFilename^2"};

    private static final QueryBuilder hiddenDocumentFilter = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("hidden", false));
    private static final QueryBuilder undeletedFilter = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("deleted", false));
    private static QueryBuilder excludeNamespaceFilter = null;

    static
    {
        String excludeNamespacePattern = PropertiesUtil.getPropertyString("search.exclude.namespace");
        if(StringUtils.isNotBlank(excludeNamespacePattern))
        {
            //got to lower case it because the namespace is internally indexed lowercase.
            //do no worry if you see the data as camel case or whatever it is ES internal storage that
            //has them indexed lowercase..
            List<String> excludeNamespaceList = StringUtils.convertStringToList(excludeNamespacePattern.toLowerCase(), ",");
            if(excludeNamespaceList != null && excludeNamespaceList.size() > 0)
            {
                excludeNamespaceFilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.termsQuery("namespace.exact", excludeNamespaceList));
            }
        }
    }

    /**
     * Find the wiki document, if not found then attempt to index it right away.
     * 
     * @param sysId
     * @param username
     * @return
     * @throws SearchException
     */
    public static WikiDocument findWikiDocumentById(String sysId, String username) throws SearchException
    {
        WikiDocument wikiDocument = searchAPI.findDocumentById(sysId, username);
        if(wikiDocument == null)
        {
            //reindex right away, very rare case
            WikiIndexAPI.indexWikiDocument(sysId, true, username);
            wikiDocument = searchAPI.findDocumentById(sysId, username);
        }
        return wikiDocument;
    }

    public static ResponseDTO<WikiDocument> searchWikiDocuments(final QueryDTO queryDTO, final UserInfo userInfo)
    {
        ResponseDTO<WikiDocument> result = new ResponseDTO<WikiDocument>();

        try
        {
            QueryBuilder readRolesFilterBuilder = QueryBuilders.termsQuery("readRoles", userInfo.getRoles());
            if(StringUtils.isNotBlank(queryDTO.getSearchTerm()))
            {
                //this script adds additional boosting to the original score (_score) that was calculated based on the
                //field level boosting (check wikiSearchFields above).
                //String script = "_score * doc['rating'].value + doc['clickCount'].value * doc['rating'].value + _score * doc['sysUpdatedOn'].value";
                //String script = "_score * doc['sysCreatedOn'].value";
                //ScoreFunctionBuilder scoreFunctionBuilder = ScoreFunctionBuilders.scriptFunction(script);
                ScoreFunctionBuilder scoreFunctionBuilder1 = ScoreFunctionBuilders.fieldValueFactorFunction("rating");
                ScoreFunctionBuilder scoreFunctionBuilder2 = ScoreFunctionBuilders.fieldValueFactorFunction("clickCount");
                FunctionScoreQueryBuilder.FilterFunctionBuilder ffb1 = new FilterFunctionBuilder(new MatchAllQueryBuilder() , scoreFunctionBuilder1); 
                FunctionScoreQueryBuilder.FilterFunctionBuilder ffb2 = new FilterFunctionBuilder(new MatchAllQueryBuilder() , scoreFunctionBuilder2); 
                
                QueryBuilder multiMatchQueryBuilder = null;
                QueryBuilder multiMatchQueryBuilderAttachment = null;
                if(SearchUtils.isSimpleQueryString(queryDTO.getSearchTerm()))
                {
                    SimpleQueryStringBuilder simpleQueryStringBuilder = QueryBuilders.simpleQueryStringQuery(queryDTO.getSearchTerm());
                    for(String field : wikiSearchFields)
                    {
                        simpleQueryStringBuilder.field(field);
                    }
                    multiMatchQueryBuilder = simpleQueryStringBuilder;

                    SimpleQueryStringBuilder simpleQueryStringBuilderAttachment = QueryBuilders.simpleQueryStringQuery(queryDTO.getSearchTerm());
                    for(String field : wikiAttachmentSearchFields)
                    {
                        simpleQueryStringBuilderAttachment.field(field);
                    }
                    multiMatchQueryBuilderAttachment = simpleQueryStringBuilderAttachment;
                }
                else
                {
                    multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), wikiSearchFields); //.fuzziness(".25").maxExpansions(5);
                    
                    QueryStringQueryBuilder queryStringQueryBuilder = QueryBuilders.queryStringQuery(queryDTO.getSearchTerm());
                    for(String field : wikiAttachmentSearchFields)
                    {
                        queryStringQueryBuilder.field(field);
                    }
                    //multiMatchQueryBuilderAttachment = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), wikiAttachmentSearchFields); //.fuzziness(".25").maxExpansions(5);
                    multiMatchQueryBuilderAttachment = queryStringQueryBuilder;
                    
                }

                BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
                queryBuilder.minimumShouldMatch(1);
                //main WIKI document
//                queryBuilder.should(QueryBuilders.functionScoreQuery(multiMatchQueryBuilder).boostMode(CombineFunction.REPLACE).add(scoreFunctionBuilder1).add(scoreFunctionBuilder2));
                queryBuilder.should(new FunctionScoreQueryBuilder(multiMatchQueryBuilder, new FilterFunctionBuilder[] { ffb1, ffb2 }).boostMode(CombineFunction.REPLACE)); 
                //QueryBuilders.functionScoreQuery(multiMatchQueryBuilder).boostMode(CombineFunction.REPLACE).add(scoreFunctionBuilder1).add(scoreFunctionBuilder2));
                
                //attachment
                queryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, multiMatchQueryBuilderAttachment, ScoreMode.None));

                //result = searchAPI.searchByTerm(new String[] {SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT, SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT}, SearchConstants.USER_QUERY_TYPE_DOCUMENT, queryDTO, queryBuilder, new FilterBuilder[] {readRolesFilterBuilder}, wikiHighlightedFields, userInfo);
                result = searchAPI.searchByTerm(SearchConstants.USER_QUERY_TYPE_DOCUMENT, queryDTO, queryBuilder, new QueryBuilder[] {readRolesFilterBuilder, hiddenDocumentFilter, undeletedFilter, excludeNamespaceFilter}, wikiHighlightedFields, userInfo);
            }
            else
            {
                QueryBuilder queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
                
                QueryBuilder tmpExcludeNamespaceFilter = excludeNamespaceFilter;
                
                if(queryDTO.getFilterItems() != null && queryDTO.getFilterItems().size() > 0)
                {
                    for(QueryFilter queryFilter : queryDTO.getFilterItems())
                    {
                        if("namespace".equalsIgnoreCase(queryFilter.getField()) && StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                                String[] namespaces = StringUtils.stringToArray(queryFilter.getValue(), ",");

                                //this is the place where, although exclusion clause said namespace must be
                                //excluded from result, but user asked for it through advanced search
                                if(tmpExcludeNamespaceFilter != null)
                                {
                                    String exclusionNamespaces = PropertiesUtil.getPropertyString("search.exclude.namespace");
                                    List<String> excludeNamespaceList = StringUtils.convertStringToList(exclusionNamespaces.toLowerCase(), ",");
                                    boolean needChange = false;
                                    for(String namespace : namespaces)
                                    {
                                        if(excludeNamespaceList.contains(namespace))
                                        {
                                            excludeNamespaceList.remove(namespace);
                                            needChange = true;
                                        }
                                    }
                                    if(needChange)
                                    {
                                    	tmpExcludeNamespaceFilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.termsQuery("namespace.exact", excludeNamespaceList));
                                        //tmpExcludeNamespaceFilter = FilterBuilders.notFilter(FilterBuilders.termsFilter("namespace.exact", excludeNamespaceList));
                                    }
                                }
                            }
                        }
                }
                
                result = searchAPI.searchByQuery(queryDTO, queryBuilder, null, new QueryBuilder[] {readRolesFilterBuilder, hiddenDocumentFilter, undeletedFilter, tmpExcludeNamespaceFilter}, wikiHighlightedFields, userInfo.getUsername());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage(e.getMessage());
        }

        return result;
    }

    public static ResponseDTO<WikiDocument> advancedSearch(final QueryDTO queryDTO, final UserInfo userInfo)
    {
        ResponseDTO<WikiDocument> result = new ResponseDTO<WikiDocument>();

        try
        {
            QueryBuilder queryBuilder = null;
            QueryBuilder rolesFilter = QueryBuilders.termsQuery("readRoles", userInfo.getRoles());
            QueryBuilder namespaceFilter = null;
            QueryBuilder tagFilter = null;
            QueryBuilder typeFilter = null;
            QueryBuilder sysUpdatedByFilter = null;
            QueryBuilder sysUpdatedOnFilter = null;
            QueryBuilder sysCreatedByFilter = null;
            QueryBuilder sysCreatedOnFilter = null;
            //FilterBuilder attachmentFilter = null;
         
            QueryBuilder multiMatchQueryBuilder = null;
            QueryBuilder attachmentMultiMatchQueryBuilder = null;
            
            QueryBuilder tmpExcludeNamespaceFilter = excludeNamespaceFilter;
            
            //this is advanced search
            String[] userSearchFields = wikiSearchFields;
            if(queryDTO.getFilterItems() != null && queryDTO.getFilterItems().size() > 0)
            {
                for(QueryFilter queryFilter : queryDTO.getFilterItems())
                {
                    if("namespace".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            String[] namespaces = StringUtils.stringToArray(queryFilter.getValue(), ",");

                            //check wiki_documents_mappings.json file for the namespace.exact
                            namespaceFilter = QueryBuilders.termsQuery("namespace.exact", namespaces);

                            //this is the place where, although exclusion clause said namespace must be
                            //excluded from result, but user asked for it through advanced search
                            if(tmpExcludeNamespaceFilter != null)
                            {
                                String exclusionNamespaces = PropertiesUtil.getPropertyString("search.exclude.namespace");
                                List<String> excludeNamespaceList = StringUtils.convertStringToList(exclusionNamespaces.toLowerCase(), ",");
                                boolean needChange = false;
                                for(String namespace : namespaces)
                                {
                                    if(excludeNamespaceList.contains(namespace))
                                    {
                                        excludeNamespaceList.remove(namespace);
                                        needChange = true;
                                    }
                                }
                                if(needChange)
                                {
                                    tmpExcludeNamespaceFilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.termsQuery("namespace.exact", excludeNamespaceList)); 
                                    		//FilterBuilders.notFilter(FilterBuilders.termsFilter("namespace.exact", excludeNamespaceList));
                                }
                            }
                        }
                    }
                    else if("tag".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            tagFilter = QueryBuilders.termsQuery("tags", SearchUtils.parseCSV(queryFilter.getValue(), ","));
                        }
                    }
                    else if("type".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            String cond = queryFilter.getCondition();
                            if (cond != null && cond.equals(QueryFilter.NOT_EQUALS)) {
                                typeFilter = QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("type", queryFilter.getValue()));
                            } else {
                                typeFilter = QueryBuilders.matchQuery("type", queryFilter.getValue());
                            }
                        }
                    }
                    else if("sysUpdatedBy".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            sysUpdatedByFilter = QueryBuilders.termQuery("sysUpdatedBy", queryFilter.getValue());
                        }
                    }
                    else if("sysUpdatedOn".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            sysUpdatedOnFilter = SearchUtils.createDateRangeFilterBuilder(queryFilter);
                        }
                    }
                    else if("sysCreatedBy".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            sysCreatedByFilter = QueryBuilders.termQuery("sysCreatedBy", queryFilter.getValue());
                        }
                    }
                    else if("sysCreatedOn".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            sysCreatedOnFilter = SearchUtils.createDateRangeFilterBuilder(queryFilter);
                        }
                    }
                    else if("attachment".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            attachmentMultiMatchQueryBuilder = createQueryBuilder(queryFilter, new String[] {"filename.exact"});
                            //queryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, multiMatchQueryBuilderAttachment));
                            //attachmentFilter = FilterBuilders.hasChildFilter(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, multiMatchQueryBuilderAttachment);
                            queryDTO.setSearchTerm(queryFilter.getValue());
                        }
                    }
                    else if("attachmentContent".equalsIgnoreCase(queryFilter.getField()))
                    {
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            attachmentMultiMatchQueryBuilder = createQueryBuilder(queryFilter, new String[] {"content"});
                            //queryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, multiMatchQueryBuilderAttachment));
                            //attachmentFilter = FilterBuilders.hasChildFilter(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, multiMatchQueryBuilderAttachment);
                            queryDTO.setSearchTerm(queryFilter.getValue());
                        }
                    }
                    else
                    {
                        if(StringUtils.isNotBlank(queryFilter.getField()))
                        {
                            userSearchFields = StringUtils.stringToArray(queryFilter.getField(), ",");
                        }
                        
                        if(StringUtils.isNotBlank(queryFilter.getValue()))
                        {
                            multiMatchQueryBuilder = createQueryBuilder(queryFilter, userSearchFields);
                        }
                        queryDTO.setSearchTerm(queryFilter.getValue());
                    }
                }
            }
            
            if(StringUtils.isBlank(queryDTO.getSearchTerm()))
            {
                //this condition is when user clicks on the search icon without
                //any input at all and dthere was nothing in special filter.
                multiMatchQueryBuilder = QueryBuilders.matchAllQuery();
            }
            else
            {
                BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
                if(multiMatchQueryBuilder != null && attachmentMultiMatchQueryBuilder != null)
                {
                    boolQueryBuilder.minimumShouldMatch(1);
                    boolQueryBuilder.should(QueryBuilders.functionScoreQuery(multiMatchQueryBuilder));
                    boolQueryBuilder.should(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, attachmentMultiMatchQueryBuilder, ScoreMode.None));
                }
                else if(multiMatchQueryBuilder != null)
                {
                    boolQueryBuilder.must(QueryBuilders.functionScoreQuery(multiMatchQueryBuilder));
                }
                else if(attachmentMultiMatchQueryBuilder != null)
                {
                    boolQueryBuilder.must(QueryBuilders.hasChildQuery(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, attachmentMultiMatchQueryBuilder, ScoreMode.None));
                }
                queryBuilder = boolQueryBuilder;
            }

            result = searchAPI.searchByTerm(SearchConstants.USER_QUERY_TYPE_WIKI, queryDTO, queryBuilder, new QueryBuilder[] { rolesFilter, namespaceFilter, tagFilter, typeFilter, sysUpdatedByFilter, sysUpdatedOnFilter, sysCreatedByFilter, sysCreatedOnFilter, hiddenDocumentFilter, undeletedFilter, tmpExcludeNamespaceFilter }, wikiHighlightedFields, userInfo);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage(e.getMessage());
        }

        return result;
    }

    private static QueryBuilder createQueryBuilder(QueryFilter queryFilter, String[] searchFields)
    {
        QueryBuilder result = null;
        String searchTerm = queryFilter.getValue();
        if(SearchUtils.isSimpleQueryString(searchTerm))
        {
            SimpleQueryStringBuilder queryBuilder = QueryBuilders.simpleQueryStringQuery(searchTerm);
            for(String field : searchFields)
            {
                 queryBuilder.field(field);
            }
            result = queryBuilder;
        }
        else
        {
            MultiMatchQueryBuilder queryBuilder = QueryBuilders.multiMatchQuery(searchTerm);
            for(String field : searchFields)
            {
                 queryBuilder.field(field);
            }
            result = queryBuilder;
        }

        return result;
    }

    public static ResponseDTO<WikiAttachment> searchWikiAttachments(QueryDTO queryDTO, UserInfo userInfo)
    {
        ResponseDTO<WikiAttachment> result = new ResponseDTO<WikiAttachment>();

        try
        {
            QueryBuilder queryBuilder = null;
            if(StringUtils.isNotBlank(queryDTO.getSearchTerm()))
            {
                if(SearchUtils.isSimpleQueryString(queryDTO.getSearchTerm()))
                {
                    SimpleQueryStringBuilder simpleQueryStringBuilder = QueryBuilders.simpleQueryStringQuery(queryDTO.getSearchTerm());
                    for(String field : wikiAttachmentSearchFields)
                    {
                        simpleQueryStringBuilder.field(field);
                    }
                    queryBuilder = simpleQueryStringBuilder;
                }
                else
                {
                    queryBuilder = QueryBuilders.multiMatchQuery(queryDTO.getSearchTerm(), wikiAttachmentSearchFields);
                }
            }
            else
            {
                queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
            }

            QueryBuilder filterBuilder1 = QueryBuilders.termsQuery("readRoles", userInfo.getRoles());
            String[] highlightedFields = new String[] {};
            result = attachmentSearchAPI.searchByQuery(queryDTO, queryBuilder, null, new QueryBuilder[] {filterBuilder1}, highlightedFields, userInfo.getUsername());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage(e.getMessage());
        }

        return result;
    }

    /**
     * This method provides number of documents aggregated based on date and interval.
     *  
     * @param queryDTO have selectColumns=sysUpdatedDt, interval="day" and filterItem with sysCreatedBy="johnuser"
     * @param username
     * @return
     */
    public static ResponseDTO<Map<Long, Long>> getDateHistogramData(final QueryDTO queryDTO, final UserInfo userInfo)
    {
        ResponseDTO<Map<Long, Long>> result = new ResponseDTO<Map<Long,Long>>();

        try
        {
            queryDTO.setModelName(SearchConstants.INDEX_NAME_WIKI_DOCUMENT);
            Map<Long, Long> data = searchAPI.getDateHistogramData(queryDTO);
            result.setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage(e.getMessage());
        }

        return result;
    }
}