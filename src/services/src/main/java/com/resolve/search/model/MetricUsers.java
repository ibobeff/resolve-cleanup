package com.resolve.search.model;

public class MetricUsers extends MetricData
{
    private static final long serialVersionUID = -2035491428257744764L;
	private long active;
    public long getActive()
    {
        return active;
    }
    
    public void setActive(long a)
    {
        active = a;
    }
}
