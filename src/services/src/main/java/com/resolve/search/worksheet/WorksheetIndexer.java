/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.worksheet;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchException;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class WorksheetIndexer implements Indexer<IndexData<String>>
{
    private static volatile WorksheetIndexer instance = null;
    private final IndexAPI worksheetIndexAPI;
    private final IndexAPI processRequestIndexAPI;
    private final IndexAPI taskResultIndexAPI;

    private final ConfigSearch config;
    private long indexThreads;

    private long counter = 0;
    
    private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

    public static WorksheetIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("WorksheetIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static WorksheetIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new WorksheetIndexer(config);
        }
        return instance;
    }

    private WorksheetIndexer(ConfigSearch config)
    {
        this.config = config;
        this.indexThreads = this.config.getIndexthreads();
        this.worksheetIndexAPI = APIFactory.getWorksheetIndexAPI();
        this.processRequestIndexAPI = APIFactory.getProcessRequestIndexAPI();
        this.taskResultIndexAPI = APIFactory.getTaskResultIndexAPI();
    }

    public void init()
    {
        Log.log.debug("Initializing WorksheetIndexer.");
        QueueListener<IndexData<String>> wikiIndexListener = new IndexListener<IndexData<String>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(wikiIndexListener);
        Log.log.debug("WorksheetIndexer initialized.");
    }

    public boolean enqueue(IndexData<String> indexData)
    {
        boolean result = false;

        if (indexData != null && indexData.getObjects().size() > 0)
        {
            result = dataQueue.offer(indexData);
        }
        return result;
    }

    @Override
    public boolean index(IndexData<String> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> sysIds = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexWorksheets(indexData, indexData.getUsername());
                    break;
                case DELETE:
                    deleteWorksheetByIds(sysIds, indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    private void indexWorksheets(final IndexData<String> indexData, final String username)
    {
/*        try
        {
            Collection<String> worksheetIds = indexData.getObjects();
            if (worksheetIds != null && !worksheetIds.isEmpty())
            {
                List<String> sysIds = (List<String>) worksheetIds;

                long startTime = 0L;
                long endTime = 0L;

                // got to throttle this or the indexer will be bombarded
                // let's try 500 at a time
                int count = sysIds.size();
                int limit = 1000;
                int fromIndex = 0;
                int toIndex = limit;
                while (fromIndex < count)
                {
                    if (toIndex > count)
                    {
                        toIndex = count;
                    }
                    List<String> subList = sysIds.subList(fromIndex, toIndex);
                    if (subList != null && subList.size() > 0)
                    {
                        startTime = System.currentTimeMillis();
                        Set<WorksheetCAS> worksheets = WorksheetDAO.getInstance().findByKeys(subList);
                        endTime = System.currentTimeMillis();
                        Log.log.debug("Time (millis) taken to get " + subList.size() + " worksheets from Cassandra : " + (endTime - startTime));
                        if (worksheets != null && worksheets.size() > 0)
                        {
                            startTime = System.currentTimeMillis();
                            worksheetIndexAPI.indexDocuments(worksheets, username);
                            endTime = System.currentTimeMillis();
                            Log.log.debug("Time (millis) taken to index " + subList.size() + " worksheets in ES : " + (endTime - startTime));
                        }
                    }
                    // sleep for 5 seconds if we have a lot of data
                    // will improve this later.
                    if (count > limit)
                    {
                        // Thread.sleep() is not a good idea. Refer to a concept
                        // like Thread.sleep() with a lock held
                        Thread.sleep(5000L);
                    }
                    fromIndex = toIndex;
                    toIndex += limit;
                }
                
                counter += worksheetIds.size();
                Log.log.debug("Total worksheets indexed since last start: " + counter);
            }
        }
        catch (Exception exp)
        {
            Log.log.info("indexWorkSheet caught exception: " + exp, exp);
        }
*/    }

    private void deleteWorksheetByIds(final Collection<String> sysIds, final String username) throws SearchException
    {
        for (String worksheetId : sysIds)
        {
            // delete any action results belongs to this worksheet
            QueryBuilder query = QueryBuilders.matchQuery("problemId", worksheetId);
            taskResultIndexAPI.deleteByQueryBuilder(query, true, username);
            // delete any process requests belongs to this worksheet
            query = QueryBuilders.matchQuery("problem", worksheetId);
            processRequestIndexAPI.deleteByQueryBuilder(query, true, username);
            // finally delete the worksheet
            worksheetIndexAPI.deleteDocumentById(worksheetId, username, true);
        }
    }
}
