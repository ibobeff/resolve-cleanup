/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.metric;

import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.Indexer;
import com.resolve.search.SearchException;
import com.resolve.search.model.MetricTimer;
import com.resolve.util.Log;

public class MetricIndexAPI
{
    private static final Indexer<IndexData<String>> metricTimerIndexer = MetricTimerIndexer.getInstance();

    private static final IndexAPI metricTimerIndexAPI = APIFactory.getMetricTimerIndexAPI();

    public static  void removeAllMetricRelatedIndex(String username) throws SearchException
    {
        try
        {
            //remove all worksheet related data from the indexes
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            metricTimerIndexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
        catch (SearchException exp)
        {
            Log.log.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     * @throws SearchException
     */
    public static  void indexMetricTimer(final MetricTimer metricTimer, final String username)
    {
        try
        {
            metricTimerIndexAPI.indexDocument(metricTimer, true, ActiveShardCount.NONE, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
        
    }
}
