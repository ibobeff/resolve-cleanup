/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.DateUtils;

public class BaseIndexModel implements Comparable<BaseIndexModel>, Serializable
{
    private static final long serialVersionUID = 1954383371389894401L;
	private String sysId;
    private String sysOrg;
    private String sysUpdatedBy;
    private Long sysUpdatedOn; // long as timestamp
    private Date sysUpdatedDt; // due to a legacy issue the name suffix is Dt instead of Date
    private String sysCreatedBy;
    private Long sysCreatedOn; // long as timestamp
    private Date sysCreatedDt; // due to a legacy issue the name suffix is Dt instead of Date
    
    public BaseIndexModel()
    {
        super();
        // blank constructor, required for JSON serialization.
    }

    public BaseIndexModel(String sysId, String sysOrg)
    {
        this.sysId = sysId;
        this.sysOrg = sysOrg;
        long now = DateUtils.GetUTCDateLong();
        this.sysCreatedOn = now;
        this.sysUpdatedOn = now;
        this.sysCreatedDt = DateUtils.getDate(now);
        this.sysUpdatedDt = sysCreatedDt;
    }

    public BaseIndexModel(String sysId, String sysOrg, String createdBy)
    {
        this.sysId = sysId;
        this.sysOrg = sysOrg;
        long now = DateUtils.GetUTCDateLong();
        this.sysCreatedOn = now;
        this.sysCreatedDt = DateUtils.getDate(now);
        this.sysUpdatedOn = now;
        this.sysUpdatedDt = sysCreatedDt;
        this.sysCreatedBy = createdBy;
        this.sysUpdatedBy = createdBy;
    }

    public BaseIndexModel(Map<String, Object> columns)
    {
        if(columns != null)
        {
            for(String key : columns.keySet())
            {
                if(key.equals("sys_id")) sysId = (String) columns.get(key);
                if(key.equals("sys_created_by")) sysCreatedBy = (String) columns.get(key);
                if(key.equals("sys_created_on")) 
                {
                    sysCreatedOn = ((Date) columns.get(key)).getTime();
                    this.sysCreatedDt = DateUtils.getDate(sysCreatedOn);
                }
                if(key.equals("sys_updated_by")) sysUpdatedBy = (String) columns.get(key);
                if(key.equals("sys_updated_on")) 
                {
                    sysUpdatedOn = ((Date) columns.get(key)).getTime();
                    this.sysUpdatedDt = DateUtils.getDate(sysUpdatedOn);
                }
            }
        }
    }

    public String getId()
    {
        return sysId;
    }

    public void setId(String id)
    {
        this.sysId = id;
    }

    public String getSysId()
    {
        return sysId;
    }

    public void setSysId(String sysId)
    {
        this.sysId = sysId;
    }

    public String getSysOrg()
    {
        return sysOrg;
    }

    public void setSysOrg(String sysOrg)
    {
        this.sysOrg = sysOrg;
    }

    public String getSysCreatedBy()
    {
        return sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    public Long getSysCreatedOn()
    {
        return sysCreatedOn;
    }

    public void setSysCreatedOn(Long sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
        this.sysCreatedDt = DateUtils.getDate(sysCreatedOn);
    }

    public Date getSysCreatedDt()
    {
        return sysCreatedDt;
    }

    public void setSysCreatedDt(Date sysCreatedDt)
    {
        this.sysCreatedDt = sysCreatedDt;
    }

    public String getSysUpdatedBy()
    {
        return sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    public Long getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Long sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
        this.sysUpdatedDt = DateUtils.getDate(sysUpdatedOn);
    }

    /**
     * have to make the api NOT START with get/set as it creates an issue with json/obj conversion.
     * 
     * @return
     */
    public String ugetSysUpdatedDate()
    {
        String result = null;
        if(this.sysUpdatedOn != null)
        {
            result = DateUtils.convertTimeInMillisToString(this.sysUpdatedOn, "yyyy-MM-dd HH:mm:ss");
        }
        return result;
    }

    public Date getSysUpdatedDt()
    {
        return sysUpdatedDt;
    }

    public void setSysUpdatedDt(Date sysUpdatedDt)
    {
        this.sysUpdatedDt = sysUpdatedDt;
    }

    public Map<String, Object> asMap()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("sys_id", getSysId());
        result.put("sys_org", getSysOrg());
        result.put("sys_created_by", getSysCreatedBy());
        result.put("sys_created_on", getSysCreatedOn());
        result.put("sys_created_date", DateUtils.convertTimeInMillisToString(getSysCreatedOn(), "MM/dd/yyyy hh:mm:ss"));
        result.put("sys_updated_by", getSysUpdatedBy());
        result.put("sys_updated_on", getSysUpdatedOn());
        result.put("sys_updated_date", DateUtils.convertTimeInMillisToString(getSysUpdatedOn(), "MM/dd/yyyy hh:mm:ss"));
        return result;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sysId == null) ? 0 : sysId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        BaseIndexModel other = (BaseIndexModel) obj;
        if (sysId == null)
        {
            if (other.sysId != null) return false;
        }
        else if (!sysId.equals(other.sysId)) return false;
        return true;
    }


    @Override
    public int compareTo(BaseIndexModel otherModel)
    {
        if (this.equals(otherModel))
        {
            return 0;
        }
        else
        {
            return this.sysId.compareTo(otherModel.sysId);
        }
    }

    @Override
    public String toString()
    {
        return "BaseIndexModel [sysId=" + sysId + ", sysOrg=" + sysOrg + ", sysUpdatedBy=" + sysUpdatedBy + ", sysUpdatedOn=" + sysUpdatedOn + ", sysCreatedBy=" + sysCreatedBy + ", sysCreatedOn=" + sysCreatedOn + "]";
    }
}
