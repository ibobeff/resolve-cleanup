package com.resolve.search.model;

public class MetricTransaction extends MetricData
{
    private static final long serialVersionUID = -4354402289944147053L;
	private long transaction;
    private long latency;
    
    
    public long getTransaction()
    {
        return transaction;
    }
    
    public void setTransaction(long c)
    {
        transaction = c;
    }
    
    public long getLatency()
    {
        return latency;
    }
    
    public void setLatency(long f)
    {
        latency = f;
    }
    

}
