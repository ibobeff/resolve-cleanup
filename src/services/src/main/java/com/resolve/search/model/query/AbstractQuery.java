/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

import java.util.TreeSet;

import com.resolve.util.DateUtils;

abstract public class AbstractQuery implements Query
{
    private static final long serialVersionUID = 1L;

    protected Suggest suggest;
    //default to 1
    private int selectCount = 1;
    //this in day (not in milliseconds)
    private long sysUpdatedOn;
    //sysId of the wiki, post, AT etc.
    private TreeSet<String> sourceIds = new TreeSet<String>(); // TreeSet to keep in order

    public AbstractQuery()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public AbstractQuery(String suggest, int weight)
    {
        this.suggest = new Suggest(suggest, weight);
    }

    public AbstractQuery(String suggest, int weight, Long sysUpdatedOn)
    {
        this.suggest = new Suggest(suggest, weight);
        if(sysUpdatedOn == null)
        {
            sysUpdatedOn = DateUtils.GetUTCDateLong();
        }
        this.sysUpdatedOn = sysUpdatedOn;
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#getSuggest()
     */
    @Override
    abstract public Suggest getSuggest();

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#setSuggest(com.resolve.search.model.query.Suggest)
     */
    @Override
    abstract public void setSuggest(Suggest suggest);

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#addSuggest(java.lang.String, int)
     */
    @Override
    public void addSuggest(String suggest, int weight)
    {
        setSuggest(new Suggest(suggest, weight));
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#getSuggestion()
     */
    @Override
    public String getSuggestion()
    {
        return suggest.getInput()[0];
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#getSelectCount()
     */
    @Override
    public int getSelectCount()
    {
        return selectCount;
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#setSelectCount(int)
     */
    @Override
    public void setSelectCount(int selectCount)
    {
        this.selectCount = selectCount;
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#addSelectCount(int)
     */
    @Override
    public void addSelectCount(int selectCount)
    {
        this.selectCount += selectCount;
    }

    //weight from the suggest
    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#getWeight()
     */
    @Override
    public int getWeight()
    {
        return suggest.getWeight();
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#getSysUpdatedOn()
     */
    @Override
    public long getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#setSysUpdatedOn(long)
     */
    @Override
    public void setSysUpdatedOn(long sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }
    
    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#getSourceIds()
     */
    @Override
    public TreeSet<String> getSourceIds()
    {
        return sourceIds;
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#setSourceIds(java.util.TreeSet)
     */
    @Override
    public void setSourceIds(TreeSet<String> sourceIds)
    {
        this.sourceIds = sourceIds;
    }

    /* (non-Javadoc)
     * @see com.resolve.search.model.query.IQuery#addSourceId(java.lang.String)
     */
    @Override
    public Query addSourceId(String sourceId)
    {
        this.sourceIds.add(sourceId);
        return this;
    }

    @Override
    public String toString()
    {
        return "Suggestion [suggest=" + suggest + ", selectCount=" + selectCount + ", sysUpdatedOn=" + sysUpdatedOn + ", query=" + getSuggestion() + "]";
    }
}
