package com.resolve.search.metric;

import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.MetricActionTaskData;
import com.resolve.search.model.MetricCPU;
import com.resolve.search.model.MetricDatabase;
import com.resolve.search.model.MetricJMSQueue;
import com.resolve.search.model.MetricJVM;
import com.resolve.search.model.MetricLatency;
import com.resolve.search.model.MetricRunbook;
import com.resolve.search.model.MetricTransaction;
import com.resolve.search.model.MetricUsers;
import com.resolve.services.hibernate.util.ResolveEventUtil;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.util.Log;

public class MetricDataIndexAPI
{
    private static final IndexAPI metricJVMIndexAPI = APIFactory.getMetricJVMIndexAPI();
    private static final IndexAPI metricRunbookIndexAPI = APIFactory.getMetricRunbookIndexAPI();
    private static final IndexAPI metricTransactionIndexAPI = APIFactory.getMetricTransactionIndexAPI();
    private static final IndexAPI metricCPUIndexAPI = APIFactory.getMetricCPUIndexAPI();
    private static final IndexAPI metricJMSQueueIndexAPI = APIFactory.getMetricJMSQueueIndexAPI();
    private static final IndexAPI metricActionTaskIndexAPI = APIFactory.getMetricActionTaskIndexAPI();
    private static final IndexAPI metricUsersIndexAPI = APIFactory.getMetricUsersIndexAPI();
    private static final IndexAPI metricLatencyIndexAPI = APIFactory.getMetricLatencyIndexAPI();
    private static final IndexAPI metricDBIndexAPI = APIFactory.getMetricDBIndexAPI();

    
    public static  void removeAllJvmMetricsRelatedIndex(String username) throws SearchException
    {
        try
        {
            //remove all JvmMetric related data from the indexes
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            metricJVMIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            metricRunbookIndexAPI.deleteByQueryBuilder(queryBuilder, username);
            metricTransactionIndexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
        catch (SearchException exp)
        {
            Log.log.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    
    
    
    public static  void indexMetricActionTask(final MetricActionTaskData jvm)
    {
        indexMetricActionTask(jvm, "system");
    }
    
    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static void indexMetricActionTask(final MetricActionTaskData matd, final String username)
    {
        try
        {
            metricActionTaskIndexAPI.indexDocument(matd, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static  void indexMetricJMSQueue(final MetricJMSQueue jvm)
    {
        indexMetricJMSQueue(jvm, "system");
    }
    
    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static  void indexMetricJMSQueue(final MetricJMSQueue cpu, final String username)
    {
        try
        {
            metricJMSQueueIndexAPI.indexDocument(cpu, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }

    public static  void indexMetricUsers(final MetricUsers jvm)
    {
        indexMetricUsers(jvm, "system");
    }
    
    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static  void indexMetricUsers(final MetricUsers users, final String username)
    {
        try
        {
            metricUsersIndexAPI.indexDocument(users, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }

    public static  void indexMetricLatency(final MetricLatency jvm)
    {
        indexMetricLatency(jvm, "system");
    }

    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static  void indexMetricLatency(final MetricLatency users, final String username)
    {
        try
        {
            metricLatencyIndexAPI.indexDocument(users, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static  void indexMetricCPU(final MetricCPU jvm)
    {
        indexMetricCPU(jvm, "system");
    }
    
    /**
     * API to index database metric.
     * @param jvm : MetricDatabase containing database metric snapshot.
     */
    public static  void indexMetricDB(final MetricDatabase jvm)
    {
        indexMetricDB(jvm, "system");
    }

    public static  void indexMetricDB(final MetricDatabase users, final String username)
    {
        try
        {
            metricDBIndexAPI.indexDocument(users, username);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static  void indexMetricCPU(final MetricCPU cpu, final String username)
    {
        try
        {
            metricCPUIndexAPI.indexDocument(cpu, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static  void indexMetricJVM(final MetricJVM jvm)
    {
        indexMetricJVM(jvm, "system");
    }
    
    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static  void indexMetricJVM(final MetricJVM jvm, final String username)
    {
        try
        {
            metricJVMIndexAPI.indexDocument(jvm, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }
    
    
    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static void indexMetricRunbook(final MetricRunbook runbook, final String username)
    {
        try
        {
            metricRunbookIndexAPI.indexDocument(runbook, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static  void indexMetricRunbook(final MetricRunbook rb)
    {
        indexMetricRunbook(rb, "system");
    }
    
    /**
     * This is a synchronous operation as it's for single document.
     * 
     * @param sysId
     * @param username
     */
    public static  void indexMetricTransaction(final MetricTransaction runbook, final String username)
    {
        try
        {
            metricTransactionIndexAPI.indexDocument(runbook, username);
        }
        catch (SearchException e)
        {
            //TODO eventually we'll have to throw this outside.
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static  void indexMetricTransaction(final MetricTransaction rb)
    {
        indexMetricTransaction(rb, "system");
    }

    public static void updateMetricJVMIndex(String indexName, int batchSize, boolean force, String username) throws Exception
    {
        String commonSettingsJson = SearchUtils.readJson("common_settings.json");
        Map<String, String> mappings = new HashMap<String, String>();
        String documentType = null;
        IndexAPI indexAPI = null;
        
        String mappingVersion = null;
        if(SearchConstants.INDEX_NAME_METRIC_JVM.equals(indexName))
        {
            String mappingJson = SearchUtils.readJson("metric_jvm_mappings.json");
            mappings.put(SearchConstants.DOCUMENT_TYPE_METRIC_JVM, mappingJson);
            documentType = SearchConstants.DOCUMENT_TYPE_METRIC_JVM;
            indexAPI = metricJVMIndexAPI;
            mappingVersion = SearchUtils.readMappingVersion(mappingJson, documentType);
        }
        else
        {
            Log.log.warn("Invalid index name: " + indexName + " provided");
            throw new SearchException("Invalid index name: " + indexName + " provided");
        }
        
        try
        {
            String dynamicIndexName = indexName;

            //check if the mapping version is same. if yes, no need to update
            String existingMappingVersion = SearchAdminAPI.getExistingMappingVersion(dynamicIndexName, documentType);
            if(!mappingVersion.equalsIgnoreCase(existingMappingVersion) || force)
            {
                //current month start
                String tmpIndex = moveIndexToTemp(indexName, batchSize, username, commonSettingsJson, mappings, documentType, indexAPI, mappingVersion, dynamicIndexName);
                
                ResolveEventVO eventVO = new ResolveEventVO();
                //this value is very very important, before changing it talk to Justin
                //because update/upgrade script relies on this
                Log.log.info(dynamicIndexName + " index is ready to accept data.");
                eventVO.setUValue("ES Migration v2 Ready");
                ResolveEventUtil.insertResolveEvent(eventVO);
                
                moveIndexFromTemp(indexName, tmpIndex, batchSize, username, commonSettingsJson, mappings, documentType, indexAPI, mappingVersion, dynamicIndexName);
            }
            
            //this value is very very important, before changing it talk to Justin
            //because update/upgrade script relies on this
            ResolveEventVO eventVO1 = new ResolveEventVO();
            Log.log.info(dynamicIndexName + " index: ES Migration v2 Ready");
            eventVO1.setUValue("ES Migration v2 Ready");
            ResolveEventUtil.insertResolveEvent(eventVO1);
            
            ResolveEventVO eventVO2 = new ResolveEventVO();
            Log.log.info("All indices updated and ES Migration v2 Ready");
            eventVO2.setUValue("ES Migration v2 Ready");
            ResolveEventUtil.insertResolveEvent(eventVO2);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    
    private static String moveIndexToTemp(String indexName, int batchSize, String username, String commonSettingsJson, Map<String, String> mappings, String documentType, IndexAPI indexAPI, String mappingVersion, String dynamicIndexName) throws SearchException
    {
        String tmpIndex = dynamicIndexName+ "_copy";
        Log.log.info("Updating " + dynamicIndexName + " started");
        SearchAdminAPI.createIndex(tmpIndex, commonSettingsJson, mappings, 1, 0);
            
        indexAPI.moveData(dynamicIndexName, tmpIndex, documentType, batchSize, username);
   
        //remove existing index
        Log.log.info("Deleting index " + dynamicIndexName);
        SearchAdminAPI.removeIndex(dynamicIndexName);
   
            //recreate with latest mappings
        Log.log.info("Recreating index " + dynamicIndexName + " with latest mappings");
        SearchAdminAPI.createIndex(dynamicIndexName, commonSettingsJson, mappings);
        
        return tmpIndex;
    }
    
    private static void moveIndexFromTemp(String indexName, String tmpIndex, int batchSize, String username, String commonSettingsJson, Map<String, String> mappings, String documentType, IndexAPI indexAPI, String mappingVersion, String dynamicIndexName) throws SearchException
    {
        //move the data back
        Log.log.info("Moving back data for " + dynamicIndexName);
        indexAPI.moveData(tmpIndex, dynamicIndexName, documentType, batchSize, username);
        
        //remove temporary indexes
        SearchAdminAPI.removeIndex(tmpIndex);
        Log.log.info("Finished updating " + indexName);
    }
    
}
