/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.property;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.Property;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class PropertyIndexAPI
{
    private static IndexAPI indexAPI = APIFactory.getPropertyIndexAPI();
    
    private static final Indexer<IndexData<String>> indexer = PropertyIndexer.getInstance();
    
    public static long getLastIndexTime() throws SearchException
    {
        return indexAPI.getLastIndexTime(SearchConstants.SYS_UPDATED_ON);
    }

    public static void indexProperty(String sysId, String username)
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("PropertyIndexAPI.indexProperties() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            if (StringUtils.isBlank(sysId))
            {
                Log.log.debug("Blank sysId provided, check the calling code");
            }
            else
            {
                try
                {
                    ResolvePropertiesVO propertiesVO = ServiceHibernate.getPropertyFromIdWithReferences(sysId, null, username);
                    Property property = new Property(propertiesVO);
                    indexAPI.indexDocument(property, username);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }
    
	@SuppressWarnings("unchecked")
	public static void updateIndexMapping(String username) {
		IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.MIGRATE, username);
		indexer.enqueue(indexData);
	}
    
    public static void indexProperties(Collection<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("PropertyIndexAPI.indexProperties() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.INDEX, username);
            indexer.enqueue(indexData);
        }
    }

    public static void purgeProperties(Set<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("PropertyIndexAPI.purgeActionTasks() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.PURGE, username);
            indexer.enqueue(indexData);
        }
    }

    public static void purgeAllProperties(String username)
    {
        IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.DELETE, username);
        indexer.enqueue(indexData);
    }
    
    public static void deleteProperties(Collection<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("PropertiesIndexAPI.deleteProperties() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
            indexer.enqueue(indexData);
        }
    }
    
}
