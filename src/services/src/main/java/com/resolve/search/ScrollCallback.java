/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/
package com.resolve.search;

import java.util.Collection;

public interface ScrollCallback
{
    <T> void process(T data);

    <T> void process(Collection<T> records);
}
