package com.resolve.search.model;

public class MetricLatency extends MetricData
{
    private static final long serialVersionUID = -8137445467766330108L;
	private long wiki;
    private long wikiResponseTime;
    private long social;
    private long socialResponseTime;
    
    public long getWiki()
    {
        return wiki;
    }
    
    public void setWiki(long w)
    {
        wiki = w;
    }

    public long getWikiResponseTime()
    {
        return wikiResponseTime;
    }
    
    public void setWikiResponseTime(long w)
    {
        wikiResponseTime = w;
    }

    public long getSocial()
    {
        return social;
    }
    
    public void setSocial(long w)
    {
        social = w;
    }

    public long getSocialResponseTime()
    {
        return socialResponseTime;
    }
    
    public void setSocialResponseTime(long w)
    {
        socialResponseTime = w;
    }
    
}
