/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.property;

import java.util.ArrayList;
import java.util.Collection;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.Server;
import com.resolve.search.model.Property;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class PropertyIndexer implements Indexer<IndexData<String>>
{
    private static volatile PropertyIndexer instance = null;
    
    private final IndexAPI indexAPI;
    
    private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

    public static PropertyIndexer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("PropertyIndexer is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static PropertyIndexer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new PropertyIndexer(config);
        }
        return instance;
    }

    private PropertyIndexer(ConfigSearch config)
    {
        this.indexAPI = APIFactory.getPropertyIndexAPI();
    }
    
    public void init()
    {
        Log.log.debug("Initializing PropertyIndexer.");
        QueueListener<IndexData<String>> propertyIndexListener = new IndexListener<IndexData<String>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(propertyIndexListener);
        Log.log.debug("PropertyIndexer initialized.");
    }
    
    public boolean enqueue(IndexData<String> indexData)
    {
        return dataQueue.offer(indexData);
    }

    @Override
    public boolean index(IndexData<String> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> sysIds = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexProperties(sysIds, indexData.getUsername());
                    break;
                case DELETE:
                    deleteProperties(sysIds, indexData.getUsername());
                    break;
                case MIGRATE:
                	updateIndexMapping();
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    private void updateIndexMapping() {
    	SearchAdminAPI.updateIndexMapping(SearchConstants.INDEX_NAME_PROPERTY, SearchConstants.DOCUMENT_TYPE_PROPERTY, Server.ES_PROPERTIES_MAPPING_FILE);
	}
    
    private void indexProperties(Collection<String> sysIds, String username) throws Exception
    {
        int start = 0;
        int limit = 100; //100 a batch
        
        Collection<Property> properties = new ArrayList<Property>();
        
        for (String sysId : sysIds)
        {
			if (start >= limit) {
				indexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, properties, false,
						username);
				properties = new ArrayList<Property>();
				start = 0;
				Thread.sleep(500L);
			}
            ResolvePropertiesVO propertiesVO = ServiceHibernate.getPropertyFromIdWithReferences(sysId, null, username);
            Property property = new Property(propertiesVO);
            properties.add(property);
            start++;
        }
        
        if(properties.size() > 0) {
        	indexAPI.indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, properties, false, username);
        }
    }
    
    private void deleteProperties(final Collection<String> sysIds, final String username) throws SearchException
    {
        if(sysIds == null)
        {
            //purging all action tasks
            Log.log.info("Deleting all properties requested by \"" + username + "\"");
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            indexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
        else
        {
            indexAPI.deleteDocumentByIds(sysIds, username);
        }
    }
}
