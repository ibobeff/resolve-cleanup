/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.admin.cluster.repositories.put.PutRepositoryResponse;
import org.elasticsearch.action.admin.cluster.snapshots.create.CreateSnapshotResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.metadata.RepositoryMetaData;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.snapshots.SnapshotInfo;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;


/**
 * This class is for various administrative operations.
 *
 */
public class SearchAdminAPI
{
    private static Server instance = null;
    private static SearchService searchService = null;
    
    public static Client getClient()
    {
        return instance.getClient();
    }

    /**
     * Initialize the indexing/searching service.
     *
     * @param configSearch
     * @throws SearchException 
     */
	public static void init(ConfigSearch configSearch) {
		init(configSearch, false);
    }
    
    /**
     * @param configSearch
     * @param esCacheEnabled - enables in-memory cache for ER calls
     */
    public static void init(ConfigSearch configSearch, boolean esCacheEnabled) {
        try
        {
            instance = Server.getInstance(configSearch);
            instance.init();
            searchService = new SearchService();
            searchService.setEsCacheEnabled(esCacheEnabled);
        }
        catch (SearchException e)
        {
            Log.log.fatal("Something terribly went wrong during RSSearch initialization: \n" + e.getMessage(), e);
            Log.log.error(e.getMessage(), e);
        }
    }

    public static SearchService getSearchService()
    {
        return searchService;
    }

    public static void shutdown()
    {
        instance.shutdown();
    }

    /**
     * Consumer may like to know if the service is intialized and/or running so it could do some work
     * with the search service.
     *
     * @return
     */
    public static boolean isServiceRunning()
    {
        return instance.isRunning();
    }

    public static void createIndex(String indexName, String settingsJson, Map<String, String> mappingsJson) throws SearchException
    {
        instance.createIndex(indexName, settingsJson, mappingsJson);
    }
    
    public static void createIndex(String indexName, String settingsJson, Map<String, String> mappingsJson, int numberOfShards, int numberOfReplicas) throws SearchException
    {
        instance.createIndex(indexName, settingsJson, mappingsJson, numberOfShards, numberOfReplicas);
    }

    public static void removeIndex(String indexName) throws SearchException
    {
        instance.removeIndex(indexName);
    }

    public static ConfigSearch getSearchConfig()
    {
        return instance.getConfig();
    }
    
    public static long getTotalCount(String indexName, String documentType)
    {
        long result = searchService.getTotalCount(indexName, documentType);
        Log.log.trace("Total number of " + documentType + ":" + result);
        return result;
    }

    public static Map<String, Long> getIndexRecordSize()
    {
        Map<String, Long> result = new HashMap<String, Long>();

        long wikidocCount = getTotalCount(SearchConstants.INDEX_NAME_WIKI_DOCUMENT, SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT);
        Log.log.info("index wikidoc have total records: " + wikidocCount);

        long wikiAttachmentCount = getTotalCount(SearchConstants.INDEX_NAME_WIKI_DOCUMENT, SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT);
        Log.log.info("index attach have total records: " + wikiAttachmentCount);

        long actionTaskCount = getTotalCount(SearchConstants.INDEX_NAME_ACTION_TASK, SearchConstants.DOCUMENT_TYPE_ACTION_TASK);
        Log.log.info("index actiontask have total records: " + actionTaskCount);

        long socialPostCount = getTotalCount(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_POST);
        Log.log.info("index posts have total records: " + socialPostCount);

        long socialCommentCount = getTotalCount(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT);
        Log.log.info("index posts have total records: " + socialPostCount);

        long tagCount = getTotalCount(SearchConstants.INDEX_NAME_TAG, SearchConstants.DOCUMENT_TYPE_TAG);
        Log.log.info("index tag have total records: " + tagCount);

        long worksheetCount = getTotalCount(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS, SearchConstants.DOCUMENT_TYPE_WORKSHEET);
        Log.log.info("index worksheet have total records: " + worksheetCount);

        long processRequestCount = getTotalCount(SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS, SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST);
        Log.log.info("index process request have total records: " + processRequestCount);

        long taskResultCount = getTotalCount(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS, SearchConstants.DOCUMENT_TYPE_TASKRESULT);

        long wsdataCount = getTotalCount(SearchConstants.INDEX_NAME_WORKSHEET_DATA, SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA);
        Log.log.info("index worksheetdata have total records: " + wsdataCount);

        long executeStateCount = getTotalCount(SearchConstants.INDEX_NAME_EXECUTE_STATE, SearchConstants.DOCUMENT_TYPE_EXECUTE_STATE);
        Log.log.info("index executestate have total records: " + wsdataCount);

        long metricTimerCount = getTotalCount(SearchConstants.INDEX_NAME_METRIC_TIMER, SearchConstants.DOCUMENT_TYPE_METRIC_TIMER);
        Log.log.info("index metrictimer have total records: " + metricTimerCount);

        Log.log.info("index taskResult have total records: " + taskResultCount);

        long propertiesCount = getTotalCount(SearchConstants.INDEX_NAME_PROPERTY, SearchConstants.DOCUMENT_TYPE_PROPERTY);
        Log.log.info("index properties have total records: " + propertiesCount);
        
        long customFormsCount = getTotalCount(SearchConstants.INDEX_NAME_CUSTOM_FORM, SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM);
        Log.log.info("index customforms have total records: " + customFormsCount);
        
        result.put(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT, wikidocCount);
        result.put(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, wikiAttachmentCount);
        result.put(SearchConstants.DOCUMENT_TYPE_ACTION_TASK, actionTaskCount);

        result.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, socialPostCount);
        result.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, socialCommentCount);
        result.put(SearchConstants.DOCUMENT_TYPE_TAG, tagCount);
        result.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET, worksheetCount);
        result.put(SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST, processRequestCount);
        result.put(SearchConstants.DOCUMENT_TYPE_TASKRESULT, taskResultCount);
        result.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA, wsdataCount);
        result.put(SearchConstants.DOCUMENT_TYPE_EXECUTE_STATE, executeStateCount);

        result.put(SearchConstants.DOCUMENT_TYPE_PROPERTY, propertiesCount);
        result.put(SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM, customFormsCount);
        
        return result;
    }
    
    public static long getWorksheetCount() {
        return getTotalCount(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS, SearchConstants.DOCUMENT_TYPE_WORKSHEET);
    }
    
    public static long getProcessRequestCount() {
        return getTotalCount(SearchConstants.INDEX_NAME_PROCESSREQUEST_ALIAS, SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST);
    }
    
    public static long getTaskResultCount() {
        return getTotalCount(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS, SearchConstants.DOCUMENT_TYPE_TASKRESULT);

    }
    
    public static Map<String, Long> getTaskResultData(){
        return new HashMap<String, Long>();
        //return searchService.getTaskResultData(SearchConstants.INDEX_NAME_TASKRESULT_ALIAS, SearchConstants.DOCUMENT_TYPE_TASKRESULT);
    }
    
    public static boolean configureSnapshot(String repositoryName, String type, String location, boolean compress)
    {
        return searchService.configureSnapshot(repositoryName, type, location, compress);
    }
    
    public static boolean createSnapshot(String repositoryName, String snapshotName, String... indices)
    {
        return searchService.createSnapshot(repositoryName, snapshotName, indices);
    }

    public static boolean restoreSnapshot(String repositoryName, String snapshotName, String... indices)
    {
        return searchService.restoreSnapshot(repositoryName, snapshotName, indices);
    }
    
    public static Map<String, String> getAllSnapshots(String repositoryName)
    {
        return searchService.getAllSnapshots(repositoryName);
    }
    
    public static String getExistingMappingVersion(String indexName, String documentType) throws SearchException
    {
        return instance.getExistingMappingVersion(indexName, documentType);
    }

    public static void configureIndex(String indices)
    {
        try
        {
            instance.configureIndex(indices);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void migrateIndex()
    {
        try
        {
        	searchService.reindex(instance.getConfig().getMigrationHost(), instance.getConfig().getMigrationPort());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
//    public static boolean setIndexPriority()
//    {
//    	return instance.setIndexPriority();
//    }
    /**
     * Truncates the simple indexes and applies latest mappings. This is necessary when the analyzer changes
     * to fix some search terms on some fields in an index. Note that this method MUST NOT be called to
     * truncate the month indexes like worksheet, processrequest, and taskresult. 
     * For that go through WorksheetIndexAPI.truncateRunbookIndex(). 
     * 
     * @param indexName
     * @throws SearchException
     */
    public static void updateIndexMapping(String indexName) throws SearchException
    {
        try
        {
            String commonSettingsJson = SearchUtils.readJson("common_settings.json");
            Map<String, String> mappings = new HashMap<String, String>();
            
            String documentTypes = SearchConstants.INDEX_DOCUMENT_TYPES.get(indexName);
            Log.log.debug("Truncating index : " + indexName);
            if(StringUtils.isNotBlank(documentTypes))
            {
                Log.log.debug("Document type(s) found : " + documentTypes);
                String[] documentTypeList = StringUtils.stringToArray(documentTypes, StringUtils.DELIMITER); 
                for(String documentType : documentTypeList)
                {
                    if(SearchConstants.INDEX_MAPPINGS.containsKey(documentType))
                    {
                        Log.log.debug("Document mapping found : " + documentType);
                        //now create with latest mapping
                        String mappingFile = SearchConstants.INDEX_MAPPINGS.get(documentType);
                        Log.log.debug("Mapping file found : " + mappingFile);
                        String mappingJson = SearchUtils.readJson(mappingFile);
                        mappings.put(documentType, mappingJson);
                    }
                }
                
                try
                {
                    removeIndex(indexName);
                }
                catch (SearchException e)
                {
                    //ignore, might not have existed anyways
                }
                
                //now create with latest mapping
                createIndex(indexName, commonSettingsJson, mappings);
            }
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public static boolean isIndexExists(String indexName)
    {
        return instance.isExists(indexName);
    }
    
    //create  functions snapshots
    
    private static boolean isRepositoryExist( String repositoryName)
    {
        Client client =  getClient(); 
        boolean result = false;

        try
        {

            List<RepositoryMetaData> repositories = client.admin().cluster().prepareGetRepositories().get().repositories();

            if (repositories.size() > 0)
            {
                for (RepositoryMetaData repo : repositories)
                    result = repositoryName.equals(repo.name()) ? true : false;
            }

        }
        catch (Exception ex){
            Log.log.error("Exception in getRepository method: " + ex.toString());
        }

        return result;
    }

    public static PutRepositoryResponse createRepository(String repositoryName, String path, boolean compress)
    {
        Client client =  getClient(); 
        PutRepositoryResponse putRepositoryResponse = null;

        try
        {

            if (!isRepositoryExist(repositoryName))
            {

                Settings settings = Settings.builder().put("location", path + repositoryName).put("compress", compress).build();

                putRepositoryResponse = client.admin().cluster().preparePutRepository(repositoryName).setType("fs").setSettings(settings).get();

                Log.log.info("Repository was created.");

            }
            else
                Log.log.info(repositoryName + " repository already exists");
        }
        catch (Exception ex)
        {
            Log.log.debug("Exception in createRepository method: " + ex.toString());

        }
        return putRepositoryResponse;
    }

    
    public static boolean isSnapshotExist( String repositoryName, String snapshotName)
    {
        Client client =  getClient(); 
        boolean result = false;

        try
        {

            List<SnapshotInfo> snapshotInfo = client.admin().cluster().prepareGetSnapshots(repositoryName).get().getSnapshots();
            if (snapshotInfo.size() > 0)
            {
                for (SnapshotInfo snapshot : snapshotInfo)
                    result = snapshotName.equals(snapshot.snapshotId().getName()) ? true : false;
            }

        }
        catch (Exception ex) {
            Log.log.error("Exception in getSnapshot method: " + ex.toString());
        }

        return result;
    }

    public static CreateSnapshotResponse createSnapshot(String repositoryName, String snapshotName, String indexName)
    {
        Client client =  getClient(); 
        CreateSnapshotResponse createSnapshotResponse = null;
        try
        {

            if (isSnapshotExist(repositoryName, snapshotName))
                Log.log.info(snapshotName + " snapshot already exists");

            else
            {

                if (indexName == null || indexName.isEmpty() || indexName.equalsIgnoreCase("ALL"))
                {
                    createSnapshotResponse = client.admin().cluster().prepareCreateSnapshot(repositoryName, snapshotName).setWaitForCompletion(false).get();
                }
                else
                {
                    createSnapshotResponse = client.admin().cluster().prepareCreateSnapshot(repositoryName, snapshotName).setWaitForCompletion(false).setIndices(indexName).get();
                }

                Log.log.info("Snapshot was created.");
            }

        }
        catch (Exception ex){
            Log.log.error("Exception in createSnapshot method: " + ex.toString());
        }

        return createSnapshotResponse;
    }

    public static boolean createSnapshotAllIndices(String repositoryName, String snapshotName) {
        boolean success = true; 
        Client client =  getClient(); 
        try {

            if (isSnapshotExist(repositoryName, snapshotName)) {
                Log.log.info(snapshotName + " snapshot already exists");
            }
            else {
                    client.admin().cluster().prepareCreateSnapshot(repositoryName, snapshotName).setWaitForCompletion(false).get();
                    Log.log.info("Elastic Search Snapshot was created.");
            }
        }
        catch (Exception ex) {
            success = false;
            Log.log.error("Exception in createSnapshot method: " + ex.toString());
        }
        return success;
    }

	public static void updateIndexMapping(String indexName, String indexType, String indexMappingFile) {
		if (instance == null) {
			throw new IllegalStateException("Server instance has not been initialized.");
		}
		String indexMappingFileJson = instance.readJson(indexMappingFile);
		getClient().admin().indices().preparePutMapping(indexName).setType(indexType).setSource(indexMappingFileJson, XContentType.JSON).get();
	}
}