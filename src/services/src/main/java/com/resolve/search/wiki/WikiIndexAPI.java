/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.wiki;

import static com.resolve.search.SearchConstants.SEARCH_TYPE_AUTOMATION;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_DECISION_TREE;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_DOCUMENT;
import static com.resolve.search.SearchConstants.SEARCH_TYPE_PLAYBOOK;
import static com.resolve.search.SearchConstants.SYS_CREATED_ON;
import static com.resolve.search.SearchConstants.SYS_ID;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.Indexer;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.WikiDocument;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WikiIndexAPI
{
    private static final IndexAPI indexAPI = APIFactory.getWikiIndexAPI();
    
    private static final Indexer indexer = WikiIndexer.getInstance();
    private static final Indexer<IndexData<String>> attachmentIndexer = WikiAttachmentIndexer.getInstance();
    /*
	 * used for indexing separate types of wikidocuments for the purposes of content
	 * browser
	 */
	private static final IndexAPI documentIndexAPI = APIFactory.getDocumentIndexAPI();
	private static final IndexAPI automationIndexAPI = APIFactory.getAutomationIndexAPI();
	private static final IndexAPI decisionTreeIndexAPI = APIFactory.getDecisiontreeIndexAPI();
	private static final IndexAPI playbookIndexAPI = APIFactory.getPlaybookIndexAPI();

    public static long getLastIndexTime() throws SearchException
    {
        return indexAPI.getLastIndexTime(SearchConstants.SYS_UPDATED_ON);
    }
    
    @SuppressWarnings("unchecked")
    public static void indexWikiDocument(String sysId, boolean indexAttachment, String username)
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("WikiIndexUtils.indexWikiDocument() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(Arrays.asList(sysId), OPERATION.INDEX, username);
            indexer.enqueue(indexData);
        }
    }

	@SuppressWarnings("unchecked")
	public static void updateIndexMapping(String username) {
		IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.MIGRATE, username);
		indexer.enqueue(indexData);
	}
	
    public static void indexWikiDocuments(Collection<String> sysIds, boolean indexAttachment, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("WikiIndexUtils.indexWikiDocuments() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.INDEX, username);
            indexer.enqueue(indexData);
        }
    }

    public static void purgeWikiDocuments(Set<String> sysIds, boolean purgeAttachment, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("WikiIndexUtils.purgeWikiDocuments() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.PURGE, username);
            indexer.enqueue(indexData);
        }
    }

    public static void purgeAllWikiDocuments(String username)
    {
        IndexData<String> indexData = new IndexData<String>(Collections.EMPTY_LIST, OPERATION.PURGE, username);
        indexer.enqueue(indexData);
    }

    public static void deleteWikiDocument(String sysId, String username) throws SearchException
    {
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("WikiIndexUtils.deleteWikiDocument() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            indexAPI.deleteDocumentById(sysId, username);
        }
    }

    public static void deleteWikiDocuments(Collection<String> sysIds, String username)
    {
        if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.warn("WikiIndexUtils.deleteWikiDocuments() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(sysIds, OPERATION.DELETE, username);
            indexer.enqueue(indexData);
        }
    }

    //attachments
    public static void indexWikiAttachments(String docSysId, Set<String> attachSysIds, String username)
    {
        if (StringUtils.isBlank(docSysId) || attachSysIds == null || attachSysIds.size() == 0)
        {
            Log.log.warn("WikiIndexUtils.indexWikiAttachments() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(attachSysIds, OPERATION.INDEX, username);
            indexData.setParentId(docSysId);
            attachmentIndexer.enqueue(indexData);
        }
    }

    public static void purgeWikiAttachments(Set<String> attachSysIds, String docId, String username) throws SearchException
    {
        if (attachSysIds == null || attachSysIds.size() == 0)
        {
            Log.log.warn("WikiIndexUtils.purgeWikiAttachments() : Avoid calling API without valid parameter, it's taxing the system. Check your code!");
        }
        else
        {
            IndexData<String> indexData = new IndexData<String>(attachSysIds, OPERATION.PURGE, username);
            indexData.setSourceId(docId);
            attachmentIndexer.enqueue(indexData);
        }
    }
    
    private static void indexWikiDocument(WikiDocument wikiDocument, String username) throws SearchException, CloneNotSupportedException
    {
        indexAPI.indexDocument(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, wikiDocument, false, username);
        indexWikiDocumentByType(wikiDocument, username);
    }

    public static void setActive(final String sysId, final String username, final boolean isActive) throws SearchException, CloneNotSupportedException
    {
        WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
        if (wikiDocument == null)
        {
            Log.log.warn("setActive failed, invalid document sys id: " + sysId);
        }
        else
        {
            wikiDocument.setActive(isActive);
            indexWikiDocument(wikiDocument, username);
        }
    }

    public static void setLocked(final String sysId, final String username, final boolean isLocked) throws SearchException, CloneNotSupportedException
    {
        WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
        if (wikiDocument == null)
        {
            Log.log.warn("setLocked failed, invalid document sys id: " + sysId);
        }
        else
        {
            wikiDocument.setLocked(isLocked);
            indexWikiDocument(wikiDocument, username);
        }
    }

    public static void setDeleted(final String sysId, final String username, final boolean isDeleted) throws SearchException, CloneNotSupportedException
    {
        WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
        if (wikiDocument == null)
        {
            Log.log.warn("setDeleted failed, invalid document sys id: " + sysId);
        }
        else
        {
            wikiDocument.setDeleted(isDeleted);
            indexWikiDocument(wikiDocument, username);
        }
    }

    public static void setHidden(final String sysId, final String username, final boolean isHidden) throws SearchException, CloneNotSupportedException
    {
        WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
        if (wikiDocument == null)
        {
            Log.log.warn("setHidden failed, invalid document sys id: " + sysId);
        }
        else
        {
            wikiDocument.setHidden(isHidden);
            indexWikiDocument(wikiDocument, username);
        }
    }

    public static boolean setRating(final String sysId, final String username, final double rating) throws SearchException, CloneNotSupportedException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("setRating failed, invalid document sys id: " + sysId);
        }
        else
        {
            WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
            if(wikiDocument != null)
            {
                wikiDocument.setRating(rating);
                indexWikiDocument(wikiDocument, username);
                result = true;
            }
        }
        return result;
    }

    public static boolean resetRating(final String sysId, final String username) throws SearchException, CloneNotSupportedException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("resetRating failed, invalid document sys id: " + sysId);
        }
        else
        {
            WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
            if(wikiDocument != null)
            {
                wikiDocument.setRating(null);
                indexWikiDocument(wikiDocument, username);
                result = true;
            }
        }
        return result;
    }

    public static boolean setClickCount(final String sysId, final Long clickCount, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("setClickCount failed, invalid document sys id: " + sysId);
        }
        else
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("OPERATION", "SET_CLICK_COUNT");
            params.put("WIKI_ID", sysId);
            params.put("COUNT", clickCount);
            params.put("USERNAME", username);

            IndexData<Map<String, Object>> indexData = new IndexData<Map<String, Object>>(Arrays.asList(params), OPERATION.CUSTOM, false, username);
            indexer.enqueue(indexData);
            result = true;
        }
        return result;
    }

    public static boolean setEditCount(final String sysId, final Long editCount, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("setEditCount failed, invalid document sys id: " + sysId);
        }
        else
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("OPERATION", "SET_EDIT_COUNT");
            params.put("WIKI_ID", sysId);
            params.put("COUNT", editCount);
            params.put("USERNAME", username);

            IndexData<Map<String, Object>> indexData = new IndexData<Map<String, Object>>(Arrays.asList(params), OPERATION.CUSTOM, false, username);
            indexer.enqueue(indexData);
            result = true;
        }
        return result;
    }

    public static boolean setExecuteCount(final String sysId, final Long executeCount, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("setExecuteCount failed, invalid document sys id: " + sysId);
        }
        else
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("OPERATION", "SET_EXECUTE_COUNT");
            params.put("WIKI_ID", sysId);
            params.put("COUNT", executeCount);
            params.put("USERNAME", username);

            IndexData<Map<String, Object>> indexData = new IndexData<Map<String, Object>>(Arrays.asList(params), OPERATION.CUSTOM, false, username);
            indexer.enqueue(indexData);
            result = true;
        }
        return result;
    }

    public static boolean resetClickCount(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("resetClickCount failed, invalid document sys id: " + sysId);
        }
        else
        {
            WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
            if(wikiDocument != null)
            {
                wikiDocument.setClickCount(null);
                indexAPI.indexDocument(wikiDocument, username);
                result = true;
            }
        }
        return result;
    }
    
    public static boolean increaseNumberOfReviews(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("increaseNumberOfReviews failed, invalid document sys id: " + sysId);
        }
        else
        {
            WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
            if(wikiDocument != null)
            {
                wikiDocument.setNumberOfReviews(wikiDocument.getNumberOfReviews() == null ? 1 : wikiDocument.getNumberOfReviews() + 1);
                indexAPI.indexDocument(wikiDocument, username);
                result = true;
            }
        }
        return result;
    }

    public static boolean resetNumberOfReviews(final String sysId, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("resetNumberOfReviews failed, invalid document sys id: " + sysId);
        }
        else
        {
            WikiDocument wikiDocument = WikiSearchAPI.findWikiDocumentById(sysId, username);
            if(wikiDocument != null)
            {
                wikiDocument.setNumberOfReviews(null);
                indexAPI.indexDocument(wikiDocument, username);
                result = true;
            }
        }
        return result;
    }
    
	public static void indexWikiDocumentByType(WikiDocument wikiDocument, String username)
			throws SearchException, CloneNotSupportedException {
		IndexAPI indexAPI = null;
		Collection<WikiDocument> wikiDocuments = new LinkedList<WikiDocument>();
		
		// remove document form wiki indices to ensure correctness of the type field
		removeDocumentFromWikiIndices(wikiDocument.getSysId(), username);
		
		if (wikiDocument.getType().contains(",")) {
			String[] types = wikiDocument.getType().split(", ");
			
			// Trim found types first
			if (types != null && types.length > 0) {
				for (int i = 0; i < types.length; i++) {
					types[i] = types[i].trim();
				}
			}
			
			for (int i = 0; i < types.length; i++) {
				WikiDocument changedWikiDocument = wikiDocument.cloneForType(types[i]);
				wikiDocuments.clear();
				wikiDocuments.add(changedWikiDocument);

				indexAPI = getIndexAPIByType(types[i]);
				indexAPI.indexDocuments(SYS_ID, SYS_CREATED_ON, wikiDocuments, false, username);
			}

		} else {
			wikiDocuments.add(wikiDocument);
			indexAPI = getIndexAPIByType(wikiDocument.getType());
			indexAPI.indexDocuments(SYS_ID, SYS_CREATED_ON, wikiDocuments, false, username);
		}

	}

	public static IndexAPI getIndexAPIByType(String wikiDocumentType) {
		IndexAPI indexAPI = null;
		switch (wikiDocumentType) {
		case SEARCH_TYPE_DOCUMENT:
			indexAPI = documentIndexAPI;
			break;
		case SEARCH_TYPE_DECISION_TREE:
			indexAPI = decisionTreeIndexAPI;
			break;
		case SEARCH_TYPE_AUTOMATION:
			indexAPI = automationIndexAPI;
			break;
		case SEARCH_TYPE_PLAYBOOK:
			indexAPI = playbookIndexAPI;
			break;
		}

		return indexAPI;
	}

	/*
	 * Composite types are pairs automation - decisiontree
	 * 
	 * Removes document form automation, decisiontree, document and playbook indices
	 */
	public static void removeDocumentFromWikiIndices(String sysId, String username) throws SearchException {
		automationIndexAPI.deleteDocumentById(sysId, username);
		decisionTreeIndexAPI.deleteDocumentById(sysId, username);
		documentIndexAPI.deleteDocumentById(sysId, username);
		playbookIndexAPI.deleteDocumentById(sysId, username);

	}

    /**
     * Updates count for a particular field.
     * 
     * @param sysId
     * @param field clickCount, editCount, executeCount, reviewCount, usefulNoCount, usefulYesCount etc.
     * @param count
     * @param username
     * @return
     * @throws SearchException
     */
/*    public static boolean setCountByField(final String sysId, final String field, final Long count, final String username) throws SearchException
    {
        boolean result = false;
        if (StringUtils.isBlank(sysId))
        {
            Log.log.warn("increase count for " + field + " failed, invalid document sys id: " + sysId);
        }
        else
        {
            String script = "ctx._source."+field+" = count";
            try
            {
                Map<String, Object> scriptParams = new HashMap<String, Object>();
                scriptParams.put("count", count);
                result = indexAPI.updateDocument(sysId, script, scriptParams, username);
            }
            catch (SearchException e)
            {
                if(SearchConstants.ERROR_DOC_NOT_FOUND.equals(e.getErrorNumber()))
                {
                    indexWikiDocument(sysId, true, username);
                }
                else
                {
                    throw e;
                }
            }
        }
        return result;
    }
*/
}