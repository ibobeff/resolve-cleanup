/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;

public class PBAuditLog extends BaseIndexModel implements Serializable
{
	private static final long serialVersionUID = -2708997183133497917L;

	public PBAuditLog()
	{
		super();
	}
	
	public PBAuditLog(String sysId, String sysOrg)
	{
		super(sysId, sysOrg);
	}
	
	public PBAuditLog(String sysId, String sysOrg, String createdBy)
	{
		super(sysId, sysOrg, createdBy);
	}
	
	public PBAuditLog(PBAuditLog auditLog)
	{
		super(auditLog.getSysId(), auditLog.getSysOrg(), auditLog.getSysCreatedBy());
		this.setIncidentId(auditLog.getIncidentId());
		this.setDescription(auditLog.getDescription());
		this.setIpAddress(auditLog.getIpAddress());
		this.setUserName(auditLog.getUserName());
		this.setModified(auditLog.isModified());
	}
	
	private String incidentId;
	private String description;
	private String ipAddress;
	private String userName;
	private Boolean modified;
	
	public String getIncidentId()
	{
		return incidentId;
	}
	public void setIncidentId(String incidentId)
	{
		this.incidentId = incidentId;
	}

	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public String getIpAddress()
	{
		return ipAddress;
	}
	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}
	
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

    public Boolean isModified()
    {
        return modified;
    }
    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }
}
