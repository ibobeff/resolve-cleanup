/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.search.model;

import com.resolve.services.hibernate.vo.MetaAccessRightsVO;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.util.DateUtils;

@StripHTML
public class CustomForm extends BaseIndexModel {

	private static final long serialVersionUID = 2344863994538404115L;

	private String name;

	private String viewName;

	private String displayName;

	private String wikiName;

	private String tableName;

	private String tableDisplayName;

	private Boolean headerVisible;

	private Boolean viewCollapsible;

	private Boolean wizard;

	private String windowTitle;

	private String type = "customform";

	private AccessRights accessRights;

	private String namespace = "";

	private boolean deleted;

	public CustomForm() {
	}

	public CustomForm(MetaFormViewVO customFormVO) {

		setSysId(customFormVO.getSys_id());
		setSysOrg(customFormVO.getSysOrg());
		setSysCreatedBy(customFormVO.getSysCreatedBy());
		if (customFormVO.getSysCreatedOn() == null) {
			// to bad using current UTC time
			setSysCreatedOn(DateUtils.GetUTCDateLong());
		} else {
			setSysCreatedOn(customFormVO.getSysCreatedOn().getTime());
		}
		setSysUpdatedBy(customFormVO.getSysUpdatedBy());
		if (customFormVO.getSysUpdatedOn() == null) {
			// to bad using current UTC time
			setSysUpdatedOn(getSysCreatedOn());
		} else {
			setSysUpdatedOn(customFormVO.getSysUpdatedOn().getTime());
		}

		this.name = customFormVO.getUFormName();
		this.viewName = customFormVO.getUViewName();
		this.displayName = customFormVO.getUDisplayName();
		this.wikiName = customFormVO.getUWikiName();
		this.tableName = customFormVO.getUTableName();
		this.tableDisplayName = customFormVO.getUTableDisplayName();
		this.headerVisible = customFormVO.getUIsHeaderVisible();
		this.viewCollapsible = customFormVO.getUIsViewCollapsible();
		this.wizard = customFormVO.getUIsWizard();
		this.windowTitle = customFormVO.getUWindowTitle();
		this.deleted = customFormVO.getSysIsDeleted();

		MetaAccessRightsVO accessRightsVO = customFormVO.getMetaAccessRights();
		if (accessRightsVO != null) {
			AccessRights accessRights = new AccessRights();
			accessRights.setReadAccess(accessRightsVO.getUReadAccess());
			accessRights.setWriteAccess(accessRightsVO.getUWriteAccess());
			accessRights.setAdminAccess(accessRightsVO.getUAdminAccess());
			accessRights.setExecuteAccess(accessRightsVO.getUExecuteAccess());
			setAccessRights(accessRights);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String formName) {
		this.name = formName;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getWikiName() {
		return wikiName;
	}

	public void setWikiName(String wikiName) {
		this.wikiName = wikiName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableDisplayName() {
		return tableDisplayName;
	}

	public void setTableDisplayName(String tableDisplayName) {
		this.tableDisplayName = tableDisplayName;
	}

	public Boolean getHeaderVisible() {
		return headerVisible;
	}

	public void setHeaderVisible(Boolean headerVisible) {
		this.headerVisible = headerVisible;
	}

	public Boolean getViewCollapsible() {
		return viewCollapsible;
	}

	public void setViewCollapsible(Boolean viewCollapsible) {
		this.viewCollapsible = viewCollapsible;
	}

	public Boolean getWizard() {
		return wizard;
	}

	public void setWizard(Boolean wizard) {
		this.wizard = wizard;
	}

	public String getWindowTitle() {
		return windowTitle;
	}

	public void setWindowTitle(String windowTitle) {
		this.windowTitle = windowTitle;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public AccessRights getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(AccessRights accessRights) {
		this.accessRights = accessRights;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

}
