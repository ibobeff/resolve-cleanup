/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.StringUtils;

/**
 * Constants related to search and indexes.
 *
 */
public class SearchConstants {
	// used whenever there is no username provided
	public static final String SYSTEM_USERNAME = "system";

	// approximate inception day for Gen-E (1999)
	public static final long INCEPTION_DAY = 441797328000L;

	// Note: index names must be all lowercase otherwise ES will fail.
	public static final String INDEX_NAME_SECURITY_INCIDENT = "security-incident";
	public static final String DOCUMENT_TYPE_SECURITY_INCIDENT = "doc";

	public static final String INDEX_NAME_WORKSHEET = "worksheet";
	public static final String DOCUMENT_TYPE_WORKSHEET = "worksheet";
	public static final String INDEX_NAME_WORKSHEET_ALIAS = "worksheetalias";

	public static final String INDEX_NAME_WORKSHEET_DATA = "worksheetdata";
	public static final String DOCUMENT_TYPE_WORKSHEET_DATA = "worksheetdata";

	public static final String INDEX_NAME_EXECUTION_SUMMARY = "executionsummary";
	public static final String DOCUMENT_TYPE_EXECUTION_SUMMARY = "executionsummary";
	public static final String INDEX_NAME_EXECUTION_SUMMARY_ALIAS = "executionsummaryalias";
	public static final String INDEX_NAME_EXECUTION_SUMMARY_ARCHIVE = "executionsummaryarchive";
	public static final String DOCUMENT_TYPE_EXECUTION_SUMMARY_ARCHIVE = "executionsummaryarchive";

	public static final String INDEX_NAME_PROCESSREQUEST = "processrequest";
	public static final String DOCUMENT_TYPE_PROCESSREQUEST = "processrequest";
	public static final String INDEX_NAME_PROCESSREQUEST_ALIAS = "processrequestalias";

	public static final String INDEX_NAME_TASKRESULT = "taskresult";
	public static final String DOCUMENT_TYPE_TASKRESULT = "taskresult";
	public static final String INDEX_NAME_TASKRESULT_ALIAS = "taskresultalias";

	public static final String INDEX_NAME_METRICTIMER = "metrictimer";
	public static final String DOCUMENT_TYPE_METRICTIMER = "metrictimer";

	public static final String INDEX_NAME_EXECUTE_STATE = "executestate";
	public static final String DOCUMENT_TYPE_EXECUTE_STATE = "executestate";

	public static final String INDEX_NAME_METRIC_TIMER = "metrictimer";
	public static final String DOCUMENT_TYPE_METRIC_TIMER = "metrictimer";

	public static final String INDEX_NAME_WIKI_DOCUMENT = "wikidocument";
	public static final String DOCUMENT_TYPE_WIKI_DOCUMENT = "wikidocument";
	public static final String DOCUMENT_TYPE_WIKI_ATTACHMENT = "wikiattachment";
	
	// PB Activity meta data & runtime data moved from ES to DB (sir_activity_metadata, sir_activity_runtimedata)
	@Deprecated
	public static final String INDEX_NAME_PB_ACTIVITY = "pbactivity";
	@Deprecated
	public static final String DOCUMENT_TYPE_PB_ACTIVITY = "pbactivity";

	public static final String INDEX_NAME_PB_NOTES = "pbnotes";
	public static final String DOCUMENT_TYPE_PB_NOTES = "pbnotes";

	public static final String INDEX_NAME_PB_ARTIFACTS = "pbartifacts";
	public static final String DOCUMENT_TYPE_PB_ARTIFACTS = "pbartifacts";

	public static final String INDEX_NAME_PB_ATTACHMENT = "pbattachment";
	public static final String DOCUMENT_TYPE_PB_ATTACHMENT = "pbattachment";

	public static final String INDEX_NAME_PB_AUDIT_LOG = "pbauditlog";
	public static final String DOCUMENT_TYPE_PB_AUDIT_LOG = "pbauditlog";

	public static final String INDEX_NAME_ACTION_TASK = "actiontask";
	public static final String DOCUMENT_TYPE_ACTION_TASK = "actiontask";

	public static final String INDEX_NAME_SOCIAL_POST = "socialpost";
	public static final String DOCUMENT_TYPE_SOCIAL_POST = "socialpost";
	public static final String DOCUMENT_TYPE_SOCIAL_COMMENT = "socialcomment";

	public static final String INDEX_NAME_SYSTEM_POST = "systempost";
	public static final String DOCUMENT_TYPE_SYSTEM_POST = "systempost";
	public static final String DOCUMENT_TYPE_SYSTEM_COMMENT = "systemcomment";

	public static final String INDEX_NAME_TAG = "tag";
	public static final String DOCUMENT_TYPE_TAG = "tag";

	public static final String INDEX_NAME_METRIC_JVM = "metricjvm";
	public static final String DOCUMENT_TYPE_METRIC_JVM = "metricjvm";
	public static final String INDEX_NAME_METRIC_RUNBOOK = "metricrunbook";
	public static final String DOCUMENT_TYPE_METRIC_RUNBOOK = "metricrunbook";
	public static final String INDEX_NAME_METRIC_TRANSACTION = "metrictransaction";
	public static final String DOCUMENT_TYPE_METRIC_TRANSACTION = "metrictransaction";
	public static final String INDEX_NAME_METRIC_CPU = "metriccpu";
	public static final String DOCUMENT_TYPE_METRIC_CPU = "metriccpu";
	public static final String INDEX_NAME_METRIC_DB = "metricdb";
	public static final String DOCUMENT_TYPE_METRIC_DB = "metricdb";
	public static final String INDEX_NAME_METRIC_JMS_QUEUE = "metricjmsqueue";
	public static final String DOCUMENT_TYPE_METRIC_JMS_QUEUE = "metricjmsqueue";
	public static final String METRIC_ACTIONTASK_UNIT_PREFIX = "ActionTaskUnit";
	public static final String INDEX_NAME_METRIC_ACTIONTASK = "metricactiontask";
	public static final String DOCUMENT_TYPE_METRIC_ACTIONTASK = "metricactiontask";
	public static final String INDEX_NAME_METRIC_USERS = "metricusers";
	public static final String DOCUMENT_TYPE_METRIC_USERS = "metricusers";
	public static final String INDEX_NAME_METRIC_LATENCY = "metriclatency";
	public static final String DOCUMENT_TYPE_METRIC_LATENCY = "metriclatency";

	public static final String INDEX_NAME_DICTIONARY = "dictionary";
	public static final String DOCUMENT_TYPE_DICTIONARY = "dictionary";

	// public static final String INDEX_NAME_DOCUMENT_QUERY = "documentquery";
	public static final String DOCUMENT_TYPE_DOCUMENT_QUERY = "documentquery";

	// public static final String INDEX_NAME_SOCIAL_QUERY = "socialquery";
	public static final String DOCUMENT_TYPE_SOCIAL_QUERY = "socialquery";

	// public static final String INDEX_NAME_AUTOMATION_QUERY = "automationquery";
	public static final String DOCUMENT_TYPE_AUTOMATION_QUERY = "automationquery";

	// public static final String INDEX_NAME_WIKI_QUERY = "wikiquery";
	public static final String DOCUMENT_TYPE_WIKI_QUERY = "wikiquery";

	public static final String INDEX_NAME_GLOBAL_QUERY = "globalquery";
	public static final String DOCUMENT_TYPE_GLOBAL_QUERY = "globalquery";

	public static final String INDEX_NAME_USER_QUERY = "userquery";
	public static final String DOCUMENT_TYPE_USER_QUERY = "userquery";

	public static final String INDEX_NAME_SEARCH_QUERY = "searchquery";

	public static final String DOCUMENT_TYPE_SYNC_LOG = "synclog";

	public static final String SUGGEST_FIELD_NAME_DOCUMENT = "documentSuggest";
	public static final String SUGGEST_FIELD_NAME_SOCIAL = "socialSuggest";
	public static final String SUGGEST_FIELD_NAME_AUTOMATION = "automationSuggest";
	public static final String SUGGEST_FIELD_NAME_WIKI = "wikiSuggest";
	public static final String SUGGEST_FIELD_NAME_TAG = "suggest";

	// used from advanced search
	public static final String SEARCH_TYPE_ACTION_TASK = "actiontask";

	public static final String SEARCH_TYPE_DOCUMENT = "document";
	public static final String SEARCH_TYPE_DECISION_TREE = "decisiontree";
	public static final String SEARCH_TYPE_AUTOMATION = "automation";
	public static final String SEARCH_TYPE_PLAYBOOK = "playbook";

	public static final String SEARCH_TYPE_POST = "post";
	public static final String SEARCH_TYPE_NOTIFICATION = "notification";

	/*
	 * public static final String INDEX_NAME_RSS_POST = "rsspost"; public static
	 * final String DOCUMENT_TYPE_RSS_POST = "rsspost"; public static final String
	 * DOCUMENT_TYPE_RSS_COMMENT = "rsscomment";
	 */

	public static final String SYS_ID = "sysId";
	public static final String SYS_CREATED_BY = "sysCreatedBy";
	public static final String SYS_CREATED_ON = "sysCreatedOn";
	public static final String SYS_UPDATED_BY = "sysUpdatedBy";
	public static final String SYS_UPDATED_ON = "sysUpdatedOn";
	public static final String SYS_ROUTING_KEY = "sysRoutingKey";

	/*
	 * public static final String COMPONENT_UPDATED_BY = "u_component_updated_by";
	 * public static final String COMPONENT_UPDATED_ON = "u_component_updated_on";
	 * public static final String COMPONENT_CREATED_BY = "u_component_created_by";
	 * public static final String COMPONENT_CREATED_ON = "u_component_created_on";
	 * public static final String COMPONENT_AVG_RATING = "u_component_avg_rating";
	 * 
	 * public static final String ACT_RES_TIMESTAMP = "UTimestamp";
	 */

	// These are the post types we're supporting at this point. when indexing
	// or searching social posts we must have one of these in the query
	// otherwise we will simply error out so there cannot be be runtime error
	// for wrong coding.
	public static final String SOCIALPOST_TYPE_INBOX = "inbox";
	public static final String SOCIALPOST_TYPE_BLOG = "blog";
	public static final String SOCIALPOST_TYPE_POST = "post";
	public static final String SOCIALPOST_TYPE_RSS = "rss";
	// for system messages to dirretentiate between genuine system
	// or some administrative broadcast.
	public static final String SOCIALPOST_TYPE_SYSTEM = "system";
	public static final String SOCIALPOST_TYPE_MANUAL = "manual";

	public static String[] POST_INDEXES = new String[] { INDEX_NAME_SOCIAL_POST, INDEX_NAME_SYSTEM_POST };
	public static String[] POST_DOCUMENT_TYPES = new String[] { DOCUMENT_TYPE_SOCIAL_POST, DOCUMENT_TYPE_SYSTEM_POST };
	public static String[] COMMENT_DOCUMENT_TYPES = new String[] { DOCUMENT_TYPE_SOCIAL_COMMENT,
			DOCUMENT_TYPE_SYSTEM_COMMENT };

	/*
	 * public static String[] POST_INDEXES = new String[] {INDEX_NAME_SOCIAL_POST,
	 * INDEX_NAME_SYSTEM_POST, INDEX_NAME_RSS_POST}; public static String[]
	 * POST_DOCUMENT_TYPES = new String[] {DOCUMENT_TYPE_SOCIAL_POST,
	 * DOCUMENT_TYPE_SYSTEM_POST, DOCUMENT_TYPE_RSS_POST}; public static String[]
	 * COMMENT_DOCUMENT_TYPES = new String[] {DOCUMENT_TYPE_SOCIAL_COMMENT,
	 * DOCUMENT_TYPE_SYSTEM_COMMENT, DOCUMENT_TYPE_RSS_COMMENT};
	 */

	// these are the categories used by the UI to display various posts
	public static final String DISPLAY_CATEGORY_ALL = "all";
	public static final String DISPLAY_CATEGORY_ACTIVITY = "activity";
	public static final String DISPLAY_CATEGORY_INBOX = "inbox";
	public static final String DISPLAY_CATEGORY_STARRED = "starred";
	public static final String DISPLAY_CATEGORY_BLOG = "blog";
	public static final String DISPLAY_CATEGORY_SENT = "sent";
	public static final String DISPLAY_CATEGORY_SYSTEM = "system";
	public static final String DISPLAY_CATEGORY_MANUAL = "manual";
	public static final String DISPLAY_CATEGORY_NAMESPACE = "namespace";

	public static final String USER_QUERY_TYPE_DOCUMENT = "document";
	public static final String USER_QUERY_TYPE_SOCIAL = "social";
	public static final String USER_QUERY_TYPE_AUTOMATION = "automation";
	public static final String USER_QUERY_TYPE_DICTIONARY = "dictionary";
	public static final String USER_QUERY_TYPE_WIKI = "wiki";

	// this Map stores the display categories for convenience
	public static Map<String, String> DISPLAY_CATEGORIES = new HashMap<String, String>();

	// error codes
	public static final String ERROR_DOC_NOT_FOUND = "ERR1"; // document not found.

	// properties index and document type
	public static final String INDEX_NAME_PROPERTY = "property";
	public static final String DOCUMENT_TYPE_PROPERTY = "property";
	public static final String SEARCH_TYPE_PROPERTY_PLAIN = "plain";

	public static final String INDEX_NAME_CUSTOM_FORM = "customform";
	public static final String DOCUMENT_TYPE_CUSTOM_FORM = "customform";

	static {
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_ALL, DISPLAY_CATEGORY_ALL);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_ACTIVITY, DISPLAY_CATEGORY_ACTIVITY);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_INBOX, DISPLAY_CATEGORY_INBOX);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_STARRED, DISPLAY_CATEGORY_STARRED);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_BLOG, DISPLAY_CATEGORY_BLOG);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_SENT, DISPLAY_CATEGORY_SENT);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_SYSTEM, DISPLAY_CATEGORY_SYSTEM);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_MANUAL, DISPLAY_CATEGORY_MANUAL);
		DISPLAY_CATEGORIES.put(DISPLAY_CATEGORY_NAMESPACE, DISPLAY_CATEGORY_NAMESPACE);
	}

	public static final Map<String, String> INDEX_MAPPINGS = new HashMap<String, String>();

	static {
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET, "worksheets_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA, "worksheets_data_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST, "process_requests_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_TASKRESULT, "task_results_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_EXECUTE_STATE, "execute_states_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_METRIC_TIMER, "metric_timer_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT, "wiki_documents_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT, "wiki_attachments_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_ACTION_TASK, "action_tasks_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_POST, "social_posts_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT, "social_comments_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_SYSTEM_POST, "system_posts_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT, "system_comments_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_TAG, "tags_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_PB_NOTES, "pb_notes_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS, "pb_artifacts_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT, "pb_attachment_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG, "pb_auditlog_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_SECURITY_INCIDENT, "security_incident_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_PROPERTY, "properties_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM, "custom_forms_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.SEARCH_TYPE_DOCUMENT, "documents_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.SEARCH_TYPE_DECISION_TREE, "decision_trees_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.SEARCH_TYPE_AUTOMATION, "automations_mappings.json");
		INDEX_MAPPINGS.put(SearchConstants.SEARCH_TYPE_PLAYBOOK, "playbooks_mappings.json");
	}

	public static final Map<String, String> INDEX_DOCUMENT_TYPES = new HashMap<String, String>();

	static {
		// INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_WORKSHEET,
		// SearchConstants.DOCUMENT_TYPE_WORKSHEET);
		// INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_PROECESSREQUEST,
		// SearchConstants.DOCUMENT_TYPE_PROECESSREQUEST);
		// INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_TASKRESULT,
		// SearchConstants.DOCUMENT_TYPE_TASKRESULT);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_WORKSHEET_DATA,
				SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_EXECUTE_STATE, SearchConstants.DOCUMENT_TYPE_EXECUTE_STATE);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_METRIC_TIMER, SearchConstants.DOCUMENT_TYPE_METRIC_TIMER);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_WIKI_DOCUMENT, SearchConstants.DOCUMENT_TYPE_WIKI_DOCUMENT
				+ StringUtils.DELIMITER + SearchConstants.DOCUMENT_TYPE_WIKI_ATTACHMENT);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_ACTION_TASK, SearchConstants.DOCUMENT_TYPE_ACTION_TASK);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_SOCIAL_POST, SearchConstants.DOCUMENT_TYPE_SOCIAL_POST
				+ StringUtils.DELIMITER + SearchConstants.DOCUMENT_TYPE_SOCIAL_COMMENT);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_SYSTEM_POST, SearchConstants.DOCUMENT_TYPE_SYSTEM_POST
				+ StringUtils.DELIMITER + SearchConstants.DOCUMENT_TYPE_SYSTEM_COMMENT);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_TAG, SearchConstants.DOCUMENT_TYPE_TAG);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_PB_NOTES, SearchConstants.DOCUMENT_TYPE_PB_NOTES);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_PB_ARTIFACTS, SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_PB_ATTACHMENT, SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_PB_AUDIT_LOG, SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_SECURITY_INCIDENT,
				SearchConstants.DOCUMENT_TYPE_SECURITY_INCIDENT);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_PROPERTY, SearchConstants.DOCUMENT_TYPE_PROPERTY);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.INDEX_NAME_CUSTOM_FORM, SearchConstants.DOCUMENT_TYPE_CUSTOM_FORM);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.SEARCH_TYPE_DOCUMENT, SearchConstants.SEARCH_TYPE_DOCUMENT);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.SEARCH_TYPE_DECISION_TREE, SearchConstants.SEARCH_TYPE_DECISION_TREE);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.SEARCH_TYPE_AUTOMATION, SearchConstants.SEARCH_TYPE_AUTOMATION);
		INDEX_DOCUMENT_TYPES.put(SearchConstants.SEARCH_TYPE_PLAYBOOK, SearchConstants.SEARCH_TYPE_PLAYBOOK);
	}

	public static final Collection<String> WORKSHEET_ASSOCIATED_DOCUMENT_TYPES = new ArrayList<String>();

	static {
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_WORKSHEET);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_WORKSHEET_DATA);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_EXECUTION_SUMMARY);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_PROCESSREQUEST);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_TASKRESULT);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_PB_NOTES);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_PB_ARTIFACTS);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_PB_ATTACHMENT);
		WORKSHEET_ASSOCIATED_DOCUMENT_TYPES.add(DOCUMENT_TYPE_PB_AUDIT_LOG);
	}
}
