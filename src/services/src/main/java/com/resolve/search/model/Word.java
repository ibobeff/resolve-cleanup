/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
public class Word implements Serializable
{
    private static final long serialVersionUID = 6058057641317251232L;
	private String word;
    private Set<WordSource> source = new HashSet<WordSource>();
    
    public Word()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public Word(String word)
    {
        this.word = word;
    }

    public Word(String word, String source)
    {
        this.word = word;
        addFrequency(source, 1);
    }

    public String getWord()
    {
        return word;
    }

    public void setWord(String word)
    {
        this.word = word;
    }

    public String getSuggest()
    {
        return word;
    }

    public Set<WordSource> getSource()
    {
        return source;
    }

    public void setSource(Set<WordSource> source)
    {
        this.source = source;
    }
    
    public void addSource(WordSource source)
    {
        if(source != null && StringUtils.isNotBlank(source.getSource()))
        {
            getSource().add(source);
        }
    }
    
    public void addFrequency(String source, int frequency)
    {
        WordSource wordSource = findSource(source);
        if(wordSource == null)
        {
            wordSource = new WordSource(source);
        }
        wordSource.addFrequency(frequency);
        getSource().add(wordSource);
    }
    
    public void addSelectCount(String source, int selectCount)
    {
        WordSource wordSource = findSource(source);
        if(wordSource == null)
        {
            wordSource = new WordSource();
        }
        wordSource.addSelectCount(selectCount);
        getSource().add(wordSource);
    }
    
    public void setUpdatedOn(String source, int updatedOn)
    {
        WordSource wordSource = findSource(source);
        if(wordSource == null)
        {
            wordSource = new WordSource();
        }
        wordSource.setUpdatedOn(updatedOn);
        getSource().add(wordSource);
    }

    public WordSource findSource(String dictionarySource)
    {
        WordSource wordSource = null;
        if(getSource() != null)
        {
            for(WordSource source : getSource())
            {
                if(dictionarySource.equals(source.getSource()))
                {
                    wordSource = source;
                    break;
                }
            }
        }
        return wordSource;
    }
    
    @Override
    public String toString()
    {
        return "Word [word=" + word + ", source=" + source + "]";
    }
}
