/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.wiki;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ParserDecorator;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import com.resolve.util.Log;

public class AttachmentUtil
{
    public static String getContent(String fileName, byte[] content)
    {
        String result = "";

        InputStream stream = null;
        
        try
        {
            stream = new ByteArrayInputStream(content);
            
            ContentHandler handler = new BodyContentHandler(-1);
            Parser parser = new RecursiveMetadataParser(new AutoDetectParser(), handler);
            ParseContext context = new ParseContext();
            context.set(Parser.class, parser);

            Metadata metadata = new Metadata();
            metadata.set(Metadata.RESOURCE_NAME_KEY, fileName);

            //String type = metadata.get(Metadata.CONTENT_TYPE);
            //System.out.println("TYPE:" + type);
            parser.parse(stream, handler, metadata, context);
            //result = org.elasticsearch.common.Base64.encodeBytes(handler.toString().getBytes());
            result = handler.toString();
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Attachment content: " + result);
            }
        }
        catch (Exception e)
        {
            Log.log.info("Failed to parse the data properly, could be corrupted file or in a format that tika does not understand, ignoring it.");
        }
        finally
        {
            if(stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (IOException e)
                {
                    //Log.log.warn(e.getMessage(), e);
                }
            }
        }

        return result;
    }

    @SuppressWarnings("serial")
    private static class RecursiveMetadataParser extends ParserDecorator
    {
        private static final long serialVersionUID = -9005821972003602306L;
		private final ContentHandler contentHandler;
        public RecursiveMetadataParser(Parser parser, ContentHandler contentHandler)
        {
            super(parser);
            this.contentHandler = contentHandler;
        }

        @Override
        public void parse(InputStream stream, ContentHandler ignore, Metadata metadata, ParseContext context) throws IOException, SAXException, TikaException
        {
            super.parse(stream, contentHandler, metadata, context);
        }
    }
}
