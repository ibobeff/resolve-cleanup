/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;

import java.io.Serializable;
import java.util.Date;

public class PBActivity extends BaseIndexModel implements Serializable
{
	private static final long serialVersionUID = 484715989893134024L;

	public PBActivity()
	{
		super();
	}
	
	public PBActivity(String sysId, String sysOrg)
	{
		super(sysId, sysOrg);
	}
	
	public PBActivity(String sysId, String sysOrg, String createdBy)
	{
		super(sysId, sysOrg, createdBy);
	}
	
	public PBActivity(PBActivity activity)
	{
		super(activity.getSysId(), activity.getSysOrg(), activity.getSysCreatedBy());
		this.setIncidentId(activity.getIncidentId());
		this.setActivityId(activity.getActivityId());
		this.setDueDate(activity.getDueDate());
		this.setStatus(activity.getStatus());
		this.setAssignee(activity.getAssignee());
		this.setModified(activity.isModified());
	}
	
	private String worksheetId;
	private String incidentId;
	private String activityId;
	private String altActivityId;
	private Date dueDate;
	private String status;
	private String assignee;
	private Boolean modified;

	public String getWorksheetId()
	{
		return worksheetId;
	}
	public void setWorksheetId(String worksheetId)
	{
		this.worksheetId = worksheetId;
	}
	
	public String getIncidentId()
	{
		return incidentId;
	}
	public void setIncidentId(String incidentId)
	{
		this.incidentId = incidentId;
	}

	public String getActivityId()
	{
		return activityId;
	}
	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}

	public String getAltActivityId()
	{
		return altActivityId;
	}
	public void setAltActivityId(String altActivityId)
	{
		this.altActivityId = altActivityId;
	}

	public Date getDueDate()
	{
		return dueDate;
	}
	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}

	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getAssignee()
	{
		return assignee;
	}
	public void setAssignee(String assignee)
	{
		this.assignee = assignee;
	}

    public Boolean isModified()
    {
        return modified;
    }
    public void setModified(Boolean modified)
    {
        this.modified = modified;
    }
}
