/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.metric;

import com.resolve.search.APIFactory;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchException;
import com.resolve.search.model.MetricTimer;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MetricSearchAPI
{
    private static final SearchAPI<MetricTimer> metricTimerSearchAPI = APIFactory.getMetricTimerSearchAPI();

    public static ResponseDTO<MetricTimer> searchMetricTimer(QueryDTO queryDTO, String username) throws SearchException
    {
        ResponseDTO<MetricTimer> result = null;

        try
        {
            result = metricTimerSearchAPI.searchByQuery(queryDTO, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }

    public static ResponseDTO<MetricTimer> findMetricTimerById(String sysId, String username) throws SearchException
    {
        ResponseDTO<MetricTimer> result = null;

        try
        {
            if (StringUtils.isBlank(sysId))
            {
                throw new SearchException("sysId must not be blank");
            }
            result = new ResponseDTO<MetricTimer>();
            result.setData(metricTimerSearchAPI.findDocumentById(sysId, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        return result;
    }
}
