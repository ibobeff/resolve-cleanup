/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

import org.codehaus.jackson.annotate.JsonProperty;

import com.resolve.search.SearchConstants;


@SuppressWarnings("serial")
public class WikiQuery extends AbstractQuery
{
    private static final long serialVersionUID = -350277024092175224L;

	public WikiQuery()
    {
        super();
        //blank constructor, required for JSON serialization.
    }

    public WikiQuery(String suggest, int weight)
    {
        super(suggest, weight);
    }

    public WikiQuery(String suggest, int weight, Long sysUpdatedOn)
    {
        super(suggest, weight, sysUpdatedOn);
    }
    
    @JsonProperty(SearchConstants.SUGGEST_FIELD_NAME_WIKI)
    @Override
    public Suggest getSuggest()
    {
        return suggest;
    }

    @Override
    public void setSuggest(Suggest suggest)
    {
        this.suggest = suggest;
    }

    @Override
    public String getSuggestFieldName()
    {
        return SearchConstants.SUGGEST_FIELD_NAME_WIKI;
    }
}
