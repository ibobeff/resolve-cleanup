/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.search;

import com.resolve.util.Log;
import com.resolve.util.queue.AbstractQueueListener;

/**
 * Listens to a data queue that receives content to be indexed.
 *
 */
public class IndexListener<T> extends AbstractQueueListener<T>
{
    private final Indexer<T> indexer;

    public IndexListener(Indexer<T> indexer)
    {
        this.indexer = indexer;
    }

    public boolean process(final T object)
    {
        boolean result = true;

        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("Received data to be indexed");
            Log.log.trace("Contents:" + object);
        }

        new Thread(new Runnable()
        {
            public void run()
            {
                try
                {
                    boolean indexResult = indexer.index(object);
                    if (!indexResult)
                    {
                        Log.log.warn("Indexing failed, unfortunately you have to check the log file for more information. Index data: " + object);
                    }
                }
                catch (SearchException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }).start();

        return result;
    }
}
