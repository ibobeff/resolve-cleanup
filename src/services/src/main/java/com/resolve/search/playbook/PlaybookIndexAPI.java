/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.playbook;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.resolve.search.APIFactory;
import com.resolve.search.IndexAPI;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchException;
import com.resolve.search.model.PBActivity;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBAuditLog;
import com.resolve.search.model.PBNotes;
import com.resolve.search.model.Worksheet;
import com.resolve.search.model.WorksheetData;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.hibernate.util.RBACUtils;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class PlaybookIndexAPI
{
	@Deprecated
	private static final IndexAPI pbActivityIndexAPI = APIFactory.getPBActivityIndexAPI();
	private static final IndexAPI pbNotesIndexAPI = APIFactory.getPBNotesIndexAPI();
	private static final IndexAPI pbArtifactsIndexAPI = APIFactory.getPBArtifactsIndexAPI();
	private static final IndexAPI pbAttachmentIndexAPI = APIFactory.getPBAttachmentIndexAPI();
	private static final IndexAPI pbAuditLogIndexAPI = APIFactory.getPBAuditLogIndexAPI();
	
	private static final SearchAPI<PBArtifacts> pbArtifactsSearchAPI = APIFactory.getPBArtifactsSearchAPI(PBArtifacts.class);
	private static final SearchAPI<PBAttachments> pbAttachmentsSearchAPI = 
														APIFactory.getPBAttachmentsSearchAPI(PBAttachments.class);
	private static final SearchAPI<PBAuditLog> pbAuditLogSearchAPI = 
														APIFactory.getPBAuditLogSearchAPI(PBAuditLog.class);
	private static final SearchAPI<PBNotes> pbNotesSearchAPI = APIFactory.getPBNotesSearchAPI(PBNotes.class);
	
	@Deprecated
	public static void indexPBActivity(PBActivity activity, final String username)
	{
		if (activity != null)
		{
			try
			{
				pbActivityIndexAPI.indexDocument(activity, true, username);
			}
			catch (SearchException e)
			{
				Log.log.error("Could not index activity for incidentId: " + activity.getIncidentId(), e);
			}
		}
	}
	
	public static String indexPBNotes(PBNotes note, final String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_NOTE_CREATE, null))
	    {
	        throw new Exception(String.format("User %s does not have access to create new note", username));
	    }
	    
		String noteId = null;
		if (note != null)
		{
			try
			{
				noteId = pbNotesIndexAPI.indexDocument(note, true, username);
			}
			catch (SearchException e)
			{
				Log.log.error("Could not index notes for SIR: " + note.getIncidentId(), e);
			}
		}
		
		return noteId;
	}
	
	public static void indexPBArtifacts(PBArtifacts artifact, final String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ARTIFACT_CREATE, null))
        {
            throw new Exception(String.format("User %s does not have access to create new artifact.", username));
        }
	    
		if (artifact != null)
		{
			try
			{
				pbArtifactsIndexAPI.indexDocument(artifact, true, username);
			}
			catch (SearchException e)
			{
				Log.log.error("Could not index artifact for SIR: " + artifact.getIncidentId(), e);
			}
		}
	}
	
	public static void deleteArtifacts(List<String> sysIds, String username) throws SearchException
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ARTIFACT_DELETE, null))
        {
            throw new SearchException(String.format("User %s does not have access to delete an artifact.", username));
        }
	    
	    if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
	    else
	    {
	        pbArtifactsIndexAPI.deleteArtifacts(sysIds, username);
	    }
	}
	
	public static void indexPBAttachment(PBAttachments attachment, final String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_UPLOAD, null))
        {
            throw new Exception(String.format("User %s does not have access to upload an attachment of this incident.", username));
        }
	    
		if (attachment != null)
		{
			try
			{
				pbAttachmentIndexAPI.indexDocument(attachment, true, username);
			}
			catch (SearchException e)
			{
				Log.log.error("Could not index attachment for SIR: " + attachment.getIncidentId(), e);
			}
		}
	}
	
	public static void deleteAttachments(List<String> sysIds, String username) throws Exception
	{
	    if (!UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DELETE, null))
        {
            throw new Exception(String.format("User %s does not have access to delete an attachment.", username));
        }
	    
	    if (sysIds == null || sysIds.size() == 0)
        {
            Log.log.debug("Empty sysIds provided, check the calling code");
        }
	    else
	    {
	        pbAttachmentIndexAPI.deleteArtifacts(sysIds, username);
	    }
	}
	
	public static void indexPBAuditLog(PBAuditLog auditLog, final String username) throws SearchException
	{
		if (auditLog != null)
		{
		    try
	        {
	            PlaybookUtils.getSecurityIncident(auditLog.getIncidentId(), null, username);
	        }
	        catch (Exception e)
	        {
	            throw new SearchException(e.getMessage());
	        }
		    
			try
			{
				pbAuditLogIndexAPI.indexDocument(auditLog, true, username);
			}
			catch (SearchException e)
			{
				Log.log.error("Could not index audit log for SIR: " + auditLog.getIncidentId(), e);
			}
		}
	}
	
	public static ResponseDTO<PBArtifacts> findArtifactsByWorksheetIds(List<String> worksheetIds) throws SearchException {
		ResponseDTO<PBArtifacts> result = null;

		try {
			if (CollectionUtils.isEmpty(worksheetIds)) {
				throw new SearchException("Worksheet Ids must not be empty");
			}

			result = new ResponseDTO<PBArtifacts>();
			result.setSuccess(false);

			List<PBArtifacts> artifacts = new ArrayList<>();

			QueryBuilder worksheetIdfilter = QueryBuilders.termsQuery("worksheetId", worksheetIds);
			QueryBuilder finalfilter = QueryBuilders.boolQuery().must(worksheetIdfilter);

			String scrollId = null;

			try {
				TimeValue scrollTimeOut = new TimeValue(pbArtifactsSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);

				Pair<ResponseDTO<PBArtifacts>, String> scrollArtifacts;

				scrollArtifacts = pbArtifactsSearchAPI.searchByFilteredQuery(null, null,
																			 new QueryBuilder[] { finalfilter }, 
																			 UserUtils.SYSTEM, scrollId, scrollTimeOut);

				while(scrollArtifacts != null && scrollArtifacts.getLeft() != null &&
					  CollectionUtils.isNotEmpty(scrollArtifacts.getLeft().getRecords())) {
					scrollId = StringUtils.isNotBlank(scrollArtifacts.getRight()) ? scrollArtifacts.getRight() : null;

					artifacts.addAll(scrollArtifacts.getLeft().getRecords());

					scrollArtifacts = null;

					if (StringUtils.isNotBlank(scrollId)) {
						scrollArtifacts = pbArtifactsSearchAPI.searchByFilteredQuery(null, null,
																					 new QueryBuilder[] { finalfilter }, 
																					 UserUtils.SYSTEM, scrollId, 
																					 scrollTimeOut);
					}
				}

				if (scrollArtifacts != null) {
					scrollId = StringUtils.isNotBlank(scrollArtifacts.getRight()) ? scrollArtifacts.getRight() : null;

					if (StringUtils.isNotBlank(scrollId)) {
						pbArtifactsSearchAPI.clearScroll(scrollId);
						scrollId = null;
					}
				}

				result.setRecords(artifacts).setSuccess(true);
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin getting artifacts for %d SIR worksheet ids", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											worksheetIds.size()), t);
			} finally {
				if (StringUtils.isNotBlank(scrollId)) {
					pbArtifactsSearchAPI.clearScroll(scrollId);
				}
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		}

		return result;
	}
	
	public static List<String> findArtifactIdsByWorksheetIds(List<String> worksheetIds) throws SearchException {
		final List<String> artifactIds = new CopyOnWriteArrayList<>();
		
		ResponseDTO<PBArtifacts> artifactsResp = findArtifactsByWorksheetIds(worksheetIds);
		
		if (artifactsResp != null && artifactsResp.isSuccess() && 
			CollectionUtils.isNotEmpty(artifactsResp.getRecords())) {
						
			artifactsResp.getRecords().parallelStream().forEach(artifact -> {
				if (artifact != null && StringUtils.isNotBlank(artifact.getSysId())) {
					artifactIds.add(artifact.getSysId());
				}
			});
		}
		
		return artifactIds;
	}
	
	public static ResponseDTO<PBAttachments> findAttachmentsByWorksheetIds(List<String> worksheetIds) throws SearchException {
		ResponseDTO<PBAttachments> result = null;

		try {
			if (CollectionUtils.isEmpty(worksheetIds)) {
				throw new SearchException("Worksheet Ids must not be empty");
			}

			result = new ResponseDTO<PBAttachments>();
			result.setSuccess(false);

			List<PBAttachments> attachments = new ArrayList<>();

			QueryBuilder worksheetIdfilter = QueryBuilders.termsQuery("worksheetId", worksheetIds);
			QueryBuilder finalfilter = QueryBuilders.boolQuery().must(worksheetIdfilter);

			String scrollId = null;

			try {
				TimeValue scrollTimeOut = new TimeValue(pbAttachmentsSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);

				Pair<ResponseDTO<PBAttachments>, String> scrollAttachments;

				scrollAttachments = pbAttachmentsSearchAPI.searchByFilteredQuery(null, null,
																			 	 new QueryBuilder[] { finalfilter }, 
																			 	 UserUtils.SYSTEM, scrollId, scrollTimeOut);

				while(scrollAttachments != null && scrollAttachments.getLeft() != null &&
					  CollectionUtils.isNotEmpty(scrollAttachments.getLeft().getRecords())) {
					scrollId = StringUtils.isNotBlank(scrollAttachments.getRight()) ? scrollAttachments.getRight() : null;

					attachments.addAll(scrollAttachments.getLeft().getRecords());

					scrollAttachments = null;

					if (StringUtils.isNotBlank(scrollId)) {
						scrollAttachments = pbAttachmentsSearchAPI.searchByFilteredQuery(null, null,
																					 	 new QueryBuilder[] { finalfilter }, 
																					 	 UserUtils.SYSTEM, scrollId, 
																					 	 scrollTimeOut);
					}
				}

				if (scrollAttachments != null) {
					scrollId = StringUtils.isNotBlank(scrollAttachments.getRight()) ? scrollAttachments.getRight() : null;

					if (StringUtils.isNotBlank(scrollId)) {
						pbAttachmentsSearchAPI.clearScroll(scrollId);
						scrollId = null;
					}
				}

				result.setRecords(attachments).setSuccess(true);
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin getting attachments for %d SIR worksheet ids", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											worksheetIds.size()), t);
			} finally {
				if (StringUtils.isNotBlank(scrollId)) {
					pbAttachmentsSearchAPI.clearScroll(scrollId);
				}
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		}

		return result;
	}
	
	public static List<String> findAttachmentIdsByWorksheetIds(List<String> worksheetIds) throws SearchException {
		final List<String> attachmentIds = new CopyOnWriteArrayList<>();
		
		ResponseDTO<PBAttachments> attachmentsResp = findAttachmentsByWorksheetIds(worksheetIds);
		
		if (attachmentsResp != null && attachmentsResp.isSuccess() && 
			CollectionUtils.isNotEmpty(attachmentsResp.getRecords())) {
						
			attachmentsResp.getRecords().parallelStream().forEach(attachment -> {
				if (attachment != null && StringUtils.isNotBlank(attachment.getSysId())) {
					attachmentIds.add(attachment.getSysId());
				}
			});
		}
		
		return attachmentIds;
	}
	
	public static ResponseDTO<PBAuditLog> findAuditLogByIncidentIds(List<String> incidentIds) throws SearchException {
		ResponseDTO<PBAuditLog> result = null;

		try {
			if (CollectionUtils.isEmpty(incidentIds)) {
				throw new SearchException("Incident Ids must not be empty");
			}

			result = new ResponseDTO<PBAuditLog>();
			result.setSuccess(false);

			List<PBAuditLog> auditLogs = new ArrayList<>();

			QueryBuilder incidentIdfilter = QueryBuilders.termsQuery("incidentId", incidentIds);
			QueryBuilder finalfilter = QueryBuilders.boolQuery().must(incidentIdfilter);

			String scrollId = null;

			try {
				TimeValue scrollTimeOut = new TimeValue(pbAuditLogSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);

				Pair<ResponseDTO<PBAuditLog>, String> scrollAuditLog;

				scrollAuditLog = pbAuditLogSearchAPI.searchByFilteredQuery(null, null,
																			 	 new QueryBuilder[] { finalfilter }, 
																			 	 UserUtils.SYSTEM, scrollId, scrollTimeOut);

				while(scrollAuditLog != null && scrollAuditLog.getLeft() != null &&
					  CollectionUtils.isNotEmpty(scrollAuditLog.getLeft().getRecords())) {
					scrollId = StringUtils.isNotBlank(scrollAuditLog.getRight()) ? scrollAuditLog.getRight() : null;

					auditLogs.addAll(scrollAuditLog.getLeft().getRecords());

					scrollAuditLog = null;

					if (StringUtils.isNotBlank(scrollId)) {
						scrollAuditLog = pbAuditLogSearchAPI.searchByFilteredQuery(null, null,
																				   new QueryBuilder[] { finalfilter }, 
																				   UserUtils.SYSTEM, scrollId, 
																				   scrollTimeOut);
					}
				}

				if (scrollAuditLog != null) {
					scrollId = StringUtils.isNotBlank(scrollAuditLog.getRight()) ? scrollAuditLog.getRight() : null;

					if (StringUtils.isNotBlank(scrollId)) {
						pbAuditLogSearchAPI.clearScroll(scrollId);
						scrollId = null;
					}
				}

				result.setRecords(auditLogs).setSuccess(true);
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin getting audit logs for %d SIR ids", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											incidentIds.size()), t);
			} finally {
				if (StringUtils.isNotBlank(scrollId)) {
					pbAuditLogSearchAPI.clearScroll(scrollId);
				}
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		}

		return result;
	}
	
	public static List<String> findAuditLogIdsByIncidentIds(List<String> incidentIds) throws SearchException {
		final List<String> auditLogIds = new CopyOnWriteArrayList<>();
		
		ResponseDTO<PBAuditLog> auditLogsResp = findAuditLogByIncidentIds(incidentIds);
		
		if (auditLogsResp != null && auditLogsResp.isSuccess() && 
			CollectionUtils.isNotEmpty(auditLogsResp.getRecords())) {
						
			auditLogsResp.getRecords().parallelStream().forEach(auditLog -> {
				if (auditLog != null && StringUtils.isNotBlank(auditLog.getSysId())) {
					auditLogIds.add(auditLog.getSysId());
				}
			});
		}
		
		return auditLogIds;
	}
	
	public static ResponseDTO<PBNotes> findNotesByWorksheetIds(List<String> worksheetIds) throws SearchException {
		ResponseDTO<PBNotes> result = null;

		try {
			if (CollectionUtils.isEmpty(worksheetIds)) {
				throw new SearchException("Worksheet Ids must not be empty");
			}

			result = new ResponseDTO<PBNotes>();
			result.setSuccess(false);

			List<PBNotes> notes = new ArrayList<>();

			QueryBuilder worksheetIdfilter = QueryBuilders.termsQuery("worksheetId", worksheetIds);
			QueryBuilder finalfilter = QueryBuilders.boolQuery().must(worksheetIdfilter);

			String scrollId = null;

			try {
				TimeValue scrollTimeOut = new TimeValue(pbNotesSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);

				Pair<ResponseDTO<PBNotes>, String> scrollNotes;

				scrollNotes = pbNotesSearchAPI.searchByFilteredQuery(null, null,
																	 new QueryBuilder[] { finalfilter }, 
																	 UserUtils.SYSTEM, scrollId, scrollTimeOut);

				while(scrollNotes != null && scrollNotes.getLeft() != null &&
					  CollectionUtils.isNotEmpty(scrollNotes.getLeft().getRecords())) {
					scrollId = StringUtils.isNotBlank(scrollNotes.getRight()) ? scrollNotes.getRight() : null;

					notes.addAll(scrollNotes.getLeft().getRecords());

					scrollNotes = null;

					if (StringUtils.isNotBlank(scrollId)) {
						scrollNotes = pbNotesSearchAPI.searchByFilteredQuery(null, null,
																			 new QueryBuilder[] { finalfilter }, 
																			 UserUtils.SYSTEM, scrollId, 
																			 scrollTimeOut);
					}
				}

				if (scrollNotes != null) {
					scrollId = StringUtils.isNotBlank(scrollNotes.getRight()) ? scrollNotes.getRight() : null;

					if (StringUtils.isNotBlank(scrollId)) {
						pbNotesSearchAPI.clearScroll(scrollId);
						scrollId = null;
					}
				}

				result.setRecords(notes).setSuccess(true);
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin getting notes for %d SIR worksheet ids", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											worksheetIds.size()), t);
			} finally {
				if (StringUtils.isNotBlank(scrollId)) {
					pbNotesSearchAPI.clearScroll(scrollId);
				}
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw new SearchException(e.getMessage(), e);
		}

		return result;
	}
	
	public static List<String> findNoteIdsByWorksheetIds(List<String> worksheetIds) throws SearchException {
		final List<String> noteIds = new CopyOnWriteArrayList<>();
		
		ResponseDTO<PBNotes> notesResp = findNotesByWorksheetIds(worksheetIds);
		
		if (notesResp != null && notesResp.isSuccess() && 
			CollectionUtils.isNotEmpty(notesResp.getRecords())) {
						
			notesResp.getRecords().parallelStream().forEach(note -> {
				if (note != null && StringUtils.isNotBlank(note.getSysId())) {
					noteIds.add(note.getSysId());
				}
			});
		}
		
		return noteIds;
	}
}
