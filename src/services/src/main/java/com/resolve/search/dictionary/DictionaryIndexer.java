/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.dictionary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.google.common.collect.Lists;
import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.Indexer;
import com.resolve.search.SearchException;
import com.resolve.search.model.Word;
import com.resolve.util.Log;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class DictionaryIndexer implements Indexer<IndexData<Word>>
{
    private static volatile DictionaryIndexer instance = null;
    private final IndexAPI indexAPI;

    private final ConfigSearch config;
    private long indexThreads;
    private final String dictionarySource;

    private ResolveConcurrentLinkedQueue<IndexData<Word>> dataQueue = null;

    public DictionaryIndexer(ConfigSearch config, String dictionarySource)
    {
        this.config = config;
        this.dictionarySource = dictionarySource;
        this.indexAPI = APIFactory.getDictionaryIndexAPI();
        this.indexThreads = this.config.getIndexthreads();
        
        Log.log.debug("Initializing DictionaryIndexer.");
        QueueListener<IndexData<Word>> actionTaskIndexListener = new IndexListener<IndexData<Word>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(actionTaskIndexListener);
        Log.log.debug("DictionaryIndexer initialized.");
    }

    public boolean enqueue(IndexData<Word> indexData)
    {
        return dataQueue.offer(indexData);
    }

    @Override
    public boolean index(IndexData<Word> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<Word> words = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexDictionaryWords(words, indexData.getUsername());
                    break;
                case PURGE:
                    purgeDictionaryWords(words, indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    private void purgeDictionaryWords(Collection<Word> words, String username) throws SearchException
    {
        if(words == null || words.size() == 0)
        {
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            indexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
        else
        {
            List<String> wordList = new ArrayList<String>();
            for(Word word : words)
            {
                wordList.add(word.getWord());
            }
            indexAPI.deleteDocumentByIds(wordList, username);
        }
    }

    private void indexWords(final Collection<Word> dictionaryWords, final String username)
    {
        try
        {
            for(Word word : dictionaryWords)
            {
                Word tmpDictionary = DictionarySearchAPI.findWordById(word.getWord(), username);
                if(tmpDictionary != null)
                {
                    Log.log.trace("Found already available word word: " + word.getWord());
                    word.addFrequency(dictionarySource, 1);
                }
            }
            indexAPI.indexDocuments("word", null, dictionaryWords, false, username);
        }
        catch (SearchException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }

    public void indexDictionaryWords(Collection<Word> words, String username)
    {
        //Collection does not have subList method hence this conversion.
        //caller gurantees that the Collection is a List
        List<Word> wordList = Lists.newLinkedList(words);

        long startTime = 0L;
        long endTime = 0L;

        int count = wordList.size();
        int fromIndex = 0;
        int toIndex = 10000;
        while (fromIndex < count)
        {
            if (toIndex > count)
            {
                toIndex = count;
            }
            List<Word> subList = wordList.subList(fromIndex, toIndex);
            if (subList != null && subList.size() > 0)
            {
                if (subList != null && subList.size() > 0)
                {
                    startTime = System.currentTimeMillis();
                    indexWords(subList, username);
                    endTime = System.currentTimeMillis();
                    if(Log.log.isTraceEnabled())
                    {
                        Log.log.trace("Time (millis) taken to index " + subList.size() + " words in ES : " + (endTime - startTime));
                    }
                }
            }
            // sleep for 5 seconds if we have a lot of data
            // will improve this later.
            if (count > 10000)
            {
                try
                {
                    Thread.sleep(5000L);
                }
                catch (InterruptedException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            fromIndex = toIndex;
            toIndex += 10000;
        }
    }
}
