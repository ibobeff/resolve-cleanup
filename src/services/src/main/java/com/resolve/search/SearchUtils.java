/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.search;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

import com.eaio.uuid.UUID;
import com.resolve.search.model.SocialComment;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.StripHTML;
import com.resolve.search.model.Tag;
import com.resolve.search.model.UserInfo;
import com.resolve.search.model.query.GlobalQuery;
import com.resolve.search.model.query.UserQuery;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.util.Constants;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.QuerySort.SortOrder;
import com.resolve.util.BeanUtil;
import com.resolve.util.DateUtils;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.NumberUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.diff_match_patch;
import com.resolve.util.diff_match_patch.Diff;

public class SearchUtils
{
    public static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyyMMdd");
    
    private static final ObjectMapper MAPPER = new ObjectMapper();
    static
    {
        MAPPER.enable(SerializationConfig.Feature.INDENT_OUTPUT);
    }

    public static enum SYNC_OPERATION
    {
        INDEX, UPDATE_BY_SCRIPT, DELETE, DELETE_BY_QUERY, BULK_DELETE
    };

    private static Map<String, String> SYNC_LOGS = new HashMap<String, String>();
    static
    {
        //SYNC_LOGS.put("actiontask", "synclog_actiontask");
        //SYNC_LOGS.put("wikidocument", "synclog_wikidocument");

        SYNC_LOGS.put("dictionary", "synclog_misc");
        SYNC_LOGS.put("globalquery", "synclog_misc");
        SYNC_LOGS.put("searchquery", "synclog_misc");
        SYNC_LOGS.put("userquery", "synclog_misc");

        SYNC_LOGS.put("socialpost", "synclog_socialpost");
        SYNC_LOGS.put("systempost", "synclog_systempost");
        
        SYNC_LOGS.put("tag", "synclog_tag");

        SYNC_LOGS.put("worksheetalias", "synclog_worksheet"); //alias
        SYNC_LOGS.put("worksheet", "synclog_worksheet"); //based on document type

        SYNC_LOGS.put("processrequestalias", "synclog_processrequest"); //alias
        SYNC_LOGS.put("processrequest", "synclog_processrequest"); //based on document type
        
        SYNC_LOGS.put("taskresultalias", "synclog_taskresult"); //alias
        SYNC_LOGS.put("taskresult", "synclog_taskresult"); //based on document type
        
        SYNC_LOGS.put("pbnotes", "synclog_pbnotes");
        SYNC_LOGS.put("pbauditlog", "synclog_pbauditlog");
        SYNC_LOGS.put("pbartifacts", "synclog_pbartifacts");
        SYNC_LOGS.put("pbattachment", "synclog_pbattachment");
        SYNC_LOGS.put("pbactivity", "synclog_pbactivity");
    }
    
    private static final Pattern TAG_PATTERN = Pattern.compile("((|\\A)#[^\\s]+)", Pattern.DOTALL);
    // private static final Pattern MENTION_PATTERN =
    // Pattern.compile("((\\s|\\A)@[^\\s]+)", Pattern.DOTALL);
    private static final Document.OutputSettings outputSettings = new Document.OutputSettings().prettyPrint(false);

    public static final String ES_SIMPLE_QUERY_REGEX = ".*[+|\\-\"*()^\\[\\]\\!\\{\\}\\:/~].*";
    
    //the following characters need to be escaped in value of a ES query: !(){}[]^"~:\
    public static final String LUCENE_SPECIAL_CHAR = ".*[\\!\\(\\)\\{\\}\\[\\]\\^\\\"~\\:\\\\].*";
    public static final String LUCENE_SPECIAL_CHAR_REPL =  "([\\!\\(\\)\\{\\}\\[\\]\\^\\\"~\\:\\\\])";
    public static final String LUCENE_SPECIAL_CHARS_FULLTEXT = "[-\\+\\|\\\"\\(\\)\\^\\[\\]\\!\\{\\}\\:/~\\\\\\&\\*<>]";
    public static final Pattern SPECIAL_CHARS_FULLTEXT_PATTERN = Pattern.compile(LUCENE_SPECIAL_CHARS_FULLTEXT);
    
    public static String[] WORKSHEET_KEYWORD_FIELDS = { "summary", "reference", "alertId" };
    public static String[] SECURITY_INCIDENT_FIELDS = { "sys_org" };
    public static String[] SPECIAL_SEARCH_COMMANDS = {"_exists_"};
    
    private static Map<String, Set<String>> modelNonanalyzedFields = new HashMap<String, Set<String>>();
    static
    {
		modelNonanalyzedFields.put("com.resolve.search.model.Worksheet",
				new HashSet<String>(Arrays.asList(WORKSHEET_KEYWORD_FIELDS)));
		modelNonanalyzedFields.put("com.resolve.search.model.SecurityIncident",
				new HashSet<String>(Arrays.asList(SECURITY_INCIDENT_FIELDS)));
    }

    // Create a new row key, borrowed from old cassandra code
    public static String getNewRowKey()
    {
        UUID id = new UUID();
        return new StringBuilder( id.toString()).delete(23, 24).delete(18, 19).delete(13, 14).delete(8, 9).toString();
    }

    /**
     * Sanitizes the QueryDTO and sets default in case the object is null or not
     * up to certain reasonable level of consistency.
     * 
     * @param queryDTO
     * @param sortField
     * @param sortDirection
     */
    public static void sanitizeQueryDTO(QueryDTO queryDTO, String sortField, SortOrder sortDirection)
    {
        if (queryDTO == null)
        {
            queryDTO = new QueryDTO();
        }

        if (queryDTO.getSortItems() == null || queryDTO.getSortItems().size() == 0)
        {
            queryDTO.setOperator(QueryDTO.AND_OPERATOR);
            QuerySort sort = new QuerySort(sortField, sortDirection);
            List<QuerySort> sorts = new ArrayList<QuerySort>();
            sorts.add(sort);
            queryDTO.setSortItems(sorts);
        }
    }

    public static QueryBuilder createBoolQueryBuilder(QueryDTO queryDTO)
    {
    	return createBoolQueryBuilder(queryDTO, null);
    }

    public static QueryBuilder createBoolQueryBuilder(QueryDTO queryDTO, String modelClass)
    {
    	return createBoolQueryBuilder(queryDTO, modelClass, false);
    }
    
    public static QueryBuilder createBoolQueryBuilder(QueryDTO queryDTO, String modelClass, boolean enforceOrg)
    {
        QueryBuilder result = null;

        List<QueryFilter> filters = queryDTO.getFilterItems();

        if (filters == null || filters.size() == 0)
        {
            // this query is for all the records, of course limited by
            // start/limit.
            result = matchAllQuery();
        }
        else
        {
            BoolQueryBuilder queryBuilder = boolQuery();

            String operator = queryDTO.getOperator();
            boolean foundFilter = false;
            for (QueryFilter filter : filters)
            {
                if (filter.isValid())
                {
                    if (!foundFilter)
                    {
                        foundFilter = true;
                    }
                    String field = filter.getField();
                    String value = filter.getValue();
                    boolean isNotQuery = false;

                    QueryBuilder filterQueryBuilder = null;

                    if (StringUtils.isBlank(value))
                    {
                        value = "";
                    }
                    else
                    {
                        value = value.trim();
                    }

                    if (filter.isBoolType())
                    {
                        // if the value is a string anything other than "true"
                        // (case
                        // insensitive, including null)
                        // then booleanValue will be false.
                        Boolean booleanValue = Boolean.valueOf(value);
                        if (QueryFilter.NOT_EQUALS.equalsIgnoreCase(filter.getCondition()))
                        {
                            booleanValue = !booleanValue; // flip it
                        }

                        filterQueryBuilder = boolQuery().must(matchQuery(field, booleanValue));
                    }
                    else if (filter.isDateType())
                    {
                        filterQueryBuilder = createDateRangeQueryBuilder(filter);
                    }
                    else if (filter.isNumericType())
                    {
                        filterQueryBuilder = createNumericRangeQueryBuilder(filter);
                    }
                    else if (filter.isTermsType())
                    {
                        filterQueryBuilder = createTermsQueryBuilder(filter);
                    }
                    else
                    {
                        String prefix = "";
                        String suffix = "";
                        
                        if (QueryFilter.NOT_EQUALS.equalsIgnoreCase(filter.getCondition()))
                        {
                            isNotQuery = true;
                        }

                        if (QueryFilter.STARTS_WITH.equalsIgnoreCase(filter.getCondition()))
                        {
                            suffix = "*";
                        }
                        else if (QueryFilter.ENDS_WITH.equalsIgnoreCase(filter.getCondition()))
                        {
                            prefix = "*";
                        }
                        else if (QueryFilter.NOT_STARTS_WITH.equalsIgnoreCase(filter.getCondition()))
                        {
                            suffix = "*";
                            isNotQuery = true;
                        }
                        else if (QueryFilter.NOT_ENDS_WITH.equalsIgnoreCase(filter.getCondition()))
                        {
                            prefix = "*";
                            isNotQuery = true;
                        }
                        else if ((QueryFilter.CONTAINS.equalsIgnoreCase(filter.getCondition())))
                        {
                            prefix = "*";
                            suffix = "*";
                        }
                        else if ((QueryFilter.NOT_CONTAINS.equalsIgnoreCase(filter.getCondition())))
                        {
                            prefix = "*";
                            suffix = "*";
                            isNotQuery = true;
                        }

                        if (QueryFilter.EQUALS.equalsIgnoreCase(filter.getCondition()) || QueryFilter.NOT_EQUALS.equalsIgnoreCase(filter.getCondition()))
                        {
                        	boolean isSet = false;
                        	if (StringUtils.isNotBlank(modelClass))
                        	{
                        		Set<String> set = modelNonanalyzedFields.get(modelClass);
                        		if (set!=null && set.contains(field))
                        		{
                        			filterQueryBuilder = QueryBuilders.termQuery(field, value); 
                        			isSet=true;
                        		}
                        	}
                        		
                            if (!isSet)
                            {
	                        	if(hasSpecialChar(value))
	                            {
	                                //got to escape them
	                                value = removeLuceneSpecialChar(value);
	                            }
	                            filterQueryBuilder = QueryBuilders.matchPhraseQuery(field, value);
                            }
                        }
                        else if (QueryFilter.NOT_EXISTS.equalsIgnoreCase(filter.getCondition()))
                        {
//                            MissingFilterBuilder mfb = missingFilter(filter.getField());
//                            filterQueryBuilder = new ConstantScoreQueryBuilder(mfb);
                            filterQueryBuilder = new BoolQueryBuilder().mustNot(new ExistsQueryBuilder(filter.getField()));
                        }
                        else if (QueryFilter.EXISTS.equalsIgnoreCase(filter.getCondition()))
                        {
//                            ExistsFilterBuilder efb = existsFilter(filter.getField());
//                            filterQueryBuilder = new ConstantScoreQueryBuilder(efb);
                            filterQueryBuilder = new ExistsQueryBuilder(filter.getField());
                        }
                        else if (QueryFilter.STARTS_WITH.equalsIgnoreCase(filter.getCondition()))
                        {
                            if(hasSpecialChar(value))
                            {
                                //got to escape them
                                value = removeLuceneSpecialChar(value);
                            }
                        	filterQueryBuilder = QueryBuilders.matchPhrasePrefixQuery(filter.getField(), value);
                        }	
                        else if (QueryFilter.FULLTEXT.equalsIgnoreCase(filter.getCondition()))
                        {
                    		boolean skip = modelNonanalyzedFields.containsKey(modelClass) && modelNonanalyzedFields.get(modelClass).contains(field);
                    		if (!skip && SPECIAL_CHARS_FULLTEXT_PATTERN.matcher(value).find()) {
                            	value = String.join("", escapeAllLuceneSpecialChars(value), "*");
                    		}
                            filterQueryBuilder = getStringQueryBuilder(value, field);
							Operator defaultOperator = StringUtils.isNotBlank(filter.getParamName())
									? Operator.fromString(filter.getParamName()) : Operator.AND;
							filterQueryBuilder = ((QueryStringQueryBuilder) filterQueryBuilder).escape(true)
									.analyzeWildcard(true).defaultOperator(defaultOperator);
                        }
                        else
                        {
                    		boolean skip = modelNonanalyzedFields.containsKey(modelClass) && modelNonanalyzedFields.get(modelClass).contains(field);

                            if(!skip && hasSpecialChar(value))
                            {
                                //got to escape them
                                value = removeLuceneSpecialChar(value);
                            }
                            
                            filterQueryBuilder = getStringQueryBuilder(String.join("", prefix, value, suffix), field);
                        }
                    }

                    if (enforceOrg && filter.getField().equals("sysOrg") && 
                            (filter.getCondition().equalsIgnoreCase(QueryFilter.EQUALS)||filter.getCondition().equalsIgnoreCase(QueryFilter.NOT_EXISTS)))
                    {
                    	queryBuilder.filter(filterQueryBuilder);
                    }
                    else if (QueryDTO.OR_OPERATOR.equalsIgnoreCase(operator))
                    {
                        queryBuilder.should(filterQueryBuilder);
                    }
                    else
                    {
                        if (isNotQuery)
                        {
                            queryBuilder.mustNot(filterQueryBuilder);
                        }
                        else
                        {
                            if (filterQueryBuilder != null)
                            {
                                queryBuilder.must(filterQueryBuilder);
                            }
                        }
                    }
                }
            }

            if (foundFilter)
            {
                result = queryBuilder;
            }
            else
            {
                result = matchAllQuery();
            }
        }

        return result;
    }

    public static QueryBuilder createDateRangeQueryBuilder(QueryFilter filter)
    {
        QueryBuilder filterQueryBuilder = null;

        if (filter != null && filter.isDateType())
        {
            String field = filter.getField();
            String value = filter.getValue();

            Date startDate = null;
            Date endDate = null;

            // Date is either a string indicating relative date or a formatted
            // date
            if (StringUtils.isNotBlank(value))
            {
                if (value.equalsIgnoreCase("today"))
                {
                    startDate = DateUtils.GetUTCDate().toDate();
                    endDate = DateUtils.GetUTCDate().plusDays(1).toDate();
                }
                else if (value.equalsIgnoreCase("tomorrow"))
                {
                    startDate = DateUtils.GetUTCDate().plusDays(1).toDate();
                    endDate = DateUtils.GetUTCDate().plusDays(2).toDate();
                }
                else if (value.equalsIgnoreCase("yesterday"))
                {
                    startDate = DateUtils.GetUTCDate().minusDays(1).toDate();
                    endDate = DateUtils.GetUTCDate().toDate();
                }
                else if (value.equalsIgnoreCase("lastWeek"))
                {
                    startDate = DateUtils.GetUTCDate().minusDays(7).toDate();
                    endDate = DateUtils.GetUTCDate().minusDays(6).toDate();
                }
                else if (value.equalsIgnoreCase("lastMonth"))
                {
                    startDate = DateUtils.GetUTCDate().minusMonths(1).toDate();
                    endDate = DateUtils.GetUTCDate().minusMonths(1).plusDays(1).toDate();
                }
                else if (value.equalsIgnoreCase("lastYear"))
                {
                    startDate = DateUtils.GetUTCDate().minusYears(1).toDate();
                    endDate = DateUtils.GetUTCDate().minusYears(1).plusDays(1).toDate();
                }
                else
                {
                    startDate = DateUtils.parseISODate(value).toDate();
                    endDate = DateUtils.parseISODate(value).plusDays(1).toDate();
                    Calendar calendar = GregorianCalendar.getInstance();
                    calendar.setTime(startDate);
                    if (calendar.get(Calendar.HOUR) > 0 || calendar.get(Calendar.MINUTE) > 0 || calendar.get(Calendar.SECOND) > 0)
                    {
                        endDate = startDate;
                    }
                }
            }

            if (QueryFilter.BETWEEN.equals(filter.getCondition()))
            {
            	startDate = DateUtils.parseISODate(filter.getStartValue()).toDate(); 
            	endDate = DateUtils.parseISODate(filter.getEndValue()).toDate();
            	if (Constants.RANGE_QUERY_FILTER_NO_TIMEZONE.equalsIgnoreCase(filter.getParamName()))
            	{
            		filterQueryBuilder = rangeQuery(field)
						.gte(startDate.getTime())
						.lte(endDate.getTime())
						.format("epoch_millis");
            	}
            	else
            	{
            		filterQueryBuilder = rangeQuery(field)
						.gte(startDate.getTime())
						.lte(endDate.getTime())
						.timeZone(DateTimeZone.UTC.toString())
						.format("epoch_millis");
            	}
            }
            else if (QueryFilter.ON.equals(filter.getCondition()))
            {
                filterQueryBuilder = rangeQuery(field).from(startDate.getTime()).to(endDate.getTime()).includeLower(true).includeUpper(true);
            }
            else if (QueryFilter.BEFORE.equals(filter.getCondition()))
            {
                filterQueryBuilder = rangeQuery(field).from(0).to(startDate.getTime()).includeLower(true).includeUpper(false);
            }
            else if (QueryFilter.AFTER.equals(filter.getCondition()))
            {
                filterQueryBuilder = rangeQuery(field).from(startDate.getTime()).includeLower(false).includeUpper(true);
            }
            else if (QueryFilter.GREATER_THAN.equals(filter.getCondition()))
            {
                filterQueryBuilder = rangeQuery(field).from(startDate.getTime()).to(Long.MAX_VALUE).includeLower(false).includeUpper(true);
            }
            else if (QueryFilter.GREATER_THAN_OR_EQUAL.equals(filter.getCondition()))
            {
                filterQueryBuilder = rangeQuery(field).from(startDate.getTime()).to(Long.MAX_VALUE).includeLower(true).includeUpper(true);
            }
            else if (QueryFilter.LESS_THAN.equals(filter.getCondition()))
            {
                filterQueryBuilder = rangeQuery(field).from(0).to(startDate.getTime()).includeLower(true).includeUpper(false);
            }
            else if (QueryFilter.LESS_THAN_OR_EQUAL.equals(filter.getCondition()))
            {
                filterQueryBuilder = rangeQuery(field).from(0).to(startDate.getTime()).includeLower(true).includeUpper(true);
            }
        }
        return filterQueryBuilder;
    }

    public static QueryBuilder createDateRangeFilterBuilder(QueryFilter filter)
    {
       QueryBuilder filterQueryBuilder = null;

        if (filter != null && filter.isDateType())
        {
            String field = filter.getField();
            String value = filter.getValue();

            Date startDate = null;
            Date endDate = null;

            // Date is either a string indicating relative date or a formatted
            // date
            if (StringUtils.isNotBlank(value))
            {
                if (value.equalsIgnoreCase("today"))
                {
                    startDate = DateUtils.GetUTCDate().toDate();
                    endDate = DateUtils.GetUTCDate().plusDays(1).toDate();
                }
                else if (value.equalsIgnoreCase("tomorrow"))
                {
                    startDate = DateUtils.GetUTCDate().plusDays(1).toDate();
                    endDate = DateUtils.GetUTCDate().plusDays(2).toDate();
                }
                else if (value.equalsIgnoreCase("yesterday"))
                {
                    startDate = DateUtils.GetUTCDate().minusDays(1).toDate();
                    endDate = DateUtils.GetUTCDate().toDate();
                }
                else if (value.equalsIgnoreCase("lastWeek"))
                {
                    startDate = DateUtils.GetUTCDate().minusDays(7).toDate();
                    endDate = DateUtils.GetUTCDate().minusDays(6).toDate();
                }
                else if (value.equalsIgnoreCase("lastMonth"))
                {
                    startDate = DateUtils.GetUTCDate().minusMonths(1).toDate();
                    endDate = DateUtils.GetUTCDate().minusMonths(1).plusDays(1).toDate();
                }
                else if (value.equalsIgnoreCase("lastYear"))
                {
                    startDate = DateUtils.GetUTCDate().minusYears(1).toDate();
                    endDate = DateUtils.GetUTCDate().minusYears(1).plusDays(1).toDate();
                }
                else
                {
                    startDate = DateUtils.parseISODate(value).toDate();
                    endDate = DateUtils.parseISODate(value).plusDays(1).toDate();
                    Calendar calendar = GregorianCalendar.getInstance();
                    calendar.setTime(startDate);
                    if (calendar.get(Calendar.HOUR) > 0 || calendar.get(Calendar.MINUTE) > 0 || calendar.get(Calendar.SECOND) > 0)
                    {
                        endDate = startDate;
                    }
                }
            }

            if (QueryFilter.BETWEEN.equals(filter.getCondition()))
            {
                startDate = DateUtils.parseISODate(filter.getStartValue()).toDate();
                endDate = DateUtils.parseISODate(filter.getEndValue()).toDate();
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(startDate.getTime()).to(endDate.getTime()).includeLower(true).includeUpper(true);
            }
            else if (QueryFilter.ON.equals(filter.getCondition()))
            {
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(startDate.getTime()).to(endDate.getTime()).includeLower(true).includeUpper(true);
            }
            else if (QueryFilter.BEFORE.equals(filter.getCondition()))
            {
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(0).to(startDate.getTime()).includeLower(true).includeUpper(false);
            }
            else if (QueryFilter.AFTER.equals(filter.getCondition()))
            {
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(startDate.getTime()).to(Long.MAX_VALUE).includeLower(false).includeUpper(true);
            }
            else if (QueryFilter.GREATER_THAN.equals(filter.getCondition()))
            {
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(startDate.getTime()).to(Long.MAX_VALUE).includeLower(false).includeUpper(true);
            }
            else if (QueryFilter.GREATER_THAN_OR_EQUAL.equals(filter.getCondition()))
            {
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(startDate.getTime()).to(Long.MAX_VALUE).includeLower(true).includeUpper(true);
            }
            else if (QueryFilter.LESS_THAN.equals(filter.getCondition()))
            {
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(0).to(startDate.getTime()).includeLower(true).includeUpper(false);
            }
            else if (QueryFilter.LESS_THAN_OR_EQUAL.equals(filter.getCondition()))
            {
                filterQueryBuilder = QueryBuilders.rangeQuery(field).from(0).to(startDate.getTime()).includeLower(true).includeUpper(true);
            }
        }
        return filterQueryBuilder;
    }

    /**
     * Check the WikiDocument and ActionTask classes to see the usage of the
     * StripHTML annotation.
     * 
     * @param object
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static Serializable cleanContent(Serializable object) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        List<Field> fields = BeanUtil.getAllFields(null, object.getClass());
        StripHTML stripHtml = object.getClass().getAnnotation(StripHTML.class);

        if (stripHtml != null && stripHtml.value())
        {
            for (Field field : fields)
            {
                StripHTML stripHtmlField = field.getAnnotation(StripHTML.class);
                if (field.getType().equals(String.class) && (stripHtmlField == null || stripHtmlField.value()))
                {
                    String value = BeanUtil.getStringProperty(object, field.getName());
                    if (StringUtils.isNotBlank(value))
                    {
                        value = cleanHtml(value);
                        BeanUtil.setProperty(object, field.getName(), value);
                    }
                }
            }
        }
        return object;
    }

    private static final String cleanHtml(String str)
    {
        String result = null;
        if (StringUtils.isNotBlank(str))
        {
            // result = Jsoup.clean(str, Whitelist.simpleText());
            // will make sure that the whitespace are there
            result = Jsoup.clean(str, "", Whitelist.none(), outputSettings);
        }
        return result;
    } // htmlCleaner

    /**
     * Prepare a Tag object to be used during creation.
     * 
     * @param name
     * @param createdBy
     * @return
     */
    public static Tag prepareTag(String sysOrg, String name, String createdBy)
    {
        Tag tag = new Tag();
        // tag.setSysId(new Guid().toString());
        tag.setSysOrg(sysOrg);
        tag.setName(name);
        tag.setSysCreatedBy(createdBy);
        tag.setSysCreatedOn(DateUtils.GetUTCDateLong());
        tag.setSysUpdatedBy(createdBy);
        tag.setSysUpdatedOn(tag.getSysCreatedOn());
        return tag;
    }

    /**
     * Prepares a SocialPost object to be used during creation.
     * 
     * @param sysOrg
     * @param content
     * @param createdBy
     * @return
     */
    public static SocialPost prepareSocialPost(String sysOrg, String content, User user)
    {
        long time = DateUtils.GetUTCDateLong();

        SocialPost post = new SocialPost();
        post.setSysId(Guid.getGuid());
        post.setContent(content);
        post.setSysCreatedOn(time);
        post.setSysUpdatedOn(time);

        if (user != null)
        {
            post.setAuthorSysId(user.getSys_id());
            post.setAuthorUsername(user.getName());
            post.setAuthorDisplayName(user.getDisplayName());
            post.setSysCreatedBy(user.getName());
            post.setSysUpdatedBy(user.getName());
        }
        else
        {
            // there may be a post/comment by a user which may be deleted and
            // does not exist anymore
            post.setAuthorSysId("not_available");
            post.setAuthorUsername("not_available");
            post.setAuthorDisplayName("Not Available");
            post.setSysCreatedBy("not_available");
            post.setSysUpdatedBy("not_available");
        }

        return post;
    }

    /**
     * Prepares a SocialComment object to be used during creation.
     * 
     * @param sysOrg
     * @param postId
     * @param postAuthorUsername
     * @param content
     * @param createdBy
     * @return
     */
    public static SocialComment prepareSocialComment(String sysOrg, String postId, String postAuthorUsername, String content, User user)
    {
        SocialComment comment = new SocialComment();
        comment.setSysId(Guid.getGuid());
        comment.setSysOrg(sysOrg);
        comment.setPostId(postId);
        comment.setPostAuthorUsername(postAuthorUsername);
        comment.setContent(content);
        comment.setSysCreatedOn(DateUtils.GetUTCDateLong());
        comment.setSysUpdatedOn(comment.getSysCreatedOn());

        if (user != null)
        {
            comment.setCommentAuthorSysId(user.getSys_id());
            comment.setCommentAuthorUsername(user.getName());
            comment.setCommentAuthorDisplayName(user.getDisplayName());
            comment.setSysCreatedBy(user.getSys_id());
            comment.setSysUpdatedBy(user.getSys_id());
        }
        else
        {
            // there may be a post/comment by a user which may be deleted and
            // does not exist anymore
            comment.setCommentAuthorSysId("not_available");
            comment.setCommentAuthorUsername("not_available");
            comment.setCommentAuthorDisplayName("Not Available");
            comment.setSysCreatedBy("not_available");
            comment.setSysUpdatedBy("not_available");
        }

        return comment;
    }

    public static GlobalQuery prepareGlobalQuery(String query, int weight, UserInfo userInfo)
    {
        GlobalQuery globalQuery = new GlobalQuery();
        globalQuery.setSysId(Guid.getGuid());
        globalQuery.setSysOrg(userInfo.getSysOrg());
        globalQuery.setSysCreatedBy(userInfo.getUsername());
        globalQuery.setSysCreatedOn(DateUtils.GetUTCDateLong());
        globalQuery.setSysUpdatedBy(userInfo.getUsername());
        globalQuery.setSysUpdatedOn(globalQuery.getSysCreatedOn());

        globalQuery.setQuery(query);
        return globalQuery;
    }

    public static UserQuery prepareUserQuery(String query, String type, int weight, UserInfo userInfo)
    {
        UserQuery userQuery = new UserQuery();
        userQuery.setSysId(Guid.getGuid());
        userQuery.setSysOrg(userInfo.getSysOrg());
        userQuery.setSysCreatedBy(userInfo.getUsername());
        userQuery.setSysCreatedOn(DateUtils.GetUTCDateLong());
        userQuery.setSysUpdatedBy(userInfo.getUsername());
        userQuery.setSysUpdatedOn(userQuery.getSysCreatedOn());

        userQuery.setQuery(query);
        userQuery.setType(type);
        userQuery.setUserSysId(userInfo.getSysId());
        userQuery.setUsername(userInfo.getUsername());
        userQuery.setWeight(weight);
        return userQuery;
    }

    /**
     * This method returns a {@link TreeSet} with various roles from a string.
     * The roleString must be a space delimited {@link String}. We'll have to
     * make sure that the rule is not violated during any refactoring. However,
     * If the rule is changed then we have to make appropriate changes here as
     * well.
     * 
     * @param roleString
     * @return
     */
    public static TreeSet<String> parseRoles(String roleString)
    {
        return parseCSV(roleString, " ");
    }

    /**
     * This method returns a {@link TreeSet} with various roles from a string.
     * 
     * @param roleString
     * @param delimeter
     * @return a {@link TreeSet} which is important to keep the roles in their
     *         natural order.
     * @throws Exception
     */
    public static TreeSet<String> parseRoles(String roleString, String delimeter)
    {
        // Important note: user.getRoles() gurantees that the roles will be
        // delimited by a space, make sure it doesn't change this rule
        // abruptly. If it does then make sure to change this code.
        return parseCSV(roleString, delimeter);
    }

    public static TreeSet<String> parseTags(String tagString)
    {
        return parseCSV(tagString, ",");
    }

    public static TreeSet<String> parseCSV(String cvs, String delimeter)
    {
        TreeSet<String> result = new TreeSet<String>();

        if (StringUtils.isNotBlank(cvs))
        {
            List<String> items = StringUtils.convertStringToList(cvs, delimeter);
            for (String item : items)
            {
                result.add(item.trim());
            }
        }
        return result;
    }

    /**
     * Finds tag(s) and return as a {@link Set}.
     * 
     * @param content
     * @return
     */
    /*
     * public static Set<String> findTags1(final String content) { Set<String>
     * hashTags = new HashSet<String>(); if(StringUtils.isNotBlank(content)) {
     * //get rid of html tags (e.g., <div> etc.) String htmlFreeContent =
     * Jsoup.parse(content).text(); Matcher matcher =
     * TAG_PATTERN.matcher(htmlFreeContent); while (matcher.find()) { String tag
     * = matcher.group(); Log.log.trace("Found hashtag : " + tag);
     * hashTags.add(tag.trim().replaceFirst("#", "").trim()); } } return
     * hashTags; }
     */

    /**
     * This method extracts tags from a string.
     * 
     * @param content
     * @return
     */
    public static Set<String> findTags(final String content)
    {
        Set<String> result = new HashSet<String>();
        if (StringUtils.isNotBlank(content))
        {
            // get rid of html tags (e.g., <div> etc.)
            String cleanContent = Jsoup.parse(content).text();

            StringBuilder tag = new StringBuilder();
            boolean started = false;
            boolean ended = true;
            for (int i = 0; i < cleanContent.length(); i++)
            {
                char ch = cleanContent.charAt(i);
                if (ch == '#' && ended)
                {
                    started = true;
                    ended = false;
                }
                else
                {
                    if (started)
                    {
                        if (isAllowed(ch))
                        {
                            tag.append(ch);
                        }
                        else
                        {
                            if (isTerminator(ch) && tag.length() > 0)
                            {
                                result.add(tag.toString());
                            }
                            tag.setLength(0);
                            started = false;
                            ended = true;
                        }
                    }
                }
            }

            // this is for the last one
            if (tag.length() > 0)
            {
                result.add(tag.toString());
            }
        }
        return result;
    }

    /**
     * Only alpha numeric and underscore allowed.
     * 
     * @param ch
     * @return
     */
    private static boolean isAllowed(char ch)
    {
        boolean result = false;
        if (ch == '_' || (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9'))
        {
            result = true;
        }
        return result;
    }

    /**
     * Detect if a character is a terminator. If there is more to be added then
     * add it here.
     * 
     * Currently supported characters: 3 - End of Text 9 - Horizontal Tab 10 -
     * Line Feed 12 - Form feed 13 - Carriage return 32 - Space
     * 
     * @param ch
     * @return
     */
    private static boolean isTerminator(char ch)
    {
        boolean result = false;
        if (ch == 3 || ch == 9 || ch == 10 || ch == 12 || ch == 13 || ch == 32)
        {
            result = true;
        }
        return result;
    }

    public static QueryBuilder createTermQueryBuilder(String searchTerm)
    {
        QueryBuilder queryBuilder = QueryBuilders.fuzzyQuery("_source", searchTerm);
        return queryBuilder;
    }

    public static QueryBuilder createTermQueryBuilder(QueryFilter queryFilter)
    {
        QueryBuilder queryBuilder = null;
        if (StringUtils.isNotBlank(queryFilter.getField()) && StringUtils.isNotBlank(queryFilter.getValue()))
        {
            queryBuilder = QueryBuilders.termQuery(queryFilter.getField(), queryFilter.getValue());
        }
        return queryBuilder;
    }

    public static QueryBuilder createTermsQueryBuilder(QueryFilter queryFilter)
    {
        QueryBuilder queryBuilder = null;
        if (StringUtils.isNotBlank(queryFilter.getField()) && StringUtils.isNotBlank(queryFilter.getValue()))
        {
            String value = queryFilter.getValue();
            if(hasSpecialChar(value))
            {
                //got to escape them
                value = escapeLuceneSpecialChar(value);
            }

            queryBuilder = QueryBuilders.termsQuery(queryFilter.getField(), parseCSV(value, ","));
        }
        return queryBuilder;
    }

    public static TermsQueryBuilder createTermsFilterBuilder(QueryFilter queryFilter)
    {
        TermsQueryBuilder queryBuilder = null;
        if (StringUtils.isNotBlank(queryFilter.getField()) && StringUtils.isNotBlank(queryFilter.getValue()))
        {
            String value = queryFilter.getValue();
            if(hasSpecialChar(value))
            {
                //got to escape them
                value = escapeLuceneSpecialChar(value);
            }

            queryBuilder = QueryBuilders.termsQuery(queryFilter.getField(), parseCSV(value, ","));
        }
        return queryBuilder;
    }

    public static TermQueryBuilder createTermFilterBuilder(QueryFilter queryFilter)
    {
        TermQueryBuilder queryBuilder = null;
        if (StringUtils.isNotBlank(queryFilter.getField()) && StringUtils.isNotBlank(queryFilter.getValue()))
        {
            String value = queryFilter.getValue();
            if(hasSpecialChar(value))
            {
                //got to escape them
                value = escapeLuceneSpecialChar(value);
            }

            queryBuilder = QueryBuilders.termQuery(queryFilter.getField(), value);
        }
        return queryBuilder;
    }
    
    public static QueryBuilder createNumericRangeQueryBuilder(QueryFilter filter)
    {
        QueryBuilder result = null;

        if (filter != null && filter.isNumericType())
        {
            String field = filter.getField();
            String condition = filter.getCondition();
            String value = filter.getValue();
            if (value != null & NumberUtils.isCreatable(value))
            {
                if (QueryFilter.GREATER_THAN.equalsIgnoreCase(condition))
                {
                    result = QueryBuilders.rangeQuery(field).gt(value);
                }
                else if (QueryFilter.GREATER_THAN_OR_EQUAL.equalsIgnoreCase(condition))
                {
                    result = QueryBuilders.rangeQuery(field).gte(value);
                }
                else if (QueryFilter.LESS_THAN_OR_EQUAL.equalsIgnoreCase(condition))
                {
                    result = QueryBuilders.rangeQuery(field).lte(value);
                }
                else if (QueryFilter.LESS_THAN.equalsIgnoreCase(condition))
                {
                    result = QueryBuilders.rangeQuery(field).lt(value);
                }
            }
            else if (QueryFilter.BETWEEN.equalsIgnoreCase(condition))
            {
                String startValue = filter.getStartValue();
                String endValue = filter.getEndValue();
                if (startValue != null && NumberUtils.isCreatable(startValue) && 
                	endValue != null && NumberUtils.isCreatable(endValue))
                {
                    result = QueryBuilders.rangeQuery(field).from(startValue).to(endValue);
                }
            }
        }
        return result;
    }
    
    public static Map<String, String> getDelta(String oldText, String newText)
    {
        Map<String, String> result = new HashMap<String, String>();

        diff_match_patch dmp = new diff_match_patch();

        LinkedList<Diff> diffs = dmp.diff_main(oldText, newText);
        if (diffs.size() > 0)
        {
            for (Diff diff : diffs)
            {
                switch (diff.operation)
                {
                    case INSERT:
                        System.out.println("insert:" + diff.text);
                        break;
                    case DELETE:
                        System.out.println("delete:" + diff.text);
                        break;
                    default:
                        break;
                }
            }
        }
        /*
         * boolean isDifferent = dmp.diff_levenshtein(diffs) > 0;
         * 
         * if (isDifferent) { LinkedList<Patch> patches =
         * dmp.patch_make(oldText, newText, false);
         * //System.out.println(dmp.patch_toText(patches)); int i = 0; for
         * (Patch patch : patches) { System.out.println(patch.toString() +
         * " >>>> " + i++); }
         * 
         * for (Diff diff : diffs) { // System.out.println(diff.text + "," +
         * diff.operation); } }
         */
        return result;
    }

    /**
     * Generates a JSON string similar to this:
     * 
     * { "documentquery" : { "_all" : { "enabled" : false }, "properties": {
     * "documentSuggest": { "type" : "completion", "analyzer" : "simple",
     * "search_analyzer" : "simple", "preserve_position_increments": false,
     * "preserve_separators": false }, "weight": { "type":"integer",
     * "null_value":0 }, "selectCount": { "type":"integer", "null_value":0 },
     * "sysUpdatedOn": { "type":"long", "null_value":0 }, "suggestion": {
     * "type":"string", "analyzer" : "standard" }, "sourceIds": {
     * "type":"string", "index" : "not_analyzed" } } } }
     * 
     * @param documentType
     * @param suggestFieldName
     * @return
     */
    public static String suggestionMapping(String documentType, String suggestFieldName)
    {
        StringBuilder result = new StringBuilder();

        result.append("{");
        result.append("\"" + documentType + "\" :");
        result.append("{");
        result.append("\"_all\" :");
        result.append("{");
        result.append("\"enabled\" : false");
        result.append("},");
        result.append("\"properties\":");
        result.append("{");
        result.append("\"" + suggestFieldName + "\":");
        result.append("{");
        result.append("\"type\" : \"completion\",");
        result.append("\"analyzer\" : \"simple\",");
        result.append("\"search_analyzer\" : \"simple\",");
        result.append("\"preserve_position_increments\": false,");
        result.append("\"preserve_separators\": false");
        result.append("},");
        result.append("\"weight\":");
        result.append("{");
        result.append("\"type\":\"integer\",");
        result.append("\"null_value\":0");
        result.append("},");
        result.append("\"selectCount\":");
        result.append("{");
        result.append("\"type\":\"integer\",");
        result.append("\"null_value\":0");
        result.append("},");
        result.append("\"sysUpdatedOn\":");
        result.append("{");
        result.append("\"type\":\"long\",");
        result.append("\"null_value\":0");
        result.append("},");
        result.append("\"suggestion\":");
        result.append("{");
        result.append("\"type\":\"string\",");
        result.append("\"analyzer\" : \"keyword_lowercase_analyzer\"");
        result.append("},");
        result.append("\"sourceIds\":");
        result.append("{");
        result.append("\"type\":\"string\",");
        result.append("\"index\" : \"not_analyzed\"");
        result.append("}");
        result.append("}");
        result.append("}");
        result.append("}");

        return result.toString();
    }

    public static BulkProcessor getBulkProcessor(Client client, int bulkSize, int threads, int flushInterval)
    {
        BulkProcessor bulkProcessor = BulkProcessor.builder(client, new BulkProcessor.Listener()
        {
            @Override
            public void beforeBulk(long executionId, BulkRequest request)
            {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("Going to execute new bulk composed of " + request.numberOfActions() + " actions.");
                }
            }

            @Override
            public void afterBulk(long executionId, BulkRequest request, BulkResponse response)
            {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("Executed bulk composed of {} " + request.numberOfActions() + " actions.");
                }
            }

            @Override
            public void afterBulk(long executionId, BulkRequest request, Throwable failure)
            {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("Error executing bulk", failure);
                }
            }
        }).setBulkActions(bulkSize).setFlushInterval(TimeValue.timeValueSeconds(flushInterval)).build();

        return bulkProcessor;
    }

    /**
     * Finds if a query is in simple query string query format.
     * 
     * Check:
     * http://www.elasticsearch.org/guide/en/elasticsearch/reference/current
     * /query-dsl-simple-query-string-query.html
     * 
     * @param query
     * @return
     */
    public static boolean isSimpleQueryString(String query)
    {
        return query.matches(ES_SIMPLE_QUERY_REGEX);
    }

    public static boolean hasSpecialChar(String value)
    {
        return value.matches(LUCENE_SPECIAL_CHAR);
    }

    /**
     * This method escapes various sensitive lucene special characters from a query.
     * So far following characters need to be escaped:
     * 
     * !(){}[]^"~:\
     * 
     * @param value
     * @return
     */
    public static String escapeLuceneSpecialChar(String value)
    {
        return value.replaceAll(LUCENE_SPECIAL_CHAR_REPL, "\\\\$1");
    }

    private static String removeLuceneSpecialChar(String value)
    {
        return value.replaceAll(LUCENE_SPECIAL_CHAR_REPL, "");
    }
    
	public static String escapeAllLuceneSpecialChars(String value) {
		return value.replaceAll(LUCENE_SPECIAL_CHARS_FULLTEXT, "\\\\$0");
	}

    public static String readJson(String fileName)
    {
        InputStream stream = SearchUtils.class.getResourceAsStream("/" + fileName);
        return StringUtils.toString(stream, "UTF-8");
    }

    public static String prettyPrint(String json)
    {
        String result = "";
        try
        {
            if (json != null)
            {
                result = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            }
        }
        catch (Exception e)
        {
            // ignore
        }
        return result;
    }

    /**
     * Reads mapping version from JSON source.
     * 
     * @param mappingJson
     * @param documentType
     * @return
     */
    public static String readMappingVersion(String mappingJson, String documentType)
    {
        String result = null;
        if (StringUtils.isNotBlank(mappingJson))
        {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj;
            try
            {
                actualObj = mapper.readValue(mappingJson, JsonNode.class);
                JsonNode taskResultData = actualObj.get(documentType);
                JsonNode metaData = taskResultData.get("_meta");
                if (metaData != null)
                {
                    JsonNode fieldData = metaData.get("version");
                    result = fieldData.getTextValue();
                    Log.log.debug("Existing mapping version for : " + documentType + "=" + result);
                }
            }
            catch (JsonParseException e)
            {
                Log.log.error(e.getMessage(), e);
            }
            catch (JsonMappingException e)
            {
                Log.log.error(e.getMessage(), e);
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    /**
     * Builds a QueryBuilder from JSON string.
     * 
     * @param source
     * @return
     */
    public static QueryBuilder fromSource(String source)
    {
        QueryBuilder result = null;
        if(StringUtils.isNotBlank(source))
        {
        	result = QueryBuilders.wrapperQuery(source);
        	
//            byte[] data = source.getBytes();
//            int offset = 0;
//            int length = data.length;
//            final Map<String, Object> sourceMap = XContentHelper.convertToMap(data, offset, length, true).v2();
//            result = new BaseQueryBuilder()
//            {
//                @Override
//                protected void doXContent(XContentBuilder builder, Params params) throws IOException
//                {
//                    for (Map.Entry<String, Object> entry : sourceMap.entrySet())
//                    {
//                        builder.field(entry.getKey());
//                        Object value = entry.getValue();
//                        if (value == null)
//                        {
//                            builder.nullValue();
//                        }
//                        else
//                        {
//                            builder.value(value);
//                        }
//                    }
//                }
//            };
        }
        return result;
    }
    
    /**
     * 
     * @param index comes as like worksheet, processrequest, taskresult
     * @return
     */
    public static String getDynamicIndexName(String index, long timeInMillis)
    {
        return getDynamicIndexName(index, new DateTime(timeInMillis));
    }
    
    /**
     * 
     * @param index comes as like worksheet, processrequest, taskresult
     * @return
     */
    public static String getDynamicIndexName(String index, DateTime dateTime)
    {
        return index + "_" +  dateTime.toString(DATE_FORMAT);
    }
    
    public static String getSyncLogIndexName(String index, String documentType)
    {
        if(SYNC_LOGS.containsKey(index))
        {
            return SYNC_LOGS.get(index);
        }
        else
        {
            return SYNC_LOGS.get(documentType);
        }
    }
    
    public static String createJson(final Serializable object) throws SearchException
    {
        String result = null;
        if (object != null)
        {
            try
            {
                // clean any String fields from HTML tags and then generate json
                Serializable tmpObject = cleanContent(object);
                result = MAPPER.writeValueAsString(tmpObject);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new SearchException(e.getMessage(), e);
            }
        }
        return result;
    }

    public static String createJson(Map<String, Object> object) throws SearchException
    {
        String result = null;
        try
        {
            // generate json
            result = MAPPER.writeValueAsString(object);
        }
        catch (JsonGenerationException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (JsonMappingException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }

        return result;
    }
    
    private static QueryStringQueryBuilder getStringQueryBuilder(String query, String field) {
    	boolean hasSpecialCommand = Arrays.stream(SPECIAL_SEARCH_COMMANDS).parallel().anyMatch(query::contains);
    	
        /*
	     * In query_string, if the search string has a space and the property data type is keyword,
	     * the space had to be escaped to be searched by full text search. This escaping is not needed
	     * if the data type is text. But it works on both.
	     */
        query = hasSpecialCommand ? query : query.replace(" ", "\\ ");
        // check for the list of fields
        if (StringUtils.isNotEmpty(field) && field.contains(",")) {
        	// prepare a map of fields with boosting factors
			Map<String, Float> fields = Pattern.compile(",").splitAsStream(field)
					.map(s -> s.trim().split("\\^", 2)).collect(Collectors.toMap(a -> a[0],
							a -> a.length > 1 ? Float.parseFloat(a[1]) : 1));

			return queryStringQuery(query).allowLeadingWildcard(true).fields(fields);
        } else {
        	return queryStringQuery(query).allowLeadingWildcard(true).field(field);
        }
    }
}