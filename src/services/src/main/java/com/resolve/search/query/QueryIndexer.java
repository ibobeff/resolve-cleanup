/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.query;

import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.core.StopAnalyzer;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.google.common.base.Joiner;
import com.resolve.search.ConfigSearch;
import com.resolve.search.IndexAPI;
import com.resolve.search.IndexData;
import com.resolve.search.IndexData.OPERATION;
import com.resolve.search.IndexListener;
import com.resolve.search.RateIndexer;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.dictionary.DictionaryIndexer;
import com.resolve.search.model.Word;
import com.resolve.search.model.query.Query;
import com.resolve.search.model.query.QueryFactory;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

public class QueryIndexer implements RateIndexer<IndexData<String>>
{
    private final IndexAPI indexAPI;
    private final SearchAPI<? extends Query> searchAPI;

    private static final String DELIMETER = "[.?!:;, ]";
    private static final Joiner joiner = Joiner.on(' ').skipNulls();

    private final ConfigSearch config;
    private long indexThreads;
    private final String dictionarySource;
    private final DictionaryIndexer dictionaryIndexer;

    // some consumer may not like the content
    // to be tokenized (e.g., Wiki name)
    private final boolean tokenize;

    private ResolveConcurrentLinkedQueue<IndexData<String>> dataQueue = null;

    public QueryIndexer(final ConfigSearch config, final IndexAPI indexAPI, final SearchAPI<? extends Query> searchAPI, final String dictionarySource, final boolean tokenize)
    {
        this.config = config;
        this.indexAPI = indexAPI;
        this.searchAPI = searchAPI;
        this.dictionarySource = dictionarySource;
        this.tokenize = tokenize;

        this.dictionaryIndexer = new DictionaryIndexer(config, dictionarySource);
        this.indexThreads = this.config.getIndexthreads();

        QueueListener<IndexData<String>> queryIndexListener = new IndexListener<IndexData<String>>(this);
        dataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(queryIndexListener);
    }

    public boolean enqueue(IndexData<String> indexData)
    {
        return dataQueue.offer(indexData);
    }

    @Override
    public boolean index(IndexData<String> indexData)
    {
        OPERATION operation = indexData.getOperation();
        Collection<String> queries = indexData.getObjects();
        try
        {
            switch (operation)
            {
                case INDEX:
                    indexQueries(indexData, false);
                    break;
                case RATE:
                    Long lastUpdatedOn = 0L;
                    // only one item must come which is the lastUpdatedOn
                    // time in millis
                    if (!queries.isEmpty())
                    {
                        try
                        {
                            lastUpdatedOn = Long.parseLong(queries.iterator().next());
                        }
                        catch (NumberFormatException e)
                        {
                            // bad caller, unbelievable! ignore and go with the
                            // default 0.
                        }
                    }
                    updateRating(lastUpdatedOn, indexData.getUsername());
                    break;
                case PURGE:
                    purgeSearchQueries(queries, indexData.getUsername());
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return true;
    }

    private void purgeSearchQueries(Collection<String> words, String username) throws SearchException
    {
        if (words == null || words.size() == 0)
        {
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            indexAPI.deleteByQueryBuilder(queryBuilder, username);
        }
        else
        {
            indexAPI.deleteDocumentByIds(words, username);
        }
    }

    /**
     * Checks if it's part of a stopwords (ignorable by Resolve)
     * 
     * @param token
     * @return
     */
    private boolean isStopword(String token)
    {
        return (token.isEmpty() || !StringUtils.isAlpha(token) || StopAnalyzer.ENGLISH_STOP_WORDS_SET.contains(token) || token.length() <= 1);
    }

    private boolean isAlreadyExists(String[] tokenArray, String token)
    {
        boolean result = false;
        for(int i=0; i<tokenArray.length; i++)
        {
            if(token.equalsIgnoreCase(tokenArray[i]))
            {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Collects phrases from the contents ignoring several words or patterns. This method will
     * ivolve overtime as we learn more about  
     * 
     * @param sourceId
     * @param contents
     * @param tokenSize
     * @param sysUpdatedOn
     * @param username
     * @return
     */
    private Collection<Query> tokenize(String sourceId, Collection<String> contents, int tokenSize, Long sysUpdatedOn, String username)
    {
        Map<String, Query> queries = new HashMap<String, Query>();
        Map<String, Word> dictionaryWords = new HashMap<String, Word>();

        for (String content : contents)
        {
            // clean it from any html tags
            content = StringUtils.htmlCleaner(content);

            // String[] strings = StringUtils.stringToArray(content,
            // "[.?!:;-\\\\(\\\\)'\"/, ]");
            String[] strings = StringUtils.stringToArray(content, DELIMETER);

            if (strings != null && strings.length > 0)
            {
                StringBuilder tokens = new StringBuilder();
                String[] tokenArray = new String[tokenSize];
                int tokenIndex = 0;                
                int i = 0;
                int tmpTokenSize = tokenSize;

                while (i < strings.length) {
                    String token = strings[i].trim().toLowerCase();
                    // ENGLISH_STOP_WORDS_SET is a Lucene specific set of words
                    // that ES/Lucene ignores during search
                    // because they are so common. Like The, To etc.
                    //
                    // Also do not add token with just 1 later.
                    //
                    if (!isStopword(token))
                    {
                        // add to the collection so it gets added to dictionary
                        if (dictionaryWords.containsKey(token))
                        {
                            dictionaryWords.get(token).addFrequency(dictionarySource, 1);
                        }
                        else
                        {
                            Word dictionaryWord = new Word(token, dictionarySource);
                            dictionaryWords.put(token, dictionaryWord);
                        }
                        tokens.setLength(0);
                        int j = i;
                        while (j < i + tmpTokenSize) 
                        {
                            try {
                                token = strings[j];
                                //we do not want to add duplicate in a phrase
                                if (isStopword(token) || isAlreadyExists(tokenArray, token))
                                {
                                    tmpTokenSize++;
                                } else {
                                    tokens.append(token);
                                    tokens.append(" ");
                                    tokenArray[tokenIndex++] = token;
                                }
                            } catch (IndexOutOfBoundsException e) {
                                // ignore;
                            }
                            j++;
                        }
                        putQuery(sourceId, queries, tokenArray, sysUpdatedOn);
                    }
                    i++;
                    tmpTokenSize = tokenSize;
                }
            }
        }

        // drop the words to be indexed
        if (dictionaryWords.size() > 0)
        {
            // this synchronized call is neccessary so the frequency of the
            // words are updated before we update the phrase weight based on word
            // frequency
            dictionaryIndexer.indexDictionaryWords(dictionaryWords.values(), username);
        }

        return queries.values();
    }

    private void putQuery(String sourceId, Map<String, Query> map, String[] tokenArray, Long sysUpdatedOn)
    {
        String phrase = joiner.join(tokenArray);
        Query query = null;
        if (map.containsKey(phrase))
        {
            query = map.get(phrase);
        }
        else
        {
            query = QueryFactory.createQuery(dictionarySource, phrase, 1);
            query.addSourceId(sourceId);
        }
        query.setSysUpdatedOn(sysUpdatedOn);
        map.put(phrase, query);
    }

    @Override
    public void indexQueries(IndexData<String> indexData, boolean updateRating) throws SearchException
    {
        indexQueries(indexData.getSourceId(), indexData.getObjects(), indexData.getSysUpdatedOn(), updateRating, indexData.getUsername());
    }

    @Override
    public void indexQueries(String sourceId, Collection<String> searchQueries, Long sysUpdatedOn, boolean updateRating, String username) throws SearchException
    {
        // Collection does not have subList method hence this conversion.
        // caller gurantees that the Collection is a List
        List<String> phrases = (List<String>) searchQueries;

        if (searchQueries != null && searchQueries.size() > 0)
        {
            int count = phrases.size();
            int fromIndex = 0;
            int toIndex = 10000;
            while (fromIndex < count)
            {
                if (toIndex > count)
                {
                    toIndex = count;
                }
                List<String> subList = phrases.subList(fromIndex, toIndex);
                if (subList != null && subList.size() > 0)
                {
                    Collection<Query> queries = null;
                    if (tokenize)
                    {
                        queries = tokenize(sourceId, subList, 3, sysUpdatedOn, username);
                    }
                    else
                    {
                        queries = new LinkedList<Query>();
                        for (String query : subList)
                        {
                            queries.add(QueryFactory.createQuery(dictionarySource, query, 1, sysUpdatedOn));
                        }
                    }

                    // at this point we are going to make sure that the
                    // frequency is updated if there is an existing phrase being
                    // updated with frequency
                    indexAPI.indexSuggester(queries, username);

                    if (updateRating)
                    {
                        updateRating(queries, username);
                    }
                }
                // sleep for 5 seconds if we have a lot of data
                // will improve this later.
                if (count > 10000)
                {
                    try
                    {
                        Thread.sleep(5000L);
                    }
                    catch (InterruptedException e)
                    {
                        // ignore
                    }
                }
                fromIndex = toIndex;
                toIndex += 10000;
            }
        }
    }

    /**
     * Updates the rating. Query object is expected to have selectCount
     * 
     * @param query
     * @param username
     * @throws SearchException
     */
    @Override
    public void updateRating(Query query, String username) throws SearchException
    {
        /* NOT USED ANYMORE
        String[] words = query.getSuggestion().split(" ");

        long weight = DictionarySearchAPI.getFrequency(dictionarySource, words, username);

        if (weight > 0)
        {
            long oldInMonth = query.getSysUpdatedOn() - SearchConstants.INCEPTION_DAY;
            if (oldInMonth < 0)
            {
                oldInMonth = query.getSysUpdatedOn();
            }
            oldInMonth = ((((oldInMonth / 1000) / 60) / 60) / 24) / 30;
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Updating '" + query.getSuggestion() + "' with word rating : " + weight + " months: " + oldInMonth + " and selectCount: " + query.getSelectCount());
            }

            // this oldInMonth will be multiplied by the rating, the terms
            // will decay as sysUpdatedOn gets old.
            weight = (weight * query.getSelectCount()) + oldInMonth;

            String script = "ctx._source." + query.getSuggestFieldName() + ".weight = weight; ctx._source.weight = weight";
            Map<String, Object> scriptParams = new HashMap<String, Object>();
            scriptParams.put("weight", weight);
            indexAPI.updateDocument(query.getSuggestion(), script, scriptParams, username);
        }
        */
    }

    /**
     * This method will check the frequency of the words found in the dictionary
     * and add them up for all the words found in each search query (phrase).
     * 
     * @param queries
     * @param username
     * @throws SearchException
     */
    private void updateRating(Collection<? extends Query> queries, String username) throws SearchException
    {
        for (Query query : queries)
        {
            updateRating(query, username);
        }
    }

    private void updateRating(final long lastUpdatedOn, final String username) throws SearchException
    {
        // chunk it
        long updatedOn = lastUpdatedOn;
        QueryDTO queryDTO = new QueryDTO(0, 1);
        QueryBuilder queryBuilder = rangeQuery(SearchConstants.SYS_UPDATED_ON).from(updatedOn).to(Long.MAX_VALUE).includeLower(false).includeUpper(true);
        int start = 0;
        int limit = 10000;
        long totalCount = searchAPI.getCountByQuery(queryBuilder);
        while (start < totalCount || (start + limit) < totalCount)
        {
            queryDTO = new QueryDTO(start, limit);
            queryBuilder = rangeQuery(SearchConstants.SYS_UPDATED_ON).from(updatedOn).to(DateUtils.GetUTCDateLong()).includeLower(false).includeUpper(true);
            queryDTO.addSortItem(new QuerySort(SearchConstants.SYS_UPDATED_ON, QuerySort.SortOrder.DESC));

            ResponseDTO<? extends Query> response = searchAPI.searchByQuery(queryDTO, queryBuilder, username);

            if (response.getRecords() != null && response.getRecords().size() > 0)
            {
                // set the last updated on from the record, since we sort DESC
                // we can rely on it
                updatedOn = response.getRecords().get(0).getSysUpdatedOn();
                updateRating(response.getRecords(), username);
            }
            start += limit;
        }
    }
}
