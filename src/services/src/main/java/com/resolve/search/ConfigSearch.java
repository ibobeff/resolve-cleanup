/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search;

import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.resolve.util.ConfigMap;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigSearch extends ConfigMap
{
    private static final long serialVersionUID = -6556833889694881298L;
    
    // Minimum duration in minutes to keep scroll context on server alive for without using it 
    public static final long MIN_SCROLL_CTX_KEEP_ALIVE_DURATION = 10l;
	private boolean active = false;
    private String cluster = "resolve";
    private String serverlist = "localhost";
    private int serverport = 9300;
    private boolean compress = true;
    private String clienttype = "transport";
    private int shards = 3;
    private int replicas = -1;
    private int replicaCount = 1;
    private int indexthreads = 3;
    private int indextimeout = 120; //in seconds
    private int indexretries = 3; //try 3 times on timeout
    private boolean http = true;
    private int httpport = 9200;

    //site to site replication config
    private boolean sync = false;
    private boolean syncTask = false;
    private int syncTaskInterval = 600; //10*60 = 10 minutes
    private String syncCluster = "resolve";
    private String syncServerlist = "localhost";
    private int syncServerport = 9300;
    private String syncExclude = "wikidocument,wikiattachment,actiontask"; //comma separated list of exclude index from sync
    private Set<String> syncExcludeSet = new HashSet<String>();
    
    private boolean removeOldLogFiles = false;
    private String cronTaskIntervalType = "DAYS";
    private int cronTaskIntervalValue = 30;
    private int fileDateThreshold = 30;

    private boolean detailCache = false;
    private int detailSize = 0;
    private int detailThreshold = 0;

    private boolean migration = false;
    private String migrationHost = "localhost";
    private int migrationPort = 9200; 
    
    
    private boolean searchGuard = false;
    private String sgKeystore = "";
    private String sgTruststore = "";
    private String sgKeystorePwd = "";
    private String sgTruststorePwd = "";
    // Elasticsearch bulkprocessor configurations
    private int concurrentRequests = 1;
    private int bulkActions = 1;
    private int flushInterval = 2000;
    private int bulkSize = 5000;
    
    // Maximum duration in minutes to keep scroll context alive for, without any use, on ES server
    private long scrollCtxKeepAliveDur = MIN_SCROLL_CTX_KEEP_ALIVE_DURATION;
    
    //in general comes from various system properties. do not attempt to
    //access PropertiesUtil from search classes as it may break when there
    //is no DB access from some component (e.g., RSRemote)
    private Map<String, String> properties = new HashMap<String, String>();
    
    // this is a TreeMap to preserve the sorting order.
    private final Map<Integer, String> servers = new TreeMap<Integer, String>();
    private final Map<Integer, String> syncServers = new TreeMap<Integer, String>();

    public ConfigSearch(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, "./SEARCH/@ACTIVE");
        define("serverlist", STRING, "./SEARCH/@SERVERLIST");
        define("serverport", INTEGER, "./SEARCH/@SERVERPORT");
        define("compress", BOOLEAN, "./SEARCH/@COMPRESS");
        define("cluster", STRING, "./SEARCH/@CLUSTER");
        define("clienttype", STRING, "./SEARCH/@CLIENTTYPE");
        define("shards", INTEGER, "./SEARCH/@SHARDS");
        define("replicas", INTEGER, "./SEARCH/@REPLICAS");
        define("indexthreads", INTEGER, "./SEARCH/@INDEXTHREADS");
        define("indextimeout", INTEGER, "./SEARCH/@INDEXTIMEOUT");
        define("indexretries", INTEGER, "./SEARCH/@INDEXRETRIES");
        define("http", BOOLEAN, "./SEARCH/@HTTP");
        define("httpport", INTEGER, "./SEARCH/@HTTPPORT");
        define("sync", BOOLEAN, "./SEARCH/@SYNC");
        define("syncTask", BOOLEAN, "./SEARCH/@SYNCTASK");
        define("syncTaskInterval", INTEGER, "./SEARCH/@SYNCTASKINTERVAL");
        define("syncCluster", STRING, "./SEARCH/@SYNCCLUSTER");
        define("syncServerlist", STRING, "./SEARCH/@SYNCSERVERLIST");
        define("syncServerport", INTEGER, "./SEARCH/@SYNCSERVERPORT");
        define("removeOldLogFiles", BOOLEAN, "./SEARCH/LOG/REMOVEOLDLOGFILES/@ENABLED");
        define("cronTaskIntervalType", STRING, "./SEARCH/LOG/REMOVEOLDLOGFILES/@CRONTASKINTERVALTYPE");
        define("cronTaskIntervalValue", INTEGER, "./SEARCH/LOG/REMOVEOLDLOGFILES/@CRONTASKINTERVALVALUE");
        define("fileDateThreshold", INTEGER, "./SEARCH/LOG/REMOVEOLDLOGFILES/@FILEDATETHRESHOLD");
        define("detailCache", BOOLEAN, "./SEARCH/@DETAILCACHE");
        define("detailSize", INTEGER, "./SEARCH/@DETAILSIZE");
        define("detailThreshold", INTEGER, "./SEARCH/@DETAILTHRESHOLD");
        define("migration", BOOLEAN, "./SEARCH/@MIGRATION");
        define("migrationHost", STRING, "./SEARCH/@MIGRATIONHOST");
        define("migrationPort", INTEGER, "./SEARCH/@MIGRATIONPORT");
        
        define("searchGuard", BOOLEAN, "./SEARCH/@SEARCHGUARD");
        define("sgKeystore", STRING, "./SEARCH/@SGKEYSTORE");
        define("sgTruststore", STRING, "./SEARCH/@SGTRUSTSTORE");
        define("sgKeystorePwd", SECURE, "./SEARCH/@SGKEYSTOREPWD");
        define("sgTruststorePwd", SECURE, "./SEARCH/@SGTRUSTSTOREPWD");
        
        define("concurrentRequests", INTEGER, "./SEARCH/BULKPROCESSOR/@CONCURRENTREQUESTS");
        define("bulkActions", INTEGER, "./SEARCH/BULKPROCESSOR/@BULKACTIONS");
        define("flushInterval", INTEGER, "./SEARCH/BULKPROCESSOR/@FLUSHINTERVAL");
        define("bulkSize", INTEGER, "./SEARCH/BULKPROCESSOR/@BULKSIZE");
        
        define("scrollCtxKeepAliveDur", LONG, "./SEARCH/@SCROLLCTXKEEPALIVEDURATIONINMINUTES");
        
    } // ConfigSearch

    public void load()
    {
        loadAttributes();

        String[] serverList = serverlist.split(",");
        int i = 0;
        for (String server : serverList)
        {
            servers.put(i, server.trim());
            i++;
        }
        if(StringUtils.isNotBlank(syncServerlist))
        {
            String[] syncServerList = syncServerlist.split(",");
            i = 0;
            for (String syncServer : syncServerList)
            {
                syncServers.put(i, syncServer.trim());
                i++;
            }
        }
        if (replicas == -1)
        {
            replicaCount = (int) Math.floor(servers.size() / 2);
            if (servers.size() > 2)
            {
                replicaCount++;
            }
        }
        else
        {
            replicaCount = replicas;
        }
        if(StringUtils.isNotBlank(syncExclude))
        {
            syncExcludeSet = StringUtils.stringToSet(syncExclude, ",");
        }
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getServerlist()
    {
        return serverlist;
    }

    public void setServerlist(String serverlist)
    {
        this.serverlist = serverlist;
    }

    public int getServerport()
    {
        return serverport;
    }

    public void setServerport(int serverport)
    {
        this.serverport = serverport;
    }

    public boolean isCompress() {
		return compress;
	}

	public void setCompress(boolean compress) {
		this.compress = compress;
	}

	public Map<Integer, String> getServers()
    {
        return servers;
    }

    public String getCluster()
    {
        return cluster;
    }

    public void setCluster(String cluster)
    {
        this.cluster = cluster;
    }

    public String getClienttype()
    {
        return clienttype;
    }

    public void setClienttype(String clienttype)
    {
        this.clienttype = clienttype;
    }

    public int getShards()
    {
        return shards;
    }

    public void setShards(int shards)
    {
        this.shards = shards;
    }

    public int getReplicas()
    {
        return replicaCount;
    }

    public void setReplicas(int replicas)
    {
        this.replicas = replicas;
    }

    public int getIndexthreads()
    {
        return indexthreads;
    }

    public void setIndexthreads(int indexthreads)
    {
        this.indexthreads = indexthreads;
    }

    public int getIndextimeout()
    {
        return indextimeout;
    }

    public void setIndextimeout(int indextimeout)
    {
        this.indextimeout = indextimeout;
    }

    public int getIndexretries()
    {
        return indexretries;
    }

    public void setIndexretries(int indexretries)
    {
        this.indexretries = indexretries;
    }

    public boolean isHttp()
    {
        return http;
    }

    public void setHttp(boolean http)
    {
        this.http = http;
    }

    public int getHttpport()
    {
        return httpport;
    }

    public void setHttpport(int httpport)
    {
        this.httpport = httpport;
    }

    public Map<String, String> getProperties()
    {
        return properties;
    }

    public void setProperties(Map<String, String> properties)
    {
        this.properties = properties;
    }

    public int getReplicaCount()
    {
        return replicaCount;
    }

    public void setReplicaCount(int replicaCount)
    {
        this.replicaCount = replicaCount;
    }

    public boolean isSync()
    {
        return sync;
    }

    public void setSync(boolean sync)
    {
        this.sync = sync;
    }

    public boolean isSyncTask()
    {
        return syncTask;
    }

    public void setSyncTask(boolean syncTask)
    {
        this.syncTask = syncTask;
    }

    public int getSyncTaskInterval()
    {
        return syncTaskInterval;
    }

    public void setSyncTaskInterval(int syncTaskInterval)
    {
        this.syncTaskInterval = syncTaskInterval;
    }

    public String getSyncCluster()
    {
        return syncCluster;
    }

    public void setSyncCluster(String syncCluster)
    {
        this.syncCluster = syncCluster;
    }

    public String getSyncServerlist()
    {
        return syncServerlist;
    }

    public void setSyncServerlist(String syncServerlist)
    {
        this.syncServerlist = syncServerlist;
    }

    public int getSyncServerport()
    {
        return syncServerport;
    }

    public void setSyncServerport(int syncServerport)
    {
        this.syncServerport = syncServerport;
    }

    public String getSyncExclude()
    {
        return syncExclude;
    }

    public Set<String> getSyncExcludeSet()
    {
        return syncExcludeSet;
    }
    
    public Map<Integer, String> getSyncServers()
    {
        return syncServers;
    }

    public boolean isRemoveOldLogFiles()
    {
        return removeOldLogFiles;
    }

    public void setRemoveOldLogFiles(boolean removeOldLogFiles)
    {
        this.removeOldLogFiles = removeOldLogFiles;
    }

    public String getCronTaskIntervalType()
    {
        return cronTaskIntervalType;
    }

    public void setCronTaskIntervalType(String cronTaskIntervalType)
    {
        this.cronTaskIntervalType = cronTaskIntervalType;
    }

    public int getCronTaskIntervalValue()
    {
        return cronTaskIntervalValue;
    }

    public void setCronTaskIntervalValue(int cronTaskIntervalValue)
    {
        this.cronTaskIntervalValue = cronTaskIntervalValue;
    }

    public int getFileDateThreshold()
    {
        return fileDateThreshold;
    }

    public void setFileDateThreshold(int fileDateThreshold)
    {
        this.fileDateThreshold = fileDateThreshold;
    }

    public boolean isDetailCache()
    {
        return detailCache;
    }

    public void setDetailCache(boolean b)
    {
        this.detailCache = b;
    }
    
    public int getDetailSize()
    {
        return detailSize;
    }

    public void setDetailSize(int s)
    {
        this.detailSize = s;
    }
    
    public int getDetailThreshold()
    {
        return detailThreshold;
    }

    public void setDetailThreshold(int s)
    {
        this.detailThreshold = s;
    }

    public boolean isMigration()
    {
        return migration;
    }

    public void setMigration(boolean b)
    {
        this.migration = b;
    }
    
    public String getMigrationHost()
    {
    	return migrationHost;
    }

    public void setMigrationHost(String h)
    {
    	migrationHost = h;
    }

    public int getMigrationPort()
    {
    	return migrationPort;
    }

    public void setMigrationPort(int p)
    {
    	migrationPort = p;
    }

    public boolean isSearchGuard()
    {
        return searchGuard;
    }

    public void setSearchGuard(boolean s)
    {
        this.searchGuard = s;
    }

    public String getSgKeystore()
    {
        return sgKeystore;
    }

    public void setSgKeystore(String s)
    {
        this.sgKeystore = s;
    }

    public String getSgKeystorePwd()
    {
        return sgKeystorePwd;
    }

    public void setSgKeystorePwd(String s)
    {
        this.sgKeystorePwd = s;
    }

    public String getSgTruststore()
    {
        return sgTruststore;
    }

    public void setSgTruststore(String s)
    {
        this.sgTruststore = s;
    }

    public String getSgTruststorePwd()
    {
        return sgTruststorePwd;
    }

    public void setSgTruststorePwd(String s)
    {
        this.sgTruststorePwd = s;
    }
    public int getConcurrentRequests() {
		return concurrentRequests;
	}

	public void setConcurrentRequests(int concurrentRequests) {
		this.concurrentRequests = concurrentRequests;
	}

	public int getBulkActions() {
		return bulkActions;
	}

	public void setBulkActions(int bulkActions) {
		this.bulkActions = bulkActions;
	}

	public int getFlushInterval() {
		return flushInterval;
	}

	public void setFlushInterval(int flushInterval) {
		this.flushInterval = flushInterval;
	}

	public int getBulkSize() {
		return bulkSize;
	}

	public void setBulkSize(int bulkSize) {
		this.bulkSize = bulkSize;
	}
	
	public long getScrollCtxKeepAliveDur() {
		return scrollCtxKeepAliveDur;
	}
	
	public Duration getScrollCtxKeepAliveDurInMinutes() {
		return Duration.ofMinutes(scrollCtxKeepAliveDur);
	}
	
	public void setScrollCtxKeepAliveDur(long scrollCtxKeepAliveDur) {
		long tScrollCtxKeepAliveDur = scrollCtxKeepAliveDur;
		
		if (tScrollCtxKeepAliveDur < MIN_SCROLL_CTX_KEEP_ALIVE_DURATION) {
    		tScrollCtxKeepAliveDur = MIN_SCROLL_CTX_KEEP_ALIVE_DURATION;
    	}
		
		this.scrollCtxKeepAliveDur = tScrollCtxKeepAliveDur;
	}
}
