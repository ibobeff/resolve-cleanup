/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model;


/**
 * This class represents a comments response that is mainly includes some additional properties.
 */
@SuppressWarnings("serial")
public class SocialCommentResponse extends SocialComment
{
    private static final long serialVersionUID = 3209692747012288446L;
	private boolean isLiked = false;

    //this flag indicates like for the user who requested this object
    public boolean isLiked()
    {
        return isLiked;
    }

    public void setLiked(boolean isLiked)
    {
        this.isLiked = isLiked;
    }
}
