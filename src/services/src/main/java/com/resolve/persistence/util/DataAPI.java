/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This API is the gateway to the persistence layer. The persistence layer could
 * be the RDBMS or NoSQL (i.e., Cassandra).
 * 
 * At present the known consumer is the Action Task "Assessor".
 * 
 */
public class DataAPI
{
    /**
     * This method returns the process request based on the processId (sys_id)
     * and statuses. If the processId is available then it must always find one
     * record.
     * 
     * @param processId
     * @param statuses
     *            like COMPLETED, ABORTED etc.
     * @return a {@link Map} with the field name as the key and value.
     * @throws Exception 
     */
    public static Map<String, Object> findProcessRequestByIdAndStatus(String processId, List<String> statuses) throws Exception
    {
        Map<String, Object> result = new HashMap<String, Object>();
        ResponseDTO<ProcessRequest> response = WorksheetSearchAPI.findProcessRequestsById(processId, "system");
        if (response != null && response.getData() != null)
        {
            ProcessRequest processRequest = response.getData();
            if (statuses == null || (processRequest.getStatus() != null && statuses.contains(processRequest.getStatus())))
            {
                result = processRequest.asMap();
            }
        }
        return result;
    }// findProcessRequestByIdAndStatus

    /**
     * This method returns the worksheet data based on the sysId. This must
     * always find one record if the sys id is valid.
     * 
     * @param sysId
     * @return
     * @throws Exception
     */
    public static Map<String, Object> findWorksheetById(String sysId) throws Exception
    {
        Map<String, Object> result = new HashMap<String, Object>();
        if (StringUtils.isBlank(sysId))
        {
            throw new Exception("ERROR: sysId must be provided");
        }
        else
        {
            ResponseDTO<Worksheet> response = WorksheetSearchAPI.findWorksheetById(sysId, "system");
            if (response != null && response.getData() != null)
            {
                Worksheet worksheet = response.getData();
                result = worksheet.asMap();
            }
            else
            {
                Log.log.info("Worksheet not found with the sysId:" + sysId);
            }
        }
        return result;
    }// findWorksheetById

    /**
     * This method returns the worksheet data based on the problem number (PRB
     * No). This must always find one record if the number is valid.
     * 
     * @param number
     * @return
     * @throws Exception
     */
    public static Map<String, Object> findWorksheetByNumber(String number) throws Exception
    {
        Map<String, Object> result = new HashMap<String, Object>();
        if (StringUtils.isBlank(number))
        {
            throw new Exception("ERROR: number must be provided");
        }
        else
        {
            ResponseDTO<Worksheet> response = WorksheetSearchAPI.findWorksheetByNumber(number, "system");
            if (response != null && response.getData() != null)
            {
                Worksheet worksheet = response.getData();
                result = worksheet.asMap();
            }
            else
            {
                Log.log.info("Worksheet not found with the number:" + number);
            }
        }
        return result;
    }// findWorksheetByNumber

    /**
     * This method returns the worksheet data based on the alert id associated
     * with the worksheet.
     * 
     * @param alertId must be provided.
     * @return a {@link List} of {@link Map}
     * @throws Exception
     */
    public static List<Map<String, Object>> findWorksheetByAlertId(String alertId) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        if (StringUtils.isBlank(alertId))
        {
            throw new Exception("ERROR: alertId must be provided");
        }
        else
        {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.addFilterItem(new QueryFilter("alertId", QueryFilter.EQUALS, alertId));
            ResponseDTO<Worksheet> response = WorksheetSearchAPI.searchWorksheets(queryDTO, "system");
            if(response != null && response.getRecords() != null && response.getRecords().size() > 0)
            {
                for (Worksheet worksheet : response.getRecords())
                {
                    result.add(worksheet.asMap());
                }
            }
        }
        return result;
    }// findWorksheetByAlertId

    /**
     * This method returns the worksheet data based on the correlation id associated
     * with the worksheet.
     * 
     * @param correlationId must be provided.
     * @return a {@link List} of {@link Map}
     * @throws Exception
     */
    public static List<Map<String, Object>> findWorksheetByCorrelationId(String correlationId) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        if (StringUtils.isBlank(correlationId))
        {
            throw new Exception("ERROR: correlationId must be provided");
        }
        else
        {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.addFilterItem(new QueryFilter("correlationId", QueryFilter.EQUALS, correlationId));
            ResponseDTO<Worksheet> response = WorksheetSearchAPI.searchWorksheets(queryDTO, "system");
            if(response != null && response.getRecords() != null && response.getRecords().size() > 0)
            {
                for (Worksheet worksheet : response.getRecords())
                {
                    result.add(worksheet.asMap());
                }
            }
        }
        return result;
    }// findWorksheetByCorrelationId

    /**
     * This method returns the worksheet data based on the reference associated
     * with the worksheet.
     * 
     * @param reference must be provided.
     * @return a {@link List} of {@link Map}
     * @throws Exception
     */
    public static List<Map<String, Object>> findWorksheetByReference(String reference) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        if (StringUtils.isBlank(reference))
        {
            throw new Exception("ERROR: reference must be provided");
        }
        else
        {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.addFilterItem(new QueryFilter("reference", QueryFilter.EQUALS, reference));
            ResponseDTO<Worksheet> response = WorksheetSearchAPI.searchWorksheets(queryDTO, "system");
            if(response != null && response.getRecords() != null && response.getRecords().size() > 0)
            {
                for (Worksheet worksheet : response.getRecords())
                {
                    result.add(worksheet.asMap());
                }
            }
        }
        return result;
    }// findWorksheetByReference

    /**
     * This method returns the resolve action result data based on the worksheetId.
     * 
     * @param worksheetId must be provided
     * @return a {@link List} of {@link Map}
     * @throws Exception
     */
    public static List<Map<String, Object>> findActionResultByWorksheetId(String worksheetId) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        if (StringUtils.isBlank(worksheetId))
        {
            throw new Exception("ERROR: worksheetId must be provided");
        }
        else
        {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, worksheetId));
            ResponseDTO<TaskResult> response = WorksheetSearchAPI.searchTaskResults(queryDTO, "system");
            
            if (response != null && response.getRecords() != null && response.getRecords().size() > 0)
            {
                for (TaskResult actionResult : response.getRecords())
                {
                    result.add(actionResult.asMap());
                }
            }
            else
            {
                Log.log.info("Task results not found with the worksheetId:" + worksheetId);
            }
        }
        return result;
    }// findActionResultByWorksheetId

    /**
     * This method returns the resolve action result data based on a {@link List} of Action Task
     * names. Additionally the Action Task name may have the node id as well.
     * 
     * @param actionTasks in the format taskname#namespace or taskname#namespace{namespace.wiki::node_id}. 
     *            Example:
     *            ["TaskName1#NS1","TaskName2#NS1"], or
     *            ["TaskName1#NS1","TaskName2#NS1{NS1.Wiki1::5}"]
     * 
     * @return a {@link List} of {@link Map}
     * @throws Exception
     */
    public static List<Map<String, Object>> findActionResultByActionTask(List<String> actionTasks) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        if (actionTasks == null || actionTasks.size() == 0)
        {
            throw new Exception("ERROR: at least one actionTask must be provided");
        }
        else
        {
            Pattern pattern = Pattern.compile("\\{(.*?)\\}"); // for node id
                                                              // {namespace.wiki::node_id}

            String taskName = null;
            String nodeId = null;
            for (String task : actionTasks)
            {
                if (StringUtils.isNotBlank(task)) // protect against the null
                                                  // value.
                {
                    taskName = task;
                    if (pattern.matcher(task).find())
                    {
                        // TODO is there a better way to do this? Need to check
                        // later
                        taskName = task.substring(0, task.indexOf("{"));
                        nodeId = task.substring(task.indexOf("{") + 1, task.length() - 1);
                    }
                    QueryDTO queryDTO = new QueryDTO();
                    queryDTO.addFilterItem(new QueryFilter("taskFullName", QueryFilter.EQUALS, taskName));
                    if (nodeId != null)
                    {
                        queryDTO.addFilterItem(new QueryFilter("nodeId", QueryFilter.EQUALS, nodeId));
                    }
                    ResponseDTO<TaskResult> actionResults = WorksheetSearchAPI.searchTaskResults(queryDTO, "system");
                    for (TaskResult actionResult : actionResults.getRecords())
                    {
                        result.add(actionResult.asMap());
                    }
                }
            }
        }
        return result;
    }// findActionResultByActionTask
}
