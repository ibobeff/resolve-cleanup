/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.attachment;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.reference.DefaultSecurityConfiguration;

import com.resolve.services.ServiceWiki;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.WikiAttachmentContentVO;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.util.StringUtils;

public class WikiFileUpload
{
    public static final String DOC_SYS_ID = "docSysId";
    public static final String DOC_FULLNAME = "docFullName";
    
    private HttpServletRequest request = null;
    private String username = null;
    
    private String docSysId = null;
    private String docFullName = null;
    
    public WikiFileUpload(HttpServletRequest request, String username) throws Exception
    {
        if(request == null || StringUtils.isEmpty(username))
        {
            throw new Exception("Request object and username cannot be null");
        }
        
        this.request = request;
        this.username = username;
    }
    
    public WikiAttachmentVO upload() throws Exception
    {
        // Check that we have a file upload request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart)
        {
            //if not an upload request, then return
            throw new Exception("Request object does not have any multipart content to upload.");
        }
        
        // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();
        
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        
        //create the attachment object for the document
        WikiAttachmentVO wikiAttachment = prepareAttachment(upload, request, username);
        if(wikiAttachment != null && (StringUtils.isNotBlank(docSysId) || StringUtils.isNotBlank(docFullName)))
        {
            //persist it to the document
            wikiAttachment = ServiceWiki.addAttachmentToWiki(docSysId, docFullName, wikiAttachment, username);
        }
        else
        {
            throw new Exception("Document sysId or full name must be present for upload");
        }
        
        return wikiAttachment;
    }
    
    @SuppressWarnings("unchecked")
    private WikiAttachmentVO prepareAttachment(ServletFileUpload upload, HttpServletRequest request, String username) throws Exception
    {
      //create the attachment object for the document
        WikiAttachmentVO wikiAttachment =  new WikiAttachmentVO();
        wikiAttachment.setULocation("");
        wikiAttachment.setUType("DB");//DEFAULT
        wikiAttachment.setSysCreatedBy(username);
        
        // Parse the request
        List<FileItem> items = upload.parseRequest(request);

        // Process the uploaded items
        Iterator<FileItem> iter = items.iterator();
        while (iter.hasNext())
        {
            FileItem item = iter.next();
//            String name = item.getFieldName();

            if (item.isFormField())
            {
                processFormField(item, wikiAttachment);
            }
            else
            {
                processUploadedFile(item, wikiAttachment, null);
            }
        }//end of while loop
        
        return wikiAttachment;
    }
    
    private void processFormField(FileItem item, WikiAttachmentVO wikiAttachment) throws WikiException
    {
        String name = item.getFieldName();
        String value = item.getString();
        if (name.equalsIgnoreCase(DOC_SYS_ID))
        {
            this.docSysId = value;
        }
        else if (name.equalsIgnoreCase(DOC_FULLNAME))
        {
           this.docFullName = value;
        }
    }
    
    public static void processUploadedFile(FileItem item, WikiAttachmentVO wikiAttachment, String overriddenFilename) throws Exception
    {
        //few attributes for debuging purpose...may be removed/commented out
        String fileName = (overriddenFilename != null? overriddenFilename : item.getName());//C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
        fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);

        fileName= ESAPI.validator().getValidFileName("Upload filename validation", fileName, ESAPI.securityConfiguration().getAllowedFileExtensions(), false);
        long sizeInBytes = item.getSize();
        if (sizeInBytes > DefaultSecurityConfiguration.getInstance().getAllowedFileUploadSize())
        {
        	throw new Exception("File size is over upload limit: " + DefaultSecurityConfiguration.getInstance().getAllowedFileUploadSize());
        }
        
        InputStream uploadedStream = item.getInputStream();
        byte[] data = new byte[(int) sizeInBytes];
        uploadedStream.read(data);

        WikiAttachmentContentVO wac = new WikiAttachmentContentVO();
        wac.setUContent(data);

        wikiAttachment.setUFilename(fileName);
        wikiAttachment.setUSize(data.length);
        wikiAttachment.usetWikiAttachmentContent(wac);
    }
    
    
}
