/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiArchiveVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocResolutionRatingVO;
import com.resolve.services.hibernate.wiki.NamespaceUtil;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.util.Log;

/**
 * unit testing for services. All apis starting with 'test' will be executed
 * 
 * @author jeet.marwah
 *
 */
// TODO: port to JUnit
public class TestServices
{
    @SuppressWarnings("unused")
    public static void test_ServiceHibernate_insertOrUpdateUsingJDBC() 
    {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("U_CONTENT", "test123");
        data.put("U_DT_ABORT_TIME", new Integer(10));
        data.put("U_EXPIRE_ON", "1378338471707");
        data.put("U_HAS_ACTIVE_MODEL", "1");
        data.put("U_RATING_BOOST", 3);
        
        Map<String, Object> data1 = new HashMap<String, Object>();
        data1.put("U_CONTENT", "test123");
        data1.put("U_DT_ABORT_TIME", new Integer(10));
        data1.put("U_EXPIRE_ON", "1378338471707");
        data1.put("U_HAS_ACTIVE_MODEL", "1");
        data1.put("U_RATING_BOOST", 3);
        
        Map<String, Object> data2 = new HashMap<String, Object>();
        data2.put("U_CONTENT", "test456");
        data2.put("U_DT_ABORT_TIME", new Integer(100));
        data2.put("U_EXPIRE_ON", "1378338471707");
        data2.put("U_HAS_ACTIVE_MODEL", "0");
        data2.put("U_RATING_BOOST", 3);

        
        List<Map<String, Object>> recs = new ArrayList<Map<String, Object>>();
        recs.add(data);
        recs.add(data1);
        recs.add(data2);
        
        try
        {
//            Map<String, Object> rec = insertOrUpdateCustomTable("system", "META_VIEW_LOOKUP", data);
//            List<Map<String, Object>> rec = ServiceHibernate.insertOrUpdateUsingJDBC("system", "wikidoc", recs);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public static void test_ServiceHibernate_updateFeedUpdatedDateForRss()
    {
        try
        {
            SocialRssDTO vo = new SocialRssDTO();
            vo.setSys_id("8a9482e5408459390140847d70f20004");
            ServiceHibernate.updateFeedUpdatedDateForRss(vo, "system");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public static void test_ServiceWiki_saveWikiDoc()
    {
        try
        {
            WikiDocumentVO doc = ServiceWiki.getWikiDoc(null, "Runbook.WebHome", "system");
            doc.setUIsHidden(false);
            ServiceWiki.saveWikiDoc(doc, "system");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public static void test_ServiceHibernate_saveUser()
    {
        try
        {
            UsersVO vo = ServiceHibernate.getUserById("718eea88c611227201bb112d7966edf8");
            vo = ServiceHibernate.saveUser(vo, null, "system");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    public static void test_ServiceWiki_getDefaultPropertiesForNewWiki()
    {
        WikiDocumentVO vo = ServiceWiki.getDefaultPropertiesForNewWiki("Runbook.SomeRB-"+System.currentTimeMillis(), "system");
        
        try
        {
            ServiceWiki.saveWikiDoc(vo, "admin");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @SuppressWarnings("unused")
    public static void test_ServiceHibernate_getActionTaskFromIdWithReferences()
    {
        ResolveActionTaskVO vo = ServiceHibernate.getActionTaskFromIdWithReferences(null, "comment#resolve", "admin");
    }
    
    public static void test_ServiceSocial_getGlobalNotificationsForUser()
    {
        User user = SocialCompConversionUtil.getSocialUser("admin");
        try
        {
            ServiceSocial.getGlobalNotificationsForUser(user);
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceSocial_getGlobalNotificationsForUser", e);
        }
    }

    public static void test_ServiceSocial_updateGlobalNotificationsForUser(String username)
    {
        User user = SocialCompConversionUtil.getSocialUser(username);
        Set<String> marked = new HashSet<String>();
        marked.add("DOCUMENT_CREATE");
        marked.add("DOCUMENT_UPDATE");
        marked.add("FORUM_CREATE");
        marked.add("FORUM_UPDATE");
        marked.add("FORUM_PURGED");
        try
        {
            ServiceSocial.updateGlobalNotificationsForUser(user, marked);
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceSocial_updateGlobalNotificationsForUser:" + username, e);
        }
    }
    
    public static void test_ServiceSocial_updateGlobalNotificationsForUser()
    {
        test_ServiceSocial_updateGlobalNotificationsForUser("admin");
    }
    
    @SuppressWarnings("unused")
    public static void test_ServiceSocial_getSpecificNotificationFor()
    {
        User user = SocialCompConversionUtil.getSocialUser("admin");
        try
        {
            ComponentNotification notify = ServiceSocial.getSpecificNotificationFor("8a9482e54185528801418568767a0009", user);
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceSocial_getSpecificNotificationFor", e);
        }
    }
    
    public static void test_ServiceSocial_updateSpecificNotificationFor()
    {
        User user = SocialCompConversionUtil.getSocialUser("admin");
        
        Map<String, String> typeAndValue = new HashMap<String, String>();
//        typeAndValue.put("FORUM_CREATE", "Global Default");
//        typeAndValue.put("FORUM_UPDATE", "True");
//        typeAndValue.put("FORUM_PURGED", "False");
        typeAndValue.put(UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL.name(), "True");
        typeAndValue.put(UserGlobalNotificationContainerType.PROCESS_DIGEST_EMAIL.name(), "False");
        
        
//        try
//        {
//            ServiceSocial.updateSpecificNotificationFor("8a9482e54185528801418568767a0009", user, typeAndValue);
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error in test_ServiceSocial_getSpecificNotificationFor", e);
//        }
    }
    
    public static void test_SocialNotification_findUsersRegisteredToReceiveThisNotificationForComp()
    {
        try
        {
//            SocialNotification.findUsersRegisteredToReceiveThisNotificationForComp("8a9482e54185528801418568767a0009", UserGlobalNotificationContainerType.FORUM_PURGED);
        }
        catch (Exception e)
        {
            Log.log.error("error in finding notify users", e);
        }
    }
    
    public static void test_ServiceTag_assignTagsToComponent() 
    {
        Set<String> tagNames = new HashSet<String>();
        tagNames.add("tag100");
        tagNames.add("tag200");
        tagNames.add("tag300");
        
        try
        {
//            ServiceTag.updateTagsForComponent("8a9482e541804d0f01418050341a000a", tagNames);
        }
        catch (Exception e)
        {
            Log.log.error("error in adding tags to comp", e);
        }
    }
    
    public static void test_ServiceWiki_moveRenameDocument()
    {
        try
        {
            ServiceWiki.moveRenameDocument("CR.Workflow", "Test.test5", false, true, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in moving and renaming document ", e);
        } 
    }
    
    public static void test_ServiceWiki_copyDocument()
    {
        try
        {
            ServiceWiki.copyDocument("System.Attachment", "Test.test16", true, true, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in moving and renaming document ", e);
        } 
    }
    
    public static void test_NamespaceUtil_findAllDocumentsForNamespace()
    {
        NamespaceUtil.findAllDocumentsForNamespace("Runbook", "admin");
    }
    
    public static void test_NamespaceUtil_getDocumentNamespaces()
    {
        NamespaceUtil.getDocumentNamespaces(new QueryDTO(-1, -1), "admin");
    }
    
//    public static void test_ServiceWiki_addRemoveRolesForNamespaces()
//    {
//        Set<String> namespaces = new HashSet<String>();
//        namespaces.add("CR");
//        
//        AccessRightsVO roles = new AccessRightsVO();
//        roles.setUAdminAccess("resolve_user");
//        roles.setUReadAccess("resolve_user");
//        roles.setUWriteAccess("resolve_user");
//        roles.setUExecuteAccess("resolve_user");
//        
//        
//        try
//        {
//            ServiceWiki.addRemoveRolesForNamespaces(namespaces, roles, true, "admin");
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error in moving and renaming document ", e);
//        } 
//        
//    }
    
    public static void test_ServiceWiki_addTagsToWiki()
    {
        Set<String> docSysIds = new HashSet<String>();
        docSysIds.add("8a9482e54011ef69014011f20df900d0");
        
        Set<String> tagSysIds = new HashSet<String>();
        tagSysIds.add("D0B8822BA1D004EE097385CCC82EA02F");
        
        try
        {
            ServiceWiki.addTagsToWiki(docSysIds, tagSysIds, "admin"); 
        }
        catch (Exception e)
        {
            Log.log.error("error in moving and renaming document ", e);
        } 
        
    }
    
//    public static void test_ServiceWiki_addRemoveDefaultRolesForNamespaces()
//    {
//        AccessRightsVO accessRights = new AccessRightsVO();
//        accessRights.setUAdminAccess("admin,resolve_dev");
//        accessRights.setUReadAccess("admin,resolve_dev");
//        accessRights.setUWriteAccess("admin,resolve_dev");
//        accessRights.setUExecuteAccess("admin,resolve_dev");
//        
//        Set<String> ns = new HashSet<String>();
//        ns.add("marwah");
//        
//        try
//        {
//            //add it 
//            ServiceWiki.addRemoveDefaultRolesForNamespaces(ns, accessRights, true, "admin");
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error in adding default ns ", e);
//        }
//        
//        //remove the user from this ns
//        accessRights.setUAdminAccess("resolve_dev");
//        accessRights.setUReadAccess("");
//        accessRights.setUWriteAccess("");
//        accessRights.setUExecuteAccess("");
//        try
//        {
//            //remove it 
//            ServiceWiki.addRemoveDefaultRolesForNamespaces(ns, accessRights, false, "admin");
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error in adding default ns ", e);
//        } 
//
//    }
    
    public static void test_ServiceWiki_archiveDocuments()
    {
        Set<String> sysIds = new HashSet<String>();
        sysIds.add("8a9482e541e2a94b0141e2c80ebb000d");
        
        String comment = "this is testing for commint";
        
        try
        {
            ServiceWiki.archiveDocuments(sysIds, comment, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in archiving ", e);
        }
        
        
    }
    
    public static void test_ServiceWiki_resetWikidocStats()
    {
        Set<String> sysIds = new HashSet<String>();
        sysIds.add("8a9482e541e2a94b0141e2c80ebb000d");
        
        try
        {
            ServiceWiki.resetWikidocStats(sysIds, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in reseting the archive ", e);
        }
    }
    
    public static void test_ServiceWiki_getAccessRightsFor()
    {
        Set<String> sysIds = new HashSet<String>();
        sysIds.add("8a9482e541e2a94b0141e2c80ebb000d");
        
        try
        {
            ServiceWiki.getAccessRightsFor(sysIds, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_getAccessRightsFor ", e);
        }
    }
    
    public static void test_ServiceWiki_getDefaultAccessRightsForNamespaces()
    {
        Set<String> ns = new HashSet<String>();
        ns.add("marwah");
        
        try
        {
            ServiceWiki.getDefaultAccessRightsForNamespaces(ns, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_getDefaultAccessRightsForNamespaces ", e);
        }
    }
    
    public static void test_ServiceWiki_removeUsersFromRoles()
    {
        Set<String> setUserSysIds = new HashSet<String>();
        setUserSysIds.add("718eea88c611227201bb112d7966edf8");
        
        Set<String> roles = new HashSet<String>();
        roles.add("action_execute");
        roles.add("content_request_admin");

        try
        {
            ServiceHibernate.removeUsersFromRoles(setUserSysIds, roles, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_removeUsersFromRoles ", e);
        }
    }
    
    public static void test_ServiceWiki_removeUsersFromGroups()
    {
        Set<String> setUserSysIds = new HashSet<String>();
        setUserSysIds.add("8a9482e6392c162301392cf6e6a000f0");
        
        Set<String> groups = new HashSet<String>();
        groups.add("test_copy");
        groups.add("Doc Group");

        try
        {
            ServiceHibernate.removeUsersFromGroups(setUserSysIds, groups, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_removeUsersFromGroups ", e);
        }
    }
    
    
    public static void test_ServiceWiki_renameAttachment()
    {
        try
        {
            ServiceWiki.renameAttachment("43ac4a410a14026e742f6ada8d35e795", null, "SugarSyncSetup.exe", "SugarSyncSetup_RENAME.exe", true, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_renameAttachment ", e);
        }
    }
    
    public static void test_create_worksheetNode()
    {
        Worksheet ws = new Worksheet();
        ws.setSys_id("sdfs333eeee333");
        ws.setId("sdfs333eeee333");
//        ws.setSys_org("");
        ws.setDisplayName("PRB-1234");
        ws.setRoles("");
        ws.setLock(false);
        
//        ServiceSocial.insertOrUpdateRSComponent(ws);
    }
    
    public static void test_ServiceWiki_getAttachmentsFor()
    {
        try
        {
            ServiceWiki.getAttachmentsFor(null, "43ac4a410a14026e742f6ada8d35e795", null, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_getAttachmentsFor ", e);
        }
    }
    
    public static void test_ServiceWiki_addGlobalAttachmentsToWiki()
    {
        Set<String> attachSysIds = new HashSet<String>();
        attachSysIds.add("8a9482e53e48350d013e56a251c67af2");
        
        try
        {
            ServiceWiki.addGlobalAttachmentsToWiki(null, "Project1Resolve.resolve_100204", attachSysIds, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_addGlobalAttachmentsToWiki ", e);
        }
    }

    public static void test_ServiceWiki_deleteAttachments()
    {
        Set<String> attachSysIds = new HashSet<String>();
        attachSysIds.add("8a9482e53e48350d013e56a251c67af2");
        
        try
        {
            ServiceWiki.deleteAttachments("43ac4a410a14026e742f6ada8d35e795", null, attachSysIds, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_ServiceWiki_deleteAttachments ", e);
        }
    }
    
    public static void test_ServiceCatalog_getCatalogByName()
    {
//        try
//        {
//            ResolveCatalog catalog = ServiceCatalog.getCatalogByName("Intro to Resolve Concepts");
//            if(catalog != null)
//            {
//                ResolveCatalog node = catalog.findCatalogNodeWithPath("/Intro to Resolve Concepts/Chapter 3/Creating a Runbook");
//                if(node != null)
//                {
//                    System.out.println("Got the node");
//                }
//                else
//                {
//                    System.out.println("No node with that path");
//                }
//            }
//            else
//            {
//                System.out.println("No catalog with that name");
//            }
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error in test_ServiceCatalog_getCatalogByName ", e);
//        }
    }
    
    public static void test_Service_updateResolutionRatingCount()
    {
        
        Set<String> docSysIds = new HashSet<String>();
        docSysIds.add("8a9482e54011ef69014011f20df900d0");
        
        
        try
        {
            WikidocResolutionRatingVO rating = ServiceWiki.getResolutionRatingForDocument("8a9482e54011ef69014011f20df900d0", null, "admin");
            rating.setU1StarCount(new Long(10));
            rating.setU2StarCount(new Long(20));
            rating.setU3StarCount(new Long(30));
            rating.setU4StarCount(new Long(40));
            rating.setU5StarCount(new Long(50));

            ServiceWiki.updateResolutionRatingCount(docSysIds, rating, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("error in test_Service_updateResolutionRatingCount ", e);
        }
    }
    
    public static void test_ServiceCatalog_createCatalog()
    {
        String json = "{\"name\":\"catalog2\",\"type\":\"CatalogReference\",\"isRoot\":\"true\",\"isRootRef\":\"false\",\"tags\":[],\"wiki\":\"\",\"link\":\"http://\",\"displayType\":\"wiki\",\"id\":\"34944.1383957227902\",\"catalogType\":\"\",\"children\":[{\"id\":\"34946.1383957228204\",\"name\":\"Group1\",\"type\":\"CatalogGroup\",\"internalName\":\"Group1\",\"tags\":[],\"children\":[{\"id\":\"34947.1383957228313\",\"name\":\"New Folder\",\"title\":\"Folder1\",\"description\":\"\",\"image\":\"/resolve/service/catalog/download?id=8a9482e5426d47e301426d5aa42a0006\",\"imageName\":\"559491_3111463227946_1305348590_32550910_497993287_n.jpg\",\"tooltip\":\"\",\"type\":\"CatalogFolder\",\"displayType\":\"\",\"wiki\":\"\",\"link\":\"\",\"form\":\"\",\"tags\":[{\"id\":\"cc82631e57a933d0b39578e57f8655f9\"}],\"internalName\":\"New Folder\",\"icon\":\"/resolve/service/catalog/download?id=8a9482e5426d47e301426d5aa42a0006\",\"openInNewTab\":\"false\",\"children\":[],\"order\":0},{\"id\":\"34948.1383957228446\",\"name\":\"Item2\",\"title\":\"Item2\",\"description\":\"\",\"image\":\"/resolve/service/catalog/download?id=8a9482e5426d47e301426d5aa42a0006\",\"imageName\":\"559491_3111463227946_1305348590_32550910_497993287_n.jpg\",\"tooltip\":\"\",\"wiki\":\"Project1Resolve.resolve_100204\",\"link\":\"http://\",\"displayType\":\"wiki\",\"type\":\"CatalogItem\",\"internalName\":\"Item2\",\"icon\":\"/resolve/service/catalog/download?id=8a9482e5426d47e301426d5aa42a0006\",\"maxImageWidth\":\"0\",\"path\":\"/catalog2/Group1/Item2\",\"openInNewTab\":\"false\",\"tags\":[],\"children\":[],\"order\":1}],\"order\":0},{\"id\":\"37857.1384819736735\",\"name\":\"Group2\",\"type\":\"CatalogGroup\",\"internalName\":\"Group2\",\"tags\":[],\"children\":[{\"id\":\"37858.1384819736829\",\"name\":\"Item1\",\"title\":\"Item1\",\"description\":\"\",\"image\":\"/resolve/service/catalog/download?id=8a9482e5426d2efb01426d322e1e0005\",\"imageName\":\"Desert.jpg\",\"tooltip\":\"\",\"wiki\":\"\",\"link\":\"http://\",\"displayType\":\"listOfDocuments\",\"type\":\"CatalogItem\",\"internalName\":\"Item1\",\"icon\":\"/resolve/service/catalog/download?id=8a9482e5426d2efb01426d322e1e0005\",\"maxImageWidth\":\"0\",\"path\":\"/catalog2/Group2/Item1\",\"openInNewTab\":\"false\",\"tags\":[{\"id\":\"cc82631e57a933d0b39578e57f8655f9\"}],\"children\":[],\"order\":0}],\"order\":1}]}";
        
        String catalogRefJson = "{   \"name\":\"New Catalog1\",   \"type\":\"CatalogReference\",   \"isRoot\":\"true\",   \"isRootRef\":\"false\",   \"tags\":[   ],   \"wiki\":\"\",   \"link\":\"http://\",   \"displayType\":\"wiki\",   \"namespace\":\"\",   \"id\":\"90280.0\",   \"catalogType\":\"\",   \"children\":[      {         \"id\":\"90281.0\",         \"name\":\"New Group1\",         \"type\":\"CatalogGroup\",         \"internalName\":\"New Group1\",         \"tags\":[         ],         \"children\":[            {               \"name\":\"New Catalog2\",               \"type\":\"CatalogReference\",               \"isRoot\":\"false\",               \"isRootRef\":\"true\",               \"tags\":[               ],               \"wiki\":\"\",               \"link\":\"http://\",               \"displayType\":\"wiki\",               \"namespace\":\"\",               \"children\":[                  {                     \"id\":\"90282.0\"                  }               ],               \"order\":0            }         ],         \"order\":0      }   ]}";
        
//        ResolveCatalog catalog = null;
//        try
//        {
//            catalog = new ObjectMapper().readValue(catalogRefJson, ResolveCatalog.class);
//            catalog.setSys_updated_by("admin");
//
//            if(StringUtils.isNotBlank(catalog.getId()))
//                ServiceCatalog.updateCatalog(catalog, "system");
//            else
//                ServiceCatalog.createCatalog(catalog, "system");
//            
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error in create...so updating now");
//            try
//            {
//                if(catalog != null)
//                    ServiceCatalog.updateCatalog(catalog, "system");
//            }
//            catch (Exception e1)
//            {
//                Log.log.error("error in update..." + e1);
//            }
//        }
    }
    
    /*public static void test_ServiceCatalog_copyUser()
    {
        UsersVO user = ServiceHibernate.getUser("admin");
        user.setUUserName("testAdmin"+ System.currentTimeMillis());
        
        String p_asswordToSet = null;
        
        if (user.getUUserSaltedPasswordHash() != null && !user.getUUserSaltedPasswordHash().equalsIgnoreCase(VO.STRING_DEFAULT))
        {
            p_asswordToSet = "_resolve";
        }
        else
        {
            p_asswordToSet = com.resolve.util.UserUtils.encryptPasswd("_resolve");
        }
        
        user.setUUserP_assword(p_asswordToSet);
        user.setSys_id(null);
        user.setId(null);
        user.setUImage(null);
        user.setUTempImage(null);
        
        
        try
        {
            UsersVO vo = ServiceHibernate.saveUser(user, "admin", "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the UsersVO", e);
        }
        
    }*/
    
    public static void test_ServiceWiki_getSpecificWikiRevisionsFor()
    {
        try
        {
            List<WikiArchiveVO> list = ServiceWiki.getSpecificWikiRevisionsFor("43ac4a410a14026e742f6ada8d35e795", null, 18, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the UsersVO", e);
        }
        
    }
    
    public static void test_ServiceWiki_rollback()
    {
        try
        {
            ServiceWiki.rollbackWiki(null, "Project1Resolve.resolve_100204", 15, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the UsersVO", e);
        }
         
    }
    
    public static void test_ServiceWiki_resetWiki()
    {
        try
        {
            ServiceWiki.resetWiki(null, "Project1Resolve.resolve_100204", "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceWiki_resetWiki", e);
        }
         
    }

    public static void test_ServiceWiki_getWikiPageInfo()
    {
        try
        {
            ServiceWiki.getWikiPageInfo(null, "AAAAAA.Jeet");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceWiki_getWikiPageInfo", e);
        }
         
    }

    public static void test_ServiceHibernate_saveResolveActionTask()
    {
        try
        {
            String json = "{   \"id\":\"ff808081393220750139367177f60324\",   \"uname\":\"CPU info\",   \"unamespace\":\"rsqa\",   \"umenuPath\":\"/rsqa\",   \"assignedTo\":{      \"id\":\"718eea88c611227201bb112d7966edf8\"   },   \"usummary\":\"\",   \"udescription\":\"test\",   \"uactive\":\"true\",   \"ulogresult\":true,   \"accessRights\":{      \"sys_id\":\"ff808081393220750139367177f70326\",      \"ureadAccess\":\"admin,resolve_dev\",      \"uwriteAccess\":\"admin,resolve_dev\",      \"uadminAccess\":\"admin,resolve_dev\",      \"uexecuteAccess\":\"admin,resolve_dev,action_execute\"   },   \"tags\":[   ],   \"resolveActionInvoc\":{      \"resolveActionInvocOptions\":[         {            \"id\":\"8a9482e542bee1920142bf65cce7002d\",            \"uname\":\"TAILLENGTH\",            \"uvalue\":\"test\"         },         {            \"id\":\"ff8080813932207501393671a98f0327\",            \"uname\":\"INPUTFILE\",            \"udescription\":\"CPU info#rsqa\",            \"uvalue\":\"test\"         }      ],      \"utype\":\"OS\",      \"utimeout\":\"300\",      \"ucommand\":\"cat /proc/cpuinfo\",      \"uargs\":\"test\",      \"id\":\"ff808081393220750139367177f60325\",      \"resolveActionParameters\":[         {            \"id\":\"8a9482e542bee1920142bf65cce8002e\",            \"uname\":\"input1\",            \"udescription\":\"test\",            \"udefaultValue\":\"test\",            \"urequired\":true,            \"uinclude\":true,            \"uencrypt\":true,            \"utype\":\"Input\"         },         {            \"id\":\"8a9482e542bee1920142bf65cceb0030\",            \"uname\":\"output\",            \"udescription\":\"test\",            \"udefaultValue\":\"test\",            \"urequired\":true,            \"uinclude\":true,            \"uencrypt\":true,            \"utype\":\"Output\"         }      ],      \"assess\":{         \"id\":\"ff8080813932207501393671a98f0328\",         \"uname\":\"CPU info#rsqa\",         \"uscript\":\"ddddd\",         \"udescription\":\"\",         \"ucnsName\":\"\",         \"uparse\":\"\",         \"uonlyCompleted\":\"true\"      },      \"preprocess\":{         \"id\":\"ff8080813932207501393671a98f0329\",         \"uname\":\"CPU info#rsqa\",         \"udescription\":\"\",         \"uscript\":\"test\"      },      \"parser\":{         \"id\":\"8a9482e542bee1920142bf63d3b30029\",         \"uscript\":\"\"      },      \"resolveActionTaskMockData\":[      ]   }}";
            
            ResolveActionTaskVO vo = new ObjectMapper().readValue(json, ResolveActionTaskVO.class);
            vo.setSysUpdatedBy("admin");
            
//            ResolveActionTaskVO vo = ServiceHibernate.getActionTaskFromIdWithReferences(null, "Support Email#rsqa", "admin");
            ServiceHibernate.saveResolveActionTask(vo, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceWiki_getWikiPageInfo", e);
        }
         
    }
    
    public static void test_ServiceHibernate_archive()
    {
        
        try
        {
            ServiceHibernate.archive("ResolveAssess", "UScript", "8a9482f13b67622c013b676b6a0f0010", "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ResolveArchiveUtils_archive", e);
        }
        
        
        
    }
    
    public static void test_ServiceHibernate_findArchivesFor()
    {
        
        try
        {
            ServiceHibernate.findArchivesFor("ResolvePreprocess", "8a9482e6397de093013993a3544c0319", -1, -1, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceHibernate_findArchivesFor", e);
        }
    }

    public static void test_ServiceGraph_exportGraph_importGraph()
    {
        Map<String, ImpexOptionsDTO> processes = new HashMap<String, ImpexOptionsDTO>();
        processes.put("8a9482e542bf76a80142bf86f4a60009", new ImpexOptionsDTO());//P12
        
        Map<String, ImpexOptionsDTO> teams = new HashMap<String, ImpexOptionsDTO>();
        teams.put("8a9482e5427c8c0d01427c8e1a4c0005", new ImpexOptionsDTO());//T5
        
        Map<String, ImpexOptionsDTO> forums = new HashMap<String, ImpexOptionsDTO>();
        forums.put("8a9482e542ca30850142ca469d590012", new ImpexOptionsDTO());//F2

        Map<String, ImpexOptionsDTO> documents = new HashMap<String, ImpexOptionsDTO>();
        documents.put("8a9482e542ca30850142ca41f0ea000b", new ImpexOptionsDTO());//AAAA.TestExport1

        Map<String, ImpexOptionsDTO> actiontasks = new HashMap<String, ImpexOptionsDTO>();
        actiontasks.put("ff8080813ee3f0c5013ee9a054a40005", new ImpexOptionsDTO());//Action_Mock1

        SocialImpexVO socialImpex = new SocialImpexVO();
        socialImpex.setFolderLocation("C:/project/resolve3/dist");
//        socialImpex.setProcesses(processes);
//        socialImpex.setTeams(teams);
//        socialImpex.setForums(forums);
//        socialImpex.setActiontasks(actiontasks);
        socialImpex.setDocuments(documents);
        
        
        try
        {
            ServiceGraph.exportGraph(socialImpex);
        }
        catch (Throwable e)
        {
            Log.log.error("Error test_ServiceGraph_exportGraph_importGraph", e);
        }
        
        
        try
        {
            ServiceGraph.importGraph(socialImpex);
        }
        catch (Throwable e)
        {
            Log.log.error("Error test_ServiceGraph_exportGraph_importGraph", e);
        }
        
    }
    
    public static void test_ServiceCustomTable_getCustomTables()
    {
        QueryDTO query = new QueryDTO();
        query.setModelName("CustomTable");
        query.setSelectColumns("sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UModelName,UDisplayName,UType,USource,UDestination");
        query.setLimit(10);
        query.setStart(0);
        
        try
        {
            ServiceCustomTable.getCustomTables(query, "admin");
        }
        catch (Throwable e)
        {
            Log.log.error("Error test_ServiceGraph_exportGraph_importGraph", e);
        }
    }

    public static void test_ServiceCustomTable_findCustomTableWithFields()
    {

        try
        {
            ServiceCustomTable.findCustomTableWithFields(null, "social_process", "admin");
        }
        catch (Throwable e)
        {
            Log.log.error("Error test_ServiceCustomTable_findCustomTableWithFields", e);
        }
    }
    

    public static void test_ServiceCustomTable_deleteCustomTableByIds()
    {
        String[] sysIds = {"8a9482e635c1d7130135c52d1dca0125"};
        try
        {
            ServiceCustomTable.deleteCustomTableByIds(sysIds, false, "admin");
        }
        catch (Throwable e)
        {
            Log.log.error("Error test_ServiceCustomTable_deleteCustomTableByIds", e);
        }
    }
    
    public static void test_ServiceSocial_getUsersForForwardEmail()
    {
        try
        {
        
//        //test starts
//        //specific
//        Set<String> streamIds = new HashSet<String>();
//        streamIds.add("8a9482e542e901b50142e905d4e40007");
//        updateSpecificNotificationType(streamIds, UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL.name(), true, request, null);
//        updateSpecificNotificationType(streamIds, UserGlobalNotificationContainerType.PROCESS_DIGEST_EMAIL.name(), true, request, null);
//        
//        getSpecificNotification("8a9482e542e901b50142e905d4e40007", request, null);
//        
            RSComponent component = new RSComponent(NodeType.PROCESS);
            component.setSys_id("8a9482e542e901b50142e905d4e40007");
    
            ServiceSocial.getUsersForForwardEmail(component, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceSocial_getUsersForForwardEmail", e);
        }
    }
    
    public static void test_ServiceHibernate_syncDBToGraph()
    {
//        ServiceMigration.syncDBToGraph();
    }
    
    public static void test_DigestNotification()
    {
        try
        {
            User user = SocialCompConversionUtil.getSocialUser("admin");
            ServiceSocial.updateSpecificNotificationTypeFor("all", user, UserGlobalNotificationContainerType.valueOf("USER_ALL_DIGEST_EMAIL"), true);
            ServiceSocial.updateSpecificNotificationTypeFor("all", user, UserGlobalNotificationContainerType.valueOf("USER_ALL_FORWARD_EMAIL"), true);
            ServiceSocial.updateSpecificNotificationTypeFor("inbox", user, UserGlobalNotificationContainerType.valueOf("USER_DIGEST_EMAIL"), true);
            ServiceSocial.updateSpecificNotificationTypeFor("inbox", user, UserGlobalNotificationContainerType.valueOf("USER_FORWARD_EMAIL"), true);
            ServiceSocial.updateSpecificNotificationTypeFor("system", user, UserGlobalNotificationContainerType.valueOf("USER_SYSTEM_DIGEST_EMAIL"), true);
            ServiceSocial.updateSpecificNotificationTypeFor("system", user, UserGlobalNotificationContainerType.valueOf("USER_SYSTEM_FORWARD_EMAIL"), true);
            
            ComponentNotification data1 = ServiceSocial.getSpecificNotificationFor("all", user);
            
//            Map<User, List<RSComponent>> result = SearchUserStreamsForDigest.getUserStreamsForDigest();
        }
        catch (Exception e)
        {
            Log.log.error("Error test_DigestNotification", e);
        }
    }
    
    public static void test_ServiceHibernate_findActiontaskReferencesForAssessors()
    {
        try
        {
            Set<String> assessorSysIds = new HashSet<String>();
            assessorSysIds.add("3477a0eb7f00000177d2cf01eede4009");
            assessorSysIds.add("d500de70c611227200659bde90c5655e");

            Set<String> actiontaskNames = ServiceHibernate.findActiontaskReferencesForAssessors(assessorSysIds, "admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceHibernate_findActiontaskReferencesForAssessors", e);
        }
    }
    
    public static void test_ServiceWiki_assignRoles()
    {
        try
        {
            Set<String> docSysIds = new HashSet<String>();
            docSysIds.add("8a9482e5439dadc201439db0dedf000b");
            
            //make it default
            ServiceWiki.assignRoles(docSysIds, null, true, "admin");
            
            //be specific
            AccessRightsVO accessRights = new AccessRightsVO();
            accessRights.setUAdminAccess("admin");
            accessRights.setUReadAccess("admin");
            accessRights.setUWriteAccess("admin");
            accessRights.setUExecuteAccess("admin");
            
            ServiceWiki.assignRoles(docSysIds, accessRights, false, "admin");
        }
        catch(Exception e)
        {
            Log.log.error("Error test_ServiceWiki_assignRoles", e);
        }
    }
    
    public static void test_ServiceWiki_setRolesForNamespaces()
    {
        try
        {
            Set<String> ns = new HashSet<String>();
            ns.add("Jeet");
            
            //make it default
            ServiceWiki.setRolesForNamespaces(ns, null, true, "admin");
            
            //be specific
            AccessRightsVO accessRights = new AccessRightsVO();
            accessRights.setUAdminAccess("admin");
            accessRights.setUReadAccess("admin");
            accessRights.setUWriteAccess("admin");
            accessRights.setUExecuteAccess("admin");
            
            ServiceWiki.setRolesForNamespaces(ns, accessRights, false, "admin");
        }
        catch(Exception e)
        {
            Log.log.error("Error test_ServiceWiki_setRolesForNamespaces", e);
        }
    }
    
    public static void test_ServiceWiki_setNamespaceDefaultRoles()
    {
        try
        {
            Set<String> ns = new HashSet<String>();
            ns.add("Jeet");
            
            //be specific
            AccessRightsVO accessRights = new AccessRightsVO();
            accessRights.setUAdminAccess("admin");
            accessRights.setUReadAccess("admin");
            accessRights.setUWriteAccess("admin");
            accessRights.setUExecuteAccess("admin");
            
            ServiceWiki.setNamespaceDefaultRoles(ns, accessRights, "admin");
        }
        catch(Exception e)
        {
            Log.log.error("Error test_ServiceWiki_setNamespaceDefaultRoles", e);
        }
    }
    
    public static void test_ServiceHibernate_getMetaFormView()
    {
        try
        {
            ServiceHibernate.getMetaFormView(null, "AAAFORM5", null, "admin", RightTypeEnum.admin, null, false);
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceHibernate_getMetaFormView", e);
        }
    }
    
    public static void test_ServiceHibernate_getAllPostAttachments()
    {
        try
        {
            ServiceHibernate.getAllPostAttachments("admin");
        }
        catch (Exception e)
        {
            Log.log.error("Error test_ServiceHibernate_getAllPostAttachments", e);
        }
    }
}