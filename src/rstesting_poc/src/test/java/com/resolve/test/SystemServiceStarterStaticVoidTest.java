package com.resolve.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.resolve.test.Service;
import com.resolve.test.ServiceListener;
import com.resolve.test.SystemServiceStarter;

@RunWith(PowerMockRunner.class)
public class SystemServiceStarterStaticVoidTest {
    private Service service;
    private SystemServiceStarter system;
    private ServiceListener serviceListener;

    @Before
    public void setupMock() {
        service = Mockito.mock(Service.class);
        serviceListener = Mockito.mock(ServiceListener.class);

        system = new SystemServiceStarter();
        system.add(service);
        system.setServiceListener(serviceListener);
    }

    @PrepareForTest({ SystemServiceStarter.class }) //This is required when we want to mock final classes or methods which either final, private, static or native.
    @Test
    public void stubStaticVoidMethod() {        
        // Call mockStatic SystemServiceStarter.class to enable static mocking
        PowerMockito.mockStatic(SystemServiceStarter.class);
        
        // Stub static void method SystemServiceStarter.notifyServiceListener to do nothing
        PowerMockito.doNothing().when(SystemServiceStarter.class);
        SystemServiceStarter.notifyServiceListener(serviceListener, service, true);
        
        // Stub using PowerMockito. service.start() should return 1 as we want start of the service to be successful
        PowerMockito.when(service.start()).thenReturn(1);       

        // Start the system
        system.start();

        // Verify static method startServiceStaticWay(service) is called
        PowerMockito.verifyStatic();
        SystemServiceStarter.startServiceStaticWay(service);

        // Verify serviceListener.onSuccess(service) is not called as notifyServiceListener is stubbed to do nothing
        Mockito.verify(serviceListener, Mockito.never()).onSuccess(service);
    }

}