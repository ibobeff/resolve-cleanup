package com.resolve.test.integration.elasticsearch;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.MediaType;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.resolve.testslib.integration.elasticsearch.ElasticSearchManager;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class ElasticSearchIT
{
    private Client client;
    private static String testIndexName = "junitindexname";
    private static final String HTTP_BASE_URL = "http://localhost";
    private static final String HTTP_PORT = "12002";
    private static final String elasticSearchBaseUrl = HTTP_BASE_URL + ":" + HTTP_PORT;

    @BeforeClass
    public static void startElasticSearch() throws Exception
    {
        ElasticSearchManager.startElasticSearch(HTTP_PORT);
    }
    
    @AfterClass
    public static void stopElasticSearch() throws IOException
    {
        ElasticSearchManager.stopElasticsearch();
    }

    @Before
    public void initialize()
    {
        // create client for each test
        ClientConfig clientConfig = new DefaultClientConfig();
        client = Client.create(clientConfig);
        client.addFilter(new LoggingFilter(System.out));
    }

    @Test
    public void testCreateIndex() throws InterruptedException
    {
        
        WebResource service = client.resource(elasticSearchBaseUrl);
        // check first that does the index already exist
        ClientResponse clientResponse = service.path(testIndexName).head();
        if (clientResponse.getClientResponseStatus().equals(ClientResponse.Status.OK))
        {
            Assert.fail("Index exists already");
        }
        String indexJson = "{\"settings\" : {\"index\" : {\"number_of_shards\" : 1,\"number_of_replicas\" : 0}}}";
        try
        {
            String response = service.path(testIndexName)
                            .accept(MediaType.APPLICATION_JSON).header("Content-type", "application/json").put(String.class, indexJson);
            if (!response.contains("true"))
            {
                Assert.fail("Creating index failed. IndexName: " + testIndexName);
            }
        }
        catch (UniformInterfaceException e)
        {
            // failed due to Client side problem
            throw e;
        }

        // wait for Elastic Search to be ready for further processing
        HashMap<String,String> statusParameters = new HashMap<>();
        final String timeout = "30s";
        statusParameters.put("timeout", timeout);
        statusParameters.put("wait_for_status", "green");
        String statusResponse = status(statusParameters);
        if (statusResponse.contains("red"))
        {
            Assert.fail("Failed to create index");
        }
    }

    public String status(final Map<String, String> optionalParameters)
    {
        WebResource service = client.resource(elasticSearchBaseUrl);
        WebResource webResource = service.path("_cluster").path("/health");
        for (Entry<String,String> entry : optionalParameters.entrySet())
        {
            webResource = webResource.queryParam(entry.getKey(), entry.getValue());
        }
        return webResource.get(String.class);
    }

}
