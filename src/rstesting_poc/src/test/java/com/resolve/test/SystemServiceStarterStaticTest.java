package com.resolve.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.resolve.test.Service;
import com.resolve.test.ServiceListener;
import com.resolve.test.SystemServiceStarter;


@RunWith(PowerMockRunner.class)
public class SystemServiceStarterStaticTest {
    private Service service;
    private SystemServiceStarter system;
    private ServiceListener serviceListener;

    @Before
    public void setupMock() {
        // Mock
        service = Mockito.mock(Service.class);
        serviceListener = Mockito.mock(ServiceListener.class);

        system = new SystemServiceStarter();
        system.add(service);
        system.setServiceListener(serviceListener);
    }

    @Test
    @PrepareForTest({SystemServiceStarter.class})
    public void stubStaticNonVoidMethod() {

        //Stub static method startServiceStaticWay to return 1
        PowerMockito.when(SystemServiceStarter.startServiceStaticWay(service))
                .thenReturn(1);
        // Run
        system.start();

        // Verify success
        // Verify using Mockito that service started successfuly
        Mockito.verify(serviceListener).onSuccess(service);

        //========================================================
        
        // Stub static method startServiceStatic to fail
        // Stub static method startServiceStaticWay to return 0
        PowerMockito.when(SystemServiceStarter.startServiceStaticWay(service))
                .thenReturn(0);

        // Start the system again
        system.start();

        // Verify failure
        //Verify using Mockito that service has failed
        Mockito.verify(serviceListener).onFailure(service);
    }
}