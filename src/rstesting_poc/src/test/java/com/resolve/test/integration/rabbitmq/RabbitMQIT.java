package com.resolve.test.integration.rabbitmq;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.resolve.testslib.integration.rabbitmq.BrokerManager;

public class RabbitMQIT {
    
    private static final boolean IS_SSL = false;
    private static final String BROKER_PORT = "5672";
    private static final String FIRST = "first";
    private static final String SECOND = "second";
    private static final String THIRD = "third";

    private static BrokerManager brokerStarter;
    
    @BeforeClass
    public static void startup() throws Exception {
        brokerStarter = new BrokerManager();
        brokerStarter.startBroker(BROKER_PORT, IS_SSL);
//        brokerStarter.startBroker(null, true);//for 4004
    }
 
    @AfterClass
    public static void tearDown() throws Exception {
        brokerStarter.stopBroker();
    }
 
    private SimpleCache cache = new SimpleCache();
 
    @Test
    public void cacheShouldContainThreeEntries_afterThreeReceivedMessages() throws Exception {
        //GIVEN
        Sender sender = new Sender(IS_SSL);
        sender.sendMessage(FIRST);
        sender.sendMessage(SECOND);
        sender.sendMessage(THIRD);

        //WHEN
        new Receiver(cache, IS_SSL).receive();
        Thread.sleep(500); // This, of course can and should be replaced with something smarter
 
        //THEN
        List<CacheEntry> cacheContent = cache.getContent();
         
        assertEquals(3, cacheContent.size());
        
        assertEquals(0, cacheContent.get(0).getSequenceNr());
        assertEquals(FIRST, cacheContent.get(0).getText());    
        
        assertEquals(1, cacheContent.get(1).getSequenceNr());
        assertEquals(SECOND, cacheContent.get(1).getText());
        
        assertEquals(2, cacheContent.get(2).getSequenceNr());
        assertEquals(THIRD, cacheContent.get(2).getText());
    }
}