package com.resolve.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.resolve.test.Service;
import com.resolve.test.ServiceListener;
import com.resolve.test.SystemServiceStarter;

@PrepareForTest({ SystemServiceStarter.class })
@RunWith(PowerMockRunner.class)
public class SystemServiceStarterPrivateMethodTest {
    private Service service;
    private SystemServiceStarter system;
    private ServiceListener serviceListener;

    @Before
    public void setupMock() {
        // Mock
        service = Mockito.mock(Service.class);
        serviceListener = Mockito.mock(ServiceListener.class);

        system = PowerMockito.spy(new SystemServiceStarter());
        system.add(service);
        system.setServiceListener(serviceListener);
    }

    @Test
    public void stubPrivateMethodAddEvent() throws Exception {
        // Stub using PowerMockito. service.start() should return 1 as we want start of the service to be successful
        PowerMockito.when(service.start()).thenReturn(1);
        
        // Stub service name to return serviceA
        Mockito.when(service.getName()).thenReturn("serviceA");
        
        // Stub private addEvent to do nothing");
        PowerMockito.doNothing().when(system, "addEvent", service, true);

        // Start the system, should start the services in turn
        system.start();

        // Since we have stubbed addEvent, assert that system.getEvents() is empty
        Assert.assertTrue(system.getEvents().isEmpty());
    }
    
    @Test
    public void stubPrivateMethodGetEventString() throws Exception {
        final String serviceA = "serviceA";
        final String serviceA_is_successful = serviceA + " is successful";
        // Stub using PowerMockito. service.start() should return 1 as we want start of the service to be successful
        PowerMockito.when(service.start()).thenReturn(1);
        
        // Stub service name to return serviceA");
        Mockito.when(service.getName()).thenReturn(serviceA);
        
        // Stub private addEvent to do nothing");
        PowerMockito.when(system, "getEvent", serviceA, true).thenReturn(serviceA_is_successful);

        // Start the system, should start the services in turn
        system.start();

        // Since we have stubbed getEvent, assert that system.getEvents() contains the event string
        Assert.assertTrue(!system.getEvents().isEmpty());
        Assert.assertEquals(serviceA_is_successful, system.getEvents().get(0));
        System.out.println(system.getEvents());
    }
    
    @Test
    public void verifyPrivateMethods() throws Exception {
        // Stub using PowerMockito. service.start() should return 1 as we want start of the service to be successful
        PowerMockito.when(service.start()).thenReturn(1);
        
        // Stub service name to return serviceA
        Mockito.when(service.getName()).thenReturn("serviceA");

        // Start the system, should start the services in turn
        system.start();

        // Verify private method addEvent(service, true) is called
        PowerMockito.verifyPrivate(system).invoke("addEvent", new Object[] { service, true });
    }

}