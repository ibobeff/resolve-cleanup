package com.resolve.test.integration.hsql;

import java.io.IOException;
import java.sql.SQLException;

import org.hsqldb.cmdline.SqlToolError;
import org.hsqldb.server.ServerAcl.AclFormatException;
import org.junit.Test;

import com.resolve.testslib.integration.hsql.InMemoryDBUtil;


public class InMemoryLoadingTestIT {
	
	@Test
	public void initAndStopDBTest() throws IOException, AclFormatException, SqlToolError, SQLException, ClassNotFoundException {
		
		InMemoryDBUtil.startInMemoryDBServer();
		InMemoryDBUtil.initResolveDB();		
		InMemoryDBUtil.getSessionFactory();	// The session factory can be used instead of mocked real one 
		InMemoryDBUtil.clearInMemoryDB();
		InMemoryDBUtil.stopInMemoryDBServer();
	}

}
