package com.resolve.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.resolve.test.Service;
import com.resolve.test.ServiceListener;
import com.resolve.test.SystemServiceStarter;

@RunWith(PowerMockRunner.class)
public class SystemServiceStarterTest {

    private Service service;
    private SystemServiceStarter system;
    private ServiceListener serviceListener;

    @Before
    public void setupMock() {
        // Mock
        service = Mockito.mock(Service.class);
        serviceListener = Mockito.mock(ServiceListener.class);
        
        system = Mockito.spy(new SystemServiceStarter());
        system.add(service);
        system.setServiceListener(serviceListener);
    }

    @Test
    public void start_withSuccessfullyStartedService_shouldEnvokeOnSuccessOnServiceListener() {
        // GIVEN
        //Stub using PowerMockito. service.start() should return 1 as we want start of the service to be successful
        PowerMockito.when(service.start()).thenReturn(1);

        // WHEN
        //execute SUT method
        system.start();     

        // THEN
        // Verify that service listener onSuccess is invoked
        Mockito.verify(serviceListener, Mockito.times(1)).onSuccess(service);
        Mockito.verify(serviceListener, Mockito.times(0)).onFailure(service);
    }

    
    @Test
    public void start_withFailedToStartService_shouldEnvokeOnFailureOnServiceListener() {
        // GIVEN
        //Stub using PowerMockito. service.start() should return 0 as we want starting the service to fail
        PowerMockito.when(service.start()).thenReturn(0);

        // WHEN
        //execute SUT method
        system.start();     

        // THEN
        // Verify that service listener onSuccess is invoked
        Mockito.verify(serviceListener, Mockito.times(0)).onSuccess(service);
        Mockito.verify(serviceListener, Mockito.times(1)).onFailure(service);
        
    }
    
    @Test
    public void start_withSuccessfullyStartedService_shouldSendEventAboutThatService() {
        // GIVEN
        //Stub using PowerMockito. service.start() should return 1 as we want start of the service to be successful
        PowerMockito.when(service.start()).thenReturn(1);
        
        String expectedServiceName = "MyExpectedServiceName";
        Mockito.when(service.getName()).thenReturn(expectedServiceName);
        

        // WHEN
        //execute SUT method
        system.start();     

        // THEN
        Assert.assertEquals(1, system.getEvents().size());
        Assert.assertEquals(expectedServiceName+" started", system.getEvents().get(0));
    }
    
}