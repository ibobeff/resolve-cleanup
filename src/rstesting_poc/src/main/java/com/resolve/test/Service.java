package com.resolve.test;

public interface Service {  
    String getName();
    int start();
}