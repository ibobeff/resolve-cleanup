package com.resolve.test.integration.rabbitmq;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Receiver
{

    private static final String BROKER_URI = "amqp://localhost:5672";
    private final static String QUEUE_NAME = "RSTESTQUEUE";
    private SimpleCache cache;
    private boolean isSSL;
    
    public Receiver(SimpleCache cache, boolean isSSL)
    {
        this.cache = cache;
        this.isSSL = isSSL;
    }

    public void receive() throws Exception
    {
        // let's setup evrything and start listening
        ConnectionFactory factory = createConnectionFactory();

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicConsume(QUEUE_NAME, true, newConsumer(channel));
    }

    protected ConnectionFactory createConnectionFactory() throws Exception
    {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(BROKER_URI);
        factory.setPassword("resolve");
        factory.setUsername("resolve");

        if(isSSL) {
            factory.useSslProtocol();
        }
        
        return factory;
    }

    private DefaultConsumer newConsumer(Channel channel)
    {
        return new DefaultConsumer(channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException
            {
                // put each message into the cache
                cache.update(new String(body));
            }
        };
    }
}
