package com.resolve.test.integration.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Sender {
    
    private static final String QUEUE_NAME = "RSTESTQUEUE";
    private static final String BROKER_URL = "amqp://localhost:5672";
 
    private ConnectionFactory factory = new ConnectionFactory();
    private boolean isSSL;
    
    public Sender(boolean isSSL) {
        this.isSSL = isSSL;
    }
     
    public void sendMessage(String text) throws Exception {
        factory.setUri(BROKER_URL);
        factory.setPassword("resolve");
        factory.setUsername("resolve");
        if(isSSL) {
            factory.useSslProtocol();
        }
         
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
 
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME,  null, text.getBytes());
    }
}