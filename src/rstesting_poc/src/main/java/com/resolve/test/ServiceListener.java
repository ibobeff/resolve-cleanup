package com.resolve.test;

public interface ServiceListener {
    void onSuccess(Service service);
    void onFailure(Service service);
    
}