/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.security.AccessControlException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import org.codehaus.jackson.map.ObjectMapper;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.ServiceWiki;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.UserService;
import com.resolve.services.exception.MissingVersionException;
import com.resolve.services.exception.VersionLimitReachedException;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.util.ResolveNamespaceUtil;
import com.resolve.services.hibernate.vo.NamespaceVO;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.CopyHelper;
import com.resolve.services.hibernate.wiki.DocUtils;
import com.resolve.services.hibernate.wiki.WikiDocLock;
import com.resolve.services.hibernate.wiki.WikiStatusEnum;
import com.resolve.services.rb.util.ResolutionBuilderUtils;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.util.WikiLockException;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.vo.FeedbackDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.WikiPageInfoDTO;
import com.resolve.services.vo.WikidocRatingDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.social.SocialUtil;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.QueryWiki;
import com.resolve.wiki.dto.AttachmentDTO;
import com.resolve.wiki.dto.HistoryDTO;
import com.resolve.wiki.dto.TagDTO;
import com.resolve.wiki.dto.ViewWikiRevisionDTO;
import com.resolve.services.exception.VersionLimitReachedException;

import net.sf.json.JSONObject;

/**
 * This controller serves the request for Wiki/Document related operations/functions.
 * <p>
 * A Wiki can be a Document, Runbook (that has a Execution Model) or a Decision Tree. 
 * <p> 
 * For more information about Wiki, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a>. 
 * <p>
 * DB Table: {@code wikidoc}</br>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.wiki.Main/name=Runbook.WebHome}
 * 
 * @author jeet.marwah
 *
 */
@Controller
public class AjaxWiki extends GenericController
{
	private static final String ERROR_RETRIEVING_NAMESPACES = "Error retrieving namespace list";
	private UserService userService;
	
	@Autowired
    public AjaxWiki(final UserService userService) {
		this.userService = userService;
	}

	//public static final String TASK_DETAIL_LINK_URI_IN_UI = "/resolve/service/result/task/detail";
	
	private static final String SAVE_AND_COMMIT_ERR_MSG = "Error during save and commit operation of a wiki document";
	private static final String SAVE_AS_ERR_MSG = "Error during Save As operation of a Wiki.";
	private static final String ROLLBACK_REV_ERR_MSG = "Error during rollback of revision.";
	private static final String RESET_REV_ERR_MSG = "Error during reset of revision.";
    
    /**
     * This api is to get the list of all the Namespaces available in system and is used to populate the UI drop down 
     * menu when a Move/Copy operation needs to be provided for bulk of wiki's or Namespace.  
     * 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  page: 1
     *  query:
     *  start: 0
     *  limit: -1
     * </pre></code>
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link NamespaceVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     *  
     * @param query This is not functional for this api as it returns all the Namespaces, so no pagination required at the moment
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true, {@link NamespaceVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/namespace/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO namespaceList(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //get the VOs
            result = ServiceWiki.getDocumentNamespaces(query, username);

            //prepare the response
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_NAMESPACES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_NAMESPACES);
        }

        return result;
    }

    // TODO Swap namespaceList with this method when ready for transition to the new namespaces
    //@RequestMapping(value = "/wiki/namespace/list", method = RequestMethod.GET)
    //@ResponseBody
	public ResponseDTO<NamespaceVO> namespaceListV2(@ModelAttribute QueryDTO query, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO<NamespaceVO> result = new ResponseDTO<>();

		try {
			List<NamespaceVO> ns = ResolveNamespaceUtil.convertToList(ResolveNamespaceUtil.getAllWikiNamespaces());
			result.setRecords(ns).setSuccess(true);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_NAMESPACES, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_NAMESPACES);
		}

		return result;
	}

    /**
     * This api is to 'get' complete detail of a particular Document based on its sysId or the name of the Document. If a document
     * does not exist, it will consider that as a new document and will return with default properties for the UI. 
     *
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link WikiDocumentVO}
     * </pre></code>
     * 
     * @param id sysId of the wiki document
     * @param name name of the wiki document 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} with success true and {@link WikiDocumentVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/get", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getWiki(@RequestParam("id") String id, 
    			@RequestParam("name") String name, 
    			@RequestParam(name = "version", required=false) String version, 
    			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        //test - this is called from UI
//        setSoftLock(name, false, request, response);
//        setSoftLock(name, true, request, response);
        
        try
        {
            WikiDocumentVO vo = ServiceWiki.getWikiDocWithGraphRelatedData(id, name, username, version);
            if (vo == null)
            {
                //this is for the new document. 
                vo = ServiceWiki.getDefaultPropertiesForNewWiki(name, username);
            }

            result.setData(vo);
            result.setSuccess(true);
        }
        catch (MissingVersionException e) {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, "Missing version requested."));
        }
        catch (Exception e)
        {
            Log.log.error("Error getting wiki doc.", e);
            result.setSuccess(false).setMessage("Error getting wiki doc.");
        }

        return result;
    }

    
    /**
     * Saves/Update a Wikidocument. If 'sysId' is set, it will be update else it will create it. 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link WikiDocumentVO}
     * </pre></code>
     * 
     * @param jsonProperty JSONObject/JSON having data of type {@link WikiDocumentVO}
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} with success true and saved {@link WikiDocumentVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/save", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return saveAndCommit(jsonProperty, null, false, false, request, response, true);
    }
    
    /**
     * Save current wiki doc snapshot as a new one.
     * 
     * @param jsonProperty : JSON String representing WikiDocument model. This model has new full name.
     * @param toDocument : String representing new wikidoc name.
     * @param request
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/wiki/saveAs", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<WikiDocumentVO> saveAs(@RequestBody JSONObject jsonProperty, @RequestParam String toDocument, @RequestParam String choice, 
    		HttpServletRequest request) throws Exception
    {
    	ResponseDTO<WikiDocumentVO> result = new ResponseDTO<WikiDocumentVO>();
    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    	boolean flag = (StringUtils.isNotBlank(choice) && choice.equals("overwrite") ? true : false);
    	
        String json = jsonProperty.toString(); 
        ObjectMapper mapper = new ObjectMapper();//.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        WikiDocumentVO fromDoc = mapper.readValue(json, WikiDocumentVO.class);
        try
        {
	        WikiDocument fromDocModel = new WikiDocument (fromDoc);
	        CopyHelper copyHelper = new CopyHelper(fromDocModel, toDocument, username);
	        copyHelper.setOverwrite(flag);
	        copyHelper.setValidateUser(true);
	
	        WikiDocumentVO doc = copyHelper.copy();
	
	        // if from doc has Automation Builder then do its copying as well
	        if (StringUtils.isNotBlank(copyHelper.getFromDocResolutionBuilderId()))
	        {
	            ResolutionBuilderUtils.copy(copyHelper.getFromDocResolutionBuilderId(), doc.getSys_id(), doc.getUName(), doc.getUNamespace(), username);
	        }
	        
	        result.setData(doc).setSuccess(true);
        }
        catch(Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, SAVE_AS_ERR_MSG));
        }
        
        return result;
    }

    // TODO Swap saveAndCommit with this method when ready for transition to the new namespaces
    //@RequestMapping(value = "/wiki/saveAs", method = { RequestMethod.POST, RequestMethod.GET })
    //@ResponseBody
    public ResponseDTO<WikiDocumentVO> saveAsV2(@RequestBody JSONObject jsonProperty, @RequestParam String toDocument, @RequestParam String choice, 
    		HttpServletRequest request) throws Exception
    {
    	ResponseDTO<WikiDocumentVO> result = new ResponseDTO<WikiDocumentVO>();
    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    	boolean flag = (StringUtils.isNotBlank(choice) && "overwrite".equalsIgnoreCase(choice) ? true : false);
    	
        String json = jsonProperty.toString(); 
        ObjectMapper mapper = new ObjectMapper();
        WikiDocumentVO fromDoc = mapper.readValue(json, WikiDocumentVO.class);
        
		try {
	        WikiDocumentVO doc = ServiceWiki.copyDocument(fromDoc, toDocument, flag, true, username, true);
			result.setSuccess(true).setData(doc);
		} catch (Exception e) {
			result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, SAVE_AS_ERR_MSG));
		}
        
        return result;
    }

    /**
     * 
     * Saves/Update and Commit a Wikidocument. If 'sysId' is set, it will be update else it will create it. This api, along with save,
     * also creates a Revision entry which will not get cleanup with the Auto Revisions. 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link WikiDocumentVO}
     * </pre></code>
     *  
     * @param jsonProperty JSONObject/JSON having data of type {@link WikiDocumentVO}
     * @param comment Comment related to this save revision
     * @param postToSocial If true, will send a post with the comment to this document
     * @param reviewed If true, mark the wiki document as reviewed and set the Reviewed Date
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} with success true and saved {@link WikiDocumentVO} as data if everything goes smooth, false otherwise with the error message.
     *          Null <code>JSONObject</code> will throw NullPointerException. 
     * @throws ServletException
     * @throws IOException
     */
	@RequestMapping(value = "/wiki/saveandcommit", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResponseDTO<WikiDocumentVO> saveAndCommit(@RequestBody JSONObject jsonProperty, @RequestParam String comment,
			@RequestParam boolean postToSocial, @RequestParam boolean reviewed, HttpServletRequest request,
			HttpServletResponse response, boolean isSave) throws ServletException, IOException {
		// using jackson to deserialize the json
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String json = jsonProperty.toString();
		WikiDocumentVO entity = new ObjectMapper().readValue(json, WikiDocumentVO.class);
		if (!isSave) {
			entity.setUIsStable(true);
		}
		if (reviewed) {
			entity.setULastReviewedOn(GMTDate.getDate());
			entity.setUReqestSubmissionOn(null);
			entity.setUIsRequestSubmission(false);
			entity.setULastReviewedBy(username);
		}

		ResponseDTO<WikiDocumentVO> result = new ResponseDTO<WikiDocumentVO>();

		try {
			WikiDocumentVO vo = ServiceWiki.saveAndCommitWikiDoc(entity, comment, username, isSave);
			if (postToSocial && StringUtils.isNotBlank(comment)) {
				Set<String> docSysIds = new HashSet<String>();
				docSysIds.add(vo.getSys_id());

				// if the flag is true, than post the comment to social also
				ServiceSocial.post("", comment, username, new ArrayList<String>(docSysIds));
			}
			result.setSuccess(true).setData(vo);
		} catch (VersionLimitReachedException e) {
			result.setSuccess(false).setMessage(e.getMessage());
			response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		} catch (Exception e) {
			result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, SAVE_AND_COMMIT_ERR_MSG));
		}

		return result;
	}

    // TODO Swap saveAndCommit with this method when ready for transition to the new namespaces
    //@RequestMapping(value = "/wiki/saveandcommit", method = { RequestMethod.POST, RequestMethod.GET })
    //@ResponseBody
	public ResponseDTO<WikiDocumentVO> saveAndCommitV2(@RequestBody JSONObject jsonProperty,
			@RequestParam String comment, @RequestParam boolean postToSocial, @RequestParam boolean reviewed,
			HttpServletRequest request) throws ServletException, IOException {
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String json = jsonProperty.toString();
		WikiDocumentVO entity = new ObjectMapper().readValue(json, WikiDocumentVO.class);
		if (reviewed) {
			entity.setULastReviewedOn(GMTDate.getDate());
			entity.setUReqestSubmissionOn(null);
			entity.setUIsRequestSubmission(false);
			entity.setULastReviewedBy(username);
		}

		ResponseDTO<WikiDocumentVO> result = new ResponseDTO<WikiDocumentVO>();

		try {
			WikiDocumentVO vo = ServiceWiki.saveAndCommitWikiDoc(entity, comment, username, true);
			if (postToSocial && StringUtils.isNotBlank(comment)) {
				Set<String> docSysIds = new HashSet<String>();
				docSysIds.add(vo.getSys_id());

				ServiceSocial.post("", comment, username, new ArrayList<String>(docSysIds));
			}
			result.setSuccess(true).setData(vo);
		} catch (Exception e) {
			result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, SAVE_AND_COMMIT_ERR_MSG));
		}

		return result;
	}
    
    /**
     * Wiki document will be permanently deleted from the system. User should have 'admin' role on the document he is trying to purge.
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     *  
     * @param docFullName Name of the wikidocument to be Purged
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/purge", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO purgeWiki(@RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        try
        {
        	if (StringUtils.isBlank(docFullName) && StringUtils.isBlank(id)) {
        		throw new IllegalArgumentException("Invalid full document name and document id");
        	}
        	
        	if (StringUtils.isNotBlank(docFullName) || StringUtils.isNotBlank(id)) {
	            ServiceWiki.deleteWikiDocument(id, docFullName, username);
	            result.setSuccess(true);
        	}
        }
        catch (Exception e)
        {
            String error = String.format("Error%spurging a wiki document%s.", 
            							 (StringUtils.isNotBlank(e.getMessage()) ? " [" + e.getMessage() + "] " : " "),
            							 (StringUtils.isNotBlank(docFullName) ? " " + docFullName : ""));
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }


    /**
     * 
     * This api is to get the survey rating and comments and is used by the 'Infobar' on the Wiki Document.
     *
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link FeedbackDTO}
     * </pre></code>
     *  
     * @param documentName Document full name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true and data set to {@link FeedbackDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/getUserSurveyValues", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getUserSurveyValues(@RequestParam String documentName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        ResponseDTO result = new ResponseDTO();
        try
        {
            FeedbackDTO feedback = new QueryWiki().getUserSurveyValues(id, documentName, username);
            result.setSuccess(true).setData(feedback);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting the survey information: ", e);
            result.setSuccess(false).setMessage("Error in getting the survey information");
        }

        return result;
    }

    /**
     * 
     *  This api is used to submit a survery for a Wiki document. Survey gathers rating, comment and how useful the doc information is for the user.
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  docFullName:Test.Test
     *  useful:true
     *  rating:3
     *  comment:Comment goes here
     * </pre></code>
     * 
     * @param feedback {@link FeedbackDTO}, represents the Feedback from the user
     * @param docFullName Document full name 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message. 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/submitSurvey", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO submitSurvey(@ModelAttribute FeedbackDTO feedback, @RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        try
        {
            ServiceWiki.submitSurvey(id, docFullName, feedback, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String error = "Error in saving the survey information.";
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;

    }


    /**
     * 
     * User can set a flag for review of a document. This api serves that.
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  docFullName:Test.Test
     *  comment:Comment goes here
     * </pre></code>
     * 
     * @param docFullName Document full name 
     * @param comment comment regarding why is this document needs review 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/flagDocument", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO flagDocument(@RequestParam String docFullName, @RequestParam String comment, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        try
        {
            ServiceWiki.submitRequest(id, docFullName, true, comment, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String error = "Error in flagging a document: " + docFullName;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;

    }

    /**
     * @deprecated 
     * 
     * 
     * 
     * @param docFullName
     * @param request
     * @param response
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/unFlagDocument", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO unFlagDocument(@RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        try
        {
            ServiceWiki.submitRequest(id, docFullName, false, null, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String error = "Error in un-flagging document: " + docFullName;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }


    /**
     * This api is to get the survey comments for a document and is used by the 'Infobar' on the Wiki Document.
     *
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link WikidocRatingDTO}
     * </pre></code>
     *  
     * @param docFullName Document full name
     * @param request  {@link HttpServletRequest} with <code>id</code> as Wiki sys_id
     * @param response  {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and data with {@link WikidocRatingDTO} if everything goes smooth, false otherwise with the error message. If both docFullname and id is not supplied, Exception is thrown.
     * @throws ServletException
     * @throws IOException
     * 
     */
    @RequestMapping(value = "/wiki/getSurveyComments", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getSurveyComments(@RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        try
        {
            if (StringUtils.isNotBlank(docFullName) || StringUtils.isNotBlank(id))
            {
                String correctDocumentName = evaluateWikiDocument(docFullName, username);
                WikidocRatingDTO feedback = ServiceWiki.getRatingFor(id, correctDocumentName, username);
                
                if(feedback != null)
                {
                    result.setSuccess(true).setData(feedback);
                }
                else
                {
                    result.setSuccess(false).setMessage("Rating info not found for document");
                }
            }
            else
            {
                throw new Exception("Document name and Sysid is empty from the UI.");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting the rating info: ", e);
            result.setSuccess(false).setMessage("Error in getting the rating info.");
        }

        return result;
    }

    /**
     * 
     * @deprecated
     * 
     * This feature is Disabled for now. Please ignore.
     * 
     * @param docFullName
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/deleteSurveyComments", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteSurveyComments(@RequestParam String docFullName, @RequestParam List<String> ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        try
        {
//            throw new Exception("This api is Deprecated and will be removed. Just kept it as UI is still referring it.Confirmed with Duke.");
            throw new Exception("Cannot delete comments.This api is Deprecated and will be removed.");

            //            if(StringUtils.isNotBlank(docFullName))
            //            {
            ////                WikidocRatingDTO feedback = ServiceWiki.getRatingFor(null, docFullName, username);
            //                result.setSuccess(true);//.setData(feedback);
            //            }
            //            else
            //            {
            //                throw new Exception("Document is empty.");
            //            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in deleting the rating info: ", e);
            result.setSuccess(false).setMessage("Cannot delete comments.This api is Deprecated and will be removed.");
        }

        return result;
    }


    /**
     * Api to get the tags for a particular document using sysId or the name of the Document.
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link TagDTO}
     *  'total' : # of total records in the table/db 
     * </pre></code>     
     * 
     * @param docSysId sysId of wikidoc
     * @param docFullName  Document full name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true and records set to array of {@link TagDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/getTags", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getTags(@RequestParam String docSysId, @RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            ArrayList<TagDTO> tagArray = new ArrayList<TagDTO>();

            WikiDocumentVO doc = ServiceWiki.getWikiDocWithGraphRelatedData(docSysId, docFullName, "system");
            if (doc != null)
            {
                String tag = doc.getUTag();
                if (StringUtils.isNotBlank(tag))
                {
                    String[] tagArr = tag.split(",");
                    for (String t : tagArr)
                    {
                        tagArray.add(new TagDTO(t));
                    }

                }
            }

            result.setSuccess(true).setRecords(tagArray);
        }
        catch (Exception e)
        {
            String error = "Error in getting tags for wiki: " + docFullName;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;

    }

    /**
     * 
     * Api to get the page info for the wiki document
     * 
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link WikiPageInfoDTO}
     * </pre></code> 
     * 
     * @param name - Document full name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true and data set to {@link WikiPageInfoDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/getPageInfo", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<WikiPageInfoDTO> getPageInfo(@RequestParam String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<WikiPageInfoDTO> result = new ResponseDTO<WikiPageInfoDTO>();

        //TODO: Please pass this value as a parameter to the signature of this api. I have not added it as UI will break. 
        String id = request.getParameter("id");

        try
        {
            WikiPageInfoDTO pageInfo = ServiceWiki.getWikiPageInfo(id, name);
            result.setSuccess(true).setData(pageInfo);
        }
        catch (Exception e)
        {
            String error = "Error in getting pageinfo for wiki :" + name;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }

    /**
     * 
     * @deprecated
     * 
     * @param query - should have the where clause filter 'UNamespace=AAAAAA', its the namespace of the wiki document 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/names", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getDocumentNames(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        query.setWhereClause("UNamespace='AAAAAA'"); //test
        return new AjaxWikiAdmin().listWikis(query, request, response);
    }

    
    /**
     * Api to get list of Help documents. This api will return docs that starts with 'Doc.Wiki_Help_*'
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link WikiDocumentVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     *  
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true and records set to List of {@link WikiDocumentVO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/listHelpDocs", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listHelpDocuments(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        try
        {
            //Get the list of documents matching Doc.Wiki_Help_*
            List<WikiDocumentVO> docs = ServiceWiki.getListOfHelpDocuments(username);

            result.setRecords(docs);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting help wiki doc(s).", e);
            result.setSuccess(false).setMessage("Error getting help wiki doc(s).");
        }

        return result;
    }

    /**
     * Api to set a Hard Lock on the Document. Admin or the user having Admin rights on a document can lock. 
     * <p>
     * Document cannot be edited if this lock is set.
     * 
     * @param id Document sysId 
     * @param lock true to lock and false to release the lock
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/setLock", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setLock(@RequestParam("id") String id, @RequestParam("lock") Boolean lock, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if (StringUtils.isNotBlank(id))
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(id));
                ServiceWiki.updateWikiStatusFlag(docSysIds, WikiStatusEnum.LOCKED, lock, true, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the locked flag for doc.", e);
            result.setSuccess(false).setMessage("Error updating the locked flag for doc.");
        }

        return result;
    }

    /**
     * Api to return user who has softlock on current document.
     * 
     * @param docName - Document full name 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/getSoftLockBy", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getSoftLockBy(@RequestParam("docName") String docName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();        
        WikiDocLock lock = DocUtils.getWikiDocLockNonBlocking(docName);
        if (lock != null)
        {
            result.setSuccess(true).setData(lock.getUsername());
        }
        else 
        {
           result.setSuccess(false);
        }
        return result;
    }
    
    /**
     * Api to apply soft lock/unlock to the documents when they are being edited.
     * 
     * @param docName - Document full name
     * @param lock - true to set the lock and false to release the lock 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/setSoftLock", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setSoftLock(@RequestParam("docName") String docName, @RequestParam("lock") Boolean lock, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if (StringUtils.isNotBlank(docName))
            {
                ServiceWiki.setSoftLock(docName, lock, username.trim());
                result.setSuccess(true);
            }
            
        }
        catch(WikiLockException lockE) {
            result.setSuccess(true).setData(lockE.getUsername()).setMessage(lockE.getMessage());
        }
        catch (Exception e)
        {
            String error = "Error updating the soft lock flag for doc :" + docName;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }
    

    @RequestMapping(value = "/wiki/requestSoftLock", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO requestSoftLock(@RequestParam("docName") String docName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if (StringUtils.isNotBlank(docName))
            {
                ServiceWiki.requestSoftLock(username, docName);
                result.setSuccess(true);
            }
            
        }
        catch(WikiLockException lockE) 
        {
            result.setSuccess(true).setData(lockE.getUsername()).setMessage(lockE.getMessage());
        }
        catch (Exception e)
        {
            String error = "Error updating the soft lock flag for doc :" + docName;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }
    
    /**
     * This api locks the Document Stream. So if any user is following a document that has its Stream locked, he will not be able to Post any comments to it.
     * 
     * @param id Document sysId 
     * @param lock true to lock and false to release the lock
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/setLockedStream", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO setLockedStream(@RequestParam String id, @RequestParam boolean lock, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            User user = SocialUtil.getSocialUser(request);
            ServiceSocial.setStreamLockUnlock(user, id, lock);

            return result.setSuccess(true);
        }
        catch (Exception e)
        {
            String error = "Error locking / unlocking a stream.";
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }
        
        return result;
    }

    ///////////////////////////////////////////////////////////////// WIKI ATTACHMENTS   ///////////////////////////////////////////////
    
    /**
     * Api to get the attachments for the wiki document either by sysId or by Document name
     * <p> 
     * To get the list of Global attachments, pass the 'docFullName = System.Attachment' to this api
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link AttachmentDTO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     * 
     * 
     * @param docSysId Document sysId
     * @param docFullName Document full name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return @return {@link ResponseDTO} with success true and records set to List of {@link AttachmentDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/getAttachments", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<AttachmentDTO> getAttachments(@RequestParam String docSysId, @RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<AttachmentDTO> result = new ResponseDTO<AttachmentDTO>();

        try
        {
            if (StringUtils.isNotBlank(docFullName) || StringUtils.isNotBlank(docSysId))
            {
                String correctDocumentName = evaluateWikiDocument(docFullName, username);
                List<AttachmentDTO> attachments = ServiceWiki.getAllAttachmentsFor(docSysId, correctDocumentName, username);
                result.setSuccess(true).setRecords(attachments);
            }
            else
            {
                throw new Exception("Document name and Sysid is empty from the UI.");
            }
        }
        catch (Exception e)
        {
            String error = "Error in getting the attachment(s) for doc: " + docFullName + " or sysId: " + docSysId;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }

    /**
     * 
     * Api to attach/refer Global attachments to a Document. 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * 
     * @param docSysId Document sysId
     * @param docFullName Document Full name
     * @param attachIds selected list of attachment sysIds (from the list of attachment for document name 'System.Attachment')
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message. 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/addGlobalAttachment", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<AttachmentDTO> addGlobalAttachment(@RequestParam String docSysId, @RequestParam String docFullName, @RequestParam("attachIds") String[] attachIds, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<AttachmentDTO> result = new ResponseDTO<AttachmentDTO>();

        try
        {
            if ((StringUtils.isNotEmpty(docSysId) || StringUtils.isNotEmpty(docFullName)) && attachIds != null && attachIds.length > 0)
            {
                Set<String> globalAttachSysIds = new HashSet<String>(Arrays.asList(attachIds));

                ServiceWiki.addGlobalAttachmentsToWiki(docSysId, docFullName, globalAttachSysIds, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            String error = "Error in updating global attachment for document: " + docFullName + " or sysId: " + docSysId;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////   WIKI REVISIONS  ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * 
     * Api to get the history/revision of the wiki document
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link HistoryDTO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     * 
     * 
     * @param docFullName Document full name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return @return {@link ResponseDTO} with success true and records set to List of {@link HistoryDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/getHistory", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<HistoryDTO> getHistory(@RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<HistoryDTO> result = new ResponseDTO<HistoryDTO>();

        try
        {
            List<HistoryDTO> revision = new QueryWiki().getAllWikiRevisionsFor(docFullName, username);
            result.setSuccess(true).setRecords(revision);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting revision for document: " + docFullName, e);
            result.setSuccess(false).setMessage("Error in getting revision for document: " + docFullName);
        }

        return result;

    }

    /**
     * @deprecated
     * 
     * @param id - Document sysId
     * @param comment Comment for this revision
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/revision/commit", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO commitRevision(@RequestParam("id") String id, @RequestParam("comment") String comment, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        boolean postToSocial = request.getParameter("postToSocial") != null ? ((String) request.getParameter("postToSocial")).equalsIgnoreCase("true") : false;

        try
        {
            if (StringUtils.isNotBlank(id))
            {
                Set<String> docSysIds = new HashSet<String>();
                docSysIds.add(id);

                ServiceWiki.archiveDocuments(docSysIds, comment, username);
                if (postToSocial && StringUtils.isNotBlank(comment))
                {
                    //if the flag is true, than post the comment to social also
                    ServiceSocial.post("", comment, username, new ArrayList<String>(docSysIds));
                }

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error during commit of docs.", e);
            result.setSuccess(false).setMessage("Error during commit of docs.");
        }

        return result;
    }

    /**
     * 
     * This api is to get a specific revision/version of Document.
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ViewWikiRevisionDTO}
     * </pre></code>
     * 
     * @param id sysId of the wiki document
     * @param docFullname Document full name 
     * @param revision Revision # to be viewed
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return @return {@link ResponseDTO} with success true and data set to {@link ViewWikiRevisionDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/revision/view", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ViewWikiRevisionDTO> viewRevision(@RequestParam("id") String id, @RequestParam("docFullname") String docFullname, @RequestParam("revision") Integer revision, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ViewWikiRevisionDTO> result = new ResponseDTO<ViewWikiRevisionDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if ((StringUtils.isNotBlank(id) || StringUtils.isNotBlank(docFullname)) && revision > 0)
            {
                ViewWikiRevisionDTO viewVersion = new QueryWiki().getSpecificWikiRevisionsFor(id, docFullname, revision, username);
                result.setSuccess(true).setData(viewVersion);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error during view of revision.", e);
            result.setSuccess(false).setMessage("Error during view of revision.");
        }

        return result;
    }

    /**
     * Returns details of 2 version of the same document to compare.     
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'records' : list of {@link ViewWikiRevisionDTO}
     * </pre></code>
     *  
     * @param id Document sysId
     * @param docFullname Document Full name
     * @param rev1 First Revision number to compare 
     * @param rev2 Second Revision number to compare
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return @return {@link ResponseDTO} with success true and records set to List of {@link ViewWikiRevisionDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/revision/compare", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ViewWikiRevisionDTO> compareRevision(@RequestParam("id") String id, @RequestParam("docFullname") String docFullname, @RequestParam("rev1") Integer rev1, @RequestParam("rev2") Integer rev2, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ViewWikiRevisionDTO> result = new ResponseDTO<ViewWikiRevisionDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if ((StringUtils.isNotBlank(id) || StringUtils.isNotBlank(docFullname)) && rev1 > 0 && rev2 > 0)
            {
                List<ViewWikiRevisionDTO> revisions = new QueryWiki().getRevisionsToCompare(id, docFullname, rev1, rev2, username);
                result.setSuccess(true).setRecords(revisions);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error during compare of revision.", e);
            result.setSuccess(false).setMessage("Error during compare of revision.");
        }

        return result;
    }

    /**
     * 
     * This api rollback's a document to a previous revision.
     * 
     * @param id Document sysId
     * @param docFullname Document Full name
     * @param revision Revision number to be rollbacked to
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/revision/rollback", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ViewWikiRevisionDTO> rollbackRevision(@RequestParam("id") String id, @RequestParam("docFullname") String docFullname, @RequestParam("revision") Integer revision, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ViewWikiRevisionDTO> result = new ResponseDTO<ViewWikiRevisionDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if ((StringUtils.isNotBlank(id) || StringUtils.isNotBlank(docFullname)) && revision > 0)
            {
                ServiceWiki.rollbackWiki(id, docFullname, revision, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ROLLBACK_REV_ERR_MSG));
        }

        return result;
    }

    /**
     * 
     * This api is to reset a document which cleans up all the previous revisions and makes the current document as the first revision.
     * 
     * @param id Document sysId
     * @param docFullname Document Full name
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/revision/reset", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ViewWikiRevisionDTO> resetWiki(@RequestParam("id") String id, @RequestParam("docFullname") String docFullname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ViewWikiRevisionDTO> result = new ResponseDTO<ViewWikiRevisionDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if ((StringUtils.isNotBlank(id) || StringUtils.isNotBlank(docFullname)))
            {
                ServiceWiki.resetWiki(id, docFullname, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, RESET_REV_ERR_MSG));
        }

        return result;
    }
    
    private String evaluateWikiDocument(String docFullName, String username)
    {
        String docName = docFullName;
        
        if(StringUtils.isNotBlank(docFullName) && docFullName.equalsIgnoreCase("HOMEPAGE"))
        {
            try
            {
                docName = this.userService.getUserHomepage(username);
            }
            catch (Exception e)
            {
               Log.log.error("Error getting the homepage for " + docFullName + " and User " + username);
            }
        }
        
        
        return docName;
    }

    /**
     * Returns a list of worksheet results.
     *
     * @param id workshe sys id
     * @param wiki
     * @param query
     * @param hidden
     * @param request
     * @return @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/resultmacro", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO resultMacro(@RequestParam String id, @RequestParam(defaultValue="") String wiki, @ModelAttribute QueryDTO query, @RequestParam("hidden") Boolean hidden, 
                    @RequestParam(defaultValue="", required = false) String macrotype, HttpServletRequest request) throws ServletException, IOException
        {
        ResponseDTO result = new ResponseDTO<Object>();
        query.getParams().put(Constants.MACRO_TYPE, macrotype);
        
        String activityId = (String)request.getParameter(Constants.ACTIVITY_ID);
        if(StringUtils.isNotEmpty(activityId)) {
            query.getParams().put(Constants.ACTIVITY_ID, activityId.trim());
        }
        
        try
        {
            if (StringUtils.isNotBlank(id))
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                result = ServiceWorksheet.serveResultMacro(id, wiki, query, hidden, username);
            }
            result.setSuccess(true);
            /*
            if (result.getTotal() > 0)
            {
                // Get the page token for /resolve/service/result/task/detail href in Result link in UI
                
                String[] taskDetailPageToken = JspUtils.getCSRFTokenForPage(request, TASK_DETAIL_LINK_URI_IN_UI);
                
                if (taskDetailPageToken != null && taskDetailPageToken.length == 2)
                {
                    result.setMessage(taskDetailPageToken[0] + ":" + taskDetailPageToken[1]);
                    Log.log.debug("Page Token name:value for " + TASK_DETAIL_LINK_URI_IN_UI + " link in UI is [" + result.getMessage() + "]");
                }
            }*/
        }
        catch (Exception e)
        {
            if (!e.getMessage().startsWith("User does not belongs to"))
            {
                Log.log.error("Error " + e.getLocalizedMessage() + " while getting worksheet results for worksheet with id " + id, e);
                result.setSuccess(false).setMessage("Error while getting worksheet results.");
            }
            else
            {
                Log.log.info("Error " + e.getLocalizedMessage() + 
                             " while getting worksheet results for worksheet with id " + id);
                result.setSuccess(false).setMessage("User does not belongs to worksheet's Orgnaization");
            }
        }
        return result;
    }
    
    /**
     * check the existence of tasks
     * @param xml
     * @return
     * @throws DocumentException
     * @throws SAXException 
     */
    @SuppressWarnings("unchecked")
    private Map<String,List<String>> validateAutomationModelXml(String xml) throws DocumentException, SAXException
    {
        List<String> runbookNotFound = new ArrayList<String>();
        List<String> tasksNotFound = new ArrayList<String>();
        List<String> documentsNotFound = new ArrayList<String>();
        List<String> invalidEdges = new ArrayList<String>();
        List<String> danglingNodes = new ArrayList<String>();
        Map<String,List<String>> notFound = new TreeMap<String,List<String>>();
        notFound.put("task", tasksNotFound);
        notFound.put("runbook", runbookNotFound);
        notFound.put("document",documentsNotFound);
        notFound.put("invalidEdges", invalidEdges);
        notFound.put("danglingNodes", danglingNodes);
        
        Map<String, String> id2NodeTypeMap = new HashMap<String, String>();
        Map<String, String> id2NodeLabelMap = new HashMap<String, String>();
        Map<String, String> id2NodeDescMap = new HashMap<String, String>();
        Map<String, Integer> idAsSource = new HashMap<String, Integer>();
        Map<String, Integer> idAsTarget = new HashMap<String, Integer>();
        
        if(StringUtils.isNotBlank(xml))
        {
            SAXReader reader = new SAXReader();
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            reader.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    return new InputSource(new ByteArrayInputStream(new byte[0]));
                }
            });
            Document doc = reader.read(new ByteArrayInputStream(xml.getBytes()));
            //Document doc = DocumentHelper.parseText(xml);
            List<Node> list = doc.selectNodes("//Task");
            String description = "";
            Node name = null;
            String taskName = "";
            
            for(Node task:list)
            {
                taskName="";
                description = task.valueOf("@description");
                String[] splits = description.split("\\?");
                if(splits.length>=1&&splits[0].contains("#"))
                {
                    taskName=splits[0];
                }
                name = task.selectSingleNode(".//name");
                if(name!=null)
                {
                    taskName = name.getText().trim();
                }
                if(ServiceHibernate.getResolveActionTask(null, taskName, "system")==null)
                {
                    tasksNotFound.add(task.valueOf("@label") + "[id:" + task.valueOf("@id") + "]");
                }
                
                id2NodeTypeMap.put(task.valueOf("@id"), "Task");
                
                if (StringUtils.isNotBlank(task.valueOf("@label")))
                {
                    id2NodeLabelMap.put(task.valueOf("@id"), task.valueOf("@label"));
                }
                
                if (StringUtils.isNotBlank(task.valueOf("@description")))
                {
                    id2NodeDescMap.put(task.valueOf("@id"), task.valueOf("@description"));
                }
                
                idAsSource.put(task.valueOf("@id"), new Integer(0));
                idAsTarget.put(task.valueOf("@id"), new Integer(0));
            }
            
            list = doc.selectNodes("//Subprocess");
            
            String runbookName = "";
            for(Node runbook:list)
            {
            	// Check if description field is populated (RBA-13533). If not, fall back to label field for Wiki Name.
            	runbookName = runbook.valueOf("@description");
            	if (StringUtils.isBlank(runbookName))
            	{
            		runbookName = runbook.valueOf("@label");
            	}
                if(WikiUtils.getWikiDoc(runbookName)==null)
                {
                    runbookNotFound.add(runbookName + " [id:" + runbook.valueOf("@id") + "]");
                }
                
                id2NodeTypeMap.put(runbook.valueOf("@id"), "Subprocess");
                
                if (StringUtils.isNotBlank(runbook.valueOf("@label")))
                {
                    id2NodeLabelMap.put(runbook.valueOf("@id"), runbook.valueOf("@label"));
                }
                
                if (StringUtils.isNotBlank(runbook.valueOf("@description")))
                {
                    id2NodeDescMap.put(runbook.valueOf("@id"), runbook.valueOf("@description"));
                }
                
                idAsSource.put(runbook.valueOf("@id"), new Integer(0));
                idAsTarget.put(runbook.valueOf("@id"), new Integer(0));
            }
            
            list = doc.selectNodes("//Document");
            
            String documentName = "";
            for(Node docNode:list)
            {
                documentName = docNode.valueOf("@label");
                if(WikiUtils.getWikiDoc(documentName)==null)
                {
                    documentsNotFound.add(documentName + " [id:" + docNode.valueOf("@id") + "]");
                }
                
                id2NodeTypeMap.put(docNode.valueOf("@id"), "Document");
                
                if (StringUtils.isNotBlank(docNode.valueOf("@label")))
                {
                    id2NodeLabelMap.put(docNode.valueOf("@id"), docNode.valueOf("@label"));
                }
                
                if (StringUtils.isNotBlank(docNode.valueOf("@description")))
                {
                    id2NodeDescMap.put(docNode.valueOf("@id"), docNode.valueOf("@description"));
                }
                
                idAsTarget.put(docNode.valueOf("@id"), new Integer(0));
            }
            
            /*
             * If Start element exists then at least one edge exists 
             * with source id equal to Start task id.
             * 
             * If End/Document elements exists then every End/Dociument element 
             * has at least edge with target id equal to End task/Document id.
             * 
             * For all elements other than Start/End/Document at least one edge
             * has source id equals to element id and at least one edge has target
             * id equal to element id.
             */
            
            list = doc.selectNodes("/mxGraphModel/root/Start");
            
            if (list != null && !list.isEmpty())
            {
                for (Node node : list)
                {
                    String nodeId = node.valueOf("@id");
                    
                    id2NodeTypeMap.put(nodeId, "Start");
                    
                    String label = node.valueOf("@label");
                    
                    if (StringUtils.isNotBlank(label))
                    {
                        id2NodeLabelMap.put(nodeId, label);
                    }
                    
                    String desc = node.valueOf("@description");
                    
                    if (StringUtils.isNotBlank(desc))
                    {
                        id2NodeDescMap.put(nodeId, desc);
                    }
                    
                    idAsSource.put(nodeId, new Integer(0));
                }
            }
            
            list = doc.selectNodes("/mxGraphModel/root/End");
            
            if (list != null && !list.isEmpty())
            {
                for (Node node : list)
                {
                    String nodeId = node.valueOf("@id");
                    
                    id2NodeTypeMap.put(nodeId, "End");
                    
                    String label = node.valueOf("@label");
                    
                    if (StringUtils.isNotBlank(label))
                    {
                        id2NodeLabelMap.put(nodeId, label);
                    }
                    
                    String desc = node.valueOf("@description");
                    
                    if (StringUtils.isNotBlank(desc))
                    {
                        id2NodeDescMap.put(nodeId, desc);
                    }
                    
                    idAsTarget.put(nodeId, new Integer(0));
                }
            }
            
            list = doc.selectNodes("/mxGraphModel/root/Event");
            
            if (list != null && !list.isEmpty())
            {
                for (Node node : list)
                {
                    String nodeId = node.valueOf("@id");
                    
                    id2NodeTypeMap.put(nodeId, "Event");
                    
                    String label = node.valueOf("@label");
                    
                    if (StringUtils.isNotBlank(label))
                    {
                        id2NodeLabelMap.put(nodeId, label);
                    }
                    
                    String desc = node.valueOf("@description");
                    
                    if (StringUtils.isNotBlank(desc))
                    {
                        id2NodeDescMap.put(nodeId, desc);
                    }
                    
                    idAsSource.put(nodeId, new Integer(0));
                    idAsTarget.put(nodeId, new Integer(0));
                }
            }
            
            list = doc.selectNodes("/mxGraphModel/root/Precondition");
            
            if (list != null && !list.isEmpty())
            {
                for (Node node : list)
                {
                    String nodeId = node.valueOf("@id");
                    
                    id2NodeTypeMap.put(nodeId, "Pre-Condition");
                    
                    String label = node.valueOf("@label");
                    
                    if (StringUtils.isNotBlank(label))
                    {
                        id2NodeLabelMap.put(nodeId, label);
                    }
                    
                    String desc = node.valueOf("@description");
                    
                    if (StringUtils.isNotBlank(desc))
                    {
                        id2NodeDescMap.put(nodeId, desc);
                    }
                    
                    idAsSource.put(nodeId, new Integer(0));
                    idAsTarget.put(nodeId, new Integer(0));
                }
            }
            
            list = doc.selectNodes("/mxGraphModel/root/Question");
            
            if (list != null && !list.isEmpty())
            {
                for (Node node : list)
                {
                    String nodeId = node.valueOf("@id");
                    
                    id2NodeTypeMap.put(nodeId, "Question");
                    
                    String label = node.valueOf("@label");
                    
                    if (StringUtils.isNotBlank(label))
                    {
                        id2NodeLabelMap.put(nodeId, label);
                    }
                    
                    String desc = node.valueOf("@description");
                    
                    if (StringUtils.isNotBlank(desc))
                    {
                        id2NodeDescMap.put(nodeId, desc);
                    }
                    
                    idAsSource.put(nodeId, new Integer(0));
                    idAsTarget.put(nodeId, new Integer(0));
                }
            }
            
            list = doc.selectNodes("//Edge");
            
            String edgeId;
            
            for (Node edgeNode : list)
            {
                edgeId = edgeNode.valueOf("@id");
                
                Node mxCellNode = edgeNode.selectSingleNode("./mxCell");
                
                if (mxCellNode == null || StringUtils.isBlank(mxCellNode.valueOf("@source")) ||
                    StringUtils.isBlank(mxCellNode.valueOf("@target")))
                {
                    invalidEdges.add("Invalid Edge [id:" + edgeNode.valueOf("@id") + ", missing source or target]");
                }
                
                if (StringUtils.isNotBlank(mxCellNode.valueOf("@source")) && idAsSource.containsKey(mxCellNode.valueOf("@source")))
                {
                    idAsSource.put(mxCellNode.valueOf("@source"), 
                                   new Integer(idAsSource.get(mxCellNode.valueOf("@source")).intValue() + 1));
                }
                
                if (StringUtils.isNotBlank(mxCellNode.valueOf("@target")) && idAsTarget.containsKey(mxCellNode.valueOf("@target")))
                {
                    idAsTarget.put(mxCellNode.valueOf("@target"), 
                                   new Integer(idAsTarget.get(mxCellNode.valueOf("@target")).intValue() + 1));
                }
            }
            
            // Check for hanging nodes
            
            for (String srcId : idAsSource.keySet())
            {
                if (idAsSource.get(srcId).intValue() == 0)
                {
                    String prefix;
                    
                    if (idAsTarget.containsKey(srcId) && idAsTarget.get(srcId).intValue() == 0)
                    {
                        prefix = "No edge terminates to and starts from ";
                        
                    }
                    else
                    {
                        prefix = "No edge starts from ";
                    }
                    
                    danglingNodes.add(prefix + id2NodeTypeMap.get(srcId) + 
                                      " [id=" + srcId + (id2NodeLabelMap.containsKey(srcId) ? ", label=" + id2NodeLabelMap.get(srcId) : "") + 
                                      (id2NodeDescMap.containsKey(srcId) ? ", description=" + id2NodeDescMap.get(srcId) : "") + 
                                      "]");
                }
            }
            
            for (String targetId : idAsTarget.keySet())
            {
                if (idAsTarget.get(targetId).intValue() == 0)
                {
                    if (!idAsSource.containsKey(targetId) ||
                        (idAsSource.containsKey(targetId) && idAsSource.get(targetId).intValue() != 0))
                    {
                        danglingNodes.add("No edge terminates to " + id2NodeTypeMap.get(targetId) + 
                                          " [id=" + targetId + (id2NodeLabelMap.containsKey(targetId) ? ", label=" + id2NodeLabelMap.get(targetId) : "") + 
                                          (id2NodeDescMap.containsKey(targetId) ? ", description=" + id2NodeDescMap.get(targetId) : "") + 
                                          "]");
                    }
                }
            }
        }
        
        return notFound;
    }
    
    private void buildErrorMessage(StringBuilder sb, String model, Map<String,List<String>> result)
    {
        if (!result.get("task").isEmpty() || !result.get("runbook").isEmpty() ||
            !result.get("document").isEmpty() || !result.get("invalidEdges").isEmpty() ||
            !result.get("danglingNodes").isEmpty())
        {
            sb.append("\n" + model + " Model Errors:\n");
            
            if (!result.get("task").isEmpty())
            {
                sb.append("\n# of Task(s) Not Found (" + result.get("task").size() + "):");
                
                for (String notFoundTaskName : result.get("task"))
                {
                    sb.append("\n" + notFoundTaskName);
                }
            }
            
            if (!result.get("runbook").isEmpty())
            {
                sb.append("\n# of Runbook(s) Not Found (" + result.get("runbook").size() + "):");
                
                for (String notFoundRunbookName : result.get("runbook"))
                {
                    sb.append("\n" + notFoundRunbookName);
                }
            }
            
            if (!result.get("document").isEmpty())
            {
                sb.append("\n# of Document(s) Not Found (" + result.get("document").size() + "):");
                
                for (String notFoundDocumentName : result.get("document"))
                {
                    sb.append("\n" + notFoundDocumentName);
                }
            }
            
            if (!result.get("invalidEdges").isEmpty())
            {
                sb.append("\n# of Invalid Edge(s) Found (" + result.get("invalidEdges").size() + "):");
                
                for (String invalidEdge : result.get("invalidEdges"))
                {
                    sb.append("\n" + invalidEdge);
                }
            }
            
            if (!result.get("danglingNodes").isEmpty())
            {
                sb.append("\n# of Non-Connected Node(s) Found (" + result.get("danglingNodes").size() + "):");
                
                for (String danglingNode : result.get("danglingNodes"))
                {
                    sb.append("\n" + danglingNode);
                }
            }
        }
    }
    
    /**
     * 
     * @param main
     * @param abort
     * @param decisionTree
     * @param request
     * @return @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/model/validate", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Map<String,List<String>>>> validateModel(@RequestParam("main") String main, @RequestParam("abort") String abort, @RequestParam("decisionTree") String decisionTree, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Map<String,List<String>>>> result = new ResponseDTO<Map<String, Map<String,List<String>>>>();
        try
        {
            Map<String, Map<String,List<String>>> validation = new TreeMap<String,Map<String,List<String>>>();
            validation.put("main", this.validateAutomationModelXml(main));
            validation.put("abort",this.validateAutomationModelXml(abort));
            validation.put("decisionTree",this.validateAutomationModelXml(decisionTree));
            result.setData(validation);
            // RBA-15127 : RS-24987 URGENT Resolve Bug: Can't save automation with unconnected tasks
            /*
            result.setSuccess(validation.get("main").get("task").isEmpty() && 
                              validation.get("main").get("runbook").isEmpty() &&
                              validation.get("main").get("document").isEmpty() &&
                              validation.get("main").get("invalidEdges").isEmpty() &&
                              validation.get("main").get("danglingNodes").isEmpty() &&
                              validation.get("abort").get("task").isEmpty() && 
                              validation.get("abort").get("runbook").isEmpty() &&
                              validation.get("abort").get("document").isEmpty() &&
                              validation.get("abort").get("invalidEdges").isEmpty() &&
                              validation.get("abort").get("danglingNodes").isEmpty() &&
                              validation.get("decisionTree").get("task").isEmpty() && 
                              validation.get("decisionTree").get("runbook").isEmpty() &&
                              validation.get("decisionTree").get("document").isEmpty() &&
                              validation.get("decisionTree").get("invalidEdges").isEmpty() &&
                              validation.get("decisionTree").get("danglingNodes").isEmpty());
            
            if (!result.isSuccess())
            {
                StringBuilder sb = new StringBuilder();
                
                buildErrorMessage(sb, "main", validation.get("main"));
                buildErrorMessage(sb, "abort", validation.get("abort"));
                buildErrorMessage(sb, "decisionTree", validation.get("decisionTree"));
                
                result.setMessage(sb.toString());
            }
            */
            result.setSuccess(true);
            StringBuilder sb = new StringBuilder();
            buildErrorMessage(sb, "main", validation.get("main"));
            buildErrorMessage(sb, "abort", validation.get("abort"));
            buildErrorMessage(sb, "decisionTree", validation.get("decisionTree"));
            result.setMessage(sb.toString());
        }
        catch (Exception e)
        {
            Log.log.error("Error in model xml validation.", e);
            result.setSuccess(false).setMessage("Error in model xml validation.");
        }
        return result;
    }
    
    /** This api is to upload a docx file to a wiki document.
     * 
     */
    @RequestMapping(value = "/wiki/docx/uploadDocx", method = {RequestMethod.GET, RequestMethod.POST} )
    @ResponseBody
    public ResponseDTO<Map<String, Map<String,List<String>>>> uploadDocx(
                    HttpServletRequest request
                    ) throws ServletException, IOException 
    {   
        ResponseDTO<Map<String, Map<String,List<String>>>> result = ServiceWiki.convertDocxToWiki(request, 
        		RSContext.getResolveHome());
        
        return result;
    
    }
    /** This api is to encrypt parameters use when adding connection in AD.
     * 
     */
    @RequestMapping(value = "/wiki/encryptParams", method = {RequestMethod.POST, RequestMethod.GET} )
    @ResponseBody
    public ResponseDTO encryptParams(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {          
        @SuppressWarnings("rawtypes")      
        ResponseDTO result = ServiceWiki.encryptParams(jsonProperty);        
        return result;
    
    }
    
    /**
     * API to retrieve list of mock names from all the tasks present in the Wiki main model.
     * 
     * @param wikiSysId : String representing sys_id of a Wiki document 
     * @param request : {@link HttpServletRequest}
     * @return
     * 	{@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/wiki/getMockList", method = {RequestMethod.POST, RequestMethod.GET} )
    @ResponseBody
    public ResponseDTO getMockList(@RequestParam String wikiSysId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
        	List<String> mockList = new ArrayList<String>();
        	mockList.addAll(com.resolve.services.hibernate.util.WikiUtils.getModelTasksMockList(wikiSysId, username));
        	result.setSuccess(true);
        	result.setRecords(mockList);
        }
        catch(Exception e)
        {
            Log.log.error("Error: Could not get wiki model tasks mock list.", e);
            result.setSuccess(false).setMessage("Error in getting wiki model tasks mock list.");
        }
        		
        
        return result;
    
    }
    
    /**
     * API to upload custom image which will be usable in automation model.
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/wiki/uploadAutomationImage", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseDTO<Object> uploadAutomationImage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        List<FileItem> fileItems = null;
        try
        {
            fileItems = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            String imageName = null;
            FileItem item = null;
            for (FileItem fileItem : fileItems)
            {
                if (!fileItem.isFormField())
                {
                	item = fileItem;
                }
                else
                {
                	if (fileItem.getFieldName() != null)
                	{
                		if (fileItem.getFieldName().equals("imageName"))
                		{
                			imageName = fileItem.getString(); 
                		}
                	}
                }
            }
            ServiceHibernate.uploadAutomationImage(imageName, item, RSContext.getResolveHome());
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error while uploading automation image.");
        }
        return result;
    }
    
    /**
     * API to list all uploaded custom images.
     * 
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/listAutomationImages", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseDTO<String> listAutomationImages(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        try
        {
            List<String> imageNames = ServiceHibernate.listAutomationImages(RSContext.getResolveHome());
            result.setRecords(imageNames);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error while listing automation images.");
        }
        return result;
    }
    
    /**
     * Delete the custom automation image
     * 
     * @param imageName
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/deleteAutomationImage", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseDTO<String> deleteAutomationImage(@RequestParam("imageName") String imageName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        try
        {
            ServiceHibernate.deleteAutomationImage(imageName, RSContext.getResolveHome());
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error while deleting automation image.");
        }
        return result;
    }
    
    /**
     * Api to get list of Playbook templates. This api will return docs whose dislay mode is set
     * to "playbook" and is template flag is true.
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link WikiDocumentVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     *  
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} with success true and records set to List of {@link WikiDocumentVO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/listPlaybookTemplates", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<WikiDocumentVO> listPlaybookTemplates(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<WikiDocumentVO> result = new ResponseDTO<WikiDocumentVO>();
        
        try
        {
            query.setModelName("WikiDocument");
            query.setSelectColumns("sys_id,UName,UFullname,UNamespace,USummary,UIsActive,UIsLocked,UIsHidden,UDisplayMode,UIsTemplate,UVersion,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
            query.setPrefixTableAlias("wd");
            query.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE,"false","UIsDeleted",QueryFilter.EQUALS));
            
            //get the data
            long timestart = System.currentTimeMillis();
            List<WikiDocumentVO> data = ServiceWiki.getWikiDocuments(query, NodeType.PLAYBOOK_TEMPLATE, username);
            System.out.println("time taken to get the list of undeleted playbook templates: " + (System.currentTimeMillis() - timestart));
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(data).setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Playbook templates.", e);
            result.setSuccess(false).setMessage("Error retrieving Playbook templates.");
        }

        return result;
    }
    
    /**
     * Api to get the attachments for the wiki template associated with SIR either by sysId or by Document name
     * of template associated with given SIR.
     * 
     * <p> 
     * To get the list of Global attachments, pass the 'docFullName = System.Attachment' to this api
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link AttachmentDTO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     * 
     * @param incidentId Incident Uniqueue Id (guid)
     * @param sir Security Incident Id (e.g. SIR-.....)
     * @param docSysId Document sysId
     * @param docFullName Document full name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return @return {@link ResponseDTO} with success true and records set to List of {@link AttachmentDTO} if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/getSIRAttachments", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<AttachmentDTO> getSIRAttachments(@RequestParam String incidentId, @RequestParam String sir, @RequestParam String docSysId, @RequestParam String docFullName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<AttachmentDTO> result = new ResponseDTO<AttachmentDTO>();

        try
        {
            ResolveSecurityIncidentVO rsSIRVO = null;
            String verfiedDocFullName = docFullName;
            
            if (StringUtils.isNotBlank(incidentId) || StringUtils.isNotBlank(sir))
            {
                rsSIRVO = PlaybookUtils.getSecurityIncident(incidentId, sir, username);
            }
            else
            {
                throw new Exception("SIR id and Sysid is empty from the UI.");
            }
            
            if (rsSIRVO != null)
            {
                if (StringUtils.isNotBlank(rsSIRVO.getPlaybook()))
                {
                    if (StringUtils.isBlank(docFullName) && StringUtils.isBlank(docSysId))
                    {
                        throw new Exception("Document name and Sysid is empty from the UI.");
                    }
                    
                    if (StringUtils.isBlank(verfiedDocFullName))
                    {
                        verfiedDocFullName = com.resolve.services.hibernate.util.WikiUtils.getWikidocFullName(docSysId);
                    }
                    
                    if (rsSIRVO.getPlaybook().equalsIgnoreCase(verfiedDocFullName))
                    {
                        String correctDocumentName = evaluateWikiDocument(docFullName, username);
                        List<AttachmentDTO> attachments = ServiceWiki.getAllAttachmentsFor(docSysId, correctDocumentName, "system");
                        result.setSuccess(true).setRecords(attachments);
                    }
                }
                else
                {
                    result.setSuccess(true).setTotal(0).setRecords(new ArrayList<AttachmentDTO>());
                }
            }
            else
            {
                throw new Exception("Could not find security incident for specified incident sys id or incident id.");
            }
        }
        catch (Exception e)
        {
            String error = "Error in getting the attachment(s) for doc: " + docFullName + " or sysId: " + docSysId;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }
    
}