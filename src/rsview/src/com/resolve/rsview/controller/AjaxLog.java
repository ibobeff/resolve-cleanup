package com.resolve.rsview.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.RSLogUtil;

/**
 * An RSView Controller to handle the request(s) to ask RSLog to collect the log files
 * as per given criteria and zip them up in a single, log.zip, and return it to the caller.
 *
 */
@Controller
public class AjaxLog extends GenericController
{
    /**
     * An API to receive log.zip file, which is a collection of different log files
     * based on the given criteria, collected at the cenral localtion.
     *  
     * @param compGUID : String representing GUID of the component whoes log is needed.
     * @param component : Optional String representing the component name. If not provided, all the files from given GUID will be considered. 
     * @param from : Optional Long representing start time from where log files are requested. If not provided, it will consider files from the start of the time.
     * @param to : Optional Long representing end time upto which the log files are requested. If not provided, all the files upto current time are considered.
     *          If both, from and to, are not provided, all the files will be considered.
     * @param request : HttpServletRequest
     * @param response : HttpServletResponse
     * @throws ServletException
     */
    
    @RequestMapping(value = {"/getLog"}, method = {RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void getLog(@RequestParam String compGUID, @RequestParam (required = false, defaultValue = "") String component,
                    @RequestParam (required = false) Long from, @RequestParam (required = false) Long to,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (UserUtils.isAdminUser(username)) {
            try {
                RSLogUtil.getLog(compGUID, component, from, to, response);
            } catch (Exception e) {
                Log.log.error("Error while getting log.zip file.", e);
            }
        }
    }
}
