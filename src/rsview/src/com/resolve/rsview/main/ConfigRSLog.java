package com.resolve.rsview.main;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigRSLog extends ConfigMap
{
    private static final long serialVersionUID = -1886868355934197268L;
    
    private String host;
    private String port;
    
    public ConfigRSLog(XDoc config)
    {
        super(config);

        define("host", STRING, "./RSLOG/@HOST");
        define("port", STRING, "./RSLOG/@PORT");
    }
    
    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    @Override
    public void load() throws Exception
    {
        loadAttributes();
    }
    
    @Override
    public void save()
    {
        saveAttributes();
    }

}
