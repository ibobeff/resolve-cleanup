package com.resolve.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.codehaus.jackson.map.ObjectMapper;
import org.owasp.esapi.ESAPI;

import com.resolve.persistence.model.ResolveRegistrationProperty;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.hibernate.util.ResolveRegistrationUtil;
import com.resolve.services.hibernate.vo.ResolveRegistrationPropertyVO;

public class RSLogUtil
{
    private static String host;
    private static String port;
    
    static {
        host = RSContext.getMain().getConfigRSLog().getHost();
        port = RSContext.getMain().getConfigRSLog().getPort();
    }
    
    public static void getLog(String compGUID, String component, Long from, Long to, HttpServletResponse response) throws Exception {
        
        if (StringUtils.isBlank(host) || StringUtils.isBlank(port)) {
            throw new Exception("RSRlog host and/or port cannot be epmty.");
        }
        
        if (StringUtils.isBlank(compGUID)) {
            throw new IllegalArgumentException ("Missing required Component GUID");
        }
        
        if (StringUtils.isBlank(component)) {
            component = "";
        }
        
        StringBuilder reqParams = new StringBuilder("compGUID=").append(compGUID);
        
        if (StringUtils.isNotBlank(component)) {
            reqParams.append("&component=").append(component);
        }
        if (from != null) {
            reqParams.append("&from=").append(from);
        }
        
        if (to != null) {
            reqParams.append("&to=").append(to);
        }
        
        byte[] postData = reqParams.toString().getBytes();
        
        String urlString = String.format("http://%s:%s/getLog", host, port);
        HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
        postHttpRequestData(conn, postData);
        int returnCode = conn.getResponseCode();
        final long time;
        synchronized (RSLogUtil.class) {
            time = new Date().getTime();
        }
        if (returnCode == 200) {
            File zipFile = readZipFileFromResponse(conn, time);
            getAndZipResolveProperties(compGUID, time);
            writeZipFileToResponse(zipFile, response);
        }
        else {
            writeErrorToResponse(returnCode, conn, response);
        }
        // clean temp files from tmp folder
        cleanTmpFolder(time);
    }
    
    @SuppressWarnings("unchecked")
    private static void getAndZipResolveProperties(String GUID, long time) {
        Map<String, Object> resolvePropertiesMap = ResolveRegistrationUtil.findRegistrationPropertiesForLogging(GUID);
        if (resolvePropertiesMap != null) {
            String fileName = String.format("%stmp/%d-sys-info.log", RSContext.getResolveHome(), time);
            File file = new File(fileName);
            try {
                if (resolvePropertiesMap.containsKey("VER")) {
                    ResolveRegistrationPropertyVO ver = (ResolveRegistrationPropertyVO)resolvePropertiesMap.get("VER");
                    FileUtils.writeStringToFile(file, ver.getUName() + "=" + ver.getUValue() + "\n", StandardCharsets.UTF_8);
                }
                final List<String> sysProperties = new ArrayList<>();
                if (resolvePropertiesMap.containsKey("SYS")) {
                    sysProperties.add("");
                    sysProperties.add("################### System Properties ###################");
                    sysProperties.add("");
                    List<ResolveRegistrationProperty> systemProperties = (List<ResolveRegistrationProperty>)resolvePropertiesMap.get("SYS");
                    if (CollectionUtils.isNotEmpty(systemProperties)) {
                        systemProperties.parallelStream().forEach(sysProperty -> {
                            if (sysProperty.getUName().equals("line.separator")) {
                                switch (sysProperty.getUValue())
                                {
                                    case "\n" : sysProperty.setUValue(sysProperty.getUValue().replace("\n", "\\n")); break;
                                    case "\r" : sysProperty.setUValue(sysProperty.getUValue().replace("\r", "\\r")); break;
                                    case "\r\n" : sysProperty.setUValue(sysProperty.getUValue().replace("\r\n", "\\r\\n")); break;
                                    default : ;
                                }
                            }
                            sysProperties.add((sysProperty.getUName() + "=" + sysProperty.getUValue()));
                        });
                        FileUtils.writeLines(file, sysProperties, true);
                        sysProperties.clear();
                    }
                }
                if (resolvePropertiesMap.containsKey("ENV")) {
                    sysProperties.add("");
                    sysProperties.add("################### Environment Properties ###################");
                    sysProperties.add("");
                    List<ResolveRegistrationProperty> envProperties = (List<ResolveRegistrationProperty>)resolvePropertiesMap.get("ENV");
                    if (CollectionUtils.isNotEmpty(envProperties)) {
                        envProperties.parallelStream().forEach(sysProperty -> {
                            sysProperties.add((sysProperty.getUName() + "=" + sysProperty.getUValue()));
                        });
                        FileUtils.writeLines(file, sysProperties, true);
                    }
                }
                zipIt(GUID, time);
            } catch(Exception e) {
                Log.log.error("Error while writing system properties file.", e);
            }
        }
    }
    
    /*
     * Appending a new file in an existing zip file is nothing but creating a
     * new zip file, adding all the files from existing zip to the new one
     * and adding a new file.
     */
    private static void zipIt(String GUID, long time) {
        File logFile = null;
        File zipFile = null;
        ZipFile oldZipFile = null;
        ZipOutputStream zip_output = null;
        
        try {
            String fileName = String.format("%stmp/%d-sys-info.log", RSContext.getResolveHome(), time);
            logFile = new File (fileName);
            if (logFile.exists()) {
                zipFile = new File (String.format("%stmp/%d.zip", RSContext.getResolveHome(), time));
                
                long localTime = 0L;
                synchronized (RSLogUtil.class) {
                    localTime = new Date().getTime();
                }
                File newZipFile = new File(String.format("%stmp/%d.zip", RSContext.getResolveHome(), localTime));
                
                oldZipFile = new ZipFile(zipFile);
                zip_output = new ZipOutputStream(new FileOutputStream(newZipFile));
                
                // read all entries from old zip file and copy them to the new one.
                Enumeration<? extends ZipEntry> entries = oldZipFile.entries();
                byte[] BUFFER = new byte[4096 * 1024];
                while (entries.hasMoreElements()) {
                    ZipEntry e = entries.nextElement();
                    zip_output.putNextEntry(e);
                    if (!e.isDirectory()) {
                        int bytesRead;
                        InputStream input = oldZipFile.getInputStream(e);
                        while ((bytesRead = input.read(BUFFER))!= -1) {
                            zip_output.write(BUFFER, 0, bytesRead);
                        }
                        input.close();
                    }
                    zip_output.closeEntry();
                }
                
                // copy sys-info log file to the new zip file.
                ZipEntry newZipEntry = new ZipEntry(String.format("%s_sys-info.log", GUID));
                zip_output.putNextEntry(newZipEntry);
                
                IOUtils.copy(new FileInputStream(logFile), zip_output);
                zip_output.closeEntry();
                zip_output.close();
                
                // Delete original file
                logFile.delete();
                zipFile.delete();
                // rename new zip file to the old zip file name.
                newZipFile.renameTo(zipFile);
            }
        } catch (Exception e) {
            Log.log.error("Error while adding system info log file to zip.", e);
        } finally {
            try {
                if (oldZipFile != null) {
                    oldZipFile.close();
                }
            } catch(Exception e) {
                // Do nothing here.
            }
        }
    }
    
    private static void cleanTmpFolder(long time) {
        List<String> fileWildCardList = new ArrayList<String>();
        fileWildCardList.add(time + "*.zip");
        FileFilter fileFilter = new WildcardFileFilter(fileWildCardList);
        File dir = new File(RSContext.getResolveHome() + "tmp/");
        
        File[] files = dir.listFiles(fileFilter);
        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public static Map<String, Object> listLogFiles(List<String> guids, List<String> components, Long from, Long to, boolean needRemoteLogs) throws Exception {
        if (CollectionUtils.isEmpty(guids)) {
            throw new Exception ("At least one GUID is needed to get the log file listing");
        }
        
        StringBuilder reqParams = new StringBuilder("guids=");
        reqParams.append(String.join(",", guids));
        
        reqParams.append("&components=").append(String.join(",", components));
        
        if (from != null) {
            reqParams.append("&from=").append(from);
        }
        if (to != null) {
            reqParams.append("&to=").append(to);
        }
        reqParams.append("&needRemoteLogs=").append(needRemoteLogs);
        
        byte[] postData = reqParams.toString().getBytes();
        
        String urlString = String.format("http://%s:%s/listLogFiles", host, port);
        HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
        
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty( "charset", "utf-8");
        conn.setRequestProperty( "Content-Length", Integer.toString( postData.length ));
        conn.getOutputStream().write(postData);
        conn.getOutputStream().flush();
        conn.getOutputStream().close();
        
        int returnCode = conn.getResponseCode();
        String response = null;
        if (returnCode == 200) {
            response = readResponse(conn, false);
        } else {
            response = readResponse(conn, true);
            throw new Exception(response);
        }
        return new ObjectMapper().readValue(response, Map.class);
    }
    
    /*
     * Read the response either from input or an error stream
     */
    private static String readResponse(HttpURLConnection conn, boolean error) throws IOException {
        InputStream connectionIn = null;
        if (error)
            connectionIn = conn.getErrorStream();
        else
            connectionIn = conn.getInputStream();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(connectionIn));
        StringBuilder resStringBuilder = new StringBuilder();
        String inputLine;
        while ((inputLine = buffer.readLine()) != null)
            resStringBuilder.append(inputLine);
        buffer.close();
        if (connectionIn != null)
            connectionIn.close();
        return resStringBuilder.toString();
    }
    
    public static void downloadLogFiles(List<String> fileNames, HttpServletResponse response) throws Exception {
        Set<String> guids = new HashSet<>();
        if (CollectionUtils.isNotEmpty(fileNames)) {
            fileNames.stream().forEach(fileName -> {
                String guid = fileName.split("_")[0];
                if (!guids.contains(guid)) 
                    guids.add(guid);
            });
            
            StringBuilder reqParams = new StringBuilder("files=");
            reqParams.append(String.join(",", fileNames));
            
            byte[] postData = reqParams.toString().getBytes();
            
            String urlString = String.format("http://%s:%s/downloadLogFiles", host, port);
            HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
            postHttpRequestData(conn, postData);
            int returnCode = conn.getResponseCode();
            if (returnCode == 200) {
                final long time;
                synchronized (RSLogUtil.class) {
                    time = new Date().getTime();
                }
                File zipFile = readZipFileFromResponse(conn, time);
                guids.stream().forEach(guid -> {
                    getAndZipResolveProperties(guid, time);
                });
                writeZipFileToResponse(zipFile, response);
                cleanTmpFolder(time);
            }else {
                writeErrorToResponse(returnCode, conn, response);
            }
        }
    }
    
    private static void postHttpRequestData(HttpURLConnection conn, byte[] postData) throws Exception {
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty( "charset", "utf-8");
        conn.setRequestProperty( "Content-Length", Integer.toString( postData.length ));
        
        if (!ESAPI.validator().isValidFileContent("Posting HTTP Request Data to RSLog", postData, postData.length, false)) {
        	throw new Exception("Detected possible intrusion attempt in posting data to RSLog");
        }
        
        conn.getOutputStream().write(postData);
        conn.getOutputStream().flush();
        conn.getOutputStream().close();
    }
    
    private static File readZipFileFromResponse(HttpURLConnection conn, long time) throws IOException {
        InputStream connectionIn = conn.getInputStream();
        File zipFile = new File(RSContext.getResolveHome() + "tmp/" + time + ".zip");
        FileOutputStream fos = new FileOutputStream(zipFile);
        byte[] buffer = new byte[4096];
        int length; 
        while((length = connectionIn.read(buffer)) > 0) {
            fos.write(buffer, 0, length);
        }
        fos.close();
        connectionIn.close();
        return zipFile;
    }
    
    private static void writeZipFileToResponse(File zipFile, HttpServletResponse response) throws Exception {
        String headerValue = "attachment;filename=\"log.zip\"";
        response.setHeader("Content-Disposition", headerValue);
        response.setHeader("Content-Type", "application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip;name=\"log.zip\"");
        FileInputStream fis = new FileInputStream(zipFile);
        byte[] fileContent = new byte[1024*8];
        while (true) {
            int len = fis.read(fileContent);
            if (len <= 0) {
                break;
            }
            response.getOutputStream().write(fileContent, 0, len);
        }
        fis.close();
    }
    
    private static void writeErrorToResponse(int returnCode, HttpURLConnection conn, HttpServletResponse response) throws IOException {
        InputStream connectionIn = conn.getErrorStream();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(connectionIn));
        StringBuilder errorString = new StringBuilder();
        String inputLine;
        while ((inputLine = buffer.readLine()) != null)
            errorString.append(inputLine);
        buffer.close();
        response.sendError(returnCode, errorString.toString());
        connectionIn.close();
    }
}
