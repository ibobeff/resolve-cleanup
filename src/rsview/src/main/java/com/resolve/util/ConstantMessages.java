package com.resolve.util;

public class ConstantMessages {
	
	public static final String ERROR_PERFORMING_SEARCH = "Error performing search.";
	public static final String ERROR_SEARCHING_WITH_TERM = "Please specify a valid search term.";
}
