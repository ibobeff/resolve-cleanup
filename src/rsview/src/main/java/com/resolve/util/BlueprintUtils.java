package com.resolve.util;

import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.LockMode;
import org.hibernate.Session;

import com.resolve.persistence.dao.ResolveBlueprintDAO;
import com.resolve.persistence.model.ResolveBlueprint;
import com.resolve.persistence.model.ResolveSharedObject;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;

public class BlueprintUtils {
    
    public static Map<String, Object> readBlueprint(List<String> guids,
                                     List<String> gatewayTypes,
                                     String username) throws Exception {
        if (CollectionUtils.isEmpty(guids)) {
            throw new Exception ("At least one GUID is needed to read a blueprint file");
        }
        
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("readBlueprint API is called with the guids: %s and gateway types: %s", guids, gatewayTypes));
        }
        final Map<String, Object> result = new HashMap<>();
        guids.stream().forEach(guid -> {
            ResolveBlueprint resolveRegistration = new ResolveBlueprint();
            resolveRegistration.setUGuid(guid);
            
            try {
            	ResolveBlueprint searchedResolveRegistration = resolveRegistration;
                  HibernateProxy.setCurrentUser(username);
            	resolveRegistration = (ResolveBlueprint) HibernateProxy.execute(() -> {
            		ResolveBlueprintDAO dao = HibernateUtil.getDAOFactory().getResolveBlueprintDAO();
                	return dao.findFirst(searchedResolveRegistration);
            	});
            } catch (Throwable t) {
                Log.log.error(String.format("Error while reading blueprint file for a guid: %s", guid), t);
                                                    HibernateUtil.rethrowNestedTransaction(t);
            }
            
            if (resolveRegistration != null) {
                String blueprint = resolveRegistration.getUBlueprint();
                try {
                    Map<String, Object> propertiesMap = readProperties(blueprint, gatewayTypes);
                    whoHasBpLock(guid, propertiesMap, username);
                    result.put(guid, propertiesMap);
                }
                catch (Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        });
        return result;
    }
    
    private static Map<String, Object> readProperties (String blueprint, List<String> gatewayTypes) throws Exception {
        final Map<String, Object> propertiesMap = new HashMap<>();
        if (CollectionUtils.isEmpty(gatewayTypes)) {
            Properties bpProperties = new Properties();
            bpProperties.load(new StringReader(blueprint));
            populatePropertiesMap(bpProperties, propertiesMap);
        } else {
            String type = null;
            Predicate<String> getAllGateways = elem -> elem.toLowerCase().equals("all");
            if (gatewayTypes.stream().filter(getAllGateways).findFirst().isPresent()) {
                    /*
                 * change type = ".*" so that the regex pattern will look like
                 * '^rsremote\\.receive\\..*\\..*|^rsremote\\.run\\..*=(TRUE|FALSE))' which will retrieve all the gateway types
                     */
                type = ".*";
            } else {
                /*
                 * in the case of multiple gateway types, change type = (type1|type2|type3), so that the regex group pattern will look like:
                 * '^rsremote\\.receive\\.(type1|type2|type3)\\..*|^rsremote\\.run\\.(type1|type2|type3)=(TRUE|FALSE))', which will retrieve all the specified gateway types in one go.
                 */
                type = String.format("(%s)", String.join("|", gatewayTypes)).toLowerCase().replace("database", "db");
                }
            Pattern pattern = Pattern.compile(String.format("(^rsremote\\.receive\\.%s\\..*|^rsremote\\.run\\.%s=(TRUE|FALSE))", type, type), Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(blueprint);
                StringBuilder builder = new StringBuilder();
                while(matcher.find()) {
                    builder.append(matcher.group(1)).append("\n");
                }
                if (builder.length() > 0) {
                    Properties properties = new Properties();
                    try {
                        properties.load(new StringReader(builder.toString()));
                        populatePropertiesMap(properties, propertiesMap);
                    }
                    catch (Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                }
        }
        
        return propertiesMap;
    }
    
    private static void populatePropertiesMap(Properties properties, Map<String, Object> propertiesMap) throws Exception {
        properties.stringPropertyNames().stream().forEach(name -> {
            readPropertiesMap(propertiesMap, name, properties.get(name));
        });
    }
    
    @SuppressWarnings("unchecked")
    private static  void readPropertiesMap(Map<String, Object> map, String name, String value) {
        if (StringUtils.isNotBlank(name) && name.contains(".")) { // get in only if there are more elements to be processed.
            String element = name.split("\\.")[0]; // take the left most element
            if (map.containsKey(element)) {
                try {
                    Object valueObj = map.get(element);
                    Map<String, Object> elementMap = null;
                    if (valueObj instanceof Map) {
                        elementMap = (HashMap<String, Object>)valueObj;
                    } else {
                        /*
                         * instead of Map, we received a value which is a string
                         * So, convert it into a leaf node, where key is 'dummy' and value (valueObj, in this case).
                         */
                        elementMap = new HashMap<>();
                        elementMap.put("dummy", valueObj);
                        map.put(element, elementMap);
                    }
                    readPropertiesMap(elementMap, name.substring(name.indexOf(".") + 1, name.length()), value);
                } catch (Exception e) {
                    Log.log.error("map : " + map + ", element: " + element, e);
                }
            } else {
                Map<String, Object> elementMap = new HashMap<>();
                map.put(element, elementMap);
                readPropertiesMap(elementMap, name.substring(name.indexOf(".") + 1, name.length()), value);
            }
            
        } else {
            if (map.containsKey(name)) {
                /*
                 * If map already kas a key (name), do not over write it with a String.
                 * Read it, and if it's a map, add a dummy key and a value.
                 */
                Object valueObj = map.get(name);
                if (valueObj instanceof Map) {
                    Map<String, Object> elementMap = (HashMap<String, Object>)valueObj;
                    elementMap.put("dummy", value);
                }
            } else { // Add a new leaf node, which should always be a String, String.
                map.put(name, value);
            }
        }
    }
    
    public static Map<String, Object> getBPLock(String guid, boolean force, String username) {
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("getPBLock invoked for guid %s, fource %s and username %s", guid, force, username));
        }
        Map<String, Object> result = new HashMap<>();
        ResolveSharedObject sharedObject = null;
        String lockName = String.format("%s-BP", guid);
        try {
            
            sharedObject = getSharedObject(lockName, username);
            if (sharedObject == null) { // no lock present, so create one
                sharedObject = createBPLock(lockName, username);
                if (sharedObject == null) {
                    /*
                     * Oh, looks like someone already created a BP lock object.
                     * Let's see who has this lock.
                     */
                    sharedObject = getSharedObject(lockName, username);
                    if (sharedObject == null) {
                        result.put("ERROR", "Could not figure out who has a lock. Please try again...");
                    } else {
                        populateLockInfo(sharedObject, result, false);
                    }
                } else { // Lock is successfully acquired.
                    populateLockInfo(sharedObject, result, false);
                }
            } else {
                // Aah, got the lock object. Check who has a lock.
                String lockedByUser = sharedObject.getSysCreatedBy();
                if (username.equalsIgnoreCase(lockedByUser) || force) {
                    /*
                     * OK, the same user has a lock.
                     * Now, lock the record and update the time.
                     */
                    sharedObject = acquireBPLock(sharedObject, username);
                    if (sharedObject == null) {
                        // Oops, lock could not be acquired
                        result.put("ERROR", "Could not acquire a lock. Please try again...");
                    } else {
                        populateLockInfo(sharedObject, result, false);
                    }
                } else {
                    // lock is with some other user.
                    populateLockInfo(sharedObject, result, true);
                }
            }
        } catch(Throwable t) {
            Log.log.error("Could not get BP Lock", t);
        }
        return result;
    }
    
    private static ResolveSharedObject getSharedObject(String lockName, String username) {
        ResolveSharedObject sharedObject = null;
        try {
            sharedObject = (ResolveSharedObject) HibernateProxy.execute(() -> {
            	return HibernateUtil.findLockObject(lockName);
            });
        } catch(Throwable t) {
            Log.log.error(String.format("Could not get Shared Object for lock %s", lockName), t);
                                           HibernateUtil.rethrowNestedTransaction(t);
        }
        return sharedObject;
    }
    
    private static void populateLockInfo(ResolveSharedObject sharedObject, Map<String, Object> result, boolean warn) {
        result.put("USER", sharedObject.getSysCreatedBy());
        result.put("TIME", sharedObject.getSysCreatedOn().getTime());
        if (warn) {
            result.put("WARNING", String.format("This lock is acquired by another user, %s", sharedObject.getSysCreatedBy()));
        }
    }
    
    private static ResolveSharedObject createBPLock (String lockName, String username) throws Exception {
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("createBPLock invoked for lock name %s by user %s", lockName, username));
        }
        
        return (ResolveSharedObject) HibernateProxy.execute(() -> {
        	 ResolveSharedObject sharedObject = null;
             try {
                 sharedObject = new ResolveSharedObject();
                 sharedObject.setSysCreatedBy(username);
                 sharedObject.setSysCreatedOn(new Date());
                 sharedObject.setUName(lockName);
                 
                 HibernateUtil.getCurrentSession().save(sharedObject);
             } catch(Throwable t) {
                 Log.log.error(String.format("Could not create shared object for lockName: %s. Message: %s", lockName, t.getMessage()), t);
                 sharedObject = null;
             }
             return sharedObject;
        });
       
    }
    
    private static ResolveSharedObject acquireBPLock(ResolveSharedObject sharedObject, String username) {
        String lockName = sharedObject.getUName();
        
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("acquireBPLock invoked for lock name %s by user %s", lockName, username));
        }
        
        try {
            
            String id = sharedObject.getsys_id();
            HibernateProxy.execute(() -> {
            	Session session = HibernateUtil.getCurrentSession();
                ResolveSharedObject so = (ResolveSharedObject) session.get(ResolveSharedObject.class, id, LockMode.PESSIMISTIC_WRITE);
                so.setSysCreatedBy(username);
                so.setSysCreatedOn(new Date());
                session.update(so);
            });
            
        } catch(Throwable t) {
            Log.log.error(String.format("Could not acquire a lock on shared object %s by user %s", lockName, username), t);
            sharedObject = null;
                                           HibernateUtil.rethrowNestedTransaction(t);
        }
        return sharedObject;
    }
    
    public static void releaseBPLock(String guid, String username) {
        String lockName = String.format("%s-BP", guid);
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("releaseBPLock invoked for lock name %s by user %s", lockName, username));
        }
        
        
        try {
            HibernateProxy.execute(() -> {
            	ResolveSharedObject sharedObject = null;
            	Session session = HibernateUtil.getCurrentSession();
                sharedObject = HibernateUtil.findLockObject(lockName);
                if (sharedObject != null) {
                    sharedObject = (ResolveSharedObject) session.get(ResolveSharedObject.class, sharedObject.getsys_id(), LockMode.PESSIMISTIC_WRITE);
                    if (username.equalsIgnoreCase(sharedObject.getSysCreatedBy())) {
                        session.delete(sharedObject);
                    } else {
                        Log.log.warn(String.format("Could not release BP lock %s initially acquired by %s because the new lock owner is %s", lockName, username, sharedObject.getSysCreatedBy()));
                    }
                } else {
                    Log.log.warn(String.format("No lock with the name %s found to release",lockName));
                }
            });
        } catch(Throwable t) {
        	HibernateUtil.rethrowNestedTransaction(t);
        }                      
    }
    
    public static void whoHasBpLock(String guid, Map<String, Object> propertiesMap, String username) {
        if (StringUtils.isNotBlank(guid)) {
            String lockName = String.format("%s-BP", guid);
            ResolveSharedObject sharedObject = getSharedObject(lockName, username);
            if (sharedObject == null) {
                propertiesMap.put("BP-OWNER", "None");
            } else {
                Map<String, Object> lockInfoMap = new HashMap<>();
                populateLockInfo(sharedObject, lockInfoMap, false);
                propertiesMap.put("BP-OWNER", lockInfoMap);
            }
        }
    }
}
