/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ImpexUtil
{
    /**
     * API to create a new Export Module definition
     * 
     * @param modulename
     *            Name of the module ot create (cannot be ALL)
     */
    public static boolean createExport(String modulename)
    {
        return createExport(modulename, null);
    }

    /**
     * API to create a new Export Module definition
     * 
     * @param modulename
     *            Name of the module ot create (cannot be ALL)
     * @param description
     *            description of the export module
     */
    public static boolean createExport(String modulename, String description)
    {
        return com.resolve.services.hibernate.util.ImpexUtil.createExport(modulename, description);
    } // createExport

    /**
     * API to add Documents to an Export definition Scan set to true by default.
     * 
     * @param modulename
     *            Export Moule to add to
     * @param docnames
     *            Document Names to add to Export
     * @return
     */
    public static boolean addDocumentsToExport(String modulename, List docnames)
    {
        return addDocumentsToExport(modulename, docnames, true);
    }// addDocumentsToExport

    /**
     * API to add Documents to an Export definition
     * 
     * @param modulename
     *            Export Moule to add to
     * @param docnames
     *            Document Names to add to Export
     * @param scan
     *            Whether or not to turn export scan on or off
     * @return
     */
    public static boolean addDocumentsToExport(String modulename, List docnames, boolean scan)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addRunbooksToExport(modulename, docnames, Constants.IMPEX_PARAM_DOCUMENT, scan);
        return result;
    }// addDocumentsToExport

    /**
     * API to add Document Namespaces to an Export definition Scan set to true
     * by default.
     * 
     * @param modulename
     *            Export Moule to add to
     * @param namespaces
     *            Namespaces to add to Export
     * @return
     */
    public static boolean addDocumentNamespacesToExport(String modulename, List namespaces)
    {
        return addDocumentNamespacesToExport(modulename, namespaces, true);
    }// addDocumentNamespacesToExport

    /**
     * API to add Document Namespaces to an Export definition
     * 
     * @param modulename
     *            Export Moule to add to
     * @param namespaces
     *            Namespaces to add to Export
     * @param scan
     *            Whether or not to turn export scan on or off
     * @return
     */
    public static boolean addDocumentNamespacesToExport(String modulename, List namespaces, boolean scan)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addRunbooksToExport(modulename, namespaces, Constants.IMPEX_PARAM_NAMESPACE, scan);
        return result;
    }// addDocumentNamespacesToExport

    /**
     * API to add Documents By Tag Name to an Export definition Scan set to true
     * by default.
     * 
     * @param modulename
     *            Export Moule to add to
     * @param tags
     *            Tags used to add documents to Export
     * @return
     */
    public static boolean addDocumentsByTagToExport(String modulename, List tags)
    {
        return addDocumentsByTagToExport(modulename, tags, true);
    }// addDocumentsByTagToExport

    /**
     * API to add Documents By Tag Name to an Export definition
     * 
     * @param modulename
     *            Export Moule to add to
     * @param tags
     *            Tags to add documents to Export
     * @param scan
     *            Whether or not to turn export scan on or off
     * @return
     */
    public static boolean addDocumentsByTagToExport(String modulename, List tags, boolean scan)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addRunbooksToExport(modulename, tags, Constants.IMPEX_PARAM_TAG, scan);
        return result;
    }// addDocumentsByTagToExport

    /**
     * Add Action Tasks to Export Module
     * 
     * @param modulename
     *            Name of the Module to add Action Task to
     * @param actionTasks
     *            Names of the Action Task to add (name#namespace)
     * @return
     */
    public static boolean addActionTasksToExport(String modulename, List actionTasks)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, actionTasks, Constants.IMPEX_PARAM_ACTIONTASK);
        return result;
    }// addActionTasksToExport

    /**
     * Add Action Tasks to Export Module By Tag Name
     * 
     * @param modulename
     *            Name of the Module to add Action Tasks to
     * @param actionTasksByTag
     *            Names of the Tag used to add Action Tasks (name#module)
     * @return
     */
    public static boolean addActionTasksByTagToExport(String modulename, List actionTasksByTag)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, actionTasksByTag, Constants.IMPEX_PARAM_ACTIONTASK_BY_TAG);
        return result;
    }// addActionTasksByTagToExport

    /**
     * Add Action Task Properties to Export Module
     * 
     * @param modulename
     *            Name of the Module to add Properties to
     * @param properties
     *            Names of the Properties to add (name#module)
     * @return
     */
    public static boolean addPropertiesToExport(String modulename, List properties)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, properties, Constants.IMPEX_PARAM_PROPERTIES);
        return result;
    }// addPropertiesToExport
    
    /**
     * Add Wiki Lookup Mappings to Export Module
     * 
     * @param modulename
     *            Name of the Module to add Lookup Mappings to
     * @param wikiLookups
     *            Names of the Wiki Lookup Mappings to add (regex#module)
     * @return
     */
    public static boolean addWikiLookupsToExport(String modulename, List wikiLookups)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, wikiLookups, Constants.IMPEX_PARAM_WIKILOOKUP);
        return result;
    }// addWikiLookupsToExport

    /**
     * Add Cron Scheduled Jobs to Export Module
     * 
     * @param modulename
     *            Name of the Module to add Cron Scheduled Jobs to
     * @param cronJobs
     *            Names of the Cron Scheduled Jobs to add (name#module)
     * @return
     */
    public static boolean addCronJobsToExport(String modulename, List cronJobs)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, cronJobs, Constants.IMPEX_PARAM_CRONJOB);
        return result;
    }// addCronJobsToExport

    /**
     * Add Menu Sets to Export Module
     * 
     * @param modulename
     *            Name of the Module to add Menu Sets to
     * @param menuSets
     *            Names of the Menu Sets to add
     * @return
     */
    public static boolean addMenuSetsToExport(String modulename, List menuSets)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, menuSets, Constants.IMPEX_PARAM_MENUSET);
        return result;
    }// addMenuSetsToExport

    /**
     * Add A Menu Section and Items to Export Module
     * 
     * @param modulename
     *            Name of the Module to add the Menu Set and Items to
     * @param menuSection
     *            Name of the Menu Section to add
     * @param menuItems
     *            Name of the Menu Items in the Section to add, if null or blank
     *            all Menu Items in the Section will be added
     * @return
     */
    public static boolean addMenuSectionsToExport(String modulename, String menuSection, List menuItems)
    {
        return com.resolve.services.hibernate.util.ImpexUtil.addMenuSectionsToExport(modulename, menuSection, menuItems);
    }// addMenuSectionsToExport

    /**
     * Add A Custom Table to Export Module Either the schema, data, or both must
     * be set to true, or no entry will be added to the Export Module
     * 
     * @param modulename
     *            Name of the Module to add the Custom Table to
     * @param customTable
     *            Name of the Custom Table to add
     * @param schema
     *            Whether the Custom Table Schema should be exported
     * @param data
     *            Whether the Custom Table Data should be exported
     * @return
     */
    public static boolean addCustomTablesToExport(String modulename, String customTable, boolean schema, boolean data)
    {
        boolean result = false;
        if (customTable != null)
        {
            if (schema || data)
            {
                if (schema)
                {
                    customTable += "#SCHEMA";
                    if (data)
                    {
                        customTable += ",DATA";
                    }
                }
                else if (data)
                {
                    customTable += "#DATA";
                }
                List<String> customTables = new ArrayList();
                customTables.add(customTable);
                result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, customTables, Constants.IMPEX_PARAM_CUSTOMTABLE);
            }
            else
            {
                Log.log.error("Cannot Add Custom Table Entry without either Schema Or Data set to true");
            }
        }
        return result;
    }// addCustomTablesToExport

    /**
     * Add Form Views to Export Module
     * 
     * @param modulename
     *            Name of the Module to add Form Views to
     * @param formViews
     *            Names of the Form Views to add
     * @return
     */
    public static boolean addFormViewsToExport(String modulename, List formViews)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, formViews, Constants.IMPEX_PARAM_METAFORMVIEW);
        return result;
    }// addFormViewsToExport

    /**
     * Add Entries to the Export based on a Custom DB Query
     * 
     * @param modulename
     *            Name of the Export Module the Custom Query will be added to
     * @param table
     *            Name of the table that will be queries
     * @param query
     *            Query to run against the table when exporting
     * @return
     */
    public static boolean addCustomDBObjectToExport(String modulename, String table, String query)
    {
        boolean result = false;
        if (StringUtils.isNotEmpty(table))
        {
            List<String> customDBObjects = new ArrayList();
            String customTable = table;
            if (StringUtils.isNotEmpty(query))
            {
                customTable = table + "#" + query;
            }
            customDBObjects.add(customTable);
            result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, customDBObjects, Constants.IMPEX_PARAM_DB);
        }
        else
        {
            Log.log.error("Failed to add Custom Table Export");
        }
        return result;
    }// addCustomDBObjectToExport

    /**
     * Add Tags to Export Module
     * 
     * @param modulename
     *            Name of the Module to add Tags to
     * @param tags
     *            Names of the Tags to add
     * @return
     */
    public static boolean addTagsToExport(String modulename, List tags)
    {
        boolean result = com.resolve.services.hibernate.util.ImpexUtil.addComponentsToExport(modulename, tags, Constants.IMPEX_PARAM_TAG);
        return result;
    }// addTagsToExport

    /**
     * API to add Runbook components to an Export definition required params:
     * MODULENAME - name of the module to add to value params (at least one
     * required): DOCUMENT - Wiki Documents to add to definition, semicolon
     * separated NAMESPACE - Wiki Namespaces to add to definition, semicolon
     * separated TAG - Tag of Wiki Documents to add to definition, semicolon
     * separated optional params: SCAN - Whether to have scan on(true/yes) or
     * off(false/no), true by default
     * 
     * @param params
     */
    public static void addRunbooksToExport(Map params)
    {
        String modulename = (String) params.get(Constants.IMPEX_PARAM_MODULENAME);
        String docnames = (String) params.get(Constants.IMPEX_PARAM_DOCUMENT);
        String namespaces = (String) params.get(Constants.IMPEX_PARAM_NAMESPACE);
        String tags = (String) params.get(Constants.IMPEX_PARAM_TAG);
        if (modulename.equalsIgnoreCase("ALL"))
        {
            Log.log.error("Impex Module name 'ALL' is a reserved impex name");
        }
        else if (StringUtils.isNotEmpty(modulename) && (StringUtils.isNotEmpty(docnames) || StringUtils.isNotEmpty(namespaces) || StringUtils.isNotEmpty(tags)))
        {
            String scanStr = (String) params.get(Constants.IMPEX_PARAM_SCAN);
            Log.log.debug("Set Scan: " + scanStr);
            boolean scan = true;
            if ("no".equalsIgnoreCase(scanStr) || "false".equalsIgnoreCase(scanStr) || "off".equalsIgnoreCase(scanStr))
            {
                scan = false;
            }
            Log.log.debug("Set Scan: " + scanStr + " -> " + scan);

            // for each docname passed, add a resolve_impex_wiki entry to the
            // resolve_impex_module
            if (StringUtils.isNotEmpty(docnames))
            {
                List docnameList = Arrays.asList(docnames.split(";"));
                addDocumentsToExport(modulename, docnameList, scan);
            }

            // for each namespace passed, add a resolve_impex_wiki entry to the
            // resolve_impex_module
            if (StringUtils.isNotEmpty(namespaces))
            {
                List namespaceList = Arrays.asList(namespaces.split(";"));
                addDocumentNamespacesToExport(modulename, namespaceList, scan);
            }

            // for each tag passed, add a resolve_impex_wiki entry to the
            // resolve_impex_module
            if (StringUtils.isNotEmpty(tags))
            {
                List tagList = Arrays.asList(tags.split(";"));
                addDocumentsByTagToExport(modulename, tagList, scan);
            }
        }
        else
        {
            Log.log.error("No Impex Module or Docnames Given");
        }
    }// addRunbooksToExport

    /**
     * API to add Components to an Export definition required params: MODULENAME
     * - name of the module to add to value params (at least one required):
     * ACTIONTASK - Action Tasks to add to export definition, semicolon
     * separated (name#namespace) ACTIONTASK_BY_TAG - Tags to use to add Action
     * Tasks to export definition, semicolon separated (name#module) PROPERTIES
     * - Properties to add to export definition, semicolon separated
     * (name#module) WIKILOOKUP - Wiki Lookup Mappings to add to export
     * definition, semicolon separated (regex#module) CRONJOB - Cron Scheduled
     * Jobs to add to export definition, semicolon separated (name#module)
     * MENUSET - Menu Sets to add to export definition, semicolon separated
     * MENUSECTION - Menu Sections to add to export definition, semicolon
     * separated Can optionally provide menu items in the section also
     * (section#menuitem#menuitem...) METAFORMVIEW - Form Views to add to export
     * definition, semicolon separated DB - Custom DB Queries to add entries to
     * export definition, semicolon separated (table#query) TAG - Tags to add to
     * export definition, semicolon separated (name#module)
     * 
     * @param params
     */
    public static void addComponentsToExport(Map params)
    {
        String modulename = (String) params.get(Constants.IMPEX_PARAM_MODULENAME);
        String actionTasks = (String) params.get(Constants.IMPEX_PARAM_ACTIONTASK);
        String actionTasksByTag = (String) params.get(Constants.IMPEX_PARAM_ACTIONTASK_BY_TAG);
        String properties = (String) params.get(Constants.IMPEX_PARAM_PROPERTIES);
        String wikiLookups = (String) params.get(Constants.IMPEX_PARAM_WIKILOOKUP);
        String cronJobs = (String) params.get(Constants.IMPEX_PARAM_CRONJOB);
        String menuSets = (String) params.get(Constants.IMPEX_PARAM_MENUSET);
        String menuSections = (String) params.get(Constants.IMPEX_PARAM_MENUSECTION);
        String customTables = (String) params.get(Constants.IMPEX_PARAM_CUSTOMTABLE);
        String metaFormViews = (String) params.get(Constants.IMPEX_PARAM_METAFORMVIEW);
        String customDBObjects = (String) params.get(Constants.IMPEX_PARAM_DB);
        String tags = (String) params.get(Constants.IMPEX_PARAM_TAG);
        if (modulename.equalsIgnoreCase("ALL"))
        {
            Log.log.error("Impex Module name 'ALL' is a reserved impex name");
        }
        else if (StringUtils.isNotEmpty(modulename)
                        && (StringUtils.isNotEmpty(actionTasks) || StringUtils.isNotEmpty(actionTasksByTag) || StringUtils.isNotEmpty(properties) || StringUtils.isNotEmpty(wikiLookups) || StringUtils.isNotEmpty(cronJobs) || StringUtils.isNotEmpty(menuSets) || StringUtils.isNotEmpty(menuSections) || StringUtils.isNotEmpty(customTables) || StringUtils.isNotEmpty(metaFormViews) || StringUtils.isNotEmpty(customDBObjects) || StringUtils.isNotEmpty(tags)))
        {
            // for each action task passed, add a resolve_impex_glide entry to
            // the resolve_impex_module
            if (StringUtils.isNotEmpty(actionTasks))
            {
                List tasks = Arrays.asList(actionTasks.split(";"));
                addActionTasksToExport(modulename, tasks);
            }

            // for each action task tag passed, add a resolve_impex_glide entry
            // to the resolve_impex_module
            if (StringUtils.isNotEmpty(actionTasksByTag))
            {
                List taskTags = Arrays.asList(actionTasksByTag.split(";"));
                addActionTasksByTagToExport(modulename, taskTags);
            }

            // for each property passed, add a resolve_impex_glide entry to the
            // resolve_impex_module
            if (StringUtils.isNotEmpty(properties))
            {
                List propertyList = Arrays.asList(properties.split(";"));
                addPropertiesToExport(modulename, propertyList);
            }
            
            // for each wiki lookup passed, add a resolve_impex_glide entry to
            // the resolve_impex_module
            if (StringUtils.isNotEmpty(wikiLookups))
            {
                List wikiLookupsList = Arrays.asList(wikiLookups.split(";"));
                addWikiLookupsToExport(modulename, wikiLookupsList);
            }

            // for each cron definition passed, add a resolve_impex_glide entry
            // to the resolve_impex_module
            if (StringUtils.isNotEmpty(cronJobs))
            {
                List cronJobsList = Arrays.asList(cronJobs.split(";"));
                addCronJobsToExport(modulename, cronJobsList);
            }

            // for each menu set passed, add a resolve_impex_glide entry to the
            // resolve_impex_module
            if (StringUtils.isNotEmpty(menuSets))
            {
                List menuSetsList = Arrays.asList(menuSets.split(";"));
                addMenuSetsToExport(modulename, menuSetsList);
            }

            // for each menu section/items passed, add resolve_impex_glide
            // entries to the resolve_impex_module
            // section value will be formatted as section#item#item#item...
            // if no items are passed, all menu items associated with the
            // section will be added
            if (StringUtils.isNotEmpty(menuSections))
            {
                List menuSectionsList = Arrays.asList(menuSections.split(";"));
                for (Object menuSectionObj : menuSectionsList)
                {
                    String menuSection = menuSectionObj.toString();
                    int hashAt = menuSection.indexOf("#");
                    String section = menuSection;
                    List menuItems = null;
                    if (hashAt > -1)
                    {
                        section = menuSection.substring(0, hashAt);
                        String items = menuSection.substring(hashAt + 1);
                        if (StringUtils.isNotEmpty(items))
                        {
                            menuItems = Arrays.asList(items.split("#"));
                        }
                    }

                    addMenuSectionsToExport(modulename, section, menuItems);
                }
            }

            // for each custom table name passed, add resolve_impex_glide
            // entries to the resolve_impex_module
            if (StringUtils.isNotEmpty(customTables))
            {
                String[] customTablesAry = customTables.split(";");
                for (String customTable : customTablesAry)
                {
                    boolean schema = false;
                    boolean data = false;
                    String table = customTable;
                    String export = null;
                    int hashAt = table.indexOf("#");
                    if (hashAt > -1)
                    {
                        table = table.substring(0, hashAt);
                        export = table.substring(hashAt + 1).toUpperCase();

                        if (export.contains("SCHEMA"))
                        {
                            schema = true;
                        }
                        if (export.contains("DATA"))
                        {
                            data = true;
                        }
                    }
                    else
                    {
                        Log.log.error("Unable to determine if custom table export is for SCHEMA, DATA, or both");
                    }
                    if (schema || data)
                    {
                        addCustomTablesToExport(modulename, table, schema, data);
                    }
                }
            }

            // for each meta form view passed, add resolve_impex_glide entries
            // to the resolve_impex_module
            if (StringUtils.isNotEmpty(metaFormViews))
            {
                List metaFormViewsAry = Arrays.asList(metaFormViews.split(";"));
                addFormViewsToExport(modulename, metaFormViewsAry);

            }

            // for each Custom DB Query passed, add resolve_impex_glide entries
            // to the resolve_impex_module
            if (StringUtils.isNotEmpty(customDBObjects))
            {
                String[] customDBObjectsAry = customDBObjects.split(";");
                for (String customDBObject : customDBObjectsAry)
                {
                    if (StringUtils.isNotEmpty(customDBObject))
                    {
                        String table = customDBObject;
                        String query = null;
                        int hashAt = customDBObject.indexOf("#");
                        if (hashAt > -1)
                        {
                            table = customDBObject.substring(0, hashAt);
                            query = customDBObject.substring(hashAt + 1);
                        }
                        addCustomDBObjectToExport(modulename, table, query);
                    }
                }
            }

            // for each tag passed, add resolve_impex_glide entries to the
            // resolve_impex_module
            if (StringUtils.isNotEmpty(tags))
            {
                List tagsAry = Arrays.asList(tags.split(";"));
                addTagsToExport(modulename, tagsAry);
            }
        }
        else
        {
            Log.log.error("No Impex Module or Components Given");
        }
    }// addComponentsToExport

    /**
     * API to add Create a new Export Module definition required params:
     * MODULENAME - name of the module to create optional params Description -
     * Wiki Documents to add to definition, semicolon separated
     * 
     * @param params
     */
    public static void createExport(Map params)
    {
        String modulename = (String) params.get(Constants.IMPEX_PARAM_MODULENAME);
        String description = (String) params.get(Constants.IMPEX_PARAM_DESCRIPTION);
        if (modulename.equalsIgnoreCase("ALL"))
        {
            Log.log.error("Impex Module name 'ALL' is a reserved impex name");
        }
        else if (StringUtils.isEmpty(modulename))
        {
            Log.log.error("A Module Name must be given to be created");
        }
        else
        {
            createExport(modulename, description);
        }
    }// createExport

    public static String getAllModuleNames()
    {
        return com.resolve.services.hibernate.util.ImpexUtil.getAllModuleNames();
    } // getAllModuleNames

}
