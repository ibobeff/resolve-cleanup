/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.wiki.radeox.macro.api.ApiDoc;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

import java.io.IOException;
import java.io.Writer;

/*
 * Lists all known API documentation repositories and mappings
 */

public class ApiDocMacro extends BaseLocaleMacro
{
    private String[] paramDescription = {};

    public String[] getParamDescription()
    {
        return paramDescription;
    }

    public String getLocaleKey()
    {
        return "macro.apidocs";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        ApiDoc apiDoc = ApiDoc.getInstance();
        apiDoc.appendTo(writer);
        return;
    }
}
