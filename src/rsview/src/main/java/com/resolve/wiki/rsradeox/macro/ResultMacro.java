/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import com.resolve.html.Table;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.WikiUtils;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.store.HibernateStoreUtil;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * Macro for defining and displaying the URL to Resolve Execute Request for ActionTask execution. The # namespace is optional (if required).
 * 
 * RESULT DEFINITION {result:[name=<name>|width=<x>|height=<y>|view=<viewname>]} [DEFINTION] {result}
 * 
 * DEFINITION
 *   syntax = description>task#namespace{wiki::nodeId}=description-wiki|result-wiki
 *   
 */
public class ResultMacro extends BaseLocaleMacro
{

    static final String DISPLAY_NONE = "display: none;";

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestContext.getWiki();

        String content = params.getContent();
        content = HttpUtil.sanitizeValue(content);

        //refresh interval in secs
        String type = StringUtils.isNotEmpty(params.get("type")) ? params.get("type") : "";
        type = HttpUtil.sanitizeValue(type);
        String refreshInterval = StringUtils.isNotEmpty(params.get("refreshInterval")) ? params.get("refreshInterval") : "5";
        refreshInterval = HttpUtil.sanitizeValue(refreshInterval, "ResolveNumber", 50);
        String refreshStartWiki = StringUtils.isNotEmpty(params.get("refreshStartWiki")) ? params.get("refreshStartWiki") : "";
        refreshStartWiki = HttpUtil.sanitizeValue(refreshStartWiki);
        String endstatus = StringUtils.isNotEmpty(params.get("endstatus")) ? params.get("endstatus") : "";
        endstatus = HttpUtil.sanitizeValue(endstatus);
        String encodeSummary = StringUtils.isNotEmpty(params.get("encodeSummary")) ? params.get("encodeSummary") : "";
        encodeSummary = HttpUtil.sanitizeValue(encodeSummary);
        String textHideExpand = StringUtils.isNotEmpty(params.get("text")) ? params.get("text") : "Show / Hide Results";
        textHideExpand = HttpUtil.sanitizeValue(textHideExpand);
        String expand = (StringUtils.isNotEmpty(params.get("expand")) && params.get("expand").equalsIgnoreCase("false")) ? "false" : "true";
        expand = HttpUtil.sanitizeValue(expand);
        boolean isOrderByAsc = (StringUtils.isNotEmpty(params.get("order")) && params.get("order").trim().equalsIgnoreCase("desc")) ? false : true;

        String imgId = "results_img_id_" + JavaUtils.generateUniqueNumber();

        String link = "<img border=\"0\" id=\"" + imgId + "\" src=\"/resolve/images/collapseall.gif\" />";
        String style = StringUtils.isNotEmpty(params.get("style")) ? params.get("style") : "";
        style = HttpUtil.sanitizeValue(style);
        if (expand != null && expand.equalsIgnoreCase("false"))
        {
            style += DISPLAY_NONE;
            link = "<img border=\"0\" id=\"" + imgId + "\" src=\"/resolve/images/expandall.gif\" />";
        }

        String currentUser = wikiRequestContext.getWikiUser().getUsername();
        currentUser = HttpUtil.sanitizeValue(currentUser);
        String documentName = wiki.getDocumentName(wikiRequestContext);
        documentName = HttpUtil.sanitizeValue(documentName, "FileName", 300);        
        String problemId = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_PROBLEMID);
        if (StringUtils.isBlank(problemId))
        {
            String orgId = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_ID);
            orgId = HttpUtil.sanitizeValue(orgId);
            
            String orgName = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_NAME);
            orgName = HttpUtil.sanitizeValue(orgName);
            
            problemId = HibernateStoreUtil.getActiveSid(currentUser, orgId, orgName);
        }
        problemId = HttpUtil.sanitizeValue(problemId, "ResolveUUID", 32);
        String[] actionTaskNamesArr = null;

        String divId = "results_" + JavaUtils.generateUniqueNumber();

        String isDoneYetId = "isDoneYetId_" + JavaUtils.generateUniqueNumber();
        String problemId_divId = WikiUtils.createProblemId(documentName);

        if (StringUtils.isNotEmpty(problemId) && !problemId.equals("-1") && !StringUtils.isEmpty(content))
        {
            content = content.trim();
            actionTaskNamesArr = content.split("\n");

            StringBuffer actionNamespaceArr = new StringBuffer("[");
            StringBuffer actionNameArr = new StringBuffer("[");
            StringBuffer nodeIdArr = new StringBuffer("[");
            StringBuffer actionWikiArr = new StringBuffer("[");
            StringBuffer descriptionArr = new StringBuffer("[");

            if (actionTaskNamesArr != null && actionTaskNamesArr.length > 0)
            {
                for (int i = 0; i < actionTaskNamesArr.length; i++)
                {
                    String atName = actionTaskNamesArr[i].trim();
                    populateArrayString(atName, actionNamespaceArr, actionNameArr, nodeIdArr, actionWikiArr, descriptionArr);

                    if (i != actionTaskNamesArr.length - 1)
                    {
                        actionNamespaceArr.append(",");
                        actionNameArr.append(",");
                        nodeIdArr.append(",");
                        actionWikiArr.append(",");
                        descriptionArr.append(",");
                    }
                }
            }

            actionNamespaceArr.append("]");
            actionNameArr.append("]");
            nodeIdArr.append("]");
            actionWikiArr.append("]");
            descriptionArr.append("]");

            StringBuffer ajaxParams = new StringBuffer();
            ajaxParams.append("problemId : (problemIdEl && problemIdEl.innerHTML) ? problemIdEl.innerHTML : ''").append(",").append("\n");
            ajaxParams.append("textHideExpand : '").append(textHideExpand).append("',").append("\n");
            ajaxParams.append("encodeSummary : '").append(encodeSummary).append("',").append("\n");

            ajaxParams.append("currentUser : '").append(currentUser).append("',").append("\n");
            ajaxParams.append("documentName : '").append(documentName).append("',").append("\n");

            ajaxParams.append("type : '").append(type).append("',").append("\n");

            ajaxParams.append("refreshInterval : '").append(refreshInterval).append("',").append("\n");
            ajaxParams.append("refreshStartWiki : '").append(refreshStartWiki).append("',").append("\n");
            ajaxParams.append("endstatus : '").append(endstatus).append("',").append("\n");
            ajaxParams.append("divStyle : '").append(style).append("',").append("\n");
            ajaxParams.append("isDoneYetId : '").append(isDoneYetId).append("',").append("\n");
            ajaxParams.append("isOrderByAsc : '").append(isOrderByAsc).append("',").append("\n");

            ajaxParams.append("actionNamespace : ").append(actionNamespaceArr.toString()).append(",").append("\n");
            ajaxParams.append("actionName : ").append(actionNameArr.toString()).append(",").append("\n");
            ajaxParams.append("nodeId : ").append(nodeIdArr.toString()).append(",").append("\n");
            ajaxParams.append("actionWiki : ").append(actionWikiArr.toString()).append(",").append("\n");
            ajaxParams.append("description : ").append(descriptionArr.toString()).append("").append("\n");

            //interval in millisecond
            long interval = 5000;
            try
            {
                interval = Long.parseLong(refreshInterval) * 1000;
            }
            catch (Throwable t)
            {
                interval = 5000;
            }

            String output = getJavascriptForResultAndDetail(divId, problemId_divId, ajaxParams.toString(), interval, ConstantValues.URL_RESULT_RPC_HTML);

            writer.write(output);
        }
        else
        {
            StringBuffer html = new StringBuffer("<a href=\"javascript:showhide('").append(divId).append("','").append(imgId).append("')\">");
            html.append(link).append("</a>").append(textHideExpand).append("&nbsp;").append(endstatus);
            html.append("<div id=\"").append(divId).append("\"style=\"").append(style).append("\" >");
            html.append("<input type='hidden' name='isDoneYet' id='").append(isDoneYetId).append("' value='" + ConstantValues.STOP_AJAX_REFRESH + "'>");
            html.append(Table.print(new ArrayList(), ResultAjax.keys, ResultAjax.heading, ResultAjax.getResultTableHtmlProperties(encodeSummary)));
            html.append("</div>");

            writer.write(html.toString());

        }

    }

    public static String getEndStatusHTML(String severity)
    {
        String result = "";

        String color = "grey";
        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
        {
            color = "red";
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
        {
            color = "orange";
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
        {
            color = "yellow";
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
        {
            color = "green";
        }

        result = "&nbsp&nbsp&nbsp<img border=\"0\" src=\"/resolve/images/colorball_" + color + ".gif\" />";

        return result;
    }

    public static String getEndStatusSeverity(String newSeverity, String currSeverity, String mode)
    {
        String result = currSeverity;

        int newSeverityValue = getSeverityValue(newSeverity);
        int currSeverityValue = getSeverityValue(currSeverity);

        if (mode.equalsIgnoreCase("BEST"))
        {
            if (newSeverityValue > currSeverityValue)
            {
                result = newSeverity;
            }
        }
        else
        {
            if (newSeverityValue < currSeverityValue)
            {
                result = newSeverity;
            }
        }

        return result;
    }

    public static int getSeverityValue(String severity)
    {
        int result = 0;

        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
        {
            result = 4;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
        {
            result = 3;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
        {
            result = 2;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
        {
            result = 1;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_UNKNOWN))
        {
            result = 0;
        }

        return result;
    }

    @Override
    public String getLocaleKey()
    {
        return "macro.result";
    }

    public static String getJavascriptForResultAndDetail(String divId, String problemId_divId, String ajaxParams, long refreshInterval, String URL)
    {
        String initFunctionId = "init_" + JavaUtils.generateUniqueNumber();

        StringBuffer ajax = new StringBuffer();
        String runnerDivId = "runner" + divId;

        ajax.append("<div id=\"").append(divId).append("\"></div>").append("\n");
        if (URL.indexOf("detail") > -1)
        {
            ajax.append("<input name=\"__details\" type=\"hidden\" value=\"").append(initFunctionId).append("()\" />").append("\n");
        }
        else
        {
            ajax.append("<input name=\"__results\" type=\"hidden\" value=\"").append(initFunctionId).append("()\" />").append("\n");
        }

        ajax.append("<div id='js'>").append("\n");
        ajax.append("<script type=\"text/javascript\">").append("\n");
        ajax.append("var " + runnerDivId + ";\n");
        ajax.append("var ").append(initFunctionId).append(" = function(){").append("\n");
        ajax.append("   var messageEl = Ext.get('").append(divId).append("');").append("\n");
        ajax.append("   var problemIdEl = document.getElementById('").append(problemId_divId).append("');").append("\n");
        ajax.append("   var current_problemId = ''; ").append("\n");
        ajax.append("   if(problemIdEl != null) { current_problemId = problemIdEl.innerHTML; }").append("\n");

        ajax.append("   var task = {").append("\n");
        ajax.append("       run: function(){").append("\n");
        ajax.append("               Ext.Ajax.request({").append("\n");
        ajax.append("                   url: '/resolve/service").append(URL).append("',").append("\n");
        ajax.append("                   params: {").append("\n");
        ajax.append(ajaxParams).append("\n");
        ajax.append("                   },").append("\n");
        ajax.append("                   success: function(response){").append("\n");
        ajax.append("                       var text;").append("\n");
        ajax.append("                       try {").append("\n");
        ajax.append("                           text = response.responseText;").append("\n");
        ajax.append("                           messageEl.update(text);").append("\n");
        ajax.append("                           messageEl.show();").append("\n");
        ajax.append("                           if(text.indexOf('").append(ConstantValues.STOP_AJAX_REFRESH).append("') > -1){").append("\n");
        ajax.append("                               Ext.TaskManager.stop(" + runnerDivId + ");").append("\n");
        ajax.append("                           }").append("\n");
        ajax.append("                       }").append("\n");
        ajax.append("                       catch(e) {                      ").append("\n");
        ajax.append("                       }").append("\n");
        ajax.append("                   },").append("\n");
        ajax.append("                   failure: function(response){").append("\n");
        ajax.append("                           if(response.statusText != ''){").append("\n"); //cancel status text is blank, timeout abort has a message
        ajax.append("                               if(typeof(resultFailureAlerted)=='undefined')resultFailureAlerted = false;").append("\n");
        ajax.append("                               if(!resultFailureAlerted){").append("\n");
        ajax.append("                                   resultFailureAlerted = true;").append("\n");
        ajax.append("                                   Ext.Msg.alert('Error', 'There was an error communicating with the server.');").append("\n");
        ajax.append("                               }").append("\n");
        ajax.append("                           }").append("\n");
        ajax.append("                   },").append("\n");
        ajax.append("                   scope: this").append("\n");
        ajax.append("               });").append("\n");
        ajax.append("       },").append("\n");
        ajax.append("       interval: ").append(refreshInterval).append(",\n");
        ajax.append("       fireOnStart: true\n");
        ajax.append("   }").append("\n");
        ajax.append("   if (" + runnerDivId + ") {\n");
        ajax.append("       " + runnerDivId + ".restart();\n");
        ajax.append("   }\n");
        ajax.append("   else\n");
        ajax.append("   {\n");
        ajax.append("       " + runnerDivId + " = (new Ext.util.TaskRunner()).newTask(task);\n");
        ajax.append("       " + runnerDivId + ".start();\n");
        ajax.append("   }\n");
        ajax.append("}//end of init").append("\n");
        ajax.append("Ext.onReady(").append(initFunctionId).append(");").append("\n");
        ajax.append("</script>").append("\n");
        ajax.append("</div>").append("\n");

        return ajax.toString();
    }

    public static void populateArrayString(String actionTaskName, StringBuffer actionNamespaceArr, StringBuffer actionNameArr, StringBuffer nodeIdArr, StringBuffer actionWikiArr, StringBuffer descriptionArr)
    {
        if (!StringUtils.isEmpty(actionTaskName))
        {
            // actiontask
            String actionName = null;
            String actionNamespace = null;
            String actionWiki = null;
            String nodeId = null;
            String description = null;

            // DEF syntax = description>task#namespace{wiki::nodeId}=wiki

            // set description
            int pos = actionTaskName.indexOf('>');
            if (pos > 0)
            {
                description = actionTaskName.substring(0, pos).trim();
                actionTaskName = actionTaskName.substring(pos + 1, actionTaskName.length()).trim();
            }

            // set actionWiki
            pos = actionTaskName.indexOf('=');
            if (pos > 0)
            {
                actionWiki = actionTaskName.substring(pos + 1, actionTaskName.length()).trim();
                actionTaskName = actionTaskName.substring(0, pos).trim();
            }

            // set nodeId
            pos = actionTaskName.indexOf('{');
            if (pos > 0)
            {
                nodeId = actionTaskName.substring(pos + 1, actionTaskName.length() - 1);
                actionTaskName = actionTaskName.substring(0, pos);
            }

            pos = actionTaskName.indexOf('#');
            if (pos > 0)
            {
                actionName = actionTaskName.substring(0, pos);
                actionNamespace = actionTaskName.substring(pos + 1, actionTaskName.length());
            }
            else
            {
                actionName = actionTaskName;
            }

            actionNamespaceArr.append("'").append(StringUtils.isNotEmpty(actionNamespace) ? actionNamespace.trim() : "").append("'");
            actionNameArr.append("'").append(StringUtils.isNotEmpty(actionName) ? actionName.trim() : "").append("'");
            nodeIdArr.append("'").append(StringUtils.isNotEmpty(nodeId) ? nodeId.trim() : "").append("'");
            if (actionWikiArr != null)
            {
                actionWikiArr.append("'").append(StringUtils.isNotEmpty(actionWiki) ? actionWiki.trim() : "").append("'");
            }

            if (descriptionArr != null)
            {
                descriptionArr.append("'").append(StringUtils.isNotEmpty(description) ? description.trim() : "").append("'");
            }
        }
    }
}
