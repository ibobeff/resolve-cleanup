/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.table;


/**
 * A function interface for functions in table cells like
 * =SUM(A1:A3)
 *
 */

public interface Function {
  public String getName();
  public void execute(Table table, int posx, int posy, int startX, int startY, int endX, int endY);
}
