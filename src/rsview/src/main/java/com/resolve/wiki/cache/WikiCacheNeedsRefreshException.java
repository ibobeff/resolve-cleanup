/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.cache;

public class WikiCacheNeedsRefreshException extends Exception
{
	private static final long serialVersionUID = -6425106136564934196L;
	protected Exception e;

	public WikiCacheNeedsRefreshException(Exception e)
	{
		this.e = e;
	}

	public String getMessage()
	{
		return e.getMessage();
	}

}
