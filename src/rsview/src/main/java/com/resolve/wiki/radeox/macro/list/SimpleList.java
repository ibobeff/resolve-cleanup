/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.list;

import com.resolve.wiki.radeox.util.Linkable;
import com.resolve.wiki.radeox.util.Nameable;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Iterator;

/**
 * Simple list formatter.
 *
 */
public class SimpleList implements ListFormatter {
  public String getName() {
    return "simple";
  }


  public void format(Writer writer, Linkable current, String listComment, Collection c, String emptyText, boolean showSize)
      throws IOException {
    writer.write("<div class=\"list\"><div class=\"list-title\">");
    writer.write(listComment);
    if (showSize) {
      writer.write(" (");
      writer.write("" + c.size());
      writer.write(")");
    }
    writer.write("</div>");
    if (c.size() > 0) {
      writer.write("<blockquote>");
      Iterator nameIterator = c.iterator();
      while (nameIterator.hasNext()) {
        Object object = nameIterator.next();
        if (object instanceof Linkable) {
          writer.write(((Linkable) object).getLink());
        } else if (object instanceof Nameable) {
          writer.write(((Nameable) object).getName());
        } else {
          writer.write(object.toString());
        }

        if (nameIterator.hasNext()) {
          writer.write(", ");
        }
      }
      writer.write("</blockquote>");
    } else {
      writer.write(emptyText);
    }
    writer.write("</div>");
  }
}
