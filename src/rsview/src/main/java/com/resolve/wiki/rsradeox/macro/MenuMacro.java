/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.services.constants.ConstantValues;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.util.MenuMacroUtil;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * {menu:[toc=<true|false>]|[type=<tree|list>]|[width=<width>]|[height=<height>]|[split=<true|false>]|position=<west|east|north|south(defaults to 'west')>|target=<new|same>}               //toc is optional. It has 2 values 'group' or 'item'
        {group:[title=<title>]}
            {item}
                title=sdfsed            //title of the menu item
                desc=sdfsd              //brief description
                url=sdfsd              //url link - opens a new browser window when clicked
                image=ddd.jpg           //image that user should upload it to the wiki document
                status=updated          //optional - predefined values are updated, new, experimental but user can put any text their
                target=new              //optional - opens the link in the new window
            {item}
            {item}
                title=sdfsed
                desc=sdfsd
                url=sdfsd
                image=ddd.jpg
                status=updated
            {item}
        {group}
        {group : [title=<title>]}
            {item}
                title=sdfsed
                desc=sdfsd
                url=sdfsd
                image=ddd.jpg
            {item}
        {group}
    {menu}
    
 * @author jeet.marwah
 *
 */
public class MenuMacro extends BaseLocaleMacro
{
    public static final String TOC = "toc";
    public static final String TYPE = "type";
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String SPLIT = "split";
    public static final String POSITION = "position";
    public static final String TARGET = "target";

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);

        Map<String, Object> attributes = new HashMap<String, Object>();
        String toc = params.get(TOC);
        String type = params.get(TYPE);
        String width = params.get(WIDTH);
        String height = params.get(HEIGHT);
        String split = params.get(SPLIT);
        String position = params.get(POSITION);
        String target = params.get(TARGET);

        if (StringUtils.isNotEmpty(toc))
        {
            attributes.put(TOC, toc.trim().equalsIgnoreCase("false") ? false : true);
        }
        if (StringUtils.isNotEmpty(type))
        {
            attributes.put(TYPE, type.trim().equalsIgnoreCase("tree") ? "tree" : "list");
        }

        if (StringUtils.isNotEmpty(width))
        {
            try
            {
                attributes.put(WIDTH, Long.parseLong(width));
            }
            catch (Exception e)
            {

            }
        }
        if (StringUtils.isNotEmpty(height))
        {
            try
            {
                attributes.put(HEIGHT, Long.parseLong(height));
            }
            catch (Exception e)
            {

            }

        }
        if (StringUtils.isNotEmpty(split))
        {
            attributes.put(SPLIT, split.trim().equalsIgnoreCase("false") ? false : true);
        }

        if (StringUtils.isNotEmpty(position))
        {
            attributes.put(POSITION, position.trim());
        }

        if (StringUtils.isNotEmpty(target) && target.trim().equalsIgnoreCase("same"))
        {
            attributes.put(TARGET, "same");
        }
        else
        {
            attributes.put(TARGET, "new");
        }

        String content = params.getContent();
        String javascript = new MenuMacroUtil(wikiRequestContext, attributes).getMenu(content);

        writer.write(javascript);
    }

    @Override
    public String getLocaleKey()
    {
        return "macro.menu";
    }

}

class MenuGroup
{
    public String title = "";
    public List<MenuItem> items;
}

class MenuItem
{
    public String title = "";
    public String desc = "";
    public String url = "";
    public String image = "";
    public String status = "";

}
