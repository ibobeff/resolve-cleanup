/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.regex;

import java.util.HashMap;
import java.util.Map;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;

/**
 * Class that extends RegexReplaceFilter but reads patterns from a locale file instead of hardwired regex
 * 
 * This class has been refactored to read the values from the hashmap rather from a properties file
 * 
 * @author jeet.marwah
 * 
 */
public abstract class LocaleRegexReplaceFilter extends RegexReplaceFilter
{
	protected abstract String getLocaleKey();

	protected abstract void setValues();//for the match-print 

	//this map is a substitute for the radeox.properties file
	private Map<String, String> values = new HashMap<String, String>();

	protected synchronized void addKeyValue(String key, String value)
	{
		values.put(key, value);
	}

	protected boolean isSingleLine()
	{
		return false;
	}

	public void setInitialContext(InitialRenderContext context)
	{
		super.setInitialContext(context);
		clearRegex();
		setValues();

		String match = values.get(getLocaleKey() + ".match");
		String print = values.get(getLocaleKey() + ".print");

		//System.err.println(getLocaleKey()+": match="+match+" pattern="+print);
		addRegex(match, print, isSingleLine() ? RegexReplaceFilter.SINGLELINE : RegexReplaceFilter.MULTILINE);
	}
}