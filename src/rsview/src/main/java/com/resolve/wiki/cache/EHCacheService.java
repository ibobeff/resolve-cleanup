/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.cache;

/*
import net.sf.ehcache.CacheManager;

import com.resolve.rsview.main.RSContext;
import com.resolve.wiki.WikiException;
*/

/**
 * This singleton provide caching APIs
 * 
 * @author jeet.marwah
 * 
 */
public class EHCacheService
{
    /*
	private static EHCacheService instance;
	private CacheManager sharedCacheManager;
	private CacheManager localCacheManager;

	//get the file locations
	private final static String SHARED_ENCHACHE_LOCATION = RSContext.getProductHome() + "WEB-INF/shared-ehcache.xml";
	private final static String LOCAL_ENCHACHE_LOCATION = RSContext.getProductHome() + "WEB-INF/local-ehcache.xml";

	//only 1 instance of this should be available
	private EHCacheService()
	{
		initShared();
		initLocal();
	}

	public static EHCacheService getInstance()
	{
		if (instance == null)
		{
			synchronized (EHCacheService.class)
			{
				if (instance == null)
				{
					instance = new EHCacheService();
				}
			}
		}

		return instance;
	}

	private void initShared()
	{
		sharedCacheManager = CacheManager.create(SHARED_ENCHACHE_LOCATION);
	}

	private void initLocal()
	{
		localCacheManager = CacheManager.create(LOCAL_ENCHACHE_LOCATION);
	}

	
	public EHCacheCache newLocalCache(String name) throws WikiException
	{
		if (localCacheManager == null)
		{
			initLocal();
		}
		return new EHCacheCache(name, localCacheManager);
	}

	public EHCacheCache newLocalCache(String name, int capacity) throws WikiException
	{
		if (localCacheManager == null)
		{
			initLocal();
		}
		return new EHCacheCache(name, capacity, localCacheManager);
	}

	public EHCacheCache newLocalCache(int capacity) throws WikiException
	{
		if (localCacheManager == null)
		{
			initLocal();
		}
		return new EHCacheCache(Thread.currentThread().getName(), capacity, localCacheManager);
	}
	

	public EHCacheCache newCache(String name) throws WikiException
	{
		if (sharedCacheManager == null)
		{
			initShared();
		}
		return new EHCacheCache(name, sharedCacheManager);
	}

	public EHCacheCache newCache(String name, int capacity) throws WikiException
	{
		if (sharedCacheManager == null)
		{
			initShared();
		}
		return new EHCacheCache(name, capacity, sharedCacheManager);
	}
	*/

}//end of class
