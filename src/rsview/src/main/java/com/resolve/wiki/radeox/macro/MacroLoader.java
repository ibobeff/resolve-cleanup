/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.util.Log;

/**
 * Plugin loader for macros
 * 
 * @author Stephan J. Schmidt
 * @version $Id: MacroLoader.java,v 1.3 2003/06/11 10:04:27 stephan Exp $
 */

public class MacroLoader extends PluginLoader
{
    public Class getLoadClass()
    {
        return Macro.class;
    }

    /**
     * Add a plugin to the known plugin map
     * 
     * @param macro
     *            Macro to add
     */
    public void add(Repository repository, Object plugin)
    {
        if (plugin instanceof Macro)
        {
            // repository.put(((Macro) plugin).getName(), plugin);
            repository.put(((Macro) plugin).getName(), plugin);
        }
        else
        {
            if(Log.log.isDebugEnabled())
            {
                Log.log.debug("MacroLoader: " + plugin.getClass() + " not of Type " + getLoadClass());
            }
        }
    }

}
