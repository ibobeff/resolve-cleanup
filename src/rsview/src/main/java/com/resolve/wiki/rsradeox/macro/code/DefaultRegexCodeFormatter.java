/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro.code;

/*
 * Default code formatter
 *
 */

import com.resolve.wiki.radeox.filter.regex.RegexReplaceFilter;

public class DefaultRegexCodeFormatter extends RegexReplaceFilter
{
	public DefaultRegexCodeFormatter(String regex, String substitute)
	{
		super(regex, substitute);
	}

	public int getPriority()
	{
		return 0;
	}
}
