/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.api.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHeaders;
import org.owasp.esapi.ESAPI;

import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.WikiRequestType;
import com.resolve.wiki.api.WebApi;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * APIs that will be exposed to the Presentation tier. 
 * create an interface based on this implementations
 * 
 * 
 * @author jeet.marwah
 * 
 */
public class WebApiImpl implements WebApi
{

	//spring injected 
	private Wiki wiki;
	private WikiRequestContext wikiRequestContext;

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Implemented methods 
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 * This API is a response to view a wiki document. 
	 * 
	 * eg. http://localhost:8080/resolve/service/wiki/view/Runbook/WebHome?param1=val1&param2=val2
	 * 
	 * @param request
	 * @param response
	 * @throws WikiException
	 */
	@Override
	public String viewAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		wikiRequestContext.setAttribute(ConstantValues.WIKI_REQUEST_TYPE, WikiRequestType.URL);

		basicOperation(wikiRequestContext);
		
		String result = wiki.getRenderedContent(wikiRequestContext);

		return result;
	}//viewAction

	/**
	 * 
	 * This API is a response to edit a wiki document. 
	 * 
	 * eg. http://localhost:8080/resolve/service/wiki/edit/Runbook/WebHome?param1=val1&param2=val2
	 * 
	 * @param request
	 * @param response
	 * @throws WikiException
	 */
	@Override
	public Map<String, String> editAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		basicOperation(wikiRequestContext);
		
		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);

		return hmValues;

	}//editAction
	
//	@Override
//    public Map<String, String> getSectionToEdit(Map<String, Object> payload) throws WikiException
//    {
//	    WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.getSectionToEdit(wikiRequestContext);
//        return hmValues;
//    }//editSection

	
	/**
	 * 
	 * This API is to edit the wiki properties like the summary, title, etc. 
	 * 
	 * @param request
	 * @param response
	 * @throws WikiException
	 */
//	@Override
//	public Map<String, String> getPropertiesToEdit(HttpServletRequest request, HttpServletResponse response) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getPropertiesToEdit(wikiRequestContext);
//
//		return hmValues;
//	}//getPropertiesToEdit
	
	/**
	 * 
	 * This API is to save the wiki document.  
	 * 
	 * @param request
	 * @param response
	 * @throws WikiException
	 */
	@Override
	public Map<String, String> saveAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		basicOperation(wikiRequestContext);
		
		Map<String, String> hmValues = wiki.save(wikiRequestContext);
		return hmValues;
	}//saveAction
	
	/**
	 * 
	 * This API is to save the wiki document.  
	 * 
	 * @param payload
	 * @throws WikiException
	 */
//	@Override
//	public Map<String, String> saveAllActionRPC(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.saveAll(wikiRequestContext);
//		return hmValues;
//	}//saveAllActionRPC

	/**
	 * 
	 * This API is to save the wiki document properties.  
	 * 
	 * @param payload
	 * @throws WikiException
	 */
//	@Override
//	public Map<String, String> saveProperties(Map<String, Object> payload)throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.saveProperties(wikiRequestContext);
//		return hmValues;
//		
//	}//saveProperties
	
	/**
	 * 
	 * This API is to save the wiki document.  
	 * 
	 * @param payload
	 * @throws WikiException
	 */
//	@Override
//	public Map<String, String> saveActionRPC(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.save(wikiRequestContext);
//		return hmValues;
//	}//saveActionRPC

	
//	@Override
//	public  Map<String, Object> diffrevAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
//	{
//		if(request.getParameter(ConstantValues.WIKI_PARAMETER_FLAG) == null 
//				|| request.getParameter(ConstantValues.WIKI_PARAMETER_REV1) == null
//				|| request.getParameter(ConstantValues.WIKI_PARAMETER_REV2) == null)
//		{
//			throw new WikiException(new Exception("Missing parameters for this request. Must have flag, rev1 and rev2 as parameters"));
//		}
//		
//		
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, Object> result = wiki.getDiffOf(wikiRequestContext);
//		return result;
//		
//	}//diffrevAction

	@Override
	public Map<String, Object> viewattachAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		basicOperation(wikiRequestContext);
		
		Map<String, Object> hmValues = wiki.getAllAttachmentsFor(wikiRequestContext);

		return hmValues;
	}//viewattachAction
	
	@Override
	public Map<String, String> lookupAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.lookup);
		
		basicOperation(wikiRequestContext);
		
		Map<String, String> hmValues = wiki.lookupAction(wikiRequestContext);

		return hmValues;
		
	}//lookupAction
	
	@Override
	public Map<String, Object> attachAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		basicOperation(wikiRequestContext);
		
		Map<String, Object> hmValues = wiki.uploadAttachment(wikiRequestContext);

		return hmValues;
	}//attachAction
	
//	@Override
//    public Map<String, String> renameAttachmentRPC(Map<String, Object> payload) throws WikiException
//    {
//	    WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.renameAttachment(wikiRequestContext);
//
//        return hmValues;
//    }//attachAction


//	@Override
//	public void downloadAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
//		
//		basicOperation(wikiRequestContext);
//		
//		wiki.downloadAttachment(wikiRequestContext);
//	}//downloadAction

	@Override
	public Map<String, String> deleteattachAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		String[] deleteAttachIds = request.getParameterValues(ConstantValues.WIKI_PARAMETER_DELETE_ATTACHMENTS);
		if(deleteAttachIds != null && deleteAttachIds.length > 0)
		{
		    String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
			String qryString = SQLUtils.prepareQueryString(deleteAttachIds, dbType);
			wikiRequestContext.setAttribute(ConstantValues.WIKI_PARAMETER_DELETE_ATTACHMENTS, qryString);
		}
		
		basicOperation(wikiRequestContext);
		
		Map<String, String> hmValues = wiki.deleteAttachments(wikiRequestContext);

		return hmValues;
	}//deleteattachAction

//	@Override
//	public Map<String, String> deleteAttachmentsRPC(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.deleteAttachments(wikiRequestContext);
//
//		return hmValues;
//	}//deleteAttachmentsRPC
	
	@Override
	public Map<String, Object> listdocAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		basicOperation(wikiRequestContext);
		
		Map<String, Object> hmValues = wiki.getListdoc(wikiRequestContext);

		return hmValues;
	}//listdocAction


	@Override
	public String viewmodelcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));

		basicOperation(wikiRequestContext);
		
		String result = wiki.getRenderedContent(wikiRequestContext);

		return result;
	}//viewmodelcAction

	
	@Override
	public Map<String, String> editmodelcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		basicOperation(wikiRequestContext);
		
		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);

		return hmValues;
	}//editmodelcAction

	@Override
	public Map<String, String> savemodelcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, request.getAttribute(ConstantValues.WIKI_ACTION_KEY));
		
		basicOperation(wikiRequestContext);
		
		Map<String, String> hmValues = wiki.save(wikiRequestContext);
		return hmValues;
	}//savemodelcAction
	
	@Override
	public String viewexcpcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		return viewmodelcAction(request, response);
	}//viewexcpcAction

	

	@Override
	public Map<String, String> editexcpcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		return editmodelcAction(request, response);
	}//editexcpcAction

	@Override
	public Map<String, String> saveexcpcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		return savemodelcAction(request, response);
	}//saveexcpcAction

	@Override
	public String viewfinalcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		return viewmodelcAction(request, response);
	}//viewfinalcAction

	
	@Override
	public Map<String, String> editfinalcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		return editmodelcAction(request, response);
	}//editfinalcAction

	@Override
	public Map<String, String> savefinalcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		return savemodelcAction(request, response);
	}//savefinalcAction
	
//	@Override
//	public GenericContainerModel getNamespaceInfo(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.qsearch);
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		GenericContainerModel hmValues = wiki.getNamespaceInfo(wikiRequestContext);
//        return hmValues;
//    }// getNamespaceInfo

//    @Override
//    public GenericContainerModel getNamespaceStats(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest) payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse) payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.qsearch);
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//
//        GenericContainerModel hmValues = wiki.getNamespaceStats(wikiRequestContext);
//        return hmValues;
//    }//getNamespaceStats

//    @Override
//	public GenericContainerModel getRelateTagsInfo(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.qsearch);
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		GenericContainerModel hmValues = wiki.getRelateTagsInfo(wikiRequestContext);
//		return hmValues;
//		
//	}//getRelateTagsInfo
	
	
//	@Override
//	public PagingLoadResult<DocumentInfoModel> getSearchResultsFor(RSPagingLoadConfig config, Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.qsearch);
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getSearchResultsFor(config, wikiRequestContext);
//		
//	}//getSearchResultsFor
	
//	@Override
//	public PagingLoadResult<DocumentInfoModel> getAdvanceSearchResultsFor(RSPagingLoadConfig config, Map<String, Object> payload)  throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.asearch);
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getAdvanceSearchResultsFor(config, wikiRequestContext);
//		
//	}//getAdvanceSearchResultsFor

//	@Override
//	public PagingLoadResult<DocumentInfoModel> getListOfDocumentsFor(RSPagingLoadConfig config, Map<String, Object> payload)throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.qsearch);
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getListOfDocumentsFor(config, wikiRequestContext);
//	}//getListOfDocumentsFor
//
//	@Override
//	public PagingLoadResult<DocumentInfoModel> recentChangesSearch(RSPagingLoadConfig config, Map<String, Object> params)
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)params.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)params.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.qsearch);
//		wikiRequestContext.addAttributes(params);
//
//		basicOperation(wikiRequestContext);
//		
//		return wiki.recentChangesSearch(config, wikiRequestContext);
//		
//	}//recentChangesSearch
	
	
//	@Override
//	public List<DocumentInfoModel> getListOfDocuments(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.qsearch);
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getListOfDocuments(wikiRequestContext);
//	}//getListOfDocumentsFor
//	
//	@Override
//	public List<DocumentInfoModel> getRecentlyVisited(Map<String, Object> payload)
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getRecentlyVisited(wikiRequestContext);
//	}//getRecentlyVisited

	
//	@Override
//	public PagingLoadResult<AttachmentModel> getListOfAttachments(RSPagingLoadConfig config, Map<String, Object> params)
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)params.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)params.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.viewattach);
//		wikiRequestContext.addAttributes(params);
//		
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getListOfAttachments(config, wikiRequestContext);
//	}//getListOfAttachments
//	
//    @Override
//	public ArrayList<AttachmentBeanModel> getAttachmentsForDocument(Map<String, Object> params)
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)params.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)params.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.viewattach);
//        wikiRequestContext.addAttributes(params);
//        
//        basicOperation(wikiRequestContext);
//        
//        return wiki.getAttachmentsForDocument(wikiRequestContext);
//    }
//	

//	@Override
//	public PagingLoadResult<AttachmentModel> getListOfGlobalAttachments(RSPagingLoadConfig config, Map<String, Object> params)
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)params.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)params.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.viewattach);
//		wikiRequestContext.addAttributes(params);
//		
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getListOfGlobalAttachments(config, wikiRequestContext);
//	}//getListOfGlobalAttachments
//	
//	@Override
//	public List<AttachmentModel> getGlobalAttachments(Map<String, Object> params)
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)params.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)params.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.viewattach);
//		wikiRequestContext.addAttributes(params);
//		
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getGlobalAttachments(wikiRequestContext);
//		
//	}//getGlobalAttachments
	
//	@Override
//	public Map<String, String> globalAttach(Map<String, Object> params)throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)params.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)params.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(params);
//		
//		basicOperation(wikiRequestContext);
//		
//		return wiki.globalAttach(wikiRequestContext);
//		
//	}//globalAttach
	
//	@Override
//	public Map<String, String> getXMLException(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);
//
//		return hmValues;
//	}//getXMLException

//	@Override
//	public Map<String, String> getXMLFinal(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);
//
//		return hmValues;
//	}//getXMLFinal
//
//	@Override
//	public Map<String, String> getXMLModel(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);
//
//		return hmValues;
//	}//getXMLModel

//	@Override
//	public Map<String, String> getToolbar(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getToolbar(wikiRequestContext); 
//
//		return hmValues;
//	}//getToolbar

//	@Override
//	public Map<String, String> getCurrentPageInfo(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getCurrentPageInfo(wikiRequestContext); 
//
//		return hmValues;
//	}//getCurrentPageInfo

//	@Override
//	public Map<String, String> getURLFor(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getURLForUserRequest(wikiRequestContext); 
//
//		return hmValues;
//	}//getURLFor
	
//	@Override
//	public Map<String, String> getXMLExceptionToEdit(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);
//
//		return hmValues;
//	}//getXMLExceptionToEdit

//	@Override
//	public Map<String, String> getXMLFinalToEdit(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);
//
//		return hmValues;
//	}//getXMLFinalToEdit

//	@Override
//	public Map<String, String> getXMLModelToEdit(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getContentToEdit(wikiRequestContext);
//
//		return hmValues;
//	}//getXMLModelToEdit
	
//	@Override
//	public Map<String, String> cancelEdit(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.cancelEdit(wikiRequestContext);
//
//		return hmValues;
//	}//cancelEdit
	
//
//	@Override
//	public Map<String, List<TagModel>> getTags(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, List<TagModel>> hmValues = wiki.getTags(wikiRequestContext);
//
//		return hmValues;
//	}//getTags
	
//	@Override
//	public List<TagModel> getAllTags()  throws WikiException
//	{
//		return wiki.getAllTags();
//	}//getAllTags
	
//	@Override
//	public List<RoleModel> getAllRoles() throws WikiException
//	{
//		return wiki.getAllRoles();
//	}//getAllRoles
	
//	public List<UserBaseModel> getUsers() throws WikiException
//    {
//        return wiki.getUsers();
//    }
	
//    @Override
//	public ArrayList<RoleBaseModel> getAllRoleModels()
//	{
//        return wiki.getAllRoleModels();
//	}
	
//	public List<RoleModel> getAllRolesWithHomepage()  throws WikiException
//	{
//		return wiki.getAllRolesWithHomepage();
//	}//getAllRolesWithHomepage
	
//	@Override
//	public Map<String, List<RoleModel>> getRoles(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, List<RoleModel>> hmValues = wiki.getRoles(wikiRequestContext);
//
//		return hmValues;
//	}//getRoles
	
//	@Override
//	public PagingLoadResult<RevisionModel> getListOfRevisions(RSPagingLoadConfig config, Map<String, Object> params)
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)params.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)params.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.revision);
//		wikiRequestContext.addAttributes(params);
//
//		basicOperation(wikiRequestContext);
//		
//		return wiki.getListOfRevisions(config, wikiRequestContext);
//	}//getListOfRevisions

//	@Override
//	public Map<String, String> getViewRevisionData(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getViewRevisionData(wikiRequestContext);
//
//		return hmValues;
//	}//getViewRevisionData
	
//    public Map<String, String> commitRevision(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.commitRevision(wikiRequestContext);
//
//        return hmValues;
//    }//commitRevision
	
//	@Override
//	public Map<String, String> rollbackToRevision(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.rollbackToRevision(wikiRequestContext);
//
//		return hmValues;
//	}//rollbackToRevision

//	@Override
//	public Map<String, String> compareRevisions(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.compareRevisions(wikiRequestContext);
//
//		return hmValues;
//	}//compareRevisions
//
//	@Override
//	public Map<String, String> resetRevision(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.resetRevision(wikiRequestContext);
//
//		return hmValues;
//	}//resetRevision
	

//	@Override
//	public Map<String, String> navigateAction(HttpServletRequest request, HttpServletResponse response) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.navigate);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.getValuesForNavigate(wikiRequestContext);
//
//		return hmValues;
//	}//navigateAction
//
//	@Override
//	public String getNavigateData(HttpServletRequest request, HttpServletResponse response) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.navigate);
//		
//		basicOperation(wikiRequestContext);
//		
//		String result = wiki.getNavigateData(wikiRequestContext);
//
//		return result;
//	}//getNavigateData
	
//	@Override
//	public Map<String, String> copy(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.copy(wikiRequestContext);
//
//		return hmValues;
//	}//copy
//
//	@Override
//	public Map<String, String> delete(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.delete(wikiRequestContext);
//
//		return hmValues;
//	}//delete
	
//	@Override
//	public Map<String, String> undelete(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.undelete(wikiRequestContext);
//
//		return hmValues;
//	}//undelete
//
//   @Override
//    public Map<String, String> activateDeactivate(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.activateDeactivate(wikiRequestContext);
//
//        return hmValues;
//    }//lockUnlock
//	    
//
//	@Override
//	public Map<String, String> lockUnlock(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.lockUnlock(wikiRequestContext);
//
//		return hmValues;
//	}//lockUnlock
//	
//	@Override
//	public Map<String, String> hideUnhide(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.hideUnhide(wikiRequestContext);
//
//		return hmValues;
//	}//hideUnhide
//	
//	@Override
//    public Map<String, String> updateRating(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.updateRating(wikiRequestContext);
//
//        return hmValues;
//    }//updateRating

//	public Map<String, String> updateSearchWeight(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.updateSearchWeight(wikiRequestContext);
//
//        return hmValues;
//        
//    }
	
	
//	@Override
//    public Map<String, String> index(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.index(wikiRequestContext);
//
//        return hmValues;
//    }//index
	
//	@Override
//    public List<BaseModel> getAllFeedbackFor(Map<String, Object> payload)  throws WikiException
//    {
//	    WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        List<BaseModel> hmValues = wiki.getAllFeedbackFor(wikiRequestContext);
//
//        return hmValues;
//        
//    }//getAllFeedbackFor
    
//	@Override
//    public Map<String, String> updateFeedback(Map<String, Object> payload)  throws WikiException
//    {
//	    WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        Map<String, String> hmValues = wiki.updateFeedback(wikiRequestContext);
//
//        return hmValues;
//    }//updateFeedback

    
//	@Override
//	public Map<String, String> updateRolesForDocuments(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.updateRolesForDocuments(wikiRequestContext);
//
//		return hmValues;
//	}//updateRolesForDocuments
//
//	@Override
//	public Map<String, String> defaultRoles(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.defaultRoles(wikiRequestContext);
//
//		return hmValues;
//	}//defaultRoles
//
//	@Override
//	public Map<String, String> updateTagsForDocuments(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.updateTagsForDocuments(wikiRequestContext);
//
//		return hmValues;
//	}//updateTagsForDocuments
	
//	@Override
//    public GenericContainerModel getDefaultRolesForNamespace(Map<String, Object> payload) throws WikiException
//    {
//	    WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        return wiki.getDefaultRolesForNamespace(wikiRequestContext);
//    }//getDefaultRolesForNamespace

//	@Override
//	public Map<String, String> updateHomePageForRoles(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.updateHomePageForRoles(wikiRequestContext);
//
//		return hmValues;
//	}//updateHomePageForRoles
//	
//	@Override
//	public Map<String, String> moveOrRename(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.moveOrRename(wikiRequestContext);
//
//		return hmValues;
//	}//moveOrRename
	
//	@Override
//	public Map<String, Object> getModel(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, Object> hmValues = wiki.getModel(wikiRequestContext);
//
//		return hmValues;
//	}//getModel
//	
//	@Override
//	public void saveModel(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		wiki.save(wikiRequestContext);
//	}//saveModel
	
//    @Override
//    public GenericContainerModel prepareImportManifest(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest) payload.get(ConstantValues.WIKI_HTTP_REQUEST),
//                        (HttpServletResponse) payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//
//        return wiki.prepareImportManifest(wikiRequestContext);
//    }// prepareExportManifest
	
//    @Override
//    public GenericContainerModel validateImportManifest(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest) payload.get(ConstantValues.WIKI_HTTP_REQUEST),
//                        (HttpServletResponse) payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//
//        return wiki.validateImportManifest(wikiRequestContext);
//    }//validateImportManifest
    
//    @Override
//    public void uploadImportFile(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        wiki.uploadImportFile(wikiRequestContext);
//        
//    }//uploadImportFile
    
//	public Map<String, String> importModule(Map<String, Object> payload) throws WikiException
//	{
//        return importModule(payload, false);
//	} // importModule
    
//	@Override
//	public Map<String, String> importModule(Map<String, Object> payload, boolean blockIndex) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.importModule(wikiRequestContext, blockIndex);
//
//		return hmValues;
//	}//importModule
//	
//	@Override
//	public Map<String, String> importModule(String moduleName) throws WikiException
//	{
//		return importModule(moduleName, "resolve.maint", false);
//	}//importModule
    
//	public Map<String, String> importModule(String moduleName, boolean blockIndex) throws WikiException
//	{
//		return importModule(moduleName, "resolve.maint", blockIndex);
//	}//importModule
    
//	@Override
//	public Map<String, String> importModule(String moduleName, String username) throws WikiException
//	{
//        return importModule(moduleName, username, false);
//	} // importModule
//	
//	public Map<String, String> importModule(String moduleName, String username, boolean blockIndex) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(null, null);
//		wikiRequestContext.setAttribute(ConstantValues.IMPORT_EXPORT_MODULE_NAME, moduleName);
//		wikiRequestContext.setAttribute(Constants.HTTP_REQUEST_USERNAME, username);
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.impex);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.importModule(wikiRequestContext, blockIndex);
//
//		return hmValues;
//
//	}//importModule
//	
//	public Map<String, Integer> getImpexStatus(String operation)
//    {
//        return wiki.getImpexStatus(operation);
//    }

//	@Override
//    public GenericContainerModel prepareExportManifest(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//        
//        basicOperation(wikiRequestContext);
//        
//        return wiki.prepareExportManifest(wikiRequestContext);
//    }//prepareExportManifest
    
//	@Override
//	public Map<String, String> exportModule(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.exportModule(wikiRequestContext);
//
//		return hmValues;
//
//	}//exportModule
//	
//	@Override
//	public Map<String, Object> downloadModule(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, Object> hmValues = wiki.downloadModule(wikiRequestContext);
//
//		return hmValues;
//
//	}//downloadModule
//	
//	@Override
//	public Map<String, String> exportModule(String moduleName) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(null, null);
//		wikiRequestContext.setAttribute(ConstantValues.IMPORT_EXPORT_MODULE_NAME, moduleName);
//		wikiRequestContext.setAttribute(Constants.HTTP_REQUEST_USERNAME, "admin");
//		wikiRequestContext.setAttribute(ConstantValues.WIKI_ACTION_KEY, WikiAction.impex);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.exportModule(wikiRequestContext);
//
//		return hmValues;
//	}//exportModule
//	
//	@Override
//	public Map<String, Object> uninstallModule(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//		
//		basicOperation(wikiRequestContext);
//		
//		Map<String, Object> hmValues = wiki.uninstallModule(wikiRequestContext);
//
//		return hmValues;
//	}//uninstallModule
	
	
//	@Override
//	public Map<String, String> executeRunbook(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.executeRunbook(wikiRequestContext);
//		return hmValues;
//	}//executeRunbook
//
//	@Override
//	public Map<String, String> debugRunbook(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.debugRunbook(wikiRequestContext);
//		return hmValues;
//	}//debugRunbook
//	
//	@Override
//	public Map<String, String> addNewPage(Map<String, Object> payload) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//		wikiRequestContext.addAttributes(payload);
//
//		basicOperation(wikiRequestContext);
//		
//		Map<String, String> hmValues = wiki.addNewPage(wikiRequestContext);
//		return hmValues;
//	}//addNewPage
	
//	@Override
//	public Map<String, String> current(HttpServletRequest request, HttpServletResponse response) throws WikiException
//	{
//		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
//		basicOperation(wikiRequestContext);
//		Map<String, String> hmValues = wiki.current(wikiRequestContext);
//
//		return hmValues;
//	}//current
	
	@Override
	public Map<String, String> homepage(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
		WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
		basicOperation(wikiRequestContext);
		Map<String, String> hmValues = wiki.getUserHomepage(wikiRequestContext);

		return hmValues;
	}//homepage
	
    @Override
    public String getHtmlForResultWikiTag(HttpServletRequest request, HttpServletResponse response) throws WikiException
    {
        WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
        basicOperation(wikiRequestContext);
        
        String html = ESAPI.encoder().encodeForHTML(wiki.getHtmlForResultWikiTag(wikiRequestContext));

        return html;
    }//getHtmlForResultWikiTag
    
    @Override
    public String getHtmlForDetailWikiTag(HttpServletRequest request, HttpServletResponse response) throws WikiException
    {
        WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
        basicOperation(wikiRequestContext);
        
        String html = ESAPI.encoder().encodeForHTML(wiki.getHtmlForDetailWikiTag(wikiRequestContext));

        return html;
    }//getHtmlForDetailWikiTag
    
//    @Override
//    public GenericSectionModel getSections(Map<String, Object> payload)  throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//
//        return wiki.getSections(wikiRequestContext);
//        
//    }//getSections

//    public HashMap<String, String> submitForm(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//
//        return wiki.submitForm(wikiRequestContext);
//    }
//    
//    public Map<String, String> submitFormExt(FormSubmitDTO formSubmitDTO, HttpServletRequest request, HttpServletResponse response) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext(request, response);
//        wikiRequestContext.setAttribute(HibernateConstants.EXT_FORM_SUBMIT_KEY, formSubmitDTO);
//
//        basicOperation(wikiRequestContext);
//
//        return wiki.submitFormExt(wikiRequestContext);
//    }
//    
//    public Map<String, String> updateExpirationDateForDocuments(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//
//        return wiki.updateExpirationDateForWikiDocs(wikiRequestContext);
//        
//    }
    
//    public Map<String, String> updateLastReviewedDateForWikiDoc(Map<String, Object> payload) throws WikiException
//    {
//        WikiRequestContext wikiRequestContext = prepareWikiRequestContext((HttpServletRequest)payload.get(ConstantValues.WIKI_HTTP_REQUEST), (HttpServletResponse)payload.get(ConstantValues.WIKI_HTTP_RESPONSE));
//        wikiRequestContext.addAttributes(payload);
//
//        basicOperation(wikiRequestContext);
//
//        return wiki.updateLastReviewedDateForWikiDoc(wikiRequestContext);
//    }

    
//    public Boolean checkWikiDocPermission(String docFullName, String userName)
//    {
//        return wiki.checkWikiDocPermission(docFullName, userName);
//    }
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// private methods 
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * prepares the wiki request context object
	 * 
	 */
	private WikiRequestContext prepareWikiRequestContext(HttpServletRequest request, HttpServletResponse response)
	{
		//		WikiRequestContext wikiRequestContext = (WikiRequestContextImpl) RSContext.appContext.getBean("wikiRequestContext");//new WikiRequestContextImpl();
		wikiRequestContext.setHttpServletRequest(request);
		wikiRequestContext.setHttpServletResponse(response);
//		wikiRequestContext.setServletContext(request.getSession().getServletContext());

		//get the wiki object that we can use to serve the wiki docs
		wikiRequestContext.setWiki(wiki);
		wiki.getWikiURLFactory().initialize(wikiRequestContext);
		
		//transfer all values from Request Obj to WikiRequestContext obj
		if(request != null)
		{
			transferParamFromRequestToWikiContext(request, wikiRequestContext);
		}
		
		return wikiRequestContext;
	}//prepareWikiRequestContext
	
	@SuppressWarnings("unchecked")
    private void transferParamFromRequestToWikiContext(HttpServletRequest request, WikiRequestContext wikiRequestContext)
	{
		Enumeration<String> enumNames = request.getParameterNames();
		while(enumNames.hasMoreElements())
		{
			String name = enumNames.nextElement();
			String value = request.getParameter(name);
			
			wikiRequestContext.setAttribute(name, value);
		}
		
		enumNames = request.getAttributeNames();
		while(enumNames.hasMoreElements())
		{
			String name = enumNames.nextElement();
			Object value = request.getAttribute(name);
			
			wikiRequestContext.setAttribute(name, value);
		}

        // add the cookie values in context
        Cookie[] c = request.getCookies();
        if (c != null)
        {
            for (Cookie co : c)
            {
                wikiRequestContext.setAttribute(co.getName(), co.getValue());
            }
        }
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if(StringUtils.isNotBlank(username))
        {
            wikiRequestContext.setWikiUser(ServiceHibernate.getUserRolesAndGroups(username));
            
            boolean addUSerDefaultOrgToParams = false;
            
            if (StringUtils.isBlank((String) wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_ID)) && 
                StringUtils.isBlank((String) wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_NAME)))
            {
                /*
                 *  If both OrgId and OrgName are not in request parameter check for 
                 *  SSO Type in request.  
                 */

                if (StringUtils.isNotBlank((String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_SSO_TYPE)))
                {
                    Log.log.debug("Request has SSOTYPE but no " + Constants.EXECUTE_ORG_ID + " or " + 
                                  Constants.EXECUTE_ORG_NAME + ", setting " + username + " default Org (if set)" +
                                  " in WikiRequestContext");
                    addUSerDefaultOrgToParams = true;
                }
            }
            
            if (!addUSerDefaultOrgToParams)
            {
                /*
                 * If SSO Type is not set in request check for it in Referer
                 */
                
                if (StringUtils.isNotBlank(request.getHeader(HttpHeaders.REFERER)) &&
                    request.getHeader(HttpHeaders.REFERER).contains(Constants.HTTP_REQUEST_SSO_TYPE))
                {
                    Log.log.debug("Request Referer: " + request.getHeader(HttpHeaders.REFERER));
                    
                    if (!request.getHeader(HttpHeaders.REFERER).contains(Constants.EXECUTE_ORG_ID) &&
                        !request.getHeader(HttpHeaders.REFERER).contains(Constants.EXECUTE_ORG_NAME))
                    {
                        Log.log.debug("Request Referer has SSOTYPE but no " + Constants.EXECUTE_ORG_ID + " or " + 
                                        Constants.EXECUTE_ORG_NAME + ", setting " + username + " default Org (if set)" +
                                        " in WikiRequestContext");
                        
                        addUSerDefaultOrgToParams = true;
                    }
                    else
                    {
                        if (request.getHeader(HttpHeaders.REFERER).contains(Constants.EXECUTE_ORG_ID))
                        {
                            String orgId = StringUtils.substringBetween(request.getHeader(HttpHeaders.REFERER), 
                                                                        Constants.EXECUTE_ORG_ID + "%3D", "%26");
                            
                            if (StringUtils.isBlank(orgId))
                            {
                                orgId = StringUtils.substringAfter(request.getHeader(HttpHeaders.REFERER), 
                                                                   Constants.EXECUTE_ORG_ID + "%3D");
                            }
                            
                            if (StringUtils.isNotBlank(orgId))
                            {
                                Log.log.debug("Request Referer has SSOTYPE and " + Constants.EXECUTE_ORG_ID + ", setting " + orgId + 
                                              " in WikiRequestContext");
                                
                                wikiRequestContext.setAttribute(Constants.EXECUTE_ORG_ID, orgId);
                            }
                        }
                        
                        if (request.getHeader(HttpHeaders.REFERER).contains(Constants.EXECUTE_ORG_NAME))
                        {
                            String orgName = StringUtils.substringBetween(request.getHeader(HttpHeaders.REFERER), 
                                                                          Constants.EXECUTE_ORG_NAME + "%3D", "%26");
                            
                            if (StringUtils.isBlank(orgName))
                            {
                                orgName = StringUtils.substringAfter(request.getHeader(HttpHeaders.REFERER), 
                                                                     Constants.EXECUTE_ORG_NAME + "%3D");
                            }
                            
                            if (StringUtils.isNotBlank(orgName))
                            {
                                try
                                {
                                    Log.log.debug("Request Referer has SSOTYPE and " + Constants.EXECUTE_ORG_NAME + ", setting " + 
                                                  URLDecoder.decode(orgName, "UTF-8") + " in WikiRequestContext");
                                
                                    wikiRequestContext.setAttribute(Constants.EXECUTE_ORG_NAME, URLDecoder.decode(orgName, "UTF-8"));
                                }
                                catch (UnsupportedEncodingException usee)
                                {
                                    Log.log.warn("Failed to decode " + orgName + " to UTF-8", usee);
                                }
                            }
                        }
                    }
                }
            }
            
            if (addUSerDefaultOrgToParams)
            {
                // Get Id of default Org (if set) for user as RESOLVE.ORG_ID attribute
                
                String defaultOrgsId = UserUtils.getDefaultOrgIdForUser(username);
                
                if (StringUtils.isNotBlank(defaultOrgsId))
                {
                    wikiRequestContext.setAttribute(Constants.EXECUTE_ORG_ID, defaultOrgsId);
                    Log.log.debug("Set " + username + " default Org Id " + defaultOrgsId + 
                                  " into WikiRequestContext.");
                }
            }
        }
	}//transferParamFromRequestToWikiContext
	
	private void basicOperation(WikiRequestContext wikiRequestContext)
	{
	   
	}//basicOperation

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Getter/Setter methods 
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public Wiki getWiki()
	{
		return wiki;
	}//getWiki

	public void setWiki(Wiki wiki)
	{
		this.wiki = wiki;
	}//setWiki

	public WikiRequestContext getWikiRequestContext()
	{
		return wikiRequestContext;
	}//getWikiRequestContext

	public void setWikiRequestContext(WikiRequestContext wikiRequestContext)
	{
		this.wikiRequestContext = wikiRequestContext;
	}//setWikiRequestContext

}//end of class
