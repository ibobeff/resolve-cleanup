/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.util;

import com.resolve.wiki.radeox.regex.Pattern;
import com.resolve.wiki.radeox.regex.Matcher;
import com.resolve.wiki.radeox.regex.Substitution;
import com.resolve.wiki.radeox.regex.MatchResult;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/*
 * Escapes and encodes Strings for web usage
 *
 */

public class Encoder {
  private final static String DELIMITER = "&\"'<>";
  private final static Map ESCAPED_CHARS = new HashMap();
  // private final static Pattern entityPattern = Pattern.compile("&(#?[0-9a-fA-F]+);");

  static {
    ESCAPED_CHARS.put("&", toEntity('&'));
    ESCAPED_CHARS.put("\"", toEntity('"'));
    ESCAPED_CHARS.put("'", toEntity('\''));
    ESCAPED_CHARS.put(">", toEntity('>'));
    ESCAPED_CHARS.put("<", toEntity('<'));
  }

  /**
   * Encoder special characters that may occur in a HTML so it can be displayed
   * safely.
   * @param str the original string
   * @return the escaped string
   */
  public static String escape(String str) {
    StringBuffer result = new StringBuffer();
    StringTokenizer tokenizer = new StringTokenizer(str, DELIMITER, true);
    while(tokenizer.hasMoreTokens()) {
      String currentToken = tokenizer.nextToken();
      if(ESCAPED_CHARS.containsKey(currentToken)) {
        result.append(ESCAPED_CHARS.get(currentToken));
      } else {
        result.append(currentToken);
      }
    }
    return result.toString();
  }

  public static String unescape(String str) {
    StringBuffer result = new StringBuffer();

    com.resolve.wiki.radeox.regex.Compiler compiler = com.resolve.wiki.radeox.regex.Compiler.create();
    Pattern entityPattern = compiler.compile("&(#?[0-9a-fA-F]+);");

    Matcher matcher = Matcher.create(str, entityPattern);
    result.append(matcher.substitute(new Substitution() {
      public void handleMatch(StringBuffer buffer, MatchResult result) {
        buffer.append(toChar(result.group(1)));
      }
    }));
    return result.toString();
  }

  public static String toEntity(int c) {
    return "&#" + c + ";";
  }

  public static char toChar(String number) {
    return (char) Integer.decode(number.substring(1)).intValue();
  }
}
