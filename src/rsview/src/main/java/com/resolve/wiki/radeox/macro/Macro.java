/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

import java.io.IOException;
import java.io.Writer;

/*
 * Class that implements base functionality to write macros
 *
 */

public interface Macro extends Comparable
{
    /**
     * Get the name of the macro. This is used to map a macro
     * in the input to the macro which should be called.
     * The method has to be implemented by subclassing classes.
     *
     * @return name Name of the Macro
     */
    public String getName();

    /**
     * Get a description of the macro. This description explains
     * in a short way what the macro does
     *
     * @return description A string describing the macro
     */
    public String getDescription();

    /**
     * Get a description of the paramters of the macro. The method
     * returns an array with an String entry for every parameter.
     * The format is {"1: description", ...} where 1 is the position
     * of the parameter.
     *
     * @return description Array describing the parameters of the macro
     */
    public String[] getParamDescription();

    public void setInitialContext(InitialRenderContext context);

    /**
     * Execute the macro. This method is called by MacroFilter to
     * handle macros.
     *
     * @param writer A write where the macro should write its output to
     * @param params Macro parameters with the parameters the macro is called with
     */
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException;
}
