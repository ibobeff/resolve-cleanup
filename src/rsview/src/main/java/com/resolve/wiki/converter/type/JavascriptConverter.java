/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter.type;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.services.util.ParseUtil;
import com.resolve.wiki.converter.AbstractConverter;


/**
 * This class is mapped to the <span> tag that renders javascript
 * This is a special case as javascript tag creates issues when used in TinyMCE. so created this tag 
 * 
 * @author jeet.marwah
 *
 */
public class JavascriptConverter extends AbstractConverter
{
	private static String CSS_NAME = "javascript";
	private static String HTML_START_TAG = "<span class=\"" + CSS_NAME + "\">";
	private static String HTML_END_TAG = "</span>";
	
	private static String WIKI_START_TAG = "{javascript}";
	private static String WIKI_END_TAG = "{javascript}";
	
	private static String JAVASCRIPT_START_TAG = "<script type=\"text/javascript\">";
	private static String JAVASCRIPT_END_TAG = "</script>";
	
	private String pattern = "\\{javascript(.*?)\\{javascript\\}";
	private String scriptPattern = "<script(.*?)</script>";//"(?<!\\\\)\\<script[^>]*\"[^>]*>(.*?)</script>";
	
	@Override
	public String getHTML2WikiPattern()
	{
		return pattern;
	}
	
	@Override
	public String getWiki2HTMLPattern()
	{
		return pattern;
	}
	
	
	@Override
	public String html2wiki(String content)
	{
		//at this point, we will get {javascript}function...{javascript}
		String contentStr = super.html2wiki(content);
		
		Pattern p = Pattern.compile(pattern, Pattern.DOTALL);
		Matcher matcher = p.matcher(contentStr);

		while (matcher.find())
		{
			String originalStr = matcher.group();
			String inbetweenContent = originalStr.substring(originalStr.indexOf("}")+1, originalStr.lastIndexOf("{")).trim();
			String newString = JAVASCRIPT_START_TAG + "\n" + inbetweenContent + "\n" + JAVASCRIPT_END_TAG + "\n";
			
			contentStr = ParseUtil.replaceString(originalStr, newString, contentStr);
		}
		
		return contentStr;
	}
	
	@Override
	public String wiki2html(String content)
	{
		String contentStr = new String(content);
		
		Pattern p = Pattern.compile(scriptPattern, Pattern.DOTALL);
		Matcher matcher = p.matcher(contentStr);

		while (matcher.find())
		{
			String str = matcher.group();
			String newString = str.substring(str.indexOf(">") + 1, str.lastIndexOf("<"));
			newString = newString.replaceAll("\r\n", "<br>");
			newString = newString.replaceAll("\n\r", "<br>");
			newString = newString.replaceAll("\n", "<br>");
			newString = newString.replaceAll("\r", "<br>");
			
			newString = HTML_START_TAG + WIKI_START_TAG + newString + WIKI_END_TAG + HTML_END_TAG + "<br/>";
			
			contentStr = ParseUtil.replaceString(str, newString, contentStr);
		}
		
		
		return contentStr;
	}
	
	protected String remove_HTML_betweenTags1(String content)
	{
		String contentStr = new String(content);
		
		String pattern = "\\{javascript(.*?)\\{javascript\\}";
		String element = "{javascript}";
		
		Pattern p = Pattern.compile(pattern);
		Matcher matcher = p.matcher(content);

		while (matcher.find())
		{
			String str = matcher.group();
			String contentBetweenTags = str.substring(str.indexOf("}") + 1, str.length() - element.length());
			
			contentBetweenTags = removeHTML(contentBetweenTags);
			
			String newString = WIKI_START_TAG + contentBetweenTags + WIKI_END_TAG;
			
			contentStr = ParseUtil.replaceString(str, newString, contentStr);
		}
		
		return contentStr;
	}
	
	@Override
	public String getCSS_NAME()
	{
		return CSS_NAME;
	}

	@Override
	public String getHTML_START_TAG()
	{
		return HTML_START_TAG;
	}

	@Override
	public String getHTML_END_TAG()
	{
		return HTML_END_TAG;
	}

	@Override
	public String getWIKI_END_TAG()
	{
		return WIKI_END_TAG;
	}

	@Override
	public String getWIKI_START_TAG()
	{
		return WIKI_START_TAG;
	}

}//JavascriptConverter
