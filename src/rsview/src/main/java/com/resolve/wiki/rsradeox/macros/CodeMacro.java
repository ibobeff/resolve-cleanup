/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.lang3.StringUtils;

import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

public class CodeMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.code";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {

        //Unique id for the div to render to
        String contentId = "code_content_" + JavaUtils.generateUniqueNumber();
        String divId = "code_" + JavaUtils.generateUniqueNumber();
        String type = params.get("type");
        Boolean showLineNumbers = Boolean.valueOf(StringUtils.isNotEmpty(params.get("showLineNumbers")) ? params.get("showLineNumbers") : "true");
        
        if( StringUtils.isBlank(type) ) type = "java"; //Groovy language by default

        //Build up div and script to render
        StringBuffer str = new StringBuffer();
        str.append("<textarea id=\"%2$s\" style=\"display:none;\">%4$s</textarea>\n");
        str.append("<div id=\"%1$s\"></div>\n");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.Code',\n");
        str.append("        contentId : '%2$s',\n");
        str.append("        type : '%3$s',\n");
        str.append("        showLineNumbers : %5$s\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    var results = glu.view(model).render('%1$s');\n");
        str.append("});\n");
        str.append("</script>");

        String result = String.format(str.toString(), divId, contentId, HttpUtil.sanitizeValue(type), HttpUtil.sanitizeValue(params.getContent()), showLineNumbers);
        Log.log.trace(result);
        writer.write(result);
    }
}
