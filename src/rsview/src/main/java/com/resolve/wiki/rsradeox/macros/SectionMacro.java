/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.owasp.esapi.ESAPI;

import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

public class SectionMacro extends BaseLocaleMacro
{
	private final Pattern titleEscapePattern = Pattern.compile("(:title=.*)(?<txt>\\[.*\\])") ;  // Pattern for escaping "[...]" in macro title value, which would be rendered as html link by xwiki engine.
	
    public String getLocaleKey()
    {
        return "macro.section";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        String content = StringUtils.chomp(params.getContent());
        //String id = params.get("id");
        String height = (StringUtils.isNotEmpty(params.get("height")) ? params.get("height") : "-1");
        String hidden = (StringUtils.isNotEmpty(params.get("hidden")) ? params.get("hidden") : "false");
        String active = (StringUtils.isNotEmpty(params.get("active")) ? params.get("active") : "true");
        if(active.equals("false")) {
            writer.write("");
            return;
        }
        //Get rid of type in the future, but have to keep it for legacy sections
        //Also get rid of the ridiculous checks for type to determine whether to apply the height.
        //We have to do this for now because all sections have height from the old system (because it wasn't using it, it just included it by default for no good reason).
//        String type = StringUtils.isNotEmpty(params.get("type")) ? params.get("type") : "";

        StringBuffer output = new StringBuffer();
        List<String> styles = new ArrayList<String>();
        
        int heightValue = Integer.valueOf(height);
        boolean hiddenValue = Boolean.valueOf(hidden);
        styles.add("height:" + heightValue + "px;overflow:auto;");
        if(hiddenValue)
            styles.add("display:none;");
        if(heightValue!=-1||hiddenValue) 
            output.append("<div style=\""+StringUtils.join(styles,"")+"\">");
            
        if (StringUtils.isNotBlank(content))
        {
            output.append(content);
        }

        if(heightValue!=-1||hiddenValue)
        {
            output.append("</div>");
        }
    
        String html = null;
        StringBuffer b = new StringBuffer();
        Matcher m = titleEscapePattern.matcher(output);
    	while (m.find())
    	{
    		StringBuffer s = new StringBuffer();
    		m.appendReplacement(b, s.append("$1").append("[").append("${txt}").append("]").toString());
    	}
    	m.appendTail(b);
    	html = b.toString();
        
        int maxFileSize = Constants.MIN_FILE_UPLOAD_SIZE_IN_MB * 1024 * 1024 * 2; // 100MB .docx (Java 200MB)
        
        long configuredMaxFileSize = PropertiesUtil.getPropertyLong(Constants.MAX_FILE_UPLOAD_SIZE_IN_MB_SYSTEM_PROPERTY) * 1024 * 1024 * 2;
        
        if (configuredMaxFileSize > maxFileSize && configuredMaxFileSize < Integer.MAX_VALUE)
        {
            maxFileSize = (int)configuredMaxFileSize;
        }
            
        try
        {
            String validatedHtml = ESAPI.validator().getValidInput("Wiki Section Macro Sanitize Section Data", html, "ResolveText", maxFileSize, true, false);
            if (validatedHtml!=null)
            {
            	writer.write(validatedHtml);
            }
            else
            {
            	writer.write("");
            }
        }
        catch(Exception e)
        {
            Log.log.info("Failed to sanitize section data due to " + e.getLocalizedMessage());
            writer.write("Failed to sanitize section data");
        }
    }
}
