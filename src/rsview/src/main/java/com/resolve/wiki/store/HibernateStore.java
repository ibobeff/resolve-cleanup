/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.store;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.hibernate.type.StringType;

import com.resolve.dto.AttachmentModel;
import com.resolve.dto.RightType;
import com.resolve.persistence.dao.ResolveTagDAO;
import com.resolve.persistence.dao.ResolveWikiLookupDAO;
import com.resolve.persistence.dao.WikiAttachmentContentDAO;
import com.resolve.persistence.dao.WikiAttachmentDAO;
import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.dao.WikidocAttachmentRelDAO;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.ResolveWikiLookup;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.WikiException;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.store.helper.StoreUtility;
import com.resolve.wiki.web.vo.DocumentInfoVO;

public class HibernateStore implements WikiStore
{
    public int maxResultGlobal = 0;
    public Integer sqlCount = 0;
    public String searchSqlGlobal = "";
    //spring injected
//    private WikiRightService wikiRightService;//handle for finding out access rights. 

//    private WorksheetDAO worksheetDao = CassandraDaoFactory.getWorksheetDAO();
//    private ResolveExecuteRequestDAO executeRequestDao = CassandraDaoFactory.getResolveExecuteRequestDAO();
//    private ResolveActionResultDAO actionResultDao = CassandraDaoFactory.getResolveActionResultDAO();

    @Override
    public void deleteAttachments(String UParentWikidocId, String listOfids)
    {
        String idsToDeleteContent = getListToDeleteContent(UParentWikidocId, listOfids);
        boolean isDeleted = false;

        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {

	            //only the parent doc can delete the attachments ...for others, only references must be deleted
	            if (idsToDeleteContent != null && idsToDeleteContent.trim().length() > 0)
	            {
	                //HibernateUtil.deleteQuery("WikiAttachmentContent", "wikiAttachment in (" + idsToDeleteContent + ")");
	                Collection<String> idsToDeleteColl = SQLUtils.convertQueryStringToSet(idsToDeleteContent);
	                HibernateUtil.deleteQuery("WikiAttachmentContent", "wikiAttachment", idsToDeleteColl);
	                //HibernateUtil.deleteQuery("WikiAttachment", "sys_id in (" + idsToDeleteContent + ")");
	                HibernateUtil.deleteQuery("WikiAttachment", "sys_id", idsToDeleteColl);
	                //HibernateUtil.deleteQuery("WikidocAttachmentRel", "wikiAttachment in (" + idsToDeleteContent + ")");
	                HibernateUtil.deleteQuery("WikidocAttachmentRel", "wikiAttachment", idsToDeleteColl);
	            }
	
	            //HibernateUtil.deleteQuery("WikidocAttachmentRel", "wikiAttachment in (" + listOfids.trim() + ") and wikidoc = '" + UParentWikidocId + "' ");
	            Collection<String> idsColl = SQLUtils.convertQueryStringToSet(listOfids.trim());
	            HibernateUtil.deleteQuery("WikidocAttachmentRel", "wikiAttachment", idsColl, "wikidoc", UParentWikidocId);
			
            });

            //just a flag to make sure that delete when through successfully
            isDeleted = true;
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        if (isDeleted)
        {
            //log it in the index table as deleted
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(ConstantValues.DELETE_INDEX, true);
            params.put(ConstantValues.WIKIDOCUMENT_SYS_ID, UParentWikidocId);
            params.put(ConstantValues.ATTACHMENT_SYS_IDS, listOfids);

            //            SystemExecutor.execute(com.resolve.services.hibernate.util.UpdateWikiDocRelations.class, "index", params);
            //            Main.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//            Main.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
        }
    }//deleteAttachments

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public String getAccessRightsFor(String resourceType, String resourceName, String resourceId, RightType rightType)
    {
        String roles = "";
        //either resourceName or resourceSys_id should be available, else return empty string
        if ((resourceId == null && resourceName == null)) return roles;

        if (rightType == null)
        {
            rightType = RightType.view;
        }

        StringBuffer sql = new StringBuffer("select a.");
        String selectColName = "";
        if (rightType == RightType.admin)
        {
            selectColName = "UAdminAccess";
        }
        else if (rightType == RightType.edit)
        {
            selectColName = "UWriteAccess";
        }
        else if (rightType == RightType.execute)
        {
            selectColName = "UExecuteAccess";
        }
        else
        {
            selectColName = "UReadAccess";
        }

        try
        {
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            sql.append(selectColName).append(" as rights from AccessRights ");
            sql.append(" a where 1=1 ");
            if (resourceId != null && resourceId.trim().length() > 0)
            {
                //sql.append(" and a.UResourceId = '").append(resourceId.trim()).append("' ");
                sql.append(" and a.UResourceId = :UResourceId");
                queryParams.put("UResourceId", resourceId.trim());
            }
            if (resourceType != null && resourceType.trim().length() > 0)
            {
                //sql.append(" and a.UResourceType = '").append(resourceType.trim()).append("' ");
                sql.append(" and a.UResourceType = :UResourceType");
                queryParams.put("UResourceType", resourceType.trim());
            }
            if (resourceName != null && resourceName.trim().length() > 0)
            {
                //sql.append(" and a.UResourceName = '").append(resourceName.trim()).append("' ");
                sql.append(" and a.UResourceName = :UResourceName").append(resourceName.trim()).append("' ");
                queryParams.put("UResourceName", resourceName.trim());
            }

          HibernateProxy.setCurrentUser("system");
            roles = (String) HibernateProxy.execute(() -> {

                Query q = HibernateUtil.createQuery(sql.toString());
                
                if (queryParams != null && !queryParams.isEmpty())
                {
                    for (String queryParamName : queryParams.keySet())
                    {
                        q.setParameter(queryParamName, queryParams.get(queryParamName));
                    }
                }
                
                List<String> lRights = q.list();
                if (lRights.size() > 0)
                {
                    return lRights.get(0);
                }
                
                return "";
            });

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return roles;
    }

    @Override
    public List<WikiAttachment> getAllAttachmentsFor(String wikiDocId, String wikiDocName)
    {
        List<WikiAttachment> lAttach = new ArrayList<WikiAttachment>();
        if ((wikiDocId == null || wikiDocId.trim().length() == 0) && (wikiDocName == null || wikiDocName.trim().length() == 0))
        {
            return lAttach;
        }

        wikiDocId = wikiDocId != null ? wikiDocId.trim() : null;
        wikiDocName = wikiDocName != null ? wikiDocName.trim() : null;

        try
        {
            //DO NOT SELECT THE CONTENT AS WE WILL NOT BE DISPLAYING THAT. Seperate API is their to DOWNLOAD that content
            //If we select CONTENT in this, it will hinder the performance as the files can be very big in size
            WikiDocument wikiDoc = new WikiDocument();
            wikiDoc.setUFullname(wikiDocName);

            String wikiDocIdFinal = wikiDocId;
            WikiDocument wikiDocFinal = wikiDoc;
          HibernateProxy.setCurrentUser("system");
            wikiDoc = (WikiDocument) HibernateProxy.execute(() -> {
            	 WikiDocumentDAO wikiDao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
                 Object result = wikiDocIdFinal != null ? (WikiDocument) wikiDao.findById(wikiDocIdFinal) : wikiDao.find(wikiDocFinal).get(0);

                 List<WikidocAttachmentRel> listRel = (List<WikidocAttachmentRel>) wikiDocFinal.getWikidocAttachmentRels();
                 if (listRel.size() > 0)
                 {
                     for (WikidocAttachmentRel rel : listRel)
                     {
                         lAttach.add(rel.getWikiAttachment());
                     }
                 }
                 
                 return result;
            });
           
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return lAttach;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<WikiDocument> findWikiDocumentByHQL(String hql) throws WikiException
    {
        List<WikiDocument> lWiki = new ArrayList<WikiDocument>();
        try
        {
          HibernateProxy.setCurrentUser("system");
        	lWiki = (List<WikiDocument>) HibernateProxy.execute(() -> {

	            Query query = HibernateUtil.createQuery(hql);
	            return query.list();

        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return lWiki;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public WikiAttachment findAttachment(String attachmentName) throws WikiException
    {
        WikiAttachment att = null;
        if (attachmentName == null || attachmentName.trim().length() == 0)
        {
            return att;
        }

        String sql = " from WikiAttachment where UFilename like '%" + attachmentName + "%' ";
        List<WikiAttachment> list = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
            list = (List<WikiAttachment>) HibernateProxy.execute(() -> {

	            Query query = HibernateUtil.createQuery(sql);
	            return query.list();

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        if (list != null && list.size() > 0)
        {
            att = list.get(0);
            populateAttachmentContent(att);
        }

        return att;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public WikiAttachment findWikiAttachmentFor(String docName, String fileName)
    {
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	return (WikiAttachment) HibernateProxy.execute(() -> {
        		WikiAttachment file = null;
        		
				String sql = "select c from WikiDocument a, WikidocAttachmentRel b, WikiAttachment c where LOWER(a.UFullname) = '"
						+ docName.trim().toLowerCase() + "' and "
						+ " a.sys_id = b.wikidoc and b.wikiAttachment = c.sys_id and LOWER(c.UFilename) = '"
						+ fileName.trim().toLowerCase() + "'";

	            Query q = HibernateUtil.createQuery(sql);
	            List<WikiAttachment> list = q.list();
	            if (list != null && list.size() > 0)
	            {
	                file = list.get(0);
	                populateAttachmentContent(file);
	            }

	            return file;
        	});

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return null;
    }//findWikiAttachmentFor

    @Override
    public WikiDocument find(String docFullName) throws WikiException
    {
        return StoreUtility.find(docFullName);
    }

    @Override
    public WikiDocument find(String namespace, String filename) throws WikiException
    {
        return StoreUtility.find(namespace, filename);
    }


    @Override
    public List<DocumentInfoVO> getListOfDocumentsFor(String namespace)
    {
        List<DocumentInfoVO> list = new ArrayList<DocumentInfoVO>();
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	WikiDocumentDAO taskDao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
                WikiDocument doc = new WikiDocument();
                doc.setUNamespace(namespace);
                List<WikiDocument> lDoc = taskDao.find(doc);

                for (WikiDocument d : lDoc)
                {
                    DocumentInfoVO vo = new DocumentInfoVO();
                    vo.setNamespace(namespace);
                    vo.setDocumentName(d.getUName());
                    vo.setModifiedBy(d.getSysUpdatedBy());
                    vo.setModifiedOn(d.getSysUpdatedOn());

                    list.add(vo);

                }
            });            
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }

        return list;
    }

    /**
     * This method return all resolvetags from database
     */
    @SuppressWarnings("unchecked")
	public static List<ResolveTag> getAllResolveTagsFromDB()
    {
        List<ResolveTag> results = new ArrayList<ResolveTag>();

        try
        {
          HibernateProxy.setCurrentUser("system");
            results = (List<ResolveTag>) HibernateProxy.execute(() -> {
            	ResolveTagDAO taskDao = HibernateUtil.getDAOFactory().getResolveTagDAO();
                return taskDao.findAll();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return results;
    }

    @Override
    public void addAttachment(WikiDocument wikidoc, WikiAttachment attachment, boolean isImportExport) throws WikiException
    {

        try
        {
          HibernateProxy.setCurrentUser(attachment.getSysCreatedBy());
	        HibernateProxy.execute(() -> {
	        	//get the existing attachment if there 
	        	WikiAttachment dbAttachment = getAttachmentFor(wikidoc.getUFullname(), attachment.getUFilename());
	
	            WikiAttachmentDAO daoWikiAttachment = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
	            WikiAttachmentContentDAO daoWikiAttachmentContent = HibernateUtil.getDAOFactory().getWikiAttachmentContentDAO();
	            WikidocAttachmentRelDAO daoREL = HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO();
	
	            WikiAttachmentContent wac = attachment.ugetWikiAttachmentContent();
	            wac.setWikiAttachment(attachment);
	            wac.setSysCreatedBy(attachment.getSysCreatedBy());
	            wac.setSysCreatedOn(attachment.getSysCreatedOn());
	            wac.setSysUpdatedBy(attachment.getSysUpdatedBy());
	            wac.setSysUpdatedOn(attachment.getSysUpdatedOn());
	
	            WikidocAttachmentRel rel = new WikidocAttachmentRel();
	            rel.setSysCreatedBy(attachment.getSysCreatedBy());
	            rel.setSysCreatedOn(attachment.getSysCreatedOn());
	            rel.setWikidoc(wikidoc);
	            rel.setParentWikidoc(wikidoc);
	            rel.setWikiAttachment(attachment);
	
	            //if the attachment exist for that document, then update the contents , else add it
	            if (dbAttachment != null)//UPDATE
	            {
	                dbAttachment = daoWikiAttachment.findById(dbAttachment.getSys_id());
	                dbAttachment.usetWikiAttachmentContent(wac);
	                dbAttachment.setSysUpdatedBy(attachment.getSysUpdatedBy());
	                dbAttachment.setSysUpdatedOn(attachment.getSysUpdatedOn());
	
	                updateAttachment(dbAttachment);
	
	                //refresh the object
	                attachment.setSys_id(dbAttachment.getSys_id());
	                //                attachment = daoWikiAttachment.findById(dbAttachment.getsys_id());
	
	            }
	            else
	            //INSERT
	            {
	                daoWikiAttachment.persist(attachment);
	                daoWikiAttachmentContent.persist(wac);
	                daoREL.persist(rel);
	            }
	
	            //log it in the index table, not if it is from Importing
	            if (!isImportExport)
	            {
	                indexAttachment(wikidoc.getSys_id(), attachment.getSys_id());
	            }
	        });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        finally
        {
            //to avoid memory leaks and release memory
            attachment.usetWikiAttachmentContent(null);
        }
        

    }//addAttachment

    @Override
    public void updateAttachment(WikiAttachment attachment)
    {
        WikiAttachmentContent wac = attachment.ugetWikiAttachmentContent();
        if (wac != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(attachment.getSysCreatedBy());
            	HibernateProxy.execute(() -> {
	                WikiAttachmentContentDAO daoWikiAttachmentContent = HibernateUtil.getDAOFactory().getWikiAttachmentContentDAO();
	                WikiAttachmentDAO daoWikiAttachment = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
	
	                //add the new content                
	                wac.setWikiAttachment(attachment);
	                daoWikiAttachment.persist(attachment);
	                daoWikiAttachmentContent.persist(wac);

            	});

            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
            finally
            {
                //to avoid memory leaks and release memory
                attachment.usetWikiAttachmentContent(null);
            }
        }
    } // updateAttachment

    @Override
    public void renameAttachment(WikiAttachment attachment)
    {
        try
        {
          HibernateProxy.setCurrentUser(attachment.getSysCreatedBy());
        	HibernateProxy.execute(() -> {
        		WikiAttachmentDAO daoWikiAttachment = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
                daoWikiAttachment.persist(attachment);
        	});
            

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        finally
        {
            //to avoid memory leaks and release memory
            attachment.usetWikiAttachmentContent(null);
        }
    }

    @Override
    public void globalAttach(WikiDocument attachToWikidoc, List<AttachmentModel> listOfAttachment) throws WikiException
    {
        WikiDocument globalDocument = find(ConstantValues.NAMESPACE_SYSTEM, ConstantValues.DOCUMENT_ATTACHMENT);
        if (globalDocument == null)
        {
            throw new WikiException(new Exception("Global document (System.Attachment) does not exist."));
        }

        for (AttachmentModel attachModel : listOfAttachment)
        {
            globalAttach(globalDocument, attachToWikidoc, attachModel);
        }
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<ResolveWikiLookup> getWikiLookup() throws WikiException
    {
        List<ResolveWikiLookup> list = new ArrayList<ResolveWikiLookup>();

        try
        {
          HibernateProxy.setCurrentUser("system");
        	list = (List<ResolveWikiLookup>) HibernateProxy.execute(() -> {
        		ResolveWikiLookupDAO dao = HibernateUtil.getDAOFactory().getResolveWikiLookupDAO();
                return dao.findAll();
        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return list;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public WikiAttachment getAttachmentFor(String wikidocFullName, String fileName)
    {
        WikiAttachment att = null;
        //      String sql = "select wa from WikiDocument as wd, WikidocAttachmentRel as rel, WikiAttachment as wa where wd.wikidocAttachmentRel = rel.wikiDocument" +
        //              " and rel.wikiAttachment = wa.wikidocAttachmentRel and wd.UFullname= :UFullname and wa.UFilename = :UFilename";

        String sql = "select wa from WikiDocument as wd, WikidocAttachmentRel as rel, WikiAttachment as wa where wd.sys_id = rel.wikidoc " + " and rel.wikiAttachment = wa.sys_id and wd.UFullname= :UFullname and wa.UFilename = :UFilename";

        List<WikiAttachment> list = null;

        try
        {

          HibernateProxy.setCurrentUser("system");
            list = (List<WikiAttachment>) HibernateProxy.execute(() -> {
                Query query = HibernateUtil.createQuery(sql);
                query.setParameter("UFullname", wikidocFullName, StringType.INSTANCE);
                query.setParameter("UFilename", fileName, StringType.INSTANCE);
                return query.list();
            });

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        if (list != null && list.size() > 0)
        {
            att = list.get(0);
        }

        return att;
    }

    /**
     * returns a list of name of documents referenced in the CONTENT of a wiki document
     */
    @SuppressWarnings("unchecked")
	private static List<String> getReferencedWikiDocs(String wikiDocFullName)
    {
        List<String> list = new ArrayList<String>();

        String sql = "select cwwr.UWikidocFullname from ContentWikidocWikidocRel cwwr, WikiDocument wd " + " where cwwr.contentWikidoc = wd.sys_id and wd.UFullname = :UFullname";

        try
        {
          HibernateProxy.setCurrentUser("system");
            list = (List<String>) HibernateProxy.execute(() -> {
            	Query q = HibernateUtil.createQuery(sql);
                q.setParameter("UFullname", wikiDocFullName, StringType.INSTANCE);
                return  q.list();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return list;
    }

    private WikiAttachment findByIdWikiAttachment(WikiAttachment obj)
    {
        WikiAttachment wikiattach = null;

        try
        {
          HibernateProxy.setCurrentUser("system");
        	wikiattach = (WikiAttachment) HibernateProxy.execute(() -> {

	            WikiAttachmentDAO dao = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
	            return dao.findById(obj.getSys_id());
	
            });
            
            populateAttachmentContent(wikiattach);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return wikiattach;

    }

    private WikiAttachment findByIdWikiAttachment(String sys_id)
    {
        WikiAttachment att = new WikiAttachment();
        att.setSys_id(sys_id);

        return findByIdWikiAttachment(att);
    }

    @SuppressWarnings("unchecked")
    private void populateAttachmentContent(WikiAttachment att)
    {
        String sql = " from WikiAttachmentContent where wikiAttachment = '" + att.getSys_id() + "'";        

        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	WikiAttachmentContent wac = null;
	            List<WikiAttachmentContent> list = HibernateUtil.createQuery(sql).list();
	            if (list != null && list.size() > 0)
	            {
	                wac = list.get(0);
	            }
	
	            att.usetWikiAttachmentContent(wac);

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }

    /**
     * filter out the ids for which the content can be deleted. So if the document owns a attachment, it can delete the content, rest of the docs can
     * only refer it. For that there is a rec in the WikidocAttachmentRel table.
     * 
     * @param UParentWikidocId
     * @param listOfids
     * @return
     */
    @SuppressWarnings("unchecked")
	private String getListToDeleteContent(String UParentWikidocId, String listOfids)
    {
        String ids = "";
       

        Collection<WikidocAttachmentRel> list = null;//doc.getWikidocAttachmentRel();

        try
        {
          HibernateProxy.setCurrentUser("system");
          list = (Collection<WikidocAttachmentRel>) HibernateProxy.execute(() -> {
        	  
        	  WikiDocument doc = new WikiDocument();
              doc.setSys_id(UParentWikidocId);
               WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
               doc = dao.findById(UParentWikidocId);
               Collection<WikidocAttachmentRel> result = null;
               
			if (doc != null)
               {
                   result  = doc.getWikidocAttachmentRels();
                   result.size();//just to make sure its loaded the data
               }
               
               return result;
           });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        //prepare the qry string 
        if (list != null && list.size() > 0)
        {
            for (WikidocAttachmentRel rel : list)
            {
                if (UParentWikidocId.equals(rel.getParentWikidoc().getSys_id()))
                {
                    //if there is any other ParentSys_Id other then the one specified, in that case, the content must not be deleted as
                    //it is referenced by other documents.
                    boolean isAttachmentContentReferencedByAnyOtherWikidoc = isAttachmentContentReferencedByAnyOtherWikidoc(rel.getWikiAttachment().getSys_id(), UParentWikidocId);

                    if (!isAttachmentContentReferencedByAnyOtherWikidoc)
                    {
                        if (listOfids.indexOf(rel.getWikiAttachment().getSys_id()) > -1)
                        {
                            if (ids.trim().length() > 0)
                            {
                                ids = ids + ",'" + rel.getWikiAttachment().getSys_id() + "'";
                            }
                            else
                            {
                                ids = "'" + rel.getWikiAttachment().getSys_id() + "'";
                            }
                        }
                    }
                }
            }
        }

        return ids;
    }//getListToDeleteContent

    @SuppressWarnings("unchecked")
	private boolean isAttachmentContentReferencedByAnyOtherWikidoc(String wikiAttachmentId, String UParentWikidocId)
    {
        boolean isAttachmentContentReferencedByAnyOtherWikidoc = false;
        Collection<WikidocAttachmentRel> wikidocAttachmentRels = null;

        try
        {
          HibernateProxy.setCurrentUser("system");
        	wikidocAttachmentRels = (Collection<WikidocAttachmentRel>) HibernateProxy.execute(() -> {

	            WikiAttachmentDAO dao = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
	
	            WikiAttachment attachment = dao.findById(wikiAttachmentId);
	            return attachment.getWikidocAttachmentRels();

        	});
        	
        	if (wikidocAttachmentRels != null)
        	{
        		wikidocAttachmentRels.size();//this is to load it in memory
        	}
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        if (wikidocAttachmentRels != null && wikidocAttachmentRels.size() > 0)
        {
            //if there is any other ParentSys_Id other then the one specified, in that case, the content must not be deleted as
            //it is referenced by other documents.
            for (WikidocAttachmentRel rel : wikidocAttachmentRels)
            {
                String parentSys_Id = rel.getParentWikidoc().getSys_id();
                if (!parentSys_Id.equals(UParentWikidocId))
                {
                    isAttachmentContentReferencedByAnyOtherWikidoc = true;
                    break;
                }
            }
        }

        return isAttachmentContentReferencedByAnyOtherWikidoc;

    }//isAttachmentContentReferencedByAnyOtherWikidoc

    private void globalAttach(WikiDocument globalDocument, WikiDocument attachToWikidoc, AttachmentModel attachModel)
    {
//        ResolveIndexLog resolveIndexLog = null;
        WikiAttachment attachment = findByIdWikiAttachment(attachModel.getSys_id());

        WikidocAttachmentRel rel = new WikidocAttachmentRel();
        rel.setWikidoc(attachToWikidoc);
        rel.setWikiAttachment(attachment);
        rel.setParentWikidoc(globalDocument);
        rel.setSysCreatedBy(attachModel.getCreatedBy());
        rel.setSysCreatedOn(GMTDate.getDate());

        try
        {

          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                WikidocAttachmentRelDAO dao = HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO();
                dao.persist(rel);

        	});

            //log it in the index table
            indexAttachment(attachToWikidoc.getSys_id(), attachment.getSys_id());

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }

    private void indexAttachment(String wikidocId, String attachmentId)
    {
        //log it in the index table
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ConstantValues.WD_ATTACHMENT_SYS_ID, wikidocId);
        params.put(ConstantValues.ATTACHMENT_SYS_ID, attachmentId);
        //        SystemExecutor.execute(com.resolve.services.hibernate.util.UpdateWikiDocRelations.class, "index", params);
        //        Main.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        Main.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);

    }

    @SuppressWarnings("unused")
    private Map<String, String> getATParameters(String taskName, String taskNamespace, String selectNodeId, String[] actionNameArr, String[] actionNamespaceArr, String[] descriptionArr, String[] actionWikiArr, String[] nodeIdArr)
    {
        Map<String, String> result = new HashMap<String, String>();
        String description = null;
        String actionWiki = null;
        String nodeId = null;

        if (StringUtils.isNotEmpty(taskName))
        {
            for (int count = 0; count < actionNameArr.length; count++)
            {
                String atName = actionNameArr[count];
                String atNamespace = actionNamespaceArr[count];
                String atDescription = descriptionArr[count];
                String atActionWiki = actionWikiArr[count];
                String atNodeId = nodeIdArr[count];

                if (StringUtils.isNotEmpty(atName) && taskName.equalsIgnoreCase(atName))
                {
                    //continue as the namespace are not equal. if the namespace is empty/null, 
                    //then we assume its the actiontask and use that description
                    if (StringUtils.isNotEmpty(atNamespace) && !taskNamespace.equalsIgnoreCase(atNamespace))
                    {
                        continue;
                    }

                    //compare the nodeId
                    if (StringUtils.isNotEmpty(atNodeId) && !selectNodeId.equalsIgnoreCase(atNodeId))
                    {
                        continue;
                    }

                    description = atDescription;
                    actionWiki = atActionWiki;
                    nodeId = atNodeId;

                    description = getNullIfEmptyString(description);
                    actionWiki = getNullIfEmptyString(actionWiki);
                    nodeId = getNullIfEmptyString(nodeId);

                    result.put("description", description);
                    result.put("actionWiki", actionWiki);
                    result.put("nodeId", nodeId);

                    break;
                }
            }//end of for loop

        }
        return result;
    }//getATParameters

    private String getNullIfEmptyString(String str)
    {
        String tempStr = str;

        if (StringUtils.isNotEmpty(tempStr))
        {
            tempStr = tempStr.trim();
        }

        //making sure it returns null if its empty string
        if (StringUtils.isEmpty(tempStr))
        {
            tempStr = null;
        }

        return tempStr;

    }
}
