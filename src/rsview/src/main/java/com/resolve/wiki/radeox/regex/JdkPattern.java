/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.regex;

/*
 * Class which stores regular expressions
 * Implementation for regex package in JDK 1.4
 *
 */

public class JdkPattern implements Pattern {
  private String regex;
  private boolean multiline;
  private java.util.regex.Pattern internPattern;

  public JdkPattern(String regex, boolean multiline) {
    this.regex = regex;
    this.multiline = multiline;

    internPattern = java.util.regex.Pattern.compile(regex,
        java.util.regex.Pattern.DOTALL | (multiline ? java.util.regex.Pattern.MULTILINE : 0));
  }

  protected java.util.regex.Pattern getPattern() {
    return internPattern;
  }

  public String getRegex() {
    return regex;
  }

  public boolean getMultiline() {
    return multiline;
  }
}