/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import java.io.Writer;

import com.resolve.rsview.main.RSContext;
import com.resolve.util.Log;
import com.resolve.wiki.radeox.api.engine.IncludeRenderEngine;
import com.resolve.wiki.radeox.api.engine.RenderEngine;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.RegexTokenFilter;
import com.resolve.wiki.radeox.macro.Macro;
import com.resolve.wiki.radeox.macro.PostMacroRepository;
import com.resolve.wiki.radeox.macro.Repository;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.radeox.regex.MatchResult;
import com.resolve.wiki.radeox.util.StringBufferWriter;

/*
 * Class that finds snippets (macros) like {link:neotis|http://www.neotis.de}
 * ---> <a href="....> {!neotis} -> include neotis object, e.g. a wiki page
 * 
 * Macros can built with a start and an end, e.g. {code} ... {code}
 * 
 * @author stephan
 * 
 * @team sonicteam
 * 
 * @version $Id: MacroFilter.java,v 1.18 2004/04/15 13:56:14 stephan Exp $
 */

public class PostMacroFilter extends RegexTokenFilter
{
    // private static MacroFilter instance;

    // Map of known macros with name and macro object
    private PostMacroRepository macros;

    // private static Object monitor = new Object();
    // private static Object[] noArguments = new Object[]{};

    public PostMacroFilter()
    {
        // optimized by Jeffrey E.F. Friedl
        super("\\{([^:}]+)(?::([^\\}]*))?\\}(.*?)\\{\\1\\}", SINGLELINE);
        addRegex("\\{([^:}]+)(?::([^\\}]*))?\\}", "", MULTILINE);
    }

    public void setInitialContext(InitialRenderContext context)
    {
        macros = (PostMacroRepository) RSContext.appContext.getBean("postMacroRepository");// MacroRepository.getInstance();
        macros.setInitialContext(context);
    }

    protected Repository getMacroRepository()
    {
        return macros;
    }

    public void handleMatch(StringBuffer buffer, MatchResult result, FilterContext context)
    {
        String command = result.group(1);

        if (command != null)
        {
            // {$peng} are variables not macros.
            if (!command.startsWith("$"))
            {
                MacroParameter mParams = context.getMacroParameter();
                // System.err.println("count="+result.groups());
                // System.err.println("1: "+result.group(1));
                // System.err.println("2: "+result.group(2));
                switch (result.groups())
                {
                    case 3:
                        mParams.setContent(result.group(3));
                        mParams.setContentStart(result.beginOffset(3));
                        mParams.setContentEnd(result.endOffset(3));
                    case 2:
                        mParams.setParams(result.group(2));
                        // Still left from ORO
                        // case 2: System.out.println(result.group(1));
                        // case 1: System.out.println(result.group(0));
                }
                mParams.setStart(result.beginOffset(0));
                mParams.setEnd(result.endOffset(0));

                // @DANGER: recursive calls may replace macros in included
                // source code
                try
                {
                    if (getMacroRepository().containsKey(command))
                    {
                        Macro macro = (Macro) getMacroRepository().get(command);
                        // recursively filter macros within macros
                        if (null != mParams.getContent())
                        {
                            mParams.setContent(filter(mParams.getContent(), context));
                        }
                        Writer writer = new StringBufferWriter(buffer);
                        macro.execute(writer, mParams);
                    }
                    else if (command.startsWith("!"))
                    {
                        // @TODO including of other snips
                        RenderEngine engine = context.getRenderContext().getRenderEngine();
                        if (engine instanceof IncludeRenderEngine)
                        {
                            String include = ((IncludeRenderEngine) engine).include(command.substring(1));
                            if (null != include)
                            {
                                // Filter paramFilter = new
                                // ParamFilter(mParams);
                                // included = paramFilter.filter(included,
                                // null);
                                buffer.append(include);
                            }
                            else
                            {
                                buffer.append(command.substring(1) + " not found.");
                            }
                        }
                        return;
                    }
                    else
                    {
                        buffer.append(result.group(0));
                        return;
                    }
                }
                catch (IllegalArgumentException e)
                {
                    buffer.append("<div class=\"error\">" + command + ": " + e.getMessage() + "</div>");
                }
                catch (Throwable e)
                {
                    Log.log.warn("MacroFilter: unable to format macro: " + result.group(1), e);
                    buffer.append("<div class=\"error\">" + command + ": " + e.getMessage() + "</div>");
                    return;
                }
            }
            else
            {
                buffer.append("<");
                buffer.append(command.substring(1));
                buffer.append(">");
            }
        }
        else
        {
            buffer.append(result.group(0));
        }
    }
}
