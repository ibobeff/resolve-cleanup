/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rights.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.resolve.dto.RightType;
import com.resolve.persistence.dao.AccessRightsDAO;
import com.resolve.persistence.dao.ResolveAssessDAO;
import com.resolve.persistence.dao.ResolveParserDAO;
import com.resolve.persistence.dao.ResolvePreprocessDAO;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.vo.WikiUser;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.rights.ActionTaskRightService;
import com.resolve.wiki.rights.ResolveAssessRightService;
import com.resolve.wiki.rights.ResolveParserRightService;
import com.resolve.wiki.rights.ResolvePreprocessRightService;
import com.resolve.wiki.store.helper.StoreUtility;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * 
 * @author jeet.marwah
 *
 */
public class ActionTaskRightServiceImpl extends RightService 
    implements ActionTaskRightService, ResolveAssessRightService, ResolvePreprocessRightService, ResolveParserRightService
{
    
    private static ActionTaskRightServiceImpl instance;

    //only 1 instance of this should be available
    private ActionTaskRightServiceImpl()
    {
    }

    public static ActionTaskRightServiceImpl getInstance()
    {
        if (instance == null)
        {
            synchronized (ActionTaskRightServiceImpl.class)
            {
                if (instance == null)
                {
                    instance = new ActionTaskRightServiceImpl();
                }
            }
        }
        return instance;
    }
    
    private boolean checkAccess(WikiAction wikiAction, ResolveActionTask resolveActionTask, WikiUser wikiUser) throws WikiException
    {
        List<String> listATRoles = null;
        //get the right mapped to the action
        RightType rightType = forAction(wikiAction);
        
        //get the role for the user
        boolean hasAccess = checkSuperUserAccess(wikiUser.getUsername(), rightType);

        //'resolve' namespace can be EDITED only by resolve.maint. ALL USERS should have READ and EXECUTE rights for this namespace
        if(!hasAccess)
        {
            if(resolveActionTask.getUNamespace().equalsIgnoreCase("resolve") && rightType.equals(RightType.view))
            {
                hasAccess = true;
            }
        }

        if(!hasAccess)
        {
            String actionTaskRoles = Wiki.getInstance().getWikiStore().getAccessRightsFor(ResolveActionTask.RESOURCE_TYPE, resolveActionTask.getUFullName(), resolveActionTask.getSys_id(), rightType);
            listATRoles = StringUtils.convertStringToList(actionTaskRoles, ",");
            if(listATRoles.size() == 0)//means this is a new doc and will be created by the user. So ALL users have this access to create it
            {
                hasAccess = true;
            }
        }
        
        if(!hasAccess)
        {
          //get the SET of ROLES that this user belongs to
            Set<String> userRoles = wikiUser.getRoles();

            outsideLoop:
            for(String ATRole : listATRoles)
            {
                for(String userRole : userRoles)
                {
                    if(ATRole.trim().equalsIgnoreCase(userRole.trim()))
                    {
                        hasAccess = true;
                        break outsideLoop;
                    }
                }
            }
        }
        
        return hasAccess;
    }

    @Override
    public boolean checkAccess(WikiAction wikiAction, String actionTaskFullName, String username) throws WikiException
    {
        if(actionTaskFullName == null || actionTaskFullName.trim().length() == 0 || actionTaskFullName.indexOf('#') == -1)
        {
            return false;
        }
        
        String[] args = actionTaskFullName.split("\\#");
        String name = args[0];
        String namespace = args[1];
    
        ResolveActionTask resolveActionTask = new ResolveActionTask();
        resolveActionTask.setUName(name);
        resolveActionTask.setUNamespace(namespace);
        resolveActionTask.setUFullName(actionTaskFullName);
        
        WikiUser wikiUser = ServiceHibernate.getUserRolesAndGroups(username);//Wiki.getInstance().getWikiStore().getUserRoles(username);
        
        return checkAccess(wikiAction, resolveActionTask, wikiUser);

    }//checkAccess

    @Override
    public boolean checkAccess(WikiAction wikiAction, ResolveActionTask resolveActionTask, WikiRequestContext context) throws WikiException
    {
        //populateUserRoles
        populateUserRoles(context);
        
        return checkAccess(wikiAction, resolveActionTask, context.getWikiUser());

    }//checkAccess
    
    
    @Override
    public boolean checkAccess(WikiAction wikiAction, String actionTaskFullName, WikiRequestContext context) throws WikiException
    {
        if(actionTaskFullName == null || actionTaskFullName.trim().length() == 0 || actionTaskFullName.indexOf('#') == -1)
        {
            return false;
        }
        
        String[] args = actionTaskFullName.split("\\#");
        String name = args[0];
        String namespace = args[1];
    
        ResolveActionTask resolveActionTask = new ResolveActionTask();
        resolveActionTask.setUName(name);
        resolveActionTask.setUNamespace(namespace);
        resolveActionTask.setUFullName(actionTaskFullName);
           
        return checkAccess(wikiAction, resolveActionTask, context);
    }//checkAccess

    @Override
    public boolean checkAccess(String actionTaskFullName, WikiRequestContext context) throws WikiException
    {
        if(actionTaskFullName == null || actionTaskFullName.trim().length() == 0 || actionTaskFullName.indexOf('#') == -1)
        {
            return false;
        }
        WikiAction wikiAction = (WikiAction)context.getAttribute(ConstantValues.WIKI_ACTION_KEY);
        return checkAccess(wikiAction, actionTaskFullName, context);
    }//checkAccess

    @Override
    public AccessRights getAccessRightsFor(String username, List<PropertiesVO> lProps, ResolveActionTask resolveActionTask) throws WikiException
    {
        AccessRights ar = getAccessRightsFor(   resolveActionTask.getUNamespace(), 
                                                resolveActionTask.getUFullName(), 
                                                resolveActionTask.getSys_id(), 
                                                resolveActionTask.ugetUIsDefaultRole() !=null ? resolveActionTask.ugetUIsDefaultRole() : false, 
                                                ResolveActionTask.RESOURCE_TYPE, 
                                                resolveActionTask.getUReadRoles() , 
                                                resolveActionTask.getUWriteRoles(), 
                                                resolveActionTask.getUAdminRoles(),
                                                resolveActionTask.getUExecuteRoles(), 
                                                lProps);
        
        ar.setSysCreatedBy(username);
        ar.setSysUpdatedBy(username);
        
        return ar;
    }
    
    @Override
    public AccessRights getAccessRightsFor(String username, ResolveAssess resolveAssess, ResolveActionTask resolveActionTask) throws WikiException
    {
        AccessRights ar = null;
        if(StringUtils.isEmpty(resolveAssess.getSys_id()))
        {
            //this is an insert
            ar = getAccessRightsFor(null, 
                            resolveAssess.getUName(), 
                            resolveAssess.getSys_id(), //this will be null, so populate it later on
                            false, 
                            ResolveAssess.RESOURCE_TYPE,  
                            resolveActionTask.getUReadRoles() , 
                            resolveActionTask.getUWriteRoles(), 
                            resolveActionTask.getUAdminRoles(),
                            resolveActionTask.getUExecuteRoles(), 
                            null);

            ar.setSysCreatedBy(username);
            ar.setSysUpdatedBy(username);
            
        }
        else
        {
            //this is an update of assessor
            Set<String> readAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUReadRoles(), ","));
            Set<String> writeAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUWriteRoles(), ","));
            Set<String> adminAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUAdminRoles(), ","));
            Set<String> executeAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUExecuteRoles(), ","));

            
            StringBuffer readAccessStr = new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK); 
            StringBuffer writeAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer adminAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer executeAccessStr =  new StringBuffer(Constants.DEFAULT_EXECUTE_RIGHT_FOR_ACTIONTASK);
            
            //get the list of all the actiontask and then their roles
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		ResolveAssess dbCopy = null; 
            		ResolveAssessDAO daoResolveAssess = HibernateUtil.getDAOFactory().getResolveAssessDAO();
                    dbCopy = daoResolveAssess.findById(resolveAssess.getSys_id());
                    
//                    if(dbCopy.getAccessRights() != null)
//                    {
//                        ar = dbCopy.getAccessRights();
//                    }
                    
                    Collection<ResolveActionInvoc> raiList = dbCopy.getResolveActionInvocs();
                    if(raiList != null && raiList.size() > 0)
                    {
                        for(ResolveActionInvoc rai : raiList)
                        {
                            ResolveActionTask rat = StoreUtility.getResolveActionTaskForInvoc(rai.getSys_id());
                            //gather the rights of all the other ActionTask other then the one that is edited
                            if(rat != null && !rat.getSys_id().equals(resolveActionTask.getSys_id()))
                            {
                                AccessRights ar1 = rat.getAccessRights();
                                if(ar1 != null)
                                {
                                    readAccessStr.append(",").append(ar1.getUReadAccess());
                                    writeAccessStr.append(",").append(ar1.getUWriteAccess());
                                    adminAccessStr.append(",").append(ar1.getUAdminAccess());
                                    executeAccessStr.append(",").append(ar1.getUExecuteAccess());
                                    
                                }
                                else
                                {
                                    readAccessStr.append(",").append(rat.getURoles());
                                    writeAccessStr.append(",").append(rat.getURoles());
                                    adminAccessStr.append(",").append(rat.getURoles());
                                    executeAccessStr.append(",").append(rat.getURoles());
                                }
                            }
                        }
                    }
                    
            	});
            }
            catch(Throwable t)
            {
                Log.log.error("Error getting accessrights for assessor:" + resolveAssess.getUName(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
            
            readAccess.addAll(StringUtils.convertStringToList(readAccessStr.toString(), ","));
            writeAccess.addAll(StringUtils.convertStringToList(writeAccessStr.toString(), ","));
            adminAccess.addAll(StringUtils.convertStringToList(adminAccessStr.toString(), ","));
            executeAccess.addAll(StringUtils.convertStringToList(executeAccessStr.toString(), ","));

            //populate accessrights
            if(ar == null)
            {
                ar = new AccessRights();
            }
            ar.setUResourceType(ResolveAssess.RESOURCE_TYPE);
            ar.setUResourceName(resolveAssess.getUName());
            ar.setUResourceId(resolveAssess.getSys_id());
            ar.setSysCreatedOn(GMTDate.getDate());
            ar.setSysUpdatedOn(GMTDate.getDate());
            ar.setUReadAccess(StringUtils.convertCollectionToString(readAccess.iterator(), ","));
            ar.setUWriteAccess(StringUtils.convertCollectionToString(writeAccess.iterator(), ","));
            ar.setUAdminAccess(StringUtils.convertCollectionToString(adminAccess.iterator(), ","));
            ar.setUExecuteAccess(StringUtils.convertCollectionToString(executeAccess.iterator(), ","));
            ar.setSysCreatedBy(username);
            ar.setSysUpdatedBy(username);
        }
        
        return ar;
    }//getAccessRightsFor

    @Override
    public AccessRights getAccessRightsFor(String username, ResolveParser resolveParser, ResolveActionTask resolveActionTask) throws WikiException
    {
        AccessRights ar = null;
        if(StringUtils.isEmpty(resolveParser.getSys_id()))
        {
            //this is an insert
            ar = getAccessRightsFor(null, 
                            resolveParser.getUName(), 
                            resolveParser.getSys_id(), //this will be null, so populate it later on
                            false, 
                            ResolveParser.RESOURCE_TYPE,    
                            resolveActionTask.getUReadRoles() , 
                            resolveActionTask.getUWriteRoles(), 
                            resolveActionTask.getUAdminRoles(),
                            resolveActionTask.getUExecuteRoles(), 
                            null);

            ar.setSysCreatedBy(username);
            ar.setSysUpdatedBy(username);
            
        }
        else
        {
            //this is an update of parser
            Set<String> readAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUReadRoles(), ","));
            Set<String> writeAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUWriteRoles(), ","));
            Set<String> adminAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUAdminRoles(), ","));
            Set<String> executeAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUExecuteRoles(), ","));

            
            StringBuffer readAccessStr = new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer writeAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer adminAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer executeAccessStr =  new StringBuffer(Constants.DEFAULT_EXECUTE_RIGHT_FOR_ACTIONTASK);
            
          //get the list of all the actiontask and then their roles
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		ResolveParser dbCopy = null;
                    ResolveParserDAO daoResolveParser = HibernateUtil.getDAOFactory().getResolveParserDAO();
                    dbCopy = daoResolveParser.findById(resolveParser.getSys_id());
                    
//                    if(dbCopy.getAccessRights() != null)
//                    {
//                        ar = dbCopy.getAccessRights();
//                    }
                    
                    Collection<ResolveActionInvoc> raiList = dbCopy.getResolveActionInvocs();
                    if(raiList != null && raiList.size() > 0)
                    {
                        for(ResolveActionInvoc rai : raiList)
                        {
                            ResolveActionTask rat = StoreUtility.getResolveActionTaskForInvoc(rai.getSys_id());
                            //gather the rights of all the other ActionTask other then the one that is edited
                            if(rat != null && !rat.getSys_id().equals(resolveActionTask.getSys_id()))
                            {
                                AccessRights ar1 = rat.getAccessRights();
                                if(ar1 != null)
                                {
                                    readAccessStr.append(",").append(ar1.getUReadAccess());
                                    writeAccessStr.append(",").append(ar1.getUWriteAccess());
                                    adminAccessStr.append(",").append(ar1.getUAdminAccess());
                                    executeAccessStr.append(",").append(ar1.getUExecuteAccess());
                                }
                                else
                                {
                                    readAccessStr.append(",").append(rat.getURoles());
                                    writeAccessStr.append(",").append(rat.getURoles());
                                    adminAccessStr.append(",").append(rat.getURoles());
                                    executeAccessStr.append(",").append(rat.getURoles());
                                }
                            }
                        }
                    }
                    
            	});
            }
            catch(Throwable t)
            {
                Log.log.error("Error getting accessrights for ResolveParser:" + resolveParser.getUName(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
            
            readAccess.addAll(StringUtils.convertStringToList(readAccessStr.toString(), ","));
            writeAccess.addAll(StringUtils.convertStringToList(writeAccessStr.toString(), ","));
            adminAccess.addAll(StringUtils.convertStringToList(adminAccessStr.toString(), ","));
            executeAccess.addAll(StringUtils.convertStringToList(executeAccessStr.toString(), ","));

            //populate accessrights
            if(ar == null)
            {
                ar = new AccessRights();
            }
            ar.setUResourceType(ResolveParser.RESOURCE_TYPE);
            ar.setUResourceName(resolveParser.getUName());
            ar.setUResourceId(resolveParser.getSys_id());
            ar.setSysCreatedOn(GMTDate.getDate());
            ar.setSysUpdatedOn(GMTDate.getDate());
            ar.setUReadAccess(StringUtils.convertCollectionToString(readAccess.iterator(), ","));
            ar.setUWriteAccess(StringUtils.convertCollectionToString(writeAccess.iterator(), ","));
            ar.setUAdminAccess(StringUtils.convertCollectionToString(adminAccess.iterator(), ","));
            ar.setUExecuteAccess(StringUtils.convertCollectionToString(executeAccess.iterator(), ","));
            ar.setSysCreatedBy(username);
            ar.setSysUpdatedBy(username);
            
        }
        
        return ar;
    }

    @Override
    public AccessRights getAccessRightsFor(String username, ResolvePreprocess resolvePreprocess, ResolveActionTask resolveActionTask) throws WikiException
    {
        AccessRights ar = null;
        if(StringUtils.isEmpty(resolvePreprocess.getSys_id()))
        {
            //this is an insert
            ar = getAccessRightsFor(null, 
                            resolvePreprocess.getUName(), 
                            resolvePreprocess.getSys_id(), //this will be null, so populate it later on
                            false, 
                            ResolvePreprocess.RESOURCE_TYPE,   
                            resolveActionTask.getUReadRoles() , 
                            resolveActionTask.getUWriteRoles(), 
                            resolveActionTask.getUAdminRoles(),
                            resolveActionTask.getUExecuteRoles(), 
                            null);

            ar.setSysCreatedBy(username);
            ar.setSysUpdatedBy(username);
            
        }
        else
        {
            //this is an update of preprocessor
            Set<String> readAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUReadRoles(), ","));
            Set<String> writeAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUWriteRoles(), ","));
            Set<String> adminAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUAdminRoles(), ","));
            Set<String> executeAccess = new TreeSet<String>(StringUtils.convertStringToList(resolveActionTask.getUExecuteRoles(), ","));

            StringBuffer readAccessStr = new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer writeAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer adminAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
            StringBuffer executeAccessStr =  new StringBuffer(Constants.DEFAULT_EXECUTE_RIGHT_FOR_ACTIONTASK);
            
            
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		ResolvePreprocess dbCopy = null;
            	    ResolvePreprocessDAO daoResolvePreprocess = HibernateUtil.getDAOFactory().getResolvePreprocessDAO();
                    dbCopy = daoResolvePreprocess.findById(resolvePreprocess.getSys_id());
                    
//                    if(dbCopy.getAccessRights() != null)
//                    {
//                        ar = dbCopy.getAccessRights();
//                    }
                    
                    Collection<ResolveActionInvoc> raiList = dbCopy.getResolveActionInvocs();
                    if(raiList != null && raiList.size() > 0)
                    {
                        for(ResolveActionInvoc rai : raiList)
                        {
                            ResolveActionTask rat = StoreUtility.getResolveActionTaskForInvoc(rai.getSys_id());
                            //gather the rights of all the other ActionTask other then the one that is edited
                            if(rat != null && !rat.getSys_id().equals(resolveActionTask.getSys_id()))
                            {
                                AccessRights ar1 = rat.getAccessRights();
                                if(ar1 != null)
                                {
                                    readAccessStr.append(",").append(ar1.getUReadAccess());
                                    writeAccessStr.append(",").append(ar1.getUWriteAccess());
                                    adminAccessStr.append(",").append(ar1.getUAdminAccess());
                                    executeAccessStr.append(",").append(ar1.getUExecuteAccess());
                                    
                                }
                                else
                                {
                                    readAccessStr.append(",").append(rat.getURoles());
                                    writeAccessStr.append(",").append(rat.getURoles());
                                    adminAccessStr.append(",").append(rat.getURoles());
                                    executeAccessStr.append(",").append(rat.getURoles());
                                }
                            }
                        }
                    }
                    
                    
            	});
            }
            catch(Throwable t)
            {
                Log.log.error("Error getting accessrights for ResolvePreprocess:" + resolvePreprocess.getUName(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
            
            readAccess.addAll(StringUtils.convertStringToList(readAccessStr.toString(), ","));
            writeAccess.addAll(StringUtils.convertStringToList(writeAccessStr.toString(), ","));
            adminAccess.addAll(StringUtils.convertStringToList(adminAccessStr.toString(), ","));
            executeAccess.addAll(StringUtils.convertStringToList(executeAccessStr.toString(), ","));

            //populate accessrights
            if(ar == null)
            {
                ar = new AccessRights();
            }
            ar.setUResourceType(ResolvePreprocess.RESOURCE_TYPE);
            ar.setUResourceName(resolvePreprocess.getUName());
            ar.setUResourceId(resolvePreprocess.getSys_id());
            ar.setSysCreatedOn(GMTDate.getDate());
            ar.setSysUpdatedOn(GMTDate.getDate());
            ar.setUReadAccess(StringUtils.convertCollectionToString(readAccess.iterator(), ","));
            ar.setUWriteAccess(StringUtils.convertCollectionToString(writeAccess.iterator(), ","));
            ar.setUAdminAccess(StringUtils.convertCollectionToString(adminAccess.iterator(), ","));
            ar.setUExecuteAccess(StringUtils.convertCollectionToString(executeAccess.iterator(), ","));
            ar.setSysCreatedBy(username);
            ar.setSysUpdatedBy(username);
        }
        
        return ar;
    }

    @Override
    public AccessRights updateAccessRightsFor(String username, String id, String simpleName) throws WikiException
    {
        AccessRights ar = null;
        
        if(simpleName.equalsIgnoreCase("ResolvePreprocess"))
        {
            ar = updateAccessRightsForResolvePreprocess(username, id);
        }
        else if(simpleName.equalsIgnoreCase("ResolveAssess"))
        {
            ar = updateAccessRightsForResolveAssess(username, id);
        }
        else if(simpleName.equalsIgnoreCase("ResolveParser"))
        {
            ar = updateAccessRightsForResolveParser(username, id);
        }
        
        return ar;
    }//updateAccessRightsFor
    
    private AccessRights updateAccessRightsForResolveAssess(String username, String id)
    {
        AccessRights ar = null;
        
        //this is an update of assessor
        Set<String> readAccess = new TreeSet<String>();
        Set<String> writeAccess = new TreeSet<String>();
        Set<String> adminAccess = new TreeSet<String>();
        Set<String> executeAccess = new TreeSet<String>();

        
        StringBuffer readAccessStr = new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer writeAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer adminAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer executeAccessStr =  new StringBuffer(Constants.DEFAULT_EXECUTE_RIGHT_FOR_ACTIONTASK);
        
      //get the list of all the actiontask and then their roles
        try {
              HibernateProxy.setCurrentUser(username);
			ar = (AccessRights) HibernateProxy.execute(() -> {
				AccessRights accessRights = null;
				ResolveAssess dbCopy = null;
			    try
			    {
			        ResolveAssessDAO daoResolveAssess = HibernateUtil.getDAOFactory().getResolveAssessDAO();
			        AccessRightsDAO daoAccessRights = HibernateUtil.getDAOFactory().getAccessRightsDAO();
			        
			        dbCopy = daoResolveAssess.findById(id);
			        
//            if(dbCopy.getAccessRights() != null)
//            {
//                ar = dbCopy.getAccessRights();
//            }
//            else
			        {
			            accessRights = new AccessRights();
			        }
			        
			        Collection<ResolveActionInvoc> raiList = dbCopy.getResolveActionInvocs();
			        if(raiList != null && raiList.size() > 0)
			        {
			            for(ResolveActionInvoc rai : raiList)
			            {
			                ResolveActionTask rat = StoreUtility.getResolveActionTaskForInvoc(rai.getSys_id());
			                //gather the rights of all the ActionTask that are referenced
			                if(rat != null)
			                {
			                    AccessRights ar1 = rat.getAccessRights();
			                    if(ar1 != null)
			                    {
			                        readAccessStr.append(",").append(ar1.getUReadAccess());
			                        writeAccessStr.append(",").append(ar1.getUWriteAccess());
			                        adminAccessStr.append(",").append(ar1.getUAdminAccess());
			                        executeAccessStr.append(",").append(ar1.getUExecuteAccess());
			                        
			                    }
			                    else
			                    {
			                        readAccessStr.append(",").append(rat.getURoles());
			                        writeAccessStr.append(",").append(rat.getURoles());
			                        adminAccessStr.append(",").append(rat.getURoles());
			                        executeAccessStr.append(",").append(rat.getURoles());
			                    }
			                }
			            }
			        }
			        
			        readAccess.addAll(StringUtils.convertStringToList(readAccessStr.toString(), ","));
			        writeAccess.addAll(StringUtils.convertStringToList(writeAccessStr.toString(), ","));
			        adminAccess.addAll(StringUtils.convertStringToList(adminAccessStr.toString(), ","));
			        executeAccess.addAll(StringUtils.convertStringToList(executeAccessStr.toString(), ","));
			        
			        accessRights.setUResourceType(ResolveAssess.RESOURCE_TYPE);
			        accessRights.setUResourceName(dbCopy.getUName());
			        accessRights.setUResourceId(dbCopy.getSys_id());
			        accessRights.setSysCreatedOn(GMTDate.getDate());
			        accessRights.setSysUpdatedOn(GMTDate.getDate());
			        accessRights.setUReadAccess(StringUtils.convertCollectionToString(readAccess.iterator(), ","));
			        accessRights.setUWriteAccess(StringUtils.convertCollectionToString(writeAccess.iterator(), ","));
			        accessRights.setUAdminAccess(StringUtils.convertCollectionToString(adminAccess.iterator(), ","));
			        accessRights.setUExecuteAccess(StringUtils.convertCollectionToString(executeAccess.iterator(), ","));
			        accessRights.setSysCreatedBy(username);
			        accessRights.setSysUpdatedBy(username);
			        
//            dbCopy.setAccessRights(ar);

			        daoAccessRights.persist(accessRights);
			        daoResolveAssess.persist(dbCopy);
			        
			                
			    }
			    catch(Throwable t)
			    {
			        Log.log.error("Error in updateAccessRightsForResolveAssess:" + dbCopy.getUName(), t);			        
			    }
			    return accessRights;
			});
		} catch (Exception e) {
			Log.log.warn(e.getMessage(), e);  
                             HibernateUtil.rethrowNestedTransaction(e);
		}
        
        return ar;
    }//updateAccessRightsForAssessor

    private AccessRights updateAccessRightsForResolvePreprocess(String username, String id)
    {
        AccessRights ar = null;
        
        //this is an update of assessor
        Set<String> readAccess = new TreeSet<String>();
        Set<String> writeAccess = new TreeSet<String>();
        Set<String> adminAccess = new TreeSet<String>();
        Set<String> executeAccess = new TreeSet<String>();

        
        StringBuffer readAccessStr = new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer writeAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer adminAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer executeAccessStr =  new StringBuffer(Constants.DEFAULT_EXECUTE_RIGHT_FOR_ACTIONTASK);
        
      //get the list of all the actiontask and then their roles
        try {
              HibernateProxy.setCurrentUser(username);
        	ar = (AccessRights) HibernateProxy.execute(() -> {
        		AccessRights accessResult = null;
        		ResolvePreprocess dbCopy = null;
			    try
			    {
			        ResolvePreprocessDAO dao = HibernateUtil.getDAOFactory().getResolvePreprocessDAO();
			        AccessRightsDAO daoAccessRights = HibernateUtil.getDAOFactory().getAccessRightsDAO();
			        
			        dbCopy = dao.findById(id);
			        
//            if(dbCopy.getAccessRights() != null)
//            {
//                ar = dbCopy.getAccessRights();
//            }
//            else
			        {
			            accessResult = new AccessRights();
			        }
			        
			        Collection<ResolveActionInvoc> raiList = dbCopy.getResolveActionInvocs();
			        if(raiList != null && raiList.size() > 0)
			        {
			            for(ResolveActionInvoc rai : raiList)
			            {
			                ResolveActionTask rat = StoreUtility.getResolveActionTaskForInvoc(rai.getSys_id());
			                //gather the rights of all the ActionTask that are referenced
			                if(rat != null)
			                {
			                    AccessRights ar1 = rat.getAccessRights();
			                    if(ar1 != null)
			                    {
			                        readAccessStr.append(",").append(ar1.getUReadAccess());
			                        writeAccessStr.append(",").append(ar1.getUWriteAccess());
			                        adminAccessStr.append(",").append(ar1.getUAdminAccess());
			                        executeAccessStr.append(",").append(ar1.getUExecuteAccess());
			                        
			                    }
			                    else
			                    {
			                        readAccessStr.append(",").append(rat.getURoles());
			                        writeAccessStr.append(",").append(rat.getURoles());
			                        adminAccessStr.append(",").append(rat.getURoles());
			                        executeAccessStr.append(",").append(rat.getURoles());
			                    }
			                }
			            }
			        }
			        
			        readAccess.addAll(StringUtils.convertStringToList(readAccessStr.toString(), ","));
			        writeAccess.addAll(StringUtils.convertStringToList(writeAccessStr.toString(), ","));
			        adminAccess.addAll(StringUtils.convertStringToList(adminAccessStr.toString(), ","));
			        executeAccess.addAll(StringUtils.convertStringToList(executeAccessStr.toString(), ","));
			        
			        accessResult.setUResourceType(ResolveAssess.RESOURCE_TYPE);
			        accessResult.setUResourceName(dbCopy.getUName());
			        accessResult.setUResourceId(dbCopy.getSys_id());
			        accessResult.setSysCreatedOn(GMTDate.getDate());
			        accessResult.setSysUpdatedOn(GMTDate.getDate());
			        accessResult.setUReadAccess(StringUtils.convertCollectionToString(readAccess.iterator(), ","));
			        accessResult.setUWriteAccess(StringUtils.convertCollectionToString(writeAccess.iterator(), ","));
			        accessResult.setUAdminAccess(StringUtils.convertCollectionToString(adminAccess.iterator(), ","));
			        accessResult.setUExecuteAccess(StringUtils.convertCollectionToString(executeAccess.iterator(), ","));
			        accessResult.setSysCreatedBy(username);
			        accessResult.setSysUpdatedBy(username);
			        
//            dbCopy.setAccessRights(ar);

			        daoAccessRights.persist(accessResult);
			        dao.persist(dbCopy);			        
			          
			    }
			    catch(Throwable t)
			    {
			        Log.log.error("Error in updateAccessRightsForResolvePreprocess:" + dbCopy.getUName(), t);
			       
			    }
			    
			    return accessResult; 
			});
		} catch (Exception e) {
			Log.log.warn(e.getMessage(), e);     
                             HibernateUtil.rethrowNestedTransaction(e);
		}
        return ar;
    }//updateAccessRightsForResolvePreprocess
    
    private AccessRights updateAccessRightsForResolveParser(String username, String id)
    {
        AccessRights ar = null;
        
        //this is an update of assessor
        Set<String> readAccess = new TreeSet<String>();
        Set<String> writeAccess = new TreeSet<String>();
        Set<String> adminAccess = new TreeSet<String>();
        Set<String> executeAccess = new TreeSet<String>();

        
        StringBuffer readAccessStr = new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer writeAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer adminAccessStr =  new StringBuffer(Constants.DEFAULT_RIGHT_FOR_ACTIONTASK);
        StringBuffer executeAccessStr =  new StringBuffer(Constants.DEFAULT_EXECUTE_RIGHT_FOR_ACTIONTASK);
        
      //get the list of all the actiontask and then their roles
        try {
              HibernateProxy.setCurrentUser(username);
			ar = (AccessRights) HibernateProxy.execute(() -> {
				AccessRights accessRights = null;
				ResolveParser dbCopy = null;
			    try
			    {
			        ResolveParserDAO dao = HibernateUtil.getDAOFactory().getResolveParserDAO();
			        AccessRightsDAO daoAccessRights = HibernateUtil.getDAOFactory().getAccessRightsDAO();
			        
			        dbCopy = dao.findById(id);
			        
//            if(dbCopy.getAccessRights() != null)
//            {
//                ar = dbCopy.getAccessRights();
//            }
//            else
			        {
			            accessRights = new AccessRights();
			        }
			        
			        Collection<ResolveActionInvoc> raiList = dbCopy.getResolveActionInvocs();
			        if(raiList != null && raiList.size() > 0)
			        {
			            for(ResolveActionInvoc rai : raiList)
			            {
			                ResolveActionTask rat = StoreUtility.getResolveActionTaskForInvoc(rai.getSys_id());
			                //gather the rights of all the ActionTask that are referenced
			                if(rat != null)
			                {
			                    AccessRights ar1 = rat.getAccessRights();
			                    if(ar1 != null)
			                    {
			                        readAccessStr.append(",").append(ar1.getUReadAccess());
			                        writeAccessStr.append(",").append(ar1.getUWriteAccess());
			                        adminAccessStr.append(",").append(ar1.getUAdminAccess());
			                        executeAccessStr.append(",").append(ar1.getUExecuteAccess());
			                        
			                    }
			                    else
			                    {
			                        readAccessStr.append(",").append(rat.getURoles());
			                        writeAccessStr.append(",").append(rat.getURoles());
			                        adminAccessStr.append(",").append(rat.getURoles());
			                        executeAccessStr.append(",").append(rat.getURoles());
			                    }
			                }
			            }
			        }
			        
			        readAccess.addAll(StringUtils.convertStringToList(readAccessStr.toString(), ","));
			        writeAccess.addAll(StringUtils.convertStringToList(writeAccessStr.toString(), ","));
			        adminAccess.addAll(StringUtils.convertStringToList(adminAccessStr.toString(), ","));
			        executeAccess.addAll(StringUtils.convertStringToList(executeAccessStr.toString(), ","));
			        
			        accessRights.setUResourceType(ResolveAssess.RESOURCE_TYPE);
			        accessRights.setUResourceName(dbCopy.getUName());
			        accessRights.setUResourceId(dbCopy.getSys_id());
			        accessRights.setSysCreatedOn(GMTDate.getDate());
			        accessRights.setSysUpdatedOn(GMTDate.getDate());
			        accessRights.setUReadAccess(StringUtils.convertCollectionToString(readAccess.iterator(), ","));
			        accessRights.setUWriteAccess(StringUtils.convertCollectionToString(writeAccess.iterator(), ","));
			        accessRights.setUAdminAccess(StringUtils.convertCollectionToString(adminAccess.iterator(), ","));
			        accessRights.setUExecuteAccess(StringUtils.convertCollectionToString(executeAccess.iterator(), ","));
			        accessRights.setSysCreatedBy(username);
			        accessRights.setSysUpdatedBy(username);
			        
//            dbCopy.setAccessRights(ar);

			        daoAccessRights.persist(accessRights);
			        dao.persist(dbCopy);                
			    }
			    catch(Throwable t)
			    {
			        Log.log.error("Error in updateAccessRightsForResolveParser:" + dbCopy.getUName(), t);			        
			    }
			    return accessRights;
			});
		} catch (Exception e) {
			Log.log.warn(e.getMessage(), e);  
                             HibernateUtil.rethrowNestedTransaction(e);
		}
        
        return ar;
    }//updateAccessRightsForResolvePreprocess
    
}//ActionTaskRightServiceImpl
