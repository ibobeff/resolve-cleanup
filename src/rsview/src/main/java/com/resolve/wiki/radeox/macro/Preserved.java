/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.wiki.radeox.util.Encoder;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * A specialized macro that allows to preserve certain special characters
 * by creating character entities. The subclassing macro may decide whether
 * to call replace() before or after executing the actual macro substitution.
 *
 */

public abstract class Preserved extends BaseMacro
{
    private Map special = new HashMap();
    private String specialString = "";

    /**
     * Encode special character c by replacing with it's hex character entity code.
     */
    protected void addSpecial(char c)
    {
        addSpecial("" + c, Encoder.toEntity(c));
    }

    /**
     * Add a replacement for the special character c which may be a string
     *
     * @param c the character to replace
     * @param replacement the new string
     */
    protected void addSpecial(String c, String replacement)
    {
        specialString += c;
        special.put(c, replacement);
    }

    /**
     * Actually replace specials in source.
     * This method can be used by subclassing macros.
     *
     * @param source String to encode
     *
     * @return encoded Encoded string
     */
    protected String replace(String source)
    {
        StringBuffer tmp = new StringBuffer();
        StringTokenizer stringTokenizer = new StringTokenizer(source, specialString, true);
        while (stringTokenizer.hasMoreTokens())
        {
            String current = stringTokenizer.nextToken();
            if (special.containsKey(current))
            {
                current = (String) special.get(current);
            }
            tmp.append(current);
        }
        return tmp.toString();
    }
}
