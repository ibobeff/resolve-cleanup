/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import com.resolve.wiki.radeox.filter.regex.LocaleRegexReplaceFilter;

/*
 * StrikeThrough finds --text-- in its input and transforms this to striked through text.
 */

public class StrikeThroughFilter extends LocaleRegexReplaceFilter implements CacheFilter
{

    public static final String LOCAL_KEY = "filter.strikethrough";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "([^!-]|^)--(?!>)(?=.*--)((?:(?:[^-]+)-?)+)--([^-]|$)");
        addKeyValue(LOCAL_KEY + ".print", "$1<strike class=\"strike\">$2</strike>$3");
    }

    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }
}
