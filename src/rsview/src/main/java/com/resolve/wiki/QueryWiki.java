/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestParam;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.WikiArchiveVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.DateManipulator;
import com.resolve.services.vo.FeedbackDTO;
import com.resolve.wiki.dto.HistoryDTO;
import com.resolve.wiki.dto.ViewWikiRevisionDTO;

public class QueryWiki
{
    
    public List<HistoryDTO> getAllWikiRevisionsFor(String docName, String username) throws Exception
    {
        List<HistoryDTO> result = new ArrayList<HistoryDTO>();
        List<WikiArchiveVO> archives = ServiceWiki.getAllWikiRevisionsFor(docName, username);
        
        if(archives != null && archives.size() > 0)
        {
            Set<Integer> revs = new HashSet<Integer>();
            for(WikiArchiveVO archive : archives)
            {
                if(!revs.contains(archive.getUVersion()))
                {
                    result.add(buildHistory(docName, archive));
                    revs.add(archive.getUVersion());
                }
            }
        }
        
        return result;
    }
    
    public ViewWikiRevisionDTO getSpecificWikiRevisionsFor(String docSysId, String fullDocName, Integer revision, String username) throws Exception
    {
        ViewWikiRevisionDTO result = new ViewWikiRevisionDTO();
        
        List<WikiArchiveVO> archives = ServiceWiki.getSpecificWikiRevisionsFor(docSysId, fullDocName, revision, username);
        if(archives != null)
        {
            for(WikiArchiveVO vo : archives)
            {
                result.setDocSysId(vo.getUTableId());
                result.setRevision(vo.getUVersion());
                result.setComment(vo.getUComment());
                result.setCommittedByUser(vo.getUUserArchive());
                
                String tableColumn = vo.getUTableColumn();
                if(WikiDocument.COL_CONTENT.equalsIgnoreCase(tableColumn))
                {
                    result.setContent(vo.getUPatch());
                }
                else if(WikiDocument.COL_MODEL_PROCESS.equalsIgnoreCase(tableColumn))
                {
                    result.setMainModel(vo.getUPatch());
                }
                else if(WikiDocument.COL_MODEL_EXCEPTION.equalsIgnoreCase(tableColumn))
                {
                    result.setAbortModel(vo.getUPatch());
                }
                else if(WikiDocument.COL_DECISION_TREE.equalsIgnoreCase(tableColumn))
                {
                    result.setDecisionTreeModel(vo.getUPatch());
                }
            }//end of for loop
        }
        
        return result;
    }
    
    public List<ViewWikiRevisionDTO> getRevisionsToCompare(String docSysId, String fullDocName, Integer rev1, Integer rev2, String username) throws Exception
    {
        List<ViewWikiRevisionDTO> result = new ArrayList<ViewWikiRevisionDTO>();
        
        //get rev1
        ViewWikiRevisionDTO revision1View = getSpecificWikiRevisionsFor(docSysId, fullDocName, rev1, username);
//        updateViewWikiRevisionForType(revision1View, type);
        
        //get rev2
        ViewWikiRevisionDTO revision2View = getSpecificWikiRevisionsFor(docSysId, fullDocName, rev2, username);
//        updateViewWikiRevisionForType(revision2View, type);
        
        //prepare the result
        result.add(revision1View);
        result.add(revision2View);
        
        return result;
    }

    public FeedbackDTO getUserSurveyValues(String sysId, String documentName, String username) throws Exception
    {
        FeedbackDTO dto = new FeedbackDTO();
        WikiDocumentVO doc = ServiceWiki.getWikiDoc(sysId, documentName, username);
        if(doc != null)
        {
            //String reviewedDate = doc.getULastReviewedOn() != null ? DateManipulator.getDateInUTCFormat(doc.getULastReviewedOn()) : "Not Reviewed";
            dto.setLastReviewedDate(doc.getULastReviewedOn());
            dto.setFlagged(doc.ugetUIsRequestSubmission());
            
            //rating info
            double avgRatingDouble = ServiceWiki.getAverageResolutionRating(doc.getSys_id(), null, username);
            float avgRating = new Float(avgRatingDouble).floatValue();
            int avtRatingInt = Math.round(avgRating);
            dto.setRating(avtRatingInt);
        }
        
        return dto;
    }
    
    //private api
//    private void updateViewWikiRevisionForType(ViewWikiRevisionDTO revision, String type)
//    {
//        if(WikiDocument.COL_CONTENT.equalsIgnoreCase(type))
//        {
//            revision.setMainModel(null);
//            revision.setAbortModel(null);
//            revision.setDecisionTreeModel(null);
//        }
//        else if(WikiDocument.COL_MODEL_PROCESS.equalsIgnoreCase(type))
//        {
//            revision.setContent(null);
//            revision.setAbortModel(null);
//            revision.setDecisionTreeModel(null);
//        }
//        else if(WikiDocument.COL_MODEL_EXCEPTION.equalsIgnoreCase(type))
//        {
//            revision.setContent(null);
//            revision.setMainModel(null);
//            revision.setDecisionTreeModel(null);
//        }
//        else if(WikiDocument.COL_DECISION_TREE.equalsIgnoreCase(type))
//        {
//            revision.setContent(null);
//            revision.setMainModel(null);
//            revision.setAbortModel(null);
//        }
//    }

	private HistoryDTO buildHistory(String docName, WikiArchiveVO archive) {
		return new HistoryDTO(docName, 
				archive.getUVersion().intValue(), 
				null, 
				archive.getUComment(),
				archive.getSysCreatedBy(), 
				DateManipulator.getDateInUTCFormat(archive.getSysCreatedOn()),
				archive.getSysCreatedOn(),
				archive.getUIsStable());
	}
    
}
