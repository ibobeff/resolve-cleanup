/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.store;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.util.StringUtils;

public class HibernateStoreUtil
{
    @Deprecated
    public static String getActiveSid(String userName)
    {
        String sys_id = "";

        sys_id = WorksheetUtils.getActiveWorksheet(userName, null, null);
        if (StringUtils.isBlank(sys_id))
        {
            sys_id = "-1";
        }

        return sys_id;

    }
    
    public static String getActiveSid(String userName, String orgId, String orgName)
    {
        String sys_id = "";

        sys_id = WorksheetUtils.getActiveWorksheet(userName, orgId, orgName);
        if (StringUtils.isBlank(sys_id))
        {
            sys_id = "-1";
        }

        return sys_id;

    }

    public static boolean isProcessCompleted(String problemId, String wiki)
    {
        boolean result = true;

        if (StringUtils.isNotBlank(problemId))
        {
            boolean isActiveWS = ServiceWorksheet.isThisWorksheetCurrent(problemId);
            if (isActiveWS)
            {
                //this is cassandra
                result = ServiceWorksheet.isProcessCompleted(problemId, wiki);
            }
            else
            {
                boolean isArchived = ServiceHibernate.isThisWorksheetInArchive(problemId);
                if (isArchived)
                {
                    //this is archive tables
                    result = ServiceHibernate.isProcessCompleted(problemId, wiki);
                }
            }
        }

        return result;
    } // isProcessCompleted

    public static String getProcessEndConditionSeverity(String processName, String userName)
    {
        String value = "";
        return value;
    }

}
