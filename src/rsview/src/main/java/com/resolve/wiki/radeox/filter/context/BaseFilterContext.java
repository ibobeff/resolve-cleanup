/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.context;

import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.parameter.BaseMacroParameter;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;


/**
 * Base impementation for FilterContext
 *
 */

public class BaseFilterContext implements FilterContext {
  protected RenderContext context;

  public MacroParameter getMacroParameter() {
    return new BaseMacroParameter(context);
  }

  public void setRenderContext(RenderContext context) {
    this.context = context;
  }

  public RenderContext getRenderContext() {
    return context;
  }
}
