/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

/**
 * Macro for defining and displaying the URL to Resolve Execute Request for ActionTask execution. The # namespace is optional (if required).
 * 
 * RESULT DEFINITION {result:[text=title]|[refreshInterval=<refreshInterval in seconds>]|encodeSummary=<true/false to encode the summary for html>]|order=<ASC/DESC>]} [DEFINTION] {result}
 * 
 * DEFINITION
 *   syntax = description>task#namespace{wiki::nodeId}=description-wiki|result-wiki
 *   
 */
public class ResultMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.result";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        //Get parameters
        String title = StringUtils.isNotEmpty(params.get("text")) ? params.get("text") : "Show / Hide Results";
        title = HttpUtil.sanitizeValue(title);
        int refreshInterval = Integer.valueOf(StringUtils.isNotEmpty(params.get("refreshInterval")) ? params.get("refreshInterval") : "5");
        if (refreshInterval < 5)
        {
        	refreshInterval = 5;
        }
        int refreshCountMax = Integer.valueOf(StringUtils.isNotEmpty(params.get("refreshCountMax")) ? params.get("refreshCountMax") : "60");
        if (refreshCountMax < 1)
        {
        	refreshCountMax = 60;
        }
        Boolean encodeSummary = Boolean.valueOf(StringUtils.isNotEmpty(params.get("encodeSummary")) ? params.get("encodeSummary") : "true");
        String order = StringUtils.isNotEmpty(params.get("order")) ? params.get("order") : "";
        order = HttpUtil.sanitizeValue(order);
        Boolean progress = Boolean.valueOf(StringUtils.isNotEmpty(params.get("progress")) ? params.get("progress") : "false");

        //Unique id for the div to render to
        String divId = "results_" + JavaUtils.generateUniqueNumber();

        //Get the content (this is going to be the optional list of action tasks
        String content = params.getContent();
        content = HttpUtil.sanitizeValue(content);

        String[] split = content.trim().replace("\r", "").split("\n");
//        String[] split=content.trim().replace("\r", "").split("<br/>");
        StringBuffer actionTaskParameters = new StringBuffer();
        for (String actionTask : split)
        {
            if (StringUtils.isNotBlank(actionTask))
            {
                if (actionTaskParameters.length() > 0) actionTaskParameters.append(",");
                actionTaskParameters.append("'").append(actionTask.replace("'", "\\'")).append("'");
            }
        }

        //Build up div and script to render
        StringBuffer str = new StringBuffer();
        str.append("<div id=\"%1$s\"></div><br/>\n");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.Results',\n");
        str.append("        title : '%2$s',\n");
        str.append("        order : '%6$s',\n");
        str.append("        refreshInterval : %3$s,\n");
        str.append("        refreshCountMax : %8$s,\n");
        str.append("        encodeSummary : %4$s,\n");
        str.append("        progress : %7$s,\n");
        str.append("        actionTasks : [%5$s]\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('%1$s');\n");
        str.append("});\n");
        str.append("</script>\n");

        String result = String.format(str.toString(), divId, title, refreshInterval, encodeSummary, actionTaskParameters, order,progress,refreshCountMax);
        writer.write(result);
    }

}
