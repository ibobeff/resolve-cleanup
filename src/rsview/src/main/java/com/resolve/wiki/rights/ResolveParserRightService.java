/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rights;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.services.exception.WikiException;

/**
 * Action task right service interface
 * 
 * @author jeet.marwah
 *
 */
public interface ResolveParserRightService
{
    public AccessRights getAccessRightsFor(String username, ResolveParser resolveParser, ResolveActionTask resolveActionTask) throws WikiException;

}//ActionTaskRightService
