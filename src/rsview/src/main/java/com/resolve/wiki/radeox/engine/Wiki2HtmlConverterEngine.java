/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.engine;


/**
 * This engine is for converting the HTML wiki markups to HTML when viewing in WYSIWYG editor
 * 
 * @author jeet.marwah
 * 
 */
public class Wiki2HtmlConverterEngine extends BaseRenderEngine
{

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public Wiki2HtmlConverterEngine()
	{
	}

	/**
	 * Name of the RenderEngine. This is used to get a RenderEngine instance with EngineManager.getInstance(name);
	 * 
	 * @return name Name of the engine
	 */
	@Override
	public String getName()
	{
		return "wiki2htmlConverterEngine";
	}
}
