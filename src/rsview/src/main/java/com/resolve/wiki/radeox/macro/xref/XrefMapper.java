/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.xref;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import com.resolve.util.Log;

/**
 * Stores information and links to xref Java source code e.g.
 * http://nanning.sourceforge.net/xref/com/tirsen/nanning/MixinInstance.html#83
 * 
 */

public class XrefMapper
{

    private final static String FILENAME = "conf/xref.txt";

    private static XrefMapper instance;
    private Map xrefMap;

    public static synchronized XrefMapper getInstance()
    {
        if (null == instance)
        {
            instance = new XrefMapper();
        }
        return instance;
    }

    public XrefMapper()
    {
        xrefMap = new HashMap();

        BufferedReader br = null;
        boolean fileNotFound = false;
        try
        {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(FILENAME)));
            addXref(br);
        }
        catch (IOException e)
        {
            Log.log.warn("Unable to read " + FILENAME);
            fileNotFound = true;
        }
        finally
        {
            if (br != null)
            {
                try
                {
	                br.close();
                }
                catch (Exception e) { };
            }
        }

        if (fileNotFound)
        {
            br = null;
            try
            {
                br = new BufferedReader(new InputStreamReader(XrefMapper.class.getResourceAsStream("/" + FILENAME)));
                addXref(br);
            }
            catch (Exception e)
            {
                Log.log.warn("Unable to read " + FILENAME);
            }
	        finally
	        {
	            if (br != null)
	            {
	                try
	                {
		                br.close();
	                }
	                catch (Exception e) { };
	            }
	        }
        }
    }

    public void addXref(BufferedReader reader) throws IOException
    {
        String line;
        while ((line = reader.readLine()) != null)
        {
            StringTokenizer tokenizer = new StringTokenizer(line, " ");
            String site = tokenizer.nextToken();
            String baseUrl = tokenizer.nextToken();
            xrefMap.put(site.toLowerCase(), baseUrl);
        }
    }

    public boolean contains(String external)
    {
        return xrefMap.containsKey(external);
    }

    public Writer expand(Writer writer, String className, String site, int lineNumber) throws IOException
    {
        site = site.toLowerCase();
        if (xrefMap.containsKey(site))
        {
            writer.write("<a href=\"");
            writer.write((String) xrefMap.get(site));
            writer.write("/");
            writer.write(className.replace('.', '/'));
            writer.write(".html");
            if (lineNumber > 0)
            {
                writer.write("#");
                writer.write("" + lineNumber);
            }
            writer.write("\">");
            writer.write(className);
            writer.write("</a>");
        }
        else
        {
            Log.log.debug("Xrefs : " + xrefMap);
            Log.log.warn(site + " not found");
        }
        return writer;
    }

    public Writer appendTo(Writer writer) throws IOException
    {
        writer.write("{table}\n");
        writer.write("Binding|Site\n");
        Iterator iterator = xrefMap.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry entry = (Map.Entry) iterator.next();
            writer.write((String) entry.getKey());
            writer.write("|");
            writer.write((String) entry.getValue());
            writer.write("\n");
        }
        writer.write("{table}");
        return writer;
    }

}
