/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.attachment.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.resolve.dto.AttachmentModel;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.ServletUtils;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.ImageUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.attachment.FileAttachmentService;
import com.resolve.wiki.attachment.FileUploadUtil;
import com.resolve.wiki.attachment.WikiAttachmentType;
import com.resolve.wiki.store.WikiStore;
import com.resolve.wiki.web.WikiRequestContext;

public class FileAttachmentServiceImpl implements FileAttachmentService
{

    private static FileAttachmentServiceImpl instance;

	//only 1 instance of this should be available
	private FileAttachmentServiceImpl()
	{
	}

	public static FileAttachmentServiceImpl getInstance()
	{
		if (instance == null)
		{
			synchronized (FileAttachmentServiceImpl.class)
			{
				if (instance == null)
				{
					instance = new FileAttachmentServiceImpl();
				}
			}
		}

		return instance;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Implemented methods
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This API is for downloading the attached file.
	 */
//	public void downloadAttachment(WikiRequestContext wikiRequestContext) throws WikiException
//	{
//		HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
//		WikiStore store = wikiRequestContext.getWiki().getWikiStore();
//
//		String id = request.getParameter(ConstantValues.WIKI_PARAMETER_ATTACHMENT_SYS_ID);
//		String attachFileName = request.getParameter(ConstantValues.WIKI_PARAMETER_ATTACHMENT_FILE_NAME);
//		String type = request.getParameter(ConstantValues.WIKI_PARAMETER_ATTACHMENT_TYPE);
//		String docName = wikiRequestContext.getWiki().getDocumentName(wikiRequestContext);
//
//		if (StringUtils.isBlank(attachFileName) && StringUtils.isBlank(id)) // User profile image if the id and name is blank
//		{
//		    downloadUserImage(request, wikiRequestContext.getHttpServletResponse());
//		}
//		else
//		{
//
//    		if(type == null || type.trim().length() == 0)
//    		{
//    			type = WikiAttachmentType.DB.toString();
//    		}
//    
//    		WikiAttachment wikiAttachment = new WikiAttachment();
//    		wikiAttachment.setSys_id(id);
//    		wikiAttachment.setUFilename(attachFileName);
//    
//    		//get the obj
//    		if(id != null && id.trim().length() > 0)
//    		{
//    			wikiAttachment = (WikiAttachment) store.findById(wikiAttachment);
//    		}
//    		else
//    		{
//    			wikiAttachment = store.findWikiAttachmentFor(docName, attachFileName);
//    		}
//    
//    		HttpServletResponse response = wikiRequestContext.getHttpServletResponse();
//    //		response.setContentType("application/octet-stream");
//    //		response.addHeader("Content-disposition", "inline; filename=\"" + wikiAttachment.getUFilename() + "\"");
//    		response.setDateHeader("Last-Modified", wikiAttachment.getSysCreatedOn().getTime());
//    //		response.setContentLength(wikiAttachment.UgetWikiAttachmentContent().getUContent().length);
//    		try
//    		{
//    			ServletUtils.sendHeaders(wikiAttachment.getUFilename(), wikiAttachment.ugetWikiAttachmentContent().getUContent().length, request, response);
//    			response.getOutputStream().write(wikiAttachment.ugetWikiAttachmentContent().getUContent());
//    		}
//    		catch (IOException e)
//    		{
//    			throw new WikiException(WikiException.MODULE_WIKI_APP, WikiException.ERROR_WIKI_APP_SEND_RESPONSE_EXCEPTION, "Exception while sending response", e);
//    		}
//		}
//	}
	
	private void downloadUserImage(HttpServletRequest request, HttpServletResponse response) throws WikiException
	{
	    String username = null;
	    if (request.getAttribute("socialUserName") == null)
	    {
	        username = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
	    }
	    else
	    {
	        username = (String)request.getAttribute("socialUserName");
	    }
        
//	    String username = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        UsersVO user = ServiceHibernate.getUser(username);

        try
        {
            if (user.getUImage() != null)
            {
                ServletUtils.sendHeaders(new Date().getTime()+".png", user.getUImage().length,
                                         Arrays.copyOf(user.getUImage(), user.getUImage().length > 128 ? 128 : user.getUImage().length),
                                         request, response);
                response.getOutputStream().write(user.getUImage());
            }
        }
        catch (IOException e)
        {
            throw new WikiException(WikiException.MODULE_WIKI_APP, WikiException.ERROR_WIKI_APP_SEND_RESPONSE_EXCEPTION, "Exception while sending response", e);
        }
	}

	public void uploadAttachmentTest(WikiDocument wikiDocument, WikiRequestContext wikiRequestContext) throws WikiException
	{
		HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
		FileUploadUtil.uploadFileInServer(request, null);
	}

	/**
	 * This method is for uploading an attachment for a wikidoc
	 *
	 * @param wikiRequestContext
	 * @throws WikiException
	 */
	public void uploadAttachment(WikiDocument wikiDocument, WikiRequestContext wikiRequestContext) throws WikiException
	{
		HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
		WikiStore store = wikiRequestContext.getWiki().getWikiStore();
		String browserTimeZone = wikiRequestContext.getAttribute(ConstantValues.BROWSER_CLIENT_TIMEZONE)!=null ? (String) wikiRequestContext.getAttribute(ConstantValues.BROWSER_CLIENT_TIMEZONE) : null;//UTC-8
		String username = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		String sss = request.getParameter("fileUploadValue");
//		String type = request.getParameter(ConstantValues.WIKI_ATTACH_TYPE_KEY);
		boolean isGlobal = false;
		boolean isMultipleUpload = false;

		// Check that we have a file upload request
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart)
		{
			//if not an upload request, then return
			return;
		}
		// Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();
        // Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		if (wikiDocument == null)
		{
		    try
            {
                List<FileItem> items = upload.parseRequest(request);
                FileItem item = items.get(0);
                uploadUserPhoto(item, username);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
		    return;
		}

		WikiAttachment wikiAttachment =  new WikiAttachment();
		wikiAttachment.setULocation("");
		wikiAttachment.setUType("DB");//DEFAULT
		wikiAttachment.setSysCreatedBy(username);




		// Create a new file upload handler
//		ServletFileUpload upload = new ServletFileUpload(factory);
//		boolean is = upload.isMultipartContent(request);

		try{
			// Parse the request
			List<FileItem> items = upload.parseRequest(request);

			// Process the uploaded items
			Iterator<FileItem> iter = items.iterator();
			while (iter.hasNext())
			{
				FileItem item = iter.next();
				String name = item.getFieldName();
				if(name != null && name.equals(HibernateConstants.IS_GLOBAL))
				{
				    String value = item.getString();
				    isGlobal = value != null && value.trim().equalsIgnoreCase("true") ? true : false;
				    continue;
				}

                if(name != null && name.equals(HibernateConstants.IS_MULTIPLE_FILE))
                {
                    String value = item.getString();
                    isMultipleUpload = value != null && value.trim().equalsIgnoreCase("true") ? true : false;
                    continue;
                }

                if (item.isFormField())
				{
					processFormField(item, wikiAttachment);
				}
				else
				{
					processUploadedFile(item, wikiAttachment);
				}
			}

			if(isMultipleUpload)
			{
			    //its a zip file , so unzip it, upload all the files to the wiki doc in the folder
			}
			
			
			boolean updateAttachment = false;
			if(wikiAttachment.getSys_id() == null)
			{
			    //check if this file exist
			    WikiAttachment  wikiAttachmentTemp = store.findWikiAttachmentFor(wikiDocument.getUFullname(), wikiAttachment.getUFilename());
			    if(wikiAttachmentTemp != null)
			    {
			        wikiAttachmentTemp.setUSize(wikiAttachment.getUSize());
			        wikiAttachmentTemp.setSysCreatedBy(username);

			        WikiAttachmentContent content = wikiAttachmentTemp.ugetWikiAttachmentContent();
			        content.setUContent(wikiAttachment.ugetWikiAttachmentContent().getUContent());
			        updateAttachment = true;
			        store.updateAttachment(wikiAttachmentTemp);
			    }
			}

			if(!updateAttachment)
			{
    			if(isGlobal)
    			{
    			    //add it to System.Attachment document
    			    WikiDocument globalDocument = store.find(ConstantValues.NAMESPACE_SYSTEM, ConstantValues.DOCUMENT_ATTACHMENT);
    			    store.addAttachment(globalDocument, wikiAttachment, false);


    			    //add a reference to the current document
    			    AttachmentModel attachModel = new AttachmentModel();
    			    attachModel.setSys_id(wikiAttachment.getSys_id());
    			    attachModel.setCreatedBy(username);
    			    List<AttachmentModel> listOfAttachment = new ArrayList<AttachmentModel>();
    			    listOfAttachment.add(attachModel);

    			    store.globalAttach( wikiDocument, listOfAttachment);

    			}
    			else
    			{
    			    store.addAttachment(wikiDocument, wikiAttachment, false);
    			}
			}

		}catch(Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

	}

	public void uploadUserPhoto(FileItem item, String username) throws Exception
	{
//	    String fieldName = item.getFieldName();
        String fileName = item.getName();//C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
//        fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
//        String contentType = item.getContentType();//text/plain
//        boolean isInMemory = item.isInMemory();

        File originalFile = FileUtils.getFile(RSContext.getResolveHome() + "tmp/" + fileName);
        originalFile.getParentFile().mkdirs();
        OutputStream os = new FileOutputStream(originalFile);
        byte[] bytes = new byte[item.get().length];
        
        InputStream is = item.getInputStream();
        is.read(bytes);
        os.write(bytes);
        os.close();
        
        File resizedFile = ImageUtil.resizeImage(originalFile, 100, 100);
        FileInputStream fos = new FileInputStream(resizedFile);
        
        byte[] data = new byte[(int) resizedFile.length()];
        fos.read(data);
        fos.close();

        UsersVO user = ServiceHibernate.getUser(username);
        user.setUImage(data);
        UserUtils.saveUser(user, username);
        
        originalFile.delete();
        resizedFile.delete();
	}

	/**
     * This method is for renaming an attachment for a wikidoc
     *
     * @param wikiRequestContext
     * @throws WikiException
     */
    public void renameAttachment(WikiDocument wikiDocument, WikiRequestContext wikiRequestContext) throws WikiException
    {
        WikiStore store = wikiRequestContext.getWiki().getWikiStore();
        HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
        String username = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        //System.out.println(wikiRequestContext.getAttribute("ATTACHMENT_ORIGINAL_FILENAME"));
        //System.out.println(wikiRequestContext.getAttribute("ATTACHMENT_NEW_FILENAME"));
        String originalFileName = (String) wikiRequestContext.getAttribute("ATTACHMENT_ORIGINAL_FILENAME");
        String newFileName = (String) wikiRequestContext.getAttribute("ATTACHMENT_NEW_FILENAME");

        WikiAttachment  wikiAttachmentTemp = store.findAttachment(originalFileName);
        if(wikiAttachmentTemp != null)
        {
            wikiAttachmentTemp.setSysCreatedBy(username);
            wikiAttachmentTemp.setUFilename(newFileName);
            store.renameAttachment(wikiAttachmentTemp);
        }

	}

	@Override
	public void deleteAttachments(WikiRequestContext wikiRequestContext) throws WikiException
	{
		String docFullName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
		WikiDocument wikidoc = wikiRequestContext.getWiki().getDocument(docFullName);
		String deleteAttachIds = (String)wikiRequestContext.getAttribute(ConstantValues.WIKI_PARAMETER_DELETE_ATTACHMENTS);
		if(deleteAttachIds != null && deleteAttachIds.trim().length() > 0)
		{
			wikiRequestContext.getWiki().getWikiStore().deleteAttachments(wikidoc.getSys_id(), deleteAttachIds);
		}
	}
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	private void processUploadedFile(FileItem item, WikiAttachment wikiAttachment) throws Exception
	{
		//few attributes for debuging purpose...may be removed/commented out
		String fieldName = item.getFieldName();
		String fileName = item.getName();//C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
		fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
		String contentType = item.getContentType();//text/plain
		boolean isInMemory = item.isInMemory();

		long sizeInBytes = item.getSize();
		InputStream uploadedStream = item.getInputStream();
		byte[] data = new byte[(int) sizeInBytes];
		uploadedStream.read(data);

		WikiAttachmentContent wac = new WikiAttachmentContent();
		wac.setUContent(data);

		wikiAttachment.setUFilename(fileName);
		wikiAttachment.setUSize(data.length);
		wikiAttachment.usetWikiAttachmentContent(wac);
	}


	private void processFormField(FileItem item, WikiAttachment wikiAttachment) throws WikiException
	{
		String name = item.getFieldName();
		String value = item.getString();
		if (name.equalsIgnoreCase(ConstantValues.WIKI_FILENAME_KEY))
		{
			wikiAttachment.setUFilename(value);
		}
		else if (name.equalsIgnoreCase(ConstantValues.WIKI_ATTACH_TYPE_KEY))
		{
			wikiAttachment.setUType(value);
		}
		else if (name.equalsIgnoreCase(ConstantValues.BROWSER_CLIENT_TIMEZONE))
		{
			if(value != null)
			{
				wikiAttachment.setSysCreatedOn(DateUtils.getCurrentDateInGMT(value, true));
				wikiAttachment.setSysUpdatedOn(DateUtils.getCurrentDateInGMT(value, true));
			}
			else
			{
				wikiAttachment.setSysUpdatedOn(GMTDate.getDate());
				wikiAttachment.setSysCreatedOn(GMTDate.getDate());
			}
		}
	}

}
