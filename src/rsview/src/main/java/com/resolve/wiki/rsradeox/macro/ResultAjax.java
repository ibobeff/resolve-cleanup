/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;

import com.resolve.html.Table;
import com.resolve.services.ServiceCache;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.util.vo.ActionTaskInfoVO;
import com.resolve.services.vo.UserWorksheetData;
import com.resolve.util.Constants;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.store.HibernateStoreUtil;

/**
 * 
 * This is a utility class that is called using the Ajax call from the UI. It
 * works in combination with the ResultMacro which prepares the ExtJs code and
 * this call suffices that request from the client. That is the reason this
 * utility class is in this package as they are together working to show the
 * results on the wiki page.
 * 
 * @author jeet.marwah
 * 
 */
public class ResultAjax
{
    public static final String keys = "DESCRIPTION,SUMMARY,RESULT";
    public static final String heading = "Description,Summary,Result";

    static AtomicInteger ajaxcount = new AtomicInteger(0);

    public String getHtmlForResultWikiTag(Wiki wiki, HttpServletRequest request)
    {
        String currentUser = request.getParameter("currentUser");
        String problemId = request.getParameter("problemId");
        String orgId = request.getParameter(Constants.EXECUTE_ORG_ID);
        String orgName = request.getParameter(Constants.EXECUTE_ORG_NAME);
        
        if (StringUtils.isEmpty(problemId) || problemId.startsWith("$"))
        {
            problemId = HibernateStoreUtil.getActiveSid(currentUser, orgId, orgName);
        }
        Log.log.trace("getHtmlForResultWikiTag - count: " + ajaxcount.getAndIncrement() + " problemid: " + problemId + " user: " + currentUser);
        String textHideExpand = request.getParameter("textHideExpand");
        String encodeSummary = request.getParameter("encodeSummary");
        String isDoneYetId = request.getParameter("isDoneYetId");
        String order = request.getParameter("isOrderByAsc");
        boolean isOrderByAsc = (order != null && order.equalsIgnoreCase("true")) ? true : false;

        String type = request.getParameter("type");// if task then refresh based on Action task else Runbook
        String refreshInterval = request.getParameter("refreshInterval");// this may not be required
        String refreshStartWiki = request.getParameter("refreshStartWiki");

        String[] actionNamespaceArr = request.getParameterValues("actionNamespace");
        String[] actionNameArr = request.getParameterValues("actionName");
        String[] nodeIdArr = request.getParameterValues("nodeId");
        String[] actionWikiArr = request.getParameterValues("actionWiki");
        String[] descriptionArr = request.getParameterValues("description");

        boolean doRefresh = true;
        int refreshIntervalInt = 5;
        if (StringUtils.isNotEmpty(refreshInterval))
        {
            refreshIntervalInt = Integer.parseInt(refreshInterval);
            Log.log.trace("got refreshInterval: " + refreshIntervalInt);
        }

        // check if the problemid exist in cache, if no, then update it right away , if yes check the time of the last qry
        if (ServiceCache.hasProblemIdInWorksheetDataCache(problemId))
        {
            UserWorksheetData userInfo = ServiceCache.getWorksheetDataFromCache(problemId);
            if (!userInfo.isCompleted)
            {
                // check if the CURRENT_TIME > QUERY_TIME
                long currentTime = System.currentTimeMillis();
                long lastQryTime = userInfo.lastQueryTime;
                long diffTime = currentTime - lastQryTime;
                if (diffTime > refreshIntervalInt)
                {
                    // update the cache from the DB
                    Log.log.trace("updating cache with problemid: " + problemId + " diffTime: " + diffTime);
                    doDBQueryAndUpdataCache(wiki, refreshIntervalInt, problemId, actionNamespaceArr, actionNameArr, nodeIdArr, actionWikiArr, descriptionArr, isOrderByAsc, true, type, refreshStartWiki);

                }
                else
                {
                    // take the data from the cache
                    Log.log.trace("taking from cache with problemid: " + problemId);
                }
            }

            doRefresh = !userInfo.isCompleted;
        }
        else
        {
            // update the cache
            Log.log.trace("updating cache with problemid: " + problemId + " not in cache");
            doDBQueryAndUpdataCache(wiki, refreshIntervalInt, problemId, actionNamespaceArr, actionNameArr, nodeIdArr, actionWikiArr, descriptionArr, isOrderByAsc, false, type, refreshStartWiki);

            UserWorksheetData userInfo = ServiceCache.getWorksheetDataFromCache(problemId);
            if (userInfo != null)
            {
                doRefresh = !userInfo.isCompleted;
            }
            else
            {
                doRefresh = true;
            }
        }

        // retrive data from cache
        List<Map<String, String>> resultTable = getDataFromCache(problemId, actionNamespaceArr, actionNameArr, nodeIdArr, actionWikiArr, descriptionArr);

        // get the updated endStatus
        String endstatus = request.getParameter("endstatus");
        endstatus = getEndStatus(wiki, currentUser, resultTable, endstatus);

        String style = request.getParameter("divStyle");
        String divId = "wiki_results_" + JavaUtils.generateUniqueNumber();// making sure that the id is unique as the wiki page can have multiple {results} tag
        String imgId = "showhide_img_id_" + JavaUtils.generateUniqueNumber();// making sure that the id is unique as the wiki page can have multiple {results} tag
        String link = "<img border=\"0\" id=\"" + imgId + "\" src=\"/resolve/images/collapseall.gif\" />";
        ;
        if (style.contains(ResultMacro.DISPLAY_NONE))
        {
            link = "<img border=\"0\" id=\"" + imgId + "\" src=\"/resolve/images/expandall.gif\" />";
        }
        Properties properties = getResultTableHtmlProperties(encodeSummary);

        StringBuffer html = new StringBuffer("<a href=\"javascript:showhide('").append(divId).append("','").append(imgId).append("')\">");
        html.append(link).append("</a>").append(textHideExpand).append("&nbsp;").append(endstatus);
        html.append("<div id=\"").append(divId).append("\"style=\"").append(style).append("\" >");
        html.append("<input type='hidden' name='isDoneYet' id='").append(isDoneYetId).append("' value='" + (doRefresh ? "" : ConstantValues.STOP_AJAX_REFRESH) + "'>");
        html.append(Table.print(resultTable, keys, heading, properties));
        html.append("</div>");

        return html.toString();
    }

    public static Properties getResultTableHtmlProperties(String encodeSummary)
    {
        Properties properties = new Properties();
        properties.setProperty("heading.style.*", "font-weight: bold;");
        properties.setProperty("table.style", "border-spacing:2px;border-collapse: separate;");
        properties.setProperty("data.style.result", "text-align: center; width: 80px");
        properties.setProperty("data.style.result.good", "background-color: lightgreen;");
        properties.setProperty("data.style.result.warning", "background-color: khaki;");
        properties.setProperty("data.style.result.severe", "background-color: orange;");
        properties.setProperty("data.style.result.critical", "background-color: tomato;");

        if (StringUtils.isEmpty(encodeSummary) || encodeSummary.equalsIgnoreCase("true"))
        {
            properties.setProperty("html.escape", "true");
        }
        else
        {
            properties.setProperty("html.escape", "false");
        }

        return properties;
    }

    public static boolean getDoRefreshValue(Wiki wiki, String refreshStartWiki, String problemId)
    {
        boolean doRefresh = !HibernateStoreUtil.isProcessCompleted(problemId, refreshStartWiki);
        return doRefresh;
    }

    public static boolean getDoRefreshValueForTask(Wiki wiki, String problemId, String[] actionNamespaceArr, String[] actionNameArr)
    {
        //TODO we will rewrite this later.
        //boolean doRefresh = !HibernateStoreUtil.isProcessCompletedForTask(problemId, actionNamespaceArr, actionNameArr);
        boolean doRefresh = true;
        return doRefresh;
    }

    private String getEndStatus(Wiki wiki, String currentUser, List<Map<String, String>> resultTable, String endstatus)
    {
        boolean calcEndStatus = false;
        String endstatusSeverity = "good";

        if (StringUtils.isNotEmpty(endstatus))
        {
            int pos = endstatus.indexOf('.');
            if (pos > 0)
            {
                endstatus = ResultMacro.getEndStatusHTML(HibernateStoreUtil.getProcessEndConditionSeverity(endstatus, currentUser));
            }

            // otherwise get the best or worst status
            else
            {
                calcEndStatus = true;

                endstatus = endstatus.toLowerCase();
                if (!endstatus.equals("best"))
                {
                    endstatus = "worst";
                }
            }
        }
        else
        {
            endstatus = "";
        }

        // set endstatus severity
        if (calcEndStatus)
        {
            for (Map<String, String> val : resultTable)
            {
                String resultSeverity = val.get("RESULT");
                endstatusSeverity = ResultMacro.getEndStatusSeverity(resultSeverity, endstatusSeverity, endstatus);
            }

            endstatus = ResultMacro.getEndStatusHTML(endstatusSeverity);
        }

        return endstatus;

    }

    private void doDBQueryAndUpdataCache(Wiki wiki, int refreshIntervalInt, String problemId, String[] actionNamespaceArr, String[] actionNameArr, String[] nodeIdArr, String[] actionWikiArr, String[] descriptionArr, boolean isOrderByAsc, boolean allowSkip, String type, String refreshStartWiki)
    {
        ReentrantLock lock = com.resolve.util.LockUtils.getLock(Constants.LOCK_RESULTMACRO_PROBLEMID + problemId);
        if (!lock.isLocked() || allowSkip == false)
        {
            try
            {
                // getting the lock
                Log.log.trace("waiting for lock - problemid: " + problemId);
                lock.lock();
                Log.log.trace("got lock");

                List<ActionTaskInfoVO> dataFromDB = getListForResultMacro(problemId, actionNamespaceArr, actionNameArr, nodeIdArr, actionWikiArr, descriptionArr, isOrderByAsc);

                // update the cache
                UserWorksheetData userInfo = null;
                if (ServiceCache.hasProblemIdInWorksheetDataCache(problemId))
                {
                    userInfo = ServiceCache.getWorksheetDataFromCache(problemId);
                }
                else
                {
                    userInfo = new UserWorksheetData();
                }

                userInfo.data = dataFromDB != null ? dataFromDB : new ArrayList<ActionTaskInfoVO>();
                userInfo.problemId = problemId;
                userInfo.lastQueryTime = System.currentTimeMillis();
                userInfo.interval = refreshIntervalInt;

                // check if the UI needs to refresh
                if (StringUtils.isNotEmpty(type) && type.equalsIgnoreCase("task"))
                {
//                    userInfo.isCompleted = !getDoRefreshValueForTask(wiki, problemId, actionNamespaceArr, actionNameArr);
                    userInfo.isCompleted = true;
                }
                else
                {
                    userInfo.isCompleted = !getDoRefreshValue(wiki, refreshStartWiki, problemId);
                }

                ServiceCache.putWorksheetDataFromCache(problemId, userInfo);
                Log.log.trace("query complete");
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
            finally
            {
                lock.unlock();
                Log.log.trace("unlocked");
            }
        }
    }

    public List<ActionTaskInfoVO> getListForResultMacro(String problemId, String[] actionNamespaceArr, String[] actionNameArr, String[] nodeIdArr, String[] actionWikiArr, String[] descriptionArr, boolean isOrderByAsc)
    {
        List<ActionTaskInfoVO> result = null;

/*        if (StringUtils.isNotEmpty(problemId))
        {
            boolean isActiveWS = ServiceCassandra.isThisWorksheetInCassandra(problemId);
            if (isActiveWS)
            {
                //this is cassandra
                result = ServiceCassandra.getListForResultMacroWorksheetCAS(problemId, null, null, nodeIdArr, actionWikiArr, descriptionArr, isOrderByAsc);
            }
            else
            {
                boolean isArchived = ServiceHibernate.isThisWorksheetInArchive(problemId);
                if (isArchived)
                {
                    //this is archive tables
                    List<String> actionTaskids = ServiceHibernate.getActionTaskids(null, null, problemId, true);
                    result = ServiceArchive.getListForResultMacroArchiveWorksheet(problemId, null, null, nodeIdArr, actionWikiArr, descriptionArr, isOrderByAsc, actionTaskids);
                }
            }
        }
*/
        return result;
    }

    private List<Map<String, String>> getDataFromCache(String problemId, String[] actionNamespaceArr, String[] actionNameArr, String[] nodeIdArr, String[] actionWikiArr, String[] descriptionArr)
    {
        List<Map<String, String>> filteredData = new ArrayList<Map<String, String>>();

        UserWorksheetData userInfo = ServiceCache.getWorksheetDataFromCache(problemId);
        if (userInfo != null)
        {
            List<ActionTaskInfoVO> allDataForWorksheet = userInfo.data;

            // TODO - may be optimized for query
            if (actionNameArr != null && actionNameArr.length > 0)
            {

                for (int count = 0; count < actionNameArr.length; count++)
                {
                    // + "#" + actionNamespaceArr[count] + "|" + nodeIdArr[count];
                    String key = actionNameArr[count];

                    boolean hasNamespace = false;
                    boolean hasNodeId = false;
                    if (StringUtils.isNotEmpty(actionNamespaceArr[count]))
                    {
                        key += "#" + actionNamespaceArr[count];
                        hasNamespace = true;
                    }
                    if (StringUtils.isNotEmpty(nodeIdArr[count]))
                    {
                        key += "|" + nodeIdArr[count];
                        hasNodeId = true;
                    }
                    for (ActionTaskInfoVO vo : allDataForWorksheet)
                    {
                        String compareKey = null;
                        if (hasNodeId == true)
                        {
                            compareKey = vo.getKey();
                        }
                        else if (hasNamespace == true)
                        {
                            //Note: not sure why it was changed but if we uncomment the line below 
                            //then there is no hyperlink, but current one renders the link. 
                            //compareKey = vo.getKeyName() + "#" + vo.getKeyNameNamespace();
                            compareKey = vo.getKeyNameNamespace();
                        }
                        else
                        {
                            compareKey = vo.getKeyName();
                        }

                        if (compareKey.equalsIgnoreCase(key))
                        {
                            //find the wiki for this actiontask and override the description if its coming from the UI
                            String atActionWiki = actionWikiArr[count];
                            String atDescription = descriptionArr[count];

                            filteredData.add(vo.getRenderedRowInfo(atActionWiki, atDescription));//getRowInfo());
                        }
                    }
                }
            }
            else
            {
                // all of the data
                for (ActionTaskInfoVO vo : allDataForWorksheet)
                {
                    //Call to getRenderedRowInfo() makes sure that hyperlink is rendered (partial RBA#4575)
                    filteredData.add(vo.getRenderedRowInfo("", ""));
                }
            }
        }
        return filteredData;
    }
}
