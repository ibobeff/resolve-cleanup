/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.resolve.services.exception.WikiException;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class GenericUtils
{
    
	/**
	 * Reads the file and returns its contents. Usually used to read *.vm files
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String getFileContent(File file) throws IOException
	{
		return getFileContent(new FileReader(file));
	}
	
	public static void writeFile(String content, File file) throws IOException
	{
		FileOutputStream fos = null;
		DataOutputStream dos = null;

		try
		{
			fos = new FileOutputStream(file);
			dos = new DataOutputStream(fos);
			dos.writeBytes(content);
		}
		finally
		{
			try
			{
				dos.flush();
				fos.flush();
				dos.close();
				fos.close();
			}
			catch(Exception e)
			{
				
			}
		}
	}//writeFile

	public static boolean isEmptyModelString(String modelXml)
    {
    	boolean isEmptyString = false;
    	
    	String baseEmptyStringFinalAbort = "<mxGraphModel><root><mxCell id=\"0\"/><mxCell id=\"1\" parent=\"0\"/></root></mxGraphModel>";
    	baseEmptyStringFinalAbort = StringUtils.deleteWhitespace(baseEmptyStringFinalAbort);
    	
    	String baseEmptyStringMain = "<mxGraphModel><root><Workflow label=\"Workflow\" description=\"\" href=\"\" id=\"0\">"+      
    								 "<mxCell/></Workflow><Layer label=\"Default Layer\" id=\"1\">"+      
    								 "<mxCell parent=\"0\"/></Layer></root></mxGraphModel>";
    	baseEmptyStringMain = StringUtils.deleteWhitespace(baseEmptyStringMain);
    	
    	
    	if(modelXml != null && !modelXml.equals(""))
    	{
    		modelXml = StringUtils.deleteWhitespace(modelXml);
    		
    		if(StringUtils.equalsIgnoreCase(modelXml, baseEmptyStringFinalAbort))
    		{
    			isEmptyString = true;
    		}
    		else if(StringUtils.equalsIgnoreCase(modelXml, baseEmptyStringMain))
    		{
    			isEmptyString = true;
    		}
    	}
    	else
    	{
    		isEmptyString = true;
    	}

    	return isEmptyString;
    	
    }

	/**
	 * Reads the file and returns its contents. Usually used to read *.vm files
	 * 
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	public static String getFileContent(Reader reader) throws IOException
	{
		StringBuffer content = new StringBuffer();
		BufferedReader fr = new BufferedReader(reader);
		String line;
		line = fr.readLine();
		
		try
		{
			whileLoop:
			while (true)
			{
				if (line == null)
				{
					break whileLoop;
				}
				content.append(line);
				content.append("\n");
				line = fr.readLine();
			}
			
			return content.toString();
		}
		finally
		{
			fr.close();
			reader.close();
		}
	}
	
	/**
	 * Alternative way to getFileContent() for reading a file from the filesystem.
	 *  
	 * @param file
	 * @return
	 */
	public static String readFile(File file)
	{
		StringBuffer fileContent = new StringBuffer();

		FileInputStream fis = null;
		BufferedReader dis = null;

		try
		{
			fis = new FileInputStream(file);

			// Here BufferedInputStream is added for fast reading.
			dis = new BufferedReader(new InputStreamReader(fis));

			String tempStr = null;

			// dis.available() returns 0 if the file does not have more lines.
			while ((tempStr = dis.readLine()) != null)
			{
				fileContent.append(tempStr);
			}

		}
		catch (FileNotFoundException e)
		{
			Log.log.error(e.getMessage(), e);
		}
		catch (IOException e)
		{
			Log.log.error(e.getMessage(), e);
		}
		finally
		{
			try
			{
				// dispose all the resources after using them.
				fis.close();
				dis.close();
			}
			catch (Exception e)
			{
			}
		}

		return fileContent.toString();
	}

	public static List<String> filenameINDir(String dirName) throws Exception
	{
		List<String> fn = new ArrayList<String>();

		File dir = FileUtils.getFile(dirName);
		String[] children = dir.list();
		if (children == null)
		{
			// Either dir does not exist or is not a directory
		}
		else
		{
			for (int i = 0; i < children.length; i++)
			{
				// Get filename of file or directory
				String filename = children[i];
//				if(filename.indexOf(".vm") > 0)
//					System.out.println(filename);
				fn.add(filename);
			}
		}

		return fn;

	}
	
	/**
	 * This is to generate the checksum for the string used for model, content, final and abort.
	 * The encoding "ISO8859_1" is used.
	 * 
	 * @param str
	 * @return
	 */
	public static String generateCheckSum(String str)
	{
		String hash = "";
		
		if(str != null && str.trim().length() > 0)
		{
			try
			{
//				MD5 md5 = new MD5();
//				md5.update(str, null);
//				hash = md5.asHex();
				hash = DigestUtils.md5Hex(str);
			}
			catch(Exception e)
			{
				hash = "";
			}
		}
		return hash;
	}//generateCheckSum
	
	/**
	 * copy dir from one location to other
	 * 
	 * @param sourceLocation
	 * @param targetLocation
	 */
	public static void copyDirectory(File sourceLocation, File targetLocation) throws WikiException
	{

		if (sourceLocation.isDirectory())
		{
			if (!targetLocation.exists())
			{
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++)
			{
				copyDirectory(FileUtils.getFile(sourceLocation, children[i]), FileUtils.getFile(targetLocation, children[i]));
			}
		}
		else
		{
			InputStream in = null;
			OutputStream out = null;
			try
			{
				in = new FileInputStream(sourceLocation);
				out = new FileOutputStream(targetLocation);

				// Copy the bits from instream to outstream
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0)
				{
					out.write(buf, 0, len);
				}

			}
			catch (Exception e)
			{
				Log.log.error(e.getMessage(), e);
				throw new WikiException(e);
			}
			finally
			{
				try
				{
					in.close();
					out.close();
				}
				catch (Exception e)
				{
				}
			}
		}

	}//copyDirectory
	
	/**
	 * This method reads the servlet request from the url to extract Java Objects that
	 * can identify the roles required and set a flag to enable or disable the visibility
	 * of wiki control panel
	 * @param request
	 * @return
	 * Example URL:http://localhost:8080/resolve/jsp/rswiki.jsp?wiki=Runbook.WebHome&control=false 
	 * wikiName = Runbook.WebHome
	 * control = false
	 * username = <login id>
	 * @throws WikiException 
	 */
//	public static String setContentPanelFlag(HttpServletRequest request) throws WikiException
//	{
//	    String wikiName = StringUtils.escapeHtml(request.getParameter("wiki"));
//	    String control = request.getParameter(AdminConstants.CONTROL.getTagName());
//	    String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
//	    
//	    //for http://localhost:9080/resolve/jsp/rswiki.jsp?wiki= or http://localhost:9080/resolve/jsp/rswiki.jsp (wiki=null)
//	    if(StringUtils.isEmpty(wikiName))
//	    {
//	        wikiName = getOriginalWikiName(request, WikiGUIConstants.WIKI_HOMEPAGE);
//	    }
//	    
//	    //only for special cases when wiki=CURRENT or wiki=HOMEPAGE
//	    if((WikiGUIConstants.WIKI_CURRENT).equalsIgnoreCase(wikiName))
//	    {
//	        wikiName = getOriginalWikiName(request, WikiGUIConstants.WIKI_CURRENT);
//	    }
//	    else if(WikiGUIConstants.WIKI_HOMEPAGE.equalsIgnoreCase(wikiName))
//	    {
//	        wikiName = getOriginalWikiName(request, WikiGUIConstants.WIKI_HOMEPAGE);
//	    }
//	    
//	    //find the full name of wiki doc by querying database - may be a performance hit??
//        String fullName = wikiName;
//        boolean isShow = true;
//        
//        try
//        {
//    	    WikiDocument wikidoc = StoreUtility.find(wikiName); // throws exception if wikidoc not found
//    	    //check if access rights match to show the wiki control panel
//            isShow = isShowControlPanel(username, wikidoc, control);    
//    	    if(wikidoc != null)
//    	    {
//    	        fullName = wikidoc.getUFullname();
//    	    }
//        }
//        catch(Exception e)
//        {
//            Log.log.error("Error in setContentPanelFlag" + e.getMessage());            
//        }
//	    
//	    
//	    //passing wiki doc name, toogle button view and boolean value to show wiki control panel to the GWT component using rswiki.jsp file 
//	    StringBuffer result = new StringBuffer();
//        result.append("<div id=\"").append(SharedConstants.SHOWCONTROLPANEL).append("\" >").append(isShow).append("</div>");
//        result.append("<div id=\"").append(SharedConstants.WIKIDOC_FULL_NAME_KEY).append("\" >").append(ESAPI.encoder().encodeForHTML(fullName)).append("</div>");
//        result.append("<div id=\"").append(SharedConstants.SHOWTOGGLEBUTTON).append("\" >").append(isShow).append("</div>");
//  
//	    return result.toString();
//	    
//	}//setContentPanelFlag
	

    /**
     * This method returns the original wikiname for special cases when wiki=CURRENT or wiki=HOMEPAGE
     * and checks to verify that wikiName is never a null value
     * @param request
     * @param specialCase
     * @return
     */
//    private static String getOriginalWikiName(HttpServletRequest request, String specialCase)
//    {
//        String wikiName = null;
//        Map hmValues = null;
//        try
//        {
//            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI); 
//            if(specialCase.equalsIgnoreCase(WikiGUIConstants.WIKI_CURRENT))
//            {
//                hmValues = webApi.current(request, null);
//            }
//            else if(specialCase.equalsIgnoreCase(WikiGUIConstants.WIKI_HOMEPAGE))
//            {
//                hmValues = webApi.homepage(request, null);
//            }        
//            assert hmValues != null;
//            wikiName = (String) hmValues.get(ConstantValues.WIKI_DOCNAME_KEY);
//        }
//        catch (Exception e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
//        
//        assert wikiName != null;
//        
//        return wikiName;
//        
//    } // getOriginalWikiName

    /**
     * This method evalutes to true if the login user roles and wiki roles/access rights permit to show the wiki 
     * control panel
     * @param userName
     * @param wikidoc
     * @param showControlPanel
     * @return
     */
//    private static boolean isShowControlPanel(String userName, WikiDocument wikidoc, String showControlPanel)
//    {
//        boolean value = false;
//        String controlFlag = showControlPanel;
//        
//        //only if control parameter exist, it will override the system setting
//        if(StringUtils.isNotEmpty(controlFlag))
//        {
//            if(controlFlag.trim().equalsIgnoreCase("true"))
//            {
//                value = true;
//            }
//        } 
//        //superusers will always see the CP
//        else if(userName.equals(Constants.RESOLVE_MAINT) || userName.equals(Constants.ADMIN))
//        {
//            value = true;
//        }
//        else if(wikidoc == null) //this is a newly created document
//        {
//            //check if the user roles is a part of exclude list, if yes, don't show the control panel for the new document
//            //ignore the wiki.controlpanel.show.default property at this point
//            List<String> valueexcludeCPProperty = PropertiesUtil.getPropertyList(Constants.WIKI_EXCLUDE_ROLES_TO_SHOW_CONTROLPANEL);
//            if((valueexcludeCPProperty != null) && !userRolesListedInExcludeRoles(userName, valueexcludeCPProperty))
//            {
//                value = true;
//            }
//            //if wiki.controlpanel.show.exclude.roles<list> = null or empty, show the control panel based on wiki.controlpanel.show.default property
//            if(valueexcludeCPProperty == null)
//            {
//                value = isSystemPropShowCP();
//            }
//        }
//        else if(isSystemPropShowCP())//get the system setting by checking system > properties > wiki.controlpanel.show.default
//        {
//            //check the system property excluded roles 
//            List<String> valueexcludeCPProperty = PropertiesUtil.getPropertyList(Constants.WIKI_EXCLUDE_ROLES_TO_SHOW_CONTROLPANEL);
//            if((valueexcludeCPProperty != null) && !userRolesListedInExcludeRoles(userName, wikidoc, valueexcludeCPProperty))           
//            {
//                //check if wiki admin and edit access rights include userroles
//                value = com.resolve.services.hibernate.util.StoreUtility.hasUserAccessRight(userName, wikidoc);
//            }
//        }
//        else 
//        {
//            //checks when wiki.controlpanel.show.default = false and wiki.controlpanel.show.exclude.roles<list> = <values>
//            boolean propertyValueControlShowDefault = isSystemPropShowCP();
//            if(propertyValueControlShowDefault == false)
//            {
//                List<String> valueexcludeCPProperty = PropertiesUtil.getPropertyList(Constants.WIKI_EXCLUDE_ROLES_TO_SHOW_CONTROLPANEL);
//                if((valueexcludeCPProperty != null) && userRolesListedInExcludeRoles(userName, wikidoc, valueexcludeCPProperty))
//                {
//                    value = true;
//                }
//                else 
//                {
//                    //check if wiki admin and edit access rights include userroles
//                    value = com.resolve.services.hibernate.util.StoreUtility.hasUserAccessRight(userName, wikidoc);
//                }
//            }
//            else 
//            {
//                value = false;
//            }
//        }
//
//        return value;
//        
//    } // isShowControlPanel
	
    /** 
     * This method returns true if any one of the roles of the Resolve Login User is listed in the
     * system > properties > wiki.controlpanel.show.exclude.roles<list>
     * @param userName
     * @param valueexcludeCPProperty
     * @return
     */
//	private static boolean userRolesListedInExcludeRoles(String userName, List<String> valueexcludeCPProperty)
//    {
//	    boolean value = false;
//        if(valueexcludeCPProperty != null && !valueexcludeCPProperty.isEmpty())
//        {
//            Set<String> userRoles = null;
//            try
//            {
//                userRoles = UserUtils.getUserRoles(userName);
//            }
//            catch (Throwable t)
//            {
//                Log.log.error("User -->" + userName + " does not have any roles listed ", t);
//            }
//
//            for(String userRole : userRoles)
//            {
//                //if there is a role to exclude
//                if(valueexcludeCPProperty.contains(userRole))
//                {
//                    value = true;
//                    break;
//                }
//            }//end of for loop
//        }
//
//        return value;
//    } // userRolesListedInExcludeRoles

    /** 
	 * This method returns true if any one of the roles of the Resolve Login User is listed in the
	 * system > properties > wiki.controlpanel.show.exclude.roles<list>
	 * @param userName
	 * @param wikidoc
	 * @param valueexcludeCPProperty
	 * @return
	 */
//	private static boolean userRolesListedInExcludeRoles(String userName, WikiDocument wikidoc, List<String> valueexcludeCPProperty)
//    {
//	    boolean value = false;
//	    if(valueexcludeCPProperty != null && !valueexcludeCPProperty.isEmpty())
//	    {
//	        Set<String> userRoles = null;
//	        try
//	        {
//	            userRoles = UserUtils.getUserRoles(userName);
//	        }
//	        catch (Throwable t)
//	        {
//	            Log.log.error("Error in userRolesListedInExcludeRoles() user -->" + userName + " with Document -->" + wikidoc.getUFullname(), t);
//	        }
//
//	        for(String userRole : userRoles)
//	        {
//	            //if there is a role to exclude
//	            if(valueexcludeCPProperty.contains(userRole))
//	            {
//	                value = true;
//	                break;
//	            }
//	        }//end of for loop
//	    }
//
//        return value;
//        
//    } // userRolesListedInExcludeRoles

    /**
     * This method returns true if the default value of show control panel property under
     * system > properties > wiki.controlpanel.show.default is set to true   
     * @return
     */
//    private static boolean isSystemPropShowCP()
//    {
//	    boolean value = false;
//	    //get the system setting by checking system > properties > wiki.controlpanel.show.default
//        String valueDefaultCPProperty = PropertiesUtil.getPropertyString(Constants.WIKI_CONTROLPANEL_SHOW_DEFAULT);
//        //set the basic flag and return
//        boolean showCP = StringUtils.isNotEmpty(valueDefaultCPProperty) && valueDefaultCPProperty.equalsIgnoreCase("false") ? false : true; 
//        if(showCP)
//        {
//            value = true;
//        }
//        else
//        {
//            value = false;
//        }
//        
//        //treat empty value for WIKI_CONTROLPANEL_SHOW_DEFAULT system property as true
//        return value;
//        
//    } // isSystemPropShowCP

    /*
    public static void main(String[] args) throws Exception
	{
//		List<String> def = filenameINDir("C:\\project\\platform\\windows\\glidesoft\\xwiki\\src\\main\\web\\skins\\default");
//		for(String filename : def)
//		{
//			if(filename.indexOf(".vm") > 0)
//				System.out.println(filename);
//		}
//		System.out.println("*****************************");
//		List<String> tmp = filenameINDir("C:\\project\\platform\\windows\\glidesoft\\xwiki\\src\\main\\web\\templates");
//		for(String filename : tmp)
//		{
//			if(!def.contains(filename))
//				System.out.println(filename);
//		}
		
		String str = "fewerf wer werfwe we werfwerwewdfkf k wsopedf kw we I love u and the world around";
		System.out.println(generateCheckSum(str));
	}
    */
	
	

}
