/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.resolve.persistence.model.WikiAttachment;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.util.HttpUtil;
import com.resolve.util.StringUtils;
import com.resolve.util.JspUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

public class ProgressMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.progress";
    }

    public void execute(Writer writer, MacroParameter params) throws IOException
    {
        RenderContext context = params.getContext();
        WikiRequestContext wikiRequestcontext = (WikiRequestContext) context.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestcontext.getWiki();

        String fullName = (String) context.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        String namespace = (String) context.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) context.get(ConstantValues.WIKI_DOCNAME_KEY);
        if (StringUtils.isEmpty(fullName))
        {
            fullName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
            namespace = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
            wikiDocName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        }
        fullName = HttpUtil.sanitizeValue(fullName, HttpUtil.VALIDATOR_FILENAME,200);
        namespace = HttpUtil.sanitizeValue(namespace, HttpUtil.VALIDATOR_FILENAME,200);
        wikiDocName = HttpUtil.sanitizeValue(wikiDocName, HttpUtil.VALIDATOR_FILENAME,200);

        String img = params.get("text", 0);
        img = HttpUtil.sanitizeValue(img, HttpUtil.VALIDATOR_FILENAME,200);
        int height = Integer.valueOf((StringUtils.isNotEmpty(params.get("height")) ? params.get("height") : "0"));
        int width = Integer.valueOf((StringUtils.isNotEmpty(params.get("width")) ? params.get("width") : "0"));
        int border = Integer.valueOf((StringUtils.isNotEmpty(params.get("border")) ? params.get("border") : "0"));
        String borderColor = StringUtils.isNotEmpty(params.get("borderColor")) ? params.get("borderColor") : "black";
        borderColor = HttpUtil.sanitizeValue(borderColor);
        String displayText = params.get("displayText");
        displayText = HttpUtil.sanitizeValue(displayText);
        String completedText = params.get("completedText");
        completedText = HttpUtil.sanitizeValue(completedText);

        WikiAttachment att = wiki.getWikiStore().getAttachmentFor(fullName, img);
        StringBuffer str = new StringBuffer();

        String url = "/resolve/images/loading.gif";

        // Now write out the image to use the script
        if (att != null)
        {
            String querystring = "attachFileName=" + img + "&attach=" + att.getSys_id();
            url = wiki.getWikiURLFactory().createForwardUrl(WikiAction.download,
					URLEncoder.encode(namespace, StandardCharsets.UTF_8.name()),
					URLEncoder.encode(wikiDocName, StandardCharsets.UTF_8.name()), querystring);
            
			if (StringUtils.isNotEmpty(url)) {
    			String[] urlParams = url.split("\\?");
    			String[] token = JspUtils.getCSRFTokenForPage(wikiRequestcontext.getHttpServletRequest(), urlParams[0]);
    			url = String.join("", urlParams[0], "?", urlParams[1], "&", token[0], "=", token[1]);
			}
        }

        str.append("<img class=\"progressMacro\"");
        str.append("style=\"border:" + border + "px solid " + borderColor + ";");
        if (height > 0)
        {
            str.append("height:" + height + "px;");
        }
        if (width > 0)
        {
            str.append("width:" + width + "px;");
        }
        str.append("\"");
        str.append("src=\"").append(url).append("\" ");
        str.append("alt=\"").append(img).append("\" />\n");
        
        if (StringUtils.isNotBlank(displayText))
        {
            str.append("<span class=\"progressMacro\">" + displayText + "</span>");
        }
        
        if (StringUtils.isNotBlank(completedText))
        {
            str.append("<span class=\"progressCompletedMacro\">" + completedText + "</span>");
        }
        
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    if( window['problemId'] )clientVM.fireEvent('refreshResult');\n");
        str.append("    else if(clientVM.user)clientVM.fireEvent('refreshResult');\n");
        str.append("    else clientVM.on('userChanged', function(){clientVM.fireEvent('refreshResult');})\n");
        str.append("});\n");
        str.append("</script>\n");

        writer.write(str.toString());
    }
}
