/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.store.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.dto.SectionModel;
import com.resolve.dto.SectionType;
import com.resolve.html.HtmlUtil;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.exception.WikiException;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.util.DateUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.helper.SectionHelper;
import com.resolve.wiki.helper.SectionUtil;
import com.resolve.wiki.web.WikiRequestContext;

public class SaveHelper extends AbstractStoreHelper
{
    public String REVIEWED_CHECKBOX_VALUE = "reviewed_checkbox_value";
    
    public final static String FORMNAME = "form";
    public final static String TABLENAME = "table";
    public final static String RELATION = "relation";
    public final static String VIEWNAME = "view";
    public final static String QUERY = "query";

    private String title = null;
    private String content = null;
    private String summary = null;
    private String forwardUrl = "";
    private boolean isWYSIWYG = false;

    private String contentType = SectionType.SOURCE.getTagName();
    private SectionType sectionType = SectionType.SOURCE;
    private String sectionId = null;
    private String sectionName = null;
    private String sectionContent = null;
    private String tags = null;
    private String modelXML = null;
    private String finalXML = null;
    private String exceptionXML = null;
    private String dtreeXML = null;

    private String readRolesStr = null;
    private String writeRolesStr = null;
    private String adminRolesStr = null;
    private String executeRolesStr = null;
    private String defaultRole = null;

    private boolean isReviewed = false;

    private boolean fromEsb = false;

    public SaveHelper(WikiRequestContext wikiRequestContext) throws WikiException
    {
        super(wikiRequestContext);

        sectionId = (String) wikiRequestContext.getAttribute(HibernateConstants.SECTION_ID);
        sectionName = (String) wikiRequestContext.getAttribute(SectionHelper.SECTION_NAME);
        isWYSIWYG = wikiRequestContext.getAttribute(ConstantValues.IS_WYSIWYG_EDIT) != null ? Boolean.parseBoolean((String) wikiRequestContext.getAttribute(ConstantValues.IS_WYSIWYG_EDIT)) : false;

        // if mark deleted, purge the document
        checkIfDocumentMarkedDeleted();

        // get all the parameters
        contentType = wikiRequestContext.getAttribute(HibernateConstants.CONTENT_TYPE) != null ? (String) wikiRequestContext.getAttribute(HibernateConstants.CONTENT_TYPE) : SectionType.SOURCE.getTagName();
        if (StringUtils.isNotEmpty(contentType) && contentType.trim().equals(SectionType.WYSIWYG.getTagName()))
        {
            isWYSIWYG = true;
            sectionType = SectionType.WYSIWYG;
        }

    }// SaveHelper

    /**
     * Saves/updates the wiki doc in the db. If the document is marked deleted,
     * then it will delete that document and create a new one. If the content is
     * coming from WYSIWYG editor, it will pass it through a converter before
     * saving it in the table Once saved, it creates a forward URL and returns
     * it to the client.
     * 
     * @return
     * @throws WikiException
     */
    public Map<String, String> save() throws WikiException
    {
        Map<String, String> result = new HashMap<String, String>();

        sectionContent = prepareSectionContent();
        // if there is no sectionId, then it is ALL the content
        if (StringUtils.isEmpty(sectionId))
        {
            if (wikiAction == WikiAction.save || wikiAction == WikiAction.saveandexit)
            {
                sectionContent = GenericSectionUtil.validateSectionStr(sectionContent, sectionType);
            }

            content = sectionContent;
        }
        else
        {
            content = editSection(sectionId, sectionContent);
        }

        // populate the new values to the wikidoc
        populateWikiDocSave();

        // save the wikidoc
//        wikiStore.saveWikiDocument(wikiRequestContext, wikiDoc);

        // prepare the forward URL
        prepareForwardURL();

        // populate the result map
        result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);

        return result;

    }// save

    /**
     * THis method saves ALL the info of a wikidocument If the document is
     * marked deleted, then it will delete that document and create a new one.
     * If the content is coming from WYSIWYG editor, it will pass it through a
     * converter before saving it in the table Once saved, it creates a forward
     * URL and returns it to the client.
     * 
     * @return
     * @throws WikiException
     */
    public Map<String, String> saveAll(String saveAndCommit) throws WikiException
    {
        Map<String, String> result = new HashMap<String, String>();

        sectionContent = prepareSectionContent();
        // if there is no sectionId, then it is ALL the content
        if (StringUtils.isEmpty(sectionId))
        {
            if (StringUtils.isNotEmpty(sectionContent) && !sectionContent.equals("null"))
            {
                sectionContent = GenericSectionUtil.validateSectionStr(sectionContent, sectionType);
                content = sectionContent;
            }
        }
        else
        {
            content = editSection(sectionId, sectionContent);
        }

        // massage the content
        massageContent();

        title = wikiRequestContext.getAttribute(ConstantValues.WIKI_DOC_TITLE_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOC_TITLE_KEY) : null;
        summary = wikiRequestContext.getAttribute(ConstantValues.WIKI_SUMMARY_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_SUMMARY_KEY) : null;
        fromEsb = wikiRequestContext.getAttribute(ConstantValues.ESB_UPDATE_KEY) != null ? (Boolean) wikiRequestContext.getAttribute(ConstantValues.ESB_UPDATE_KEY) : false;

        tags = wikiRequestContext.getAttribute(ConstantValues.WIKI_TAGS_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_TAGS_KEY) : null;
        modelXML = wikiRequestContext.getAttribute(ConstantValues.MAIN_XML_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.MAIN_XML_KEY) : null;
        finalXML = wikiRequestContext.getAttribute(ConstantValues.FINAL_XML_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.FINAL_XML_KEY) : null;
        exceptionXML = wikiRequestContext.getAttribute(ConstantValues.ABORT_XML_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.ABORT_XML_KEY) : null;
        dtreeXML = wikiRequestContext.getAttribute(ConstantValues.DTREE_XML_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.DTREE_XML_KEY) : null;

        readRolesStr = wikiRequestContext.getAttribute(ConstantValues.READ_ROLES_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.READ_ROLES_KEY) : null;
        writeRolesStr = wikiRequestContext.getAttribute(ConstantValues.WRITE_ROLES_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.WRITE_ROLES_KEY) : null;
        adminRolesStr = wikiRequestContext.getAttribute(ConstantValues.ADMIN_ROLES_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.ADMIN_ROLES_KEY) : null;
        executeRolesStr = wikiRequestContext.getAttribute(ConstantValues.EXECUTE_ROLES_KEY) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.EXECUTE_ROLES_KEY) : null;
        defaultRole = wikiRequestContext.getAttribute(ConstantValues.DEFAULT_ROLE) != null ? (String) wikiRequestContext.getAttribute(ConstantValues.DEFAULT_ROLE) : null;

        isReviewed = wikiRequestContext.getAttribute(REVIEWED_CHECKBOX_VALUE) != null ? ((String) wikiRequestContext.getAttribute(REVIEWED_CHECKBOX_VALUE)).equalsIgnoreCase("true") : false;

        // populate the wikidoc object
        if (fromEsb)
        {
            populateWikiDocUpdate();
        }
        else
        {
            populateWikiDocSaveAll();
        }

        // save the document
        // wikiRequestContext.setAttribute(WikiGUIConstants.WIKI_SAVE_EXIT_AND_COMMIT,
        // saveAndCommit);
//        wikiStore.saveWikiDocument(wikiRequestContext, wikiDoc);

        // prepare the forward URL
        prepareForwardURL();

        // populate the result map
        result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);

        return result;
    }// saveAll

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE METHODS
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void checkIfDocumentMarkedDeleted() throws WikiException
    {
        // if the document is marked deleted, then delete the document before
        // saving it.
        if (wikiDoc.getUIsDeleted())
        {
            // this will be a new doc now.
            wikiDoc.setSys_id(null);

            try
            {
                ServiceWiki.deleteWikiDocument(wikiDoc.getSys_id(), null, user);
            }
            catch (Exception e)
            {
                Log.log.error("Error in purging a deleted documeent - " + wikiDoc.getUFullname(), e);
            }
        }
    }// checkIfDocumentMarkedDeleted

    private void populateWikiDocSave()
    {
        if (wikiAction == WikiAction.save || wikiAction == WikiAction.saveandexit)
        {
            content = evaluateContent(content);
            wikiDoc.setUContent(content);

            if (title != null)
            {
                wikiDoc.setUTitle(title);
            }
            else if (StringUtils.isEmpty(wikiDoc.getUTitle()))
            {
                wikiDoc.setUTitle(wikiDoc.getUFullname());
            }

            // if summary is null or empty, add the first 200 chars of the
            // content in it
            if (summary != null)
            {
                wikiDoc.setUSummary(summary);
            }
            else if (wikiDoc.getUSummary() == null || wikiDoc.getUSummary().trim().length() == 0)
            {
                int totalChars = 200;
                if (content.length() > totalChars)
                {
                    wikiDoc.setUSummary(ParseUtil.removeHTML(content.substring(0, totalChars)));
                }
                else
                {
                    wikiDoc.setUSummary(ParseUtil.removeHTML(content));
                }
            }
            // wikiDoc.setUSummary(summary);
            // wikiDoc.setUTag(tags);
        }
        else if (wikiAction == WikiAction.savemodelc)
        {
            wikiDoc.setUModelProcess(content);
            if (StringUtils.isBlank(content) || !content.contains("<Start"))
            {
                wikiDoc.setUHasActiveModel(new Boolean(false));
                Log.log.info("save model from automation with blank content.");
            }
            else
            {
                wikiDoc.setUHasActiveModel(new Boolean(true));
            }
        }
        else if (wikiAction == WikiAction.saveexcpc)
        {
            wikiDoc.setUModelException(content);
        }
        else if (wikiAction == WikiAction.savefinalc)
        {
            wikiDoc.setUModelFinal(content);
        }
        else if (wikiAction == WikiAction.savedtreec)
        {
            wikiDoc.setUDecisionTree(content);
            if (StringUtils.isEmpty(content) || !content.contains("<Start"))
            {
                wikiDoc.setUIsRoot(new Boolean(false));
                Log.log.info("save decision tree from automation with blank content.");
            }
            else
            {
                wikiDoc.setUIsRoot(new Boolean(true));
            }
        }

        wikiDoc.setUIsDeleted(false);
        wikiDoc.setUIsHidden(false);
        wikiDoc.setUIsLocked(false);
        wikiDoc.setUIsActive(true);
        wikiDoc.setSysUpdatedBy(user);// will need user info here
        if (browserTimeZone != null)
        {
            wikiDoc.setSysUpdatedOn(DateUtils.getCurrentDateInGMT(browserTimeZone, true));
        }
        else
        {
            wikiDoc.setSysUpdatedOn(GMTDate.getDate());
        }

        // this means its a new object/wiki
        if (wikiDoc.getSys_id() == null)
        {
            if (browserTimeZone != null)
            {
                wikiDoc.setSysCreatedOn(DateUtils.getCurrentDateInGMT(browserTimeZone, true));
            }
            else
            {
                wikiDoc.setSysCreatedOn(GMTDate.getDate());
            }
        }
    }// populateWikiDoc

    private void prepareForwardURL()
    {
        String params = "";
        if (StringUtils.isNotEmpty(sectionName))
        {
            params = SectionHelper.SECTION_NAME + "=" + sectionName;
        }

        if (wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY).equals(WikiAction.save))
        {
            forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.edit, namespace, wikiDocName, params);
        }
        else if (wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY).equals(WikiAction.saveandexit))
        {
            forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.view, namespace, wikiDocName, params);
        }
        else if (wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY).equals(WikiAction.savemodelc))
        {
            forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.editmodelc, namespace, wikiDocName, params);
        }
        else if (wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY).equals(WikiAction.saveexcpc))
        {
            forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.editexcpc, namespace, wikiDocName, params);
        }
        else if (wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY).equals(WikiAction.savefinalc))
        {
            forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.editfinalc, namespace, wikiDocName, params);
        }
    }// prepareForwardURL

    private String evaluateContent(String contentToEvaluate)
    {
        String newContent = new String(contentToEvaluate);
        if (isWYSIWYG)
        {
            // tidy up the content
            newContent = Wiki.clean(newContent);

            // if edit is in and Editor, them apply the filter, else save it as
            // Raw data
            newContent = wiki.getWikiConverter().applyHtml2WikiConverter(newContent);
        }

        return newContent;
    }// evaluateContent

    private void populateWikiDocSaveAll()
    {
        wikiDoc.setUContent(content);

        if (title != null)
        {
            wikiDoc.setUTitle(title);
        }
        else if (StringUtils.isEmpty(wikiDoc.getUTitle()))
        {
            wikiDoc.setUTitle(wikiDoc.getUFullname());
        }

        // if summary is null or empty, add the first 200 chars of the content
        // in it
        if (summary != null)
        {
            wikiDoc.setUSummary(summary);
        }
        else if (StringUtils.isEmpty(wikiDoc.getUSummary()))
        {
            int totalChars = 200;
            String summaryText = "";
            if (content.length() > totalChars)
            {
                summaryText = content.substring(0, totalChars);
            }
            else
            {
                summaryText = content;
            }

            summaryText = ParseUtil.removeHTML(summaryText);
            summaryText = ParseUtil.removeWikiTags(summaryText);

            wikiDoc.setUSummary(summaryText);
        }

        wikiDoc.setUTag(tags);
        wikiDoc.setUIsDeleted(false);
        wikiDoc.setUIsHidden(false);
        wikiDoc.setUIsLocked(false);
        wikiDoc.setUIsActive(true);
        wikiDoc.setUModelProcess(modelXML);
        if (StringUtils.isEmpty(modelXML))
        {
            wikiDoc.setUHasActiveModel(new Boolean(false));
        }
        else
        {
            wikiDoc.setUHasActiveModel(new Boolean(true));
        }
        wikiDoc.setUDecisionTree(dtreeXML);
        if (StringUtils.isEmpty(dtreeXML))
        {
            wikiDoc.setUIsRoot(new Boolean(false));
        }
        else
        {
            wikiDoc.setUIsRoot(new Boolean(true));
        }
        wikiDoc.setUModelException(exceptionXML);
        wikiDoc.setUModelFinal(finalXML);
        wikiDoc.setUReadRoles(readRolesStr);
        wikiDoc.setUWriteRoles(writeRolesStr);
        wikiDoc.setUAdminRoles(adminRolesStr);
        wikiDoc.setUExecuteRoles(executeRolesStr);
        wikiDoc.setUIsDefaultRole("true".equals(defaultRole) ? true : false);
        wikiDoc.setSysUpdatedBy(user);// will need user info here
        if (browserTimeZone != null)
        {
            wikiDoc.setSysUpdatedOn(DateUtils.getCurrentDateInGMT(browserTimeZone, true));
        }
        else
        {
            wikiDoc.setSysUpdatedOn(GMTDate.getDate());
        }
        // this means its a new object/wiki
        if (wikiDoc.getSys_id() == null)
        {
            if (browserTimeZone != null)
            {
                wikiDoc.setSysCreatedOn(DateUtils.getCurrentDateInGMT(browserTimeZone, true));
            }
            else
            {
                wikiDoc.setSysCreatedOn(GMTDate.getDate());
            }
        }

        if (isReviewed)
        {
            wikiDoc.setULastReviewedOn(DateUtils.getCurrentDateInGMT(browserTimeZone, true));
            wikiDoc.setUReqestSubmissionOn(null);
            wikiDoc.setUIsRequestSubmission(false);
        }

    }// populateWikiDocSaveAll();

    private void populateWikiDocUpdate()
    {
        if (StringUtils.isEmpty(wikiDoc.getSys_id()) || wikiDoc.getUIsDeleted() || wikiDoc.getUIsHidden() || wikiDoc.getUIsLocked())
        {
            Log.log.error("Document " + wikiDoc.getUFullname() + " is either deleted, hidden, locked or does not exist. SO update FAILED.");

        }
        else
        {
            if (title != null)
            {
                wikiDoc.setUTitle(title);
            }

            if (content != null)
            {
                wikiDoc.setUContent(content);
            }

            if (tags != null)
            {
                wikiDoc.setUTag(tags);
            }

            if (modelXML != null)
            {
                wikiDoc.setUModelProcess(modelXML);
            }

            if (StringUtils.isEmpty(wikiDoc.getUModelProcess()))
            {
                wikiDoc.setUHasActiveModel(new Boolean(false));
            }
            else
            {
                wikiDoc.setUHasActiveModel(new Boolean(true));
            }

            if (dtreeXML != null)
            {
                wikiDoc.setUDecisionTree(dtreeXML);
            }

            if (StringUtils.isEmpty(wikiDoc.getUDecisionTree()))
            {
                wikiDoc.setUIsRoot(new Boolean(false));
            }
            else
            {
                wikiDoc.setUIsRoot(new Boolean(true));
            }

            if (exceptionXML != null)
            {
                wikiDoc.setUModelException(exceptionXML);
            }

            if (readRolesStr != null)
            {
                wikiDoc.setUReadRoles(readRolesStr);
            }

            if (writeRolesStr != null)
            {
                wikiDoc.setUWriteRoles(writeRolesStr);
            }

            if (adminRolesStr != null)
            {
                wikiDoc.setUAdminRoles(adminRolesStr);
            }

            if (executeRolesStr != null)
            {
                wikiDoc.setUExecuteRoles(executeRolesStr);
            }

            if (defaultRole != null)
            {
                wikiDoc.setUIsDefaultRole("true".equals(defaultRole) ? true : false);
            }

            if (browserTimeZone != null)
            {
                wikiDoc.setSysUpdatedOn(DateUtils.getCurrentDateInGMT(browserTimeZone, true));
            }
            else
            {
                wikiDoc.setSysUpdatedOn(GMTDate.getDate());
            }

            wikiDoc.setSysUpdatedBy(user);// will need user info here
        }

    }// populateWikiDocUPdate

    private String editSection(String sectionId, String sectionContent)
    {
        StringBuffer allContent = new StringBuffer();
        String originalContent = wikiDoc.getUContent();

        List<SectionModel> listOfSections = SectionUtil.convertContentToSectionModels(originalContent);

        // update the new section content
        for (SectionModel sm : listOfSections)
        {
            if (sm.getId().equals(sectionId))
            {
                String height = (String) wikiRequestContext.getAttribute(HibernateConstants.IFRAME_HEIGHT);
                height = StringUtils.isNotEmpty(height) ? height.trim() : SectionModel.DEFAULT_IFRAME_HEIGHT;

                sm.setHeight(height);
                SectionUtil.updateSection(sm, sectionContent);
                break;
            }
        }// end of for loop

        // convert the sections back to content string
        allContent.append(SectionUtil.convertSectionModelsToContent(listOfSections));

        return allContent.toString();

    }// editSection

    private String prepareSectionContent()
    {
        StringBuffer contentLocal = new StringBuffer();
        if (contentType.equalsIgnoreCase(SectionType.TABLE.getTagName()))
        {
            // tablename, query, height
            // <iframe src="$url"
            // style="position:absolute;width:99%;height:$height;border:0"></iframe>
            // <iframe
            // src="/resolve/jsp/rsactiontask.jsp?type=table&query=ResolveActionTask.assignedTo.UUserName='admin'"
            // style="position:absolute;width:99%;height:300;border:0"></iframe>
            String tableName = (String) wikiRequestContext.getAttribute(HibernateConstants.GRID_TABLENAME);
            String viewName = (String) wikiRequestContext.getAttribute(HibernateConstants.GRID_VIEWNAME);
            String query = (String) wikiRequestContext.getAttribute(HibernateConstants.GRID_QUERY);
            query = (StringUtils.isNotEmpty(query) ? query.trim() : "");
            String height = (String) wikiRequestContext.getAttribute(HibernateConstants.IFRAME_HEIGHT);
            height = StringUtils.isNotEmpty(height) ? height.trim() : SectionModel.DEFAULT_IFRAME_HEIGHT;

            String url = SectionModel.TABLE_PREFIX + "main=table" + "&" + TABLENAME + "=" + (StringUtils.isNotEmpty(tableName) ? tableName.trim() : "") + "&" + VIEWNAME + "=" + (StringUtils.isNotEmpty(viewName) ? viewName.trim() : "") + "&" + (StringUtils.isNotEmpty(query) ? (query + "&") : "") + QUERY + "=" + query;

            contentLocal.append("<iframe src=\"").append(url).append("\" style=\"position:absolute;width:99%;height:").append(height).append(";border:0\"></iframe>");
        }
        else if (contentType.equalsIgnoreCase(SectionType.RELATIONS.getTagName()))
        {
            /**
             * from Fen
             * /resolve/jsp/rstable.jsp?main=table&table=gateway&sys_id=
             * 2c9181e732b27f790132b2c4e18c00c2
             * &type=relation&relation=gateway_gatewayfilter_rel&control=false
             * table=gateway&sys_id=2c9181e732b27f790132b2c4e18c00c2
             */
            // tablename, query, height
            // <iframe src="$url"
            // style="position:absolute;width:99%;height:$height;border:0"></iframe>
            // <iframe
            // src="/resolve/jsp/rstable.jsp?main=table&type=relation&control=false&table=B&relation=b_c_rel&sys_id=1"
            // style="position:absolute;width:99%;height:300;border:0"></iframe>
            String relTableName = (String) wikiRequestContext.getAttribute(HibernateConstants.RELATIONS_TABLE_NAME);
            String query = (String) wikiRequestContext.getAttribute(HibernateConstants.GRID_QUERY);
            query = (StringUtils.isNotEmpty(query) ? query.trim() : "");
            String height = (String) wikiRequestContext.getAttribute(HibernateConstants.IFRAME_HEIGHT);
            height = StringUtils.isNotEmpty(height) ? height.trim() : SectionModel.DEFAULT_IFRAME_HEIGHT;

            String url = SectionModel.TABLE_PREFIX + "main=table&type=relation&control=false" + "&" + TABLENAME + "=$table" + "&" + "sys_id=$!sys_id" + "&" + RELATION + "=" + (StringUtils.isNotEmpty(relTableName) ? relTableName.trim() : "") + "&" + (StringUtils.isNotEmpty(query) ? (query + "&") : "") + QUERY + "=" + query;
            contentLocal.append("#set($sys_id = $request.getParameter(\"sys_id\"))").append("\n");
            contentLocal.append("#set($table = $request.getParameter(\"table\"))").append("\n");
            contentLocal.append("<iframe src=\"").append(url).append("\" style=\"position:absolute;width:99%;height:").append(height).append(";border:0\"></iframe>");
        }
        else if (contentType.equalsIgnoreCase(SectionType.FORM.getTagName()))
        {
            String serializedViewToFormNames = (String) wikiRequestContext.getAttribute(HibernateConstants.META_FORMNAME);
            String query = (String) wikiRequestContext.getAttribute(HibernateConstants.FORM_QUERY);
            String height = (String) wikiRequestContext.getAttribute(HibernateConstants.IFRAME_HEIGHT);
            height = StringUtils.isNotEmpty(height) ? height.trim() : "0";// SectionModel.DEFAULT_IFRAME_HEIGHT;
            int heightVal = Integer.valueOf(height);

            // Deserialize the view|=|form string
            Map<String, String> viewsToForm = new HashMap<String, String>();
            String[] formNames = serializedViewToFormNames.split("\\|&\\|");
            for (String formNameWithValue : formNames)
            {
                String[] formNameAndValue = formNameWithValue.split("\\|=\\|");
                if (formNameAndValue.length == 2)
                {
                    viewsToForm.put(formNameAndValue[0], formNameAndValue[1]);
                }
            }

            // Configure the velocity scripts for formbuilder
            contentLocal.append(HtmlUtil.createFormVelocityScript(viewsToForm));
            contentLocal.append(HtmlUtil.createFormScript(sectionId + "_formviewer", "$form", null, "$!sys_id", query, heightVal));
        }
        else if (contentType.equalsIgnoreCase(SectionType.REPORTS.getTagName()))
        {
            /*
             * {section:type=REPORTS|title=test|id=rssection_1293309278604|height
             * =300} <iframe src=
             * "/sree/Reports?op=vs&path=/Resolve/Runbook/Daily Execution Summary"
             * style="position:absolute;width:99%;height:300;border:0"></iframe>
             * {section}
             */
            String reportName = (String) wikiRequestContext.getAttribute(HibernateConstants.REPORTS_REPORTNAME);
            String height = (String) wikiRequestContext.getAttribute(HibernateConstants.IFRAME_HEIGHT);
            height = StringUtils.isNotEmpty(height) ? height.trim() : SectionModel.DEFAULT_IFRAME_HEIGHT;

            String url = "/sree/Reports?op=vs&path=/Resolve/";
            if( StringUtils.isNotEmpty(reportName) && !reportName.contains("/") ) {
                url += "Runbook/";
            }
            url += (StringUtils.isNotEmpty(reportName) ? reportName : "");

            contentLocal.append("<iframe src=\"").append(url).append("\" style=\"position:absolute;width:99%;height:").append(height).append(";border:0\"></iframe>");

        }
        else if (contentType.equalsIgnoreCase(SectionType.IMAGE.getTagName()))
        {
            String selectedFileName = wikiRequestContext.getAttribute(HibernateConstants.ATTACH_FILE_NAME).toString();
            int height = Integer.valueOf(wikiRequestContext.getAttribute(HibernateConstants.IMAGE_HEIGHT).toString());
            int width = Integer.valueOf(wikiRequestContext.getAttribute(HibernateConstants.IMAGE_WIDTH).toString());
            String zoom = wikiRequestContext.getAttribute(HibernateConstants.IMAGE_ZOOM).toString();
            contentLocal.append("{image:").append(selectedFileName);
            if (height > 0)
            {
                contentLocal.append("|height=" + height);
            }
            if (width > 0)
            {
                contentLocal.append("|width=" + width);
            }
            if (zoom.equalsIgnoreCase("true"))
            {
                contentLocal.append("|zoom=true");
            }
            else
            {
                contentLocal.append("|zoom=false");
            }
            contentLocal.append("}");

        }
        else if (contentType.equalsIgnoreCase(SectionType.ATTACHMENT.getTagName()))
        {
            String selectedFileName = (String) wikiRequestContext.getAttribute(HibernateConstants.ATTACH_FILE_NAME);
            contentLocal.append("{attach:").append(selectedFileName).append("}");

        }
        else if (contentType.equalsIgnoreCase(SectionType.URL.getTagName()))
        {
            // url, height
            // <iframe src="$url"
            // style="position:absolute;width:99%;height:$height;border:0"></iframe>
            // <iframe src="http://www.yahoo.com"
            // style="position:absolute;width:99%;height:300;border:0"></iframe>
            String url = (String) wikiRequestContext.getAttribute(HibernateConstants.URL);
            String height = (String) wikiRequestContext.getAttribute(HibernateConstants.IFRAME_HEIGHT);
            height = StringUtils.isNotEmpty(height) ? height.trim() : SectionModel.DEFAULT_IFRAME_HEIGHT;

            contentLocal.append("<iframe src=\"").append(url).append("\" style=\"position:absolute;width:99%;height:").append(height).append(";border:0\"></iframe>");
        }
        else if (contentType.equalsIgnoreCase(SectionType.INFOBAR.getTagName()))
        {
            // insert the infobar macro here
            String height = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_HEIGHT);
            String displaySocialTab = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_SOCIAL_TAB);
            String displayFeedbackTab = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_FEEDBACK_TAB);
            String displayRatingTab = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_RATING_TAB);
            String displayAttachmentsTab = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_ATTACHMENTS_TAB);
            String displayHistoryTab = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_HISTORY_TAB);
            String displayTagsTab = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_TAGS_TAB);
            String displayPageInfoTab = (String) wikiRequestContext.getAttribute(HibernateConstants.INFOBAR_DISPLAY_PAGEINFO_TAB);

            contentLocal.append(HtmlUtil.createInfoBarMacro(height, displaySocialTab, displayFeedbackTab, displayRatingTab, displayAttachmentsTab, displayHistoryTab, displayTagsTab, displayPageInfoTab));
        }
        else if( contentType.equalsIgnoreCase(SectionType.TABLEOFCONTENTS.getTagName())) {
            contentLocal.append("#toc()");
        }
        else if (isWYSIWYG)
        {
            contentLocal.append(evaluateContent((String) wikiRequestContext.getAttribute(ConstantValues.WIKI_CONTENT_KEY)));
        }
        else
        // its source
        {
            contentLocal.append((String) wikiRequestContext.getAttribute(ConstantValues.WIKI_CONTENT_KEY));
        }

        return contentLocal.toString();
    }// prepareSectionContent

    private void massageContent()
    {
        if (StringUtils.isNotBlank(this.content))
        {
            content = content.replace("<ul>", "<ul class=\"star\">");
            content = content.replace("<ol>", "<ol class=\"star\">");
        }
    }

}// SaveHelper
