/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

/**
 * TYpes of Request that the wiki application is capable of handling
 * 
 * @author jeet.marwah
 *
 */
public enum WikiRequestType
{
	RPC,//from the RPC call
	JMS,//a JMS call
	URL//from the browser
	;
	
}
