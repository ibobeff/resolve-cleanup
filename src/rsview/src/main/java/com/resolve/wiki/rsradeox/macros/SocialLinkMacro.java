/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

public class SocialLinkMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.sociallink";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        //Get parameters
        String text = StringUtils.isNotEmpty(params.get("text", 0)) ? params.get("text", 0) : "";
        String to = StringUtils.isNotEmpty(params.get("to")) ? params.get("to") : "";
        Boolean openInNewTab = Boolean.valueOf(StringUtils.isNotEmpty(params.get("openInNewTab")) ? params.get("openInNewTab") : "false");
        String preceedingText = StringUtils.isNotEmpty(params.get("preceedingText")) ? params.get("preceedingText") : "";
        //Unique id for the div to render to
        String divId = "sociallink_" + JavaUtils.generateUniqueNumber();

        //Build up div and script to render
        StringBuffer str = new StringBuffer();
        str.append("<div id=\"%1$s\"></div>\n");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.SocialLink',\n");
        str.append("        text : '%2$s',\n");
        str.append("        to : '%3$s',\n");
        str.append("        preceedingText : '%4$s',\n");
        str.append("        openInNewTab : %5$s\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('%1$s');\n");
        str.append("});\n");
        str.append("</script>\n");

        text = HttpUtil.sanitizeMacroValue(text);
        to = HttpUtil.sanitizeMacroValue(to);
        preceedingText = HttpUtil.sanitizeMacroValue(preceedingText);

        String result = String.format(str.toString(), divId, text, to.replace("'", "\\'"),preceedingText, openInNewTab);
        writer.write(result);
    }
}
