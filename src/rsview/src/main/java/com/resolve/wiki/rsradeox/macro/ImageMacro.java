/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This is for the image macro - eg. {image:resolve-systems.gif}
 * 
 * @author jeet.marwah
 * 
 */
public class ImageMacro extends BaseLocaleMacro
{

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        RenderContext context = params.getContext();
        WikiRequestContext wikiRequestcontext = (WikiRequestContext) context.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestcontext.getWiki();

        String fullName = (String) context.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        String namespace = (String) context.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) context.get(ConstantValues.WIKI_DOCNAME_KEY);
        if (StringUtils.isEmpty(fullName))
        {
            fullName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
            namespace = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
            wikiDocName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        }
        fullName = HttpUtil.sanitizeValue(fullName);
        namespace = HttpUtil.sanitizeValue(namespace);
        wikiDocName = HttpUtil.sanitizeValue(wikiDocName);

        String img = params.get("text", 0);
        img = HttpUtil.sanitizeValue(img, HttpUtil.VALIDATOR_FILENAME, 200, HttpUtil.HTML_ENCODING);
        String height = params.get("height");
        height = HttpUtil.sanitizeValue(height);
        String width = params.get("width");
        width = HttpUtil.sanitizeValue(width);
        String border = params.get("border") != null ? params.get("border") : "0";
        border = HttpUtil.sanitizeValue(border);
        boolean zoom = params.get("zoom") != null ? Boolean.valueOf(params.get("zoom")) : false;

        Log.log.trace("***************** IMAGE MACRO******************");
        Log.log.trace("doc fullname: " + fullName);
        Log.log.trace("doc namespace: " + namespace);
        Log.log.trace("doc wikiDocName: " + wikiDocName);
        Log.log.trace("doc img: " + img);

//        WikiAttachment att = wiki.getWikiStore().getAttachmentFor(fullName, img);
        WikiAttachmentVO att = null;
        
        try
        {
            att = ServiceWiki.findWikiAttachmentVO(fullName, null, img, false);
        }
        catch (Exception e)
        {
            Log.log.error("error getting the attachment " + img + " for document " + fullName, e);
        }
        
        StringBuffer str = new StringBuffer();
        if (att != null)
        {
            String querystring = ConstantValues.WIKI_PARAMETER_ATTACHMENT_FILE_NAME + "=" + img + "&" + ConstantValues.WIKI_PARAMETER_ATTACHMENT_SYS_ID + "=" + att.getSys_id();
            String url = wiki.getWikiURLFactory().createForwardUrl(WikiAction.download, namespace, wikiDocName, querystring);

            str.append("<img ");
            if (zoom)
            {
                str.append("onclick=\"javascript:displayLargeImage('" + url + "', '" + img + "')\" ");
            }
            str.append("border=\"" + border + "\" ");
            str.append("src=\"");
            str.append(url);
            str.append("\" ");
            if ((!"none".equals(height)) && (height != null) && (!"".equals(height.trim())))
            {
                str.append("height=\"" + height.trim() + "\" ");
            }
            if ((!"none".equals(width)) && (width != null) && (!"".equals(width.trim())))
            {
                str.append("width=\"" + width.trim() + "\" ");
            }
            str.append("alt=\"");
            str.append(img);
            str.append("\" />");
        }
        else
        {
            str.append("<b>Error: ");
            str.append(img);
            str.append(" image file does not exist.</b>");
        }

        Log.log.trace(str);
        writer.write(str.toString());
    }

    @Override
    public String getLocaleKey()
    {
        return "macro.image";
    }

}
