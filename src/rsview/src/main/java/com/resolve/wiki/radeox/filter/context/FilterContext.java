/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.context;

import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

/**
 * FilterContext stores basic data for the context
 * filters are called in. FilterContext is used to allow
 * filters be called in different enviroments, inside
 */

public interface FilterContext {
  public MacroParameter getMacroParameter();
  public void setRenderContext(RenderContext context);
  public RenderContext getRenderContext();
}
