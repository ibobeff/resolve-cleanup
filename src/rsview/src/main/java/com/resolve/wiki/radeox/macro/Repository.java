/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import java.util.List;

/**
 * Repository for Macros
 *
 */

public interface Repository
{
    public boolean containsKey(String key);

    public Object get(String key);

    public List getPlugins();

    public void put(String key, Object value);
}
