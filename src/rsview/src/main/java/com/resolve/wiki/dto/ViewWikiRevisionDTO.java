/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.dto;

public class ViewWikiRevisionDTO
{
    private String docSysId;
//    private String docFullname;
    private Integer revision;
    private String comment;
    private boolean committedByUser;
    
    private String content;
    private String mainModel;
    private String abortModel;
    private String decisionTreeModel;
    
    public String getDocSysId()
    {
        return docSysId;
    }
    public void setDocSysId(String docSysId)
    {
        this.docSysId = docSysId;
    }
//    public String getDocFullname()
//    {
//        return docFullname;
//    }
//    public void setDocFullname(String docFullname)
//    {
//        this.docFullname = docFullname;
//    }
    public Integer getRevision()
    {
        return revision;
    }
    public void setRevision(Integer revision)
    {
        this.revision = revision;
    }
    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }
    public boolean isCommittedByUser()
    {
        return committedByUser;
    }
    public void setCommittedByUser(boolean committedByUser)
    {
        this.committedByUser = committedByUser;
    }
    public String getMainModel()
    {
        return mainModel;
    }
    public void setMainModel(String mainModel)
    {
        this.mainModel = mainModel;
    }
    public String getAbortModel()
    {
        return abortModel;
    }
    public void setAbortModel(String abortModel)
    {
        this.abortModel = abortModel;
    }
    public String getDecisionTreeModel()
    {
        return decisionTreeModel;
    }
    public void setDecisionTreeModel(String decisionTreeModel)
    {
        this.decisionTreeModel = decisionTreeModel;
    }
    
    
    
    
    
    
    

}
