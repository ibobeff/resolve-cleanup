/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro.code;

/*
 * JavaCodeFilter colourizes Java source code
 *
 * 
 */

public class JavaCodeFormatter extends DefaultRegexCodeFormatter implements SourceCodeFormatter
{

	private static final String KEYWORDS = "\\b(abstract|break|byvalue|case|cast|catch|" + "const|continue|default|do|else|extends|"
			+ "false|final|finally|for|future|generic|goto|if|" + "implements|import|inner|instanceof|interface|"
			+ "native|new|null|operator|outer|package|private|" + "protected|public|rest|return|static|super|switch|"
			+ "synchronized|this|throw|throws|transient|true|try|" + "var|volatile|while)\\b";

	private static final String OBJECTS = "\\b(Boolean|Byte|Character|Class|ClassLoader|Cloneable|Compiler|"
			+ "Double|Float|Integer|Long|Math|Number|Object|Process|" + "Runnable|Runtime|SecurityManager|Short|String|StringBuffer|"
			+ "System|Thread|ThreadGroup|Void|boolean|char|byte|short|int|long|float|double)\\b";

	private static final String QUOTES = "\"(([^\"\\\\]|\\.)*)\"";

	public JavaCodeFormatter()
	{
		super(QUOTES, "<span class=\"java-quote\">\"$1\"</span>");
		addRegex(KEYWORDS, "<span class=\"java-keyword\">$1</span>");
		addRegex(OBJECTS, "<span class=\"java-object\">$1</span>");
	}

	public String getName()
	{
		return "java";
	}
}