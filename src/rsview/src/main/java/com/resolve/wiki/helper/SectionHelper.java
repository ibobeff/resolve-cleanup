/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.helper;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.dto.GenericSectionModel;
import com.resolve.dto.SectionModel;
import com.resolve.dto.SectionType;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.wiki.DocUtils;
import com.resolve.services.hibernate.wiki.WikiDocLock;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.rsradeox.macro.SectionMacro;
import com.resolve.wiki.store.helper.SaveHelper;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * class for Section Edit
 */

public class SectionHelper
{

    public static final String SECTION_NAME = "section";
    public static final String SECTION_ID = "sectionid";
    public static final String UNLOCK_DOC_FLAG = "unlock";

    private Wiki wiki = null;
    private WikiRequestContext wikiRequestContext = null;

    private Map<String, String> result = new HashMap<String, String>();

    private String docName = null;
    private String namespace = null;
    private String wikiDocName = null;
    private WikiAction wikiAction = null;
    private String user = null;

    private boolean userHasAdminRole = false;
    private WikiDocument wikiDoc = null;
    private String wikiContent = null;
    private String strSave = null;
    private String strSaveAndExit = null;
    private String forwardUrl = null;

    private String section_name = null;
    private String section_id = null;

    private SectionModel selectedSectionToEdit = null;
    private List<SectionModel> listOfSectionsFromUI = null;
    private List<SectionModel> listOfSectionsFromDB = null;

    private boolean unlock = false;

    public SectionHelper(WikiRequestContext wikiRequestContext) throws WikiException
    {
        this.wiki = wikiRequestContext.getWiki();
        this.wikiRequestContext = wikiRequestContext;

        docName = wiki.getDocumentName(wikiRequestContext);
        namespace = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
        wikiDocName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        wikiAction = (WikiAction) wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY);
        user = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        unlock = wikiRequestContext.getAttribute(UNLOCK_DOC_FLAG) != null && ((String) wikiRequestContext.getAttribute(UNLOCK_DOC_FLAG)).equalsIgnoreCase("true") ? true : false;

        // get the list of sections
        section_name = wikiRequestContext.getAttribute(SECTION_NAME) != null ? (String) wikiRequestContext.getAttribute(SECTION_NAME) : null;
        section_id = wikiRequestContext.getAttribute(SECTION_ID) != null ? (String) wikiRequestContext.getAttribute(SECTION_ID) : null;

        // set the flag if the user has admin role
        userHasAdminRole = doesUserHasAdminRole();

        // do the basic validation
        basicValidation();

        // load the document
        loadDocument();

        // validate the document
        validateDocument();
    }

    /**
     * from the controller
     * 
     * section=<section name> OR sectionid =<sectoin id> if type is WYSIWYG,
     * then show the editor, else just a textarea
     * 
     * 
     * @return
     * @throws WikiException
     */
    private Map<String, String> getSectionToEditFromURL() throws WikiException
    {

        // means that all the validation are fine and there are no errors
        if (result.size() == 0)
        {
            // populate the wikiContent with DB
            wikiContent = StringUtils.isNotEmpty(wikiDoc.getUContent()) ? wikiDoc.getUContent() : "";

            // get the list of sections from DB
            listOfSectionsFromDB = SectionUtil.convertContentToSectionModels(wikiContent);

            // find the section
            SectionModel sectionToEdit = null;
            if (StringUtils.isNotEmpty(section_id))
            {
                sectionToEdit = findSectionContent(section_id);
            }
            else if (StringUtils.isNotEmpty(section_name))
            {
                sectionToEdit = findSectionContent(section_name);
            }

            // convert to map
            if (sectionToEdit != null)
            {
                // prepare the urls
                prepareUrls();

                result.putAll(convertModelToMap(sectionToEdit));

                result.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
                result.put(ConstantValues.WIKI_DOCNAME_KEY, wikiDocName);
                result.put(ConstantValues.WIKI_DOC_FULLNAME_KEY, wikiDoc.getUFullname());
                result.put(ConstantValues.WIKI_SAVE_LINK_KEY, strSave);

            }
        }

        return result;
    }

    public Map<String, String> getSectionToEdit() throws WikiException
    {
        // means that all the validation are fine and there are no errors
        if (result.size() == 0)
        {
            // populate the wikiContent with DB
            wikiContent = StringUtils.isNotEmpty(wikiDoc.getUContent()) ? wikiDoc.getUContent() : "";

            // get the list of sections
            listOfSectionsFromUI = (List<SectionModel>) wikiRequestContext.getAttribute(ConstantValues.LIST_OF_SECTIONS_FROM_UI);
            selectedSectionToEdit = (SectionModel) wikiRequestContext.getAttribute(ConstantValues.SELECTED_SECTION_TO_EDIT);

            // get the list of sections from DB
            listOfSectionsFromDB = SectionUtil.convertContentToSectionModels(wikiContent);

            // move the content from DB to UI as rest of the data is suppose to
            // be deleted
            populateUIListWithContent();

            // reset the models as the title can be changed
            resetAllSections();

            // update the wikicontent based on the user edit
            wikiContent = SectionUtil.convertSectionModelsToContent(listOfSectionsFromUI);
            wikiDoc.setUContent(wikiContent);
//            wiki.getWikiStore().saveWikiDocument(wikiRequestContext, wikiDoc);

            // get the selected model from the UI list
            selectedSectionToEdit = SectionUtil.getSelectedSectionToEdit(selectedSectionToEdit, listOfSectionsFromUI);

            // if the selected section is null, then edit the WHOLE document
            String contentToEdit = wikiContent;
            String contentType = SectionType.SOURCE.getTagName();
            String id = "";
            String title = "";
            if (selectedSectionToEdit != null)
            {
                contentType = selectedSectionToEdit.getType();
                contentToEdit = SectionUtil.getContent(selectedSectionToEdit.getContent());
                id = selectedSectionToEdit.getId();
                title = selectedSectionToEdit.getTitle();

                // if WYSIWYG, then use converter
                if (selectedSectionToEdit.getType().equals(SectionType.WYSIWYG.getTagName()))
                {
                    // if editing in Editor, then apply the filter, else show
                    // the raw content
                    contentToEdit = wiki.getWikiConverter().applyWiki2HtmlConverter(contentToEdit, wikiRequestContext);
                }
                else if (contentType.equalsIgnoreCase(SectionType.TABLE.getTagName()))
                {
                    // tablename, query, height
                    /*
                     * {section:type=TABLE|title=tableEx|id=rssection_1313457244082
                     * |height=300} <iframe src=
                     * "/resolve/jsp/rstable.jsp?main=table&table=cust_table_6&view=Default&query="
                     * style
                     * ="position:absolute;width:99%;height:300;border:0"></
                     * iframe> {section}
                     */

                    String height = getIframeHeight(contentToEdit);
                    String url = getUrl(contentToEdit);
                    Map<String, String> urlParams = getUrlParams(url);

                    result.put(HibernateConstants.GRID_TABLENAME, urlParams.get(SaveHelper.TABLENAME));
                    result.put(HibernateConstants.GRID_VIEWNAME, urlParams.get(SaveHelper.VIEWNAME));
                    result.put(HibernateConstants.GRID_QUERY, urlParams.get(SaveHelper.QUERY));
                    result.put(HibernateConstants.IFRAME_HEIGHT, height);

                }
                else if (contentType.equalsIgnoreCase(SectionType.RELATIONS.getTagName()))
                {
                    /**
                     * from Fen
                     * /resolve/jsp/rstable.jsp?main=table&table=gateway
                     * &sys_id=2
                     * c9181e732b27f790132b2c4e18c00c2&type=relation&relation
                     * =gateway_gatewayfilter_rel&control=false
                     */
                    // tablename, query, height
                    // <iframe src="$url"
                    // style="position:absolute;width:99%;height:$height;border:0"></iframe>
                    // <iframe
                    // src="/resolve/jsp/rstable.jsp?main=table&type=relation&control=false&table=B&relation=b_c_rel&sys_id=1"
                    // style="position:absolute;width:99%;height:300;border:0"></iframe>
                    if (StringUtils.isNotEmpty(contentToEdit) && contentToEdit.indexOf("<iframe") > -1)
                    {
                        String iframe = contentToEdit.substring(contentToEdit.indexOf("<iframe"), contentToEdit.indexOf("</iframe>") + "</iframe>".length());
                        String height = getIframeHeight(iframe);
                        String url = getUrl(iframe);
                        Map<String, String> urlParams = getUrlParams(url);

                        result.put(HibernateConstants.RELATIONS_TABLE_NAME, urlParams.get(SaveHelper.TABLENAME));
                        result.put(HibernateConstants.GRID_QUERY, urlParams.get(SaveHelper.QUERY));
                        result.put(HibernateConstants.IFRAME_HEIGHT, height);
                    }
                }
                else if (contentType.equalsIgnoreCase(SectionType.FORM.getTagName()))
                {
                    // Find all the if($view=="viewName") and $form="formName"
                    // instances and set those up for the editing of the section
                    if (StringUtils.isNotEmpty(contentToEdit))
                    {
                        Map<String, String> viewToFormNames = new HashMap<String, String>();

                        Pattern viewPattern = Pattern.compile("\\$view==\"[\\w]+\"");
                        Matcher viewMatches = viewPattern.matcher(contentToEdit);

                        Pattern formPattern = Pattern.compile("\\$form=\"[\\w]+\"");
                        Matcher formMatches = formPattern.matcher(contentToEdit);

                        ArrayList<String> views = new ArrayList<String>();
                        while (viewMatches.find())
                        {
                            views.add(viewMatches.group(0).substring(8, viewMatches.group(0).length() - 1));
                        }

                        ArrayList<String> forms = new ArrayList<String>();
                        while (formMatches.find())
                        {
                            String temp = formMatches.group(0);
                            int count = formMatches.groupCount();
                            forms.add(formMatches.group(0).substring(7, formMatches.group(0).length() - 1));
                        }

                        // At this point we should have forms and views and the
                        // views should be one less than the forms because there
                        // is a default form in the else block
                        // Thankfully, the default form will always be the
                        // "last" form we have
                        for (int i = 0; i < views.size() && i < forms.size(); i++)
                        {
                            viewToFormNames.put(views.get(i), forms.get(i));
                        }

                        viewToFormNames.put("Default", forms.size() > 0 ? forms.get(forms.size() - 1) : "");

                        StringBuffer meta_formName = new StringBuffer();
                        for (String key : viewToFormNames.keySet())
                        {
                            if (meta_formName.length() > 0) meta_formName.append("|&|");
                            meta_formName.append(key + "|=|" + viewToFormNames.get(key));
                        }

                        String temp = contentToEdit.substring(contentToEdit.indexOf("formName") > -1 ? contentToEdit.indexOf("formName") : 0);
                        temp = temp.substring(temp.indexOf("'") > -1 ? temp.indexOf("'") : 0);
                        temp = temp.substring(0, temp.indexOf(",") > -1 ? temp.indexOf(",") : 0);
                        temp = temp.trim();
                        if (temp.startsWith("'")) temp = temp.substring(1);
                        if (temp.endsWith("'")) temp = temp.substring(0, temp.length() - 1);
                        // Should be the formName at this point
                        temp = temp.replace("\\", "");
                        result.put(HibernateConstants.META_FORMNAME, meta_formName.toString());
                    }

                    // Find the query: 'query text' in the content json
                    if (StringUtils.isNotEmpty(contentToEdit) && contentToEdit.indexOf("queryString") > -1)
                    {
                        String temp = contentToEdit.substring(contentToEdit.indexOf("queryString"));
                        temp = temp.substring(temp.indexOf("'"));
                        temp = temp.substring(0, temp.indexOf(","));
                        temp = temp.trim();
                        if (temp.startsWith("'")) temp = temp.substring(1);
                        if (temp.endsWith("'")) temp = temp.substring(0, temp.length() - 1);
                        // Should be the formName at this point
                        temp = temp.replace("\\", "");
                        result.put(HibernateConstants.FORM_QUERY, temp);
                    }

                    // Legacy code
                    if (StringUtils.isNotEmpty(contentToEdit) && contentToEdit.indexOf("<iframe") > -1)
                    {
                        String iframe = contentToEdit.substring(contentToEdit.indexOf("<iframe"), contentToEdit.indexOf("</iframe>") + "</iframe>".length());
                        String height = getIframeHeight(iframe);
                        String url = getUrl(iframe);
                        Map<String, String> urlParams = getUrlParams(url);

                        result.put(HibernateConstants.META_FORMNAME, urlParams.get(SaveHelper.FORMNAME));
                        result.put(HibernateConstants.FORM_TABLENAME, urlParams.get(SaveHelper.TABLENAME));
                        result.put(HibernateConstants.FORM_VIEWNAME, urlParams.get(SaveHelper.VIEWNAME));
                        result.put(HibernateConstants.FORM_QUERY, urlParams.get(SaveHelper.QUERY));
                        result.put(HibernateConstants.IFRAME_HEIGHT, height);
                    }

                }
                else if (contentType.equalsIgnoreCase(SectionType.REPORTS.getTagName()))
                {
                    /*
                     * {section:type=REPORTS|title=test|id=rssection_1293309278604
                     * |height=300} <iframe src=
                     * "/sree/Reports?op=vs&path=/Resolve/Runbook/Daily Execution Summary"
                     * style
                     * ="position:absolute;width:99%;height:300;border:0"></
                     * iframe> {section}
                     */
                    if (StringUtils.isNotEmpty(contentToEdit) && contentToEdit.indexOf("<iframe") > -1)
                    {
                        String iframe = contentToEdit.substring(contentToEdit.indexOf("<iframe"), contentToEdit.indexOf("</iframe>") + "</iframe>".length());
                        String height = getIframeHeight(iframe);
                        String url = getUrl(iframe);
                        String reportPath = url.substring(0, url.lastIndexOf("/"));
                        reportPath = reportPath.substring(reportPath.lastIndexOf("/")+1) + "/";
                        if( reportPath.equalsIgnoreCase("Runbook/") ) {
                            reportPath= "";
                        }
                        String reportName = reportPath + url.substring(url.lastIndexOf("/") + 1);

                        result.put(HibernateConstants.REPORTS_REPORTNAME, reportName);
                        result.put(HibernateConstants.IFRAME_HEIGHT, height);
                    }

                }
                else if (contentType.equalsIgnoreCase(SectionType.IMAGE.getTagName()))
                {
                    // {image:Oracle.jpg|width=50}
                    // {image:Oracle.jpg}
                    String selectedFileName = "";
                    if (StringUtils.isNotEmpty(contentToEdit))
                    {
                        contentToEdit = contentToEdit.trim();
                        if (contentToEdit.startsWith("{image"))
                        {
                            if (contentToEdit.indexOf("|") > -1)
                            {
                                selectedFileName = contentToEdit.substring(contentToEdit.indexOf(":") + 1, contentToEdit.indexOf("|"));
                            }
                            else
                            {
                                selectedFileName = contentToEdit.substring(contentToEdit.indexOf(":") + 1, contentToEdit.lastIndexOf("}"));
                            }
                        }
                    }// end of if

                    result.put(HibernateConstants.ATTACH_FILE_NAME, selectedFileName);
                }
                else if (contentType.equalsIgnoreCase(SectionType.ATTACHMENT.getTagName()))
                {
                    /*
                     * {attach:sqls.sql}
                     */
                    String selectedFileName = "";
                    if (StringUtils.isNotEmpty(contentToEdit))
                    {
                        contentToEdit = contentToEdit.trim();
                        if (contentToEdit.startsWith("{attach"))
                        {
                            selectedFileName = contentToEdit.substring(contentToEdit.indexOf(":") + 1, contentToEdit.lastIndexOf("}"));
                        }
                    }// end of if

                    result.put(HibernateConstants.ATTACH_FILE_NAME, selectedFileName);

                }
                else if (contentType.equalsIgnoreCase(SectionType.URL.getTagName()))
                {
                    // url, height
                    // <iframe src="$url"
                    // style="position:absolute;width:99%;height:$height;border:0"></iframe>
                    // <iframe src="http://www.yahoo.com"
                    // style="position:absolute;width:99%;height:300;border:0"></iframe>
                    String height = getIframeHeight(contentToEdit);
                    String url = getUrl(contentToEdit);

                    result.put(HibernateConstants.URL, url);
                    result.put(HibernateConstants.IFRAME_HEIGHT, height);

                }
            }// end of if

            // prepare the urls
            prepareUrls();

            // prepare the result map
            result.put(ConstantValues.WIKI_CONTENT_KEY, contentToEdit);
            result.put(HibernateConstants.CONTENT_TYPE, contentType);
            result.put(HibernateConstants.SECTION_ID, id);
            result.put(HibernateConstants.SECTION_TITLE, title);
            result.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
            result.put(ConstantValues.WIKI_DOCNAME_KEY, wikiDocName);
            result.put(ConstantValues.WIKI_ADMIN_KEY, userHasAdminRole + "");
            result.put(ConstantValues.WIKI_SUMMARY_KEY, wikiDoc.getUSummary());
            result.put(ConstantValues.WIKI_TAGS_KEY, wikiDoc.getUTag());
            result.put(ConstantValues.WIKI_CREATEDBY_KEY, wikiDoc.getSysCreatedBy());
            result.put(ConstantValues.WIKI_SAVE_LINK_KEY, strSave);
            result.put(ConstantValues.WIKI_SAVEANDEXIT_LINK_KEY, strSaveAndExit);
            result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
            result.put(Constants.HTTP_REQUEST_USERNAME, user);
        }

        return result;

    }// getSectionToEdit

    public Map<String, String> getSourceContentToEdit() throws WikiException
    {
        // means that all the validation are fine and there are no errors
        if (result.size() == 0)
        {
            if (StringUtils.isNotEmpty(section_id) || (StringUtils.isNotEmpty(section_name)))
            {
                getSectionToEditFromURL();// this will populate the values in
                                          // the map
            }
            else
            {

                wikiContent = StringUtils.isNotEmpty(wikiDoc.getUContent()) ? wikiDoc.getUContent() : "";
                String contentToEdit = "";

                if (wikiAction == WikiAction.edit || wikiAction == WikiAction.view || wikiAction == WikiAction.viewsrc)
                {
                    contentToEdit = GenericSectionUtil.validateSectionStr(wikiContent, null);
                }
                else if (wikiAction == WikiAction.editmodelc || wikiAction == WikiAction.viewmodelc)
                {
                    contentToEdit = wikiDoc.getUModelProcess();
                }
                else if (wikiAction == WikiAction.editexcpc || wikiAction == WikiAction.viewexcpc)
                {
                    contentToEdit = wikiDoc.getUModelException();
                }
                else if (wikiAction == WikiAction.editfinalc || wikiAction == WikiAction.viewfinalc)
                {
                    contentToEdit = wikiDoc.getUModelFinal();
                }

                // prepare the urls
                prepareUrls();

                // prepare the result map
                result.put(ConstantValues.WIKI_CONTENT_KEY, contentToEdit);
                result.put(HibernateConstants.CONTENT_TYPE, SectionType.SOURCE.getTagName());
                result.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
                result.put(ConstantValues.WIKI_DOCNAME_KEY, wikiDocName);
                result.put(ConstantValues.WIKI_ADMIN_KEY, userHasAdminRole + "");
                result.put(ConstantValues.WIKI_SUMMARY_KEY, wikiDoc.getUSummary());
                result.put(ConstantValues.WIKI_TAGS_KEY, wikiDoc.getUTag());
                result.put(ConstantValues.WIKI_CREATEDBY_KEY, wikiDoc.getSysCreatedBy());
                result.put(ConstantValues.WIKI_SAVE_LINK_KEY, strSave);
                result.put(ConstantValues.WIKI_SAVEANDEXIT_LINK_KEY, strSaveAndExit);
                result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
                result.put(Constants.HTTP_REQUEST_USERNAME, user);

            }
        }// end of if

        return result;
    }// getContentToEdit

    public GenericSectionModel getSections() throws WikiException
    {
        GenericSectionModel genericSectionModel = new GenericSectionModel();

        if (result.isEmpty())
        {
            genericSectionModel.listOfSections = new ArrayList<SectionModel>(SectionUtil.convertContentToSectionModels(this.wikiDoc.getUContent()));
        }
        else
        {
            // there is an error
            genericSectionModel.mapOfString = new HashMap<String, String>(result);
        }

        return genericSectionModel;
    }// getSections

    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE METHODS
    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private boolean doesUserHasAdminRole()
    {
        return ServiceHibernate.isAdminUser(user);
    }// doesUserHasAdminRole

    private void basicValidation() throws WikiException
    {
        if (StringUtils.isEmpty(user))
        {
            throw new WikiException(new Exception("User info cannot be empty. Please verify."));
        }

        // validate the namespace
        validateNamespace();

        // if there is no error yet, check if the user has access right to do
        // this
        checkUserAccessRights();

    }// basicValidation

    private void validateNamespace() throws WikiException
    {
        if (result.size() == 0)
        {
            if (StringUtils.isEmpty(namespace))
            {
                String message = "The document you are trying to edit is either a System document or you don't have access rights to it. Please select other document to edit.";

                result.putAll(wiki.getUserHomepage(wikiRequestContext));
                result.put(ConstantValues.ERROR_EXCEPTION_KEY, message);
            }
        }
    }// validateNamespace

    private void checkUserAccessRights()
    {
        if (result.size() == 0)
        {
            try
            {
//                userHasAdminRole = wiki.getWikiRightService().checkAccess(docName, wikiRequestContext);
                userHasAdminRole = ServiceWiki.hasRightsToDocument(docName, user, RightTypeEnum.edit);
                if(!userHasAdminRole)
                {
                    String error = "User '" + user + "' does not have '" + "edit" + "' right on '" + docName + "' document.";
                    throw new WikiException(error);
                }
            }
            catch (WikiException e)
            {
                String forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.view, namespace, wikiDocName, null);
                result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
                result.put(ConstantValues.ERROR_EXCEPTION_KEY, e.getGUIMessage());
            }
        }
    }// checkUserAccessRights();

    private void loadDocument() throws WikiException
    {
        if (result.size() == 0)
        {
            wikiDoc = wiki.loadDocument(docName, true);
        }
    }// loadDocument

    private void validateDocument()
    {
        if (result.size() == 0)
        {
            if (wikiAction == WikiAction.edit || wikiAction == WikiAction.editmodelc || wikiAction == WikiAction.editexcpc || wikiAction == WikiAction.editfinalc)
            {
                // check if this document is LOCKED by any other user. If not,
                // then proceed and let this user EDIT the doc
                WikiDocLock lock = DocUtils.getWikiDocLockNonBlocking(docName);
                String message = null;
                String forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.view, namespace, wikiDocName, null);

                if (lock != null)
                {
                    if (unlock)
                    {
                        DocUtils.unlock(docName);
                        DocUtils.lock(user, docName);
                    }
                    else
                    {
                        if (!lock.getUsername().equals(user))// if its not the
                                                             // same user, then
                                                             // throw exception
                        {
                            if (userHasAdminRole)
                            {
                                message = "The document '" + docName + "' is currently used/locked by '" + lock.getUsername() + "'. Would you like to UnLock this document?.";
                            }
                            else
                            {
                                message = "The document '" + docName + "' is currently used/locked by '" + lock.getUsername() + "'. So you cannot edit this document.";
                            }

                            result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
                            result.put(ConstantValues.WIKI_ADMIN_KEY, userHasAdminRole + "");
                            result.put(ConstantValues.WIKI_IS_LOCKED, "yes");
                            result.put(ConstantValues.ERROR_EXCEPTION_KEY, message);
                        }
                    }
                }
                else
                {
                    if (wikiDoc.ugetUIsLocked())
                    {
                        if (userHasAdminRole)
                        {
                            message = "The document '" + docName + "' is currently locked. Would you like to UnLock this document?.";
                        }
                        else
                        {
                            message = "The document '" + docName + "' is currently locked. So you cannot edit this document.";
                        }

                        result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
                        result.put(ConstantValues.WIKI_IS_LOCKED, "yes");
                        result.put(ConstantValues.WIKI_ADMIN_KEY, userHasAdminRole + "");
                        result.put(ConstantValues.ERROR_EXCEPTION_KEY, message);

                    }
                    else if (wikiDoc.ugetUIsDeleted())
                    {
                        if (userHasAdminRole)
                        {
                            message = "The document '" + docName + "' is marked DELETED. Would you like to UnDelete this document?";
                        }
                        else
                        {
                            message = "The document '" + docName + "' is marked DELETED. So you cannot edit this document.";
                        }

                        result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
                        result.put(ConstantValues.WIKI_IS_DELETED, "yes");
                        result.put(ConstantValues.WIKI_ADMIN_KEY, userHasAdminRole + "");
                        result.put(ConstantValues.ERROR_EXCEPTION_KEY, message);

                    }
                    else if (wikiDoc.ugetUIsHidden())
                    {
                        if (userHasAdminRole)
                        {
                            message = "The document '" + docName + "' is marked HIDDEN. Would you like to UnHide this document?.";
                        }
                        else
                        {
                            message = "The document '" + docName + "' is marked HIDDEN. So you cannot edit this document.";
                        }

                        result.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
                        result.put(ConstantValues.WIKI_IS_HIDDEN, "yes");
                        result.put(ConstantValues.WIKI_ADMIN_KEY, userHasAdminRole + "");
                        result.put(ConstantValues.ERROR_EXCEPTION_KEY, message);

                    }
                    else
                    {
                        DocUtils.lock(user, docName);// lock the document
                    }
                }
            }
        }
    }// validateDocument

    private void prepareUrls()
    {
        if (result.size() == 0)
        {
            URL urlSave = null;
            URL urlSaveAndExit = null;

            Map<String, String> values = new HashMap<String, String>();
            values.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
            values.put(ConstantValues.WIKI_DOCNAME_KEY, wikiDocName);
            values.put(ConstantValues.WIKI_ADMIN_KEY, userHasAdminRole + "");

            if (wikiAction == WikiAction.edit)
            {
                urlSave = wiki.getWikiURLFactory().createURL(values, wikiRequestContext, WikiAction.save.toString(), null, null);
                urlSaveAndExit = wiki.getWikiURLFactory().createURL(values, wikiRequestContext, WikiAction.saveandexit.toString(), null, null);
            }
            else if (wikiAction == WikiAction.editmodelc)
            {
                urlSave = wiki.getWikiURLFactory().createURL(values, wikiRequestContext, WikiAction.savemodelc.toString(), null, null);
                urlSaveAndExit = urlSave;
            }
            else if (wikiAction == WikiAction.editexcpc)
            {
                urlSave = wiki.getWikiURLFactory().createURL(values, wikiRequestContext, WikiAction.saveexcpc.toString(), null, null);
                urlSaveAndExit = urlSave;
            }
            else if (wikiAction == WikiAction.editfinalc)
            {
                urlSave = wiki.getWikiURLFactory().createURL(values, wikiRequestContext, WikiAction.savefinalc.toString(), null, null);
                urlSaveAndExit = urlSave;
            }

            strSave = wiki.getWikiURLFactory().getURL(urlSave, wikiRequestContext);
            strSaveAndExit = wiki.getWikiURLFactory().getURL(urlSaveAndExit, wikiRequestContext);
            forwardUrl = wiki.getWikiURLFactory().createForwardUrl(WikiAction.view, namespace, wikiDocName, null);

        }
    }// prepareSaveUrls

    private void resetAllSections()
    {
        for (SectionModel section : listOfSectionsFromUI)
        {
            SectionUtil.resetSectionContent(section);
        }
    }// resetAllSections

    // ////////////////////////////
    // private methods
    // ////////////////////////////

    private void populateUIListWithContent()
    {
        String id = "";
        for (SectionModel smUI : listOfSectionsFromUI)
        {
            // if id is null then its a new one, so no need to get the DB
            // content
            if (StringUtils.isEmpty(smUI.getId()))
            {
                id = GenericSectionUtil.createSectionId();
                String content = StringUtils.isNotBlank(smUI.getContent()) ? smUI.getContent() : "";
                smUI.setId(id);
                smUI.setContent(SectionMacro.getSectionDefinition(id, smUI.getTitle(), smUI.getType(), content));
                continue;
            }

            populateContent(smUI, listOfSectionsFromDB);

        }// end of for loop

    }// populateUIListWithContent

    private void populateContent(SectionModel sm, List<SectionModel> listOfSectionsFromDBLocal)
    {
        for (SectionModel smDB : listOfSectionsFromDBLocal)
        {
            if (smDB.getId().equals(sm.getId()))
            {
                sm.setContent(smDB.getContent());
                break;
            }
        }// end of for loop
    }// populateContent

    private String getIframeHeight(String contentToEdit)
    {
        String height = "";

        try
        {
            height = contentToEdit.substring(contentToEdit.indexOf("height:"));
            height = height.substring(height.indexOf(":") + 1, height.indexOf(";"));
        }
        catch (Throwable t)
        {

        }

        return height;

    }// getIframeHeight

    // <iframe
    // src="/resolve/jsp/rsactiontask.jsp?type=table&query=ResolveActionTask.assignedTo.UUserName='admin'"
    // style="position:absolute;width:99%;height:300;border:0"></iframe>
    // <iframe src="http://www.yahoo.com"
    // style="position:absolute;width:99%;height:300;border:0"></iframe>
    private String getUrl(String contentToEdit)
    {
        String url = "";

        try
        {
            url = contentToEdit.substring(contentToEdit.indexOf("src="));
            url = url.substring(url.indexOf('"') + 1);
            url = url.substring(0, url.indexOf('"'));
        }
        catch (Throwable t)
        {
            Log.log.error("Error parsing url for content -->" + contentToEdit, t);
        }

        return url;
    }// getUrl

    // /resolve/jsp/rsactiontask.jsp?type=table&query=ResolveActionTask.assignedTo.UUserName='admin'
    // /resolve/jsp/rstable.jsp?main=form&form=view11&table=cust_table_11&view=view1&sys_id=2c9181e53265244201326530f0e20085&U_REFERENCE=ABC&query=sys_id=2c9181e53265244201326530f0e20085&U_REFERENCE=ABC
    private Map<String, String> getUrlParams(String url)
    {
        Map<String, String> map = new HashMap<String, String>();
        if (StringUtils.isNotEmpty(url))
        {
            if (url.indexOf("?") > -1)
            {
                url = url.substring(url.indexOf("?") + 1);
            }

            // filter out the 'query'
            if (url.indexOf(SaveHelper.QUERY) > -1)
            {
                // main=form&form=view11&table=cust_table_11&view=view1&sys_id=2c9181e53265244201326530f0e20085&query=sys_id=2c9181e53265244201326530f0e20085
                String query = url.substring(url.indexOf(SaveHelper.QUERY) + SaveHelper.QUERY.length() + 1);
                map.put(SaveHelper.QUERY, query);

                url = url.substring(0, url.indexOf(SaveHelper.QUERY) - 1);
            }

            map.putAll(StringUtils.urlToMap(url));

        }

        // map.put(SaveHelper.TABLENAME, getValue(url, SaveHelper.TABLENAME));
        // map.put(SaveHelper.VIEWNAME, getValue(url, SaveHelper.VIEWNAME));
        // map.put(SaveHelper.QUERY, getValue(url, SaveHelper.QUERY));

        return map;
    }// getUrlParams

    private String getValue(String str, String name)
    {
        String value = "";
        String key = name + "=";
        if (str.indexOf(key) > -1)
        {
            value = str.substring(str.indexOf(key) + key.length());

            // for 'query', it should be everything after that
            if (value.indexOf('&') > -1 && !name.equalsIgnoreCase(SaveHelper.QUERY))
            {
                value = value.substring(0, value.indexOf('&'));
            }
        }

        return value;
    }// getValue

    private SectionModel findSectionContent(String sectionNameOrId)
    {
        SectionModel section = null;

        if (listOfSectionsFromDB != null && listOfSectionsFromDB.size() > 0)
        {
            for (SectionModel model : listOfSectionsFromDB)
            {
                if (model.getId().equalsIgnoreCase(sectionNameOrId) || model.getTitle().equalsIgnoreCase(sectionNameOrId))
                {
                    section = model;
                    break;
                }
            }// end of for
        }

        return section;
    }

    private Map<String, String> convertModelToMap(SectionModel model)
    {
        Map<String, String> map = new HashMap<String, String>();

        map.put(SectionModel.ID, model.getId());
        map.put(SectionModel.TITLE, model.getTitle());
        map.put(SectionModel.TYPE, model.getType());
        map.put(SectionModel.CONTENT, SectionUtil.getContent(model.getContent()));

        return map;

    }

}// EditSectionHelper
