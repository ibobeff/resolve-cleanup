/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.resolve.dto.DocumentInfoModel;
import com.resolve.dto.GenericSectionModel;
import com.resolve.dto.SectionModel;
import com.resolve.persistence.model.ResolveWikiLookup;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.search.wiki.WikiIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.UserService;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.constants.WikiDocStatsCounterName;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.hibernate.util.TagUtil;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocStatisticsVO;
import com.resolve.services.hibernate.wiki.DocUtils;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.WikiUser;
import com.resolve.services.vo.form.FormSubmitDTO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.wiki.api.WikiAPI;
import com.resolve.wiki.api.WikiDocumentAPI;
import com.resolve.wiki.attachment.FileAttachmentService;
import com.resolve.wiki.attachment.MultipleFileUpload;
import com.resolve.wiki.converter.WikiConverter;
import com.resolve.wiki.dto.ViewWikiRevisionDTO;
import com.resolve.wiki.helper.SectionHelper;
import com.resolve.wiki.helper.SectionUtil;
import com.resolve.wiki.render.WikiRenderingService;
import com.resolve.wiki.render.WikiVelocityRenderer;
import com.resolve.wiki.rights.ActionTaskRightService;
import com.resolve.wiki.rsradeox.macro.DetailAjax;
import com.resolve.wiki.rsradeox.macro.ResultAjax;
import com.resolve.wiki.store.WikiStore;
import com.resolve.wiki.store.helper.SaveHelper;
import com.resolve.wiki.store.helper.StoreUtility;
import com.resolve.wiki.util.GenericUtils;
import com.resolve.wiki.web.WikiRequestContext;
import com.resolve.wiki.web.WikiURLFactory;
import com.resolve.wiki.web.vo.DocumentInfoVO;

import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.SourceFormatter;

/**
 * This class serves the Wiki related objects like Wikidoc, URLs, attachments, etc and also acts as a container for all the services. This is a Singleton and has references to all the required services
 *
 * @author jeet.marwah
 *
 */
public class Wiki
{

    //contents of view.vm file
    public static String TEMPLATE_VIEW_VM = null;

    private static Wiki instance = null;

    private WikiRenderingService wikiRenderingService = null;
    
    private ActionTaskRightService actionTaskRightService = null;
    private WikiURLFactory wikiURLFactory = null;//url factory handle
    private WikiStore wikiStore = null;//handle to the persistance layer.
//    private WikiArchiveService wikiArchiveService = null; //for versioning and archiving
    private FileAttachmentService fileAttachmentService = null;//for attachments - upload and download
//    private WikiSessionManager wikiSessionManager = null;//maintains a map of token - urls for the user
    private WikiConverter wikiConverter = null;//converts the tag from html to wiki and vice versa

    //this is a special case as this renderer can be accessed from WikiRenderingService obj. This is still a SINGLETON, so its just a reference.
    private WikiVelocityRenderer wikiVelocityRenderer = null;

//    private Import importModule;
//    private Export exportModule;
//
    //	private JBossPOJOCacheService cache = JBossPOJOCacheService.getInstance();//THIS MAY POSSIBLY CHANGE.
    
    /*** TEMPORARY PLACE FOR CONSTANTS...WILL BE MOVED/REMOVED DURING THE REVIEW ***/
    public final static String WIKI_ABORT_IN_SECS = "wikiExpiryInSecs";
    public final static String WIKI_DOC_FULLNAME_KEY = "fullname";
    public final static String WIKI_FORWARD_URL_KEY = "forwardUrl";
    public final static String WIKI_IS_ROOT = "isRoot";
    public final static String WIKI_IS_ACTIVE = "isActive";
    public final static String WIKI_RATING_BOOST_KEY = "ratingBoost";
    public final static String ERROR_EXCEPTION_KEY = "errorException";
    public final static String SUCCESS_ERROR_KEY = "successOrError";
    /**************/

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Constructors
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private Wiki()
    {
    }

    public static Wiki getInstance()
    {
        if (instance == null)
        {
            synchronized (Wiki.class)
            {
                if (instance == null)
                {
                    instance = new Wiki();
                }
            }
        }

        return instance;
    }//getInstance

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *
     * This API is a response to view a wiki document.
     *
     * eg. http://localhost:8080/resolve/service/wiki/view/Runbook/WebHome?param1=val1&param2=val2
     *
     * @param wikiRequestContext
     * @throws WikiException
     */
    public String getRenderedContent(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String errorMessage = "";
        String docFullName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        try
        {
            WikiUtils.validateWikiDocName(docFullName);
        }
        catch (Exception ex)
        {
            errorMessage = ex.getMessage();
        }

        //update the session information based on token id

        String docName = getDocumentName(wikiRequestContext);
        String sectionName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_SECTION_KEY);
        String revisionNo = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_PARAMETER_REVISION);
        String user = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String showTOC = (String) wikiRequestContext.getAttribute(ConstantValues.SHOW_TOC);
        if (showTOC == null || showTOC.trim().length() == 0)
        {
            showTOC = "false";
        }
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        String result = "";
        StringBuffer html = new StringBuffer("<html><head><title>Resolve RBA</title></head><body class=\"x-body\" style=\"padding:20px\">");

        //check the rights for the user
        boolean userHasAccess = true;
        try
        {
//            wikiRightService.checkAccess(docName, wikiRequestContext);
            boolean hasRights = ServiceWiki.hasRightsToDocument(docName, username, RightTypeEnum.view);
            if(!hasRights)
            {
                String error = "User '" + username + "' does not have '" + "view" + "' right on '" + docName + "' document.";
                throw new WikiException(error);
            }
        }
        catch (WikiException t)
        {
            userHasAccess = false;
            errorMessage = t.getGUIMessage();
        }

        if (StringUtils.isEmpty(errorMessage) && userHasAccess)
        {
            WikiDocument wikiDoc = null;
            try
            {
                wikiDoc = getDocument(docName);
            }
            catch (Exception e)
            {
                wikiDoc = null;
            }

            Map<String, String> values = new HashMap<String, String>();
            values.put(ConstantValues.MAIN_WIKI_DOC_TO_VIEW_KEY, (wikiDoc != null && wikiDoc.getUFullname() != null) ? wikiDoc.getUFullname() : docName);
            values.put(ConstantValues.WIKI_SECTION_KEY, sectionName);
            values.put(ConstantValues.REVISION_NO_KEY, revisionNo);
            values.put(ConstantValues.SHOW_TOC, showTOC);

            //set the handles so that it is available to all the Renderers
            WikiAPI wikiAPI = getWikiAPI(wikiRequestContext);
            WikiDocumentAPI docAPI = getWikiDocumentAPI(wikiRequestContext, wikiDoc);

            //set it in the context
            wikiRequestContext.setWikiAPI(wikiAPI);
            wikiRequestContext.setWikiDocumentAPI(docAPI);

            if (wikiDoc != null)
            {
                //if the namespace is 'System' process that document even if any of the flags like delete, hidden or locked are set.
                String namespace = wikiDoc.getUNamespace().trim();
                if (!namespace.equalsIgnoreCase("System") && wikiDoc.ugetUIsDeleted())
                {
                    //Wiki is marked for deletion
                    html.append("<font style=\"color:red\">'<b>" + docName + "</b>' doc is marked DELETED. To recover, please contact the Administrator.</font>");
                    html.append("</div></body></html>");

                    result = html.toString();
                }
                else
                {
                    // update thread id
                    Log.setProcessId(wikiDoc.getUFullname());

                    boolean renderOnlyContent = (wikiRequestContext.getAttribute(ConstantValues.RENDER_WIKI_CONTENT_ONLY) != null && (Boolean) wikiRequestContext.getAttribute(ConstantValues.RENDER_WIKI_CONTENT_ONLY)) ? true : false;
                    if (renderOnlyContent)
                    {
                        result = getRenderedContent(docName, sectionName, wikiRequestContext);
                    }
                    else
                    {
                        try
                        {
                            result = renderMainDocument(wikiRequestContext, values);
                        }
                        catch(Throwable t)
                        {
                            throw new WikiException(t.getMessage());
                        }
                    }

                    // clear thread id
                    Log.clearProcessId();

                    //increment the VIEW counter for this document
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put(ConstantValues.WIKI_DOCUMENT_SYS_ID, wikiDoc.getSys_id());
                    params.put(ConstantValues.WIKIDOC_STATS_COUNTER_NAME_KEY, WikiDocStatsCounterName.VIEWED);
                    MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MWiki.updateWikiRelationships", params);
                    
                    //index the wiki so the statistical data is updated
                    WikiIndexAPI.indexWikiDocument(wikiDoc.getSys_id(), false, user);
//                    wikiSessionManager.addUrlFor(user, token, requestURI);
                }
            }
            else
            {
                //Wiki doesn't exist, prompt for content request
                String url = "/resolve/jsp/rsclient.jsp?#RS.formbuilder.Viewer/name=CONTENT_REQUEST_USER";
                String editUrl = "<a style=\"color:red\" href=\"#\" onClick =\"window.open('" + url + "', '_blank');return false;\">Click here to make a content request.</a>";
                html.append("<font style=\"color:red\">The document '<b>" + docName + "</b>' does not exist. </font>" + editUrl);
                html.append("</div></body></html>");
                result = html.toString();

//                wikiSessionManager.addUrlFor(user, token, requestURI);
            }
        }
        else
        {
            //Generic Error message
            html.append("<font style=\"color:red\">" + errorMessage + "</font>");
            html.append("</div></body></html>");
            result = html.toString();
        }

        return result;

    }//getRenderedContent

    /**
     * Saves/updates the wiki doc in the db.
     * If the document is marked deleted, then it will delete that document and create a new one.
     * If the content is coming from WYSIWYG editor, it will pass it through a converter before saving it in the table
     * Once saved, it creates a forward URL and returns it to the client.
     *
     * @param wikiRequestContext
     * @throws WikiException
     */
    public Map<String, String> save(WikiRequestContext wikiRequestContext) throws WikiException
    {
        return (new SaveHelper(wikiRequestContext)).save();

    }//save

    /**
     * This method is used for the edit of the wiki doc. It returns all the values that can be edited by the user
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public Map<String, String> getContentToEdit(WikiRequestContext wikiRequestContext) throws WikiException
    {
        return (new SectionHelper(wikiRequestContext)).getSourceContentToEdit();

    }

    /**
     * returns list of section for a wiki document
     *
     * @param wikiDocFullName
     * @return
     */
    public GenericSectionModel getSections(WikiRequestContext wikiRequestContext) throws WikiException
    {
        return new SectionHelper(wikiRequestContext).getSections();
    }//getSections

    public List<SectionModel> getAllSections(String docFullName, String username) throws WikiException
    {
        WikiDocumentVO wikidoc = ServiceWiki.getWikiDoc(null, docFullName, username);// loadDocument(docFullName, true);
        List<SectionModel> list = new ArrayList<SectionModel>();

        if (wikidoc != null)
        {
            list = SectionUtil.convertContentToSectionModels(wikidoc.getUContent());
        }

        return list;
    }//getSections

    public String getSectionContent(String docFullName, String sectionName, String username) throws WikiException
    {
        String result = "";
        List<SectionModel> sections = getAllSections(docFullName, username);
        if (sections != null && sections.size() > 0)
        {
            for (SectionModel section : sections)
            {
                if (section.getTitle().equalsIgnoreCase(sectionName))
                {
                    result = section.getContent();
                    break;
                }
            }//end of for

            //remove the {section} tag around it
            if (StringUtils.isNotEmpty(result))
            {
                result = SectionUtil.getContent(result);
            }
        }

        return result;
    }//getSectionContent

    public boolean isSectionExist(String docFullName, String sectionName, String username)
    {
        boolean result = false;

        try
        {
            List<SectionModel> sections = getAllSections(docFullName, username);
            if (sections != null && sections.size() > 0)
            {
                for (SectionModel section : sections)
                {
                    if (section.getTitle().equalsIgnoreCase(sectionName))
                    {
                        result = true;
                        break;
                    }
                }//end of for
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error in isSectionExist for " + docFullName + " and section " + sectionName, t);
            result = false;
        }

        return result;
    }//isSectionExist

    /**
     * THis method is to show all the attachments for a wiki doc
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public Map<String, Object> getAllAttachmentsFor(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String docName = getDocumentName(wikiRequestContext);
        String username =  (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        //check the rights for the user
        boolean hasRights = ServiceWiki.hasRightsToDocument(docName, username, RightTypeEnum.view);
        if(!hasRights)
        {
            String error = "User '" + username + "' does not have '" + "view" + "' right on '" + docName + "' document.";
            throw new WikiException(error);
        }

        List<WikiAttachment> lAttach = wikiStore.getAllAttachmentsFor(null, docName);

        String namespace = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
        String name = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);

        Map<String, String> values = new HashMap<String, String>();
        values.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
        values.put(ConstantValues.WIKI_DOCNAME_KEY, name);

        //prepare the urls for the client side action - Attach,Delete,Download
        String strAttach = wikiURLFactory.createForwardUrl(WikiAction.attach, namespace, name, null);
        String strDelete = wikiURLFactory.createForwardUrl(WikiAction.deleteattach, namespace, name, null);
        String strDownload = wikiURLFactory.createForwardUrl(WikiAction.download, namespace, name, null);

        Map<String, Object> hmValues = new HashMap<String, Object>();
        hmValues.put(ConstantValues.WIKI_ATTACHMENTS_KEY, lAttach);
        hmValues.put(ConstantValues.WIKI_ATTACHFILE_LINK_KEY, strAttach);
        hmValues.put(ConstantValues.WIKI_DELETEFILE_LINK_KEY, strDelete);
        hmValues.put(ConstantValues.WIKI_DOWNLOADFILE_LINK_KEY, strDownload);

        return hmValues;
    }

    /**
     * Lookup
     *
     * eg. rswiki.jsp?lookup=mttrapd.tools.ssm@@GTP AlertLogMonitoring@destgsremp2.de.alcatel-lucent.com@User netcool User for Netcool logged in on pts/3 from 146.112.138.189. There are now 2 users logged in.
     * will forward it to the lookup '.*@.*@GTP AlertLogMonitoring@destgsremp.*@.*logged in.*' set up in the database
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    @SuppressWarnings("unused")
	public Map<String, String> lookupAction(WikiRequestContext wikiRequestContext) throws WikiException
    {
        List<ResolveWikiLookup> listLookup = wikiStore.getWikiLookup();
        Collections.sort(listLookup);

        String querystring = getQueryString(wikiRequestContext.getHttpServletRequest());

        String DEFAULT_WIKIDOC = "";
        String forwardToWikiDoc = "";
        String matchLookup = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_LOOKUP_KEY);
        if (matchLookup != null && matchLookup.trim().length() > 0)
        {
            final String REGEX_GROUP_PATTERN = "\\$\\{(.*?)\\}";
            Pattern pattern = null;

            forLoop: for (ResolveWikiLookup lup : listLookup)
            {
                pattern = Pattern.compile(lup.getURegex().trim());
                Matcher matcher = pattern.matcher(matchLookup);
                if (matcher.find())
                {
                    forwardToWikiDoc = lup.getUWiki();

                    // replace grouping in forwardToWikiDoc
                    if (forwardToWikiDoc.indexOf('$') >= 0)
                    {
                        String matchContent = forwardToWikiDoc;
                        int last = 0;
                        Pattern groupPattern = Pattern.compile(REGEX_GROUP_PATTERN);
                        Matcher groupMatcher = groupPattern.matcher(matchContent);
                        while (groupMatcher.find())
                        {
                            try
                            {
                                int groupIdx = Integer.parseInt(groupMatcher.group(1));
                                //String matchStr = "${"+groupIdx+"}";
                                //String replaceStr = matcher.group(groupIdx);
                                forwardToWikiDoc = forwardToWikiDoc.replace("${" + groupIdx + "}", matcher.group(groupIdx));
                            }
                            catch (Throwable e)
                            {
                                Log.log.warn(e.getMessage(), e);
                            }

                            last = matcher.end();
                        }
                    }

                    // search for wikidoc if has wildcard
                    if (forwardToWikiDoc.indexOf('%') >= 0)
                    {
                        try
                        {
                            String whereClause = "doc.UFullname LIKE '" + forwardToWikiDoc + "'";
                            List<WikiDocumentAPI> docs = searchDocumentsNames(whereClause, wikiRequestContext);
                            if (docs.size() > 0)
                            {
                                forwardToWikiDoc = docs.get(0).getFullName();
                            }
                            else
                            {
                                forwardToWikiDoc = null;
                            }
                        }
                        catch (Exception e)
                        {
                            forwardToWikiDoc = null;
                        }
                    }

                    break forLoop;
                }
                else if (lup.getURegex().trim().equals("DEFAULT"))
                {
                    DEFAULT_WIKIDOC = lup.getUWiki();
                }
            }
        }

        //if there is no doc, then DEFAULT_WIKIDOC
        if (forwardToWikiDoc == null || forwardToWikiDoc.trim().length() == 0)
        {
            forwardToWikiDoc = DEFAULT_WIKIDOC;
        }

        Map<String, String> hmValues = null;
        if (forwardToWikiDoc.trim().length() != 0)
        {
            String forwardUrl = wikiURLFactory.createForwardUrl(WikiAction.view, forwardToWikiDoc.trim(), querystring);
            hmValues = new HashMap<String, String>();
            hmValues.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
        }
        else
        {
            //check if the matchLookup itself is a wiki page. If yes, then forward to that page.
            WikiDocument wikidoc = getDocument(matchLookup);
            if (wikidoc != null)
            {
                String forwardUrl = wikiURLFactory.createForwardUrl(WikiAction.view, wikidoc.getUFullname(), querystring);
                hmValues = new HashMap<String, String>();
                hmValues.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
            }
            else
            {
                //HOME PAGE if nothing is there
                hmValues = getUserHomepage(wikiRequestContext);
            }
        }

        return hmValues;
    }//lookupAction

    /**
     * Uploads the file in the DB or file system based on the user selection
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public Map<String, Object> uploadAttachment(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String docName = getDocumentName(wikiRequestContext);
        String namespace = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        String isMultipleUpload = (String) wikiRequestContext.getAttribute(HibernateConstants.IS_MULTIPLE_FILE);
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        Map<String, Object> hmValues = new HashMap<String, Object>();
        if (wikiDocName.equals("profile"))
        {
            fileAttachmentService.uploadAttachment(null, wikiRequestContext);
        }
        else
        {
            WikiDocument wikiDocument = loadDocument(docName, false);

            //check the rights for the user
//            wikiRightService.checkAccess(docName, wikiRequestContext);
            boolean hasRights = ServiceWiki.hasRightsToDocument(docName, username, RightTypeEnum.view);
            if(!hasRights)
            {
                String error = "User '" + username + "' does not have '" + "view" + "' right on '" + docName + "' document.";
                throw new WikiException(error);
            }

            if (StringUtils.isNotEmpty(isMultipleUpload) && isMultipleUpload.equalsIgnoreCase("true"))
            {
                //for multiple file upload
                try
                {
                    new MultipleFileUpload(wikiDocument, wikiRequestContext).upload();
                }
                catch (Exception e)
                {
                    Log.log.error("error doing the upload", e);
                }
            }
            else
            {
                fileAttachmentService.uploadAttachment(wikiDocument, wikiRequestContext);
            }

            String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY) != null ? (String) hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY) : null;
            if (StringUtils.isEmpty(forwardUrl))
            {
                forwardUrl = wikiURLFactory.createForwardUrl(WikiAction.viewattach, namespace, wikiDocName, null);
                hmValues.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
            }
        }
        return hmValues;
    }

    /**
     * Delete the list of attachments selected by the user.
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public Map<String, String> deleteAttachments(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String global = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_GLOBAL_FLAG);
        String docName = getDocumentName(wikiRequestContext);
        String namespace = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        if (global != null && global.trim().length() > 0)
        {
            docName = ConstantValues.NAMESPACE_SYSTEM + "." + ConstantValues.DOCUMENT_ATTACHMENT;
            namespace = ConstantValues.NAMESPACE_SYSTEM;
            wikiDocName = ConstantValues.DOCUMENT_ATTACHMENT;

            wikiRequestContext.setAttribute(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
            wikiRequestContext.setAttribute(ConstantValues.WIKI_DOCNAME_KEY, wikiDocName);
            wikiRequestContext.setAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY, docName);
        }
        else
        {
            //check the rights for the user
//            wikiRightService.checkAccess(docName, wikiRequestContext);
            boolean hasRights = ServiceWiki.hasRightsToDocument(docName, username, RightTypeEnum.view);
            if(!hasRights)
            {
                String error = "User '" + username + "' does not have '" + "view" + "' right on '" + docName + "' document.";
                throw new WikiException(error);
            }
        }

        Map<String, String> hmValues = new HashMap<String, String>();

        fileAttachmentService.deleteAttachments(wikiRequestContext);

        String forwardUrl = wikiURLFactory.createForwardUrl(WikiAction.viewattach, namespace, wikiDocName, null);
        hmValues.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
        return hmValues;
    }//deleteAttachments


    /**
     * This method may be called by the velocity engine to include the other doc
     *
     * @param docName
     * @return
     */
    private String getRenderedContent(String docName, String sectionName, WikiRequestContext wikiRequestContext) throws WikiException
    {
        String result = "";
        WikiAction wikiAction = (WikiAction) wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY);

        WikiDocument wikiDoc = loadDocument(docName, true); //If it is not there, then qry the db to prepare the doc

        if (wikiAction == WikiAction.view)
            result = renderTheDocContent(wikiDoc, sectionName, wikiRequestContext);
        else if (wikiAction == WikiAction.viewmodelc)
            result = wikiDoc.getUModelProcess();
        else if (wikiAction == WikiAction.viewexcpc)
            result = wikiDoc.getUModelException();
        else if (wikiAction == WikiAction.viewfinalc) result = wikiDoc.getUModelFinal();

        return result;
    }//getRenderedContent

    /**
     * This method returns a list of documents that belong to a namespace.
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public Map<String, Object> getListdoc(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String namespace = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
        List<DocumentInfoVO> lData = wikiStore.getListOfDocumentsFor(namespace);

        //prepare urls for view and edit
        String strUrl = "";
        Map<String, String> values = new HashMap<String, String>();
        values.put(ConstantValues.WIKI_NAMESPACE_KEY, (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY));
        for (DocumentInfoVO vo : lData)
        {
            values.put(ConstantValues.WIKI_DOCNAME_KEY, vo.getDocumentName());

            strUrl = wikiURLFactory.createForwardUrl(WikiAction.view, vo.getDocumentFullName().trim(), null);
            vo.setViewLink(strUrl);

            strUrl = wikiURLFactory.createForwardUrl(WikiAction.edit, vo.getDocumentFullName().trim(), null);
            vo.setEditLink(strUrl);
        }

        Map<String, Object> hmValues = new HashMap<String, Object>();
        hmValues.put(ConstantValues.WIKI_DOCLIST_KEY, lData);

        return hmValues;

    }//getListdoc

    @SuppressWarnings("unchecked")
	public Map<String, String> updateLastReviewedDateForWikiDoc(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        List<String> docNames = new ArrayList<String>();
        Map<String, String> result = new HashMap<String, String>();
        List<DocumentInfoModel> wikiDocs = (List<DocumentInfoModel>) wikiRequestContext.getAttribute(ConstantValues.WIKIDOCS_KEY);
        String browserTimeZone = (String) wikiRequestContext.getAttribute(ConstantValues.BROWSER_CLIENT_TIMEZONE);

        if (wikiDocs != null && wikiDocs.size() > 0)
        {
            for (DocumentInfoModel model : wikiDocs)
            {
                String fullName = model.getFullname();
                String namespace = model.getNamespace();

                if (StringUtils.isNotBlank(fullName))
                {
                    docNames.add(model.getFullname());
                }
                else if (StringUtils.isNotBlank(namespace))
                {
                    try
                    {
                        StoreUtility.updateLastReviewedDateForNamespace(namespace, browserTimeZone, username);
                    }
                    catch (Exception e)
                    {
                        result.put(ERROR_EXCEPTION_KEY, "Some error happened in update the review date for namespace " + namespace);
                    }
                }
            }

            if (docNames.size() > 0)
            {
                try
                {
                    StoreUtility.updateLastReviewedDateForWikiDocs(docNames, browserTimeZone, username);
                }
                catch (Exception e)
                {
                    result.put(ERROR_EXCEPTION_KEY, "Some error happened in update the review date. Please check the logs.");
                }
            }
        }

        return result;
    }

    public String submitRequestToExecuteRunbook(WikiRequestContext wikiRequestContext, Map<String, Object> params) throws WikiException
    {
        //        String docName = getDocumentName(wikiRequestContext);
        //check if there is a specific runbook to execute
        String userid = wikiRequestContext.getWikiUser().getUsername();
        String runbookToExecute = null;
        if (params != null)
        {
            runbookToExecute = (String) params.get(HibernateConstants.EXECUTE_RUNBOOK);
        }

        //if no, then use the current document to execute
        if (StringUtils.isEmpty(runbookToExecute))
        {
            runbookToExecute = getDocumentName(wikiRequestContext);
        }
        String result = "";

        //check if the user has rights to execute
//        wikiRightService.checkAccess(runbookToExecute, wikiRequestContext);
        boolean hasRights = ServiceWiki.hasRightsToDocument(runbookToExecute, userid, RightTypeEnum.execute);
        if(!hasRights)
        {
            String error = "User '" + userid + "' does not have '" + "execute" + "' right on '" + runbookToExecute + "' document.";
            throw new WikiException(error);
        }
        
        try
        {

            Map<String, Object> sendParams = new HashMap<String, Object>();
            if (wikiRequestContext.getHttpServletRequest() != null)
            {
                sendParams.putAll(ControllerUtil.getParameterMap(wikiRequestContext.getHttpServletRequest()));
            }

            // override with params
            if (params != null)
            {
                sendParams.putAll(params);
            }

            
            sendParams.put(Constants.EXECUTE_USERID, userid);

            if (StringUtils.isEmpty((String) sendParams.get(Constants.HTTP_REQUEST_PROBLEMID)))
            {
                String PROBLEMID = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_PROBLEMID);
                if (PROBLEMID != null)
                {
                    sendParams.put(Constants.HTTP_REQUEST_PROBLEMID, PROBLEMID);
                }
            }

            sendParams.put(Constants.HTTP_REQUEST_WIKI, runbookToExecute);
            sendParams.put(Constants.HTTP_REQUEST_ACTION, "EXECUTEPROCESS");

            //Log.log.debug("Form ExecuteRunbook.start() params: "+sendParams);

            //send the request to execute the runbook
            result = ExecuteRunbook.start(sendParams);
            
            if (StringUtils.isNotBlank(result))
            {
                if (result.toUpperCase().contains("ERROR") || result.toUpperCase().contains("FAILURE"))
                {
                    throw new WikiException ("Failed to execute runbook " + runbookToExecute + ". " + result);
                }
            }
            
            if (params == null)
            {
                params = new HashMap<String, Object>();
            }
            params.putAll(sendParams);

            String problemId = (String) sendParams.get(Constants.EXECUTE_PROBLEMID);
            wikiRequestContext.setAttribute(Constants.EXECUTE_PROBLEMID, problemId);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            result = e.getMessage();
        }

        return result;

    }//submitRequestToExecuteRunbook

    /**
     * Homepage when the user logs in
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public Map<String, String> getUserHomepage(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String forwardUrl = "";
        String homepage = ConstantValues.WIKI_NAMESPACE_HOME + "." + ConstantValues.WIKI_WEB_HOME;
        //		String systemHomepage = PropertiesUtil.getPropertyString(ClientConstants.PROPERTY_SYSTEM_HOMEPAGE);
        String lookupDefaultWikiDoc = PropertiesUtil.getPropertyString(ConstantValues.PROP_WIKI_LOOKUP_DEFAULT_DOC);
        String propertyFileWikiDoc = PropertiesUtil.getPropertyString(ConstantValues.PROP_WIKI_PROPERTY_TABLE_HOMEPAGE);
        String wikiTableRoles = PropertiesUtil.getPropertyString(ConstantValues.WIKI_PROPERTY_TABLE_ROLES);
        boolean setWikiPropertyHomepage = false;

        if (wikiTableRoles != null)
        {
            if (wikiTableRoles.equalsIgnoreCase("true"))
            {
                //if the user has only 1 homepage, go to that homepage else list all the homepages assigned to that user
                List<DocumentInfoVO> listOfHomePages = includeListOfHomepages(wikiRequestContext);
                if (listOfHomePages != null)
                {
                    if (listOfHomePages.size() == 1)
                    {
                        homepage = listOfHomePages.get(0).getDocumentFullName();
                    }
                    else if (listOfHomePages.size() > 1)
                    {
                        WikiDocument wikidoc = loadDocument(ConstantValues.WIKI_SYSTEM_HOMEPAGE, false);
                        if (StringUtils.isNotBlank(wikidoc.getSys_id()))
                        {
                            homepage = ConstantValues.WIKI_SYSTEM_HOMEPAGE;
                        }
                    }
                    else
                    {
                        setWikiPropertyHomepage = true;
                    }
                }
            }
            else
            {
                setWikiPropertyHomepage = true;
            }
        }
        else
        {
            setWikiPropertyHomepage = true;
        }

        if (setWikiPropertyHomepage)
        {
            WikiDocument wikidoc = null;
            //this is from a new system property wiki.lookup.default.doc
            if(StringUtils.isNotBlank(lookupDefaultWikiDoc))
            {
                wikidoc = loadDocument(lookupDefaultWikiDoc.trim(), false);
                if (wikidoc != null && StringUtils.isNotBlank(wikidoc.getSys_id()))
                {
                    homepage = lookupDefaultWikiDoc.trim();
                }
            }
            else
            {
                if (StringUtils.isNotBlank(propertyFileWikiDoc))
                {
                    wikidoc = loadDocument(propertyFileWikiDoc.trim(), false);
                    if (wikidoc != null && StringUtils.isNotBlank(wikidoc.getSys_id()))
                    {
                        homepage = propertyFileWikiDoc.trim();
                    }
                }
            }
        }

        String querystring = wikiRequestContext.getHttpServletRequest().getQueryString();//TODO: VERIFY THIS
        String[] arr = homepage.split("\\.");
        String namespace = arr[0];
        String fileName = arr[1];
        forwardUrl = wikiURLFactory.createForwardUrl(WikiAction.view, namespace, fileName, querystring);

        Map<String, String> hmValues = new HashMap<String, String>();
        hmValues.put(ConstantValues.WIKI_FORWARD_URL_KEY, forwardUrl);
        hmValues.put(ConstantValues.WIKI_DOCNAME_KEY, homepage);
        return hmValues;
    }//getUserHomepage

    public String includeForm(String topic, WikiRequestContext wikiRequestContext) throws WikiException
    {
        return includeForm(topic, wikiRequestContext, true, null);
    }

    /**
     * this is a macro and defined in the macros.vm file. It is used on the Homepage to list all the docs that this user can look at
     *
     * @param wikiRequestContext
     * @return
     */
    public List<DocumentInfoVO> includeListOfHomepages(WikiRequestContext wikiRequestContext)
    {
        WikiUser wikiUser = wikiRequestContext.getWikiUser();
        List<DocumentInfoVO> list = new ArrayList<DocumentInfoVO>();
        if (wikiUser != null)
        {
            try
            {
                list = new UserService().getUserListOfHomepages(wikiUser.getUsername());
                populateLinks(list, "", 1);
            }
            catch (Exception e)
            {
                Log.log.error("Error in includeListOfHomepages:", e);
            }
        }
        return list;
    }

    public String includeForm(String topic, WikiRequestContext wikiRequestContext, boolean pre, String revisionNo) throws WikiException
    {
        if (pre) {
            return "{pre}" + include(topic, null, wikiRequestContext, revisionNo) + "{pre}";
        }
        
        return include(topic, null, wikiRequestContext, revisionNo);
    }

    public String includeForm(String topic, String sectionname, WikiRequestContext wikiRequestContext, boolean pre, String revisionNo) throws WikiException
    {
        if (pre)
        {
            return "{pre}" + include(topic, sectionname, wikiRequestContext, revisionNo) + "{pre}";
        }
        
        return include(topic, sectionname, wikiRequestContext, revisionNo);
    }

    private String include(String topic, String sectionname, WikiRequestContext wikiRequestContext, String revisionNo) throws WikiException
    {
        String result = "";

        //Test.test#main
        if (StringUtils.isNotBlank(topic) && topic.contains("."))
        {
	        String[] arr = topic.split("\\.");
	        String namespace = arr[0];//Test
	        String docname = ControllerUtil.getDocumentNameFrom(arr[1]);//test
	        String sectionName = ControllerUtil.getSectionNameFromDocName(arr[1]);//main
	        if (StringUtils.isNotEmpty(sectionname))
	        {
	            sectionName = sectionname;
	        }
	
	        String fullName = namespace + "." + docname;
	        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
	
	        //check the rights for the user
	        boolean hasRights = ServiceWiki.hasRightsToDocument(fullName, username, RightTypeEnum.view);
	        if(!hasRights)
	        {
	            String error = "User '" + username + "' does not have '" + "view" + "' right on '" + fullName + "' document.";
	            throw new WikiException(error);
	        }
	
	        int revNo = -1;
	        try
	        {
	            if (revisionNo != null)
	            {
	                revNo = Integer.parseInt(revisionNo);
	            }
	        }
	        catch (Exception e)
	        {
	            //ignore it...show the current version
	        }
	        if (revNo > 0)
	        {
	            result = getRenderedContentForRevision(fullName, revNo, wikiRequestContext);
	        }
	        else
	        {
	            result = getRenderedContent(fullName, sectionName, wikiRequestContext);
	        }
        }

        return result;

    }

    /**
     * 
     * @param tagname - tag1&tag2,tag4,tag5&tag6
     * @param wikiRequestContext
     * @return
     */
    public List<DocumentInfoVO> getListOfDocumentsForTag(String tagname, WikiRequestContext wikiRequestContext)
    {
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        List<DocumentInfoVO> list = new ArrayList<DocumentInfoVO>();
        try
        {
            Set<String> tagNames = new HashSet<String>();
            tagNames.addAll(Arrays.asList(tagname.split(",")));
            
            list = getListOfDocumentsForTag(tagNames, null, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting the list of documents for tag:" + tagname, e);
        }

        return list;
    }

    public List<DocumentInfoVO> getListOfDocumentsForTag(String tagname, String namespace, WikiRequestContext wikiRequestContext)
    {
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        List<DocumentInfoVO> list = new ArrayList<DocumentInfoVO>();

        try
        {
            Set<String> tagNames = new HashSet<String>();
            tagNames.addAll(Arrays.asList(tagname.split(",")));
            
            list = getListOfDocumentsForTag(tagNames, namespace, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting the list of documents for tag:" + tagname, e);
        }
        
        return list;

    }
    
    private List<DocumentInfoVO> getListOfDocumentsForTag(Set<String> tagNames, String namespace, String username) throws Exception
    {
        List<DocumentInfoVO> list = new ArrayList<DocumentInfoVO>();
        
        List<WikiDocumentVO> docs = TagUtil.findListOfDocumentsHavingTags(tagNames, namespace, username);
        for(WikiDocumentVO doc : docs)
        {
            DocumentInfoVO vo = new DocumentInfoVO();
            vo.setDocumentFullName(doc.getUFullname());
            vo.setDocumentName(doc.getUName());
            vo.setModifiedBy(doc.getSysUpdatedBy());
            vo.setModifiedOn(doc.getSysUpdatedOn());
            vo.setNamespace(doc.getUNamespace());
            
            String viewLink = wikiURLFactory.createForwardUrl(WikiAction.view, doc.getUNamespace(), doc.getUName(), null);
            vo.setViewLink(viewLink);
         
            list.add(vo);
        }
        
        return list;
    }

    /**
     * returns the count a doc is viewed
     *
     * @param wikiRequestContext
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentViewCount(WikiRequestContext wikiRequestContext, String docFullName) throws WikiException
    {
        long count = -1;

        //if its null, then return the count of current document
        if (StringUtils.isEmpty(docFullName))
        {
            docFullName = getDocumentName(wikiRequestContext);
        }

        try
        {
            //now if the docName is set, get the count
            if (StringUtils.isNotEmpty(docFullName))
            {
                String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                WikidocStatisticsVO stats = ServiceWiki.findWikidocStatisticsForWiki(null, docFullName, username);

                count = stats.getUViewCount();//StoreUtility.getStatisticForDocument(docFullName, WikiDocStatsCounterName.VIEWED);
            }//end of if
        }
        catch (Exception e)
        {
            Log.log.error("Error getting the stats for doc " + docFullName, e);
        }

        return count;
    }//getWikiDocumentViewCount

    /**
     * returns the count a doc is edited
     *
     * @param wikiRequestContext
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentEditCount(WikiRequestContext wikiRequestContext, String docFullName) throws WikiException
    {
        long count = -1;

        //if its null, then return the count of current document
        if (StringUtils.isEmpty(docFullName))
        {
            docFullName = getDocumentName(wikiRequestContext);
        }
        
        try
        {
            //now if the docName is set, get the count
            if (StringUtils.isNotEmpty(docFullName))
            {
                String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                WikidocStatisticsVO stats = ServiceWiki.findWikidocStatisticsForWiki(null, docFullName, username);

                count = stats.getUEditCount();//StoreUtility.getStatisticForDocument(docFullName, WikiDocStatsCounterName.VIEWED);
            }//end of if
        }
        catch (Exception e)
        {
            Log.log.error("Error getting the stats for doc " + docFullName, e);
        }

        return count;
    }//getWikiDocumentEditCount

    /**
     * returns the count a doc is executed
     *
     * @param wikiRequestContext
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentExecuteCount(WikiRequestContext wikiRequestContext, String docFullName) throws WikiException
    {
        long count = -1;

        //if its null, then return the count of current document
        if (StringUtils.isEmpty(docFullName))
        {
            docFullName = getDocumentName(wikiRequestContext);
        }
        
        try
        {
            //now if the docName is set, get the count
            if (StringUtils.isNotEmpty(docFullName))
            {
                String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                WikidocStatisticsVO stats = ServiceWiki.findWikidocStatisticsForWiki(null, docFullName, username);

                count = stats.getUExecuteCount();//StoreUtility.getStatisticForDocument(docFullName, WikiDocStatsCounterName.VIEWED);
            }//end of if
        }
        catch (Exception e)
        {
            Log.log.error("Error getting the stats for doc " + docFullName, e);
        }
        
        return count;
    }//getWikiDocumentExecuteCount

    /**
     *
     * eg. --> #set ($sql = "doc.web = 'RSQA' and doc.name != 'WebHome' and doc.name not like 'Performance%' ORDER BY doc.name")
     * 
     * "doc.UNamespace = 'Doc3' and doc.UName != 'Recent' and doc.UName != 'WebHome' and DATEDIFF(SYSDATE(), doc.sysUpdatedOn) &lt;= 30 
     * order by doc.sysUpdatedOn desc
     *
     * @param wheresql
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public List<WikiDocumentAPI> searchDocumentsNames(String wheresql, WikiRequestContext wikiRequestContext) throws WikiException
    {
        List<WikiDocumentAPI> docsAPI = new ArrayList<WikiDocumentAPI>();
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            List<WikiDocumentVO> wikiDocs = ServiceWiki.searchWikiDocsUsingHQL(wheresql, username);
//        List<WikiDocument> wikiDocs = wikiStore.searchWikiDocsByHQL(wheresql, user);
            for (WikiDocumentVO doc : wikiDocs)
            {
                docsAPI.add(new WikiDocumentAPI(doc, wikiRequestContext));
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting the docs with wheresql :"+ wheresql, e);
        }

        return docsAPI;

    }

    /**
     * submits request to execute a runbook
     *
     * @param fullname
     * @param params
     * @param wikiRequestContext
     */
    public void executeRunbook(String fullname, String params, WikiRequestContext wikiRequestContext)
    {
        if (StringUtils.isNotEmpty(fullname) && fullname.indexOf(".") > -1)
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put(HibernateConstants.EXECUTE_RUNBOOK, fullname);

            if (StringUtils.isNotEmpty(params))
            {
                map.putAll(StringUtils.urlToMap(params));
            }

            try
            {
                submitRequestToExecuteRunbook(wikiRequestContext, map);
            }
            catch (Throwable t)
            {
                Log.log.error("Error executing runbook :" + fullname, t);
            }
        }
    }//executeRunbook

    ////used in resolve_listpages.vm
    public List<DocumentInfoVO> getListOfDocumentsForNamespace(String namespace, WikiRequestContext wikiRequestContext) throws Exception
    {
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        //prepare the query
        QueryDTO query = new QueryDTO();
        query.setModelName("WikiDocument");
        query.setSelectColumns("sys_id,UFullname,UNamespace,UName,USummary,UIsActive,UIsDeleted,UIsLocked,UIsHidden,UHasActiveModel,UIsRoot,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
        query.addFilterItem(new QueryFilter("auto", namespace, "UNamespace", QueryFilter.EQUALS));
        query.addSortItem(new QuerySort("UFullname", QuerySort.SortOrder.ASC));
        
        //fetch the data based on the query
        List<DocumentInfoVO> allData = new ArrayList<DocumentInfoVO>();
        List<WikiDocumentVO> data = ServiceWiki.getWikiDocuments(query, null, username);

        //prepare the view link and set it
        for (WikiDocumentVO dm : data)
        {
            DocumentInfoVO vo = new DocumentInfoVO();
            vo.setDocumentFullName(dm.getUFullname());
            vo.setDocumentName(dm.getUName());
            vo.setNamespace(dm.getUNamespace());
            vo.setModifiedBy(dm.getSysUpdatedBy());
            vo.setModifiedOn(dm.getSysUpdatedOn());

            String viewLink = wikiURLFactory.createForwardUrl(WikiAction.view, dm.getUNamespace(), dm.getUName(), null);
            vo.setViewLink(viewLink);


            allData.add(vo);
        }

        return allData;
    }

    /**
     * returns the value of the property set
     * @param name
     * @return
     */
    public String getSystemProperty(String name)
    {
        return StoreUtility.getSystemProperty(name);

    }// getSystemProperty

    /**
     * request that returns the html for {result} tag on the wiki page
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public String getHtmlForResultWikiTag(WikiRequestContext wikiRequestContext)
    {
        HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
        String html = "";

        try
        {
            html = (new ResultAjax()).getHtmlForResultWikiTag(this, request);
        }
        catch (Throwable t)
        {
            Log.log.error("Error in getHtmlForResultWikiTag:", t);
            html = "<div><font color=RED>Error in the {result} tag.</font></div>";
        }

        return html;

    }//getHtmlForResultWikiTag

    /**
     * request that returns the html for {detail} tag on the wiki page
     *
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    public String getHtmlForDetailWikiTag(WikiRequestContext wikiRequestContext)
    {
        HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
        String html = "";

        try
        {
            html = (new DetailAjax()).getHtmlForDetailWikiTag(this, request);
        }
        catch (Throwable t)
        {
            Log.log.error("Error in getHtmlForDetailWikiTag:", t);
            html = "<div><font color=RED>Error in the {detail} tag.</font></div>";
        }

        return html;

    }//getHtmlForDetailWikiTag

    public Map<String, String> submitFormExt(WikiRequestContext wikiRequestContext) throws WikiException
    {
        String user = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        FormSubmitDTO formSubmitDTO = (FormSubmitDTO) wikiRequestContext.getAttribute(HibernateConstants.EXT_FORM_SUBMIT_KEY);

        try
        {
            return ServiceHibernate.executeForm(formSubmitDTO, user);
        }
        catch (Exception e)
        {
            throw new WikiException(e);
        }
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Other methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private String renderMainDocument(WikiRequestContext wikiRequestContext, Map<String, String> params)
    {
        //check for the FLAGS to set a WORKSHEET as ACTIVE
        
        try
        {
            setWorksheetActive(wikiRequestContext);
        }
        catch(Throwable t)
        {
            throw t;
        }

        if (StringUtils.isEmpty(TEMPLATE_VIEW_VM))
        {
            TEMPLATE_VIEW_VM = getResourceContent(RSContext.getWebHome() + "WEB-INF/classes/templates/view.vm");
        }
        String result = wikiVelocityRenderer.evaluate(TEMPLATE_VIEW_VM, "view", wikiRequestContext, params);
        return result;
    } // renderMainDocument

    //reads the template contents
    private String getResourceContent(String templateName)
    {
        String content = "";
        try
        {
            // InputStream it = this.getClass().getResourceAsStream(templateName);
            content = GenericUtils.getFileContent(FileUtils.getFile(templateName));
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return content;
    }

    /**
     * THis method is for displaying the revision of the doc
     *
     * @param docName
     * @param wikiRequestContext
     * @return
     */
    private String getRenderedContentForRevision(String docName, int revisionNo, WikiRequestContext wikiRequestContext) throws WikiException
    {
        String result = "";
        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ViewWikiRevisionDTO viewVersion = new QueryWiki().getSpecificWikiRevisionsFor(null, docName, revisionNo, username);
            String revisionContent = viewVersion.getContent();//wikiArchiveService.getContentFor(wikiRequestContext.getWiki().getWikiStore(), docName, revisionNo);

            //create a dummy wiki doc object and pass it to render it.
            WikiDocument doc = new WikiDocument();
            doc.setUFullname(docName);
            doc.setUContent(revisionContent);

            //render it
            result = renderTheDocContent(doc, null, wikiRequestContext);
        }
        catch (Exception e)
        {
           Log.log.error("Error in getting revision #"+ revisionNo +" for document " + docName, e);
        }

        return result;

    }//getRenderedContentForRevision

    public WikiDocument loadDocument(String docName, boolean cacheable) throws WikiException
    {
        WikiDocument wikiDocument = null;
        Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, docName);
        if (obj != null)
        {
            wikiDocument = (WikiDocument) obj;
        }
        else
        {
            try
            {
                wikiDocument = StoreUtility.find(docName, false);
            }
            catch (Exception e)
            {
                //this is check if the doc exist. 
            }

            if (wikiDocument != null)
            {
                if (cacheable)
                {
                    HibernateUtil.cacheDBObject(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, 
                    							new String[] { "com.resolve.persistence.model.WikiDocument" }, 
                    							docName, wikiDocument);
                }
            }
            else
            {
                String[] nameArr = docName.split("\\.");
                
                if (nameArr.length >= 2)
                {
                    String namespace = nameArr[0].trim();
                    String name = nameArr[1].trim();

                    //qrys the DB and prepares the wikidoc obj
                    //give an empty new obj EVERYTIME. So DO NOT USE INJECTION FOR THIS **** WARNING ******
                    wikiDocument = new WikiDocument();
                    wikiDocument.setUFullname(namespace + "." + name);
                    wikiDocument.setUNamespace(namespace);
                    wikiDocument.setUName(name);
                }
                else
                {
                    throw new WikiException("Specified wiki document name [" + docName + 
                    						"] is not in expected format of name_space.document_name.");
                }
            }
        }

        if (wikiDocument.getAccessRights() != null)
        {
            wikiDocument.setUReadRoles(wikiDocument.getAccessRights().getUReadAccess());
            wikiDocument.setUWriteRoles(wikiDocument.getAccessRights().getUWriteAccess());
            wikiDocument.setUExecuteRoles(wikiDocument.getAccessRights().getUExecuteAccess());
            wikiDocument.setUAdminRoles(wikiDocument.getAccessRights().getUAdminAccess());
        }
        else
        {
            //this means that the user wants to create a new wiki document, so just return the obj as is
            wikiDocument.setUIsDeleted(false);
            wikiDocument.setUIsHidden(false);
            wikiDocument.setUIsLocked(false);
            wikiDocument.setUIsActive(true);
            wikiDocument.setUIsDefaultRole(true);
        }

        return wikiDocument;
    }

    private String renderTheDocContent(WikiDocument wikiDoc, String sectionTitle, WikiRequestContext wikiRequestContext) throws WikiException
    {
        String result = "";

        String fullName = wikiDoc.getUFullname().replace('.', ':');
        String[] val = fullName.split(":");
        String namespace = val[0];
        String docName = val[1];

        Map<String, String> values = new HashMap<String, String>();
        values.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
        values.put(ConstantValues.WIKI_DOCNAME_KEY, docName);
        values.put(ConstantValues.WIKI_DOC_FULLNAME_KEY, wikiDoc.getUFullname());
        values.put(Constants.HTTP_REQUEST_USERNAME, wikiRequestContext.getWikiUser().getUsername());
        values.put(ConstantValues.MAIN_WIKI_DOC_TO_VIEW_KEY, wikiDoc.getUFullname());

        //set the handles so that it is available to all the Renderers
        WikiAPI wikiAPI = getWikiAPI(wikiRequestContext);
        WikiDocumentAPI docAPI = getWikiDocumentAPI(wikiRequestContext, wikiDoc);

        //set it in the context
        wikiRequestContext.setWikiAPI(wikiAPI);
        wikiRequestContext.setWikiDocumentAPI(docAPI);

        if (!StringUtils.isEmpty(wikiDoc.getUContent()))
        {
            String contentToRender = wikiDoc.getUContent();
            //if its section of a document then get that, else the whole content
            if (StringUtils.isNotEmpty(sectionTitle))
            {
                SectionModel section = SectionUtil.getSection(sectionTitle, wikiDoc.getUContent());
                if (section != null)
                {
                    contentToRender = section.getContent();
                }
                else
                {
                    contentToRender = "<font color=RED>For Wiki '" + wikiDoc.getUFullname() + "', section '" + sectionTitle + "' does not exist.</font>";
                }
            }

            result = renderTheContent(contentToRender, values, wikiRequestContext);
        }
        return result;
    } // renderTheDocContent

    private String renderTheContent(String content, Map<String, String> params, WikiRequestContext wikiRequestContext) throws WikiException
    {
        String contentWithSection = GenericSectionUtil.validateSectionStr(content, null);
        String urlParams = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_URL_PARAMS);
        if (StringUtils.isNotEmpty(urlParams))
        {
            params.putAll(StringUtils.urlToMap(urlParams));

            //also add it to context
            if (params != null)
            {
                for (Map.Entry<String, String> entry : params.entrySet())
                {
                    wikiRequestContext.setAttribute((String) entry.getKey(), entry.getValue());
                }
            }
        }//end of if

        return wikiRenderingService.renderText(contentWithSection, params, wikiRequestContext);
    } // renderTheContent

    private WikiAPI getWikiAPI(WikiRequestContext context)
    {
        WikiAPI wikiApi = context.getWikiAPI();
        if (wikiApi == null)
        {
            wikiApi = new WikiAPI(context.getWiki(), context);
        }
        return wikiApi;
    }

    private WikiDocumentAPI getWikiDocumentAPI(WikiRequestContext context, WikiDocument doc)
    {
        WikiDocumentAPI api = context.getWikiDocumentAPI();

        if (api == null )
        {
            WikiDocumentVO docVO = null;
            try
            {
                if(doc != null)
                {
                    docVO = doc.doGetVO();
                }
                api = new WikiDocumentAPI(docVO, context);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return api;
    }

    public WikiDocument getDocument(String fullname) throws WikiException
    {
        Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, fullname);
        if (obj != null)
        {
            return (WikiDocument) obj;
        }

        WikiDocument doc = new WikiDocument();
        doc.setUFullname(fullname);

        String hql = "from WikiDocument wd where LOWER(wd.UFullname) like '" + StringUtils.escapeSql(fullname.toLowerCase()) + "'";

        //		List<WikiDocument> alWd = wikiStore.findByExample(doc);
        List<WikiDocument> alWd = wikiStore.findWikiDocumentByHQL(hql);
        if (alWd != null && alWd.size() > 0)
        {
            doc = alWd.get(0);
            HibernateUtil.cacheDBObject(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, 
            							new String[] { "com.resolve.persistence.model.WikiDocument" }, fullname, doc);
        }
        else
        {
            doc = null;
        }

        return doc;
    }

    /**
     * This returns the doc name based on the values set or from the URL.
     *
     * @param wikiRequestContext
     * @return
     */
    public String getDocumentName(WikiRequestContext wikiRequestContext)
    {
        String docFullName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);

        //if the values are set in the context, then use that , else use the URL to get the wikidoc name.
        if (docFullName != null && docFullName.trim().length() > 0)
        {
            String arr[] = docFullName.split("\\.");
            String namespace = arr[0];
            String name = arr[1];
            String sectionName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_SECTION_KEY);//maybe already processed
            String urlParams = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_URL_PARAMS);//maybe already processed
            if (StringUtils.isEmpty(sectionName))
            {
                sectionName = ControllerUtil.getSectionNameFromDocName(name);
            }
            else
            {
                if (StringUtils.isEmpty(urlParams))
                {
                    if (sectionName.trim().indexOf("?") > -1)
                    {
                        sectionName = sectionName.trim();
                        urlParams = sectionName.substring(sectionName.indexOf("?") + 1);
                        sectionName = sectionName.substring(0, sectionName.indexOf("?"));
                    }
                }
            }
            String docname = ControllerUtil.getDocumentNameFrom(name);

            wikiRequestContext.setAttribute(ConstantValues.WIKI_NAMESPACE_KEY, namespace.trim());
            wikiRequestContext.setAttribute(ConstantValues.WIKI_DOCNAME_KEY, docname.trim());
            wikiRequestContext.setAttribute(ConstantValues.WIKI_SECTION_KEY, sectionName.trim());
            wikiRequestContext.setAttribute(ConstantValues.WIKI_URL_PARAMS, urlParams);
            wikiRequestContext.setAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY, namespace.trim() + "." + docname.trim());
            basicOperation(wikiRequestContext);
        }

        return docFullName;
    }//getDocumentName

    private void basicOperation(WikiRequestContext wikiRequestContext)
    {
        //UNLOCK the docs as the user can click anywhere
        String docFullName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        if (docFullName == null || docFullName.trim().length() == 0)
        {
            return;
        }

        String user = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        WikiAction wikiAction = (WikiAction) wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY);
        if (user != null)
        {
            if (wikiAction != null && wikiAction != WikiAction.edit && wikiAction != WikiAction.editabort && wikiAction != WikiAction.editexcpc && wikiAction != WikiAction.editfinal && wikiAction != WikiAction.editfinalc && wikiAction != WikiAction.editmodel && wikiAction != WikiAction.editmodelc && wikiAction != WikiAction.editdtree && wikiAction != WikiAction.save && wikiAction != WikiAction.saveexcpc && wikiAction != WikiAction.savefinalc && wikiAction != WikiAction.savemodelc
                            && wikiAction != WikiAction.savedtreec)
            {
                DocUtils.unlock(docFullName, user);
            }
        }
    }

    private void populateLinks(List<DocumentInfoVO> list, String querystring, int typeOfUrl)
    {
        //prepare the link and populate the object
        for (DocumentInfoVO dm : list)
        {
            dm.setViewLink(prepareForwardLink(dm.getNamespace(), dm.getDocumentName(), querystring, typeOfUrl));
        }
    }//populateLinks

    private String prepareForwardLink(String docNamespace, String docName, String queryString, int typeOfUrl)
    {
        docNamespace = ControllerUtil.encode(docNamespace.trim());
//        docName = ControllerUtil.encode(docName.trim());
        docName = docName.trim();

        String viewLink = "";
        String prefix = null;

        //decide what will be the prefix
        switch (typeOfUrl)
        {
            case 1: //creates the View link using the factory
                prefix = null;
                break;

            case 2: //"/resolve/jsp/rswiki.jsp?wiki=Test.test1"
                prefix = ConstantValues.LINK_RSWIKI_JSP;
                break;

            case 3: //rswiki.jsp?
                //					prefix = ConstantValues.RSWIKI_JSP;
                prefix = "rsclient.jsp?";
                break;
            default:
                prefix = null;
                break;
        }//end of switch

        if (prefix != null)
        {
            String docNameWIthQryString = ConstantValues.PARAMS_WIKI + "=" + docNamespace + "." + docName;
            if (queryString != null && queryString.trim().length() > 0)
            {
                docNameWIthQryString += "&" + queryString;
            }

            viewLink = "/resolve/jsp/" + prefix + docNameWIthQryString;
        }
        else
        {
            viewLink = wikiURLFactory.createForwardUrl(WikiAction.view, docNamespace, docName, queryString);
        }

        return viewLink;

    }//prepareForwardLink

    private String getQueryString(HttpServletRequest request)
    {
        String str = "";
        if (request != null)
        {
            Enumeration<String> enumNames = request.getParameterNames();
            while (enumNames.hasMoreElements())
            {
                String name = (String) enumNames.nextElement();
                String value = request.getParameter(name);

                if (str.trim().length() > 0)
                {
                    str = str + "&" + name + "=" + value;
                }
                else
                {
                    str = name + "=" + value;
                }
            }
        }

        return str;
    }

    /**
     * set the worksheet to be active based on the parameters
     * Check for the following
     * 	- REFERENCE
    	- ALERTID
    	- PROBLEMID
    	- PROBLEMNUMBER
     *
     * For PROBLEMID, the value can be NEW (i.e. to create a new worksheet), ACTIVE (get active worksheet) or Sys_Id
     *
     * @param wikiRequestContext
     */
    private void setWorksheetActive(WikiRequestContext wikiRequestContext)
    {
        HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
        Map<String, String> params = ControllerUtil.getParameterMap(request);

        if (request != null)
        {
            String user_id = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            String orgId = (String) wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_ID);
            String orgName = (String) wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_NAME);
            
            String id = null;
            params.put(Constants.HTTP_REQUEST_USERNAME, user_id);
            String PROBLEMID = params.get(WorksheetUtils.PROBLEMID);
            
            if (PROBLEMID != null && PROBLEMID.trim().length() > 0)
            {
                PROBLEMID = PROBLEMID.trim();

                //create a new worksheet
                if (PROBLEMID.equalsIgnoreCase("NEW"))
                {
                    //create a new worksheet
                    try
                    {
                        id = ServiceWorksheet.createNewWorksheet(params, user_id);
                    }
                    catch(Throwable t)
                    {
                        throw t;
                    }
                }
                else if (PROBLEMID.equalsIgnoreCase("ACTIVE"))
                {
                    //do nothing
                }
                else
                //its a id
                {
                    id = PROBLEMID;
                }
            }
            else
            {
                id = WorksheetUtils.getWorksheetByExampleCAS(params);
            }

            if (id != null)
            {
                //set it as ACTIVE
                try
                {
                    WorksheetUtils.setActiveWorksheet(user_id, id, orgId, orgName);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

    }

    public static String clean(String html)
    {
        StringWriter output = new StringWriter();

        try
        {
            //jericho
            Source source = new Source(html);
            SourceFormatter sf = new SourceFormatter(source);
            sf.setIndentString("    ");
            //            sf.setIndentAllElements(true);
            //            sf.setTidyTags(true);
            sf.writeTo(output);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return output.toString();
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Setters and Getters
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return the actionTaskRightService
     */
    public ActionTaskRightService getActionTaskRightService()
    {
        return actionTaskRightService;
    }

    /**
     * @param actionTaskRightService the actionTaskRightService to set
     */
    public void setActionTaskRightService(ActionTaskRightService actionTaskRightService)
    {
        this.actionTaskRightService = actionTaskRightService;
    }

    public WikiRenderingService getWikiRenderingService()
    {
    	return wikiRenderingService;
    }
    
    public void setWikiRenderingService(WikiRenderingService wikiRenderingService)
    {
    	this.wikiRenderingService = wikiRenderingService;
    }
    
    public WikiURLFactory getWikiURLFactory()
    {
        return wikiURLFactory;
    }

    public void setWikiURLFactory(WikiURLFactory wikiURLFactory)
    {
        this.wikiURLFactory = wikiURLFactory;
    }

    public WikiStore getWikiStore()
    {
        return wikiStore;
    }

    public void setWikiStore(WikiStore wikiStore)
    {
        this.wikiStore = wikiStore;
    }

    public FileAttachmentService getFileAttachmentService()
    {
        return fileAttachmentService;
    }

    public void setFileAttachmentService(FileAttachmentService fileAttachmentService)
    {
        this.fileAttachmentService = fileAttachmentService;
    }

    public WikiVelocityRenderer getWikiVelocityRenderer()
    {
        return wikiVelocityRenderer;
    }

    public void setWikiVelocityRenderer(WikiVelocityRenderer wikiVelocityRenderer)
    {
        this.wikiVelocityRenderer = wikiVelocityRenderer;
    }

    public WikiConverter getWikiConverter()
    {
        return wikiConverter;
    }

    public void setWikiConverter(WikiConverter wikiConverter)
    {
        this.wikiConverter = wikiConverter;
    }
}