/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.util.HttpUtil;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.radeox.macro.xref.XrefMapper;

import java.io.IOException;
import java.io.Writer;

/*
 * Macro that replaces {xref} with external URLS to xref source code
 */

public class XrefMacro extends BaseLocaleMacro
{
    private String[] paramDescription = { "1: class name, e.g. java.lang.Object or java.lang.Object@Nanning", "?2: line number" };

    public String[] getParamDescription()
    {
        return paramDescription;
    }

    public String getLocaleKey()
    {
        return "macro.xref";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        String project;
        String klass;
        int lineNumber = 0;

        if (params.getLength() >= 1)
        {
            klass = params.get("0");

            int index = klass.indexOf("@");
            if (index > 0)
            {
                project = klass.substring(index + 1);
                klass = klass.substring(0, index);
            }
            else
            {
                project = "Resolve";
            }
            if (params.getLength() == 2)
            {
                lineNumber = Integer.parseInt(params.get("1"));
            }
        }
        else
        {
            throw new IllegalArgumentException("xref macro needs one or two paramaters");
        }
        
        klass = HttpUtil.sanitizeValue(klass);
        project = HttpUtil.sanitizeValue(project);
        XrefMapper.getInstance().expand(writer, klass, project, lineNumber);
        return;
    }
}
