/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.util.HttpUtil;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

import java.io.IOException;
import java.io.Writer;

/*
 * Macro to display quotations from other sources. The output is wrapped usually
 * in <blockquote> to look like a quotation.
 * 
 */

public class QuoteMacro extends LocalePreserved
{
    private String[] paramDescription = { "?1: source", "?2: displayed description, default is Source" };

    public String[] getParamDescription()
    {
        return paramDescription;
    }

    public QuoteMacro()
    {
    }

    public String getLocaleKey()
    {
        return "macro.quote";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {

        writer.write("<blockquote class=\"quote\">");
        writer.write(HttpUtil.sanitizeValue(params.getContent()));
        String source = "Source"; // i18n
        if (params.getLength() == 2)
        {
            source = params.get(1);
            source= HttpUtil.sanitizeValue(source, HttpUtil.VALIDATOR_RESOLVETEXT , 500,HttpUtil.URL_ENCODING); 
        }
        
        // if more than one was present, we should show a description for the link
        if (params.getLength() > 0)
        {
            writer.write("<a href=\"" + HttpUtil.sanitizeValue(params.get(0), HttpUtil.VALIDATOR_RESOLVETEXT , 500,HttpUtil.URL_ENCODING) + "\">");
            writer.write(source);
            writer.write("</a>");
        }
        writer.write("</blockquote>");
    }
}
