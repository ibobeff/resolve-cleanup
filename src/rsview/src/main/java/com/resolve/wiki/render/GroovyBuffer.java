/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.render;

/**
 * This class collects the data based on the groovy script executed 
 * 
 * @author jeet.marwah
 *
 */
public class GroovyBuffer
{
	private StringBuffer OUT = new StringBuffer();

	public void println(String str)
	{
		OUT.append(str).append("\n");
	}
	
	@Override
	public String toString()
	{
		return OUT.toString();
	}
	
}
