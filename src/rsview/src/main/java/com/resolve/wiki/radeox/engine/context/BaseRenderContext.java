/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.engine.context;

import com.resolve.wiki.radeox.api.engine.RenderEngine;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;

import java.util.HashMap;
import java.util.Map;


/**
 * Base impementation for RenderContext
 *
 */

public class BaseRenderContext implements RenderContext {
  private boolean cacheable = true;
  private boolean tempCacheable = false;;

  private RenderEngine engine;
  private Map params;
  private Map values;

  public BaseRenderContext() {
    values = new HashMap();
  }

  public Object get(String key) {
    return values.get(key);
  }

  public void set(String key, Object value) {
    values.put(key, value);
  }

  public Map getParameters() {
    return params;
  }

  public void setParameters(Map parameters) {
    this.params = parameters;
  }

  public RenderEngine getRenderEngine() {
    return engine;
  }

  public void setRenderEngine(RenderEngine engine) {
    this.engine = engine;
  }

  public void setCacheable(boolean cacheable) {
    tempCacheable = cacheable;
  }

  public void commitCache() {
    cacheable = cacheable && tempCacheable;
    tempCacheable = false;
  }

  public boolean isCacheable() {
    return cacheable;
  }
}
