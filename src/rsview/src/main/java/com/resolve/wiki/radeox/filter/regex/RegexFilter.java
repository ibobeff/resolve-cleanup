/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.regex;

import java.util.ArrayList;
import java.util.List;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.filter.FilterSupport;
import com.resolve.wiki.radeox.filter.context.FilterContext;

/*
 * Class that stores regular expressions, can be subclassed for special Filters
 * 
 */

public abstract class RegexFilter extends FilterSupport
{
    protected List pattern = new ArrayList();
    protected List substitute = new ArrayList();

    public final static boolean SINGLELINE = false;
    public final static boolean MULTILINE = true;

    // TODO future use
    // private RegexService regexService;

    public RegexFilter()
    {
        super();
    }

    /**
     * create a new regular expression that takes input as multiple lines
     */
    public RegexFilter(String regex, String substitute)
    {
        this();
        addRegex(regex, substitute);
    }

    /**
     * create a new regular expression and set
     */
    public RegexFilter(String regex, String substitute, boolean multiline)
    {
        addRegex(regex, substitute, multiline);
    }

    public void clearRegex()
    {
        pattern.clear();
        substitute.clear();
    }

    public void addRegex(String regex, String substitute)
    {
        addRegex(regex, substitute, MULTILINE);
    }

    public void addRegex(String regex, String substitute, boolean multiline)
    {
        // compiler.compile(regex, (multiline ? Perl5Compiler.MULTILINE_MASK :
        // Perl5Compiler.SINGLELINE_MASK) | Perl5Compiler.READ_ONLY_MASK));
        try
        {
            com.resolve.wiki.radeox.regex.Compiler compiler = com.resolve.wiki.radeox.regex.Compiler.create();
            compiler.setMultiline(multiline);
            this.pattern.add(compiler.compile(regex));
            // Pattern.DOTALL
            this.substitute.add(substitute);
        }
        catch (Exception e)
        {
            Log.log.warn("bad pattern: " + regex + " -> " + substitute + " " + e);
        }
    }

    public abstract String filter(String input, FilterContext context);
}
