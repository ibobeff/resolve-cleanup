/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.attachment;

/**
 * Types of attachments that our application will support
 * - Can be in DB, which is a default
 * - can be on the filesystem which is mapped to the server
 * - can be in memory (we may not go this route)
 * 
 * @author jeet.marwah
 *
 */

public enum WikiAttachmentType
{
	DB,//database
	filesystem
}
