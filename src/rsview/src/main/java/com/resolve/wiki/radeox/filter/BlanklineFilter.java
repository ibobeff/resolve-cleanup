/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.LocaleRegexReplaceFilter;

/*
 * Blankline filter finds \n\n in its input and transforms this to <br/>
 */

public class BlanklineFilter extends LocaleRegexReplaceFilter implements CacheFilter
{
	public static final String LOCAL_KEY = "filter.blankline";

	@Override
	protected void setValues()
	{
	    addKeyValue(LOCAL_KEY + ".match", "((\r\n)|\n){2}");
        addKeyValue(LOCAL_KEY + ".print", "<br/><br/>");    //If you are here and are thinking there is too much space with 2 returns,
        //then you can remove the extra <br/>, but be warned that there is some DSLAM document that will not get the proper spacing, causing you to return
        //to this code in a week when someone else says that there is WAY too much spacing in between headers and the content.
        //'Hear, hear..'-- from another ui guy
	}
	

	protected String getLocaleKey()
	{
		return LOCAL_KEY;
	}
	//temporary hack.. need to improve
	@Override
	public String filter(String input, FilterContext context)
    {
        String result = input;
        Pattern p = Pattern.compile("\\n{2,}");
        Matcher m = p.matcher(result);
        String find = "";
        while(m.find()) {
            find = result.substring(m.start()+1,m.end());
            find = find.replace("\n", "<br/>");
            result = m.replaceFirst("\n" + find);
            m = p.matcher(result);
        }
        return result;
    }
}
