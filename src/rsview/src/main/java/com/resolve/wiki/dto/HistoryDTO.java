/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.dto;

import java.util.Date;


public class HistoryDTO
{
    private String documentName;
    private String type;
    
    private int version;
    private String comment;
    private String createdBy;
    private String createdOn;
    private Date sysCreatedOn;
    private Boolean isStable;

	public HistoryDTO(String documentName, int version, String type, String comment, String createdBy, String createdOn, Date sysCreatedOn, Boolean Stable)
    {
        this.documentName = documentName;
        this.version = version;
        this.type = type;
        this.comment = comment;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.sysCreatedOn = sysCreatedOn;
        this.isStable = Stable;
    }
    
    public String getDocumentName()
    {
        return documentName;
    }
    public void setDocumentName(String documentName)
    {
        this.documentName = documentName;
    }
    public int getVersion()
    {
        return version;
    }
    public void setVersion(int version)
    {
        this.version = version;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    public String getCreatedBy()
    {
        return createdBy;
    }
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    public String getCreatedOn()
    {
        return createdOn;
    }
    public void setCreatedOn(String createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getSysCreatedOn()
    {
        return sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

	public Boolean getIsStable() {
		return this.isStable;
	}

	public void setIsStable(Boolean isStable) {
		this.isStable = isStable;
	}
    
}
