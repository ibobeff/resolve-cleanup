/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.filter;

import com.resolve.services.util.ParseUtil;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.LocaleRegexTokenFilter;
import com.resolve.wiki.radeox.regex.MatchResult;

/**
 * 
 * This filter class is to cleanup the <script> tag with all the html tags in between as that creates issues in execution when rendering
 * the wiki document
 * 
 * @author jeet.marwah
 * 
 */
public class CleanupJavascriptFilter extends LocaleRegexTokenFilter
{

    public static final String LOCAL_KEY = "filter.cleanupjavascript";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "(?<!\\\\)\\<script[^>]*\"[^>]*>(.*?)</script>");
    }

    @Override
    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }

    @Override
    public void handleMatch(StringBuffer bufferAll, MatchResult result, FilterContext context)
    {
        String javascriptString = result.group(1);
        if (javascriptString != null && javascriptString.trim().length() > 0)
        {
            //cleanup the code
            javascriptString = ParseUtil.removeHTML(javascriptString);

            //add the javascript signature
            javascriptString = "<script type=\"text/javascript\">" + javascriptString + "</script>";

            //add it to the buffer
            bufferAll.append(javascriptString);
        }
    }
}
