/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro.code;

/*
 * SqlCodeFilter colourizes SQL source code
 *
 */

public class SqlCodeFormatter extends DefaultRegexCodeFormatter implements SourceCodeFormatter
{

	private static final String KEYWORDS = "\\b(SELECT|DELETE|UPDATE|WHERE|FROM|GROUP|BY|HAVING)\\b";

	private static final String OBJECTS = "\\b(VARCHAR)" + "\\b";

	private static final String QUOTES = "\"(([^\"\\\\]|\\.)*)\"";

	public SqlCodeFormatter()
	{
		super(QUOTES, "<span class=\"sql-quote\">\"$1\"</span>");
		addRegex(OBJECTS, "<span class=\"sql-object\">$1</span>");
		addRegex(KEYWORDS, "<span class=\"sql-keyword\">$1</span>");
	}

	public String getName()
	{
		return "sql";
	}

}