/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import com.resolve.wiki.radeox.filter.regex.LocaleRegexReplaceFilter;

/*
 * LineFilter finds ---- in its input and transforms this
 * to <hr/>
 *
 * @author stephan
 * @team sonicteam
 * @version $Id: LineFilter.java,v 1.4 2003/08/13 12:37:05 stephan Exp $
 */

public class LineFilter extends LocaleRegexReplaceFilter implements CacheFilter
{

	@Override
	protected void setValues()
	{
		addKeyValue("filter.line.match", "-----*");
		addKeyValue("filter.line.print", "<hr class=\"line\"/>");
	}

	protected String getLocaleKey()
	{
		return "filter.line";
	}
}