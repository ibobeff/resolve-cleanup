/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro.code;

import com.resolve.wiki.radeox.filter.context.FilterContext;

/**
 * Displays source code with syntax highlighting etc.
 * 
 */

public interface SourceCodeFormatter
{
	public String getName();

	public int getPriority();

	public String filter(String content, FilterContext context);
}
