/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.attachment;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.resolve.rsview.main.RSContext;
import com.resolve.services.ServiceWiki;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.services.util.ServletUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;

/**
 * 
 * Utility to upload and download a file from the server
 * 
 * @author jeet.marwah
 *
 */
public class FileUploadUtil
{
	private final static String LOCATION = RSContext.getResolveHome() + "rsexpert/";

	   
    /**
     * 
     * @param attachmentSysId - sysId of the wikiattachment table
     * @param username
     * @param request
     * @param response
     * @throws Exception
     */
    public static void downloadFile(HttpServletRequest request, HttpServletResponse response, String attachmentSysId, String username) throws Exception
    {
        WikiAttachmentVO attachment = ServiceWiki.getAttachmentWithContent(attachmentSysId, username); 
        if(attachment != null)
        {
            Log.log.info("User '" + username + "' downloaded attachment '" + attachment.getUFilename() + "'");
            Log.auth.info("User '" + username + "' downloaded attachment '" + attachment.getUFilename() + "'");
            
            response.setDateHeader("Last-Modified", attachment.getSysCreatedOn().getTime());
            
            ServletUtils.sendHeaders(attachment.getUFilename(), attachment.ugetWikiAttachmentContent().getUContent().length, 
                                     Arrays.copyOf(attachment.ugetWikiAttachmentContent().getUContent(), 
                                                   attachment.ugetWikiAttachmentContent().getUContent().length > 128 ? 
                                                   128 : attachment.ugetWikiAttachmentContent().getUContent().length), 
                                     request, response);
            response.getOutputStream().write(attachment.ugetWikiAttachmentContent().getUContent());
        }
    }
    
    public static void downloadFile(HttpServletRequest request, HttpServletResponse response, String fileName, String docName, String username) throws Exception
    {
        WikiAttachmentVO attachment = ServiceWiki.findWikiAttachmentVO(docName, null, fileName, true);
        if (attachment != null)
        {
            Log.log.info("User '" + username + "' downloaded attachment '" + attachment.getUFilename() + "'");
            Log.auth.info("User '" + username + "' downloaded attachment '" + attachment.getUFilename() + "'");

            response.setDateHeader("Last-Modified", attachment.getSysCreatedOn().getTime());
            
            ServletUtils.sendHeaders(attachment.getUFilename(), attachment.ugetWikiAttachmentContent().getUContent().length, 
                                     Arrays.copyOf(attachment.ugetWikiAttachmentContent().getUContent(), 
                                                   attachment.ugetWikiAttachmentContent().getUContent().length > 128 ? 
                                                   128 : attachment.ugetWikiAttachmentContent().getUContent().length), 
                                     request, response);
            response.getOutputStream().write(attachment.ugetWikiAttachmentContent().getUContent());
        }
    }
    
	/**
	 * Uploads the file and returns the name of the uploaded file
	 * 
	 * @param request
	 * @return
	 */
	public static String uploadFileInServer(HttpServletRequest request, List<FileItem> items)
	{
		String fileName = null;
		List<FileItem> fileItemList = null;
		// Check that we have a file upload request
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart)
		{
			//if not an upload request, then return 
			return fileName;
		}

		
		// Create a factory for disk-based file items
		FileItemFactory factory = new DiskFileItemFactory();

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		try
		{
			// Parse the request
		    if (items == null)
		    {
		        fileItemList = upload.parseRequest(request);
		    }
		    else
		    {
		        fileItemList = items;
		    }

			// Process the uploaded items
			Iterator<FileItem> iter = fileItemList.iterator();
			while (iter.hasNext())
			{
				FileItem item = (FileItem) iter.next();

				if (item.isFormField())
				{
					//					processFormField(item);
				}
				else
				{
					fileName = processUploadedFile(item);
					break;
				}
			}

		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
		
		return fileName;

	}

	private static String processUploadedFile(FileItem item) throws Exception
	{
		//few attributes for debuging purpose...may be removed/commented out 
		String fieldName = item.getFieldName();
		String fileName = item.getName();//C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
		fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);

		String contentType = item.getContentType();//text/plain
		boolean isInMemory = item.isInMemory();

		long sizeInBytes = item.getSize();

		File file = FileUtils.getFile(LOCATION, fileName);
		item.write(file);

		return fileName;
	}

}
