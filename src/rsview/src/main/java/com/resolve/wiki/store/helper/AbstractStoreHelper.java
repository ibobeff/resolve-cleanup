/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.store.helper;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.exception.WikiException;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.store.WikiStore;
import com.resolve.wiki.web.WikiRequestContext;

public abstract class AbstractStoreHelper
{
    protected Wiki wiki = null;
    protected WikiStore wikiStore = null;
    protected WikiRequestContext wikiRequestContext = null;
    
    protected String docName = null;
    protected String namespace = null;
    protected String wikiDocName = null;
    protected WikiAction wikiAction = null;
    protected String user = null;
    protected String browserTimeZone = null;

    protected boolean userHasAdminRole = false;
    protected WikiDocument wikiDoc = null;
    

    public AbstractStoreHelper(WikiRequestContext wikiRequestContext) throws WikiException
    {
        this.wikiRequestContext = wikiRequestContext;
        this.wiki = wikiRequestContext.getWiki();
        this.wikiStore = wiki.getWikiStore();
        
        docName = wiki.getDocumentName(wikiRequestContext);
        namespace = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
        wikiDocName = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        wikiAction = (WikiAction) wikiRequestContext.getAttribute(ConstantValues.WIKI_ACTION_KEY);
        user = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        browserTimeZone = wikiRequestContext.getAttribute(ConstantValues.BROWSER_CLIENT_TIMEZONE)!=null ? (String) wikiRequestContext.getAttribute(ConstantValues.BROWSER_CLIENT_TIMEZONE) : null;//GMT-07:00

        //set the flag if the user has admin role
        userHasAdminRole = doesUserHasAdminRole();
        
        //do the basic validation
        basicValidation();
        
        //load the document
        loadDocument();
    }//AbstractStoreHelper
    
    private boolean doesUserHasAdminRole()
    {
        return ServiceHibernate.isAdminUser(user);
    }//doesUserHasAdminRole
    
    private void basicValidation() throws WikiException
    {
        if(StringUtils.isEmpty(user))
        {
            throw new WikiException(new Exception("User info cannot be empty. Please verify."));
        }
        
        //validate the namespace
        validateNamespace();
        
        //if there is no error yet, check if the user has access right to do this 
        checkUserAccessRights();
        
        
    }//basicValidation
    
    private void validateNamespace() throws WikiException
    {
        if (StringUtils.isEmpty(namespace))
        {
            String message = "The document you are trying to edit is either a System document or you don't have access rights to it. Please select other document to edit.";
            throw new WikiException(new Exception(message));
        }
    }// validateNamespace    
    
    private void checkUserAccessRights()  throws WikiException
    {
        userHasAdminRole = ServiceWiki.hasRightsToDocument(docName, user, RightTypeEnum.edit);
        if(!userHasAdminRole)
        {
            String error = "User '" + user + "' does not have '" + "edit" + "' right on '" + docName + "' document.";
            throw new WikiException(error);
        }

//        userHasAdminRole = wiki.getWikiRightService().checkAccess(docName, wikiRequestContext);
    }// checkUserAccessRights();
    
    private void loadDocument() throws WikiException
    {
        wikiDoc = wiki.loadDocument(docName, false);
    }// loadDocument

}//AbstractStoreHelper
