/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.parameter;

import com.resolve.wiki.radeox.api.engine.context.RenderContext;

import java.util.Map;

/**
 * Encapsulates parameters for an executed Macro call
 *
 */

public interface MacroParameter {
  public void setParams(String stringParams);

  public String getContent();

  public void setContent(String content);

  public int getLength();

  public String get(String index, int idx);

  public String get(String index);

  public String get(int index);

  public Map getParams();

  public void setStart(int start);

  public void setEnd(int end);

  public int getStart();

  public int getEnd();

  public void setContentStart(int start);

  public void setContentEnd(int end);

  public int getContentStart();

  public int getContentEnd();

  public RenderContext getContext();
}
