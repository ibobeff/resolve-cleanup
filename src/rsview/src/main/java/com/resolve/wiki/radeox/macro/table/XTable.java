/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

/**
 * TODO: 
 * list down different date formats supported
 * list down different type available and supported --> string, date, float, int, boolean
 * 
 * {xtable} 
    Id:width=100&type=int|Company:width=100|Price:width=60&type=float|Date:width=50&type=date&dataformat=Y-m-d&format=d-m-Y|Visible:type=boolean&width=50|Size
    1|3m Co|71.72|2007-09-01|1|large
    2|AT&T Inc.|31.61|2008-02-01|1|extra large
    3|Aloca Inc|29.01|2007-08-01|0|medium
    {xtable} 
    
    {xtable} 
    col1:width=100&type=date&format=Y-m-d|col2:width=400|col3=:width=200
     data1|data2|data3
     data1|data2|data3
     data1|data2|data3
    {xtable}
    
    {xtable:sql=SELECT u_namespace, u_name, u_fullname, u_tag, u_has_active_model, URatingBoost, sys_created_on FROM wikidoc WHERE u_namespace = 'System'}
    Namespace, Name, Full Name:width=300|Tag:width=300|Active Model:type=boolean&width=100|Rating Boost:width=100&type=float|Created On:type=date&format=d-m-Y
    {xtable} 
 
    {xtable:target=_blank|drilldown=/resolve/service/wiki/view/^Namespace/^RB Name?test=val1|sql=SELECT u_namespace, u_name, u_fullname, u_tag, u_has_active_model, URatingBoost, sys_created_on FROM wikidoc WHERE u_namespace = 'Test'}
    Namespace|RB Name|Full Name:width=300|Tag:width=300|Active Model:type=boolean&width=100|Rating Boost:width=100&type=float|Created On:type=date&format=d-m-Y
    {xtable} 
    
     
    {xtable:sql=SELECT u_namespace, u_name, u_fullname, u_has_active_model, sys_created_on FROM wikidoc WHERE u_namespace = 'Test'}
    Namespace|Name|Full Name:width=300|Active Model:type=boolean&width=100|Created On:type=date&format=d-m-Y
    {xtable} 
 
 * 
 * @author jeet.marwah
 *
 */
public class XTable
{
    public static final String WIDTH = "width";
    public static final String TYPE = "type";
    public static final String FORMAT = "format";
    public static final String DATAFORMAT = "dataformat";
    public static final String SORTABLE = "sortable";
    public static final String FILTER = "filter";
    public static final String DRAGGABLE = "draggable";
    
//    public static final String DEFAULT_SQL_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String DEFAULT_SQL_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String DEFAULT_EXTJS_DATE_FORMAT = "c";
    public static final String DEFAULT_DATE_RENDERER_FORMAT = "m/d/Y g:i a";
    
    public static final String LINK_HEADING = "Link";
    
    public int width = -1;
    public int height = -1;
    public String sql = "";
    public boolean fitwidth = false;
    public String drilldown = "";
    public String target = "";
    public String content = "";
    
    private StringBuffer js = new StringBuffer();
    private String[] contentArr = null;
    private String divId = null;
    private String modelFieldsVar = null;
    private String columnsListVar = null;
    private String tableDataVar = null;
    private boolean addLinkColumn = false;

    private List<XColumn> columns = new ArrayList<XColumn>();
    private String[] colNames = null;
    private List<XRow> rows = new ArrayList<XRow>();
    
    public String getJavascriptDefinition()
    {
        if(StringUtils.isNotEmpty(content))
        {
            if(StringUtils.isNotEmpty(drilldown))
            {
//                this.drilldown = ControllerUtil.decode(this.drilldown);
                addLinkColumn = true;
            }
            
            //convert the content to array based on new line 
            contentArr = content.split("[\\r\\n]+");
            
            //prepare the header
            prepareHeaderColumns();
            
            //prepare the data
            prepareData();
            
            System.out.println("All data is ready now");
            
            //prepare js
            prepareJs();
        }//end of if

        return js.toString();
    }// getJavascriptDefinition
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///private methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private void prepareHeaderColumns()
    {
        //col1:width=100&type=date&format=Y-m-d|col2:width=400|col3=:width=200
        String headerStr = contentArr[0];
        StringTokenizer tokenizer = new StringTokenizer(headerStr, "|", false);
        
        //add a column for the drill down link
        if(addLinkColumn)
        {
            columns.add(prepareColumnForDrillDown());
        }
        
        while (tokenizer.hasMoreTokens()) 
        {
            //col1:width=100&type=date&format=Y-m-d
            String colConfig = tokenizer.nextToken();
            
            //prepare the col
            XColumn col = prepareColumn(colConfig);
            
            //add to the list
            columns.add(col);
            
        }//end of while loop
        
        //set the renderer - have to do after types are set for cols
        colNames = new String[columns.size()];
        for(int count = 0; count < columns.size(); count++)
        {
            XColumn col = columns.get(count);
            
            //add it to the cols array
            colNames[count] = col.dataIndex;
            
            //for date - set the renderer
            if(col.type.equalsIgnoreCase("date"))
            {
//                if(StringUtils.isNotBlank(col.format))
//                {
                    col.renderer = "Ext.util.Format.dateRenderer('" + col.format +"')";    
//                }
//                else
//                {
//                    col.renderer = "Ext.util.Format.dateRenderer('m/d/Y g:i a')";
//                }      
                
                if(StringUtils.isEmpty(col.dataformat))
                {
                    col.dataformat = DEFAULT_EXTJS_DATE_FORMAT;
                }
            }//for date
        }//end of for
    } //prepareHeaderColumns
    
    private void prepareData()
    {
        if(StringUtils.isNotEmpty(sql))
        {
            //qry to get the data
            prepareSqlData();                        
        }
        else
        {
            //use the content for data
            String[] dataArr = Arrays.copyOfRange(contentArr, 1, contentArr.length); 
            for(String rowData : dataArr)
            {
                XRow row = prepareRow(rowData);
                rows.add(row);
            }//end of for loop
        }
    } //prepareData
    
    private void prepareJs()
    {
        long uniqueId = JavaUtils.generateUniqueNumber();
        divId = "__xtable_" + uniqueId;
        modelFieldsVar = "__modelFields_" + uniqueId;
        columnsListVar = "__columnsList_" + uniqueId;
        tableDataVar = "__tableData_" + uniqueId;

        js.append("<div>").append("\n");
        js.append("<script type='text/javascript'>").append("\n");

        js.append(HttpUtil.sanitizeValue(getModelFields())).append("\n");
        js.append(HttpUtil.sanitizeValue(getColumnsList())).append("\n");
        js.append(HttpUtil.sanitizeValue(getTableData())).append("\n");
        js.append(getOnReadyFunction()).append("\n");

        js.append("</script>").append("\n");
        js.append("<div id='").append(divId).append("'></div></div>").append("\n");

    }//prepareJs
    
    private String getModelFields()
    {
        StringBuffer result = new StringBuffer();
        
        result.append("var ").append(modelFieldsVar).append(" = [").append("\n");
        
        for(int count=0; count < columns.size(); count++)
        {
            XColumn col = columns.get(count);
            
            result.append(getColumnField(col));
            
            if(count != columns.size() - 1)
            {
                result.append(",").append("\n");
            }
            else
            {
                result.append("\n");
            }
            
        }//end of for loop

        result.append("];").append("\n");
        
        return result.toString();
    }//getModelFields
    
    private String getColumnsList()
    {
        StringBuffer result = new StringBuffer();

        result.append("var ").append(columnsListVar).append(" = [").append("\n");
        for(int count=0; count < columns.size(); count++)
        {
            XColumn col = columns.get(count);
            
            if(count != columns.size() - 1)
            {
                result.append(getColumnList(col, false));
                result.append(",").append("\n");
            }
            else
            {
                //last col should not have width so that 'flex' can be applied to it and covers the rest of the space in the grid
                result.append(getColumnList(col, true));
                result.append("\n");
            }
        }//end of for loop
        
        result.append("];").append("\n");

        return result.toString();
    }//getColumnsList

    
    private String getTableData()
    {
        StringBuffer result = new StringBuffer();

        result.append("var ").append(tableDataVar).append(" = [").append("\n");
        for(int count=0; count < rows.size(); count++)
        {
            XRow row = rows.get(count);
            
            result.append(getData(row));
            
            if(count != rows.size() - 1)
            {
                result.append(",").append("\n");
            }
            else
            {
                result.append("\n");
            }
        }//end of for loop
        
        result.append("];").append("\n");
        
        return result.toString();
    }//getTableData

    private String getColumnField(XColumn col)
    {
        StringBuffer result = new StringBuffer();
        
        result.append("{").append("\n");
        result.append(" name : '").append(col.name).append("',").append("\n");
        result.append(" type : '").append(col.type).append("'");
        
        //for date, add the format also
        if("date".equalsIgnoreCase(col.type))
        {
            result.append(",").append("\n");
            result.append(" dateFormat : '").append(col.dataformat).append("'").append("\n");
        }
        else
        {
            result.append("\n");
        }
           
        result.append("}").append("\n");

        return result.toString();
    }//getColumnField

    private String getColumnList(XColumn col, boolean ignoreWidth)
    {
        StringBuffer result = new StringBuffer();
        
        result.append("{").append("\n");
        result.append(" dataIndex : '").append(col.dataIndex).append("',").append("\n");

        //if the type is a string, ignore the renderer as it will take a default one.
        if(!col.type.equalsIgnoreCase("string"))
        {
            result.append(" renderer : ").append(col.renderer).append(",").append("\n");
        }
        result.append(" filter : ").append(col.filter).append(",").append("\n");//all columns will have filter by default
        if(!col.sortable)
        {
            result.append(" sortable : ").append(col.sortable).append(",").append("\n");
        }
        if(!col.draggable)
        {
            result.append(" draggable : ").append(col.draggable).append(",").append("\n");
        }
        if(!ignoreWidth)
        {
            result.append(" width : ").append(col.width).append(",").append("\n");
        }
        result.append(" text : '").append(col.title).append("'").append("\n");
        result.append("}").append("\n");

        return result.toString();
    }//getColumnField
    
    private String getData(XRow row)
    {
        StringBuffer result = new StringBuffer();
        String drillDownLink = null;
        if(this.addLinkColumn)
        {
            drillDownLink = prepareDrillDownLink(row);
        }
        
        result.append("{").append("\n");
        
        for(int count=0; count< row.columnData.size(); count++)
        {
            Object o = row.columnData.get(count);
            
            //for link
            if(colNames[count].equalsIgnoreCase(LINK_HEADING))
            {
                result.append("'").append(colNames[count]).append("' : '").append(processDrillDownLink(o, drillDownLink)).append("'").append("\n");
            }
            else
            {
                result.append("'").append(colNames[count]).append("' : '").append(o).append("'").append("\n");
            }
            
            if(count != columns.size() - 1)
            {
                result.append(",").append("\n");
            }
            else
            {
                result.append("\n");
            }
        }//end of for loop
        
        result.append("}").append("\n");

        return result.toString();
    }//getData
    
    ///resolve/service/wiki/view/$col1/$col2?col1=val&....
    private String prepareDrillDownLink(XRow row)
    {
        //convert to title-value pair 
        Map<String, String> rowData = new HashMap<String, String>();
        for(int count=0; count< row.columnData.size(); count++)
        {
            Object o = row.columnData.get(count);
            if(o == null)
            {
                o = "";
            }
            
            rowData.put(colNames[count], o.toString());
        }//end of for loop
        
        Iterator<String> it = rowData.keySet().iterator();
        String modifiedDrillDownLink = this.drilldown;
        String qryStr = "1=1";
        while(it.hasNext())
        {
            String colName = it.next();
            String value = filterValue(rowData.get(colName));
            
            
            //prepare the qry string and exclude the link column
            if(!colName.equalsIgnoreCase(LINK_HEADING))
            {
                qryStr = qryStr + "&" + colName + "=" + value;
            }
            String searchKey = "\\^" + colName;
            
            modifiedDrillDownLink = modifiedDrillDownLink.replaceAll(searchKey, value);
        }//end of while loop
        
        if(modifiedDrillDownLink.indexOf('?') > -1)
        {
            modifiedDrillDownLink = modifiedDrillDownLink + "&" + qryStr;
        }
        else
        {
            modifiedDrillDownLink = modifiedDrillDownLink + "?" + qryStr;
        }
        
        StringBuffer result = new StringBuffer();
        result.append("<a href=\"");
        result.append(modifiedDrillDownLink);
        result.append("\" target=\"").append(target).append("\"><img src=\"/resolve/images/link.gif\"></img></a>");
        //<span class="wikilink"><a href="/resolve/service/wiki/view/Runbook/WebHome">3m Co</a></span>
        return result.toString();
    }
    
    //replace the place holder with the text
    private String processDrillDownLink(Object o, String drillDownLinkTemplate)
    {
        StringBuffer result = new StringBuffer();
        result.append(drillDownLinkTemplate);
        
        
        
        return result.toString();
        
    }
    
    private String getOnReadyFunction()
    {
        StringBuffer result = new StringBuffer();
        
        result.append("Ext.onReady(function() {").append("\n");              
        result.append("     var panel = Ext.create('RS.wiki.table.TablePanel', {").append("\n");  
        result.append("         modelFields : ").append(HttpUtil.sanitizeValue(modelFieldsVar)).append(",").append("\n");  
        result.append("         columnsList : ").append(HttpUtil.sanitizeValue(columnsListVar)).append(",").append("\n");  
        result.append("         tableData : ").append(HttpUtil.sanitizeValue(tableDataVar)).append(",").append("\n");
        
        if(width > 0)
        {
            result.append("         width : ").append(width).append(",").append("\n");
        }
        
        if(height > 0)
        {
            result.append("         height : ").append(height).append(",").append("\n");
        }
        
        if(fitwidth)
        {
            result.append("         fitwidth : ").append(fitwidth).append(",").append("\n");
        }
        
        result.append("         renderTo : '").append(divId).append("'").append("\n");  
        result.append("     });").append("\n");  
        result.append("});").append("\n");  
        
        return result.toString();
    }//getOnReadyFunction

    private XColumn prepareColumn(String colConfig)
    {
        //col1:width=100&type=date&format=Y-m-d
        XColumn col = new XColumn();
        if(colConfig.indexOf(':') > -1)
        {
            String[] colArr = colConfig.split(":");
            col.title = colArr[0];
            col.dataIndex = colArr[0];
            col.name = colArr[0];
            
            //add other attributes
            addOtherColAttributes(col, colArr[1]);
            
        }
        else
        {
            //only column title is present, so use defaults
            col.dataIndex = colConfig;
            col.name = colConfig;
            col.title = colConfig;
        }
        
        if(col.type.equalsIgnoreCase("date") && StringUtils.isEmpty(col.format))
        {
            col.format = DEFAULT_DATE_RENDERER_FORMAT;
        }
        
        return col;
    }
    
    private XColumn prepareColumnForDrillDown()
    {
        XColumn col = new XColumn();
        col.title = LINK_HEADING;
        col.dataIndex = col.title;
        col.name = col.title;
        col.width = 40;
        col.filter = false;
        
        return col;
    }//prepareColumnForDrillDown
    
    private XRow prepareRow(String rowData)
    {
        XRow row = new XRow();
        String[] arrColData = rowData.split("\\|");
        int dataArrLength = arrColData.length;
        if(addLinkColumn)
        {
            dataArrLength++;//add 1 if the link column needs to be added
            row.addData(drilldown);//add the first thing in the row 
        }
        
        if(dataArrLength != columns.size())
        {
            Log.log.error("throw exception as the col data not same as data");
            throw new RuntimeException("Column Headers does not match with the data columns. Please verify1.");
        }
        
        //add it to the list
        for(String data : arrColData)
        {
            row.addData(data.trim());
        }
        
        return row;
    }
    
    private void addOtherColAttributes(XColumn col, String colAttributes)
    {
        //width=100&type=date&format=Y-m-d&sortable=boolean&filter=boolean&draggable=boolean
        String[] arrAttributes = colAttributes.split("&");
        for(String att : arrAttributes)
        {
            String arr[] = att.split("=");
            if(WIDTH.equalsIgnoreCase(arr[0]))
            {
                try
                {
                    col.width = Integer.parseInt(arr[1]);
                }
                catch(Exception e)
                {
                    col.width = 100;
                }
            }
            else if(TYPE.equalsIgnoreCase(arr[0]))
            {
                col.type = arr[1]; 
            }
            else if(FORMAT.equalsIgnoreCase(arr[0]))
            {
                col.format = arr[1]; 
            }
            else if(DATAFORMAT.equalsIgnoreCase(arr[0]))
            {
                col.dataformat = arr[1];
            }
            else if(SORTABLE.equalsIgnoreCase(arr[0]))
            {
                col.sortable = Boolean.valueOf(arr[1]);
            }
            else if(FILTER.equalsIgnoreCase(arr[0]))
            {
                col.filter = Boolean.valueOf(arr[1]);
            }
            else if(DRAGGABLE.equalsIgnoreCase(arr[0]))
            {
                col.draggable = Boolean.valueOf(arr[1]);
            }
            
        }//end of for loop
        
    }//addOtherColAttributes
    
    private void prepareSqlData()
    {
        Connection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        ResultSetMetaData rsmd = null;
        
        try
        {
            connection = HibernateUtil.getConnection();
            
            statement = connection.prepareStatement(SQLUtils.getSafeSQL(sql));
            
            rs = statement.executeQuery();
            rsmd = rs.getMetaData();
            
            int numberOfColumns = rsmd.getColumnCount();
            if(addLinkColumn)
            {
                numberOfColumns++;//add 1 if the link column needs to be added
                
            }

            //check the columns
            if(numberOfColumns != columns.size())
            {
                Log.log.error("throw exception as the col data not same as data");
                throw new RuntimeException("Column Headers does not match with the data columns. Please verify.");
            }
            
            //prepare the list of column array so that data can be fetched 
            numberOfColumns = rsmd.getColumnCount();//reset the # of columns
            String[] dbColumnNames = new String[numberOfColumns];
            for(int count=0; count < numberOfColumns; count++)
            {
                //columns start with 1 , so count+1
                dbColumnNames[count] = rsmd.getColumnName(count+1);
            }

            //while loop to get the data 
            while(rs.next())
            {
                XRow row = new XRow();
                if(addLinkColumn)
                {
                    row.addData(drilldown);//add the first thing in the row
                }
                
                //add the data to the row
                for(int count=0; count < dbColumnNames.length; count++)
                {
                    Object rawdata = rs.getObject(dbColumnNames[count]);
                    if(rawdata == null)
                    {
                        rawdata = "";
                    }
                    
                    if(rawdata instanceof Timestamp)
                    {
                        String strDate = new SimpleDateFormat(DEFAULT_SQL_DATE_FORMAT).format(rawdata) + "-0000";
                        row.addData(strDate);
                    }
                    else
                    {
                        row.addData(rawdata);
                    }
                }
                
                if(row.columnData.size() > 0)
                {
                    this.rows.add(row);
                }
            }//end of while loop
            
            
        }
        catch(Throwable t)
        {
            Log.log.error("Error in XTAble :" + t.getMessage(), t);
            throw new RuntimeException(t.getMessage());
        }
        finally
        {

            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                
                if(statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }
        
    }//prepareSqlData();
    
    private String filterValue(String val)
    {
        String filterVal = "";
        
        //check the html if any
        if(StringUtils.isNotEmpty(val))
        {
            //eg --> <span class="wikilink"><a href="/resolve/service/wiki/view/Runbook/WebHome">3m Co</a></span>
            if(val.indexOf("</") > -1)
            {
                String temp = val.substring(0, val.indexOf("</"));
                filterVal = temp.substring(temp.lastIndexOf(">")+1);
            }
            else
            {
                filterVal = val;
            }
        }
        
        return filterVal;
    }
    
}//end of class

class XColumn
{
    //name = dataIndex
    public String dataIndex = "";
    public String title = "";
    public int width = 100;
    public String name = "";
    public String type = "string";//date, float, int, boolean
    public String dataformat = "";//format in which data is available
    public String format = "";
    public String renderer = "this.wrapText";//default for string, defined in the js file, depends on 'format'
    public boolean filter = true;
    public boolean sortable = true;
    public boolean draggable = true;
}

class XRow
{
    //it can be string, date, number, etc
    public List<Object> columnData = new ArrayList<Object>();
    
    public void addData(Object data)
    {
        columnData.add(data);
    }
}
