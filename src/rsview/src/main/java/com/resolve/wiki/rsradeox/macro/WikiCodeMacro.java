/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.CodeMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

public class WikiCodeMacro extends CodeMacro
{

	public WikiCodeMacro()
    {
        super();
    }

    public String getLocaleKey()
    {
        return "macro.code";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        String content = params.getContent();
        content = StringUtils.replace(content, "<", "&#60;");
        content = StringUtils.replace(content, ">", "&#62;");
        params.setContent(content);
        super.execute(writer, params);
    }
}
