/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.regex;

/*
 * Called with a MatchResult which is substituted
 *
 */

public interface Substitution {
  /**
   * When substituting matches in a matcher, the handleMatch method
   * of the supplied substitution is called with a MatchResult.
   * This method then does something with the match and replaces
   * the match with some output, like replace all 2*2 with (2*2 =) 4.
   *
   * @param buffer StringBuffer to append the output to
   * @param result MatchResult with the match
   */
    public void handleMatch(StringBuffer buffer, MatchResult result);
}