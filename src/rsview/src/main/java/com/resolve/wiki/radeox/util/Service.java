/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.owasp.esapi.ESAPI;

import com.resolve.util.FileUtils;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;

/**
 * After the Service class from Sun and the Apache project.
 * With help from Fr?d?ric Miserey.
 *
 * @credits Fr?d?ric Miserey, Joseph Oettinger
 * @author Matthias L. Jugel
 * @version $id$
 */
public class Service {

  static HashMap services = new HashMap();

  public static synchronized Iterator providerClasses(Class cls) {
    return providers(cls, false);
  }

  public static synchronized Iterator providers(Class cls) {
    return providers(cls, true);
  }

  public static synchronized Iterator providers(Class cls, boolean instantiate) {
    ClassLoader classLoader = cls.getClassLoader();
    String providerFile = "" + cls.getName();

    // check whether we already loaded the provider classes
    List providers = (List) services.get(providerFile);
    if (providers != null) {
      return providers.iterator();
    }

    // create new list of providers
    providers = new ArrayList();
    services.put(providerFile, providers);

    try {
      Enumeration providerFiles = classLoader.getResources(providerFile);

      if (providerFiles.hasMoreElements()) {
        // cycle through the provider files and load classes
        while (providerFiles.hasMoreElements()) {
          try {
            URL url = (URL) providerFiles.nextElement();
            Reader reader = new InputStreamReader(url.openStream(), "UTF-8");
            if (instantiate) {
              loadResource(reader, classLoader, providers);
            } else {
              loadClasses(reader, classLoader, providers);
            }
          } catch (Exception ex) {
            //ex.printStackTrace();
            // Just try the next file...
          }
        }
      } else {
        // Workaround for broken classloaders, e.g. Orion
    	File file = FileUtils.getFile(providerFile);
    	//System.out.println("in service.java -->" + file.getAbsolutePath());
//        InputStream is = classLoader.getResourceAsStream(providerFile);
    	InputStream is = new FileInputStream(file); 
        if (is == null) {
          providerFile = providerFile.substring(providerFile.lastIndexOf('.') + 1);
          is = classLoader.getResourceAsStream(providerFile);
        }
        if (is != null) {
          Reader reader = new InputStreamReader(is, "UTF-8");
          loadResource(reader, classLoader, providers);
        }
        
        // close
        if (is != null)
        {
            is.close();
        }
      }
    } catch (IOException ioe) {
      //ioLog.log.error(e.getMessage(), e);
      // ignore exception
    }
    return providers.iterator();
  }

  private static List loadClasses(Reader input, ClassLoader classLoader, List classes) throws IOException {
    BufferedReader reader = new BufferedReader(input);

    String line = reader.readLine();
    while (line != null) {
      try {
        // First strip any comment...
        int idx = line.indexOf('#');
        if (idx != -1) {
          line = line.substring(0, idx);
        }

        // Trim whitespace.
        line = line.trim();

        // load class if a line was left
        if (line.length() > 0) {
          // Try and load the class
          classes.add(classLoader.loadClass(HttpUtil.sanitizeValue(
              ESAPI.validator().getValidInput("Invalid class name ", line, "ResolveText", 65535, true)
          )));
        }
      } catch (Exception ex) {
    	 Log.log.error(ex.getMessage(), ex);
        // Just try the next line
      }
      line = reader.readLine();
    }
    return classes;
  }

  private static void loadResource(Reader input, ClassLoader classLoader, List providers) throws IOException {
    List classes = new ArrayList();
    loadClasses(input, classLoader, classes);
    Iterator iterator = classes.iterator();
    while (iterator.hasNext()) {
      Class klass = (Class) iterator.next();
      try {
        Object obj = klass.newInstance();
        // stick it into our vector...
        providers.add(obj);
      } catch (InstantiationException e) {
        Log.log.error(e.getMessage(), e);
      } catch (IllegalAccessException e) {
        Log.log.error(e.getMessage(), e);
      }
      //Logger.debug("Service: loaded "+ obj.getClass().getName());
    }
  }
}
