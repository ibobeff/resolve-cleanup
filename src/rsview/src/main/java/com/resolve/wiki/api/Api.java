/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.api;

import com.resolve.wiki.web.WikiRequestContext;

public class Api
{
	protected WikiRequestContext context;
	

	public Api(WikiRequestContext context)
    {
        this.context = context;
    }

	public WikiRequestContext getContext()
	{
		return context;
	}

}
