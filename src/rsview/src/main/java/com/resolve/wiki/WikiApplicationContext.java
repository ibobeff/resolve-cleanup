/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

import javax.servlet.ServletContext;

/**
 * The purpose of this context is to be used across the application.
 * 
 * @author jeet.marwah
 * 
 */

public interface WikiApplicationContext
{
	public Object getAttribute(String name);

	public void setAttribute(String name, Object object);

	public abstract void removeAttribute(String name);
	
	public void setServletContext(ServletContext sc);
	
	public ServletContext getServletContext();

}
