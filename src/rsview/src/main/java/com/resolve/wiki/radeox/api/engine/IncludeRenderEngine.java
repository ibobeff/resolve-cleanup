/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.api.engine;

/**
 * Interface for RenderEngines that allow to include content like wiki pages
 * or snips, e.g. with {!includeWiki} in MacroFilter
 *
 */

public interface IncludeRenderEngine {
    /**
   * Include an object in the input. This could be a
   * wiki page, snips, comments.
   *
   * @param name Name of the object to include, e.g. wiki page name
   * @return result A string representation of the included object
   */
  public String include(String name);
}
