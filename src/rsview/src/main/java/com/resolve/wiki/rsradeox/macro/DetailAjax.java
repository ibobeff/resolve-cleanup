/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import javax.servlet.http.HttpServletRequest;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.Constants;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.store.HibernateStoreUtil;

/**
 * 
 * This is a utility class that is called using the Ajax call from the UI. It works in combination with the DetailMacro which prepares the ExtJs code
 * and this call suffices that request from the client. That is the reason this utility class is in this package as they are together working 
 * to show the results on the wiki page. 
 * 
 * @author jeet.marwah
 *
 */
public class DetailAjax
{
    public String getHtmlForDetailWikiTag(Wiki wiki, HttpServletRequest request)
    {
        String divId = "wiki_results_" + JavaUtils.generateUniqueNumber();//making sure that the id is unique as the wiki page can have multiple {results} tag
        String currentUser = request.getParameter("currentUser");
        String problemId = request.getParameter("problemId");
        String orgId = request.getParameter(Constants.EXECUTE_ORG_ID);
        String orgName = request.getParameter(Constants.EXECUTE_ORG_NAME);
        
        if (StringUtils.isEmpty(problemId) || problemId.startsWith("$"))
        {
            problemId = HibernateStoreUtil.getActiveSid(currentUser, orgId, orgName);
        }

        String[] actionNamespaceArr = request.getParameterValues("actionNamespace");
        String[] actionNameArr = request.getParameterValues("actionName");
        String[] nodeIdArr = request.getParameterValues("nodeId");

        String type = request.getParameter("type");//if task then refresh based on Action task else Runbook
        String separator = request.getParameter("separator"); // separator between multiple detail results for the same actiontask
        String maxStr = request.getParameter("max");
        Integer max = null;
        String offsetStr = request.getParameter("offset");
        Integer offset = null;
        String refreshStartWiki = request.getParameter("refreshStartWiki");
        String isDoneYetId = request.getParameter("isDoneYetId");
        boolean doRefresh = true;
        if (StringUtils.isNotEmpty(type) && type.equalsIgnoreCase("task"))
        {
            doRefresh = ResultAjax.getDoRefreshValueForTask(wiki, problemId, actionNamespaceArr, actionNameArr);
        }
        else
        {
            doRefresh = ResultAjax.getDoRefreshValue(wiki, refreshStartWiki, problemId);
        }
        if (StringUtils.isNotEmpty(maxStr) && StringUtils.isNumeric(maxStr))
        {
            max = Integer.parseInt(maxStr);
        }
        if (StringUtils.isNotEmpty(offsetStr) && StringUtils.isNumeric(offsetStr))
        {
            offset = Integer.parseInt(offsetStr);
        }

        StringBuffer html = new StringBuffer();
        html.append("<div id='").append(divId).append("'>");
        html.append("<input type='hidden' name='isDoneYet' id='").append(isDoneYetId).append("' value='" + (doRefresh ? "" : ConstantValues.STOP_AJAX_REFRESH) + "'>");
        //get the data
        if (actionNameArr != null && actionNameArr.length > 0)
        {
            for (int count = 0; count < actionNamespaceArr.length; count++)
            {
                String actionNamespace = actionNamespaceArr[count];
                String actionName = actionNameArr[count];
                String nodeId = nodeIdArr[count];

                String task_id = ServiceHibernate.getActionIdFromFullname(actionNamespace + "#" + actionName, "system");

                //if the task does not exist, then no need to go furthur
                if (StringUtils.isNotEmpty(task_id))
                {
                    String output = ServiceHibernate.getActionResultDetail(problemId, task_id, nodeId, separator, max, offset);
                    html.append(output).append("\n");
                }

            }
        }
        else
        {
            //don't do anything as detail macro can have only 1 record to show
        }
        html.append("</div>");

        return html.toString();
    }
}
