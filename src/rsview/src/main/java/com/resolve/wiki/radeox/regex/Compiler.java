/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.regex;

/*
 * Class that compiles regular expressions to patterns
 *
 */

public abstract class Compiler {
  /**
   * Create a new Compiler object depending on the used implementation
   *
   * @return Compiler object with the used implementation
   */
  public static Compiler create() {
    return new JdkCompiler();
  }

  /**
   * Whether the compiler should create multiline patterns
   * or single line patterns.
   *
   * @param multiline True if the pattern is multiline, otherwise false
   */
  public abstract void setMultiline(boolean multiline);

  /**
   * Compile a String regular expression to a regex pattern
   *
   * @param regex String representation of a regular expression
   * @return Compiled regular expression
   */
  public abstract Pattern compile(String regex);
}