/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.util.logging;


/**
 * Concrete Logger that logs to System Out
 *
 */

public class SystemOutLogger implements LogHandler {
  public void log(String output) {
    System.out.println(output);
  }

  public void log(String output, Throwable e) {
    System.out.println(output);
    if (Logger.PRINT_STACKTRACE) e.printStackTrace(System.out);
  }

}
