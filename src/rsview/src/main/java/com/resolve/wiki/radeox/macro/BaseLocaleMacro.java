/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;

/*
 * Class that implements base functionality to write macros and reads it's name
 * from a locale file
 * 
 */

public abstract class BaseLocaleMacro extends BaseMacro implements LocaleMacro
{
    private String name;

    public String getName()
    {
        name = getLocaleKey();
        return name;
    }

    public void setInitialContext(InitialRenderContext context)
    {
        super.setInitialContext(context);
        description = getLocaleKey() + ".description";
    }
}
