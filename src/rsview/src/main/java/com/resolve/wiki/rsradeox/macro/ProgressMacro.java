/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.persistence.model.WikiAttachment;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

/*
 * Macro for defining and displaying tables. The rows of the table are devided
 * by newlins and the columns are divided by pipe symbols "|". The first line of
 * the table is rendered as column headers. {table} A|B|C 1|2|3 {table}
 */

public class ProgressMacro extends BaseLocaleMacro
{
    private String[] paramDescription = {};

    public String[] getParamDescription()
    {
        return paramDescription;
    }

    public String getLocaleKey()
    {
        return "macro.progress";
    }

    public void execute(Writer writer, MacroParameter params) throws IOException
    {
        RenderContext context = params.getContext();
        WikiRequestContext wikiRequestcontext = (WikiRequestContext) context.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestcontext.getWiki();

        String fullName = (String) context.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        String namespace = (String) context.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) context.get(ConstantValues.WIKI_DOCNAME_KEY);
        if (StringUtils.isEmpty(fullName))
        {
            fullName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
            namespace = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
            wikiDocName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        }
        fullName = HttpUtil.sanitizeValue(fullName, HttpUtil.VALIDATOR_FILENAME,200);
        namespace = HttpUtil.sanitizeValue(namespace, HttpUtil.VALIDATOR_FILENAME,200);
        wikiDocName = HttpUtil.sanitizeValue(wikiDocName, HttpUtil.VALIDATOR_FILENAME,200);

        String img = params.get("text", 0);
        img = HttpUtil.sanitizeValue(img, HttpUtil.VALIDATOR_FILENAME,200);
        String height = params.get("height");
        height = HttpUtil.sanitizeValue(height, HttpUtil.VALIDATOR_RESOLVENUMBER, 20);
        String width = params.get("width");
        width = HttpUtil.sanitizeValue(width, HttpUtil.VALIDATOR_RESOLVENUMBER, 20);
        String border = params.get("border") != null ? params.get("border") : "0";
        border = HttpUtil.sanitizeValue(border, HttpUtil.VALIDATOR_RESOLVENUMBER, 20);
        String displayText = params.get("displayText");
        displayText = HttpUtil.sanitizeValue(displayText);

        Log.log.trace("***************** Progress MACRO******************");
        Log.log.trace("doc fullname: " + fullName);
        Log.log.trace("doc namespace: " + namespace);
        Log.log.trace("doc wikiDocName: " + wikiDocName);
        Log.log.trace("doc img: " + img);

        WikiAttachment att = wiki.getWikiStore().getAttachmentFor(fullName, img);
        StringBuffer str = new StringBuffer();

        // Write out the progress bar script
        writer.write(getProgressLoadingIndicator());

        String url = "/resolve/images/loading.gif";

        // Now write out the image to use the script
        if (att != null)
        {
            String querystring = ConstantValues.WIKI_PARAMETER_ATTACHMENT_FILE_NAME + "=" + img + "&" + ConstantValues.WIKI_PARAMETER_ATTACHMENT_SYS_ID + "=" + att.getSys_id();
            url = wiki.getWikiURLFactory().createForwardUrl(WikiAction.download, namespace, wikiDocName, querystring);
        }

        str.append("<div id=\"showReload\">");
        str.append("<img ");
        str.append("onLoad=\"checkReloading();\" ");
        str.append("border=\"" + border + "\" ");
        str.append("src=\"");
        str.append(url);
        str.append("\" ");
        if ((!"none".equals(height)) && (height != null) && (!"".equals(height.trim())))
        {
            str.append("height=\"" + height.trim() + "\" ");
        }
        else
        {
            str.append("height=\"16px\" ");
        }
        if ((!"none".equals(width)) && (width != null) && (!"".equals(width.trim())))
        {
            str.append("width=\"" + width.trim() + "\" ");
        }
        else
        {
            str.append("width=\"16px\" ");
        }
        str.append("alt=\"");
        str.append(img);
        str.append("\" />");

        if (displayText != null)
        {
            str.append(displayText);
        }

        str.append("</div>");

        Log.log.trace(str);

        writer.write(str.toString());
        return;
    }

    public String getProgressLoadingIndicator()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("<script>\n");
        sb.append("function checkReloading() {\n");
        sb.append("var refresh = document.getElementsByName(\"isDoneYet\");\n");
        sb.append("var n = document.getElementById(\"showReload\");\n");
        sb.append("if (refresh.length > 0 && refresh.item(0).value == \"STOP_REFRESH\")\n");
        sb.append("{\n");
        sb.append("  n.style.visibility='hidden';\n");
        sb.append("}\n");
        sb.append("else\n");
        sb.append("{\n");
        sb.append("  n.style.visibility= 'visible';\n");
        sb.append("  setTimeout(checkReloading, 5000);\n");
        sb.append("}\n");
        sb.append("}\n");
        sb.append("</script>\n");
        return sb.toString();
    }
}
