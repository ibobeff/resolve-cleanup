/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.api.engine.context;

/**
 * IntialRenderContext tells a Filter how to behave at initializiation. For example the Filter can read pattern locales for it's pattern matching.
 * 
 */

public interface InitialRenderContext extends RenderContext
{
//	public String getPropertiesFilename();
}
