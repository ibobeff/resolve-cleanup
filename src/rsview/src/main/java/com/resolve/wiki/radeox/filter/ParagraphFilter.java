/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import com.resolve.wiki.radeox.filter.regex.LocaleRegexReplaceFilter;

/*
 * The paragraph filter finds any text between two empty lines and inserts a <p/>
 */

public class ParagraphFilter extends LocaleRegexReplaceFilter implements CacheFilter
{

    public static final String LOCAL_KEY = "filter.paragraph";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "([ \t\r]*[\n]){2}");
        addKeyValue(LOCAL_KEY + ".print", "<p> </p>");
    }

    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }

    protected boolean isSingleLine()
    {
        return true;
    }
}
