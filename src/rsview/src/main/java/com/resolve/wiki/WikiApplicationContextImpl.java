/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

import java.util.HashMap;

import javax.servlet.ServletContext;

/**
 * The purpose of this context is to be used across the application.
 * 
 * @author jeet.marwah
 * 
 */
public class WikiApplicationContextImpl implements WikiApplicationContext
{
	private HashMap<String, Object> hm = new HashMap<String, Object>();
	private ServletContext sc = null;

	public Object getAttribute(String name)
	{
		return hm.get(name);
	}

	@Override
	public synchronized void setAttribute(String name, Object object)
	{
		hm.put(name, object);
	}

	@Override
	public synchronized void removeAttribute(String name)
	{
		hm.remove(name);
	}

	@Override
	public ServletContext getServletContext()
	{
		return sc;
	}

	@Override
	public void setServletContext(ServletContext sc)
	{
		this.sc = sc;

	}

}//end of class
