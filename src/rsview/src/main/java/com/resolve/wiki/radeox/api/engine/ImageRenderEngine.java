/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.api.engine;

/**
 * RenderEngine interface for RenderEngines that know how to handle images,
 * e.g. small arrows for external links in the {link...} macro.
 *
 */

public interface ImageRenderEngine {
  /**
   * Get a link to an image. This can be used by filters or
   * macros to get images for e.g. external links or icons
   * Should be refactored to get other images as well
   *
   * @return result String with an HTML link to an image
   */
  public String getExternalImageLink();
}
