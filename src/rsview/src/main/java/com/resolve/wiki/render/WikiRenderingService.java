/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.render;

import java.util.List;
import java.util.Map;

import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.WikiException;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This is a singleton class that represents a rendering engine. The purpose of
 * this engine is to provide APIs to render the wiki doc
 * 
 * @author jeet.marwah
 * 
 */
public class WikiRenderingService
{

    private static WikiRenderingService instance;
    private List<WikiRenderer> allRenderers = null;

    // only 1 instance of this should be available
    private WikiRenderingService()
    {
        // setAllRenderes();
    }

    public static WikiRenderingService getInstance()
    {
        if (instance == null)
        {
            synchronized (WikiRenderingService.class)
            {
                if (instance == null)
                {
                    instance = new WikiRenderingService();
                }
            }
        }

        return instance;
    }

    /**
     * This method will set the list of all the renderers which will be applied
     * to the Wiki docs one by one.
     * 
     * @return
     */
    // TODO: we may want to change it by - added a sequence # that we want to
    // exe it - sort it on that seq # - then return the list after sorting
    // private void setAllRenderes()
    // {
    // allRenderers = new ArrayList<WikiRenderer>();
    // allRenderers.add(new WikiVelocityRenderer());//adding the velocity
    // renderer
    // // allRenderers.add("com.resolve.wiki.render.");
    // // allRenderers.add("com.resolve.wiki.render.");
    //
    // }

    /**
     * This method will render the content of wiki doc and return an HTML to be
     * sent to the client
     * 
     * @param wikiDoc
     * @param context
     * @return
     */
    // public String renderText(WikiDocument wikiDoc, WikiRequestContext
    // context) throws WikiException
    public String renderText(String content, Map<String, String> values, WikiRequestContext context) throws WikiException
    {
        String result = content;
        
//        result = preRenderingOperation(result);

        for (WikiRenderer renderer : allRenderers)
        {
            result = renderer.preRender(result, values, context);
            result = renderer.render(result, values, context);
            result = renderer.postRender(result, values, context);
        }
        //
        // //This is just for testing the Radeox
        // WikiRadeoxRender wr = (WikiRadeoxRender)
        // RSContext.appContext.getBean("wikiRadeoxRender");
        // result = wr.render(result, wikiDoc, context);

        // //////////////////////

        return result;

    }

    /**
     * This key is stored in the cache so that when ANY USER tries to view this
     * WIKI doc, it pulls up the rendered text which will be give better
     * performance.
     * 
     * @param wikiDoc
     * @param context
     * @return
     */
    private String getUniqueKeyForCache(Map<String, String> values, WikiRequestContext context)
    {
        // make the key with DOC NAME
        String key = values.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        return key;
    }
    
//    private String preRenderingOperation(String content)
//    {
//        String result = content;
//        
//        if(StringUtils.isNotEmpty(result))
//        {
//            //for {code} macro, change it to {code}{pre} to avoid any processing in it.
//            result = result.indexOf("{code}") > -1 ? ParseUtil.preProcessCodeMacro(result) : result;
//        }
//        
//        return result;
//    }


    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Getter and Setters
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public List<WikiRenderer> getAllRenderers()
    {
        return allRenderers;
    }

    public void setAllRenderers(List<WikiRenderer> allRenderers)
    {
        this.allRenderers = allRenderers;
    }

}
