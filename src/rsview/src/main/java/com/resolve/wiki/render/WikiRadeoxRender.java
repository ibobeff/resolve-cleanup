/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.render;

import java.util.Map;

import com.resolve.services.util.ParseUtil;
import com.resolve.util.StringUtils;
import com.resolve.wiki.render.substitution.PreTagSubstitution;
import com.resolve.wiki.rsradeox.RadeoxProcessor;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * The purpose of this class is to render the wiki doc using the radeox package
 * 
 */
public class WikiRadeoxRender implements WikiRenderer
{

    // injected property
    private RadeoxProcessor radeoxProcessor;

    @Override
    public String render(String content, Map<String, String> values, WikiRequestContext context)
    {
        String result = content;

        PreTagSubstitution preTagSubstitution = new PreTagSubstitution();

        // do not process the {pre} tag. So substitute with some non -wiki tag
        result = preTagSubstitution.replacePreTagWithNonWikiText(result);
        
        result = preTagSubstitution.replaceCodeTagWithNonWikiText(result);

        // forward the content to the radeox engine for processing
        result = radeoxProcessor.renderRadeox(result, values, context, null);

        // set the values back for the {pre} tag
        result = preTagSubstitution.replaceNonWikiTextWithPreTag(result);

        return result;
    }
    
    private String preProcessBeforePreTagOperation(String content)
    {
        String result = content;
        
        if(StringUtils.isNotEmpty(result))
        {
            //for {code} macro, change it to {code}{pre} to avoid any processing in it.
            result = result.indexOf("{code}") > -1 ? ParseUtil.preProcessCodeMacro(result) : result;
        }
        
        return result;
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Getters and Setters
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public RadeoxProcessor getRadeoxProcessor()
    {
        return radeoxProcessor;
    }

    public void setRadeoxProcessor(RadeoxProcessor radeoxProcessor)
    {
        this.radeoxProcessor = radeoxProcessor;
    }

    @Override
    public String preRender(String content, Map<String, String> values, WikiRequestContext context)
    {
        return content;
    }

    @Override
    public String postRender(String content, Map<String, String> values, WikiRequestContext context)
    {
        return content;
    }

}
