/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox;

import java.util.Map;

import com.resolve.wiki.radeox.api.engine.RenderEngine;

/**
 * Access point to dock several different rendering engines into 
 * 
 * This is the point of entry to get the handle for the rendering engine.
 * 
 * @author jeet.marwah
 * 
 */
public class EngineManager
{
	//map the engine names in the spring context,this is used to grab the engine from the hashmap 
	public static final String WIKI_DEFAULT = "wikiEngine";//default engine

	private Map<String, RenderEngine> availableEngines = null;//new HashMap<String, RenderEngine>();
	
	private static EngineManager instance = null;


	private EngineManager(){}
	
	public static EngineManager getInstance()
	{
		if (instance == null)
		{
			synchronized (EngineManager.class)
			{
				if (instance == null)
				{
					instance = new EngineManager();
				}
			}
		}

		return instance;
	}
	/**
	 * Get an instance of a RenderEngine. This is a factory method.
	 * 
	 * @param name
	 *            Name of the RenderEngine to get
	 * @return engine RenderEngine for the requested name
	 */
	public synchronized RenderEngine getRenderEngineInstance(String name)
	{
		return (RenderEngine) availableEngines.get(name);
	}

	/**
	 * Get an instance of a RenderEngine. This is a factory method. Defaults to a default RenderEngine. Currently this is a basic EngineManager with no additional features that is distributed with Radeox.
	 * 
	 * @return engine default RenderEngine
	 */
	public synchronized RenderEngine getRenderEngineInstance()
	{
		return (RenderEngine) availableEngines.get(WIKI_DEFAULT);
	}

	public static String getVersion()
	{
		return "3.0";
	}

	public Map<String, RenderEngine> getAvailableEngines()
	{
		return availableEngines;
	}

	public void setAvailableEngines(Map<String, RenderEngine> availableEngines)
	{
		this.availableEngines = availableEngines;
	}
}
