/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.table;

import com.resolve.util.Log;

/**
 * A function that summerizes table cells
 * 
 */

public class SumFunction implements Function
{
    public String getName()
    {
        return "SUM";
    }

    public void execute(Table table, int posx, int posy, int startX, int startY, int endX, int endY)
    {
        float sum = 0;
        boolean floating = false;
        for (int x = startX; x <= endX; x++)
        {
            for (int y = startY; y <= endY; y++)
            {
                // Logger.debug("x="+x+" y="+y+" >"+getXY(x,y));
                try
                {
                    sum += Integer.parseInt((String) table.getXY(x, y));
                }
                catch (Exception e)
                {
                    try
                    {
                        sum += Float.parseFloat((String) table.getXY(x, y));
                        floating = true;
                    }
                    catch (NumberFormatException e1)
                    {
                        Log.log.debug("SumFunction: unable to parse " + table.getXY(x, y));
                    }
                }
            }
        }
        // Logger.debug("Sum="+sum);
        if (floating)
        {
            table.setXY(posx, posy, "" + sum);
        }
        else
        {
            table.setXY(posx, posy, "" + (int) sum);
        }
    }

}
