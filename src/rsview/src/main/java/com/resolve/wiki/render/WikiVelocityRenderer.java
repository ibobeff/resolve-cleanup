/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.wiki.render;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.generic.EscapeTool;
import org.apache.velocity.tools.view.tools.ImportTool;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.util.RenderJspUtil;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.HttpUtil;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.api.WikiAPI;
import com.resolve.wiki.api.WikiDocumentAPI;
import com.resolve.wiki.store.helper.StoreUtility;
import com.resolve.wiki.web.WikiRequestContext;

public class WikiVelocityRenderer implements WikiRenderer
{
	private static final String CLEARED_CACHED_WIKIDOC_FROM_DB_CACHE_REGION_LOG = 
																			"Cleared %s from Hibernate DB cache region %s";
    private static VelocityEngine velocityEngine = null;
    //private VelocityConfigurer velocityConfigurer;

    private Map<String, String> noVelocityBlocks = new ConcurrentHashMap<String, String>();

    private static Map<String, String> cacheForStaticStrings = new HashMap<String, String>();

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Constructors
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public WikiVelocityRenderer()
    {
        if (velocityEngine == null)
        {
            Properties velocityProperties = new Properties();
            
            try
            {
                String webinf = Paths.get("").toAbsolutePath().toString();
                if (webinf.contains("tomcat"))
                {
                    webinf += "/webapps/resolve/WEB-INF/";
                }
                else
                {
                    webinf += "/tomcat/webapps/resolve/WEB-INF/";
                }
                String file = webinf + "velocity.properties";
                InputStream is = new FileInputStream(file);
                velocityProperties.load(is);
                velocityProperties.setProperty("file.resource.loader.path", webinf + "classes");
                velocityEngine = new VelocityEngine(velocityProperties);
            }
            catch (Exception e)
            { 
                e.printStackTrace();
            }
        }
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Implemented methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * This is the method that gets called and does the real magic
     */
    @Override
    public String render(String content, Map<String, String> values, WikiRequestContext context)
    {
        String result = "";
        String tagName = values.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);// wikiDoc.getFullname();//full
                                                                          // name
                                                                          // of
                                                                          // the
                                                                          // wiki
                                                                          // doc
        result = evaluate(content, tagName, context, values);// rendered content
                                                             // of the wiki doc
        return result;
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Other Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Velocity method that does all the rendering work
     */
    public String evaluate(String content, String name, WikiRequestContext context, Map<String, String> values)
    {
        String result = "";

        try
        {
            StringWriter writer = new StringWriter();
            VelocityContext vcontext = prepareContext(context, values);
            velocityEngine.evaluate(vcontext, writer, System.currentTimeMillis() + "", new StringReader(content));
            result = writer.toString();
        }
        catch (Exception e)
        {
            com.resolve.util.Log.log.error("Error rendering name: " + name + ". " + e.getMessage(), e);
            Log.log.error(e.getMessage(), e);
            String message = e.getMessage();
            if (message != null)
            {
                message = StringUtils.escapeHtml(message);
                message = message.replaceAll("\n", "<br>");
                message = message.replaceAll(" ", "&nbsp");
            }
            result = content + "<br><hr><br>{pre}Velocity Exception: " + message + "{pre}";
        }
        return result;
    } // evaluate

    /**
     * This method prepares the Velocity context.
     * 
     * @throws IntrusionException
     * @throws ValidationException
     */
    private VelocityContext prepareContext(WikiRequestContext context, Map<String, String> values) throws ValidationException, IntrusionException
    {
        VelocityContext vcontext = context.getVelocityContext();
        if (values == null)
        {
            values = new HashMap<String, String>();
        }

        if (vcontext == null)
        {
            WikiDocumentAPI docAPI = context.getWikiDocumentAPI();
            WikiAPI wikiAPI = context.getWikiAPI();
            // String urlParams = (String)
            // context.getAttribute(ConstantValues.WIKI_URL_PARAMS);
            // if(StringUtils.isNotEmpty(urlParams))
            // {
            // values.putAll(StringUtils.urlToMap(urlParams));
            // }

            vcontext = new VelocityContext();
            vcontext.put("esc", new EscapeTool());
            vcontext.put("import", new ImportTool());
            vcontext.put("jutils", new JspUtils());
            ImportTool it = new ImportTool();

            String moduleLoaderJsp = "";
            if (cacheForStaticStrings.containsKey("module-loader.jsp"))
            {
                moduleLoaderJsp = cacheForStaticStrings.get("module-loader.jsp");
            }
            else
            {
                moduleLoaderJsp = RenderJspUtil.renderJsp("/client/module-loader.jsp", context.getHttpServletRequest(), context.getHttpServletResponse());
                cacheForStaticStrings.put("module-loader.jsp", moduleLoaderJsp);
            }

            if (StringUtils.isEmpty(moduleLoaderJsp))
            {
                moduleLoaderJsp = " ";
            }
            vcontext.put("commonLoader", moduleLoaderJsp);

            StringBuilder cssInclude = new StringBuilder();

            // String allPath =
            // context.getHttpServletRequest().getSession().getServletContext().getRealPath("css/wikiOverrides/all");
            HttpServletRequest httpsr = context.getHttpServletRequest();
            String allPath = httpsr.getSession().getServletContext().getRealPath("/css/wikiOverrides/all");
//            HttpSession httpsession = httpsr.getSession();
//            ServletContext httpsc = httpsession.getServletContext();
//            String allPath = httpsc.getRealPath("/css/wikiOverrides/all");

            // String allPath =
            // context.getHttpServletRequest().getSession().getServletContext().getRealPath("/css/wikiOverrides/all");
            File all = new File(allPath);
            if (all.isDirectory())
            {
                for (File f : all.listFiles(new FilenameFilter()
                {
                    public boolean accept(File dir, String name)
                    {
                        return name.toLowerCase().endsWith(".css");
                    }
                }))
                {
                    cssInclude.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/resolve/css/wikiOverrides/all/" + f.getName() + "\"/>");
                }
            }

            // String systemPath =
            // context.getHttpServletRequest().getSession().getServletContext().getRealPath("css/wikiOverrides/system.css");
            String systemPath = context.getHttpServletRequest().getSession().getServletContext().getRealPath("/css/wikiOverrides/system.css");
            File system = new File(systemPath);
            if (system.exists())
            {
                cssInclude.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/resolve/css/wikiOverrides/system.css\"/>");
            }

            String namespacePath = context.getHttpServletRequest().getSession().getServletContext().getRealPath("/css/wikiOverrides/" + docAPI.getNamespace() + ".css");
            File namespace = FileUtils.getFile(namespacePath);
            // File namespace = new File(namespacePath);
            if (namespace.exists())
            {
                cssInclude.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/resolve/css/wikiOverrides/" + docAPI.getNamespace() + ".css\"/>");
            }

            vcontext.put("cssInclude", cssInclude.toString());

            // vcontext.put("formatter", new VelocityFormatter(vcontext));//will
            // refactor this obj later on for 1.6 version of velocity

            // vcontext.put("response", context.getHttpServletResponse());
            // FOR BACKWARD COMPATIBILITY
            vcontext.put(ConstantValues.WIKI_NAME_KEY, wikiAPI);// context.getWiki());
            vcontext.put(ConstantValues.WIKI_DOC_KEY, docAPI);
            vcontext.put(ConstantValues.WIKI_USER_KEY, context.getWikiUser());// pass
                                                                              // the
                                                                              // UserAccessRights
                                                                              // Object
                                                                              // in
                                                                              // the
                                                                              // Velocity
                                                                              // Renderer
            vcontext.put(ConstantValues.WIKI_HTTP_REQUEST, context.getHttpServletRequest());
            vcontext.put(Constants.GROOVY_BINDING_ESB, MainBase.getESB());

            // vcontext.put(ConstantValues.MAIN_WIKI_DOC_TO_VIEW_KEY,
            // context.getAttribute(ConstantValues.MAIN_WIKI_DOC_TO_VIEW_KEY));
            // vcontext.put(ConstantValues.REVISION_NO_KEY,
            // context.getAttribute(ConstantValues.REVISION_NO_KEY));

            // adding the reference of WikiRequestContext. I was trying to avoid
            // this to reduce the interdependcy between the VelocityContext and
            // WikiRequestContext
            // but I guess its not avoidable. There are objects like HttpRequest
            // that is required by the app which the wikiContext has
            String problemId = (String) context.getAttribute(Constants.EXECUTE_PROBLEMID);
            if (StringUtils.isNotBlank(problemId)) {
            	problemId = HttpUtil.validateInput(problemId);
            }

            vcontext.put(ConstantValues.WIKI_CONTEXT, context);
            vcontext.put(Constants.WIKI_CONTEXT_VELOCITY, context);// for making
                                                                   // it same as
                                                                   // in Groovy

            // new references
            vcontext.put(Constants.WIKI, wikiAPI);// context.getWiki());
            vcontext.put(Constants.WIKIDOCUMENT, docAPI);
            vcontext.put(Constants.GROOVY_HTTP_REQUEST, context.getHttpServletRequest());

            if (values != null)
            {
                for (Map.Entry entry : values.entrySet())
                {
                    vcontext.put((String) entry.getKey(), entry.getValue());
                }
            }

            // Put the Velocity Context in the context so that includes can use
            // it..
            context.setVelocityContext(vcontext);
        }

        // {USERNAME=admin, docname=Test Velocity 2, mainWikiDocToView=Test.Test
        // Velocity 2, fullname=Test.Test Velocity 2, namespace=Test}
        String currDocName = values.get(ConstantValues.MAIN_WIKI_DOC_TO_VIEW_KEY);
        if (StringUtils.isNotEmpty(currDocName))
        {
            try
            {
                WikiDocument currDoc = StoreUtility.find(currDocName);
                
                if (currDoc != null)
                {
                    WikiDocumentAPI currWikidocApi = new WikiDocumentAPI(currDoc.doGetVO(), context);
    
                    vcontext.put(ConstantValues.WIKI_CURRDOC_KEY, currWikidocApi);
                    context.setCurrWikiDocumentAPI(currWikidocApi);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error looking up document :" + currDocName, e);
            }
        }

        return vcontext;
    } // prepareContext
    
    private String updateContentAndReset(Pattern p, Matcher m, String content, WikiRequestContext context)
    {
        String updatedContent = content;
        
        StringBuilder sb = new StringBuilder();
        
        int fromIndx = 0;
        
        while (content.indexOf("<novelocity", fromIndx) >= fromIndx)
        {
            int toIndx = content.indexOf("<novelocity", fromIndx);
            
            sb.append(content.substring(fromIndx, toIndx));
            
            int indxOfTagEnd = content.indexOf(">", toIndx);
            
            String noVelocityTag = content.substring(toIndx, indxOfTagEnd + 1);
            
            if (!noVelocityTag.contains(":id="))
            {
                noVelocityTag = "<novelocity:id=" + UUID.randomUUID().toString() + noVelocityTag.substring(11);
            }
            
            sb.append(noVelocityTag);
            
            fromIndx = indxOfTagEnd + 1;
        }
        
        sb.append(content.substring(fromIndx));
        
        updatedContent = sb.toString();
        
        /* 
         * HP TODO
         * 
         * Updates the current version in WikiDoc since currently SIR does not support versions 
         * for wikidocs associated to playbook. Once versioning is supported it needs to update the
         * specific version.
         */
        
        context.getCurrWikiDocumentAPI().getWikiDoc().setUContent(updatedContent);
        WikiUtils.updateWikiContent(updatedContent, context.getCurrWikiDocumentAPI().getWikiDoc().getSys_id());
        WikiUtils.updateWikiArchiveContent(updatedContent, context.getCurrWikiDocumentAPI().getWikiDoc().getSys_id(), context.getCurrWikiDocumentAPI().getWikiDoc().getUVersion());
        // Clear wikidoc cache
        WikiDocument cachedWikiDoc = (WikiDocument)HibernateUtil.clearCachedDBObject(
        													DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, 
        													context.getCurrWikiDocumentAPI().getWikiDoc().getUFullname());
        
        if (cachedWikiDoc != null && Log.log.isDebugEnabled())
        {
            Log.log.debug(String.format(CLEARED_CACHED_WIKIDOC_FROM_DB_CACHE_REGION_LOG, cachedWikiDoc.getUFullname(),
            			  DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION.getValue()));
        }
        
        m.reset(updatedContent);
        noVelocityBlocks.clear();
        
        return updatedContent;
    }
    
    private String updateEmptyWysiwigSections(String content, WikiRequestContext context)
    {
        String updatedContent = content;
        
        StringBuilder sb = new StringBuilder();
        
        int fromIndx = 0;
        boolean updated = false;
        
        while (content.indexOf("{section:type=wysiwyg", fromIndx) != -1)
        {
            int toIndx = content.indexOf("{section:type=wysiwyg", fromIndx);
            
            sb.append(content.substring(fromIndx, toIndx));
            
            int indxOfStartSectionTagEnd = content.indexOf("}", toIndx);
            
            fromIndx = toIndx;
            toIndx = indxOfStartSectionTagEnd + 1;
            
            sb.append(content.substring(fromIndx, toIndx));
            
            int indxOfEndSectionTagStart = content.indexOf("{section}", toIndx);
            
            String wysiwygSectionContent = content.substring(toIndx, indxOfEndSectionTagStart);
            
            if (StringUtils.isBlank(wysiwygSectionContent.trim()) || !wysiwygSectionContent.contains("<novelocity"))
            {
                sb.append("\n<novelocity:id=" + UUID.randomUUID().toString() + ">\n");
                sb.append(wysiwygSectionContent.trim() + "\n");
                sb.append("</novelocity>\n");
                updated = true;
            }
            else
            {
                sb.append(wysiwygSectionContent);
            }
            
            fromIndx = indxOfEndSectionTagStart;
        }
        
        sb.append(content.substring(fromIndx));
        
        updatedContent = sb.toString();
        
        if (updated)
        {
            /* 
             * HP TODO
             * 
             * Updates the current version in WikiDoc since currently SIR does not support versions 
             * for wikidocs associated to playbook. Once versioning is supported it needs to update the
             * specific version.
             */
            
            context.getCurrWikiDocumentAPI().getWikiDoc().setUContent(updatedContent);
            WikiUtils.updateWikiContent(updatedContent, context.getCurrWikiDocumentAPI().getWikiDoc().getSys_id());
            WikiUtils.updateWikiArchiveContent(updatedContent, context.getCurrWikiDocumentAPI().getWikiDoc().getSys_id(),
            								   context.getCurrWikiDocumentAPI().getWikiDoc().getUVersion());
            // Clear wikidoc cache
            WikiDocument cachedWikiDoc = (WikiDocument)HibernateUtil.clearCachedDBObject(
            												DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, 
            												context.getCurrWikiDocumentAPI().getWikiDoc().getUFullname());
            
            if (cachedWikiDoc != null && Log.log.isDebugEnabled())
            {
                Log.log.debug(String.format(CLEARED_CACHED_WIKIDOC_FROM_DB_CACHE_REGION_LOG, cachedWikiDoc.getUFullname(),
                							DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION.getValue()));
            }
        }
        
        return updatedContent;
    }
    
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Getter/Setter methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    public VelocityConfigurer getVelocityConfigurer()
//    {
//        return velocityConfigurer;
//    }
//
//    public void setVelocityConfigurer(VelocityConfigurer velocityConfigurer)
//    {
//        this.velocityConfigurer = velocityConfigurer;
//    }

    @Override
    public String preRender(String content, Map<String, String> values, WikiRequestContext context)
    {
        // TODO: Add in velocity ignore helper
        // Find all the <novelocity></novelocity> blocks in the content and put
        // them into the noVelocityBlocks map for future restoration
        // Pattern p = Pattern.compile("<novelocity>(.*?)</novelocity>",
        // Pattern.DOTALL);
        //Pattern p = Pattern.compile("[<{]((?:novelocity)|(?:code))[>}](.*?)[<{]/?\\1[}>]", Pattern.DOTALL);
        
        /*
         * replaced the % with _PERCENT_, so that it should not interfere with String formatting.
         * It will be put back in the post renderer
         */
        content = content.replace("%", "_PERCENT_");
        
        String sir = context.getHttpServletRequest().getParameter("sir");
        
        // Check for wysiwyg type without any novelocity tag and if found fix them
        
        if (StringUtils.isNotBlank(sir))
        {
            content = updateEmptyWysiwigSections(content, context);
        }
        
        Pattern p = Pattern.compile("[<{]((?:novelocity)|(?:code)|(?:novelocity)[:]id[=])(.*?)[>}](.*?)[<{]/?\\1[}>]", Pattern.DOTALL);

        Matcher m = p.matcher(content);
        while (m.find())
        {
            
            String fullText = m.group();
            // Each section is marked by
            // {section}
            // ...
            // {section}
            // After going through filters, they are removed but newlines remained if exist.
            // Since they will be converted into line breaks <BR> causing unexpected
            // space between sections so need to remove them if the exist.
            String text = StringUtils.removeStartAndEndNewLine(m.group(3));
            String id = m.group(2);
            if ((fullText.startsWith("{code")) && (id.contains(":type=")))
            {
                continue;
            }
            
            if (StringUtils.isNotBlank(sir))
            {
                if ((StringUtils.isNotBlank(id)) && (id.contains(":id=")))
                {
                    id = id.replace(":id=", "");
                }
                else
                {
                    content = updateContentAndReset(p, m, content, context);
                    continue;
                }
            }
            else
            {
                id = UUID.randomUUID().toString();
            }
            
            if (fullText.startsWith("{code}"))
            {
                text = "{code}" + text + "{code}";
            }

            String replacement = "<noVelocity" + id + ">";
            noVelocityBlocks.put(replacement, text);
            content = content.replace(fullText, replacement);
            // counter++;
        }
        
        // noVelocityBlocks.put(key, value)
        return content;
    }

    @Override
    public String postRender(String content, Map<String, String> values, WikiRequestContext context)
    {
        String sir = context.getHttpServletRequest().getParameter("sir");
        
        // Replace any velocity text that was remove in the preRender
        for (String key : noVelocityBlocks.keySet())
        {
            if (StringUtils.isNotEmpty(sir)) {
                String componentId = key.replace("noVelocity", "");
                
                int endOfComponentId = componentId.indexOf(":") != -1 ? componentId.indexOf(":") : componentId.indexOf(">");
                componentId = componentId.substring(componentId.indexOf("<") + 1, endOfComponentId);

                String incidentId = context.getHttpServletRequest().getParameter("incidentId");
                String activityId = context.getHttpServletRequest().getParameter("activityId");
                String activityName = context.getHttpServletRequest().getParameter("activityName");
                
                String divId = "texteditorNotes_" + UUID.randomUUID().toString();
                String noVelocityBlockContent = noVelocityBlocks.get(key);

                //Build up div and script to render
                StringBuilder str = new StringBuilder();
                str.append("<div id=\"%1$s\" class=\"texteditor\">\n");
                str.append(noVelocityBlockContent);
                str.append("<div id=\"%2$s\" class=\"texteditorNotes\"></div>\n");
                str.append("</div>\n");
                str.append("<script type=\"text/javascript\">\n");
                str.append("Ext.onReady(function() {\n");
                str.append("    var model = glu.model({\n");
                str.append("        mtype : 'RS.wiki.macros.TextEditor',\n");
                str.append("        id : '%1$s',\n");
                str.append("        incidentId : '%3$s',\n");
                str.append("        activityId : '%4$s',\n");
                str.append("        activityName : '%5$s',\n");
                str.append("    });\n");
                str.append("    model.init();\n");
                str.append("    glu.view(model).render('%2$s');\n");
                str.append("});\n");
                str.append("</script>\n");
                content = content.replace(key, String.format(str.toString(), componentId, divId, incidentId, activityId, activityName));
            }
            else {
                content = content.replace(key, noVelocityBlocks.get(key));
            }
            
        }

        /*
         * put back the % sign which might have got replaced in to pre-renderer.
         */
        content = content.replace("_PERCENT_", "%");

        /*
         * Replace all HTML/css image tags linked to internal endpoints with *-data so browser doesn't automatically downlaod the image 
         * Front-End will parse and inject CSRF Tokens to the links before downloading these images
         */
        content = content.replaceAll("(?i)background[\\s]*=[\\s]*\"/resolve/service/wiki/download", "_background-data=\"/resolve/service/wiki/download");
        content = content.replaceAll("(?i)src[\\s]*=[\\s]*\"/resolve/service/wiki/download", "_src-data=\"/resolve/service/wiki/download");
        content = content.replaceAll("(?i)background-image[\\s]*:[\\s]*url[(]/resolve/service/wiki/download", "_background-image-data:url(/resolve/service/wiki/download");
        content = content.replaceAll("(?i).src[\\s]*=[\\s]*\"/resolve/service/wiki/view", "[\"_src-data\"]=\"/resolve/service/wiki/view");
        
        return content;
    }

}
