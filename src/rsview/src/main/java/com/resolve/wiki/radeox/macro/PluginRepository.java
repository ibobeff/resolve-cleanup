/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repository for plugins
 * 
 */

public class PluginRepository implements Repository
{
    protected Map<String, Object> plugins;
    protected List<Object> list;

    protected static Repository instance;

    public PluginRepository()
    {
        plugins = new HashMap<String, Object>();
        list = new ArrayList<Object>();
    }

    public boolean containsKey(String key)
    {
        return plugins.containsKey(key);
    }

    public Object get(String key)
    {
        return plugins.get(key);
    }

    public List<Object> getPlugins()
    {
        return new ArrayList<Object>(plugins.values());
    }

    public void put(String key, Object value)
    {
        plugins.put(key, value);
        list.add(value);
    }
}
