/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.filter;

import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.WikiUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.RenderEngine;
import com.resolve.wiki.radeox.api.engine.WikiRenderEngine;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.LocaleRegexTokenFilter;
import com.resolve.wiki.radeox.regex.MatchResult;
import com.resolve.wiki.radeox.util.Encoder;

/**
 * 
 * This filter class is for the links
 * 
 * @author jeet.marwah
 * 
 */
public class WikiLinkFilter extends LocaleRegexTokenFilter
{

    public static final String LOCAL_KEY = "filter.link";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "\\[(.*?)\\]");
    }

    @Override
    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }

    @Override
    public void handleMatch(StringBuffer bufferAll, MatchResult result, FilterContext context)
    {
        RenderEngine engine = context.getRenderContext().getRenderEngine();
        WikiRenderEngine wikiEngine = (WikiRenderEngine) engine;
        InitialRenderContext initialContext = (InitialRenderContext) context.getRenderContext();
        //		WikiRequestContext wikiRequestContext = (WikiRequestContext)initialContext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        //		String controlId = (String) wikiRequestContext.getAttribute(ConstantValues.WIKI_UI_CONTROL_ID_KEY);
        //		String querystring = ConstantValues.WIKI_UI_CONTROL_ID_KEY + "=" + controlId;

        StringBuffer buffer = new StringBuffer();
        String fullLink = result.group(1);

        //some doc heading>Test.test1 
        if (fullLink != null)
        {
            fullLink = Encoder.unescape(fullLink.trim());
            // Is there an alias and/or target like [alias@target>link] ?
            String alias = "";
            String target = "_top";
            String link = fullLink;

            // get [alias>link]
            boolean hasAlias = false;
            int separatorLength = 1;

            // get position of >
            int pos = fullLink.indexOf('>');
            if (pos > 0)
            {
                hasAlias = true;
            }
            else
            {
                pos = fullLink.indexOf("&gt;");
                if (pos > 0)
                {
                    hasAlias = true;
                    separatorLength = 4;
                }
            }

            if (hasAlias)
            {
                alias = fullLink.substring(0, pos);
                link = fullLink.substring(pos + separatorLength);
            }

            //get target - alias@target
            // check if alias as target and does not start with mailto:
            pos = alias.indexOf('@');
            if (pos > 0 && !link.contains("mailto:"))
            {
                // get [alias@target>link]
                target = alias.substring(pos + separatorLength);
                alias = alias.substring(0, pos);
            }
            else if (pos > 0)
            {
                // Add support for mailto for xwiki v1
                // XWiki v1.0 syntax is [john@smith.net>mailto:john@smith.net] that renders john@smith.net hyperlink. 
                bufferAll.append("<span class=\"wikiexternallink\"><a ");
                bufferAll.append("href=\"");
                bufferAll.append(link);
                bufferAll.append("\">");
                bufferAll.append(alias);
                bufferAll.append("</a></span>");
                return;
            }

            // External link
            int protocolIndex = link.indexOf("://");
            if (protocolIndex >= 0)
            {
                // default view to link
                String view = link;

                // check if mailto
                if (link.startsWith("mailto:"))
                {
                    view = link.substring(7, link.length());
                }

                // update view to alias if defined
                if (hasAlias)
                {
                    view = alias;
                }

                // check if /absolute path and no target specified
                if (StringUtils.isBlank(target) && link.charAt(0) == '/')
                {
                    target = "_parent";
                }

                buffer.append("<span class=\"wikiexternallink\"><a");
                if (StringUtils.isNotBlank(target))
                {
                    buffer.append(" target=\"");
                    buffer.append(target);
                    buffer.append("\"");
                }
                buffer.append(" href=\"");
                buffer.append(link.trim());
                buffer.append("\">");
                buffer.append(Encoder.toEntity(view.charAt(0)) + view.substring(1));
                buffer.append("</a></span>");

                bufferAll.append(buffer);
                return;
            }
            // Internal link
            if (link.startsWith("/"))
            {
                // default view to link
                String view = link;

                // update view to alias if defined
                if (hasAlias)
                {
                    view = alias;
                }

                // check if /absolute path and no target specified
                if (StringUtils.isBlank(target) && link.charAt(0) == '/')
                {
                    target = "_parent";
                }

                buffer.append("<span class=\"wikiexternallink\"><a");
                if (target != null)
                {
                    buffer.append(" target=\"");
                    buffer.append(target);
                    buffer.append("\"");
                }
                buffer.append(" href=\"");
                buffer.append(link.trim());
                buffer.append("\">");
                buffer.append(Encoder.toEntity(view.charAt(0)) + view.substring(1));
                buffer.append("</a></span>");

                bufferAll.append(buffer);
                return;
            }

            // get actiontask name and namespace
            int hashIndex = link.lastIndexOf('#');

            String hash = "";
            if (-1 != hashIndex && hashIndex != link.length() - 1)
            {
                hash = link.substring(hashIndex + 1);
                link = link.substring(0, hashIndex);
            }

            if (isValidLinkName(link))
            {
                // check if wiki document exists
                //If the link doesn't contain a namespace, then we need to add it at this point
                String[] tokens = link.split("\\?");
                String wikiName = tokens[0];
                String linkFullName = wikiName;
                if (!linkFullName.contains("."))
                {
                    linkFullName = initialContext.get("namespace") + "." + wikiName;
                }
                
                if (wikiEngine.exists(initialContext, linkFullName))
                {
                    String view = getWikiView(link);
                    if (hasAlias)
                    {
                        view = alias;
                    }
                    // Do not add hash if an alias was given
                    if (-1 != hashIndex)
                    {
                        wikiEngine.appendLink(initialContext, buffer, link, view, hash, target);
                    }
                    else
                    {
                        wikiEngine.appendLink(initialContext, buffer, link, view, target);
                    }
                }
                else if (wikiEngine.showCreate())
                {
                    String view = getWikiView(link);
                    if (hasAlias)
                    {
                        view = alias;
                    }
                    wikiEngine.appendCreateLink(initialContext, buffer, wikiName, view,target);
                    // links with "create" are not cacheable because
                    // a missing wiki could be created
                    context.getRenderContext().setCacheable(false);
                }
                else
                {
                    // cannot display/create wiki, so just display the text
                    buffer.append(link);
                }
            }
            else
            {
                // cannot display/create wiki, so just display the text
                buffer.append(link);
            }

        }
        else
        {
            buffer.append(Encoder.escape(result.group(0)));
        }

        bufferAll.append(buffer);
    }

    protected boolean isValidLinkName(String link)
    {
        boolean result = false;

        int pos = link.indexOf(':');
        if (pos == -1)
        {
            pos = link.indexOf('.');
            if (pos > 0)
            {
                String web = link.substring(0, pos);
                String name = link.substring(pos + 1, link.length());
                if (web.length() > 0 && name.length() > 0)
                {
                    try
                    {
                        result = StringUtils.isAlphanumeric("" + web.charAt(0)) && StringUtils.isAlphanumeric("" + name.charAt(0));
                    }
                    catch (Throwable t)
                    {
                        Log.log.error("Error parsing :" + link, t);
                    }
                }
            }
            else
            {
                if (link.length() > 0)
                {
                    result = StringUtils.isAlphanumeric("" + link.charAt(0));
                }
            }
        }
        return result;
    } // isValidLinkName

    public String getGeneratedWikiLink(Wiki wiki, WikiRenderEngine wikiEngine, String fullLink)
    {
        StringBuffer buffer = new StringBuffer();

        //some doc heading>Test.test1
        if (StringUtils.isNotBlank(fullLink))
        {
            fullLink = Encoder.unescape(fullLink.trim());
            // Is there an alias and/or target like [alias@target>link] ?
            String alias = "";
            String target = null;
            String link = fullLink;

            // get [alias>link]
            boolean hasAlias = false;
            int separatorLength = 1;

            // get position of >
            int pos = fullLink.indexOf('>');
            if (pos > 0)
            {
                hasAlias = true;
            }
            else
            {
                pos = fullLink.indexOf("&gt;");
                if (pos > 0)
                {
                    hasAlias = true;
                    separatorLength = 4;
                }
            }

            if (hasAlias)
            {
                alias = fullLink.substring(0, pos);
                link = fullLink.substring(pos + separatorLength);
            }

            //get target - alias@target
            // check if alias as target and does not start with mailto:
            pos = alias.indexOf('@');
            if (pos > 0 && !alias.startsWith("mailto:"))//NOT SUPPORTING MAILTO FOR NOW...
            {
                // get [alias@target>link]
                target = alias.substring(pos + separatorLength);
                alias = alias.substring(0, pos);
            }

            // External link
            int protocolIndex = link.indexOf("://");
            if (protocolIndex >= 0)
            {
                // default view to link
                String view = link;

                // check if mailto
                if (link.startsWith("mailto:"))
                {
                    view = link.substring(7, link.length());
                }

                // update view to alias if defined
                if (hasAlias)
                {
                    view = alias;
                }

                // check if /absolute path and no target specified
                if (target == null && link.charAt(0) == '/')
                {
                    target = "_parent";
                }

                buffer.append("<span class=\"wikiexternallink\"><a");
                if (target != null)
                {
                    buffer.append(" target=\"");
                    buffer.append(target);
                    buffer.append("\"");
                }
                buffer.append(" href=\"");
                buffer.append(link.trim());
                buffer.append("\">");
                buffer.append(Encoder.toEntity(view.charAt(0)) + view.substring(1));
                buffer.append("</a></span>");

                //                bufferAll.append(buffer);
                //                return;
            }
            // Internal link
            else if (link.startsWith("/"))
            {
                // default view to link
                String view = link;

                // update view to alias if defined
                if (hasAlias)
                {
                    view = alias;
                }

                // check if /absolute path and no target specified
                if (target == null && link.charAt(0) == '/')
                {
                    target = "_parent";
                }

                buffer.append("<span class=\"wikiexternallink\"><a");
                if (target != null)
                {
                    buffer.append(" target=\"");
                    buffer.append(target);
                    buffer.append("\"");
                }
                buffer.append(" href=\"");
                buffer.append(link.trim());
                buffer.append("\">");
                buffer.append(Encoder.toEntity(view.charAt(0)) + view.substring(1));
                buffer.append("</a></span>");

                //                bufferAll.append(buffer);
                //                return;
            }
            else
            {
                // get actiontask name and namespace
                int hashIndex = link.lastIndexOf('#');

                String hash = "";
                if (-1 != hashIndex && hashIndex != link.length() - 1)
                {
                    hash = link.substring(hashIndex + 1);
                    link = link.substring(0, hashIndex);
                }

                if (isValidLinkName(link))
                {
                    // check if wiki document exists
                    if (docExist(wiki, link))
                    {
                        String view = getWikiView(link);
                        if (hasAlias)
                        {
                            view = alias;
                        }
                        // Do not add hash if an alias was given
                        if (-1 != hashIndex)
                        {
                            wikiEngine.appendLink(wiki, buffer, link, null, view, hash, target);
                        }
                        else
                        {
                            wikiEngine.appendLink(wiki, buffer, link, null, view, null, target);
                        }
                    }
                    else if (wikiEngine.showCreate())
                    {
                        String view = getWikiView(link);
                        if (hasAlias)
                        {
                            view = alias;
                        }
                        wikiEngine.appendCreateLink(wiki, buffer, link, null, view);
                    }
                    else
                    {
                        // cannot display/create wiki, so just display the text
                        buffer.append(link);
                    }
                }
                else
                {
                    // cannot display/create wiki, so just display the text
                    buffer.append(link);
                }
            }
        }

        return buffer.toString();
    }

    /**
     * Returns the view of the wiki name that is shown to the user. Overwrite to support other views for example transform "WikiLinking" to "Wiki Linking". Does nothing by default.
     * 
     * @return view The view of the wiki name
     */

    protected String getWikiView(String name)
    {
        return convertWikiWords(name);
    }

    public static String convertWikiWords(String name)
    {
        try
        {
            name = name.substring(name.indexOf(".") + 1);
            return name.replaceAll("([a-z])([A-Z])", "$1 $2");
        }
        catch (Exception e)
        {
            return name;
        }
    }

    private boolean docExist(Wiki wiki, String docFullName)
    {
        boolean exist = false;

        if (StringUtils.isNotBlank(docFullName))
        {
            exist = WikiUtils.isWikidocExist(docFullName);
        }

        return exist;
    }

}
