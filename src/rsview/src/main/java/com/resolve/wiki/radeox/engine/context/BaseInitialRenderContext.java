/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.engine.context;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;

/**
 * Base impementation for InitialRenderContext
 * 
 */

public class BaseInitialRenderContext extends BaseRenderContext implements InitialRenderContext
{

	//spring injected
//	private String propertiesFilename;

	//This will read the file - radeox_markup_wiki.properties and load everything in it
	public BaseInitialRenderContext()
	{
//		Locale languageLocale = Locale.getDefault();
//		Locale locale = new Locale("wiki", "wiki");
//		set(RenderContext.INPUT_LOCALE, locale);
//		set(RenderContext.OUTPUT_LOCALE, locale);
//		set(RenderContext.LANGUAGE_LOCALE, languageLocale);
//		set(RenderContext.INPUT_BUNDLE_NAME, "radeox");
//		set(RenderContext.OUTPUT_BUNDLE_NAME, "radeox");
//		set(RenderContext.LANGUAGE_BUNDLE_NAME, "radeox_messages");
		set(RenderContext.DEFAULT_FORMATTER, "default");
	}

//	public String getPropertiesFilename()
//	{
//		return propertiesFilename;
//	}
//
//	public void setPropertiesFilename(String propertiesFilename)
//	{
//		this.propertiesFilename = propertiesFilename;
//	}
}
