/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.plugin.impl;

import com.resolve.wiki.plugin.WikiPluginInterface;

/**
 * All plugins should extend this class. This is the Base class for all the Plugins
 * 
 * @author jeet.marwah
 *
 */
public abstract class BasePlugin implements WikiPluginInterface
{

	/**
	 * Fully qualified class name of the plugin.
	 */
	private String name;

	public BasePlugin(String name)
	{
		this.name = name;
	}

	@Override
	public String getName()
	{
		return name;
	}

}
