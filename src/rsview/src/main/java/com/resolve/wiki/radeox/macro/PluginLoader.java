/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import java.util.Iterator;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.util.Service;

/**
 * Plugin loader
 * 
 */

public abstract class PluginLoader
{
    protected Repository repository;

    public Repository loadPlugins(Repository repository)
    {
        return loadPlugins(repository, getLoadClass());
    }

    public void setRepository(Repository repository)
    {
        this.repository = repository;
    }

    public Iterator getPlugins(Class klass)
    {
        return Service.providers(klass);
    }

    public Repository loadPlugins(Repository repository, Class klass)
    {
        if (null != repository)
        {
            /* load all macros found in the services plugin control file */
            Iterator iterator = getPlugins(klass);
            while (iterator.hasNext())
            {
                try
                {
                    Object plugin = iterator.next();
                    add(repository, plugin);
                    if(Log.log.isTraceEnabled())
                    {
                        Log.log.trace("PluginLoader: Loaded plugin: " + plugin.getClass());
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn("PluginLoader: unable to load plugin", e);
                }
            }
        }
        return repository;
    }

    /**
     * Add a plugin to the known plugin map
     * 
     * @param plugin
     *            Plugin to add
     */
    public abstract void add(Repository repository, Object plugin);

    public abstract Class getLoadClass();
}
