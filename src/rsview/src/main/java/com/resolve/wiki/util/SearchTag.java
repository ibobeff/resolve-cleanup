/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.util;

import com.resolve.services.exception.WikiException;

/**
 * Utility class for tag manipulation
 * 
 * @author jeet.marwah
 *
 */
public class SearchTag
{
	private String START_TAG = "";
	private String END_TAG = "";
//	private String content = "";
	
	public SearchTag(String START_TAG, String END_TAG)
	{
		this.START_TAG = START_TAG;
		this.END_TAG = END_TAG;
//		this.content = content;
	}
	
	
	/**
	 * Return true if there is script in the content.
	 * 
	 * @param content
	 * @return
	 */
	public boolean hasTag(String content) throws WikiException
	{
		//compare the # of open and closed brackets
		hasSameNumberOfOpenCloseBracketFor(content);//will throw exception if not true;

		boolean hasTag = false;

		//Just the start tag is sufficient as we are already checking for the Open and close count. 
		int posStart = content.indexOf(START_TAG);
		//		int posEndGroovy = content.indexOf(GROOVY_END_TAG);

		if (posStart != -1)
		{
			hasTag = true;
		}
		return hasTag;
	}
	
	private boolean hasSameNumberOfOpenCloseBracketFor(String content) throws WikiException
	{
		boolean hasSameNumberOfOpenCloseBracket = true;
		int countOpen = getCountFor(content, START_TAG);
		int countClose = getCountFor(content, END_TAG);
		if (countOpen != countClose)
		{
			throw new WikiException(new Exception("There is a mismatch of '" + START_TAG + "' and '" + END_TAG + "'. Count of '"
					+ START_TAG + "' is " + countOpen + " and count of '" + END_TAG + "' is " + countClose));
		}
        if (START_TAG.equals(END_TAG) && countOpen % 2 != 0)
        {
			throw new WikiException(new Exception("There is a mismatch of open and close '" + START_TAG + "'. Count of '"
					+ START_TAG + "' (" + countOpen + ") should be even"));
        }

		return hasSameNumberOfOpenCloseBracket;

	}
	
	public int getCountFor(String content, String countFor)
	{
		int count = 0;
		String localStr = content;//this is done to make this method multithread proof
		int pos = -1;

		while ((pos = localStr.indexOf(countFor)) != -1)
		{
			localStr = localStr.substring(pos + countFor.length());
			count++;
		}

		return count;
	}
	
	/**
	 * This is called after checking if the code is there
	 * 
	 * @param content
	 * @return
	 */
	public String getTheScriptForTag(String content)
	{
		String start = content.substring(content.indexOf(START_TAG) + START_TAG.length());
		String script = start.substring(0, start.indexOf(END_TAG));
		return script;
	}
	
	public String replaceCodeWithReplacmentString(String content, String replacedString)
	{
		String localResult = content;
        int startIndex = localResult.indexOf(START_TAG);
        int endIndex = localResult.indexOf(END_TAG);
        if (startIndex == endIndex)
        {
            endIndex = localResult.indexOf(END_TAG, startIndex + START_TAG.length());
        }
		String startString = localResult.substring(0, startIndex);
		String endString = localResult.substring(endIndex + END_TAG.length());

		StringBuffer str = new StringBuffer(startString);
		str.append(replacedString);
		str.append(endString);

		return str.toString();
	}
	
	

}
