/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.wiki.radeox.EngineManager;
import com.resolve.wiki.radeox.api.engine.RenderEngine;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.filter.Filter;
import com.resolve.wiki.radeox.filter.FilterPipe;
import com.resolve.wiki.radeox.filter.context.BaseFilterContext;
import com.resolve.wiki.radeox.filter.context.FilterContext;

/**
 * Base implementation of RenderEngine
 * 
 */

public class BaseRenderEngine implements RenderEngine
{

	protected List<String> listOfFilters = null;

	//spring injected
//	protected InitialRenderContext initialContext;
	
	protected FilterPipe fp;

//	public BaseRenderEngine(InitialRenderContext context)
//	{
////		this.initialContext = context;
//	}

	public BaseRenderEngine()
	{
		//this(new BaseInitialRenderContext());
	}

	protected synchronized void initialize(InitialRenderContext initialContext)
	{
		if (null == fp)
		{
			fp = new FilterPipe(initialContext);
			ClassLoader classLoader = Filter.class.getClassLoader();
			for (String filterName : listOfFilters)
			{
				try
				{
					Filter filter = (Filter) classLoader.loadClass(filterName).newInstance();
					fp.addFilter(filter);
				}
				catch (Exception e)
				{
					Log.log.error(e.getMessage(), e);
				}
			}

			fp.init();
		}
	}

	/**
	 * Name of the RenderEngine. This is used to get a RenderEngine instance with EngineManager.getInstance(name);
	 * 
	 * @return name Name of the engine
	 */
	public String getName()
	{
		return EngineManager.WIKI_DEFAULT;
	}

	/**
	 * Render an input with text markup and return a String with e.g. HTML
	 * 
	 * @param content
	 *            String with the input to render
	 * @param context
	 *            Special context for the filter engine, e.g. with configuration information
	 * @return result Output with rendered content
	 */
	public String render(String content, RenderContext context)
	{
//		this.initialContext = (InitialRenderContext)context;//setting the value as it may not have been set
		
		initialize((InitialRenderContext)context);
		FilterContext filterContext = new BaseFilterContext();
		filterContext.setRenderContext(context);
		return fp.filter(content, filterContext);
	}

	/**
	 * Render an input with text markup from a Reader and write the result to a writer
	 * 
	 * @param in
	 *            Reader to read the input from
	 * @param context
	 *            Special context for the render engine, e.g. with configuration information
	 */
	public String render(Reader in, RenderContext context) throws IOException
	{
		StringBuffer buffer = new StringBuffer();
		BufferedReader inputReader = new BufferedReader(in);
		String line;
		while ((line = inputReader.readLine()) != null)
		{
			buffer.append(line);
		}
		return render(buffer.toString(), context);
	}

	public void render(Writer out, String content, RenderContext context) throws IOException
	{
	    out.write(HttpUtil.sanitizeMacroValue(render(content, context)));
	}

	public List<String> getListOfFilters()
	{
		return listOfFilters;
	}

	public void setListOfFilters(List<String> listOfFilters)
	{
		this.listOfFilters = listOfFilters;
	}

//	public RenderContext getRenderContext()
//	{
//		return initialContext;
//	}
//	
//	public InitialRenderContext getInitialContext()
//	{
//		return initialContext;
//	}
//
//	public void setInitialContext(InitialRenderContext initialContext)
//	{
//		this.initialContext = initialContext;
//	}

}
