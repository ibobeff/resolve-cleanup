/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.regex;

/*
 * Class which stores compiled regular expressions
 *
 */

public interface Pattern {
  /**
   * Return a string representation of the regular expression
   *
   * @return String representation of the regular expression
   */
  public String getRegex();
  /**
   * Return whether the pattern is multiline or not
   *
   * @return Ture if the pattern is multiline
   */
  public boolean getMultiline();
}