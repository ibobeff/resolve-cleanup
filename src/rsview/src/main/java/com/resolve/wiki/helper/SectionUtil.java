/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.helper;

import java.util.ArrayList;
import java.util.List;

import com.resolve.dto.SectionModel;
import com.resolve.dto.SectionType;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.services.util.ParseUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.rsradeox.macro.SectionMacro;

public class SectionUtil
{
    public static List<SectionModel> convertContentToSectionModels(String content)
    {
        List<SectionModel> list = new ArrayList<SectionModel>();
        if(StringUtils.isNotEmpty(content))
        {
            //if the content has no sections as of yet, define the whole content as 1 section
            content = GenericSectionUtil.validateSectionStr(content, null);
            
            List<String> sections = ParseUtil.getListOfSections(content);
            for(String sectionContent : sections)
            {
                SectionModel sm = getSectionModel(sectionContent);
                list.add(sm);
                
            }//end of for loop
            
        }//end of if
        
        return list;
    }//convertContentToSectionModel
    

    public static String convertSectionModelsToContent(List<SectionModel> listOfSectionsFromUILocal)
    {
        StringBuffer content = new StringBuffer();

        for(SectionModel local : listOfSectionsFromUILocal)
        {
            content.append(local.getContent()).append("\n");
        }//end of for loop
        
        return content.toString();
    }//convertSectionModelsToContent
    
    
    public static String updateSectionContent(SectionModel section, String newContent)
    {
        StringBuffer content = new StringBuffer();
        newContent = StringUtils.isNotEmpty(newContent) ? newContent.trim() : "";
        
        content.append("{section:")
                .append(SectionModel.TYPE).append("=").append(StringUtils.isNotEmpty(section.getType()) ? section.getType() : SectionType.SOURCE.getTagName()).append("|")
                .append(SectionModel.TITLE).append("=").append(StringUtils.isNotEmpty(section.getTitle()) ? section.getTitle() : "main").append("|")
                .append(SectionModel.ID).append("=").append(StringUtils.isNotEmpty(section.getId()) ? section.getId() : GenericSectionUtil.MAIN_SECTION_ID).append("|")
                .append(SectionModel.HEIGHT).append("=").append(StringUtils.isNotEmpty(section.getHeight()) ? section.getHeight() : SectionModel.DEFAULT_IFRAME_HEIGHT)
                .append("}").append("\n");
        content.append(newContent).append("\n");
        content.append("{section}");
        
        return content.toString();
    }//updateSectionContent
    
    public static void updateSection(SectionModel section, String newContent)
    {
        section.setContent(updateSectionContent(section, newContent));
    }//updateSection
    
    public static void resetSectionContent(SectionModel section)
    {
        String content = getContent(section.getContent());
        updateSection(section, content);

    }//resetContent
    
    /**
     * return what ever is there within the {section} tag
     * 
     * @param sectionContent
     * @return
     */
    public static String getContent(String sectionContent)
    {
       String content = sectionContent;
       
       try
       {
           sectionContent = sectionContent.trim();
           if(sectionContent.startsWith("{section"))
           {
               content = sectionContent.substring(sectionContent.indexOf("}")+1, sectionContent.lastIndexOf("{"));
           }
       }
       catch(Exception e) 
       {
           Log.log.error("Error in getContent", e);
       }
       
       return content;
        
    }//getContent
    
    public static SectionModel getSection(String sectionName, String content)
    {
        //create search model
        SectionModel search = new SectionModel();
        search.setTitle(sectionName);
        
        //get all sections
        List<SectionModel> allSections = SectionUtil.convertContentToSectionModels(content);
        
        //get the section 
        SectionModel result =  getSelectedSectionToEdit(search, allSections);
        
        return result;
        
    }//getSection

    public static SectionModel getSelectedSectionToEdit(SectionModel selectedSectionToEditLocal, List<SectionModel> listOfSectionsFromUILocal)
    {
        SectionModel sm = null;
        
        if(selectedSectionToEditLocal != null)
        {
            for(SectionModel local : listOfSectionsFromUILocal)
            {
                if(selectedSectionToEditLocal.getTitle().equals(local.getTitle()))
                {
                    sm = local;
                    break;
                }
            }//end of for loop
        }

        return sm;
    }//getSelectedSectionToEdit

    /**
     * 
     * @param   sectionContent
     *            {section:type=source|title=wikiText|id=rssection_12312312}
     *            content 
     *            {section}
     * @return
     */
    private static SectionModel getSectionModel(String sectionContent)
    {
        SectionModel sm = new SectionModel();
        sm.setId(GenericSectionUtil.createSectionId());
        sm.setTitle("** NO TITLE **");
        sm.setType(SectionType.SOURCE.getTagName());
        
        if(StringUtils.isNotEmpty(sectionContent))
        {
            if(sectionContent.indexOf(":") > -1)
            {
                String params = sectionContent.substring(sectionContent.indexOf(":")+1, sectionContent.indexOf("}"));
                String[] keyValues = params.split("\\|");
                for(String keyValue : keyValues)
                {
                    String[] arr = keyValue.split("\\=");
                    String key = arr[0];
                    String value = arr[1];
                    
                    if(key.equalsIgnoreCase(SectionModel.ID))
                    {
                        sm.setId(value);
                    }
                    else if(key.equalsIgnoreCase(SectionModel.TITLE))
                    {
                        sm.setTitle(value);
                    }
                    else if(key.equalsIgnoreCase(SectionModel.TYPE))
                    {
                        sm.setType(value);
                    }
                    else if(key.equalsIgnoreCase(SectionModel.HEIGHT))
                    {
                        sm.setHeight(value);
                    }            
                }//end of for loop
                
                sm.setContent(sectionContent);
            }
            else
            {
                //it may be like {section}sdfsdfs{section}
                String innerContent = sectionContent.substring(sectionContent.indexOf('}')+1, sectionContent.lastIndexOf('{'));
                sm.setContent(SectionMacro.getSectionDefinition(sm.getId(), sm.getTitle(), sm.getType(), innerContent));
            }
        }//end of if 
        
        return sm;
    }//getSectionModel
    
}
