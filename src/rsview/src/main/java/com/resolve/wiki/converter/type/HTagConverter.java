/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter.type;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.services.util.ParseUtil;
import com.resolve.wiki.converter.AbstractConverter;
import com.resolve.wiki.converter.WikiConverter;
import com.resolve.wiki.util.TOCGenerator;


/**
 * This converter adds the <span> tag for all the Heading tags (h1, h2, h3,...)
 * 
 * @author jeet.marwah
 *
 */
public class HTagConverter extends AbstractConverter
{

	private String CSS_NAME = "";
	private String HTML_START_TAG = "";
	private String HTML_END_TAG = "";
	private String WIKI_START_TAG = "";
	private String WIKI_END_TAG = "";

	private String pattern = "";
	
	
	//add the <span> tag to all the headings
	/**
	 * <h2 class="paragraph heading-1-1">Heading 2</h2>  ==> <h2 class="paragraph heading-1-1"><span id="_H_Heading2">Heading 2</span></h2>
	 */
	@Override
	public String html2wiki(String content)
	{
		List<String> patterns = TOCGenerator.getPatternsForTOC();
		String contentStr = new String(content);
		String id = "";
		String contentBetweenTags = "";

		for(String patternStr : patterns)
		{
			Pattern pattern = Pattern.compile(patternStr, Pattern.MULTILINE);
			Matcher matcher = pattern.matcher(content);
			while (matcher.find())
			{
				String hTag = matcher.group();
				//if the hTag has no <span> tag, add one
				if(hTag.indexOf("span") == -1)
				{
					contentBetweenTags = ParseUtil.getContentBetweenTags(hTag);
					
					String refinedContentBetweenTags = contentBetweenTags.replace(WikiConverter.BR_PLACE_HOLDER, "");
					//create the id 
					id = TOCGenerator.makeHeadingID(refinedContentBetweenTags, 0);
					
					//add the span tag in front of it
					String spanTag = "<span id=\"" + id + "\">" + contentBetweenTags + "</span>";
					contentStr = contentStr.replace(contentBetweenTags, spanTag);
				}
			}
		}

		return contentStr;
	}
	
	
	//do nothing
	@Override
	public String wiki2html(String content)
	{
		String contentStr = new String(content);
		
		return contentStr;
	}
	
	@Override
	public String getHTML2WikiPattern()
	{
		return pattern;
	}
	
	@Override
	public String getWiki2HTMLPattern()
	{
		return pattern;
	}
	
	@Override
	public String getCSS_NAME()
	{
		return CSS_NAME;
	}

	@Override
	public String getHTML_START_TAG()
	{
		return HTML_START_TAG;
	}

	@Override
	public String getHTML_END_TAG()
	{
		return HTML_END_TAG;
	}

	@Override
	public String getWIKI_END_TAG()
	{
		return WIKI_END_TAG;
	}

	@Override
	public String getWIKI_START_TAG()
	{
		return WIKI_START_TAG;
	}



}//JavascriptConverter
