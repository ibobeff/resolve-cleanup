/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.list;

import com.resolve.wiki.radeox.util.Linkable;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

/**
 * List formatter interface. List formatters are loaded via the plugin mechanism.
 *
 */
public interface ListFormatter {
  public String getName();

  /**
   * Display a simple vertical list.
   *
   * @param writer Writer to write the list output to
   * @param current the current linkable
   * @param listComment String to display before the list
   * @param c Collection of Linkables, Snips or Nameables to display
   * @param emptyText Text to display if collection is empty
   * @param showSize If showSize is true then the size of the collection is displayed
   */
  public void format(Writer writer,
                     Linkable current,
                     String listComment,
                     Collection c,
                     String emptyText,
                     boolean showSize)
      throws IOException;
}
