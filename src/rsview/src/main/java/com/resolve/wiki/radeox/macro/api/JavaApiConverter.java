/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.api;

import java.io.IOException;
import java.io.Writer;

import com.resolve.util.HttpUtil;

/**
 * Converts a Java class name to an API url
 *
 */

public class JavaApiConverter extends BaseApiConverter {
  public void appendUrl(Writer writer, String className) throws IOException {
    writer.write(baseUrl);
    className=HttpUtil.sanitizeValue(className);
    writer.write(className.replace('.', '/'));
    writer.write(".html");
  }

  public String getName() {
    return "Java";
  }
}
