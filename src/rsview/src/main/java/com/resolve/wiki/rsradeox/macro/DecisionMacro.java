/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.resolve.services.ServiceWiki;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.vo.DTMetricLogList;
import com.resolve.vo.DTMetricLogVO;
import com.resolve.vo.TMetricLogVO;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * DECISION_DEFINITION
 *   {decision:[type=<radio(default), combobox>]|[style=<cssName>]|question=<header>} VALUE_DEFINITION {decision}
 *   
 * VALUE_DEFINITION
 *   DocName = <answer text>=<namespace>.<pagename>#section?param1=value1
 *   
 */
public class DecisionMacro extends BaseLocaleMacro
{
    public static final String DT_METRIC = "dt_metric";
    
    private static final String TYPE_COMBOBOX = "ComboBox";
    private static final String TYPE_RADIO = "Radio";

    private static String DEFAULT_OPTION = null;
    private static Integer DEFAULT_ABORT_TIME = -1;

    private static final String DecisionMacro_TYPE = "type";
    private static final String DecisionMacro_STYLE = "style";
    private static final String DecisionMacro_QUESTION = "question";
    private static final String DecisionMacro_RECOMMENDATION = "recommendation";

//    private String style = "";
//    private String question = "";
    private class Answer {
        private String answerText;
        private String answerInfo;
        Answer(String answerText, String answerInfo) {
            this.answerText = answerText;
            this.answerInfo = answerInfo;
        }
    };
    private class QAInfo {
        private String question;
        private String recommendationText;
        private String metricData;
        private List<Answer> answers;
        QAInfo(String question, String recommendationText, String metricData, List<Answer> answers) {
            this.answers = answers;
            this.question = question;;
            this.recommendationText = recommendationText;
            this.metricData = metricData;
        }
    };

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        DEFAULT_OPTION = PropertiesUtil.getPropertyString("dt.option.default");
        if (StringUtils.isEmpty(DEFAULT_OPTION))
        {
            DEFAULT_OPTION = TYPE_COMBOBOX;
        }

        if (DEFAULT_ABORT_TIME == -1)
        {
            DEFAULT_ABORT_TIME = (int) PropertiesUtil.getPropertyLong("dt.node.abortInSecs.default");
        }

        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);

        String problemId = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_PROBLEMID);
        Log.log.debug("PROBLEMID in DecisionTree --" + problemId);
        problemId = HttpUtil.sanitizeValue(problemId, "ResolveUUID", 100, HttpUtil.HTML_ENCODING);

        HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
        String currWikiNode = (String) request.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        currWikiNode = HttpUtil.sanitizeValue(currWikiNode);
        String metricData = request.getParameter("metricData");
                
        metricData = HttpUtil.sanitizeValue(metricData, HttpUtil.VALIDATOR_RESOLVETEXT, 4000, HttpUtil.URL_ENCODING);
        
        //if this flag is true, than its from the new UI
        boolean dtviewer = (request.getParameter("decisiontreeviewer") != null && request.getParameter("decisiontreeviewer").equalsIgnoreCase("true")) ? true : false;
        String rootDocumentName = null;
        
        //eg. CR.Deploy?ROOT=DT.dt_reena&NODE_ID=4&ANSWER_TEXT=CR.Deploy
        String paramDocFullName = request.getParameter("docFullName"); //For DT Viewer, this value will have the ROOT value which is the ROOT document.
        if(StringUtils.isNotBlank(paramDocFullName) && paramDocFullName.contains("ROOT"))
        {
            rootDocumentName = paramDocFullName.substring(paramDocFullName.indexOf("ROOT=")+5);
            rootDocumentName = rootDocumentName.substring(0, rootDocumentName.indexOf('&'));
        }
        paramDocFullName = HttpUtil.sanitizeValue(paramDocFullName);
        rootDocumentName = HttpUtil.sanitizeValue(rootDocumentName);

        String username = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        username = HttpUtil.sanitizeValue(username);

        String list = params.getContent();
        list = HttpUtil.sanitizeValue(list);
        String decisionType = params.get(DecisionMacro_TYPE);
        if (StringUtils.isEmpty(decisionType))
        {
            decisionType = DEFAULT_OPTION;
        }
        decisionType = HttpUtil.sanitizeValue(decisionType);

        String type = decisionType.equalsIgnoreCase(TYPE_COMBOBOX) ? TYPE_COMBOBOX : TYPE_RADIO;

        String style = StringUtils.isNotEmpty(params.get(DecisionMacro_STYLE)) ? params.get(DecisionMacro_STYLE) : "";
        style = HttpUtil.sanitizeValue(style);

        String question = StringUtils.isNotEmpty(params.get(DecisionMacro_QUESTION)) ? params.get(DecisionMacro_QUESTION) : "";
        String recommendationText = StringUtils.isNotEmpty(params.get(DecisionMacro_RECOMMENDATION)) ? params.get(DecisionMacro_RECOMMENDATION) : "";
        question= HttpUtil.sanitizeMacroValue(question);

        Map<String, String> docNameWithText = new HashMap<String, String>();
        List<String> keys = new ArrayList<String>();
        
        if (StringUtils.isNotBlank(list))
        {
            String[] arr = list.split("\\n");
            for (String line : arr)
            {
                if (StringUtils.isNotEmpty(line) && line.indexOf('=') > -1)
                {
                    String[] docText = new String[2];
                    docText[0] = line.substring(0, line.indexOf("="));//text
                    docText[0] = HttpUtil.sanitizeValue(docText[0]);
                    docText[1] = line.substring(line.indexOf("=") + 1);//document name - <docname>#section?name=value
                    docText[1]= HttpUtil.sanitizeValue(docText[1]);
                    docNameWithText.put(docText[0], docText[1]);
                    keys.add(docText[0]);
                }
            }
        }

        long uniqueNo = JavaUtils.generateUniqueNumber();
        String divId = "div_" + uniqueNo; 
        String formId = "form_" + uniqueNo;
        String selectId = "select_" + uniqueNo;
        String radioButtonName = "radioName_" + uniqueNo;
        String resultId = "div_result_" + uniqueNo;
        String dataId = "metricData_" + uniqueNo;

        //prepare the metric object
        String data = "";
        TMetricLogVO currTmetricVO = prepareTmetricLog(metricData, HttpUtil.sanitizeValue(wikiRequestContext.getWikiDocumentAPI().getWikiDoc().getUFullname()), null, username, false, false, dtviewer, problemId, rootDocumentName);
        try
        {
            data = currTmetricVO.encode();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        //prepare the output
        StringBuffer out = new StringBuffer();
        String doNotRenderQA = request.getParameter(ConstantValues.RENDER_DT_QA_AS_DATA);
        if (doNotRenderQA != null && doNotRenderQA.toLowerCase().equals("true")) {

            List<Answer> answers = new ArrayList<Answer>();
            Pattern re = Pattern.compile("(%__[\\d]+__%)", Pattern.MULTILINE);
            Matcher matcher = re.matcher(params.getContent());
            while (matcher.find()) {
                String answer = matcher.group();
                answers.add(new Answer(answer,  matcher.find() ? matcher.group() : ""));
            }
            QAInfo qaInfo = new QAInfo(question, recommendationText, data, answers);
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            String data_qa = gson.toJson(qaInfo).replaceAll("\\[", "");

            out.append(String.format("<meta id='RSDecisionTreeQuestionAndAnswersInfo' data-DT-question-answers=%s></meta>", data_qa));
        } else {
            out.append("<div id='").append(divId).append("' ");
            if (StringUtils.isNotEmpty(style))
            {
                out.append(" class=\"").append(style).append("\"");
            }
            out.append(">").append("\n");
            out.append("<form name='WikiDecisionForm' id='").append(formId).append("'>").append("\n");

            //TODO: add a callback function to the {decision} macro so that user can specify the function it wants to execute after the decision macro is rendered
            //TODO: callback: Ext.isDefined(theirCallbackFunctionName) ? theirCallbackFunctionName : Ext.EmptyFn

            if (type.equals(TYPE_COMBOBOX))
            {
                out.append(createComboBoxElement(keys, docNameWithText, selectId, style, question, recommendationText)).append("<p><br></p>");
                out.append(getDataDiv(data, dataId));

                StringBuffer onClickFunction = new StringBuffer("submitComboBoxDecisionRequest(Ext.get('").append(resultId).append("'), Ext.get('").append(selectId).append("'), Ext.get('").append(divId).append("'))");

                out.append("<input type='button' value='Next' onclick=\"").append(onClickFunction).append("\" />");
            }
            else
            {
                out.append(createRadioElement(keys, docNameWithText, radioButtonName, style, question, recommendationText)).append("<p><br></p>");
                out.append(getDataDiv(data, dataId));

                StringBuffer onClickFunction = new StringBuffer("submitRadioDecisionRequest(Ext.get('").append(resultId).append("'), document.getElementsByName('").append(radioButtonName).append("'), Ext.get('").append(divId).append("'))");

                out.append("<input type='button' value='Next' onclick=\"").append(onClickFunction).append("\" />");
            }

            out.append("</form>").append("\n");
            out.append("</div>").append("\n");
            out.append("<div name='WikiDecisionDiv' id='").append(resultId).append("'></div>").append("\n");
        }
        request.setAttribute(ConstantValues.DT_TMETRIC_LOG, currTmetricVO);
        writer.write(HttpUtil.sanitizeEncodedValue(out.toString(), true));

    }

    /**
     * 
     *  <fieldset class="radiogroup"> 
          <legend><span class="wrap">Trevor counted 77 coins in his bank.  He counted 29 quarters.  The rest are dimes.  How many more dimes than quarters does Trevor have?</span></legend> 
          <ul class="radio"> 
            <li><input type="radio" name="b" id="b1" value="1" /><label for="b1">17</label></li> 
            <li><input type="radio" name="b" id="b2" value="2" /><label for="b2">29</label></li> 
            <li><input type="radio" name="b" id="b3" value="3" /><label for="b3">19</label></li> 
            <li><input type="radio" name="b" id="b4" value="4" /><label for="b4">48</label></li> 
          </ul> 
        </fieldset> 
     * @param docNameWithText
     * @param radioGroupName
     * @return
     */
    private String createRadioElement(List<String> keys, Map<String, String> docNameWithText, String radioGroupName, String style, String question, String recommendation)
    {
        StringBuffer str = new StringBuffer();
        String id = null;

        //fieldset element
        str.append("<fieldset ");
        if (StringUtils.isNotEmpty(style))
        {
            str.append("class=\"").append(style).append("\"");
        }
        str.append(">");

        //legend
        if (StringUtils.isNotEmpty(question))
        {
            str.append("<legend><b>").append(question).append("</b>").append(getRecommendationDiv(recommendation)).append("</legend>");
        }

        //create the <ui> element
        str.append("<ul style='list-style-type: none; padding-left:5px'>");

        int count = 0;
        Iterator<String> it = keys.iterator();
        while (it.hasNext())
        {
            id = "radio_" + JavaUtils.generateUniqueNumber();
            String text = it.next();
            String docName = docNameWithText.get(text);

            str.append("<li style='list-style-type: none;'><input type='radio' id='").append(id).append("' name='").append(radioGroupName).append("' value='").append(docName).append("'");
            if (count == 0)
            {
                str.append(" CHECKED");
            }
            str.append("/>");
            str.append("<label id='").append(id).append("_label' for='").append(id).append("'>");
            str.append(text).append("</label></li>");
            count++;
        }

        str.append("</ul>");
        str.append("</fieldset>");

        return str.toString();

    }

    private String createComboBoxElement(List<String> keys, Map<String, String> docNameWithText, String id, String style, String question, String recommendation)
    {
        StringBuffer str = new StringBuffer();

        //fieldset element
        str.append("<fieldset");
        if (StringUtils.isNotEmpty(style))
        {
            str.append(" class=\"").append(style).append("\"");
        }
        str.append(">");

        //legend
        if (StringUtils.isNotEmpty(question))
        {
            str.append("<legend><b>").append(question).append("</b>").append(getRecommendationDiv(recommendation)).append("</legend><br/>");
        }

        str.append("<select id='").append(id).append("'>").append("\n");

        Iterator<String> it = keys.iterator();
        while (it.hasNext())
        {
            String text = it.next();
            String docName = docNameWithText.get(text);

            str.append("<option value='").append(docName).append("'>").append(text).append("</option>").append("\n");
        }

        str.append("</select>").append("\n");
        str.append("</fieldset>");

        return str.toString();

    }

    private String getDataDiv(String data, String id)
    {
        StringBuffer str = new StringBuffer();
        str.append("<div class='x-hidden' name=\"data\" id=\"").append(id).append("\">").append(data).append("</div>");

        return str.toString();

    }
    private String getRecommendationDiv(String text)
    {
        StringBuffer str = new StringBuffer();
        str.append("<div class='x-hidden' name=\"recommendation\">").append(text).append("</div>");

        return str.toString();
    }
    private static TMetricLogVO prepareTmetricLog(String metricData, WikiDocumentVO currDoc, String parentRootDT, String username, boolean isLeaf, boolean isPathComplete, boolean dtviewer, String problemId, String rootDocumentName)
    {
        TMetricLogVO currTmetricVO = new TMetricLogVO();
        TMetricLogVO prevTmetricVO = null;
        String rootDt = null;
        try
        {
            /*
             *  Metric data is Base64 encoded. 
             *  Valid characters are A-Z, a-z, 0-9, '/', '+', and optional padding character '='.
             *   
             *  If metric data is URL encoded then / gets replaced by "%2F", + by "%2B", 
             *  and = by "%3D". (See note below reagrding mixed encoding)
             *  
             *  To identify whether metric data is NOT URL encoded is to 
             *  check for presence of characters '/' or '+'.
             *   
             *  If metric data is mixed encoded i.e. only padding character(s) '=' (at the end)
             *  are URL encoded then string will end with sequence of one or two '%3D' 
             *  corresponding to padding characters.  
             */
            
            if (StringUtils.isNotBlank(metricData) && (metricData.contains("/") || metricData.contains("+")))
            {
                String decodedMetricData = metricData;
                
                // Check for mixed encoding i.e. URL encoding of padding character '=' at the end
                
                if (metricData.endsWith("%3D"))
                {
                    decodedMetricData = metricData.replaceAll("%3D", "=");
                }
                
                metricData = decodedMetricData;
            }
            
            if (StringUtils.isNotBlank(metricData))
            {
                if (metricData.contains("%"))
                {
                    prevTmetricVO = new TMetricLogVO(metricData, true);
                }
                else
                {
                    prevTmetricVO = new TMetricLogVO(metricData);
                }
                
                rootDt =  prevTmetricVO.getRootDt();
                if (StringUtils.isNotBlank(parentRootDT))
                {
                    rootDt = parentRootDT;
                }
            }
            else
            {
                if(dtviewer && StringUtils.isNotEmpty(rootDocumentName))
                {
                    System.out.println("The NEW Decision Viewer was REFRESHED FROM UI. Root is :" + rootDocumentName);
                    Log.log.info("The NEW Decision Viewer was REFRESHED FROM UI. Root is :" + rootDocumentName);
                    rootDt = rootDocumentName;
                }
                else
                {
                    System.out.println("The decision tree started. Root is :" + currDoc.getUFullname());
                    Log.log.info("The decision tree started. Root is :" + currDoc.getUFullname());
                    currTmetricVO.setRootDt(currDoc.getUFullname());
                    rootDt = currDoc.getUFullname();
                }
                
            }
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }

        //data for collecting the metric
        long uniqueNo = JavaUtils.generateUniqueNumber();
        String executionId = null;
        if(dtviewer && StringUtils.isNotBlank(problemId))
        {
            executionId = "EXE-" + problemId + "-" + rootDt;
        }
        else
        {
            executionId = prevTmetricVO != null && StringUtils.isNotBlank(prevTmetricVO.getExecutionId()) ? prevTmetricVO.getExecutionId() : "EXE-" + uniqueNo;
        }
        System.out.println("Execution Id:" + executionId);
        
        //if the problemId is not null, get the info from the WSDATA
        String currPath = null;
        if(StringUtils.isNotBlank(problemId))
        {
            Object property = ServiceWorksheet.getWorksheetWSDATAProperty(problemId, DT_METRIC, username);
            if(property != null)
            {
                try
                {
                    DTMetricLogList dtMetricLogList = (DTMetricLogList) new ObjectMapper().readValue((String) property, DTMetricLogList.class);
                    List<DTMetricLogVO> dtMetricLogs = dtMetricLogList.getDtMetricLogs();
                    DTMetricLogVO currDT = null;
                    
                    //get the object belonging to curr DT in question
                    for(DTMetricLogVO vo : dtMetricLogs)
                    {
                        if(StringUtils.isNotBlank(vo.getRootDtFullName()) && vo.getRootDtFullName().equalsIgnoreCase(rootDt))
                        {
                            currDT = vo;
                            break;
                        }
                    }
                    
                    
//                    metricLog = new ObjectMapper().readValue((String) property, DTMetricLogVO.class);
                    
                    if (currDT != null)
                    {
                        //update the rootDt, exeId
                        rootDt = currDT.getRootDtFullName();
                        executionId = currDT.getExecutionId();
                        //get the curr path
                        List<TMetricLogVO> logs = currDT.getLogs();
                        if (logs != null && logs.size() > 0)
                        {
                            currPath = logs.get(logs.size() - 1).getCurrDTPath() + "-" + currDoc.getUFullname();
                        }
                        else
                        {
                            currPath = currDoc.getUFullname();
                        }
                    }
                    else
                    {
                        currPath = prevTmetricVO != null ? prevTmetricVO.getCurrDTPath() + "-" + currDoc.getUFullname() : currDoc.getUFullname();
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn("Error in parsing the metric data for DT", e);
                }
            }
        }
        else
        {
            currPath = prevTmetricVO != null ? prevTmetricVO.getCurrDTPath() + "-" + currDoc.getUFullname() : currDoc.getUFullname();
        }
        
        Long startTimeInMs = System.currentTimeMillis();

        currTmetricVO.setExecutionId(executionId);
        currTmetricVO.setRootDt(rootDt);
        currTmetricVO.setStartTimeInMS(startTimeInMs + "");
        currTmetricVO.setCurrWikiNode(currDoc.getUFullname());
        currTmetricVO.setCurrDTPath(currPath);
        currTmetricVO.setUsername(username);
        currTmetricVO.setAbortTimeInSecs(currDoc.getUDTAbortTime() != null && currDoc.getUDTAbortTime() > 0 ? currDoc.getUDTAbortTime() + "" : DEFAULT_ABORT_TIME + "");
        if (prevTmetricVO != null)
        {
            currTmetricVO.setPrevWikiNode(prevTmetricVO.getCurrWikiNode());
            currTmetricVO.setPrevWikiStartTimeInMS(prevTmetricVO.getStartTimeInMS());
            currTmetricVO.setStopTimeInMS(startTimeInMs + "");

            String currDTTime = (prevTmetricVO != null && StringUtils.isNotEmpty(prevTmetricVO.getCurrDTPathTimeInMS())) ? (Long.parseLong(prevTmetricVO.getCurrDTPathTimeInMS()) + currTmetricVO.getNodeTimeInMS()) + "" : "" + currTmetricVO.getNodeTimeInMS();
            currTmetricVO.setCurrDTPathTimeInMS(currDTTime);
            currTmetricVO.setIsLeaf(isLeaf ? "true" : "false");
            //set this to 'false' always as it gets evaluated in TMetricDecisionTree.java based on the last node
            currTmetricVO.setIsPathComplete("false");//currTmetricVO.setIsPathComplete(isPathComplete ? "true" : "false");
        }

        return currTmetricVO;
    }

    public static TMetricLogVO prepareTmetricLog(String metricData, String docName, String parentRootDT, String username, boolean isLeaf, boolean isPathComplete, boolean dtviewer, String problemId, String rootDocumentName)//throws Exception
    {
        WikiDocumentVO currDoc = ServiceWiki.getWikiDoc(null, docName, username);
        TMetricLogVO metric = null;
        
        if(currDoc != null)
        {
            metric = prepareTmetricLog(metricData, currDoc, parentRootDT, username, isLeaf, isPathComplete, dtviewer, problemId, rootDocumentName);
        }

        return metric;
    }

    @Override
    public String getLocaleKey()
    {
        return "macro.decision";
    }

}
