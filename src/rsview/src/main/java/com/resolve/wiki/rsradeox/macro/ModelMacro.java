/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.store.HibernateStoreUtil;
import com.resolve.wiki.web.WikiRequestContext;

public class ModelMacro extends BaseLocaleMacro
{

    public final static String AUTOMATION_INLINEMODEL_REFRESHINTERVAL = "automation.inlinemodel.refreshinterval";
    public final static Integer DEFAULT_INTERVAL = 3000; //milliseconds

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {

        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestContext.getWiki();

        String content = params.getContent();
        String refreshStatus = StringUtils.isNotEmpty(params.get("refresh")) ? params.get("refresh") : "";
        refreshStatus = HttpUtil.sanitizeValue(refreshStatus);

        //refresh interval in secs
        String automationRefreshIntervalSec = PropertiesUtil.getPropertyString(AUTOMATION_INLINEMODEL_REFRESHINTERVAL);

        if (StringUtils.isEmpty(automationRefreshIntervalSec))
        {
            automationRefreshIntervalSec = "3";
        }

        String refreshInterval = StringUtils.isNotEmpty(params.get("refreshInterval")) ? params.get("refreshInterval") : automationRefreshIntervalSec;
        refreshInterval = HttpUtil.sanitizeValue(refreshInterval);
        String refreshWiki = StringUtils.isNotEmpty(params.get("wiki")) ? params.get("wiki") : "";
        refreshWiki = HttpUtil.sanitizeValue(refreshWiki);
        String status = StringUtils.isNotEmpty(params.get("status")) ? params.get("status") : "condition";
        status = HttpUtil.sanitizeValue(status);
        String zoom = StringUtils.isNotEmpty(params.get("zoom")) ? params.get("zoom") : "1.0";
        zoom = HttpUtil.sanitizeValue(zoom);
        String iwidth = StringUtils.isNotEmpty(params.get("width")) ? params.get("width") : "100%";
        iwidth = HttpUtil.sanitizeValue(iwidth);
        String iheight = StringUtils.isNotEmpty(params.get("height")) ? params.get("height") : "420";
        iheight = HttpUtil.sanitizeValue(iheight);

        String expand = (StringUtils.isNotEmpty(params.get("expand")) && params.get("expand").equalsIgnoreCase("false")) ? "false" : "true";
        expand = HttpUtil.sanitizeValue(expand);
        boolean isOrderByAsc = (StringUtils.isNotEmpty(params.get("order")) && params.get("order").trim().equalsIgnoreCase("desc")) ? false : true;

        //String exclude = StringUtils.isNotEmpty(params.get("exclude")) ? params.get("exclude") : "";
        String style = StringUtils.isNotEmpty(params.get("style")) ? params.get("style") : "";
        if (expand != null && expand.equalsIgnoreCase("false"))
        {
            style += "display: none;";
        }
        style = HttpUtil.sanitizeValue(style);

        String currentUser = wikiRequestContext.getWikiUser().getUsername();
        currentUser = HttpUtil.sanitizeValue(currentUser);
        
        String documentName = wiki.getDocumentName(wikiRequestContext);
        documentName = HttpUtil.sanitizeValue(documentName);
        String problemId = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_PROBLEMID);
        if (StringUtils.isBlank(problemId))
        {
            String orgId = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_ID);
            orgId = HttpUtil.sanitizeValue(orgId);
            
            String orgName = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_NAME);
            orgName = HttpUtil.sanitizeValue(orgName);
            
            problemId = HibernateStoreUtil.getActiveSid(currentUser, orgId, orgName);
        }
        problemId = HttpUtil.sanitizeValue(problemId);

        //String[] actionTaskNamesArr = null;        
        String divId = "results_" + JavaUtils.generateUniqueNumber();
        String isDoneYetId = "isDoneYetId_" + JavaUtils.generateUniqueNumber();

        String namespace = "";
        String docName = "";

        if (StringUtils.isNotEmpty(refreshWiki) && refreshWiki.contains("."))
        {
            namespace = refreshWiki.substring(0, refreshWiki.indexOf("."));
            docName = refreshWiki.substring(refreshWiki.indexOf(".") + 1, refreshWiki.length());
        }
        else
        {
            refreshWiki = documentName;
            namespace = documentName.substring(0, documentName.indexOf("."));
            docName = documentName.substring(documentName.indexOf(".") + 1, documentName.length());
        }

        if (StringUtils.isNotEmpty(problemId) && !problemId.equals("-1") && !StringUtils.isEmpty(content) && StringUtils.isNotEmpty(namespace) && StringUtils.isNotEmpty(docName))
        {
            //interval in millisecond
            long interval = DEFAULT_INTERVAL;
            try
            {
                interval = Long.parseLong(refreshInterval) * 1000;
            }
            catch (Throwable t)
            {
                interval = DEFAULT_INTERVAL;
            }

            String output = getAutomationModel(namespace, docName, problemId, refreshWiki, status, zoom, refreshStatus, String.valueOf(interval), iwidth, iheight);

            writer.write(output);
        }
        else
        {
            StringBuffer html = new StringBuffer();
            html.append("<div id=\"").append(divId).append("\"style=\"").append(style).append("\" >");
            html.append("<input type='hidden' name='isDoneYet' id='").append(isDoneYetId).append("' value='" + ConstantValues.STOP_AJAX_REFRESH + "'>");
            html.append("</div>");

            writer.write(html.toString());
        }

    }

    public String getAutomationModel(String namespce, String docName, String problemID, String refreshWiki, String status, String zoom, String refreshStatus, String refreshInterval, String width, String iheight)
    {
        String model = "<iframe width=" + width + " height=" + iheight + " frameborder=\"0\" src=\"/resolve/service/wiki/viewmodel/" + namespce + "/" + docName + "?PROBLEMID=" + problemID + "&WIKI=" + refreshWiki + "&STATUS=" + status + "&ZOOM=" + zoom + "&REFRESH=" + refreshStatus + "&REFRESHINTERVAL=" + refreshInterval + "\"></iframe>";
        return model;
    }

    public static String getEndStatusHTML(String severity)
    {
        String result = "";
        String color = "grey";

        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
        {
            color = "red";
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
        {
            color = "orange";
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
        {
            color = "yellow";
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
        {
            color = "green";
        }

        result = "&nbsp&nbsp&nbsp<img border=\"0\" src=\"/resolve/images/colorball_" + color + ".gif\" />";

        return result;
    }

    public static String getEndStatusSeverity(String newSeverity, String currSeverity, String mode)
    {
        String result = currSeverity;

        int newSeverityValue = getSeverityValue(newSeverity);
        int currSeverityValue = getSeverityValue(currSeverity);

        if (mode.equalsIgnoreCase("BEST"))
        {
            if (newSeverityValue > currSeverityValue)
            {
                result = newSeverity;
            }
        }
        else
        {
            if (newSeverityValue < currSeverityValue)
            {
                result = newSeverity;
            }
        }

        return result;
    }

    public static int getSeverityValue(String severity)
    {
        int result = 0;

        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
        {
            result = 4;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
        {
            result = 3;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
        {
            result = 2;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
        {
            result = 1;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_UNKNOWN))
        {
            result = 0;
        }

        return result;
    }

    @Override
    public String getLocaleKey()
    {
        return "macro.model";
    }

    public static void populateArrayString(String actionTaskName, StringBuffer actionNamespaceArr, StringBuffer actionNameArr, StringBuffer nodeIdArr, StringBuffer actionWikiArr, StringBuffer descriptionArr)
    {
        if (!StringUtils.isEmpty(actionTaskName))
        {
            // actiontask
            String actionName = null;
            String actionNamespace = null;
            String actionWiki = null;
            String nodeId = null;
            String description = null;

            // DEF syntax = description>task#namespace(wiki::nodeId)=wiki

            // set description
            int pos = actionTaskName.indexOf('>');
            if (pos > 0)
            {
                description = actionTaskName.substring(0, pos).trim();
                actionTaskName = actionTaskName.substring(pos + 1, actionTaskName.length()).trim();
            }

            // set actionWiki
            pos = actionTaskName.indexOf('=');
            if (pos > 0)
            {
                actionWiki = actionTaskName.substring(pos + 1, actionTaskName.length()).trim();
                actionTaskName = actionTaskName.substring(0, pos).trim();
            }

            // set nodeId
            pos = actionTaskName.indexOf('{');
            if (pos > 0)
            {
                nodeId = actionTaskName.substring(pos + 1, actionTaskName.length() - 1);
                actionTaskName = actionTaskName.substring(0, pos);
            }

            // set actionName and actionNamespace
            pos = actionTaskName.indexOf('#');
            if (pos > 0)
            {
                actionName = actionTaskName.substring(0, pos);
                actionNamespace = actionTaskName.substring(pos + 1, actionTaskName.length());
            }
            else
            {
                actionName = actionTaskName;
            }

            actionNamespaceArr.append("'").append(StringUtils.isNotEmpty(actionNamespace) ? actionNamespace.trim() : "").append("'");
            actionNameArr.append("'").append(StringUtils.isNotEmpty(actionName) ? actionName.trim() : "").append("'");
            nodeIdArr.append("'").append(StringUtils.isNotEmpty(nodeId) ? nodeId.trim() : "").append("'");
            if (actionWikiArr != null)
            {
                actionWikiArr.append("'").append(StringUtils.isNotEmpty(actionWiki) ? actionWiki.trim() : "").append("'");
            }

            if (descriptionArr != null)
            {
                descriptionArr.append("'").append(StringUtils.isNotEmpty(description) ? description.trim() : "").append("'");
            }

        }
    }
}
