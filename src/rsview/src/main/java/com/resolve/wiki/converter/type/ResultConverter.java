/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter.type;

import com.resolve.wiki.converter.AbstractConverter;


/**
 * 
 * 
 * @author jeet.marwah
 *
 */
public class ResultConverter extends AbstractConverter
{

	private String CSS_NAME = "result";
	private String HTML_START_TAG = "<span class=\"" + CSS_NAME + "\">";
	private String HTML_END_TAG = "</span>";
	private String WIKI_START_TAG = "{result}";
	private String WIKI_END_TAG = "{result}";

	private String pattern = "\\{result(.*?)\\{result\\}";
	
	@Override
	public String getHTML2WikiPattern()
	{
		return pattern;
	}
	
	@Override
	public String getWiki2HTMLPattern()
	{
		return pattern;
	}
	
	@Override
	public String getCSS_NAME()
	{
		return CSS_NAME;
	}

	@Override
	public String getHTML_START_TAG()
	{
		return HTML_START_TAG;
	}

	@Override
	public String getHTML_END_TAG()
	{
		return HTML_END_TAG;
	}

	@Override
	public String getWIKI_END_TAG()
	{
		return WIKI_END_TAG;
	}

	@Override
	public String getWIKI_START_TAG()
	{
		return WIKI_START_TAG;
	}



}//JavascriptConverter
