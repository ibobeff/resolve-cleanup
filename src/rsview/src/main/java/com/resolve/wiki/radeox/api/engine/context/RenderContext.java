/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.api.engine.context;

import com.resolve.wiki.radeox.api.engine.RenderEngine;

import java.util.Map;


/**
 * RenderContext stores basic data for the context
 * the RenderEngine is called in. RenderContext
 * can be used by the Engine in whatever way it likes to.
 * The Radeox RenderEngine uses RenderContext to
 * construct FilterContext.
 *
 */

public interface RenderContext {
//  public final static String INPUT_BUNDLE_NAME = "RenderContext.input_bundle_name";
//  public final static String OUTPUT_BUNDLE_NAME = "RenderContext.output_bundle_name";
//  public final static String LANGUAGE_BUNDLE_NAME = "RenderContext.language_bundle_name";
//  public final static String LANGUAGE_LOCALE = "RenderContext.language_locale";
//  public final static String INPUT_LOCALE = "RenderContext.input_locale";
//  public final static String OUTPUT_LOCALE = "RenderContext.output_locale";
  public final static String DEFAULT_FORMATTER = "RenderContext.default_formatter";
  
//  public final static String WIKI_PROPERTIES_FILENAME = "RenderContext.filename";

  /**
   * Returns the RenderEngine handling this request.
   *
   * @return engine RenderEngine handling the request within this context
   */
  public RenderEngine getRenderEngine();

  /**
   * Stores the current RenderEngine of the request
   *
   * @param engine Current RenderEnginge
   */
  public void setRenderEngine(RenderEngine engine);

  public Object get(String key);

  public void set(String key, Object value);

  public Map getParameters();

  /**
   * Set the parameters for this execution context. These
   * parameters are read when encountering a variable in
   * macros like {search:$query} or by ParamFilter in {$query}.
   * Query is then read from
   * the parameter map before given to the macro
   *
   * @param parameters Map of parameters with name,value pairs
   */
  public void setParameters(Map parameters);

  public void setCacheable(boolean cacheable);

  public void commitCache();

  public boolean isCacheable();
}
