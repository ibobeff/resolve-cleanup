/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet
{
	private static final long serialVersionUID = 7608887345586399653L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

//		Locale locale = new Locale(Locale.US);
		File file = new File("radeox.properties");
		System.out.println(file.getAbsolutePath());
	    FileInputStream fis = new FileInputStream(file);


		ResourceBundle rs = new PropertyResourceBundle(fis);// ResourceBundle.getBundle("radeox", Locale.US);//, locale);
		PrintWriter out = response.getWriter();
		out.println("This is a test");
		
		fis.close();
	}

}
