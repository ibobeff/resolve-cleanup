/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.web.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.exception.WikiException;
import com.resolve.services.exception.WikiExceptionCodes;
import com.resolve.util.Log;
import com.resolve.wiki.web.WikiRequestContext;
import com.resolve.wiki.web.WikiURLFactory;

/**
 * This URL factory and will be implemented as a SINGLETON
 * 
 * @author jeet.marwah
 * 
 */
public class WikiURLFactoryImpl implements WikiURLFactory
{
	//this will be set once when the server comes up using the WIkiRequestContext as it has HttpRequestObject
	protected static URL serverURL = null;
	protected static String basePath = "";

	private static WikiURLFactoryImpl instance = null;

	private WikiURLFactoryImpl()
	{
	}

	public static WikiURLFactoryImpl getInstance()
	{
		if (instance == null)
		{
			synchronized (WikiURLFactoryImpl.class)
			{
				if (instance == null)
				{
					instance = new WikiURLFactoryImpl();
				}
			}
		}
		return instance;
	}

	public void initialize(WikiRequestContext context)
	{
		//just for testing -- will be removed 
		HttpServletRequest request = context.getHttpServletRequest();
		if(request == null)
		{
			return;
		}
		String pathInfo = request.getPathInfo();///wiki/view/Test/test2
		String pathTransalated = request.getPathTranslated();//C:\project\resolve3\dist\tomcat\webapps\resolve\wiki\view\Test\test2
		String contextPath = request.getContextPath();///resolve
		String servletPath = request.getServletPath();///service

		String localAddr = request.getLocalAddr();//127.0.0.1
		String localName = request.getLocalName();//localhost
		int localPort = request.getLocalPort();//8080
		String serverName = request.getServerName();//localhost
		int serverPort = request.getServerPort();//8080
		String method = request.getMethod();
		String queryString = request.getQueryString();
		String requestURI = request.getRequestURI();///resolve/service/wiki/view/Test/test2
		StringBuffer requestURL1 = request.getRequestURL();//http://localhost:8080/resolve/service/wiki/view/Test/test2

		if (serverURL == null)
        {
            //http://localhost:8080/resolve/service/wiki/view/Test/test2
            //URL url = getRequestURL(context.getHttpServletRequest());
            //basePath = request.getContextPath() + request.getServletPath() + "/wiki/";
            basePath = "resolve/service/wiki/";
            try
            {
                //server URL is read from the system property name system.resolve.url
                URL url = new URL(RSContext.resolveUrl);
                serverURL = new URL(url, "/");//http://localhost:8080/
            }
            catch (MalformedURLException e)
            {
                // This can't happen
                Log.log.fatal("Check system.resolve.url system property value, currently it is: " + RSContext.resolveUrl, e);
            }
        }
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Implemented methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	/**
//	 * Creates the URL for the attachment action - can have edit, view, download, execute values
//	 * 
//	 * eg. http://localhost:8080/xwiki/bin/download/Test/test1/test.rtf
//	 */
//	@Override
//	public URL createAttachmentURL(Map<String, String> values, WikiRequestContext wikiRequestContext, String fileName, String action) throws WikiException
//	{
//
//		try
//		{
//			// TODO Auto-generated method stub
//			return new URL("http://localhost:8080/xwiki/bin/download/Test/test1/test.rtf");
//		}
//		catch (Exception e)
//		{
//			throw new WikiException(new Exception(e));
//		}
//	}

	@Override
	public URL createURL(Map<String, String> values, WikiRequestContext context, String action, String querystring, String anchor)
	{
	    return createURL(values, action, querystring, anchor);
	}
	
	@Override
    public URL createURL(Map<String, String> values, String action, String querystring, String anchor)
	{
        StringBuffer newpath = new StringBuffer(basePath);

        String namespace = values.get(ConstantValues.WIKI_NAMESPACE_KEY);//wikiDocument.getNamespace();
        String name = values.get(ConstantValues.WIKI_DOCNAME_KEY);//wikiDocument.getName();

        addAction(newpath, action);
        addSpace(newpath, namespace);
        addName(newpath, name);

        if ((querystring != null) && (!querystring.equals("")))
        {
            newpath.append("?");
            newpath.append(querystring);
            // newpath.append(querystring.replaceAll("&","&amp;"));
        }

        if ((anchor != null) && (!anchor.equals("")))
        {
            newpath.append("#");
            newpath.append(anchor);
        }

        try
        {
            return new URL(serverURL, newpath.toString());
        }
        catch (MalformedURLException e)
        {
            // This should not happen
            return null;
        }
	    
	}

	@Override
	public String getURL(URL url, WikiRequestContext context)
	{
	    return getURL(url);
	}
	
	@Override
    public String getURL(URL url)
    {
	    try
        {
            if (url == null)
                return "";

            String surl = url.toString();
            if (!surl.startsWith(serverURL.toString()))
                return surl;
            else
            {
                StringBuffer sbuf = new StringBuffer(url.getPath());
                String querystring = url.getQuery();
                if ((querystring != null) && (!querystring.equals("")))
                {
                    sbuf.append("?");
                    sbuf.append(querystring);
                    // sbuf.append(querystring.replaceAll("&","&amp;"));
                }

                String anchor = url.getRef();
                if ((anchor != null) && (!anchor.equals("")))
                {
                    sbuf.append("#");
                    sbuf.append(anchor);
                }
                return sbuf.toString();
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            return "";
        }
    }

	/**
	 * replaces the string in the url eg. String requestURL = "http://localhost:8080/resolve/service/wiki/save/Test/test2"; String findStr = "save"; String replacewith = "edit";
	 * 
	 * System.out.println(createForwardUrl(requestURL, findStr, replacewith));
	 * 
	 * 
	 * from RPC --> http://localhost:8080/resolve/rswiki/WikiService.rpc
	 * 
	 * @param requestURL
	 * @param findStr
	 * @param replaceWith
	 * @return
	 */
	//	@Override
	//	public String createForwardUrl(String requestURL, String findStr, String replaceWith)
	//	{
	//		StringBuffer url = new StringBuffer();
	//		url.append(requestURL.substring(0, requestURL.indexOf(findStr)));
	//		url.append(replaceWith);
	//		url.append(requestURL.substring(requestURL.indexOf(findStr) + findStr.length()));
	//		return url.toString();
	//
	//	}
	@Override
	public String createForwardUrl(WikiAction wikiAction, String namespace, String docName, String queryString)
	{

		StringBuffer url = new StringBuffer();
//		url.append(serverURL.toString());//http://localhost:8080/
		url.append("/");
		url.append(basePath);//http://localhost:8080/resolve/service/wiki/
		url.append(wikiAction.toString()).append("/");//http://localhost:8080/resolve/service/wiki/view/
		if(namespace != null && namespace.trim().length()>0)
		{
			url.append(namespace).append("/");//http://localhost:8080/resolve/service/wiki/view/Test/
		}
		
		if(docName != null && docName.trim().length() > 0)
		{
			url.append(docName);//http://localhost:8080/resolve/service/wiki/view/Test/test2
		}
		if (queryString != null && queryString.trim().length() > 0)
		{
			url.append("?").append(queryString);//http://localhost:8080/resolve/service/wiki/view/Test/test2?controlId=ldoe33jr04
		}

		return url.toString();
	}
	
	@Override
	public String createForwardUrl(WikiAction wikiAction, String docFullName, String queryString)
	{
		String[] arr = docFullName.split("\\.");
		String namespace = arr[0];
		String docName = arr[1];
		
		return createForwardUrl(wikiAction, namespace, docName, queryString);
	}

	/**
	 * returns 'http://localhost:8080/'
	 * 
	 * @return
	 */
	@Override
	public String getServerURL()
	{
		String strUrl = serverURL.toString();
		if(strUrl.endsWith("/"))
		{
			strUrl = strUrl.substring(0, strUrl.length()-1);
		}
		
		return strUrl;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Other methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private URL getRequestURL(HttpServletRequest request)
	{
		try
		{
			StringBuffer requestURL = request.getRequestURL();
			String qs = request.getQueryString();
			if ((qs != null) && (!qs.equals("")))
				return new URL(requestURL.toString() + "?" + qs);
			else
				return new URL(requestURL.toString());
		}
		catch (Exception e)
		{
			WikiException e1 = new WikiException(WikiExceptionCodes.MODULE_WIKI_APP, WikiExceptionCodes.ERROR_WIKI_APP_URL_EXCEPTION,
					"Exception while getting URL from request", e);
			Log.log.error(e1.getMessage(), e1);
			return null;
		}
	}

	private void addAction(StringBuffer newpath, String action)
	{
		boolean showViewAction = true;
		if ((!action.equals("view") || (showViewAction)))
		{
			newpath.append(action);
			newpath.append("/");
		}
	}

	private void addSpace(StringBuffer newpath, String web)
	{

		newpath.append(ControllerUtil.encode(web));
		newpath.append("/");
	}

	private void addName(StringBuffer newpath, String name)
	{
		newpath.append(ControllerUtil.encode(name));
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Setter and getter methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
