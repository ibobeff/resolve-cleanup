/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.context;

/**
 * InitialFilterContext is used to
 * give the filter information after it's startup
 * (e.g. locales)
 *
 * @author Stephan J. Schmidt
 * @version $Id: InitialFilterContext.java,v 1.1 2003/08/11 13:19:57 stephan Exp $
 */

public interface InitialFilterContext {
}
