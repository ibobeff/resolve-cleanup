/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.VelocityContext;

import com.resolve.services.vo.WikiUser;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.api.WikiAPI;
import com.resolve.wiki.api.WikiDocumentAPI;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;

/**
 * This interface is for each REQUEST that comes in. The purpose is to get everything that is required related to a request at 1 place.
 * 
 * Request, Response, (WikiDoc, User and his Credentials, etc) can use the set and get attributes
 * 
 * @author jeet.marwah
 * 
 */

public interface WikiRequestContext
{
	public void setHttpServletRequest(HttpServletRequest request);

	public HttpServletRequest getHttpServletRequest();

	public void setHttpServletResponse(HttpServletResponse response);

	public HttpServletResponse getHttpServletResponse();

	public Object getAttribute(String name);

	public void setAttribute(String name, Object object);

	public void removeAttribute(String name);
	
	public void addAttributes(Map<String, Object> hmAdditional);

//	public void setServletContext(ServletContext sc);
//
//	public ServletContext getServletContext();

	public WikiDocumentAPI getWikiDocumentAPI();

	public void setWikiDocumentAPI(WikiDocumentAPI wikiDocAPI);

    public WikiDocumentAPI getCurrWikiDocumentAPI();

    public void setCurrWikiDocumentAPI(WikiDocumentAPI wikiDocAPI);

    public Wiki getWiki();

	public void setWiki(Wiki wiki);
	
    public WikiAPI getWikiAPI();

    public void setWikiAPI(WikiAPI wiki);

    public VelocityContext getVelocityContext();
	
	public void setVelocityContext(VelocityContext vc);
	
	public WikiUser getWikiUser();
	
	public void setWikiUser(WikiUser wikiUser);
	
	public RenderContext getRenderContext();
	
	public void setRenderContext(RenderContext renderContext);
	
	public void log(String logMessage);

	public String getLogMessages();


}
