/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

public class WikiNotExistException extends RuntimeException
{
    private static final long serialVersionUID = -1881637516599024565L;

	public WikiNotExistException(String msg)
    {
        super(msg);
    }
    
} // WikiNotExistException
