/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.api;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import com.resolve.util.Log;

/**
 * Stores information and links to api documentation, e.g. for Java, Ruby, JBoss
 * 
 */

public class ApiDoc
{
    private static ApiDoc instance;
    private Map apiDocs;

    public static synchronized ApiDoc getInstance()
    {
        if (null == instance)
        {
            instance = new ApiDoc();
        }
        return instance;
    }

    public ApiDoc()
    {
        apiDocs = new HashMap();

        BufferedReader br = null;
        boolean fileNotFound = false;
        
        try
        {
            br = new BufferedReader(new InputStreamReader(new FileInputStream("conf/apidocs.txt")));
            addApiDoc(br);
        }
        catch (IOException e)
        {
            Log.log.warn("Unable to read conf/apidocs.txt");
            fileNotFound = true;
        }
        finally
        {
            if (br != null)
            {
                try
                {
	                br.close();
                }
                catch (Exception e) { };
            }
        }

        if (fileNotFound)
        {
            br = null;
            try
            {
                br = new BufferedReader(new InputStreamReader(ApiDoc.class.getResourceAsStream("/conf/apidocs.txt")));
                addApiDoc(br);
            }
            catch (Exception e)
            {
                Log.log.warn("Unable to read conf/apidocs.txt from jar");
            }
	        finally
	        {
                try
                {
	                br.close();
                }
                catch (Exception e) { };
	        }
        }
    }

    public void addApiDoc(BufferedReader reader) throws IOException
    {
        String line;
        while ((line = reader.readLine()) != null)
        {
            StringTokenizer tokenizer = new StringTokenizer(line, " ");
            String mode = tokenizer.nextToken();
            String baseUrl = tokenizer.nextToken();
            String converterName = tokenizer.nextToken();
            ApiConverter converter = null;
            try
            {
                converter = (ApiConverter) Class.forName("com.resolve.wiki.radeox.macro.api." + converterName + "ApiConverter").newInstance();
            }
            catch (Exception e)
            {
                Log.log.warn("Unable to load converter: " + converterName + "ApiConverter", e);
            }
            converter.setBaseUrl(baseUrl);
            apiDocs.put(mode.toLowerCase(), converter);
        }
    }

    public boolean contains(String external)
    {
        return apiDocs.containsKey(external);
    }

    public Writer expand(Writer writer, String className, String mode) throws IOException
    {
        mode = mode.toLowerCase();
        if (apiDocs.containsKey(mode))
        {
            writer.write("<a href=\"");
            ((ApiConverter) apiDocs.get(mode)).appendUrl(writer, className);
            writer.write("\">");
            writer.write(className);
            writer.write("</a>");
        }
        else
        {
            Log.log.warn(mode + " not found");
        }
        return writer;
    }

    public Writer appendTo(Writer writer) throws IOException
    {
        writer.write("{table}\n");
        writer.write("Binding|BaseUrl|Converter Name\n");
        Iterator iterator = apiDocs.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry entry = (Map.Entry) iterator.next();
            writer.write((String) entry.getKey());
            ApiConverter converter = (ApiConverter) entry.getValue();
            writer.write("|");
            writer.write(converter.getBaseUrl());
            writer.write("|");
            writer.write(converter.getName());
            writer.write("\n");
        }
        writer.write("{table}");
        return writer;
    }

}
