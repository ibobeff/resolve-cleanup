/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.filter;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.LocaleRegexTokenFilter;
import com.resolve.wiki.radeox.regex.MatchResult;
import com.resolve.wiki.util.TOCGenerator;

public class WikiHeadingFilter extends LocaleRegexTokenFilter
{

    private MessageFormat formatter;

    public static final String LOCAL_KEY = "filter.heading";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "^[\\p{Blank}]*(1(\\.1)*)[\\p{Blank}]+(.*?)$");
        addKeyValue(LOCAL_KEY + ".print", "<h{4} class=\"heading-{1}\" id=\"{5}\">{2}{3}</h{4}>");
    }

    @Override
    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }

    public void setInitialContext(InitialRenderContext context)
    {
        super.setInitialContext(context);
        String outputTemplate = values.get(getLocaleKey() + ".print");
        formatter = new MessageFormat("");
        formatter.applyPattern(outputTemplate);
    }

    @Override
    public void handleMatch(StringBuffer buffer, MatchResult result, FilterContext context)
    {
        String id = null;
        String level = result.group(1);
        int level_i = (level.length() + 3) / 2;
        String hlevel = (level_i <= 6 ? level_i : 6) + "";
        String text = result.group(3);
        String numbering = "";

        RenderContext rcontext = context.getRenderContext();
        // generate unique ID of the heading
        List<String> processedHeadings = (List<String>) rcontext.get("processedHeadings");
        if (processedHeadings == null)
        {
            processedHeadings = new ArrayList<String>();
            rcontext.set("processedHeadings", processedHeadings);
        }
        boolean isIdOk = false;
        id = TOCGenerator.makeHeadingID(text, 0);
        while (!isIdOk)
        {
            int occurence = 0;
            for (String key : processedHeadings)
            {
                if (id.equals(key))
                {
                    occurence++;
                }
            }
            id = TOCGenerator.makeHeadingID(id, occurence);
            if (occurence == 0)
            {
                isIdOk = true;
            }
        }
        processedHeadings.add(id);

        buffer.append(formatter.format(new Object[] { id, level.replace('.', '-'), numbering, text, hlevel, id }));
    }
}
