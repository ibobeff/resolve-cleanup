/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.engine;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.WikiUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.EngineManager;
import com.resolve.wiki.radeox.api.engine.ImageRenderEngine;
import com.resolve.wiki.radeox.api.engine.WikiRenderEngine;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.web.WikiRequestContext;
import com.resolve.wiki.web.WikiURLFactory;

/**
 * This is the main rendering engine for the filters and macros that are conf using radeox
 * 
 * @author jeet.marwah
 * 
 */
public class WikiRadeoxRenderEngine extends BaseRenderEngine implements WikiRenderEngine, ImageRenderEngine
{

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public WikiRadeoxRenderEngine()
	{
	}

	/**
	 * Name of the RenderEngine. This is used to get a RenderEngine instance with EngineManager.getInstance(name);
	 * 
	 * @return name Name of the engine
	 */
	@Override
	public String getName()
	{
		return EngineManager.WIKI_DEFAULT;
	}
//	public WikiRadeoxRenderEngine(InitialRenderContext context)
//	{
//		super(context);
//
//	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Implemented methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * @deprecated
	 * will be decommissioned and clean up
	 */
	@Override
	public void appendCreateLink(InitialRenderContext initialContext, StringBuffer buffer, String name, String view)
	{

		String originalName = name;
		
		int colonIndex = name.indexOf(":");
		if (colonIndex != -1)
		{
			name = name.substring(colonIndex + 1);
		}
		int qsIndex = name.indexOf("?");
		String urlParams = null;
		if (qsIndex != -1)
		{
			name = name.substring(0, qsIndex);
			urlParams = originalName.substring(qsIndex + 1);
		}

		// + is use for spaces
		name = name.replace('+', ' ');

		WikiRequestContext context = (WikiRequestContext)initialContext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
		Wiki wiki = context.getWiki();
//		WikiDocument wikidoc = (WikiDocument) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WIKIDOCUMENT);
//		wikidoc.setFullname(name);
//		try
//		{
//			wikidoc.load();
//		}
//		catch (Exception e)
//		{
//			Log.log.error(e.getMessage(), e);//TODO: we have do something when a doc does not exists in the database. Don't know yet what to do
//		}

		String namespace =(String) context.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
		String docName = "";
		if(name.indexOf(".") > 0)
		{
			String[] arr = name.split("\\.");
			namespace = arr[0];
			docName = arr[1];
		}
		else
		{
			docName = name;
		}
		
		WikiURLFactory urlFactory = wiki.getWikiURLFactory();
		Map<String, String> values = new HashMap<String, String>();
		values.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
		values.put(ConstantValues.WIKI_DOCNAME_KEY, docName);
		
		String querystring = urlParams;
		
		URL url = urlFactory.createURL(values, context, "view", querystring, null);
		String surl = urlFactory.getURL(url, context);
//		surl = "#RS.wiki.Main/name=" + values.get("namespace") + "." + values.get("docname");
		buffer.append("<a class=\"wikicreatelink\" onclick=\"return openLink(this, {name:'" + namespace + "." + docName + "'})\" href=\"");
		buffer.append(surl);
		buffer.append("\">");
		buffer.append("<span class=\"wikicreatelinktext\">");
		buffer.append(view);
		buffer.append("</span>");
		buffer.append("<span class=\"wikicreatelinkqm\">?</span>");
		buffer.append("</a>");

	}
	
	@Override
	public void appendCreateLink(Wiki wiki, StringBuffer buffer, String name, String defaultNamespace, String view)
	{
	    String originalName = name;
        
        int colonIndex = name.indexOf(":");
        if (colonIndex != -1)
        {
            name = name.substring(colonIndex + 1);
        }
        int qsIndex = name.indexOf("?");
        String urlParams = null;
        if (qsIndex != -1)
        {
            name = name.substring(0, qsIndex);
            urlParams = originalName.substring(qsIndex + 1);
        }

        // + is use for spaces
        name = name.replace('+', ' ');

        String namespace = defaultNamespace;
        String docName = "";
        if(name.indexOf(".") > 0)
        {
            String[] arr = name.split("\\.");
            namespace = arr[0];
            docName = arr[1];
        }
        else
        {
            docName = name;
        }
        
        WikiURLFactory urlFactory = wiki.getWikiURLFactory();
        Map<String, String> values = new HashMap<String, String>();
        values.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
        values.put(ConstantValues.WIKI_DOCNAME_KEY, docName);
        
        String querystring = urlParams;
        
        URL url = urlFactory.createURL(values, "view", querystring, null);
        String surl = urlFactory.getURL(url);
        buffer.append("<a class=\"wikicreatelink\" href=\"");
        buffer.append(surl);
        buffer.append("\">");
        buffer.append("<span class=\"wikicreatelinktext\">");
        buffer.append(view);
        buffer.append("</span>");
        buffer.append("<span class=\"wikicreatelinkqm\">?</span>");
        buffer.append("</a>");

        
	}
	

	@Override
    public void appendCreateLink(InitialRenderContext initialContext, StringBuffer buffer, String name, String view, String target)
    {
        // + is use for spaces
        name = name.replace('+', ' ');
        WikiRequestContext context = (WikiRequestContext)initialContext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        String namespace =(String) context.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
        String docName = "";
        if(name.indexOf(".") > 0)
        {
            String[] arr = name.split("\\.");
            namespace = arr[0];
            docName = arr[1];
        }
        else
        {
            docName = name;
        }
      
        String surl="/resolve/jsp/rsclient.jsp#RS.wiki.Main/name=" + namespace + "." + docName;
        buffer.append("<a class=\"wikicreatelink\" onclick=\"return openLink(this, {name:'" + namespace + "." + docName + "'})\" href=\"");
        buffer.append(surl);
        if(StringUtils.isNotBlank(target))
            buffer.append("\" target=\"" + target);
        buffer.append("\">");
        buffer.append("<span class=\"wikicreatelinktext\">");
        buffer.append(view);
        buffer.append("</span>");
        buffer.append("<span class=\"wikicreatelinkqm\">?</span>");
        buffer.append("</a>");
    }

	@Override
	public void appendLink(InitialRenderContext initialContext, StringBuffer buffer, String name, String view, String anchor, String target)
	{
		// allow using spaces in links to anchors
		if (anchor != null)
			anchor = anchor.replace(' ', '+');

		if (name.length() == 0 && anchor != null)
		{
			appendInternalLink(buffer, view, anchor);
		}
		else
		{
            WikiRequestContext context = (WikiRequestContext)initialContext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
            Wiki wiki = context.getWiki();

            String defaultNamespace =(String) context.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);

            appendLink(wiki, buffer, name, defaultNamespace, view,  anchor, target);
		}

	}
	
	@Override
	public void appendLink(Wiki wiki, StringBuffer buffer, String name, String defaultNamespace, String view, String anchor, String target)
    {
	    int colonIndex = name.indexOf(":");
        if (colonIndex != -1)
        {
            name = name.substring(colonIndex + 1);
        }

        String urlParams = null;
        String wikiName = name;
        int qsIndex = name.indexOf("?");
        if (qsIndex != -1)
        {
            wikiName = name.substring(0, qsIndex);
            urlParams = name.substring(qsIndex + 1);
        }

//        buffer.append("<span class=\"wikilink\"><a href=\"");

        // + is use for spaces
        name = wikiName.replace('+', ' ');

        String namespace = defaultNamespace;
        String docName = "";
        if(name.indexOf(".") > 0)
        {
            String[] arr = name.split("\\.");
            namespace = arr[0];
            docName = arr[1];
        }
        else
        {
            docName = name;
        }
        
        appendLink(wiki, buffer, namespace, docName, urlParams, view, anchor, target);
    }
	
	//anchor is for router params
	public void appendLink(Wiki wiki, StringBuffer buffer, String namespace, String docName, String urlParams, String view, String anchor, String target)
    {
//	    WikiURLFactory urlFactory = wiki.getWikiURLFactory();

        Map<String, String> values = new HashMap<String, String>();
        values.put(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
        values.put(ConstantValues.WIKI_DOCNAME_KEY, docName);
                    
//        String querystring = urlParams;
        
        //http://localhost:8080/xwiki/bin/view/Main/
        //URL url = urlFactory.createURL(values, "view", querystring, anchor);
        //String surl = urlFactory.getURL(url);
        String surl = "/resolve/jsp/rsclient.jsp";
        if(!StringUtils.isEmpty(urlParams))
            surl+="?"+urlParams;
        surl+="#RS.wiki.Main/name=" + values.get("namespace") + "." + values.get("docname") + (StringUtils.isEmpty(anchor)?"":"&" + anchor);
        buffer.append("<span class=\"wikilink\"><a ");
        if( StringUtils.isNotEmpty(target) )buffer.append(" target=\"" + target + "\" ");
        buffer.append("onclick=\"return openLink(this, {name:'" + namespace + "." + docName + "'})\" href=\"");
        buffer.append(surl);
        buffer.append("\">");
        buffer.append(view);
        buffer.append("</a></span>");
    }
	

	@Override
	public void appendLink(InitialRenderContext initialContext, StringBuffer buffer, String name, String view, String target)
	{
		appendLink(initialContext,buffer, name, view, null, target);

	}

	/**
	 * This method checks if the document exists with this name
	 * 
	 */
	@Override
	public boolean exists(InitialRenderContext initialContext, String fullname)
	{
		if (StringUtils.isEmpty(fullname))
			return false;

		try
		{
//			// check if name contains $ for variable reference
//			if (fullname.indexOf('$') != -1)
//			{
//				return false;
//			}
//
//			int colonIndex = fullname.indexOf(":");
//			if (colonIndex != -1)
//			{
//				return false;
//			}
//
//			int qsIndex = fullname.indexOf("?");
//			if (qsIndex != -1)
//			{
//				fullname = fullname.substring(0, qsIndex);
//			}
//
//			// + is use for spaces
//			fullname = fullname.replace('+', ' ');
			
//			WikiRequestContext context = (WikiRequestContext)initialContext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
//			Wiki wiki = context.getWiki();
//			String namespace =(String) context.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
//			
//			String docName = "";
//			if(fullname.indexOf(".") > 0)
//			{
//				String[] arr = fullname.split("\\.");
//				namespace = arr[0];
//				docName = arr[1];
//			}
//			else
//			{
//				docName = fullname;
//			}
			

			//query the DB to find out if there exists a document with 'fullname'
//			WikiDocument wikidoc = new WikiDocument();
//			wikidoc.setUFullname(namespace.trim() + "." + docName.trim());
			
			boolean exists = WikiUtils.isWikidocExist(fullname);//wiki.getWikiStore().isWikiDocumentAvailable(wikidoc);

			// If the document exists with the spaces and accents converted then
			// we use this one
			if (exists)
				return true;
			else
				return false;

		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			return false;
		}

	}

	@Override
	public boolean showCreate()
	{
		return true;
	}

	@Override
	public String getExternalImageLink()
	{
		return "";
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Other methods
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void appendInternalLink(StringBuffer buffer, String view, String anchor)
	{
		buffer.append("<span class=\"wikilink\"><a href=\"#");
		buffer.append(anchor);
		buffer.append("\">");
		if (view.length() == 0)
		{
			view = ControllerUtil.decode(anchor);
		}
		buffer.append(view);
		buffer.append("</a></span>");
	}

}
