/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.parameter;

import com.resolve.wiki.radeox.api.engine.context.RenderContext;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class BaseMacroParameter implements MacroParameter {
  private String content;
  protected Map params;
  private int size;
  protected RenderContext context;
  private int start;
  private int end;
  private int contentStart;
  private int contentEnd;

  public BaseMacroParameter() {
  }

  public BaseMacroParameter(RenderContext context) {
    this.context = context;
  }

  public void setParams(String stringParams) {
    params = split(stringParams, "|");
    size = params.size();
  }

  public RenderContext getContext() {
    return context;
  }

  public Map getParams() {
    return params;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getLength() {
    return size;
  }

  public String get(String index, int idx) {
    String result = get(index);
    if (result == null) {
      result = get(idx);
    }
    return result;
  }

  public String get(String index) {
    return (String) params.get(index);
  }

  public String get(int index) {
    return get("" + index);
  }

  /**
   *
   * Splits a String on a delimiter to a List. The function works like
   * the perl-function split.
   *
   * @param aString    a String to split
   * @param delimiter  a delimiter dividing the entries
   * @return           a Array of splittet Strings
   */

  private Map split(String aString, String delimiter) {
    Map result = new HashMap();

    if (null != aString) {
      StringTokenizer st = new StringTokenizer(aString, delimiter);
      int i = 0;

      while (st.hasMoreTokens()) {
        String value = st.nextToken();
        String key = "" + i;
        if (value.indexOf("=") != -1) {
          // Store this for
          result.put(key.trim(), insertValue(value.trim()));
          int index = value.indexOf("=");
          key = value.substring(0, index);
          value = value.substring(index + 1);

          result.put(key.trim(), insertValue(value.trim()));
        } else {
          result.put(key.trim(), insertValue(value.trim()));
        }
        i++;
      }
    }
    return result;
  }

  private String insertValue(String s) {
      // HP TODO Support substitution of multiple global variables instad of single global variable
      int idx = s.indexOf('$');
      if (idx != -1) {
          StringBuffer tmp = new StringBuffer();
          Map globals = context.getParameters();
      
          if (globals != null && !globals.isEmpty()) {
              if (s.length() > (idx + 1)) {
                  tmp.append(s.substring(0, idx));
                  String var = s.substring(idx + 1);
                  
                  String globalVarName = var;
                  String suffixAfterGlobalVarName = "";
                  
                  if (var.indexOf(' ') != -1) {
                      globalVarName = var.substring(0, var.indexOf(' '));
                      suffixAfterGlobalVarName = var.substring(var.indexOf(' '));
                  }
                  
                  //if (idx > 0) tmp.append(s.substring(0, idx));
                  if (globals.containsKey(globalVarName)) {
                      tmp.append(globals.get(globalVarName)).append(suffixAfterGlobalVarName);
                  }
                  else {
                      tmp.append(idx);
                  }
                  
                  return tmp.toString();
              }
          }
      }
      
      return s;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public void setEnd(int end) {
    this.end = end;
  }

  public int getStart() {
    return this.start;
  }

  public int getEnd() {
    return this.end;
  }

  public int getContentStart() {
    return contentStart;
  }

  public void setContentStart(int contentStart) {
    this.contentStart = contentStart;
  }

  public int getContentEnd() {
    return contentEnd;
  }

  public void setContentEnd(int contentEnd) {
    this.contentEnd = contentEnd;
  }
}
