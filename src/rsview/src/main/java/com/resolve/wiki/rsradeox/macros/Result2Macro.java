/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import org.owasp.esapi.ESAPI;

import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

public class Result2Macro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.result2";
    }

    private String sanitizeValue(String value)
    {
        if (StringUtils.isEmpty(value))
        {
            return "";
        }
                        
        value = ESAPI.encoder().encodeForHTML(value);

        try
        {
            value = ESAPI.validator().getValidInput("Invalid return value ", value, "ResolveText", 400000, true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
            value = "Failed to validate input value"; 
        }
        return value;
    }
    
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        //Get parameters
        String title = StringUtils.isNotEmpty(params.get("title")) ? params.get("title") : "Show / Hide Results";
        title = sanitizeValue(title);
                
        String intervalStr = params.get("refreshInterval");
        intervalStr = sanitizeValue(intervalStr);
        int refreshInterval = Integer.valueOf(StringUtils.isNotEmpty(intervalStr) ? intervalStr : "5");
//        int refreshInterval = Integer.valueOf(StringUtils.isNotEmpty(params.get("refreshInterval")) ? params.get("refreshInterval") : "5");
        if (refreshInterval < 5)
        {
        	refreshInterval = 5;
        }
        
        String refreshCountMaxStr = params.get("refreshCountMax");
        refreshCountMaxStr = sanitizeValue(refreshCountMaxStr);
//        int refreshCountMax = Integer.valueOf(StringUtils.isNotEmpty(params.get("refreshCountMax")) ? params.get("refreshCountMax") : "60");
        int refreshCountMax = Integer.valueOf(StringUtils.isNotEmpty(refreshCountMaxStr) ? refreshCountMaxStr : "60");
        
        if (refreshCountMax < 1)
        {
        	refreshCountMax = 60;
        }
        int descriptionWidth = Integer.valueOf(StringUtils.isNotEmpty(params.get("descriptionWidth")) ? params.get("descriptionWidth") : "0");
        Boolean encodeSummary = Boolean.valueOf(StringUtils.isNotEmpty(params.get("encodeSummary")) ? params.get("encodeSummary") : "true");
        String order = StringUtils.isNotEmpty(params.get("order")) ? params.get("order") : "";
        order = sanitizeValue(order);
        
        Boolean autoHide = Boolean.valueOf(StringUtils.isNotEmpty(params.get("autoHide")) ? params.get("autoHide") : "false");
        Boolean autoCollapse = Boolean.valueOf(StringUtils.isNotEmpty(params.get("autoCollapse")) ? params.get("autoCollapse") : "false");
        String filter = StringUtils.isNotEmpty(params.get("filter")) ? params.get("filter") : "";
        filter = sanitizeValue(filter);        
        Boolean progress = Boolean.valueOf(StringUtils.isNotEmpty(params.get("progress")) ? params.get("progress") : "false");
        Boolean selectAll = Boolean.valueOf(StringUtils.isNotEmpty(params.get("selectAll")) ? params.get("selectAll") : "false");
        Boolean preserveTaskOrder = Boolean.valueOf(StringUtils.isNotEmpty(params.get("preserveTaskOrder")) ? params.get("preserveTaskOrder") : "false");
        String showWiki = StringUtils.isNotEmpty(params.get("showWiki")) ? params.get("showWiki") : "";
        Boolean includeStartEnd = Boolean.valueOf(StringUtils.isNotEmpty(params.get("includeStartEnd")) ? params.get("includeStartEnd") : "true");
        showWiki = sanitizeValue(showWiki);
        //Unique id for the div to render to
        String divId = "results2_" + JavaUtils.generateUniqueNumber();

        //Get the content (this is going to be the optional list of action tasks
        String content = params.getContent();
        content = sanitizeValue(content);
        String[] split = content.trim().replace("\r", "").split("\n");
        StringBuffer actionTaskParameters = new StringBuffer();
        for (String actionTask : split)
        {
            if (StringUtils.isNotBlank(actionTask))
            {
                if (actionTaskParameters.length() > 0) actionTaskParameters.append(",");
                /*
                 * We first escape the single quote and do HTML encoding after that because later part of the code escapes the back slash, '\' :)
                 */
                actionTask = actionTask.replace("'", "\\'");
                actionTaskParameters.append("'").append(ESAPI.encoder().encodeForHTMLAttribute(actionTask)).append("'");
            }
        }

        //Build up div and script to render
        StringBuffer str = new StringBuffer();
        str.append("<div id=\"%1$s\"></div>\n");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.Results2',\n");
        str.append("        title : '%2$s',\n");
        str.append("        order : '%6$s',\n");
        str.append("        refreshInterval : %3$s,\n");
        str.append("        refreshCountMax : %12$s,\n");
        str.append("        autoHide : %7$s,\n");
        str.append("        autoCollapse : %9$s,\n");
        str.append("        encodeSummary : %4$s,\n");
        str.append("        filter : '%8$s',\n");
        str.append("        progress : %11$s,\n");
        str.append("        selectAll : %13$s,\n");
        str.append("        includeStartEnd : %16$s,\n");
        str.append("        preserveTaskOrder : %14$s,\n");
        str.append("        showWiki : '%15$s',\n");
        str.append("        descriptionWidth : %10$s,\n");
        str.append("        actionTasks : [%5$s]\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('%1$s');\n");
        str.append("});\n");
        str.append("</script>\n");

        String result = String.format(str.toString(), divId, title, refreshInterval, encodeSummary, actionTaskParameters, order, autoHide, filter, autoCollapse, descriptionWidth,
                                      progress, refreshCountMax, selectAll, preserveTaskOrder, showWiki, includeStartEnd);
        writer.write(result);
    }
}
