/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.rsview.util.ControllerUtil;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.rsradeox.macro.MenuMacro;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * utility class for MenuMacro
 * 
 * @author jeet.marwah
 *
 */
public class MenuMacroUtil
{
//    private WikiRequestContext wikiRequestContext = null;
    private String imageUrl = null;
    private Map<String, Object> attributes = null;
    
    public MenuMacroUtil(WikiRequestContext wikiRequestContext, Map<String, Object> attributes)
    {
//        this.wikiRequestContext = wikiRequestContext;
        this.attributes = attributes;
        
        imageUrl = "/resolve/service/wiki/download/" + wikiRequestContext.getCurrWikiDocumentAPI().getFullName().replace('.', '/') + "?attachFileName=";//resolve/service/wiki/download/Runbook/Test1?attachFileName=acc-tree.gif
    }
    
    public String getMenu(String content)
    {
        List<MenuGroup> groups = getMenuGroups(content, imageUrl);
        String javascript = getJavascriptForMenu(groups, attributes);
        
        return javascript;
    }
    
    //private apis
    private String getJavascriptForMenu(List<MenuGroup> groups,  Map<String, Object> attributes)
    {
        String divId = "__menu_" + JavaUtils.generateUniqueNumber();
        StringBuffer result = new StringBuffer("<div>");

//      result.append("{pre}").append("\n");
        result.append("<script type='text/javascript'>").append("\n");
        
        result.append(getJavascriptData(groups)).append("\n");
        result.append(getMenuFunction(divId, attributes)).append("\n");
        
        result.append("</script>").append("\n");
//      result.append("{pre}").append("\n");
        result.append("<div id='").append(divId).append("'></div></div>").append("\n");
        
        return result.toString();
    }
    
    private List<MenuGroup> getMenuGroups(String content, String imageUrl)
    {
        List<MenuGroup> groups = new ArrayList<MenuGroup>();
        if (StringUtils.isNotBlank(content))
        {
            String menuGroupPatter = "\\{group(.*?)\\{group\\}";
            Pattern p = Pattern.compile(menuGroupPatter, Pattern.DOTALL);
            Matcher matcher = p.matcher(content);
            while (matcher.find())
            {
                String str = matcher.group();
                List<MenuItem> items = getItems(str, imageUrl);

                MenuGroup group = new MenuGroup();
                group.title = getGroupTitle(str.substring(0, str.indexOf("}") + 1));
                group.items = items;
                groups.add(group);
            }
        }
        return groups;
    }
    
    /**
     * 
     * @param str --> {group:title=Grouptitle}
     * @return
     */
    private String getGroupTitle(String str)
    {
        String title = "";
        
        if(StringUtils.isNotEmpty(str) && str.indexOf(":") > 0)
        {
            try
            {
                title = str.substring(str.indexOf("title=") + "title=".length(), str.indexOf("}")).trim();
            }
            catch(Exception t)
            {
                title = "";
            }
        }

        return title;
    }
    

    private List<MenuItem> getItems(String group, String imageUrl)
    {
        List<MenuItem> items = new ArrayList<MenuItem>();
        
        String itemPattern = "\\{item(.*?)\\{item\\}";
        String content = group;

        Pattern p = Pattern.compile(itemPattern, Pattern.DOTALL);
        Matcher matcher = p.matcher(content);

        while (matcher.find())
        {
            /**
             * {item}
                title=sdfsed
                desc=sdfsd     
                    url=sdfsd
                image=ddd.jpg
                status=updated
                {item}
             */
            String itemBlock = matcher.group();
            String[] itemArr = itemBlock.split("[\\r\\n]+");//to get new lines with no empty lines in it
            String globalTarget = (String)this.attributes.get(MenuMacro.TARGET);
            
            MenuItem item = new MenuItem();
            item.target = globalTarget.equalsIgnoreCase("same") ? "_top" : "_blank";
            
            for(String str : itemArr)
            {
                str = str.trim();
                
                if(str.startsWith("title"))
                {
                    item.title = str.substring(str.indexOf("=")+1).trim();
                }
                else if(str.startsWith("desc"))
                {
                    item.desc = str.substring(str.indexOf("=")+1).trim();
                }
                else if(str.startsWith("url"))
                {
                    String urlValue = str.substring(str.indexOf("=")+1).trim();
                    String rsclientUrl = "/resolve/jsp/rsclient.jsp?title=" + ControllerUtil.encode(item.title) +"&url=" + ControllerUtil.encode(urlValue);

                    if(urlValue.startsWith("http:") || urlValue.startsWith("https:"))
                    {
                        rsclientUrl = urlValue;
                    }
//                    else if(urlValue.startsWith("rswiki.jsp"))
//                    {
//                        rsclientUrl = "/resolve/jsp/rsclient.jsp?title=" + ControllerUtil.encode(item.title) +"&url=" + ControllerUtil.encode(urlValue);
//                    }
//                    else
//                    {
//                        String url = AdminUtil.getModifyURL(urlValue);
//                        rsclientUrl = UIUtil.addTitleAndEncodeURL(item.title, url);
//                    }
                    
                    item.url = rsclientUrl;
                }
                else if(str.startsWith("image"))
                {
                    //resolve/service/wiki/download/Runbook/Test1?attachFileName=acc-tree.gif
                    item.image = imageUrl + str.substring(str.indexOf("=")+1).trim();
                }
                else if(str.startsWith("status"))
                {
                    item.status = str.substring(str.indexOf("=")+1).trim();
                }
                else if(str.startsWith("target"))
                {
                    String targetValue = str.substring(str.indexOf("=")+1).trim();
                    if(StringUtils.isNotEmpty(targetValue) && targetValue.trim().equalsIgnoreCase("same"))
                    {
                        item.target = "_top";
                    }
                    else
                    {
                        item.target = "_blank";
                    }
                }
                
            }//end of for loop
            
            items.add(item);
        }//end of while loop
        
        return items;
    }
   
    private String getJavascriptData(List<MenuGroup> groups)
    {
        StringBuffer result = new StringBuffer();
        
        result.append("RScatalog = [").append("\n");
        
        for(int currIndex = 0; currIndex < groups.size(); currIndex++)
        {
            MenuGroup group = groups.get(currIndex);
            
            result.append("{").append("\n");
            result.append("    title : '").append(HttpUtil.sanitizeValue(group.title)).append("',").append("\n");
            result.append("    items : [").append("\n");
            result.append(getMenuItemsData(group.items)).append("\n");
            result.append("    ]").append("\n");
            
            if(currIndex != groups.size() - 1)
            {
                result.append("},").append("\n");
            }
            else
            {
                result.append("}").append("\n");
            }
            
        }//end of for loop
        
        result.append("];").append("\n");
        return result.toString();
    }
    
    private String getMenuItemsData(List<MenuItem> items)
    {
        StringBuffer result = new StringBuffer();
        
        for(int currIndex = 0; currIndex < items.size(); currIndex++)
        {
            MenuItem item = items.get(currIndex);
            
            result.append("{").append("\n");
            result.append("    title : '").append(HttpUtil.sanitizeValue(item.title)).append("',").append("\n");
            result.append("    desc : '").append(HttpUtil.sanitizeValue(item.desc)).append("',").append("\n");
            result.append("    url : '").append(HttpUtil.sanitizeValue(item.url, HttpUtil.VALIDATOR_RESOLVETEXT, 4000, HttpUtil.URL_ENCODING)).append("',").append("\n");
            result.append("    image : '").append(HttpUtil.sanitizeValue(item.image)).append("',").append("\n");
            result.append("    target : '").append(HttpUtil.sanitizeValue(item.target)).append("',").append("\n");
            result.append("    status : '").append(HttpUtil.sanitizeValue(item.status)).append("'").append("\n");
            
            if(currIndex != items.size() - 1)
            {
                result.append("},").append("\n");
            }
            else
            {
                result.append("}").append("\n");
            }

        }//end of for loop
        
        return result.toString();
    }

    private String getMenuFunction(String divId,  Map<String, Object> attributes)
    {
        StringBuffer result = new StringBuffer();
        result.append("Ext.onReady(function() {").append("\n");
        result.append("    var panel = Ext.create('RS.wiki.menu.MenuPanel', {").append("\n");
        result.append("        catalog : RScatalog,").append("\n");
        
        //toc and type
        if(attributes.get(MenuMacro.TOC) != null)
        {
            boolean toc = (Boolean) attributes.get(MenuMacro.TOC);
            result.append("        tableOfContents: {").append("\n");           
            result.append("            display: ").append(toc);
            
            if(attributes.get(MenuMacro.TYPE) != null)
            {
                result.append(",").append("\n");
                result.append("       type: '").append(HttpUtil.sanitizeValue((String)attributes.get(MenuMacro.TYPE))).append("'").append("\n");
            }
            else
            {
                result.append("\n");
            }
            
            if(attributes.get(MenuMacro.POSITION) != null)
            {
                result.append(",").append("\n");
                result.append("       position: '").append(HttpUtil.sanitizeValue((String)attributes.get(MenuMacro.POSITION))).append("'").append("\n");
            }
            
            
            if(attributes.get(MenuMacro.SPLIT) != null)
            {
                result.append(",").append("\n");
                result.append("       split: ").append(HttpUtil.sanitizeValue(attributes.get(MenuMacro.SPLIT).toString())).append("\n");
            }

            result.append("        },").append("\n");
        }
        else if(attributes.get(MenuMacro.TYPE) != null)
        {
            result.append("        tableOfContents: {").append("\n");     
            result.append("             type: '").append(HttpUtil.sanitizeValue((String)attributes.get(MenuMacro.TYPE))).append("'").append("\n");
            result.append("        },").append("\n");
        }
        
        //height and width
        if(attributes.get(MenuMacro.HEIGHT) != null || attributes.get(MenuMacro.WIDTH) != null)
        {
            result.append("        autoSize : false,").append("\n");
            if(attributes.get(MenuMacro.HEIGHT) != null)
            {
                result.append("        height : ").append(HttpUtil.sanitizeValue(attributes.get(MenuMacro.HEIGHT).toString())).append(",").append("\n");
            }
            if(attributes.get(MenuMacro.WIDTH) != null)
            {
                result.append("        width : ").append(HttpUtil.sanitizeValue(attributes.get(MenuMacro.WIDTH).toString())).append(",").append("\n");
            }
        }
        
        result.append("        renderTo : '").append(divId).append("'").append("\n");
        result.append("    });").append("\n");
        result.append("});").append("\n");
        
        return result.toString();
    }
       

}

class MenuGroup
{
    public String title = "";
    public List<MenuItem> items;
}

class MenuItem
{
    public String title = "";
    public String desc = "";
    public String url = "";
    public String image = "";
    public String status = "";
    public String target = "_blank";

}
