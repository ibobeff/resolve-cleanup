/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.util.HttpUtil;
import com.resolve.wiki.radeox.macro.api.ApiDoc;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

import java.io.IOException;
import java.io.Writer;

/*
 * Macro that replaces {api} with external URLS to api documentation
 */

public class ApiMacro extends BaseLocaleMacro
{
    private String[] paramDescription = { "1: class name, e.g. java.lang.Object or java.lang.Object@Java131", "?2: mode, e.g. Java12, Ruby, defaults to Java" };

    public String[] getParamDescription()
    {
        return paramDescription;
    }

    public String getLocaleKey()
    {
        return "macro.api";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        String mode;
        String klass;

        if (params.getLength() == 1)
        {
            klass = params.get("0");
            int index = klass.indexOf("@");
            if (index > 0)
            {
                mode = klass.substring(index + 1);
                klass = klass.substring(0, index);
            }
            else
            {
                mode = "java";
            }
        }
        else if (params.getLength() == 2)
        {
            mode = params.get("1").toLowerCase();
            klass = params.get("0");
        }
        else
        {
            throw new IllegalArgumentException("api macro needs one or two paramaters");
        }

        mode = HttpUtil.sanitizeValue(mode);
        klass = HttpUtil.sanitizeValue(klass);
        ApiDoc.getInstance().expand(writer, klass, mode);
        return;
    }
}
