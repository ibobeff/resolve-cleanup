/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.regex;

/*
 * The result when a Matcher object finds matches in some input
 * Implementation for regex package in JDK 1.4
 *
 */

public class JdkMatchResult extends MatchResult {
  private java.util.regex.Matcher matcher;

  public JdkMatchResult(java.util.regex.Matcher matcher) {
    this.matcher = matcher;
  }

  public JdkMatchResult(Matcher matcher) {
    this.matcher = ((JdkMatcher) matcher).getMatcher();
  }

  public int groups() {
    return matcher.groupCount();
  }

  public String group(int i) {
    return matcher.group(i);
  }

  public int beginOffset(int i) {
    return matcher.start(i);
  }

  public int endOffset(int i) {
    return matcher.end(i);
  }
}