/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.table;

import java.util.List;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.macro.PluginRepository;
import com.resolve.wiki.radeox.macro.Repository;

/**
 * Repository for functions
 * 
 * @author jeet.marwah
 * 
 */
public class FunctionRepository extends PluginRepository
{
	protected static Repository instance;

	//spring injected
	private List<String> listOfFunctions;

	public synchronized static Repository getInstance()
	{
		if (null == instance)
		{
			instance = new FunctionRepository();
		}
		return instance;
	}

	//executed by spring 
	public void init()
	{
		ClassLoader cl = this.getClass().getClassLoader();
		for (String funcName : listOfFunctions)
		{
			try
			{
				Function func = (Function) cl.loadClass(funcName).newInstance();
				list.add(func);
				plugins.put(func.getName(), func);
			}
			catch (Exception e)
			{
				Log.log.error(e.getMessage(), e);
			}
		}
	}

	private FunctionRepository()
	{

	}

	public List<String> getListOfFunctions()
	{
		return listOfFunctions;
	}

	public void setListOfFunctions(List<String> listOfFunctions)
	{
		this.listOfFunctions = listOfFunctions;
	}
}
