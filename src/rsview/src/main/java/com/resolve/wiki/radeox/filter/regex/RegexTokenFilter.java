/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.regex;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.regex.MatchResult;
import com.resolve.wiki.radeox.regex.Matcher;
import com.resolve.wiki.radeox.regex.Pattern;
import com.resolve.wiki.radeox.regex.Substitution;

/*
 * Filter that calls a special handler method handleMatch() for every occurance
 * of a regular expression.
 * 
 */

public abstract class RegexTokenFilter extends RegexFilter
{
    public RegexTokenFilter()
    {
        super();
    }

    /**
     * create a new regular expression and set
     */
    public RegexTokenFilter(String regex, boolean multiline)
    {
        super(regex, "", multiline);
    }

    /**
     * create a new regular expression and set
     */
    public RegexTokenFilter(String regex)
    {
        super(regex, "");
    }

    protected void setUp(FilterContext context)
    {
    }

    /**
     * Method is called for every occurance of a regular expression. Subclasses
     * have to implement this mehtod.
     * 
     * @param buffer
     *            Buffer to write replacement string to
     * @param result
     *            Hit with the found regualr expression
     * @param context
     *            FilterContext for filters
     */
    public abstract void handleMatch(StringBuffer buffer, MatchResult result, FilterContext context);

    public String filter(String input, final FilterContext context)
    {
        setUp(context);

        String result = null;
        int size = pattern.size();
        for (int i = 0; i < size; i++)
        {
            Pattern p = (Pattern) pattern.get(i);
            try
            {
                Matcher m = Matcher.create(input, p);
                result = m.substitute(new Substitution()
                {
                    public void handleMatch(StringBuffer buffer, MatchResult result)
                    {
                        RegexTokenFilter.this.handleMatch(buffer, result, context);
                    }
                });

                // result = Util.substitute(matcher, p, new
                // ActionSubstitution(s, this, context), result, limit);
            }
            catch (Exception e)
            {
                Log.log.warn("<span class=\"error\">Exception</span>: " + this, e);
            }
            catch (Error err)
            {
                Log.log.warn("<span class=\"error\">Error</span>: " + this + ": " + err);
            }
            input = result;
        }
        return input;
    }
}
