/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.api.engine;

import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;

/**
 * Interface for RenderEngines that know about Wiki pages.
 *
 */

public interface WikiRenderEngine {
  /**
   * Test for the existence of a wiki page
   *
   * @param name Name of an Wiki Page
   * @return result True if wiki page exists
   */

  public boolean exists(InitialRenderContext context, String name);

  public boolean showCreate();

  public void appendLink(InitialRenderContext context, StringBuffer buffer, String name, String view, String anchor, String target);
  public void appendLink(Wiki wiki, StringBuffer buffer, String name, String defaultNamespace, String view, String anchor, String target);

  public void appendLink(InitialRenderContext context, StringBuffer buffer, String name, String view, String target);

  public void appendCreateLink(InitialRenderContext context, StringBuffer buffer, String name, String view);
  public void appendCreateLink(InitialRenderContext context, StringBuffer buffer, String name, String view,String target);
  public void appendCreateLink(Wiki wiki, StringBuffer buffer, String name, String defaultNamespace, String view);
}
