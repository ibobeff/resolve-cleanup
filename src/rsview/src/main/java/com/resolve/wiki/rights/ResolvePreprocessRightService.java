/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rights;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.services.exception.WikiException;

/**
 * Action task right service interface
 * 
 * @author jeet.marwah
 *
 */
public interface ResolvePreprocessRightService
{
    public AccessRights getAccessRightsFor(String username, ResolvePreprocess resolvePreprocess, ResolveActionTask resolveActionTask) throws WikiException;

}//ActionTaskRightService
