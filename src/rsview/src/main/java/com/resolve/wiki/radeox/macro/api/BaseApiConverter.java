/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.api;

import java.io.IOException;
import java.io.Writer;

/**
 * Base class for API converters, stores a base URL
 *
 */

public abstract class BaseApiConverter implements ApiConverter {
  protected String baseUrl;

  public abstract void appendUrl(Writer writer, String className) throws IOException ;

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public String getBaseUrl() {
    return this.baseUrl;
  }

  public abstract String getName();
}
