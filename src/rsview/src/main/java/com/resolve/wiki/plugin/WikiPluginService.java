/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.plugin;

/**
 * This is a singleton class that represents a plugin engine. The purpose of this engine is a single entry point to all the plugins in the application.
 * 
 * @author jeet.marwah
 * 
 */
public class WikiPluginService
{

	private static WikiPluginService instance;

	//only 1 instance of this should be available
	private WikiPluginService()
	{
	}

	public static WikiPluginService getInstance()
	{
		if (instance == null)
		{
			synchronized (WikiPluginService.class)
			{
				if (instance == null)
				{
					instance = new WikiPluginService();
				}
			}
		}

		return instance;
	}
	
	
	
	

}//end of class
