/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;

import com.resolve.util.Log;
import com.resolve.wiki.render.substitution.PreTagSubstitution;
import com.resolve.wiki.rsradeox.RadeoxProcessor;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This is a singleton class.
 * 
 * this is the main converter class that will apply the conversion from html to wiki and vice versa
 * when the user is editing a wiki document
 * 
 * Steps to add new converter are:
 * 1) create a css in tinymce_all.css file
 * 2) create a converter similar to JavascriptConverter.java or GroovyConverter.java, whichever is applicable
 * 3) modify the wiki-context.xml file with the mapping of css name and java file
 * 
 * @author jeet.marwah
 *
 */
public class WikiConverter
{
	//spring injected - map of CSS name and it's mapping converter
	private Map<String, String> cssConverterMap = null;//css name mapped with its converter java class
	private RadeoxProcessor radeoxProcessor = null;

	//this if for the {pre} tag
	private PreTagSubstitution preTagSubstitution = new PreTagSubstitution();
	
	private static WikiConverter instance = null;
	
	public static final String BR_PLACE_HOLDER = "RS_BR_RS";
	
	private WikiConverter(){}
	
	public static WikiConverter getInstance()
	{
		if (instance == null)
		{
			synchronized (WikiConverter.class)
			{
				if (instance == null)
				{
					instance = new WikiConverter();
				}
			}
		}
		return instance;
	}
	
	/**
	 * THis should be called before saving the wiki content in the database
	 * 
	 *steps to take from HTML2WIKI
	//1) get list of <span> tags
	//2) replace the tag with wiki tag
	//3) remove all the html tags from the string - THIS IS DONE IN CONVERTER
	//4) find and replace the original with the new strings
	 * 
	 * @param content
	 * @return
	 */
	public String applyHtml2WikiConverter(String content)
	{
		String contentStr = new String(content); 
		
		Iterator<String> it = cssConverterMap.keySet().iterator();
		String javaClassConverter = null;
		
		contentStr = contentStr.replaceAll("<br>", BR_PLACE_HOLDER);
		contentStr = contentStr.replaceAll("<br/>", BR_PLACE_HOLDER);
		contentStr = contentStr.replaceAll("<br />", BR_PLACE_HOLDER);
		
		while(it.hasNext())
		{
			javaClassConverter = cssConverterMap.get(it.next());
			
			try
			{
				//create the converter object
				Converter converter = (Converter)Class.forName(javaClassConverter.trim()).newInstance();

				//apply the html 
				contentStr = converter.html2wiki(contentStr);
			}
			catch(Exception e)
			{
				//if there is an error, then continue to the next one. Logging error for info
				Log.log.debug("Error in html2wiki class - " + javaClassConverter, e);
			}
		}

		contentStr = contentStr.replaceAll(BR_PLACE_HOLDER, "<br>");

		//convert all the escape characters to their original values 
		contentStr = StringEscapeUtils.unescapeHtml(contentStr);

		return contentStr;
	}//applyHtml2WikiConverter
	
	/**
	 * This api adds the html tag that is defined for each wiki tag
	 * this is called before rendering in EDIT mode to the user.
	 * 
	 * @param content
	 * @return
	 */
	public String applyWiki2HtmlConverter(String content, WikiRequestContext wikiRequestContext)
	{
		String contentStr = new String(content);

		//apply wiki filters
		contentStr = applyWikiFilters(content, wikiRequestContext);

		Iterator<String> it = cssConverterMap.keySet().iterator();
		String javaClassConverter = null;
		
		while(it.hasNext())
		{
			javaClassConverter = cssConverterMap.get(it.next());
			
			try
			{
				//create the converter object
				Converter converter = (Converter)Class.forName(javaClassConverter.trim()).newInstance();

				//apply the html 
				contentStr = converter.wiki2html(contentStr);
			}
			catch(Exception e)
			{
				//if there is an error, then continue to the next one. Logging error for info
				Log.log.debug("Error in wiki2html class - " + javaClassConverter, e);
			}
		}
		
//		contentStr = preTagSubstitution.replaceNonWikiTextWithPreTagSyntax(contentStr);
		
		return contentStr;
	}//applyWiki2HtmlConverter
	
	private String applyWikiFilters(String content, WikiRequestContext wikiRequestContext)
	{
		String contentStr = new String(content);
		
		//do not process the {pre} tag. So substitute with some non -wiki tag
		contentStr = preTagSubstitution.replacePreTagWithNonWikiText(contentStr);
		
		contentStr = radeoxProcessor.renderRadeox(contentStr, null, wikiRequestContext, "wiki2htmlConverterEngine");
		
		//set the values back for the {pre} tag
//		contentStr = preTagSubstitution.replaceNonWikiTextWithPreTag(contentStr);
		contentStr = preTagSubstitution.replaceNonWikiTextWithPreTagSyntax(contentStr);
		
		
		return contentStr;
	}
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	//////  GETTER and SETTERS
	//////////////////////////////////////////////////////////////////////////////////////////////////
	public Map<String, String> getCssConverterMap()
	{
		return cssConverterMap;
	}

	public void setCssConverterMap(Map<String, String> cssConverterMap)
	{
		this.cssConverterMap = cssConverterMap;
	}

	public RadeoxProcessor getRadeoxProcessor()
	{
		return radeoxProcessor;
	}

	public void setRadeoxProcessor(RadeoxProcessor radeoxProcessor)
	{
		this.radeoxProcessor = radeoxProcessor;
	}


}
