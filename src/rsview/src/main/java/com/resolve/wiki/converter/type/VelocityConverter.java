/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter.type;

import java.util.List;

import com.resolve.services.util.ParseUtil;
import com.resolve.wiki.converter.AbstractConverter;
import com.resolve.wiki.converter.WikiConverter;


/**
 * 
 * 
 * @author jeet.marwah
 *
 */
public class VelocityConverter extends AbstractConverter
{

	private String CSS_NAME = "";
	private String HTML_START_TAG = "";
	private String HTML_END_TAG = "";
	private String WIKI_START_TAG = "";
	private String WIKI_END_TAG = "";

	@Override
	public String wiki2html(String content)
	{
		String contentStr = new String(content);
		StringBuffer sbNew = new StringBuffer();
		
		String[] lines = contentStr.split("\\n");
		for(String line : lines)
		{
		    //this is velocity
			if(line.startsWith("#"))
			{
				sbNew.append("<p>").append(line).append("</p>");
			}
			else
			{
			    sbNew.append(line);
			}
            sbNew.append("\n");
		}
		
		return  sbNew.toString();
	}
	
	@Override
    public String html2wiki(String content)
    {
        String contentStr = new String(content);
        StringBuffer sbNew = new StringBuffer();

        String[] lines = contentStr.split("\\n");
        String newLine = null;
        for(String line : lines)
        {
            newLine = removeHTML(line);
            if(newLine.startsWith("#"))
            {
                //this is velocity code
                sbNew.append(newLine);
            }
            else
            {
                //don't manipulate that line
                sbNew.append(line);
            }
            sbNew.append("\n");
        }
        
        return sbNew.toString();
        
    }
	
//	@Override
	public String html2wiki_old(String content)
	{
		String contentStr = new String(content);
		
		//check all the <p> lines for velocity syntax
		List<String> pStrings = ParseUtil.getHtml_P_Tag(contentStr);
		pStrings.addAll(ParseUtil.getHtml_br_Tag(contentStr));
		
		for(String pTag : pStrings)
		{
			String pOriginalTag = new String(pTag);

			if(pTag.startsWith("#") || pTag.indexOf(">#") > -1 || pTag.contains(">#"))
			{
//				//<p>#set ($myvar="test")
//				if(pOriginalTag.indexOf(WikiConverter.BR_PLACE_HOLDER) > -1)
//				{
//					pOriginalTag = pOriginalTag.substring(0, pTag.indexOf(WikiConverter.BR_PLACE_HOLDER));
//					pTag = pOriginalTag;
//				}
				
				pTag = pTag.replaceAll("<br>", "\n");
				pTag = pTag.replaceAll(WikiConverter.BR_PLACE_HOLDER, "\n");
//				pTag = pTag.replaceAll("</p>", "\n");
				
				//<p>#set ($myvar="test") ==> #set ($myvar="test")	
				pTag = pTag.substring(pTag.indexOf("#"));
				
				//#set ($myvar="test")
				pTag = removeHTML(pTag);
				
//				if(!(pTag.endsWith("\n") || pTag.endsWith("\r")))
//				{
//					pTag += "\n";
//				}

				//replace only if its a velocity code
				contentStr = ParseUtil.replaceString(pOriginalTag, pTag, contentStr);
			}
			//eg. <p>
			//	  The condition code will be in the Alert Group of Netcool's Event list.RS_BR_RS##Condition Code: [$!condition]RS_BR_RSRS_BR_RS</p>
			else if(pTag.contains("##") || pTag.indexOf("##") > -1)
			{
				pTag = pTag.replaceAll("<br>", "\n");
				pTag = pTag.replaceAll(WikiConverter.BR_PLACE_HOLDER, "\n");
				
				//replace only if its a velocity code
				contentStr = ParseUtil.replaceString(pOriginalTag, pTag, contentStr);
			}
				
			
			
		}

		return contentStr;
	}
	
	@Override
	public String getCSS_NAME()
	{
		return CSS_NAME;
	}

	@Override
	public String getHTML_START_TAG()
	{
		return HTML_START_TAG;
	}

	@Override
	public String getHTML_END_TAG()
	{
		return HTML_END_TAG;
	}

	@Override
	public String getWIKI_END_TAG()
	{
		return WIKI_END_TAG;
	}

	@Override
	public String getWIKI_START_TAG()
	{
		return WIKI_START_TAG;
	}



}//JavascriptConverter
