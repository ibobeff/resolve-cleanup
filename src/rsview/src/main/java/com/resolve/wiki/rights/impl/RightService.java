/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rights.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.resolve.dto.RightType;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.Properties;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.vo.WikiUser;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.StringUtils;
import com.resolve.wiki.store.helper.StoreUtility;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * 
 * @author jeet.marwah
 *
 */
public abstract class RightService
{
  //maps the action with the Rights
    protected static Map<WikiAction, RightType> actionRightMap = null;

    static
    {
        //for each ACTION, there should be matching RIGHT
        actionRightMap = new HashMap<WikiAction, RightType>();
        actionRightMap.put(WikiAction.toolbar, RightType.view);
        actionRightMap.put(WikiAction.listdoc, RightType.view);
        actionRightMap.put(WikiAction.listns, RightType.view);
        actionRightMap.put(WikiAction.listrelated, RightType.view);
        actionRightMap.put(WikiAction.listrecent, RightType.view);
        actionRightMap.put(WikiAction.view, RightType.view);
        actionRightMap.put(WikiAction.viewsrc, RightType.view);
        actionRightMap.put(WikiAction.viewtoc, RightType.view);
        actionRightMap.put(WikiAction.viewattach, RightType.view);
        actionRightMap.put(WikiAction.attach, RightType.view);
        actionRightMap.put(WikiAction.deleteattach, RightType.view);
        actionRightMap.put(WikiAction.info, RightType.view);
        actionRightMap.put(WikiAction.edit, RightType.edit);
        actionRightMap.put(WikiAction.save, RightType.edit);
        actionRightMap.put(WikiAction.savemodelc, RightType.edit);
        actionRightMap.put(WikiAction.savedtreec, RightType.edit);
        actionRightMap.put(WikiAction.saveandexit, RightType.edit);
        actionRightMap.put(WikiAction.cancel, RightType.edit);
        actionRightMap.put(WikiAction.preview, RightType.view);
        actionRightMap.put(WikiAction.execute, RightType.execute);
        actionRightMap.put(WikiAction.editmodel, RightType.edit);
        actionRightMap.put(WikiAction.editabort, RightType.edit);
        actionRightMap.put(WikiAction.editfinal, RightType.edit);
        actionRightMap.put(WikiAction.editdtree, RightType.edit);
        actionRightMap.put(WikiAction.editmodelc, RightType.edit);
        actionRightMap.put(WikiAction.editexcpc, RightType.edit);
        actionRightMap.put(WikiAction.editfinalc, RightType.edit);
        actionRightMap.put(WikiAction.viewmodelc, RightType.view);
        actionRightMap.put(WikiAction.viewexcpc, RightType.view);
        actionRightMap.put(WikiAction.viewfinalc, RightType.view);
        actionRightMap.put(WikiAction.lock, RightType.edit);
        actionRightMap.put(WikiAction.unlock, RightType.edit);
        actionRightMap.put(WikiAction.delete, RightType.edit);
        actionRightMap.put(WikiAction.move, RightType.edit);
        actionRightMap.put(WikiAction.copy, RightType.edit);
        actionRightMap.put(WikiAction.viewmonitor, RightType.view);
        actionRightMap.put(WikiAction.setmonitor, RightType.edit);
        actionRightMap.put(WikiAction.rmmonitor, RightType.edit);
        actionRightMap.put(WikiAction.revision, RightType.view);
        actionRightMap.put(WikiAction.rollback, RightType.admin);
        actionRightMap.put(WikiAction.reset, RightType.admin);
        actionRightMap.put(WikiAction.diffrev, RightType.view);
        actionRightMap.put(WikiAction.download, RightType.view);
        actionRightMap.put(WikiAction.downloadrev, RightType.view);
        actionRightMap.put(WikiAction.qsearch, RightType.view);
        actionRightMap.put(WikiAction.asearch, RightType.view);
        actionRightMap.put(WikiAction.viewhelp, RightType.view);
        actionRightMap.put(WikiAction.viewwiki, RightType.view);
        actionRightMap.put(WikiAction.impex, RightType.admin);
    
    }
    
    protected RightType forAction(WikiAction wikiAction)
    {
        return actionRightMap.get(wikiAction);
    }
    
    /**
     * adds the user roles in the context and also checks if it is super user
     * 
     * @param context
     * @return
     */
    protected boolean checkSuperUserAccess(String user, RightType rightType)
    {
        boolean hasAccess = false;

        //if the user is 'admin' and it is NOT EDIT, then return true
        if (user.equalsIgnoreCase(ConstantValues.WIKI_ADMIN))
        {
            hasAccess = true;
        }
        
        //if the user is 'resolve.maint', then return true - it has all the access
        if (user.equalsIgnoreCase(ConstantValues.WIKI_RESOLVE_MAINT))
        {
            hasAccess = true;
        }

        return hasAccess;
    }
    
    protected void populateUserRoles(WikiRequestContext context)
    {
        String user = "";
        WikiUser wikiUser = null;
        
        if(context.getWikiUser() != null)
        {
            wikiUser = context.getWikiUser();
            user = wikiUser.getUsername();
        }
        else
        {
            user = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            wikiUser = ServiceHibernate.getUserRolesAndGroups(user); //context.getWiki().getWikiStore().getUserRoles(user);
            context.setWikiUser(wikiUser);
        }
    }

    protected AccessRights getAccessRightsFor(String namespace, String fullName, String id, boolean isDefaultRole, String resourceType, 
                    String readRoles, String writeRoles, String adminRoles, String executeRoles, List<PropertiesVO> lProps, WikiUser wikiUser)
    {
//        Set<String> userRoles = wikiUser.getRoles();
//        String userRolesStr = StringUtils.convertCollectionToString(userRoles.iterator(), ",");
//        
//        readRoles = StringUtils.isNotEmpty(readRoles) ? readRoles + "," + userRolesStr : userRolesStr;
//        writeRoles = StringUtils.isNotEmpty(writeRoles) ? writeRoles + "," + userRolesStr : userRolesStr;
//        adminRoles = StringUtils.isNotEmpty(adminRoles) ? adminRoles + "," + userRolesStr : userRolesStr;
//        executeRoles = StringUtils.isNotEmpty(executeRoles) ? executeRoles + "," + userRolesStr : userRolesStr;
        
        return getAccessRightsFor(namespace, fullName, id, isDefaultRole, resourceType, readRoles, writeRoles, adminRoles, executeRoles, lProps);
        
    }

        
    protected AccessRights getAccessRightsFor(String namespace, String fullName, String id, boolean isDefaultRole, String resourceType, String readRoles, String writeRoles, String adminRoles, String executeRoles, List<PropertiesVO> lProps)
    {
        Set<String> wikiDocReadAccess = new TreeSet<String>(StringUtils.convertStringToList(readRoles, ","));
        Set<String> wikiDocWriteAccess = new TreeSet<String>(StringUtils.convertStringToList(writeRoles, ","));
        Set<String> wikiDocAdminAccess = new TreeSet<String>(StringUtils.convertStringToList(adminRoles, ","));
        Set<String> wikiDocExecuteAccess = new TreeSet<String>(StringUtils.convertStringToList(executeRoles, ","));

        AccessRights ar = new AccessRights();
        List<AccessRights> accessRight = StoreUtility.getAccessRightsFor(id, fullName, resourceType);
        if(accessRight!=null && accessRight.size() > 0)
        {
            ar = accessRight.get(0);
        }
//        wikiDocReadAccess.addAll(StringUtils.convertStringToList(ar.getUReadAccess(), ","));
//        wikiDocWriteAccess.addAll(StringUtils.convertStringToList(ar.getUWriteAccess(), ","));
//        wikiDocAdminAccess.addAll(StringUtils.convertStringToList(ar.getUAdminAccess(), ","));
//        wikiDocExecuteAccess.addAll(StringUtils.convertStringToList(ar.getUExecuteAccess(), ","));

        Set<String> readAccess = new TreeSet<String>();
        readAccess.add("admin");
        if(isDefaultRole)
        {
            readAccess.addAll(StoreUtility.getRolesFor(RightType.view, lProps, namespace));
        }
        else
        {
            readAccess.addAll(wikiDocReadAccess);
        }
        
        Set<String> writeAccess = new TreeSet<String>();
        writeAccess.add("admin");
        if(isDefaultRole)
        {
            writeAccess.addAll(StoreUtility.getRolesFor(RightType.edit, lProps, namespace));
        }
        else
        {
            writeAccess.addAll(wikiDocWriteAccess);
        }
        
        Set<String> adminAccess = new TreeSet<String>();
        adminAccess.add("admin");
        if(isDefaultRole)
        {
            adminAccess.addAll(StoreUtility.getRolesFor(RightType.admin, lProps, namespace));
        }
        else
        {
            adminAccess.addAll(wikiDocAdminAccess);
        }

        Set<String> exeAccess = new TreeSet<String>();
        exeAccess.add("admin");
        if(isDefaultRole)
        {
            exeAccess.addAll(StoreUtility.getRolesFor(RightType.execute, lProps, namespace));
        }
        else
        {
            exeAccess.addAll(wikiDocExecuteAccess);
        }
        
        
        ar.setUResourceType(resourceType);
        ar.setUResourceName(fullName);
        ar.setUResourceId(id);
        ar.setSysCreatedOn(GMTDate.getDate());
        ar.setSysUpdatedOn(GMTDate.getDate());
        ar.setUReadAccess(StringUtils.convertCollectionToString(readAccess.iterator(), ","));
        ar.setUWriteAccess(StringUtils.convertCollectionToString(writeAccess.iterator(), ","));
        ar.setUAdminAccess(StringUtils.convertCollectionToString(adminAccess.iterator(), ","));
        ar.setUExecuteAccess(StringUtils.convertCollectionToString(exeAccess.iterator(), ","));

        return ar;
    }
    
}//RightService
