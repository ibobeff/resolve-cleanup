/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter.type;

import com.resolve.wiki.converter.AbstractConverter;


/**
 * This class is mapped to the <span> tag that renders groovy code
 * 
 * @author jeet.marwah
 *
 */
public class CodeConverter extends AbstractConverter
{

	private String CSS_NAME = "code";
	private String HTML_START_TAG = "<span class=\"" + CSS_NAME + "\">";
	private String HTML_END_TAG = "</span>";
	private String WIKI_START_TAG = "{code}";
	private String WIKI_END_TAG = "{code}";

	private String pattern = "\\{code(.*?)\\{code\\}";
	
	@Override
	public String getHTML2WikiPattern()
	{
		return pattern;
	}
	
	@Override
	public String getWiki2HTMLPattern()
	{
		return pattern;
	}
	
	
	@Override
	public String getCSS_NAME()
	{
		return CSS_NAME;
	}

	@Override
	public String getHTML_START_TAG()
	{
		return HTML_START_TAG;
	}

	@Override
	public String getHTML_END_TAG()
	{
		return HTML_END_TAG;
	}

	@Override
	public String getWIKI_END_TAG()
	{
		return WIKI_END_TAG;
	}

	@Override
	public String getWIKI_START_TAG()
	{
		return WIKI_START_TAG;
	}



}//JavascriptConverter
