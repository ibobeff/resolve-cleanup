/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;

/*
 * Abstract Filter Class that supplies the
 * Filter interface. Concrete Filters should
 * inherit from Filter. Filters transform a
 * String (usually snip content) to another String
 * (usually HTML).
 *
 */

public abstract class FilterSupport implements Filter {
  protected InitialRenderContext initialContext;

  public FilterSupport() {
  }

  public String[] replaces() {
    return FilterPipe.NO_REPLACES;
  }

  public String[] before() {
    return FilterPipe.EMPTY_BEFORE;
  }

  public void setInitialContext(InitialRenderContext context) {
     this.initialContext = context;
  }

  public String getDescription() {
    return "";
  }
}