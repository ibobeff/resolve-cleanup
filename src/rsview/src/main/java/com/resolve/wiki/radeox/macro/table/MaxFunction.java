/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.table;

import com.resolve.util.Log;

/**
 * A function that finds the max of table cells
 * 
 */

public class MaxFunction implements Function
{
    public String getName()
    {
        return "MAX";
    }

    public void execute(Table table, int posx, int posy, int startX, int startY, int endX, int endY)
    {
        float max = 0;
        boolean floating = false;
        for (int x = startX; x <= endX; x++)
        {
            for (int y = startY; y <= endY; y++)
            {
                // Logger.debug("x="+x+" y="+y+" >"+getXY(x,y));
                float value = 0;
                try
                {
                    value += Integer.parseInt((String) table.getXY(x, y));
                }
                catch (Exception e)
                {
                    try
                    {
                        value += Float.parseFloat((String) table.getXY(x, y));
                        floating = true;
                    }
                    catch (NumberFormatException e1)
                    {
                        Log.log.debug("MaxFunction: unable to parse " + table.getXY(x, y));
                    }
                }
                if (max < value)
                {
                    max = value;
                }
            }
        }
        // Logger.debug("Sum="+sum);
        if (floating)
        {
            table.setXY(posx, posy, "" + max);
        }
        else
        {
            table.setXY(posx, posy, "" + (int) max);
        }
    }

}
