/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.filter;

import com.resolve.wiki.radeox.filter.regex.LocaleRegexReplaceFilter;

public class StrongFilter extends LocaleRegexReplaceFilter
{
	public static final String LOCAL_KEY = "filter.strong";
	
	@Override
	protected void setValues()
	{
		//addKeyValue(LOCAL_KEY + ".match", "(^|>|[\\p{Punct}\\p{Space}]+)\\*(.*?)\\*([\\p{Punct}\\p{Space}]+|<|$)");
	    // Takes care of HTML column width syntax with * with none to 3 digits i.e. <col width="*"> to <col width="999*">
        addKeyValue(LOCAL_KEY + ".match", "(^|>|[\\p{Punct}\\p{Space}]+)(?<!width=\"\\d{0,3})\\*(?!\">)(.*?)(?<!width=\"\\d{0,3})\\*(?!\">)([\\p{Punct}\\p{Space}]+|<|$)");
		addKeyValue(LOCAL_KEY + ".print", "$1<strong class=\"strong\">$2</strong>$3");
	}

	@Override
	protected String getLocaleKey()
	{
		 return LOCAL_KEY;
	}

}
