/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.services.util.ParseUtil;

/**
 * This class is mapped to the <span> tag 
 * 
 * @author jeet.marwah
 *
 */
public abstract class AbstractConverter implements Converter
{

	public abstract String getCSS_NAME();
	public abstract String getHTML_START_TAG();
	public abstract String getHTML_END_TAG();
	public abstract String getWIKI_START_TAG();
	public abstract String getWIKI_END_TAG();
	
	
	/**
	 * This to be overriden to remove the html between tags
	 * eg. {javascript} {/javascript} tag can have html which we would like to filter out'
	 * 
	 * @return
	 */
	public String getHTML2WikiPattern()
	{
		return null;
	}
	
	public String getWiki2HTMLPattern()
	{
		return null;
	}
	
	/**
	 * This can be overridden in case there any other special tags to filter from the <p> tag for a converter.
	 * For eg. Groovy converter
	 * 
	 * @return
	 */
	protected List<String> filterFor_P_Tag()
	{
		List<String> list = new ArrayList<String>();
		list.add(getWIKI_START_TAG());
		list.add(getWIKI_END_TAG());
		
		return list;
	}
	
	@Override
	public String html2wiki(String content)
	{
		String contentStr = new String(content);
		
		//<span> processing
		contentStr = removeWikiSpanTags(contentStr);

		//<p> processing
		contentStr = remove_P_tagForCSS(contentStr);
		
		//filter based on patter if set {code}....{code}
		if(getHTML2WikiPattern() != null)
		{
			contentStr = processTextBetweenPattern(contentStr);
		}
		
		return contentStr;
	}

	@Override
	public String wiki2html(String content)
	{
		String contentStr = new String(content);
		
		if(getWiki2HTMLPattern() != null)
		{
			Pattern p = Pattern.compile(getWiki2HTMLPattern(), Pattern.DOTALL);
			Matcher matcher = p.matcher(contentStr);
	
			
			while (matcher.find())
			{
				String originalStr = matcher.group();
				
				String htmlStr = getHTML_START_TAG() + originalStr + getHTML_END_TAG();
				htmlStr = htmlStr.replaceAll("\r\n", "<br>");
				htmlStr = htmlStr.replaceAll("\n\r", "<br>");
				htmlStr = htmlStr.replaceAll("\n", "<br>");
				htmlStr = htmlStr.replaceAll("\r", "<br>");
				
				contentStr = ParseUtil.replaceString(originalStr, htmlStr, contentStr);
				
			}
		}

		return contentStr;
	}
	
	/**
	 * convert the <span class="code">{code}</span> ==> {code}
	 * 
	 * @param content
	 * @return
	 */
	protected String removeWikiSpanTags(String content)
	{
		String contentStr = new String(content);
		
		//1) get list of <span> tags 
		//eg <span class="wikijavascript">{javascript}function....{/javascript}</span>
		List<String> spanStrings = ParseUtil.getHtmlSpanTag(contentStr);
		
		//for each
		//2) replace the tag with wiki tag
		for(String span : spanStrings)
		{
			if(span.indexOf(getCSS_NAME()) > -1)
			{
				String originalSpan = new String(span); 

				//get the content within the tag
				span =  ParseUtil.getContentBetweenTags(span);
				
				//replace it
				contentStr = ParseUtil.replaceString(originalSpan, span, contentStr);
				
			}//if clause
			
		}//for loop
		
		return contentStr;
	}//replaceWikiTags

	
	/**
	 * <p> </p> ==> <br>
	 * <p>{code}</p> ==> {code}\n
	 * 
	 * @param content
	 * @return
	 */
	protected String remove_P_tagForCSS(String content)
	{
		String contentStr = new String(content);
		
		List<String> pStrings = ParseUtil.getHtml_P_Tag(contentStr);
		for(String pTag : pStrings)
		{
			String pOriginalTag = new String(pTag); 
			
			String inBetweenText = ParseUtil.getContentBetweenTags(pTag);
			//<p> </p> ==> <br>
			if(inBetweenText.trim().length() == 0)
			{
				pTag = WikiConverter.BR_PLACE_HOLDER;
			}
			//<p>{code}</p> ==> {code}\n
			else if(pTag.indexOf(getWIKI_START_TAG()) > -1)
			{
				//replace the <br> to \n
				inBetweenText = inBetweenText.replaceAll(WikiConverter.BR_PLACE_HOLDER, "\n");
				if(!(inBetweenText.endsWith("\n") || inBetweenText.endsWith("\r")))
				{
					inBetweenText += "\n";
					
				}
				pTag = inBetweenText;
			}
			
			//replace it - if they are not same
			if(!pTag.equals(pOriginalTag))
			{
				contentStr = ParseUtil.replaceString(pOriginalTag, pTag, contentStr);
			}

		}//for loop
		return contentStr;
	}
	
	/**
	 * Cleaning html for the tags/patters that are set
	 * 
	 * @param content
	 * @return
	 */
	protected String processTextBetweenPattern(String content)
	{
		String contentStr = new String(content);
		
		if(getHTML2WikiPattern() != null)
		{
			Pattern p = Pattern.compile(getHTML2WikiPattern(), Pattern.DOTALL);
			Matcher matcher = p.matcher(contentStr);
	
			while (matcher.find())
			{
				String originalStr = matcher.group();

				String newString = originalStr.replaceAll("<br>", "\n");
				newString = newString.replaceAll(WikiConverter.BR_PLACE_HOLDER, "\n");
				newString = newString.replaceAll("</p>", "\n");
				newString = removeHTML(newString);
				
				contentStr = ParseUtil.replaceString(originalStr, newString, contentStr);
			}
		}
		
		return contentStr;
	}
	
	private String processPTag(String pTag)
	{
		//get the content within the tag
		pTag =  ParseUtil.getContentBetweenTags(pTag);//contentStr.substring(contentStr .indexOf(">")+1, contentStr .lastIndexOf("<")).trim();
		
		//remove the html tags if any 
		pTag = removeHTML(pTag).trim();
		
		//add the content within <p> tag
		//pTag = "<p>" + pTag + "</p>";
//		pTag = pTag + "\n";
		pTag = pTag.replaceAll(WikiConverter.BR_PLACE_HOLDER, "\n");
		
		return pTag;

	}
	
	private boolean isWikiSyntax(String pTag)
	{
		boolean isWikiSyntax = false;
		
		for(String filter : filterFor_P_Tag())
		{
			if(pTag.indexOf(filter) > -1)
			{
				isWikiSyntax = true;
				break;
			}
		}
		
		return isWikiSyntax;
	}
	
	protected String removeHTML(String htmlString)
	{
	    String string = "";
	    if(htmlString != null)
	    {
	        string = new String(htmlString);
		
	        string = string.replaceAll("\\<.*?\\>", "").trim();

    		//unescape the content 
    //		string = StringEscapeUtils.unescapeHtml(string);
	    }
		
		return string;
	}
	

}
