/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro.code;

import com.resolve.wiki.radeox.filter.context.FilterContext;

/*
 * Dummy filter that does nothing
 *
 */

public class NullCodeFormatter implements SourceCodeFormatter
{

	public NullCodeFormatter()
	{
	}

	public String filter(String content, FilterContext context)
	{
		return content;
	}

	public String getName()
	{
		return "none";
	}

	public int getPriority()
	{
		return 0;
	}
}