/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.owasp.esapi.ESAPI;

import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.VariableUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

public class EncryptMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.encrypt";
    }

    public void execute(Writer writer, MacroParameter params) throws IOException
    {
        RenderContext context = params.getContext();
        WikiRequestContext wikiRequestcontext = (WikiRequestContext) context.get(ConstantValues.WIKI_REQUEST_CONTEXT);

        String fullName = (String) context.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        String namespace = (String) context.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) context.get(ConstantValues.WIKI_DOCNAME_KEY);
        if (StringUtils.isEmpty(fullName))
        {
            fullName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
            namespace = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
            wikiDocName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        }

        String key = params.get("key", 0);
        String text = params.get("value");

        String divId = "encrypt" + JavaUtils.generateUniqueNumber();

        // Convert the key/value pair from system properties to their real values
        String propKey = VariableUtil.getProperty(key);
        String propText = VariableUtil.getProperty(text);

        String encryptedValues = "'', ''";
        try
        {
            encryptedValues = encrypt(propText, propKey);
        }
        catch (Exception e)
        {
            Log.log.debug("Exception while encrypting properties - key: " + key + " value: " + text, e);
        }

        Log.log.trace("***************** Encrypt MACRO******************");
        Log.log.trace("doc fullname: " + fullName);
        Log.log.trace("doc namespace: " + namespace);
        Log.log.trace("doc wikiDocName: " + wikiDocName);

        StringBuffer str = new StringBuffer();

        str.append("<div id=\"" + divId + "\" >");
        str.append("    <a href=\"javascript:decryptText('" + divId + "', " + 
        		   ESAPI.encoder().encodeForHTML(encryptedValues) + ")\">Show encrypted text</a>");
        str.append("</div>");

        Log.log.trace(str);

        writer.write(str.toString());
        return;
    }

    public String encrypt(String valueToEncode, String keyText) throws Exception
    {
        String result = "";

        if (StringUtils.isNotEmpty(valueToEncode) && StringUtils.isNotEmpty(keyText))
        {
            String pass = keyText;
            // String salt = JavaUtils.generateUniqueString();
            String salt = new BigInteger(130, new SecureRandom()).toString(32);
            String text = valueToEncode;

            /* Derive the key, given password and salt. */
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec(pass.toCharArray(), salt.getBytes("UTF-8"), 1024, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

            /* Encrypt the message. */
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            AlgorithmParameters params = cipher.getParameters();
            byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] ciphertext = cipher.doFinal(text.getBytes("UTF-8"));
//            result = "'" + Base64.encodeBase64String(ciphertext) + "', '" + Base64.encodeBase64String(iv) + "'";
            result = "'" + salt + "$" + Base64.encodeBase64String(ciphertext) + "', '" + Base64.encodeBase64String(iv) + "'"; 
        }
        return result;
    }
}
