/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.LocaleRegexTokenFilter;
import com.resolve.wiki.radeox.regex.MatchResult;

/**
 * This class is used for the List
 * 
 * @author jeet.marwah
 * 
 */
public class ListFilter extends LocaleRegexTokenFilter
{
    private final static Map<Character, String> openList = new HashMap<Character, String>();
    private final static Map<Character, String> closeList = new HashMap<Character, String>();

    private static final String UL_CLOSE = "</ul>";
    private static final String OL_CLOSE = "</ol>";
    public final static String END_SYMBOL = "-";

    public static final String LOCAL_KEY = "filter.list";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "(^[\\p{Blank}]*([-?]+|[-*]+|[-*]*[iIaA1ghHkKj]+[\\([\\p{Digit}|-]+\\)]*\\.)[\\p{Space}]+([^\r\n]+)((\r\n)|\n){1})+");//consume only one return for each list item
    }

    protected boolean isSingleLine()
    {
        return false;
    }

    public ListFilter()
    {
        super();

        openList.put(new Character('-'), "<ul class=\"minus\">");
        openList.put(new Character('*'), "<ul class=\"star\">");
        openList.put(new Character('?'), "<ul class=\"star\">");
        openList.put(new Character('1'), "<ol class=\"star\">");
        openList.put(new Character('i'), "<ol class=\"roman\">");
        openList.put(new Character('I'), "<ol class=\"ROMAN\">");
        openList.put(new Character('a'), "<ol class=\"alpha\">");
        openList.put(new Character('A'), "<ol class=\"ALPHA\">");
        openList.put(new Character('g'), "<ol class=\"greek\">");
        openList.put(new Character('h'), "<ol class=\"hiragana\">");
        openList.put(new Character('H'), "<ol class=\"HIRAGANA\">");
        openList.put(new Character('k'), "<ol class=\"katakana\">");
        openList.put(new Character('K'), "<ol class=\"KATAKANA\">");
        openList.put(new Character('j'), "<ol class=\"HEBREW\">");

        closeList.put(new Character('-'), UL_CLOSE);
        closeList.put(new Character('*'), UL_CLOSE);
        closeList.put(new Character('?'), UL_CLOSE);
        closeList.put(new Character('i'), OL_CLOSE);
        closeList.put(new Character('I'), OL_CLOSE);
        closeList.put(new Character('a'), OL_CLOSE);
        closeList.put(new Character('A'), OL_CLOSE);
        closeList.put(new Character('1'), OL_CLOSE);
        closeList.put(new Character('g'), OL_CLOSE);
        closeList.put(new Character('G'), OL_CLOSE);
        closeList.put(new Character('h'), OL_CLOSE);
        closeList.put(new Character('H'), OL_CLOSE);
        closeList.put(new Character('k'), OL_CLOSE);
        closeList.put(new Character('K'), OL_CLOSE);
        closeList.put(new Character('j'), OL_CLOSE);
    }

    @Override
    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }

    @Override
    public void handleMatch(StringBuffer buffer, MatchResult result, FilterContext context)
    {
        try
        {
            BufferedReader reader = new BufferedReader(new StringReader(result.group(0)));
            char[] lastBullet = new char[0];
            String line = null;

            while ((line = reader.readLine()) != null)
            {
                // no nested list handling, trim lines:
                line = line.trim();

                // remove tab
                line = line.replace('\t', ' ');

                if (line.length() == 0)
                {
                    continue;
                }

                // get end of bullet tag
                int bulletEnd = line.indexOf(' ');
                if (bulletEnd < 1)
                {
                    continue;
                }

                int value = 0;
                if (line.charAt(bulletEnd - 1) == '.')
                {
                    // check if has start value
                    int openBracketPos = line.indexOf('(');
                    if (openBracketPos != -1 && openBracketPos < bulletEnd)
                    {
                        // get start value
                        int closeBracketPos = line.indexOf(')');
                        if (closeBracketPos != -1 && closeBracketPos < bulletEnd)
                        {
                            try
                            {
                                String sym = line.substring(openBracketPos + 1, closeBracketPos);
                                if (sym.equals(END_SYMBOL))
                                {
                                    value = 0;
                                }
                                else
                                {
                                    value = Integer.parseInt(sym);
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        }

                        bulletEnd = openBracketPos;
                    }
                    else
                    {
                        bulletEnd--;
                    }
                }

                char[] bullet = line.substring(0, bulletEnd).toCharArray();

                // Logger.log("found bullet: ('" + new String(lastBullet) + "') '" +
                // check whether we find a new list
                int sharedPrefixEnd;
                for (sharedPrefixEnd = 0;; sharedPrefixEnd++)
                {
                    if (bullet.length <= sharedPrefixEnd || lastBullet.length <= sharedPrefixEnd || +bullet[sharedPrefixEnd] != lastBullet[sharedPrefixEnd])
                    {
                        break;
                    }
                }

                for (int i = sharedPrefixEnd; i < lastBullet.length; i++)
                {
                    // Logger.log("closing " + lastBullet[i]);
                    buffer.append(closeList.get(new Character(lastBullet[i])));
                }

                for (int i = sharedPrefixEnd; i < bullet.length; i++)
                {
                    // Logger.log("opening " + bullet[i]);
                    buffer.append(openList.get(new Character(bullet[i])));
                }

                if (value == 0)
                {
                    buffer.append("<li>");
                }
                else
                {
                    buffer.append("<li value=\"" + value + "\">");
                }
                buffer.append(line.substring(line.indexOf(' ') + 1));
                buffer.append("</li>");
                lastBullet = bullet;
            }

            for (int i = lastBullet.length - 1; i >= 0; i--)
            {
                // Logger.log("closing " + lastBullet[i]);
                buffer.append(closeList.get(new Character(lastBullet[i])));
            }
            buffer.append("<br/>");
//            buffer.append("<br/><p class=\"paragraph\"/>");
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

    }

}
