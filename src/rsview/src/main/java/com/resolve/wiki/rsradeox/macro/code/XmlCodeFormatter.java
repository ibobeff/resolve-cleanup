/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro.code;

/*
 * XmlCodeFilter colourizes Xml Code
 *
 */

public class XmlCodeFormatter extends DefaultRegexCodeFormatter implements SourceCodeFormatter
{
	private static final String KEYWORDS = "\\b(xsl:[^&\\s]*)\\b";
	private static final String TAGS = "(&#60;/?.*?&#62;)";
	private static final String QUOTE = "\"(([^\"\\\\]|\\.)*)\"";

	public XmlCodeFormatter()
	{
		super(QUOTE, "<span class=\"xml-quote\">\"$1\"</span>");
		addRegex(TAGS, "<span class=\"xml-tag\">$1</span>");
		addRegex(KEYWORDS, "<span class=\"xml-keyword\">$1</span>");
	}

	public String getName()
	{
		return "xml";
	}
}
