/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.lang.StringEscapeUtils;

import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

/*
  Macro for defining and displaying tables. The rows of the table are
  devided by newlines and the columns are divided by pipe symbols "|".
  The first line of the table is rendered as column headers.
 
   {xtable} 
    col1:width=100&type=date&format=Y-m-d|col2:width=400|col3=:width=200
     data1|data2|data3
     data1|data2|data3
     data1|data2|data3
    {xtable}
    
    {xtable:width=200|height=400|sql=select * from xyz|fitwidth=<true|false (by default)>} 
    col1:width=100&type=date&format=Y-m-d|col2:width=400|col3=:width=200 
    {xtable}
    
    fitwidth=<true|false (by default) --> general use of it is if this is the only component on the wiki page, make this flag as true
    drilldown=/resolve/service/wiki/view/^col1/^col2?col1=val&....
    target=_blank|_parent|_self|_top  
*/
public class XTableMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.xtable";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        //Get parameters
        String sql = params.get("sql");     //sql to get the data
        sql= HttpUtil.sanitizeValue(sql, "ResolveText", 2000, HttpUtil.SQL_ENCODING);
        String drillDown = params.get("drilldown");
        drillDown= HttpUtil.sanitizeValue(drillDown, "ResolveText", 2000, HttpUtil.SQL_ENCODING);
        String target = params.get("target");
        target= HttpUtil.sanitizeMacroValue(target);
                        
        //Unique id for the div to render to
        String divId = "results" + JavaUtils.generateUniqueNumber();

        String content = params.getContent();
        content= HttpUtil.sanitizeMacroValue(content);

        String[] split = content.trim().split("\\r*\\n+");
        StringBuffer columns = new StringBuffer();
        StringBuffer rows = new StringBuffer();
        for (String r : split)
        {
            if (StringUtils.isNotBlank(r))
            {
                if (columns.length() == 0)
                    columns.append(r);
                else
                {
                    if (rows.length() > 0) rows.append("#");
                    rows.append(r);
                }
            }
        }

        //Build up div and script to render
        StringBuffer str = new StringBuffer();
        str.append("<div id=\"%1$s\"></div>\n");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.XTable',\n");
        str.append("        columnsConfig : '%2$s',\n");
        //        if( height > 0)str.append("        height : ").append(height).append(",\n");
        //        if( width > 0)str.append("        width : ").append(width).append(",\n");
        str.append("        rows : '\"%3$s\"',\n");
        str.append("        sql : '%4$s',\n");
        str.append("        drillDown : '%5$s',\n");
        str.append("        target : '%6$s',\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('%1$s');\n");
        str.append("});\n");
        str.append("</script>\n");

        String result = String.format(str.toString(), divId, columns, rows, StringUtils.isNotBlank(sql) ? StringEscapeUtils.escapeHtml(sql.replace("'", "\\'")) : "", StringUtils.isNotBlank(drillDown) ? drillDown.replace("'", "\\'") : "", StringUtils.isNotBlank(target) ? target.replace("'", "\\'") : "");
        writer.write(result);
    }
}
