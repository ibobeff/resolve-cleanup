/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.services.exception.WikiException;
import com.resolve.services.vo.form.FormSubmitDTO;

/**
 * Interface for all the actions available in the application. Its like for each click by the user, a method in this interface gets
 * invoked.
 * 
 * @author jeet.marwah
 *
 */
public interface WebApi
{
	public String viewAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE

	public Map<String, String> editAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
//    public Map<String, String> getSectionToEdit(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> getPropertiesToEdit(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
	public Map<String, String> saveAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
//	public Map<String, String> saveActionRPC(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> saveAllActionRPC(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> saveProperties(Map<String, Object> hmValues)throws WikiException;
	
//	public  Map<String, Object> diffrevAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE
	
	public Map<String, Object> viewattachAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE
	
	public Map<String, String> lookupAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE
	
	public Map<String, Object> attachAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
//	public Map<String, String> renameAttachmentRPC(Map<String, Object> payload) throws WikiException;
	
//	public void downloadAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE
	
	public Map<String, String> deleteattachAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
//	public Map<String, String> deleteAttachmentsRPC(Map<String, Object> payload) throws WikiException;
	
	public Map<String, Object> listdocAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE
	
	public String viewmodelcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
	public Map<String, String> editmodelcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
	public Map<String, String> savemodelcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;

	public String viewexcpcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
	public Map<String, String> editexcpcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
	public Map<String, String> saveexcpcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;

	public String viewfinalcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
	public Map<String, String> editfinalcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
	public Map<String, String> savefinalcAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
//	public GenericContainerModel getNamespaceInfo(Map<String, Object> payload) throws WikiException;
//	public GenericContainerModel getNamespaceStats(Map<String, Object> payload) throws WikiException;
//	public GenericContainerModel getRelateTagsInfo(Map<String, Object> payload) throws WikiException;
//	
//	public PagingLoadResult<DocumentInfoModel> getSearchResultsFor(RSPagingLoadConfig config, Map<String, Object> params) throws WikiException;
////	public PagingLoadResult<DocumentInfoModel> getAdvanceSearchResultsFor(RSPagingLoadConfig config, Map<String, Object> params)  throws WikiException;
//	
//	public PagingLoadResult<DocumentInfoModel> getListOfDocumentsFor(RSPagingLoadConfig config, Map<String, Object> params) throws WikiException;
//	public PagingLoadResult<DocumentInfoModel> recentChangesSearch(RSPagingLoadConfig config, Map<String, Object> params);
//	public List<DocumentInfoModel> getListOfDocuments(Map<String, Object> params) throws WikiException;
//	public List<DocumentInfoModel> getRecentlyVisited(Map<String, Object> params);
//	
//	
//	public PagingLoadResult<AttachmentModel> getListOfAttachments(RSPagingLoadConfig config, Map<String, Object> params);
//	public ArrayList<AttachmentBeanModel> getAttachmentsForDocument(Map<String, Object> params);
//	public PagingLoadResult<AttachmentModel> getListOfGlobalAttachments(RSPagingLoadConfig config, Map<String, Object> params);
//	public List<AttachmentModel> getGlobalAttachments(Map<String, Object> params);
//	public Map<String, String> globalAttach(Map<String, Object> params)throws WikiException;
	
//	public Map<String, String> getXMLModel(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> getXMLException(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> getXMLFinal(Map<String, Object> payload) throws WikiException;
	
//	public Map<String, String> getXMLModelToEdit(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> getXMLExceptionToEdit(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> getXMLFinalToEdit(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> cancelEdit(Map<String, Object> payload) throws WikiException;
	
//	public Map<String, List<TagModel>> getTags(Map<String, Object> payload) throws WikiException;
//	public Map<String, List<RoleModel>> getRoles(Map<String, Object> payload) throws WikiException;
//	public List<TagModel> getAllTags()  throws WikiException;
//	public List<RoleModel> getAllRoles()  throws WikiException;
//	public List<UserBaseModel> getUsers()  throws WikiException;
//	public ArrayList<RoleBaseModel> getAllRoleModels();
//	public List<RoleModel> getAllRolesWithHomepage()  throws WikiException;
	
//	public Map<String, String> getURLFor(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> getCurrentPageInfo(Map<String, Object> payload) throws WikiException;
	
//	public Map<String, String> getToolbar(Map<String, Object> payload) throws WikiException;
	
//	public PagingLoadResult<RevisionModel> getListOfRevisions(RSPagingLoadConfig config, Map<String, Object> params);
	
//	public Map<String, String> commitRevision(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> getViewRevisionData(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> rollbackToRevision(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> compareRevisions(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> resetRevision(Map<String, Object> payload) throws WikiException;

	
//	public Map<String, String> navigateAction(HttpServletRequest request, HttpServletResponse response) throws WikiException;
//	public String getNavigateData(HttpServletRequest request, HttpServletResponse response) throws WikiException;
	
//	public Map<String, String> moveOrRename(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> copy(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> delete(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> undelete(Map<String, Object> payload) throws WikiException;
//    public Map<String, String> activateDeactivate(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> lockUnlock(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> hideUnhide(Map<String, Object> payload) throws WikiException;
//    public Map<String, String> updateRating(Map<String, Object> payload) throws WikiException;
//    public Map<String, String> updateSearchWeight(Map<String, Object> payload) throws WikiException;
//    public Map<String, String> index(Map<String, Object> payload) throws WikiException;
    
//    public List<BaseModel> getAllFeedbackFor(Map<String, Object> payload)  throws WikiException;
//    public Map<String, String> updateFeedback(Map<String, Object> payload)  throws WikiException;
	
//	public Map<String, String> updateRolesForDocuments(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> defaultRoles(Map<String, Object> payload) throws WikiException;
//	public GenericContainerModel getDefaultRolesForNamespace(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> updateTagsForDocuments(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> updateHomePageForRoles(Map<String, Object> payload) throws WikiException;
	
//	public Map<String, Object> getModel(Map<String, Object> payload) throws WikiException;
//	public void saveModel(Map<String, Object> payload) throws WikiException;
	
//    public GenericContainerModel prepareImportManifest(Map<String, Object> payload) throws WikiException;
//    public GenericContainerModel validateImportManifest(Map<String, Object> payload) throws WikiException;
//    public void uploadImportFile(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> importModule(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> importModule(Map<String, Object> payload, boolean blockIndex) throws WikiException;
//	public Map<String, String> importModule(String moduleName) throws WikiException;
//	public Map<String, String> importModule(String moduleName, String username) throws WikiException;
//	public Map<String, String> importModule(String moduleName, boolean blockIndex) throws WikiException;
//	public Map<String, String> importModule(String moduleName, String username, boolean blockIndex) throws WikiException;
//    public GenericContainerModel prepareExportManifest(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> exportModule(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> exportModule(String moduleName) throws WikiException;
//	public Map<String, Object> downloadModule(Map<String, Object> payload) throws WikiException;
//	public Map<String, Object> uninstallModule(Map<String, Object> payload) throws WikiException;

//	public Map<String, String> executeRunbook(Map<String, Object> payload) throws WikiException;
//	public Map<String, String> debugRunbook(Map<String, Object> payload) throws WikiException;
//	
//	public Map<String, String> addNewPage(Map<String, Object> payload) throws WikiException;

//	public Map<String, String> current(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE
	public Map<String, String> homepage(HttpServletRequest request, HttpServletResponse response) throws WikiException;//DONE
	
	public String getHtmlForResultWikiTag(HttpServletRequest request, HttpServletResponse response) throws WikiException;
    public String getHtmlForDetailWikiTag(HttpServletRequest request, HttpServletResponse response) throws WikiException;
    
//    public GenericSectionModel getSections(Map<String, Object> payload)  throws WikiException;    
    
//    public HashMap<String, String> submitForm(Map<String, Object> payload) throws WikiException;
//    public Map<String, String> submitFormExt(FormSubmitDTO formSubmitDTO, HttpServletRequest request, HttpServletResponse response) throws WikiException;
//    public Boolean checkWikiDocPermission(String docFullName, String userName);
//    public Map<String, Integer> getImpexStatus(String operation);
    
//    public Map<String, String> updateExpirationDateForDocuments(Map<String, Object> params) throws WikiException;
//    public Map<String, String> updateLastReviewedDateForWikiDoc(Map<String, Object> params) throws WikiException;


}
