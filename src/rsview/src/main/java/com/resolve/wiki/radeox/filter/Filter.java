/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.filter.context.FilterContext;
/*
 * Filter interface. Concrete Filters should
 * implement Filter. Filters transform a
 * String (usually snip content) to another String
 * (usually HTML).
 */

public interface Filter {
  public String filter(String input, FilterContext context);

  public String[] replaces();

  public String[] before();

  public void setInitialContext(InitialRenderContext context);

  public String getDescription();
}