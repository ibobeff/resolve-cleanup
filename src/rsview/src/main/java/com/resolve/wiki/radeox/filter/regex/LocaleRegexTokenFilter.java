/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.regex;

import java.util.HashMap;
import java.util.Map;

import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;

/**
 * Filter that extends RegexTokenFilter but reads regular expressions from a locale
 * 
 * This class has been refactored to read the values from the hashmap rather from a properties file
 * 
 * @author jeet.marwah
 * 
 */
public abstract class LocaleRegexTokenFilter extends RegexTokenFilter
{

	protected abstract void setValues();//for the match-print

	//this map is a substitute for the radeox.properties file
	protected Map<String, String> values = new HashMap<String, String>();

	protected synchronized void addKeyValue(String key, String value)
	{
		values.put(key, value);
	}

	protected boolean isSingleLine()
	{
		return false;
	}

	public void setInitialContext(InitialRenderContext context)
	{
		super.setInitialContext(context);
		clearRegex();
		setValues();

		String match = values.get(getLocaleKey() + ".match");
		addRegex(match, "", isSingleLine() ? RegexReplaceFilter.SINGLELINE : RegexReplaceFilter.MULTILINE);
	}

	protected abstract String getLocaleKey();
}
