/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import org.owasp.esapi.ESAPI;

import com.resolve.dto.SectionModel;
import com.resolve.dto.SectionType;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

/**
 * Macro for defining and displaying tables. The rows of the table are
 * devided by newlins and the columns are divided by pipe symbols "|".
 * The first line of the table is rendered as column headers.
 * 
 * type ==> source --> textarea
 *          wysiwyg --> textarea with editor
 *          url --> iframe inputs
 *          grid --> iframe inputs to prepare grid/table view
 * 
 * {section:type=source|title=wikiText|id=rssection_12312312|height=500}
 * other macros, etc
 * {section}
 *
 * @author jeet.marwah
 *
 */
public class SectionMacro extends BaseLocaleMacro
{
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        String content = params.getContent();
        String type = StringUtils.isNotEmpty(params.get(SectionModel.TYPE)) ? params.get(SectionModel.TYPE) : "";
        String id = StringUtils.isNotEmpty(params.get(SectionModel.ID)) ? params.get(SectionModel.ID) : "";
        String height = StringUtils.isNotEmpty(params.get(SectionModel.HEIGHT)) ? params.get(SectionModel.HEIGHT) : "";

        StringBuffer output = new StringBuffer();
        if (StringUtils.isNotBlank(id))
        {
            output.append("<div id='").append(id).append("' ");
            if (StringUtils.isNotBlank(height) && shouldUseHeight(type))
            {
                output.append("style='height:").append(height).append(";'");
            }
            output.append(">");
        }
        
        output.append(content);
        
        if (StringUtils.isNotBlank(id))
        {
            output.append("</div>");
        }
        
        writer.write("<div class='encodedHtml'> " + ESAPI.encoder().encodeForHTML(output.toString()) + " </div>");
    }

    private boolean shouldUseHeight(String type)
    {
        return type.equalsIgnoreCase(SectionType.TABLE.getTagName()) || type.equalsIgnoreCase(SectionType.RELATIONS.getTagName()) || type.equalsIgnoreCase(SectionType.REPORTS.getTagName()) || type.equalsIgnoreCase(SectionType.URL.getTagName());
    }

    @Override
    public String getLocaleKey()
    {
        return "macro.section";
    }

    public static String getSectionDefinition(String id, String title, String type, String content)
    {
        StringBuffer sb = new StringBuffer();

        sb.append("{section:id=").append(id).append("|title=").append(title).append("|type=").append(type).append("}").append("\n");
        if (StringUtils.isNotEmpty(content))
        {
            sb.append(content).append("\n");
        }
        sb.append("{section}");

        return sb.toString();
    }
}
