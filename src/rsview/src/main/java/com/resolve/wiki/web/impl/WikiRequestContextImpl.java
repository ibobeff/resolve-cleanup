/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.web.impl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.VelocityContext;

import com.resolve.services.vo.WikiUser;
import com.resolve.util.Constants;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.api.WikiAPI;
import com.resolve.wiki.api.WikiDocumentAPI;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This interface is for each REQUEST that comes in. The purpose is to get everything that is required related to a request at 1 place.
 * 
 * Request, Response, ServletContext, (WikiDoc, User and his Credentials, etc) can use the set and get attributes
 * 
 * @author jeet.marwah
 * 
 */
public class WikiRequestContextImpl implements WikiRequestContext
{
	private Map<String, Object> hm = new HashMap<String, Object>();
	private HttpServletRequest httpServletRequest;
	private HttpServletResponse httpServletResponse;
//	private ServletContext servletContext = null;
	private WikiDocumentAPI wikiDocumentAPI = null;
    private WikiDocumentAPI currWikiDocumentAPI = null;
	private Wiki wiki = null;
	private WikiAPI wikiAPI = null;
	private VelocityContext velocityContext;
	private WikiUser wikiUser;
	private RenderContext renderContext;//for the Radeox renderer
	private StringBuffer log = new StringBuffer();

	@Override
	public Wiki getWiki()
	{
		return wiki;
	}

	@Override
	public void setWiki(Wiki wiki)
	{
		this.wiki = wiki;
	}

	@Override
	public HttpServletRequest getHttpServletRequest()
	{
		return httpServletRequest;
	}

	@Override
	public void setHttpServletRequest(HttpServletRequest request)
	{
		this.httpServletRequest = request;
	}

	@Override
	public HttpServletResponse getHttpServletResponse()
	{
		return httpServletResponse;
	}

	@Override
	public void setHttpServletResponse(HttpServletResponse response)
	{
		this.httpServletResponse = response;
	}

	@Override
	public Object getAttribute(String name)
	{
		return hm.get(name);
	}

	@Override
	public synchronized void setAttribute(String name, Object object)
	{
		hm.put(name, object);
	}

	@Override
	public synchronized void removeAttribute(String name)
	{
		hm.remove(name);
	}
	
	@Override
	public synchronized void addAttributes(Map<String, Object> hmAdditional)
	{
		hm.putAll(hmAdditional);
	}

//	@Override
//	public ServletContext getServletContext()
//	{
//		return servletContext;
//	}
//
//	@Override
//	public void setServletContext(ServletContext sc)
//	{
//		this.servletContext = sc;
//
//	}

	@Override
	public WikiDocumentAPI getWikiDocumentAPI()
	{
		return wikiDocumentAPI;
	}

	@Override
	public void setWikiDocumentAPI(WikiDocumentAPI wikiDoc)
	{
		this.wikiDocumentAPI = wikiDoc;
	}

	@Override
	public VelocityContext getVelocityContext()
	{
		return velocityContext;
	}

	@Override
	public void setVelocityContext(VelocityContext vc)
	{
		this.velocityContext = vc;
	}

	public String getUsername()  
	{
		return (String)getAttribute(Constants.HTTP_REQUEST_USERNAME);
	}

	@Override
	public WikiUser getWikiUser()
	{
		return wikiUser;
	}

	@Override
	public void setWikiUser(WikiUser wikiUser)
	{
		this.wikiUser = wikiUser;
	}

	@Override
	public RenderContext getRenderContext()
	{
		return renderContext;
	}

	@Override
	public void setRenderContext(RenderContext renderContext)
	{
		this.renderContext = renderContext;
	}
	
	public StringBuffer getLog()
	{
		return log;
	}

	public void setLog(StringBuffer log)
	{
		this.log = log;
	}
	
	@Override
	public void log(String logMessage)
	{
		this.log.append(logMessage).append("</br>");
	}

	@Override
	public String getLogMessages()
	{
		return this.log.toString();
	}

    @Override
    public WikiAPI getWikiAPI()
    {
        return wikiAPI;
    }

    @Override
    public void setWikiAPI(WikiAPI wiki)
    {
        wikiAPI = wiki;
    }

    @Override
    public WikiDocumentAPI getCurrWikiDocumentAPI()
    {
        return currWikiDocumentAPI;
    }

    @Override
    public void setCurrWikiDocumentAPI(WikiDocumentAPI wikiDocAPI)
    {
       this.currWikiDocumentAPI = wikiDocAPI;
        
    }

}
