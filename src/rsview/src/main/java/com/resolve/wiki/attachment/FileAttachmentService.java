/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.attachment;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.exception.WikiException;
import com.resolve.wiki.web.WikiRequestContext;

public interface FileAttachmentService
{

	/**
	 * This API is for downloading the attached file.
	 */
//	public void downloadAttachment(WikiRequestContext wikiRequestContext) throws WikiException;

	/**
	 * This method is for uploading an attachment for a wikidoc
	 * 
	 * @param wikiRequestContext
	 * @throws WikiException
	 */
	public void uploadAttachment(WikiDocument wikiDocument, WikiRequestContext wikiRequestContext) throws WikiException;
	
	/**
     * This method is for renaming an attachment for a wikidoc
     * 
     * @param wikiRequestContext
     * @throws WikiException
     */
    public void renameAttachment(WikiDocument wikiDocument, WikiRequestContext wikiRequestContext) throws WikiException;
	
	/**
	 * This method is to delete a list of attachments selected by the user
	 * 
	 * @param wikiRequestContext
	 * @throws WikiException
	 */
	public void deleteAttachments(WikiRequestContext wikiRequestContext) throws WikiException;
	
}
