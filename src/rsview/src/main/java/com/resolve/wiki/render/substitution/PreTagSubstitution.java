/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.render.substitution;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.StringUtils;
import com.resolve.wiki.util.SearchTag;

/**
 * Substitution class for {pre} tag
 * 
 * @author jeet.marwah
 *
 */
public class PreTagSubstitution
{
    private List<String> listValues = new ArrayList<String>();
    private static final String START_PRETAG = "{pre}";
    private static final String END_PRETAG = "{pre}";
    private static final String PRE_PAD = "%__";
    private static final String POST_PAD = "__%";
    private int counter = 0;

    /**
     * This is to replace the {pre} with some non-wiki tag
     * 
     * @param content
     * @return
     */
    public String replacePreTagWithNonWikiText(String content)
    {
        String result = content;
        SearchTag st = new SearchTag(START_PRETAG, END_PRETAG);
        String script = "";
        String replacingString = "";

        try
        {
            while (st.hasTag(result))
            {
                script = st.getTheScriptForTag(result);
                listValues.add(script);
                replacingString = PRE_PAD + counter + POST_PAD;

                result = st.replaceCodeWithReplacmentString(result, replacingString);
                counter++;
            }
        }
        catch (Exception e)
        {
            result = st.replaceCodeWithReplacmentString(result, e.getMessage());
        }

        return result;

    }

    /**
     * This is to replace the {code} with some non-wiki tag
     * 
     * @param content
     * @return
     */
    public String replaceCodeTagWithNonWikiText(String content)
    {
        String result = content;
        String matchResult = content;
        Pattern pattern = Pattern.compile("\\{code(.*?)?\\}");
        Matcher matcher = pattern.matcher(content);

        String start = "";
        String code = "";
        String end = "";
        while (matcher.find())
        {
            String tag = matcher.group(0);
            if (StringUtils.isBlank(start))
                start = tag;
            else if (StringUtils.isBlank(end))
            {
                end = tag;
                code = matchResult.substring(matchResult.indexOf(start) + start.length());
                code = code.substring(0, code.indexOf(end));
                listValues.add(StringUtils.escapeHtml(code).trim());
                String startString = start + code + end;
                String replaceString = start + PRE_PAD + counter + POST_PAD + end;
                result = result.replace(startString, replaceString);
                matchResult = matchResult.substring(matchResult.indexOf(startString) + startString.length());
                counter++;

                start = "";
                end = "";
            }
        }

        return result;
    }

    /**
     * This is to replace back the values of {pre}
     * 
     * @param content
     * @return
     */
    public String replaceNonWikiTextWithPreTag(String content)
    {

        for (int i = listValues.size() - 1; i >= 0; i--)
        {
            content = StringUtils.replace(content, PRE_PAD + i + POST_PAD, listValues.get(i));
        }
        return content.replaceAll("(\r\n|\n|\r)", "\r\n");

    }

    /**
     * This is to replace back the values of {pre} ==> {pre}text....{pre}
     * 
     * @param content
     * @return
     */
    public String replaceNonWikiTextWithPreTagSyntax(String content)
    {

        for (int i = 0; i < listValues.size(); i++)
        {
            content = StringUtils.replace(content, PRE_PAD + i + POST_PAD, START_PRETAG + (String) listValues.get(i) + END_PRETAG);
        }
        return content;

    }

}
