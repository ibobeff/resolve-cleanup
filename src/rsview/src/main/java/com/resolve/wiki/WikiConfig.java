/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.resolve.services.exception.WikiException;

/**
 * This class reads the wiki config file.
 * 
 * @author jeet.marwah
 *
 */
public class WikiConfig extends Properties
{
	private static final long serialVersionUID = 3251899663564567785L;

	public WikiConfig(){}
	
	public WikiConfig(String path) throws WikiException
    {
        try
        {
            FileInputStream fis = new FileInputStream(path);
            loadConfig(fis, path);
            fis.close();
        }
        catch (Exception e)
        {
            Object[] args = { path };
            throw new WikiException(WikiException.MODULE_WIKI_CONFIG, WikiException.ERROR_WIKI_CONFIG_FILENOTFOUND, "Configuration file {0} not found", e, args);
        }
    }

    public WikiConfig(InputStream is) throws WikiException
    {
        loadConfig(is, "");
    }

    public void loadConfig(InputStream is, String path) throws WikiException
    {
        try
        {
            load(is);
        }
        catch (IOException e)
        {
            Object[] args = { path };
            throw new WikiException(WikiException.MODULE_WIKI_CONFIG, WikiException.ERROR_WIKI_CONFIG_FORMATERROR, "Error reading configuration file", e, args);
        }
    }
}//end of class
