/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.store.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.query.Query;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.resolve.dto.DocumentInfoModel;
import com.resolve.dto.RightType;
import com.resolve.persistence.dao.AccessRightsDAO;
import com.resolve.persistence.dao.MetaFormViewDAO;
import com.resolve.persistence.dao.ResolveActionInvocDAO;
import com.resolve.persistence.dao.ResolveActionInvocOptionsDAO;
import com.resolve.persistence.dao.ResolveActionParameterDAO;
import com.resolve.persistence.dao.ResolveActionTaskDAO;
import com.resolve.persistence.dao.ResolveAssessDAO;
import com.resolve.persistence.dao.ResolveParserDAO;
import com.resolve.persistence.dao.ResolvePreprocessDAO;
import com.resolve.persistence.dao.WikiAttachmentDAO;
import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.dao.WikidocAttachmentRelDAO;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ArchiveWorksheet;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionInvocOptions;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.model.WikidocStatistics;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.search.model.Worksheet;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class StoreUtility
{

    //private static WorksheetDAO worksheetDao = CassandraDaoFactory.getWorksheetDAO();

    /**
     * Returns a map having the doc name as KEY and the sys_id as VALUE
     * 
     * @param docFullName
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static Map<String, String> getWikiDocIdOf(List<String> docFullName)
    {
        Map<String, String> docids = new HashMap<String, String>();
        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        String qryFileNameStr = SQLUtils.prepareQueryString(docFullName, dbType);
        String sql = "select UFullname, sys_id from WikiDocument where UFullname IN (" + qryFileNameStr + ")";

        try
        {

          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                Query query = HibernateUtil.createQuery(sql);
                List page = query.list();
                for (Object obj : page)
                {
                    Object[] arr = (Object[]) obj;
                    String UFullname = (String) arr[0];
                    String id = (String) arr[1];

                    docids.put(UFullname, id);

                }
        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return docids;
    }

    /**
     * Updates the wikidocument objs
     * 
     * @param list
     */
    public static void saveListOfWikiDocs(Collection<WikiDocument> list, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            for (WikiDocument doc : list)
	            {
	                dao.persist(doc);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }
    }

    /**
     * Returns true if the wikidoc with this name already exist, else false
     * 
     * @param namespace
     *            - Test
     * @param filename
     *            - test2
     * @return
     */
    public static boolean isWikidocExist(String namespace, String filename)
    {
        return isWikidocExist(namespace + "." + filename);
    }

    public static boolean isWikidocDeleted(String namespace, String filename) throws WikiException
    {
        return isWikidocDeleted(namespace + "." + filename);
    }

    /**
     * Returns true if the wikidoc with this name already exist, else false
     * 
     * @param fullname
     *            - Test.test1
     * @return
     */
    public static boolean isWikidocExist(String fullname)
    {
        return WikiUtils.isWikidocExist(fullname);
    }//isWikidocExist

    /**
     * API to find if the delete flag is set for a wikidocument
     * 
     * @param fullname
     * @return
     * @throws WikiException
     */
    public static boolean isWikidocDeleted(String fullname) throws WikiException
    {
        boolean isDeleted = false;

        try
        {
          HibernateProxy.setCurrentUser("system");
        	isDeleted = (boolean) HibernateProxy.execute(() -> {
        		WikiDocument findDoc = new WikiDocument();
        		findDoc.setUFullname(fullname);

	            WikiDocumentDAO taskDao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            List<WikiDocument> list = taskDao.find(findDoc);
        	
	            if (list.size() > 0)
	            {
	                findDoc = list.get(0);
	                return findDoc.getUIsDeleted();
	            }
	            else
	            {
	                throw new WikiException("WikiDocument " + fullname + " does not exist. ");
	            }
			});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return isDeleted;
    }

    public static boolean isCustomTable(String tableName)
    {
        return ServiceHibernate.isCustomTable(tableName);
    }// isCustomTable

    public static WikiDocument findById(String id) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
	            WikiDocument doc = null;
	            AccessRights ar = null;
	            
	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            doc = dao.findById(id);
	
	            if (doc != null)
	            {
	                // load the stats for the doc
	                if (doc.getWikidocStatistics() != null)
	                {
	                    for (WikidocStatistics stats : doc.getWikidocStatistics())
	                    {
	                        /*Integer in =*/ stats.getUViewCount();
	                    }
	                }
	
	                // populate the access rights
	                if (doc.getAccessRights() != null)
	                {
	                    ar = doc.getAccessRights();
	                }
	                else
	                {
	                    ar = new AccessRights();
	                    ar.setUResourceType(WikiDocument.RESOURCE_TYPE);
	                    ar.setUResourceName(doc.getUFullname());
	                    ar.setUResourceId(doc.getSys_id());
	
	                    AccessRightsDAO daoAR = HibernateUtil.getDAOFactory().getAccessRightsDAO();
	                    ar = daoAR.findFirst(ar);
	                }
	
	                doc.setUReadRoles(ar.getUReadAccess());
	                doc.setUWriteRoles(ar.getUWriteAccess());
	                doc.setUAdminRoles(ar.getUAdminAccess());
	                doc.setUExecuteRoles(ar.getUExecuteAccess());
	            }
	            
	            return doc;
            });
        }
        catch (Throwable e)
        {
            Log.log.error("Error while looking for Wikidoc Sys_Id:" + id, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return  null;
    }

    public static WikiAttachment findAttachmentById(String attachmentId) throws WikiException
    {
        WikiAttachment att = null;

        try
        {
          HibernateProxy.setCurrentUser("system");
            att = (WikiAttachment) HibernateProxy.execute(() -> {

            	WikiAttachmentDAO dao = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
            	return dao.findById(attachmentId);

            });
        }
        catch (Throwable e)
        {
            Log.log.debug(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return att;
    }// findAttachmentById

    /**
     * Find a doc
     * 
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public static WikiDocument find(String docFullName) throws WikiException
    {
        return find(docFullName, true);
    } //find

    /**
     * Find a doc
     * 
     * @param docFullName
     * @param throwException
     * @return
     * @throws WikiException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static WikiDocument find(String docFullName, boolean throwException) throws WikiException {
    	WikiDocument returnedDoc = null;
    	
        if (StringUtils.isNotBlank(docFullName)) {        	
            try {
                  HibernateProxy.setCurrentUser("system");
            	returnedDoc = (WikiDocument) HibernateProxy.executeNoCache(() -> {
                	AccessRights ar = null;
                	WikiDocument doc = null;
	                List<WikiDocument> lWiki = new ArrayList<WikiDocument>();
	                String hql = "from WikiDocument wd where LOWER(wd.UFullname) like '" + StringUtils.escapeSql(docFullName.toLowerCase()) + "'";
	                Query query = HibernateUtil.createQuery(hql);
	                lWiki = query.list();
	
	                if (lWiki.size() == 1) {
	                    doc = lWiki.get(0);
	                } else if (lWiki.size() > 1) {
	                    throw new WikiException(new Exception("There are more then 1 copy of <" + docFullName + "> document. Please contact the Administrator."));
	                } else {
	                    if (throwException) {
	                        throw new WikiException(new Exception("There is no document by name <" + docFullName + ">."));
	                    }
	                }
	
	                // populate the access rights
	                if (doc != null) {
	                    ar = doc.getAccessRights();
	                }
                
	                if (ar != null) {
	                	doc.setUReadRoles(ar.getUReadAccess());
	                	doc.setUWriteRoles(ar.getUWriteAccess());
	                	doc.setUAdminRoles(ar.getUAdminAccess());
	                	doc.setUExecuteRoles(ar.getUExecuteAccess());
	                }
	                
	                return doc;
                });
            } catch (Throwable e) {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
                return returnedDoc;
            }
        }

        return returnedDoc;

    } //find

    public static WikiDocument find(String namespace, String filename) throws WikiException
    {
        return find(namespace + "." + filename);
    }

    public static WikiDocument findAndLoadReferences(String docFullName) throws WikiException
    {
    	WikiDocument docResult = new WikiDocument();
    	docResult.setUFullname(docFullName);

    	try
        {
          HibernateProxy.setCurrentUser("system");
            docResult = (WikiDocument) HibernateProxy.execute(() -> {
            	WikiDocument doc = new WikiDocument();
            	doc.setUFullname(docFullName);
            	AccessRights ar = null;

	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            List<WikiDocument> lWiki = dao.find(doc);
	            if (lWiki.size() == 1)
	            {
	                doc = lWiki.get(0);
	            }
	            else if (lWiki.size() > 1)
	            {
	                throw new WikiException(new Exception("There are more then 1 copy of <" + docFullName + "> document. Please contact the Administrator."));
	            }
	            else
	            {
	                throw new WikiException(new Exception("There is no document by name <" + docFullName + ">."));
	            }
	
	            // load references
	            if (doc.getWikidocAttachmentRels() != null)
	            {
	                doc.getWikidocAttachmentRels().size();
	            }
	            if (doc.getWikidocResolveTagRels() != null)
	            {
	                doc.getWikidocResolveTagRels().size();
	            }
	            if (doc.getWikidocStatistics() != null)
	            {
	                doc.getWikidocStatistics().size();
	            }
	            if (doc.getRoleWikidocHomepageRels() != null)
	            {
	                doc.getRoleWikidocHomepageRels().size();
	            }
	
	            // populate the access rights
	            ar = new AccessRights();
	            ar.setUResourceType(WikiDocument.RESOURCE_TYPE);
	            ar.setUResourceName(doc.getUFullname());
	            ar.setUResourceId(doc.getSys_id());
	
	            AccessRightsDAO daoAR = HibernateUtil.getDAOFactory().getAccessRightsDAO();
	            ar = daoAR.findFirst(ar);
	            if (ar == null)
	            {
	                throw new WikiException("The document " + doc.getUFullname() + " has no AccessRights. Please contact the administrator to fix it.");
	            }

	            if (ar != null)
	            {
	            	doc.setUReadRoles(ar.getUReadAccess());
	            	doc.setUWriteRoles(ar.getUWriteAccess());
	            	doc.setUAdminRoles(ar.getUAdminAccess());
	            	doc.setUExecuteRoles(ar.getUExecuteAccess());
	            }
	            
	            return doc;
            });
        }
        catch (Throwable e)
        {
            Log.log.debug(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
		
    	return docResult;
    }

    /**
     * utility method for insert, update and delete
     * 
     * @param sql
     * @throws WikiException
     */
    @SuppressWarnings("rawtypes")
    public static void executeHibernateSQL(String sql, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
	            Query query = HibernateUtil.createQuery(sql);
	            query.executeUpdate();
            });
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }

    /**
     * returns a list of ids based on the select HQL passed. The select should
     * only have 1 string to select
     * 
     * 
     * @param selectSql
     *            - eg. select id from WikiDocument
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<String> getListOfidsUsingHibernateSQL(String selectSql)
    {
        List<String> result = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (List<String>) HibernateProxy.execute(() -> {
        		Query query = HibernateUtil.createQuery(selectSql);
                return query.list();
        	});
            
        }
        catch (Exception e)
        {
            // to avoid null pointer
            result = new ArrayList<String>();

            Log.log.error("Error with qry : " + selectSql);
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }

    public static Set<String> getRolesFor(RightType rightType, List<PropertiesVO> lProps, String docNamespace)
    {
        Set<String> roles = new TreeSet<>();
        boolean hasNamespaceDefaultSet = false;

        if (docNamespace != null && docNamespace.trim().length() > 0)
        {
            docNamespace = docNamespace.trim().toLowerCase();
        }
        else
        {
            docNamespace = null;
        }

        if (lProps != null && lProps.size() > 0)
        {
            for (PropertiesVO p : lProps)
            {
                String uname = p.getUName().toLowerCase();//make it lower case to make it case insensitive
                if (uname.indexOf(rightType.toString()) > 0)
                {
                    int startPos = uname.indexOf(docNamespace);
                    if (startPos > 0)
                    {
                        if (uname.substring(startPos, uname.length()).equalsIgnoreCase(docNamespace))
                        {
                            hasNamespaceDefaultSet = true;
                            String values = p.getUValue();
                            if (StringUtils.isNotEmpty(values))
                            {
                                String[] tempRoles = values.split(",");
                                for (String s : tempRoles)
                                {
                                    if (!StringUtils.isEmpty(s))
                                    {
                                        roles.add(s.trim());
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // if there is no setting for this namespace, then use ALL
            if (!hasNamespaceDefaultSet && roles.isEmpty())
            {
                for (PropertiesVO p : lProps)
                {
                    String uname = p.getUName();
                    if (uname.indexOf(rightType.toString()) > 0)
                    {
                        if (uname.indexOf("ALL") > 0)
                        {
                            String values = p.getUValue();
                            if (StringUtils.isNotEmpty(values))
                            {
                                String[] tempRoles = p.getUValue().split(",");
                                for (String s : tempRoles)
                                {
                                    roles.add(s.trim());
                                }
                            }
                        }
                    }
                }
            }
        }

        return roles;
    }

    /**
     * prepares a list of wikidoc ids for a given model
     * 
     * @param collection
     * @return
     */
    public static Set<String> getWikiDocids(Collection<DocumentInfoModel> collection)
    {
        Set<String> ids = new HashSet<String>();

        if (collection != null && collection.size() > 0)
        {
            for (DocumentInfoModel model : collection)
            {
                String id = model.getSys_Id();
                String docName = model.getDocumentName();
                String namespace = model.getNamespace();
                String fullname = model.getFullname();

                // if there is no id, then find it
                if (StringUtils.isEmpty(id))
                {
                    try
                    {
                        if (!StringUtils.isEmpty(fullname))
                        {
                            id = find(fullname).getSys_id();
                        }
                        else if (!StringUtils.isEmpty(docName) && !StringUtils.isEmpty(namespace))
                        {
                            id = find(namespace, docName).getSys_id();
                        }
                    }
                    catch (Throwable t)
                    {
                        Log.log.error("Error while fetching for document " + fullname, t);
                    }
                }

                if (!StringUtils.isEmpty(id))
                {
                    ids.add(id);
                }
            }
        }

        return ids;

    }

    public static List<DocumentInfoModel> getWikiDocsForNamespace(List<DocumentInfoModel> namespaceOnly)
    {
        List<DocumentInfoModel> wikiDocs = new ArrayList<DocumentInfoModel>();

        for (DocumentInfoModel doc : namespaceOnly)
        {
            String namespace = doc.getNamespace();
            wikiDocs.addAll(getAllDocuments(namespace));

            // if it was ALL the namespace, then we are done collecting the data
            if (namespace.equalsIgnoreCase("ALL"))
            {
                break;
            }
        }

        return wikiDocs;
    }

    public static List<DocumentInfoModel> getAllDocuments(String oldNamespace)
    {
        List<DocumentInfoModel> wikiDocsToCopy = new ArrayList<DocumentInfoModel>();
        WikiDocument example = new WikiDocument();
        example.setUNamespace(oldNamespace);

        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
                List<WikiDocument> list = null;
                if (oldNamespace.equalsIgnoreCase("ALL"))
                {
                    list = dao.findAll();
                }
                else
                {
                    list = dao.find(example);
                }
                if (list != null && list.size() > 0)
                {
                    for (WikiDocument wikidoc : list)
                    {
                        DocumentInfoModel documentInfoModel = new DocumentInfoModel();
                        documentInfoModel.setFullname(wikidoc.getUFullname());
                        documentInfoModel.setNamespace(wikidoc.getUNamespace());
                        documentInfoModel.setDocumentName(wikidoc.getUName());
                        documentInfoModel.setSys_Id(wikidoc.getSys_id());
                        documentInfoModel.setLocked(wikidoc.getUIsLocked());
                        documentInfoModel.setDeleted(wikidoc.getUIsDeleted());
                        documentInfoModel.setHidden(wikidoc.getUIsHidden());

                        wikiDocsToCopy.add(documentInfoModel);
                    }
                }

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return wikiDocsToCopy;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<String> getAllDocumentFullNames(String whereClause)
    {
        List<String> list = new ArrayList<String>();
        String sql = "select UFullname from WikiDocument";
        if (StringUtils.isNotEmpty(whereClause))
        {
            sql = sql + " where " + whereClause;
        }

        sql = sql + " order by UPPER(UFullname) ";

        try
        {
        	String sqlFinal = sql;
          HibernateProxy.setCurrentUser("system");
        	list = (List<String>) HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sqlFinal);
                return q.list();
        	});

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return list;
    }

    public static ResolveActionTask findResolveActionTaskById(String actionTaskId)
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (ResolveActionTask) HibernateProxy.execute(() -> {
        		ResolveActionTask obj = null;

	            ResolveActionTaskDAO daoAT = HibernateUtil.getDAOFactory().getResolveActionTaskDAO();
	            obj = daoAT.findById(actionTaskId);
	            if (obj != null && obj.getResolveActionInvoc() != null)
	            {
	                if (obj.getResolveActionInvoc().getResolveActionInvocOptions() != null)
	                {
	                    obj.getResolveActionInvoc().getResolveActionInvocOptions().size();
	                }
	
	                if (obj.getResolveActionInvoc().getResolveActionParameters() != null)
	                {
	                    obj.getResolveActionInvoc().getResolveActionParameters().size();
	                }
	            }

	            return obj;
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return null;
    }

    public static ResolveActionTask findResolveActionTaskByName(String actionname, String namespace) throws WikiException
    {
        ResolveActionTask task = new ResolveActionTask();
        if (actionname.indexOf('#') > 0)// this is a fullname then
        {
            task.setUFullName(actionname);
        }
        else
        {
            task.setUName(actionname);
            task.setUNamespace(namespace);
        }

        try
        {
        	ResolveActionTask searchedTask = task;
          HibernateProxy.setCurrentUser("system");
        	task = (ResolveActionTask) HibernateProxy.execute(() -> {
	            // get the task object
            	return HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findFirst(searchedTask);

        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if (task == null)
        {
            throw new WikiException("Action Task with actionname <" + actionname + "> and namespace <" + namespace + "> does not exist.");
        }
        return task;
    }

    public static ResolveAssess findResolveAssessByName(String accessorName)
    {
        ResolveAssess model = new ResolveAssess();
        model.setUName(accessorName);

        try
        {
        	ResolveAssess searchedModel = model; 
          HibernateProxy.setCurrentUser("system");
        	model = (ResolveAssess) HibernateProxy.execute(() -> {
	            // get the task object
            	return HibernateUtil.getDAOFactory().getResolveAssessDAO().findFirst(searchedModel);
            });
            
            if (model != null && model.getResolveActionInvocs() != null)
            {
                // to load the the references
                model.getResolveActionInvocs().size();
            }

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return model;
    }

    public static ResolvePreprocess findResolvePreprocessByName(String name)
    {
        ResolvePreprocess model = new ResolvePreprocess();
        model.setUName(name);

        try
        {
        	ResolvePreprocess searchedModel = model; 
          HibernateProxy.setCurrentUser("system");
        	model = (ResolvePreprocess) HibernateProxy.execute(() -> {
		        // get the task object
        		return HibernateUtil.getDAOFactory().getResolvePreprocessDAO().findFirst(searchedModel);
        	});
        	
            if (model != null && model.getResolveActionInvocs() != null)
            {
                // to load the the references
                model.getResolveActionInvocs().size();
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return model;
    }

    public static ResolveParser findResolveParserByName(String name)
    {
        ResolveParser model = new ResolveParser();
        model.setUName(name);

        try
        {
        	ResolveParser searchedModel = model;
          HibernateProxy.setCurrentUser("system");
        	model = (ResolveParser) HibernateProxy.execute(() -> {
	            // get the task object
        		return HibernateUtil.getDAOFactory().getResolveParserDAO().findFirst(searchedModel);
        	});
        	
            if (model != null && model.getResolveActionInvocs() != null)
            {
                // to load the the references
                model.getResolveActionInvocs().size();
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return model;
    }

    public static MetaFormView findMetaFormView(String id)
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (MetaFormView) HibernateProxy.execute(() -> {
        		MetaFormView obj = null;

	            if (StringUtils.isNotEmpty(id))
	            {
	                MetaFormViewDAO daoMetaFormView = HibernateUtil.getDAOFactory().getMetaFormViewDAO();
	                obj = daoMetaFormView.findById(id);
	            }

	            return obj;
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return null;
    }

	public static MetaFormView findMetaFormViewByName(String formName) {
		try {
        HibernateProxy.setCurrentUser("system");
			HibernateProxy.execute(() -> {
				MetaFormView obj = null;

				if (StringUtils.isNotEmpty(formName)) {
					obj = new MetaFormView();
					obj.setUFormName(formName);

					MetaFormViewDAO daoMetaFormView = HibernateUtil.getDAOFactory().getMetaFormViewDAO();
					obj = daoMetaFormView.findFirst(obj);
				}
				return obj;
			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		return null;
	}

    /**
     * use to cleanup the data, update the references with null if the
     * references are deleted Specially in case where we are using the
     * GenericAPI and there are dangling references. For eg, if a Preprocessor
     * is deleted, the ActionInvoc still has a ref to it.
     * 
     * @param modelName
     *            - ResolveActionInvoc, ResolveActionTask, etc
     * @param attibuteName
     *            - attribute of the model
     * @param queryString
     *            - '123123', '13131' - values that needs to be updated with
     *            null
     */
    public static void updateDeletedRefWithNull(String modelName, String attibuteName, List<String> ids)
    {
        if (ids != null && ids.size() > 0)
        {
            String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
            String queryString = SQLUtils.prepareQueryString(ids, dbType);

            try
            {
                String updateSql = "update " + modelName + " set " + attibuteName + " = null where " + attibuteName + " IN (" + queryString + ") ";
                executeHibernateSQL(updateSql, "resolve.maint");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

    }

    /**
     * return list of attachment ids for a particular document
     * 
     * @param wikidocId
     * @return
     */
    public static List<String> getAllAttachmentsForWikidocument(String wikidocId)
    {
        List<String> list = new ArrayList<String>();

        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	 WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();

                 WikiDocument doc = dao.findById(wikidocId);
                 if (doc != null)
                 {
                     Collection<WikidocAttachmentRel> coll = doc.getWikidocAttachmentRels();
                     if (coll != null && coll.size() > 0)
                     {
                         String id = null;
                         for (WikidocAttachmentRel rel : coll)
                         {
                             id = rel.getWikiAttachment().getSys_id();
                             if (!StringUtils.isEmpty(id))
                             {
                                 list.add(id);
                             }
                         }
                     }
                 }
            });           
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return list;

    }

    @SuppressWarnings("unchecked")
	public static Map<String, String> getAllWikidocAttachments()
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (Map<String, String>) HibernateProxy.execute(() -> {
        		
        		Map<String, String> mapWikiDocAttId = new HashMap<String, String>();
                WikidocAttachmentRelDAO dao = HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO();
                List<WikidocAttachmentRel> list = dao.findAll();

                if (list != null && list.size() > 0)
                {
                    for (WikidocAttachmentRel rel : list)
                    {
                        mapWikiDocAttId.put(rel.getWikidoc().getSys_id(), rel.getWikiAttachment().getSys_id());
                    }
                }
                
                return mapWikiDocAttId;

        	});
        }
        catch (Exception e)
        {
            // Log.log.error("Error with qry : " + sql);
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return new HashMap<String, String>();

    }

    public static String getSystemProperty(String name)
    {
        String value = "";

        if (StringUtils.isNotEmpty(name))
        {
            try
            {
                PropertiesVO prop = ServiceHibernate.findPropertyByName(name);
                value = prop.getUValue();
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return value;

    }

    public static String saveAccessRights(AccessRights ar, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

            	AccessRightsDAO taskDao = HibernateUtil.getDAOFactory().getAccessRightsDAO();
            	taskDao.persist(ar);

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }

        return ar.getSys_id();
    }

    public static String saveResolveActionTask(ResolveActionTask model, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            ResolveActionTaskDAO taskDao = HibernateUtil.getDAOFactory().getResolveActionTaskDAO();
	            taskDao.persist(model);
	
	            // HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return model.getSys_id();
    }

    public static String saveResolveActionInvoc(ResolveActionInvoc model, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
	            ResolveActionInvocDAO dao = HibernateUtil.getDAOFactory().getResolveActionInvocDAO();
	            dao.persist(model);

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return model.getSys_id();
    }// saveResolveActionInvoc

    public static void saveResolveActionParameters(Collection<ResolveActionParameter> models, String username) throws WikiException
    {
        if (models != null)
        {
            for (ResolveActionParameter model : models)
            {
                saveResolveActionParameter(model, username);
            }
        }
    }// saveResolveActionParameters

    public static String saveResolveActionParameter(ResolveActionParameter model, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            ResolveActionParameterDAO dao = HibernateUtil.getDAOFactory().getResolveActionParameterDAO();
	            dao.persist(model);
	
	            // HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return model.getSys_id();
    }// saveResolveActionParameter

    public static void saveResolveActionInvocOptions(Collection<ResolveActionInvocOptions> models, String username) throws WikiException
    {
        if (models != null)
        {
            for (ResolveActionInvocOptions model : models)
            {
                saveResolveActionInvocOption(model, username);
            }
        }
    }// saveResolveActionInvocOptions

    public static String saveResolveActionInvocOption(ResolveActionInvocOptions model, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            ResolveActionInvocOptionsDAO dao = HibernateUtil.getDAOFactory().getResolveActionInvocOptionsDAO();
	            dao.persist(model);
	
	            // HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return model.getSys_id();
    }

    public static String saveResolveAssess(ResolveAssess model, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            ResolveAssessDAO dao = HibernateUtil.getDAOFactory().getResolveAssessDAO();
	            dao.persist(model);
	
	            // HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return model.getSys_id();
    }// saveResolveAssess

    public static String saveResolvePreprocess(ResolvePreprocess model, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            ResolvePreprocessDAO dao = HibernateUtil.getDAOFactory().getResolvePreprocessDAO();
	            dao.persist(model);
	
	            // HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return model.getSys_id();
    }// saveResolvePreprocess

    public static String saveResolveParser(ResolveParser model, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            ResolveParserDAO dao = HibernateUtil.getDAOFactory().getResolveParserDAO();
	            dao.persist(model);
	
	            // HibernateUtil.getCurrentSession().flush();
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return model.getSys_id();
    }// saveResolveParser

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<AccessRights> getAccessRightsFor(String resourceId, String resourceName, String resourceType)
    {
        List<AccessRights> listAccessRights = new ArrayList<AccessRights>();
        String sql = " from AccessRights where UResourceType = :UResourceType and UResourceName= :UResourceName and UResourceId = :UResourceId";

        try
        {
          HibernateProxy.setCurrentUser("system");
        	listAccessRights = (List<AccessRights>) HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sql);
                q.setParameter("UResourceType", resourceType, StringType.INSTANCE);
                q.setParameter("UResourceName", resourceName, StringType.INSTANCE);
                q.setParameter("UResourceId", resourceId, StringType.INSTANCE);
                return q.list();
        	});           

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return listAccessRights;
    }// getAccessRightsFor

    public static ResolveActionParameter findResolveActionParameterById(String id)
    {
        ResolveActionParameter result = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (ResolveActionParameter) HibernateProxy.execute(() -> {

            // get the task object
            return HibernateUtil.getDAOFactory().getResolveActionParameterDAO().findById(id);

        	});
        }
        catch (Throwable t)
        {
            Log.log.error("Error finding ResolveActionParameter sys_id :" + id, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return result;
    } // findResolveActionTaskByName

    public static ResolveActionInvocOptions findResolveActionInvocOptionsById(String id)
    {
        ResolveActionInvocOptions result = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (ResolveActionInvocOptions) HibernateProxy.execute(() -> {

	            // get the task object
        		return HibernateUtil.getDAOFactory().getResolveActionInvocOptionsDAO().findById(id);

            });
        }
        catch (Throwable t)
        {
            Log.log.error("Error finding ResolveActionInvocOptions sys_id :" + id, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return result;
    } // findResolveActionInvocOptionsById
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResolveActionInvocOptions findResolveActionInvocOptionsByIdAndUName(String id, String UName)
    {
        ResolveActionInvocOptions result = null;
        List<ResolveActionInvocOptions> list = null;
        try
        {
            String hql = "from ResolveActionInvocOptions where invocation = '" + id + "' and UName = '" + UName + "'";
            
            
          HibernateProxy.setCurrentUser("system");
            list = (List<ResolveActionInvocOptions>) HibernateProxy.execute(() -> {

            	Query query = HibernateUtil.createQuery(hql);
            	return query.list();

            });
        }
        catch (Throwable t)
        {
            Log.log.error("Error finding ResolveActionInvocOptions sys_id :" + id, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        if (list != null && list.size() > 0)
        {
            result = list.get(0);
        }

        return result;
    } // findResolveActionInvocOptionsByIdAndUName
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResolveActionInvocOptions findInvocOptionByActionInvocId(String actionInvocId)
    {
        ResolveActionInvocOptions result = null;
        
        if (StringUtils.isNotBlank(actionInvocId))
        {
            String hql = "from ResolveActionInvocOptions where invocation = '" + actionInvocId + "'";
            List<ResolveActionInvocOptions> list = null;
            try
            {
              HibernateProxy.setCurrentUser("system");
                list = (List<ResolveActionInvocOptions>) HibernateProxy.execute(() -> {
	                Query query = HibernateUtil.createQuery(hql);
	                return query.list();
                });
            }
            catch (Throwable t)
            {
                Log.log.error("Error finding ResolveActionInvocOptions by actionInvocId " + actionInvocId, t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
            
            if (list != null && list.size() > 0)
            {
                result = list.get(0);
            }
        }
        
        return result;
    }

    public static ResolveAssess findResolveAssessById(String id)
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
            return (ResolveAssess) HibernateProxy.execute(() -> {
            	ResolveAssess result = null;

	            // get the task object
	            result = HibernateUtil.getDAOFactory().getResolveAssessDAO().findById(id);
	            if (result != null && result.getResolveActionInvocs() != null)
	            {
	                // to load the the references
	                result.getResolveActionInvocs().size();
	            }
	            
	            return result;
            });
        }
        catch (Throwable t)
        {
            Log.log.error("Error finding ResolveAssess Sys_Id :" + id, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return null;
    } // findResolveAssessById

    public static ResolvePreprocess findResolvePreprocessById(String id)
    {
        ResolvePreprocess result = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (ResolvePreprocess) HibernateProxy.execute(() -> {
        		// get the task object
        		return HibernateUtil.getDAOFactory().getResolvePreprocessDAO().findById(id);
        	});
        	
            if (result != null && result.getResolveActionInvocs() != null)
            {
                // to load the the references
                result.getResolveActionInvocs().size();
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error finding ResolveAssess Sys_Id :" + id, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return result;
    } // findResolvePreprocessById

    public static ResolveParser findResolveParserById(String id)
    {
        ResolveParser result = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (ResolveParser) HibernateProxy.execute(() -> {
	            // get the task object
        		return HibernateUtil.getDAOFactory().getResolveParserDAO().findById(id);
        	});
        	
            if (result != null && result.getResolveActionInvocs() != null)
            {
                // to load the the references
                result.getResolveActionInvocs().size();
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error finding ResolveAssess Sys_Id :" + id, t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return result;
    } // findResolveParserById

    @SuppressWarnings("unchecked")
    public static ResolveActionTask getResolveActionTaskForInvoc(String invocId)
    {
        ResolveActionTask task = null;

        if (StringUtils.isNotEmpty(invocId))
        {
            try
            {
                String sql = "from ResolveActionTask where resolveActionInvoc = '" + invocId + "'";

              HibernateProxy.setCurrentUser("system");
                task = (ResolveActionTask) HibernateProxy.execute(() -> {
                    // get the task object
                    List<ResolveActionTask> list = HibernateUtil.createQuery(sql).list();
                    if (list != null && list.size() > 0)
                    {
                        return list.get(0);
                    }
                    return null;
                });
            }
            catch (Throwable t)
            {
                Log.log.error("invocSys_Id: " + invocId + " " + t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return task;
    }

    @SuppressWarnings("unchecked")
    public static void updateResolveActionTaskWithInvocation()
    {        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.executeNoCache(() -> {

	            // update u_invocation in actiontask
	            String sql = "select sys_id, UTask from ResolveActionInvoc where UTask is not null";
	            List<Object> objs = HibernateUtil.getCurrentSession().createQuery(sql).list();
	            if (objs != null)
	            {
	                for (Object obj : objs)
	                {
	                    Object[] row = (Object[]) obj;
	
	                    String sys_id = (String) row[0];
	                    String task_id = (String) row[1];
	
	                    ResolveActionTask task = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(task_id);
	                    if (task != null)
	                    {
	                        task.setResolveActionInvoc(new ResolveActionInvoc().usetsys_id(sys_id));
	                    }
	                }
	                
	                sql = "update ResolveActionInvoc set UTask = null";
	                HibernateUtil.getCurrentSession().createQuery(sql).executeUpdate();
	            }

        	});
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
    }

    public static void updateDefaultAssignedUserToResolveActionTask()
    {        
        try
        {
            UsersVO userAdmin = UserUtils.getUser("admin");            

            // update u_invocation in actiontask
            if (userAdmin != null)
            {
                String sql = "update ResolveActionTask set assignedTo = '" + userAdmin.getSys_id() + "' where assignedTo is null";
              HibernateProxy.setCurrentUser("system");
                HibernateProxy.execute(() -> HibernateUtil.getCurrentSession().createQuery(sql).executeUpdate());
            }            
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }        
    }

    public static Map<String, String> getDefaultRolesForActionTask(String namespace, String name)
    {
        //first try the namespace
        List<String> propNames = new ArrayList<String>();
        propNames.add("actiontask.rights.view." + namespace);
        propNames.add("actiontask.rights.edit." + namespace);
        propNames.add("actiontask.rights.admin." + namespace);
        propNames.add("actiontask.rights.execute." + namespace);
        propNames.add("actiontask.rights.view.ALL");
        propNames.add("actiontask.rights.edit.ALL");
        propNames.add("actiontask.rights.admin.ALL");
        propNames.add("actiontask.rights.execute.ALL");

        Map<String, String> properties = PropertiesUtil.getProperties(propNames);

        Map<String, String> result = new HashMap<String, String>();
        populateRole(ConstantValues.READ_ROLES_KEY, "actiontask.rights.view." + namespace, "actiontask.rights.view.ALL", properties, result);
        populateRole(ConstantValues.WRITE_ROLES_KEY, "actiontask.rights.edit." + namespace, "actiontask.rights.edit.ALL", properties, result);
        populateRole(ConstantValues.ADMIN_ROLES_KEY, "actiontask.rights.admin." + namespace, "actiontask.rights.admin.ALL", properties, result);
        populateRole(ConstantValues.EXECUTE_ROLES_KEY, "actiontask.rights.execute." + namespace, "actiontask.rights.execute.ALL", properties, result);

        return result;
    }//getDefaultRolesForActionTask

    private static void populateRole(String roleType, String namespaceRole, String defaultRole, Map<String, String> roles, Map<String, String> selectedRoles)
    {
        String value = roles.get(namespaceRole);
        if (StringUtils.isEmpty(value))
        {
            value = roles.get(defaultRole);
        }

        selectedRoles.put(roleType, value);
    }
    
    @Deprecated
    public static String getProblemId(String type, String value)
    {
        return getProblemId(type, value, null, null, "system");
    }
    
    public static String getProblemId(String type, String value, String orgId, String orgName, String username)
    {
        if (StringUtils.isNotBlank(type))
        {
            try
            {
            	String result = null;
            	final ArchiveWorksheet archive = new ArchiveWorksheet();
                
                if (type.equals(Constants.HTTP_REQUEST_NUMBER))
                {
                    // try worksheet
                    Worksheet worksheet = WorksheetUtil.findWorksheet(null, value, null, null, null, orgId, orgName, username);

                    if (worksheet != null)
                    {
                        result = worksheet.getSysId();
                    }
                    else
                    {
                        archive.setUNumber(value);
                    }
                }
                else if (type.equals(Constants.HTTP_REQUEST_REFERENCE))
                {
                    result = WorksheetUtil.findWorksheetSysId(null, null, value, null, null, orgId, orgName, username);

                    if (StringUtils.isBlank(result))
                    {
                        archive.setUReference(value);
                    }
                }
                else if (type.equals(Constants.HTTP_REQUEST_ALERTID))
                {
                    result = WorksheetUtil.findWorksheetSysId(null, null, null, value, null,  orgId, orgName, username);

                    if (StringUtils.isBlank(result))
                    {
                        archive.setUAlertId(value);
                    }
                }
                else if (type.equals(Constants.HTTP_REQUEST_CORRELATIONID))
                {
                    result = WorksheetUtil.findWorksheetSysId(null, null, null, null, value,  orgId, orgName, username);

                    if (StringUtils.isBlank(result))
                    {
                        archive.setUCorrelationId(value);
                    }
                }
                
                if (StringUtils.isBlank(result)) {
                    HibernateProxy.setCurrentUser(username);
                    result =  (String) HibernateProxy.execute(() -> {
                        String id = null;
                        ArchiveWorksheet aws = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO().findFirst(archive);
                        if (aws != null) {
                            id = aws.getSys_id();
                        }
                        return id;
                    });
                }
                
                return result;
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return null;
        
    }

    public static void updateLastReviewedDateForWikiDocs(List<String> docNames, String browserTimezone, 
    		String username) throws Exception
    {
        if (StringUtils.isEmpty(username))
        {
            username = "system";
        }

        if (docNames != null && docNames.size() > 0)
        {
            Iterator<String> it = docNames.iterator();
            while (it.hasNext())
            {
                String docFullName = it.next();
                updateLastReviewedDateForWikiDoc(docFullName, browserTimezone, username);
            }
        }
    }

    @SuppressWarnings("rawtypes")
    public static void updateLastReviewedDateForNamespace(String namespace, String browserTimezone, String username) throws Exception
    {
        if (StringUtils.isNotEmpty(namespace))
        {
            String sql = "update WikiDocument set ULastReviewedOn = :ULastReviewedOn , " + " UReqestSubmissionOn = :UReqestSubmissionOn ," + " UIsRequestSubmission = :UIsRequestSubmission " + " where UNamespace = :UNamespace";

            try
            {
                Date date = DateUtils.getCurrentDateInGMT(browserTimezone, true);

              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                Query query = HibernateUtil.createQuery(sql);
	                query.setParameter("ULastReviewedOn", date);
	                query.setParameter("UReqestSubmissionOn", null, TimestampType.INSTANCE);
	                query.setParameter("UIsRequestSubmission", Boolean.FALSE);
	                query.setParameter("UNamespace", namespace);
	                query.executeUpdate();

                });
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e);
            }
        }
    }

    private static void updateLastReviewedDateForWikiDoc(String docFullName, String browserTimeZone, String username) throws Exception
    {
        if (StringUtils.isNotEmpty(docFullName))
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	            WikiDocument doc = new WikiDocument();
	            doc.setUFullname(docFullName);
	            try
	            {
                

	                doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findFirst(doc);
	                if (doc != null)
	                {
	                    doc.setULastReviewedOn(DateUtils.getCurrentDateInGMT(browserTimeZone, true));
	                    doc.setUReqestSubmissionOn(null);
	                    doc.setUIsRequestSubmission(false);
	
	                    HibernateUtil.getDAOFactory().getWikiDocumentDAO().persist(doc);
	                }
	                
	            }
	            catch (Throwable e)
	            {
	                Log.log.error(e.getMessage(), e);                
	                throw new Exception(e);
	            }
        	});
        }
    }
}
