/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.store;

import java.util.List;

import com.resolve.dto.AttachmentModel;
import com.resolve.dto.RightType;
import com.resolve.persistence.model.ResolveWikiLookup;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.exception.WikiException;
import com.resolve.wiki.web.vo.DocumentInfoVO;

/**
 * An interface to provide the domain objects to SAVE, UPDATE, DELETE,LOAD, SEARCH and all the DB functions.
 * 
 * @author jeet.marwah
 * 
 */
public interface WikiStore
{
	public List<WikiAttachment> getAllAttachmentsFor(String wikiDocId, String wikiDocName);
	
	public void deleteAttachments(String UParentWikidocId, String listOfIds);
	
	public String getAccessRightsFor(String resourceType, String resourceName, String resourceId, RightType rightType);
	
	public List<WikiDocument> findWikiDocumentByHQL(String hql)throws WikiException ;
	
	public WikiAttachment findAttachment(String attachmentName) throws WikiException;

	public WikiAttachment getAttachmentFor(String wikidocFullName, String fileName);
	
	public WikiDocument find(String docFullName) throws WikiException ;
	
	public WikiAttachment findWikiAttachmentFor(String docName, String fileName);
	
	public WikiDocument find(String namespace, String filename) throws WikiException ;

	public List<DocumentInfoVO> getListOfDocumentsFor(String namespace);
	
	public void addAttachment(WikiDocument wikidoc, WikiAttachment attachment, boolean isImportExport) throws WikiException;
	public void updateAttachment (WikiAttachment attachment);
	public void renameAttachment (WikiAttachment attachment);
	public void globalAttach(WikiDocument attachToWikidoc, List<AttachmentModel> listOfAttachment)throws WikiException;
	public List<ResolveWikiLookup> getWikiLookup()throws WikiException;
}