/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.cache;

//import org.jboss.cache.pojo.PojoCache;
//import org.jboss.cache.pojo.PojoCacheFactory;

import com.resolve.rsview.main.RSContext;

/**
 * This singleton provide caching APIs
 * 
 * @author jeet.marwah
 * 
 */
public class JBossPOJOCacheService
{
	private static JBossPOJOCacheService instance;
	//private static PojoCache cache = null;

	//get the file locations
	private final static String JBOSS_POJO_CONFIG_FILE = RSContext.getWebHome() + "WEB-INF/cache/replSync-service.xml";
	private final static String RESOLVE_NODE = "resolve/";

	//only 1 instance of this should be available
	private JBossPOJOCacheService()
	{
		start();
	}

	public static JBossPOJOCacheService getInstance()
	{
		if (instance == null)
		{
			synchronized (JBossPOJOCacheService.class)
			{
				if (instance == null)
				{
					instance = new JBossPOJOCacheService();
				}
			}
		}

		return instance;
	}

	/**
	 * This method starts the Caching service
	 */
	private void start()
	{
	    /*
		if (cache == null)
		{
			cache = PojoCacheFactory.createCache(JBOSS_POJO_CONFIG_FILE, true);//start the cache automatically
		}
		*/
	}

	/**
	 * This utility method is to attach a Object to caching
	 * 
	 * @param key
	 * @param value
	 */
	public synchronized void attach(String key, Object value)
	{
		//cache.attach(RESOLVE_NODE + key, value);
	}

	/**
	 * This utility method is to detach an object from the caching
	 * 
	 * @param key
	 */
	public synchronized void detach(String key)
	{
//		cache.detach(RESOLVE_NODE + key);
	}

	/**
	 * This method returns the ref of the object mapped to the key
	 * 
	 * @param key
	 * @return
	 */
	public Object getObject(String key)
	{
//		return cache.find(RESOLVE_NODE + key);
	    return null;
	}

	/**
	 * This method stops the caching service
	 */
	public void stop()
	{
//		cache.stop();
	}

}//end of class
