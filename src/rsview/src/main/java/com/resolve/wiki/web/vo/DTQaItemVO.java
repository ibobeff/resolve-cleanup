/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.web.vo;

/**
 * VO used for the new DT Viewer
 * 
 * @author jeet.marwah
 *
 */
public class DTQaItemVO
{
    private String question;
    private String value;
    private String id;
    private long durationInMs;
    
    public DTQaItemVO() {}
        
    public String getQuestion()
    {
        return question;
    }
    public void setQuestion(String question)
    {
        this.question = question;
    }
    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public long getDurationInMs()
    {
        return durationInMs;
    }

    public void setDurationInMs(long durationInMs)
    {
        this.durationInMs = durationInMs;
    }
    
    

}
