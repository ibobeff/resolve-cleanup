/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.resolve.rsbase.MainBase;
import com.resolve.dto.AttachmentModel;
import com.resolve.dto.SectionModel;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.SystemStats;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.dto.AttachmentDTO;
import com.resolve.wiki.web.WikiRequestContext;
import com.resolve.wiki.web.vo.DocumentInfoVO;

/**
 * This class file encapsulates the Wiki and provides accessor methods that can be used in the Velocity template
 * 
 * @author jeet.marwah
 * 
 */
public class WikiAPI extends Api
{
    private Wiki wiki;
    private static final String NOT_SUPPORTED_MSG = "NOT SUPPORTED IN RESOLVE 3.5";


    public WikiAPI(Wiki wiki, WikiRequestContext context)
    {
        super(context);
        this.wiki = wiki;
    }

    
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // API for the user 
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public WikiDocumentVO getDocument(String wikiFullName)  throws WikiException
    {
        WikiDocumentVO doc = null;
        
        try
        {
            String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            doc = ServiceWiki.getWikiDoc(null, wikiFullName, username);
        }
        catch (Exception e)
        {
           Log.log.error("Error in getting the document " + wikiFullName, e);
        }
        
        return doc;
    }//getDocument
    
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // methods defined in the macros.vm file 
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public String includeForm(String topic, WikiRequestContext wikiRequestContext) throws WikiException
    {
        try
        {
            return wiki.includeForm(topic, wikiRequestContext, true, null);
        }
        catch(WikiException e)
        {
            return e.getGUIMessage();
        }
    }//includeForm


    public String includeForm(String topic, WikiRequestContext wikiRequestContext, boolean pre, String revisionNo) throws WikiException
    {
        return wiki.includeForm(topic, wikiRequestContext, pre, revisionNo);
    }//includeForm

    public String includeForm(String topic, String sectionname, WikiRequestContext wikiRequestContext, boolean pre, String revisionNo) throws WikiException
    {
        return wiki.includeForm(topic, sectionname, wikiRequestContext, pre, revisionNo);
    }//includeForm

    
    
    //this is to get the list of sections of the current document
    public List<SectionModel> getSections(WikiRequestContext wikiContext)  throws WikiException
    {
        List<SectionModel> list = wiki.getSections(wikiContext).listOfSections;
        if(list == null)
        {
            list = new ArrayList<SectionModel>();
        }
        
        return list;
        
    }//getSections
    
    //get sections for a specific document
    public List<SectionModel> getAllSections(String docFullName)  throws WikiException
    {
        String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        List<SectionModel> list = wiki.getAllSections(docFullName, username);
        if(list == null)
        {
            list = new ArrayList<SectionModel>();
        }
        
        return list;
    }//getAllSections
    
    //get the content of a specific section
    public String getSectionContent(String docFullName, String sectionName) throws WikiException 
    {
        String result = "";
        String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        if(StringUtils.isNotEmpty(sectionName) && StringUtils.isNotEmpty(docFullName))
        {
            result = wiki.getSectionContent(docFullName, sectionName, username);
        }
        
        return result;
    }//getSectionContent
    
    public boolean isWikidocExist(String docFullName) 
    {
        boolean result = ServiceWiki.isWikidocExist(docFullName, true);// wiki.isWikidocExist(docFullName);
        return result;
    }//isSectionExist
    
    public boolean isSectionExist(String docFullName, String sectionName) 
    {
        String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        boolean result = wiki.isSectionExist(docFullName, sectionName, username);
        return result;
    }//isSectionExist

    public List<DocumentInfoVO> getListOfDocumentsForTag(String tagname)
    {
        return wiki.getListOfDocumentsForTag(tagname, context);
    }//getListOfDocumentsForTag

    public List<DocumentInfoVO> getListOfDocumentsForTag(String tagname, String namespace)
    {
        return wiki.getListOfDocumentsForTag(tagname, namespace, context);
    }//getListOfDocumentsForTag
    
    public List<DocumentInfoVO> getListOfDocumentsForNamespace(String namespace) throws Exception
    {
        return wiki.getListOfDocumentsForNamespace(namespace, context);
    }//getListOfDocumentsForNamespace

    /**
     * 
     * @param copyFromDocname
     *            - Doc name to copy
     * @param copyToDocname
     *            - will be copied to this name
     * @param reset
     *            - THIS IS NOT FUNCTIONAL AND NOT A REQUIREMENT, Kept for backward compatibility
     * @param force
     *            - if this is set to true, then it will delete the document with name 'copyToDocname' if it exist, if this value is 'false' it will not copy if it exists
     * @return
     * @throws WikiException
     */
    public boolean copyDocument(String copyFromDocname, String copyToDocname, boolean reset, boolean force) throws WikiException
    {
        if (copyFromDocname == null || copyFromDocname.trim().length() == 0 || copyFromDocname.indexOf('.') == -1 || copyToDocname == null
                || copyToDocname.trim().length() == 0 || copyToDocname.indexOf('.') == -1)
        {
            return false;
        }
        else
        {
            boolean result = false;
            String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            try
            {
                ServiceWiki.copyDocument(copyFromDocname, copyToDocname, force, true, username);
                result = true;
            }
            catch (Exception e)
            {
                Log.log.error("error copying " + copyFromDocname + " to " + copyToDocname, e);
            }
            
            return result;
        }
    } // copyDocument

    //TODO: Note, we have remove the AttachmentModel and use the DTO. This is just temporary as this api is exposed to the client and also used in macros
    public List<AttachmentModel> getListOfAttachments(String docFullName)  throws WikiException
    {
        List<AttachmentModel> result = new ArrayList<AttachmentModel>();
        try
        {
            String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            List<AttachmentDTO> attachments = ServiceWiki.getAllAttachmentsFor(null, docFullName, username);
            if(attachments != null && attachments.size() > 0)
            {
                for(AttachmentDTO att : attachments)
                {
                    AttachmentModel model = new AttachmentModel();
                    model.setSys_id(att.getSys_id());
                    model.setFilename(att.getFileName());
                    model.setCreatedBy(att.getSys_created_by());
                    model.setCreatedOn(att.getSys_created_on());
                    model.setSize(att.getSize());
                    
                    result.add(model);
                    
                }
            }
        }
        catch(Exception e)
        {
            Log.log.error("Error getting the attachments for document " + docFullName, e);
        }
        
        
        return result;
    }//getListOfAttachments
    
    /**
     * returns the count of how many times the current document is viewed
     * 
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentViewCount() throws WikiException
    {
        return getWikiDocumentViewCount(null);
    }//getWikiDocumentViewCount
    
    /**
     * returns the count of how many times a particular document is viewed
     * 
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentViewCount(String docFullName) throws WikiException
    {
        return wiki.getWikiDocumentViewCount(context, docFullName);
    }//getWikiDocumentViewCount
    
    
    /**
     * returns the count of how many times the current document is edited
     * 
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentEditCount() throws WikiException
    {
        return getWikiDocumentEditCount(null);
    }//getWikiDocumentEditCount
    
    /**
     * returns the count of how many times a particular document is edited
     * 
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentEditCount(String docFullName) throws WikiException
    {
        return wiki.getWikiDocumentEditCount(context, docFullName);
    }//getWikiDocumentEditCount
    
    /**
     * returns the count of how many times the current document is edited
     * 
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentExecuteCount() throws WikiException
    {
        return getWikiDocumentExecuteCount(null);
    }//getWikiDocumentExecuteCount
    
    /**
     * returns the count of how many times a particular document is edited
     * 
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public long getWikiDocumentExecuteCount(String docFullName) throws WikiException
    {
        return wiki.getWikiDocumentExecuteCount(context, docFullName);
    }//getWikiDocumentExecuteCount


    /**
     * used to initialize the rating
     * 
     * @param docFullName
     * @param total
     * @param count
     * @throws WikiException
     */
    public void initRating(String docFullName, int total, int count) throws WikiException
    {
        String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            ServiceWiki.initRating(null, docFullName, total, count, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error in initRating from ESB: docFullName :" + docFullName + " , total:" + total + ", count:" + count, e);
        }
    }
    
    
    /**
     * 
     * @param docFullName
     * @param rating  - 1-5 range
     * @param multiplier = +ve #
     * @throws WikiException
     */
    public void incrementRating(String docFullName, int rating, int multiplier) throws WikiException
    {
        String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            ServiceWiki.incrementRating(null, docFullName, rating, multiplier, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error doing incrementRating for document " + docFullName,e);
        }
        
    }
    

    /**
     * returns Key-value pair of the row data for a specific table having a id as parameter
     * the Key will the column name and value is the data value of the row.
     * 
     * 
     * @param tableName
     * @param id
     * @return
     * @throws WikiException
     */
    public Map<String, Object> getRowData(String tableName, String id) throws WikiException
    {
        Map<String, Object> result = null;
        
        if (StringUtils.isNotEmpty(tableName) && StringUtils.isNotEmpty(id))
        {
            try
            {
                result = ServiceHibernate.findById(tableName, id);
            }
            catch (Throwable t)
            {
                Log.log.error("Error in getting data for table " + tableName + " having id as " + id, t);
            }
        }// end of if
        
        return result;//wiki.getRowData(tableName, id, context);
    }//getRowData

    public String getDatabaseType()
    {
        return ((Main) MainBase.main).getConfigSQL().getDbtype();
    }
    /**
     * For backward compatibility - not implemented and always return true
     * 
     * @param level
     * @param user
     * @param docname
     * @return
     */
    @Deprecated
    public boolean hasAccessLevel(String level, String user, String docname)
    {
        return true;
    }//hasAccessLevel
    
    public ActionPluginApi getAction()
    {
        return new ActionPluginApi(wiki, context);
    }//getAction
    
    public String getVersion()
    {
        return MainBase.main.release.version;
    } // getVersion
    
    public List<WikiDocumentAPI> searchDocuments(String wheresql) throws WikiException 
    {
        List<WikiDocumentAPI> list = wiki.searchDocumentsNames(wheresql, context);
        return list;
    }//searchDocuments
    
    //its in the UI now
//    public List<DocumentInfoModel> getRecentlyVisited()
//    {
//        return wiki.getRecentlyVisited(context);
//    }
    
    public void executeRunbook(String fullname, String params)
    {
        wiki.executeRunbook(fullname, params, context);
    }//executeRunbook
    
    public String formatDate(Date date)
    {
        String result = "";
        if (date != null)
        {
            result = ConstantValues.fullDateFormatter.format(date);
        }
        return result;
    } //formatDate
    
    public String getAddress(HttpServletRequest request)
    {
        return SystemStats.getAddress(request);
    } // getAddress
    
    public String getOSArch()
    {
        return SystemStats.getOSArch();
    } // getArch
    
    public String getOSName()
    {
        return SystemStats.getOSName();
    } // getOSName
    
    public String getOSVersion()
    {
        return SystemStats.getOSVersion();
    } // getOSVersion
    
    public int getOSProcessors()
    {
        return SystemStats.getOSProcessors();
    } // getOSProcessors
    
    public String getJVMVendor()
    {
        return SystemStats.getJVMVendor();
    } // getJVMVendor
    
    public String getJVMName()
    {
        return SystemStats.getJVMName();
    } // getJVMName
    
    public String getJVMVersion()
    {
        return SystemStats.getJVMVersion();
    } // getJVMVersion
    
    public String getMemoryMax()
    {
        return SystemStats.getMemoryMax();
    } // getMemoryMax
    
    public String getMemoryTotal()
    {
        return SystemStats.getMemoryTotal();
    } // getMemoryTotal
    
    public String getMemoryFree()
    {
        return SystemStats.getMemoryFree();
    } // getMemoryFree
    
    public String getMemoryFreePercentage()
    {
        return SystemStats.getMemoryFreePercentage();
    } // getMemoryFreePercentage
    
    public String getHeapMax()
    {
        return SystemStats.getHeapMax();
    } // getHeapMax
    
    public String getHeapTotal()
    {
        return SystemStats.getHeapTotal();
    } // getHeapTotal
    
    public String getHeapFree()
    {
        return SystemStats.getHeapFree();
    } // getHeapFree
    
    public String getHeapFreePercentage()
    {
        return SystemStats.getHeapFreePercentage();
    } // getHeapFreePercentage
    
    public int getThreadPeak()
    {
        return SystemStats.getThreadPeak();
    } // getThreadPeak
    
    public int getThreadLive()
    {
        return SystemStats.getThreadLive();
    } // getThreadLive
    
    public int getThreadDaemon()
    {
        return SystemStats.getThreadDaemon();
    } // getThreadDaemon
    
    public String getSessionTable()
    {
        return Authentication.getSessionTable(true);
    } // getSessionTable
    
    public String getSessionTableNoCheckbox()
    {
        return Authentication.getSessionTable(false);
    } // getSessionTableNoCheckbox
    
    public long getSessionActive()
    {
        return Authentication.getSessionActive();
    } // getSessionActive
    
    public long getSessionMaxActive()
    {
        return Authentication.getSessionMaxActive();
    } // getSessionMaxActive
    
    public String getProperty(String name)
    {
        String value = "";
        try
        {
            String username = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            ResolvePropertiesVO prop = ServiceHibernate.findResolvePropertiesById(null, null, name, username);
            if(prop != null)
            {
                value = prop.getUValue();
                if(StringUtils.isNotBlank(value) && prop.getUType().equalsIgnoreCase("Encrypt"))
                {
                    value = CryptUtils.decrypt(value);
                }
            }
        }
        catch (Exception e)
        {
           Log.log.error("Error getting property " + name,e);
        }
        
        return value;
        
    } // getProperty
    
    public String getSystemProperty(String name)
    {
        //return wiki.getSystemProperty(name);
        return ServiceHibernate.getPropertyString(name); //PropertiesUtil.getPropertyString(name);
    } // getSystemProperty
    
    public String evaluateViewLookup(String lookupString)
    {
        String username = context.getWikiUser() != null ? context.getWikiUser().getUsername() : (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String result = "";
        
        try
        {
            //example --> ${view_lookup:CR}
            result = ServiceHibernate.evaluateViewLookup(lookupString, username);//StoreUtility.evaluateViewLookup(lookupString, username);
        }
        catch (Exception e)
        {
           Log.log.error("error in evaluateViewLookup for " + lookupString, e);
        } 
            
        return result;
    }
    
    public String getLicenseInfo()
    {
        Map<String, String> licenseInfo = MainBase.main.getLicenseService().getLicenseInfo();
        String result = "{table}\n*Property*|*Value*";
        for(Map.Entry<String,String> entry : licenseInfo.entrySet())
        {
            result += "\n*" + entry.getKey() + "*|" + entry.getValue();
        }
        result += "\n{table}";
        return result;
    }
    
    public String getWikiDocumentFullName(WikiRequestContext wikiRequestContext) {
        return this.wiki.getDocumentName(wikiRequestContext);
    }
    
    /////////////////////////////////////////////////////////       LIST OF APIS NOT SUPPORTED ANYMORE ///////////////////////////////////////////////////////////
    @Deprecated
    public String includeTemplate(String topic, WikiRequestContext wikiRequestContext) throws WikiException
    {
        return NOT_SUPPORTED_MSG;
    }//includeTemplate

    //this is for getting the TOC for a document - called by macros.vm 
    @Deprecated
    public Map getTOC(String docFullName, int init, int max, boolean numbered) throws WikiException
    {
        return new HashMap();//wiki.getTOC(docFullName, init, max, numbered, context);
    }//getTOC
    

    //called by the resolve_toolbar.vm, resolve_history.vm file
    @Deprecated
    public Map<String, Object> getDataForToolbar(String documentName, WikiRequestContext wikiRequestContext) throws WikiException
    {
        return new HashMap<String, Object>();//wiki.getDataForToolbar(documentName, wikiRequestContext);
    }//getDataForToolbar

    /**
     * called by the resolve_pageinfo.vm
     * 
     * @deprecated - new PageInfo for 3.5 is available
     * 
     * @param documentName
     *            - page info for this doc. If this is NULL, then use the control Id to get the current doc and show that info
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    @Deprecated
    public Map<String, Object> getDataForPageInfo(String documentName, WikiRequestContext wikiRequestContext) throws WikiException
    {
        return new HashMap<String, Object>();//wiki.getDataForPageInfo(documentName, wikiRequestContext);
    }//getDataForPageInfo

    /**
     * This is called from the macro.vm file for the following syntax
     * 
     * #macro( template $tname ) $wiki.parseTemplate($tname, $wikiUser) #end
     * 
     * @param docFullName
     * @param templateName
     * @param wikiRequestContext
     * @return
     * @throws WikiException
     */
    @Deprecated
    public String parseTemplate(String docFullName, String templateName, WikiRequestContext wikiRequestContext) throws WikiException
    {
        return NOT_SUPPORTED_MSG;
    }//parseTemplate

    /**
     * this is a macro and defined in the macros.vm file. It is used on the Homepage to list all the docs that this user can look at
     * 
     * @param wikiRequestContext
     * @return
     */
    public List<DocumentInfoVO> includeListOfHomepages(WikiRequestContext wikiRequestContext)
    {
        return wiki.includeListOfHomepages(wikiRequestContext);
    }//includeListOfHomepages

    @Deprecated
    public String includeTemplate(String filename, WikiRequestContext wikiRequestContext, boolean pre, String revisionNo) throws WikiException
    {
        return NOT_SUPPORTED_MSG;
    }//includeTemplate

} // WikiAPI

