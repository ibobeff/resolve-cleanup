/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import com.resolve.wiki.radeox.filter.regex.LocaleRegexReplaceFilter;

/*
 * NewlineFilter finds \n in its input and transforms this to <br/>
 */

public class NewlineFilter extends LocaleRegexReplaceFilter implements CacheFilter
{
    public static final String LOCAL_KEY = "filter.newline";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "\\\\n");
        addKeyValue(LOCAL_KEY + ".print", "<br/>");
    }

    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }
}
