/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.cache;

/*
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

import com.resolve.util.Log;
*/

public class CacheUtils
{
//    public static CacheManager getLocalCacheManager(String configFile)
//    {
//        return new CacheManager(configFile);
//    } // getLocalCacheManager
//    
//    public static CacheManager getSharedCacheManager(String configFile)
//    {
//        return CacheManager.create(configFile);
//        //return CacheManager.getInstance();
//    } // getSharedCacheManager

    /*
    public static Cache addCache(String name, CacheManager cacheManager)
    {
        Cache result = null;
        
        try
        {
            synchronized (cacheManager)
            {
                result = cacheManager.getCache(name);
                if (result == null)
                {
                    cacheManager.addCache(name);
	                result = cacheManager.getCache(name);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to addCache: "+e.getMessage(), e);
        }
        
        return result;
    } // addCache
//    
//    public static Cache addLongCache(String name, CacheManager cacheManager)
//    {
//        Cache result = null;
//        
//        try
//        {
//            synchronized (cacheManager)
//            {
//                result = cacheManager.getCache(name);
//                if (result == null)
//                {
//                    result = new Cache(
//                        name,                                          // name
//                        1000,                                          // maxElementsInMemory
//                        MemoryStoreEvictionPolicy.LFU,                 // memoryStoreEvictionPolicy
//                        false,                                         // overflowToDisk
//                        null,                                          // diskStorePath
//                        false,                                         // external
//                        0,                                             // timeToLiveSeconds
//                        14400,                                         // timeToIdleSeconds
//                        false,                                         // diskPersistent
//                        0,                                             // diskExpiryThreadIntervalSeconds
//                        null                                           // registerdEventListeners
//                        );
//                            
//                    cacheManager.addCache(result);
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Failed to initialize cache: "+e.getMessage(), e);
//        }
//        
//        return result;
//    } // addLongCache
//    
//    public static Object get(Cache cache, String key)
//    {
//        Object result = null;
//        
//        if (cache != null && cache.isKeyInCache(key) == true)
//        {
//            Element element = cache.get(key);
//            if (element != null)
//            {
//                result = element.getValue();
//            }
//        }
//        
//        return result;
//    } // get
//    
//    public synchronized static void put(Cache cache, String key, Object value)
//    {
//        cache.put(new Element(key, value));
//    } // put
//
//    public synchronized static void remove(Cache cache, String key)
//    {
//        cache.remove(key);
//    } // remove
//    
//    public synchronized static void clear(Cache cache)
//    {
//        cache.removeAll();
//    } // clear
//
//    public static boolean containsKey(Cache cache, String key)
//    {
//        return cache.isKeyInCache(key);
//    } // containsKey
//    
//    public static int size(Cache cache)
//    {
//        return cache.getSize();
//    } // size
 */

}
