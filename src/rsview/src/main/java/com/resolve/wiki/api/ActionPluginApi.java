/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.api;

import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * For backward compatibility
 * 
 * @author jeet.marwah
 *
 */
public class ActionPluginApi extends Api
{
	private Wiki wiki;

	public ActionPluginApi(Wiki wiki, WikiRequestContext context)
	{
		super(context);
		this.wiki = wiki;
	}

	
    public String getForm() throws Exception
    {
        return wiki.getWikiURLFactory().createForwardUrl(WikiAction.view, ConstantValues.NAMESPACE_SYSTEM, ConstantValues.DOCUMENT_ACTIONFORM, null);
    } // getForm
    
    public String getExecute() throws Exception
    {
    	String contextPath = context.getHttpServletRequest().getContextPath();///resolve
    	String servletPath = context.getHttpServletRequest().getServletPath();///service
    	String url = contextPath + servletPath + "/execute";

    	return url;
    } // getExecute

    public String getEvent() throws Exception
    {
    	String contextPath = context.getHttpServletRequest().getContextPath();///resolve
    	String servletPath = context.getHttpServletRequest().getServletPath();///service
    	String url = contextPath + servletPath + "/event/execute";

    	return url;
    } // getEvent

    public String getResult() throws Exception
    {
    	String contextPath = context.getHttpServletRequest().getContextPath();///resolve
    	String servletPath = context.getHttpServletRequest().getServletPath();///service
    	String url = contextPath + servletPath + "/actiontask/result";

    	return url;
    } // getResult
	
    public String executeRunbook(String formName) throws Exception
    {
        return "executeRunbookUsingAjax('"+ formName + "')";
    } // executeRunbook
	
    public String executeEvent(String formName) throws Exception
    {
        return "executeEventUsingAjax('"+ formName + "')";
    } // executeEvent
	
} // ActionPluginApi
