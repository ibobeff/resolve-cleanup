/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.render;

import groovy.lang.Binding;

import java.sql.Connection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.rsbase.MainBase;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.WikiException;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * The main purpose of this is to - trace the Groovy code which will be between
 * <% and %> - execute it - and return the result set.
 * 
 */
public class WikiGroovyRenderer implements WikiRenderer
{

    private static final String GROOVY_START_TAG = "<%";
    private static final String GROOVY_END_TAG = "%>";

    private static final String GROOVY_SYNTAX_START_TAG = "{groovy}";
    private static final String GROOVY_SYNTAX_END_TAG = "{groovy}";

    private String oldPattern = "(?<!\\\\)<%(.*?)%>";
    private String newPattern = "(?<!\\\\)\\{groovy(.*?)\\{groovy\\}";

    @Override
    public String render(String content, Map<String, String> values, WikiRequestContext context)
    {
        String result = content;// to help us in multithreading situations
        String groovyScript = "";
        String groovyResult = "";

        // this will be recursive too as their may be multiple blocks of groovy
        // code
        // the groovy <% will be deprecated slowly and be removed and {groovy}
        // will be the new tag for groovy code
        try
        {
            String tmpResult = "";
            Pattern p = Pattern.compile(oldPattern, Pattern.DOTALL);
            Matcher matcher = p.matcher(result);
            int last = 0;
            while (matcher.find())
            {
                groovyScript = matcher.group(1);
                try
                {
                    groovyResult = getResultFromGroovy(groovyScript, values, context);
                }
                catch (Exception e)
                {
                    Log.log.warn("Wiki Groovy Render Exception", e);
                    groovyResult = e.toString();
                }
                tmpResult = tmpResult + result.substring(last, matcher.start()) + groovyResult;
                last = matcher.end();
            }
            result = tmpResult + result.substring(last);
        }
        catch (Exception e)
        {
            // replace the groovy code with the error.
            result = replaceGroovyCodeWithResult(result, e.getMessage(), false);
        }

        // this will be recursive too as their may be multiple blocks of groovy
        // code
        // the groovy <% will be deprecated slowly and be removed and {groovy}
        // will be the new tag for groovy code
        // this while loop is for the syntax
        try
        {
            Pattern p = Pattern.compile(newPattern, Pattern.DOTALL);
            Matcher matcher = p.matcher(result);

            String tmpResult = "";
            int last = 0;
            while (matcher.find())
            {
                String originalStr = matcher.group();
                groovyScript = originalStr.substring(originalStr.indexOf("}") + 1, originalStr.lastIndexOf("{")).trim();
                try
                {
                    groovyResult = getResultFromGroovy(groovyScript, values, context);
                }
                catch (Exception e)
                {
                    Log.log.warn("Wiki Groovy Render Exception", e);
                    groovyResult = e.toString();
                }

                tmpResult = tmpResult + result.substring(last, matcher.start()) + groovyResult;
                last = matcher.end();
                // result = ParseUtil.replaceString(originalStr, groovyResult,
                // result);
            }
            result = tmpResult + result.substring(last);
        }
        catch (Exception e)
        {
            // replace the groovy code with the error.
            result = replaceGroovyCodeWithResult(result, e.getMessage(), true);
        }

        return result;
    }

    /**
     * Return true if there is groovy code in the content.
     * 
     * @param content
     * @return
     */
    private boolean hasGroovyCode(String content, boolean isSyntax) throws WikiException
    {
        String startTag = isSyntax ? GROOVY_SYNTAX_START_TAG : GROOVY_START_TAG;
        String endTag = isSyntax ? GROOVY_SYNTAX_END_TAG : GROOVY_END_TAG;

        // compare the # of open and closed brackets
        hasSameNumberOfOpenCloseBracketFor(content, isSyntax);// will throw
                                                              // exception if
                                                              // not true;

        boolean hasGroovy = false;

        // Just the start tag is sufficient as we are already checking for the
        // Open and close count. So even if one Open is there, there is Groovy
        // script in the content.
        int posStartGroovy = content.indexOf(startTag);
        // int posEndGroovy = content.indexOf(GROOVY_END_TAG);

        if (posStartGroovy != -1)// && posStartGroovy < posEndGroovy)
        {
            hasGroovy = true;
        }
        return hasGroovy;
    }

    /**
     * The script should have same # of Open and Close tags. Else throw error.
     * 
     * @param content
     * @return
     * @throws WikiException
     */
    private boolean hasSameNumberOfOpenCloseBracketFor(String content, boolean isSyntax) throws WikiException
    {
        boolean hasSameNumberOfOpenCloseBracket = true;

        String startTag = isSyntax ? GROOVY_SYNTAX_START_TAG : GROOVY_START_TAG;
        String endTag = isSyntax ? GROOVY_SYNTAX_END_TAG : GROOVY_END_TAG;

        int countOpen = getCountFor(content, startTag);
        int countClose = getCountFor(content, endTag);

        if (countOpen != countClose)
        {
            throw new WikiException(new Exception("There is a mismatch of '" + startTag + "' and '" + endTag + "'. Count of '" + startTag + "' is " + countOpen + " and count of '" + endTag + "' is " + countClose));
        }

        return hasSameNumberOfOpenCloseBracket;

    }

    /**
     * Returns the count of # of instances of 'countFor' in the String
     * 'content'. For eg. how many times does '<%' exists in 'content'
     * 
     * @param content
     * @param countFor
     * @return
     */
    private int getCountFor(String content, String countFor)
    {
        int count = 0;
        String localStr = content;// this is done to make this method
                                  // multithread proof
        int pos = -1;

        while ((pos = localStr.indexOf(countFor)) != -1)
        {
            localStr = localStr.substring(pos + countFor.length());
            count++;
        }

        return count;
    }

    /**
     * This is called after checking if the code is there
     * 
     * @param content
     * @return
     */
    private String getTheGroovyScript(String content, boolean isSyntax)
    {
        String startTag = isSyntax ? GROOVY_SYNTAX_START_TAG : GROOVY_START_TAG;
        String endTag = isSyntax ? GROOVY_SYNTAX_END_TAG : GROOVY_END_TAG;

        String start = content.substring(content.indexOf(startTag) + startTag.length());
        String groovyScript = start.substring(0, start.indexOf(endTag));
        return groovyScript;
    }

    /**
     * The actual method that executes the Groovy script and returns the result.
     * If there is an error in execution, it will return the error string
     * 
     * @param groovyScript
     * @return
     */
    private String getResultFromGroovy(String groovyScript, Map<String, String> values, WikiRequestContext context)
    {
        String result = "";

        // create a new instance - CREATE A NEW WIKI CLASS TO HANDLE THE OUTPUT 
        GroovyBuffer OUT = new GroovyBuffer();
        Connection conn = null;
        boolean transaction = false;
        
        try
        {
            String userId = (String) context.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            if (StringUtils.isEmpty(userId))
            {
                userId = (String) context.getAttribute(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
            }
            Log.log.info("UserId in Groovy Renderer :" + userId);

            String problemId = (String) context.getAttribute(Constants.EXECUTE_PROBLEMID);
            if (StringUtils.isEmpty(problemId) || problemId.equalsIgnoreCase(Constants.PROBLEMID_NEW) || problemId.equalsIgnoreCase(Constants.PROBLEMID_ACTIVE))
            {
                problemId = WorksheetUtils.getActiveWorksheet(userId, (String) context.getAttribute(Constants.EXECUTE_ORG_ID), 
                                                              (String) context.getAttribute(Constants.EXECUTE_ORG_NAME));
            }
            Log.log.info("ProblemId in Groovy Renderer :" + problemId);

            // init db connection ONLY if it is used in the groovy script
            if (GroovyScript.checkOpenTransaction(groovyScript))
            {
                transaction = true;
                if (GroovyScript.checkScriptUseDB(groovyScript, true))
                {
                    conn = HibernateUtil.getConnection();
                }
            }

            Binding binding = new Binding();
            binding.setVariable(Constants.GROOVY_GROOVYBUFFER_OUT, OUT);
            binding.setVariable(Constants.GROOVY_BINDING_DB, conn);
            binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.getESB());
            binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
            binding.setVariable(Constants.GROOVY_BINDING_USERID, userId);

            // variables common to Velocity and Groovy renderer
            // pass the UserAccessRights Object in the Velocity Renderer
            binding.setVariable(ConstantValues.WIKI_USER_KEY, context.getWikiUser());
            binding.setVariable(Constants.GROOVY_HTTP_REQUEST, context.getHttpServletRequest());
            binding.setVariable(Constants.WIKI, context.getWikiAPI());
            binding.setVariable(Constants.WIKIDOCUMENT, context.getWikiDocumentAPI());
            binding.setVariable(Constants.CURR_WIKIDOCUMENT, context.getCurrWikiDocumentAPI());
            binding.setVariable(Constants.WIKI_CONTEXT, context);

            // for backward compatibility
            binding.setVariable(ConstantValues.WIKI_NAME_KEY, context.getWikiAPI());
            binding.setVariable(ConstantValues.WIKI_DOC_KEY, context.getWikiDocumentAPI());// context.getWiki());

            if (StringUtils.isNotEmpty(problemId))
            {
                binding.setVariable(Constants.GROOVY_BINDING_PROBLEMID, problemId);
            }
            else
            {
                binding.setVariable(Constants.GROOVY_BINDING_PROBLEMID, null);
                Log.log.warn("Failed to Look up Worksheet, setting PROBLEMID to null");
            }
            
            // binding the values that are passed - similar to what we do for
            // VelocityContext
            if (values != null)
            {
                for (Map.Entry entry : values.entrySet())
                {
                    binding.setVariable((String) entry.getKey(), entry.getValue());
                }
            }

            WSDataMap wsData = ServiceWorksheet.getWorksheetWSDATA(problemId);
            binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsData);

            String scriptName = context.getWikiDocumentAPI().getFullName();
            Object value = GroovyScript.execute(groovyScript, scriptName, true, binding);
           
            if(wsData != null)
            {
                //ServiceWorksheet.saveWorksheetWSDATA(problemId, wsData, "system");
            }

            if (value != null)
            {
                try
                {
                    result = (String) value;

                }
                catch (Exception e)
                {
                    Log.log.error("Error in WikiGroovyRenderer :" + userId, e);
                    throw new WikiException(new Exception("The groovy script executed but returned a different object other than String. Please make sure that it returns a String."));
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error in WikiGroovyRenderer :", t);
            
            // just put the actual exception content and return it.
            result = t.getMessage();
            throw new RuntimeException(result);
        }
        finally
        {
            if (transaction)
            {
                try
                {
                    if (conn != null && !conn.isClosed())
                    {
                        conn.close();
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
                
            }
        }

        OUT.println(result);
        return OUT.toString();
    }

    /**
     * replace the Groovy script with Groovy result and return it
     * 
     * @param result
     * @param groovyScript
     * @param groovyResult
     * @return
     */
    private String replaceGroovyCodeWithResult(String result, String groovyResult, boolean isSyntax)
    {
        String localResult = result;
        String startTag = isSyntax ? GROOVY_SYNTAX_START_TAG : GROOVY_START_TAG;
        String endTag = isSyntax ? GROOVY_SYNTAX_END_TAG : GROOVY_END_TAG;

        if (localResult == null || localResult.indexOf(startTag) < 0)
        {
            return localResult;
        }
        String startString = localResult.substring(0, localResult.indexOf(startTag));
        String endString = localResult.substring(localResult.indexOf(endTag) + endTag.length(), localResult.length());

        StringBuffer str = new StringBuffer(startString);
        str.append(groovyResult);
        str.append(endString);

        return str.toString();
    }

    /*
     * This is just to unit test the logic
     * 
     * @param args
     * @throws WikiException
    public static void main(String[] args) throws WikiException
    {
        String content = "This is < System.out.println(\"Test1\"); return \"JEET1\"; %> mixed example. This is one more groovy block <% System.out.println(\"Test2\"); return \"JEET2\"; %> and here can be velocity template";
        System.out.println("ORIGINAL CONTENT :" + content);
        System.out.println("GROOVY PARSED CONTENT :" + new WikiGroovyRenderer().render(content, null, null));
    }
     */

    @Override
    public String preRender(String content, Map<String, String> values, WikiRequestContext context)
    {
        return content;
    }

    @Override
    public String postRender(String content, Map<String, String> values, WikiRequestContext context)
    {
        return content;
    }

}
