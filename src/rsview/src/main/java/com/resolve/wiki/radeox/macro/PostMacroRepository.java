/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import java.util.Iterator;
import java.util.List;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;

/**
 * Repository for plugins
 * 
 * I have removed the dependency of Loader from the Repository
 * 
 * @author jeet.marwah
 * 
 */
public class PostMacroRepository extends PluginRepository
{
	protected static PostMacroRepository instance;

	//spring injected
	private List<String> listOfMacros;

	public synchronized static PostMacroRepository getInstance()
	{
		if (null == instance)
		{
			instance = new PostMacroRepository();
		}
		return instance;
	}

	//executed by spring 
	public void init()
	{
		ClassLoader cl = this.getClass().getClassLoader();
		for (String macroName : listOfMacros)
		{
			try
			{
				Macro ma = (Macro) cl.loadClass(macroName).newInstance();
				list.add(ma);
				plugins.put(getNameForPlugin(ma.getName()), ma);
			}
			catch (Exception e)
			{
				Log.log.error(e.getMessage(), e);
			}
		}
	}

	public void setInitialContext(InitialRenderContext context)
	{
		Iterator<Object> iterator = list.iterator();
		while (iterator.hasNext())
		{
			Macro macro = (Macro) iterator.next();
			macro.setInitialContext(context);
		}
	}

	private PostMacroRepository()
	{

	}

	private static String getNameForPlugin(String pluginName)
	{
		if (pluginName == null || pluginName.trim().length() == 0)
			return null;

		String name = "";
		try
		{
			String[] str = pluginName.split("\\.");
			name = str[1];
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			name = pluginName;
		}
		return name;
	}

	public List<String> getListOfMacros()
	{
		return listOfMacros;
	}

	public void setListOfMacros(List<String> listOfMacros)
	{
		this.listOfMacros = listOfMacros;
	}

}
