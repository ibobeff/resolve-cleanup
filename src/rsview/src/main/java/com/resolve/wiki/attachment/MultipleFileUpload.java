/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.attachment;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.resolve.dto.AttachmentModel;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.store.WikiStore;
import com.resolve.wiki.web.WikiRequestContext;

public class MultipleFileUpload
{
    private WikiDocument wikiDocument = null;
    private WikiDocument globalWikiDocument = null;
    private WikiRequestContext wikiRequestContext = null;
    private  HttpServletRequest request = null;
    private WikiStore store = null;
    private String browserTimeZone = null;
    private String username = null;
    private boolean isGlobal = false;
    
    private WikiAttachment wikiAttachment = null;
    private WikiAttachmentContent wikiAttachmentContent = null;

    //presently, it will always be true but later on can be refactored for single upload also
    private boolean isMultipleUpload = false;
    
    private File zipFile = null;
    private File unzipFolder = null;

    private final static String DESTINATION_FOLDER = RSContext.getResolveHome() + "rsexpert/";
    private String MODULE_FOLDER_LOCATION = DESTINATION_FOLDER;
    private String ZIP_FILE = DESTINATION_FOLDER;


    
    public MultipleFileUpload(WikiDocument wikiDocument, WikiRequestContext wikiRequestContext) throws Exception
    {
        this.wikiDocument = wikiDocument;
        this.wikiRequestContext = wikiRequestContext;
        this.request = wikiRequestContext.getHttpServletRequest();
        this.store = wikiRequestContext.getWiki().getWikiStore();
        
        boolean isMultipart = ServletFileUpload.isMultipartContent(this.request);
        if (!isMultipart)
        {
            //if not an upload request, then return
            throw new Exception("This is not a multipart upload. So aborting.");
        }

        this.browserTimeZone = wikiRequestContext.getAttribute(ConstantValues.BROWSER_CLIENT_TIMEZONE)!=null ? (String) wikiRequestContext.getAttribute(ConstantValues.BROWSER_CLIENT_TIMEZONE) : null;//UTC-8
        this.username = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String isMultipleUploadStr = (String) wikiRequestContext.getAttribute(HibernateConstants.IS_MULTIPLE_FILE);
        isMultipleUpload = StringUtils.isNotEmpty(isMultipleUploadStr) && isMultipleUploadStr.equalsIgnoreCase("true") ? true : false;
        
    }
    
    public void upload()
    {
        if(isMultipleUpload)
        {
            proceedToMultipleFileUpload();
        }
        
    }
    
    private void proceedToMultipleFileUpload()
    {
        //read the file
        readAndPlaceFileToTempLocation();
        if(isGlobal)
        {
            try
            {
                this.globalWikiDocument = store.find(ConstantValues.NAMESPACE_SYSTEM, ConstantValues.DOCUMENT_ATTACHMENT);
            }
            catch(Exception e)
            {
                Log.log.error("error getting the handle to global document", e);
            }
        }
        
        //unzip it
        unzip();
        
        //recurse each file and upload it
        unzipFolder = FileUtils.getFile(MODULE_FOLDER_LOCATION);
        updateFilesToDB(unzipFolder);
        
        //cleanup
        cleanup();
        
    }
    
    private void readAndPlaceFileToTempLocation()
    {
     // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        try
        {
            // Parse the request
            List<FileItem> items = upload.parseRequest(request);

            // Process the uploaded items
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext())
            {
                FileItem item = (FileItem) iter.next();
                String name = item.getFieldName();
                if(name != null && name.equals(HibernateConstants.IS_GLOBAL))
                {
                    String value = item.getString();
                    isGlobal = value != null && value.trim().equalsIgnoreCase("true") ? true : false;
                    continue;
                }

                if (item.isFormField())
                {
                    //                  processFormField(item);
                }
                else
                {
                    processUploadedFile(item);
                }
            }

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    private void processUploadedFile(FileItem item) throws Exception
    {
        //few attributes for debuging purpose...may be removed/commented out 
        String fieldName = item.getFieldName();
        String fileName = item.getName();//C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
        fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);

        MODULE_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + fileName.substring(0, fileName.indexOf('.')) + "/";
        ZIP_FILE = ZIP_FILE + fileName;

        String contentType = item.getContentType();//text/plain
        boolean isInMemory = item.isInMemory();

        long sizeInBytes = item.getSize();

        this.zipFile = FileUtils.getFile(DESTINATION_FOLDER, fileName);
        item.write(this.zipFile);
        
    }

    private void unzip() 
    {
        
        try
        {
            //delete the destination folder if it exist
            File moduleFolder = FileUtils.getFile(MODULE_FOLDER_LOCATION);
            if(moduleFolder != null && moduleFolder.isDirectory() && moduleFolder.exists())
            {
                Log.log.info("Deleting  existing folder -->" + moduleFolder.getName());
                FileUtils.deleteDirectory(moduleFolder);
            }
            
            FileUtils.unzip( FileUtils.getFile(ZIP_FILE), FileUtils.getFile(MODULE_FOLDER_LOCATION));
        }
        catch(Exception e)
        {
            Log.log.error("error in unziping the file:" + this.zipFile.getName(), e);
        }
        
    }//unzip
    
    private void updateFilesToDB(File folder)
    {
        if(folder.exists())
        {
            if(folder.isDirectory())
            {
                File[] files = folder.listFiles();
                
                for (File file : files)
                {
                    if (file.isFile())
                    {
                        try
                        {
                            updateFile(file);
                        }
                        catch(Throwable t)
                        {
                            Log.log.error("Error attaching the file :" + file.getName() + " to the document : " + this.wikiDocument.getUFullname(), t);
                        }
                    }
                    else if(file.isDirectory())
                    {
                        updateFilesToDB(file);
                    }
                }//end of for loop
            }//end of if
        }//end of if
        else
        {
            Log.log.error("This File/Folder does not exist: " + MODULE_FOLDER_LOCATION);
        }
    }//updateFileToDB
    
    private void updateFile(File file) throws Throwable
    {
        reinitialize();
        
        //file attributes
        long sizeInBytes = file.length();
        String filename = file.getName();

        //read the file 
        byte[] data = new byte[(int) sizeInBytes];
        FileInputStream inputFileInputStream = new FileInputStream(file);
        inputFileInputStream.read(data);
        inputFileInputStream.close();
        
        //update the objects
        wikiAttachmentContent.setUContent(data);
        wikiAttachmentContent.setSysUpdatedOn(GMTDate.getDate());
        wikiAttachmentContent.setSysCreatedOn(GMTDate.getDate());

        wikiAttachment.setUFilename(filename);
        wikiAttachment.setUSize(data.length);
        wikiAttachment.setULocation("");
        wikiAttachment.setUType("DB");
        wikiAttachment.usetWikiAttachmentContent(wikiAttachmentContent);
        wikiAttachment.setSysUpdatedOn(GMTDate.getDate());
        wikiAttachment.setSysCreatedOn(GMTDate.getDate());
        wikiAttachment.setSysCreatedBy("system");
        
        //persist
        if(isGlobal)
        {
            store.addAttachment(this.globalWikiDocument, wikiAttachment, false);
            
            //add a reference to the current document
            AttachmentModel attachModel = new AttachmentModel();
            attachModel.setSys_id(wikiAttachment.getSys_id());
            attachModel.setCreatedBy(username);
            List<AttachmentModel> listOfAttachment = new ArrayList<AttachmentModel>();
            listOfAttachment.add(attachModel);

            store.globalAttach( wikiDocument, listOfAttachment);
        }
        else
        {
            store.addAttachment(this.wikiDocument, wikiAttachment, false);
        }
        
    }
    
    private void reinitialize()
    {
        wikiAttachment = new WikiAttachment();
        wikiAttachmentContent = new WikiAttachmentContent();
    }//reset
    
    private void cleanup()
    {
        //delete the folder
        try
        {
            FileUtils.deleteDirectory(unzipFolder);
        }
        catch(Exception e)
        {
            Log.log.error("error deleteing the folder:" + MODULE_FOLDER_LOCATION, e);
            
        }        
        
        //delete the zip file
        this.zipFile.delete();
    }
    

}
