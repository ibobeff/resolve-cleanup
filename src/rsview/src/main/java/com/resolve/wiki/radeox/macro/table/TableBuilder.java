/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.table;

/**
 * Built a table from a string
 *
 */

public class TableBuilder
{
	public static Table build(String content)
	{
		/*
		 * If user adds | as part of either column header or content,
		 * UI escapes the pile by adding a back slash, \. When request comes to
		 * server, that back slash is getting converted to &#92;
		 */
		content = content.replace("&#92;", "\\");
		
		Table table = new Table();
		String[] contentArray = content.split("(?<!\\\\)[|]");
		for (String splitContet : contentArray)
		{
			String[] splitContentArray = splitContet.split("\n");
			if (splitContentArray.length > 0)
			{
				if (splitContentArray.length == 1)
				{
					String token = splitContentArray[0].replace("\\", "");
					table.addCell(token);
				}
				else
				{
					String token = splitContentArray[0].replace("\\", "");
					table.addCell(token);
					table.newRow();
					token = splitContentArray[1].replace("\\", "");
					table.addCell(token);
					
				}
			}
			else
			{
				table.addCell(" ");
			}
		}
		table.newRow();
		
//		StringTokenizer tokenizer = new StringTokenizer(content, "|\n", true);
//		String lastToken = null;
//		while (tokenizer.hasMoreTokens())
//		{
//			String token = tokenizer.nextToken();
//			if ("\n".equals(token))
//			{
//				// Handles "\n" - "|\n"
//				if (null == lastToken || "|".equals(lastToken))
//				{
//					table.addCell(" ");
//				}
//				table.newRow();
//			}
//			else if (!"|".equals(token))
//			{
//				table.addCell(token);
//			}
//			else if (null == lastToken || "|".equals(lastToken))
//			{
//				// Handles "|" "||"
//				table.addCell(" ");
//			}
//			lastToken = token;
//		}
		return table;
	}
}
