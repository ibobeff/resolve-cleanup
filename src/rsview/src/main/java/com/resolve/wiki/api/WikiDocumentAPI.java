/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.api;

import java.util.Date;

import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Log;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This class file encapsulates the Wiki doc and provides accessor methods that can be used in the Velocity template
 * 
 * @author jeet.marwah
 * 
 */
public class WikiDocumentAPI extends Api
{
	private WikiDocumentVO wikiDoc;

	public WikiDocumentAPI(WikiDocumentVO doc, WikiRequestContext context)
	{
		super(context);
		this.wikiDoc = doc;
	}

	
	public WikiDocumentVO getWikiDoc()
	{
		return wikiDoc;
	}//getWikiDoc
	
    public String getName()
    {
        return wikiDoc.getUName();
    }//getName

    public String getWeb()
    {
        return wikiDoc.getUNamespace();
    }//getWeb

    public String getFullName()
    {
        return wikiDoc.getUFullname();
    }//getFullName
    
    public String getSummary()
    {
    	return wikiDoc.getUSummary();
    }//getSummary
    
    public String getTitle()
    {
        return wikiDoc.getUTitle();
    }//getTitle

    public String getCreatedBy()
    {
    	return wikiDoc.getSysCreatedBy();
    }//getCreatedBy
    
    public Date getCreatedOn()
    {
    	return wikiDoc.getSysCreatedOn();
    }//getCreatedOn

    public String getUpdatedBy()
    {
    	return wikiDoc.getSysUpdatedBy();
    }//getUpdatedBy
    
    public Date getUpdatedOn()
    {
    	return wikiDoc.getSysUpdatedOn();
    }//getUpdatedOn
    
    public long getViewCount()
    {
    	long count = -1;
    	
    	try
    	{
    		count = context.getWiki().getWikiDocumentViewCount(context, getFullName());
    	}
    	catch(Exception e)
    	{
    		Log.log.error(e.getMessage(), e);
    	}
    	return count;
    }//getViewCount

    public long getEditCount()
    {
    	long count = -1;
    	
    	try
    	{
    		count = context.getWiki().getWikiDocumentEditCount(context, getFullName());
    	}
    	catch(Exception e)
    	{
    		Log.log.error(e.getMessage(), e);
    	}
    	return count;
    }//getEditCount
    
    public long getExecuteCount()
    {
    	long count = -1;
    	
    	try
    	{
    		count = context.getWiki().getWikiDocumentExecuteCount(context, getFullName());
    	}
    	catch(Exception e)
    	{
    		Log.log.error(e.getMessage(), e);
    	}
    	return count;
    }//getExecuteCount
    
    public String getNamespace() {
        return wikiDoc.getUNamespace();
    }
    
}//end of class file

