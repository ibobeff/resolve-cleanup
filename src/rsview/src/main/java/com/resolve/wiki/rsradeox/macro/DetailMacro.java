/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.WikiUtils;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.store.HibernateStoreUtil;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * Macro for defining and displaying the URL to Resolve Execute Request
 * for ActionTask execution. The # namespace is optional (if required).
 * 
 * DETAIL DEFINITION
 *   {detail:[name=<name>|width=<x>|height=<y>|view=<viewname>]} [DEFINTION] {detail}
 *   
 * DEFINITION
 *   syntax = task#namespace(wiki::nodeId)
 *   
 */
public class DetailMacro extends BaseLocaleMacro
{

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestContext.getWiki();
        
        String taskList = params.getContent();
        taskList = HttpUtil.sanitizeValue(taskList);
        String type = StringUtils.isNotEmpty(params.get("type")) ? params.get("type"): "";
        type = HttpUtil.sanitizeValue(type,"FileName", 400);
        String separator = StringUtils.isNotEmpty(params.get("separator")) ? params.get("separator"): ""; 
        separator = HttpUtil.sanitizeValue(separator);
        String max = StringUtils.isNotEmpty(params.get("max")) ? params.get("max") : ""; 
        max = HttpUtil.sanitizeValue(max);
        String offset = StringUtils.isNotEmpty(params.get("offset")) ? params.get("offset") : ""; 
        offset = HttpUtil.sanitizeValue(offset);
        String refreshInterval = StringUtils.isNotEmpty(params.get("refreshInterval")) ? params.get("refreshInterval"): "5";
        refreshInterval = HttpUtil.sanitizeValue(refreshInterval);
        String refreshStartWiki = StringUtils.isNotEmpty(params.get("refreshStartWiki")) ? params.get("refreshStartWiki"): "";
        refreshStartWiki = HttpUtil.sanitizeValue(refreshStartWiki);
        
        String[] actionTaskNamesArr = null;
        String currentUser = wikiRequestContext.getWikiUser().getUsername();
        currentUser = HttpUtil.sanitizeValue(currentUser);
        String documentName = wiki.getDocumentName(wikiRequestContext);
        documentName = HttpUtil.sanitizeValue(documentName);
        String problemId = (String)wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_PROBLEMID);
        
        if (StringUtils.isBlank(problemId))
        {
            String orgId = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_ID);
            orgId = HttpUtil.sanitizeValue(orgId);
            
            String orgName = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_NAME);
            orgName = HttpUtil.sanitizeValue(orgName);
            
            problemId = HibernateStoreUtil.getActiveSid(currentUser, orgId, orgName);
        } 
        problemId = HttpUtil.sanitizeValue(problemId);
        
        String isDoneYetId = "isDoneYetId_" + JavaUtils.generateUniqueNumber();
        String divId = "details_" + JavaUtils.generateUniqueNumber();
        String problemId_divId = WikiUtils.createProblemId(documentName);//"_problemId_" + documentName.replaceAll(" ", "_");//should map to the view.vm , a div is defined in that file with this id

        if(StringUtils.isNotEmpty(problemId) && !problemId.equals("-1") && StringUtils.isNotEmpty(taskList))
        {
            actionTaskNamesArr = taskList.trim().split("\n");
            
            StringBuffer actionNamespaceArr = new StringBuffer("[");
            StringBuffer actionNameArr = new StringBuffer("[");
            StringBuffer nodeIdArr = new StringBuffer("[");
            
            if(actionTaskNamesArr != null && actionTaskNamesArr.length > 0)
            {
                for(int i = 0; i < actionTaskNamesArr.length; i++)
                {
                    String atName = actionTaskNamesArr[i].trim();
                    populateArrayString(atName, actionNamespaceArr, actionNameArr, nodeIdArr);
                    
                    if(i != actionTaskNamesArr.length - 1)
                    {
                        actionNamespaceArr.append(",");
                        actionNameArr.append(",");
                        nodeIdArr.append(",");
                    }
                }
            }
            
            actionNamespaceArr.append("]");
            actionNameArr.append("]");
            nodeIdArr.append("]");
            
            //prepare the params
            StringBuffer ajaxParams = new StringBuffer();
            ajaxParams.append("problemId : (problemIdEl && problemIdEl.innerHTML) ? problemIdEl.innerHTML : ''").append(",").append("\n"); 
    
            ajaxParams.append("currentUser : '").append(currentUser).append("',").append("\n");
            ajaxParams.append("documentName : '").append(documentName).append("',").append("\n");

            ajaxParams.append("type : '").append(type).append("',").append("\n");
            ajaxParams.append("separator : '").append(StringUtils.escapeHtml(separator)).append("',").append("\n");
            ajaxParams.append("max : '").append(max).append("',").append("\n");
            ajaxParams.append("offset : '").append(offset).append("',").append("\n");
            
            ajaxParams.append("refreshStartWiki : '").append(refreshStartWiki).append("',").append("\n");
            ajaxParams.append("isDoneYetId : '").append(isDoneYetId).append("',").append("\n");            
            
            ajaxParams.append("actionNamespace : ").append(actionNamespaceArr.toString()).append(",").append("\n");
            ajaxParams.append("actionName : ").append(actionNameArr.toString()).append(",").append("\n");
            ajaxParams.append("nodeId : ").append(nodeIdArr.toString()).append("").append("\n");
            
            //interval in millisecond
            long interval = 5000;
            try
            {
                interval = Long.parseLong(refreshInterval) * 1000;
            }
            catch(Throwable t)
            {
                interval = 5000;
            }
            
            String output = ResultMacro.getJavascriptForResultAndDetail(divId, problemId_divId, ajaxParams.toString(), interval, ConstantValues.URL_DETAIL_RPC_HTML);
            
            writer.write(output);
            
        }//end of if
        else
        {
            StringBuffer html = new StringBuffer();
            html.append("<div id='").append(divId).append("'>");
            html.append("<input type='hidden' name='isDoneYet' id='").append(isDoneYetId).append("' value='"+ ConstantValues.STOP_AJAX_REFRESH +"'>");
            html.append("</div>");
            writer.write(html.toString());
        }
           
    }

	@Override
	public String getLocaleKey()
	{
		return "macro.detail";
	}
	
    private void populateArrayString(String atName, StringBuffer actionNamespaceArr, StringBuffer actionNameArr, StringBuffer nodeIdArr)
    {
        if(StringUtils.isNotEmpty(atName))
        {
            ResultMacro.populateArrayString(atName, actionNamespaceArr, actionNameArr, nodeIdArr, null, null);
        }
        
    }//end of populateArrayString


}
