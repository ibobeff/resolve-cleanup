/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.regex;

/*
 * Class that compiles regular expressions to patterns
 * Implementation for regex package in JDK 1.4
 *
 */

public class JdkCompiler extends Compiler {
  private boolean multiline;

  public void setMultiline(boolean multiline) {
    this.multiline = multiline;
  }

  public Pattern compile(String regex) {
    return new JdkPattern(regex, multiline);
  }
}