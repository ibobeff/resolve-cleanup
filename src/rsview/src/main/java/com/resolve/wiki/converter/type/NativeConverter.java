/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter.type;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.services.util.ParseUtil;
import com.resolve.util.StringUtils;
import com.resolve.wiki.converter.AbstractConverter;
import com.resolve.wiki.converter.WikiConverter;


/**
 * This class is mapped to the <span> tag that renders groovy code
 * 
 * @author jeet.marwah
 *
 */
public class NativeConverter extends AbstractConverter
{

	private String CSS_NAME = "native";
	private String HTML_START_TAG = "<span class=\"" + CSS_NAME + "\">";
	private String HTML_END_TAG = "</span>";
	private String WIKI_START_TAG = "{native}";
	private String WIKI_END_TAG = "{native}";

	private String html2wikiPattern = "\\{native(.*?)\\{native\\}";
	
	public String getHTML2WikiPattern()
	{
		return html2wikiPattern;
	}
	
	@Override
	protected String processTextBetweenPattern(String content)
	{
		String contentStr = new String(content);
		
		if(getHTML2WikiPattern() != null)
		{
			Pattern p = Pattern.compile(getHTML2WikiPattern(), Pattern.DOTALL);
			Matcher matcher = p.matcher(contentStr);
	
			while (matcher.find())
			{
				String originalStr = matcher.group();
				
				String newString = originalStr.replaceAll("<br>", "\n");
				newString = newString.replaceAll("</p>", "\n");
				newString = newString.replaceAll(WikiConverter.BR_PLACE_HOLDER, "\n");
				newString = removeHTML(newString);
				
				newString = StringUtils.unescapeHtml(newString);
				newString = newString.substring(newString.indexOf("}")+1, newString.lastIndexOf("{")).trim();
				
				contentStr = ParseUtil.replaceString(originalStr, newString, contentStr);
			}
		}
		
		return contentStr;
	}

	/**
	 * 
	 */
//	@Override
//	public String html2wiki(String content)
//	{
//		String contentStr = new String(content);
//		
//		//<span> processing
//		contentStr = removeWikiSpanTags(contentStr);
//		
//		//<p> processing
//		contentStr = remove_P_tagForCSS(contentStr);
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		Pattern p = Pattern.compile(html2wikiPattern, Pattern.DOTALL);
//		Matcher matcher = p.matcher(contentStr);
//		
//		while (matcher.find())
//		{
//			String originalStr = matcher.group();
//			
//			String inbetweenContent = originalStr.substring(originalStr.indexOf("}")+1, originalStr.lastIndexOf("{")).trim();
//			inbetweenContent = removeHTML(inbetweenContent);
//			inbetweenContent = StringUtils.unescapeHtml(inbetweenContent);
//			
//			contentStr = ParseUtil.replaceString(originalStr, inbetweenContent, contentStr);
//		}
//		return contentStr;
//	}
	
	@Override
	public String wiki2html(String content)
	{
		String contentStr = new String(content);
		
		for(String pattern: getPatternsToConvertToNative())
		{
			Pattern p = Pattern.compile(pattern, Pattern.DOTALL);
			Matcher matcher = p.matcher(contentStr);
			
			while (matcher.find())
			{
				String originalStr = matcher.group();
				
				String newString = StringUtils.escapeHtml(originalStr);
				newString = WIKI_START_TAG + newString + WIKI_END_TAG;
				newString = HTML_START_TAG + newString + HTML_END_TAG;
				
				newString = newString.replaceAll("\r\n", "<br>");
				newString = newString.replaceAll("\n\r", "<br>");
				newString = newString.replaceAll("\n", "<br>");
				newString = newString.replaceAll("\r", "<br>");
				
				contentStr = ParseUtil.replaceString(originalStr, newString, contentStr);
			}
		}
		
		return contentStr;
	}
	
	private List<String> getPatternsToConvertToNative()
	{
		List<String> patterns = new ArrayList<String>();
		patterns.add("\\<iframe(.*?)\\</iframe\\>");
		
		return patterns;
	}
	
	
	
	@Override
	public String getCSS_NAME()
	{
		return CSS_NAME;
	}

	@Override
	public String getHTML_START_TAG()
	{
		return HTML_START_TAG;
	}

	@Override
	public String getHTML_END_TAG()
	{
		return HTML_END_TAG;
	}

	@Override
	public String getWIKI_END_TAG()
	{
		return WIKI_END_TAG;
	}

	@Override
	public String getWIKI_START_TAG()
	{
		return WIKI_START_TAG;
	}



}//JavascriptConverter
