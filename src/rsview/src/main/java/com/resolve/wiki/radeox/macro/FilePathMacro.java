/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.util.HttpUtil;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

import java.io.IOException;
import java.io.Writer;

/*
 * Displays a file path. This is used to store a filepath in an OS independent
 * way and then display the file path as needed. This macro also solves the
 * problems with to many backslashes in Windows filepaths when they are entered
 * 
 */

public class FilePathMacro extends LocalePreserved
{
    private String[] paramDescription = { "1: file path" };

    public String getLocaleKey()
    {
        return "macro.filepath";
    }

    public FilePathMacro()
    {
        addSpecial('\\');
    }

    public String getDescription()
    {
        return "Displays a file system path. The file path should use slashes. Defaults to windows.";
    }

    public String[] getParamDescription()
    {
        return paramDescription;
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {

        if (params.getLength() == 1)
        {
            String path = HttpUtil.sanitizeValue(params.get("0")).replace('/', '\\');
            writer.write(replace(path));
        }
        return;
    }
}
