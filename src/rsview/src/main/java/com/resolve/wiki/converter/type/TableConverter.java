/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.converter.type;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.services.util.ParseUtil;
import com.resolve.wiki.converter.AbstractConverter;


/**
 * 
 * 
 * @author jeet.marwah
 *
 */
public class TableConverter extends AbstractConverter
{

	private String CSS_NAME = "table";
	private String HTML_START_TAG = "<span class=\"" + CSS_NAME + "\">";
	private String HTML_END_TAG = "</span>";
	private String WIKI_START_TAG = "{table}";
	private String WIKI_END_TAG = "{table}";

	private String pattern = "\\{table(.*?)\\{table\\}";

    @Override
    public String wiki2html(String content)
    {
        String contentStr = new String(content);

        if (getWiki2HTMLPattern() != null)
        {
            Pattern p = Pattern.compile(getWiki2HTMLPattern(), Pattern.DOTALL);
            Matcher matcher = p.matcher(contentStr);

            while (matcher.find())
            {
                String originalStr = matcher.group();

                String htmlStr = originalStr.replaceAll("<p>", "");
                htmlStr = htmlStr.replaceAll("</p>", "");
                htmlStr = htmlStr.replaceAll("\n", "<br>");
                htmlStr = getHTML_START_TAG() + htmlStr + getHTML_END_TAG();

                contentStr = ParseUtil.replaceString(originalStr, htmlStr, contentStr);

            }
        }

        return contentStr;
    }
	
	
	@Override
	public String getHTML2WikiPattern()
	{
		return pattern;
	}
	
	@Override
	public String getWiki2HTMLPattern()
	{
		return pattern;
	}
	
	
	@Override
	public String getCSS_NAME()
	{
		return CSS_NAME;
	}

	@Override
	public String getHTML_START_TAG()
	{
		return HTML_START_TAG;
	}

	@Override
	public String getHTML_END_TAG()
	{
		return HTML_END_TAG;
	}

	@Override
	public String getWIKI_END_TAG()
	{
		return WIKI_END_TAG;
	}

	@Override
	public String getWIKI_START_TAG()
	{
		return WIKI_START_TAG;
	}



}//JavascriptConverter
