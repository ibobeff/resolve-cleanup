/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.api;

import java.io.IOException;
import java.io.Writer;

/**
 * Converts a class name to an API url
 *
 * @author Stephan J. Schmidt
 * @version $Id: ApiConverter.java,v 1.4 2003/05/23 10:47:25 stephan Exp $
 */

public interface ApiConverter {
  /**
   * Converts a class name to an url and adds the url to an
   * Writer. The url usually shows som API information about
   * the class e.g. for Java classes this points to the API
   * documentation on the Sun site.
   *
   * @param writer Writer to add the class url to
   * @param className Namee of the class to create pointer for
   */
  public void appendUrl(Writer writer, String className) throws IOException ;

  /**
   * Set the base Url for the Converter. A converter
   * creates an API pointer by combining an base url
   * and the name of a class.
   *
   * @param baseUrl Url to use when creating an API pointer
   */
  public void setBaseUrl(String baseUrl);

  /**
   * Get the base Url for the Converter. A converter
   * creates an API pointer by combining an base url
   * and the name of a class.
   *
   * @return baseUrl Url the converter uses when creating an API pointer
   */
  public String getBaseUrl();

  /**
   * Returns the name of the converter. This is used to configure
   * the BaseUrls and associate them with a concrete converter.
   *
   * @return name Name of the Converter, e.g. Java12
   */
  public String getName();
}
