/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.filter;

import java.text.MessageFormat;

import com.resolve.wiki.radeox.api.engine.ImageRenderEngine;
import com.resolve.wiki.radeox.api.engine.RenderEngine;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.filter.CacheFilter;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.LocaleRegexTokenFilter;
import com.resolve.wiki.radeox.regex.MatchResult;
import com.resolve.wiki.radeox.util.Encoder;

/*
 * 
 * UrlFilter finds http:// style URLs in its input and transforms them to <a href="url">url</a>
 *
 */

public class UrlFilter extends LocaleRegexTokenFilter implements CacheFilter
{
    private MessageFormat formatter;

    protected void setValues()
    {
        addKeyValue(getLocaleKey() + ".match", "([^\"'=]|^)((http|ftp)s?://(%[\\p{Digit}A-Fa-f][\\p{Digit}A-Fa-f]|[-_.!~*';/?:@#&=+$,\\p{Alnum}])+)");
        addKeyValue(getLocaleKey() + ".print", "<span class=\"nobr\">{0}<a href=\"{1}\">{2}</a></span>");
    }

    protected String getLocaleKey()
    {
        return "filter.url";
    }

    public void setInitialContext(InitialRenderContext context)
    {
        super.setInitialContext(context);
        String outputTemplate = values.get(getLocaleKey() + ".print");
        formatter = new MessageFormat("");
        formatter.applyPattern(outputTemplate);
    }

    public void handleMatch(StringBuffer buffer, MatchResult result, FilterContext context)
    {
        buffer.append(result.group(1));
        RenderEngine engine = context.getRenderContext().getRenderEngine();
        String externalImage = "";
        if (engine instanceof ImageRenderEngine)
        {
            buffer.append(((ImageRenderEngine) engine).getExternalImageLink());
        }

        buffer.append(formatter.format(new Object[] { externalImage, Encoder.escape(result.group(2)),
                Encoder.toEntity(result.group(2).charAt(0)) + result.group(2).substring(1) }));
    }
}
