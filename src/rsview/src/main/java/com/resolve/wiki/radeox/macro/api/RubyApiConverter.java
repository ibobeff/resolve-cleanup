/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.api;

import java.io.IOException;
import java.io.Writer;

/**
 * Converts a Ruby class name to an Ruby api doku url
 *
 */

public class RubyApiConverter extends BaseApiConverter {

  public void appendUrl(Writer writer, String className) throws IOException {
    writer.write(baseUrl);
    writer.write(className.toLowerCase());
    writer.write(".html");
  }

  public String getName() {
    return "Ruby";
  }
}
