/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.web;

import java.net.URL;
import java.util.Map;

import com.resolve.services.constants.WikiAction;

/**
 * This interface is for creating URL APIs
 * 
 * @author jeet.marwah
 *
 */
public interface WikiURLFactory
{
	//this should be the first method to be called for the URL 
	public void initialize(WikiRequestContext context);
	
//	public URL createAttachmentURL(Map<String, String> values, WikiRequestContext wikiRequestContext, String fileName, String action) throws WikiException;
	
	/**
	 * replaces the string in the url
	 * eg.
	 * 	String requestURL = "http://localhost:8080/resolve/service/wiki/save/Test/test2";
	 * 	String findStr = "save";
	 * 	String replacewith = "edit";
	 * 	
	 * 	System.out.println(createForwardUrl(requestURL, findStr, replacewith));
	 * 			
	 * @param requestURL
	 * @param findStr
	 * @param replaceWith
	 * @return
	 */
//	public String createForwardUrl(String requestURL, String findStr, String replaceWith);
	
	
	public String createForwardUrl(WikiAction wikiAction, String namespace, String docName, String parameters);
	public String createForwardUrl(WikiAction wikiAction, String docFullName, String queryString);
	
//    public void init(WikiRequestContext context);

//    public URL createURL(String web, String name, WikiRequestContext context);
//
//    public URL createURL(String web, String name, String action, WikiRequestContext context);
//
//    public URL createURL(String web, String name, String action, boolean redirect, WikiRequestContext context);
//
    public URL createURL(Map<String, String> values, WikiRequestContext context, String action, String querystring, String anchor);
    public URL createURL(Map<String, String> values, String action, String querystring, String anchor);
    
//
//    public URL createExternalURL(String web, String name, String action, String querystring, String anchor, WikiRequestContext context);
//
//    public URL createURL(String web, String name, String action, String querystring, String anchor, String xwikidb, WikiRequestContext context);
//
//    public URL createExternalURL(String web, String name, String action, String querystring, String anchor, String xwikidb, WikiRequestContext context);
//
//    public URL createSkinURL(String filename, String skin, WikiRequestContext context);
//
//    public URL createSkinURL(String filename, String web, String name, WikiRequestContext context);
//
//    public URL createSkinURL(String filename, String web, String name, String xwikidb, WikiRequestContext context);
//
//    public URL createAttachmentURL(String filename, String web, String name, String action, String querystring, WikiRequestContext context);
//
//    public URL createAttachmentURL(String filename, String web, String name, String action, String querystring, String xwikidb, WikiRequestContext context);
//
//    public URL createAttachmentRevisionURL(String filename, String web, String name, String revision, String querystring, WikiRequestContext context);
//
//    public URL createAttachmentRevisionURL(String filename, String web, String name, String revision, String querystring, String xwikidb, WikiRequestContext context);
//
//    public URL getRequestURL(WikiRequestContext context);
//
    public String getURL(URL url, WikiRequestContext context);
    public String getURL(URL url);
    
    public String getServerURL();

}
