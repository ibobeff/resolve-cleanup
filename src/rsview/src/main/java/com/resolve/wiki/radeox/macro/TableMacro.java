/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.radeox.macro.table.Table;
import com.resolve.wiki.radeox.macro.table.TableBuilder;

import java.io.IOException;
import java.io.Writer;

/*
 * Macro for defining and displaying tables. The rows of the table are
 * devided by newlins and the columns are divided by pipe symbols "|".
 * The first line of the table is rendered as column headers.
 * {table}
 *  A|B|C
 *  1|2|3
 * {table}
 *
 */

public class TableMacro extends BaseLocaleMacro {
  private String[] paramDescription = {};

  public String[] getParamDescription() {
    return paramDescription;
  }

  public String getLocaleKey() {
    return "macro.table";
  }

  public void execute(Writer writer, MacroParameter params)
      throws IllegalArgumentException, IOException {

    String content = params.getContent();

    if (null == content) throw new IllegalArgumentException("TableMacro: missing table content");

    content = content.trim() + "\n";

    Table table = TableBuilder.build(content);
    table.calc(); // calculate macros like =SUM(A1:A3)
    table.appendTo(writer);
    return;
  }
}
