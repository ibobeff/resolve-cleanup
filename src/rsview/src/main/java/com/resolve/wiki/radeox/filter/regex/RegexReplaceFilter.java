/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter.regex;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.regex.Matcher;
import com.resolve.wiki.radeox.regex.Pattern;

/*
 * Class that applies a RegexFilter, can be subclassed for special Filters.
 * Regular expressions in the input are replaced with strings.
 * 
 */

public class RegexReplaceFilter extends RegexFilter
{
    public RegexReplaceFilter()
    {
        super();
    }

    public RegexReplaceFilter(String regex, String substitute)
    {
        super(regex, substitute);
    }

    public RegexReplaceFilter(String regex, String substitute, boolean multiline)
    {
        super(regex, substitute, multiline);
    }

    public String filter(String input, FilterContext context)
    {
        String result = input;
        int size = pattern.size();
        Pattern p;
        String s;
        for (int i = 0; i < size; i++)
        {
            p = (Pattern) pattern.get(i);
            s = (String) substitute.get(i);
            try
            {
                Matcher matcher = Matcher.create(result, p);
                result = matcher.substitute(s);

                // Util.substitute(matcher, p, new Perl5Substitution(s,
                // interps), result, limit);
            }
            catch (Exception e)
            {
                // log.warn("<span class=\"error\">Exception</span>: " + this +
                // ": " + e);
                Log.log.warn("Exception for: " + this + " " + e);
            }
            catch (Error err)
            {
                // log.warn("<span class=\"error\">Error</span>: " + this + ": "
                // + err);
                Log.log.warn("Error for: " + this);
            }
        }
        return result;
    }
}
