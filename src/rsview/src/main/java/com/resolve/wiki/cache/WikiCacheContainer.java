/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.cache;

import java.util.Date;

/**
 * The purpose of this class is to have refrences to the objects that needs to be cached. Also, to keep track of the time.
 * This is the object that gets stored in the cache.
 * 
 * @author jeet.marwah
 * 
 */

public class WikiCacheContainer
{

	private String key;
	private Object content;
	private int cacheDuration;
	private Date date;

	public WikiCacheContainer(String key, Object content, int cacheDuration, Date date)
	{
		this.key = key;
		this.content = content;
		this.cacheDuration = cacheDuration;
		this.date = date;
	}

	public String getKey()
	{
		return key;
	}

	public Object getContent()
	{
		return content;
	}

	public int getCacheDuration()
	{
		return cacheDuration;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public boolean isValid()
	{
		Date cdate = new Date();
		return ((cdate.getTime() - getDate().getTime()) < cacheDuration * 1000);
	}

}//end of class
