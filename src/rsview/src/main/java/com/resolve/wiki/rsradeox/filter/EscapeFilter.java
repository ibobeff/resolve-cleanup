/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.filter;

import com.resolve.wiki.radeox.filter.CacheFilter;
import com.resolve.wiki.radeox.filter.FilterPipe;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.filter.regex.LocaleRegexTokenFilter;
import com.resolve.wiki.radeox.regex.MatchResult;
import com.resolve.wiki.radeox.util.Encoder;

/*
 * Transforms multiple \ into single backspaces and escapes other characters.
 *
 * @version $Id: EscapeFilter.java,v 1.13 2004/04/15 13:56:14 stephan Exp $
 */

public class EscapeFilter extends LocaleRegexTokenFilter implements CacheFilter
{
        protected void setValues()
    {
        addKeyValue(getLocaleKey() + ".match", "\\\\\\\\()|\\\\([^n])");
    }

    protected String getLocaleKey()
    {
        return "filter.escape";
    }

    public void handleMatch(StringBuffer buffer, MatchResult result, FilterContext context)
    {
        buffer.append(handleMatch(result, context));
    }

    public String handleMatch(MatchResult result, FilterContext context)
    {
        if (result.group(1) == null)
        {
            String match = result.group(2);
            if (match == null)
            {
                match = result.group(3);
            }
            if ("\\".equals(match))
            {
                return "\\\\";
            }
            return Encoder.toEntity(match.charAt(0));
        }
        else
        {
            return "&#92;";
        }
    }

    public String[] before()
    {
        return FilterPipe.FIRST_BEFORE;
    }
}
