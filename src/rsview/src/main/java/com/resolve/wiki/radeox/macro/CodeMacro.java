/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.HttpUtil;
import com.resolve.wiki.radeox.api.engine.context.InitialRenderContext;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.filter.context.BaseFilterContext;
import com.resolve.wiki.radeox.filter.context.FilterContext;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.rsradeox.macro.code.DefaultCodeFormatter;
import com.resolve.wiki.rsradeox.macro.code.JavaCodeFormatter;
import com.resolve.wiki.rsradeox.macro.code.NullCodeFormatter;
import com.resolve.wiki.rsradeox.macro.code.SourceCodeFormatter;
import com.resolve.wiki.rsradeox.macro.code.SqlCodeFormatter;
import com.resolve.wiki.rsradeox.macro.code.XmlCodeFormatter;

/*
 * Macro for displaying programming language source code. CodeMacro knows about
 * different source code formatters which can be plugged into radeox to
 * display more languages. CodeMacro displays Java, XML or SQL code.
 * 
 *
 */

public class CodeMacro extends LocalePreserved
{
	private static String START = "<div class=\"code\"><pre>";//this is to removed the dependency from the properties file
	private static String END = "</pre></div>";

	private Map<String, SourceCodeFormatter> formatters;
	private FilterContext nullContext = new BaseFilterContext();

		private String[] paramDescription = { "?1: syntax highlighter to use, defaults to java" };

	public String[] getParamDescription()
	{
		return paramDescription;
	}

	public String getLocaleKey()
	{
		return "macro.code";
	}

	public void setInitialContext(InitialRenderContext context)
	{
		super.setInitialContext(context);
	}

	public CodeMacro()
	{
		formatters = new HashMap<String, SourceCodeFormatter>();
		formatters.put("default", new DefaultCodeFormatter());
		formatters.put("java", new JavaCodeFormatter());
		formatters.put("none", new NullCodeFormatter());
		formatters.put("sql", new SqlCodeFormatter());
		formatters.put("xml", new XmlCodeFormatter());

		addSpecial('[');
		addSpecial(']');
		addSpecial('{');
		addSpecial('}');
		addSpecial('*');
		addSpecial('-');
		addSpecial('\\');
	}

	public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
	{

		SourceCodeFormatter formatter = null;

		if (params.getLength() == 0 || !formatters.containsKey(params.get("0")))
		{
			formatter = (SourceCodeFormatter) formatters.get(initialContext.get(RenderContext.DEFAULT_FORMATTER));
			if (null == formatter)
			{
				System.err.println("Formatter not found.");
				formatter = (SourceCodeFormatter) formatters.get("java");
			}
		}
		else
		{
			formatter = (SourceCodeFormatter) formatters.get(params.get("0"));
		}

		String result = formatter.filter(params.getContent(), nullContext);

		writer.write(START);
        writer.write(HttpUtil.sanitizeValue(replace(result.trim())));
		writer.write(END);
		return;
	}
}
