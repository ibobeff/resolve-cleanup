/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rights;

import java.util.List;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * Action task right service interface
 * 
 * @author jeet.marwah
 *
 */
public interface ActionTaskRightService
{
    public boolean checkAccess(WikiAction wikiAction, ResolveActionTask resolveActionTask, WikiRequestContext context) throws WikiException;
    
    public boolean checkAccess(WikiAction wikiAction, String actionTaskFullName, String username) throws WikiException;
    
    public boolean checkAccess(WikiAction wikiAction, String actionTaskFullName, WikiRequestContext context) throws WikiException;

    public boolean checkAccess(String actionTaskFullName, WikiRequestContext context) throws WikiException;
    
    public AccessRights getAccessRightsFor(String username, List<PropertiesVO> lProps, ResolveActionTask resolveActionTask) throws WikiException;
    
    public AccessRights updateAccessRightsFor(String username, String id, String simpleName) throws WikiException;

}//ActionTaskRightService
