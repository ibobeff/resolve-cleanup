/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.attachment;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Map;

import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.wiki.store.WikiStore;

/**
 * Usage:
 * 
 *  def params = new HashMap()

    params.put("Runbook.Test5", "C:/project/resolve3/dist/tmp/system.zip");
    params.put("Runbook.CO Equipment", "C:/project/resolve3/dist/tmp");
    params.put("Runbook.Extjs1", "C:/project/resolve3/dist/tmp|C:/project/resolve3/dist/tomcat/LICENSE|C:/project/resolve3/dist/lib/activation.jar");
    
    
    ESB.sendMessage('RSVIEW', 'MWiki.importBulkAttachments', params);
 * 
 * @author jeet.marwah
 *
 */
public class FileToDBUtil
{
    private static final String SEPERATOR = "|";
    private static final String REGEX_OF_SEPERATOR = "\\|";
    
    private String[] filesAndFolderLocations = {};
    
    private WikiStore store = null;
    private WikiDocument document = null;
    private WikiAttachment wikiAttachment = null;
    private WikiAttachmentContent wikiAttachmentContent = null;
    
    private Map<String, Object> params = null;

    public FileToDBUtil(Map<String, Object> params) throws Exception
    {
        this.params = params;
        
        // Get reference of HibernateStore from Spring context.
        store = (WikiStore) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_HIBERNATESTORE);
    }
    
    /**
     * the key is the docname
     * value is the location where the file is 
     */
    public void importAllAttachments()
    {
        Iterator<String> keys = params.keySet().iterator();
        while(keys.hasNext())
        {
            String docname = keys.next();
            String doclocations = (String) params.get(docname);
            
            if(doclocations.indexOf(SEPERATOR) > -1)
            {
                filesAndFolderLocations = doclocations.split(REGEX_OF_SEPERATOR);
            }
            else
            {
                filesAndFolderLocations = new String[1];
                filesAndFolderLocations[0] = doclocations;
            }
            
            try
            {
                Log.log.trace("*** Start the UPLOAD for document: " + docname);
                document  = store.find(docname);
                
                if (document != null)
                {
                    updateFileToDB();
                    Log.log.trace("*** END Of the upload ");
                }
            }
            catch(Throwable t)
            {
                Log.log.error("Error importing the attachments for " + docname + " having location " + doclocations, t);
            }
        }
    }
    
    private void updateFileToDB()
    {
        for(String doclocation : filesAndFolderLocations)
        {
            File folder = FileUtils.getFile(doclocation);
            if(folder.exists())
            {
                if(folder.isDirectory())
                {
                    File[] files = folder.listFiles();
                    
                    for (File file : files)
                    {
                        if (file.isFile())
                        {
                           update(file);
                        }
                    }//end of for loop
                }//end of if
                else if(folder.isFile())
                {
                    update(folder);
                }
            }//end of if
            else
            {
                Log.log.error("This File/Folder does not exist: " + doclocation);
            }
        }//end of for loop
            
    }//updateFileToDB
    
    private void update(File file)
    {
        try
        {
            updateFile(file);
        }
        catch (Throwable e)
        {
            // ignore failures
            Log.log.error("Error with file: " + file.getName(), e);
        }
    }
    
    private void updateFile(File file) throws Throwable
    {
        reinitialize();
        
        //file attributes
        long sizeInBytes = file.length();
        String filename = file.getName();

        //read the file 
        byte[] data = new byte[(int) sizeInBytes];
        FileInputStream inputFileInputStream = new FileInputStream(file);
        inputFileInputStream.read(data);
        inputFileInputStream.close();
        
        //update the objects
        wikiAttachmentContent.setUContent(data);
        wikiAttachmentContent.setSysUpdatedOn(GMTDate.getDate());
        wikiAttachmentContent.setSysCreatedOn(GMTDate.getDate());

        wikiAttachment.setUFilename(filename);
        wikiAttachment.setUSize(data.length);
        wikiAttachment.setULocation("");
        wikiAttachment.setUType("DB");
        wikiAttachment.usetWikiAttachmentContent(wikiAttachmentContent);
        wikiAttachment.setSysUpdatedOn(GMTDate.getDate());
        wikiAttachment.setSysCreatedOn(GMTDate.getDate());
        wikiAttachment.setSysCreatedBy("system");
        
        //persist
        store.addAttachment(document, wikiAttachment, false);
        
    }
    
    private void reinitialize()
    {
        wikiAttachment = new WikiAttachment();
        wikiAttachmentContent = new WikiAttachmentContent();
    }//reset

}
