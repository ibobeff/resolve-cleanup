/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.plugin;

import com.resolve.wiki.web.WikiRequestContext;

/**
 * This interface provides an interface for plugins which help the renders to process
 * 
 * @author jeet.marwah
 * 
 */
public interface WikiPluginInterface
{
	/**
	 * Returns fully qualified class name of the plugin
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * This is the first interface that gets called before rendering is applied on a wiki page
	 * 
	 * @param context
	 */
	public void beginRendering(WikiRequestContext context);

	/**
	 * This method is called after all the rendering is done. It basically does a lot of cleanup
	 * 
	 * @param context
	 */
	public void endRendering(WikiRequestContext context);

}
