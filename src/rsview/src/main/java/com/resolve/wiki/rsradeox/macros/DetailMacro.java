/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

/**
 * Macro for defining and displaying the URL to Resolve Execute Request
 * for ActionTask execution. The # namespace is optional (if required).
 * 
 * DETAIL DEFINITION
 *   {detail:[refreshInterval=<refreshInterval in seconds>]} [DEFINTION] {detail}
 *   
 * DEFINITION
 *   syntax = task#namespace{wiki::nodeId}
 *   
 */
public class DetailMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.detail";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        //Get parameters
        int refreshInterval = Integer.valueOf(StringUtils.isNotEmpty(params.get("refreshInterval")) ? params.get("refreshInterval") : "5");
        if (refreshInterval < 5)
        {
        	refreshInterval = 5;
        }
        int refreshCountMax = Integer.valueOf(StringUtils.isNotEmpty(params.get("refreshCountMax")) ? params.get("refreshCountMax") : "60");
        if (refreshCountMax < 1)
        {
        	refreshCountMax = 60;
        }
        String onRenderListenerName = StringUtils.isNotEmpty(params.get("onRender"))?params.get("onRender"):"";
        onRenderListenerName = HttpUtil.sanitizeValue(onRenderListenerName,"FileName", 200);
        Boolean progress = Boolean.valueOf(StringUtils.isNotEmpty(params.get("progress")) ? params.get("progress") : "false");
        int max = Integer.valueOf(StringUtils.isNotEmpty(params.get("max")) ? params.get("max") : "1");
        int offset = Integer.valueOf(StringUtils.isNotEmpty(params.get("offset")) ? params.get("offset") : "0");
        //Unique id for the div to render to
        String divId = "details_" + JavaUtils.generateUniqueNumber();

        //Get the content (this is going to be the optional list of action tasks
        String content = params.getContent();
        content = HttpUtil.sanitizeValue(content);

        String[] split = content.trim().replace("\r", "").split("\n");
        StringBuffer actionTaskParameters = new StringBuffer();
        for (String actionTask : split)
        {
            if (StringUtils.isNotBlank(actionTask))
            {
                if (actionTaskParameters.length() > 0) actionTaskParameters.append(",");
                actionTaskParameters.append("'").append(actionTask.replace("'", "\\'")).append("'");
            }
        }

        //Build up div and script to render
        StringBuffer str = new StringBuffer();
        str.append("<div id=\"%1$s\"></div>\n");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.Details',\n");
        str.append("        refreshInterval : %2$s,\n");
        str.append("        refreshCountMax : %8$s,\n");
        str.append("        progress : %5$s,\n");
        str.append("        maximum : %6$s,\n");
        str.append("        offset : %7$s,\n");
        if(!StringUtils.isEmpty(onRenderListenerName))
            str.append("        onRender: %4$s,\n");
        str.append("        actionTasks : [%3$s]\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('%1$s');\n");
        str.append("});\n");
        str.append("</script>\n");

        String result = String.format(str.toString(), divId, refreshInterval, actionTaskParameters,onRenderListenerName,progress,max,offset,refreshCountMax);
        writer.write(result);
    }
}
