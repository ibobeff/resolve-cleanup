/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.api.engine;

import com.resolve.wiki.radeox.api.engine.context.RenderContext;

import java.io.IOException;
import java.io.Writer;
import java.io.Reader;

/**
 * Interface for RenderEngines. A RenderEngine renders
 * a input string to an output string with the help
 * of filters.
 *
 */

public interface RenderEngine {
  /**
   * Name of the RenderEngine. This is used to get a RenderEngine instance
   * with EngineManager.getInstance(name);
   *
   * @return name Name of the engine
   */
  public String getName();

  /**
   * Render an input with text markup and return a String with
   * e.g. HTML
   *
   * @param content String with the input to render
   * @param context Special context for the render engine, e.g. with
   *                configuration information
   * @return result Output with rendered content
   */
  public String render(String content, RenderContext context);

  /**
   * Render an input with text markup and an write the result
   * e.g. HTML to a writer
   *
   * @param out Writer to write the output to
   * @param content String with the input to render
   * @param context Special context for the render engine, e.g. with
   *                configuration information
   */

  public void render(Writer out, String content, RenderContext context) throws IOException;

  /**
   * Render an input with text markup from a Reader and write the result to a writer
   *
   * @param in Reader to read the input from
   * @param context Special context for the render engine, e.g. with
   *                configuration information
   */
  public String render(Reader in, RenderContext context) throws IOException;

//  public RenderContext getRenderContext();
  //public void render(Writer out, Reader in, RenderContext context);
}
