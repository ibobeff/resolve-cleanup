/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.filter;

import com.resolve.wiki.radeox.filter.regex.LocaleRegexReplaceFilter;

/*
 * Italic finds ~~text~~ in its input and transforms this to <i>text</i>
 */

public class ItalicFilter extends LocaleRegexReplaceFilter implements CacheFilter
{

    public static final String LOCAL_KEY = "filter.italic";

    @Override
    protected void setValues()
    {
        addKeyValue(LOCAL_KEY + ".match", "~~(.*?)~~");
        addKeyValue(LOCAL_KEY + ".print", "<i class=\"italic\">$1</i>");
    }

    protected String getLocaleKey()
    {
        return LOCAL_KEY;
    }
}
