/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;

/*
 * Macro for defining and displaying the URL to Resolve Execute Request
 * for ActionTask execution. The # namespace is optional (if required).
 * 
 * ACTION DEFINITION
 *   {action:[manual|[CONDITION]|[EXPRESSION]]} [DEFINTION] {action}
 *   
 *   [DEFINITION] = [TASKNAME]
 *   [DEFINITION] = [TASKNAME] ? [ACTIONPARAM]
 *   [DEFINITION] = [ACTIONTEXT] > [TASKNAME]
 *   [DEFINITION] = [ACTIONTEXT] > [TASKNAME] ? [ACTIONPARAM]
 * 
 *   [CONDITION] = condition|[COND_EVAL]|[COND_EVAL]|...
 *   
 *     [COND_EVAL] = completion == <TRUE|FALSE>
 *     [COND_EVAL] = condition == <GOOD|BAD>
 *     [COND_EVAL] = severity == <CRITICAL|SEVERE|WARNING|GOOD>
 *     [COND_EVAL] = merge == <ANY|ALL>
 *   
 * ACTION TASKNAME
 *   [TASKNAME] = action_taskname
 *   [TASKNAME] = action_taskname # action_namespace
 *   [TASKNAME] = action_namespace.action_taskname
 * 
 * ACTION TEXT
 *   [ACTIONTEXT] actiontext
 * 
 * ACTION PARAM
 *   [ACTIONPARAM] = [PARAMNAME]=[PARAMVAL]&...
 *   
 *   [PARAMNAME] = TARGET_GUID | TARGET_IPADDR | TARGET_NAME | PARAM_[name]
 *   [PARAMVAL] = value
 *
 */

public class ActionMacro extends BaseLocaleMacro
{
    private boolean isDebug = false;
    private boolean showPrompt = true;//prompt to get the input

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        try
        {
            String prompt = params.get("prompt");
            showPrompt = (StringUtils.isNotEmpty(prompt) && prompt.equalsIgnoreCase("false")) ? false : true;

            String content = params.getContent();
            if (!StringUtils.isEmpty(content))
            {
                content = content.trim();

                String text = null;
                String name = null;
                String namespace = null;
                String actionparam = null;

                // extract description text
                String[] def = content.split(">");
                if (def.length == 1)
                {
                    content = def[0].trim();
                }
                else
                {
                    text = def[0].trim();
                    content = def[1].trim();
                }
                text = HttpUtil.sanitizeValue(text);

                // extract name and parameter
                def = content.split("\\?");
                content = def[0].trim();
                if (def.length > 1)
                {
                    actionparam = def[1].trim();
                }
                actionparam = HttpUtil.sanitizeValue(actionparam);
                
                // extract name and namespace
                int pos = content.indexOf('#');
                if (pos > 0)
                {
                    name = content.substring(0, pos);
                    namespace = content.substring(pos + 1, content.length());
                }
                else
                {
                    name = content;
                }
                name = HttpUtil.sanitizeValue(name);
                namespace = HttpUtil.sanitizeValue(namespace);
                
                // get xwiki context
                RenderContext rcontext = params.getContext();
                //WikiRequestContext context = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);

                // init document fullname
                String docname = (String) rcontext.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
                docname = HttpUtil.sanitizeValue(docname);
                
                // init userid
                String userid = (String) rcontext.get(Constants.HTTP_REQUEST_USERNAME);
                userid = HttpUtil.sanitizeValue(userid);
                
                // init action
                String action = params.get("action");
                action = HttpUtil.sanitizeValue(action, HttpUtil.VALIDATOR_FILENAME, 200, HttpUtil.HTML_ENCODING);
                
                // append URL parameters to actionparam
//                HttpServletRequest request = context.getHttpServletRequest();
//                Map<String, String[]> urlParams = null;
//                if (request != null)
//                {
//                    urlParams = request.getParameterMap();
//                }
//                else
//                {
//                    urlParams = new HashMap<String, String[]>();
//                }

                writer.write(getActionTaskLink(name, namespace, action, actionparam, text, docname, userid));
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    
    }

    private String getActionTaskLink(String name, String namespace, String action, String actionparams, String text, String docname, String userid)
    {
        String result = null;

        if (StringUtils.isBlank(text))
        {
            text = name;
        }

        //ActionPlugin.getActionTask(name, namespace);//get the sys_id from db
//        ResolveActionTask task = wiki.getWikiStore().getActionTask(namespace, name);
        ResolveActionTaskVO task = ServiceHibernate.getActionTaskWithReferencesExceptGraph(null, name + "#" + namespace, userid);
        if (task == null)
        {
            result = "ERROR: Unknown ActionTask name: " + name;
            if (namespace != null)
            {
                result += " namespace: " + namespace;
            }
        }
        else
        {
            if (StringUtils.isEmpty(action))
            {
                action = "TASK";
            }

            if (UserUtils.checkTaskPermission(task.getSys_id(), userid))
            {
                if (showPrompt)
                {
                    result = "<a href='#' id=\"resolve_action\" onclick=\"return executeActionTaskPopUp('" + HttpUtil.sanitizeValue(task.getSys_id()) + "', '" + HttpUtil.sanitizeValue(task.getUNamespace()) + "', '" + HttpUtil.sanitizeValue(task.getUName()) + "', " + isDebug + ",'"+actionparams+"'); return false;\">" + text + "</a>";
                }
                else
                {
                    result = "<a id=\"resolve_action\" href=\"/resolve/service/execute?action=" + action + "&wiki=" + docname + "&actionid=" + HttpUtil.sanitizeValue(task.getSys_id()) + "&" + actionparams + "\">" + text + "</a>";
                }
            }
        }

        return result;
    }

    @Override
    public String getLocaleKey()
    {
        return "macro.action";
    }

}
