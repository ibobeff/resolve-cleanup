/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.util.HttpUtil;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

public class ImageMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.image";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        RenderContext context = params.getContext();
        WikiRequestContext wikiRequestcontext = (WikiRequestContext) context.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestcontext.getWiki();

        String fullName = (String) context.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        String namespace = (String) context.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) context.get(ConstantValues.WIKI_DOCNAME_KEY);
        if (StringUtils.isEmpty(fullName))
        {
            fullName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
            namespace = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
            wikiDocName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        }
        fullName = HttpUtil.sanitizeValue(fullName);
        namespace = HttpUtil.sanitizeValue(namespace);
        wikiDocName = HttpUtil.sanitizeValue(wikiDocName);

        String img = params.get("text", 0);
        img = HttpUtil.sanitizeValue(img, HttpUtil.VALIDATOR_FILENAME, 200, HttpUtil.HTML_ENCODING);
        String height = (StringUtils.isNotEmpty(params.get("height")) ? params.get("height") : "0");
        height = HttpUtil.sanitizeValue(height, HttpUtil.NUMBERWITHPERCENT, 10);
        String width = (StringUtils.isNotEmpty(params.get("width")) ? params.get("width") : "0");
        width = HttpUtil.sanitizeValue(width, HttpUtil.NUMBERWITHPERCENT, 10);
        int border = Integer.valueOf((StringUtils.isNotEmpty(params.get("border")) ? params.get("border") : "0"));
        String borderColor = StringUtils.isNotEmpty(params.get("borderColor")) ? params.get("borderColor") : "black";
        borderColor = HttpUtil.sanitizeValue(borderColor);
        boolean zoom = params.get("zoom") != null ? Boolean.valueOf(params.get("zoom")) : false;

        WikiAttachmentVO att = null;
        try
        {
            att = ServiceWiki.findWikiAttachmentVO(fullName, null, img, false);
        }
        catch (Exception e)
        {
            Log.log.error("error getting the attachment " + img + " for document " + fullName, e);
        }

        StringBuffer str = new StringBuffer();
        if (att != null)
        {
            String querystring = "attachFileName=" + img + "&attach=" + att.getSys_id();
			String url = wiki.getWikiURLFactory().createForwardUrl(WikiAction.download,
					URLEncoder.encode(namespace, StandardCharsets.UTF_8.name()),
					URLEncoder.encode(wikiDocName, StandardCharsets.UTF_8.name()), querystring);
            
			if (StringUtils.isNotEmpty(url)) {
    			String[] urlParams = url.split("\\?");
    			String[] token = JspUtils.getCSRFTokenForPage(wikiRequestcontext.getHttpServletRequest(), urlParams[0]);
    			url = String.join("", urlParams[0], "?", urlParams[1], "&", token[0], "=", token[1]);
			}

            str.append("<img ");
            if (zoom)
            {
                str.append("class=\"rs-link\" onclick=\"javascript:displayLargeImage('" + url + "', '" + img + "')\" ");
            }
            str.append("style=\"border:" + border + "px solid " + borderColor + ";");
            if (StringUtils.isNotBlank(height) && !"0".equalsIgnoreCase(height))
            {
                str.append("height:" + height + (StringUtils.isInteger(height) ? "px" : "") + ";");
            }
            if (StringUtils.isNotBlank(width)) {
                if(!"0".equalsIgnoreCase(width)) {
                    str.append("width:" + width + (StringUtils.isInteger(width) ? "px" : "") + ";");
                }
                else
                {
                    str.append("width:100%;");
                }
            }           
            str.append("\"");
            str.append("src=\"").append(url).append("\" ");
            str.append("alt=\"").append(img).append("\" />");
        }
        else
        {
            str.append("<b>Error: ").append(img).append(" image file does not exist.</b>");
        }
        
        writer.write(str.toString());
    }
}
