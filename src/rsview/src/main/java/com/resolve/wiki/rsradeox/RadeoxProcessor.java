/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox;

import java.util.Map;

import com.resolve.rsview.main.RSContext;
import com.resolve.services.constants.ConstantValues;
import com.resolve.wiki.radeox.EngineManager;
import com.resolve.wiki.radeox.api.engine.RenderEngine;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This is like a facade for the Radeox processing of the content
 * 
 * @author jeet.marwah
 * 
 */
public class RadeoxProcessor
{
	//spring injected
	private EngineManager engineManager;
	
	/**
	 * Render the content and return the rendered result
	 * 
	 * @param content
	 * @return
	 */
	public String renderRadeox(String content, Map<String, String> values, WikiRequestContext context, String engineName)
	{
		String result = content;
		RenderContext renderContext = (RenderContext) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WIKIRENDERCONTEXT);//renderEngine.getRenderContext();
		
//		if(renderEngine == null) {
//			renderEngine = new BaseRenderEngine();
//			renderContext = renderEngine.getRenderContext();
//		}
		

		//Get a handle to the Radeox engine and render it
		RenderEngine renderEngine = engineManager.getRenderEngineInstance();
		if(engineName != null)
		{
			renderEngine = engineManager.getRenderEngineInstance(engineName);
		}
		else
		{
			renderEngine = engineManager.getRenderEngineInstance();
		}
		
		renderContext.setRenderEngine(renderEngine);//just making sure that they are connected.
//		renderContext.set(ConstantValues.WIKI_DOCUMENT_NAME, wikiDoc);//this is used to create URLs in different places in Radeox macros
		renderContext.set(ConstantValues.WIKI_REQUEST_CONTEXT, context);//this has handle to everything
		if (values != null)
		{
		    for (Map.Entry entry : values.entrySet())
		    {
		        renderContext.set((String)entry.getKey(), entry.getValue());
		    }
		}
		result = renderEngine.render(result, renderContext);

		return result;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Getters and Setters
	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public EngineManager getEngineManager()
	{
		return engineManager;
	}
	public void setEngineManager(EngineManager engineManager)
	{
		this.engineManager = engineManager;
	}
}
