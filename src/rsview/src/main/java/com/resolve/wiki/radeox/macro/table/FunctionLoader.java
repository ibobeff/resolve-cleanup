/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.macro.table;

import com.resolve.util.Log;
import com.resolve.wiki.radeox.macro.PluginLoader;
import com.resolve.wiki.radeox.macro.Repository;

/**
 * Plugin loader for table functions
 * 
 */

public class FunctionLoader extends PluginLoader
{
    protected static FunctionLoader instance;

    public static synchronized PluginLoader getInstance()
    {
        if (null == instance)
        {
            instance = new FunctionLoader();
        }
        return instance;
    }

    public Class getLoadClass()
    {
        return Function.class;
    }

    /**
     * Add a plugin to the known plugin map
     * 
     * @param plugin
     *            Function to add
     */
    public void add(Repository repository, Object plugin)
    {
        if (plugin instanceof Function)
        {
            repository.put(((Function) plugin).getName().toLowerCase(), plugin);
        }
        else
        {
            Log.log.debug("FunctionLoader: " + plugin.getClass() + " not of Type " + getLoadClass());
        }
    }

}
