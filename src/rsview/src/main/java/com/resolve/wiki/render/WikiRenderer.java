/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.render;

import java.util.Map;

import com.resolve.services.exception.WikiException;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This interface should be implemented by all the Renderers that will render
 * the wiki doc.
 */

public interface WikiRenderer
{
    /**
     * This method actually renders the wiki content. Sets the rendered content
     * in the WikiDOcument object - renderedContent attribute
     * 
     * @param wikiDoc
     * @param context
     * @return
     * @throws WikiException
     */
    // public String render(String content, WikiDocument wikiDoc,
    // WikiRequestContext context);
    public String render(String content, Map<String, String> values, WikiRequestContext context);

    public String preRender(String content, Map<String, String> values, WikiRequestContext context);

    public String postRender(String content, Map<String, String> values, WikiRequestContext context);
}
