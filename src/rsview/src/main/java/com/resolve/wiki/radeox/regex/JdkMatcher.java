/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.radeox.regex;

/*
 * Matcher matches regular expressions (Pattern) to input
 * Implementation for regex package in JDK 1.4
 *
 */

public class JdkMatcher extends Matcher {
  private JdkPattern pattern;
  private String input;
  private java.util.regex.Matcher internalMatcher;

  public String substitute(Substitution substitution) {
    MatchResult matchResult = new JdkMatchResult(internalMatcher);

    StringBuffer buffer = new StringBuffer();
    while (internalMatcher.find()) {
      internalMatcher.appendReplacement(buffer, "");
      substitution.handleMatch(buffer, matchResult);
    }
    internalMatcher.appendTail(buffer);
    return buffer.toString();
  }

  public String substitute(String substitution) {
    return internalMatcher.replaceAll(substitution);
  }

  protected java.util.regex.Matcher getMatcher() {
    return internalMatcher;
  }

  public JdkMatcher(String input, Pattern pattern) {
    this.input = input;
    this.pattern = (JdkPattern) pattern;
    internalMatcher = this.pattern.getPattern().matcher(this.input);

  }

  public boolean contains() {
    internalMatcher.reset();
    return internalMatcher.find();
  }

  public boolean matches() {
    return internalMatcher.matches();
  }
}