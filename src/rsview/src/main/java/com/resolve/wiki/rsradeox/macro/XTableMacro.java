/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.util.HttpUtil;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.radeox.macro.table.XTable;


/**
 * Macro for defining and displaying tables. The rows of the table are
 * devided by newlins and the columns are divided by pipe symbols "|".
 * The first line of the table is rendered as column headers.
 *
 *  {xtable} 
    col1:width=100&type=date&format=Y-m-d|col2:width=400|col3=:width=200
     data1|data2|data3
     data1|data2|data3
     data1|data2|data3
    {xtable}
    
    
    {xtable:width=200|height=400|sql=select * from xyz|fitwidth=<true|false (by default)>} 
    col1:width=100&type=date&format=Y-m-d|col2:width=400|col3=:width=200 
    {xtable}
    
    fitwidth=<true|false (by default) --> general use of it is if this is the only component on the wiki page, make this flag as true
    drilldown=/resolve/service/wiki/view/^col1/^col2?col1=val&....
    target=_blank|_parent|_self|_top

 *
 * @author jeet.marwah
 *
 */
public class XTableMacro extends BaseLocaleMacro
{
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String SQL = "sql";
    public static final String FITWIDTH = "fitwidth";
    public static final String DRILLDOWN = "drilldown";
    public static final String TARGET = "target";


	@Override
	public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
	{
	    //table related attributes	    
        String width = params.get(WIDTH);
        width= HttpUtil.sanitizeValue(width, "ResolveNumber", 100);

        String height = params.get(HEIGHT);
        height= HttpUtil.sanitizeValue(height, "ResolveNumber", 100);
        
        String sql = params.get(SQL);//sql to get the data
        sql= HttpUtil.sanitizeValue(sql, "ResolveText", 1000);
        String fitwidth = params.get(FITWIDTH);
        fitwidth= HttpUtil.sanitizeValue(fitwidth, HttpUtil.VALIDATOR_RESOLVENUMBER, 100);
        String drilldown = params.get(DRILLDOWN);
        drilldown= HttpUtil.sanitizeValue(drilldown, HttpUtil.VALIDATOR_RESOLVETEXT, 1000);
        String target = params.get(TARGET);
        target= HttpUtil.sanitizeValue(target);
        
        //col1:width=100&type=date&format=Y-m-d|col2:width=400|col3=:width=200 
        String content = params.getContent();
        content= HttpUtil.sanitizeValue(content);
        
        if (null == content)
            throw new IllegalArgumentException("XTableMacro: missing table content");

        content = content.trim() + "\n";
        
        XTable table = new XTable();
        table.width = getInt(width);
        table.height = getInt(height);
        table.sql = sql; 
        table.drilldown = drilldown;
        table.target = StringUtils.isNotBlank(target) ? target.trim() : "_self";
        table.fitwidth = StringUtils.isNotEmpty(fitwidth) && fitwidth.trim().equalsIgnoreCase("true") ? true : false;
        table.content = content;
        
        String js = table.getJavascriptDefinition();

        writer.write(js);
        return;

	}
	
	private int getInt(String val)
	{
	    int newval = -1;
	    try
	    {
	        newval = Integer.parseInt(val);
	    }
	    catch(Exception e)
	    {
	        
	    }
	    
	    return newval;
	}

	@Override
	public String getLocaleKey()
	{
		return "macro.xtable";
	}

}
