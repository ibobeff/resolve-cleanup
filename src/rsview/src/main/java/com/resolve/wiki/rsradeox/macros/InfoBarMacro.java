/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import com.resolve.services.constants.ConstantValues;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

/*
 * Macro for defining and displaying the InfoBar.
 */

public class InfoBarMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.infobar";
    }

    public void execute(Writer writer, MacroParameter params) throws IOException
    {
        RenderContext context = params.getContext();
        WikiRequestContext wikiRequestcontext = (WikiRequestContext) context.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        String sysId = wikiRequestcontext.getWikiDocumentAPI().getWikiDoc().getSys_id();
        sysId = HttpUtil.sanitizeValue(sysId,"ResolveUUID", 32);

        String fullName = (String) context.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        if (StringUtils.isEmpty(fullName))
        {
            fullName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        }
        fullName = HttpUtil.sanitizeValue(fullName,"FileName", 500);
        
        String height = StringUtils.isNotBlank(params.get("height")) ? params.get("height") : "400";
        height = HttpUtil.sanitizeValue(height,HttpUtil.VALIDATOR_RESOLVETEXT, 500);
        boolean displaySocialTab = params.get("social") == null ? true : Boolean.valueOf(params.get("social"));
        boolean displayFeedbackTab = params.get("feedback") == null ? true : Boolean.valueOf(params.get("feedback"));
        boolean displayFeedbackResultsTab = params.get("rating") == null ? true : Boolean.valueOf(params.get("rating"));
        boolean displayAttachmentsTab = params.get("attachments") == null ? true : Boolean.valueOf(params.get("attachments"));
        boolean displayHistoryTab = params.get("history") == null ? true : Boolean.valueOf(params.get("history"));
        boolean displayTagsTab = params.get("tags") == null ? true : Boolean.valueOf(params.get("tags"));
        boolean displayPageInfoTab = params.get("pageInfo") == null ? true : Boolean.valueOf(params.get("pageInfo"));

        String divId = "infoBar_" + JavaUtils.generateUniqueNumber();

        StringBuffer str = new StringBuffer();

        str.append("<div id=\"" + divId + "\"></div>");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("  if( clientVM.user ){\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.InfoBar',\n");
        str.append("        displaySocialTab : " + displaySocialTab + ",\n");
        str.append("        displayFeedbackTab : " + displayFeedbackTab + ",\n");
        str.append("        displayFeedbackResultsTab : " + displayFeedbackResultsTab + ",\n");
        str.append("        displayAttachmentsTab : " + displayAttachmentsTab + ",\n");
        str.append("        displayHistoryTab : " + displayHistoryTab + ",\n");
        str.append("        displayTagsTab : " + displayTagsTab + ",\n");
        str.append("        displayPageInfoTab : " + displayPageInfoTab + ",\n");
        str.append("        wikiName : '" + fullName + "',\n");
        str.append("        sysId : '" + sysId + "',\n");
        str.append("        height : " + (StringUtils.isInteger(height) ? height : "'" + height + "'") + "\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('" + divId + "');\n");
        str.append("  }\n");
        str.append("  else {\n");
        str.append("    clientVM.on('userChanged', function(){\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.InfoBar',\n");
        str.append("        displaySocialTab : " + displaySocialTab + ",\n");
        str.append("        displayFeedbackTab : " + displayFeedbackTab + ",\n");
        str.append("        displayFeedbackResultsTab : " + displayFeedbackResultsTab + ",\n");
        str.append("        displayAttachmentsTab : " + displayAttachmentsTab + ",\n");
        str.append("        displayHistoryTab : " + displayHistoryTab + ",\n");
        str.append("        displayTagsTab : " + displayTagsTab + ",\n");
        str.append("        displayPageInfoTab : " + displayPageInfoTab + ",\n");
        str.append("        wikiName : '" + fullName + "',\n");
        str.append("        sysId : '" + sysId + "',\n");
        str.append("        height : " + (StringUtils.isInteger(height) ? height : "'" + height + "'") + "\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('" + divId + "');\n");
        str.append("    });");
        str.append("  }\n");
        str.append("});");
        str.append("</script>\n");

        writer.write(str.toString());
    }
}
