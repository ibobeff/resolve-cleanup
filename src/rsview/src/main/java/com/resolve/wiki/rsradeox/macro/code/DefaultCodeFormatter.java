/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro.code;

import com.resolve.wiki.radeox.filter.context.FilterContext;

public class DefaultCodeFormatter implements SourceCodeFormatter
{

	@Override
	public String filter(String content, FilterContext context)
	{
		return content.trim().replace("\n", "<br/>");
	}

	@Override
	public String getName()
	{
		return "default";
	}

	@Override
	public int getPriority()
	{
		return 0;
	}

}
