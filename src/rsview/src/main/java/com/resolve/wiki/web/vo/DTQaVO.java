/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.web.vo;

import java.util.List;

/**
 * VO used for the new DT Viewer
 * 
 * @author jeet.marwah
 *
 */
public class DTQaVO
{
    private String problemId;//workshee that this DT execution is mapped to
    private String rootDtFullName;//name of the DT wiki
    private List<DTQaItemVO> qaItems; //list of QA items for this DT
    
    public DTQaVO() {}
    
    public String getProblemId()
    {
        return problemId;
    }
    public void setProblemId(String problemId)
    {
        this.problemId = problemId;
    }
    public String getRootDtFullName()
    {
        return rootDtFullName;
    }
    public void setRootDtFullName(String rootDtFullName)
    {
        this.rootDtFullName = rootDtFullName;
    }
    public List<DTQaItemVO> getQaItems()
    {
        return qaItems;
    }
    public void setQaItems(List<DTQaItemVO> qaItems)
    {
        this.qaItems = qaItems;
    }
        
    
}
