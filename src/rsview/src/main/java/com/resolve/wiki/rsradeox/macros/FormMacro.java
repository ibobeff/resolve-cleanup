/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.resolve.services.constants.ConstantValues;
import com.resolve.util.HttpUtil;
import com.resolve.util.JavaUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

public class FormMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.form";
    }

    @SuppressWarnings("unchecked")
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        HttpServletRequest request = wikiRequestContext.getHttpServletRequest();
        
        //get the sysId/recordId
        String sysId = request.getParameter("sys_id");
        if(StringUtils.isBlank(sysId))
        {
            sysId = request.getParameter("recordId");
        }
        //empty string if its null
        if(StringUtils.isBlank(sysId))
        {
            sysId = "";
        }
        sysId = HttpUtil.sanitizeValue(sysId, "ResolveUUID", 32);
        
        //prepare the queryString
        String queryString = "";//StringUtils.mapToString(allParams, "=", "&");
        String urlQueryString = request.getQueryString();//getRequestURI();
        if (StringUtils.isNotBlank(urlQueryString))
        {
            /*
             * urlQueryString has parameters passed to a URL and it's URL encoded.
             * So, URL decode it first.
             */
            urlQueryString = URLDecoder.decode(urlQueryString, "UTF-8");
            Map<String, String> allParams = new HashMap<String, String>(StringUtils.stringToMap(urlQueryString, "=", "&"));
            allParams.remove("sys_id");
            allParams.remove("recordId");
            
            queryString = StringUtils.mapToString(allParams, "=", "&");
            
        }

        if (StringUtils.isNotBlank(queryString)) {
        	queryString = HttpUtil.validateInput(queryString);
        }
        //Get parameters
        String name = StringUtils.isNotEmpty(params.get("name")) ? params.get("name") : "";
        name = HttpUtil.sanitizeValue(name);
        //        int height = Integer.valueOf(StringUtils.isNotEmpty(params.get("height")) ? params.get("height") : "0");
        //        int width = Integer.valueOf(StringUtils.isNotEmpty(params.get("width")) ? params.get("width") : "0");
        Boolean popout = Boolean.valueOf(StringUtils.isNotEmpty(params.get("popout")) ? params.get("popout") : "false");
        Boolean collapsed = Boolean.valueOf(StringUtils.isNotEmpty(params.get("collapsed")) ? params.get("collapsed") : "false");
        String tooltip = StringUtils.isNotEmpty(params.get("tooltip")) ? params.get("tooltip") : "";
        tooltip = HttpUtil.sanitizeValue(tooltip);
        String title = StringUtils.isNotEmpty(params.get("title")) ? params.get("title") : "";
        title = HttpUtil.sanitizeValue(title);
        int popoutHeight = Integer.valueOf(StringUtils.isNotEmpty(params.get("popoutHeight")) ? params.get("popoutHeight") : "0");
        int popoutWidth = Integer.valueOf(StringUtils.isNotEmpty(params.get("popoutWidth")) ? params.get("popoutWidth") : "0");

        //Unique id for the div to render to
        String divId = "form_" + JavaUtils.generateUniqueNumber();

        //Build up div and script to render
        StringBuffer str = new StringBuffer();
        str.append("<div id=\"%1$s\"></div>\n");
        str.append("<script type=\"text/javascript\">\n");
        str.append("Ext.onReady(function() {\n");
        str.append("    var model = glu.model({\n");
        str.append("        mtype : 'RS.wiki.macros.Form',\n");
        str.append("        name : '%2$s',\n");        
        str.append("        recordId : '%3$s',\n");
        str.append("        queryString : '%4$s',\n");
        str.append("        popout : %5$s,\n");
        str.append("        collapsed : %6$s,\n");
        str.append("        tooltip : '%7$s',\n");
        str.append("        title : '%8$s',\n");
        str.append("        popoutHeight : %9$s,\n");
        str.append("        popoutWidth : %10$s\n");
        str.append("    });\n");
        str.append("    model.init();\n");
        str.append("    glu.view(model).render('%1$s');\n");
        str.append("});\n");
        str.append("</script>\n");

        String result = String.format(str.toString(), divId, name, sysId, queryString, popout, collapsed, tooltip, title, popoutHeight, popoutWidth);
        writer.write(result);
    }
}
