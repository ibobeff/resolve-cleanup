/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macro;

import java.io.IOException;
import java.io.Writer;

import com.resolve.persistence.model.WikiAttachment;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.util.HttpUtil;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.radeox.util.Encoder;
import com.resolve.wiki.web.WikiRequestContext;

/**
 * This is for the attachment in the wiki doc eg. {attach:test.rtf}
 * 
 * @author jeet.marwah
 * 
 */
public class AttachMacro extends BaseLocaleMacro
{

    @Override
    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestcontext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestcontext.getWiki();

        String fullName = (String) rcontext.get(ConstantValues.WIKI_DOC_FULLNAME_KEY);
        String namespace = (String) rcontext.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String wikiDocName = (String) rcontext.get(ConstantValues.WIKI_DOCNAME_KEY);
        if (StringUtils.isEmpty(fullName))
        {
            fullName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY);
            namespace = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_NAMESPACE_KEY);
            wikiDocName = (String) wikiRequestcontext.getAttribute(ConstantValues.WIKI_DOCNAME_KEY);
        }
        fullName = HttpUtil.sanitizeValue(fullName);
        namespace = HttpUtil.sanitizeValue(namespace);
        wikiDocName = HttpUtil.sanitizeValue(wikiDocName);
        
        String text = params.get("text", 0);
        String filename = params.get("file", 1);

        if (params.getLength() == 1)
        {
            filename = text;
            text = Encoder.toEntity(text.charAt(0)) + Encoder.escape(text.substring(1));
        }
        text = HttpUtil.sanitizeMacroValue(text);
        filename = HttpUtil.sanitizeMacroValue(filename);

        WikiAttachment att = wiki.getWikiStore().getAttachmentFor(fullName, filename);
        StringBuffer str = new StringBuffer();
        if (att != null)
        {
            String querystring = ConstantValues.WIKI_PARAMETER_ATTACHMENT_FILE_NAME + "=" + filename + "&" + ConstantValues.WIKI_PARAMETER_ATTACHMENT_SYS_ID + "=" + att.getSys_id();
            String url = wiki.getWikiURLFactory().createForwardUrl(WikiAction.download, namespace, wikiDocName, querystring);

            str.append("<a href=\"");
            str.append(url);
            str.append("\" />");
            str.append(text);
            str.append("</a>");
        }
        else
        {
            str.append("<b>Error: ");
            str.append(filename);
            str.append(" file does not exist.</b>");
        }

        writer.write(str.toString());

    }

    @Override
    public String getLocaleKey()
    {
        return "macro.attach";
    }

}
