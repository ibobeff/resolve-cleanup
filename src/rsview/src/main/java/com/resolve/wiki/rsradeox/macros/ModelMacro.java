/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

public class ModelMacro extends BaseLocaleMacro
{
    private final static String AUTOMATION_INLINEMODEL_REFRESHINTERVAL = "automation.inlinemodel.refreshinterval";
    private final static Integer DEFAULT_INTERVAL = 5000; //milliseconds
    private final static Integer DEFAULT_REFRESH_COUNT_MAX = 60;

    public String getLocaleKey()
    {
        return "macro.model";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {

        RenderContext rcontext = params.getContext();
        WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
        Wiki wiki = wikiRequestContext.getWiki();

        String content = params.getContent();
        String refreshStatus = StringUtils.isNotEmpty(params.get("refresh")) ? params.get("refresh") : "";
        refreshStatus = HttpUtil.sanitizeValue(refreshStatus);

        //refresh interval in secs
        String automationRefreshIntervalSec = PropertiesUtil.getPropertyString(AUTOMATION_INLINEMODEL_REFRESHINTERVAL);

        if (StringUtils.isEmpty(automationRefreshIntervalSec))
        {
            automationRefreshIntervalSec = "5";
        }

        String refreshInterval = StringUtils.isNotEmpty(params.get("refreshInterval")) ? params.get("refreshInterval") : automationRefreshIntervalSec;
        refreshInterval = HttpUtil.sanitizeValue(refreshInterval);
        String refreshWiki = StringUtils.isNotEmpty(params.get("wiki")) ? params.get("wiki") : "";
        refreshWiki = HttpUtil.sanitizeValue(refreshWiki);
        String status = StringUtils.isNotEmpty(params.get("status")) ? params.get("status") : "condition";
        status = HttpUtil.sanitizeValue(status);
        String zoom = StringUtils.isNotEmpty(params.get("zoom")) ? params.get("zoom") : "1.0";
        zoom = HttpUtil.sanitizeValue(zoom);
        String width = StringUtils.isNotEmpty(params.get("width")) ? params.get("width") : "100%";
        width = HttpUtil.sanitizeValue(width);
        String height = StringUtils.isNotEmpty(params.get("height")) ? params.get("height") : "420";
        height = HttpUtil.sanitizeValue(height);
        String refreshCountMax = StringUtils.isNotEmpty(params.get("refreshCountMax")) ? params.get("refreshCountMax") : "60";
        refreshCountMax = HttpUtil.sanitizeValue(refreshCountMax);

        String username = wikiRequestContext.getWikiUser().getUsername();
        username = HttpUtil.sanitizeValue(username);
        String documentName = wiki.getDocumentName(wikiRequestContext);
        documentName = HttpUtil.sanitizeValue(documentName);
        String problemId = (String) wikiRequestContext.getAttribute(Constants.HTTP_REQUEST_PROBLEMID);

        if (StringUtils.isBlank(problemId))
        {
            String orgId = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_ID);
            orgId = HttpUtil.sanitizeValue(orgId);
            
            String orgName = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_NAME);
            orgName = HttpUtil.sanitizeValue(orgName);
            
            problemId = ServiceHibernate.getActiveWorksheet(username, orgId, orgName);
        }
        problemId = HttpUtil.sanitizeValue(problemId);

        String namespace = "";
        String docName = "";

        if (StringUtils.isNotEmpty(refreshWiki) && refreshWiki.contains("."))
        {
            namespace = refreshWiki.substring(0, refreshWiki.indexOf("."));
            docName = refreshWiki.substring(refreshWiki.indexOf(".") + 1, refreshWiki.length());
        }
        else
        {
            refreshWiki = documentName;
            namespace = documentName.substring(0, documentName.indexOf("."));
            docName = documentName.substring(documentName.indexOf(".") + 1, documentName.length());
        }

        if (StringUtils.isNotEmpty(problemId) && !"-1".equals(problemId) && StringUtils.isNotEmpty(content) && StringUtils.isNotEmpty(namespace) && StringUtils.isNotEmpty(docName))
        {
            //interval in millisecond
            long interval = DEFAULT_INTERVAL;
            try
            {
                interval = Long.parseLong(refreshInterval) * 1000;
                if (interval < DEFAULT_INTERVAL)
                {
                	interval = DEFAULT_INTERVAL;
                }
            }
            catch (Throwable t)
            {
                interval = DEFAULT_INTERVAL;
            }
            
            int refreshCount = DEFAULT_REFRESH_COUNT_MAX;
            try
            {
            	refreshCount = Integer.valueOf(refreshCountMax);
            	if (refreshCount < 1)
            	{
            		refreshCount = DEFAULT_REFRESH_COUNT_MAX;
            	}
            }
            catch(Exception e)
            {
            	refreshCount = DEFAULT_REFRESH_COUNT_MAX;
            }

            String output = "<iframe style=\"width:" + width + (StringUtils.isInteger(width) ? "px" : "") + ";height:" + height +(StringUtils.isInteger(height) ? "px" : "") +  ";frameborder:0;border:0\" src=\"/resolve/service/wiki/viewmodel/" + namespace + "/" + docName + "?PROBLEMID=" + problemId + "&WIKI=" + refreshWiki + "&STATUS=" + status + "&ZOOM=" + zoom + "&REFRESH=" + refreshStatus + "&REFRESHINTERVAL=" + interval + "&REFRESHCOUNTMAX=" + refreshCount + "\"></iframe>";

            writer.write(output);
        }
    }
}
