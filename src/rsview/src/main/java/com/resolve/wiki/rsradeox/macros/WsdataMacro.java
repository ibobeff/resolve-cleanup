/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.rsradeox.macros;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.lang3.StringUtils;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.wiki.radeox.api.engine.context.RenderContext;
import com.resolve.wiki.radeox.macro.BaseLocaleMacro;
import com.resolve.wiki.radeox.macro.parameter.MacroParameter;
import com.resolve.wiki.web.WikiRequestContext;

public class WsdataMacro extends BaseLocaleMacro
{
    public String getLocaleKey()
    {
        return "macro.wsdata";
    }

    public void execute(Writer writer, MacroParameter params) throws IllegalArgumentException, IOException
    {
    	String username = (String)params.getContext().get(ConstantValues.USERNAME);
        username = HttpUtil.sanitizeValue(username);
        String key = params.get("text", 0);
        key = HttpUtil.sanitizeValue(key);
        StringBuilder builder = new StringBuilder();
        
        if (StringUtils.isNotBlank(key))
        {
            RenderContext rcontext = params.getContext();
            WikiRequestContext wikiRequestContext = (WikiRequestContext) rcontext.get(ConstantValues.WIKI_REQUEST_CONTEXT);
            
            String orgId = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_ID);
            orgId = HttpUtil.sanitizeValue(orgId);
            
            String orgName = (String)wikiRequestContext.getAttribute(Constants.EXECUTE_ORG_NAME);
            orgName = HttpUtil.sanitizeValue(orgName);
            
            String problemId = ServiceHibernate.getActiveWorksheet(username, orgId, orgName);
            problemId = HttpUtil.sanitizeValue(problemId);
	        String value = (String)ServiceWorksheet.getWorksheetWSDATAProperty(problemId, key, username);
	        builder.append(key).append(":").append(StringUtils.isBlank(value)? "" : value).append("<br>");
        }
        else
        {
        	builder.append("<b>Error: ").append("WSDATA property name cannot be empty.</b><br>");
        }
	        
        writer.write(builder.toString());
    }
}
