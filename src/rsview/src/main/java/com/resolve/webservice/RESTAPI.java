/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.webservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.util.StringUtils;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

public class RESTAPI
{
    private static String TLS = "TLS";
    
    @Deprecated
    public static String getWorksheetResult(String problemId, String processId, String username, int timeout) {
        return getWorksheetResult(problemId, processId, username, timeout, null, null);
    }
    
    public static String getWorksheetResult(String problemId, String processId, String username, int timeout, String orgId, String orgName) {
        
        String result = "";
        JSONObject json = new JSONObject();
        
        try {         
            result = WorksheetUtil.getRunbookResult(problemId, processId, username, true, false, timeout, orgId, orgName);
            
            if(result == null) {
                json.accumulate("message", "Worksheet with id: " + problemId + " is not available.");
                result = json.toString();
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            json.accumulate("message", "Failed to get worksheet result for worksheet id: " + problemId + ".");
            result = json.toString();
        }
        
        return result;
    }
    
    @Deprecated
    public static String getActionTaskResult(String username, String problemId, String processId, String actionTaskName, int timeout) {
        return getActionTaskResult(username, problemId, processId, actionTaskName, timeout, null, null);
    }
    
    public static String getActionTaskResult(String username, String problemId, String processId, String actionTaskName, int timeout, String orgId, String orgName) {
        
        String result = "";
        JSONObject json = new JSONObject();
        
        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(Constants.EXECUTE_PROBLEMID, problemId);
            
            result = WorksheetUtil.getActionTaskResult(problemId, processId, actionTaskName, username, timeout, orgId, orgName);
            
            if(result == null) {
                json.accumulate("message", "Worksheet with id: " + problemId + " is not available.");
                result = json.toString();
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            json.accumulate("message", "Failed to get worksheet result for worksheet id: " + problemId + ".");
            result = json.toString();
        }
        
        return result;
    }

    public static String executeRunbook(Map<String, Object> params) {

        JSONObject json = new JSONObject();
        
        try {
            params.put(Constants.EXECUTE_PROBLEMID, "");
            params.put(Constants.EXECUTE_PROCESSID, "CREATE");
            params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTEPROCESS");

            String resultStr = ExecuteRunbook.start(params);
       
            if(resultStr != null) {
                if(resultStr.toUpperCase().contains("ERROR") || resultStr.toUpperCase().contains("FAILURE"))
                    json.accumulate("error", resultStr);
                else {
                    String[] lines = resultStr.split("\\n");
                    if(lines != null && lines.length > 1) {
                        for(String line:lines) {
                            String[] keyValues = line.split(":");
                            if(keyValues != null && keyValues.length == 2) {
                                String key = keyValues[0];
                                if(key != null)
                                    key = key.trim();
                                String value = keyValues[1];
                                if(value != null)
                                    value = value.trim();
                                if(key.equals("runbook"))
                                    key = "runbookName";
                                json.accumulate(key, value);
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return json.toString();
    }
    
    public static String executeRunbook(String username, String runbookName) {

        JSONObject json = new JSONObject();
        
        try {         
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(Constants.EXECUTE_PROBLEMID, "");
            params.put(Constants.EXECUTE_WIKI, runbookName);
            params.put(Constants.EXECUTE_USERID, username);
            params.put(Constants.EXECUTE_PROCESSID, "CREATE");
            params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTEPROCESS");
            
            String resultStr = ExecuteRunbook.start(params);
       
            if(resultStr != null) {
                if(resultStr.toUpperCase().contains("ERROR") || resultStr.toUpperCase().contains("FAILURE"))
                    json.accumulate("error", resultStr);
                else {
                    String[] lines = resultStr.split("\\n");
                    if(lines != null && lines.length > 1) {
                        for(String line:lines) {
                            String[] keyValues = line.split(":");
                            if(keyValues != null && keyValues.length == 2) {
                                String key = keyValues[0];
                                if(key != null)
                                    key = key.trim();
                                String value = keyValues[1];
                                if(value != null)
                                    value = value.trim();
                                if(key.equals("runbook"))
                                    key = "runbookName";
                                json.accumulate(key, value);
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return json.toString();
    }
    
    public static String getHTTPResponse(String urlStr, String method, Map<String, String> httpHeader, String body) {
        
        return getHTTPResponse(urlStr, method, httpHeader, body, false);
    }
    
    public static String getHTTPResponse(String urlStr, String method, Map<String, String> httpHeader, String body, boolean selfSigned) {
      
        HttpURLConnection conn = null;
        OutputStream output = null;
        BufferedReader bufferedReader = null;
        StringBuilder response = new StringBuilder();
        
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieStore cookieStore = cookieManager.getCookieStore();
        CookieHandler.setDefault(cookieManager);
        
        try {       
            URL url = new URL(urlStr);
//            Log.log.info("url = " + urlStr);
            
            String protocol = url.getProtocol();
            if(StringUtils.isNotBlank(protocol) && protocol.equalsIgnoreCase("https")) {
                if(selfSigned) {
                    HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                        public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                            return true;
                        }
                    });
                }
                
                conn = (HttpsURLConnection)url.openConnection();
//                conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                conn.setDoInput(true);
            } // https
            
            else
                conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod(method);
            
            if(httpHeader != null) {
                Set<String> keys = httpHeader.keySet();
                for(Iterator it = keys.iterator(); it.hasNext();) {
                    String key = (String)it.next();
                    String value = httpHeader.get(key);
                    
                    conn.addRequestProperty(key,  value);
                }
            }
            
            List<HttpCookie> cookies = cookieStore.getCookies();
            
            if (cookies != null) {
                StringBuilder sb = new StringBuilder();
                
                for (HttpCookie cookie : cookies) {
                    String cookieName = cookie.getName();
                    String cookieValue = cookie.getValue();
                    
                    sb.append(cookieName).append("=").append(cookieValue).append(";");
                }
                
//                Log.log.info("cookies = " + sb.toString());
                conn.addRequestProperty("Cookie", sb.toString());
            }
            
            if(body != null) {
                byte[] outputInBytes = body.getBytes("UTF-8");
                conn.setDoOutput(true);
                output = conn.getOutputStream();
                output.write(outputInBytes);
            }
            
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line);

            conn.getContent();
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            response.append("error: " + e.getMessage());
        } finally {
            try {
                if(output != null)
                    output.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response.toString();
    } // getHTTPResponse()
    
    private static TrustManager[] trustAllCertificates() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        
        return trustAllCerts;
    }

} // RESTAPI
