/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.webservice;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.net.HttpHeaders;
import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.controller.AjaxPlaybook;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBNotes;
import com.resolve.search.playbook.PlaybookSearchAPI;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;
import com.resolve.webservice.RESTAuth.AuthorizationException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class RESTService
{
    private static CookieManager cookieManager = null;
    private static CookieStore cookieStore = null;

    public static final RESTService instance = new RESTService();

    private RESTService()
    {
        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        cookieStore = cookieManager.getCookieStore();
        CookieHandler.setDefault(cookieManager);
    }

    public String getCookie(String name)
    {

        if (StringUtils.isBlank(name)) return null;

        List<HttpCookie> cookies = cookieStore.getCookies();

        if (cookies != null)
        {
            for (HttpCookie cookie : cookies)
            {
                String cookieName = cookie.getName();
                if (cookieName != null && cookieName.equals(name)) return cookie.getValue();
            }
        }

        return null;
    }

    public static String getCookie(HttpServletRequest request, String name)
    {

        if (StringUtils.isBlank(name)) return null;

        Cookie[] cookies = request.getCookies();

        if (cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                String cookieName = cookie.getName();
                Log.log.debug("cookie name = " + cookieName);
                Log.log.debug("cookie value = " + cookie.getValue());
                if (cookieName != null && cookieName.equals(name)) return cookie.getValue();
            }
        }

        return null;
    }

    private Cookie setCookie(String name, String value, boolean isSecure)
    {

        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/resolve/rest");
        cookie.setMaxAge(RESTAuth.getTokenTimeout());
        // cookie.setMaxAge(-1); // session cookie

        if (isSecure) cookie.setSecure(true);

        // HTTPOnly cookie will not shown in browser's HTTP response headers,
        // and cannot be accessed by JavaScript.
        cookie.setHttpOnly(true);

        return cookie;
    }

    /**
     * Method: GET 
     * Access URL :https://localhost:8443/resolve/rest/login?usename=
     * <username>&password=<password> or
     * embed username and password in HTTP header using Basic Authentication.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String token = "";
        String user = "";
        String pass = "";

        JSONObject resp = new JSONObject();
        response.setContentType("application/json");

        request.getSession(true);

        try
        {
            user = request.getParameter("username");
            pass = request.getParameter("password");

            if (StringUtils.isEmpty(user))
            {
                Enumeration<String> headers = request.getHeaderNames();
                while (headers.hasMoreElements())
                {
                    String header = headers.nextElement();
                    if (header.equalsIgnoreCase(HttpHeaders.AUTHORIZATION))
                    {
                        String value = request.getHeader(HttpHeaders.AUTHORIZATION);
                        if (StringUtils.isNotEmpty(value))
                        {
                            int index = value.indexOf("Basic ");
                            if(index == -1)
                                continue;
                            
                            value = value.substring(index+6);
//                          byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(value.getBytes());
                            byte [] decoded = java.util.Base64.getDecoder().decode(value); 
                            String credential = new String(decoded);
                            if (StringUtils.isNotEmpty(credential))
                            {
                                String[] keyValue = credential.split(":");
                                user = keyValue[0];
                                pass = keyValue[1];
                            }
                        }
                    }
                }
            }

            user = ESAPI.validator().getValidInput("Invalid input username ", user, "HTTPURL", 4000, true);
            pass = ESAPI.validator().getValidInput("Invalid input pass ", pass, "HTTPURL", 4000, true);

            String usernameDecrypted = null;
            String passwordDecrypted = null;
            
            try
            {
                usernameDecrypted = CryptUtils.isEncrypted(user) ? CryptUtils.decrypt(user) : user;
                passwordDecrypted = CryptUtils.isEncrypted(pass) ? CryptUtils.decrypt(pass) : pass;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (StringUtils.isBlank(usernameDecrypted) || StringUtils.isBlank(passwordDecrypted))
            {
                resp.accumulate("error", "We didn't recognize your username or password. Please try again.");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return resp.toString();
            }

            token = RESTAuth.instance.validateUser(usernameDecrypted, passwordDecrypted);

            if (token == null)
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + "RESTService" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), usernameDecrypted, request.getLocalAddr(), syslogMessage));
                Log.log.warn("Received invalid login request for " + usernameDecrypted + " from " + request.getRemoteAddr());
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                resp.accumulate("error", "We didn't recognize your username or password. Please try again.");
            }
            else
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + "RESTService" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), usernameDecrypted, request.getLocalAddr(), syslogMessage));
                resp.accumulate("username", usernameDecrypted);
                resp.accumulate("token", token);

                // response.addCookie(setCookie("RESTTOKEN", token,
                // request.isSecure()));
                ESAPI.httpUtilities().addCookie(response, setCookie("RESTTOKEN", token, request.isSecure()));
            }
        }
        catch (Exception e)
        {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            Log.log.error(e.getMessage(), e);
            resp.accumulate("error", "We didn't recognize your username or password. Please try again.");
        }

        return resp.toString();
    }

    /**
     * Method: POST
     * Access URL: https://localhost:8443/resolve/rest/login with POST method
     * Set content-type to be "application/json" in HTTP header Put {"username":
     * "<username>", "password":"<password>"} in JSON payload or
     * embed username and password in HTTP header using Basic Authentication.
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String login(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String token = "";
        String user = "";
        String pass = "";

        JSONObject resp = new JSONObject();
        response.setContentType("application/json");

        request.getSession(true);

        try
        {
            if (jsonProperty != null)
            {
                user = jsonProperty.getString("username");
                pass = jsonProperty.getString("password");
            }

            else
            {
                user = request.getParameter("username");
                pass = request.getParameter("password");
            }
            
            if (StringUtils.isEmpty(user))
            {
                Enumeration<String> headers = request.getHeaderNames();
                while (headers.hasMoreElements())
                {
                    String header = headers.nextElement();
                    if (header.equalsIgnoreCase(HttpHeaders.AUTHORIZATION))
                    {
                        String value = request.getHeader(HttpHeaders.AUTHORIZATION);
                        if (StringUtils.isNotEmpty(value))
                        {
                            int index = value.indexOf("Basic ");
                            if(index == -1)
                                continue;
                            
                            value = value.substring(index+6);
//                          byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(value.getBytes());
                            byte [] decoded = java.util.Base64.getDecoder().decode(value); 
                            String credential = new String(decoded);
                            if (StringUtils.isNotEmpty(credential))
                            {
                                String[] keyValue = credential.split(":");
                                user = keyValue[0];
                                pass = keyValue[1];
                            }
                        }
                    }
                }
            }

            user = ESAPI.validator().getValidInput("Invalid input username ", user, "HTTPURL", 4000, true);
            pass = ESAPI.validator().getValidInput("Invalid input pass ", pass, "HTTPURL", 4000, true);

            String usernameDecrypted = null;
            String passwordDecrypted = null;
            try
            {
                usernameDecrypted = CryptUtils.isEncrypted(user) ? CryptUtils.decrypt(user) : user;
                passwordDecrypted = CryptUtils.isEncrypted(pass) ? CryptUtils.decrypt(pass) : pass;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if (StringUtils.isBlank(usernameDecrypted) || StringUtils.isBlank(passwordDecrypted))
            {
                resp.accumulate("error", "We didn't recognize your username or password. Please try again.");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return resp.toString();
            }

            token = RESTAuth.instance.validateUser(usernameDecrypted, passwordDecrypted);

            if (token == null)
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + "REST" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), usernameDecrypted, request.getLocalAddr(), syslogMessage));
                Log.log.warn("Received invalid login request for " + usernameDecrypted + " from " + request.getRemoteAddr());
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                resp.accumulate("error", "We didn't recognize your username or password. Please try again.");
            }

            else
            {
                resp.accumulate("username", usernameDecrypted);
                resp.accumulate("token", token);

                // response.addCookie(setCookie("RESTTOKEN", token,
                // request.isSecure()));
                ESAPI.httpUtilities().addCookie(response, setCookie("RESTTOKEN", token, request.isSecure()));
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.accumulate("error", "We didn't recognize your username or password. Please try again.");
        }

        return resp.toString();
    }

    /**
     * Access URL: https://localhost:8443/resolve/rest/logout Put in HTTP
     * header: {"Authorization":"<token>"} Method: POST
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String result = "";
        String token = request.getHeader("Authorization");

        JSONObject resp = new JSONObject();
        response.setContentType("application/json");

        try
        {
            if (StringUtils.isBlank(token) || token.equals("null")) token = RESTService.getCookie(request, "RESTTOKEN");

            if (StringUtils.isBlank(token))
            {
                resp.accumulate("error", "We didn't recognize your token. Please try again.");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return resp.toString();
            }

            result = RESTAuth.instance.logoutWithToken(token);
            resp.accumulate("message", result);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            resp.accumulate("error", "Failed to logout. Please try again.");
        }

        return resp.toString();
    }

    /**
     * Access URL:
     * https://localhost:8443/resolve/rest/getWorksheetResult?PROBLEMID=
     * <problemId> Put in HTTP header: {"Authorization":"<token>"} or add in the
     * requset URL with "&token=<token>" Method: GET
     */
    @RequestMapping(value = "/getWorksheetResult", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getWorksheetResult(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String resp = "";
        JSONObject json = new JSONObject();
        String token = request.getHeader("Authorization");

        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(ControllerUtil.getParameterMap(request));

        String problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
        String processId = (String) params.get(Constants.EXECUTE_PROCESSID);
        String orgId = (String)params.get(Constants.EXECUTE_ORG_ID);
        String orgName = (String)params.get(Constants.EXECUTE_ORG_NAME);

        if (StringUtils.isBlank(problemId))
        {
            json.accumulate("error", "We didn't recognize your worksheet id. Please try again.");
            return json.toString();
        }

        request.getSession(true);

        try
        {
            if (StringUtils.isBlank(token)) token = (String) request.getParameter("token");

            token = ESAPI.validator().getValidInput("Invalid input token ", token, "HTTPURL", 4000, true);

            if (StringUtils.isBlank(token) || token.equals("null")) token = RESTService.instance.getCookie("RESTTOKEN");

            if (StringUtils.isBlank(token))
            {
                json.accumulate("error", "We didn't recognize your token. Please log in again.");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return json.toString();
            }

            Log.log.debug("validate token: " + token);
            String username = RESTAuth.instance.validateToken(token);

            problemId = ESAPI.validator().getValidInput("Invalid input problemId ", problemId, "HTTPURL", 4000, true);
            processId = ESAPI.validator().getValidInput("Invalid input processId ", processId, "HTTPURL", 4000, true);

            int timeout = 100;
            try
            {
                String timedout = (String) params.get(Constants.EXECUTE_TIMEOUT);
                timeout = ((new Integer(timedout))).intValue();
            }
            catch (Exception e)
            {
            }

            resp = RESTAPI.getWorksheetResult(problemId, processId, username, timeout, orgId, orgName);

            if (StringUtils.isBlank(resp))
            {
                json.accumulate("error", "No information is available at this time.");
                resp = json.toString();
            }

            response.setContentType("application/json");
            // response.addCookie(setCookie("RESTTOKEN", token,
            // request.isSecure()));
            ESAPI.httpUtilities().addCookie(response, setCookie("RESTTOKEN", token, request.isSecure()));
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            json.accumulate("error", e.getMessage());
            resp = json.toString();
        }

        return resp;
    }

    /**
     * Access URL:
     * https://localhost:8443/resolve/rest/getActionTaskResult?PROBLEMID=
     * <problemId>&actionTaskName=<taskName>%23<namespace> Put in HTTP header:
     * {"Authorization":"<token>"} or add in the requset URL with
     * "&token=<token>" Method: GET
     */
    @RequestMapping(value = "/getActionTaskResult", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getActionTaskResult(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String resp = "";
        JSONObject json = new JSONObject();
        String token = request.getHeader("Authorization");

        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(ControllerUtil.getParameterMap(request));

        String problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
        String processId = (String) params.get(Constants.EXECUTE_PROCESSID);
        String actionTaskName = (String) params.get(Constants.ACTIONTASK_DETAIL_OUTPUT);
        String orgId = (String)params.get(Constants.EXECUTE_ORG_ID);
        String orgName = (String)params.get(Constants.EXECUTE_ORG_NAME);

        if (StringUtils.isBlank(problemId))
        {
            json.accumulate("error", "We didn't recognize your worksheet id. Please try again.");
            return json.toString();
        }

        if (StringUtils.isBlank(actionTaskName))
        {
            json.accumulate("error", "We didn't recognize your action task name. Please try again.");
            return json.toString();
        }

        request.getSession(true);

        try
        {
            if (StringUtils.isBlank(token)) token = (String) request.getParameter("token");

            token = ESAPI.validator().getValidInput("Invalid input token ", token, "HTTPURL", 4000, true);

            if (StringUtils.isBlank(token) || token.equals("null")) token = RESTService.getCookie(request, "RESTTOKEN");

            if (StringUtils.isBlank(token))
            {
                json.accumulate("error", "We didn't recognize your token. Please log in again.");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return json.toString();
            }

            String username = RESTAuth.instance.validateToken(token);

            problemId = ESAPI.validator().getValidInput("Invalid input problemId ", problemId, "HTTPURL", 4000, true);
            processId = ESAPI.validator().getValidInput("Invalid input processId ", processId, "HTTPURL", 4000, true);
            actionTaskName = ESAPI.validator().getValidInput("Invalid input actionTaskName ", actionTaskName, "HTTPURL", 4000, true);

            int timeout = 100;
            try
            {
                String timedout = (String) params.get(Constants.EXECUTE_TIMEOUT);
                timeout = ((new Integer(timedout))).intValue();
            }
            catch (Exception e)
            {
            }

            resp = RESTAPI.getActionTaskResult(username, problemId, processId, actionTaskName, timeout, orgId, orgName);

            if (StringUtils.isBlank(resp))
            {
                json.accumulate("error", "No information is available at this time.");
                resp = json.toString();
            }

            response.setContentType("application/json");
            // response.addCookie(setCookie("RESTTOKEN", token,
            // request.isSecure()));
            ESAPI.httpUtilities().addCookie(response, setCookie("RESTTOKEN", token, request.isSecure()));
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            json.accumulate("error", e.getMessage());
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp = json.toString();
        }

        return resp;
    }

    /**
     * Access URL:
     * https://localhost:8443/resolve/rest/executeRunbook?WIKI=<runbook name>
     * Put in HTTP header: {"Authorization":"<token>"} or add in the requset URL
     * with "&token=<token>" Method: GET
     */
    @RequestMapping(value = "/executeRunbook", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String executeRunbook(@RequestParam String WIKI, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String resp = "";
        JSONObject json = new JSONObject();
        String token = request.getHeader("Authorization");

        request.getSession(true);

        try
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.putAll(ControllerUtil.getParameterMap(request));

            if (StringUtils.isBlank(WIKI))
            {
                json.accumulate("error", "We didn't recognize your runbook name. Please try again.");
                return json.toString();
            }

            if (StringUtils.isBlank(token)) token = (String) request.getParameter("token");

            token = ESAPI.validator().getValidInput("Invalid input token ", token, "HTTPURL", 4000, true);

            if (StringUtils.isBlank(token) || token.equals("null")) token = RESTService.getCookie(request, "RESTTOKEN");

            if (StringUtils.isBlank(token))
            {
                json.accumulate("error", "We didn't recognize your token. Please log in again.");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return json.toString();
            }

            String username = RESTAuth.instance.validateToken(token);
            params.put(Constants.EXECUTE_USERID, username);
            params.put(Constants.EXECUTE_WIKI, WIKI);
            
            resp = RESTAPI.executeRunbook(params);

            if (StringUtils.isBlank(resp))
            {
                json.accumulate("error", "No information is available at this time. Please try again.");
                resp = json.toString();
            }

            response.setContentType("application/json");
            // response.addCookie(setCookie("RESTTOKEN", token,
            // request.isSecure()));
            ESAPI.httpUtilities().addCookie(response, setCookie("RESTTOKEN", token, request.isSecure()));
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            json.accumulate("error", e.getMessage());
            resp = json.toString();
        }

        return resp;
    }

    /**
     * This is SYNCHRONOUS API for executing a RUNBOOK or an ACTIONTASK. So it
     * will wait to fetch the results Access URL:
     * https://localhost:8443/resolve/rest/executeRunbookWithResult?WIKI=<
     * runbook name>
     * https://localhost:8443/resolve/rest/executeRunbookWithResult?WIKI=<
     * runbook name>&DETAIL_OUTPUT_FLAG=Y(or any value)
     * https://localhost:8443/resolve/rest/executeRunbookWithResult?WIKI=<
     * runbook name>&WSDATA_FLAG=Y (or any value)
     * https://localhost:8443/resolve/rest/executeRunbookWithResult?WIKI=<
     * runbook name>&&ACTIONTASK_DETAIL_OUTPUT=task%23namespace Put in HTTP
     * header: {"Authorization":"<token>"} or add in the requset URL with
     * "&token=<token>" Method: GET
     * @throws IntrusionException 
     * @throws ValidationException 
     */
    @RequestMapping(value = "/executeRunbookWithResult", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String executeRunbookWithResult(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ValidationException, IntrusionException
    {

        String resp = "";
        JSONObject json = new JSONObject();
        String token = request.getHeader("Authorization");

        request.getSession(true);

        response.setContentType(ContentType.APPLICATION_JSON.toString());
        
        try
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.putAll(ControllerUtil.getParameterMap(request));

            params.put(Constants.EXECUTE_PROBLEMID, "NEW");
            params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTEPROCESS");

            if (StringUtils.isBlank(token)) token = (String) request.getParameter("token");

            token = ESAPI.validator().getValidInput("Invalid input token ", token, "HTTPURL", 4000, true);

            if (StringUtils.isBlank(token) || token.equals("null")) token = RESTService.getCookie(request, "RESTTOKEN");

            if (StringUtils.isBlank(token))
            {
                json.accumulate("error", "We didn't recognize your token. Please log in again.");
                return json.toString();
            }

            String username = RESTAuth.instance.validateToken(token);

            params.put(Constants.EXECUTE_USERID, username);

            resp = ExecuteRunbook.execute(params);

            if (StringUtils.isBlank(resp))
            {
                json.accumulate("error", "No information is available at this time. Please try again.");
                resp = json.toString();
            }

            // response.addCookie(setCookie("RESTTOKEN", token,
            // request.isSecure()));
            ESAPI.httpUtilities().addCookie(response, setCookie("RESTTOKEN", token, request.isSecure()));
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            json.accumulate("error", e.getMessage());
            resp = json.toString();
        }
        
        return ESAPI.validator().getValidSafeHTML("executeRunbookWithResult", resp, 65536, false);
    }

    /**
     * This is SYNCHRONOUS API for invoking a pre-defined HTTP Filter. So it
     * will wait and return the results depends on the blocking level, and
     * update the result if required. Access URL:
     * https://localhost:8443/resolve/rest/invokeHTTPFilter
     * 
     * @param in
     *            request: filter_host, filter_port, filter_uri, filter_ssl,
     *            (optional)blocking, (optional)timeout,
     *            (optional)show_worksheet Put in HTTP header: {"Authorization":
     *            "<token>"} or add in the requset URL with "&token=<token>"
     *            Method: GET, POST
     */
    @RequestMapping(value = "/invokeHTTPFilter", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json")
    public @ResponseBody String invokeHTTPFilter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            String resp = "";
            JSONObject json = new JSONObject();
            String token = request.getHeader("Authorization");
            token = ESAPI.validator().getValidInput("Invalid input token ", token, "HTTPURL", 4000, true);

            String host = (String) request.getParameter("filter_host");
            host = ESAPI.validator().getValidInput("Invalid input host ", host, "HTTPURL", 4000, true);

            String port = (String) request.getParameter("filter_port");
            port = ESAPI.validator().getValidInput("Invalid input port ", port, "HTTPURL", 4000, true);

            String uri = (String) request.getParameter("filter_uri");
            uri = ESAPI.validator().getValidInput("Invalid input uri ", uri, "HTTPURI", 4000, true);

            String ssl = (String) request.getParameter("filter_ssl");
            ssl = ESAPI.validator().getValidInput("Invalid input ssl ", ssl, "HTTPURL", 4000, true);

            boolean redirect = StringUtils.isBlank((String) request.getParameter("show_worksheet")) ? false : true;
            request.getSession(true);

            try
            {
                if (StringUtils.isBlank(token)) token = (String) request.getParameter("token");

                if (StringUtils.isBlank(token) || token.equals("null")) token = RESTService.getCookie(request, "RESTTOKEN");

                token = ESAPI.validator().getValidInput("Invalid input token ", token, "HTTPURL", 4000, true);

                if (StringUtils.isBlank(token))
                {
                    json.accumulate("error", "We didn't recognize your token. Please log in again.");
                    return json.toString();
                }

                if (StringUtils.isBlank(uri))
                {
                    json.accumulate("error", "We didn't recognize the filter URI.");
                    return json.toString();
                }

                String username = RESTAuth.instance.validateToken(token);

                if (!uri.startsWith("/")) uri = "/" + uri;

                StringBuilder sb = new StringBuilder();
                String protocol = (ssl != null && ssl.length() != 0 && ssl.equals("true")) ? "https://" : "http://";
                sb.append(protocol).append(host).append(":");

                if (StringUtils.isBlank(port))
                    port = "7777";
                else
                {
                    try
                    {
                        port = (new Integer(port)).toString();
                    }
                    catch (Exception ee)
                    {
                        port = "7777";
                    }
                }

                sb.append(port);

                sb.append(uri);

                sb.append("?").append(Constants.EXECUTE_USERID).append("=").append(username);

                String method = request.getMethod();
                method = ESAPI.validator().getValidInput("Invalid input method ", method, "FileName", 4000, true);

                if (StringUtils.isNotEmpty(method) && method.equalsIgnoreCase("post"))
                {
                    String inputString = StringUtils.toString(request.getInputStream(), "utf-8");
                    // these are the post parameters through a form
                    Map<String, String> params = StringUtils.urlToMap(inputString);

                    int i = 0;
                    int size = params.size();
                    if (size != 0) sb.append("&");

                    for (String key : params.keySet())
                    {
                        String value = params.get(key);
                        value = ESAPI.validator().getValidInput("Invalid input value ", value, "FileName", 4000, true);

                        sb.append(key).append("=").append(URLEncoder.encode(value, "UTF-8"));
                        i++;
                        if (i < size) sb.append("&");
                    }
                }

                else
                {
                    // this loop will read from the query string parameters.
                    // e.g. ?name1=value&name2=value2
                    Map<String, String[]> requestParameters = request.getParameterMap();

                    int i = 0;
                    int size = requestParameters.size();
                    if (size != 0) sb.append("&");

                    for (String key : requestParameters.keySet())
                    {
                        key = ESAPI.validator().getValidInput("Invalid input key ", key, "FileName", 4000, true);
                        String[] values = requestParameters.get(key);
                        String value = StringUtils.arrayToString(requestParameters.get(key), ",");
                        value = ESAPI.validator().getValidInput("Invalid input value ", value, "FileName", 4000, true);

                        sb.append(key).append("=").append(URLEncoder.encode(value, "UTF-8"));
                        i++;
                        if (i < size) sb.append("&");
                    }
                }

                String endpoint = sb.toString();
                Log.log.debug("endpoint = " + endpoint);
                resp = RESTAPI.getHTTPResponse(endpoint, "GET", null, null);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                json.accumulate("error", "Failed to connect to Resolve Gateway.");
                json.accumulate("reason", e.getMessage());
                return json.toString();
            }

            if (StringUtils.isBlank(resp))
            {
                json.accumulate("error", "No information is available at this time. Please try again.");
                resp = json.toString();
            }

            else if (redirect)
            {
                try
                {
                    JSONObject msg = JSONObject.fromObject(resp);
                    String message = msg.getString("Message");
                    message = ESAPI.validator().getValidInput("Invalid input message ", message, "FileName", 4000, true);

                    if (StringUtils.isNotEmpty(message))
                    {
                        JSONObject result = JSONObject.fromObject(message);
                        String problemId = result.getString("worksheetId");

                        if (StringUtils.isNotEmpty(problemId))
                        {
                            String redirectUrl = "/resolve/jsp/rsclient.jsp#RS.worksheet.Worksheet/id=" + problemId;
                            // response.sendRedirect(redirectUrl);
                            DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                            try
                            {
                                dhttp.sendRedirect(response, redirectUrl);
                            }
                            catch (AccessControlException ace)
                            {
                                Log.log.error(ace.getMessage(), ace);
                            }
                        }

                        else
                            response.setContentType("application/json");
                    }

                    else
                        response.setContentType("application/json");
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }

            ESAPI.httpUtilities().addCookie(response, setCookie("RESTTOKEN", token, request.isSecure()));
            // resp = ESAPI.validator().getValidInput("Invalid return value ",
            // resp, "ResolveText", 400000, true);

            return HttpUtil.sanitizeValue(resp);
        }
        catch (ValidationException ex)
        {
            Log.log.error("Failed to validate input: " + ex.getMessage(), ex);
            return "Failed to get correct return value";
        }
    }

    /**
     * Sample request using curl: <code>
     * curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/getSIR?sir=SIR-000000115' -H "Content-Type: application/json" -H 'Authorization: u8haq5mvlr9ogg0j2ia99724je'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed get SIR","result":{"id":"8a94945a5ba25bd6015ba25f8c8a00da","sys_id":"8a94945a5ba25bd6015ba25f8c8a00da","sysCreatedOn":1493077822000,"sysCreatedBy":"admin","sysUpdatedOn":1493077822000,"sysUpdatedBy":"admin","sysModCount":0,"sysPerm":"UNDEFINED","sysOrg":null,"sysIsDeleted":false,"sysOrganizationName":"UNDEFINED","title":"test SIR - ","externalReferenceId":"1003","investigationType":"Phishing","type":"security","severity":"level : 25","description":"UNDEFINED","sourceSystem":"Splunk (identity)","playbook":"test.Dako1","externalStatus":null,"status":"Open","owner":"admin","playbookVersion":1,"sequence":115,"sir":"SIR-000000115","closedOn":null,"closedBy":null,"primary":false,"members":["admin"],"sirStarted":false,"problemId":"02f4157c669c435e831635cb71b949ea","dataCompromised":false,"dueBy":null,"requestData":null}}
     * </code>
     * 
     * @param sir
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/getSIR", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getSIR(@RequestParam String sir, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            //
            ResolveSecurityIncidentVO securityIncident = PlaybookUtils.getResolveSecurityIncidentBySIR(sir, username);
            result.put("status", "OK");
            result.put("message", "Successfully performed get SIR");
            result.put("result", new ObjectMapper().writeValueAsString(securityIncident));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not get SIR", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not get SIR. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /**
     * Sample request using curl: <code>
     * curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/upsertSIR' -H 'Authorization: 7tqm2sel5gvjhjsonu90ur0l6s' -H 'Content-Type: application/json' -d '{"title":"title","investigationType":"Phishing","playbook":"test.Dako1","severity":"High","dueBy":null,"owner":"admin","sourceSystem":"Resolve","externalReferenceId":null,"status":"Open"}'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed upsert on SIR","result":{"id":"8a94945a5b9da79d015b9db190fb0003","sys_id":"8a94945a5b9da79d015b9db190fb0003","sysCreatedOn":1493024511611,"sysCreatedBy":"admin","sysUpdatedOn":1493024511611,"sysUpdatedBy":"admin","sysModCount":0,"sysPerm":"UNDEFINED","sysOrg":null,"sysIsDeleted":false,"sysOrganizationName":"UNDEFINED","title":"title","externalReferenceId":null,"investigationType":"Phishing","type":null,"severity":"High","description":"UNDEFINED","sourceSystem":"Resolve","playbook":"test.Dako1","externalStatus":null,"status":"Open","owner":"admin","playbookVersion":1,"sequence":112,"sir":"SIR-000000112","closedOn":null,"closedBy":null,"primary":false,"members":["admin"],"sirStarted":false,"problemId":"fea72c6356884353b0bae6870bf5cac5","dataCompromised":false,"dueBy":null,"requestData":null}}
     * </code>
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/upsertSIR", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String upsertSIR(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            String json = jsonProperty.toString();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            //
            ResolveSecurityIncident model = mapper.readValue(json, ResolveSecurityIncident.class);
            ResolveSecurityIncidentVO vo = PlaybookUtils.saveSecurityIncident(model, username);
            //
            result.put("status", "OK");
            result.put("message", "Successfully performed upsert on SIR");
            result.put("result", mapper.writeValueAsString(vo));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not upsert SIR", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not upsert SIR. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /**
     * Sample request using curl: <code>
     * curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/upsertSIRNote' -H 'Authorization: 3ukk0stjh56jhhjmhmmvu9j3np' -H 'Content-Type: application/json' -d '{"note":"Test 2","category":"","incidentId":"8a94945a5b9da79d015b9db190fb0003"}'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed upsert on SIR Note","result":{"sysId":"b4d1019f6cc901e05dd31a2ffcd4442d","sysOrg":null,"sysUpdatedBy":"admin","sysUpdatedOn":1493001106831,"sysUpdatedDt":1493001106831,"sysCreatedBy":"admin","sysCreatedOn":1493001106831,"sysCreatedDt":1493001106831,"worksheetId":"fea72c6356884353b0bae6870bf5cac5","incidentId":"8a94945a5b9da79d015b9db190fb0003","note":"Test 2","activityName":null,"category":"","postedBy":"admin","componentId":null,"activityId":null,"id":"b4d1019f6cc901e05dd31a2ffcd4442d"}}
     * </code>
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/upsertSIRNote", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String upsertSIRNote(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            PBNotes note = PlaybookUtils.saveNote(jsonProperty.toString(), username);

            PlaybookUtils.auditAction(note.getAuditMessage(), note.getIncidentId(), request.getRemoteAddr(), username);
            result.put("status", "OK");
            result.put("message", "Successfully performed upsert on SIR Note");
            result.put("result", new ObjectMapper().writeValueAsString(note));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not upsert SIR Note", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not upsert SIR Note. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /**
     * Sample request using curl: <code>
     * curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/getSIRNotes?incidentId=8a94945a5b9da79d015b9db190fb0003' -H 'Authorization: 3otasdvj9g3kn2jm2r2tldvr10' -H 'Content-Type: application/json'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed get SIR Notes","result":[{"sysId":"b4d1019f6cc901e05dd31a2ffcd4442d","sysOrg":null,"sysUpdatedBy":"admin","sysUpdatedOn":1493001106831,"sysUpdatedDt":1493001106831,"sysCreatedBy":"admin","sysCreatedOn":1493001106831,"sysCreatedDt":1493001106831,"worksheetId":"fea72c6356884353b0bae6870bf5cac5","incidentId":"8a94945a5b9da79d015b9db190fb0003","note":"Test 2","activityName":null,"category":"","postedBy":"admin","componentId":null,"activityId":null,"id":"b4d1019f6cc901e05dd31a2ffcd4442d"}]}
     * </code>
     * 
     * @param incidentId
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/getSIRNotes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getSIRNotes(@RequestParam String incidentId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            //
            int maxLimit = PropertiesUtil.getPropertyInt(AjaxPlaybook.PROPERTY_SIR_MAX_NOTES);
            ResponseDTO<PBNotes> notesResponseDTO = PlaybookUtils.searchPBNotesByIncidentId(incidentId, username, maxLimit);
            result.put("status", "OK");
            result.put("message", "Successfully performed get SIR Notes");
            result.put("result", new ObjectMapper().writeValueAsString(notesResponseDTO.getRecords()));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not get SIR Notes", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not get SIR Notes. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /**
     * Sample request using curl: <code>
     * curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/upsertSIRArtifact' -H 'Authorization: ofm1s3ahdl547ohad47ndjolui' -H 'Content-Type: application/json' -d '{"sir":"SIR-000000112","name":"Name2","value":"Value2","description":"Desc. 2"}'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed upsert on SIR Artifact","result":{"sysId":null,"sysOrg":null,"sysUpdatedBy":"admin","sysUpdatedOn":1493003299657,"sysUpdatedDt":1493003299657,"sysCreatedBy":"admin","sysCreatedOn":1493003299657,"sysCreatedDt":1493003299657,"worksheetId":"fea72c6356884353b0bae6870bf5cac5","incidentId":"8a94945a5b9da79d015b9db190fb0003","name":null,"value":"Value2","description":"Desc. 2","sir":"SIR-000000112","activityId":null,"activityName":null,"id":null}}
     * </code>
     * 
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/upsertSIRArtifact", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String upsertSIRArtifact(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            String json = jsonProperty.toString();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            //
            Map<String, Object> pbArtifactMap = new ObjectMapper().readValue(json, Map.class);
            PBArtifacts artifact = PlaybookUtils.saveArtifact(pbArtifactMap, false, username);
            //
            result.put("status", "OK");
            result.put("message", "Successfully performed upsert on SIR Artifact");
            result.put("result", mapper.writeValueAsString(artifact));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not upsert SIR Artifact", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not upsert SIR Artifact. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /**
     * Sample request using curl: <code>
     *  curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/deleteSIRArtifacts' -H 'Authorization: rotvb8ptpgn5sminpa2rl185tl' -H 'Content-Type: application/json' -d '["8a94945a5b9da79d015b9db190fb0003"]'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed upsert on SIR Artifact","result":[{"id":"8a94945a5b9da79d015b9db190fb0003"}]}
     * </code>
     * 
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/deleteSIRArtifacts", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json")
    public @ResponseBody String deleteSIRArtifacts(@RequestBody JSONArray jsonIDsArray, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            List<Map<String, String>> artifacts = new ArrayList<>();
            for (int i = 0; i < jsonIDsArray.size(); i++)
            {
                Map<String, String> artifact = new HashMap<>();
                artifact.put("id", jsonIDsArray.getString(i));
                artifacts.add(artifact);
            }
            // TODO needs to privide incidentId. For time being, passing null
            // and will not delete artifact values stored in WS.
            PlaybookUtils.deleteArtifacts(artifacts, null, username);
            //
            result.put("status", "OK");
            result.put("message", "Successfully performed upsert on SIR Artifact");
            result.put("result", new ObjectMapper().writeValueAsString(artifacts));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not upsert SIR Artifact", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not upsert SIR Artifact. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /**
     * Sample request using curl: <code>
     * curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/appendSIRArtifact' -H 'Authorization: ofm1s3ahdl547ohad47ndjolui' -H 'Content-Type: application/json' -d '{"sir":"SIR-000000112","name":"Name2","value":"Value2","description":"Desc. 2"}'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed upsert on SIR Artifact","result":{"sysId":null,"sysOrg":null,"sysUpdatedBy":"admin","sysUpdatedOn":1493003299657,"sysUpdatedDt":1493003299657,"sysCreatedBy":"admin","sysCreatedOn":1493003299657,"sysCreatedDt":1493003299657,"worksheetId":"fea72c6356884353b0bae6870bf5cac5","incidentId":"8a94945a5b9da79d015b9db190fb0003","name":null,"value":"Value2","description":"Desc. 2","sir":"SIR-000000112","activityId":null,"activityName":null,"id":null}}
     * </code>
     * 
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    /*
     * @SuppressWarnings("unchecked")
     * 
     * @RequestMapping(value = "/appendSIRArtifact", method = RequestMethod.GET,
     * produces = "application/json") public @ResponseBody String
     * appendSIRArtifact(@RequestBody JSONObject jsonProperty,
     * HttpServletRequest request, HttpServletResponse response) throws
     * ServletException, IOException {
     * 
     * JSONObject result = new JSONObject(); request.getSession(true); try {
     * String username = RESTAuth.instance.authorizeUsingToken(request); String
     * json = jsonProperty.toString(); ObjectMapper mapper = new ObjectMapper();
     * mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false); // Map
     * <String, String> pbArtifactMap = new ObjectMapper().readValue(json,
     * Map.class); PBArtifacts artifact =
     * PlaybookUtils.saveArtifact(pbArtifactMap, true, username); //
     * result.put("status", "OK"); result.put("message",
     * "Successfully performed upsert on SIR Artifact"); result.put("result",
     * mapper.writeValueAsString(artifact)); } catch (AuthorizationException ae)
     * { String message = String.format(
     * "Authentication failed. Error message is '%s'", ae.getMessage());
     * Log.log.error(message, ae); result.put("status", "ERROR");
     * result.put("message", message);
     * response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); } catch
     * (Exception e) { Log.log.error("Could not upsert SIR Artifact", e);
     * result.put("status", "ERROR"); result.put("message", String.format(
     * "Could not upsert SIR Artifact. Error message is '%s'", e.getMessage()));
     * } return result.toString(); }
     */

    /**
     * Sample request using curl: <code>
     * curl -v -k -X POST 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/uploadSIRAttachment' -H 'Authorization: feq01anun1sd5e4am1m5a0u732' -F 'docFullName=8a94945a5b9da79d015b9db190fb0003' -F 'name=@c:/project/resolve/build.properties'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed upload SIR Attachment","result":"build.properties"}
     * </code>
     * 
     * @param incidentId
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/uploadSIRAttachment", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public @ResponseBody String uploadSIRAttachment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            //
            PBAttachments attachment = PlaybookUtils.uploadAttachment(request, username);
            result.put("status", "OK");
            result.put("message", "Successfully performed upload SIR Attachment");
            result.put("result", new ObjectMapper().writeValueAsString(attachment.getName()));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not upload SIR Attachment", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not upload SIR Attachment. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /**
     * Sample request using curl: <code>
     * curl -v -k -X GET 'https://localhost.cloud.resolvesys.com:8443/resolve/rest/getSIRAttachments?incidentId=8a94945a5b9da79d015b9db190fb0003' -H 'Authorization: aj0glrnssptjosrk1t9k8e4430' -H 'Content-Type: application/json'
     * </code> Sample response: <code>
     * {"status":"OK","message":"Successfully performed get SIR Attachments","result":[{"sysId":"8a94945a5b9dfa64015b9dfc535d0004","sysOrg":null,"sysUpdatedBy":"admin","sysUpdatedOn":1493004211164,"sysUpdatedDt":1493004211164,"sysCreatedBy":"admin","sysCreatedOn":1493004211164,"sysCreatedDt":1493004211164,"worksheetId":"fea72c6356884353b0bae6870bf5cac5","incidentId":"8a94945a5b9da79d015b9db190fb0003","activityId":null,"name":"splunk.der","size":559,"id":"8a94945a5b9dfa64015b9dfc535d0004"}]}
     * </code>
     * 
     * @param incidentId
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/getSIRAttachments", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getSIRAttachments(@RequestParam String incidentId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        JSONObject result = new JSONObject();
        request.getSession(true);
        try
        {
            String username = RESTAuth.instance.authorizeUsingToken(request);
            //
            int maxLimit = PropertiesUtil.getPropertyInt(AjaxPlaybook.PROPERTY_SIR_MAX_ATTACHMENTS);
            ResponseDTO<PBAttachments> attachmentsResponseDTO = PlaybookUtils.searchPBAttachmentsByIncident(incidentId, username, maxLimit);
            result.put("status", "OK");
            result.put("message", "Successfully performed get SIR Attachments");
            result.put("result", new ObjectMapper().writeValueAsString(attachmentsResponseDTO.getRecords()));
        }
        catch (AuthorizationException ae)
        {
            String message = String.format("Authentication failed. Error message is '%s'", ae.getMessage());
            Log.log.error(message, ae);
            result.put("status", "ERROR");
            result.put("message", message);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        catch (Exception e)
        {
            Log.log.error("Could not get SIR Attachments", e);
            result.put("status", "ERROR");
            result.put("message", String.format("Could not get SIR Attachments. Error message is '%s'", e.getMessage()));
        }
        return result.toString();
    }

    /*
     * public static void main(String[] args) {
     * 
     * unitTest(); }
     */
    private static void unitTest()
    {

        // String host = "http://loadbalancer.resolvesys.com";
        String host = "http://10.20.20.26:8080";
        String token = "";
        String endpoint = "";
        String message = "";

        JSONObject json = null;

        // 1. login GET
        System.out.println("1. login GET");
        endpoint = host + "/resolve/rest/login?username=admin&password=resolve";
        System.out.println(endpoint);
        // RESTAPI.getHTTPResponse(endpoint, "GET", null, null, false);
        message = RESTAPI.getHTTPResponse(endpoint, "GET", null, null);

        if (StringUtils.isNotBlank(message))
        {
            if (message.contains("error"))
                System.err.println("1." + message);

            else
            {
                json = JSONObject.fromObject(message);
                token = (String) json.get("token");
                System.out.println("1. Login successful with token: " + token);
            }
        }

        System.out.println("");

        // 2. logout with the token
        System.out.println("2. logout with the token");
        endpoint = host + "/resolve/rest/logout";
        System.out.println(endpoint);
        Map<String, String> httpHeader = new HashMap<String, String>();
        httpHeader.put("Authorization", token);
        // message = RESTAPI.getHTTPResponse(endpoint, "POSRESTAPI.;
        message = RESTAPI.getHTTPResponse(endpoint, "POST", httpHeader, null);

        if (StringUtils.isNotBlank(message) && message.indexOf("error") != -1)
            System.err.println("2." + message);

        else
            System.out.println("2." + message);

        httpHeader.remove("Authorization");
        System.out.println("");

        // 3. login POST
        System.out.println("3. login POST");
        endpoint = host + "/resolve/rest/login";
        System.out.println(endpoint);
        httpHeader.put("Content-Type", "application/json");
        json = new JSONObject();
        json.accumulate("username", "admin");
        json.accumulate("password", "resolve");
        String body = json.toString();
        // RESTAPI.getHTTPResponse(endpoint, "POST", httpHeader, body);
        message = RESTAPI.getHTTPResponse(endpoint, "POST", httpHeader, body);

        if (StringUtils.isNotBlank(message))
        {
            if (message.indexOf("error") != -1)
                System.err.println("3." + message);

            else
            {
                json = JSONObject.fromObject(message);
                token = (String) json.get("token");
                System.out.println("3. Login successful with token: " + token);
            }
        }

        System.out.println("");

        // 4. execute runbook
        System.out.println("4. execute runbook");
        httpHeader.put("Content-Type", "application/json");
        // endpoint = host +
        // "/resolve/rest/executeRunbook?WIKI=test.XML%20Parser";
        // System.out.println(endpoint);
        // message = RESTAPI.getHTTPResponse(endpoint + token, "GET",
        // httpHeader, null, false);
        endpoint = host + "/resolve/rest/executeRunbook?WIKI=test.XML%20Parser&token=";
        System.out.println(endpoint + token);
        // httpHeader.put("Authorization", token);
        message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null);
        System.out.println(message);
        System.out.println("");

        String problemId = "";
        String processId = "";

        if (StringUtils.isNotEmpty(message) && message.contains("worksheetId"))
        {
            json = JSONObject.fromObject(message);
            if (json != null)
            {
                problemId = (String) json.get("worksheetId");
                processId = (String) json.getString("processId");
            }
        }

        // 5. get worksheet results by problem id
        System.out.println("5. get worksheet results by problem id");
        endpoint = host + "/resolve/rest/getWorksheetResult?PROBLEMID=";
        httpHeader.put("Content-Type", "application/json");
        StringBuilder sb = new StringBuilder();
        // sb.append(endpoint).append(problemId).append("&PROCESSID=").append(processId).append("&TIMEOUT=180");
        // message = RESTAPI.getHTTPResponse(sb.toString(), "GET", httpHeader,
        // null, false);
        sb.append(endpoint).append(problemId).append("&PROCESSID=").append(processId).append("&TIMEOUT=180").append("&token=").append(token);
        // httpHeader.put("Authorization", token);
        message = RESTAPI.getHTTPResponse(sb.toString(), "GET", httpHeader, null);
        System.out.println(sb.toString());
        System.out.println(message);
        System.out.println("");

        // 6. get action task result by problem id and action task full name
        System.out.println("6. get action task result by problem id and action task full name");
        endpoint = host + "/resolve/rest/getWorksheetResult?PROBLEMID=";
        httpHeader.put("Content-Type", "application/json");
        sb = new StringBuilder();
        // sb.append(endpoint).append(problemId).append("&PROCESSID=").append(processId).append("&ACTIONTASK_DETAIL_OUTPUT=httpConnect%23resolve&TIMEOUT=180");
        // message = RESTAPI.getHTTPResponse(sb.toString(), "GET", httpHeader,
        // null, false);
        // httpHeader.put("Authorization", token);
        // endpoint = host + "/resolve/rest/getActionTaskResult?PROBLEMID=" +
        // problemId +
        // "&ACTIONTASK_DETAIL_OUTPUT=httpConnect%23resolve&TIMEOUT=180&token="
        // + token;
        sb.append(endpoint).append(problemId).append("&PROCESSID=").append(processId).append("&ACTIONTASK_DETAIL_OUTPUT=httpConnect%23resolve&TIMEOUT=180").append("&token=").append(token);
        message = RESTAPI.getHTTPResponse(sb.toString(), "GET", httpHeader, null);
        System.out.println(sb.toString());
        System.out.println(message);
        System.out.println("");

        // 7. execute runbook with summary of results
        System.out.println("7. execute runbook with summary of results");
        httpHeader.put("Content-Type", "application/json");
        // endpoint = host +
        // "/resolve/rest/executeRunbookWithResult?WIKI=test.result&TIMEOUT=180";
        // message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null,
        // false);
        // httpHeader.put("Authorization", token);
        endpoint = host + "/resolve/rest/executeRunbookWithResult?WIKI=test.result&TIMEOUT=180&token=" + token;
        message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null);
        System.out.println(endpoint);
        System.out.println(message);
        System.out.println("");

        // 8. execute runbook with details of results
        System.out.println("8. execute runbook with details of results");
        httpHeader.put("Content-Type", "application/json");
        // endpoint = host +
        // "/resolve/rest/executeRunbookWithResult?WIKI=test.result&DETAIL_OUTPUT_FLAG=Y&TIMEOUT=180";
        // message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null,
        // false);
        // httpHeader.put("Authorization", token);
        endpoint = host + "/resolve/rest/executeRunbookWithResult?WIKI=test.result&DETAIL_OUTPUT_FLAG=Y&TIMEOUT=180&token=" + token;
        message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null);
        System.out.println(endpoint);
        System.out.println(message);
        System.out.println("");

        // 9. execute runbook with action task details
        System.out.println("9. execute runbook with action task details");
        httpHeader.put("Content-Type", "application/json");
        // endpoint = host +
        // "/resolve/rest/executeRunbookWithResult?WIKI=test.result&ACTIONTASK_DETAIL_OUTPUT=WSDATA_get%23test&TIMEOUT=100";
        // message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null,
        // false);
        // httpHeader.put("Authorization", token);
        endpoint = host + "/resolve/rest/executeRunbookWithResult?WIKI=test.result&ACTIONTASK_DETAIL_OUTPUT=WSDATA_get%23test&TIMEOUT=100&token=" + token;
        message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null);
        System.out.println(endpoint);
        System.out.println(message);
        System.out.println("");

        // 10. execute runbook with WSDATA in the results
        System.out.println("10. execute runbook with WSDATA in the results");
        httpHeader.put("Content-Type", "application/json");
        // endpoint = host +
        // "/resolve/rest/executeRunbookWithResult?WIKI=test.result&WSDATA_FLAG=Y&TIMEOUT=120";
        // message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null,
        // false);
        // httpHeader.put("Authorization", token);
        endpoint = host + "/resolve/rest/executeRunbookWithResult?WIKI=test.result&WSDATA_FLAG=Y&TIMEOUT=120&token=" + token;
        message = RESTAPI.getHTTPResponse(endpoint, "GET", httpHeader, null);
        System.out.println(endpoint);
        System.out.println(message);
        System.out.println("");
    }

} // RESTService
