/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.webservice;

public class Row
{
	private Pair[] columns;

	public Pair[] getColumns()
	{
		return columns;
	} // getColumns

	public void setColumns(Pair[] cols)
	{
		columns = cols;
	} // setColumns
} // Row
