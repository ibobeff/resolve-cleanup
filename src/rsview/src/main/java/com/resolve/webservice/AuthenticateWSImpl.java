package com.resolve.webservice;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.owasp.esapi.ESAPI;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.common.net.HttpHeaders;
import com.resolve.rsview.auth.Authentication;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;

import net.sf.json.JSONObject;
@Component
public class AuthenticateWSImpl implements IAuthenticateWS{
	
	private static ConcurrentHashMap<String, WSUser> userTokens = new ConcurrentHashMap<String, WSUser>();
	static AuthenticateWSImpl instance = new AuthenticateWSImpl();
	public AuthenticateWSImpl() {
		
	}
//	@Bean
//	public static AuthenticateWSImpl authenticateWSImplIns() {
//		return instance;
//	}
	/**
	 * 
	 * @param jsonProperty
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	    public WSUser login(HttpServletRequest request)throws ServletException, IOException
	    								
	    {
	    	WSUser user = null;
	        String userName = "";
	        String password = "";
	        request.getSession(true);
	        try
	        {
	                Enumeration<String> headers = request.getHeaderNames();
	                while (headers.hasMoreElements())
	                {
	                    String header = headers.nextElement();
	                    if (header.equalsIgnoreCase(HttpHeaders.AUTHORIZATION))
	                    {
	                        String value = request.getHeader(HttpHeaders.AUTHORIZATION);
	                        if (StringUtils.isNotEmpty(value))
	                        {
	                            int index = value.indexOf("Basic ");
	                            if(index == -1)
	                                continue;
	                            
	                            value = value.substring(index+6);
//	                          byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(value.getBytes());
	                            byte [] decoded = java.util.Base64.getDecoder().decode(value); 
	                            String credential = new String(decoded);
	                            if (StringUtils.isNotEmpty(credential))
	                            {
	                                String[] keyValue = credential.split(":");
	                                userName = keyValue[0];
	                                password = keyValue[1];
	                                break;
	                            }
	                        }
	                    }
	                }
	            //Only if the user name and passwords were part of the header
	            if(!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(password)) {
		            userName = ESAPI.validator().getValidInput("Invalid input username ", userName, "HTTPURL", 4000, true);
		            password = ESAPI.validator().getValidInput("Invalid input pass ", password, "HTTPURL", 4000, true);
	
		            String usernameDecrypted = null;
		            String passwordDecrypted = null;
		            try
		            {
		                usernameDecrypted = CryptUtils.isEncrypted(userName) ? CryptUtils.decrypt(userName) : userName;
		                passwordDecrypted = CryptUtils.isEncrypted(password) ? CryptUtils.decrypt(password) : password;
		            }
		            catch (Exception e)
		            {
		                e.printStackTrace();
		            }
		            user = validateCreds(usernameDecrypted, passwordDecrypted);
	            }
	        }
	        catch (Exception e)
	        {
	            Log.log.error(e.getMessage(), e);
//	            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//	            resp.accumulate("error", "We didn't recognize your username or password. Please try again.");
	        }

	        return user;
	    }
	
	
	
	public String login(String username, String password)
    {
        Log.log.info("Web service listener: new login request, username: " + username);

        WSUser user = validateCreds(username, password);
        String token = System.nanoTime() + username.hashCode() + password.hashCode() + "";
        userTokens.put(token, user);
        Log.log.info("User " + username + " logged in via web service listener. New user token is " + token);
        return token;
    } // login

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static WSUser validateCreds(String username, String password)
    {
		WSUser result = null;

        try
        {
            if (!Authentication.authenticate(username, password).valid)
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + "WEBService" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(InetAddress.getLocalHost().getHostAddress(), username, InetAddress.getLocalHost().getHostAddress(), syslogMessage));
                Log.log.info("User " + username + " tried to log in with incorrect credential");
                throw new RuntimeException("Failed to log in: invalid user credential for user " + username);
            }
            else
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + "RESTService" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(InetAddress.getLocalHost().getHostAddress(), username, InetAddress.getLocalHost().getHostAddress(), syslogMessage));
            }
        }
        catch (Exception ex)
        {
            Log.log.error("Error when user was authenticated. username: " + username, ex);
            throw new RuntimeException("Error when user was authenticated. username: " + username, ex);
        }

        return new WSUser(username, password);

    } // validateUser
}
