/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.webservice;

public class ActionTaskResult
{
    private String actionTaskResultID;
    private String actionTask;
    private String summary;
    private String detail;
    private String condition;
    private String severity;

    public ActionTaskResult(String actionTaskResultID, String actionTask, String summary, String condition, String severity)
    {
        this.actionTaskResultID = actionTaskResultID;
        this.actionTask = actionTask;
        this.summary = summary;
        this.condition = condition;
        this.severity = severity;
    }

    public ActionTaskResult(String actionTaskResultID, String actionTask, String summary, String detail, String condition, String severity)
    {
        this.actionTaskResultID = actionTaskResultID;
        this.actionTask = actionTask;
        this.summary = summary;
        this.detail = detail;
        this.condition = condition;
        this.severity = severity;
    }

    public String getActionResultID()
    {
        return actionTaskResultID;
    }
    public void setActionResultID(String actionTaskResultID)
    {
        this.actionTaskResultID = actionTaskResultID;
    }
    public String getActionTask()
    {
        return actionTask;
    }
    public void setActionTask(String actionTask)
    {
        this.actionTask = actionTask;
    }
    public String getSummary()
    {
        return summary;
    }
    public void setSummary(String summary)
    {
        this.summary = summary;
    }
    public String getDetail()
    {
        return detail;
    }
    public void setDetail(String detail)
    {
        this.detail = detail;
    }
    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }
    public String getSeverity()
    {
        return severity;
    }
    public void setSeverity(String severity)
    {
        this.severity = severity;
    }
}
