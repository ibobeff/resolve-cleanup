package com.resolve.webservice;

public class WSUser {
	public String uname;
    public String password;
    long lastAccessTime;

    public WSUser(String u, String p)
    {
        uname = u;
        password = p;
        lastAccessTime = System.currentTimeMillis();
    } // User

    public String toString()
    {
        return uname;
    } 
 

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uname == null) ? 0 : uname.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        return result;
    } // hashCode

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        WSUser other = (WSUser) obj;
        if (uname == null)
        {
            if (other.uname != null) return false;
        }
        else if (!uname.equals(other.uname)) return false;
        if (password == null)
        {
            if (other.password != null) return false;
        }
        else if (!password.equals(other.password)) return false;
        return true;
    } // equals

}
