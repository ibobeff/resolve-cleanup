/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.webservice;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.stream.XMLStreamException;
import javax.xml.ws.WebServiceContext;

//import javax.xml.ws.handler.MessageContext;
import org.apache.axis2.context.MessageContext;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;

import com.resolve.cron.CronAPI;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.Main;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;

// WSDL location:
// http://172.16.5.205:8080/resolve/webservice/WebserviceListener?wsdl
//@WebService
public class WebserviceListener
{
    private static String PROCESS_ID = "_RESOLVE_RUNBOOK_PROCESS_ID_";
    private static String PROCESS_TIMEOUT = "_RESOLVE_RUNBOOK_PROCESS_TIMEOUT_";
    private static ConcurrentHashMap<String, User> userTokens = new ConcurrentHashMap<String, User>();
    private static long TOKEN_EXPIRATION_PERIOD = 30 * 60 * 1000; // 30 minutes
    private static volatile long lastCleanupTime = System.currentTimeMillis();
    private static long TOKEN_CLEANUP_INTERVAL = 10 * 60 * 1000; // 10 minutes

    private static final String STATUS = "STATUS";
    private static final String NUMBER = "NUMBER";
    private static final String LAST_UPDATED = "LAST_UPDATED";

    private static final String SUMMARY = "SUMMARY";
    private static final String DETAIL = "DETAIL";
    //private static final String ACTION_TASK_QUERY = "select actionResult from ResolveActionResult actionResult where actionResult.task in (select task from ResolveActionTask task where task.UFullName=:taskFullName)";

    //Event gateway related
    private static final String EVENT_GATEWAY_QUEUE = "event.gateway.queue";
    private static final String EVENT_GATEWAY_MESSAGEHANDLER = "event.gateway.messagehandler";
    private static final String EVENT_GATEWAY_FILTER_NAME = "event.gateway.filter.name";
    private static String eventGatewayQueueName; // e.g., NETCOOL
    private static String eventGatewayTopicName; // e.g., derived as NETCOOL_TOPIC
    private static String eventGatewayClassmethod; //e.g., derived as MNetcool.processFilter
    private static String eventGatewayFilterIds; // e.g., MyEvtFilter1,MyEvtFilter2
    
    private static final String EXECUTE_WAIT_COMPLETION = "soap.execute.result.wait";

    @Resource
    WebServiceContext wctx;
    static
    {
        eventGatewayQueueName = PropertiesUtil.getPropertyString(EVENT_GATEWAY_QUEUE);
        if(eventGatewayQueueName != null)
        {
            eventGatewayTopicName = eventGatewayQueueName.toUpperCase() + "_TOPIC";
        }
        else
        {
            eventGatewayTopicName = null;
        }
        String eventGatewayMessageHanlder = PropertiesUtil.getPropertyString(EVENT_GATEWAY_MESSAGEHANDLER);
        if(eventGatewayMessageHanlder != null)
        {
            eventGatewayClassmethod = eventGatewayMessageHanlder + ".processFilter";
        }
        eventGatewayFilterIds = PropertiesUtil.getPropertyString(EVENT_GATEWAY_FILTER_NAME);
    }
    
    public String login(String username, String password)
    {
        Log.log.info("Web service listener: new login request, username: " + username);

        User user = validateUser(username, password);
        String token = System.nanoTime() + username.hashCode() + password.hashCode() + "";
        userTokens.put(token, user);
        Log.log.info("User " + username + " logged in via web service listener. New user token is " + token);
        return token;
    } // login

    public String logout(String token)
    {

        User user = userTokens.get(token);
        if (user != null)
        {
            userTokens.remove(token);
            Log.log.info("User token " + token + " was invalidated for user " + user.name);
        }
        else
        {
            // It doesn't matter. Expired tokens might have been removed from
            // token map already.
            Log.log.info("User token " + token + " couldn't be found for logout.");
        }

        return "User token " + token + " has been invalidated.";
    } // logout

    private static long getTokenExpirationPeriod()
    {
        return TOKEN_EXPIRATION_PERIOD;
    } // getTokenExpirationPeriod

    private static User validateUser(String token)
    {
        User user = userTokens.get(token);
        if (user != null)
        {
            long now = System.currentTimeMillis();
            if ((now - user.lastAccessTime) < getTokenExpirationPeriod())
            {
                user.lastAccessTime = now; // update last access time

                // Clean up expired tokens in map
                if ((now - lastCleanupTime) > TOKEN_CLEANUP_INTERVAL)
                {
                    for (Map.Entry<String, User> e : userTokens.entrySet())
                    {
                        if ((now - e.getValue().lastAccessTime) > getTokenExpirationPeriod())
                        {
                            userTokens.remove(e.getKey());
                        }
                    }
                    lastCleanupTime = now;
                }

                return user;
            }
            else
            {
                userTokens.remove(token);
                throw new RuntimeException("Invalid token: user token expired: " + token + ". Please log in again.");
            }

        }
        else
        {
            throw new RuntimeException("Invalid user token " + token + ". Try login first.");
        }
    } // validateUser

    private static User validateUser(String username, String password)
    {
        User result = null;

        try
        {
            if (!Authentication.authenticate(username, password).valid)
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + "WEBService" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(InetAddress.getLocalHost().getHostAddress(), username, InetAddress.getLocalHost().getHostAddress(), syslogMessage));
                Log.log.info("User " + username + " tried to log in with incorrect credential");
                throw new RuntimeException("Failed to log in: invalid user credential for user " + username);
            }
            else
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + "RESTService" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(InetAddress.getLocalHost().getHostAddress(), username, InetAddress.getLocalHost().getHostAddress(), syslogMessage));
            }
        }
        catch (Exception ex)
        {
            Log.log.error("Error when user was authenticated. username: " + username, ex);
            throw new RuntimeException("Error when user was authenticated. username: " + username, ex);
        }

        return new User(username, password);

    } // validateUser

    public String ping(String token, String str)
    {
        Log.log.info("Ping from token " + token);
        validateUser(token);
        return "Ping response from web service listener: your input is " + str;
    } // ping

    public String getActionDetailResultByString(String token, String actionID)
    {
        Pair[] results = getActionDetailResult(token, actionID);
        return getQueryStringFromPairs(results);
    }

    private static String getQueryStringFromPairs(Pair[] pairs)
    {
        if (pairs == null) return "";

        GetMethod method = new GetMethod("http://www.resolve-system.com");
        NameValuePair[] nvpairs = new NameValuePair[pairs.length];
        int i = 0;
        for (Pair p : pairs)
        {
            nvpairs[i] = new NameValuePair(p.getName(), p.getValue());
            i++;
        }
        method.setQueryString(nvpairs);
        return method.getQueryString();
    } // getQueryStringFromPairs

    private static String getQueryStringFromMap(Map<String, String> seed)
    {
        if (seed == null) return "";

        GetMethod method = new GetMethod("http://www.resolve-system.com");
        NameValuePair[] nvpairs = new NameValuePair[seed.size()];
        int i = 0;
        for (String key : seed.keySet())
        {
            nvpairs[i] = new NameValuePair(key, seed.get(key));
            i++;
        }
        method.setQueryString(nvpairs);
        return method.getQueryString();
    } // getQueryStringFromPairs

    private static Map<String, String> getMapFromPairs(Pair[] params)
    {
        Map<String, String> result = new HashMap<String, String>();
        if(params != null)
        {
            for (Pair p : params)
            {
                result.put(p.getName(), p.getValue());
            }
        }
        return result;
    } // getMapFromPairs

    private static Pair[] getPairsFromMap(Map<String, String> status)
    {
        Pair[] results = new Pair[status.size()];

        int i = 0;
        for (String key : status.keySet())
        {
            results[i] = new Pair();

            results[i].setName(key.toUpperCase());
            results[i].setValue(status.get(key));
            ++i;
        }
        return results;
    } // getPairsFromMap

    public Pair[] getActionDetailResult(String token, String actionID)
    {
        Log.log.info("WebserviceListener: getActionDetailResult() is called with ActionID = " + actionID + ", token = " + token);

        User user = validateUser(token);
        if (StringUtils.isEmpty(actionID))
        {
            throw new RuntimeException("ActionID parameter in getActionDetailResult() call can not be null or empty");
        }

        Pair[] result = new Pair[0];
        // check if request entry exists

        try
        {
            TaskResult taskResult = WorksheetSearchAPI.findTaskResultById(actionID, user.name).getData();
            String detail = "";
            if (taskResult != null)
            {
                detail = (taskResult.getDetail() == null ? "" : taskResult.getDetail());

                // still has "<pre>" tag around detail text?
                //detail = detail.substring(detail.indexOf("<pre>") + 5, detail.indexOf("</pre>"));
                result = new Pair[1];
                result[0] = new Pair();
                result[0].setName("DETAIL");
                result[0].setValue(detail);
            }
        }
        catch (Throwable ex)
        {
            Log.log.error("Failed to retrive action result detail: actionID = " + actionID);
        }

        return result;
    } // getActionDetailResult

    public String getActionRawResultByString(String token, String actionID)
    {
        return getQueryStringFromPairs(getActionRawResult(token, actionID));
    }

    public Pair[] getActionRawResult(String token, String actionID)
    {
        Log.log.info("WebserviceListener: getActionRawResult() is called with ActionID = " + actionID + ", token = " + token);
        
        User user = validateUser(token);
        if (StringUtils.isEmpty(actionID))
        {
            throw new RuntimeException("ActionID parameter in getActionRawResult() call can not be null or empty");
        }

        Pair[] result = new Pair[0];
        // check if request entry exists
        try
        {
            TaskResult taskResult = WorksheetSearchAPI.findTaskResultById(actionID, user.name).getData();
            if (taskResult != null)
            {
                result = new Pair[1];
                result[0] = new Pair();
                result[0].setName("DETAIL");
                result[0].setValue((taskResult.getRaw() == null ? "" :  taskResult.getRaw()));
            }
        }
        catch (Throwable ex)
        {
            Log.log.error("Failed to retrive action result raw detail: actionID = " + actionID);
        }
        return result;
    } // getActionRawResult

    public String executeRunbookWithLoginByString(String username, String password, String params)
    {
        User user = validateUser(username, password);

        return StringUtils.mapToUrl(executeRunbookInternal(StringUtils.urlToMap(params), user));
    } // executeRunbookWithLoginByString

    public Pair[] executeRunbookWithLogin(String username, String password, Pair[] params)
    {
        User user = validateUser(username, password);
        return getPairsFromMap(executeRunbookInternal(getMapFromPairs(params), user));
    } // executeRunbookWithLogin

    public String executeRunbookByString(String token, String params)
    {
        User user = validateUser(token);

        return StringUtils.mapToUrl(executeRunbookInternal(StringUtils.urlToMap(params), user));
    }

    public ActionTaskResult[] simpleExecuteRunbook(String username, String password, String runbook, String problemid, String params, String[] responseActionTasks)
    {
        List<ActionTaskResult> nodes = new ArrayList<ActionTaskResult>();
        WSUser user = null;
        User oUser = null;
        //Get the WS Message Context
        MessageContext msgCtx = MessageContext.getCurrentMessageContext();
//      //Grab the HTTP Request Headers
        HttpServletRequest req = (HttpServletRequest)msgCtx.getProperty("transport.http.servletRequest");
        
        System.out.println("Acceptable character set: " +req.getHeader("Authorization"));
        System.out.println("Acceptable Media Type: "+ req.getHeader("Username"));
        AuthenticateWSImpl auth = new AuthenticateWSImpl();
        try {
        	user = auth.login(req);
        	if(user == null) {
        		user = auth.validateCreds(username, password);
        	}
        	oUser = new User(user.getUname(), user.getPassword());
        }catch (Exception ex) {
        	Log.log.error("Failed to retrive User name password from the request");
        	throw new RuntimeException("Error when user was authenticated. Check the user name and password for validity ", ex);
        }

        Map paramMap = StringUtils.urlToMap(params);
        if (StringUtils.isNotEmpty(runbook))
        {
            paramMap.put(Constants.EXECUTE_WIKI, runbook);
        }
        if (StringUtils.isNotEmpty(problemid))
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, problemid);
        }
        else
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, "NEW");
        }

        Map status = executeRunbookInternal(paramMap, oUser);
        problemid = (String) status.get(Constants.EXECUTE_PROBLEMID);
        if (problemid != null)
        {
            nodes = getDetailResultByWorksheetID(problemid, responseActionTasks, oUser.name);
        }
        return nodes.toArray(new ActionTaskResult[nodes.size()]);
    } // simpleExecuteRunbook

    public Pair[] executeRunbook(String token, Pair[] params)
    {
        Log.log.info("executeRunbook() is called with token = " + token + ", parameters = " + Arrays.toString(params));

        User user = validateUser(token);
        return getPairsFromMap(executeRunbookInternal(getMapFromPairs(params), user));
    } // executeRunbook

    public ActionTaskResult[] executeRunbookByParams(String username, String password, String runbook, String problemid, Pair[] params, String[] responseActionTasks)
    {
        List<ActionTaskResult> nodes = new ArrayList<ActionTaskResult>();
        User user = validateUser(username, password);

        Map paramMap = getMapFromPairs(params);
        if (StringUtils.isNotEmpty(runbook))
        {
            paramMap.put(Constants.EXECUTE_WIKI, runbook);
        }
        if (StringUtils.isNotBlank(problemid))
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, problemid);
        }
        else
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, "NEW");
        }

        Map status = executeRunbookInternal(paramMap, user);
        problemid = (String) status.get(Constants.EXECUTE_PROBLEMID);
        if (problemid != null)
        {
            nodes = getDetailResultByWorksheetID(problemid, responseActionTasks, user.name);
        }
        return nodes.toArray(new ActionTaskResult[nodes.size()]);
    } // executeRunbookByParams

    /**
     * Returns only a selective set of data. For example ActionID, ActionTask Name, and Summery.
     *
     * @param username
     * @param password
     * @param runbook
     * @param problemid
     * @param params
     * @return
     */
    public String executeRunbookLight(String username, String password, String runbook, String problemid, String params)
    {
        String result = "";
        User user = validateUser(username, password);

        Map paramMap = StringUtils.urlToMap(params);
        if (StringUtils.isNotEmpty(runbook))
        {
            paramMap.put(Constants.EXECUTE_WIKI, runbook);
        }
        if (StringUtils.isNotEmpty(problemid))
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, problemid);
        }
        else
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, "NEW");
        }

        Map status = executeRunbookInternal(paramMap, user);
        problemid = (String) status.get(Constants.EXECUTE_PROBLEMID);
        if (problemid != null)
        {
            result = getLightResultByWorksheetIDInternal(problemid, user.name);
        }
        return result;
    } // executeRunbookLight

    private Map executeRunbookInternal(Map params, User user)
    {
        if (params == null)
        {
            throw new RuntimeException("Missing parmater in startRunbook() call");
        }

        Map<String, String> status = triggerRunbook(params, user);

        // processid
        String processID = status.get(PROCESS_ID);
        status.remove(PROCESS_ID);
        String state = status.get(STATUS).trim();
        String wiki = status.get(Constants.EXECUTE_WIKI);
        
        if (processID == null)
        {
            throw new RuntimeException("Missing PROCESSID for execution");
        }

        // process timeout
        String processTimeoutStr = status.get(PROCESS_TIMEOUT);
        int processTimeout = Integer.parseInt(processTimeoutStr) * 60;
        try
        {
            //limit 2 because we are just getting maximum 2 task results
            QueryDTO queryDTO = new QueryDTO(0, 2);
            queryDTO.addFilterItem(new QueryFilter("terms", "end#resolve,abort#resolve", "taskFullName", QueryFilter.CONTAINS, true));
            queryDTO.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processID));
            queryDTO.addFilterItem(new QueryFilter("wiki", QueryFilter.EQUALS, wiki));
            
            long taskResultsCount = WorksheetSearchAPI.getTaskResultsCount(queryDTO, user.name);
            
            int maxWaitTime = 1;
            while (StringUtils.isEmpty(state) || "OPEN".equals(state) || taskResultsCount <= 0)
            {
                // quit out after 20 minutes if runbook is still not finished or
                // aborted.
                if (maxWaitTime++ > processTimeout)
                {
                    throw new RuntimeException("Failed to finish runbook: " + params + " in 20 minutes. Runbook processID is " + processID);
                }
    
                // Sleep for one second to wait for runbook to finish
                Thread.currentThread().sleep(1000L);
                status = getProcessStatus(processID, user.name);
                //System.out.println("status: " + status);
                state = status.get(STATUS).trim();
                taskResultsCount = WorksheetSearchAPI.getTaskResultsCount(queryDTO, user.name);
            }
            //wait extra 5 seconds to anticipating all the task results were indexed.
            long waitTime = PropertiesUtil.getPropertyLong(EXECUTE_WAIT_COMPLETION);
            if (waitTime > 0) {
            	Thread.currentThread().sleep(waitTime * 1000L);
            } else if (waitTime != 0){
	            Thread.currentThread().sleep(5000L);
            }
        }
        catch (Exception ex)
        {
            Log.log.info("Exception is thrown when waiting for runbook to finish", ex);
            throw new RuntimeException(ex);
        }
        
        status.put(Constants.EXECUTE_PROCESSID, processID);
        return status;
    } // executeRunbookInternal

    private static Map<String, String> getProcessStatus(String processID, String username)
    {
        HashMap<String, String> result = new HashMap<String, String>();

        try
        {
            // init status
            result.put(STATUS, "");

            // check if request entry exists
            ProcessRequest process = WorksheetSearchAPI.findProcessRequestsById(processID, username).getData();

            if (process != null)
            {
                String status = process.getStatus();
                if (status == null)
                {
                    status = "";
                }
                result.put(STATUS, status);

                String problemID = process.getProblem();
                if (problemID != null)
                {
                    result.put(Constants.EXECUTE_PROBLEMID, problemID);
                    result.put(NUMBER, process.getProblemNumber());
                }

                result.put(LAST_UPDATED, "" + process.getSysUpdatedOn());
                result.put(Constants.EXECUTE_WIKI, process.getWiki());
            }
            else
            {
                throw new Exception("Invalid processID: " + processID);
            }
        }
        catch (Throwable e)
        {
            Log.log.warn("Failed to retrive process status for processid : " + processID, e);
            result.put("Error", e.getMessage());
        }
        return result;
    }

    private Map<String, String> triggerRunbook(Map params, User user)
    {
        if (params == null)
        {
            throw new RuntimeException("No parameter is provided in triggerRunbook() call");
        }

        // init user
        params.put(Constants.EXECUTE_USERID, user.name);

        // init runbook
        String runbook = null;
        if (params.containsKey(Constants.EXECUTE_WIKI))
        {
            runbook = (String) params.get(Constants.EXECUTE_WIKI);
        }
        else if (params.containsKey("RUNBOOK"))
        {
            runbook = (String) params.get("RUNBOOK");
        }
        else
        {
            throw new RuntimeException("Missing WIKI parameter to execute runbook");
        }
        params.put("WIKI", runbook);
        params.put("SUMMARY", "Runbook: " + runbook);

        // init WEB and NAME
        int pos = runbook.indexOf('.');
        if (pos > 0)
        {
            //Log.log.debug("Executing wiki: " + runbook);
            String web = runbook.substring(0, pos);
            String name = runbook.substring(pos + 1, runbook.length());

            params.put("WEB", web);
            params.put("NAME", name);
        }

        // init PROBLEMID
        if (!params.containsKey(Constants.EXECUTE_PROBLEMID))
        {
            params.put("PROBLEMID", "NEW");
        }

        // init processid
        if (!params.containsKey(Constants.EXECUTE_PROCESSID))
        {
            params.put(Constants.EXECUTE_PROCESSID, "NEW");
        }

        // init process_timeout
        String processTimeout = "10";
        if (params.containsKey(Constants.EXECUTE_PROCESS_TIMEOUT))
        {
            processTimeout = null;
            if(params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
                processTimeout = (String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT); // optional
            } else if (params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
                processTimeout = "" + params.get(Constants.EXECUTE_PROCESS_TIMEOUT);
            }
        }

        String processID = null;
        Map<String, String> status = new HashMap<String, String>();
        if (runbook != null)
        {
            Log.log.info("Web service listener starts runbook with message : " + params);
            // login as system
            processID = ExecuteRunbook.WebserviceStart(params);

            // Main.main.sendMessage(headerHeader, paramMap);
            Log.log.info("Web service listener successfully started runbook " + runbook + " with processid = " + processID);

            status = getProcessStatus(processID, user.name);
            status.put(PROCESS_ID, processID);
            status.put(PROCESS_TIMEOUT, processTimeout);
            status.put(Constants.EXECUTE_WIKI, runbook);
        }
        else
        {
            throw new RuntimeException("Fatal error: parameter 'RUNBOOK' is missing in startRunbook() web service call.");
        }

        return status;
    } // triggerRunbook

    public String simpleStartRunbook(String username, String password, String runbook, String problemid, String params)
    {
        User user = validateUser(username, password);

        Map paramMap = StringUtils.urlToMap(params);
        if (StringUtils.isNotEmpty(runbook))
        {
            paramMap.put(Constants.EXECUTE_WIKI, runbook);
        }
        if (StringUtils.isNotEmpty(problemid))
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, problemid);
        }
        else
        {
            paramMap.put(Constants.EXECUTE_PROBLEMID, "NEW");
        }

        return StringUtils.mapToUrl(startRunbookInternal(paramMap, user));
    } // simpleStartRunbook

    public String startRunbookWithLoginByString(String username, String password, String params)
    {
        User user = validateUser(username, password);

        Map paramMap = StringUtils.urlToMap(params);
        return StringUtils.mapToUrl(startRunbookInternal(paramMap, user));
    } // startRunbookWithLoginByString

    public Pair[] startRunbookWithLogin(String username, String password, Pair[] params)
    {
        User user = validateUser(username, password);
        return getPairsFromMap(startRunbookInternal(getMapFromPairs(params), user));
    } // startRunbookWithLogin

    public String startRunbookByString(String token, String params)
    {
        User user = validateUser(token);

        Map paramMap = StringUtils.urlToMap(params);
        return StringUtils.mapToUrl(startRunbookInternal(paramMap, user));
    } // startRunbookByString

    public Pair[] startRunbook(String token, Pair[] params)
    {
        Log.log.info("startRunbook() is called with token = " + token + ", parameters = " + Arrays.toString(params));
        User user = validateUser(token);
        return getPairsFromMap(startRunbookInternal(getMapFromPairs(params), user));
    } // startRunbook

    private Map<String, String> startRunbookInternal(Map params, User user)
    {
        if (params == null)
        {
            throw new RuntimeException("Missing parmater in startRunbook() call");
        }

        Map<String, String> result = triggerRunbook(params, user);
        String processID = result.get(PROCESS_ID);
        result.remove(PROCESS_ID);
        result.remove(PROCESS_TIMEOUT);

        result.put(Constants.EXECUTE_PROCESSID, processID);
        return result;
    } // startRunbookInternal

    public String getRunbookStatusByString(String token, String processID)
    {
        return getQueryStringFromPairs(getRunbookStatus(token, processID));
    }

    public Pair[] getRunbookStatus(String token, String processID)
    {
        Log.log.info("getRunbookStatus() is called with token = " + token + ", processID = " + processID);

        User user = validateUser(token);

        Map<String, String> status = getProcessStatus(processID, user.name);
        status.put("PROCESSID", processID);
        Pair[] result = getPairsFromMap(status);
        return result;
    } // getRunbookStatus

    public String[] getRunbookResultByString(String token, String processID)
    {
        return getUrlQueryStrings(getRunbookResult(token, processID));
    }

    public Row[] getRunbookResult(String token, String processID)
    {
        Log.log.info("getRunbookResult() is called with token = " + token + ", processID = " + processID);

        User user = validateUser(token);

        ArrayList<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            // check if request entry exists
            ProcessRequest process = WorksheetSearchAPI.findProcessRequestsById(processID, user.name).getData();

            if (process != null)
            {
                List<TaskResult> actionResults = WorksheetSearchAPI.findTaskResultByProcessId(processID, user.name).getRecords();
                for (TaskResult actionResult : actionResults)
                {
                    HashMap<String, String> row = new HashMap<String, String>();

                    row.put("ActionTask", actionResult.getTaskName());
                    row.put("Completion", actionResult.getCompletion());
                    row.put("Condition", actionResult.getCondition());
                    row.put("Severity", actionResult.getSeverity());
                    row.put("Duration", "" + actionResult.getDuration());
                    row.put("Address", actionResult.getAddress());
                    row.put("Summary", (actionResult.getSummary() == null ? "" :  actionResult.getSummary()));
                    row.put("Last updated", actionResult.ugetSysUpdatedDate());
                    row.put("Sys Updated On", actionResult.getSysUpdatedOn().toString());
                    row.put("ActionID", actionResult.getSysId());

                    result.add(row);
                }
            }
            else
            {
                throw new Exception("Invalid processID: " + processID);
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to get runbook details for processid " + processID + " : " + e.getMessage());
        }

        Row[] rows = new Row[result.size()];
        int i = 0;
        for (Map<String, String> r : result)
        {
            rows[i] = new Row();
            rows[i].setColumns(new Pair[r.keySet().size()]);
            int j = 0;
            for (String key : r.keySet())
            {
                (rows[i].getColumns())[j] = new Pair();
                (rows[i].getColumns())[j].setName(key.toUpperCase());
                (rows[i].getColumns())[j].setValue(r.get(key));
                ++j;
            }
            ++i;
        }
        return rows;
    } // getRunbookResult

    private List<Map<String, String>> getRunbookResultByWorksheetIDInternal(String problemID, String username)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            if(StringUtils.isNotBlank(problemID))
            {
                List<TaskResult> actionResults = WorksheetSearchAPI.findTaskResultByWorksheetId(problemID, null, username).getRecords();
                for (TaskResult actionResult : actionResults)
                {
                    HashMap<String, String> row = new HashMap<String, String>();
                    row.put("ActionTask", actionResult.getTaskName());
                    row.put("Completion", actionResult.getCompletion());
                    row.put("Condition", actionResult.getCondition());
                    row.put("Severity", actionResult.getSeverity());
                    row.put("Duration", "" + actionResult.getDuration());
                    row.put("Address", actionResult.getAddress());
                    row.put("Summary", (actionResult.getSummary() == null ? "" :  actionResult.getSummary()));
                    row.put("Last updated", actionResult.ugetSysUpdatedDate());
                    row.put("Sys Updated On", actionResult.getSysUpdatedOn().toString());
                    row.put("ActionID", actionResult.getSysId());
                    result.add(row);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to get runbook details for problemID " + problemID + " : " + e.getMessage());
        }

        return result;
    } // getRunbookResultByWorksheetIDInternal

    private static String getLightResultByWorksheetIDInternal(String problemId, String username)
    {
        StringBuffer result = new StringBuffer();

        try
        {
            if(StringUtils.isNotBlank(problemId))
            {
                List<TaskResult> actionResults = WorksheetSearchAPI.findTaskResultByWorksheetId(problemId, null, username).getRecords();
                for (TaskResult actionResult : actionResults)
                {
                    result.append(lightResultGenerator(actionResult, SUMMARY));
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to get detail result for problemID " + problemId + " : " + e.getMessage());
        }

        return result.toString();
    } // getLightResultByWorksheetIDInternal

    private static String lightResultGenerator(TaskResult actionResult, String fieldName)
    {
        StringBuilder result = new StringBuilder();
        if(actionResult != null)
        {
            result.append("<!-- ACTION_RESULT_START ActionResultID=\"");
            result.append(actionResult.getSysId());
            result.append("\" ActionTaskName=\"");
            result.append(actionResult.getTaskFullName());
            result.append("\" -->");
            if(fieldName.equals(SUMMARY))
            {
                result.append((actionResult.getSummary() == null ? "" :  actionResult.getSummary()));
            }
            if(fieldName.equals(DETAIL))
            {
                result.append((actionResult.getDetail() == null ? "" :  actionResult.getDetail()));
            }

            result.append("<!-- ACTION_RESULT_END -->\n");
        }
        return result.toString();
    }

    private static ActionTaskResult simpleResultGenerator(TaskResult actionResult, String fieldName) throws XMLStreamException
    {
        ActionTaskResult result = null;
        if(actionResult != null)
        {
            String actionTaskName = "";
            if(actionResult.getTaskFullName() != null)
            {
                actionTaskName = actionResult.getTaskFullName();
            }

            String actionTaskSysId = actionResult.getSysId();
            String condition = actionResult.getCondition();
            String severity = actionResult.getSeverity();

            if(StringUtils.isBlank(fieldName))
            {
                result = new ActionTaskResult(actionTaskSysId, actionTaskName, actionResult.getSummary() == null ? "" :  actionResult.getSummary(), actionResult.getDetail() == null ? "" :  actionResult.getDetail(), condition, severity);
            }
            else if(SUMMARY.equals(fieldName))
            {
                result = new ActionTaskResult(actionTaskSysId, actionTaskName, actionResult.getSummary() == null ? "" :  actionResult.getSummary(), condition, severity);
            }
            else if(DETAIL.equals(fieldName))
            {
                result = new ActionTaskResult(actionTaskSysId, actionTaskName, null, actionResult.getDetail() == null ? "" :  actionResult.getDetail(), condition, severity);
            }
        }
        return result;
    }

    private String[] getUrlQueryStrings(Row[] rows)
    {
        if (rows == null)
        {
            return new String[0];
        }
        String[] results = new String[rows.length];
        for (int i = 0; i < rows.length; ++i)
        {
            results[i] = getQueryStringFromPairs(rows[i].getColumns());
        }
        return results;
    }

    public String[] getRunbookResultByWorksheetIDByString(String token, String worksheetID)
    {
        return getUrlQueryStrings(getRunbookResultByWorksheetID(token, worksheetID));
    }

    public Row[] getRunbookResultByWorksheetID(String token, String worksheetID)
    {
        Log.log.info("getRunbookResultByWorksheetID() is called with token = " + token + ", worksheetID = " + worksheetID);
        User user = validateUser(token);

        List<Map<String, String>> result = getRunbookResultByWorksheetIDInternal(worksheetID, user.name);
        Row[] rows = new Row[result.size()];
        int i = 0;
        for (Map<String, String> r : result)
        {
            rows[i] = new Row();
            rows[i].setColumns(new Pair[r.keySet().size()]);
            int j = 0;
            for (String key : r.keySet())
            {
                (rows[i].getColumns())[j] = new Pair();
                (rows[i].getColumns())[j].setName(key.toUpperCase());
                (rows[i].getColumns())[j].setValue(r.get(key));
                ++j;
            }
            ++i;
        }
        return rows;
    } // getRunbookResultByWorksheetID

    private static String getDetailResultByWorksheetIDInternal(String problemId, String[] responseActionTasks, String username)
    {
        List<String> tasks = null;

        if(responseActionTasks != null)
        {
            tasks = Arrays.asList(responseActionTasks);
        }

        StringBuilder result = new  StringBuilder("");

        try
        {
            if (StringUtils.isNotBlank(problemId))
            {
                List<TaskResult> actionResults = WorksheetSearchAPI.findTaskResultByWorksheetId(problemId, null, username).getRecords();
                for (TaskResult actionResult : actionResults)
                {
                    if(tasks != null && tasks.size() > 0)
                    {
                        if(actionResult.getTaskFullName() != null && tasks.contains(actionResult.getTaskFullName()))
                        {
                            //result += (actionResult.getActionResultLob() == null ? "" :  actionResult.getActionResultLob().getUSummary());
                            result.append(lightResultGenerator(actionResult, SUMMARY));
                        }
                    }
                    else
                    {
                        //result += (actionResult.getActionResultLob() == null ? "" :  actionResult.getActionResultLob().getUSummary());
                        result.append(lightResultGenerator(actionResult, SUMMARY));
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to get detail result for problemID " + problemId + " : " + e.getMessage());
        }

        return result.toString();
    } // getDetailResultByWorksheetID

    private static List<ActionTaskResult> getDetailResultByWorksheetID(String problemId, String[] responseActionTasks, String username)
    {
        List<String> tasks = null;

        if(responseActionTasks != null)
        {
            tasks = Arrays.asList(responseActionTasks);
        }

        List<ActionTaskResult> result = new  ArrayList<ActionTaskResult>();

        try
        {
            if (StringUtils.isNotBlank(problemId))
            {
                List<TaskResult> actionResults = WorksheetSearchAPI.findTaskResultByWorksheetId(problemId, null, username).getRecords();
                for (TaskResult actionResult : actionResults)
                {
                    if(tasks != null && tasks.size() > 0)
                    {
                        if(actionResult.getTaskFullName() != null && tasks.contains(actionResult.getTaskFullName()))
                        {
                            result.add(simpleResultGenerator(actionResult, null));
                        }
                    }
                    else
                    {
                        result.add(simpleResultGenerator(actionResult, null));
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to get detail result for problemID " + problemId + " : " + e.getMessage());
        }

        return result;
    } // getDetailResultByWorksheetID

    public String getDetailResultByWorksheetID(String token, String worksheetID)
    {
        Log.log.info("getDetailResultByWorksheetID() is called with token = " + token + ", worksheetID = " + worksheetID);
        User user = validateUser(token);

        return getDetailResultByWorksheetIDInternal(worksheetID, new String[] {}, user.name);
    } // getDetailResultByWorksheetID

    public String getRawResultByWorksheetID(String token, String worksheetID)
    {
        Log.log.info("getDetailResultByWorksheetID() is called with token = " + token + ", worksheetID = " + worksheetID);
        User user = validateUser(token);

        return getRawResultByWorksheetIDInternal(worksheetID, user.name);
    } // getRawResultByWorksheetID

    private static String getRawResultByWorksheetIDInternal(String problemId, String username)
    {
        String result = "";

        try
        {
            if (StringUtils.isNotBlank(problemId))
            {
                List<TaskResult> actionResults = WorksheetSearchAPI.findTaskResultByWorksheetId(problemId, null, username).getRecords();
                for (TaskResult actionResult : actionResults)
                {
                    result += getRawResultByActionResult(actionResult);
                    result += "\n\n";
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to get detail result for problemID " + problemId + " : " + e.getMessage());
        }
        return result;
    } // getRawResultByWorksheetID

    private static String getRawResultByActionResult(TaskResult actionResult)
    {
        String result = "";

        if (actionResult != null)
        {
            String detail = "" + (actionResult.getRaw() == null ? "" :  actionResult.getRaw());
            detail = detail.substring(detail.indexOf("<pre>") + 5, detail.indexOf("</pre>"));
            result += detail + "\n";
        }

        return result;
    } // getRawResultByActionResultID

    private static class User
    {
        public String name;
        public String password;
        long lastAccessTime;

        public User(String u, String p)
        {
            name = u;
            password = p;
            lastAccessTime = System.currentTimeMillis();
        } // User

        public String toString()
        {
            return name;
        } // toString

        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            result = prime * result + ((password == null) ? 0 : password.hashCode());
            return result;
        } // hashCode

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            User other = (User) obj;
            if (name == null)
            {
                if (other.name != null) return false;
            }
            else if (!name.equals(other.name)) return false;
            if (password == null)
            {
                if (other.password != null) return false;
            }
            else if (!password.equals(other.password)) return false;
            return true;
        } // equals

    } // User

    /**
     *
     * @param username
     * @param password
     * @param processid
     * @param params in the format PROBLEMID=ccb484a914fe492490be7d9a46d2d269%26WIKI=bipul.test
     * @return
     */
    public String simpleAbortRunbook(String username, String password, String processid, String params)
    {
        User user = validateUser(username, password);

        // insert processid
        Map paramMap = StringUtils.urlToMap(params);
        paramMap.put(Constants.EXECUTE_PROCESSID, processid);
        paramMap.put(Constants.EXECUTE_USERID, user.name);

        Map status = abortRunbookInternal(paramMap, user);
        return StringUtils.mapToUrl(status);
    }// abortRunbookByString

    public Pair[] abortRunbook(String token, Pair[] params)
    {
        User user = validateUser(token);
        Map<String, String> abortParams = getMapFromPairs(params);
        abortParams.put(Constants.EXECUTE_USERID, user.name);
        Map status = abortRunbookInternal(abortParams, user);
        return getPairsFromMap(status);
    } // abortRunbook

    public String abortRunbookByString(String token, String params)
    {
        User user = validateUser(token);
        Map paramMap = StringUtils.urlToMap(params);
        Map<String, String> result = abortRunbookInternal(paramMap, user);
        return getQueryStringFromMap(result);
    }// abortRunbookByString

    public Pair[] abortRunbookWithLogin(String username, String password, Pair[] params)
    {
        User user = validateUser(username, password);
        Map<String, String> abortParams = getMapFromPairs(params);
        abortParams.put(Constants.EXECUTE_USERID, user.name);
        Map status = abortRunbookInternal(abortParams, user);

        return getPairsFromMap(status);
    } // abortRunbookWithLogin

    public String abortRunbookWithLoginByString(String username, String password, String params)
    {
        User user = validateUser(username, password);
        Map paramMap = StringUtils.urlToMap(params);
        Map<String, String> result = abortRunbookInternal(paramMap, user);
        return getQueryStringFromMap(result);
    }// abortRunbookWithLoginByString

    private Map<String, String> abortRunbookInternal(Map<String, String> params, User user)
    {
        Map<String, String> result = new HashMap<String, String>();

        StringBuilder errors = new StringBuilder();
        errors.append("<ERRORR>");

        //inject ths user id here because if it reches this point
        //user validation passed.
        params.put(Constants.EXECUTE_USERID, user.name);

        //PROCESSID, PROBLEMID, WIKI, and USERID is required to abort a runbook
        boolean isError = false;
        if(!params.containsKey(Constants.EXECUTE_PROCESSID))
        {
            isError = true;
            errors.append("<ERROR>" + Constants.EXECUTE_PROCESSID + " is a mandatory parameter for abort" + "</ERROR>\n");
        }
        if(!params.containsKey(Constants.EXECUTE_PROBLEMID))
        {
            isError = true;
            errors.append("<ERROR>" + Constants.EXECUTE_PROBLEMID + " is a mandatory parameter for abort" + "</ERROR>\n");
        }
        if(!params.containsKey(Constants.EXECUTE_WIKI))
        {
            isError = true;
            errors.append("<ERROR>" + Constants.EXECUTE_WIKI + " is a mandatory parameter for abort" + "</ERROR>\n");
        }
        /*
        if(!params.containsKey(Constants.EXECUTE_USERID))
        {
            isError = true;
            errors.append("<ERROR>" + Constants.EXECUTE_USERID + " is a mandatory parameter for abort" + "</ERROR>\n");
        }*/

        if(isError)
        {
            Log.log.error(errors.toString());
            result.put("Error", errors.toString());
        }
        else
        {
            try
            {
                result.put(STATUS, ExecuteRunbook.abortProcess(params));
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                result.put("Error", e.getMessage());
            }
        }
        return result;
    } // abortRunbookInternal

    public String sendEvent(String token, String params)
    {
        User user = validateUser(token);

        // get params
        Map paramsMap = StringUtils.urlToMap(params);
        String runbook = (String) paramsMap.get(Constants.EXECUTE_WIKI);
        String eventid = (String) paramsMap.get(Constants.EVENT_EVENTID);
        String eventReference = (String) paramsMap.get(Constants.EVENT_REFERENCE);
        String processid = (String) paramsMap.get(Constants.EXECUTE_PROCESSID);

        return sendEventInternal(user, runbook, eventid, eventReference, processid, paramsMap);
    } // sendEvent

    public String simpleSendEvent(String username, String password, String runbook, String eventid, String eventReference, String processid, String problemId, String alertId, String prbnumber, String reference, String params)
    {
        User user = validateUser(username, password);
        Map<String, String> eventParams = StringUtils.urlToMap(params);

        if(StringUtils.isNotEmpty(problemId))
        {
            eventParams.put(Constants.EXECUTE_PROBLEMID, problemId);
        }
        if(StringUtils.isNotEmpty(alertId))
        {
            eventParams.put(WorksheetUtils.ALERTID, alertId);
        }
        if(StringUtils.isNotEmpty(reference))
        {
            eventParams.put(WorksheetUtils.NUMBER, reference);
        }
        if(StringUtils.isNotEmpty(prbnumber))
        {
            eventParams.put(WorksheetUtils.REFERENCE, prbnumber);
        }

        return sendEventInternal(user, runbook, eventid, eventReference, processid, StringUtils.urlToMap(params));
    } // sendEvent

    private String sendEventInternal(User user, String runbook, String eventid, String eventReference, String processid, Map params)
    {
        String result = "";
//        boolean isStart = isPersistent; //if it's persistent event then start is always true;

        if (processid == null)
        {
            processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        }

        if (eventid == null)
        {
            throw new RuntimeException("Missing EVENT_EVENTID parameter");
        }

        if (processid == null && eventReference == null)
        {
            throw new RuntimeException("Missing PROCESSID or EVENT_REFERENCE - processid: " + processid + " event_reference: " + eventReference);
        }

        params.put(Constants.EXECUTE_USERID, user.toString());
        params.put(Constants.EXECUTE_PROCESSID, "");
        params.put(Constants.EXECUTE_WIKI, runbook);
        params.put(Constants.EVENT_EVENTID, eventid);
        params.put(Constants.EVENT_REFERENCE, eventReference);
        Main.getESB().sendMessage(Constants.ESB_NAME_RSCONTROL, "MAction.executeProcess", params);

        result = "Event Sent - eventid: " + eventid + " processid: " + processid + " event_reference: " + eventReference;
        Log.log.info(result);

        return result;
    } // sendEventInternal

    private String getActionTaskData(String username, String password, String[] actionTasks, String returnFieldName)
    {
        Log.log.info("getActionTaskSummary() is called");

        User user = validateUser(username, password);

        if(actionTasks == null || actionTasks.length == 0)
        {
            throw new RuntimeException("actionTasks parameter cannot be null or empty");
        }

        StringBuilder result = new StringBuilder();

        try
        {
            StringBuilder query = new StringBuilder();
            QueryDTO queryDTO = new QueryDTO();
            String taskNames = StringUtils.arrayToString(actionTasks, ",");
            queryDTO.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, taskNames, "taskFullName", QueryFilter.EQUALS));
            
            List<TaskResult> actionResults = WorksheetSearchAPI.searchTaskResults(queryDTO, user.name).getRecords();
            for(TaskResult taskResult : actionResults)
            {
                result.append(lightResultGenerator(taskResult, returnFieldName));
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to get summary result for the ActionTasks");
        }
        return result.toString();
    } // getActionTaskData

    /**
     * Retrieves the result summary of incoming action tasks.
     *
     * @param username
     * @param password
     * @param actionTasks is an Array of String that includes the ActionTask name in the form "MyTaskName#MyNameSpace"
     * @return
     */
    public String getActionTaskSummary(String username, String password, String[] actionTasks)
    {
        Log.log.info("getActionTaskSummary() is called");
        return getActionTaskData(username, password, actionTasks, SUMMARY);
    } // getActionTaskSummary

    /**
     * Retrieves the result detail of incoming action tasks.
     *
     * @param username
     * @param password
     * @param actionTasks is an Array of String that includes the ActionTask name in the form "MyTaskName#MyNameSpace"
     * @return
     */
    public String getActionTaskDetail(String username, String password, String[] actionTasks)
    {
        Log.log.info("getActionTaskDetail() is called");
        return getActionTaskData(username, password, actionTasks, DETAIL);
    } // getActionTaskSummary

    /**
     * Creates a new task or updates it if it's already existed.
     *
     * @param username
     * @param password
     * @param taskName
     * @param module
     * @param runbook
     * @param cronParams
     * @param expression like "0 0 12 * * ?" which is fired at 12pm (noon) every day.
     * @param startTime
     * @param endTime
     * @param userid
     * @param active
     * @return
     */
    public boolean scheduleTask(String username, String password, String taskName, String module, String runbook, Pair[] cronParams, String expression, Date startTime, Date endTime, String userid, Boolean active)
    {
        Log.log.info("scheduleTask() is called");
        //if there is any problem login it's expected to
        //throw RuntimeException.
        login(username, password);

        Map<String, String> cronParamMap = getMapFromPairs(cronParams);
        return CronAPI.scheduleTask(taskName, module, runbook, cronParamMap, expression, startTime, endTime, userid, active);
    }

    /**
     * Removes the task.
     *
     * @param username
     * @param password
     * @param taskName
     * @return
     */
    public boolean unscheduleTask(String username, String password, String taskName)
    {
        Log.log.info("unscheduleTask() is called for the task: " + taskName);

        //if there is any problem login it's expected to
        //throw RuntimeException.
        login(username, password);

        return CronAPI.unscheduleTask(taskName, username);
    }

    /**
     * Activates a Task.
     *
     * @param username
     * @param password
     * @param taskName
     * @return
     */
    public boolean activateTask(String username, String password, String taskName)
    {
        Log.log.info("activateTask() is called for the task: " + taskName);
        //if there is any problem login it's expected to
        //throw RuntimeException.
        login(username, password);

        return CronAPI.activateTask(taskName, username);
    }

    /**
     * Deactivates a Task.
     *
     * @param username
     * @param password
     * @param taskName
     * @return
     */
    public boolean deactivateTask(String username, String password, String taskName)
    {
        Log.log.info("deactivateTask() is called for the task: " + taskName);
        //if there is any problem login it's expected to
        //throw RuntimeException.
        login(username, password);

        return CronAPI.deactivateTask(taskName, username);
    }

    public String processGatewayFilterByUser(String username, String password, Pair[] params)
    {
        validateUser(username, password);
        return processGatewayFilter(params);
    }
    
    public String processGatewayFilterByToken(String token, Pair[] params)
    {
        validateUser(token);
        return processGatewayFilter(params);
    } // eventGateway
    
    /**
     * This method receives message from other system to process events in a gateway. 
     * The following parameters (as Pair) could be passed to avoid system properties
     * 
     * GATEWAY_QUEUE=NETCOOL
     * GATEWAY_MESSAGE_HANDLER=MNetcool (heavily case sensitive)
     * GATEWAY_FILTER_NAME=MyEvtFilter1,MyEvtFilter2 (heavily case sensitive)
     * 
     * @param params
     * @return
     */
    private String processGatewayFilter(Pair[] params)
    {
        String result = "Event received and submitted for processing by gateway";
        
        Map<String, String> gatewayParam = new HashMap<String, String>();
        String tmpEventGatewayQueueName = eventGatewayQueueName;
        String tmpEventGatewayTopicName = eventGatewayTopicName;
        String tmpEventGatewayClassmethod = eventGatewayClassmethod;
        String tmpEventGatewayFilterIds = eventGatewayFilterIds;
        
        for(Pair pair : params)
        {
            String paramName = pair.getName();
            String paramValue = pair.getValue();
            if("GATEWAY_QUEUE".equalsIgnoreCase(paramName) && StringUtils.isNotBlank(paramValue))
            {
                tmpEventGatewayQueueName = paramValue.toUpperCase();
                tmpEventGatewayTopicName = paramValue.toUpperCase() + "_TOPIC";
            }
            if("GATEWAY_MESSAGE_HANDLER".equalsIgnoreCase(paramName) && StringUtils.isNotBlank(paramValue))
            {
                tmpEventGatewayClassmethod = paramValue + ".processFilter";
            }
            if("GATEWAY_FILTER_NAME".equalsIgnoreCase(paramName) && StringUtils.isNotBlank(paramValue))
            {
                tmpEventGatewayFilterIds = paramValue;
            }

            gatewayParam.put(pair.getName(), pair.getValue());
            Log.log.debug("pair:" + pair.getName() + ", value:" + pair.getValue());
        }
        
        if(tmpEventGatewayTopicName == null || tmpEventGatewayClassmethod == null || tmpEventGatewayFilterIds == null)
        {
            Log.log.warn("Following system properties must be set before this method is being called:\n 1. "+ EVENT_GATEWAY_QUEUE +"\n 2. "+ EVENT_GATEWAY_MESSAGEHANDLER +"\n 3. " + EVENT_GATEWAY_FILTER_NAME);
            throw new RuntimeException("Resolve is not ready to accept this call. Following system properties must be set and restart RSVIEW. (1) "+ EVENT_GATEWAY_QUEUE +" (2) "+ EVENT_GATEWAY_MESSAGEHANDLER +"(3) " + EVENT_GATEWAY_FILTER_NAME);
        }
        else
        {
            List<String> filterIds = StringUtils.convertStringToList(tmpEventGatewayFilterIds, ",");
            for(String filterId : filterIds)
            {
                gatewayParam.put("FILTER_ID", filterId);
                //sending to the topic, only the primary gateway will process it.
                if(!MainBase.esb.sendMessage(tmpEventGatewayTopicName, tmpEventGatewayClassmethod, gatewayParam))
                {
                    result = "Something terrible happened with ESB, event could not be submitted to " + tmpEventGatewayQueueName + " for filter: " + filterId;
                }
            }
        }
        return result; 
    } // eventGateway
} // WebserviceListener
