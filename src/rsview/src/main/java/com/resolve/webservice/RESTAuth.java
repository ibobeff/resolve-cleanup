/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.webservice;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.ValidationException;

import com.resolve.rsview.auth.AuthSession;
import com.resolve.rsview.auth.Authentication;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RESTAuth
{
    private static int tokenTimeout = 1209600; // default: 2 weeks
    private static int maxTokenCacheSize = 10000; // default value
    
    private static Map<String, AuthSession> tokenCache = null;

    private RESTAuth() {
        tokenCache = new ConcurrentHashMap<String, AuthSession>();
        loadSystemProperties();
    }
    
    public static final RESTAuth instance = new RESTAuth();
    
    public static int getTokenCacheSize() {
        
        return tokenCache.size();
    }
    
    public static int getTokenTimeout() {
        
        return tokenTimeout;
    }
    
    private void loadSystemProperties() {
        
        try {
            PropertiesVO timeout = PropertiesUtil.findPropertyByName("rest.token.timeout");
            if(StringUtils.isNotEmpty(timeout.getUValue()))
                tokenTimeout = new Integer(timeout.getUValue()).intValue();

            PropertiesVO maxCacheSize = PropertiesUtil.findPropertyByName("rest.max.token.cache.size");
            if(StringUtils.isNotEmpty(maxCacheSize.getUValue()))
                maxTokenCacheSize = new Integer(maxCacheSize.getUValue()).intValue();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
            
    }
    
	public String validateUser(String username, String pass) {

		String token = null;

		if (Authentication.authenticate(username, pass).valid) {
			token = getExistingToken(username);
			if (token == null) {
				SecureRandom random = new SecureRandom();
				token = new BigInteger(130, random).toString(32);

				addToTokenCache(username, token);
			}
		}

		return token;
	}
    
    public String logoutWithToken(String token) {

        String result = null;
        
        AuthSession session = tokenCache.get(token);
        
        if(session == null)
            result = "Error: Token is not recognized.";

        else {
            tokenCache.remove(token);
            result = "Logout successful.";
        }
        
        return result;
    }

    String validateToken(String token) throws Exception {
        
        String username = "";
        
        if(StringUtils.isBlank(token) || tokenCache.get(token) == null)
            throw new Exception("We didn't recoginze your token. Please try again.");
        
        AuthSession session = tokenCache.get(token);
        if(session == null)
            throw new Exception("We didn't recoginze your token. Please try again.");
        
        if((System.currentTimeMillis() - session.getUpdateTime())/1000 >= tokenTimeout) {
            if(tokenCache.containsKey(token))
                tokenCache.remove(token);
            throw new Exception("Token is expired. Please login in again.");
        }

        username = updateTokenCache(token);
        
        return username;
    }
    
    private void addToTokenCache(String username, String token) {
        
        AuthSession session = new AuthSession();
        session.setUsername(username);
        session.setToken(token);
        session.setUpdateTime(System.currentTimeMillis());
        
        tokenCache.put(token, session);
        Log.log.debug("added token: " + token);
        Log.log.debug("token cache size = " + tokenCache.size());
        
        if(tokenCache != null && getTokenCacheSize() != 0 && getTokenCacheSize() > maxTokenCacheSize) {
            try {
                CleanTokenCache thread = new CleanTokenCache();
                thread.start();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    private String updateTokenCache(String token) {
        
        AuthSession authSession = tokenCache.get(token);
        authSession.setUpdateTime(System.currentTimeMillis());
        
        tokenCache.put(token, authSession);
        
        return authSession.getUsername();
    }
    
    private String getExistingToken(String username) {

        Set<String> tokens = tokenCache.keySet();
        
        for(Iterator<String> it = tokens.iterator(); it.hasNext();) {
            String token = it.next();
            AuthSession authSession = tokenCache.get(token);
            String user = authSession.getUsername();
            if(user != null && user.equals(username)) {
                if(System.currentTimeMillis() - authSession.getUpdateTime() <= tokenTimeout*1000)
                    return token;
                else {
                    tokenCache.remove(token);
                    break;
                }
            }
        }

        return null;
    }
    
    private class CleanTokenCache extends Thread implements Runnable {
        
        @Override
        public void run() {
            List<String> expiredTokens = new ArrayList<String>();
            
            Set<String> keys = tokenCache.keySet();
            for(Iterator<String> it=keys.iterator(); it.hasNext();) {
                String key = (String)it.next();
                AuthSession authSession = tokenCache.get(key);
                
                if(System.currentTimeMillis() - authSession.getUpdateTime() >= tokenTimeout*1000)
                    expiredTokens.add(key);
            }
            
            for(String token:expiredTokens)
                tokenCache.remove(token);
        }
    } // CleanTokenCache
    
	protected class AuthorizationException extends Exception {

		private static final long serialVersionUID = 1L;

		public AuthorizationException(String message) {
			super(message);
		}
	}

	protected String authorizeUsingToken(HttpServletRequest request) throws AuthorizationException, ValidationException {
		String token = request.getHeader("Authorization");
		if (StringUtils.isBlank(token)) {
			token = (String) request.getParameter("token");
		}
		token = ESAPI.validator().getValidInput("Invalid input token ", token, "HTTPURL", 4000, true);

		if (StringUtils.isBlank(token) || token.equals("null")) {
			token = RESTService.getCookie(request, "RESTTOKEN");
		}

		if (StringUtils.isBlank(token)) {
			throw new AuthorizationException("Invalid token");
		}

		String username = null;
		try {
			username = RESTAuth.instance.validateToken(token);
		} catch (Exception e) {
			throw new AuthorizationException(e.getLocalizedMessage());
		}
		return username;
	}
    
} // RESTfulAuth
