/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.webservice;

public class Pair
{
    private String name;
    private String value;

    public Pair()
    {
        
    }

    public Pair(String name, String value)
    {
        this.name = name;
        this.value = value;
    }
    
    public String getName()
    {
        return name;
    } // getName

    public void setName(String n)
    {
        name = n;
    } // setName

    public String getValue()
    {
        return value;
    } // getValue

    public void setValue(String v)
    {
        value = v;
    } // setValue

    public String toString()
    {
        return "(" + name + "=" + value + ")";
    } // toString
} // Pair
