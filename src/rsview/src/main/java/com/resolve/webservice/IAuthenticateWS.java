package com.resolve.webservice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.netty.handler.codec.http.HttpResponse;

public interface IAuthenticateWS {
	
	public String login(String username, String password);
	
//	public String logout();
	
	public WSUser login(HttpServletRequest request)throws ServletException, IOException;
//	
//	public HttpResponse logout(HttpServletRequest request, HttpServletResponse response);
}
