/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/
package com.resolve.search.saas;

import static com.resolve.util.Log.log;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.dateHistogram;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.RegexpQueryBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.ExtendedBounds;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalDateHistogram;
import org.elasticsearch.search.aggregations.bucket.missing.Missing;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.cardinality.CardinalityAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.cardinality.InternalCardinality;
import org.elasticsearch.search.sort.SortOrder;
import org.hibernate.query.Query;
import org.hibernate.type.TimestampType;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.resolve.persistence.model.AggrExecSummary;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ExecutionSummary;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SaaSReport {
	public static final String BUCKET_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String ALL = "All";
	public static final String INCIDENT = "Incident";
	public static final String EVENT = "Event";
	public static final String LEGACY_NETCOOL = "legacy_netcool";
	
	private static final String EXEC_SUMMARY_INDX_RBC_FIELD_NAME = "rbc";
	private static final String EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME = "runbook.lower";
	private static final String NAMESPACE_SEPARATOR = ".";
	private static final String WILDCARD_CHARACTER = "*";
	private static final String RESOLVE_NAMESPACE = "resolve";
	private static final String DEVOPS_NAMESPACE = "devops";
	private static final String SYSTEM_NAMESPACE = "system";
	private static final String SYSTEMADMIN_NAMESPACE = "systemadmin";
	private static final String EXEC_SUMMARY_INDX_SYSUPDATEDON_FIELD_NAME = "sysUpdatedOn";
	private static final String USAGE_SUMMARY_V2_OVER_A_DAY_HISTOGRAM_NAME = "over_a_day";
	private static final String UNDESCORE = "_";
	private static final String EXEC_SUMMARY_INDX_SYSORG_FIELD_NAME = "sysOrg";
	private static final String HAVING_AGGR_NAME_PREFIX = "having";
	private static final String HAVING_SYSORG_AGGR_NAME = HAVING_AGGR_NAME_PREFIX + UNDESCORE + 
														  EXEC_SUMMARY_INDX_SYSORG_FIELD_NAME;
	private static final String MISSING_AGGR_NAME_PREFIX = "missing";
	private static final String MISSING_SYSORG_AGGR_NAME = MISSING_AGGR_NAME_PREFIX + UNDESCORE +
														   EXEC_SUMMARY_INDX_SYSORG_FIELD_NAME;
	private static final String EXEC_SUMMARY_INDX_SYSORGID_KEYWORD_FIELD_NAME = "sysOrg.id";
	private static final String ANY_TEXT_REGEXP = ".+";
	private static final String UNIQUE_AGGR_NAME_PREFIX = "unique";
	private static final String UNIQUE_SYSORG_AGGR_NAME = UNIQUE_AGGR_NAME_PREFIX + UNDESCORE + 
														  EXEC_SUMMARY_INDX_SYSORG_FIELD_NAME;
	private static final String EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME = "rbcSource";
	private static final String UNIQUE_SYSORG_RBCSOURCE_AGGR_NAME = UNIQUE_SYSORG_AGGR_NAME + UNDESCORE +
																	EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME;
	private static final String UNIQUE_SYSORG_MISSING_RBCSOURCE_AGGR_NAME = UNIQUE_SYSORG_AGGR_NAME + UNDESCORE +
																			MISSING_AGGR_NAME_PREFIX + UNDESCORE +
																			EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME;
	private static final String MISSING_SYSORG_RBCSOURCE_AGGR_NAME = MISSING_SYSORG_AGGR_NAME + UNDESCORE +
																	 EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME;
	private static final String UNIQUE_SYSORG_MISSING_RBCSOURCE_LEGACY_NETCOOL_AGGR_NAME = 
																				MISSING_SYSORG_RBCSOURCE_AGGR_NAME + 
																				UNDESCORE + LEGACY_NETCOOL;
	private static final String LEGACY_EVENT_TEXT_REGEXP = "/\\$Event";
	private static final String MISSING_SYSORG_MISSING_RBCSOURCE_AGGR_NAME = MISSING_SYSORG_AGGR_NAME + UNDESCORE +
																			 MISSING_AGGR_NAME_PREFIX + UNDESCORE +
																			 EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME;
	private static final String MISSING_SYSORG_MISSING_RBCSOURCE_LEGACY_NETCOOL_AGGR_NAME = 
																			MISSING_SYSORG_MISSING_RBCSOURCE_AGGR_NAME + 
																			UNDESCORE + LEGACY_NETCOOL;
																			
	private static final String ABORT_LC = "abort";
	private static final String COLON = ":";
	/* Max # of Sys Orgs supported is 1024 */
	private static final int SUPPORTED_MAX_UNIQUE_ORG_SIZE = 1024;
	/* Max # of RBC Sources supported is 4096 */
	private static final int SUPPORTED_MAX_UNIQUE_RBCSOURCE_SIZE = 4096;
	
    /**
     * Get the number of Resolution Records being used from starting date to now
     * @return the number of used Resoliution Records
     */
    public static long getUsedResolutionRecords() {
        
        long totalUsed = 0;
        String searchTerm = "{'size': 0, 'query': {'constant_score': {'filter': {'range': {'sysUpdatedOn': {'from': '2016-03-01 00:00:00','format': 'yyyy-MM-dd HH:mm:ss'}}}}}";
        
        try {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.setSearchTerm(searchTerm);
            
            QueryBuilder queryBuilder = SearchUtils.createBoolQueryBuilder(queryDTO);
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_WORKSHEET);
            searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);
            
            SearchResponse response = searchRequestBuilder.execute().actionGet();
            totalUsed = response.getHits().getTotalHits();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        Log.log.info("Totol usage = " + totalUsed);
        
        return totalUsed;
    } // getUsedResolutionRecords()
    
    /**
     * Retrieve the usage per month from starting month up to current month
     * @return an array of usage per month
     */
    public static ResponseDTO<Map<String, Object>> getMonthlyUsage(Customer customer, String timezone, Integer previousLicYear) {
        
        long totalUsed = 0;
        String message = "Time zone is: " + timezone;
        long  startDate = customer.getStartDate().getTime();
        Calendar c = Calendar.getInstance();
        
        c.setTime(customer.getStartDate());
        c.add(Calendar.DAY_OF_YEAR, customer.getDuration());
        long endDate = c.getTimeInMillis();
        
        Log.log.debug("License Start Timestamp : " + new Date(startDate).toString() + 
                      ", Ends Before Timestamp : " + new Date(endDate).toString());
        
        long currTime = System.currentTimeMillis();
        
        // Determine # of years since start date
        
        LocalDate lds = LocalDateTime.ofInstant(new Date(startDate).toInstant(), ZoneId.systemDefault()).toLocalDate();
        LocalDate ldc = LocalDateTime.ofInstant(new Date(currTime).toInstant(), ZoneId.systemDefault()).toLocalDate();
        Period pbsc = Period.between(lds, ldc);
        
        Log.log.debug("Period between License Local Start Date and Current Local Date is : " +
                      pbsc.getYears() + " Year(s), " + pbsc.getMonths() + ", Month(s), " +
                      pbsc.getDays() + "Day(s)");
        
        int pbscYears = pbsc.getYears();
        
        int subtractYears = 0;
        
        Integer effectivePrevLicYear = (previousLicYear != null) ? previousLicYear : new Integer(customer.getPreviousLicYear());
        
        if (effectivePrevLicYear.intValue() <= pbscYears)
        {
            subtractYears = effectivePrevLicYear.intValue();
        }
        else
        {
            subtractYears = pbscYears;
        }
        
        Calendar licYearStartDateCal = Calendar.getInstance();
        licYearStartDateCal.setTime(customer.getStartDate());
        licYearStartDateCal.add(Calendar.YEAR, (pbscYears - subtractYears));
        
        long licYearStartDateLong = licYearStartDateCal.getTimeInMillis();
        
        Calendar licYearEndDateCal = Calendar.getInstance();
        licYearEndDateCal.setTimeInMillis(licYearStartDateLong);
        licYearEndDateCal.add(Calendar.YEAR, 1);
        
        long licYearEndDateLong = licYearEndDateCal.getTimeInMillis();
        
        Log.log.debug("License Year Start Timestamp : " + new Date(licYearStartDateLong).toString() + 
                      ", Ends Before Timestamp : " + new Date(licYearEndDateLong).toString());
        
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        Map<String, Object> monthlyUsage = new TreeMap<String, Object>();
        
        if (licYearStartDateLong >= endDate)
        {
            Log.log.debug("Selected license year start time " + new Date(licYearStartDateLong).toString() + " is past license expiration time of " + 
                          new Date(endDate).toString() + "!!!");
            result.setData(monthlyUsage);
            result.setSuccess(false);
        }
        
        Map<String, Long> usages = new TreeMap<String, Long>();
        
        try {
            BoolQueryBuilder queryBuilder = boolQuery();
            
            RangeQueryBuilder rangeQueryBuilder = rangeQuery("sysUpdatedOn").from(licYearStartDateLong).to(licYearEndDateLong).includeLower(true).includeUpper(false);
            
            queryBuilder.must(rangeQueryBuilder);
            queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*resolve.*"));
            queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*devops.*"));
            queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*system.*"));
            queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*systemadmin.*"));
            
            SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_WORKSHEET);
            searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            searchRequestBuilder.setSize(0);
            searchRequestBuilder.setQuery(queryBuilder);

            DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram("monthly").field("sysUpdatedOn").dateHistogramInterval(new DateHistogramInterval("month")).minDocCount(0).timeZone(DateTimeZone.forID(timezone));
            searchRequestBuilder.addAggregation(dateHistogramAgg);
 
            SearchResponse response = searchRequestBuilder.execute().actionGet();

            totalUsed = response.getHits().getTotalHits();
            
            InternalDateHistogram agg = response.getAggregations().get("monthly");
            
            for(InternalDateHistogram.Bucket bucket : agg.getBuckets()) {
                String key = bucket.getKeyAsString();
                long docCount = bucket.getDocCount();
                usages.put(key, new Long(docCount));
            }
            
            List<Map<String, Object>> totals = new ArrayList<Map<String, Object>>();
            Map<String, Object> records = new HashMap<String, Object>();
            records.put("Start", new Date(licYearStartDateLong));
            records.put("Duration", new Integer(365));
            records.put("Quota", new Long(customer.getQuota()));
            records.put("Used", new Long(totalUsed));
            totals.add(records);
            result.setRecords(totals);
            
            monthlyUsage.putAll(usages);
            result.setData(monthlyUsage);
            
            result.setSuccess(true);
        } catch(Exception e) {
            message = "None of the configured ElasticSearch nodes are available.";
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false);
        }

        result.setMessage(message);
        
        return result;
    } // getMonthlyUsage()
    
    public static ResponseDTO<Map<String, Object>> getEventUsage(Customer customer, String timezone) {
        
        String message = "Time zone is: " + timezone;
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        try {
            List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
            
            Map<String, Object> record = new HashMap<String, Object>();
            record.put("Name", customer.getCustomerName());
            record.put("Start", customer.getStartDate());
            record.put("Duration", new Integer(customer.getDuration()));
            
            records.add(record);
            
            Map<String, Object> dailyEventUsage = new TreeMap<String, Object>();
            
            Map<String, Long> eventUsages = getDailyEventUsage(customer.getStartDate(), timezone);
            Map<String, Long> archivedEventUsages = getDailyArchivedEventUsage(customer.getStartDate());
            
            dailyEventUsage.putAll(eventUsages);
            dailyEventUsage.putAll(archivedEventUsages);
            
            long total = 0;
            for(Iterator<Long> it=eventUsages.values().iterator(); it.hasNext();)
                total += it.next().longValue();
            Log.log.info("Total event used: " + total);
            for(Iterator<Long> it=archivedEventUsages.values().iterator(); it.hasNext();)
                total += it.next().longValue();
            Log.log.info("Total archived event used: " + total);
            
            result.setRecords(records);
            result.setData(dailyEventUsage);
            result.setSuccess(true);
            result.setTotal(total);
        } catch(Exception e) {
            message = "None of the configured ElasticSearch nodes are available.";
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false);
        }

        result.setMessage(message);
        
        return result;
    } // getEventUsage()
    
    public static ResponseDTO<Map<String, Object>> getTicketUsage(Customer customer, String timezone) {
        
        String message = "Time zone is: " + timezone;
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        try {
            List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
            
            Map<String, Object> record = new HashMap<String, Object>();
            record.put("Name", customer.getCustomerName());
            record.put("Start", customer.getStartDate());
            record.put("Duration", new Integer(customer.getDuration()));
            
            records.add(record);
            
            Map<String, Object> dailyTicketUsage = new TreeMap<String, Object>();
            
            Map<String, Long> ticketUsages = getDailyTicketUsage(customer.getStartDate(), timezone);
            Map<String, Long> archivedTicketUsages = getDailyArchivedTicketUsage(customer.getStartDate());
            
            dailyTicketUsage.putAll(ticketUsages);
            dailyTicketUsage.putAll(archivedTicketUsages);
            
            long total = 0;
            for(Iterator<Long> it=ticketUsages.values().iterator(); it.hasNext();)
                total += it.next().longValue();
            Log.log.info("Total ticket used: " + total);
            for(Iterator<Long> it=archivedTicketUsages.values().iterator(); it.hasNext();)
                total += it.next().longValue();
            Log.log.info("Total archived ticket used: " + total);

            result.setRecords(records);
            result.setData(dailyTicketUsage);
            result.setSuccess(true);
            result.setTotal(total);
        } catch(Exception e) {
            message = "None of the configured ElasticSearch nodes are available.";
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false);
        }

        result.setMessage(message);
        
        return result;
    } // getTicketUsage()
    
    public static ResponseDTO<Map<String, Object>> getLicenseInfo(Customer customer) {

        String message = "";

        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        try {

            List<Map<String, Object>> totals = new ArrayList<Map<String, Object>>();
            Map<String, Object> records = new HashMap<String, Object>();
            
            records.put("Name", customer.getCustomerName());
            records.put("Start", customer.getStartDate());
            records.put("Duration", new Integer(customer.getDuration()));
            records.put("EventQuota", new Long(customer.getEventQuota()));
            records.put("TicketQuota", new Long(customer.getTicketQuota()));
            
            totals.add(records);
            
            result.setRecords(totals);
            result.setSuccess(true);
        } catch(Exception e) {
            message = "None of the configured ElasticSearch nodes are available.";
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false);
        }
    
        result.setMessage(message);
        
        return result;
    } // getLicenseInfo()
    
    private static Map<String, Long> getDailyEventUsage(Date startDate, String timezone) throws Exception {
        
        // queryBuilder will fetch: reference = "" AND alertId != ""
        Map<String, String> mustFields = new HashMap<String, String>();
        mustFields.put("reference", "");
        Map<String, String> mustNotFields = new HashMap<String, String>();
        mustNotFields.put("alertId", "");

        SearchRequestBuilder searchRequestBuilder = filterUsage(startDate.getTime(), Long.MAX_VALUE, mustFields, mustNotFields);

        DateHistogramAggregationBuilder dateHistogramAgg = dateHistogram("daily").field("sysUpdatedOn").dateHistogramInterval(new DateHistogramInterval("day")).minDocCount(0).timeZone(DateTimeZone.forID(timezone));
        searchRequestBuilder.addAggregation(dateHistogramAgg);

        SearchResponse response = searchRequestBuilder.execute().actionGet();
        
        InternalDateHistogram agg = response.getAggregations().get("daily");
        
        Map<String, Long> usages = new TreeMap<String, Long>();
        
        if(agg == null || agg.getBuckets().size() == 0) {
            Log.log.warn("Daily Event Used: 0");
            return usages;
        }
        
        Log.log.debug("Daily Event Used: ");
        
        for(InternalDateHistogram.Bucket bucket : agg.getBuckets()) {
            String key = bucket.getKeyAsString();
            long docCount = bucket.getDocCount();
            usages.put(key, new Long(docCount));
            Log.log.debug(key + ": " + docCount);
        }
        
        return usages;
    } // getDailyEventUsage()
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static Map<String, Long> getDailyArchivedEventUsage(Date startDate) throws Exception {

        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        if(dbType == null)
            throw new Exception("No database type is available.");
        
        Map<String, Long> usages = new TreeMap<String, Long>();
        
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        Date todayOfLastYear = calendar.getTime();
        
        Log.log.debug((new Date(todayOfLastYear.getTime())));
        
        if(startDate.before(todayOfLastYear))
            startDate = todayOfLastYear;
        
        String func = "date";
        if(dbType.toLowerCase().startsWith("oracle"))
            func = "trunc";
        
        String sql = "SELECT " + func + "(sys_created_on), count(*) " +
                     "FROM archive_worksheet " +
                     "WHERE (u_reference = '' OR u_reference IS NULL) AND (u_alert_id != '' OR u_alert_id IS NOT NULL) " +
                     "AND (sys_created_on > :start_date) " + 
                     "AND (u_summary not like 'Runbook: resolve.%' AND u_summary not like 'Runbook: devops.%' " + 
                     "AND u_summary not like 'Runbook: System.%' AND u_summary not like 'Runbook: SystemAdmin.%') " +
                     "GROUP BY " + func + "(sys_created_on) " +
                     "ORDER BY " + func + "(sys_created_on) ASC";
        Log.log.debug(sql);
        
        try
        {
        	HibernateProxy.setCurrentUser("system");
        	Timestamp sd = new java.sql.Timestamp(startDate.getTime());
        	List<Object[]> resultList = (List<Object[]>) HibernateProxy.executeHibernateInitLocked(() -> {
        		Query sqlQuery = HibernateUtil.createSQLQuery(sql);
                sqlQuery.setParameter("start_date", sd, TimestampType.INSTANCE);
                return sqlQuery.list();
        	});
            
            
            for(int i=0; i<resultList.size(); i++) {
                Object[] map = (Object[])resultList.get(i);
                Long date = ((Date)map[0]).getTime();
                long usage = 0;
                
                if(map[1] instanceof BigInteger)
                    usage = ((BigInteger)map[1]).longValue();
                
                else if(map[1] instanceof BigDecimal)
                    usage = ((BigDecimal)map[1]).longValue();
                
                Log.log.debug(date + ": " + usage);
                    
                usages.put(date.toString(), usage);
            }
        }
        catch(Throwable t)
        {
        	Log.log.error(String.format("Error %sin getting daily archived event usage", 
				    					(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new Exception(t);
        }
        
        return usages;
    } // getDailyArchivedEventUsage()
    
    private static Map<String, Long> getDailyTicketUsage(Date startDate, String timezone) throws Exception {
        
        // queryBuilder1 will fetch: reference = "" AND alertId = ""
        Map<String, String> mustFields = new HashMap<String, String>();
        mustFields.put("reference", "");
        mustFields.put("alertId", "");
        
        SearchRequestBuilder searchRequestBuilder1 = filterUsage(startDate.getTime(), Long.MAX_VALUE, mustFields, null);
        
        // queryBuilder2 will fetch: reference != ""
        Map<String, String> mustNotFields = new HashMap<String, String>();
        mustNotFields.put("reference", "");
 
        SearchRequestBuilder searchRequestBuilder2 = filterUsage(startDate.getTime(), Long.MAX_VALUE, null, mustNotFields);

        DateHistogramAggregationBuilder dateHistogramAgg1 = dateHistogram("daily").field("sysUpdatedOn").dateHistogramInterval(new DateHistogramInterval("day")).minDocCount(0).timeZone(DateTimeZone.forID(timezone));
        searchRequestBuilder1.addAggregation(dateHistogramAgg1);

        DateHistogramAggregationBuilder dateHistogramAgg2 = dateHistogram("daily").field("sysUpdatedOn").dateHistogramInterval(new DateHistogramInterval("day")).minDocCount(0).timeZone(DateTimeZone.forID(timezone));

        CardinalityAggregationBuilder distinctAgg = AggregationBuilders.cardinality("distinct_reference").field("reference");
        dateHistogramAgg2.subAggregation(distinctAgg);

        searchRequestBuilder2.addAggregation(dateHistogramAgg2);
        
        SearchResponse response1 = searchRequestBuilder1.execute().actionGet();
        SearchResponse response2 = searchRequestBuilder2.execute().actionGet();
        
        InternalDateHistogram agg1 = response1.getAggregations().get("daily");
        InternalDateHistogram agg2 = response2.getAggregations().get("daily");
        
        Map<String, Long> usages = new TreeMap<String, Long>();
        
        Log.log.debug("Daily Ticket Used: ");
        
        Log.log.debug("Number of distinct tickets with reference == \"\" AND alertId = \"\"");
        
        for(InternalDateHistogram.Bucket bucket : agg1.getBuckets()) {
            String key = bucket.getKeyAsString();
            long docCount = bucket.getDocCount();
            usages.put(key, new Long(docCount));
            Log.log.debug(key + ": " + docCount);
        }
        
        Log.log.debug("Number of distinct tickets with reference != \"\"");
        
        for(InternalDateHistogram.Bucket bucket : agg2.getBuckets()) {
            Aggregations aggs = bucket.getAggregations();
            InternalCardinality distinctRef = aggs.get("distinct_reference");
            String value = distinctRef.getValueAsString();
            
            long docCount = 0;
            try {
                docCount = (new BigDecimal(value)).longValue();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
            
            String key = bucket.getKeyAsString();
            Log.log.debug(key + "= " + docCount);
            
            if(usages.get(key) != null) {
                Long count = usages.get(key);
                usages.put(key, new Long(count+docCount));
            }
            
            else
                usages.put(key, new Long(docCount));
        }
        
        return usages;
    } // getDailyTicketUsage()
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static Map<String, Long> getDailyArchivedTicketUsage(Date startDate) throws Exception {
        
        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        if(dbType == null)
            throw new Exception("No database type is available.");
        
        String func = "date";
        if(dbType.equalsIgnoreCase("oracle"))
            func = "trunc";
        
        Map<String, Long> usages = new TreeMap<String, Long>();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        Date todayOfLastYear = calendar.getTime();
        
        Log.log.debug("today of last year: " + (new Date(todayOfLastYear.getTime())));
        
        if(startDate.before(todayOfLastYear))
            startDate = todayOfLastYear;
        
        String sql1 = "SELECT " + func + "(sys_created_on), count(*) " +
                      "FROM archive_worksheet " +
                      "WHERE (u_reference = '' OR u_reference IS NULL) AND (u_alert_id = '' OR u_alert_id IS NULL) " +
                      "AND (sys_created_on > :start_date) " + 
                      "AND (u_summary not like 'Runbook: resolve.%' AND u_summary not like 'Runbook: devops.%' " + 
                      "AND u_summary not like 'Runbook: System.%' AND u_summary not like 'Runbook: SystemAdmin.%') " +
                      "GROUP BY " + func + "(sys_created_on) " +
                      "ORDER BY " + func + "(sys_created_on) ASC";
        String sql2 = "SELECT " + func + "(sys_created_on), count(distinct(u_reference)) " +
                      "FROM archive_worksheet " +
                      "WHERE (u_reference != '' OR u_reference IS NOT NULL) " +
                      "AND (sys_created_on > :start_date) " + 
                      "AND (u_summary not like 'Runbook: resolve.%' AND u_summary not like 'Runbook: devops.%' " + 
                      "AND u_summary not like 'Runbook: System.%' AND u_summary not like 'Runbook: SystemAdmin.%') " +
                      "GROUP BY " + func + "(sys_created_on) " +
                      "ORDER BY " + func + "(sys_created_on) ASC";
        
        Log.log.debug(sql1);
        
        Date startDateFinal = startDate;
        try
        {            
        	HibernateProxy.setCurrentUser("system");
        	List<Object[]> resultList1 = (List<Object[]>) HibernateProxy.executeHibernateInitLocked(() -> {
        		Query sqlQuery1 = HibernateUtil.createSQLQuery(sql1);
                sqlQuery1.setParameter("start_date", startDateFinal, TimestampType.INSTANCE);
                return sqlQuery1.list();
        	});
        	
        	List<Object[]> resultList2 = (List<Object[]>) HibernateProxy.executeHibernateInitLocked(() -> {
        		Query sqlQuery2 = HibernateUtil.createSQLQuery(sql2);
                sqlQuery2.setParameter("start_date", startDateFinal, TimestampType.INSTANCE);
                return sqlQuery2.list();
        	});            
            
            for(int i=0; i<resultList1.size(); i++) {
                Object[] map = (Object[])resultList1.get(i);
                Long date = ((Date)map[0]).getTime();
                long usage = 0;
                
                if(map[1] instanceof BigInteger)
                    usage = ((BigInteger)map[1]).longValue();
                
                else if(map[1] instanceof BigDecimal)
                    usage = ((BigDecimal)map[1]).longValue();
                
                Log.log.debug(date + ": " + usage);
                    
                usages.put(date.toString(), usage);
            }
            
            for(int i=0; i<resultList2.size(); i++) {
                Object[] map = (Object[])resultList2.get(i);
                Long date = ((Date)map[0]).getTime();
                long usage = 0;
                
                if(map[1] instanceof BigInteger)
                    usage = ((BigInteger)map[1]).longValue();
                
                else if(map[1] instanceof BigDecimal)
                    usage = ((BigDecimal)map[1]).longValue();
                
                Log.log.debug(date + ": " + usage);
                
                Long usage1 = usages.get(date.toString());
                if(usage1 == null)
                    usage1 = new Long(0);
                usages.put(date.toString(), usage1 + usage);
            }
        }
        catch(Throwable t)
        {
            Log.log.error(String.format("Error %sin getting daily archived ticket usage", 
            						    (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new Exception(t);
        }
        
        return usages;
    } // getDailyArchivedTicketUsage()
    
    private static SearchRequestBuilder filterUsage(long startDate, long endDate, Map<String, String> mustFields, Map<String, String> mustNotFields) throws Exception {
        
        BoolQueryBuilder queryBuilder = boolQuery();
        RangeQueryBuilder rangeQueryBuilder = rangeQuery("sysUpdatedOn").from(startDate).to(endDate);
        queryBuilder.must(rangeQueryBuilder);
        
        if(mustFields != null) {
            Set<String> fields = mustFields.keySet();
            for(String field:fields) {
                String value = mustFields.get(field);
                QueryBuilder builder = QueryBuilders.termQuery(field, value);
                queryBuilder.must(builder);
            }
        }
        
        if(mustNotFields != null) {
            Set<String> fields = mustNotFields.keySet();
            for(String field:fields) {
                String value = mustNotFields.get(field);
                QueryBuilder builder = QueryBuilders.termQuery(field, value);
                queryBuilder.mustNot(builder);
            }
        }
        
        queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*resolve.*"));
        queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*devops.*"));
        queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*system.*"));
        queryBuilder.mustNot(QueryBuilders.wildcardQuery("summary", "*systemadmin.*"));
        
        SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
        searchRequestBuilder.setIndices(SearchConstants.INDEX_NAME_WORKSHEET_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_WORKSHEET);
        searchRequestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        searchRequestBuilder.setSize(0);
        searchRequestBuilder.setQuery(queryBuilder);
        
        return searchRequestBuilder;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO getVolumeBasedUsageReport(Customer customer, String userType, String timeZone, Integer previousLicYear)
    {
        ResponseDTO result = new ResponseDTO();
        
        List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        Map<String, Object> record = new HashMap<String, Object>();
        
        long  startDate = customer.getStartDate().getTime();
        Calendar c = Calendar.getInstance();
        
        c.setTime(customer.getStartDate());
        c.add(Calendar.DAY_OF_YEAR, customer.getDuration());
        long endDate = c.getTimeInMillis();
        
        Log.log.debug("License Start Timestamp : " + new Date(startDate).toString() + 
                      ", Ends Before Timestamp : " + new Date(endDate).toString());
        
        long currTime = System.currentTimeMillis();
        
        // Determine # of years since start date
        
        LocalDate lds = LocalDateTime.ofInstant(new Date(startDate).toInstant(), ZoneId.systemDefault()).toLocalDate();
        LocalDate ldc = LocalDateTime.ofInstant(new Date(currTime).toInstant(), ZoneId.systemDefault()).toLocalDate();
        Period pbsc = Period.between(lds, ldc);
        
        Log.log.debug("Period between License Local Start Date and Current Local Date is : " +
                      pbsc.getYears() + " Year(s), " + pbsc.getMonths() + ", Month(s), " +
                      pbsc.getDays() + "Day(s)");
        
        int pbscYears = pbsc.getYears();
        
        int subtractYears = 0;
        
        Integer effectivePrevLicYear = (previousLicYear != null) ? previousLicYear : new Integer(customer.getPreviousLicYear());
        
        if (effectivePrevLicYear.intValue() <= pbscYears)
        {
            subtractYears = effectivePrevLicYear.intValue();
        }
        else
        {
            subtractYears = pbscYears;
        }
        
        Calendar licYearStartDateCal = Calendar.getInstance();
        licYearStartDateCal.setTime(customer.getStartDate());
        licYearStartDateCal.add(Calendar.YEAR, (pbscYears - subtractYears));
        
        long licYearStartDateLong = licYearStartDateCal.getTimeInMillis();
        
        Calendar licYearEndDateCal = Calendar.getInstance();
        licYearEndDateCal.setTimeInMillis(licYearStartDateLong);
        licYearEndDateCal.add(Calendar.YEAR, 1);
        
        long licYearEndDateLong = licYearEndDateCal.getTimeInMillis();
        
        Log.log.debug("License Year Start Timestamp : " + new Date(licYearStartDateLong).toString() + 
                      ", Ends Before Timestamp : " + new Date(licYearEndDateLong).toString());
        
        
        record.put("Name", customer.getCustomerName());
        record.put("Start", new Date(licYearStartDateLong));
        record.put("Duration", new Integer(365));
        
        records.add(record);
        
        Map<String,  Long[]> bucketMap = new LinkedHashMap<String, Long[]>();
        
        if (licYearStartDateLong >= endDate)
        {
            Log.log.debug("Selected license year start time " + new Date(licYearStartDateLong).toString() + " is past license expiration time of " + 
                          new Date(endDate).toString() + "!!!");
            result.setSuccess(false);
            result.setMessage("Selected license year start time " + new Date(licYearStartDateLong).toString() + " is past license expiration time of " + 
                              new Date(endDate).toString() + ".");
            result.setRecords(records);
            result.setData(bucketMap);
            return result;
        }
        
        SearchRequestBuilder srb = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
        srb.setIndices(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS).setTypes(SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY);
        srb.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        srb.setSize(0);

        SortOrder sortOrder = SortOrder.ASC;
        srb.addSort("sysUpdatedOn", sortOrder);
        
        BoolQueryBuilder boolQueryBuilder = boolQuery();
        RangeQueryBuilder rangeQueryBuilder = rangeQuery("sysUpdatedOn").from(licYearStartDateLong).to(licYearEndDateLong).includeLower(true).includeUpper(false);
        
        boolQueryBuilder.must(rangeQueryBuilder);
        
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME, 
				 											 WILDCARD_CHARACTER + RESOLVE_NAMESPACE + 
				 											 NAMESPACE_SEPARATOR + WILDCARD_CHARACTER));
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME,
				 											 WILDCARD_CHARACTER + DEVOPS_NAMESPACE + 
				 											 NAMESPACE_SEPARATOR + WILDCARD_CHARACTER));
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME, 
				 											 WILDCARD_CHARACTER + SYSTEM_NAMESPACE + 
				 											 NAMESPACE_SEPARATOR + WILDCARD_CHARACTER));
        boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME, 
				 											 WILDCARD_CHARACTER + SYSTEMADMIN_NAMESPACE + 
				 											 NAMESPACE_SEPARATOR + WILDCARD_CHARACTER));  
        
        srb.setQuery(boolQueryBuilder);
        
        DateHistogramAggregationBuilder dateHistogramBuilder = 
        									AggregationBuilders
        									.dateHistogram("avghistogram")
        									.field(EXEC_SUMMARY_INDX_SYSUPDATEDON_FIELD_NAME)
        									.dateHistogramInterval(DateHistogramInterval.DAY)
        									.minDocCount(0)
        									.timeZone(DateTimeZone.forID(timeZone))
        									.extendedBounds(new ExtendedBounds(licYearStartDateLong, licYearEndDateLong - 1));
        
        dateHistogramBuilder.subAggregation(AggregationBuilders.terms("rbcName").field("rbc"));
        
        srb.addAggregation(dateHistogramBuilder);
        
        SearchResponse response = srb.execute().actionGet();
        InternalDateHistogram dateHistrogram =  response.getAggregations().get("avghistogram");
        
        Map<String, Integer> rbcOrderMap = new HashMap<String, Integer>();
        rbcOrderMap.put("/$Event", 0);
        rbcOrderMap.put("/$Ticket", 1);
        rbcOrderMap.put("/$Unclassified", 2);
        rbcOrderMap.put("/$", 3);
        rbcOrderMap.put(ExecutionSummary.RBC_UNBILLABLE, 4);
        for(InternalDateHistogram.Bucket avgHistogram : dateHistrogram.getBuckets())
        {
            Long countList[] = {0L,0L,0L,0L,0L};
            //bucketMap.put(avgHistogram.getKey(), avgHistogram.getDocCount());
            if (avgHistogram.getDocCount() > 0)
            {
                StringTerms rbcNameAgg = avgHistogram.getAggregations().get("rbcName");
                for (Terms.Bucket bucket : rbcNameAgg.getBuckets())
                {
                    if (bucket.getDocCount() > 0)
                    {
                        if (bucket.getKeyAsString().startsWith("/$"))
                        {
                            if (rbcOrderMap.get(bucket.getKey()) == null)
                            {
                                Log.log.error("ERROR: Unknown RBC: " + bucket.getKey());
                            }
                            else
                            {
                                countList[rbcOrderMap.get(bucket.getKey())] = bucket.getDocCount();
                            }
                        }
                    }
                }
            }
            bucketMap.put(((DateTime)avgHistogram.getKey()).getMillis()+"", countList);
        }
        
        WildcardQueryBuilder wildcardQueryBuiler = QueryBuilders.wildcardQuery("runbook", "ABORT:*");
        boolQueryBuilder.must(wildcardQueryBuiler);
        
        response = srb.execute().actionGet();
        
        dateHistrogram =  response.getAggregations().get("avghistogram");
        
        for(InternalDateHistogram.Bucket avgHistogram : dateHistrogram.getBuckets())
        {
            if (avgHistogram.getDocCount() > 0)
            {
                StringTerms rbcNameAgg = avgHistogram.getAggregations().get("rbcName");
                Long[] localCountList = null;
                for (Terms.Bucket bucket : rbcNameAgg.getBuckets())
                {
                    if (bucket.getDocCount() > 0)
                    {
                        if (bucket.getKeyAsString().startsWith("/$"))
                        {
                            int location = rbcOrderMap.get(bucket.getKey());
                            localCountList = bucketMap.get(((DateTime)avgHistogram.getKey()).getMillis()+"");
                            long originalCount = localCountList[location];
                            long revisedCount = originalCount - (bucket.getDocCount() * 2);
                            localCountList[location] = revisedCount;
                        }
                    }
                }
                bucketMap.put(((DateTime)avgHistogram.getKey()).getMillis()+"", localCountList);
            }
        }
        
        result.setRecords(records);
        result.setData(bucketMap);
        result.setSuccess(true);
        
        return result;
    }
    
    /*
     * This method retrieves daily execution summary info based off of execution summary index within 
     * specified duration. 
     * 
     * @param startDate					Starting instant in # of ms since Epoch. (inclusive)
     * @param endDate					Ending instant in # of ms since Epoch. (inclusive)
     * @param countAbortExecutions		Flag indicating retrieval of abort counts
     *  
     * @response						Search query response
     * 
     * Note: Only single execution record per worksheet is created in execution summary from release 6.3 
     * 
     * Query to get daily execution counts aggregated first by Org and then by Resolve Billing Code (RBC) Source
     * Excluding  following
     * 
     * 1. Execution records with RBC "/$UNbillable" set for data received from gateways whcih is not processed
     *    by Resolve.
     * 2. Execution records having runbook namespaces (lower case) starting with "resolve", "devops", "system", and 
     *    "systemadmin".
     * 3. Legacy aborted execution records i.e. runbook name (lower case) starting with "abort:". 
     *  
     * GET executionsummaryalias/_search
	 * {
     *     "query" : {
     *         "bool" : {
     *             "must_not": [
     *                 {
     *                     "term" : {
     *                         "rbc" : {
     *                             "value" : "/$Unbillable"
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "resolve.*"
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "devops.*"
     *                         }
     *                      }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "system.*"
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "systemadmin.*"
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "abort:*"
     *                         }
     *                     }
     *                 }
     *             ],
     *             "must": [
     *                 {
     *                     "range" : {
     *                         "sysUpdatedOn" : {
     *                             "gte" : 1483228800000,
     *                             "lte" : 1514764799999
     *                         }
     *                     }
     *                 }
     *             ] 
     *       
     *         }
     *     },
     *     "size" : 0,   
     *     "aggs" : {
     *         "over_a_day" : {
     *             "date_histogram" : {
     *                 "field" : "sysUpdatedOn",
     *                 "interval" : "day",
     *                 "min_doc_count" : 1
     *             },    
     *             "aggs" : {
     *                 "having_sysOrg" : {
     *                     "filter" : {
     *                         "regexp" : {
     *                             "sysOrg.id" : ".+"
     *                         }
     *                     },
     *                     "aggs" : {
     *                         "unique_sysOrg" : {
     *                             "terms" : {
     *                                 "field" : "sysOrg.id"
     *                             },
     *                             "aggs" : {
     *                                 "unique_sysOrg_rbcSource" : {
     *                                     "terms" : {
     *                                         "field" : "rbcSource"
     *                                     }
     *                                 },
     *                                 "unique_sysOrg_missing_rbcSource" : {
     *                                     "missing" : {
     *                                         "field" : "rbcSource"
     *                                     }
     *                                 }
     *                             }
     *                         }
     *                     }
     *                 },
     *                 "missing_sysOrg" : {
     *                     "missing" : {
     *                         "field" : "sysOrg.id"
     *                     },
     *                     "aggs" : {
     *                         "missing_sysOrg_rbcSource" : {
     *                             "terms" : {
     *                                 "field" : "rbcSource"
     *                             }
     *                         },
     *                         "missing_sysOrg_missing_rbcSource" : {
     *                             "missing" : {
     *                                 "field" : "rbcSource"
     *                             }
     *                         }
     *                     }
     *                 }
     *             }
     *         }
     *     }
     * }
     *
     * NOT USED ANYMORE AS ABOVE QUERY FILTERS OUT LEGACY ABORTED EXECUTION RECORDS. 
     * FROM RELEASE 6.3 ONWARDS THERE WILL BE ONLY ONE RECORD CREATED PER EXECUTION (WORKSHEET).
     * FOR BOTH LEGACY and NEW EXECUTION RECORDS OF ABORTED RUNBOOKS IT IS ASSUMED
     * THAT CAUSE OF RUNBOOK NOT COMPLETING SUCCESSFULLY IS NOT A RESOLVE ISSUE.
     * 
     * Query to get daily abort counts aggregated first by Org and then by Resolve Billing Code (RBC) Source
     *  
     * GET executionsummaryalias/_search
	 * {
     *     "query" : {
     *         "bool" : {
     *             "must_not": [
     *                 {
     *                     "term" : {
     *                         "rbc" : {
     *                             "value" : "/$Unbillable"
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "*resolve.*"
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "*devops.*"
     *                         }
     *                      }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "*system.*"
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard" : {
     *                         "runbook.lower" : {
     *                             "value" : "*systemadmin.*"
     *                         }
     *                     }
     *                 }
     *             ],
     *             "must": [
     *                 {
     *                     "range" : {
     *                         "sysUpdatedOn" : {
     *                             "gte" : 1483228800000,
     *                             "lte" : 1514764799999
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "wildcard": {
     *                         "runbook.lower": {
     *                             "value": "abort:*"
     *                         }
     *                     }
     *                 }
     *             ] 
     *       
     *         }
     *     },
     *     "size" : 0,   
     *     "aggs" : {
     *         "over_a_day" : {
     *             "date_histogram" : {
     *                 "field" : "sysUpdatedOn",
     *                 "interval" : "day",
     *                 "min_doc_count" : 1
     *             },    
     *             "aggs" : {
     *                 "having_sysOrg" : {
     *                     "filter" : {
     *                         "regexp" : {
     *                             "sysOrg.id" : ".+"
     *                         }
     *                     },
     *                     "aggs" : {
     *                         "unique_sysOrg" : {
     *                             "terms" : {
     *                                 "field" : "sysOrg.id"
     *                             },
     *                             "aggs" : {
     *                                 "unique_sysOrg_rbcSource" : {
     *                                     "terms" : {
     *                                         "field" : "rbcSource"
     *                                     }
     *                                 },
     *                                 "unique_sysOrg_missing_rbcSource" : {
     *                                     "missing" : {
     *                                         "field" : "rbcSource"
     *                                     }
     *                                 }
     *                             }
     *                         }
     *                     }
     *                 },
     *                 "missing_sysOrg" : {
     *                     "missing" : {
     *                         "field" : "sysOrg.id"
     *                     },
     *                     "aggs" : {
     *                         "missing_sysOrg_rbcSource" : {
     *                             "terms" : {
     *                                 "field" : "rbcSource"
     *                             }
     *                         },
     *                         "missing_sysOrg_missing_rbcSource" : {
     *                             "missing" : {
     *                                 "field" : "rbcSource"
     *                             }
     *                         }
     *                     }
     *                 }
     *             }
     *         }
     *     }
     * }	
     */
    private static SearchResponse getUsageBucketsByOrgByRBC(Long startDate, Long endDate, boolean countAbortExecutions) {
    	SearchRequestBuilder srb = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
    	srb.setIndices(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS)
    	.setTypes(SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY);
    	
        srb.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);

        BoolQueryBuilder boolQueryBuilder = boolQuery();
        
        
        // Aborted runbooks names starts with ABORT:.....
        
        if (countAbortExecutions) {
        	boolQueryBuilder.must(
        			QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME,
        										ABORT_LC + COLON + WILDCARD_CHARACTER));
        }
        
        RangeQueryBuilder rangeQueryBuilder = rangeQuery(EXEC_SUMMARY_INDX_SYSUPDATEDON_FIELD_NAME)
        									  .from(startDate)
        									  .to(endDate)
        									  .includeLower(true)
        									  .includeUpper(true);

        boolQueryBuilder.must(rangeQueryBuilder);

        /* 
         * Exclude the system runbooks starting with name spaces "resolve", "devops", 
         * "system", "systemadmin", and Resolve Billing Code "/$Unbillable"
         *
         * Note by HP:
         * 
         * Not sure reason for excluding certain namespaces as any runbook executed 
         * even if for Resolve to function properly should be considered as usage 
         * by customer. 
         * 
         * Customers can put their own runbooks in any of these namespaces preventing
         * correct usage accounting.
         */
        
        boolQueryBuilder
        .mustNot(QueryBuilders.termQuery(EXEC_SUMMARY_INDX_RBC_FIELD_NAME, ExecutionSummary.RBC_UNBILLABLE))
        .mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME, 
        									 RESOLVE_NAMESPACE + NAMESPACE_SEPARATOR + WILDCARD_CHARACTER))
        .mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME,
        									 DEVOPS_NAMESPACE + NAMESPACE_SEPARATOR + WILDCARD_CHARACTER))
        .mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME,
        									 SYSTEM_NAMESPACE + NAMESPACE_SEPARATOR + WILDCARD_CHARACTER))
        .mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME, 
        									 SYSTEMADMIN_NAMESPACE + NAMESPACE_SEPARATOR + WILDCARD_CHARACTER));
        
        if (!countAbortExecutions) {
        	boolQueryBuilder
            .mustNot(QueryBuilders.wildcardQuery(EXEC_SUMMARY_INDX_RUNBOOK_LC_STRING_FIELD_NAME,
												 ABORT_LC + COLON + WILDCARD_CHARACTER));
        }
        
        boolQueryBuilder.disableCoord(true);
        
        // Daily aggregates first by Org and then by RBC Source
        
        DateHistogramAggregationBuilder overADayHistogram = 
        	AggregationBuilders
        	.dateHistogram(USAGE_SUMMARY_V2_OVER_A_DAY_HISTOGRAM_NAME)
        	.field(EXEC_SUMMARY_INDX_SYSUPDATEDON_FIELD_NAME)
        	.dateHistogramInterval(DateHistogramInterval.DAY)
        	.minDocCount(1)												/* Exclude records with 0 executions */
        	.extendedBounds(new ExtendedBounds(startDate, endDate))
        	.subAggregation(											/* Aggregated counts for all non-null Orgs */
        		AggregationBuilders.filter(
        			HAVING_SYSORG_AGGR_NAME, 
    				new RegexpQueryBuilder(
    						EXEC_SUMMARY_INDX_SYSORGID_KEYWORD_FIELD_NAME,
    						ANY_TEXT_REGEXP)
    			)
        		.subAggregation(										/* Aggregated counts by unique Org */
        			AggregationBuilders
        			.terms(UNIQUE_SYSORG_AGGR_NAME)
    				.field(EXEC_SUMMARY_INDX_SYSORGID_KEYWORD_FIELD_NAME)
    				.size(SUPPORTED_MAX_UNIQUE_ORG_SIZE)				/* Max # of Sys Orgs supported is 1024 */
    				.subAggregation(									/* Aggregated counts by unique Org and RBC Source */ 
    					AggregationBuilders
    					.terms(UNIQUE_SYSORG_RBCSOURCE_AGGR_NAME)
    					.field(EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME)
    					.size(SUPPORTED_MAX_UNIQUE_RBCSOURCE_SIZE)		/* Max #of RBC Sources supported is 4096 */
    				)
    				.subAggregation(									/* Aggregated counts by unique Org but no RBC Source */
    					AggregationBuilders
    					.missing(UNIQUE_SYSORG_MISSING_RBCSOURCE_AGGR_NAME)
    					.field(EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME)
    					.subAggregation(								/* Aggregated count by unique Org but no RBC Source  */
    					 	AggregationBuilders.filter(					/* and legacy netcool i.e. rbc set to /$Event */
    					 		UNIQUE_SYSORG_MISSING_RBCSOURCE_LEGACY_NETCOOL_AGGR_NAME,
    					 		new RegexpQueryBuilder(
    					 				EXEC_SUMMARY_INDX_RBC_FIELD_NAME,
    					 				LEGACY_EVENT_TEXT_REGEXP)
    					 	)
    					)
    				)
    			)       		
        	)
        	.subAggregation(											/* Aggregated counts for null Org i.e. None Org */
        		AggregationBuilders
        		.missing(MISSING_SYSORG_AGGR_NAME)
        		.field(EXEC_SUMMARY_INDX_SYSORGID_KEYWORD_FIELD_NAME)
        		.subAggregation(										/* Aggregated counts for null Org or None Org with unique RBC Source */
        			AggregationBuilders
    				.terms(MISSING_SYSORG_RBCSOURCE_AGGR_NAME)
    				.field(EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME)
    				.size(SUPPORTED_MAX_UNIQUE_RBCSOURCE_SIZE)			/* Max # of RBC Sources supported is 4096 */
    			)
        		.subAggregation(										/* Aggregated counts for null Org or None Org and no RBC Source */ 
        			AggregationBuilders
        			.missing(MISSING_SYSORG_MISSING_RBCSOURCE_AGGR_NAME)
        			.field(EXEC_SUMMARY_INDX_RBCSOURCE_FIELD_NAME)
        			.subAggregation(									/* Aggregated count for null Org or None Org and no RBC */
					 	AggregationBuilders.filter(						/* Source and legacy netcool i.e. rbc set to /$Event */
					 		MISSING_SYSORG_MISSING_RBCSOURCE_LEGACY_NETCOOL_AGGR_NAME,
					 		new RegexpQueryBuilder(
					 				EXEC_SUMMARY_INDX_RBC_FIELD_NAME,
					 				LEGACY_EVENT_TEXT_REGEXP)
					 	)
					)
        		)
        	);
                        
        srb.addAggregation(overADayHistogram);
        
        srb.setSize(0);
        
        srb.setQuery(boolQueryBuilder);
        
        return srb.execute().actionGet();
    }
    
    /*
     * This method parses the daily execution summary info response from . 
     * 
     * @param searchResponse		Search query response
     * @param eventSources			Set of RBC sources exeutions for which should be considered
     * 								as Events instaed of Incidents
     * @param countSign				Sign of the count +ve or -ve. -ve for Abort count to exclude
     *                              Legacy usage report used to reduce usage count by 2 for every abort count
     *                              assuming the runbook was aborted due to a Resolve issue.
     *                              This assumption has been ignored and only abort runbooks are 
     *                              excluded from the usage count. 
     * 
     * @return						List of daily counts aggregated by RBC, and then by Org 
     *
     * Sample response from get daily counts aggregated first by Org and then by Resolve Billing Code (RBC) Source
     * 
     * {
     *    "took": 12684,
     *    "timed_out": false,
     *    "num_reduce_phases": 7,
     *    "_shards": {
     *    "total": 3120,
     *    "successful": 3120,
     *    "failed": 0
     * },
     * "hits": {
     *    "total": 1174,
     *    "max_score": 0,
     *    "hits": []
     * },
     * "aggregations": {
     *    "over_a_day": {
     *       "buckets": [
     *          {
     *             "key": 1483228800000,
     *             "doc_count": 2,
     *             "having_sysOrg": {
     *                "doc_count": 0,
     *                "unique_sysOrg": {
     *                   "doc_count_error_upper_bound": 0,
     *                   "sum_other_doc_count": 0,
     *                   "buckets": []
     *                }
     *             },
     *             "missing_sysOrg": {
     *                "doc_count": 2,
     *                   "missing_sysOrg_missing_rbcSource": {
     *                      "doc_count": 2,
     *                      "missing_sysOrg_missing_rbcSource_aborted": {
     *                         "doc_count": 0
     *                      }
     *                   },
     *                   "missing_sysOrg_rbcSource": {
     *                      "doc_count_error_upper_bound": 0,
     *                      "sum_other_doc_count": 0,
     *                      "buckets": []
     *                   }
     *             }
     *          },
     *          
     *          ...
     *          
     *          {
     *             "key_as_string": "2018-08-30T00:00:00.000Z",
     *             "key": 1535587200000,
     *             "doc_count": 6,
     *             "having_sysOrg": {
     *                "doc_count": 5,
     *                "unique_sysOrg": {
     *                   "doc_count_error_upper_bound": 0,
     *                   "sum_other_doc_count": 0,
     *                   "buckets": [
     *                      {
     *                         "key": "8a9494c65f957fb4015f959d804e0058",
     *                         "doc_count": 5,
     *                         "unique_sysOrg_missing_rbcSource": {
     *                            "doc_count": 0,
     *                            "unique_sysOrg_missing_rbcSource_aborted": {
     *                               "doc_count": 0
     *                            }
     *                         },
     *                         "unique_sysOrg_rbcSource": {
     *                            "doc_count_error_upper_bound": 0,
     *                            "sum_other_doc_count": 0,
     *                            "buckets": [
     *                               {
     *                                  "key": "remedyx",
     *                                  "doc_count": 2
     *                               },
     *                               {
     *                                  "key": "db",
     *                                  "doc_count": 1
     *                               },
     *                               {
     *                                  "key": "netcool",
     *                                  "doc_count": 1
     *                               },
     *                               {
     *                                  "key": "servicenow",
     *                                  "doc_count": 1
     *                               }
     *                            ]
     *                         }
     *                      }
     *                   ]
     *                }
     *             },
     *             "missing_sysOrg": {
     *                "doc_count": 1,
     *                "missing_sysOrg_missing_rbcSource": {
     *                   "doc_count": 1
     *                },
     *                "missing_sysOrg_rbcSource": {
     *                   "doc_count_error_upper_bound": 0,
     *                   "sum_other_doc_count": 0,
     *                   "buckets": []
     *                }
     *             }
     *          }
     *       ]
     *    }
     * }
     * 		
     * Sample response from get daily abort counts aggregated first by Org and then by Resolve Billing Code (RBC) Source
     * 
     * {
     *    "took": 42203,
     *    "timed_out": false,
     *    "num_reduce_phases": 7,
     *    "_shards": {
     *       "total": 3120,
     *       "successful": 3120,
     *       "failed": 0
     *    },
     *    "hits": {
     *       "total": 42,
     *       "max_score": 0,
     *       "hits": []
     *    },
     *    "aggregations": {
     *       "over_a_day": {
     *          "buckets": [
     *             {
     *                "key": 1483747200000,
     *                "doc_count": 1,
     *                "having_sysOrg": {
     *                   "doc_count": 0,
     *                   "unique_sysOrg": {
     *                      "doc_count_error_upper_bound": 0,
     *                      "sum_other_doc_count": 0,
     *                      "buckets": []
     *                   }
     *                },
     *                "missing_sysOrg": {
     *                   "doc_count": 1,
     *                   "missing_sysOrg_missing_rbcSource": {
     *                      "doc_count": 1
     *                   },
     *                   "missing_sysOrg_rbcSource": {
     *                      "doc_count_error_upper_bound": 0,
     *                      "sum_other_doc_count": 0,
     *                      "buckets": []
     *                   }
     *                }
     *             },
     *             
     *             ...
     *          ]
     *       }
     *    }
     * }								 
     */
    private static List<Map<String, Map<String, Map<String, Long>>>> processUsageBucketsByOrgByRBC(
    																	SearchResponse searchResponse,
    																	Set<String> eventSources,
    																	Long countSign) {
    	List<Map<String, Map<String, Map<String, Long>>>> v2UsageBuckets = 
    			new ArrayList<Map<String, Map<String, Map<String, Long>>>>();
    	
    	v2UsageBuckets = ((Histogram)searchResponse.getAggregations()
    							   .get(USAGE_SUMMARY_V2_OVER_A_DAY_HISTOGRAM_NAME))
    				     .getBuckets()
    				     .parallelStream()
    				     .map(b -> {
    				    	 SimpleDateFormat sdf = new SimpleDateFormat(BUCKET_DATE_TIME_FORMAT);
    				    	 sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    				    	 final String bucketKeyString = "" + b.getKeyAsString();
    					     Map<String, Map<String, Map<String, Long>>> v2UsageBucket = 
    							     new HashMap<String, Map<String, Map<String, Long>>>();
    					     
    					     Date bucketStart = null;
    					     
    					     try {
    					    	 if (bucketKeyString.endsWith("Z")) {
    					    		 bucketStart = sdf.parse(bucketKeyString);
    					    	 } else {
    					    		 bucketStart = new Date(Long.parseLong(bucketKeyString));
    					    	 }
    					    	 
    					    	 if (Log.log.isTraceEnabled()) {
    					    		 Log.log.trace(String.format("Bucket Start Date \"%s\" to Date Object [%s] to " +
    					        		 					 	 "DateFormat.format(Date) [%s].", bucketKeyString, 
    					        		 					 	 bucketStart.toString(), sdf.format(bucketStart)));
    					    	 }
							 } catch (ParseException | NumberFormatException e) {
								Log.log.debug(String.format("Error [%s] in parsing bucket key start date \"%s\" to Date.", 
														    (StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""),
														    bucketKeyString), e);
							 }
    					     
    					     String keyString = bucketStart != null ? sdf.format(bucketStart) : bucketKeyString;
    					     
    					     if (StringUtils.isBlank(keyString)) {
    					    	 Log.log.warn(String.format("Bucket Key String parsed from bucket key as string " +
    					    			 				    "[%s] is null or blank, returning null Bucket object!!!",
    					    			 				    bucketKeyString));
    					    	 return null;
    					     }
    					     
    					     if (!v2UsageBucket.containsKey(keyString)) {
    					    	 v2UsageBucket.put(keyString, new HashMap<String, Map<String, Long>>());
    					     }
    					     
    					     // Process total counts by all Orgs + None Org and by All RBC Sources + No RBC Source
    					     
    					     Map<String, Long> byRBCs = null;
    					     
    					     Map<String, Map<String, Long>> byOrgsByRBCs = v2UsageBucket.get(keyString);
    					     
    					     if (eventSources != null && b.getDocCount() > 0l) {
	    					     if (!byOrgsByRBCs.containsKey(ALL)) {
	    					    	 byOrgsByRBCs.put(ALL, new HashMap<String, Long>());
	    					     }
	    					     
	    					     byRBCs = byOrgsByRBCs.get(ALL);
	    					     
	    					     if (byRBCs == null) {
	    					    	 Log.log.warn(String.format("Bucket for bucket key [%s] as string " +
			    			 				    				"is missing RBC Source Counts By Orgs for \"ALL\" Org!!!",
			    			 				      bucketKeyString));
	    					    	 return null;
	    					     }
	    					     
	    					     byRBCs.put(ALL, (b.getDocCount() * countSign.longValue()));
    					     }
    					     
    					     // Process Org counts
    					     
    					     Filter havingOrgAggr = b.getAggregations().get(HAVING_SYSORG_AGGR_NAME);
    					     
    					     Terms uniqueOrgAggr = havingOrgAggr.getAggregations().get(UNIQUE_SYSORG_AGGR_NAME);
    					     
    					     List<Terms.Bucket> uniqueOrgBuckets = uniqueOrgAggr.getBuckets();
    					     
    					     List<Map<String, Map<String, Long>>> allRBCsCountByOrgs = 
    					    		 uniqueOrgBuckets
    					    		 .parallelStream()
    					    		 .map(uob -> {
    					    			 Map<String, Map<String, Long>> rbcsCountByOrg = 
    					    					 new HashMap<String, Map<String, Long>>();
    					    			 
    					    			 rbcsCountByOrg.put(uob.getKeyAsString(), new HashMap<String, Long>());
    					    			 
    					    			 Map<String, Long> countsByRBC = rbcsCountByOrg.get(uob.getKeyAsString());
    					    			 
    					    			 if (eventSources != null && uob.getDocCount() > 0l) {
    					    				 countsByRBC.put(ALL, (uob.getDocCount() * countSign.longValue()));
    					    			 }
    					    			 
    					    			 // Process unique Org Missing RBC Source counts
    					    			 
    					    			 Missing uniqueOrgMissingRBCAggr = 
    					    					 uob.getAggregations().get(UNIQUE_SYSORG_MISSING_RBCSOURCE_AGGR_NAME);
    					    			 
    					    			 if (uniqueOrgMissingRBCAggr.getDocCount() > 0l) {
    					    				 long effectiveUniqueOrgMissingRBCSrcDocCnt = uniqueOrgMissingRBCAggr.getDocCount();
    					    				 
    					    				 // First check unique Org missing RBC Source and legacy netcool
    					    				 
    					    				 Filter uniqueOrgMissingRBCSrcLegacyNetcoolAggr = 
    					    						 	uniqueOrgMissingRBCAggr.getAggregations()
    					    						 	.get(UNIQUE_SYSORG_MISSING_RBCSOURCE_LEGACY_NETCOOL_AGGR_NAME);
    					    				 
    					    				 if (uniqueOrgMissingRBCSrcLegacyNetcoolAggr.getDocCount() > 0l) {
    					    					 countsByRBC.put(LEGACY_NETCOOL, 
    					    							 		 uniqueOrgMissingRBCSrcLegacyNetcoolAggr.getDocCount() *
    					    							 		 countSign.longValue());
    					    					 
    					    					 effectiveUniqueOrgMissingRBCSrcDocCnt -= 
    					    							 uniqueOrgMissingRBCSrcLegacyNetcoolAggr.getDocCount();
    					    				 }
    					    				 
    					    				 if (effectiveUniqueOrgMissingRBCSrcDocCnt > 0l) {
    					    					 countsByRBC.put(OrgsVO.NONE_ORG_NAME, 
	    					    					 		 	 (effectiveUniqueOrgMissingRBCSrcDocCnt * 
	    					    					 		 	  countSign.longValue()));
    					    				 }	 
    					    			 }
    					    			 
    					    			 // Process unique Org and unique RBC counts
    					    			 
    					    			 Terms uniqueOrgRBCAggr = 
    					    					 uob.getAggregations().get(UNIQUE_SYSORG_RBCSOURCE_AGGR_NAME);
    					    			 
    					    			 List<Terms.Bucket> uniqueOrgRBCBuckets = uniqueOrgRBCAggr.getBuckets();
    					    			 
    					    			 List<Map<String, Long>> countByRBCs = 
    					    					 uniqueOrgRBCBuckets
    					    					 .parallelStream()
    					    					 .map(uorb -> {
    					    						 Map<String, Long> countByRBCMap = new HashMap<String, Long>();
    					    						 
    					    						 countByRBCMap.put(uorb.getKeyAsString(), 
    					    								 		   (uorb.getDocCount() * countSign.longValue()));
    					    						     					    						 
    					    						 return countByRBCMap;
    					    					 })
    					    					 .collect(Collectors.toList());
    					    			 
    					    			 for (Map<String, Long> countByRBCElm : countByRBCs) {
    					    				 countsByRBC.putAll(countByRBCElm);
    					    			 }
    					    			 
    					    			 if (rbcsCountByOrg != null && rbcsCountByOrg.isEmpty()) {
    					    				 rbcsCountByOrg = null;
    					    			 }
    					    			 
    					    			 return rbcsCountByOrg;
    					    		 })
    					    		 .filter(Objects::nonNull)
    					    		 .collect(Collectors.toList());
    					     
    					     for (Map<String, Map<String, Long>> allRBCsCountByOrg : allRBCsCountByOrgs) {
    					    	 if (allRBCsCountByOrg != null && !allRBCsCountByOrg.isEmpty()) {
    					    		 byOrgsByRBCs.putAll(allRBCsCountByOrg);
    					    	 }
    					     }
    					     
    					     // Process No Org counts
    					     
    					     Missing noneOrgAggr = b.getAggregations().get(MISSING_SYSORG_AGGR_NAME);
    					     
    					     if (noneOrgAggr.getDocCount() > 0l) {
	    					     if (!byOrgsByRBCs.containsKey(OrgsVO.NONE_ORG_NAME)) {
	    					    	 byOrgsByRBCs.put(OrgsVO.NONE_ORG_NAME, new HashMap<String, Long>());
	    					     }
	    					     
	    					     byRBCs = byOrgsByRBCs.get(OrgsVO.NONE_ORG_NAME);
	    					     
	    					     if (eventSources != null) {
	    					    	 byRBCs.put(ALL, (noneOrgAggr.getDocCount() * countSign.longValue()));
	    					     }
    					         					     
	    					     // Process No Org No RBC Source counts
	    					     
	    					     Missing noneOrgRBCAggr = 
	    					    		 noneOrgAggr.getAggregations().get(MISSING_SYSORG_MISSING_RBCSOURCE_AGGR_NAME);
	    					     
	    					     if (noneOrgRBCAggr.getDocCount() > 0l) {
	    					    	 long effectiveMissingOrgMissingRBCSrcDocCnt = noneOrgRBCAggr.getDocCount();
	    					    	 
	    					    	 // First check missing Org missing RBC Source and legacy netcool
	    					    	 
	    					    	 Filter missingOrgMissingRBCSrcLegacyNetcoolAggr = 
	    					    			 	noneOrgRBCAggr.getAggregations()
				    						 	.get(MISSING_SYSORG_MISSING_RBCSOURCE_LEGACY_NETCOOL_AGGR_NAME);
	    					    	 
	    					    	 if (missingOrgMissingRBCSrcLegacyNetcoolAggr.getDocCount() > 0l) {
	    					    		 byRBCs.put(LEGACY_NETCOOL, 
				    							 	missingOrgMissingRBCSrcLegacyNetcoolAggr.getDocCount() *
				    							 	countSign.longValue());
				    					 
	    					    		 effectiveMissingOrgMissingRBCSrcDocCnt -= 
	    					    				 missingOrgMissingRBCSrcLegacyNetcoolAggr.getDocCount();
				    				 }
	    					    	 
	    					    	 if (effectiveMissingOrgMissingRBCSrcDocCnt > 0l) {
	    					    		 byRBCs.put(OrgsVO.NONE_ORG_NAME, 
	    					    				 	(effectiveMissingOrgMissingRBCSrcDocCnt * countSign.longValue()));
	    					    	 }
	    					     }
	    					     
	    					     // Process No Org unique RBC counts
	    					     
	    					     Terms noOrgUniqueRBCAggr = noneOrgAggr.getAggregations().get(MISSING_SYSORG_RBCSOURCE_AGGR_NAME);
				    			 
				    			 List<Terms.Bucket> noneOrgUniqueRBCBuckets = noOrgUniqueRBCAggr.getBuckets();
				    			 
				    			 List<Map<String, Long>> noneOrgCountByRBCs =
				    					 noneOrgUniqueRBCBuckets
				    					 .parallelStream()
				    					 .map(nourb -> {
				    						 Map<String, Long> noneOrgCountByRBCMap = new HashMap<String, Long>();
				    						 
				    						 noneOrgCountByRBCMap.put(nourb.getKeyAsString(), 
				    								 				  (nourb.getDocCount() * countSign.longValue()));
				    						 
				    						 return noneOrgCountByRBCMap;
				    					 })
				    					 .collect(Collectors.toList());
				    			 
				    			 for (Map<String, Long> noneOrgCountByRBCsElm : noneOrgCountByRBCs) {
				    				 if (noneOrgCountByRBCsElm != null && !noneOrgCountByRBCsElm.isEmpty()) {
				    					 byRBCs.putAll(noneOrgCountByRBCsElm);
				    				 }
				    			 }
    					     }
    					     
			    			 if (eventSources != null) {
				    			 // Inject Incident/Event count based on existing RBCs including No RBC (None)
				    			 
				    			 Map<String, Map<String, Long>> rbcCountsByOrg = v2UsageBucket.get(keyString);
				    			 
				    			 if (rbcCountsByOrg != null && !rbcCountsByOrg.isEmpty()) {
					    			 Map<String, Long> generatedCountsAllOrgs = new HashMap<String, Long>();
			    					 
					    			 generatedCountsAllOrgs.put(INCIDENT, 0l);
					    			 generatedCountsAllOrgs.put(EVENT, 0l);
			    					 
					    			 for (String org : rbcCountsByOrg.keySet()) {
					    				 if (!ALL.equalsIgnoreCase(org)) {
					    					 Map<String, Long> countsByRBC = rbcCountsByOrg.get(org);
					    					 
					    					 Map<String, Long> generatedCounts = new HashMap<String, Long>();
					    					 
					    					 generatedCounts.put(INCIDENT, 0l);
					    					 generatedCounts.put(EVENT, 0l);
					    					 
					    					 for (String rbcSource : countsByRBC.keySet()) {
					    						 if (!ALL.equalsIgnoreCase(rbcSource)) {
					    							 if (eventSources.contains(rbcSource) || 
					    								 rbcSource.equalsIgnoreCase(LEGACY_NETCOOL)) {
					    								 generatedCounts.put(EVENT, 
					    										 			 generatedCounts.get(EVENT).longValue() + 
					    										 			 countsByRBC.get(rbcSource).longValue());
					    							 } else {
					    								 generatedCounts.put(INCIDENT, 
		    										 			 			 generatedCounts.get(INCIDENT).longValue() + 
		    										 			 			 countsByRBC.get(rbcSource).longValue());
					    							 }
					    						 }
					    					 }
					    					 
					    					 if (generatedCounts.get(INCIDENT).longValue() <= 0) {
					    						 generatedCounts.remove(INCIDENT);
					    					 }
					    					 
					    					 if (generatedCounts.get(EVENT).longValue() <= 0) {
					    						 generatedCounts.remove(EVENT);
					    					 }
					    					 
					    					 countsByRBC.putAll(generatedCounts);
					    					 
					    					 // Add to generatedCountsAllOrgs
					    					 
					    					 if (generatedCounts.containsKey(INCIDENT)) {
					    						 generatedCountsAllOrgs.put(INCIDENT, 
					    								 					generatedCountsAllOrgs.get(INCIDENT).longValue() +
					    								 					generatedCounts.get(INCIDENT).longValue());
					    					 }
					    					 
					    					 if (generatedCounts.containsKey(EVENT)) {
					    						 generatedCountsAllOrgs.put(EVENT, 
					    								 					generatedCountsAllOrgs.get(EVENT).longValue() +
					    								 					generatedCounts.get(EVENT).longValue());
					    					 }
					    				 }
					    			 }
					    			 
					    			 if (generatedCountsAllOrgs.get(INCIDENT).longValue() <= 0) {
					    				 generatedCountsAllOrgs.remove(INCIDENT);
			    					 }
			    					 
			    					 if (generatedCountsAllOrgs.get(EVENT).longValue() <= 0) {
			    						 generatedCountsAllOrgs.remove(EVENT);
			    					 }
			    					 
			    					 Map<String, Long> countsByRBCAllOrgs = rbcCountsByOrg.get(ALL);
			    					 countsByRBCAllOrgs.putAll(generatedCountsAllOrgs);
				    			 }
			    			 }
			    			 
    					     return v2UsageBucket;
    				     })
    				     .filter(Objects::nonNull)
    				     .collect(Collectors.toList());
    	
    	
    	return v2UsageBuckets;
    }
    
    /*
     * This methods adds (subtract) abort counts from aggregated runbook usage counts by Org and by RBC Source
     * 
     *  @param usageBuckets			Usage buckets for all runbooks including Abort runbooks
     *  @param abortUsageBuckets	Usage buckets for abort runbooks
     */
    @SuppressWarnings("unused")
	private static List<Map<String, Map<String, Map<String, Long>>>> 
    					addUsageBuckets(List<Map<String, Map<String, Map<String, Long>>>> usageBuckets,
    								    List<Map<String, Map<String, Map<String, Long>>>> abortUsageBuckets,
    								    Long factor) {
    	List<Map<String, Map<String, Map<String, Long>>>> netUsageBuckets = 
    														new ArrayList<Map<String, Map<String, Map<String, Long>>>>();
    	
    	Map<String, Map<String, Map<String, Long>>> abortUsageBuckectKeyToOrgRBCBuckets = 
    														new HashMap<String, Map<String, Map<String, Long>>>();
    	
    	for (Map<String, Map<String, Map<String, Long>>> abortUsageBucket : abortUsageBuckets) {
    		abortUsageBuckectKeyToOrgRBCBuckets.putAll(abortUsageBucket);
    	}
    	
    	netUsageBuckets = usageBuckets
    					  .parallelStream()
    					  .map(ub -> {
    						  Map<String, Map<String, Map<String, Long>>> netUsageBucket = new HashMap<>();
    						  
    						  String usageBucketKey = ub.keySet().iterator().next();
    						  
    						  if (abortUsageBuckectKeyToOrgRBCBuckets.containsKey(usageBucketKey)) {
    							  Map<String, Map<String, Long>> usageOrgRBCs = ub.get(usageBucketKey);
    							  Map<String, Map<String, Long>> abortUsageOrgRBCs = 
    									  							abortUsageBuckectKeyToOrgRBCBuckets.get(usageBucketKey);
    							  
    							  List<Map<String, Map<String, Long>>> netUsageOrgRBCs =
    							  (usageOrgRBCs.keySet().parallelStream().collect(Collectors.toList()))
    							  .parallelStream()
    							  .map(usageOrgs -> {
    								  Map<String, Map<String, Long>> netUsageOrgRBC = new HashMap<>();
    								  
    								  if (abortUsageOrgRBCs.containsKey(usageOrgs)) {
    									  Map<String, Long> usageRBCs = usageOrgRBCs.get(usageOrgs);
    									  Map<String, Long> abortUsageRBCs = abortUsageOrgRBCs.get(usageOrgs);
    									  
    									  List<Map<String, Long>> netUsageRBCs = 
    									  (usageRBCs.keySet().parallelStream().collect(Collectors.toList()))
    									  .parallelStream()
    									  .map(usgeRBC -> {
    										  Map<String, Long> netUsageRBC = new HashMap<>();
    										  
    										  if (abortUsageRBCs.containsKey(usgeRBC)) {
    											  long netCount = usageRBCs.get(usgeRBC).longValue();
    											  
    											  if ((netCount + 
    												   (abortUsageRBCs.get(usgeRBC).longValue() * factor)) >= 0 ) {
    												  netUsageRBC.put(usgeRBC, 
    														  		  (netCount + 
    														  		   (abortUsageRBCs.get(usgeRBC).longValue() * 
    														  			factor)));
    											  } else {
    												  netUsageRBC.put(usgeRBC, 0l);
    											  }
    										  } else {
    											  netUsageRBC.put(usgeRBC, usageRBCs.get(usgeRBC));
    										  }
    										  
    										  return netUsageRBC;
    									  })
    									  .collect(Collectors.toList());
    									  
    									  Map<String, Long> netUsageRBCElms = new HashMap<>();
    									  
    									  for (Map<String, Long> netUsageRBCElm : netUsageRBCs) {
    										  netUsageRBCElms.putAll(netUsageRBCElm);
    									  }
    									  
    									  netUsageOrgRBC.put(usageOrgs, netUsageRBCElms);
    								  } else {
    									  netUsageOrgRBC.put(usageOrgs, usageOrgRBCs.get(usageOrgs));
    								  }
    								  
    								  return netUsageOrgRBC;
    							  })
    							  .collect(Collectors.toList());
    							  
    							  Map<String, Map<String, Long>> netUsageOrgRBCElms = new HashMap<>();
    							  
    							  for (Map<String, Map<String, Long>> netUsageOrgRBCElm : netUsageOrgRBCs) {
    								  netUsageOrgRBCElms.putAll(netUsageOrgRBCElm);
    							  }
    							  
    							  netUsageBucket.put(usageBucketKey, netUsageOrgRBCElms);
    						  } else {
    							  netUsageBucket.put(usageBucketKey, ub.get(usageBucketKey));
    						  }
    						  
    						  return netUsageBucket;
    					  })
    					  .collect(Collectors.toList());
    			
    	return netUsageBuckets;
    }
    
    public static ResponseDTO<Map<String, Map<String, Map<String, Long>>>> getVolumeBasedUsageReportV2(
    																						Long startDate, 
    																						Long endDate,
    																						Set<String> eventSources,
    																						boolean fromUI) {    	
    	ResponseDTO<Map<String, Map<String, Map<String, Long>>>> result = 
    			new ResponseDTO<Map<String, Map<String, Map<String, Long>>>>();
    	
    	Long currentMillis = Instant.now().toEpochMilli();
    	Long validEndDate = currentMillis < endDate ? currentMillis : endDate;
    	Long endDateEOD = validEndDate - (validEndDate % Duration.ofDays(1).toMillis()) + (Duration.ofDays(1).toMillis() - 1);
    	Long esStartDate = startDate > endDateEOD ? validEndDate - (validEndDate % Duration.ofDays(1).toMillis()) : 
    												(startDate - (startDate % Duration.ofDays(1).toMillis()));
    	
    	/*
    	 * If the request is from UI then get as many days of aggregated execution summary from DB first 
    	 * before searching and aggregating from ES for remaining period.
    	 * 
    	 * Please note requests from BE is always for period for which data is not available in DB 
 		 * up to current day.
 		 * 
 		 * Please note exception to above BE rule is requests for EOD Refresh which is for previous day only.
    	 */
    	
    	List<Map<String, Map<String, Map<String, Long>>>> dbUsageBuckets = null;
    	
    	if (fromUI) {
	    	AggrExecSummary latestAggrExecSummary = ExecSummaryAggregation.getLatestAggrExecSummary();
	    	
	    	if (latestAggrExecSummary != null) {
	    		Date dbStartDateFrom = new Date(startDate - (startDate % Duration.ofDays(1).toMillis()));
	    		Date latestAggrExecSummaryUpdatedOn = latestAggrExecSummary.getSysUpdatedOn();
	    		
	    		Date dbStartDateTo = endDateEOD > latestAggrExecSummaryUpdatedOn.getTime() ? 
	    							 (latestAggrExecSummaryUpdatedOn.before(dbStartDateFrom) ? 
	    							  dbStartDateFrom : 
	    							  new Date(latestAggrExecSummaryUpdatedOn.getTime() - 
	    									   (latestAggrExecSummaryUpdatedOn.getTime() % Duration.ofDays(1).toMillis()))) :
	    							 new Date(endDateEOD - (endDateEOD % Duration.ofDays(1).toMillis()));
	    		
	    		try {
					dbUsageBuckets = ExecSummaryAggregation.getDBUsageBuckets(dbStartDateFrom, dbStartDateTo, eventSources);
				} catch (Exception e) {
					log.warn(String.format("Error [%s] in getting DB Aggregated Usage for From Start Date %s to To Start Date %s, " +
										   "reverting to ES...", (StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""),
										   dbStartDateFrom, dbStartDateTo), e);
				}
	    		
	    		if (dbUsageBuckets != null && !dbUsageBuckets.isEmpty()) {
	    			log.info(String.format("Found %d DB Usage Buckets between From Start Date %s to To Start Date %s", 
										   dbUsageBuckets.size(), dbStartDateFrom, dbStartDateTo));
	    			
	    			if (log.isTraceEnabled()) {
	    				dbUsageBuckets
	    				.stream()
	    				.forEach(dbb -> {
	    					log.trace(String.format("DB Usage Bucket: %s", StringUtils.mapToString(dbb, "=", ", ", true)));
	    				});
	    			}
	    			
	    			esStartDate = dbStartDateTo.getTime() - (dbStartDateTo.getTime() % Duration.ofDays(1).toMillis()) + 
	    						  Duration.ofDays(1).toMillis();
	    		}
	    	}
    	}
    	
    	List<Map<String, Map<String, Map<String, Long>>>> esUsageBuckets = null;
    	
    	if (esStartDate < endDateEOD) {
    		log.info(String.format("Getting ES Usage Buckets from %s to %s", new Date(esStartDate), new Date(endDateEOD)));
    		SearchResponse usageBucketsResponse = getUsageBucketsByOrgByRBC(esStartDate, endDateEOD, false);
    	
    		esUsageBuckets = processUsageBucketsByOrgByRBC(usageBucketsResponse, eventSources, 1l);
    	
	    	/*
	    	 * TBD
	    	 * 
	    	 * From 6.3 only single execution record is created per worksheet. 
	    	 * Pre 6.3 multiple executions in worksheet was creating multiple records in execution summary including abort 
	    	 * runbook execution.
	    	 * 
	    	 * Failure of runbook determined by existence of record in execution summary with runbook name (lower case) 
	    	 * prefixed with "abort:" does not necessarily means Resolve’s issue.
	    	 * 
	    	 * Pre 6.3 every unsuccessful runbook execution was considered as Resolve's issue asn was not counted towards 
	    	 * entitled usage.
	    	 * 
	    	 * From 6.3 and beyond this assumption is not valid.
	    	 * 
	    	 * Since getUsageBucketsByOrgByRBC() already filters out execution records with runbook lower case name 
	    	 * prefixed with "abort:" there is no need to requery to find aborted runbook execution records to determine 
	    	 * net usage.
	    	 *
	    	SearchResponse abortUsageBucketsResponse = getUsageBucketsByOrgByRBC(startDate, endDate, true);
	    	
	    	List<Map<String, Map<String, Map<String, Long>>>> abortUsageBuckets = processUsageBucketsByOrgByRBC(
	    																				abortUsageBucketsResponse,
					  																	eventSources, -1l);
	    	
	    	List<Map<String, Map<String, Map<String, Long>>>> netUsageBuckets = addUsageBuckets(usageBuckets, 
	    																						abortUsageBuckets,
	    																						1l);
	      	result.setRecords(netUsageBuckets);
	    	 */
    	}
    	
    	List<Map<String, Map<String, Map<String, Long>>>> usageBuckets = 
    															new ArrayList<Map<String, Map<String, Map<String, Long>>>>();
    	
    	if (dbUsageBuckets != null && !dbUsageBuckets.isEmpty()) {
    		log.info(String.format("Returning %d Usage Buckets from DB", dbUsageBuckets.size()));
    		usageBuckets.addAll(dbUsageBuckets);
    	}
    	
    	if (esUsageBuckets != null && !esUsageBuckets.isEmpty()) {
    		log.info(String.format("Returning %d Usage Buckets from ES", esUsageBuckets.size()));
    		usageBuckets.addAll(esUsageBuckets);
    	}
    	
    	// Sort all buckets by date for UI only
    	
    	if (usageBuckets != null && !usageBuckets.isEmpty() && fromUI) {
    		Collections.sort(usageBuckets, (arg0, arg1) -> {
    			SimpleDateFormat sdf = new SimpleDateFormat(SaaSReport.BUCKET_DATE_TIME_FORMAT);
    	    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	    	Date arg0StartDate = null;
    	    	Date arg1StartDate = null;
    	    	String arg0StartDateStr = null;
    	    	String arg1StartDateStr = null;
    	    	
    	    	if (arg0 != null && arg0.keySet() != null && !arg0.keySet().isEmpty() && arg0.keySet().iterator().hasNext()) {
    	    		arg0StartDateStr = arg0.keySet().iterator().next();
    	    	}
    	    	
    	    	if (arg1 != null && arg1.keySet() != null && !arg1.keySet().isEmpty() && arg1.keySet().iterator().hasNext()) {
    	    		arg1StartDateStr = arg1.keySet().iterator().next();
    	    	}
    	    	
    	    	if (StringUtils.isNotBlank(arg0StartDateStr) && arg0StartDateStr.endsWith("Z")) {
    		    	try {
    					arg0StartDate = sdf.parse(arg0StartDateStr);
    				} catch (ParseException e) {
    					log.warn(String.format("Failed to parse [%s] to Date in bucket %s", arg0StartDateStr, 
    										   StringUtils.mapToString(arg0, "=", ", ", true)), e);
    				}
    	    	} else {
    	    		log.warn(String.format("Invalid arg0 Start Date [%s] in bucket %s", 
    	    							   (StringUtils.isNotBlank(arg0StartDateStr) ? arg0StartDateStr : ""),
    	    							   StringUtils.mapToString(arg0, "=", ", ", true)));
    	    	}
    	    	
    	    	if (StringUtils.isNotBlank(arg1StartDateStr) && arg1StartDateStr.endsWith("Z")) {
    		    	try {
    					arg1StartDate = sdf.parse(arg1StartDateStr);
    				} catch (ParseException e) {
    					log.warn(String.format("Failed to parse [%s] to Date in bucket %s", arg1StartDateStr,
    										   StringUtils.mapToString(arg1, "=", ", ", true)), e);
    				}
    	    	} else {
    	    		log.warn(String.format("Invalid arg1 Start Date [%s] in bucket %s", 
    						   			   (StringUtils.isNotBlank(arg1StartDateStr) ? arg1StartDateStr : ""),
    						   			   StringUtils.mapToString(arg1, "=", ", ", true)));
    	    	}
    	    	
    	    	if (arg0StartDate != null && arg1StartDate != null) {
    	    		return arg0StartDate.compareTo(arg1StartDate);
    	    	}
    	    	
    			return 0;
    		});
    	}
    	
    	log.info(String.format("Returning Total %d Usage Buckets", usageBuckets.size()));
    	
    	result.setRecords(usageBuckets);
    	
    	return result;
    }
} // SaaSReport
