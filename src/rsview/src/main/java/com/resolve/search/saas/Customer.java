/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/
package com.resolve.search.saas;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import java.text.SimpleDateFormat;



import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.util.Log;

public class Customer {
    
    private boolean isSaaSCustomer;
    private String customerName;
    private Date startDate; // MM/dd/yyyy
    private int quota;
    private int eventQuota;
    private int ticketQuota;
    private int duration; // number of months
    private int previousLicYear = 0; // Previous nth License Year For testing without UI change
    
    public Customer() throws Exception {
        loadCustomerProfile();
    }
    
    public boolean isSaaSCustomer()
    {
        return isSaaSCustomer;
    }
    public void setSaaSCustomer(boolean isSaaSCustomer)
    {
        this.isSaaSCustomer = isSaaSCustomer;
    }
    public String getCustomerName()
    {
        return customerName;
    }
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }
    public Date getStartDate()
    {
        return startDate;
    }
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }
    public int getQuota()
    {
        return quota;
    }
    public void setQuota(int quota)
    {
        this.quota = quota;
    }
    public int getEventQuota()
    {
        return eventQuota;
    }

    public void setEventQuota(int eventQuota)
    {
        this.eventQuota = eventQuota;
    }
    public int getTicketQuota()
    {
        return ticketQuota;
    }
    public void setTicketQuota(int ticketQuota)
    {
        this.ticketQuota = ticketQuota;
    }
    public int getDuration()
    {
        return duration;
    }
    public void setDuration(int duration)
    {
        this.duration = duration;
    }
    
    public int getPreviousLicYear()
    {
        return previousLicYear;
    }
    
    public void setPreviousLicYear(int previousLicYear)
    {
        this.previousLicYear = previousLicYear;
    }
    
    private void loadCustomerProfile() throws Exception {
        
        PropertiesVO customerNameVO = PropertiesUtil.findPropertyByName("saas.customer.name");
        String customerName = customerNameVO.getUValue();
        
        if(StringUtils.isBlank(customerName))
            this.isSaaSCustomer = false;
        else {
            this.isSaaSCustomer = true;
            this.customerName = customerName;
        }
        
        PropertiesVO eventQuotaVO = PropertiesUtil.findPropertyByName("saas.event.quota");
        String quotaStr = eventQuotaVO.getUValue();
        
        try {
            this.eventQuota = Integer.parseInt(quotaStr);
        } catch(Exception e) {
            Log.log.error("Saas customer event quota is not a number.", e);
            throw new Exception("Saas customer event quota is not a number.");
        }
        
        PropertiesVO ticketQuotaVO = PropertiesUtil.findPropertyByName("saas.ticket.quota");
        quotaStr = ticketQuotaVO.getUValue();
        
        try {
            this.ticketQuota = Integer.parseInt(quotaStr);
        } catch(Exception e) {
            Log.log.error("Saas customer ticket quota is not a number.", e);
            throw new Exception("Saas customer ticket quota is not a number.");
        }
        
        PropertiesVO durationVO = PropertiesUtil.findPropertyByName("saas.duration");
        String durationStr = durationVO.getUValue();
        
        try {
            int durationInt = Integer.parseInt(durationStr);
            
            if (durationInt < 365)
            {
                durationInt = 365;
            }
            
            if ((durationInt % 365) != 0)
            {
                durationInt = ((durationInt / 365) + 1) * 365;
            }
            
            this.duration = durationInt;
        } catch(Exception e) {
            Log.log.error("Saas customer license duration is not a number.", e);
            throw new Exception("Saas customer license duration is not a number.");
        }
        
        PropertiesVO startDateVO = PropertiesUtil.findPropertyByName("saas.start.date");
        String startDate = startDateVO.getUValue();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date date = sdf.parse(startDate);
            this.startDate = date;
        } catch(Exception e) {
            Log.log.error("Saas customer license starting date format has different format from: yyyy-mm-dd hh:mm:ss.", e);
            throw new Exception("Saas customer license starting date format has different format from: yyyy-mm-dd hh:mm:ss.");
        }
        
        PropertiesVO previousLicYearVO = PropertiesUtil.findPropertyByName("saas.subtractlicenseyearby");
        
        if (previousLicYearVO != null)
        {
            String previousLicYearStr = previousLicYearVO.getUValue();
            
            if (StringUtils.isNotBlank(previousLicYearStr))
            {
                try
                {
                    this.previousLicYear = Integer.parseInt(previousLicYearStr);
                    
                    if (previousLicYear < 0)
                    {
                        previousLicYear = -previousLicYear;
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn("Saas subtract license year by shhould be positive integer value. " +
                                 "Error " + e.getMessage() + " in converting " + previousLicYearStr + " to positive integer.", e);
                }
            }
        }
    }
    
} // Customer