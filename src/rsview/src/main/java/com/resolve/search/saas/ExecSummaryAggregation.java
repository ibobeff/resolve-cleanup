/******************************************************************************
 * (C) Copyright 2018
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/
package com.resolve.search.saas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.sort.SortOrder;
import org.hibernate.ReplicationMode;
import org.hibernate.query.Query;

import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.model.AggrExecSummary;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MInfo;
import com.resolve.search.APIFactory;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.model.ExecutionSummary;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.StringUtils;

import static com.resolve.services.hibernate.util.UserUtils.SYSTEM;
import static com.resolve.services.interfaces.VO.*;
import static com.resolve.services.vo.QueryFilter.*;
import static com.resolve.util.Log.log;

public final class ExecSummaryAggregation {	

	/*
	 * Get update time of aggregated by day for latest (i.e. with latest start date) 
	 * execution summary record in DB.
	 */
	
	private static final String SYS_UPDATED_ON_FIELD_NAME = "sysUpdatedOn";
	private static boolean deleteIndicesInProgress;
	private static final String EXECUTION_SUMMARY_ALIAS_DATE_SUFFIX_FORMAT = "yyyyMMdd";
	private static boolean aggrExecSummaryInProgress;
	
	@SuppressWarnings("unchecked")
	public static AggrExecSummary getLatestAggrExecSummary () {
		AggrExecSummary latestAggrExecSummary = null;
		
		List<OrderbyProperty> orderByProperties = new ArrayList<OrderbyProperty>();
		orderByProperties.add(new OrderbyProperty("startDate", false));
		orderByProperties.add(new OrderbyProperty("sysUpdatedOn", false));
		orderByProperties.add(new OrderbyProperty("sysCreatedOn", false));
		
		List<AggrExecSummary> list = null;
		
		try
        {

			HibernateProxy.setCurrentUser(SYSTEM);
            log.trace("Querying for latest Aggregated Execution Summaries...");
            list = (List<AggrExecSummary>) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
            	return HibernateUtil.getDAOFactory().getAggrExecSummaryDAO().find(1, orderByProperties);
            });
            log.trace("Returned " + (list != null ? list.size() : 0) + " Aggregated Execution Summaries...");
        }
        catch (Throwable t)
        {
            log.error(String.format("Error %sin getting latest Aggregated Execution Summaries",
            						(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new RuntimeException(t);
        }
		
		if (list != null && !list.isEmpty() && list.iterator().hasNext()) {
			latestAggrExecSummary = list.iterator().next();
		}
		
		return latestAggrExecSummary;
	}
	
	private static Date getFirstExecutionSummaryUpdatedOn() {
		Date firstExecSummaryCreatedOn = null;
		
		SearchRequestBuilder srb = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
	    srb.setIndices(SearchConstants.INDEX_NAME_EXECUTION_SUMMARY_ALIAS)
	    .setTypes(SearchConstants.DOCUMENT_TYPE_EXECUTION_SUMMARY);
	    srb.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
	    srb.setSize(1);
	    srb.addSort(SYS_UPDATED_ON_FIELD_NAME, SortOrder.ASC);
	    
	    SearchResponse response = srb.execute().actionGet();
	    
	    if (response.getHits().getHits().length > 0) {
	    	Long createdOnMilliSec = (Long) response.getHits().getHits()[0].getSource().get(SYS_UPDATED_ON_FIELD_NAME);
	    	
	    	if (createdOnMilliSec != null) {
	    		firstExecSummaryCreatedOn = new Date(createdOnMilliSec.longValue());
	    	}
	    }
		
		return firstExecSummaryCreatedOn;
	}
	
	private static Date getEexecSummaryIndxSearchEndDate(Date startDate) {
		long currentMillis = Instant.now().toEpochMilli();
		Date esiSearchEndDate = new Date(currentMillis - 1);
		
		if (Duration.ofMillis(esiSearchEndDate.getTime() - startDate.getTime()).toDays() > 90) {
			esiSearchEndDate = new Date(startDate.getTime() + (Duration.ofDays(90).toMillis() - 1));
		}
		
		return esiSearchEndDate;
	}

	private static AggrExecSummary setupAggrExecSummary(Date startDate, String orgId, String rbcSource,
			 										    Long usageCount, Date searchEndDate) {
		AggrExecSummary aggrExecSummary = new AggrExecSummary();
		
		if (searchEndDate != null) {
			// Setup BaseModel			
			aggrExecSummary.setSysCreatedBy(SYSTEM);
			aggrExecSummary.setSysCreatedOn(searchEndDate);
			aggrExecSummary.setSysUpdatedBy(SYSTEM);
			aggrExecSummary.setSysUpdatedOn(searchEndDate);
			aggrExecSummary.setSysModCount(NON_NEGATIVE_INTEGER_DEFAULT);
			aggrExecSummary.setSysOrg(null);
			aggrExecSummary.setSysIsDeleted(Boolean.FALSE);
			aggrExecSummary.setChecksum(NON_NEGATIVE_INTEGER_DEFAULT);
		}
		
		// Setup Aggregation Execution Summary
		
		aggrExecSummary.setStartDate(startDate);
		aggrExecSummary.setOrgId(orgId);
		aggrExecSummary.setRbcSource(rbcSource);
		
		if (usageCount != null) {
			aggrExecSummary.setUsageCount(usageCount);
		}
		
		return aggrExecSummary;
	}
	
	private static AggrExecSummary findAggExecSummary(Date startDate, String orgId, String rbcSource) {
		AggrExecSummary aggrExecSummary = null;
		AggrExecSummary eAES = setupAggrExecSummary(startDate, orgId, rbcSource, null, null);
		
		try {
			HibernateProxy.setCurrentUser(SYSTEM);
            aggrExecSummary = (AggrExecSummary) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
            	return HibernateUtil.getDAOFactory().getAggrExecSummaryDAO().findFirst(eAES);
            });
		} catch (Throwable t) {
            log.error(String.format("Error %sin getting Aggregated Execution Summary based on example %s",
            						(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
            						eAES), t);
            HibernateUtil.rethrowNestedTransaction(t);
            throw new RuntimeException(t);
        }
		
		return aggrExecSummary;
	}
	
	private static void handleEODRefresh(Date yesterday, Date today) {
		Date yesterdayStart = new Date(yesterday.getTime() - (yesterday.getTime() % (24 * 60 * 60 * 1000)));
		Date yesterdayEnd = new Date( yesterdayStart.getTime() + (Duration.ofDays(1).toMillis() - 1));
				
		log.info(String.format("Handle EOD Refresh from yesterday [%s] to today [%s]", yesterdayStart, yesterdayEnd));
		
		ResponseDTO<Map<String, Map<String, Map<String, Long>>>> result2 = SaaSReport.getVolumeBasedUsageReportV2(
																										yesterdayStart.getTime(), 
																										yesterdayEnd.getTime(),
																										null,
																										false);

		int esRecCnt = (result2 != null && result2.getRecords() != null) ? 
					   result2.getRecords().size() : VO.NON_NEGATIVE_INTEGER_DEFAULT.intValue();

		log.info(String.format("Handle EOD Refresh : ES Execution Summary Index Search Start Date [%s] to End Date [%s] " +
							   "returned %d records", yesterdayStart, yesterdayEnd, esRecCnt));
		
		if (result2 != null && result2.getRecords() != null && !result2.getRecords().isEmpty()) {
        	
        	if (log.isDebugEnabled()) {
        		result2.getRecords()
        		.stream()
        		.forEach(b -> log.debug(String.format("Handle EOD Refresh : Bucket[%s]", 
        								StringUtils.mapToString(b, "=", ", ", true))));
        	}
        	
    		persistUsageBuckets(result2.getRecords(), yesterdayStart, yesterdayEnd);
    		
        }
	}
	
	private static AggrExecSummary upsertAggrExecSummary(Date startDate, String orgId, String rbcSource,
			 											 Long usageCount, Date searchStartDate, Date searchEndDate) {		
		boolean upsert = false;
		boolean replicate = false;
		AggrExecSummary aggrExecSummary = null;
		
		/*
		 * Assumption:
		 * 
		 * While aggregating Execution Summary data from ES to DB by primary RSView, 
		 * searches for historical data will be done for max 90 days at a time till
		 * at least one aggregated bucket is returned.
		 * 
		 * This will avoid overloading of ES and RSView by execution summary aggregation thread.
		 * 
		 * Once all historical data is aggregated execution summary aggregation thread will search
		 * ES every hour for current day's data.
		 */
		
		if (startDate.compareTo(searchStartDate) <= 0) {
			log.debug(String.format("Aggregated Execution Summary to Upsert's Start Date %s is same or before " +
									" Search Start Date %s so find existing aggregated execution summary to update...",
									startDate, searchStartDate ));
			
			AggrExecSummary dbAggrExecSummary = findAggExecSummary(startDate, orgId, rbcSource);
			
			if (dbAggrExecSummary != null && (dbAggrExecSummary.getUsageCount().compareTo(usageCount) != 0 ||
											  dbAggrExecSummary.getSysUpdatedOn().before(searchEndDate))) {
				upsert = true;
				dbAggrExecSummary.setUsageCount(usageCount);
				dbAggrExecSummary.setSysModCount(Integer.valueOf(dbAggrExecSummary.getSysModCount().intValue() + 1));
				
				// Identify EOD (End Of Day) 
				
				Period diffPeriod = Period.ofDays((int) (Duration.ofMillis(startDate.getTime()).toDays()))
									.minus(Period.ofDays((int) (Duration.ofMillis(searchStartDate.getTime()).toDays())))
									.normalized();
					
				log.debug(String.format("Difference between Start Day Period and Search Start Day Period is %s",
										diffPeriod));
	
				if (diffPeriod.getDays() == 1 && diffPeriod.getMonths() == 0 && diffPeriod.getYears() == 0 ) {
					/*
					 * If reached EOD then update previous day usage to account for 
					 * usage between last refresh of previous day till EOD
					 */
					handleEODRefresh(dbAggrExecSummary.getSysUpdatedOn(), searchEndDate);
				}
				
				dbAggrExecSummary.setSysUpdatedOn(searchEndDate);
				aggrExecSummary = dbAggrExecSummary;
				replicate = true; 
			}
		}
		
		if (aggrExecSummary == null && !upsert) {
			upsert = true;
			aggrExecSummary = setupAggrExecSummary(startDate, orgId, rbcSource, usageCount, searchEndDate);
		}
		
		AggrExecSummary newAggrExecSummary = aggrExecSummary;
		
		if (upsert) {
			boolean replicateFinal = replicate;
			AggrExecSummary aggrExecSummaryFinal = aggrExecSummary;
			try {
				HibernateProxy.setCurrentUser(SYSTEM);
				newAggrExecSummary = (AggrExecSummary) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
					AggrExecSummary result;
					if (!replicateFinal) {
						result = HibernateUtil.getDAOFactory().getAggrExecSummaryDAO()
										 	.insertOrUpdate(aggrExecSummaryFinal);
					} else {
						/*
						 * Resolve update interceptor overwrites the sysUpdatedOn on updating entity
						 * which messes up the sysUpdatedOn logic used by Execution Summary Aggregation
						 * hence update the entity by replicating avoiding update interceptor.
						 */
						HibernateUtil.getCurrentSession().replicate(AggrExecSummary.class.getName(), 
																	aggrExecSummaryFinal, ReplicationMode.OVERWRITE);
						result = aggrExecSummaryFinal;
					}
					
					return result;	
				});
				
			} catch (Throwable t) {
				log.error(String.format("Error %sin inserting/updating Aggregated Execution Summary %s", 
										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										aggrExecSummaryFinal), t);
                HibernateUtil.rethrowNestedTransaction(t);
                throw new RuntimeException(t);
			}
		}

		return newAggrExecSummary;
	}
	
	private static void persistUsageBuckets(List<Map<String, Map<String, Map<String, Long>>>> usageBuckets, 
									        Date execSummaryIndxSearchStartDate,
									        Date execSummaryIndxSearchEndDate) {
		if (usageBuckets != null && !usageBuckets.isEmpty()) {
			
			usageBuckets
			.parallelStream()
			.forEach(ub -> {
				if (ub.keySet() != null && !ub.keySet().isEmpty()) {
					SimpleDateFormat sdf = new SimpleDateFormat(SaaSReport.BUCKET_DATE_TIME_FORMAT);
			    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
					final String startDateString = "" + ub.keySet().iterator().next();
					
					Date startDate = null;
					
					try {
						if (startDateString.endsWith("Z")) {
							startDate = sdf.parse(startDateString);
						} else {
							startDate = new Date(Long.parseLong(startDateString));
						}
				    	 
						if (log.isTraceEnabled()) {
							log.trace(String.format("Start Date \"%s\" to Date Object [%s] to " +
				        		 					"DateFormat.format(Date) [%s].", startDateString, 
				        		 					startDate.toString(), sdf.format(startDate)));
						}
					} catch (ParseException | NumberFormatException e) {
						log.debug(String.format("Error [%s] in parsing bucket key start date \"%s\" to Date.", 
												(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""),
												startDateString), e);
					}
					
					if (startDate == null) {
						log.warn(String.format("Bucket Key String parsed from bucket key as string " +
			 				    			   "[%s] is null or blank, returning null Bucket object!!!",
			 				    			   startDateString));
						return;
					}
					
					final Date unmodifiableStartDate = startDate;
					
					Map<String, Map<String, Long>> byOrgsByRBCs = ub.get(startDateString);
					
					byOrgsByRBCs
					.keySet()
					.parallelStream()
					.forEach(o -> {
						String orgId = o;
						
						if (!orgId.equalsIgnoreCase(SaaSReport.ALL)) {
							Map<String, Long> byRBCs = byOrgsByRBCs.get(orgId);
							
							byRBCs
							.keySet()
							.parallelStream()
							.forEach(r -> {
								String rbcSource = r;
								
								try {
									if (!rbcSource.equalsIgnoreCase(SaaSReport.ALL)) {
										Long usageCount = byRBCs.get(rbcSource);
										
										if (usageCount != null && usageCount.longValue() > 0l) {
											AggrExecSummary aggrExecSummary = 
																upsertAggrExecSummary(unmodifiableStartDate, orgId, 
																					  rbcSource, usageCount,
																					  execSummaryIndxSearchStartDate,
																					  execSummaryIndxSearchEndDate);
											
											log.trace("Upserted:" + aggrExecSummary);
										}
									}
								} catch (Throwable t) {
									log.error(StringUtils.isNotBlank(t.getMessage()) ? t.getMessage() : "", t);
								}
							});
						}
					});
				}
			});
		}
	}
	
	/*
	 * Aggregate ES Execution Summary and store in DB.
	 * Called from Primary RSView every x minutes.
	 */
	public static void aggregateUsage() {
		// Exit if previous aggregate execution summary indices operation is still in progress
		if (aggrExecSummaryInProgress) {
			log.info("Skipping aggregate executon summary indices as previous as previous operatin is still in progress...");
			return;
		}
		
		if (deleteIndicesInProgress) {
			log.info("Skipping aggregate executon summary indices as currently delete aggregated execution summary indices " +
					 "operatin is in progress...");
			return;
		}
				
		aggrExecSummaryInProgress = true;
				
		log.info(String.format("aggregateUsage In DBPoolUsage [%s]", 
				   			   StringUtils.mapToString(
				   					   			new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		
		try {
			Date latestAggrExecSummaryUpdatedOn = null;
			AggrExecSummary latestAggrExecSummary = getLatestAggrExecSummary();
			
			if (latestAggrExecSummary != null) {
				log.info(String.format("Latest Aggregated Execution Summary in DB: %s", latestAggrExecSummary));
				latestAggrExecSummaryUpdatedOn = new Date(latestAggrExecSummary.getSysUpdatedOn().getTime() + 1);
			} else {
				log.info("No Aggrgated Execution Summary in DB!!!, looking for first record in ES Execution Summary Index...");
			}
			
			Date firstExecSummaryUpdatedOn = latestAggrExecSummaryUpdatedOn != null ?
											 latestAggrExecSummaryUpdatedOn : getFirstExecutionSummaryUpdatedOn();
	        
			if (firstExecSummaryUpdatedOn != null) {
				log.info(String.format("First record's Updated On in ES Execution Summary Index: %s", firstExecSummaryUpdatedOn));
			} else {
				log.info("No records in ES Execution Summary Index!!!, bailing out till next run...");
				return;
			}
			
	        Date execSummaryIndxSearchStartDate = new Date(firstExecSummaryUpdatedOn.getTime() - 
	        										       (firstExecSummaryUpdatedOn.getTime() % Duration.ofDays(1).toMillis()));
	        
	        log.info(String.format("ES Execution Summary Index Search Start Date: %s", execSummaryIndxSearchStartDate));
	        
	        Date execSummaryIndxSearchEndDate = getEexecSummaryIndxSearchEndDate(execSummaryIndxSearchStartDate);
	        
	        log.info(String.format("ES Execution Summary Index Search End Date: %s", execSummaryIndxSearchEndDate));
	        
	        ResponseDTO<Map<String, Map<String, Map<String, Long>>>> result2 = 
																		SaaSReport.getVolumeBasedUsageReportV2(
																			execSummaryIndxSearchStartDate.getTime(), 
																			execSummaryIndxSearchEndDate.getTime(),
																			null,
																			false);
	        
	        int esRecCnt = (result2 != null && result2.getRecords() != null) ? 
				    	   result2.getRecords().size() : VO.NON_NEGATIVE_INTEGER_DEFAULT.intValue();
				    	   
	        log.info(String.format("ES Execution Summary Index Search Start Date [%s] to End Date [%s] returned %d records",
	        					   execSummaryIndxSearchStartDate, execSummaryIndxSearchEndDate, esRecCnt));
	        
	        boolean upserted = false;
	        
	        if (result2 != null && result2.getRecords() != null && !result2.getRecords().isEmpty()) {
	        	
	        	if (log.isTraceEnabled()) {
	        		result2.getRecords()
	        		.stream()
	        		.forEach(b -> log.trace(String.format("ES Bucket[%s]", StringUtils.mapToString(b, "=", ", ", true))));
	        	}
	        	
	    		upserted = true;
	    		persistUsageBuckets(result2.getRecords(), execSummaryIndxSearchStartDate, execSummaryIndxSearchEndDate);
	    		
	        } else {
	        	log.info(String.format("Update Latest Aggregated Execution Summary: %s sysUpdatedOn to %s", 
	        						   latestAggrExecSummary, execSummaryIndxSearchEndDate));
	        }
	        
	        if (!upserted) {
	        	if (latestAggrExecSummary != null) {
		        	AggrExecSummary updatedLatestAggrExecSummary = upsertAggrExecSummary(latestAggrExecSummary.getStartDate(),
							 															 latestAggrExecSummary.getOrgId(),
							 															 latestAggrExecSummary.getRbcSource(),
							 															 latestAggrExecSummary.getUsageCount(),
							 															 execSummaryIndxSearchEndDate,
							 															 execSummaryIndxSearchEndDate);
		        	
		        	if (updatedLatestAggrExecSummary != null) {
		        		log.info(String.format("Updated Latest Aggregated Execution Summary: %s", updatedLatestAggrExecSummary));
		        	}
	        	}
	        }
		} catch (Throwable t) {
			log.error(String.format("Error%sin aggregating usage data from ES into DB.", 
									(StringUtils.isNotBlank(t.getMessage()) ? " [" + t.getMessage() + "] " : " ")));
		} finally {		
			log.info(String.format("aggregateUsage Out DBPoolUsage [%s]", 
	   			   			   	   StringUtils.mapToString(new MInfo().dbpoolUsage(new HashMap<String, Object>()), 
	   			   			   			   				   "=", ", ")));
			aggrExecSummaryInProgress = false;
		}
	}
	
	private static Map<String, Map<String, Long>> convertConcurrentRBCCountsByOrgToMap(
													ConcurrentMap<String, ConcurrentMap<String, Long>> rbcCountsByOrgConcurrent) {
		Map<String, Map<String, Long>> rbcCountsByOrg = new HashMap<String, Map<String, Long>>();
		
		rbcCountsByOrgConcurrent
		.keySet()
		.parallelStream()
		.forEach(org -> {
			ConcurrentMap<String, Long> countsByRBCConcurrent = rbcCountsByOrgConcurrent.get(org);
			
			Map<String, Long> countsByRBC = new HashMap<String, Long>();
			countsByRBC.putAll(countsByRBCConcurrent);
			
			rbcCountsByOrg.put(org, countsByRBC);
		});
		
		return rbcCountsByOrg;
	}
	
	private static ConcurrentMap<Long, ConcurrentMap<String, ConcurrentMap<String, Long>>> 
						aesSummariesToOrgRBCCountsByStartDate(List<AggrExecSummary> aesSummaries) {
		ConcurrentMap<Long, ConcurrentMap<String, ConcurrentMap<String, Long>>> orgRBCCountsByStartDate = 
				new ConcurrentHashMap<Long, ConcurrentMap<String, ConcurrentMap<String, Long>>>();
		
		Set<Long> bucketsToCheckForNull = new HashSet<Long>();
		ConcurrentMap<Long, Set<String>> bucketOrgToCheckForNull = new ConcurrentHashMap<Long, Set<String>>();
		
		aesSummaries
		.parallelStream()
		.forEach(aes -> {
			ConcurrentMap<String, ConcurrentMap<String, Long>> rbcCountsByOrg = 
				orgRBCCountsByStartDate.putIfAbsent(Long.valueOf(aes.getStartDate().getTime()), 
													new ConcurrentHashMap<String, ConcurrentMap<String, Long>>());
			
			rbcCountsByOrg = orgRBCCountsByStartDate.get(Long.valueOf(aes.getStartDate().getTime()));
			
			if (rbcCountsByOrg == null) {
				log.warn(String.format("Failed to create/find after creating bucket for Aggregated Execution Summary [%s]!!!", 
									   aes));
				return;
			}
			
			ConcurrentMap<String, Long> countsByRBC = rbcCountsByOrg.putIfAbsent(aes.getOrgId(), 
																	   			 new ConcurrentHashMap<String, Long>());
			
			countsByRBC = rbcCountsByOrg.get(aes.getOrgId());
			
			if (countsByRBC == null) {
				log.warn(String.format("Failed to create/find after creating RBC Counts By Org for Aggregated Execution " +
									   "Summary [%s]!!!", aes));
				bucketsToCheckForNull.add(Long.valueOf(aes.getStartDate().getTime()));
				return;
			}
			
			if (aes.ugetUsageCount().longValue() > 0l) {
				countsByRBC.put(aes.getRbcSource(), aes.ugetUsageCount());
			} else {
				log.info(String.format("Not inserting Aggregated Execution Summary [%s] into bucket as Usage Count is 0", aes));
				bucketsToCheckForNull.add(Long.valueOf(aes.getStartDate().getTime()));
				Set<String> orgIds = bucketOrgToCheckForNull.putIfAbsent(Long.valueOf(aes.getStartDate().getTime()), 
																		 new HashSet<String>());
				
				orgIds = bucketOrgToCheckForNull.get(Long.valueOf(aes.getStartDate().getTime()));
				
				if (orgIds != null) {
					orgIds.add(aes.getOrgId());
				}
			}
		});
		
		// Filter out buckets with no Counts By RBC Source By Org for Start Date
		
		for (Long startDate : bucketsToCheckForNull) {
			if (orgRBCCountsByStartDate.containsKey(startDate)) {
				ConcurrentMap<String, ConcurrentMap<String, Long>> rbcCountsByOrg = orgRBCCountsByStartDate.get(startDate);
				
				if (rbcCountsByOrg == null || rbcCountsByOrg.isEmpty()) {
					orgRBCCountsByStartDate.remove(startDate);
					continue;
				}
				
				if (bucketOrgToCheckForNull.containsKey(startDate) && bucketOrgToCheckForNull.get(startDate) != null &&
					!bucketOrgToCheckForNull.get(startDate).isEmpty()) {
					for (String orgId : bucketOrgToCheckForNull.get(startDate)) {
						ConcurrentMap<String, Long> countsByRBC = rbcCountsByOrg.get(orgId);
						
						if (countsByRBC == null || countsByRBC.isEmpty()) {
							rbcCountsByOrg.remove(orgId);
						}
					}
				}
				
				if (rbcCountsByOrg.isEmpty()) {
					orgRBCCountsByStartDate.remove(startDate);
				}
			}
		}
		
		return orgRBCCountsByStartDate;
	}
	
	private static void injectIncidentAndEventRBCSourceCountsForOrg(
							ConcurrentMap<Long, ConcurrentMap<String, ConcurrentMap<String, Long>>> orgRBCCountsByStartDate,
							Set<String> eventSources) {
		orgRBCCountsByStartDate
		.keySet()
		.parallelStream()
		.forEach(sd -> { // Map of RBC Source Counts By Org By Start Date
			ConcurrentMap<String, ConcurrentMap<String, Long>> rbcCountsByOrg = orgRBCCountsByStartDate.get(sd);
			ConcurrentMap<String, Long> allOrgCountsByRBC = rbcCountsByOrg.putIfAbsent(SaaSReport.ALL, 
																					   new ConcurrentHashMap<String, Long>());
			
			allOrgCountsByRBC = rbcCountsByOrg.get(SaaSReport.ALL);
			
			if (allOrgCountsByRBC == null) {
				log.warn(String.format("Failed to create/find after creating Counts By RBC By Org for Start Date [%s]!!!", 
						   			   new Date(sd.longValue())));
				return;
			}
			
			allOrgCountsByRBC.put(SaaSReport.INCIDENT, NON_NEGATIVE_LONG_DEFAULT);
			allOrgCountsByRBC.put(SaaSReport.EVENT, NON_NEGATIVE_LONG_DEFAULT);
			
			rbcCountsByOrg
			.keySet()
			.parallelStream()
			.forEach(org -> { // Map of RBC Source Counts By Org
				if (!org.equalsIgnoreCase(SaaSReport.ALL)) {
					ConcurrentMap<String, Long> countsByRBC = rbcCountsByOrg.get(org);
					
					if (countsByRBC == null || countsByRBC.isEmpty()) {
						log.warn(String.format("Failed to find Counts By RBC for Org [%s] for Start Date [%s]!!!", 
					   			   			   org, new Date(sd.longValue())));
						return;
					}
					
					countsByRBC.putIfAbsent(SaaSReport.INCIDENT, NON_NEGATIVE_LONG_DEFAULT);
					countsByRBC.putIfAbsent(SaaSReport.EVENT, NON_NEGATIVE_LONG_DEFAULT);
					
					if (countsByRBC.containsKey(SaaSReport.INCIDENT) && countsByRBC.containsKey(SaaSReport.EVENT)) {
						countsByRBC
						.keySet()
						.stream()
						.forEach(rbcs -> {
							if (!rbcs.equalsIgnoreCase(SaaSReport.INCIDENT) && !rbcs.equalsIgnoreCase(SaaSReport.EVENT)) {
								if (eventSources.contains(rbcs.toLowerCase())) {
									countsByRBC.put(SaaSReport.EVENT, 
													Long.valueOf(countsByRBC.get(SaaSReport.EVENT).longValue() + 
																 countsByRBC.get(rbcs).longValue()));
								} else {
									countsByRBC.put(SaaSReport.INCIDENT, 
													Long.valueOf(countsByRBC.get(SaaSReport.INCIDENT).longValue() + 
											   					 countsByRBC.get(rbcs).longValue()));
								}
							}
						});
					}
					
					// Remove Incident/Event with no usage
					
					if (countsByRBC.get(SaaSReport.INCIDENT).longValue() == NON_NEGATIVE_LONG_DEFAULT.longValue()) {
						countsByRBC.remove(SaaSReport.INCIDENT);
					}
					
					if (countsByRBC.get(SaaSReport.EVENT).longValue() == NON_NEGATIVE_LONG_DEFAULT.longValue()) {
						countsByRBC.remove(SaaSReport.EVENT);
					}
				}
			});
			
			// Aggregate Incident/Event counts for all Orgs and update "All" Org Incident & Event counts
			
			rbcCountsByOrg
			.keySet()
			.stream()
			.forEach(org -> {
				if (!org.equalsIgnoreCase(SaaSReport.ALL)) {
					ConcurrentMap<String, Long> countsByRBC = rbcCountsByOrg.get(org);
					
					if (countsByRBC.containsKey(SaaSReport.INCIDENT)) {
						rbcCountsByOrg.get(SaaSReport.ALL)
						.put(SaaSReport.INCIDENT, 
							 Long.valueOf(rbcCountsByOrg.get(SaaSReport.ALL).get(SaaSReport.INCIDENT).longValue() + 
									 	  countsByRBC.get(SaaSReport.INCIDENT).longValue()));
					}
					
					if (countsByRBC.containsKey(SaaSReport.EVENT)) {
						rbcCountsByOrg.get(SaaSReport.ALL)
						.put(SaaSReport.EVENT, 
							 Long.valueOf(rbcCountsByOrg.get(SaaSReport.ALL).get(SaaSReport.EVENT).longValue() + 
									 	  countsByRBC.get(SaaSReport.EVENT).longValue()));
					}
				}
			});
			
			// Remove "All" Org Incident/Event with no usage
			
			if (allOrgCountsByRBC.get(SaaSReport.INCIDENT).longValue() == NON_NEGATIVE_LONG_DEFAULT.longValue()) {
				allOrgCountsByRBC.remove(SaaSReport.INCIDENT);
			}
			
			if (allOrgCountsByRBC.get(SaaSReport.EVENT).longValue() == NON_NEGATIVE_LONG_DEFAULT.longValue()) {
				allOrgCountsByRBC.remove(SaaSReport.EVENT);
			}
		});		
	}
	
	private static List<Map<String, Map<String, Map<String, Long>>>> 
						createDBUsageBuckets(
							ConcurrentMap<Long, ConcurrentMap<String, ConcurrentMap<String, Long>>> orgRBCCountsByStartDate) {
		List<Map<String, Map<String, Map<String, Long>>>> dbUsageBuckets = 
				new ArrayList<Map<String, Map<String, Map<String, Long>>>>();
		
		ConcurrentMap<Long, Map<String, Map<String, Map<String, Long>>>> dbUsageBucketsByStartDate = 
				new ConcurrentHashMap<Long, Map<String, Map<String, Map<String, Long>>>>(orgRBCCountsByStartDate.keySet().size());
		
		log.trace(String.format("orgRBCCountsByStartDate KeySet size %d", orgRBCCountsByStartDate.keySet().size()));
		
		orgRBCCountsByStartDate
		.keySet()
		.parallelStream()
		.forEach(sd -> {
			final long startDateLong = sd.longValue();
			SimpleDateFormat sdf = new SimpleDateFormat(SaaSReport.BUCKET_DATE_TIME_FORMAT);
	    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
	    	
	    	Map<String, Map<String, Map<String, Long>>> dbUsageBucket = 
	    			dbUsageBucketsByStartDate.put(Long.valueOf(startDateLong), 
	    										  new HashMap<String, Map<String, Map<String, Long>>>());
	    	
	    	dbUsageBucket = dbUsageBucketsByStartDate.get(Long.valueOf(startDateLong));
	    	
	    	if (dbUsageBucket == null) {
	    		log.warn(String.format("dbUsageBucket for Start Date [%s] is null, not adding " +
						   			   "to dbUsageBucket!!!", new Date(startDateLong)));
	    		return;
	    	}
	    	
	    	ConcurrentMap<String, ConcurrentMap<String, Long>> rbcCountsByOrgConcurrent = 
	    															orgRBCCountsByStartDate.get(Long.valueOf(startDateLong));
	    	final String startDateStr = sdf.format(new Date(startDateLong));
	    	
	    	if (rbcCountsByOrgConcurrent == null || rbcCountsByOrgConcurrent.isEmpty()) {
	    		log.warn(String.format("RBC Source Counts By Org Concurrent for Start Date [%s] is null or empty, not adding " +
	    							   "to dbUsageBucket!!!", new Date(startDateLong)));
	    		return;
	    	}
	    	
	    	if (StringUtils.isBlank(startDateStr) || !startDateStr.endsWith("Z")) {
	    		log.warn(String.format("Invalid Start Date String [%s] for %d milliseconds, not adding to dbUsageBucket!!!", 
	    								startDateStr, startDateLong));
	    		return;
	    	}
	    	
	    	final Map<String, Map<String, Long>> rbcCountsByOrg = 
	    												convertConcurrentRBCCountsByOrgToMap(rbcCountsByOrgConcurrent);
	    	
	    	if (rbcCountsByOrg == null || rbcCountsByOrg.isEmpty()) {
	    		log.warn(String.format("Converted RBC Source Counts By Org for Start Date [%s] is null or empty, not adding " +
						   			   "to dbUsageBucket!!!", startDateStr));
	    		return;
	    	}
	    	
	    	Map<String, Map<String, Long>> previousRBCCountsByOrg = dbUsageBucket.put(startDateStr, rbcCountsByOrg);
	    	
	    	if (previousRBCCountsByOrg != null) {
	    		log.warn(String.format("Found Previous RBC Source Counts By Org [%s] for Start Date [%s]!!!",
			   			   			   StringUtils.mapToString(previousRBCCountsByOrg, "=", ", ", true), startDateStr));
	    	}
	    	
	    	if (log.isTraceEnabled()) {
	    		log.trace(String.format("Adding DB Bucket %s to dbUsageBuckets", 
	    								StringUtils.mapToString(dbUsageBucket, "=", ", ", true)));
	    	}	    	
		});
		
		log.trace(String.format("Size of dbUsageBucketsByStartDate Keys is %d and values is %d", 
								dbUsageBucketsByStartDate.keySet().size(), dbUsageBucketsByStartDate.values().size()));
		
		dbUsageBuckets.addAll(dbUsageBucketsByStartDate.values());
		
		log.trace(String.format("Unsorted DB Usage Buckets Count %d B4 removing null Usage Buckets", dbUsageBuckets.size()));
		
		List<Map<String, Map<String, Map<String, Long>>>> dbUsageNonNullBuckets =
																dbUsageBuckets
																.parallelStream()
																.filter(Objects::nonNull)
																.collect(Collectors.toList());
		
		log.trace(String.format("Unsorted Non Null DB Usage Buckets Count %d B4 sorting", 
								dbUsageNonNullBuckets.size()));
		
		if (log.isTraceEnabled()) {
			dbUsageNonNullBuckets
			.stream()
			.forEach(dbb -> {
				log.trace(String.format("Unsorted Non Null DB Usage Bucket: %s", StringUtils.mapToString(dbb, "=", ", ", true)));
			});
		}
		
		return dbUsageNonNullBuckets;
	}
	
	private static List<Map<String, Map<String, Map<String, Long>>>> processAggExecSummaries(List<AggrExecSummary> aesSummaries, 
																			  				 Set<String> eventSources) {
		List<Map<String, Map<String, Map<String, Long>>>> dbUsageBuckets = 
				new ArrayList<Map<String, Map<String, Map<String, Long>>>>();
		
		log.debug(String.format("processAggExecSummaries: %d aesSummaries", aesSummaries.size()));
		
		// AggrExecSummary unique composite key is Start Date + Org + RBC Source
		
		if (aesSummaries != null && !aesSummaries.isEmpty()) {
			
			ConcurrentMap<Long, ConcurrentMap<String, ConcurrentMap<String, Long>>> orgRBCCountsByStartDate = 
					aesSummariesToOrgRBCCountsByStartDate(aesSummaries);
			
			log.debug(String.format("processAggExecSummaries: After aesSummariesToOrgRBCCountsByStartDate %d orgRBCCountsByStartDates", orgRBCCountsByStartDate.size()));
			
			// Add legacy_netcool RBC Source as default event source
			
			if (eventSources == null) {
				eventSources = new HashSet<String>();
			}
			
			eventSources.add(SaaSReport.LEGACY_NETCOOL.toLowerCase());
			
			/*
			 * Inject pseudo Incident & Event RBC counts based on specified event sources
			 * and aggregate of Incident and Event RBC counts for all Orgs ("All" Org name)
			 */
			
			injectIncidentAndEventRBCSourceCountsForOrg(orgRBCCountsByStartDate, eventSources);
			
			log.debug(String.format("processAggExecSummaries: After injectIncidentAndEventRBCSourceCountsForOrg %d " +
								    "orgRBCCountsByStartDates", orgRBCCountsByStartDate.size()));
			
			// Create List of Map of RBC Source Counts By Orgs By Start Date as UTC 
			
			dbUsageBuckets = createDBUsageBuckets(orgRBCCountsByStartDate);
			
			log.debug(String.format("processAggExecSummaries: After createDBUsageBuckets %d dbUsageBuckets", dbUsageBuckets.size()));
		}
		
		return dbUsageBuckets;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Map<String, Map<String, Map<String, Long>>>> getDBUsageBuckets(Date startDateFrom, 
																					  Date startDateTo,
																					  Set<String> eventSources) throws Exception {
		List<Map<String, Map<String, Map<String, Long>>>> dbUsageBuckets = null;
		
		QueryDTO aesQueryDTO = new QueryDTO();
		
		aesQueryDTO.setModelName(AggrExecSummary.class.getSimpleName());
		SimpleDateFormat sdf = new SimpleDateFormat(SaaSReport.BUCKET_DATE_TIME_FORMAT);
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
		aesQueryDTO.addFilterItem(new QueryFilter(DATE_TYPE, "startDate", sdf.format(startDateFrom), 
				  			   				   	  sdf.format(startDateTo), BETWEEN));
		
		List<AggrExecSummary> aesSummaries = null;
		
		try {
			HibernateProxy.setCurrentUser(SYSTEM);
			aesSummaries = (List<AggrExecSummary>) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
				Query<AggrExecSummary> aesQuery = HibernateUtil.createQuery(aesQueryDTO);
				
				return aesQuery.list();
			});			
			
		} catch(Throwable t) {
            log.error(String.format("Error [%s] occurred while getting Aggregated Executed Suammries for Start Dates from " +
            						"%s to %s" + 
            						(StringUtils.isNotBlank(t.getMessage()) ? t.getMessage() : ""), startDateFrom, 
            						startDateTo), t);
            throw new Exception(t);
        }
		
		dbUsageBuckets = processAggExecSummaries(aesSummaries, eventSources);
		
		return dbUsageBuckets;
	}
	
	private static Instant getDayBeforeLatestAggrExecSummary() {
		Instant dayBeforeLAES = null;		
		AggrExecSummary latestAggrExecSummary = getLatestAggrExecSummary();
		
		if (latestAggrExecSummary != null) {
			if (log.isDebugEnabled()) {
				log.debug(String.format("Latest Aggregated Execution Summary in DB: %s", latestAggrExecSummary));
			}

			dayBeforeLAES = latestAggrExecSummary.getStartDate().toInstant().minus(1l, ChronoUnit.DAYS);
		
			if (log.isDebugEnabled()) {
				SimpleDateFormat sdf = new SimpleDateFormat(EXECUTION_SUMMARY_ALIAS_DATE_SUFFIX_FORMAT);
				sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
				log.debug(String.format("Day Before Latest Aggergated Execution Summary is %s", 
										sdf.format(new Date(dayBeforeLAES.toEpochMilli()))));
			}
		}
		
		return dayBeforeLAES;
	}
	
	private static List<String> getExecSummaryIndicesOnOrBefore(Instant dayBeforeLAES) {
		List<String> indxNames = new ArrayList<String>();
		
		ResponseDTO<String> respDTO = (APIFactory
									   .getExecutionSummarySearchAPI(ExecutionSummary.class))
									  .getAllIndexNames(dayBeforeLAES);
		
		if (respDTO != null && respDTO.isSuccess()) {
			indxNames = respDTO.getRecords();
		}
		
		log.info(String.format("Found %d execution summary indices on or before %s which needs to be deleted", 
							   indxNames.size(), dayBeforeLAES));
		
		if (log.isDebugEnabled()) {
			log.debug(String.format("Execution Summary Indices to be dropped are [%s]", 
									StringUtils.listToString(indxNames, ", ")));
		}
		
		return indxNames;
	}
	
	private static void deleteExecSummaryIndices(List<String> indxNamesToDelete) {
		indxNamesToDelete.parallelStream().forEach(indxNameToDelete -> {
			DeleteIndexRequest delIndxReq = new DeleteIndexRequest(indxNameToDelete);
			delIndxReq.timeout(TimeValue.timeValueMinutes(10)); // Timeout to wait for all nodes to acknowledge deletion
			delIndxReq.masterNodeTimeout(TimeValue.timeValueMinutes(4)); // Timeout to connect to master node
			delIndxReq.indicesOptions(IndicesOptions.strictSingleIndexNoExpandForbidClosed());
			
			long startTime = System.currentTimeMillis();
			
			DeleteIndexResponse deleteResponse = SearchAdminAPI.getClient().admin().indices().delete(delIndxReq).actionGet();
			
			if (!deleteResponse.isAcknowledged()) {
				log.warn(String.format("Failed to synchronously delete ES index %s, Response: [%s]", indxNameToDelete, 
									   deleteResponse));
			}
			
			if (log.isDebugEnabled() && deleteResponse.isAcknowledged()) {
				log.debug(String.format("Successfully deleted ES index %s in %d msec", indxNameToDelete,
										System.currentTimeMillis() - startTime));
			}
		});
	}
	
	private static List<String> getEmptyExecSummaryIndicesFromTo(Instant from, Instant to) {
		List<String> indxNames = new ArrayList<String>();
		
		ResponseDTO<String> respDTO = (APIFactory
				   					   .getExecutionSummarySearchAPI(ExecutionSummary.class))
				  					  .getAllEmptyIndexNames(from, to);

		if (respDTO != null && respDTO.isSuccess()) {
			indxNames = respDTO.getRecords();
		}

		log.info(String.format("Found %d empty execution summary indices%s%s (%sinclusive) " +
							   "which needs to be deleted", indxNames.size(), 
							   (from != null ? " between " + from + " to " : " upto "), to, (from != null ? "both " : "")));

		if (log.isDebugEnabled()) {
			log.debug(String.format("Empty Execution Summary Indices to be dropped are [%s]", 
									StringUtils.listToString(indxNames, ", ")));
		}

		return indxNames;
	}
	
	private static void delNonAggrgatedEmptyExecSummaryIndices(Instant dayBeforeLAES) throws Exception {
		Instant twoDaysBeforeToday = Instant.now().truncatedTo(ChronoUnit.DAYS).minus(2l, ChronoUnit.DAYS);
		
		/*
		 * Process non-aggregated execution summary indices only if day before 
		 * last aggregated execution summary is 2 days before today
		 */
		if (dayBeforeLAES != null && dayBeforeLAES.compareTo(twoDaysBeforeToday) >= 0) {
			log.info(String.format("Day before last aggregated execution summary %s is same or chronologially after " +
								   "two day before today %s, deletion of empty execution summary indices between %s to %s " +
								   "(both inclusive) is skipped", dayBeforeLAES, twoDaysBeforeToday, dayBeforeLAES, 
								   twoDaysBeforeToday));
			return;
		}
		
		List<String> indxNamesToDelete = getEmptyExecSummaryIndicesFromTo(dayBeforeLAES, twoDaysBeforeToday);
		
		if (CollectionUtils.isEmpty(indxNamesToDelete)) {
			log.info(String.format("No empty execution summary indices%s%s (%sinclusive) which needs to be deleted " +
								   "found in ES, deletion of empty execution summary indices%s%s (%sinclusive) is skipped", 
								   (dayBeforeLAES != null ? " between " + dayBeforeLAES + " to " : " upto "), 
								   twoDaysBeforeToday, (dayBeforeLAES != null ? "both " : ""),
								   (dayBeforeLAES != null ? " between " + dayBeforeLAES + " to " : " upto "), 
								   twoDaysBeforeToday, (dayBeforeLAES != null ? "both " : "")));
			return;
		}
		
		long startTime = System.currentTimeMillis();
		log.info(String.format("Started synchronous deletion of %d empty execution summary indices", 
							   indxNamesToDelete.size()));
		deleteExecSummaryIndices(indxNamesToDelete);
		log.info(String.format("Completed synchronous deletion of %d empty execution summary indices in %d msec", 
							   indxNamesToDelete.size(), System.currentTimeMillis() - startTime));
	}
	
	private static void delAggrtedExecSummaryIndxs() throws Exception {
		Instant dayBeforeLAES = getDayBeforeLatestAggrExecSummary();
		
		if (dayBeforeLAES == null) {
			log.info("No aggrgated execution summaries found in DB!!!, checking non aggregated empty execution summary " +
					 "indices to delete...");
			delNonAggrgatedEmptyExecSummaryIndices(dayBeforeLAES);
			return;
		}
		
		List<String> indxNamesToDelete = getExecSummaryIndicesOnOrBefore(dayBeforeLAES);
		
		if (CollectionUtils.isEmpty(indxNamesToDelete)) {
			log.info(String.format("No execution summary indices on or before %s and which needs to be deleted " +
								   "found in ES, checking non aggregated empty execution summary indices to delete......", 
								   dayBeforeLAES));
			delNonAggrgatedEmptyExecSummaryIndices(dayBeforeLAES);
			return;
		}
		
		long startTime = System.currentTimeMillis();
		log.info(String.format("Started synchronous deletion of %d execution summary indices", indxNamesToDelete.size()));
		deleteExecSummaryIndices(indxNamesToDelete);
		log.info(String.format("Completed synchronous deletion of %d execution summary indices in %d msec", 
							   indxNamesToDelete.size(), System.currentTimeMillis() - startTime));
		
		delNonAggrgatedEmptyExecSummaryIndices(dayBeforeLAES);
	}
	
	/*
	 * Deletes aggregated execution summary indices.
	 * Called from Primary RSView every x minutes.
	 */
	public static void deleteAggregatedExecSummaryIndices() {
		// Exit if previous delete execution summary indices operation is still in progress
		if (deleteIndicesInProgress) {
			log.info("Skipping delete aggregated executon summary indices as previous operatin is still in progress...");
			return;
		}
		
		if (aggrExecSummaryInProgress) {
			log.info("Skipping delete aggregated executon summary indices as currently aggregate execution summary indices " +
					 "operatin is in progress...");
			return;
		}
		
		deleteIndicesInProgress = true;
		
		log.info(String.format("deleteAggregatedExecSummaryIndices In DBPoolUsage [%s]", 
	   			   			   StringUtils.mapToString(new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		
		try {
			delAggrtedExecSummaryIndxs();
		} catch (Throwable t) {
			log.error(String.format("Error%sin deleting aggregated execution summary indices from ES.", 
									(StringUtils.isNotBlank(t.getMessage()) ? " [" + t.getMessage() + "] " : " ")));
		} finally {				
			log.info(String.format("deleteAggregatedExecSummaryIndices Out DBPoolUsage [%s]", 
							   	   StringUtils.mapToString(new MInfo().dbpoolUsage(new HashMap<String, Object>()), 
							   			   				   "=", ", ")));
			deleteIndicesInProgress = false;
		}
	}
}