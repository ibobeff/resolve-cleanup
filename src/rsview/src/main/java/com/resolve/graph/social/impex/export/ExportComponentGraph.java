/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.export;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.util.StringUtils;

public abstract class ExportComponentGraph
{
    protected String sysId = null;
    protected ImpexOptionsDTO options = null;//optional
    protected SocialRelationshipTypes type = null;

    protected List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
    
    public ExportComponentGraph(String sysId, ImpexOptionsDTO options, SocialRelationshipTypes type) throws Exception
    {
        if(StringUtils.isEmpty(sysId) || type == null)
        {
            throw new Exception("sysId and type are mandatory.");
        }
        
        this.sysId = sysId;
        this.options = options;
        this.type = type;
    }
    
    public List<GraphRelationshipDTO> export(SocialImpexVO socialImpex) throws Exception
    {
        Node compNode = validate();
        return exportRelationships(compNode, socialImpex);
    }
    
    protected abstract List<GraphRelationshipDTO> exportRelationships(Node compNode, SocialImpexVO socialImpex) throws Exception;
    protected Node validate() throws Exception
    {
        Node compNode = ResolveGraphFactory.findNodeByIndexedID(sysId);
        if(compNode == null)
        {
            throw new Exception(type + " with sysId " + sysId + " does not exist");
        }
        
//        String sourceName = (String) compNode.getProperty(RSComponent.DISPLAYNAME);
//        if(compNode.hasProperty(RSComponent.TYPE))
//        {
//            String sourceType = (String) compNode.getProperty(RSComponent.TYPE);
//            if(!sourceType.equalsIgnoreCase(type.name()))
//            {
//                throw new Exception("This node is not of a " + type + " but of " + sourceType + " with name " + sourceName);
//            }
//        }
        
        return compNode;
    }
    
    protected void addRelationship(Node compNode, Node anyOtherNode) throws Exception
    {
//        String sourceName = (String) compNode.getProperty(RSComponent.DISPLAYNAME);
//        String sourceType = (String) compNode.getProperty(RSComponent.TYPE);
//        String targetType = (String) anyOtherNode.getProperty(RSComponent.TYPE);
//        String targetName = (String) anyOtherNode.getProperty(RSComponent.DISPLAYNAME);
//        if(targetType.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//        {
//            targetName =  (String) anyOtherNode.getProperty(RSComponent.USERNAME);
//        }
//        
//        GraphRelationshipDTO relation = new GraphRelationshipDTO();
//        relation.setSourceName(sourceName);
//        relation.setSourceType(SocialRelationshipTypes.valueOf(sourceType));
//        relation.setTargetName(targetName);
//        relation.setTargetType(SocialRelationshipTypes.valueOf(targetType));
//        
//        relationships.add(relation);
    }

}
