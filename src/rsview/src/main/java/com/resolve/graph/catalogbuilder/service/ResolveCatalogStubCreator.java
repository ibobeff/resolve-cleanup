/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;


public class ResolveCatalogStubCreator
{
//    private String path = null;
////    private String username = null;
//    
//    private ResolveCatalog rootCatalog = new ResolveCatalog();
//    
//    //optional - we should be able to create a place holder with just the path
//    private ResolveCatalog referenceCatalog = null;
//    
//    public ResolveCatalogStubCreator(String path, String username) throws Exception
//    {
//        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(path))
//        {
//            throw new Exception("Path and username are mandatory");
//        }
//        
//        if(!path.startsWith("/"))
//        {
//            throw new Exception("Invalid Path. It should start with '/' ");
//        }
//        
//        this.path = path;
////        this.username = username;
//    }
//    
//    public ResolveCatalog getReferenceCatalog()
//    {
//        return referenceCatalog;
//    }
//
//    public void setReferenceCatalog(ResolveCatalog referenceCatalog)
//    {
//        this.referenceCatalog = referenceCatalog;
//    }
//
//    public ResolveCatalog createStub() throws Exception
//    {
//        create(path, null);
//        
//        return rootCatalog;
//    }
//    
//    // /Jeet1/Group1/Item1
//    private void create(String originalPath, ResolveCatalog child)
//    {
//        String catalogName = originalPath.substring(originalPath.lastIndexOf("/")+1); //Item1
//        String parentPath = originalPath.substring(0, originalPath.lastIndexOf("/"));// /Jeet1/Group1
//        
//        if(parentPath.equals("/") || parentPath.equals(""))
//        {
//            //this is root
//            rootCatalog.setType(SocialRelationshipTypes.CatalogReference.name());
//            rootCatalog.setRoot(true);
//            rootCatalog.setName(catalogName);
//            rootCatalog.setPath(originalPath);
//            
//            if(child != null)
//            {
//                List<ResolveCatalog> children = new ArrayList<ResolveCatalog>();
//                children.add(child);
//                rootCatalog.setChildren(children);
//            }
//        }
//        else
//        {
//            //create the current catalog object
//            ResolveCatalog parent = new ResolveCatalog();
//            parent.setName(catalogName);
//            parent.setDisplayType(catalogName);
//            parent.setPath(originalPath);            
//            if(this.referenceCatalog != null && this.referenceCatalog.getPath().equals(originalPath))
//            {
//                parent.setType(referenceCatalog.getType());
//                parent.setTitle(referenceCatalog.getTitle());
//                parent.setInternalName(referenceCatalog.getInternalName());
//                parent.setNamespace(referenceCatalog.getNamespace());
//                parent.setOpenInNewTab(referenceCatalog.isOpenInNewTab());
//            }
//            else
//            {
//                //defaults for a catalog node
//                parent.setInternalName(catalogName);
//                parent.setType(SocialRelationshipTypes.CatalogGroup.name());
//            }
//            
//
//            if(child != null)
//            {
//                List<ResolveCatalog> children = new ArrayList<ResolveCatalog>();
//                children.add(child);
//                parent.setChildren(children);
//            }
//            
//            //**RECURSIVE
//            create(parentPath, parent);
//        }
//        
//    }
    
    

}
