/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.constants;

import org.neo4j.graphdb.RelationshipType;

public enum SocialRootRelationshipTypes implements RelationshipType
{
    PROCESSES, TEAMS, USERS, FORUMS, ACTIONTASKS, WORKSHEET, NAMESPACE, RUNBOOKS, DOCUMENT, DECISIONTREES, RSS, POSTS, DELETEDPOST, DELETEDCOMP, NOTIFICATIONS, NOTIFICATIONCONFIG
}

