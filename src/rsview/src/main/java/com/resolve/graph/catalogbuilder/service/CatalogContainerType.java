/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;

import org.neo4j.graphdb.RelationshipType;

public enum CatalogContainerType implements RelationshipType
{
    Catalog 
}
