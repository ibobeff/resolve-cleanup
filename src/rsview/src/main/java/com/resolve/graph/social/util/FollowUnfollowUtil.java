/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.rsview.main.Main;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.hibernate.util.SocialAdminUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class FollowUnfollowUtil
{
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Rss
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    public static void loadRss(Rss rss)
//    {
////        long follows = GraphUtil.getFollowerCountForComp(rss.getSys_id());
////        if(follows > 0)
////        {
////            Map<String, String> content = new HashMap<String, String>();
////            content.put(Constants.RSS_NAME, rss.getDisplayName());
////            content.put(Constants.RSS_SYS_ID, rss.getSys_id());
////            
////            Main.getESB().sendMessage("RSCONTROL", "MSocial.loadRSS", content);
////        }
////        else
////        {
////            removeRssCronJob(rss.getSys_id(), rss.getDisplayName());
////        }
//    }//loadRSS
//    
//    public static void removeRss(Rss rss)
//    {
////        long follows = GraphUtil.getFollowerCountForComp(rss.getSys_id());
////        if(follows == 0)
////        {
////            removeRssCronJob(rss.getSys_id(), rss.getDisplayName());
////        }
//        
//    }//removeRss
//    
//    public static void removeRssCronJob(String sysId, String rssName)
//    {
//        Map<String, String> content = new HashMap<String, String>();
//        content.put(Constants.RSS_NAME, rssName);
//        content.put(Constants.RSS_SYS_ID, sysId);
//        
//        Main.getESB().sendMessage("RSCONTROLS", "MSocial.removeRSS", content);
//        
//    }
    
    
    public static void acceptUserToJoin(String comptype, String sysId, String owner, String requestedUsername) throws Exception
    {
        UsersVO ownerVO = UserUtils.getUser(owner);
        Set<String> roles = UserUtils.getUserRoles(owner);

//        if(comptype.equalsIgnoreCase(Process.TYPE))
//        {
//            SocialProcessDTO processDTO = SocialAdminUtil.getProcess(sysId, owner);
//
//            //if the user who clicked the Accept button is the Owner or is an admin, than he can add the requested user to Process
//            if(processDTO.getU_owner().equalsIgnoreCase(owner) || roles.contains("admin"))
//            {
//                //add the user to Process
////                Process process = SocialCompConversionUtil.convertSocialProcessDTOToProcess(processDTO);
//                User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);
////                FollowUnfollowUtil.addUserToProcess(requestedUser, process);
//                
//                List<String> ids = new ArrayList<String>();
//                ids.add(sysId);
//                ServiceSocial.setFollowStreams(requestedUser, ids, true);
//                
//                //send the Post
//                SocialGenericActionsUtil.sendJoinConfirmPostForUser(AdvanceTree.PROCESS, ownerVO, requestedUser, processDTO);
//            }
//        }
//        else if(comptype.equalsIgnoreCase(Team.TYPE))
//        {
//            SocialTeamDTO teamDTO = SocialAdminUtil.getTeam(sysId, owner);
//            
//            //if the user who clicked the Accept button is the Owner or is an admin, than he can add the requested user to Team
//            if(teamDTO.getU_owner().equalsIgnoreCase(owner) || roles.contains("admin"))
//            {
//                //add the user to Team
////                Team team = SocialCompConversionUtil.convertSocialTeamDTOToTeam(teamDTO);
//                User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);
////                FollowUnfollowUtil.addUserToTeam(requestedUser, team);
//
//                List<String> ids = new ArrayList<String>();
//                ids.add(sysId);
//                ServiceSocial.setFollowStreams(requestedUser, ids, true);
//
//                //send the Post
//                SocialGenericActionsUtil.sendJoinConfirmPostForUser(AdvanceTree.TEAMS, ownerVO, requestedUser, teamDTO);
//            }
//        }
//        else if(comptype.equalsIgnoreCase(Forum.TYPE))
//        {
//            SocialForumDTO forumDTO = SocialAdminUtil.getForum(sysId, owner);
//            
//            //if the user who clicked the Accept button is the Owner or is an admin, than he can add the requested user to Forum
//            if(forumDTO.getU_owner().equalsIgnoreCase(owner) || roles.contains("admin"))
//            {
//                //add the user to Team
////                Forum forum = SocialCompConversionUtil.convertSocialForumDTOToForum(forumDTO);
//                User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);
////                FollowUnfollowUtil.addUserToForum(requestedUser, forum);
//                
//                List<String> ids = new ArrayList<String>();
//                ids.add(sysId);
//                ServiceSocial.setFollowStreams(requestedUser, ids, true);
//                
//                //send the Post
//                SocialGenericActionsUtil.sendJoinConfirmPostForUser(AdvanceTree.FORUMS, ownerVO, requestedUser, forumDTO);
//            }
//        }
    }
    
//    public static void rejectUserToJoin(String comptype, String sysId, String owner, String requestedUsername)
//    {
//        UsersVO ownerVO = UserUtils.getUser(owner);
//        User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);
//        SocialDTO socialDTO = null;
//        
////        if(comptype.equalsIgnoreCase(Process.TYPE))
////        {
////            socialDTO = SocialAdminUtil.getProcess(sysId, owner);
////        }
////        else if(comptype.equalsIgnoreCase(Team.TYPE))
////        {
////            socialDTO = SocialAdminUtil.getTeam(sysId, owner);            
////        }
////        else if(comptype.equalsIgnoreCase(Forum.TYPE))
////        {
////            socialDTO = SocialAdminUtil.getForum(sysId, owner);
////        }
////
////        //send the Post
////        SocialGenericActionsUtil.sendRejectConfirmPostForUser(AdvanceTree.PROCESS, ownerVO, requestedUser, socialDTO);
//    }
    
    
    public static void unfollowComponent(String fromCompId, String toCompId) throws Exception
    {
        Node userNode = null;
        Node compNode = null;

        if (StringUtils.isNotEmpty(fromCompId))
        {
            userNode = ResolveGraphFactory.findNodeByIndexedID(fromCompId);

            if (StringUtils.isNotEmpty(toCompId))
            {
                compNode = ResolveGraphFactory.findNodeByIndexedID(toCompId);

                if (userNode != null && compNode != null)
                {
                    //any one of the relation is present at any point of time
                    ResolveGraphFactory.deleteRelation(userNode, compNode, RelationType.MEMBER);//for containers
                    ResolveGraphFactory.deleteRelation(userNode, compNode, RelationType.FOLLOWER);//for components
                    
                    //remove it from the Home also
                    ResolveGraphFactory.deleteRelation(userNode, compNode, RelationType.FAVORITE);
                }
            }
        }
    }
    
    public static void followComponent(String compId, String followingCompId) throws Exception
    {
        Node compNode = null;
        Node followingCompNode = null;

        if (StringUtils.isNotEmpty(compId))
        {
            compNode = ResolveGraphFactory.findNodeByIndexedID(compId);
            if (StringUtils.isNotEmpty(followingCompId))
            {
                followingCompNode = ResolveGraphFactory.findNodeByIndexedID(followingCompId);

//                if (compNode != null && followingCompNode != null)
//                {
//                    String type = (String) followingCompNode.getProperty(RSComponent.TYPE);
//                    if(type.equalsIgnoreCase(SocialRelationshipTypes.PROCESS.name()) 
//                                    || type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()) 
//                                    || type.equalsIgnoreCase(SocialRelationshipTypes.FORUM.name()))
//                    {
//                        ResolveGraphFactory.addRelation(compNode, followingCompNode, RelationType.MEMBER);
//                    }
//                    else
//                    {
//                        ResolveGraphFactory.addRelation(compNode, followingCompNode, RelationType.FOLLOWER);
//                    }
//                    
////                    ResolveGraphFactory.addRelation(compNode, followingCompNode, RelationType.FAVORITE);
//                }
            }
        }
    }
    
}
