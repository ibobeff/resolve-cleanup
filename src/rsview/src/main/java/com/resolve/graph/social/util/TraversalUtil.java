/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import com.resolve.graph.ResolveRelationshipType;
import com.resolve.graph.catalogbuilder.service.CatalogContainerType;
import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.util.Log;

public class TraversalUtil
{
    public static Iterable<Node> findProcessThatThisForumBelongsTo(Node forumNode)
    {
        //prepare the traversal to get the process nodes
        TraversalDescription traversalForumForAllProcesses =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.OUTGOING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a PROCESS node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.PROCESS.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalForumForAllProcesses.traverse(forumNode).nodes();
    }
    
    public static Iterable<Node> findProcessThatThisTeamBelongsTo(Node teamNode)
    {
        //prepare the traversal to get the process nodes
        TraversalDescription traversalTeamForAllProcesses =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.OUTGOING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a PROCESS node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.PROCESS.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForAllProcesses.traverse(teamNode).nodes();
    }
    
    public static Iterable<Node> findTeamsThatThisTeamIsMemberOf(Node teamNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.OUTGOING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a TEAM node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(teamNode).nodes();
    }
    
    public static Iterable<Node> findTeamsBelongingToTeam(Node teamNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a TEAM node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(teamNode).nodes();
    }
    
    public static Iterable<Node> findMembersBelongingToProcess(Node processNode)
    {
        TraversalDescription traversalProcessForAllItsMembers =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1));
                        
        return traversalProcessForAllItsMembers.traverse(processNode).nodes();
        
    }
    
    public static Iterable<Node> findTeamsBelongingToProcess(Node processNode)
    {
        TraversalDescription traversalProcessForAllItsMembers =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator(new Evaluator()
                        {
                            public Evaluation evaluate(Path path)
                            {
                                Node node = path.endNode();
                                if(node.hasProperty(SocialFactory.NODE_TYPE))
                                {
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                    
                                    //if its a TEAM, than include it
                                    if (type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                }
                                
                                return Evaluation.EXCLUDE_AND_CONTINUE;
                            }
                        });
                        
        return traversalProcessForAllItsMembers.traverse(processNode).nodes();
        
    }
    
    public static Iterable<Node> findDocumentsBelongingToNamespace(Node namespaceNode)
    {
        TraversalDescription traversalNSForAllDocuments =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.NAMESPACE, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator(new Evaluator()
                        {
                            public Evaluation evaluate(Path path)
                            {
                                Node node = path.endNode();
                                if(node.hasProperty(SocialFactory.NODE_TYPE))
                                {
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                    
                                    //if its a DOCUMENT, than include it
                                    if (type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                }
                                
                                return Evaluation.EXCLUDE_AND_CONTINUE;
                            }
                        });
        
        
        return traversalNSForAllDocuments.traverse(namespaceNode).nodes();
    }
    
    public static Iterable<Node> findUsersBelongingToThisProcess(Node processNode)
    {
        TraversalDescription traversalUsersForAllProcesses = Traversal.description().breadthFirst()
                        .relationships(RelationType.MEMBER, Direction.INCOMING)
                        .uniqueness(Uniqueness.NODE_GLOBAL)
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator(new Evaluator()
                        {
                            public Evaluation evaluate(Path path)
                            {
                                Node node = path.endNode();
                                String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                Log.log.debug("types for Process: " + type);
                                
                                //if its a PROCESS node, than include it, else ignore
                                if (type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                {
                                    Log.log.debug("type should be USER : " + type);
                                    return Evaluation.INCLUDE_AND_CONTINUE;
                                }
                                else
                                {
                                    return Evaluation.EXCLUDE_AND_CONTINUE;
                                }
                            }
                        });
        
        
        return traversalUsersForAllProcesses.traverse(processNode).nodes();
    }
    
    public static Iterable<Node> findAllCompsBelongingToProcessExcludingUsers(Node processNode)
    {
        TraversalDescription traversalUsersForAllProcesses =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator(new Evaluator()
                        {
                            public Evaluation evaluate(Path path)
                            {
                                Node node = path.endNode();
                                String type = (String) node.getProperty(SocialFactory.NODE_TYPE);

                                //if its a PROCESS node, than include it, else ignore
                                if (type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                {
                                    Log.log.debug("type should be ANYTHING ELSE EXCEPT USER : " + type);
                                    return Evaluation.EXCLUDE_AND_CONTINUE;
                                }
                                else
                                {
                                    return Evaluation.INCLUDE_AND_CONTINUE;
                                }
                            }
                        });
        
        
        return traversalUsersForAllProcesses.traverse(processNode).nodes();
    }
    
    public static Iterable<Node> findUsersBelongingToTeam(Node teamNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a USER node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(teamNode).nodes();
    }
    
    public static Iterable<Node> findUsersBelongingToForum(Node forumNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a USER node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(forumNode).nodes();
    }
    
    public static Iterable<Node> findUsersDirectOrIndirectMembersOrFollowerOf(Node compNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
//                        .relationships(RelationType.INDIRECT_FOLLOW, Direction.INCOMING)
                        .relationships(RelationType.FOLLOWER, Direction.INCOMING)
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a TEAM node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(compNode).nodes();
    }
    
    public static Iterable<Node> findUsersDirectMembersOrFollowerOf(Node compNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .relationships(RelationType.FOLLOWER, Direction.INCOMING)
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
                                    
                                    //if its a TEAM node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(compNode).nodes();
    }
    
    public static Iterable<Node> findUsersThatThisUserIsFollowing(Node userNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.FOLLOWER, Direction.OUTGOING)
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
//                                    String name = (String) node.getProperty(RSComponent.DISPLAYNAME);
//                                    Log.log.debug("type : " + type + " name:" + name);
                                    
                                    //if its a TEAM node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(userNode).nodes();
    }
   
    
    public static Iterable<Node> findTagsFor(Node compNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(ResolveRelationshipType.ResolveTag)
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
//                                    String name = (String) node.getProperty(RSComponent.DISPLAYNAME);
//                                    Log.log.debug("type : " + type + " name:" + name);
                                    
                                    //if its a TEAM node, than include it, else ignore
//                                    if(type.equalsIgnoreCase(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                                    {
//                                        return Evaluation.INCLUDE_AND_CONTINUE;
//                                    }
//                                    else
//                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
//                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(compNode).nodes();
    }
    
    public static Iterable<Node> findCatalogNodes(Node catalogRootNode)
    {
        TraversalDescription traversalForCatalogNodes = 
                        Traversal.description().breadthFirst()
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .relationships(SocialRelationshipTypes.CHILD_OF_CATALOG, Direction.INCOMING)
                        .relationships(SocialRelationshipTypes.CATALOG_REF, Direction.INCOMING)
//                        .relationships(CatalogRelationshipType.TAG_OF_CAT, Direction.INCOMING)
                        .evaluator(Evaluators.excludeStartPosition())
                        .evaluator( new Evaluator()
                        {
                                @Override
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();        
                                    String type = "type";//(String) node.getProperty(ResolveCatalog.TYPE);
                                    if (type.equalsIgnoreCase(SocialRelationshipTypes.CatalogReference.name()) 
                                                    || type.equalsIgnoreCase(SocialRelationshipTypes.CatalogGroup.name()) 
                                                    || type.equalsIgnoreCase(SocialRelationshipTypes.CatalogFolder.name()) 
                                                    || type.equalsIgnoreCase(SocialRelationshipTypes.CatalogItem.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }  
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                    
//                                    if (path.lastRelationship() !=null && path.lastRelationship().getType().equals(CatalogRelationshipType.CATALOG_REF))
//                                    {
//                                        return Evaluation.EXCLUDE_AND_PRUNE;
//                                    }
//                                    
//                                    return Evaluation.INCLUDE_AND_CONTINUE;
                                    
                                }
                            } );
        
        return traversalForCatalogNodes.traverse(catalogRootNode).nodes();
    }
    
    public static Iterable<Node> findAllRootCatalogs() throws Exception
    {
        Node catalogContainer = null;//CatalogFactory.getContainerNode(CatalogContainerType.Catalog);
        if(catalogContainer == null)
        {
            throw new Exception("CatalogContainer root node is not available. Please contact the administrator.");
        }
        
        TraversalDescription trav = Traversal.description()
                                        .breadthFirst()
                                        .relationships(CatalogContainerType.Catalog, Direction.OUTGOING)
                                        .evaluator(Evaluators.excludeStartPosition());
        
        return trav.traverse(catalogContainer).nodes();
        
    }

    
}
