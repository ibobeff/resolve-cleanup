/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.graph.social.util.legacy.SocialUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.component.tree.ActionTaskTree;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.graph.social.model.component.tree.DecisionTreeTree;
import com.resolve.services.graph.social.model.component.tree.DocumentTree;
import com.resolve.services.graph.social.model.component.tree.ForumTree;
import com.resolve.services.graph.social.model.component.tree.NameSpaceTree;
import com.resolve.services.graph.social.model.component.tree.ProcessTree;
import com.resolve.services.graph.social.model.component.tree.RssTree;
import com.resolve.services.graph.social.model.component.tree.RunbookTree;
import com.resolve.services.graph.social.model.component.tree.TeamTree;
import com.resolve.services.graph.social.model.component.tree.UserTree;
import com.resolve.services.graph.social.model.component.tree.WorkSheetTree;
import com.resolve.services.graph.social.model.message.NotificationTypeEnum;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class GetSocial
{

    public static SocialUsersFactory socialUsersFactory = new SocialUsersFactory();
    public static SocialForumFactory socialForumFactory = new SocialForumFactory();
    public static SocialTeamFactory socialTeamFactory = new SocialTeamFactory();
    public static SocialProcessFactory socialProcessFactory = new SocialProcessFactory();
    public static SocialActionTaskFactory socialActionTaskFactory = new SocialActionTaskFactory();
    public static SocialDocumentFactory socialDocumentFactory = new SocialDocumentFactory();
    public static SocialRssFactory socialRssFactory = new SocialRssFactory();
    public static SocialGlobalNotificationFactory sgNotificationFactory = new SocialGlobalNotificationFactory();
    public static final String NOTIFICATION_TYPE = "NOTIFICATION";

    //public static Analyzer analyzer = new StandardAnalyzer(IndexUtil.VERSION);
//
//    //get social tree
//    public static AdvanceTree getAdvanceTree(User user)
//    {
//        AdvanceTree advanceTree = new AdvanceTree();
//        advanceTree.setOwner(user);
//
//        //the key is the node id
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//        Map<String, String> forumsMap = new ConcurrentHashMap<String, String>();
//
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Team> teams = new ArrayList<Team>();
//        Collection<Forum> forums = new ArrayList<Forum>();
//        Collection<Namespace> namespaces = new ArrayList<Namespace>();
//        Collection<User> users = new ArrayList<User>();
//        Collection<ActionTask> actiontasks = new ArrayList<ActionTask>();
//        Collection<Runbook> runbooks = new ArrayList<Runbook>();
//        Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//        Collection<Document> documents = new ArrayList<Document>();
//        Collection<Rss> rsss = new ArrayList<Rss>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            Node endNode = rel.getEndNode();
//                            BuildOneDepthTree.buildOneDepthTree(endNode, users, actiontasks, runbooks, dts, documents, rsss, forums, namespaces, 1);
//                        }
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//
//                    if(memberRels != null)
//                    {
//                        for(Relationship member: memberRels)
//                        {
//                            Node endNode = member.getEndNode();
//
//                            //make sure this post is not locked by the user
////                            boolean locked = SocialFactory.findRelation(userNode, endNode, RelationType.LOCK);
//                            Boolean locked = false;
//                            try
//                            {
//                                if(endNode.hasProperty(RSComponent.LOCK))
//                                    locked = (Boolean) endNode.getProperty(RSComponent.LOCK);
//                            }
//                            catch (Exception e)
//                            {
//                                locked = false;
//                            }
//
//                            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcess(endNode);
//                                    process.setLock(locked);
//                                    process.setIsFollowing(true);
//                                    processes.add(process);
//                                    processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                                }
//                            }
//                            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//                            {
//                                Collection<Node> forumRootNodes = new ArrayList<Node>();
//                                TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//
//                                if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                                {
//                                    for(Node forumRootNode: forumRootNodes)
//                                    {
//                                        if(forumRootNode != null)
//                                        {
//                                            if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                            {
//                                                if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                                                {
//                                                    Process process = TraverseGraph.getProcess(forumRootNode);
//                                                    process.setLock(locked);
//                                                    processes.add(process);
//                                                    processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                                else
//                                {
//                                    if(!forumsMap.containsKey(Long.toString(endNode.getId())))
//                                    {
//                                        Forum forum = TraverseGraph.getForum(endNode);
//                                        forum.setLock(locked);
//                                        forum.setIsFollowing(true);
//                                        forums.add(forum);
//                                        forumsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                                    }
//                                }
//                            }
//                            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                            {
//                                Collection<Node> teamRootNodes = new ArrayList<Node>();
//                                TraverseGraph.getRootNodes(endNode, teamRootNodes);
//
//                                if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                                {
//                                    for(Node teamRootNode: teamRootNodes)
//                                    {
//                                        if(teamRootNode != null)
//                                        {
//                                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                            {
//
//                                                if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                                {
//                                                    Process process = TraverseGraph.getProcess(teamRootNode);
//                                                    processes.add(process);
//                                                    processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(endNode.getId()));
//                                                }
//                                            }
//                                            else
//                                            {
//                                                if(!teamsMap.containsKey(Long.toString(teamRootNode.getId())))
//                                                {
//                                                    Team team = TraverseGraph.getTeam(teamRootNode);
//                                                    teams.add(team);
//                                                    teamsMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                                else
//                                {
//                                    if(!teamsMap.containsKey(Long.toString(endNode.getId())))
//                                    {
//                                        Team team = TraverseGraph.getTeam(endNode);
//                                        team.setLock(locked);
//                                        team.setIsFollowing(true);
//                                        teams.add(team);
//                                        teamsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getAdvanceTree catch exception: " + t,t);
//        }
//
//        advanceTree.setProcesses(processes);
//        mergeProcessTeams(teams, processes);
//        advanceTree.setTeams(teams);
//
//        margeProcessForums(forums, processes);
//        advanceTree.setForums(forums);
//
//        margeProcessNamespaces(namespaces, processes);
//        advanceTree.setNamespaces(namespaces);
//
//        //set us
//        advanceTree.setUsers(users);
//
//        //set actiontask
////        mergeProcessToActionTasks(actiontasks, processes, new HashSet<String>(), true);
////        List<ActionTask> sortedActionTasks = new ArrayList<ActionTask>(actiontasks);
////        sortOnDisplayName(sortedActionTasks);
////        advanceTree.setActionTasks(sortedActionTasks);
//        advanceTree.setActionTasks(actiontasks);
//
//        //set runbook
////        mergeProcessToRunbook(runbooks, processes, new HashSet<String>(), true);
////        List<Runbook> sortedRunbook = new ArrayList<Runbook>(runbooks);
////        sortOnDisplayName(sortedRunbook);
////        advanceTree.setRunbooks(sortedRunbook);
//        advanceTree.setRunbooks(runbooks);
//
//        advanceTree.setDecisionTrees(dts);
//
//        //set Document
////        mergeProcessToDocument(documents, processes, new HashSet<String>(), true);
////        List<Document> sortedDocument = new ArrayList<Document>(documents);
////        sortOnDisplayName(sortedDocument);
////        advanceTree.setDocuments(sortedDocument);
//        advanceTree.setDocuments(documents);
//
//        advanceTree.setRsss(rsss);
//        advanceTree.setOwner(user);
//
//        return advanceTree;
//    }
//
//
////    public static List<RSComponent> getUserStreams(User user)
////    {
//////        return getUserStreams_OLD(user);
////        return new ArrayList<RSComponent>(new SearchUserStreamsUtil(user).getUserStreams());
////    }
//
//    public static void mergeProcessTeams(Collection<Team> teams, Collection<Process> processes)
//    {
//        try
//        {
//            if(processes != null)
//            {
//                Set<String> rootTeamNames = new HashSet<String>();
//
//                if(teams != null)
//                {
//                    for(Team teamLocal: teams)
//                    {
//                        rootTeamNames.add(teamLocal.getSys_id());
//                    }
//                }
//
//                for(Process processLocal: processes)
//                {
//                    Collection<Team> procTeams = processLocal.getTeams();
//
//                    if(processes != null)
//                    {
//                        for(Team procTeam: procTeams)
//                        {
//                            if(!rootTeamNames.contains(procTeam.getSys_id()))
//                            {
//                                teams.add(procTeam);
//                                rootTeamNames.add(procTeam.getSys_id());
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("mergeProcessTeams catched exception: " + t.getMessage(), t);
//        }
//    }
//
//    public static void margeProcessForums(Collection<Forum> forums, Collection<Process> processes)
//    {
//        try
//        {
//            if(processes != null)
//            {
//                Set<String> forumNames = new HashSet<String>();
//
//                if(forums != null)
//                {
//                    for(Forum forumLocal: forums)
//                    {
//                        forumNames.add(forumLocal.getSys_id());
//                    }
//                }
//
//                for(Process process: processes)
//                {
//                    Collection<Forum> procForums =  process.getForums();
//
//                    if(procForums != null)
//                    {
//                        for(Forum procForumsLocal: procForums)
//                        {
//                            if(!forumNames.contains(procForumsLocal.getSys_id()))
//                            {
//                                forums.add(procForumsLocal);
//                                forumNames.add(procForumsLocal.getSys_id());
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("margeProcessForums catched exception: " + t.getMessage(), t);
//        }
//    }
//
//    public static void margeProcessNamespaces(Collection<Namespace> namespaces, Collection<Process> processes)
//    {
//        try
//        {
//            if(processes != null)
//            {
//                Set<String> namespacesNames = new HashSet<String>();
//
//                if(namespaces != null)
//                {
//                    for(Namespace namespaceLocal: namespaces)
//                    {
//                        namespacesNames.add(namespaceLocal.getSys_id());
//                    }
//                }
//
//                for(Process process: processes)
//                {
//                    Collection<Namespace> procNamespaces =  process.getNameSpaces();
//
//                    if(procNamespaces != null)
//                    {
//                        for(Namespace procNamespaceocal: procNamespaces)
//                        {
//                            if(!namespacesNames.contains(procNamespaceocal.getSys_id()))
//                            {
//                                namespaces.add(procNamespaceocal);
//                                namespacesNames.add(procNamespaceocal.getSys_id());
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("margeProcessNamespaces catched exception: " + t.getMessage(), t);
//        }
//    }
//
//    //
//    public static void addToTeams(Collection<Team> teams, Team team)
//    {
//        if(teams != null)
//        {
//            if(teams.isEmpty())
//            {
//                teams.add(team);
//            }
//            else
//            {
//                Team findTeam = null;
//
//                for(Team teamLocal: teams)
//                {
//                    if(teamLocal.getSys_id().equals(team.getSys_id()))
//                    {
//                        findTeam = teamLocal;
//                        mergeTeam(teamLocal, team);
//                    }
//                }
//
//                if(findTeam == null)
//                {
//                    teams.add(team);
//                }
//            }
//        }
//    }
//
//    public static void mergeTeam(Team findTeam, Team theTeam)
//    {
//        Collection<Team> findTeams = findTeam.getTeams();
//        Collection<Team> theTeams = theTeam.getTeams();
//
//        if(theTeams != null && !theTeams.isEmpty() && findTeams != null && !findTeams.isEmpty())
//        {
//            for(Team theTeamLocal: theTeams)
//            {
//                Team findTeamLocal = findTeam(findTeams, theTeamLocal);
//
//                if(findTeamLocal == null)
//                {
//                    findTeams.add(theTeamLocal);
//                }
//                else
//                {
//                    mergeTeam(findTeamLocal, theTeamLocal);
//                }
//            }
//        }
//        else if(findTeams == null || findTeams.isEmpty())
//        {
//            findTeam.setTeams(theTeams);
//        }
//    }
//
//    public static Team findTeam(Collection<Team> findTeams, Team targetTeam)
//    {
//        Team findTeam = null;
//
//        if(findTeams != null && !findTeams.isEmpty())
//        {
//            for(Team teamLocal: findTeams)
//            {
//                if(teamLocal.getSys_id().equals(targetTeam.getSys_id()))
//                {
//                    findTeam = teamLocal;
//                }
//            }
//        }
//
//        return findTeam;
//    }
//
//    public static ProcessTree getProcessTree(User user)
//    {
//        ProcessTree processTree = new ProcessTree();
//        processTree.setOwner(user);
//
//        Set<String> processIDSet = new HashSet<String>();
//        Collection<Process> processes = new ArrayList<Process>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//
//                    for(Relationship member: memberRels)
//                    {
//                        Node endNode = member.getEndNode();
//
//                        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                        {
//                            if(!processIDSet.contains(Long.toString(endNode.getId())))
//                            {
//                                Process process = new Process();
//                                process.convertNodeToObject(endNode);
//                                processes.add(process);
//                                processIDSet.add(Long.toString(endNode.getId()));
//                            }
//                        }
//                        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//                        {
//                            Collection<Node> forumRootNodes = new ArrayList<Node>();
//                            TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//
//                            if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                            {
//                                for(Node forumRootNode: forumRootNodes)
//                                {
//                                    if(forumRootNode != null)
//                                    {
//                                        if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                        {
//                                            if(!processIDSet.contains(Long.toString(forumRootNode.getId())))
//                                            {
//                                                //Process process = TraverseGraph.getProcessForForumTab(forumRootNode);
//                                                Process process = TraverseGraph.getProcess(forumRootNode);
//                                                processes.add(process);
//                                                processIDSet.add(Long.toString(forumRootNode.getId()));
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                        {
//                            Collection<Node> teamRootNodes = new ArrayList<Node>();
//                            TraverseGraph.getRootNodes(endNode, teamRootNodes);
//
//                            if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                            {
//                                for(Node teamRootNode: teamRootNodes)
//                                {
//                                    if(teamRootNode != null)
//                                    {
//                                        if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                        {
//                                            if(!processIDSet.contains(Long.toString(teamRootNode.getId())))
//                                            {
//                                                Process process = new Process();
//                                                process.convertNodeToObject(teamRootNode);
//                                                processes.add(process);
//                                                processIDSet.add(Long.toString(teamRootNode.getId()));
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getProcessTree catched exception: " + t);
//        }
//
//        processTree.setProcesses(processes);
//
//        List<Process> sortedProcesses = new ArrayList<Process>();
//        sortedProcesses.addAll(processes);
//        sortOnDisplayName(sortedProcesses);
//
//        return processTree;
//    }
//
//    public static TeamTree getTeamTree(User user)
//    {
//        TeamTree teamTree = new TeamTree();
//        teamTree.setOwner(user);
//
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Team> teams = new ArrayList<Team>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//
//                    for(Relationship member: memberRels)
//                    {
//                        Node endNode = member.getEndNode();
//
//                        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                        {
//                            Collection<Node> teamRootNodes = new ArrayList<Node>();
//                            TraverseGraph.getRootNodes(endNode, teamRootNodes);
//
//                            if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                            {
//                                for(Node teamRootNode: teamRootNodes)
//                                {
//                                    if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                    {
//                                        if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                        {
//                                            Process process = TraverseGraph.getProcess(endNode);
//                                            processes.add(process);
//                                            processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(endNode.getId()));
//                                        }
//                                    }
//                                    else
//                                    {
//                                        if(!teamsMap.containsKey(Long.toString(teamRootNode.getId())))
//                                        {
//                                            Team team = TraverseGraph.getTeam(teamRootNode);
//                                            teams.add(team);
//                                            teamsMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                        }
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                if(!teamsMap.containsKey(Long.toString(endNode.getId())))
//                                {
//                                    Team team = TraverseGraph.getTeam(endNode);
//                                    teams.add(team);
//                                    teamsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                                }
//                            }
//                        }
//                        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                        {
//                            if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                            {
//                                Process process = TraverseGraph.getProcess(endNode);
//                                processes.add(process);
//                                processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                            }
//                        }
//                        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//                        {
//                            Collection<Node> forumRootNodes = new ArrayList<Node>();
//                            TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//
//                            if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                            {
//                                for(Node forumRootNode: forumRootNodes)
//                                {
//                                    if(forumRootNode != null)
//                                    {
//                                        if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                        {
//                                            if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                                            {
//                                                Process process = TraverseGraph.getProcess(forumRootNode);
//                                                processes.add(process);
//                                                processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getTeamTree catched exception: " + t);
//        }
//
//        //need build teams without hierarchy
//        //teamTree.setProcesses(processes);
//        mergeProcessTeams(teams, processes);
//
//        Collection<Team> oneLevelTeams = new ArrayList<Team>();
//        Set<String> teamSysID = new HashSet<String>();
//
//        mergeTeamsInOneLevel(teams, oneLevelTeams, teamSysID);
//
//        List<Team> sortedTeams = new ArrayList<Team>();
//        sortedTeams.addAll(oneLevelTeams);
//        sortOnDisplayName(sortedTeams);
//
//        teamTree.setTeams(sortedTeams);
//
//        return teamTree;
//    }
//
//    public static void sortOnDisplayName(List<? extends RSComponent> comps)
//    {
//        if(!comps.isEmpty())
//        {
//            Collections.sort(comps, new Comparator<RSComponent>()
//            {
//                public int compare(final RSComponent comp1, final RSComponent comp2)
//                {
//                    return comp1.getDisplayName().compareTo(comp2.getDisplayName());
//                }
//            });
//        }
//    }
//
//    protected static void mergeTeamsInOneLevel(Collection<Team> teams, Collection<Team> oneLevelTeams, Set<String> teamIDs)
//    {
//        if(teams != null)
//        {
//            for(Team team: teams)
//            {
//                if(!teamIDs.contains(team.getSys_id()))
//                {
//                    Collection<Team> teamsLocal = team.getTeams();
//                    team.setTeams(null);
//                    team.setUsers(null);
//                    oneLevelTeams.add(team);
//
//                    mergeTeamsInOneLevel(teamsLocal, oneLevelTeams, teamIDs);
//                    teamIDs.add(team.getSys_id());
//                }
//            }
//        }
//    }
//
//    public static List<Team> getUserDirectMemberOfTeams(User user)
//    {
//        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//        List<Team> teams = new ArrayList<Team>();
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.MEMBER);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//
//                        if(endNode != null && endNode.getProperty(SocialFactory.NODE_TYPE) != null
//                                 && endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                        {
//                            if(!teamsMap.containsKey(Long.toString(endNode.getId())))
//                            {
//                                Team team = new Team();
//                                team.convertNodeToObject(endNode);
//                                teamsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                                teams.add(team);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getUserDirectMemberOfTeams catched exception: " + t.getMessage() + t);
//        }
//
//        return teams;
//    }
//
//    public static TeamTree getTeamTreePost(User user)
//    {
//
//        Set<String> postPathTeams = new HashSet<String>();
//        Set<String> postPathTeamsToRootTeam = new HashSet<String>();
//        TeamTree teamTree = new TeamTree();
//        teamTree.setOwner(user);
//
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        Map<String, String> teamMap = new ConcurrentHashMap<String, String>();
//
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Team> teams = new ArrayList<Team>();
//
//        Node userNode = null;
//
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//            if(userNode != null)
//            {
//                Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//
//                for(Relationship member: memberRels)
//                {
//                    Node endNode = member.getEndNode();
//
//                    if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                    {
//                        Collection<Node> teamRootNodes = new ArrayList<Node>();
//                        TraverseGraph.getRootNodes(endNode, teamRootNodes);
//
//                        if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                        {
//                            for(Node teamRootNode: teamRootNodes)
//                            {
//                                if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                {
//                                     if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                     {
//                                         //the toRootPath is from this team endNode to the rootNode (the root node is process)
//                                         //Set<String> currentPostPathTeams = TraverseGraph.getToRootPathTeams(endNode);
//
////                                         if(currentPostPathTeams != null)
////                                         {
////                                             postPathTeams.addAll(currentPostPathTeams);
////                                         }
//
//                                         Process process = TraverseGraph.getProcess(teamRootNode); //TraverseGraph.getProcessForTeamTab(teamRootNode);
//                                         processes.add(process);
//                                         processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                    }
//                                }
//                                else if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                                {
//                                    if(!teamMap.containsKey(Long.toString(teamRootNode.getId())))
//                                    {
//                                        //Set<String> currentPostPathTeamsLocal = TraverseGraph.getToRootPathTeams(endNode);
//
////                                        if(currentPostPathTeamsLocal != null)
////                                        {
////                                            postPathTeamsToRootTeam.addAll(currentPostPathTeamsLocal);
////                                        }
//
//                                        Team team = TraverseGraph.getTeam(teamRootNode);
//                                        teams.add(team);
//                                        teamMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                    }
////                                    else
////                                    {
////                                        postPathTeamsToRootTeam.add((String) endNode.getProperty(RSComponent.SYS_ID));
////                                    }
//                                }
//                            }
//                        }
//                        else
//                        {
//                            if(!teamMap.containsKey(Long.toString(endNode.getId())))
//                            {
////                                Set<String> currentPostPathTeamsLocal = TraverseGraph.getToRootPathTeams(endNode);
////
////                                if(currentPostPathTeamsLocal != null)
////                                {
////                                    postPathTeamsToRootTeam.addAll(currentPostPathTeamsLocal);
////                                }
//
//                                Team team = TraverseGraph.getTeam(endNode);
//                                teams.add(team);
//                                teamMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                            }
////                            else
////                            {
////                                postPathTeamsToRootTeam.add((String) endNode.getProperty(RSComponent.SYS_ID));
////                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        teamTree.setProcesses(processes);
//        mergeProcessTeams(teams, processes);
//        teamTree.setTeams(teams);
//        teamTree.setPostPathTeamsToRootProcess(postPathTeams);
//        teamTree.setPostPathTeamsToRootTeams(postPathTeamsToRootTeam);
//
//        return teamTree;
//    }
//
//    public static UserTree getUserTree(User user)
//    {
//        UserTree userTree = new UserTree();
//        Collection<User> users = new ArrayList<User>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthUserTree(endNode, users, 1);
//                    }
//                }
//
//                userTree = new UserTree();
//
//                List<User> usersList = new ArrayList<User>(users);
//                sortOnDisplayName(usersList);
//                userTree.setUsers(usersList);
//
//                userTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getUserTree catched exception: " + t);
//        }
//
//        return userTree;
//    }
//
//    public static ActionTaskTree getActionTaskTree(User user, boolean withMember)
//    {
//        ActionTaskTree actionTaskTree = new ActionTaskTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<ActionTask> actiontasks = new ArrayList<ActionTask>();
//        Set<String> actiontaskID = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthActionTaskTree(endNode, actiontasks, 1, actiontaskID);
//                    }
//
//                    if(withMember)
//                    {
//                        Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                        TraverseGraph.buildProcessesForActionTaskTab(memberRels, processes);
//                    }
//                }
//
//                if(withMember)
//                {
//                    //actionTaskTree.setProcesses(processes);
//                    mergeProcessToActionTasks(actiontasks, processes, actiontaskID, false);
//                }
//
//                List<ActionTask> sortedActionTasks = new ArrayList<ActionTask>(actiontasks);
//                sortOnDisplayName(sortedActionTasks);
//                actionTaskTree.setActionTasks(sortedActionTasks);
//                actionTaskTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getActionTaskTree catched exception: " + t);
//        }
//
//        return actionTaskTree;
//    }
//
//    /**
//     * Only merge one user from process level.
//     *
//     */
//    public static void mergeProcessToUsers(Collection<User> users,
//                                           Collection<Process> processes,
//                                           Set<String> usersID,
//                                           boolean recalculateUsersIDS)
//    {
//        if(processes != null)
//        {
//            if(users != null)
//            {
//                for(User userLocal: users)
//                {
//                    String userLocalSysID = userLocal.getSys_id();
//
//                    if(StringUtils.isNotEmpty(userLocalSysID))
//                    {
//                        usersID.add(userLocalSysID);
//                    }
//                }
//            }
//
//            for(Process process: processes)
//            {
//                Collection<User> usersFromProcess = process.getUsers();
//
//                if(usersFromProcess != null)
//                {
//                    for(User user: usersFromProcess)
//                    {
//                        user.addProcessNames(process.getDisplayName());
//
//                        if(!usersID.contains(user.getSys_id()))
//                        {
//                            users.add(user);
//                            usersID.add(user.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public static void mergeProcessToActionTasks(Collection<ActionTask> actiontasks,
//                                                 Collection<Process> processes,
//                                                 Set<String> actiontaskID,
//                                                 boolean recalculateActionTaskIDS)
//    {
//        if(processes != null)
//        {
//
//            if(recalculateActionTaskIDS)
//            {
//                if(actiontasks != null)
//                {
//                    for(ActionTask actLocal: actiontasks)
//                    {
//                        String actLocalID = actLocal.getSys_id();
//
//                        if(StringUtils.isNotEmpty(actLocalID))
//                        {
//                            actiontaskID.add(actLocalID);
//                        }
//                    }
//                }
//            }
//
//
//            for(Process process: processes)
//            {
//                Collection<ActionTask> ats = process.getActiontasks();
//
//                if(ats != null)
//                {
//                    for(ActionTask at: ats)
//                    {
//                        at.addProcessNames(process.getDisplayName());
//
//                        if(!actiontaskID.contains(at.getSys_id()))
//                        {
//                            actiontasks.add(at);
//                            actiontaskID.add(at.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public static ActionTaskTree getActionTaskTreeWithPath(User user)
//    {
//        ActionTaskTree actionTaskTree = new ActionTaskTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<ActionTask> actiontasks = new ArrayList<ActionTask>();
//        Set<String> actionTaskIDs = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthActionTaskTree(endNode, actiontasks, 1, actionTaskIDs);
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForActionTaskTab(memberRels, processes);
//                }
//
//                actionTaskTree.setProcesses(processes);
//                actionTaskTree.setActionTasks(actiontasks);
//                actionTaskTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getActionTaskTree catched exception: " + t);
//        }
//
//        return actionTaskTree;
//    }
//
//    public static WorkSheetTree getWorkSheetTree(User user)
//    {
//        WorkSheetTree worksheetTree = new WorkSheetTree();
//        Collection<Worksheet> worksheets = new ArrayList<Worksheet>();
//        Set<String> worksheetID = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthWorkSheetTree(endNode, worksheets, 1, worksheetID);
//                    }
//                }
//
//                List<Worksheet> sortedWorkSheets = new ArrayList<Worksheet>(worksheets);
//                sortOnDisplayName(sortedWorkSheets);
//                worksheetTree.setWorkSheets(sortedWorkSheets);
//                worksheetTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getWorkSheetTree catched exception: " + t, t);
//        }
//
//        return worksheetTree;
//    }
//
//    public static RunbookTree getRunbookTree(User user, boolean withMember)
//    {
//        RunbookTree runbookTree = new RunbookTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Runbook> runbooks = new ArrayList<Runbook>();
//        Set<String> runbookIDs = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        String nodeName = (String) endNode.getProperty("displayName");
//                        BuildOneDepthTree.buildOneDepthRunbookTree(endNode, runbooks, 1, runbookIDs);
//                    }
//
//                    if(withMember)
//                    {
//                        Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                        TraverseGraph.buildProcessesForRunbookTab(memberRels, processes);
//                    }
//                }
//
//                if(withMember)
//                {
//                    //runbookTree.setProcesses(processes);
//                    mergeProcessToRunbook(runbooks, processes, runbookIDs, false);
//                }
//
//                List<Runbook> sortedRunbook = new ArrayList<Runbook>(runbooks);
//                sortOnDisplayName(sortedRunbook);
//
//                runbookTree.setRunbooks(sortedRunbook);
//                runbookTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getRunbookTree catched exception: " + t);
//        }
//
//
//        return runbookTree;
//    }
//
//
//    public static void mergeProcessToRunbook(Collection<Runbook> runbooks,
//                                             Collection<Process> processes,
//                                             Set<String> runbookIDs,
//                                             boolean recalculateRunbooksIDs)
//    {
//        if(processes != null)
//        {
//            if(runbooks != null)
//            {
//                for(Runbook runbookLocal: runbooks)
//                {
//                    String runbookLocalSysID = runbookLocal.getSys_id();
//
//                    if(StringUtils.isNotEmpty(runbookLocalSysID))
//                    {
//                        runbookIDs.add(runbookLocalSysID);
//                    }
//                }
//            }
//
//            for(Process process: processes)
//            {
//                Collection<Runbook> runbooksLocal = process.getRunbooks();
//
//                if(runbooksLocal != null && !runbooksLocal.isEmpty())
//                {
//                    for(Runbook runbook: runbooksLocal)
//                    {
//                        runbook.addProcessNames(process.getDisplayName());
//
//                        if(!runbookIDs.contains(runbook.getSys_id()))
//                        {
//                            runbooks.add(runbook);
//                            runbookIDs.add(runbook.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//
//    public static RunbookTree getRunbookTreeWithPath(User user)
//    {
//        RunbookTree runbookTree = new RunbookTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Runbook> runbooks = new ArrayList<Runbook>();
//        Set<String> runbookIDs = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthRunbookTree(endNode, runbooks, 1, runbookIDs);
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForRunbookTab(memberRels, processes);
//                }
//
//                runbookTree.setProcesses(processes);
//                runbookTree.setRunbooks(runbooks);
//                runbookTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getRunbookTree catched exception: " + t);
//        }
//
//        return runbookTree;
//    }
//
//
//    public static DecisionTreeTree getDecisionTreeTree(User user, boolean withMember)
//    {
//        DecisionTreeTree dtTree = new DecisionTreeTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//        Set<String> dtIDs = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        String nodeName = (String) endNode.getProperty("displayName");
//                        BuildOneDepthTree.buildOneDepthDecisionTreeTree(endNode, dts, 1, dtIDs);
//                    }
//
//                    if(withMember)
//                    {
//                        Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                        TraverseGraph.buildProcessesForDecisionTreeTab(memberRels, processes);
//                    }
//                }
//
//                if(withMember)
//                {
//                    mergeProcessToDecisionTree(dts, processes, dtIDs, false);
//                }
//
//                List<DecisionTree> sortedDT = new ArrayList<DecisionTree>(dts);
//                sortOnDisplayName(sortedDT);
//
//                dtTree.setDecisionTrees(sortedDT);
//                dtTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getDecisionTree catched exception: " + t);
//        }
//
//
//        return dtTree;
//    }
//
//    public static void mergeProcessToDecisionTree(Collection<DecisionTree> dts,
//                    Collection<Process> processes,
//                    Set<String> dtIDs,
//                    boolean recalculateRtsIDs)
//    {
//        if(processes != null)
//        {
//            if(dts != null)
//            {
//                for(DecisionTree dtLocal: dts)
//                {
//                    String dtLocalSysID = dtLocal.getSys_id();
//
//                    if(StringUtils.isNotEmpty(dtLocalSysID))
//                    {
//                        dtIDs.add(dtLocalSysID);
//                    }
//                }
//            }
//
//            for(Process process: processes)
//            {
//                Collection<DecisionTree> dtsLocal = process.getDecisionTrees();
//
//                if(dtsLocal != null && !dtsLocal.isEmpty())
//                {
//                    for(DecisionTree dt: dtsLocal)
//                    {
//                        dt.addProcessNames(process.getDisplayName());
//
//                        if(!dtIDs.contains(dt.getSys_id()))
//                        {
//                            dts.add(dt);
//                            dtIDs.add(dt.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public static DecisionTreeTree getDecisionTreeTreeWithPath(User user)
//    {
//
//        DecisionTreeTree dtTree = new DecisionTreeTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//        Set<String> dtIDs = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthDecisionTreeTree(endNode, dts, 1, dtIDs);
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForDecisionTreeTab(memberRels, processes);
//                }
//
//                dtTree.setProcesses(processes);
//                dtTree.setDecisionTrees(dts);
//                dtTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getDecisionTreeTreeWithPath catched exception: " + t);
//        }
//
//        return dtTree;
//    }
//
//    public static DocumentTree getDocumentTree(User user, boolean withMember)
//    {
//        DocumentTree documentTree = new DocumentTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Document> documents = new ArrayList<Document>();
//        Set<String> docIDS = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthDocumentTree(endNode, documents, 1, docIDS);
//                    }
//
//                    if(withMember)
//                    {
//                        Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                        TraverseGraph.buildProcessesForDocumentTab(memberRels, processes);
//                    }
//                }
//
//                if(withMember)
//                {
//                    //documentTree.setProcesses(processes);
//                    mergeProcessToDocument(documents, processes, docIDS, false);
//                }
//
//                List<Document> sortedDocs = new ArrayList<Document>(documents);
//                sortOnDisplayName(sortedDocs);
//
//                documentTree.setDocuments(sortedDocs);
//                documentTree.setOwner(user);
//
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getDocumentTree catched exception: " + t);
//        }
//
//        return documentTree;
//    }
//
//    public static void mergeProcessToDocument(Collection<Document> documents,
//                                              Collection<Process> processes,
//                                              Set<String> docIDs,
//                                              boolean recalculateDocumentIDS)
//    {
//        if(processes != null)
//        {
//
//            if(documents != null)
//            {
//                for(Document docLocal: documents)
//                {
//                    String docLocalSysID = docLocal.getSys_id();
//
//                    if(StringUtils.isNotEmpty(docLocalSysID))
//                    {
//                        docIDs.add(docLocalSysID);
//                    }
//                }
//            }
//
//            for(Process process: processes)
//            {
//                Collection<Document> docs = process.getDocuments();
//
//                if(docs != null)
//                {
//                    for(Document doc: docs)
//                    {
//                        doc.addProcessNames(process.getDisplayName());
//
//                        if(!docIDs.contains(doc.getSys_id()))
//                        {
//                            documents.add(doc);
//                            docIDs.add(doc.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public static DocumentTree getDocumentTreeWithPath(User user)
//    {
//        DocumentTree documentTree = new DocumentTree();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Document> documents = new ArrayList<Document>();
//        Set<String> docIDS = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//                        BuildOneDepthTree.buildOneDepthDocumentTree(endNode, documents, 1, docIDS);
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForDocumentTab(memberRels, processes);
//                }
//
//                documentTree.setProcesses(processes);
//                documentTree.setDocuments(documents);
//                documentTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getDocumentTree catched exception: " + t);
//        }
//
//        return documentTree;
//    }
//
//    public static RssTree getRssTree(User user)
//    {
//        RssTree rssTree = new RssTree();
//
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Rss> rsss = new ArrayList<Rss>();
//        Set<String> rssIDs = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            Node endNode = rel.getEndNode();
//                            BuildOneDepthTree.buildOneDepthRssTree(endNode, rsss, 1, rssIDs);
//                        }
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForRssTab(memberRels, processes);
//                }
//
//                //rssTree.setProcesses(processes);
//                mergeProcessToRss(rsss, processes, rssIDs);
//
//                List<Rss> sortedRss = new ArrayList<Rss>(rsss);
//                sortOnDisplayName(sortedRss);
//                rssTree.setRsss(sortedRss);
//
//                rssTree.setOwner(user);
//
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getRssTree catched exception: " + t);
//        }
//
//        return rssTree;
//    }
//
//    public static void mergeProcessToRss(Collection<Rss> rsses, Collection<Process> processes, Set<String> rssIDS)
//    {
//        if(processes != null)
//        {
//            for(Process process: processes)
//            {
//                Collection<Rss> rssLocal = process.getRss();
//
//                if(rssLocal != null)
//                {
//                    for(Rss rss: rssLocal)
//                    {
//                        rss.addProcessNames(process.getDisplayName());
//
//                        if(!rssIDS.contains(rss.getSys_id()))
//                        {
//                            rsses.add(rss);
//                            rssIDS.add(rss.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public static RssTree getRssTreeWithPath(User user)
//    {
//        RssTree rssTree = new RssTree();
//
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Rss> rsss = new ArrayList<Rss>();
//        Set<String> rssIDs = new HashSet<String>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            Node endNode = rel.getEndNode();
//                            BuildOneDepthTree.buildOneDepthRssTree(endNode, rsss, 1, rssIDs);
//                        }
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForRssTab(memberRels, processes);
//                }
//
//                rssTree.setProcesses(processes);
//                rssTree.setRsss(rsss);
//                rssTree.setOwner(user);
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getRssTree catched exception: " + t);
//        }
//
//        return rssTree;
//    }
//
//    public static ForumTree getForumsTree(User user)
//    {
//        ForumTree forumTree = new ForumTree();
//
//        Set<String> forumIDSet = new HashSet<String>();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Forum> forums = new ArrayList<Forum>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            Node endNode = rel.getEndNode();
//                            BuildOneDepthTree.buildOneDepthForumTree(endNode, forums, 1, forumIDSet);
//                        }
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForForumTab(memberRels, processes, forums, forumIDSet);
//                }
//
//                //forumTree.setProcesses(processes);
//                mergeProcessForums(forums, processes, forumIDSet);
//                List<Forum> forumList = new ArrayList<Forum>(forums);
//
//                sortOnDisplayName(forumList);
//                forumTree.setForums(forumList);
//                forumTree.setOwner(user);
//
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getForumsTree catched exception: " + t);
//        }
//
//        return forumTree;
//    }
//
//    public static NameSpaceTree getNameSpaceTree(User user)
//    {
//        NameSpaceTree namespaceTree = new NameSpaceTree();
//
//        Set<String> namespaceIDSet = new HashSet<String>();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Namespace> namespaces = new ArrayList<Namespace>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            Node endNode = rel.getEndNode();
//                            BuildOneDepthTree.buildOneDepthNameSpaceTree(endNode, namespaces, 1, namespaceIDSet);
//                        }
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForNameSpaceTab(memberRels, processes, namespaces, namespaceIDSet);
//                }
//
//                //forumTree.setProcesses(processes);
//                mergeProcessNameSpaces(namespaces, processes, namespaceIDSet);
//                List<Namespace> namespaceList = new ArrayList<Namespace>(namespaces);
//
//                sortOnDisplayName(namespaceList);
//                namespaceTree.setNameSpaces(namespaceList);
//                namespaceTree.setOwner(user);
//
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getNameSpaceTree catched exception: " + t);
//        }
//
//        return namespaceTree;
//    }
//
//    public static void mergeProcessForums(Collection<Forum> forums, Collection<Process> processes, Set<String> forumIDSet)
//    {
//        if(forums != null && processes != null)
//        {
//            for(Process process: processes)
//            {
//                Collection<Forum> forumsProcess = process.getForums();
//
//                if(forumsProcess != null)
//                {
//                    for(Forum forum: forumsProcess)
//                    {
//                        forum.addProcessNames(process.getDisplayName());
//
//                        if(!forumIDSet.contains(forum.getSys_id()))
//                        {
//                            forums.add(forum);
//                            forumIDSet.add(forum.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public static void mergeProcessNameSpaces(Collection<Namespace> namespaces, Collection<Process> processes, Set<String> namespaceIDSet)
//    {
//        if(namespaces != null && processes != null)
//        {
//            for(Process process: processes)
//            {
//                Collection<Namespace> namespacesProcess = process.getNameSpaces();
//
//                if(namespacesProcess != null)
//                {
//                    for(Namespace namespaceLocal: namespacesProcess)
//                    {
//                        namespaceLocal.addProcessNames(process.getDisplayName());
//
//                        if(!namespaceIDSet.contains(namespaceLocal.getSys_id()))
//                        {
//                            namespaces.add(namespaceLocal);
//                            namespaceIDSet.add(namespaceLocal.getSys_id());
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public static ForumTree getForumsTreeWithPath(User user)
//    {
//        ForumTree forumTree = new ForumTree();
//
//        Set<String> forumIDSet = new HashSet<String>();
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Forum> forums = new ArrayList<Forum>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FOLLOWER, Direction.OUTGOING);
//
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            Node endNode = rel.getEndNode();
//                            BuildOneDepthTree.buildOneDepthForumTree(endNode, forums, 1, forumIDSet);
//                        }
//                    }
//
//                    Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//                    TraverseGraph.buildProcessesForForumTab(memberRels, processes, forums, forumIDSet);
//                }
//
//                forumTree.setProcesses(processes);
//
//                forumTree.setForums(forums);
//                forumTree.setOwner(user);
//
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getForumsTree catched exception: " + t);
//        }
//
//        return forumTree;
//    }
//
//    public static int getActiveSocialUserCount()
//    {
//        return socialUsersFactory.getContainerUserCount();
//    }
//
//    public static String getAllUserIndirectMemberContainerIDS(User user)
//    {
//        String results = "";
//
//        try
//        {
//            StringBuffer targetIDs = new StringBuffer();
//            AdvanceTree advancedTree = getAdvanceTreeForProcess(user);
//
//            if(advancedTree != null && advancedTree.getProcesses() != null && !advancedTree.getProcesses().isEmpty())
//            {
//                for(Process processLocal: advancedTree.getProcesses())
//                {
//                    buildContainerTargetIDsFromProcess(user, processLocal, targetIDs);
//                }
//            }
//
//            results = targetIDs.toString();
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getAllUserIndirectMemberContainerIDS catched exception: " + t, t);
//        }
//
//        return results;
//    }
//
//    public static String getAllTeamsIDSUntilRootTeam(User user, Team rootTeam)
//    {
//        //get user team tree
//        TeamTree teamTree = getTeamTreePost(user);
//
//        //one depth of teams
//        Collection<Team> teams = teamTree.getTeams();
//        StringBuffer targetIDs = new StringBuffer();
//
//        if(teams != null)
//        {
//            for(Team team: teams)
//            {
//                buildTeamTargetIDsWithRootTeam(targetIDs, team, rootTeam);
//            }
//        }
//
//        //Traverse the runbook as member of the process
//        Collection<Process> processes = teamTree.getProcesses();
//
//        if(processes != null)
//        {
//            //Set<String> postPathTeams = teamTree.getPostPathTeamsToRootProcess();
//
//            for(Process proc : processes)
//            {
//                Collection<Team> teamsLocal  = proc.getTeams();
//
//                if(teamsLocal != null)
//                {
//                    for(Team team: teamsLocal)
//                    {
//                        //if(postPathTeams.contains(team.getSys_id()))
//                        //{
//                        //buildTeamTargetIDsWithRootTeamForProcess(targetIDs, team, rootTeam, postPathTeams);
//                        buildTeamTargetIDsWithRootTeamForProcess(targetIDs, team, rootTeam);
//                        //}
//                    }
//                }
//            }
//        }
//
//        return targetIDs.toString();
//    }
//
//    public static void buildTargetIDsFromProcess(User user, Process process, StringBuffer targetIDs, StringBuffer namespaces)
//    {
//      //get process id
//        targetIDs.append(process.getSys_id() +  " ");
//
//        //get namespace display name
//        if(process.getNameSpaces() != null && !process.getNameSpaces().isEmpty())
//        {
//            for(Namespace namespaceLocal: process.getNameSpaces())
//            {
//                namespaces.append(namespaceLocal.getName() + " ");
//            }
//        }
//
//        //get actiontask ids
//        if(process.getActiontasks() != null && !process.getActiontasks().isEmpty())
//        {
//            for(ActionTask actionTask: process.getActiontasks())
//            {
//                targetIDs.append(actionTask.getSys_id() +  " ");
//            }
//        }
//
//        //get runbook ids
//        if(process.getRunbooks() != null && !process.getRunbooks().isEmpty())
//        {
//            for(Runbook runbook: process.getRunbooks())
//            {
//                targetIDs.append(runbook.getSys_id() +  " ");
//            }
//        }
//
//        //get decisiontree ids
//        if(process.getDecisionTrees() != null && !process.getDecisionTrees().isEmpty())
//        {
//            for(DecisionTree dt: process.getDecisionTrees())
//            {
//                targetIDs.append(dt.getSys_id() +  " ");
//            }
//        }
//
//        //get document ids
//        if(process.getDocuments() != null && !process.getDocuments().isEmpty())
//        {
//            for(Document document: process.getDocuments())
//            {
//                targetIDs.append(document.getSys_id() +  " ");
//            }
//        }
//
//        //get Forum ids
//        if(process.getForums() != null && !process.getForums().isEmpty())
//        {
//            for(Forum forum: process.getForums())
//            {
//                targetIDs.append(forum.getSys_id() +  " ");
//            }
//        }
//
//        //get teams only in my path
//        //get all team nodes -- user member of, and belong to process
////        Set<String> pathTeamNames = new HashSet<String>();
////        List<Node> startTeams = getStartTeamNodesInProcess(user);
////        Set<String> currentPostPathTeams = new HashSet<String>();
//
////        for(Node nodeLocal: startTeams)
////        {
////            currentPostPathTeams = TraverseGraph.getToRootPathTeams(nodeLocal);
////
////            if(currentPostPathTeams != null)
////            {
////                pathTeamNames.addAll(currentPostPathTeams);
////            }
////        }
//
//        //get teams ids
//        if(process.getTeams() != null && !process.getTeams().isEmpty())
//        {
//            for(Team team: process.getTeams())
//            {
//                //if(pathTeamNames.contains(team.getSys_id()))
//                //{
//                    //buildTeamTargetIDsFromProcess(targetIDs, team, pathTeamNames);
//                    buildTeamTargetIDsFromProcess(targetIDs, team);
//                //}
//            }
//        }
//    }
//
//    public static void buildContainerTargetIDsFromProcess(User user, Process process, StringBuffer targetIDs)
//    {
//      //get process id
//        targetIDs.append(process.getSys_id() +  " ");
//
//        //get Forum ids
//        if(process.getForums() != null && !process.getForums().isEmpty())
//        {
//            for(Forum forum: process.getForums())
//            {
//                targetIDs.append(forum.getSys_id() +  " ");
//            }
//        }
//
//        //get teams ids
//        if(process.getTeams() != null && !process.getTeams().isEmpty())
//        {
//            for(Team team: process.getTeams())
//            {
//                buildTeamTargetIDsFromProcess(targetIDs, team);
//            }
//        }
//    }
//
//    public static long getRssFollwerCount(Rss rss)
//    {
//        Node rssNode = null;
//        long count = 0l;
//
//        try
//        {
//            if(rss != null && StringUtils.isNotEmpty(rss.getSys_id()))
//            {
//                rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
//
//                if(rssNode != null)
//                {
//                    Iterable<Relationship> rels = rssNode.getRelationships(RelationType.FOLLOWER);
//
//                    for(Relationship rel: rels)
//                    {
//                        count++;
//                    }
//
//                    TraversalDescription trav = Traversal.description()
//                                                                .breadthFirst()
//                                                                .relationships(RelationType.MEMBER, Direction.OUTGOING)
//                                                                .evaluator(Evaluators.excludeStartPosition());
//
//                    for(Node nodeLocal: trav.traverse(rssNode).nodes())
//                    {
//                        if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                        {
//                            count++;
//                        }
//                    }
//
////                    for(Node processNodeLocal: process)
////                    {
////                        Traverser travProcess = processNodeLocal.traverse(Order.BREADTH_FIRST,
////                                                 StopEvaluator.END_OF_GRAPH,
////                                                 ReturnableEvaluator.ALL_BUT_START_NODE,
////                                                 RelationType.MEMBER,
////                                                 Direction.INCOMING);
////
////                        for(Node userNodeLocal: travProcess)
////                        {
////                            if(userNodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.USER.name()))
////                            {
////                                count++;
////                            }
////                        }
////                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getRssFollwerCount catched exception: " + t);
//        }
//
//        return count;
//    }
//
//    public static List<User> getAllUsersLikeThisPost(String postid)
//    {
//        List<User> users = new ArrayList<User>();
//        Node postNode = null;
//        User user = null;
//
//        try
//        {
//            if(StringUtils.isNotEmpty(postid))
//            {
//                postNode = SocialPostFactory.findPost(postid, null);
//
//                if(postNode != null)
//                {
//                    Iterable<Relationship> rels = postNode.getRelationships(Direction.INCOMING, RelationType.LIKE);
//
//                    for(Relationship userRel: rels)
//                    {
//                        Node startNode = userRel.getStartNode();
//
//                        if(startNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()))
//                        {
//                            user = new User();
//                            user.convertNodeToObject(startNode);
//                            users.add(user);
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getAllUsersLikeThisPost catched exception: " + t);
//        }
//
//        return users;
//    }
//
//    public static AdvanceTree getAdvanceTreeForProcess(User user)
//    {
//        AdvanceTree advanceTree = new AdvanceTree();
//        advanceTree.setOwner(user);
//
//        //the key is the node id
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Team> teams = new ArrayList<Team>();
//        Collection<Forum> forums = new ArrayList<Forum>();
//        Collection<User> users = new ArrayList<User>();
//        Collection<ActionTask> actiontasks = new ArrayList<ActionTask>();
//        Collection<Runbook> runbooks = new ArrayList<Runbook>();
//        Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//        Collection<Document> documents = new ArrayList<Document>();
//        Collection<Rss> rsss = new ArrayList<Rss>();
//
//        Node userNode = null;
//
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//            if(userNode != null)
//            {
//                Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//
//                for(Relationship member: memberRels)
//                {
//                    Node endNode = member.getEndNode();
//                   boolean test =     endNode.hasProperty(SocialFactory.NODE_TYPE);
//                   if(!test)
//                   {
//                       continue;
//                   }
//
//                    if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                    {
//                        if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                        {
//                            Process process = TraverseGraph.getProcess(endNode);
//                            processes.add(process);
//                            processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                        }
//                    }
//                    else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                    {
//                        Collection<Node> teamRootNodes = new ArrayList<Node>();
//                        TraverseGraph.getRootNodes(endNode, teamRootNodes);
//
//                        if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                        {
//                            for(Node teamRootNode: teamRootNodes)
//                            {
//                                if(teamRootNode != null)
//                                {
//                                    Object test1 =     teamRootNode.getProperty(SocialFactory.NODE_TYPE);
//                                    if(test1 == null)
//                                    {
//                                        System.out.println("this is null");
//                                    }
//
//                                    if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                    {
//                                        if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                        {
//                                            Process process = TraverseGraph.getProcess(teamRootNode);
//                                            processes.add(process);
//                                            processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(endNode.getId()));
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//                    {
//                        Collection<Node> forumRootNodes = new ArrayList<Node>();
//                        TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//
//                        if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                        {
//                            for(Node forumRootNode: forumRootNodes)
//                            {
//                                if(forumRootNode != null)
//                                {
//                                    Object test1 =     forumRootNode.getProperty(SocialFactory.NODE_TYPE);
//                                    if(test1 == null)
//                                    {
//                                        System.out.println("this is null");
//                                    }
//                                    if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                    {
//                                        if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                                        {
//                                            Process process = TraverseGraph.getProcess(forumRootNode);
//                                            processes.add(process);
//                                            processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        advanceTree.setProcesses(processes);
//        advanceTree.setTeams(teams);
//        advanceTree.setForums(forums);
//        advanceTree.setUsers(users);
//        advanceTree.setActionTasks(actiontasks);
//        advanceTree.setRunbooks(runbooks);
//        advanceTree.setDecisionTrees(dts);
//        advanceTree.setDocuments(documents);
//        advanceTree.setRsss(rsss);
//        advanceTree.setOwner(user);
//
//        return advanceTree;
//    }
//
//    /**
//     * TODO --- for multiple target??
//     *
//     * @param userid
//     * @param targets
//     * @return
//     */
//    public static void isUserFollowingTheTarget(String userid, Collection<RSComponent> targets)
//    {
//        boolean isFollowing = false;
//
//        try
//        {
//            if(targets != null)
//            {
//                for(RSComponent comp: targets)
//                {
//                    String targetID = comp.getSys_id();
//
//                    if(StringUtils.isNotEmpty(userid) && StringUtils.isNotEmpty(targetID))
//                    {
//                        Node userNode = ResolveGraphFactory.findNodeByIndexedID(userid);
//                        Node targetNode = ResolveGraphFactory.findNodeByIndexedID(targetID);
//
//                        if(userNode != null && targetNode != null)
//                        {
//                            isFollowing = ResolveGraphFactory.findRelation(userNode, targetNode, RelationType.FOLLOWER);
//
//                            if(!isFollowing)
//                            {
//                                isFollowing = ResolveGraphFactory.findRelation(userNode, targetNode, RelationType.MEMBER);
//                            }
//                        }
//                    }
//
//                    comp.setIsFollowing(isFollowing);
//                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//
//    }
//
//    public static boolean isUserFollowingThisTarget(Node userNode, Node targetNode)
//    {
//        boolean following = false;
//
//        if(userNode != null && targetNode != null)
//        {
//            following = ResolveGraphFactory.findRelation(userNode, targetNode, RelationType.FOLLOWER);
//            if(!following)
//            {
//                following = ResolveGraphFactory.findRelation(userNode, targetNode, RelationType.MEMBER);
//            }
//        }
//
//        return following;
//    }
//
//    protected static void buildTeamTargetIDsFromProcess(StringBuffer targetIDs, Team team)
//    {
//        if(team != null)
//        {
//            if(targetIDs != null && !targetIDs.toString().contains(team.getSys_id()))
//            {
//                targetIDs.append(team.getSys_id() +  " ");
//
//                Collection<Team> teamsLocal = team.getTeams();
//
//                if(teamsLocal != null)
//                {
//                    for(Team teamLocal: teamsLocal)
//                    {
//                        buildTeamTargetIDsFromProcess(targetIDs, teamLocal);
//                    }
//                }
//            }
//        }
//    }
//
//    protected static void buildTeamTargetIDs(StringBuffer targetIDs, Team team)
//    {
//        if(team != null)
//        {
//            targetIDs.append(team.getSys_id() +  " ");
//
//            Collection<Team> teamsLocal = team.getTeams();
//
//            if(teamsLocal != null)
//            {
//                for(Team teamLocal: teamsLocal)
//                {
//                    buildTeamTargetIDs(targetIDs, teamLocal);
//                }
//            }
//        }
//    }
//
//    protected static void buildTeamTargetIDsWithPath(StringBuffer targetIDs, Team team)
//    {
//        if(team != null)
//        {
//            if(targetIDs != null && !targetIDs.toString().contains(team.getSys_id()))
//            {
//                targetIDs.append(team.getSys_id() +  " ");
//
//                Collection<Team> teamsLocal = team.getTeams();
//
//                if(teamsLocal != null)
//                {
//                    for(Team teamLocal: teamsLocal)
//                    {
//                        buildTeamTargetIDsWithPath(targetIDs, teamLocal);
//                    }
//                }
//            }
//        }
//    }
//
////    protected static void buildTeamTargetIDsWithPath(StringBuffer targetIDs, Team team, Set<String> postPathTeamsToRootTeam)
////    {
////        if(team != null && !postPathTeamsToRootTeam.isEmpty())
////        {
////            if(targetIDs != null && !targetIDs.toString().contains(team.getSys_id()))
////            {
////                targetIDs.append(team.getSys_id() +  " ");
////
////                Collection<Team> teamsLocal = team.getTeams();
////
////                if(teamsLocal != null)
////                {
////                    for(Team teamLocal: teamsLocal)
////                    {
////                        if(postPathTeamsToRootTeam.contains(teamLocal.getSys_id()))
////                        {
////                            buildTeamTargetIDsWithPath(targetIDs, teamLocal, postPathTeamsToRootTeam);
////                        }
////                    }
////                }
////            }
////        }
////    }
//
//
//
//    protected static void buildTeamTargetIDsWithRootTeamForProcess(StringBuffer targetIDs,
//                                                                   Team team,
//                                                                   Team rootTeam)
//    {
//        if(team != null)
//        {
//            Collection<Team> teamsLocal = team.getTeams();
//
//            if(team.getSys_id().equals(rootTeam.getSys_id()))
//            {
//                targetIDs.append(team.getSys_id() +  " ");
//
//                if(teamsLocal != null)
//                {
//                    for(Team teamLocal: teamsLocal)
//                    {
//                        buildTeamTargetIDsFromProcess(targetIDs, teamLocal);
//                    }
//                }
//            }
//            else
//            {
//                if(teamsLocal != null)
//                {
//                    for(Team teamLocal: teamsLocal)
//                    {
//                        if(teamLocal.getSys_id().equals(rootTeam.getSys_id()))
//                        {
//                            buildTeamTargetIDsWithRootTeam(targetIDs, teamLocal, rootTeam);
//                        }
//                    }
//                }
//            }
//        }
//    }
//
////    protected static void buildTeamTargetIDsWithRootTeamForProcess(StringBuffer targetIDs,
////                                                                   Team team,
////                                                                   Team rootTeam,
////                                                                   Set<String> postPathTeams)
////    {
////        if(team != null)
////        {
////            Collection<Team> teamsLocal = team.getTeams();
////
////            if(team.getSys_id().equals(rootTeam.getSys_id()) && postPathTeams.contains(team.getSys_id()))
////            {
////                targetIDs.append(team.getSys_id() +  " ");
////
////                if(teamsLocal != null)
////                {
////                    for(Team teamLocal: teamsLocal)
////                    {
////                        if(postPathTeams.contains(teamLocal.getSys_id()))
////                        {
////                            buildTeamTargetIDsFromProcess(targetIDs, teamLocal, postPathTeams);
////                        }
////                    }
////                }
////            }
////            else
////            {
////                if(teamsLocal != null)
////                {
////                    for(Team teamLocal: teamsLocal)
////                    {
////                        if(teamLocal.getSys_id().equals(rootTeam.getSys_id()))
////                        {
////                            buildTeamTargetIDsWithRootTeam(targetIDs, teamLocal, rootTeam);
////                        }
////                    }
////                }
////            }
////        }
////    }
//
//    protected static void buildTeamTargetIDsWithRootTeam(StringBuffer targetIDs, Team team, Team rootTeam)
//    {
//        if(team != null)
//        {
//            Collection<Team> teamsLocal = team.getTeams();
//
//            if(team.getSys_id().equals(rootTeam.getSys_id()))
//            {
//                targetIDs.append(team.getSys_id() +  " ");
//
//                if(teamsLocal != null)
//                {
//                    for(Team teamLocal: teamsLocal)
//                    {
//                        buildTeamTargetIDs(targetIDs, teamLocal);
//                    }
//                }
//            }
//            else
//            {
//                if(teamsLocal != null)
//                {
//                    for(Team teamLocal: teamsLocal)
//                    {
//                        if(teamLocal.getSys_id().equals(rootTeam.getSys_id()))
//                        {
//                            buildTeamTargetIDsWithRootTeam(targetIDs, teamLocal, rootTeam);
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    //User can existed in multiple Team Nodes
//    public static List<Node> getStartTeamNodesInProcess(User user)
//    {
//        List<Node> userStartNodes = new ArrayList<Node>();
//
//        Node userNode = null;
//
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//            if(userNode != null)
//            {
//                Iterable<Relationship> memberRels = userNode.getRelationships(Direction.OUTGOING, RelationType.MEMBER);
//
//                for(Relationship member: memberRels)
//                {
//                    Node endNode = member.getEndNode();
//
//                    if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                    {
//                        Collection<Node> teamRootNodes = new ArrayList<Node>();
//                        TraverseGraph.getRootNodes(endNode, teamRootNodes);
//
//                        if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                        {
//                            for(Node teamRootNode: teamRootNodes)
//                            {
//                                if(teamRootNode != null)
//                                {
//                                    if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                    {
//                                        userStartNodes.add(endNode);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return userStartNodes;
//    }
//
//    public static AdvanceTree getUserFavorite(User user)
//    {
//        Collection<Process> processes = new ArrayList<Process>();
//        Collection<Team> teams = new ArrayList<Team>();
//        Collection<Forum> forums = new ArrayList<Forum>();
//        Collection<User> users = new ArrayList<User>();
//        Collection<ActionTask> actiontasks = new ArrayList<ActionTask>();
//        Collection<Runbook> runbooks = new ArrayList<Runbook>();
//        Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//        Collection<Document> documents = new ArrayList<Document>();
//        Collection<Rss> rsss = new ArrayList<Rss>();
//
//        AdvanceTree advanceTree = new AdvanceTree();
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    Iterable<Relationship> rels = userNode.getRelationships(RelationType.FAVORITE);
//
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            Node endNode = rel.getEndNode();
//
//                            BuildOneDepthTree.buildOneDepthFavorite(
//                                                endNode,
//                                                users,
//                                                actiontasks,
//                                                runbooks,
//                                                dts,
//                                                documents,
//                                                rsss,
//                                                forums,
//                                                teams,
//                                                processes,
//                                                1);
//
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getUserFavorite catch exception: " + t);
//        }
//
//        advanceTree.setProcesses(processes);
//        advanceTree.setTeams(teams);
//        advanceTree.setForums(forums);
//        advanceTree.setUsers(getMergedUsers(users, user));
//        advanceTree.setActionTasks(actiontasks);
//        advanceTree.setRunbooks(runbooks);
//        advanceTree.setDecisionTrees(dts);
//        advanceTree.setDocuments(documents);
//        advanceTree.setRsss(rsss);
//        advanceTree.setOwner(user);
//
//        return advanceTree;
//    }
//
//    protected static Collection<User> getMergedUsers(Collection<User> users, User currentUser)
//    {
//        Collection<User> mergedUsers = new ArrayList<User>();
//
//        if(users != null && currentUser != null)
//        {
//            for(User user: users)
//            {
//                if(!user.getSys_id().trim().equalsIgnoreCase(currentUser.getSys_id().trim()))
//                {
//                    mergedUsers.add(user);
//                }
//            }
//        }
//
//        return mergedUsers;
//    }
//
//    protected static Team findRootTeam(Team rootTeam)
//    {
//        Team rootTeamLocal = null;
//
//        if(rootTeam != null)
//        {
//            Node rootTeamNode = ResolveGraphFactory.findNodeByIndexedID(rootTeam.getSys_id());
//            rootTeamLocal = TraverseGraph.getTeam(rootTeamNode);
//        }
//
//        return rootTeamLocal;
//    }
//
//    public static List<NotificationConfig> getCompNotificationType(RSComponent comp, boolean forNotificationType)
//    {
//        List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>();
//
//        try
//        {
//            String className = comp.getClass().getName();
//            EnumSet<NotificationTypeEnum> enumSet = null;
//
//            if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.container.Process"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetProcess;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetProcessEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.container.Team"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetTeam;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetTeamEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.container.Forum"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetForum;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetForumEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.ActionTask"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetActionTask;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetActionTaskEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Runbook"))
//            {
////                if(forNotificationType)
////                {
////                    enumSet = NotificationTypeEnum.enumSetRunbook;
////                }
////                else
////                {
////                    enumSet = NotificationTypeEnum.enumSetRunbookEmail;
////                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.DecisionTree"))
//            {
////                if(forNotificationType)
////                {
////                    enumSet = NotificationTypeEnum.enumSetDecisionTree;
////                }
////                else
////                {
////                    enumSet = NotificationTypeEnum.enumSetDecisionTreeEmail;
////                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Document"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetDocument;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetDocumentEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Namespace"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetNamespace;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetNamespaceEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Worksheet"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetWorksheet;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetWorksheetEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Rss"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetRss;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetRssEmail;
//                }
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.User"))
//            {
//                if(forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetUser;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetUserEmail;
//                }
//            }
//
//            if(enumSet != null)
//            {
//                NotificationConfig config = null;
//
//                for(NotificationTypeEnum typeEnum: enumSet)
//                {
//                    config = new NotificationConfig();
//                    config.setType(typeEnum.getType());
//                    config.setDisplayName(typeEnum.getDisplayName());
//                    notifyTypes.add(config);
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getCompNotificationType catch exception: " + t);
//        }
//
//        return notifyTypes;
//    }
//
//    public static ComponentNotification getAllNotificationTypeOfComponent(RSComponent comp)
//    {
//        ComponentNotification compNotify = new ComponentNotification();
//
//        try
//        {
//            String className = comp.getClass().getName();
//
//            if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.container.Process"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new Process());
//                compNotify.getRSComponent().setDisplayName("Process");
//                compNotify.setNotifyTypes(getCompNotificationType(new Process(), true));
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.container.Team"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new Team());
//                compNotify.getRSComponent().setDisplayName("Team");
//                compNotify.setNotifyTypes(getCompNotificationType(new Team(), true));
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.container.Forum"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new Forum());
//                compNotify.getRSComponent().setDisplayName("Forum");
//                compNotify.setNotifyTypes(getCompNotificationType(new Forum(), true));
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.ActionTask"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new ActionTask());
//                compNotify.getRSComponent().setDisplayName("ActionTask");
//                compNotify.setNotifyTypes(getCompNotificationType(new ActionTask(), true));
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Runbook"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new Runbook());
//                compNotify.getRSComponent().setDisplayName("Runbook");
//                compNotify.setNotifyTypes(getCompNotificationType(new Runbook(), true));
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Document"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new Document());
//                compNotify.getRSComponent().setDisplayName("Document");
//                compNotify.setNotifyTypes(getCompNotificationType(new Document(), true));
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.Rss"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new Rss());
//                compNotify.getRSComponent().setDisplayName("RSS");
//                compNotify.setNotifyTypes(getCompNotificationType(new Rss(), true));
//            }
//            else if(className.equalsIgnoreCase("com.resolve.services.graph.social.model.component.User"))
//            {
//                compNotify = new ComponentNotification();
//                compNotify.setRSComponent(new User());
//                compNotify.getRSComponent().setDisplayName("User");
//                compNotify.setNotifyTypes(getCompNotificationType(new User(), true));
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getAllNotificationTypeOfComponent catch exception: " + t);
//        }
//
//        return compNotify;
//    }
//
//    public static List<ComponentNotification> getAllNotificationType(boolean forNotificationType)
//    {
//        List<ComponentNotification> allTypes = new ArrayList<ComponentNotification>();
//
//        try
//        {
//            ComponentNotification compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new Process());
//            compNotify.getRSComponent().setDisplayName("Process");
//            compNotify.setNotifyTypes(getCompNotificationType(new Process(), forNotificationType));
//            allTypes.add(compNotify);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new Team());
//            compNotify.getRSComponent().setDisplayName("Team");
//            compNotify.setNotifyTypes(getCompNotificationType(new Team(), forNotificationType));
//            allTypes.add(compNotify);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new Forum());
//            compNotify.getRSComponent().setDisplayName("Forum");
//            compNotify.setNotifyTypes(getCompNotificationType(new Forum(), forNotificationType));
//            allTypes.add(compNotify);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new Runbook());
//            compNotify.getRSComponent().setDisplayName("Runbook");
//            compNotify.setNotifyTypes(getCompNotificationType(new Runbook(), forNotificationType));
//            allTypes.add(compNotify);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new Document());
//            compNotify.getRSComponent().setDisplayName("Document");
//            compNotify.setNotifyTypes(getCompNotificationType(new Document(), forNotificationType));
//            allTypes.add(compNotify);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new ActionTask());
//            compNotify.getRSComponent().setDisplayName("ActionTask");
//            compNotify.setNotifyTypes(getCompNotificationType(new ActionTask(), forNotificationType));
//            allTypes.add(compNotify);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new Rss());
//            compNotify.getRSComponent().setDisplayName("RSS");
//            compNotify.setNotifyTypes(getCompNotificationType(new Rss(), forNotificationType));
//            allTypes.add(compNotify);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(new User());
//            compNotify.getRSComponent().setDisplayName("User");
//            compNotify.setNotifyTypes(getCompNotificationType(new User(), forNotificationType));
//            allTypes.add(compNotify);
//
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getAllNotificationType catch exception: " + t);
//        }
//
//        return allTypes;
//    }
//
//
//    public static List<NotificationConfig> getCompNotificationTypeForUser(User user, RSComponent comp)
//    {
//        List<NotificationConfig> userCompTypes = new ArrayList<NotificationConfig>();
//        Node userNode = null;
//        Node rsComponentNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null && comp != null && StringUtils.isNotEmpty(comp.getSys_id()))
//                {
//                    SocialFactory socialFactory = SocialUtil.getSocialFactory(comp.getClass().getName());
//
//                    if (socialFactory != null)
//                    {
//                        rsComponentNode = ResolveGraphFactory.findNodeByIndexedID(comp.getSys_id());
//
//                        if (rsComponentNode != null)
//                        {
//                            //find the user -- rscomponent notification type
//                            List<NotificationConfig> compNotifyTypes = getCompNotificationType(comp, true);
//
//                            Relationship relLocal = null;
//
//                            if(compNotifyTypes != null && !compNotifyTypes.isEmpty())
//                            {
////                                for(NotificationConfig type: compNotifyTypes)
////                                {
////                                    relLocal = ResolveGraphFactory.findRelationForType(userNode, rsComponentNode, type.getType());
////
////                                    if(relLocal != null)
////                                    {
////                                        NotificationConfig configLocal = new NotificationConfig();
////                                        configLocal.setType(type.getType());
////                                        configLocal.setDisplayName(type.getDisplayName());
////
////                                        boolean value = (relLocal.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE) == null)?false:
////                                                            Boolean.valueOf((String)relLocal.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE));
////
////                                        configLocal.setValue(value+"");
////                                        userCompTypes.add(configLocal);
////                                    }
////                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getCompNotificationTypeForUser catch exception: " + t);
//        }
//
//        return userCompTypes;
//    }
//
//    public static NotificationConfig getCompSpecialNotificationTypeForUser(User user,
//                                                                           RSComponent comp,
//                                                                           UserGlobalNotificationContainerType type)
//    {
//        NotificationConfig notifyType = null;
//
//        Node userNode = null;
//        Node rsComponentNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null && comp != null && StringUtils.isNotEmpty(comp.getSys_id()) && type != null)
//                {
//                    SocialFactory socialFactory = SocialUtil.getSocialFactory(comp.getClass().getName());
//
//                    if(socialFactory != null)
//                    {
//                        rsComponentNode = ResolveGraphFactory.findNodeByIndexedID(comp.getSys_id());
//
////                        Relationship relLocal = ResolveGraphFactory.findRelationForType(userNode, rsComponentNode, type);
////
////                        if(relLocal != null)
////                        {
////                            notifyType = new NotificationConfig();
////                            notifyType.setType(type);
////
////                            boolean value = (relLocal.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE) == null)?false:
////                                                Boolean.valueOf((String)relLocal.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE));
////
////                            notifyType.setValue(value+"");
////                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getCompSpecialNotificationTypeForUser catch exception: " + t);
//        }
//
//        return notifyType;
//    }
//
//
//    public static List<ComponentNotification> getUserGlobalNotificationType(User user)
//    {
//        List<ComponentNotification> allUserTypes = getAllNotificationType(true);
//        return getUserGlobalNotificationTypes(user, allUserTypes);
//    }
//
//    public static List<ComponentNotification> getUserGlobalNotificationTypes(User user, List<ComponentNotification> allUserTypes)
//    {
//        List<ComponentNotification> userGlobalTypes = new ArrayList<ComponentNotification>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    if(allUserTypes != null && !allUserTypes.isEmpty())
//                    {
//                        List<NotificationConfig> currentTypes = null;
//
//                        for(ComponentNotification compType: allUserTypes)
//                        {
//                            ComponentNotification compNotifyLocal = new ComponentNotification();
//                            compNotifyLocal.setRSComponent(compType.getRSComponent());
//
//                            currentTypes = new ArrayList<NotificationConfig>();
//
//                            List<NotificationConfig> allCompTypes = compType.getNotifyTypes();
//
//                            if(allCompTypes != null)
//                            {
//                                for(NotificationConfig notifyConfig: allCompTypes)
//                                {
//                                    Node typeNode = sgNotificationFactory.getNotificationConfigTypeNode(notifyConfig.getType());
//
////                                    if(typeNode != null)
////                                    {
////                                        Relationship rel = ResolveGraphFactory.findRelationForType(typeNode, userNode, notifyConfig.getType());
////
////                                        if(rel != null)
////                                        {
////                                            NotificationConfig configLocal = new NotificationConfig();
////                                            configLocal.setType(notifyConfig.getType());
////                                            configLocal.setDisplayName(notifyConfig.getDisplayName());
////
////                                            boolean value = (rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE) == null)?false:
////                                                Boolean.valueOf((String)rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE));
////                                            configLocal.setValue(value+"");
////
////                                            currentTypes.add(configLocal);
////                                        }
////                                    }
//                                }
//                            }
//
//                            compNotifyLocal.setNotifyTypes(currentTypes);
//                            userGlobalTypes.add(compNotifyLocal);
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getUserGlobalNotificationType catch exception: " + t);
//        }
//
//        return userGlobalTypes;
//
//    }
//
//    public static Map<RSComponent, NotificationConfig> getUserCustomNotificationTypeMap(User user, boolean forNotification)
//    {
//        Map<RSComponent, NotificationConfig> resultsMap = new HashMap<RSComponent, NotificationConfig>();
//
//        List<ComponentNotification> cnList = getUserCustomNotificationType(user, forNotification);
//
//        if(cnList != null)
//        {
//            for(ComponentNotification cn: cnList)
//            {
//                resultsMap.put(cn.getRSComponent(), (cn.getNotifyTypes() == null || cn.getNotifyTypes().size() <1)?null:cn.getNotifyTypes().get(0));
//            }
//        }
//
//        return resultsMap;
//    }
//
//    public static Set<RSComponent> getUserCustomEmailTypeMap(User user)
//    {
//        Set<RSComponent> resultsSet = new HashSet<RSComponent>();
//
//        List<ComponentNotification> notifyCompList = new ArrayList<ComponentNotification>();
//        List<ComponentNotification> allUserTypes = getAllNotificationType(false);
//
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//            if(userNode != null)
//            {
//                if(allUserTypes != null)
//                {
//                    for(ComponentNotification compNotify: allUserTypes)
//                    {
//                        notifyCompList = new ArrayList<ComponentNotification>();
//                        notifyCompList = TraverseGraph.getAllUserCustomNotificationComps(userNode, compNotify);
//
//                        if(notifyCompList != null)
//                        {
//                            for(ComponentNotification compNotification: notifyCompList)
//                            {
//                                RSComponent comp = compNotification.getRSComponent();
//                                NotificationConfig nConfig = compNotification.getNotifyTypes().get(0);
//
//                                if(nConfig != null && nConfig.getValue().equalsIgnoreCase("true"))
//                                {
//                                    resultsSet.add(comp);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return resultsSet;
//    }
//
//    public static List<ComponentNotification> getUserCustomNotificationType(User user, boolean forNotification)
//    {
//        List<ComponentNotification> userGlobalTypes = new ArrayList<ComponentNotification>();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    List<ComponentNotification> allNotifyTypes = getAllNotificationType(forNotification);
//
//                    if(allNotifyTypes != null && !allNotifyTypes.isEmpty())
//                    {
//                        List<NotificationConfig> currentTypes = null;
//
//                        for(ComponentNotification compType: allNotifyTypes)
//                        {
//                            ComponentNotification compNotifyLocal = new ComponentNotification();
//                            compNotifyLocal.setRSComponent(compType.getRSComponent());
//
//                            currentTypes = new ArrayList<NotificationConfig>();
//
//                            List<NotificationConfig> allCompTypes = compType.getNotifyTypes();
//
//                            if(allCompTypes != null)
//                            {
//                                for(NotificationConfig notifyConfig: allCompTypes)
//                                {
//                                    Node typeNode = sgNotificationFactory.getNotificationConfigTypeNode(notifyConfig.getType());
//
////                                    if(typeNode != null)
////                                    {
////                                        Relationship rel = ResolveGraphFactory.findRelationForType(typeNode, userNode, notifyConfig.getType());
////
////                                        if(rel != null)
////                                        {
////                                            NotificationConfig configLocal = new NotificationConfig();
////                                            configLocal.setType(notifyConfig.getType());
////                                            configLocal.setDisplayName(notifyConfig.getDisplayName());
////
////                                            boolean value = (rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE) == null)?false:
////                                                Boolean.valueOf((String)rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE));
////                                            configLocal.setValue(value+"");
////
////                                            currentTypes.add(configLocal);
////                                        }
////                                    }
//                                }
//                            }
//
//                            compNotifyLocal.setNotifyTypes(currentTypes);
//                            userGlobalTypes.add(compNotifyLocal);
//
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getUserCustomNotificationType catch exception: " + t);
//        }
//
//        return userGlobalTypes;
//
//    }
//
//    public static ComponentNotification getUserGlobalNotificationTypeOfComponent(User user, RSComponent comp)
//    {
//        ComponentNotification compNotifyLocal = new ComponentNotification();
//
//        Node userNode = null;
//
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if(userNode != null)
//                {
//                    ComponentNotification allUserTypesOftheComp = getAllNotificationTypeOfComponent(comp);
//
//                    if(allUserTypesOftheComp != null)
//                    {
//                        List<NotificationConfig> currentTypes = new ArrayList<NotificationConfig>();
//
//                        compNotifyLocal.setRSComponent(allUserTypesOftheComp.getRSComponent());
//
//                        List<NotificationConfig> allCompTypes = allUserTypesOftheComp.getNotifyTypes();
//
//                        if(allCompTypes != null)
//                        {
//                            for(NotificationConfig notifyConfig: allCompTypes)
//                            {
//                                Node typeNode = sgNotificationFactory.getNotificationConfigTypeNode(notifyConfig.getType());
//
////                                if(typeNode != null)
////                                {
////                                    Relationship rel = ResolveGraphFactory.findRelationForType(typeNode, userNode, notifyConfig.getType());
////
////                                    if(rel != null)
////                                    {
////                                        NotificationConfig configLocal = new NotificationConfig();
////                                        configLocal.setType(notifyConfig.getType());
////                                        configLocal.setDisplayName(notifyConfig.getDisplayName());
////
////                                        boolean value = (rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE) == null)?false:
////                                                Boolean.valueOf((String)rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE));
////                                                configLocal.setValue(value+"");
////
////                                         currentTypes.add(configLocal);
////                                    }
////                                }
//                            }
//                       }
//
//                       compNotifyLocal.setNotifyTypes(currentTypes);
//
//                     }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("getUserGlobalNotificationTypeOfComponent catch exception: " + t);
//        }
//
//        return compNotifyLocal;
//
//    }
//
//    public static NotificationConfig getUserNotificationType(User user, UserGlobalNotificationContainerType type)
//    {
//        NotificationConfig notificationConfig = null;
//        Node typeNodeLocal = null;
//        Node userNode = null;
//
//        if(type != null && user !=null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            typeNodeLocal = sgNotificationFactory.getNotificationConfigTypeNode(type);
//            userNode = userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
////            if(typeNodeLocal != null && userNode != null)
////            {
////                Relationship relLocal = ResolveGraphFactory.findRelationForType(typeNodeLocal, userNode, type);
////
////                if(relLocal != null)
////                {
////                    notificationConfig = new NotificationConfig();
////                    notificationConfig.setType(type);
////
////                    String valueStr =  (String) relLocal.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
////                    boolean value = (StringUtils.isEmpty(valueStr))?false:(valueStr.equalsIgnoreCase("true")?true:false);
////                    notificationConfig.setValue(value+"");
////
////                }
////            }
//        }
//
//        return notificationConfig;
//    }
//
//    public static Set<String> getGlobalDigestEmailTypes(User user)
//    {
//        Set<String> results = new HashSet<String>();
//
//        List<ComponentNotification> allUserGlobalEmailTypes = getAllNotificationType(false);
//        List<ComponentNotification> globalTypes = getUserGlobalNotificationTypes(user, allUserGlobalEmailTypes);
//
//        if(globalTypes != null)
//        {
//            for(ComponentNotification cn: globalTypes)
//            {
//                List<NotificationConfig> notifyTypes = cn.getNotifyTypes();
//
//                if(notifyTypes != null && notifyTypes.size() == 1)
//                {
//                    NotificationConfig config = notifyTypes.get(0);
//
//                    if(config != null && config.getValue().equalsIgnoreCase("true"))
//                    {
//                        results.add(cn.getRSComponent().getClass().getName());
//                    }
//                }
//            }
//        }
//
//        return results;
//    }
}
