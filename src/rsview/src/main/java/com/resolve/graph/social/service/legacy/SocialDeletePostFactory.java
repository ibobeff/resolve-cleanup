/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.services.graph.social.model.message.Post;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class SocialDeletePostFactory extends SocialFactory
{
    private static Node containerNodeLocal;
    
    public static void init()
    {
        init(SocialRootRelationshipTypes.DELETEDPOST, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.DELETEDPOST.name(), containerNode);
    }
    
    public static Node create(Post post)
    {
        Node node = null;
        Transaction tx = null;
               
        try
        {
            getContainerNode();

            node = findPost(post.getSys_id(), post.getTitleUrl());
            
            if(node == null)
            {
//                tx = GraphDBUtil.getGraphDBService().beginTx();  
//                node = GraphDBUtil.getGraphDBService().createNode();
//                post.convertObjectToNode(node);  
//                node.setProperty(NODE_TYPE, SocialRelationshipTypes.DELETEDPOST.name());
//                containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.DELETEDPOST);
//                tx.success();
            }
        }
        catch(Throwable t)
        {
            Log.log.warn("create deletedPost catched exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
        
        return node;
    }
    
    
    public static Node findPost(String id, String titleUrl)
    {
        Node node = null;
        
        if(!StringUtils.isEmpty(id) && !StringUtils.isEmpty(titleUrl))
        {
            node = ResolveGraphFactory.findNodeByIndexedID(id);
        }
        else if(!StringUtils.isEmpty(id) && StringUtils.isEmpty(titleUrl))
        {
            node = ResolveGraphFactory.findNodeByIndexedID(id);
        }
        else if(!StringUtils.isEmpty(titleUrl) && StringUtils.isEmpty(id))
        {
            node = findNodeByIndexedTitleURL(titleUrl);
        }
            
        return node;
    }
    
    public Node findPostNode(String id, String titleUrl)
    {
        return findPost(id, titleUrl);
    }
    
    public Node findNode(String id)
    {
        return ResolveGraphFactory.findNodeByIndexedID(id);
    }
    
    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.DELETEDPOST.name());
        }
        
        return containerNodeLocal;
    }

}

