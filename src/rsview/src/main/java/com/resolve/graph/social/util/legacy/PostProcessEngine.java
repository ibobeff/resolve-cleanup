/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util.legacy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.search.SearchUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.util.StringUtils;

public class PostProcessEngine
{
    private static final Pattern MENTION_PATTERN = Pattern.compile("((\\s|\\A)@[^\\s]+)", Pattern.DOTALL);
    private static final Pattern MENTION_PATTERN_UI = Pattern.compile("(#RS.user.User/username=[^\"]+)", Pattern.DOTALL);

    public static final String BASE_URL_FOR_USER = "<a target=\"_blank\" href=\"/resolve/jsp/rsclient.jsp#RS.user.User/username=";
//    public static final String BASE_URL_FOR_HASHTAGS = "<a target=\"_blank\" href=\"/resolve/social/rssocial.jsp?queryType=ALL&searchKey=|Hash|";
    public static final String BASE_URL_FOR_HASHTAGS = "<a target=\"_blank\" href=\""+ "/resolve/jsp/rsclient.jsp#RS.search.Main/search=";
    
    //url for hashtag
    //http://localhost:8080/resolve/social/rssocial.jsp?searchKey=|Hash|test&queryType=ALL

    public static final String HTML_BEGIN = StringUtils.HTML_BEGIN;
    public static final String HTML_END = StringUtils.HTML_END;
    
    private String postOrComment = null;
    private String modifiedPostOrComment = "";
    private Set<String> users =  new HashSet<String>();
    private List<RSComponent> mentions  = new ArrayList<RSComponent>();
    
    private Map<String, User> userCache = new HashMap<String, User>();
    private Set<String> tagsCache = new HashSet<String>();
    
    //for place holder
    private Set<String> htmlContent = new HashSet<String>();
    private Map<String, String> htmlPlaceholder = new HashMap<String, String>();
    
    
    
    public PostProcessEngine(String post, boolean parseTag)
    {
        this.postOrComment = post;
        
        //process mentions
        processMentions();        
        
        //process tags
        if(parseTag)
        {
            tagsCache = SearchUtils.findTags(this.postOrComment);
            /* 
             * This following code is incorrect because we have no rights to manipulate user enter text.
             * So what happens now is, if the text includes special character preceded by a #
             * then we simply keep it as plain text.
             * 
             * As we are replacing all special chars from tag to an underscore,
             * entire POST content is replaced accordingly.
             */
/*            if (tagsCache != null && tagsCache.size() > 0)
            {
                for (String tag : tagsCache)
                {
                    String modifiedTag = tag.replaceAll(HibernateConstants.REGEX_TAG_NAME, "_");
                    if (!modifiedTag.equals(tag))
                    {
                        postOrComment = postOrComment.replace(tag, modifiedTag);
                    }
                }
            }
*/        }
        
        //modify the post 
        modifyPost();
        
    }
    
    public List<RSComponent> getPostUserMentions()
    {
        return mentions;
    }
    
    public String getModifiedPostOrComment()
    {
        return modifiedPostOrComment;
    }
    
    //private
    private void modifyPost()
    {
        //massage the Post content
        massagePostOrComments();
                
        modifiedPostOrComment = postOrComment;
        
        //for user cache
        addUserUrls();
    }
    
    
    private void addUserUrls()
    {
        Iterator<String> it = userCache.keySet().iterator();
        while(it.hasNext())
        {
            String key = it.next();
            User user = userCache.get(key);
            String url = HTML_BEGIN + BASE_URL_FOR_USER + user.getName() + "\">@" + user.getDisplayName() + "</a>" + HTML_END;
            
            key = key.replace("[","\\[");
            key = key.replace("]","\\]");
            
            modifiedPostOrComment = modifiedPostOrComment.replaceAll(key, url);
        }
    }
    
//    private void addHashTagsUrl()
//    {
//        for(String tagStr : tagsCache)
//        {
//            //String tag = tagStr.substring(2, tagStr.length() -1);//remove the '#[' and ']'
//            String url = HTML_BEGIN + BASE_URL_FOR_HASHTAGS + tagStr + "\">" + tagStr + " </a>" + HTML_END;
//
//            //tagStr = tagStr.replace("[","\\[");
//            //tagStr = tagStr.replace("]","\\]");
//            
//            modifiedPostOrComment = modifiedPostOrComment.replaceAll(tagStr, url);
//        }
//    }
    
    private void processMentions()
    {
        //set the users 
        setListOfUsers();
        
        for(String user : users)
        {
            //String username = user.substring(2, user.length()-1);//remove the '@[' and ']''
            String username = user.substring(1);//remove the '@'
            RSComponent socialUser = SocialCompConversionUtil.getSocialUser(username);
            if(socialUser != null)
            {
                mentions.add(socialUser);
                userCache.put(user, (User)socialUser);
            }
        }//end of for loop
    }
    
    
    private void setListOfUsers()
    {
        if(StringUtils.isNotBlank(this.postOrComment))
        {
            Matcher matcher = MENTION_PATTERN.matcher(this.postOrComment);

            while (matcher.find())
            {
                String str = matcher.group();
                users.add(str);
            }
            
            //when the post comes from the UI it has a different format
            //e.g., <a href="#RS.user.User/username=john">
            matcher = MENTION_PATTERN_UI.matcher(this.postOrComment);
            while (matcher.find())
            {
                String str = matcher.group();
                if(str != null)
                {
                    String username = str.substring(str.lastIndexOf("=")+1);
                    if(StringUtils.isNotBlank(username))
                    {
                        users.add("@" + username);
                    }
                }
            }
        }
    }//getListOfUsers
    
    private void massagePostOrComments()
    {
        //escape out the <script></script> javascript as its malicious in nature
        this.postOrComment = StringUtils.escapeJavascript(this.postOrComment);
        
        //escape the meta tag
        this.postOrComment = StringUtils.escapeMeta(this.postOrComment);
        
    }
    
   
    
//    public static void main(String[] args)
//    {
////        String post = "sdf asdf as #[jeet] sdf asd asdf a #[marwah] sdf ase";
//        String post = "asdf asd <script>document.test =eee </script> test test ";
//        
//        System.out.println(new PostProcessEngine(post, true).massagePostOrComments(post));
//    }
    
    
} // PostProcessEngine
