/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;


public class GetCatalog
{
//
//    public enum CATALOG_COMPARISION
//    {
//        equals, notEquals, contains, notContains, startsWith, notStartsWith, endsWith, notEndsWith
//    }
//
//    public static ResolveCatalog getCatalogByName(String catalogName) throws Exception
//    {
//        ResolveCatalog result = null;
//        String sysId = null;
//        
//        //first get the sysId for this catalog
//        Iterable<Node> rootNodes = TraversalUtil.findAllRootCatalogs();
//        if(rootNodes != null)
//        {
//            for(Node node : rootNodes)
//            {
//                if(node.hasProperty(ResolveCatalog.DISPLAYNAME))
//                {
//                    String displayName = (String) node.getProperty(ResolveCatalog.DISPLAYNAME);
//                    if(displayName.equalsIgnoreCase(catalogName))
//                    {
//                        sysId = (String) node.getProperty(ResolveCatalog.SYS_ID);
//                        break;
//                    }
//                }
//            }//end of for loop
//
//            //now get the catalog with Id
//            if(sysId != null)
//            {
//                result = getCatalog(sysId);
//            }
//            
//        }
//        
//        return result;
//    }
//    
//    public static boolean isExistingCatalog(String catalogId)
//    {
//        boolean result = false;
//        
//        if (StringUtils.isNotBlank(catalogId))
//        {
//            if (CatalogFactory.findNodeByIndexedID(catalogId) != null)
//            {
//                result = true;
//            }
//        }
//        
//        return result;
//    }
//    
//    public static ResolveCatalog getCatalog(String sysID) throws Exception
//    {
//        ResolveCatalog cRoot = null;
//
//        if (StringUtils.isNotEmpty(sysID))
//        {
//            Node catalogRootNode = CatalogFactory.findNodeByIndexedID(sysID);
//
//            if (catalogRootNode != null)
//            {
//                //catalog root
//                cRoot = convertNodeToObject(catalogRootNode);
//
//                if (cRoot.isRoot())
//                {
//                    TraversalDescription trave = Traversal.description()
//                                    .breadthFirst()
//                                    .relationships(SocialRelationshipTypes.CHILD_OF_CATALOG, Direction.INCOMING)
//                                    .relationships(SocialRelationshipTypes.CATALOG_REF, Direction.INCOMING)
//                                    .relationships(SocialRelationshipTypes.TAG_OF_CAT, Direction.INCOMING)
//                                    .evaluator(Evaluators.excludeStartPosition())
//                                    .evaluator(Evaluators.atDepth(1))
//                                    .evaluator(new Evaluator()
//                    {
//                        @Override
//                        public Evaluation evaluate(Path path)
//                        {
//                            if (path.lastRelationship() != null && path.lastRelationship().getType().equals(SocialRelationshipTypes.CATALOG_REF))
//                            {
//                                return Evaluation.INCLUDE_AND_PRUNE;
//                            }
//
//                            return Evaluation.INCLUDE_AND_CONTINUE;
//                        }
//                    });
//
//                    for (Node childNode : trave.traverse(catalogRootNode).nodes())
//                    {
//                        String childNodeDisplayName = (String) childNode.getProperty(ResolveCatalog.DISPLAYNAME);
//                        String type = (String) childNode.getProperty(ResolveCatalog.TYPE);
//
////                        Log.log.info("-- the childNode -- " + childNodeDisplayName + " type:" + type);
//
//                        if (StringUtils.isNotEmpty(type))
//                        {
//                            if (!type.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                            {
//                                ResolveCatalog childCatalog = convertNodeToObject(childNode);
//
//                                // to get all child catalog
//                                traverseChild(childNode, childCatalog, cRoot, trave);
//
//                            }
////                            else if (type.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
////                            {
////                                ResolveTag tagLocal = new ResolveTag();
////                                tagLocal.convertNodeToObject(childNode);
////                                cRoot.getTags().add(tagLocal);
////
////                                Log.log.info("-- the  cRoot -- " + cRoot.getName() + "-- add tag of --" + tagLocal.getName());
////                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        if (cRoot != null)
//        {
//            sortOnOrder(cRoot.getChildren());
//        }
////        sortOnTag(cRoot.getTags());
//
////        Log.log.info("-- finished retrieve catalog " + ((cRoot == null) ? "" : cRoot.getName()));
//
//        return cRoot;
//    }
//    
//    
//    public static ResponseDTO<ResolveCatalog> getCatalogs(QueryDTO query, String username) throws Exception
//    {
//        return new SearchCatalog(query, username).getCatalogs();
//    }
//    
//    /**
//     * get list of catalogs that a catalog can reference to. Filter out the ones that it is referenced in.
//     * 
//     * @param catalogId
//     * @param catalogName
//     * @param username
//     * @return
//     * @throws Exception
//     */
//    public static ResponseDTO<ResolveCatalog> getRefCatalogs(String catalogId, String catalogName, String username) throws Exception
//    {
//        ResponseDTO<ResolveCatalog> result = new ResponseDTO<ResolveCatalog>();
//        ResolveCatalog currentCatalog = null;
//        
//        if(StringUtils.isNotBlank(catalogId))
//        {
//            currentCatalog = getCatalog(catalogId);
//        }
//        else if(StringUtils.isNotBlank(catalogName))
//        {
//            currentCatalog = getCatalogByName(catalogName);
//        }
//        
//        if(currentCatalog != null)
//        {
//            //get list of all the catalogs
//            result = getCatalogs(null, username);
//            
//            //get list of catalogs that reference this one
//            Set<String> catalogRefNames = getCatalogNamesThatIsReferenceing(currentCatalog.getId());
//            
//            //filter the recs
//            List<ResolveCatalog> catalogResult = result.getRecords();
//            List<ResolveCatalog> filteredResult = new ArrayList<ResolveCatalog>(); 
//            if(catalogResult != null)
//            {
//                for(ResolveCatalog c : catalogResult)
//                {
//                    //make sure that the refering catalog is not there in the drop down
//                    if(!catalogRefNames.contains(c.getName()) && !currentCatalog.getName().equalsIgnoreCase(c.getName()))
//                    {
//                        filteredResult.add(c);
//                    }
//                }//end of for loop
//                
//                result.setRecords(filteredResult);
//                result.setTotal(filteredResult.size());
//            }//end of if
//            
//        }//end of if
//        
//        
//        return result;
//    }
//
//
//    public static List<ResolveCatalog> getAllCatalogs(QueryDTO query) throws Exception
//    {
//        ResponseDTO<ResolveCatalog> response = getCatalogs(query, "system");
//        List<ResolveCatalog> results = response.getRecords();
//        return results;
//    }
//
//
//    //private apis
//    private static void sortOnOrder(List<? extends Object> catalog)
//    {
//        if (!catalog.isEmpty())
//        {
//            Collections.sort(catalog, new Comparator<Object>()
//            {
//                public int compare(final Object cat1, final Object cat2)
//                {
//                    return (((ResolveCatalog) cat1).getOrder() > ((ResolveCatalog) cat2).getOrder() ? 1 : -1);
//                }
//            });
//        }
//    }
//    
//
//    private static ResolveCatalog convertNodeToObject(Node catalogNode)
//    {
//        ResolveCatalog catalog = new ResolveCatalog();
//        catalog.convertNodeToObject(catalogNode);
//        
//        //get the tags for this node
//        List<Tag> tags = getTagsForCatalogItemNode(catalog.getId());
//        catalog.setTags(tags);
//        
//        //TODO: get the document ref displayType="wiki"
////        String displayType = catalog.getDisplayType();
//        if (StringUtils.isNotBlank(catalog.getDisplayType()) 
//                        && (catalog.getDisplayType().equalsIgnoreCase(ResolveCatalog.DISPLAY_TYPE_WIKI) || catalog.getDisplayType().equalsIgnoreCase(ResolveCatalog.DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
//        {
//            String wiki = getDocumentFullNameReferedByCatalogItemNode(catalogNode);
//            if(StringUtils.isNotBlank(wiki))
//            {
//                catalog.setWiki(wiki);
//            }
//        }
//        
//        return catalog;
//    }
//    
//    private static List<Tag> getTagsForCatalogItemNode(String catalogNodeId)
//    {
//        List<Tag> tags = new ArrayList<Tag>();
//        
//        if(StringUtils.isNotBlank(catalogNodeId))
//        {
//            Node catalogNode = ResolveGraphFactory.findNodeByIndexedID(catalogNodeId);
//            if(catalogNode != null)
//            {
//                Iterable<Relationship> rels = catalogNode.getRelationships(SocialRelationshipTypes.TAG_OF_CAT);
//                if (rels != null)
//                {
//                    for (Relationship rel : rels)
//                    {
//                        Node tagNode = rel.getOtherNode(catalogNode);
//                        
//                        String sysId = (String) tagNode.getProperty(RSComponent.SYS_ID);
//                        try
//                        {
////                            Tag tag = ServiceTag.findTagById(sysId, "system");
////                            if(tag != null)
////                            {
////                                tags.add(tag);
////                            }
//                        }
//                        catch (Exception e)
//                        {
//                            Log.log.error("cannot find tag with id :" + sysId + " - name : " + (String) tagNode.getProperty(RSComponent.TAG_NAME), e);
//                        }
//                    }
//                }
//            }
//        }
//        
//        
//        return tags;
//    }
//    
//    private static String getDocumentFullNameReferedByCatalogItemNode(Node catalogNode)
//    {
//        String docFullName = null;
//        
//        if(catalogNode != null)
//        {
////            String path = (String) catalogNode.getProperty(ResolveCatalog.PATH);
//            Iterable<Relationship> rels = catalogNode.getRelationships(SocialRelationshipTypes.CATALOG_DOCUMENT_REF);
//            if(rels != null)
//            {
//                Set<String> docs = new HashSet<String>();
//                for(Relationship rel : rels)
//                {
//                    Node docNode = rel.getOtherNode(catalogNode);
//                    if(docNode != null && docNode.hasProperty(RSComponent.DISPLAYNAME))
//                    {
//                        docs.add((String) docNode.getProperty(RSComponent.DISPLAYNAME));
//                    }
//                }
//
//                docFullName = StringUtils.convertCollectionToString(docs.iterator(), ",");
//            }
//        }
//
//        return docFullName;
//    }
//
//    protected static List<ResolveCatalog> getAllCatalogs() throws Exception
//    {
//        List<ResolveCatalog> results = new ArrayList<ResolveCatalog>();
//
//        Node catalogContainer = CatalogFactory.getContainerNode(CatalogContainerType.Catalog);
//        if (catalogContainer != null)
//        {
//            TraversalDescription trav = Traversal.description()
//                                            .breadthFirst()
//                                            .relationships(CatalogContainerType.Catalog, Direction.OUTGOING)
//                                            .evaluator(Evaluators.excludeStartPosition());
//            
//            for (Node catalogNode : trav.traverse(catalogContainer).nodes())
//            {
//                ResolveCatalog catalog = convertNodeToObject(catalogNode);
//                results.add(catalog);
//            }
//        }
//
//        return results;
//
//    }
//    
//    private static void traverseChild(Node childNode, ResolveCatalog childCatalog, ResolveCatalog parentCatalog, TraversalDescription travChild)
//    {
//
//        List<ResolveCatalog> childChildCatalogList = new ArrayList<ResolveCatalog>();
//        Set<ResolveCatalog> childChildCatalogSet = new HashSet<ResolveCatalog>();
//
//        for (Node childChildNode : travChild.traverse(childNode).nodes())
//        {
//            String type = (String) childChildNode.getProperty(ResolveCatalog.TYPE);
//            String childChildNodeName = (String) childChildNode.getProperty(ResolveCatalog.DISPLAYNAME);
//
////            Log.log.info("--- childChildNode -- " + childChildNodeName);
//
//            
//
//            if (StringUtils.isNotEmpty(type))
//            {
//                if (!type.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                {
//                    ResolveCatalog childChildCatalog = convertNodeToObject(childChildNode);
//
//                    for (Node subChildNode : travChild.traverse(childChildNode).nodes())
//                    {
//                        String typeLocal = (String) subChildNode.getProperty(ResolveCatalog.TYPE);
//
//                        if (StringUtils.isNotEmpty(typeLocal))
//                        {
//                            if (!typeLocal.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                            {
//                                ResolveCatalog subChildCatalog = convertNodeToObject(subChildNode);
//
//                                //** RECURSIVE
//                                traverseChild(subChildNode, subChildCatalog, childChildCatalog, travChild);
//
//                            }
////                            else if (typeLocal.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
////                            {
////                                ResolveTag tagLocal = new ResolveTag();
////                                tagLocal.convertNodeToObject(subChildNode);
////                                childChildCatalog.getTags().add(tagLocal);
////
////                                Log.log.info("-- the childChildNode -- " + childChildCatalog.getName() + " --- add tag -- " + tagLocal.getName());
////                            }
//                        }
//
//                    }
//
//                    sortOnOrder(childChildCatalog.getChildren());
////                    sortOnTag(childChildCatalog.getTags());
//
//                    childChildCatalogSet.add(childChildCatalog);
//
//                }
////                else if (type.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
////                {
////                    ResolveTag tagLocal = new ResolveTag();
////                    tagLocal.convertNodeToObject(childChildNode);
////                    childCatalog.getTags().add(tagLocal);
////                }
//            }
//        }
//
//        childChildCatalogList.addAll(childChildCatalogSet);
//        childCatalog.getChildren().addAll(childChildCatalogList);
//        sortOnOrder(childCatalog.getChildren());
////        sortOnTag(childCatalog.getTags());
//
//        if (parentCatalog != null)
//        {
//            parentCatalog.getChildren().add(childCatalog);
//        }
//
//    }
//
//    /**
//     * get set of catalog sysId that are referencing a particular catalog
//     * 
//     * @param catalogId
//     * @return
//     */
//    private static Set<String> getCatalogNamesThatIsReferenceing(String catalogId)
//    {
//        Set<String> catalogRefSysIds = new HashSet<String>();
//        
//        if(StringUtils.isNotBlank(catalogId))
//        {
//            Node catalogNode = CatalogFactory.findNodeByIndexedID(catalogId);
//            if(catalogNode != null)
//            {
//                Iterable<Relationship> rels = catalogNode.getRelationships(SocialRelationshipTypes.CHILD_OF_CATALOG, Direction.OUTGOING);
//                if (rels != null)
//                {
//                    for (Relationship rel : rels)
//                    {
//                        Node otherReferencedCatalog = rel.getOtherNode(catalogNode);
//                        if(otherReferencedCatalog != null)
//                        {
//                            if(otherReferencedCatalog.hasProperty(ResolveCatalog.TYPE) && ((String)otherReferencedCatalog.getProperty(ResolveCatalog.TYPE)).equalsIgnoreCase(SocialRelationshipTypes.CatalogReference.name()))
//                            {
//                                String path = (String) otherReferencedCatalog.getProperty(ResolveCatalog.PATH); // /C1/C2
//                                if(StringUtils.isNotBlank(path))
//                                {
//                                    String catalogRefName = path.substring(1);//remove the first '/' --> C1/C2
//                                    catalogRefName = catalogRefName.substring(0, catalogRefName.indexOf('/')); // C1
//                                    catalogRefSysIds.add(catalogRefName);
//                                }
//                            }
//                        }//end of if
//                    }//end of for loop
//                }//end of if
//            }//end of if
//        }//end of if
//        
//        return catalogRefSysIds;
//    }
//
}
