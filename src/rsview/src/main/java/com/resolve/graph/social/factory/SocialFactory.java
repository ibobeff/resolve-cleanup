/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.IndexHits;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.message.Post;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public abstract class SocialFactory
{
    public static final String NODE_TYPE = "NODE_TYPE";
    public static final String OUTBOX = "OUTBOX";

    //user global config for notification
    public static final String AT_NOTIFY_ON_SAVE   = "AT_NOTIFY_ON_SAVE";
    public static final String AT_NOTIFY_ON_UPDATE = "AT_NOTIFY_ON_UPDATE";
    public static final String AT_NOTIFY_ON_DELETE = "AT_NOTIFY_ON_DELETE";
    public static final String AT_NOTIFY_ON_COMMIT = "AT_NOTIFY_ON_COMMIT";

    public static final String RB_NOTIFY_ON_SAVE   = "RB_NOTIFY_ON_SAVE";
    public static final String RB_NOTIFY_ON_UPDATE = "RB_NOTIFY_ON_UPDATE";
    public static final String RB_NOTIFY_ON_DELETE = "RB_NOTIFY_ON_DELETE";
    public static final String RB_NOTIFY_ON_COMMIT = "RB_NOTIFY_ON_COMMIT";

    protected static Node containerNode;
    protected static Map<String, Node> allContainerNodes  = new ConcurrentHashMap<String, Node>();

    //abstract apis
//    protected abstract Node getContainerNode();


    @SuppressWarnings("deprecation")
    public static void init(RelationshipType type, Direction direction)
    {

        Transaction tx = null;

        try
        {
            Relationship rel = findRelationship(type, direction);

            if(rel == null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();
                containerNode =  GraphDBUtil.getGraphDBService().createNode();
//                GraphDBUtil.getGraphDBService().getReferenceNode().createRelationshipTo(containerNode, type);
                GraphDBUtil.getReferenceNode().createRelationshipTo(containerNode, type);
                tx.success();
            }
            else
            {
                containerNode = rel.getEndNode();
                Log.log.trace("===== the endNode is : " + containerNode.getId() + "---- for type : " + type.name());
            }
        }
        catch(Throwable t)
        {
            Log.log.warn("SocialUsers create exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }


    /**
     *
     * @param containerNode - MAIN MOTHER node for the component type
     * @param type
     * @return
     */
    public static Set<RSComponent> findAllRsComponentFor(Node containerNode, SocialRelationshipTypes type)
    {
        Set<RSComponent> result = new HashSet<RSComponent>();

        if(containerNode != null)
        {
            //get the container node and get all the nodes that has WORKSHEET relationship
            Iterable<Relationship> typeRels = containerNode.getRelationships(type);
            if(typeRels != null)
            {
                for(Relationship rel : typeRels)
                {
//                    Node node = rel.getOtherNode(containerNode);
//                    if(node.hasProperty(RSComponent.TYPE))
//                    {
//                        RSComponent comp = new RSComponent();
//                        comp.convertNodeToObject(node);
//
//                        result.add(comp);
//                    }
                }
            }
        }

        return result;
    }

    public static Set<RSComponent> findAllRsComponentFor(SocialRelationshipTypes type)
    {
        Set<RSComponent> result = new HashSet<RSComponent>();

        Node containerNode = allContainerNodes.get(type.name());
        if(containerNode != null)
        {
            result.addAll(findAllRsComponentFor(containerNode, type));
        }

        return result;
    }
    
//    public static Set<RSComponent> findAllRsComponentFor(SocialRelationshipTypes type, String searchString)
//    {
//        Set<RSComponent> result = findAllRsComponentFor(type);
//        if (StringUtils.isNotBlank(searchString))
//        {
//            if (result != null && result.size() > 0)
//            {
//                Iterator<RSComponent> i = result.iterator();
//                while (i.hasNext())
//                {
//                    if (!i.next().getName().toLowerCase().startsWith(searchString))
//                    {
//                        i.remove();
//                    }
//                }
//            }
//        }
//        return result;
//    }

    //private apis
    @SuppressWarnings("deprecation")
    private static Relationship findRelationship(RelationshipType type, Direction direction)
    {
        Relationship relLocal = null;

        try
        {

//            Iterable<Relationship> rels = GraphDBUtil.getGraphDBService().getReferenceNode().getRelationships();
            Iterable<Relationship> rels = GraphDBUtil.getReferenceNode().getRelationships();

            for(Relationship rel: rels)
            {
               if(rel.getType().name().equals(type.name()))
               {
                   relLocal = rel;
                   break;
               }
            }
        }
        catch(Exception exp)
        {
            if(exp instanceof NullPointerException)
            {
                Log.log.info("--- the graphdb referenceNode may be null");
            }
            else
            {
                Log.log.info(exp, exp);
            }
        }

        return relLocal;
    }

//    public static void addIndirectFollowRelation(Node userNode, Node compNode, int postUnReadCount) throws Exception
//    {
//        Transaction tx = null;
//
//        try
//        {
//            Relationship indirectFollow = ResolveGraphFactory.findRelationForType(userNode, compNode, RelationType.INDIRECT_FOLLOW);
//            Relationship directFollow = ResolveGraphFactory.findRelationForType(userNode, compNode, RelationType.FOLLOWER);
//
//            tx = GraphDBUtil.getGraphDBService().beginTx();
//            if(indirectFollow == null)
//            {
//                indirectFollow = userNode.createRelationshipTo(compNode, RelationType.INDIRECT_FOLLOW);
//            }
//
//            indirectFollow.setProperty(RSComponent.UNREAD_COUNT, postUnReadCount);
//            if(directFollow != null)//keep the direct follow in sink with the indirect follow
//            {
//                directFollow.setProperty(RSComponent.UNREAD_COUNT, postUnReadCount);
//            }
//
//            tx.success();
//        }
//        catch(Exception t)
//        {
//            Log.log.warn("addIndirectFollowRelation exception: " + t, t);
//            throw t;
//        }
//        finally
//        {
//            if(tx != null)
//            {
//                tx.finish();
//            }
//        }
//    }

    protected static RSComponent updateComponent(RSComponent rscomponent, Node node) throws Exception
    {
        Transaction tx = null;
        try
        {
            if (node != null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();
//                String sys_idLocal = (String) node.getProperty(RSComponent.SYS_ID);
//                String sys_created_byLocal = node.hasProperty(RSComponent.SYS_CREATED_BY) ? (String) node.getProperty(RSComponent.SYS_CREATED_BY) : rscomponent.getSys_created_by();
//                Long sys_created_on = node.hasProperty(RSComponent.SYS_CREATED_ON) ? (Long) node.getProperty(RSComponent.SYS_CREATED_ON) : rscomponent.getSys_created_on();
//
//                rscomponent.convertObjectToNode(node);
//
//                node.setProperty(RSComponent.SYS_ID, sys_idLocal);
//                node.setProperty(RSComponent.SYS_CREATED_BY, sys_created_byLocal);
//                node.setProperty(RSComponent.SYS_CREATED_ON, sys_created_on);

                tx.success();
            }
        }
        catch (Exception t)
        {
            Log.log.info(t.getMessage(), t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return rscomponent;
    }
    
    protected static void deleteNodes(Set<String> dbSysIds, Iterable<Relationship> rels)
    {
        if(rels != null && dbSysIds != null)
        {
            Set<Node> nodesToDelete = new HashSet<Node>();
            Set<String> names = new HashSet<String>();
            String type = null;
            
            for(Relationship rel: rels)
            {
                Node endNode = rel.getEndNode();
//                String sysId = endNode.hasProperty(RSComponent.SYS_ID) ? (String) endNode.getProperty(RSComponent.SYS_ID): null;
//                type = (String) endNode.getProperty(RSComponent.TYPE);
//                
//                if(StringUtils.isNotBlank(sysId) && dbSysIds.contains(sysId))
//                {
//                    //don't delete
//                }
//                else
//                {
//                    //delete it
//                    nodesToDelete.add(endNode);
//                    names.add(sysId);
//                }
            }//end of for
            
            
            if(nodesToDelete.size() > 0)
            {
                Log.log.debug("Deleteing Nodes: type: " + type);
                Log.log.debug("Deleteing Nodes: type: " + StringUtils.convertCollectionToString(names.iterator(), ","));
                
                try
                {
                    ResolveGraphFactory.deleteNodes(nodesToDelete);
                }
                catch (Exception e)
                {
                    Log.log.warn("Error deleting some of the nodes", e);
                }
            }
        }
        
        
        
    }


















    ////////////////////////////////////////// LEGACY




//TODO: Wait till Bipul is done with search prototype and we decide to remove the Post from Graph all-together
    public static Node findNodeByIndexedTitleURL(String titleUrl)
    {
        Node nodeLocal = null;
        IndexHits<Node> nodes = GraphDBUtil.getAutoNodeIndex().get(Post.TITLE_URL, titleUrl);

        for(Node node: nodes)
        {
            if(node.hasProperty(Post.TITLE_URL))
            {
                if(node.getProperty(Post.TITLE_URL).equals(titleUrl))
                {
                    nodeLocal = node;
                    break;
                }
            }
        }

        return nodeLocal;
    }

    public static Node findNodeByIndexedID(String id)
    {
        Node nodeLocal = null;
        
        if(StringUtils.isNotEmpty(id))
        {
//            IndexHits<Node> nodes = GraphDBUtil.getAutoNodeIndex().get(RSComponent.SYS_ID, id);
//            
//            for(Node node: nodes)
//            {
//                if(node.getProperty(RSComponent.SYS_ID).equals(id))
//                {
//                    nodeLocal = node;
//                    break;
//                }
//            }
        }
        
        return nodeLocal;
    }


//  //TODO: Wait till Bipul is done with search prototype and we decide to remove the Post from Graph all-together
//    public static void deleteNotificationRelation(Node thisNode, Node otherNode,
//                                                  List<NotificationConfig> types)
//                                                  throws Exception
//    {
//        UserGlobalNotificationContainerType typeLocal = null;
//
//        try
//        {
//            if(thisNode != null && otherNode != null)
//            {
//                if(types != null && !types.isEmpty())
//                {
//                    for(NotificationConfig type: types)
//                    {
//                        typeLocal = type.getType();
//
//                        if(typeLocal != null)
//                        {
//                            Iterable<Relationship> rels = thisNode.getRelationships(typeLocal);
//
//                            if(rels != null)
//                            {
//                                for(Relationship rel: rels)
//                                {
//                                    Node endNode = rel.getEndNode();
//
//                                    if(endNode != null
//                                           && endNode.getProperty(RSComponent.SYS_ID).equals(otherNode.getProperty(RSComponent.SYS_ID)))
//                                    {
//                                        rel.delete();
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Exception t)
//        {
//            Log.log.warn("deleteRelation catched exception: " + t, t);
//            throw t;
//        }
//    }



//    //TODO: Wait till Bipul is done with search prototype and we decide to remove the Post from Graph all-together
//    public static void addNotificationRelation(Node thisNode,
//                                               Node otherNode,
//                                               List<NotificationConfig> types) throws Exception
//    {
//        Transaction tx = null;
//        String localValueString = "";
//        UserGlobalNotificationContainerType localType = null;
//
//        try
//        {
//            if(types != null && !types.isEmpty())
//            {
//
//                Relationship relLocal = null;
//                tx = GraphDBUtil.getGraphDBService().beginTx();
//
//                for(NotificationConfig type: types)
//                {
//                    localType = type.getType();
//                    localValueString = String.valueOf(type.getValue());
//
//                    relLocal = ResolveGraphFactory.findRelationForType(thisNode, otherNode, localType);
//
//                    if(relLocal == null)
//                    {
//                        Relationship rel = thisNode.createRelationshipTo(otherNode, localType);
//                        rel.setProperty(ResolveGraphFactory.NOTIFICATIONVALUE, localValueString);
//                    }
//                    else
//                    {
//                        //just update the value
//                        relLocal.setProperty(ResolveGraphFactory.NOTIFICATIONVALUE, localValueString);
//                    }
//                }
//
//                tx.success();
//            }
//        }
//        catch(Exception t)
//        {
//            Log.log.warn("addNotificationRelation catched exception: " + t, t);
//            throw t;
//        }
//        finally
//        {
//            if(tx != null)
//            {
//                tx.finish();
//            }
//        }
//    }

    public static void addRelationWithProperty(Node thisNode, Node otherNode, RelationshipType relationType, String propValue) throws Exception
    {
        Transaction tx = null;

        try
        {
            Relationship relLocal = ResolveGraphFactory.findRelationForType(thisNode, otherNode, relationType);
            tx = GraphDBUtil.getGraphDBService().beginTx();

            if(relLocal == null)
            {
                relLocal = thisNode.createRelationshipTo(otherNode, relationType);
            }

            relLocal.setProperty(ResolveGraphFactory.NOTIFICATIONVALUE, propValue);
            tx.success();

        }
        catch(Exception t)
        {
            Log.log.warn("addRelation exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

//    //TODO: Wait till Bipul is done with search prototype and we decide to remove the Post from Graph all-together
//    public static void addRelationForOutbox(Node thisNode, Node otherNode, RelationType relationType, String propValue) throws Exception
//    {
//
//        Transaction tx = null;
//
//        try
//        {
//            if(!findRelationForOutbox(thisNode, otherNode, relationType))
//            {
//                tx = GraphDBUtil.getGraphDBService().beginTx();
//                Relationship rel = thisNode.createRelationshipTo(otherNode, relationType);
//                rel.setProperty(OUTBOX, propValue);
//                tx.success();
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.warn("addRelation exception: " + t.getMessage(), t);
//            throw new Exception(t.getMessage());
//        }
//        finally
//        {
//            if(tx != null)
//            {
//                tx.finish();
//            }
//        }
//    }
//
//
//    //TODO: Wait till Bipul is done with search prototype and we decide to remove the Post from Graph all-together
//    public static void updatePostNodeTargetLastActivityOn(Node postNode)
//    {
//        try
//        {
//            if(postNode != null)
//            {
//
//                TraversalDescription trav = Traversal.description()
//                                                .breadthFirst()
//                                                .relationships(RelationType.TARGET, Direction.OUTGOING)
//                                                .evaluator(Evaluators.excludeStartPosition())
//                                                .evaluator(Evaluators.atDepth(1));
//
//                for(Node targetNode: trav.traverse(postNode).nodes())
//                {
//                    ResolveGraphFactory.updateLastActivityOnForNode(targetNode);
//                }
//
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//    }




//  //TODO: Wait till Bipul is done with search prototype and we decide to remove the Post from Graph all-together
//    private static boolean findRelationForOutbox(Node thisNode, Node otherNode, RelationType relationType)
//    {
//        boolean result = false;
//
//        if(thisNode != null && otherNode != null)
//        {
//            Iterable<Relationship> rels = thisNode.getRelationships(relationType);
//
//            if(rels != null)
//            {
//                for(Relationship rel: rels)
//                {
//                    Node endNode = rel.getEndNode();
//
//                    if(endNode != null
//                             && endNode.getProperty(RSComponent.SYS_ID).equals(otherNode.getProperty(RSComponent.SYS_ID))
//                             && rel.getProperty(OUTBOX).equals(OUTBOX))
//                    {
//                        result = true;
//                        break;
//                    }
//                }
//            }
//        }
//
//        return result;
//    }

    public static void reset()
    {
        Log.log.debug("Reseting... ");
        if(allContainerNodes != null)
        {
            for(String key : allContainerNodes.keySet())
            {
                Log.log.debug("Here is a node with key: " + key + " and the ID: " + allContainerNodes.get(key).getId());
            }
            allContainerNodes.clear();
        }
        if(containerNode != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNode.getId());
            containerNode = null;
        }
    }
}
