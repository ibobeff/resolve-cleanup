/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.util.Log;


public class SocialActionTaskFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.ACTIONTASKS, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.ACTIONTASKS.name(), containerNode);
    }

    public static ActionTask insertOrUpdateProcess(ActionTask actiontask, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(actiontask.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                actiontask = insert(actiontask);
//            }
//            else
//            {
//                //update
//                actiontask = (ActionTask) updateComponent(actiontask, docNode);
//            }
        }
        catch(Exception t)
        {
            Log.log.warn("create ActionTask catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
         return actiontask;
    }

    public static Set<RSComponent> findAllActionTasks()
    {
        Node containerNode = getContainerNode();
        return findAllRsComponentFor(containerNode, SocialRelationshipTypes.ACTIONTASK);
    }
    
    public static void syncGraphToDB(Set<String> dbSysIds)
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.ACTIONTASKS.name());
        }
        
        if(dbSysIds == null)
        {
            //to avoid the null pointer
            dbSysIds = new HashSet<String>();
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.ACTIONTASK);
            deleteNodes(dbSysIds, rels);
        }
    }


    private static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.ACTIONTASKS.name());
        }

        return containerNodeLocal;
    }

    private static ActionTask insert(ActionTask actiontask) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

            //insert
            tx = GraphDBUtil.getGraphDBService().beginTx();

            Node node = GraphDBUtil.getGraphDBService().createNode();
//            actiontask.convertObjectToNode(node);
//            //                node.setProperty(NODE_TYPE, SocialObjectType.DOCUMENT.name());
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.ACTIONTASK);
//
//            tx.success();
//
//            //update the object
//            actiontask.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create ActionTask catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return actiontask;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }
}
