/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;


public class CatalogUtil
{
    /**
     * assign a document to a path of a catalog
     *
     * @param doc
     * @param path
     * @param username
     * @throws Exception
     */
//    public static void updateCatalogItemPathsToDocument(Document doc, Set<String> path, String username) throws Exception
//    {
//        if(doc != null && path != null && StringUtils.isNotEmpty(username))
//        {
//            //first check for the doc node
//            Node documentNode = ResolveGraphFactory.findNodeByIndexedID(doc.getSys_id());
//            if(documentNode == null)
//            {
//                throw new Exception("Node for document " + doc.getDisplayName() + " does not exist. Please edit and save again.");
//            }
//            
//            //delete the existing tag relationships first
//            Iterable<Relationship> rels = documentNode.getRelationships(SocialRelationshipTypes.DOCUMENT_TO_CATALOG_REF);
//            if(rels != null)
//            {
//                for(Relationship rel : rels)
//                {
//                    ResolveGraphFactory.deleteRelation(rel);
//                }
//            }            
//            
//            //create new ones
//            for(String p : path)
//            {
//                assignDocumentToCatalogNode(documentNode, p, username);
//            }
//        }
//    }
//    
//    public static void addAdditionalCatalogItemPathsToDocument(Document doc, Set<String> paths, String username) throws Exception
//    {
//        if(doc != null && paths != null && paths.size() > 0  && StringUtils.isNotEmpty(username))
//        {
//            //first check for the doc node
//            Node documentNode = ResolveGraphFactory.findNodeByIndexedID(doc.getSys_id());
//            if(documentNode == null)
//            {
//                throw new Exception("Node for document " + doc.getDisplayName() + " does not exist. Please edit and save again.");
//            }
//
//            //create new ones
//            for(String p : paths)
//            {
//                assignDocumentToCatalogNode(documentNode, p, username);
//            }
//        }
//        
//    }
//    
//    
//    public static Set<String> getCatalogNodesForWiki(Document doc) throws Exception
//    {
//        Set<String> paths = new HashSet<String>();
//        
//        if(doc != null)
//        {
//            Node documentNode = ResolveGraphFactory.findNodeByIndexedID(doc.getSys_id());
//            if(documentNode != null)
//            {
//                Iterable<Relationship> rels = documentNode.getRelationships(SocialRelationshipTypes.DOCUMENT_TO_CATALOG_REF);
//                if(rels != null)
//                {
//                    for(Relationship rel : rels)
//                    {
//                        Node catalogItemNode = rel.getOtherNode(documentNode);
//                        if(catalogItemNode.hasProperty(ResolveCatalog.PATH))
//                        {
//                            String path = (String) catalogItemNode.getProperty(ResolveCatalog.PATH);
//                            if(StringUtils.isNotBlank(path))
//                            {
//                                paths.add(path);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return paths;
//    }
//    
//    public static Set<RSComponent> getWikiComponetsForPath(String path, List<String> tags) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//        
//        Node catalogItemNode = getCatalogItemNodeForPath(path);
//        if(catalogItemNode == null)
//        {
//            throw new Exception("Node with path " + path + " does not exist.");            
//        }
//        
//        //get all the nodes that are related to document
//        result.addAll(findAllDocumentReferecedByThisCatalogItemNode(catalogItemNode));
//        
//        //get all the tag nodes and than the document nodes that tag is assigned to
//        result.addAll(findAllDocumentReferecedByTagReferencedByThisCatalogItemNode(catalogItemNode));
//        
//        //get all the documents from this tags list
//        if(tags != null)
//        {
//            for(String tag : tags)
//            {
//                result.addAll(findAllDocumentsReferencedByTag(tag));
//            }
//        }
//        
//        return result;
//    }
//   
//    public static void assignWikiToCatalogItemNode(Node documentNode, Node catalogNode) throws Exception
//    {
//        if(documentNode != null && catalogNode != null)
//        {
//            //add the relationship
//            ResolveGraphFactory.addRelation(documentNode, catalogNode, SocialRelationshipTypes.CATALOG_DOCUMENT_REF);
//        }
//    }
//
//    
//    //private api
//    /**
//     * 
//     * @param doc
//     * @param path --> /Intro/Chapter 1 , 
//     * @param username
//     * @throws Exception
//     */
//    private static void assignDocumentToCatalogNode(Node documentNode, String path, String username) throws Exception
//    {
//        if (documentNode != null && StringUtils.isNotBlank(path) && StringUtils.isNotBlank(username))
//        {
//            Node catalogItemNode = getCatalogItemNodeForPath(path);
//
//            //add the relationship
//            //                    assignWikiToCatalogItemNode(documentNode, catalogItemNode);
//            if (documentNode != null && catalogItemNode != null)
//            {
//                //add the relationship
//                ResolveGraphFactory.addRelation(documentNode, catalogItemNode, SocialRelationshipTypes.DOCUMENT_TO_CATALOG_REF);
//            }
//        }
//    }
//    
//    private static Node getCatalogItemNodeForPath(String path) throws Exception
//    {
//        Node catalogItemNode = null;
//        
//        ResolveCatalog catalogItem = getCatalogItemForPath(path);
//        if(catalogItem != null)
//        {
//            catalogItemNode = ResolveGraphFactory.findNodeByIndexedID(catalogItem.getId()); 
//        }
//        
//        
//        return catalogItemNode;
//    }
//    
//    private static ResolveCatalog getCatalogItemForPath(String path) throws Exception
//    {
//        ResolveCatalog result = null;
//        
//        if(StringUtils.isNotBlank(path))
//        {
//            String[] arr = path.split("/");
//            String catalogRootName = arr[1];//0 will be empty string
//            
//            ResolveCatalog catalog = GetCatalog.getCatalogByName(catalogRootName);
//            if(catalog != null)
//            {
//                result = catalog.findCatalogNodeWithPath(path);
//            }
//        }
//        
//        
//        return result;
//    }
//    
//    
//    private static Set<RSComponent> findAllDocumentReferecedByThisCatalogItemNode(Node catalogItemNode) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//        
//      //get all the nodes that are related to document
//        Iterable<Relationship> rels = catalogItemNode.getRelationships(SocialRelationshipTypes.CATALOG_DOCUMENT_REF, SocialRelationshipTypes.DOCUMENT_TO_CATALOG_REF);
//        if(rels != null)
//        {
//            for(Relationship rel : rels)
//            {
//                Node documentNode = rel.getOtherNode(catalogItemNode);
//                if(documentNode != null && documentNode.hasProperty(RSComponent.TYPE))
//                {
//                    String type = (String) documentNode.getProperty(RSComponent.TYPE);
//                    if(type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//                    {
//                        RSComponent doc = new RSComponent();
//                        doc.convertNodeToObject(documentNode);
//                        
//                        result.add(doc);
//                    }
//                }
//            }
//        }
//        
//        return result;
//    }
//    
//    private static Set<RSComponent> findAllDocumentReferecedByTagReferencedByThisCatalogItemNode(Node catalogItemNode) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//        
//      //get all the nodes that are related to document
//        Iterable<Relationship> rels = catalogItemNode.getRelationships(SocialRelationshipTypes.TAG_OF_CAT);
//        if(rels != null)
//        {
//            for(Relationship rel : rels)
//            {
//                Node tagNode = rel.getOtherNode(catalogItemNode);
//                if(tagNode != null && tagNode.hasProperty(ResolveCatalog.TYPE))
//                {
//                    String type = (String) tagNode.getProperty(ResolveCatalog.TYPE);
//                    if(type.equalsIgnoreCase(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                    {
//                       //for this tagNode, get the documents refered by it
//                        result.addAll(findAllDocumentsReferencedByTag(tagNode));
//                    }
//                }
//            }
//        }
//        
//        return result;
//    }
//    
//    private static Set<RSComponent> findAllDocumentsReferencedByTag(Node tagNode) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//        
//        if(tagNode != null)
//        {
//            Iterable<Relationship> rels = tagNode.getRelationships(ResolveRelationshipType.ResolveTag);
//            if(rels != null)
//            {
//                for(Relationship rel : rels)
//                {
//                    Node documentNode = rel.getOtherNode(tagNode);
//                    if(documentNode != null && documentNode.hasProperty(RSComponent.TYPE))
//                    {
//                        String type = (String) documentNode.getProperty(RSComponent.TYPE);
//                        if(type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//                        {
//                            RSComponent doc = new RSComponent();
//                            doc.convertNodeToObject(documentNode);
//                            
//                            result.add(doc);
//                        }
//                    }
//                }
//            }
//        }
//        
//        return result;
//    }
//    
//    private static Set<RSComponent> findAllDocumentsReferencedByTag(String tagId) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//        
//        if(StringUtils.isNotEmpty(tagId))
//        {
//            Node tagNode = ResolveGraphFactory.findNodeByIndexedID(tagId);
//            if(tagNode != null)
//            {
//                result.addAll(findAllDocumentsReferencedByTag(tagNode));
//            }
//        }
//        
//        
//        return result;
//    }
    
}
