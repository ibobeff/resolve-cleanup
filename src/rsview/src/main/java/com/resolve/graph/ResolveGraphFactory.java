/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.DateTime;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.IndexHits;

import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class ResolveGraphFactory
{
//    public static final String NODE_TYPE = "NODE_TYPE";
//    public static final String OUTBOX = "OUTBOX";
//    public static final String NOTIFICATIONTYPE = "NOTIFICATIONTYPE";
    public static final String NOTIFICATIONVALUE = "NOTIFICATIONVALUE";

    //user global config for notification
//    public static final String AT_NOTIFY_ON_SAVE   = "AT_NOTIFY_ON_SAVE";
//    public static final String AT_NOTIFY_ON_UPDATE = "AT_NOTIFY_ON_UPDATE";
//    public static final String AT_NOTIFY_ON_DELETE = "AT_NOTIFY_ON_DELETE";
//    public static final String AT_NOTIFY_ON_COMMIT = "AT_NOTIFY_ON_COMMIT";
//
//    public static final String RB_NOTIFY_ON_SAVE   = "RB_NOTIFY_ON_SAVE";
//    public static final String RB_NOTIFY_ON_UPDATE = "RB_NOTIFY_ON_UPDATE";
//    public static final String RB_NOTIFY_ON_DELETE = "RB_NOTIFY_ON_DELETE";
//    public static final String RB_NOTIFY_ON_COMMIT = "RB_NOTIFY_ON_COMMIT";

    //protected static Node containerNode;
    protected static Map<String, Node> allContainerNodes  = new ConcurrentHashMap<String, Node>();


    //create container node for the type
    @SuppressWarnings("deprecation")
    public static void init(RelationshipType type, Direction direction)
    {
        Transaction tx = null;

        try
        {
            Relationship rel = findRelationship(type, direction);
            Node containerNode = null;

            if(rel == null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();
                containerNode =  GraphDBUtil.getGraphDBService().createNode();
//                GraphDBUtil.getGraphDBService().getReferenceNode().createRelationshipTo(containerNode, type);
                GraphDBUtil.getReferenceNode().createRelationshipTo(containerNode, type);
                tx.success();
            }
            else
            {
                containerNode = rel.getEndNode();
            }

            allContainerNodes.put(type.name(), containerNode);

            Log.log.info("-- finished init root container -- " + type.name());
        }
        catch(Throwable t)
        {
            Log.log.warn("catalog builder create exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

    public static Node createNode(ResolveGraphNode nodeObj, RelationshipType containerType) throws Exception
    {
        Node resultNode = null;
        Transaction tx = null;
        Node containerNodeLocal = null;

        try
        {
            if(StringUtils.isNotEmpty(nodeObj.getSys_id()))
            {
                resultNode = findNodeByIndexedID(nodeObj.getSys_id().toLowerCase());
            }

            if(resultNode == null)
            {

                tx = GraphDBUtil.getGraphDBService().beginTx();

                //create the node
                resultNode = GraphDBUtil.getGraphDBService().createNode();
//                nodeObj.convertObjectToNode(resultNode);

                //add the node to the container
                containerNodeLocal = allContainerNodes.get(containerType.name());
                if (containerNodeLocal != null)
                {
                    containerNodeLocal.createRelationshipTo(resultNode, containerType);
                }

                tx.success();
            }
            else
            {
                Log.log.error("Node already exist. So no update is done ");
            }
        }
        catch(Exception t)
        {
            Log.log.error("create node exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }

        return resultNode;
    }

    public static Node insertOrUpdateNode(ResolveGraphNode nodeObj, RelationshipType containerType) throws Exception
    {
        Node resultNode = null;
        Transaction tx = null;
        Node containerNodeLocal = null;

        try
        {
            if(StringUtils.isNotEmpty(nodeObj.getSys_id()))
            {
                resultNode = findNodeByIndexedID(nodeObj.getSys_id());
            }

            if(resultNode != null)
            {
                //update
                tx = GraphDBUtil.getGraphDBService().beginTx();
//                nodeObj.convertObjectToNode(resultNode);
                tx.success();
            }
            else
            {
                //insert
                tx = GraphDBUtil.getGraphDBService().beginTx();

                //create the node
                resultNode = GraphDBUtil.getGraphDBService().createNode();
//                nodeObj.convertObjectToNode(resultNode);

                //add the node to the container
                containerNodeLocal = allContainerNodes.get(containerType.name());
                if (containerNodeLocal != null)
                {
                    containerNodeLocal.createRelationshipTo(resultNode, containerType);
                }

                tx.success();
            }
        }
        catch(Exception t)
        {
            Log.log.error("create node exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }

        return resultNode;
    }



    @SuppressWarnings("deprecation")
    public static Relationship findRelationship(RelationshipType type, Direction direction)
    {
        Relationship relLocal = null;

        try
        {
//            Iterable<Relationship> rels = GraphDBUtil.getGraphDBService().getReferenceNode().getRelationships();
            Iterable<Relationship> rels = GraphDBUtil.getReferenceNode().getRelationships();

            for(Relationship rel: rels)
            {
               if(rel.getType().name().equals(type.name()))
               {
                   relLocal = rel;
                   break;
               }
            }
        }
        catch(Exception exp)
        {
            if(exp instanceof NullPointerException)
            {
                Log.log.info("--- the graphdb findRelationship may be null");
            }
            else
            {
                Log.log.info(exp, exp);
            }
        }

        return relLocal;
    }

    public static Node findNodeByIndexedID(String id)
    {
        Node nodeLocal = null;

        if(StringUtils.isNotEmpty(id))
        {
            IndexHits<Node> nodes = GraphDBUtil.getAutoNodeIndex().get("sys_id", id);
            for(Node node: nodes)
            {
                if(node.getProperty("sys_id").equals(id))
                {
                    nodeLocal = node;
                    break;
                }
            }
        }

        return nodeLocal;
    }

    public static Node getContainerNode(RelationshipType type)
    {
        Node result = null;

        if(type != null)
        {
            result = allContainerNodes.get(type.name());
        }

        return result;
    }

    public static void deleteNode(Node node) throws Exception
    {
        Transaction tx = null;

        try
        {
            if (node != null && node != null)
            {
                boolean hasRels = node.hasRelationship();

                tx = GraphDBUtil.getGraphDBService().beginTx();

                //delete the relationships first, else the commit will throw exception
                if(hasRels)
                {
                    Iterable<Relationship> rels = node.getRelationships();
                    if (rels != null)
                    {
                        for (Relationship rel : rels)
                        {
                            rel.delete();
                        }
                    }
                }

                //delete the node
                node.delete();

                tx.success();
            }

        }
        catch (Exception t)
        {
            Log.log.warn("deleteNode exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }
    
    
    public static void deleteNodes(Collection<Node> nodesToDelete) throws Exception
    {
        Transaction tx = null;

        try
        {
            if (nodesToDelete != null && nodesToDelete.size() > 0)
            {
                
                tx = GraphDBUtil.getGraphDBService().beginTx();

                //delete all the nodes in one transaction
                for(Node node : nodesToDelete)
                {
                    boolean hasRels = node.hasRelationship();

                    //delete the relationships first, else the commit will throw exception
                    if(hasRels)
                    {
                        Iterable<Relationship> rels = node.getRelationships();
                        if (rels != null)
                        {
                            for (Relationship rel : rels)
                            {
                                rel.delete();
                            }
                        }
                    }

                    //delete the node
                    node.delete();
                }
                
                tx.success();
            }

        }
        catch (Exception t)
        {
            Log.log.warn("deleteNodes exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

    public static void deleteRelation(Node thisNode, Node otherNode, RelationshipType relationType) throws Exception
    {

        Transaction tx = null;

        try
        {
            if(thisNode != null && otherNode != null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();

                Iterable<Relationship> rels = thisNode.getRelationships(relationType);
                if(rels != null)
                {
                    for(Relationship rel: rels)
                    {
                        Node endNode = rel.getEndNode();

                        if(endNode != null
//                                 && endNode.getProperty(ResolveCatalog.SYS_ID).equals(otherNode.getProperty(ResolveCatalog.SYS_ID))
                                        )
                        {
                            rel.delete();
                            break;
                        }
                    }
                }
                tx.success();
            }


        }
        catch(Exception t)
        {
            Log.log.warn("deleteRelation catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

    public static void deleteAllRelationforNode(Node node, RelationshipType relationType) throws Exception
    {
        Transaction tx = null;

        try
        {
            if(node != null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();

                Iterable<Relationship> rels = node.getRelationships(relationType);
                if (rels != null)
                {
                    for (Relationship rel : rels)
                    {
                        rel.delete();
                    }
                }

                tx.success();
            }
        }
        catch(Exception t)
        {
            Log.log.warn("deleteRelation catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }


    public static void deleteRelation(Relationship rel) throws Exception
    {
        Transaction tx = null;

        try
        {
            if (rel != null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();
                rel.delete();
                tx.success();
            }

        }
        catch (Exception t)
        {
            Log.log.warn("Error in deleting relation: " + rel.getId(), t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }
    }

    public static void addRelation(Node thisNode, Node otherNode, RelationshipType relationType) throws Exception
    {
        Transaction tx = null;

        try
        {
            if(!ResolveGraphFactory.findRelation(thisNode, otherNode, relationType))
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();
                thisNode.createRelationshipTo(otherNode, relationType);

                tx.success();
            }
        }
        catch(Exception t)
        {
            Log.log.warn("addRelation exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

    public static boolean findRelation(Node thisNode, Node otherNode, RelationshipType relationType)
    {
        boolean result = false;

        if(thisNode != null && otherNode != null)
        {
            //first make sure had relationship
            boolean hasRel = thisNode.hasRelationship(relationType, Direction.OUTGOING);

            if(hasRel)
            {
                Iterable<Relationship> rels = thisNode.getRelationships(relationType, Direction.OUTGOING);

                if(rels != null)
                {
                    for(Relationship rel: rels)
                    {
                        Node endNode = rel.getEndNode();

                        if(endNode != null
                                 && endNode.getProperty("sys_id").equals(otherNode.getProperty("sys_id")))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
        }

        return result;
    }

    public static Relationship findRelationForType(Node thisNode, Node otherNode, RelationshipType relationType)
    {
        Relationship foundRel = null;

        if(thisNode != null && otherNode != null)
        {
            Iterable<Relationship> rels = thisNode.getRelationships(relationType);

            if(rels != null)
            {
                for(Relationship rel: rels)
                {
                    Node endNode = rel.getEndNode();

                    if(endNode != null
                             && endNode.getProperty("sys_id").equals(otherNode.getProperty("sys_id")))
                    {
                        foundRel = rel;
                        break;
                    }
                }
            }
        }

        return foundRel;
    }

    public static void updateLastActivityOnForNode(Node targetNode)
    {
        Transaction tx = null;

        try
        {
            tx = GraphDBUtil.getGraphDBService().beginTx();

            if(targetNode != null)
            {
                long lastActivityOn = DateTime.now().getMillis();//DateManipulator.GetUTCDateLong();
                targetNode.setProperty(ResolveGraphNode.LAST_ACTIVITY_ON, lastActivityOn);
                tx.success();
            }
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

    public static void updatePropertyOnRelationship(String propertyName, String propertyValue, Relationship rel)
    {
        Transaction tx = null;

        try
        {
            tx = GraphDBUtil.getGraphDBService().beginTx();
            rel.setProperty(propertyName, propertyValue);
            tx.success();
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

    public static void reset()
    {
        Log.log.debug("Reseting... ");
        if(allContainerNodes != null)
        {
            for(String key : allContainerNodes.keySet())
            {
                Log.log.debug("Here is a node with key: " + key + " and the ID: " + allContainerNodes.get(key).getId());
            }
            allContainerNodes.clear();
        }
    }
}
