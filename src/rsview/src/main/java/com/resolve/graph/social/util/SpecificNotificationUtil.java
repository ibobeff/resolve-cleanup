/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Node;

import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.NotificationTypeEnum;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SpecificNotificationUtil
{
    private static final String GLOBAL_DEFAULT = "Global Default";
    private static final String SELECT_TRUE = "True";
    private static final String SELECT_FALSE = "False";
    
    private Node streamNode = null;
    private Node userNode = null;
    private String streamType = null;
    private String streamDisplayName = null;
    private EnumSet<NotificationTypeEnum> enumSetNotificationType = null;
    private Set<UserGlobalNotificationContainerType> validNotificationTypes = new HashSet<UserGlobalNotificationContainerType>(); 
    private Map<UserGlobalNotificationContainerType, String> userCurrentSettings = null;
//    private User user = null;
    
    public SpecificNotificationUtil(String streamId, User user) throws Exception
    {
        if(StringUtils.isEmpty(streamId) || user == null)
        {
            throw new Exception("StreamId and user are mandatory ");
        }
        
//        this.streamNode = ResolveGraphFactory.findNodeByIndexedID(streamId);
//        if(streamNode == null)
//        {
//            throw new Exception("stream with id " + streamId + " does not exist.");
//        }
//        if(!streamNode.hasProperty(SocialFactory.NODE_TYPE))
//        {
//            throw new Exception("Node type for stream " + streamNode.getProperty(RSComponent.DISPLAYNAME) + " is missing");
//        }
//        
//        this.userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSysId());
//        if(userNode == null)
//        {
//            throw new Exception("Node for User " + user.getDisplayName() + " does not exist.");
//        }
//        
//        //type of stream
//        streamType = (String) streamNode.getProperty(SocialFactory.NODE_TYPE);
//        streamDisplayName = (String) streamNode.getProperty(RSComponent.DISPLAYNAME);
//        enumSetNotificationType = SocialNotification.findNotificationTypesForType(SocialRelationshipTypes.valueOf(streamType), true); 
//        validNotificationTypes.addAll(NotificationTypeEnum.prepareNotificationTypes(enumSetNotificationType));
//        this.user = user;
    }
    
    public ComponentNotification getSpecificNotifications()  throws Exception
    {
        //get the specific settings of the user with this node
        if(userCurrentSettings == null)
        {
            userCurrentSettings = getUserCurrentSpecificSettings();
        }

        //list of notifications available for this type of component
        List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>();
        if (enumSetNotificationType != null)
        {
            for (NotificationTypeEnum typeEnum : enumSetNotificationType)
            {
                notifyTypes.add(getSpecificNotification(typeEnum));
            }//end of for loop
        }


        //collect all the data and prepare the object
        ComponentNotification compNotify = new ComponentNotification();
        compNotify.setCompType(NodeType.valueOf(streamType));
        compNotify.setNotifyTypes(notifyTypes);
        compNotify.setComponentDisplayName(streamDisplayName);
        
        return compNotify;
    }
    
    private NotificationConfig getSpecificNotification(NotificationTypeEnum typeEnum)
    {
        UserGlobalNotificationContainerType typeOfNotification = typeEnum.getType();
        if(userCurrentSettings == null)
        {
            userCurrentSettings = getUserCurrentSpecificSettings();
        }

        String value = userCurrentSettings.containsKey(typeOfNotification) ? userCurrentSettings.get(typeOfNotification) : GLOBAL_DEFAULT;
        
        NotificationConfig config = new NotificationConfig();
        config.setType(typeOfNotification);
        config.setDisplayName(typeEnum.getDisplayName());
        config.setValue(value);
        
        return config;
    }
    
    /**
     *  type: "FORUM_USER_REMOVED"
        value: "Global Default", "True", "False"
     * @param typeAndValue
     */
    public void update(Map<String, String> typeAndValue) throws Exception
    {
        if(typeAndValue != null)
        {
            Iterator<String> it = typeAndValue.keySet().iterator();
            while(it.hasNext())
            {
                String type = it.next();
                String value = typeAndValue.get(type);
                UserGlobalNotificationContainerType relType = UserGlobalNotificationContainerType.valueOf(type);

//                if (value.equalsIgnoreCase(GLOBAL_DEFAULT))
//                {
//                    //remove the relationship
//                    ResolveGraphFactory.deleteRelation(userNode, streamNode, relType);
//                }
//                else
//                {
//                    updateType(relType, Boolean.valueOf(value));
//                }
               
            }//end of while loop
        }
    }
    
    /**
        This api is to set/update the value for a specific component type.
     * 
     * For eg, if user wants to receive email for all the Post for a Process 'P1', the values for this api will be
     * streamId = '8a9482e54185528801418568767a0009'  <== sysId of Process P1
     * type =  "PROCESS_FORWARD_EMAIL"  <== mapped to UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL
     * value = true or false
     * 
     * @param selectedType
     * @param value
     * @throws Exception
     */
    public void updateType(UserGlobalNotificationContainerType type, boolean value) throws Exception
    {
        if(validNotificationTypes.contains(type))
        {
            //add the relationship and set the property
//            ResolveGraphFactory.addRelation(userNode, streamNode, type);
//            Relationship rel = ResolveGraphFactory.findRelationForType(userNode, streamNode, type);
//            if(rel != null)
//            {
//                ResolveGraphFactory.updatePropertyOnRelationship(ResolveGraphFactory.NOTIFICATIONVALUE, value+"", rel);
//            }
        }
        else
        {
            Log.log.warn("Type " + type + " is not valid for stream " + this.streamDisplayName + " which is of type " + this.streamType);
        }
    }
    
    
    //private apis
    private Map<UserGlobalNotificationContainerType, String> getUserCurrentSpecificSettings()
    {
        Map<UserGlobalNotificationContainerType, String> result = new HashMap<UserGlobalNotificationContainerType, String>();
        
//        String streamId = (String) this.streamNode.getProperty(RSComponent.SYS_ID);
//        Iterable<Relationship> rels = userNode.getRelationships(Direction.OUTGOING);
//        for(Relationship rel : rels)
//        {
//            Node otherNode = rel.getOtherNode(userNode);
//            if(!otherNode.hasProperty(RSComponent.SYS_ID))
//            {
//                //if sysId not there, than continue
//                continue;
//            }
//            
//            String currNodeId = (String) otherNode.getProperty(RSComponent.SYS_ID);
//            if(currNodeId.equals(streamId))
//            {
//                //Introspect this relationship where the id matches
//                boolean hasProperty = rel.hasProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
//                if(hasProperty)
//                {
//                    //this is a good one...
//                    String value = (String) rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
//                    value = value.equalsIgnoreCase("true") ? SELECT_TRUE : SELECT_FALSE;
//                    
//                    //add it in the map
//                    result.put(UserGlobalNotificationContainerType.valueOf(rel.getType().name()), value);
//                }
//            }
//        }
        
        
        return result;
    }
    
    
    

}
