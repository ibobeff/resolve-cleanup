/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.catalogbuilder.service.CatalogUtil;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.vo.GraphRelationshipDTO;

public class ImportDocumentGraph  extends ImportComponentGraph
{
    private Document document = null;
    
    public ImportDocumentGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        document = SocialCompConversionUtil.createDocumentByName(rel.getSourceName());
        if(document != null)
        {
//            Node atNode = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
//            if(atNode != null)
//            {
//                switch(rel.getTargetType())
//                {
//                    case ResolveTag: 
//                        assignTagToDocument();
//                        break;
//                        
//                    case CatalogItem:
//                    case CatalogFolder:
//                    case CatalogGroup:
//                    case CatalogReference:                        
//                        addDocumentToCatalogReference();
//                        break;
//                        
//                    default:
//                        break;
//                }
//            }
//            else
//            {
//                throw new Exception("Document " + document.getDisplayName() + " is not sync with graph db." );
//            } 
        }
    }
    
    private void assignTagToDocument() throws Exception
    {
        String tagName = rel.getTargetName();
        Set<String> tagNames = new HashSet<String>();
        tagNames.add(tagName);
        
//        ServiceTag.addAdditionalTagsToComponent(document.getSys_id(), tagNames);
    }
    

    private void addDocumentToCatalogReference()  throws Exception
    {
        //example - /Jeet1/Group1/Item1
        String catalogPath = rel.getTargetName();
        String catalogName = catalogPath.split("/")[1]; 
        
        //create a stub of the catalog if it does not exist.
//        ResolveCatalog catalog = ServiceCatalog.getCatalogByName(catalogName);
//        if(catalog == null)
//        {
//            catalog = createCatalogStubs(catalogPath, rel.getTargetObject());
//        }
        
        Set<String> paths = new HashSet<String>();
        paths.add(catalogPath);
        
        //assign the path to the catalog
//        CatalogUtil.addAdditionalCatalogItemPathsToDocument(document, paths, username);
        
    }
    
    // /Jeet1/Group1/Item1
//    private ResolveCatalog createCatalogStubs(String path, ResolveCatalog refCatalog) throws Exception
//    {
//        ResolveCatalog catalog = null;
//        
//        //create the stub
//        ResolveCatalogStubCreator stub = new ResolveCatalogStubCreator(path, username);
//        stub.setReferenceCatalog(refCatalog);
//        catalog = stub.createStub();
//        
//        //create it
////        catalog = ServiceCatalog.createCatalog(catalog, "system");
//        
//        return catalog;
//    }

}
