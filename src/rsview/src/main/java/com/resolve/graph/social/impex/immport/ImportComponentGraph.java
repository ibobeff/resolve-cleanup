/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.util.StringUtils;

public abstract class ImportComponentGraph
{
    protected GraphRelationshipDTO rel = null;
    protected String username = null;
    
    public ImportComponentGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        if(rel == null || StringUtils.isEmpty(username))
        {
            throw new Exception("rel obj and username are mandatory");
        }
        
        //validate the object
        rel.validate();
        
        this.rel = rel;
        this.username = username;
    }
    
    public abstract void immport() throws Exception;
    
    
    
    
    
    
}
