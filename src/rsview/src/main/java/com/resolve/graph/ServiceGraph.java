/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph;

import org.apache.log4j.Level;
import org.neo4j.graphdb.GraphDatabaseService;

import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.rsview.main.RSContext;
import com.resolve.util.ERR;
import com.resolve.util.Log;

public class ServiceGraph
{

    //public static Properties configNeo = null;

    /**
     * This method support create neo4j graph db service and also init all social
     * factories
     *
     * @param configNeo
     */
    public static void initGraphDB()
    {
        Log.log.info("Initializing GraphDB: "+RSContext.getWebHome()+"WEB-INF/graphdb");
        System.out.println("Initializing GraphDB: " + RSContext.getWebHome()+"WEB-INF/graphdb");

        deleteTempCopyDir();

        // connect to zk and init graphdb
        boolean connected = GraphManager.getInstance().initGraphDB();//connectGraphDB();

        //if (connected && graphDB != null)
        if (connected && getGraphDBService() != null)
        {
            initAllFactories();

//            if (Log.log.getLevel() == Level.TRACE)
//            {
//                checkGraphSize();
//            }

            System.out.println("GraphDB initialized successfully");
            Log.log.info("GraphDB initialized successfully");
        }
        else
        {
            Log.alert(ERR.E20004);
        }
    }

    public static void initAllFactories()
    {
        initAllSocialFactory();
        initAllCatalogFactory();
        initAllTagFactory();
    }

    public static void stopGraphDB()
    {
        GraphManager.resetAllSocialFactory();
        GraphManager.resetAllCatalogFactory();
        GraphManager.resetAllTagFactory();
        GraphManager.getInstance().stopGraphDB();
    }

    /**
     * @return the service for graphdb
     */
    private static GraphDatabaseService getGraphDBService()
    {
        return GraphDBUtil.getGraphDBService();
    }

    private static void initAllCatalogFactory()
    {
        GraphManager.initAllCatalogFactory();
    }


    private static void initAllTagFactory()
    {
        GraphManager.initAllTagFactory();
    }

    /**
     * init all social factory
     */
    private static void initAllSocialFactory()
    {
        GraphManager.initAllSocialFactory();
    }

    /**
     * delete the temp copy directory
     */
    private static void deleteTempCopyDir()
    {
        GraphManager.deleteTempCopyDir();
    }

    /**
     * This method to check how many node in graphdb
     */
//    private static void checkGraphSize()
//    {
//        GraphManager.checkGraphSize();
//    }
}
