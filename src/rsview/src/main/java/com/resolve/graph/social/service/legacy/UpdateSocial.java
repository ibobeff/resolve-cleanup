/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.CreateOrUpdateSocial;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialNameSpaceFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class UpdateSocial
{
    public static final String MAX_TEAMS_DEPTH = "max.teams.depth";

    public static SocialUsersFactory socialUsersFactory = new SocialUsersFactory();
    public static SocialForumFactory socialForumFactory = new SocialForumFactory();
    public static SocialTeamFactory socialTeamFactory = new SocialTeamFactory();
    public static SocialProcessFactory socialProcessFactory = new SocialProcessFactory();
    public static SocialActionTaskFactory socialActionTaskFactory = new SocialActionTaskFactory();
    public static SocialNameSpaceFactory socialNameSpaceFactory = new SocialNameSpaceFactory();
    public static SocialDocumentFactory socialDocumentFactory = new SocialDocumentFactory();
    public static SocialRssFactory socialRssFactory = new SocialRssFactory();

    
//    // update component
//    private static void updateComponent(RSComponent rscomponent) throws Exception
//    {
//        Transaction tx = null;
//        Node node = null;
//        String sys_idLocal = "";
//        String sys_created_byLocal = "";
//        long sys_created_on = 0l;
//
//        try
//        {
//            if (rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
//            {
//                node = SocialFactory.findNodeByIndexedID(rscomponent.getSys_id());
//
//                if (node != null)
//                {
//                    tx = GraphDBUtil.getGraphDBService().beginTx();
//                    sys_idLocal = (String) node.getProperty(RSComponent.SYS_ID);
//                    sys_created_byLocal = (String) node.getProperty(RSComponent.SYS_CREATED_BY);
//                    sys_created_on = (Long) node.getProperty(RSComponent.SYS_CREATED_ON);
//
//                    rscomponent.convertObjectToNode(node);
//                    node.setProperty(RSComponent.SYS_ID, sys_idLocal);
//                    node.setProperty(RSComponent.SYS_CREATED_BY, sys_created_byLocal);
//                    node.setProperty(RSComponent.SYS_CREATED_ON, sys_created_on);
//
//                    tx.success();
//                }
//
//                String compClassName = rscomponent.getClass().getSimpleName();
//
//                if (compClassName.equalsIgnoreCase(Runbook.TYPE) || compClassName.equalsIgnoreCase(Document.TYPE) || compClassName.equalsIgnoreCase(DecisionTree.TYPE))
//                {
//                    removeRSWikiFromNamespace(rscomponent);
//                    addRSComponentToNameSpace(rscomponent);
//                }
//
//            }
//        }
//        catch (Exception t)
//        {
//            Log.log.info(t.getMessage(), t);
//            throw t;
//        }
//        finally
//        {
//            if (tx != null)
//            {
//                tx.finish();
//            }
//        }
//    }

//    public static void swapRunbookOrDocument(RSComponent rscomponent)
//    {
//        Node currentNode = null;
//        String nodetype = null;
//        Transaction tx = null;
//
//        try
//        {
//            if (rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
//            {
//                SocialFactory socialFactory = SocialUtil.getSocialFactory(rscomponent.getClass().getName());
//
//                if (socialFactory != null)
//                {
//                    currentNode = socialFactory.findNode(rscomponent.getSys_id());
//
//                    if (currentNode != null)
//                    {
//                        boolean hasKey = currentNode.hasProperty(SocialFactory.NODE_TYPE);
//
//                        if (hasKey)
//                        {
//                            tx = GraphDBUtil.getGraphDBService().beginTx();
//
//                            nodetype = (String) currentNode.getProperty(SocialFactory.NODE_TYPE);
//
//                            if (StringUtils.isNotEmpty(nodetype) && nodetype.equals(SocialObjectType.RUNBOOK.name()))
//                            {
//                                SocialRunbookFactory.removeRunbookFromContainer(currentNode);
//                                currentNode.setProperty(SocialFactory.NODE_TYPE, SocialObjectType.DOCUMENT.name());
//                                SocialDocumentFactory.addDocumentToContainer(currentNode);
//                            }
//                            else if (StringUtils.isNotEmpty(nodetype) && nodetype.equals(SocialObjectType.DOCUMENT.name()))
//                            {
//                                SocialDocumentFactory.removeDocumentFromContainer(currentNode);
//                                currentNode.setProperty(SocialFactory.NODE_TYPE, SocialObjectType.RUNBOOK.name());
//                                SocialRunbookFactory.addRunbookToContainer(currentNode);
//                            }
//
//                            tx.success();
//                        }
//                    }
//                }
//            }
//        }
//        catch (Throwable t)
//        {
//            Log.log.info(t.getMessage() + t);
//        }
//        finally
//        {
//            if (tx != null)
//            {
//                tx.finish();
//            }
//        }
//    }

    // add configuration
//    public static void addUserToForum(User user, Forum forum) throws Exception
//    {
//        Node userNode = null;
//        Node forumNode = null;
//
//        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            if (forum != null && StringUtils.isNotEmpty(forum.getSys_id()))
//            {
//                forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
//
//                if (userNode != null && forumNode != null)
//                {
//                    ResolveGraphFactory.addRelation(userNode, forumNode, RelationType.MEMBER);
//                    
//                    String subject = " added user " + user.getUsername() + " to forum " + forum.getDisplayName();
//                    notifyUser(user, UserGlobalNotificationContainerType.USER_ADDED_TO_FORUM, subject);
//                }
//            }
//        }
//    }
//    
//    public static void addUserToTeam(User user, Team team) throws Exception
//    {
//        Node userNode = null;
//        Node teamNode = null;
//
//        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//            if (team != null && StringUtils.isNotEmpty(team.getSys_id()))
//            {
//                teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
//
//                if (userNode != null && teamNode != null)
//                {
//                    ResolveGraphFactory.addRelation(userNode, teamNode, RelationType.MEMBER);
//                    
//                    String subject = " added user " + user.getUsername() + " to team " + team.getDisplayName();
//                    notifyUser(user, UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM, subject);
//                }
//            }
//        }
//    }
    

//    public static void addUserToProcess(User user, Process process) throws Exception
//    {
//        Node userNode = null;
//        Node processNode = null;
//
//        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
//            {
//                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
//
//                if (userNode != null && processNode != null)
//                {
//                    ResolveGraphFactory.addRelation(userNode, processNode, RelationType.MEMBER);
//                   
//                    String subject = " added user " + user.getUsername() + " to process " + process.getDisplayName();
//                    notifyUser(user, UserGlobalNotificationContainerType.USER_ADDED_TO_PROCESS, subject);
//                }
//            }
//        }
//    }
   
//    public static void notifyUser(User notifyuser, UserGlobalNotificationContainerType notifyType, String subject)
//    {
//        new RuntimeException("BD: Need to know who is calling so we can divert the post to ES");
//        /*
//        try
//        {
//            User  adminUser = SocialCompConversionUtil.getSocialUser(UserUtils.ADMIN);
//            String content = adminUser.getUsername() + subject;
//            SystemNotification notif = new SystemNotification(notifyType);
//            
//            notif.setDisplayName(adminUser.getUsername());
//            notif.setContent(content);
//            notif.setAuthor(adminUser.getUsername());
//            notif.addComments(new ArrayList());
//            notif.setSys_created_on(new Date().getTime());
//            notif.setSys_updated_on(new Date().getTime());
//            notif.setSys_created_by(UserUtils.ADMIN);
//            notif.setSys_updated_by(UserUtils.ADMIN);
//            notif.setRoles(UserUtils.ADMIN +  " " + PostSocial.INTERNAL_POST_MEMBER_ROLE);
//     
//            Set<String> targets = new HashSet<String>();
//            Set<String> receiver = new HashSet<String>();
//            Set<String> starredIDsSet = new HashSet<String>();
//            Set<String> outboxIDsSet = new HashSet<String>();
//            Set<String> namespaceSet = new HashSet<String>();
//            
//            List<RSComponent> targetLocal = new ArrayList();
//            
//            if(notif != null)
//            {
//                targetLocal.add(notifyuser);
//                notif.setTarget(targetLocal);
//                
//                Node postNode = null;
//                Node targetUserNode = null;
//                
//                postNode = PostSocial.createPost(notif);
//                PostSocial.userSentPost(adminUser, notif, postNode);
//               
//                if(postNode != null && notifyuser != null && StringUtils.isNotEmpty(notifyuser.getSys_id()))
//                {
//                    targetUserNode = ResolveGraphFactory.findNodeByIndexedID(notifyuser.getSys_id());
//                    
//                    if(postNode != null && targetUserNode != null)
//                    {
//                        ResolveGraphFactory.addRelation(postNode, targetUserNode, RelationType.TARGET);
//                        //SocialFactory.updateRSComponentLastActivityOn(targetUserNode);
//                    }
//                }
//                
//                receiver.add(notifyuser.getSys_id());
//                targets.add(notifyuser.getSys_id());
//                
//                PostSocial.buildindexPost(notif, adminUser, targets, receiver, starredIDsSet, outboxIDsSet, namespaceSet);
//           
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.error("process notification catch exception: " + exp, exp);
//        }
//        */
//    }

//    public static void addActionTaskToProcess(ActionTask actiontask, Process process) throws Exception
//    {
//        Node atNode = null;
//        Node processNode = null;
//
//        if (actiontask != null && StringUtils.isNotEmpty(actiontask.getSys_id()))
//        {
//            atNode =  ResolveGraphFactory.findNodeByIndexedID(actiontask.getSys_id());
//            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
//            {
//                processNode =  ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
//
//                if (atNode != null && processNode != null)
//                {
//                    ResolveGraphFactory.addRelation(atNode, processNode, RelationType.MEMBER);
//                }
//            }
//        }
//    }
//    
//    public static void addRSComponentsToNameSpace(List<RSComponent> comps) throws Exception
//    {
//        if(comps != null)
//        {
//            for(RSComponent comp: comps)
//            {
//                try
//                {
//                    addRSComponentToNameSpace(comp);
//                }
//                catch(Exception exp)
//                {
//                    Log.log.info(exp.getMessage(), exp);
//                }
//            }
//        }
//    }
//    
//    public static void addRSComponentToNameSpace(RSComponent comp) throws Exception
//    {
//        Node compNode = null;
//        Node namespaceNode = null;
////        boolean compNodeIsNull = false;
//
//        if(comp != null)
//        {
//            String compClassName = comp.getClass().getSimpleName();
//            if(compClassName.equalsIgnoreCase(Runbook.TYPE) || compClassName.equalsIgnoreCase(Document.TYPE) || compClassName.equalsIgnoreCase(DecisionTree.TYPE))
//            {        
//                String fullName = comp.getDisplayName();
//                if(StringUtils.isNotBlank(fullName) && fullName.indexOf('.') > -1)
//                {
//                    //create/update the namespace
//                    String namespace = fullName.split("\\.")[0];
//                    Namespace namespaceObject = new Namespace(namespace, namespace);
//                    namespaceObject = (Namespace) CreateOrUpdateSocial.insertOrUpdateRSComponent(namespaceObject);
//
//                    //get the nodes
//                    namespaceNode = ResolveGraphFactory.findNodeByIndexedID(namespaceObject.getId());
//                    compNode = ResolveGraphFactory.findNodeByIndexedID(comp.getSys_id());
//                    
//                    //add the relationship
//                    if (compNode != null && namespaceNode != null)
//                    {
//                        ResolveGraphFactory.addRelation(compNode, namespaceNode, SocialRelationshipTypes.NAMESPACE);
//                    }
//                }
//            }
//        }
//    }
//    
////    public static void removeRSWikiFromNamespace(RSComponent comp) throws Exception
////    {
////        Node wikidocNode = null;
////        String compDisplayName = null;
////        
////        if (comp != null && StringUtils.isNotEmpty(comp.getSys_id()))
////        {
////            
////            String compClassName = comp.getClass().getSimpleName(); 
////            
////            if(compClassName.equalsIgnoreCase(Runbook.TYPE) || compClassName.equalsIgnoreCase(Document.TYPE) || compClassName.equalsIgnoreCase(DecisionTree.TYPE))
////            { 
////                wikidocNode = ResolveGraphFactory.findNodeByIndexedID(comp.getSys_id());
////                
////                if(wikidocNode != null)
////                {
////                    Iterable<Relationship> rels = wikidocNode.getRelationships(RelationType.NAMESPACE, Direction.OUTGOING);
////                    
////                    if(rels != null)
////                    {
////                        for(Relationship rel: rels)
////                        {
////                            Node endNode = rel.getEndNode();
////                            
////                            if(endNode != null && StringUtils.isNotEmpty((String)endNode.getProperty(Namespace.NAMESPACE_NAME)))
////                            {
////                                compDisplayName = (String)endNode.getProperty(Namespace.NAMESPACE_NAME);
////                                break;
////                            }
////                        }
////                    }
////                }
////                
////                if(StringUtils.isNotEmpty(compDisplayName))
////                {
////                    Node namespaceNodeLocal = socialNameSpaceFactory.findNameSpaceNodeByName(compDisplayName);
////                        
////                    if(namespaceNodeLocal != null)
////                    {
////                        ResolveGraphFactory.deleteRelation(wikidocNode, namespaceNodeLocal, RelationType.NAMESPACE);
////                    }
////                }  
////            }
////        }
////    }
//
//    
//
////    public static void addDocumentToProcess(Document document, Process process) throws Exception
////    {
////        Node documentNode = null;
////        Node processNode = null;
////
////        if (document != null && StringUtils.isNotEmpty(document.getSys_id()))
////        {
////            documentNode = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (documentNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.addRelation(documentNode, processNode, RelationType.MEMBER);
////                }
////            }
////        }
////    }
////    
////    public static void addForumToProcess(Forum forum, Process process) throws Exception
////    {
////        Node forumNode = null;
////        Node processNode = null;
////
////        if (forum != null && StringUtils.isNotEmpty(forum.getSys_id()))
////        {
////            forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (forumNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.addRelation(forumNode, processNode, RelationType.MEMBER);
////                }
////            }
////        }
////    }
////    
////    public static void addNameSpaceToProcess(Namespace namespace, Process process) throws Exception
////    {
////        Node namespaceNode = null;
////        Node processNode = null;
////
////        if (namespace != null && StringUtils.isNotEmpty(namespace.getName()))
////        {
////            namespaceNode = socialNameSpaceFactory.findNameSpaceNodeByName(namespace.getName());
////            
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (namespaceNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.addRelation(namespaceNode, processNode, RelationType.MEMBER);
////                }
////            }
////        }
////    }
////
////    public static void subscribeRssToProcess(Rss rss, Process process) throws Exception
////    {
////        Node rssNode = null;
////        Node processNode = null;
////
////        if (rss != null && StringUtils.isNotEmpty(rss.getSys_id()))
////        {
////            rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (rssNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.addRelation(rssNode, processNode, RelationType.MEMBER);
////                }
////            }
////        }
////    }
////
////    public static void addTeamToProcess(Team team, Process process) throws Exception
////    {
////        Node teamNode = null;
////        Node processNode = null;
////
////        if (team != null && StringUtils.isNotEmpty(team.getSys_id()))
////        {
////            teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (teamNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.addRelation(teamNode, processNode, RelationType.MEMBER);
////                }
////            }
////        }
////    }
////    
////
////    public static void addTeamToTeam(Team team, Team otherTeam) throws Exception
////    {
////        Node teamNode = null;
////        Node otherTeamNode = null;
////
////        if (team != null && StringUtils.isNotEmpty(team.getSys_id()))
////        {
////            teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
////
////            if (otherTeam != null && StringUtils.isNotEmpty(otherTeam.getSys_id()))
////            {
////                otherTeamNode = ResolveGraphFactory.findNodeByIndexedID(otherTeam.getSys_id());
////
////                if (teamNode != null && otherTeamNode != null)
////                {
////                    // to check if the teamNode alrady is parent of otherTeam
////                    Set<String> parentIDS = SocialConfig.getParentsOfTeam(otherTeam.getSys_id());
////                    //long maxTeamDepth = PropertiesUtil.getPropertyLong(MAX_TEAMS_DEPTH);
////                    
////                    //if(thisTeamCanAddedToOtherTeam(teamNode, otherTeamNode, maxTeamDepth))
////                    //{
////                        if ((parentIDS == null || parentIDS.isEmpty()) || (parentIDS != null && !parentIDS.contains(team.getSys_id())))
////                        {
////                            ResolveGraphFactory.addRelation(teamNode, otherTeamNode, RelationType.MEMBER);
////                        }
////                        else
////                        {
////                            throw new Exception("You can't add parent team to child team"); // or teams depth more than " + maxTeamDepth + ".");
////                        }
////                    //}
////                }
////            }
////        }
////    }
////    
////
////    public static void subscribeRssToUser(Rss rss, User user) throws Exception
////    {
////        Node rssNode = null;
////        Node userNode = null;
////
////        if (rss != null && StringUtils.isNotEmpty(rss.getSys_id()))
////        {
////            rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
////
////            if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////            {
////                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////                if (rssNode != null && userNode != null)
////                {
////                    ResolveGraphFactory.addRelation(userNode, rssNode, RelationType.FOLLOWER);
////                }
////            }
////        }
////    }
////    
////    
////    public static void addNotificationRelation(User user, List<ComponentNotification> compNotifications) throws Exception
////    {
////        Node userNode = null;
////        Node rsComponentNode = null;
////
////        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////        {
////            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////            if (userNode != null && compNotifications != null && !compNotifications.isEmpty())
////            {
////                for (ComponentNotification compNotify : compNotifications)
////                {
////                    RSComponent rsComp = compNotify.getRSComponent();
////                    List<NotificationConfig> types = compNotify.getNotifyTypes();
////
////                    if(rsComp != null && StringUtils.isNotEmpty(rsComp.getSys_id()))
////                    {
////                        SocialFactory socialFactory = SocialUtil.getSocialFactory(rsComp.getClass().getName());
////
////                        if (socialFactory != null)
////                        {
////                            rsComponentNode = ResolveGraphFactory.findNodeByIndexedID(rsComp.getSys_id());
////
////                            if (rsComponentNode != null)
////                            {
////                                SocialUsersFactory.addNotificationRelation(userNode, rsComponentNode, types);
////                            }
////                        }
////                    }
////                }
////            }
////        }
////    }
//
//    // remove configuration
////    public static void removeNotificationRelation(User user, List<ComponentNotification> compNotifications) throws Exception
////    {
////        Node userNode = null;
////        Node rsComponentNode = null;
////
////        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////        {
////            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////            if (userNode != null && compNotifications != null && !compNotifications.isEmpty())
////            {
////                for (ComponentNotification compNotify : compNotifications)
////                {
////                    RSComponent rsComp = compNotify.getRSComponent();
////                    List<NotificationConfig> types = compNotify.getNotifyTypes();
////
////                    if (rsComp != null && StringUtils.isNotEmpty(rsComp.getSys_id()))
////                    {
////                        SocialFactory socialFactory = SocialUtil.getSocialFactory(rsComp.getClass().getName());
////
////                        if (socialFactory != null)
////                        {
////                            rsComponentNode = ResolveGraphFactory.findNodeByIndexedID(rsComp.getSys_id());
////
////                            if (rsComponentNode != null)
////                            {
////                                SocialFactory.deleteNotificationRelation(userNode, rsComponentNode, types);
////                            }
////                        }
////                    }
////                }
////            }
////        }
////    }
////
////    public static void removeUserFromForum(User user, Forum forum) throws Exception
////    {
////        Node userNode = null;
////        Node forumNode = null;
////
////        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////        {
////            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////            if (forum != null && StringUtils.isNotEmpty(forum.getSys_id()))
////            {
////                forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
////
////                if (userNode != null && forumNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(userNode, forumNode, RelationType.MEMBER);
////                    removeFromThisUserHome(user, forum);
////                }
////            }
////        }
////    }
////
////    public static void removeUserFromTeam(User user, Team team) throws Exception
////    {
////        Node userNode = null;
////        Node teamNode = null;
////
////        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////        {
////            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////            if (team != null && StringUtils.isNotEmpty(team.getSys_id()))
////            {
////                teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
////
////                if (userNode != null && teamNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(userNode, teamNode, RelationType.MEMBER);
////                    removeFromThisUserHome(user, team);
////                }
////            }
////        }
////    }
////
////    public static void removeUserFromProcess(User user, Process process) throws Exception
////    {
////        Node userNode = null;
////        Node processNode = null;
////
////        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////        {
////            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (userNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(userNode, processNode, RelationType.MEMBER);
//////                    removeFromUsersHome(user);
////                    removeFromHome(user,process);
////                }
////            }
////        }
////    }
////
////    public static void removeActionTaskFromProcess(ActionTask actiontask, Process process) throws Exception
////    {
////        Node actiontaskNode = null;
////        Node processNode = null;
////
////        if (actiontask != null && StringUtils.isNotEmpty(actiontask.getSys_id()))
////        {
////            actiontaskNode =  ResolveGraphFactory.findNodeByIndexedID(actiontask.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode =  ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (actiontaskNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(actiontaskNode, processNode, RelationType.MEMBER);
////                    removeFromUsersHome(actiontask);
////                }
////            }
////        }
////    }
////
////    public static void removeDocumentFromProcess(Document document, Process process) throws Exception
////    {
////        Node documentNode = null;
////        Node processNode = null;
////
////        if (document != null && StringUtils.isNotEmpty(document.getSys_id()))
////        {
////            documentNode = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (documentNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(documentNode, processNode, RelationType.MEMBER);
////                    removeFromUsersHome(document);
////                }
////            }
////        }
////    }
////
////    public static void removeForumFromProcess(Forum forum, Process process) throws Exception
////    {
////        Node forumNode = null;
////        Node processNode = null;
////
////        if (forum != null && StringUtils.isNotEmpty(forum.getSys_id()))
////        {
////            forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (forumNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(forumNode, processNode, RelationType.MEMBER);
////                    removeFromUsersHome(forum);
////                }
////            }
////        }
////    }
////    
////    public static void removeNameSpaceFromProcess(Namespace namespace, Process process) throws Exception
////    {
////        Node namespaceNode = null;
////        Node processNode = null;
////
////        if (namespace != null && StringUtils.isNotEmpty(namespace.getName()))
////        {
////            namespaceNode = SocialNameSpaceFactory.findNameSpaceNodeByName(namespace.getName());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (namespaceNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(namespaceNode, processNode, RelationType.MEMBER);
////                }
////            }
////        }
////    }
////
////    public static void removeTeamFromProcess(Team team, Process process) throws Exception
////    {
////        Node teamNode = null;
////        Node processNode = null;
////
////        if (team != null && StringUtils.isNotEmpty(team.getSys_id()))
////        {
////            teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (teamNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(teamNode, processNode, RelationType.MEMBER);
////                    removeFromUsersHome(team);
////                }
////            }
////        }
////    }
////
////    public static void removeTeamFromTeam(Team team, Team otherTeam) throws Exception
////    {
////        Node teamNode = null;
////        Node otherTeamNode = null;
////
////        if (team != null && StringUtils.isNotEmpty(team.getSys_id()))
////        {
////            teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
////
////            if (otherTeam != null && StringUtils.isNotEmpty(otherTeam.getSys_id()))
////            {
////                otherTeamNode = ResolveGraphFactory.findNodeByIndexedID(otherTeam.getSys_id());
////
////                if (teamNode != null && otherTeamNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(teamNode, otherTeamNode, RelationType.MEMBER);
////                    removeFromUsersHome(team);
////                }
////            }
////        }
////    }
////
////    public static void unsubscribeRssFromProcess(Rss rss, Process process) throws Exception
////    {
////        Node rssNode = null;
////        Node processNode = null;
////
////        if (rss != null && StringUtils.isNotEmpty(rss.getSys_id()))
////        {
////            rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
////
////            if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
////            {
////                processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
////
////                if (rssNode != null && processNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(rssNode, processNode, RelationType.MEMBER);
////                    removeFromUsersHome(rss);
////                }
////            }
////        }
////    }
////
////    public static void unsubscribeRssFromUser(Rss rss, User user) throws Exception
////    {
////        Node rssNode = null;
////        Node userNode = null;
////
////        if (rss != null && StringUtils.isNotEmpty(rss.getSys_id()))
////        {
////            rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
////
////            if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////            {
////                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////                if (rssNode != null && userNode != null)
////                {
////                    ResolveGraphFactory.deleteRelation(userNode, rssNode, RelationType.FOLLOWER);
////                    removeFromThisUserHome(user, rss);
////                }
////            }
////        }
////    }
////
////    public static void addToHome(User user, RSComponent rscomponent) throws Exception
////    {
////        Node node = null;
////        Node userNode = null;
////
////        try
////        {
////            if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
////            {
////                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////                if (userNode != null && rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
////                {
////                    node = ResolveGraphFactory.findNodeByIndexedID(rscomponent.getSys_id());
////
////                    if (node != null)
////                    {
////                        //check if the user is following this comp first
////                        boolean isFollowing = GetSocial.isUserFollowingThisTarget(userNode, node);
////                        if (isFollowing)
////                        {
////                            ResolveGraphFactory.addRelation(userNode, node, RelationType.FAVORITE);
////                        }
////                    }
////                }
////            }
////        }
////        catch (Exception t)
////        {
////            Log.log.info("addToHome catched exception: " + t);
////            throw t;
////        }
////    }
//
//    private static void removeFromHome(User user, RSComponent rscomponent) throws Exception
//    {
//        Node userNode = null;
//        Node node = null;
//
//        try
//        {
//            if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if (userNode != null && rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
//                {
//                    node = ResolveGraphFactory.findNodeByIndexedID(rscomponent.getSys_id());
//
//                    if (node != null)
//                    {
//                        ResolveGraphFactory.deleteRelation(userNode, node, RelationType.FAVORITE);
//                    }
//                }
//            }
//        }
//        catch (Exception t)
//        {
//            Log.log.info("removeFromHome catched exception: " + t);
//            throw t;
//        }
//    }
////    
////    public static void removeFromUsersHome(RSComponent rscomponent) throws Exception
////    {
////        Node nodeComp = null;
////        
////        try
////        {
////            if (rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
////            {
////                nodeComp = ResolveGraphFactory.findNodeByIndexedID(rscomponent.getSys_id());
////
////                if (nodeComp != null)
////                {
////
////                    TraversalDescription trav = Traversal.description().breadthFirst().relationships(RelationType.FAVORITE, Direction.INCOMING).evaluator(Evaluators.excludeStartPosition()).evaluator(Evaluators.atDepth(1));
////
////                    for (Node nodeLocal : trav.traverse(nodeComp).nodes())
////                    {
////                        User user = new User();
////                        user.convertNodeToObject(nodeLocal);
////                        removeFromThisUserHome(user, rscomponent);
////                    }
////                }
////            }
////        }
////        catch(Exception t)
////        {
////            Log.log.info("removeFromUsersHome catched exception: " + t);
////            throw t;
////        }
////    }
////    
//    public static void removeFromThisUserHome(User user, RSComponent rscomponent)
//    {
//        try
//        {
//            Node userNode = null;
//            Node nodeComp = null;
//            
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//                
//                if(userNode != null && rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
//                {
//                    nodeComp = ResolveGraphFactory.findNodeByIndexedID(rscomponent.getSys_id());
//
//                    TraversalDescription trav = Traversal.description().depthFirst()
//                                    .relationships(RelationType.FOLLOWER, Direction.OUTGOING)
//                                    .relationships(RelationType.MEMBER)
//                                    .evaluator(Evaluators.excludeStartPosition())
//                                    .evaluator(Evaluators.includeWhereEndNodeIs(nodeComp))
//                                    .uniqueness(Uniqueness.RELATIONSHIP_GLOBAL);
//
//                    boolean hasPath = false;
//
//                    for (Path pathLocal : trav.traverse(userNode))
//                    {
//                        if (pathLocal != null)
//                        {
//                            hasPath = true;
//                        }
//                    }
//
//                    if (!hasPath)
//                    {
//                        removeFromHome(user, rscomponent);
//                    }
//                }
//            }
//        }
//        catch(Exception t)
//        {
//            Log.log.info("removeFromHomeByReference catched exception: " + t);
//        }
//        
//    }
////
////    public static void addUserNotificationConfig(User user, List<NotificationConfig> notificationConfig) throws Exception
////    {
////        SocialGlobalNotificationFactory.addUserNotificationConfig(user, notificationConfig);
////    }
////    
////    public static void removeUserNotificationConfig(User user, List<NotificationConfig> notificationConfig) throws Exception
////    {
////        Node userNode = null;
////        
////        if(user != null && !StringUtils.isEmpty(user.getSys_id()))
////        {
////            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////         
////            if(userNode != null)
////            {
////                SocialGlobalNotificationFactory.removeAllRelationsFromUser(userNode, notificationConfig);
////            }
////        }
////    }
////    
////    public static void updateUserNotificationConfig(User user, List<NotificationConfig> notificationConfig) throws Exception
////    {
////        SocialGlobalNotificationFactory.updateUserNotificationConfig(user, notificationConfig);
////    }
////    
//    public static void addRelation(String fromSysID, String toSysID, String rel)
//    {
//        Node fromNode = null;
//        Node toNode = null;
//        
//        try
//        {
//            if(StringUtils.isNotEmpty(fromSysID) && StringUtils.isNotEmpty(toSysID))
//            {
//                fromNode = ResolveGraphFactory.findNodeByIndexedID(fromSysID);
//                toNode = ResolveGraphFactory.findNodeByIndexedID(toSysID);
//                RelationshipType relType = RelationType.valueOf(rel);
//                
//                if(fromNode != null && toNode != null && relType != null)
//                {
//                    ResolveGraphFactory.addRelation(fromNode, toNode, relType);
//                }   
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info("addRelation catched exception: " + exp, exp);
//        }
//    }
//    
//    public static void deleteRelation(String fromSysID, String toSysID, String rel)
//    {
//        Node fromNode = null;
//        Node toNode = null;
//        
//        try
//        {
//            if(StringUtils.isNotEmpty(fromSysID) && StringUtils.isNotEmpty(toSysID))
//            {
//                fromNode = ResolveGraphFactory.findNodeByIndexedID(fromSysID);
//                toNode = ResolveGraphFactory.findNodeByIndexedID(toSysID);
//                RelationshipType relType = RelationType.valueOf(rel);
//               
//                if(fromNode != null && toNode != null && relType != null)
//                {
//                    ResolveGraphFactory.deleteRelation(fromNode, toNode, relType);
//                }   
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info("deleteRelation catched exception: " + exp, exp);
//        }
//    }
//    
//    public static void addNotificationConfig(String fromSysID, String toSysID, String rel, String propValue)
//    {
//        Node fromNode = null;
//        Node toNode = null;
//        
//        try
//        {
//            if(StringUtils.isNotEmpty(fromSysID) && StringUtils.isNotEmpty(toSysID))
//            {
//                fromSysID = fromSysID.toUpperCase().trim();
//                fromNode = ResolveGraphFactory.findNodeByIndexedID(fromSysID);
//                toNode = ResolveGraphFactory.findNodeByIndexedID(toSysID);
////                RelationshipType relType = UserGlobalNotificationContainerType.valueOf(rel);
////               
////                if(fromNode != null && toNode != null && relType != null)
////                {
////                    SocialFactory.addRelationWithProperty(fromNode, toNode, relType, propValue);
////                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info("addGlobalNotificationConfig catched exception: " + exp, exp);
//        }
//    }
//    
//    public static void removeRelation(String fromSysID, String toSysID, String rel)
//    {
//        Node fromNode = null;
//        Node toNode = null;
//        
//        try
//        {
//            if(StringUtils.isNotEmpty(fromSysID) && StringUtils.isNotEmpty(toSysID))
//            {
//                fromSysID = fromSysID.toUpperCase().trim();
//                
//                fromNode = ResolveGraphFactory.findNodeByIndexedID(fromSysID);
//                toNode = ResolveGraphFactory.findNodeByIndexedID(toSysID);
////                RelationshipType relType = UserGlobalNotificationContainerType.valueOf(rel);
////               
////                if(fromNode != null && toNode != null && relType != null)
////                {
////                    ResolveGraphFactory.deleteRelation(fromNode, toNode, relType);
////                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info("removeRelation catched exception: " + exp, exp);
//        }
//    }

   
}
