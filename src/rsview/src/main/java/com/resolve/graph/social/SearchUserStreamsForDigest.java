/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.graph.social.util.SearchUserStreamsUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.util.Log;

/**
 * util to get map of users with the list of component/s post that should be available in the digest
 * specific api for '_DIGEST_EMAIL' notification type
 * 
 * @author jeet.marwah
 *
 */
public class SearchUserStreamsForDigest
{
    public static Map<User, List<RSComponent>> getUserStreamsForDigest()
    {
        Map<User, List<RSComponent>> result = new HashMap<User, List<RSComponent>>();
        
        //get all the users 
        Collection<User> allUsers = SocialUsersFactory.getAllUsers();
        
        //for each user
        //check if the 'allDigestEmail' flag is set
        //if yes, than send all the components that this user is following
        //else prepare list of components who has relationship set 
        for(User user : allUsers)
        {
            try
            {
                Set<RSComponent> userStreams = new SearchUserStreamsUtil(user).getUserStreams();
                if(user.isAllDigestEmail())
                {
                    result.put(user, new ArrayList<RSComponent>(userStreams));
                }
                else
                {
                    //prepare the list of items that have 
                    List<RSComponent> listOfCompsForDigest = new ArrayList<RSComponent>();
                    
                    //for each stream, find out which flags are set to true
                    for(RSComponent comp : userStreams)
                    {
                        //handle the inbox
//                        if("USER".equals(comp.getType()) && user.getSys_id().equals(comp.getSys_id()) && user.isInboxDigestEmail())
//                        {
//                            listOfCompsForDigest.add(comp);
//                        }
//                        else
//                        {
//                            //new SpecificNotificationUtil(comp.getSys_id(), user).getSpecificNotifications();
//                            //no need to make a call again for notificaition as its already done in getting the streams
//                            ComponentNotification notifications = comp.getComponentNotification();
//                            List<NotificationConfig> notifyTypes = notifications.getNotifyTypes();
//                            for(NotificationConfig config : notifyTypes)
//                            {
//                                UserGlobalNotificationContainerType type = config.getType();
//                                
//                                //get the DIGEST_EMAIL type and if its true, than add this comp to the list
//                                if(type.name().contains("DIGEST_EMAIL"))
//                                {
//                                    String value = config.getValue();
//                                    if(value.equalsIgnoreCase("true"))
//                                    {
//                                        listOfCompsForDigest.add(comp);
//                                        break;
//                                    }
//                                }
//                            }//end of for loop
//                        }
                    }//end of for loop
                    
                    //add the user only if there is anything to send
                    if(listOfCompsForDigest.size() > 0 || user.isInboxDigestEmail() || user.isSystemDigestEmail())
                    {
                        result.put(user, listOfCompsForDigest);
                    }
                }
                
            }
            catch (Exception e)
            {
//                Log.log.error("Error in getting list of followed comps for user " + user.getUsername(), e);
            }
        }

        return result;
    } 
    
    

}
