/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialNameSpaceFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.graph.social.factory.SocialWorkSheetFactory;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class DeleteSocial
{
    public static SocialUsersFactory socialUsersFactory = new SocialUsersFactory();
    public static SocialForumFactory socialForumFactory = new SocialForumFactory();
    public static SocialNameSpaceFactory socialNameSpaceFactory = new SocialNameSpaceFactory();
    public static SocialTeamFactory socialTeamFactory = new SocialTeamFactory();
    public static SocialProcessFactory socialProcessFactory = new SocialProcessFactory();
    public static SocialActionTaskFactory socialActionTaskFactory = new SocialActionTaskFactory();
    public static SocialWorkSheetFactory socialWorkSheetFactory = new SocialWorkSheetFactory();
    public static SocialDocumentFactory socialDocumentFactory = new SocialDocumentFactory();
    public static SocialRssFactory socialRssFactory = new SocialRssFactory();
    public static SocialPostFactory socialPostFactory = new SocialPostFactory();
    public static SocialDeletePostFactory socialDeletePostFactory = new SocialDeletePostFactory();



    // deletes
    public static void deleteUser(User user) throws Exception
    {
        Node userNode = null;

//        if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//            if (userNode != null)
//            {
//                ResolveGraphFactory.deleteNode(userNode);
//            }
//        }
    }

    public static void deleteActionTask(ActionTask actiontask) throws Exception
    {
        Node actionTaskNode = null;

//        if (actiontask != null && StringUtils.isNotEmpty(actiontask.getSys_id()))
//        {
//            actionTaskNode =  ResolveGraphFactory.findNodeByIndexedID(actiontask.getSys_id());
//
//            if (actionTaskNode != null)
//            {
//                ResolveGraphFactory.deleteNode(actionTaskNode);
//            }
//        }
    }

    public static void deleteWorkSheet(Worksheet worksheet) throws Exception
    {
        Node worksheetNode = null;

//        if (worksheet != null && StringUtils.isNotEmpty(worksheet.getSys_id()))
//        {
//            worksheetNode = ResolveGraphFactory.findNodeByIndexedID(worksheet.getSys_id());
//
//            if(worksheetNode != null)
//            {
//                ResolveGraphFactory.deleteNode(worksheetNode);
//            }
//        }
    }

    public static void deleteRunbook(Runbook runbook) throws Exception
    {
        Node runbookNode = null;

//        if (runbook != null && StringUtils.isNotEmpty(runbook.getSys_id()))
//        {
//            runbookNode = ResolveGraphFactory.findNodeByIndexedID(runbook.getSys_id());
//
//            if (runbookNode != null)
//            {
//                ResolveGraphFactory.deleteNode(runbookNode);
//            }
//        }
    }


    public static void deleteWikidoc(String sysid) throws Exception
    {
        Node node = null;

        try
        {
            if (StringUtils.isNotEmpty(sysid))
            {
                node = ResolveGraphFactory.findNodeByIndexedID(sysid);

                if (node != null)
                {
                    ResolveGraphFactory.deleteNode(node);
                }
            }
        }
        catch(Exception exp)
        {
            if(!(exp instanceof org.neo4j.graphdb.NotFoundException))
            {
                Log.log.warn("deleteWikidoc got exception: " + exp, exp);
                throw exp;
            }
        }
    }

    public static void deleteDecisionTree(DecisionTree dt) throws Exception
    {
        Node dtNode = null;

//        if (dt != null && StringUtils.isNotEmpty(dt.getSys_id()))
//        {
//            dtNode = ResolveGraphFactory.findNodeByIndexedID(dt.getSys_id());
//
//            if (dtNode != null)
//            {
//                ResolveGraphFactory.deleteNode(dtNode);
//            }
//
//            Log.log.info("--- delete the decisiontree of : " + dt.getDisplayName());
//        }
    }

    public static void deleteDocument(Document document) throws Exception
    {
        Node documentNode = null;

//        if (document != null && StringUtils.isNotEmpty(document.getSys_id()))
//        {
//            documentNode = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
//
//            if (documentNode != null)
//            {
//                ResolveGraphFactory.deleteNode(documentNode);
//            }
//        }
    }

    public static void deleteRss(Rss rss) throws Exception
    {
        Node rssNode = null;

//        if (rss != null && StringUtils.isNotEmpty(rss.getSys_id()))
//        {
//            rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
//
//            if (rssNode != null)
//            {
//                ResolveGraphFactory.deleteNode(rssNode);
//            }
//        }
    }

    public static void deleteForum(Forum forum) throws Exception
    {
        Node forumNode = null;

//        if (forum != null && StringUtils.isNotEmpty(forum.getSys_id()))
//        {
//            forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
//
//            if (forumNode != null)
//            {
//                ResolveGraphFactory.deleteNode(forumNode);
//            }
//        }
    }

    public static void deleteNameSpace(Namespace namespace) throws Exception
    {
        Node namespaceNode = null;

//        if (namespace != null && StringUtils.isNotEmpty(namespace.getSys_id()))
//        {
//            namespaceNode = ResolveGraphFactory.findNodeByIndexedID(namespace.getSys_id());
//
//            if (namespaceNode != null)
//            {
//                ResolveGraphFactory.deleteNode(namespaceNode);
//            }
//        }
    }

    public static void deleteTeam(Team team) throws Exception
    {
        Node teamNode = null;

//        if (team != null && StringUtils.isNotEmpty(team.getSys_id()))
//        {
//            teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
//
//            if (teamNode != null)
//            {
//                ResolveGraphFactory.deleteNode(teamNode);
//            }
//        }
    }

    public static void deleteProcess(Process process) throws Exception
    {
        Node processNode = null;

//        if (process != null && StringUtils.isNotEmpty(process.getSys_id()))
//        {
//            processNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
//
//            if (processNode != null)
//            {
//                ResolveGraphFactory.deleteNode(processNode);
//            }
//        }
    }

    public static void deletePostUserTargetMentionRel(String postid, User user) throws Exception
    {
//        if (StringUtils.isNotEmpty(postid) && user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            removePostUserTargetAndMentionRelFronGraph(postid, user);
//        }
    }

    public static void removePostUserTargetAndMentionRelFronGraph(String postId, User user) throws Exception
    {

        // boolean lastRel = true;

        Node postNode = ResolveGraphFactory.findNodeByIndexedID(postId);
        Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());

        try
        {
            if (postNode != null && userNode != null)
            {

                // only delete relation, not index
                ResolveGraphFactory.deleteRelation(postNode, userNode, RelationType.MENTIONS);
                ResolveGraphFactory.deleteRelation(postNode, userNode, RelationType.TARGET);

                // check if no any relation to post, delete the post from graph
                // Iterable<Relationship> rels = postNode.getRelationships();

                // if(rels != null)
                // {
                // for(Relationship rel: rels)
                // {
                // lastRel = false;
                // break;
                // }
                // }

                // if(lastRel)
                // {
                // deletePost(postId);
                // }
                // else
                // {
                // String userRoles =
                // (StringUtils.isNotEmpty(user.getRoles()))?user.getRoles():"";
                // userRoles = userRoles + " " +
                // PostSocial.INTERNAL_POST_MEMBER_ROLE;
                // updateDocumentTarget(postId, user.getSys_id(), userRoles);
                //
                // Map<String, Object> queryMap = new ConcurrentHashMap<String,
                // Object>();
                // queryMap.put(SearchConstants.POST_ID.getTagName(), postId);
                // queryMap.put(SearchConstants.POST_ROLES.getTagName(),
                // userRoles);
                // queryMap.put(SearchConstants.USERS.getTagName(),
                // user.getSys_id());
                // queryMap.put(SocialConstants.POST_GUID,
                // Main.main.configId.guid);
                //
                // Main.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS,
                // "MSocial.updateDocumentTargetRel", queryMap);
                // }
            }
        }
        catch (Throwable t)
        {
            Log.log.warn("removePostUserTargetAndMentionRelFronGraph catched exception: " + t, t);
        }
    }

    public static void deleteRSComponent(RSComponent rscomponent)
    {
        try
        {
            if (ResolveGraphFactory.findNodeByIndexedID(rscomponent.getSys_id()) == null)
            {
                if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.container.Process"))
                {
                    deleteProcess((Process) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.container.Team"))
                {
                    deleteTeam((Team) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.container.Forum"))
                {
                    deleteForum((Forum) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.User"))
                {
                    deleteUser((User) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.ActionTask"))
                {
                    deleteActionTask((ActionTask) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.Runbook"))
                {
                    deleteRunbook((Runbook) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.Document"))
                {
                    deleteDocument((Document) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.WorkSheet"))
                {
                    deleteWorkSheet((Worksheet) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.DecisionTree"))
                {
                    deleteDecisionTree((DecisionTree) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.NameSpace"))
                {
                    deleteNameSpace((Namespace) rscomponent);
                }
                else if (rscomponent.getClass().getName().equals("com.resolve.services.graph.social.model.component.Rss"))
                {
                    deleteRss((Rss) rscomponent);
                }
            }
        }
        catch (Exception exp)
        {
            Log.log.error("deleteRSComponent catch Exception: " + exp, exp);
        }
    }

    /**
     * This API cleanup all relations between user - follow  --- namespaceNode
     * and also Runbook/Document/DecisionTree -- namespace --- namespaceNode
     *
     * cleanup all link between namespaceNode and users, and namespaceNode -- Runbook/Document/DecisionTree
     *
     * @throws Exception
     */
//    public static void purgeAllNameSpacesChild() throws Exception
//    {
//
//        int count = 0;
//        String namespaceDisplayName = null;
//
//        try
//        {
//            Analyzer analyzer = new StandardAnalyzer(IndexUtil.VERSION);
//
//            QueryParser parser = new QueryParser(IndexUtil.VERSION, Namespace.NAMESPACE_NAME, analyzer);
//            parser.setDefaultOperator(QueryParser.OR_OPERATOR);
//            parser.setAllowLeadingWildcard(true);
//            Query query = parser.parse("*");
//
//            IndexHits<Node> nodes = GraphDBUtil.getAutoNodeIndex().query(query);
//
//            if(nodes != null)
//            {
//                for(Node nodeLocal: nodes)
//                {
//                    namespaceDisplayName = (String) nodeLocal.getProperty("displayName");
//                    Log.log.info("--- the purged namespace name is : " + namespaceDisplayName);
//
//                    SocialNameSpaceFactory.deleteNameSpaceNodeChildOnly(nodeLocal);
//
//                    count++;
//                }
//            }
//
//            Log.log.info("--- total purge wikidoc number of " + count);
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp.getMessage(), exp);
//            throw exp;
//        }
//    }

    public static synchronized void deleteComponent(String expiredid, String type)
    {
        try
        {
            if (StringUtils.isNotEmpty(expiredid) && StringUtils.isNotEmpty(type))
            {
                Node nodeLocal = SocialDeleteCompFactory.findNodeByID(expiredid);

                if(nodeLocal == null)
                {
                    SocialDeleteCompFactory.create(expiredid, type);
                }
            }
        }
        catch(Exception exp)
        {
            Log.log.info("---- deleteComponent catched exception " + exp, exp);
        }
    }

    public static List<RSComponent> getAllDeletedComponents(long startTime, long endTime)
    {

        List<RSComponent> result = new ArrayList<RSComponent>();
        Runbook obj = new Runbook();

        try
        {
            TraversalDescription trav = Traversal.description().breadthFirst().relationships(SocialRelationshipTypes.DELETEDCOMP,
                                                    Direction.OUTGOING).evaluator(Evaluators.excludeStartPosition()).evaluator(Evaluators.atDepth(1));

            Node containerNode = SocialDeleteCompFactory.getContainerNode();

            if(containerNode != null)
            {
                boolean hasRel = containerNode.hasRelationship(SocialRelationshipTypes.DELETEDCOMP, Direction.OUTGOING);

                if(hasRel)
                {
                    for (Node nodeLocal : trav.traverse(containerNode).nodes())
                    {
                        try
                        {
                            obj = new Runbook();
//                            obj.setId((String) nodeLocal.getProperty(RSComponent.SYS_ID));
//                            obj.setSys_id((String) nodeLocal.getProperty(RSComponent.SYS_ID));
//                            obj.setSys_created_on((Long) nodeLocal.getProperty(RSComponent.SYS_CREATED_ON));
//                            obj.setSys_updated_on((Long) nodeLocal.getProperty(RSComponent.SYS_UPDATED_ON));
//                            obj.setDisplayName((String) nodeLocal.getProperty(RSComponent.TYPE));


                            if (obj != null)
                            {
                                long createdtime = obj.getSys_created_on();
                                long expiredtime = endTime - (1 * 10 * 24 * 60 * 60 * 1000l);

                                if (createdtime >= startTime && createdtime <= endTime)
                                {
                                    result.add(obj);
                                }
                                else if (createdtime <= expiredtime)
                                {
                                    ResolveGraphFactory.deleteNode(nodeLocal);
                                }
                            }
                        }
                        catch(Exception exp)
                        {}
                    }
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.info(t.getMessage(), t);
        }

        return result;
    }

}
