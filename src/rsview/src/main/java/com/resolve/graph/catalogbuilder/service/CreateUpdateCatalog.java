/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;


public class CreateUpdateCatalog
{
//    public static ResolveCatalog createCatalog(ResolveCatalog catalog, String username) throws Exception
//    {
//        //validate
//        String errors = verifyCatalog(catalog);
//        if(StringUtils.isNotBlank(errors))
//        {
//            throw new Exception(errors);
//        }
//        
//        return createUpdateCatalog(catalog, username);
//    }
//    
//    //
//    // ********************** Update Catalog *********************************************
//    //
//    public static String updateCatalog(ResolveCatalog catalogFromUI, String username) throws Exception
//    {
//        if (catalogFromUI != null && catalogFromUI.isRoot())
//        {
//            String catalogSysID = catalogFromUI.getId();
//            
//            if (StringUtils.isNotEmpty(catalogSysID))
//            {
//                //validate
//                String errors = verifyCatalog(catalogFromUI);
//                if(StringUtils.isNotBlank(errors))
//                {
//                    throw new Exception(errors);
//                }
//
//                
//                //delete the nodes that are not available from the UI and been deleted 
//                Set<String> validCatalogIdsFromUI = catalogFromUI.findAllCatalogNodeIds();
//                deleteCatalogNodes(catalogSysID, validCatalogIdsFromUI);
//                
//                // in this case -- not delete ref of other node.
////                DeleteCatalog.deleteCatalog(catalogSysID);
//                createUpdateCatalog(catalogFromUI, username);
//            }
//        }
//
//        Log.log.info("-- finished update catalog " + catalogFromUI.getName());
//        return catalogFromUI.getId();
//    }
//    
//    
//    public static void addRelation(String fromId, String toId, SocialRelationshipTypes type)
//    {
//        try
//        {
//            if(StringUtils.isNotEmpty(fromId) && StringUtils.isNotEmpty(toId) && type != null)
//            {
//                Node fromNode = CatalogFactory.findNodeByIndexedID(fromId);
//                Node toNode = CatalogFactory.findNodeByIndexedID(toId);
//                
//                if(fromNode != null && toNode != null)
//                {
//                    ResolveGraphFactory.addRelation(fromNode, toNode, type);
//                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//    }
//    
//    public static void deleteRelation(String fromId, String toId, SocialRelationshipTypes type)
//    {
//        try
//        {
//            if(StringUtils.isNotEmpty(fromId) && StringUtils.isNotEmpty(toId) && type != null)
//            {
//                Node fromNode = CatalogFactory.findNodeByIndexedID(fromId);
//                Node toNode = CatalogFactory.findNodeByIndexedID(toId);
//                
//                if(fromNode != null && toNode != null)
//                {
//                    ResolveGraphFactory.deleteRelation(fromNode, toNode, type);
//                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//    }
//    
//    
//    private static String verifyCatalog(ResolveCatalog catalog)
//    {
//        StringBuffer errors = new StringBuffer();
//        
//        if(catalog != null)
//        {
//            //validate if the catalog name already exist
//            try
//            {
//                String catalogName = catalog.getName();
//                String id = catalog.getId();
//                ResolveCatalog testCatalogName = GetCatalog.getCatalogByName(catalogName);
//                if(testCatalogName != null)
//                {
//                    if(!testCatalogName.getId().equalsIgnoreCase(catalog.getId()))
//                        errors.append("Catalog with name " + catalogName + " already exist. Please make the name unique.");
//                    else
//                    {
//                        catalog.setSys_created_by(testCatalogName.getSys_created_by());
//                        catalog.setSys_created_on(testCatalogName.getSys_created_on());
//                    }
//                }
//                else if(StringUtils.isNotBlank(id))
//                {
//                    //if the catalog exist, than use the same created by and created on
//                    testCatalogName = GetCatalog.getCatalog(id);
//                    if(testCatalogName != null)
//                    {
//                        catalog.setSys_created_by(testCatalogName.getSys_created_by());
//                        catalog.setSys_created_on(testCatalogName.getSys_created_on());
//                    }
//                }
//            
//            
//                
//            }
//            catch (Exception e)
//            {
//                Log.log.error("error getting the catalog with name for validation", e);
//                errors.append(e.getMessage());
//            }
//
//            //make sure that 'path' is updated for all the catalog nodes
//            catalog.updatePath();
//            
//            //update the wiki names in the catalog based on the root namespace set by the user
//            catalog.updateWikiDocumentNames();
//            
//            //validate if the doc names are valid
//            try
//            {
//                catalog.validate();
//            }
//            catch (Exception e)
//            {
//                Log.log.error(e.getMessage(), e);
//                errors.append(e.getMessage());
//            }
//            
//            
//           //validate if the docs referenced are valid - we will be creating stubs/place holder for docs
////           Set<String> docNames = catalog.findAllDocumentNames();
////           if(docNames != null && docNames.size() > 0)
////           {
////               for(String docFullName : docNames)
////               {
////                   if(StringUtils.isNotEmpty(docFullName))
////                   {
////                       WikiDocumentVO doc = ServiceWiki.getWikiDoc(null, docFullName, "system");
////                       if(doc == null)
////                       {
////                           errors.append("Document " + docFullName + " does not exist.").append("\n");
////                       }
////                   }
////               }
////           }
//        }
//        
//        return errors.toString();
//    }
//
//    /**
//     * pass catalog graph
//     * 
//     * @param catalog
//     * @throws Exception
//     */
//    private static ResolveCatalog createUpdateCatalog(ResolveCatalog catalog, String username) throws Exception
//    {
//        Set<String> catalogIDs = new HashSet<String>();
//
//        if (catalog != null)
//        {
//            Node rootNode = CatalogFactory.createUpdateCatalogNode(catalog, true);
//            catalog.convertNodeToObject(rootNode);
//            
//            //assign tags, wiki to this node
//            assignReferencesToCatalogItemNode(catalog, username);
//
//            List<ResolveCatalog> childList = new ArrayList<ResolveCatalog>();
//            if(catalog.getChildren() != null)
//            {
//                for (Object childNode : catalog.getChildren())
//                {
//                    if (childNode instanceof ResolveCatalog)
//                    {
//                        childList.add((ResolveCatalog) childNode);
//                    }
//                    else
//                    {
//                        JSONObject fromObject = JSONObject.fromObject(childNode);
//                        ResolveCatalog childCatalog = (ResolveCatalog) JSONObject.toBean(fromObject, ResolveCatalog.class);
//                        childList.add(childCatalog);
//                    }
//                }
//            }
//
//            if (childList != null && !childList.isEmpty())
//            {
//                for (ResolveCatalog childCatalog : childList)
//                {
//                    if (StringUtils.isEmpty(childCatalog.getId()) || !catalogIDs.contains(childCatalog.getId()))
//                    {
//                        createCatalogChild(rootNode, childCatalog, username);
//                        catalogIDs.add(childCatalog.getId());
//                    }
//                }
//            }
//
//        }
//
//        Log.log.info("-- finished create catalog " + catalog.getName());
//        return catalog;
//    }
//
//    /**
//     * This is recursive call
//     * 
//     * @param catalog
//     */
//    private static void createCatalogChild(Node parentNode, ResolveCatalog childCatalog, String username) throws Exception
//    {
//        Set<String> catalogIDs = new HashSet<String>();
//
//        if (parentNode != null && childCatalog != null)
//        {
//            String type = childCatalog.getType();
////            if(StringUtils.isNotBlank(type) && type.equalsIgnoreCase(SocialRelationshipTypes.CatalogReference.name()))
////            {
////                childCatalog.setId(null);
////            }
//            
//            Node childCatalogNode = CatalogFactory.createUpdateCatalogNode(childCatalog, false);
//            childCatalog.convertNodeToObject(childCatalogNode);
//            
//            //assign tags to this node
//            assignReferencesToCatalogItemNode(childCatalog, username);
//
//            List<ResolveCatalog> childChildCatalogList = new ArrayList<ResolveCatalog>();
//            if(childCatalog.getChildren() != null)
//            {
//                for (Object childNode : childCatalog.getChildren())
//                {
//                    if (childNode instanceof ResolveCatalog)
//                    {
//                        childChildCatalogList.add((ResolveCatalog) childNode);
//                    }
//                    else
//                    {
//                        JSONObject fromObject = JSONObject.fromObject(childNode);
//                        ResolveCatalog tempChildCatalog = (ResolveCatalog) JSONObject.toBean(fromObject, ResolveCatalog.class);
//                        childChildCatalogList.add(tempChildCatalog);
//                    }
//                }
//            }
//
//            if (childChildCatalogList != null && !childChildCatalogList.isEmpty())
//            {
//                //just take the first rec for ref catalog
//                ResolveCatalog childChildCatalogRef = childChildCatalogList.get(0);
//
//                //if the node is of type=CatalogReference, the child has an id of the reference catalog 
//                if (StringUtils.isNotBlank(type) && type.equalsIgnoreCase(SocialRelationshipTypes.CatalogReference.name()))
//                {
//                    //delete the first relation
//                    Iterable<Relationship> rels = childCatalogNode.getRelationships(SocialRelationshipTypes.CHILD_OF_CATALOG, Direction.INCOMING);
//                    if (rels != null)
//                    {
//                        for (Relationship rel : rels)
//                        {
//                            Node otherReferencedCatalog = rel.getOtherNode(childCatalogNode);
////                            String name = (String) otherReferencedCatalog.getProperty(ResolveCatalog.DISPLAYNAME);
////                            String id = (String) otherReferencedCatalog.getProperty(ResolveCatalog.SYS_ID);
////                            System.out.println("name:" + name + " - id:" + id);
//                            if (otherReferencedCatalog != null && otherReferencedCatalog.hasProperty(ResolveCatalog.IS_ROOT) && ((Boolean) otherReferencedCatalog.getProperty(ResolveCatalog.IS_ROOT)))
//                            {
//                                ResolveGraphFactory.deleteRelation(rel);
//                            }
//                        }
//                    }
//
//                    //create a new one
//                    if (StringUtils.isNotBlank(childChildCatalogRef.getId()))
//                    {
//                        Node refCatalogNode = ResolveGraphFactory.findNodeByIndexedID(childChildCatalogRef.getId());
//                        if (refCatalogNode != null)
//                        {
////                            String name = (String) refCatalogNode.getProperty(ResolveCatalog.DISPLAYNAME);
////                            String id = (String) refCatalogNode.getProperty(ResolveCatalog.SYS_ID);
////                            System.out.println("name:" + name + " - id:" + id);
//                            if (refCatalogNode != null)
//                            {
//                                //check if there are any existing CHILD_OF_CATALOG for the child node, if yes, delete that before adding the new one
//                                //this is important as the User must have done some move operation on the UI
//                                Iterable<Relationship> relsToDelete = childCatalogNode.getRelationships(SocialRelationshipTypes.CHILD_OF_CATALOG, Direction.OUTGOING);
//                                for(Relationship rel : relsToDelete)
//                                {
//                                    ResolveGraphFactory.deleteRelation(rel);
//                                }
//                                
//                                //add the new relationship
//                                ResolveGraphFactory.addRelation(refCatalogNode, childCatalogNode, SocialRelationshipTypes.CHILD_OF_CATALOG);
//                            }
//                        }
//                    }
//
//                }
//                else
//                {
//                    for (ResolveCatalog childChildCatalog : childChildCatalogList)
//                    {
//                        if (StringUtils.isEmpty(childChildCatalog.getId()) || !catalogIDs.contains(childChildCatalog.getId()))
//                        {
//                            //** RECURSIVE
//                            createCatalogChild(childCatalogNode, childChildCatalog, username);
//                            catalogIDs.add(childChildCatalog.getId());
//                        }
//                    }
//                }
//
//            }
//            
//            if(!childCatalog.isRootRef())
//            {
//                //check if there are any existing CHILD_OF_CATALOG for the child node, if yes, delete that before adding the new one
//                //this is important as the User must have done some move operation on the UI
//                Iterable<Relationship> relsToDelete = childCatalogNode.getRelationships(SocialRelationshipTypes.CHILD_OF_CATALOG, Direction.OUTGOING);
//                for(Relationship rel : relsToDelete)
//                {
//                    ResolveGraphFactory.deleteRelation(rel);
//                }
//            }
//            
//            
//            ResolveGraphFactory.addRelation(childCatalogNode, parentNode, childCatalog.isRootRef() ? SocialRelationshipTypes.CATALOG_REF : SocialRelationshipTypes.CHILD_OF_CATALOG);
//        }
//    }
//    
//
//    private static void deleteCatalogNodes(String catalogSysID, Set<String> validCatalogIdsFromUI) throws Exception
//    {
//        //get the persisted catalog first
//        ResolveCatalog dbCatalog = GetCatalog.getCatalog(catalogSysID);
//        if(dbCatalog != null)
//        {
//            //get the list of node ids to be deleted and delete the nodes
//            Set<String> idsToBeDeleted = findAllCatalogNodeIdsToBeDeleted(dbCatalog, validCatalogIdsFromUI);
//            if(idsToBeDeleted != null && idsToBeDeleted.size() > 0)
//            {
//                for(String id : idsToBeDeleted)
//                {
//                    Node node = ResolveGraphFactory.findNodeByIndexedID(id);
//                    if(node != null)
//                    {
//                        ResolveGraphFactory.deleteNode(node);
//                    }
//                }
//            }
//        }
//    }
//    
//    private static Set<String> findAllCatalogNodeIdsToBeDeleted(ResolveCatalog catalog, Set<String> validCatalogIdsFromUI)
//    {
//        Set<String> result = new HashSet<String>();
//        
//        if(catalog != null)
//        {
//            //add the id
//            if (StringUtils.isNotBlank(catalog.getId()) && !validCatalogIdsFromUI.contains(catalog.getId()))
//            {
//                result.add(catalog.getId());
//            }
//            
//            //add the child ids
//            List<ResolveCatalog> objects = catalog.getChildren();
//            if (objects != null && objects.size() > 0)
//            {
//                for (ResolveCatalog localCatalog : objects)
//                {
//                    boolean isRootRef = localCatalog.isRootRef();
//                    if(isRootRef)
//                    {
//                        //this is ref another catalog, so do not get the children from this point onwards
//                        result.add(localCatalog.getId());
//                    }
//                    else
//                    {
//                        //** RECURSIVE
//                        result.addAll(findAllCatalogNodeIdsToBeDeleted(localCatalog, validCatalogIdsFromUI));
//                    }
//                }//end of for loop
//            }
//        }
//        
//        return result;
//    }
//    
//
//    private static void assignReferencesToCatalogItemNode(ResolveCatalog catalog, String username) throws Exception
//    {
//        if(catalog != null)
//        {
//
////          //TODO : TEST
////            if(catalog.getName().equalsIgnoreCase("New Item"))
////            {
////                List<Tag> tags = new ArrayList<Tag>();
////                tags.add(new Tag("", "", "AA_NewTag"));
////                catalog.setTags(tags);
////            }
//            
//            
//            //first delete the existing tag relationships if any
//            Node catalogNode = ResolveGraphFactory.findNodeByIndexedID(catalog.getId());
//            if (catalogNode != null)
//            {
//                //assign tags
//                assignTagsToCatalogItemNode(catalogNode, catalog.getTags());
//                
//                //assign wiki - if the displayType = wiki
//                if (StringUtils.isNotBlank(catalog.getDisplayType()) 
//                                && StringUtils.isNotBlank(catalog.getWiki()) 
//                                && (catalog.getDisplayType().equalsIgnoreCase(ResolveCatalog.DISPLAY_TYPE_WIKI) || catalog.getDisplayType().equalsIgnoreCase(ResolveCatalog.DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
//                {
//                    assignWikiToCatalogItemNode(catalogNode, catalog.getWiki(), username);    
//                }
//                else
//                {
//                    //delete the existing relationship of this catalog with document
//                    ResolveGraphFactory.deleteAllRelationforNode(catalogNode, SocialRelationshipTypes.CATALOG_DOCUMENT_REF);
//                }
//                
//            }
//        }
//    }
//    
//    private static void assignWikiToCatalogItemNode(Node catalogNode, String documentFullNames, String username) throws Exception
//    {
//        if (catalogNode != null)
//        {
//            //delete the existing relationship of this catalog with document
//            ResolveGraphFactory.deleteAllRelationforNode(catalogNode, SocialRelationshipTypes.CATALOG_DOCUMENT_REF);
//
//            //check for the new doc relationship now
//            if(StringUtils.isNotBlank(documentFullNames))
//            {
//                Set<String> docs = StringUtils.stringToSet(documentFullNames, ",");
//                for(String documentFullName : docs)
//                {
//                    //get the doc object 
//                    WikiDocumentVO doc = ServiceWiki.getWikiDoc(null, documentFullName, "system");
//                    
//                    //if the doc does not exist, create it
//                    if (doc == null)
//                    {
//                        doc = createWikiPlaceHolder(documentFullName, username);
//                    }
//                    
//                    //get the document node
//                    Node documentNode = ResolveGraphFactory.findNodeByIndexedID(doc.getSys_id());
//                    if (documentNode == null)
//                    {
//                        throw new Exception("Graph Node for document " + documentFullName + " does not exist.");
//                    }
//
//                    //add the relationship
//                    CatalogUtil.assignWikiToCatalogItemNode(documentNode, catalogNode);
//                }
//            }
//        }
//    }
//    
//    private static void assignTagsToCatalogItemNode(Node catalogNode, List<Tag> tags) throws Exception
//    {
//        if (catalogNode != null)
//        {
//            //delete all the relations of Tags with this catalog node
//            ResolveGraphFactory.deleteAllRelationforNode(catalogNode, SocialRelationshipTypes.TAG_OF_CAT);
//            
//            //add the new ones
//            if (tags != null && tags.size() > 0)
//            {
//                for (Tag tag : tags)
//                {
//                    String tagName = tag.getName();
//                    String id = tag.getId();
//                    
//                    Node tagNode = null;
//                    
//                    //first check for the tagname and than for the id
//                    if (StringUtils.isNotBlank(tagName))
//                    {
////                        tagNode = TagFactory.findNodeByTagName(tagName);
//                    }
//                    else if (StringUtils.isNotBlank(id))
//                    {
//                        tagNode = ResolveGraphFactory.findNodeByIndexedID(id);
//                    }
//                    
//                    try
//                    {
//                        //if the tagNode is null, than it does not exist in the system. So create it
//                        if(tagNode == null && StringUtils.isNotBlank(tagName))
//                        {
////                            ResolveTag newTag = ServiceTag.createTag(tagName, "system");
////                            tagNode = ResolveGraphFactory.findNodeByIndexedID(newTag.getId());
//                        }
//                        
//                        //make sure that the tag is available in graph
//                        if (tagNode != null)
//                        {
//                            ResolveGraphFactory.addRelation(tagNode, catalogNode, SocialRelationshipTypes.TAG_OF_CAT);
//                        }
//                        else
//                        {
//                            throw new Exception("Tag " + id + ":" + tagName + " is not available in graph");
//                        }
//                    }
//                    catch (Exception e)
//                    {
//                        Log.log.error("Error in assigning the tag to catalog node", e);
//                    }
//                    
//                }//end of for loop
//            }
//        }
//
//    }
//    
//    private static WikiDocumentVO createWikiPlaceHolder(String documentFullName, String username) throws Exception
//    {
//        WikiDocumentVO doc = null;
//        String[] arr = documentFullName.split("\\.");
//        if(StringUtils.isEmpty(username))
//        {
//            username = "system";
//        }
//        
//        //create a stub/place holder for this document
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put(ConstantValues.WIKI_NAMESPACE_KEY, arr[0]);
//        params.put(ConstantValues.WIKI_DOCNAME_KEY, arr[1]);
//        params.put(ConstantValues.WIKI_CONTENT_KEY, "Missing Wiki Content");
//        try
//        {
//            doc = ServiceWiki.createWiki(params, username);
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error in creating doc :" + documentFullName, e);
//            throw new Exception("Document " + documentFullName + " was not created successfully.");
//        }
//        
//        return doc;
//    }
}
