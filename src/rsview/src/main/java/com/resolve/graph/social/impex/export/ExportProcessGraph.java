/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.export;

import java.util.List;

import org.neo4j.graphdb.Node;

import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.util.TraversalUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.GraphRelationshipDTO;

public class ExportProcessGraph extends ExportComponentGraph
{
    public ExportProcessGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
        super(sysId, options, SocialRelationshipTypes.PROCESS);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode, SocialImpexVO socialImpex) throws Exception
    {
      //get all the nodes that this process node is refering too
        Iterable<Node> membersOfProcessNode = TraversalUtil.findMembersBelongingToProcess(compNode);
        if(membersOfProcessNode != null)
        {
            for(Node anyNode : membersOfProcessNode)
            {
                prepareReturnNode(compNode, anyNode, socialImpex);
            }//end of for
        }
        
        return relationships;
    }
    
    private void prepareReturnNode(Node compNode, Node anyNode, SocialImpexVO socialImpex) throws Exception
    {
//        String type = (String) anyNode.getProperty(RSComponent.TYPE);
//        String displayName = (String) anyNode.getProperty(RSComponent.DISPLAYNAME);
//        String sysId = (String) anyNode.getProperty(RSComponent.SYS_ID);
//        
//        if(type.equalsIgnoreCase(SocialRelationshipTypes.FORUM.name()))
//        {
//            if(options != null && options.getProcessForums())
//            {
//                addRelationship(compNode, anyNode);
//                socialImpex.addReturnForum(new Forum(sysId, displayName));
//            }
//        }
//        else if(type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
//        {
//            if(options != null && options.getProcessTeams())
//            {
//                addRelationship(compNode, anyNode);
//                socialImpex.addReturnTeam(new Team(sysId, displayName));
//            }
//        }
//        else if(type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//        {
//            if(options != null && options.getProcessWiki())
//            {
//                addRelationship(compNode, anyNode);
//                socialImpex.addReturnDocument(new Document(sysId, displayName));
//            }
//        }
//        else if(type.equalsIgnoreCase(SocialRelationshipTypes.ACTIONTASK.name()))
//        {
//            if(options != null && options.getProcessATs())
//            {
//                addRelationship(compNode, anyNode);
//                socialImpex.addReturnActionTask(new ActionTask(sysId, displayName));
//            }
//        }
//        else if(type.equalsIgnoreCase(SocialRelationshipTypes.RSS.name()))
//        {
//            if(options != null && options.getProcessRSS())
//            {
//                addRelationship(compNode, anyNode);
//                socialImpex.addReturnRss(new Rss(sysId, displayName));
//            }
//        }
//        else if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//        {
//            if(options != null && options.getProcessUsers())
//            {
//                addRelationship(compNode, anyNode);
//                String userName = (String) anyNode.getProperty(RSComponent.USERNAME);
//                socialImpex.addReturnUser(new User(sysId, userName, displayName));
//            }
//        }
        
    }
    
    
}
