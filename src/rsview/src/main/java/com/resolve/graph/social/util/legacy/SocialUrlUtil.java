/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util.legacy;

import javax.servlet.http.HttpServletRequest;

import com.resolve.services.graph.social.model.SocialUIPreferences;
import com.resolve.util.Constants;

public class SocialUrlUtil
{
    public static String RSCLIENT_BASE_URL = "/resolve/jsp/rsclient.jsp?url=";
    private static String RSSOCIAL_BASE_URL = RSCLIENT_BASE_URL + SocialUtil.encodeUrlForJS("/resolve/social/rssocial.jsp?");
    
    private String uri = null;
    
    private String type = "";
    private String sysId = null;
    private String value = null;
    private String username = null;
    private String url = "";
    private HttpServletRequest request = null;
    
    private SocialUIPreferences preferences = null;
    
    public SocialUrlUtil(String type, String sysId)
    {
        this.type = type;
        this.sysId = sysId;        
    }
    
    public SocialUrlUtil(HttpServletRequest request)
    {
        this.type =  request.getParameter("type");//url, document, actiontask, worksheet, forum, rss, user;
        this.sysId = request.getParameter("sysId");//sysid, PRB#;      
        this.value = request.getParameter("value");
        this.username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        this.request = request;
        
        uri = request.getRequestURL().toString();//http://127.0.0.1:8080/resolve/service/social/showentity
        uri = uri.substring(0, uri.indexOf("/resolve"));
    }
}