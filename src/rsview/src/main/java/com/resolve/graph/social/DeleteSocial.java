/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social;

import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialNameSpaceFactory;
import com.resolve.services.graph.social.model.RSComponent;

public class DeleteSocial
{
    public static void deleteComponent(String compSysId, String username)  throws Exception
    {
        if(StringUtils.isNotBlank(compSysId))
        {
            Node compNode = ResolveGraphFactory.findNodeByIndexedID(compSysId);
            if(compNode != null)
            {
                String displayName = null;
                
                //for a Document, if this is the last documnent, delete the Namespace node also
//                if(compNode.hasProperty(RSComponent.TYPE) && ((String) compNode.getProperty(RSComponent.TYPE)).equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//                {
//                    displayName = compNode.hasProperty(RSComponent.DISPLAYNAME) ? (String) compNode.getProperty(RSComponent.DISPLAYNAME): null;
//                }

                //delete the node
                ResolveGraphFactory.deleteNode(compNode);

                //delete the NS if applicable
                if(StringUtils.isNotEmpty(displayName) && displayName.indexOf('.') > -1)
                {
                    deleteNamespace(displayName.split("\\.")[0], username);
                }
            }
        }
    }
    
    private static void deleteNamespace(String namespace, String username) throws Exception
    {
        if(StringUtils.isNotEmpty(namespace))
        {
            Node compNode = SocialNameSpaceFactory.findNameSpaceNodeByName(namespace);
            if(compNode != null)
            {
                boolean deleteNS = true;
                Iterable<Relationship> relationships = compNode.getRelationships(RelationType.NAMESPACE);
                for(Relationship rel : relationships)
                {
                    Node docNode = rel.getOtherNode(compNode);
//                    if(docNode.hasProperty(RSComponent.TYPE) && ((String) docNode.getProperty(RSComponent.TYPE)).equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//                    {
//                        deleteNS = false;
//                        break;
//                    }
                }//end of for
                
                //if the flag is true, delete it
                if(deleteNS)
                {
                  //delete the node
                    ResolveGraphFactory.deleteNode(compNode);
                }
            }
        }
    }
    
    
}
