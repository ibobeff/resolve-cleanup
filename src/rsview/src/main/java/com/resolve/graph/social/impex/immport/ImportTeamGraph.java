/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.services.vo.social.SocialTeamDTO;

public class ImportTeamGraph  extends ImportComponentGraph
{
    private Team team = null;
    
    public ImportTeamGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        SocialTeamDTO teamDTO =  ServiceHibernate.getTeamByName(rel.getSourceName(), username);
        if(teamDTO != null)
        {
            Node teamNode = ResolveGraphFactory.findNodeByIndexedID(teamDTO.getSys_id());
            if(teamNode != null)
            {
                team = new Team();
//                team.convertNodeToObject(teamNode);

                switch(rel.getTargetType())
                {
                    case USER: 
                        addUserToTeam();
                        break;
                        
                    case TEAM :
                        addTeamToTeam();
                        break;
                        
                    default:
                        break;
                }
            }
            else
            {
                throw new Exception("Team " + teamDTO.getU_display_name() + " is not sync with graph db." );
            } 
        }
    }
    
    private void addUserToTeam() throws Exception
    {
        String usernameTobeAdded = rel.getTargetName();
        User user = SocialCompConversionUtil.getSocialUser(usernameTobeAdded);
        if(user != null)
        {
//            Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            if(userNode != null)
//            {
//                //add user to this team
//                ServiceSocial.addUserToTeam(user, team);
//            }
//            else
//            {
//                throw new Exception("User " + user.getUsername() + " is not sync with graph db." );
//            } 
        }
    }
    
    
    
    private void addTeamToTeam() throws Exception
    {
        String teamName = rel.getTargetName();
        SocialTeamDTO teamDTO = ServiceHibernate.getTeamByName(teamName, username);
        if(teamDTO != null)
        {
            Node teamNode = ResolveGraphFactory.findNodeByIndexedID(teamDTO.getSys_id());
//            if(teamNode != null)
//            {
//                Team teamGrpahModel = new Team();
//                teamGrpahModel.convertNodeToObject(teamNode);
//                
//                //add team to this process
//                ServiceSocial.addTeamToTeam(teamGrpahModel, team);
//            }
//            else
//            {
//                throw new Exception("Team " + teamDTO.getU_display_name() + " is not sync with graph db." );
//            } 
        }
    }
    

}
