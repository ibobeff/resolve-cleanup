/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.export;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import org.neo4j.graphdb.Node;

import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.GraphRelationshipDTO;

public class ExportCatalogGraph extends ExportComponentGraph
{
    private Set<String> catalogIdsToTraverse = new ConcurrentSkipListSet<String>();

    public ExportCatalogGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
        super(sysId, options, SocialRelationshipTypes.CHILD_OF_CATALOG);
    }

    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode, SocialImpexVO socialImpex) throws Exception
    {
        if(options.catalogRefCatalogs)
        {
            catalogIdsToTraverse.add(sysId);

            //added the while loop as the for loop inside takes only the current state of the object and if new values are added, it will skip it
            //this while loop takes care of that
            while(catalogIdsToTraverse.size() > 0)
            {
                for(String sysId : catalogIdsToTraverse)
                {
//                    ResolveCatalog catalog = ServiceCatalog.getCatalog(sysId);
//                    if(catalog != null)
//                    {
//                        traverseCatalog(catalog);
//                        catalogIdsToTraverse.remove(sysId);
//                    }
                }//end of for
            }//end of while
        }

        return relationships;
        
    }
    
//    private void traverseCatalog(ResolveCatalog catalog) throws Exception
//    {
//        if (catalog != null)
//        {
//            //if this is a Root Ref node and the type is 'CatalogReference'
//            if (catalog.isRootRef() && catalog.getType().equals("CatalogReference"))
//            {
//                //get the ref catalog. There will always be 1
//                ResolveCatalog refCatalog = catalog.getChildren().get(0);
//                
//                //get the nodes
//                Node sourceNode = ResolveGraphFactory.findNodeByIndexedID(refCatalog.getId());
//                Node targetNode = ResolveGraphFactory.findNodeByIndexedID(catalog.getId());
//                
//                //add it to the relationship
//                addRelationship(sourceNode, targetNode);
//                
//                //set the chilren to null and add the ref id to be traversed
//                catalog.setChildren(null);
//                catalogIdsToTraverse.add(refCatalog.getId());
//            }
//            
//            List<ResolveCatalog> localCatalogList = catalog.getChildren();
//            if (localCatalogList != null && localCatalogList.size() > 0)
//            {
//                for (ResolveCatalog localCatalog : localCatalogList)
//                {
//                    //**RECURSIVE
//                    traverseCatalog(localCatalog);
//                }
//            }
//        }
//    }
    
    //have to override as Catalog info is different compared to others
    @Override
    protected void addRelationship(Node compNode, Node anyOtherNode) throws Exception
    {
//        String sourceName = (String) compNode.getProperty(RSComponent.SYS_ID);
//        String sourceType = this.type.name();
//        String targetName = (String) anyOtherNode.getProperty(RSComponent.SYS_ID);
//        String targetType = this.type.name();
//        
//        GraphRelationshipDTO relation = new GraphRelationshipDTO();
//        relation.setSourceName(sourceName);
//        relation.setSourceType(SocialRelationshipTypes.valueOf(sourceType));
//        relation.setTargetName(targetName);
//        relation.setTargetType(SocialRelationshipTypes.valueOf(targetType));
//        
//        relationships.add(relation);
    }
    

}
