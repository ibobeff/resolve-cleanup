/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class SocialDeleteCompFactory extends SocialFactory
{   
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.DELETEDCOMP, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.DELETEDCOMP.name(), containerNode);
    }
    
    public static void create(String compID, String type) throws Exception
    {
        Transaction tx = null;
               
        try
        {
            getContainerNode();
            
            if(StringUtils.isNotEmpty(compID) && StringUtils.isNotEmpty(type))
            {
                Node nodeLocal = ResolveGraphFactory.findNodeByIndexedID(compID);
                
                if(nodeLocal == null)
                {
                    tx = GraphDBUtil.getGraphDBService().beginTx();  
                    Node node = GraphDBUtil.getGraphDBService().createNode();
                    
//                    node.setProperty(RSComponent.SYS_ID, compID);
//                    node.setProperty(RSComponent.DISPLAYNAME, compID);
//                    node.setProperty(RSComponent.SYS_CREATED_ON, System.currentTimeMillis());
//                    node.setProperty(RSComponent.SYS_UPDATED_ON, System.currentTimeMillis());
//                    node.setProperty(RSComponent.TYPE, type);
//                    node.setProperty(NODE_TYPE, SocialRelationshipTypes.DELETEDCOMP.name());
                    tx.success();
                    
                    Node nodeAfter = ResolveGraphFactory.findNodeByIndexedID(compID);
                    
                    if(nodeAfter != null)
                    {
                        ResolveGraphFactory.addRelation(containerNodeLocal, nodeAfter, SocialRelationshipTypes.DELETEDCOMP);
                    }
                }
            }
        }
        catch(Throwable t)
        {
            Log.log.warn("create delete comp catched exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
        
    }
    
    public static Node findNodeByID(String id)
    {
        Node resultNode = null;
        getContainerNode();
        
        if(StringUtils.isNotEmpty(id))
        {
            Iterable<Relationship> rels = containerNodeLocal.getRelationships(SocialRelationshipTypes.DELETEDCOMP, Direction.OUTGOING);
            
            if(rels != null)
            {
                for(Relationship rel: rels)
                {
                    Node endNode = rel.getEndNode();
                    
//                    if(endNode != null 
//                             && endNode.getProperty(RSComponent.SYS_ID).equals(id))
//                    {
//                        resultNode = endNode;
//                        break;
//                    }
                }
            }
        }
        
        return resultNode;
    }
    
    public Node findNode(String id)
    {
        return null;
    }
    
    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.DELETEDCOMP.name());
        }
        
        return containerNodeLocal;
    }

}
