/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.tag;


public class TagFactory
{
//    public static ResolveTag insertOrUpdateNode(ResolveTag tag, String username) throws Exception
//    {
//        if (tag != null)
//        {
//            //verify if the tag node exist
//            Node tagNode = ResolveGraphFactory.findNodeByIndexedID(tag.getSys_id());
//            if(tagNode != null)
//            {
//                //this is update..so continue
//            }
//            else
//            {
//                //this is insert
//                //varify if the tag name exist or not                
//                ResolveTag tagByName = findTagByNameInGraphDB(tag.getName());
//                if(tagByName != null
//                                && StringUtils.isNotEmpty(tag.getSys_id())
//                                && !tag.getSys_id().equalsIgnoreCase(tagByName.getSys_id()))
//                {
//                    throw new Exception("Tag with name " + tagByName.getName() + " already exist.");
//                }
//            }
//            
//            //update or insert now            
//            tagNode = ResolveGraphFactory.insertOrUpdateNode(tag, ResolveRelationshipType.ResolveTag);
//            if(tagNode != null)
//            {
//                //update the model from the node
//                tag.convertNodeToObject(tagNode);
//            }
//            
//            Log.log.trace("-- created resolveTag id is -- " + tag.getSys_id() + " -- and name is --" + tag.getName());
//
//        }
//        return tag;
//    }
//    
//    public static Set<ResolveTag> findAllTags()
//    {
//        Set<ResolveTag> tags = new HashSet<ResolveTag>();
//        Node containerNode = ResolveGraphFactory.getContainerNode(SocialRelationshipTypes.ResolveTag);
//        
//        if(containerNode != null)
//        {
//            Iterable<Relationship> typeRels = containerNode.getRelationships(SocialRelationshipTypes.ResolveTag);
//            if(typeRels != null)
//            {
//                for(Relationship rel : typeRels)
//                {
//                    Node node = rel.getOtherNode(containerNode);
//                    if(node.hasProperty(RSComponent.TYPE))
//                    {
//                        ResolveTag tag = new ResolveTag();
//                        tag.convertNodeToObject(node);
//
//                        tags.add(tag);
//                    }
//                }
//            }
//        }
//        
//        return tags;
//    }
//    
//    /**
//     * api used to sync tags from ES to Graph
//     * 
//     * @param esTag
//     * @param username
//     * @return
//     * @throws Exception
//     */
//    public static ResolveTag syncTag(ResolveTag esTag, String username) throws Exception
//    {
//        ResolveTag graphTag = null;
//        
//        if(esTag != null)
//        {
//            //make sure that sysId is present for sync, else its a save tag 
////            if(StringUtils.isEmpty(esTag.getSys_id()))
////            {
////                return insertOrUpdateNode(esTag, username);
////            }
//            
//            graphTag = findTagByNameInGraphDB(esTag.getName());
//            if(graphTag != null)
//            {
//                //tag already exist, check that the sysIds are same, if not, update the sysId
//                if(!graphTag.getSys_id().equalsIgnoreCase(esTag.getSys_id()))
//                {
//                    Transaction tx = null;
//                    Node tagNode = null;
//
//                    try
//                    {
//                        //get the node using the older sysId
//                        tagNode = ResolveGraphFactory.findNodeByIndexedID(graphTag.getSys_id());
//                        if(tagNode != null)
//                        {
//                            //start transaction
//                            tx = GraphDBUtil.getGraphDBService().beginTx();
//                            
//                            //update the sysId same as in ES
//                            tagNode.setProperty(RSComponent.SYS_ID, esTag.getSys_id());
//                            graphTag.setSys_id(esTag.getSys_id());
//                            
//                            tx.success();
//                        }
//                    }
//                    catch(Exception t)
//                    {
//                        Log.log.error("create node exception: " + t, t);
//                        throw t;
//                    }
//                    finally
//                    {
//                        if(tx != null)
//                        {
//                            tx.finish();
//                        }
//                    }
//                }
//            }
//            else
//            {
//                //create this tag as it does not exist in Graph
//                graphTag = insertOrUpdateNode(esTag, username);
//            }
//            
//        }
//        
//        return graphTag;
//    }
//    
////    public static Tag saveTag(Tag tag)throws Exception
////    {
////        if(tag != null)
////        {
////            ResolveTag rsTag = TagUtil.convertTagToResolveTag(tag);
////            rsTag = insertOrUpdateNode(rsTag, "system");
////            
////            tag = TagUtil.convertResolveTagToTag(rsTag);
////        }
////        
////        return tag;
////    }
//    
////    public static ResolveTag createTag(String tagName, String username) throws Exception
////    {
////        ResolveTag tag = null;
////        
////        if(StringUtils.isNotBlank(tagName))
////        {
////            tag = insertOrUpdateNode(new ResolveTag(tagName), username);
////        }
////        
////        return tag;
////    }
//    
////    public static void updateTagName(String fromName, String toName) throws Exception
////    {
////        Transaction tx = null;
////
////        try
////        {
////            if(StringUtils.isNotEmpty(fromName) && StringUtils.isNotEmpty(toName))
////            {
////                Node tagNode = ResolveGraphFactory.findNodeByIndexedID(fromName);
////
////                if(tagNode != null)
////                {
////                    tx = GraphDBUtil.getGraphDBService().beginTx();
////                    tagNode.setProperty(ResolveGraphNode.SYS_ID, toName);
////                    tagNode.setProperty(ResolveGraphNode.DISPLAYNAME, toName);
////
////                    tx.success();
////                }
////            }
////        }
////        catch(Exception t)
////        {
////            Log.log.warn("update node exception: " + t, t);
////            throw t;
////        }
////        finally
////        {
////            if(tx != null)
////            {
////                tx.finish();
////            }
////        }
////
////    }
//
//    
//    /**
//     * @param tag -- right now ResolveTag just one single node
//     * @return the sysid of the created Resolvetag in graph
//     * @throws Exception
//     */
//    public static void createResolveTags(List<ResolveTag> resolveTags, boolean index) throws Exception
//    {
//        if(resolveTags != null && !resolveTags.isEmpty())
//        {
//            for(ResolveTag resolveTag: resolveTags)
//            {
//                insertOrUpdateNode(resolveTag, "system");
//            }
//        }
//      
//    }
//
//    public static void addAllResolveTagFromDBToGraph() throws Exception
//    {
//        List<Tag> tags = ServiceHibernate.getAllResolveTagsNamesFromDB();
//        if (tags != null && tags.size() > 0)
//        {
//            for (Tag tag : tags)
//            {
//                try
//                {
//                    ServiceTag.saveTag(tag, "system");
//                }
//                catch (Exception e)
//                {
//                    Log.log.error("error creating the tag " + tag, e);
//                }
//            }
//
//            //purge the table
//            ServiceHibernate.purgeDBTable("ResolveTag", "system");
//        }
//    }
//
//    public static void deleteResolveTag(String sysId) throws Exception
//    {
//        if (StringUtils.isNotBlank(sysId))
//        {
//            try
//            {
//                Node tagNode = ResolveGraphFactory.findNodeByIndexedID(sysId);
//                if (tagNode != null)
//                {
//                    ResolveGraphFactory.deleteNode(tagNode);
//                }
//            }
//            catch (Exception t)
//            {
//                Log.log.warn("deleteNode exception: " + t, t);
//                throw t;
//            }
//        }
//        
//    }
//    
//    public static void deleteResolveTag(Set<String> tagSysIds) throws Exception
//    {
//        if(tagSysIds != null)
//        {
//            for(String sysId : tagSysIds)
//            {
//                deleteResolveTag(sysId);
//            }
//        }
//    }
//    
//    public static ResolveTag getResolveTag(String id) throws Exception
//    {
//        ResolveTag resolveTag = null;
//
//        try
//        {
//            if(StringUtils.isNotEmpty(id))
//            {
//                Node tagNode = ResolveGraphFactory.findNodeByIndexedID(id);
//
//                if(tagNode != null)
//                {
//                    resolveTag = new ResolveTag();
//                    resolveTag.convertNodeToObject(tagNode);
//                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//
//        return resolveTag;
//    }
//    
//    public static ResolveTag findTagByNameInGraphDB(String tagName) throws Exception
//    {
//        ResolveTag tag = null;
//        
//        //get the container node
//        Node tagNode = findNodeByTagName(tagName);
//        if(tagNode != null)
//        {
//            tag = new ResolveTag();
//            tag.convertNodeToObject(tagNode);
//        }
//        
//        return tag;
//    }
//    
//    
//    public static void updateTagsForComponent(String compId, Set<String> tagNames) throws Exception
//    {
//        if(StringUtils.isNotBlank(compId))
//        {
//            Node compNode = ResolveGraphFactory.findNodeByIndexedID(compId);
//            if(compNode != null)
//            {
//                //delete the existing tag relationships first
//                Iterable<Relationship> rels = compNode.getRelationships(ResolveRelationshipType.ResolveTag);
//                if(rels != null)
//                {
//                    for(Relationship rel : rels)
//                    {
//                        ResolveGraphFactory.deleteRelation(rel);
//                    }
//                }
//                
//                if(tagNames != null && tagNames.size() > 0)
//                {
//                    addAdditionalTagsToComponent(compId, tagNames);
//                }
//            }
//        }
//    }
//    
//    public static void assignTagsToComponentWithIds(Set<String> compIds, Set<String> tagIds, String username) throws Exception
//    {
//        if(compIds != null && tagIds != null && StringUtils.isNotBlank(username))
//        {
//            for(String compId : compIds)
//            {
//                Node compNode = ResolveGraphFactory.findNodeByIndexedID(compId);
//                if(compNode != null)
//                {
//                    for(String tagId : tagIds)
//                    {
//                        //assign the tag
//                        assignTag(compNode, tagId);
//                    }
//                }
//            }
//        }
//    }
//
//    
//    
//    public static void addAdditionalTagsToComponent(String compId, Set<String> tagNames) throws Exception
//    {
//        if(StringUtils.isNotBlank(compId) && tagNames != null && tagNames.size() > 0)
//        {
//            Node compNode = ResolveGraphFactory.findNodeByIndexedID(compId);
//            if(compNode != null)
//            {
//                //add the new ones 
//                for(String tagName : tagNames)
//                {
//                    if (StringUtils.isNotBlank(tagName))
//                    {
//                        tagName = tagName.replaceAll(HibernateConstants.REGEX_TAG_NAME, "_");
//                        try
//                        {
//                            Tag tag = null;
//                            ResolveTag rsTag = findTagByNameInGraphDB(tagName);
//                            if (rsTag == null)
//                            {
//                                //create one as you have the name
//                                tag = ServiceTag.saveTag(tagName, "system");
//                            }
//                            else
//                            {
//                                tag = TagUtil.convertResolveTagToTag(rsTag);
//                            }
//                            
//                            //assign the tag
//                            assignTag(compNode, tag.getSysId());
//                        }
//                        catch (Exception e)
//                        {
//                            Log.log.error("Issue with assigning tag " + tagName + " to Comp " + compNode.getProperty(RSComponent.DISPLAYNAME), e);
//                        }
//                    }
//                    else
//                    {
//                        Log.log.debug("Note that Tag is empty.");
//                    }
//                }
//            }
//        }
//    }
//    
//    public static void addDeleteAdditionalTagsToComponentUsingSysIds(Set<String> compIds, Set<String> tagSysIds, boolean add) throws Exception
//    {
//        if(compIds != null && compIds.size() > 0 && tagSysIds != null && tagSysIds.size() > 0)
//        {
//            Set<Node> tagNodes = new HashSet<Node>();
//            
//            //collect the tag nodes
//            for(String tagSysId : tagSysIds)
//            {
//                Node tagNode = ResolveGraphFactory.findNodeByIndexedID(tagSysId);
//                if(tagNode != null)
//                {
//                    tagNodes.add(tagNode);
//                }
//            }
//            
//            //add the tag and comp relationship
//            for(String compId : compIds)
//            {
//                Node compNode = ResolveGraphFactory.findNodeByIndexedID(compId);
//                if(compNode != null)
//                {
//                    for(Node tagNode : tagNodes)
//                    {
//                        if(add)
//                        {
//                            //add the relationship now
//                            ResolveGraphFactory.addRelation(tagNode, compNode, ResolveRelationshipType.ResolveTag);
//                        }
//                        else
//                        {
//                            //delete the relationship now
//                            ResolveGraphFactory.deleteRelation(tagNode, compNode, ResolveRelationshipType.ResolveTag);
//                        }
//                    }//end of for loop
//                }
//            }//end of for
//        }
//                  
//    }
//    
//    public static List<ResolveTag> getTagsForComponent(String compId)  throws Exception
//    {
//        List<ResolveTag> tags = new ArrayList<ResolveTag>();
//        
//        if(StringUtils.isNotBlank(compId))
//        {
//            Node compNode = ResolveGraphFactory.findNodeByIndexedID(compId);
//            if(compNode != null)
//            {
//                Iterable<Node> tagNodes = TraversalUtil.findTagsFor(compNode);
//                if(tagNodes != null)
//                {
//                    for(Node tagNode : tagNodes)
//                    {
//                        ResolveTag tag = new ResolveTag();
//                        tag.convertNodeToObject(tagNode);
//                        
//                        tags.add(tag);
//                    }//end of for
//                }
//            }
//        }
//        
//        return tags;
//    }
//    
//    public static Set<RSComponent> getCompsReferredByTagNames(Set<String> tagNames) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//
//        if(tagNames != null && tagNames.size() > 0)
//        {
//            Set<String> tagSysIds = new HashSet<String>();
//            for(String tagName : tagNames)
//            {
//                ResolveTag tag = findTagByNameInGraphDB(tagName);
//                if(tag != null)
//                {
//                    tagSysIds.add(tag.getSys_id());
//                }
//            }
//            
//            result.addAll(getCompsReferredByTagSysIds(tagSysIds));
//            
//        }
//        
//        return result;
//    }
//    
//    public static Set<RSComponent> getCompsReferredByTagSysIds(Set<String> tagSysIds) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//        
//        if(tagSysIds != null)
//        {
//            for(String tagSysId : tagSysIds)
//            {
//                Node tagNode = ResolveGraphFactory.findNodeByIndexedID(tagSysId);
//                if(tagNode != null && tagNode.hasProperty(ResolveCatalog.TYPE))
//                {
//                    String type = (String) tagNode.getProperty(ResolveCatalog.TYPE);
//                    if(type.equalsIgnoreCase(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                    {
//                       //for this tagNode, get the documents refered by it
//                        result.addAll(findAllComponentsReferencedByTag(tagNode));
//                    }
//                }
//            }
//        }
//        
//        
//        return result;
//    }
//    
//    public static List<String> getTagsNamesForComponent(String compId)  throws Exception
//    {
//        List<String> tagNames = new ArrayList<String>();
//        
//        List<ResolveTag> tags = getTagsForComponent(compId);
//        for(ResolveTag tag : tags)
//        {
//            tagNames.add(tag.getName());
//        }
//        
//        return tagNames;
//    }
//    
//
//    public static Node findNodeByTagName(String tagName)
//    {
//        Node nodeLocal = null;
//        
//        if(StringUtils.isNotEmpty(tagName))
//        {
//            tagName = tagName.toLowerCase();
//            IndexHits<Node> nodes = GraphDBUtil.getAutoNodeIndex().get(ResolveGraphNode.TAG_NAME, tagName);
//            for(Node node: nodes)
//            {
//                if(((String)node.getProperty(ResolveGraphNode.TAG_NAME)).equalsIgnoreCase(tagName))
//                {
//                    nodeLocal = node;
//                    break;
//                }
//            }
//        }
//        
//        return nodeLocal;
//    }
//
//    private static void assignTag(Node compNode, String tagSysId)  throws Exception
//    {
//        if(compNode != null && StringUtils.isNotBlank(tagSysId))
//        {
//            //get the tagNode
//            Node tagNode = ResolveGraphFactory.findNodeByIndexedID(tagSysId);
//            
//            //add the relationship now
//            ResolveGraphFactory.addRelation(tagNode, compNode, ResolveRelationshipType.ResolveTag);
//        }
//    }
//    
//
//    private static Set<RSComponent> findAllComponentsReferencedByTag(Node tagNode) throws Exception
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//
//        if (tagNode != null)
//        {
//            Iterable<Relationship> rels = tagNode.getRelationships(ResolveRelationshipType.ResolveTag);
//            if (rels != null)
//            {
//                for (Relationship rel : rels)
//                {
//                    Node documentNode = rel.getOtherNode(tagNode);
//                    if (documentNode != null && documentNode.hasProperty(RSComponent.TYPE))
//                    {
//                        RSComponent doc = new RSComponent();
//                        doc.convertNodeToObject(documentNode);
//
//                        result.add(doc);
//                    }
//                }
//            }
//        }
//
//        return result;
//    }
    

}
