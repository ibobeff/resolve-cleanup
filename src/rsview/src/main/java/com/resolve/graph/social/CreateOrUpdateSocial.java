/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialNameSpaceFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.graph.social.factory.SocialWorkSheetFactory;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CreateOrUpdateSocial
{
    public static RSComponent insertOrUpdateRSComponent(RSComponent rscomponent)
    {
//        String type = rscomponent.getType();
        try
        {
            if (rscomponent.getClass().equals(Process.class))
            {
                rscomponent = SocialProcessFactory.insertOrUpdateProcess((Process) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(Team.class))
            {
                rscomponent = SocialTeamFactory.insertOrUpdateProcess((Team) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(Forum.class))
            {
                rscomponent = SocialForumFactory.insertOrUpdateForum((Forum) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(User.class))
            {
                rscomponent = SocialUsersFactory.insertOrUpdateProcess((User) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(ActionTask.class))
            {
                rscomponent = SocialActionTaskFactory.insertOrUpdateProcess((ActionTask) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(Worksheet.class))
            {
                rscomponent = SocialWorkSheetFactory.insertOrUpdateWorksheet((Worksheet) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(Document.class))
            {
                rscomponent = SocialDocumentFactory.insertOrUpdateDocument((Document) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(Rss.class))
            {
                rscomponent = SocialRssFactory.insertOrUpdateProcess((Rss) rscomponent, "system");
            }
            else if (rscomponent.getClass().equals(Namespace.class))
            {
                rscomponent = SocialNameSpaceFactory.insertOrUpdateNamespace((Namespace) rscomponent, "system");
            }
            
            verifyAccessRightsFor(rscomponent);
        }
        catch (Exception exp)
        {
            Log.log.error("insertOrUpdateRSComponent catched exception: " + exp, exp);
        }

        return rscomponent;

    }
    
    //verify if the users who are following this comp have appropriate access rights to follow it.
    //if not, then remove the reationship between them
    private static void verifyAccessRightsFor(RSComponent comp)
    {
        //get list of users that have direct or indirect relationship with this comp
        //verify for each user if he has right to follow this comp or not
        //remove the rel if it does not have any rights
//        String type = comp.getType();
//        
//        //ignore the validation for User, Namespace, Worksheet
//        if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()) 
//            || type.equalsIgnoreCase(SocialRelationshipTypes.NAMESPACE.name())
//            || type.equalsIgnoreCase(SocialRelationshipTypes.WORKSHEET.name()))
//        {
//            //ignore
//        }
//        else
//        {
//            String compRoles = SocialCompConversionUtil.convertSocialRolesStringToRoles(comp.getEditRoles() + " " + comp.getRoles() + " " + comp.getPostRoles());
//            Node compNode = ResolveGraphFactory.findNodeByIndexedID(comp.getSys_id());
//            Iterable<Relationship> relationships = compNode.getRelationships(RelationType.MEMBER, RelationType.FOLLOWER);
//            for(Relationship rel : relationships)
//            {
//                Node userNode = rel.getOtherNode(compNode);
//                String typeLocal = (String)userNode.getProperty(SocialFactory.NODE_TYPE);
//                if(typeLocal.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//                {
//                    //check if the user has the ROLES property. It will not be there in the 3.4 and will throw error other wise
//                    if(userNode.hasProperty(RSComponent.ROLES))
//                    {
//                        String userRolesStr = SocialCompConversionUtil.convertSocialRolesStringToRoles((String) userNode.getProperty(RSComponent.ROLES));
//                        Set<String> userRoles = new HashSet<String>(StringUtils.convertStringToList(userRolesStr, ","));
//                        String username = (String) userNode.getProperty(User.USERNAME);
//                        
//                        boolean hasRole = UserUtils.hasRole(username, userRoles, compRoles);
//                        if(!hasRole)
//                        {
//                            //remove the relationship
//                            try
//                            {
//                                ResolveGraphFactory.deleteRelation(rel);
//                            }
//                            catch (Exception e)
//                            {
//                                Log.log.error("error deleting the relation while verifying the access rights of node: " + comp.getDisplayName(), e);
//                            }
//                        }//end of if
//                    }
//                }//end of if
//            }//end of for loop
            
            
//            Iterable<Node> userNodes = TraversalUtil.findUsersDirectOrIndirectMembersOrFollowerOf(compNode);
//            for(Node userNode : userNodes)
//            {
//                String userRolesStr = SocialCompConversionUtil.convertSocialRolesStringToRoles((String) userNode.getProperty(RSComponent.ROLES));
//                Set<String> userRoles = new HashSet<String>(StringUtils.convertStringToList(userRolesStr, ","));
//                String username = (String) userNode.getProperty(User.USERNAME);
//                
//                boolean hasRole = UserUtils.hasRole(username, userRoles, compRoles);
//                if(!hasRole)
//                {
//                    //remove the relationship
//                }
//                
//            }
            
        }
        
//    }
}
