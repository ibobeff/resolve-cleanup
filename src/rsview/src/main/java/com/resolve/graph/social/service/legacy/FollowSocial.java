/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.graph.social.factory.SocialWorkSheetFactory;
import com.resolve.graph.social.util.LookupComponent;
import com.resolve.graph.social.util.legacy.SocialUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSContainer;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.message.SystemNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class FollowSocial
{
    public static SocialUsersFactory socialUsersFactory = new SocialUsersFactory();
    public static SocialForumFactory socialForumFactory = new SocialForumFactory();
    public static SocialTeamFactory socialTeamFactory = new SocialTeamFactory();
    public static SocialProcessFactory socialProcessFactory = new SocialProcessFactory();
    public static SocialActionTaskFactory socialActionTaskFactory = new SocialActionTaskFactory();
    public static SocialWorkSheetFactory socialWorkSheetFactory = new SocialWorkSheetFactory();
    public static SocialDocumentFactory socialDocumentFactory = new SocialDocumentFactory();
    public static SocialRssFactory socialRssFactory = new SocialRssFactory();
    
    
    //follow/add relationship
    public static void followUser(User user, User targetUser) throws Exception
    {
        throw new RuntimeException("BD: Let's find out who called this, this should move to ServiceSocial");
        /*
        Node userNode = null;
        Node targetUserNode = null;
        
        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
        {
            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
            
            if(targetUser != null && StringUtils.isNotEmpty(targetUser.getSys_id()))
            {
                targetUserNode = ResolveGraphFactory.findNodeByIndexedID(targetUser.getSys_id());
                
                if(userNode != null && targetUserNode != null)
                {
                    ResolveGraphFactory.addRelation(userNode, targetUserNode, RelationType.FOLLOWER);
                    
                    String content = user.getUsername() + " is now following you ";
                    SystemNotification notif = new SystemNotification(UserGlobalNotificationContainerType.USER_FOLLOW_ME);
                    
                    notif.setDisplayName(user.getUsername());
                    notif.setContent(content);
                    notif.setAuthor(user.getUsername());
                    notif.addComments(new ArrayList());
                    notif.setSys_created_on(new Date().getTime());
                    notif.setSys_updated_on(new Date().getTime());
                    notif.setSys_created_by("admin");
                    notif.setSys_updated_by("admin");
                    notif.setRoles("admin");
                    
                    PostSocial.notifyUser(user, targetUser, notif);
                }
            }
        }
        */        
    }
    
    
    public static void setFollowStreams(User user, List<String> ids, boolean follow) throws Exception
    {
        Node userNode = null;
        Node followNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(userNode != null && ids != null)
//            {
//                for(String followId: ids)
//                {
//                    followNode = ResolveGraphFactory.findNodeByIndexedID(followId);
//                    RSComponent comp = LookupComponent.getRSComponentById(followId);
//                    
//                    if(followNode != null)
//                    {
//                        if(follow)
//                        {
//                            if(comp instanceof RSContainer)
//                            {
//                                ResolveGraphFactory.addRelation(userNode, followNode, RelationType.MEMBER);
//                            }
//                            else
//                            {
//                                ResolveGraphFactory.addRelation(userNode, followNode, RelationType.FOLLOWER);
//                            }
//                        }
//                        else
//                        {
//                            //TODO: delete all the relationships between the user and comp - if there is no INDIRECT relationship present
//                            if(comp instanceof RSContainer)
//                            {
//                                ResolveGraphFactory.deleteRelation(userNode, followNode, RelationType.MEMBER);
//                            }
//                            else
//                            {
//                                ResolveGraphFactory.deleteRelation(userNode, followNode, RelationType.FOLLOWER);
//                            }
//                        }
//                    }
//                }
//            }
//        }
        
    }
    
    public static void followActionTask(User user, ActionTask actiontask) throws Exception
    {
        Node userNode = null;
        Node actiontaskNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode =  ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(actiontask != null && StringUtils.isNotEmpty(actiontask.getSys_id()))
//            {
//                actiontaskNode =  ResolveGraphFactory.findNodeByIndexedID(actiontask.getSys_id());
//                
//                if(userNode != null && actiontaskNode != null)
//                {
//                    ResolveGraphFactory.addRelation(userNode, actiontaskNode, RelationType.FOLLOWER);
//                }
//            }
//        }        
    }
    
    public static void followWorkSheet(User user, Worksheet worksheet) throws Exception
    {
        Node userNode = null;
        Node worksheetNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode =  ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(worksheet != null && StringUtils.isNotEmpty(worksheet.getSys_id()))
//            {
//                worksheetNode = ResolveGraphFactory.findNodeByIndexedID(worksheet.getSys_id());
//                
//                if(userNode != null && worksheetNode != null)
//                {
//                    ResolveGraphFactory.addRelation(userNode, worksheetNode, RelationType.FOLLOWER);
//                }
//            }
//        }        
    }

    
    public static void followDocument(User user, Document document) throws Exception
    {
        Node userNode = null;
        Node documentNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(document != null && StringUtils.isNotEmpty(document.getSys_id()))
//            {
//                documentNode = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
//                
//                if(userNode != null && documentNode != null)
//                {
//                    ResolveGraphFactory.addRelation(userNode, documentNode, RelationType.FOLLOWER);
//                }
//            }
//        }        
    }
    
//    public static void followNameSpace(User user, Namespace namespace) throws Exception
//    {
//        Node userNode = null;
//        Node namespaceNode = null;
//        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = socialUsersFactory.findNode(user.getSys_id());
//            
//            if(namespace != null && StringUtils.isNotEmpty(namespace.getName()))
//            {
//                namespaceNode = SocialNameSpaceFactory.findNodeByIndexedNamespaceName(namespace.getName());
//                
//                if(userNode != null && namespaceNode != null)
//                {
//                    SocialUsersFactory.addRelation(userNode, namespaceNode, RelationType.FOLLOWER);
//                    Log.log.info("User " + user.getDisplayName() + " follow namespace: " + namespace.getName());
//                }
//            }
//        }        
//    }
    
    
    public static void followForum(User user, Forum forum) throws Exception
    {
        Node userNode = null;
        Node forumNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(forum != null && StringUtils.isNotEmpty(forum.getSys_id()))
//            {
//                forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
//                
//                if(userNode != null && forumNode != null)
//                {
//                    ResolveGraphFactory.addRelation(userNode, forumNode, RelationType.FOLLOWER);
//                }
//            }
//        }        
    }
    
//    public void followRss(User user, Rss rss);//same as subscribe
    //public void followForum(User user, Forum forum);
    //public void followTeam(User user, Team team);
    //public void followProject(User user, Project project);3. add
    
    //unfollow/remove relationships 
    public static void unfollowUser(User user, User targetUser) throws Exception
    {
        Node userNode = null;
        Node targetUserNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(targetUser != null && StringUtils.isNotEmpty(targetUser.getSys_id()))
//            {
//                targetUserNode = ResolveGraphFactory.findNodeByIndexedID(targetUser.getSys_id());
//                
//                if(userNode != null && targetUserNode != null)
//                {
//                    ResolveGraphFactory.deleteRelation(userNode, targetUserNode, RelationType.FOLLOWER);
//                    UpdateSocial.removeFromThisUserHome(user, targetUser);
//                }
//            }
//        }       
    }
    
    public static void unfollowActionTask(User user, ActionTask actiontask) throws Exception
    {
        Node userNode = null;
        Node actiontaskNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode =  ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(actiontask != null && StringUtils.isNotEmpty(actiontask.getSys_id()))
//            {
//                actiontaskNode =  ResolveGraphFactory.findNodeByIndexedID(actiontask.getSys_id());
//                
//                if(userNode != null && actiontaskNode != null)
//                {
//                    ResolveGraphFactory.deleteRelation(userNode, actiontaskNode, RelationType.FOLLOWER);
//                    UpdateSocial.removeFromThisUserHome(user, actiontask);
//                }
//            }
//        }       
    }
    
    public static void unfollowWorkSheet(User user, Worksheet worksheet) throws Exception
    {
        Node userNode = null;
        Node worksheetNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode =  ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(worksheet != null && StringUtils.isNotEmpty(worksheet.getSys_id()))
//            {
//                worksheetNode = ResolveGraphFactory.findNodeByIndexedID(worksheet.getSys_id());
//                
//                if(userNode != null && worksheetNode != null)
//                {
//                    ResolveGraphFactory.deleteRelation(userNode, worksheetNode, RelationType.FOLLOWER);
//                    UpdateSocial.removeFromThisUserHome(user, worksheet);
//                }
//            }
//        }       
    }

    
    public static void unfollowDocument(User user, Document document) throws Exception
    {
        Node userNode = null;
        Node documentNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(document != null && StringUtils.isNotEmpty(document.getSys_id()))
//            {
//                documentNode = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
//                
//                if(userNode != null && documentNode != null)
//                {
//                    ResolveGraphFactory.deleteRelation(userNode, documentNode, RelationType.FOLLOWER);
//                    UpdateSocial.removeFromThisUserHome(user, document);                 
//                }
//            }
//        }       
    }
    
//    public static void unfollowNameSpace(User user, Namespace namespace) throws Exception
//    {
//        Node userNode = null;
//        Node namespaceNode = null;
//        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = socialUsersFactory.findNode(user.getSys_id());
//            
//            if(namespace != null && StringUtils.isNotEmpty(namespace.getName()))
//            {
//                namespaceNode = SocialNameSpaceFactory.findNodeByIndexedNamespaceName(namespace.getName());
//                
//                if(userNode != null && namespaceNode != null)
//                {
//                    SocialUsersFactory.deleteRelation(userNode, namespaceNode, RelationType.FOLLOWER);                 
//                }
//            }
//        }       
//    }
    
    public static void unfollowForum(User user, Forum forum) throws Exception
    {
        Node userNode = null;
        Node forumNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(forum != null && StringUtils.isNotEmpty(forum.getSys_id()))
//            {
//                forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
//                
//                if(userNode != null && forumNode != null)
//                {
//                    ResolveGraphFactory.deleteRelation(userNode, forumNode, RelationType.FOLLOWER);
//                    //UpdateSocial.removeFromHome(user, forum);
//                }
//            }
//        }       
    }
    
    public static boolean isUserDirstFollowTheComponent(User user, RSComponent comp)
    {
        boolean result = false;
        
        if(comp != null)
        {
            String compClassName = comp.getClass().getName();
            
            if(compClassName.equals("com.resolve.services.graph.social.model.component.container.Forum") || 
                            compClassName.equals("com.resolve.services.graph.social.model.component.container.Process") ||
                                compClassName.equals("com.resolve.services.graph.social.model.component.container.Team") )
            {
                result = isUserDirectMemberOfTheComponent(user, comp);
            }
            else
            {
                result = isUserFollowTheComponent(user, comp);
            }
        }
        
        return result;
    }
    
    
    public static boolean isUserDirectMemberOfTheComponent(User user, RSComponent comp)
    {
        boolean isUserMember = false;
        
        Node userNode = null;
        Node rsComponentNode = null;
        
//        try
//        {
//            if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//                
//                if(userNode != null && comp != null && StringUtils.isNotEmpty(comp.getSys_id()))
//                {
//                    SocialFactory socialFactory = SocialUtil.getSocialFactory(comp.getClass().getName());
//                    
//                    if(socialFactory != null)
//                    {
//                        rsComponentNode = ResolveGraphFactory.findNodeByIndexedID(comp.getSys_id());
//                        
//                        Relationship relLocal = ResolveGraphFactory.findRelationForType(userNode, rsComponentNode, RelationType.MEMBER);
//                        
//                        if(relLocal != null)
//                        {
//                            isUserMember = true;
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("isUserDirectMemberOfTheComponent catch exception: " + t);
//        }
        
        return isUserMember;
    }
    
    public static boolean isUserFollowTheComponent(User user, RSComponent comp)
    {
        boolean isDirectFollow = false;
        
        Node userNode = null;
        Node rsComponentNode = null;
        
//        try
//        {
//            if(user !=null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//                
//                if(userNode != null && comp != null && StringUtils.isNotEmpty(comp.getSys_id()))
//                {
//                    SocialFactory socialFactory = SocialUtil.getSocialFactory(comp.getClass().getName());
//                    
//                    if(socialFactory != null)
//                    {
//                        rsComponentNode = ResolveGraphFactory.findNodeByIndexedID(comp.getSys_id());
//                        
//                        Relationship relLocal = ResolveGraphFactory.findRelationForType(userNode, rsComponentNode, RelationType.FOLLOWER);
//                        
//                        if(relLocal != null)
//                        {
//                            isDirectFollow = true;
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info("isUserDirstFollowTheComponent catch exception: " + t);
//        }
        
        return isDirectFollow;
    }
}
