/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.graph.social.service.legacy.UpdateSocial;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class SocialDocumentFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.DOCUMENT, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.DOCUMENT.name(), containerNode);
    }

    public static Document insertOrUpdateDocument(Document document, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                document = insert(document);
//            }
//            else
//            {
//                //update
//                document = (Document) updateComponent(document, docNode);
//            }

            //additional operations
//            UpdateSocial.removeRSWikiFromNamespace(document);
//            UpdateSocial.addRSComponentToNameSpace(document);
        }
        catch(Exception t)
        {
            Log.log.warn("create Document catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }

        return document;
    }
    
    public static void bulkInsertOrUpdateDocumentGraphNodes(Collection<Document> documents, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

            //start the transaction
            tx = GraphDBUtil.getGraphDBService().beginTx();
            
            for(Document document : documents)
            {
//                Node node = ResolveGraphFactory.findNodeByIndexedID(document.getSys_id());
//                if(node == null)
//                {
//                    //insert
//                    node = GraphDBUtil.getGraphDBService().createNode();
//                    document.convertObjectToNode(node);
//                    
//                    containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.DOCUMENT);
//                    
//                }
//                else
//                {
//                    //update
//                    String sys_idLocal = (String) node.getProperty(RSComponent.SYS_ID);
//                    String sys_created_byLocal = node.hasProperty(RSComponent.SYS_CREATED_BY) ? (String) node.getProperty(RSComponent.SYS_CREATED_BY) : (StringUtils.isNotEmpty(document.getSys_created_by()) ? document.getSys_created_by() : "admin");
//                    Long sys_created_on = node.hasProperty(RSComponent.SYS_CREATED_ON) ? (Long) node.getProperty(RSComponent.SYS_CREATED_ON) : document.getSys_created_on();
//
//                    document.convertObjectToNode(node);
//
//                    node.setProperty(RSComponent.SYS_ID, sys_idLocal);
//                    node.setProperty(RSComponent.SYS_CREATED_BY, sys_created_byLocal);
//                    node.setProperty(RSComponent.SYS_CREATED_ON, sys_created_on);
//                }
            }//end of for loop
            
            //successfully close the transaction
            tx.success();

            //additional operations - add relationship between namespace and wiki
            for(Document document : documents)
            {
//                UpdateSocial.addRSComponentToNameSpace(document);  
            }
        }
        catch(Exception t)
        {
            Log.log.warn("creating bulk Documents catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }
    

    public static Set<RSComponent> findAllDocuments()
    {
        Node containerNode = getContainerNode();
        return findAllRsComponentFor(containerNode, SocialRelationshipTypes.DOCUMENT);
    }
    
    public static void syncGraphToDB(Set<String> dbSysIds)
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.DOCUMENT.name());
        }
        
        if(dbSysIds == null)
        {
            //to avoid the null pointer
            dbSysIds = new HashSet<String>();
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.DOCUMENT);
            deleteNodes(dbSysIds, rels);
        }
    }

//    private static void removeDocumentFromContainer(Node node) throws Exception
//    {
//        getContainerNode();
//        deleteRelation(containerNodeLocal, node, SocialObjectType.DOCUMENT);
//    }
//
//    private static void addDocumentToContainer(Node node) throws Exception
//    {
//        getContainerNode();
//        addRelation(containerNodeLocal, node, SocialObjectType.DOCUMENT);
//    }

    private static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.DOCUMENT.name());
        }

        return containerNodeLocal;
    }

    private static Document insert(Document document) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

            //insert
            tx = GraphDBUtil.getGraphDBService().beginTx();

//            Node node = GraphDBUtil.getGraphDBService().createNode();
//            document.convertObjectToNode(node);
//            //                node.setProperty(NODE_TYPE, SocialObjectType.DOCUMENT.name());
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.DOCUMENT);
//
//            tx.success();
//
//            //update the object
//            document.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create Document catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return document;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }
}

