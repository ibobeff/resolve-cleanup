/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.util.StringUtils;

public class ImportCatalogGraph  extends ImportComponentGraph
{

    public ImportCatalogGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        String sourceSysId = rel.getSourceName();
        String targetSysId = rel.getTargetName();
        SocialRelationshipTypes type = rel.getSourceType();

        if(StringUtils.isNotEmpty(sourceSysId) && StringUtils.isNotBlank(targetSysId) && type != null)
        {
            Node sourceNode = ResolveGraphFactory.findNodeByIndexedID(sourceSysId);
            Node targetNode = ResolveGraphFactory.findNodeByIndexedID(targetSysId);
        
            if(sourceNode != null && targetNode != null)
            {
                ResolveGraphFactory.addRelation(sourceNode, targetNode, type);
            }
        }
    }

}
