/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.services.vo.ImpexDTO;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ImportGraph
{
    private String location = null;
    private String username = null;
    
    //collection of relationships that will be imported
    private List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
    
    public ImportGraph(String location, String username) throws Exception
    {
        if(StringUtils.isBlank(location) || StringUtils.isBlank(username))
        {
            throw new Exception("Location and username are both mandatory");
        }
        
        this.location = location;
        this.username = username;
        
    }
    
    public void immport() throws Exception
    {
        //read the file
        importFile();
        
        //start importing one by one
        importRelationships();
        
    }
    
    private void importRelationships()  throws Exception
    {
        for(GraphRelationshipDTO rel : relationships)
        {
            try
            {
                boolean valid = rel.validate();
                if(valid)
                {
                    switch(rel.getSourceType())
                    {
                        case ACTIONTASK:
                            new ImportActiontaskGraph(rel, username).immport();
                            break;
                            
                        case DOCUMENT:
                            new ImportDocumentGraph(rel, username).immport();
                            break;
                            
//                        case NAMESPACE:
//                            break;
                           
                        case FORUM:
                            new ImportForumGraph(rel, username).immport();
                            break;
                            
//                        case RSS:
//                            break;
                        
                        case TEAM :
                            new ImportTeamGraph(rel, username).immport();
                            break;
                            
                        case PROCESS :
                            new ImportProcessGraph(rel, username).immport();
                            break;
                            
                        case CHILD_OF_CATALOG://this is for Catalog-Ref 
                            new ImportCatalogGraph(rel, username).immport();
                            break;
                            
                        default:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Could not import relationship. " + e.getMessage());
            }
        }
        
    }
    
    
    private void importFile()  throws Exception
    {
        File installFolder = FileUtils.getFile(location);
        if(!installFolder.isDirectory())
        {
            throw new Exception("Location " + location + " is not a valid folder");
        }
        
        //only json files to be read
        FilenameFilter filter = new FilenameFilter()
        {
            @Override
            public boolean accept(File dir, String name)
            {
                if (StringUtils.isNotBlank(name) && name.endsWith(".json"))
                    return true;
                else
                    return false;
            }
        };
        
        //get all the files 
        File[] installfiles = installFolder.listFiles(filter);
        for(File installF: installfiles)
        {
            if (installF.getName().startsWith("graph_relation"))
            {
                if(installF.isFile())
                {
                    String json = FileUtils.readFileToString(installF, ImpexUtil.UTF8);
                    ImpexDTO vo = new ObjectMapper().readValue(json, ImpexDTO.class);
                    relationships.addAll(vo.getRelationships());
                }
            }
        }
        
        //test
//        for(GraphRelationshipDTO dto : relationships)
//        {
//            ResolveCatalog cat1 = null;
//            if(dto.getTargetObject() != null)
//            {
//                cat1 = (ResolveCatalog) dto.getTargetObject();
//            }
//            SocialRelationshipTypes type = dto.getTargetType();
//            
//        }
    }

}
