/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.migration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jms.IllegalStateException;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.service.legacy.SocialGlobalNotificationFactory;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class GraphMigration
{
    private final GraphDatabaseService graphDb;
    private final ExecutionEngine engine;

    public GraphMigration(final GraphDatabaseService graphDb) throws IllegalStateException
    {
        if (graphDb == null)
        {
            throw new IllegalStateException("Graph DB must be in running state.");
        }
        this.graphDb = graphDb;
        this.engine = new ExecutionEngine(graphDb);
    }

    public void migrate()
    {
        refreshNodes();
        refreshRelationships();
    }
    
    /**
     * This method will delete all POST nodes from GraphDB
     */
    public void deleteAllPostNodes()
    {
        String nodeQuery = "start n = node(*) return n";
        ExecutionResult result = this.engine.execute(nodeQuery);
        
        Log.log.debug("Start POST node deletion.");
        for (Map<String, Object> row : result)
        {
            Node node = (Node) row.get("n");
            
//            if(node != null && node.hasProperty(RSComponent.TYPE))
//            {
//                if (((String)node.getProperty(RSComponent.TYPE)).equalsIgnoreCase(SocialRelationshipTypes.POST.name()))
//                {
//                    try
//                    {
//                        ResolveGraphFactory.deleteNode(node);
//                    }
//                    catch (Exception e)
//                    {
//                        Log.log.error("Error while deleting POST node. Message: " + e.getMessage());
//                    }
//                }
//            }
        }
        Log.log.debug("All POST nodes deleted.");
    }
    
    /**
     * This method will delete all FOVARITE relationships from GraphDB
     */    
    public void deleteAllFavoriteRel()
    {
        String relQuery = "start r = relationship(*) return r";
        
        ExecutionResult result = engine.execute(relQuery);
        
        for (Map<String, Object> row : result)
        {
            try
            {
                Relationship rel = (Relationship) row.get("r");

                if (rel.getType().equals(RelationType.FAVORITE))
                {
                    ResolveGraphFactory.deleteRelation(rel);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while deleting FAVORITE relationship. Message: " + e.getMessage());
            }
        }
    }

    /**
     * This method simply reads every single nodes in the Graph DB and updates them 
     * so the indexes are refreshed.
     */
    private void refreshNodes()
    {
        Log.log.debug("*********node migration start**************");
        //String nodeQuery = "start n = node(*) where not(has(n._copied)) and not(has(n._tmpId)) return n";
        String nodeQuery = "start n = node(*) return n";

        int count = 0;
        ExecutionResult result = this.engine.execute(nodeQuery);

        for (Map<String, Object> row : result)
        {
            Transaction tx = this.graphDb.beginTx();
            try
            {
                Node node = (Node) row.get("n");
                Log.log.debug("updating node: " + node.getId());
                
                //special case to fix data issue in old graphdb
                String username = getUsername(node);
                for (String key : node.getPropertyKeys())
                {
                    if("username".equalsIgnoreCase(key))
                    {
                        Log.log.debug("Key: " + key + ", value: " + username);
                        node.setProperty(key, username);
                    }
                    else
                    {
                        Log.log.debug("Key: " + key + ", value: " + node.getProperty(key));
                        node.setProperty(key, node.getProperty(key));    
                    }
                }
                tx.success();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
            finally
            {
                tx.finish();
            }
            count++;
        }
        Log.log.debug(count + " nodes found.");
    }

    /**
     * There are data problem in old graphdb, hence this check
     * during migration so we get the username from DB.
     * 
     * @param node
     * @return
     */
    private String getUsername(Node node)
    {
        String result = null;
        
        if(node.hasProperty("NODE_TYPE"))
        {
            String nodeType = (String) node.getProperty("NODE_TYPE");
            if("USER".equalsIgnoreCase(nodeType))
            {
                if(node.hasProperty("username"))
                {
                    result = (String) node.getProperty("username");
                    if(StringUtils.isBlank(result))
                    {
                        String sysId = (String) node.getProperty("sys_id");
                        if(StringUtils.isNotBlank(sysId))
                        {
                            //get from database
                            UsersVO user = UserUtils.findUserById(sysId);
                            if(user != null)
                            {
                                result = user.getUUserName();
                                Log.log.debug("Fix blank username by getting from DB: " + result);
                            }
                        }
                    }
                }
                else
                {
                    Log.log.debug("Completely bogus user node in GraphDB, ignore!");
                }
            }
        }
        return result;
    }

    /**
     * This method simply reads every single relationships in the Graph DB and updates them 
     * so the indexes are refreshed.
     */
    private void refreshRelationships()
    {
        Log.log.debug("************rel migration start**********");
        //String relQuery = "start r = relationship(*) where not(has(r._copied)) return r";
        String relQuery = "start r = relationship(*) return r";
        int count = 0;
        ExecutionResult result = this.engine.execute(relQuery);
        for (Map<String, Object> row : result)
        {
            Transaction tx = this.graphDb.beginTx();
            try
            {
                Relationship rel = (Relationship) row.get("r");
                
                //just a flag to indicate this relationship is migrated
                //helps with the transaction to kick off indexing.
                rel.setProperty("migrated", "true");
                Log.log.debug("Updating relation:" + rel.getId() + " " + rel.getType());
                for (String key : rel.getPropertyKeys())
                {
                    rel.setProperty(key, rel.getProperty(key));
                }
                tx.success();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
            finally
            {
                tx.finish();
            }
            count++;
        }
        Log.log.debug(count + " relationship found.");
    }
    
    /**
     * This method will migrate DIGEST and FORWARD global notification settings.
     * E.g, if ACTIONTASK_DIGEST_EMAIL global notification is set to true, all FOLLOWED
     * Actiontasks should set ACTIONTASK_DIGEST_EMAIL notification to true, provided is not
     * set at component level to false.
     */
    public void migrateGlobalEmailNotifications()
    {
        //String nodeQuery = "start n = node(*) return n";
        //This will generate a cypher query like: start n = node(*) WHERE HAS (n.NODE_TYPE) AND n.NODE_TYPE=~"(?i)USER" return n
        //(?i) is for case insensitivity
        String nodeQuery = "";//"start n = node(*) WHERE HAS (n." + RSComponent.TYPE + ") AND n." + RSComponent.TYPE +"=~ \"(?i)"+ SocialRelationshipTypes.USER.name() +"\" return n";

        ExecutionResult result = engine.execute(nodeQuery);
        
        List<UserGlobalNotificationContainerType> globalNotificationList = new ArrayList<UserGlobalNotificationContainerType>();
        globalNotificationList.add(UserGlobalNotificationContainerType.DOCUMENT_DIGEST_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.DOCUMENT_FORWARD_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.ACTIONTASK_DIGEST_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.ACTIONTASK_FORWARD_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.FORUM_DIGEST_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.FORUM_FORWARD_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.PROCESS_DIGEST_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.RSS_DIGEST_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.RSS_FORWARD_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.TEAM_DIGEST_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.TEAM_FORWARD_EMAIL);
//        globalNnotificationList.add(UserGlobalNotificationContainerType.RUNBOOK_DIGEST_EMAIL);
//        globalNnotificationList.add(UserGlobalNotificationContainerType.RUNBOOK_FORWARD_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.USER_DIGEST_EMAIL);
        globalNotificationList.add(UserGlobalNotificationContainerType.USER_FORWARD_EMAIL);
        
        for (Map<String, Object> row : result)
        {
            Transaction tx = graphDb.beginTx();
            try
            {
                Node node = (Node) row.get("n");
                
                if(node != null)
                {
//                    User user = SocialCompConversionUtil.getSocialUser((String)node.getProperty(RSComponent.USERNAME));
//                    for (UserGlobalNotificationContainerType globalNotificationType : globalNotificationList)
//                    {
//                        boolean global = getGlobalNotificationFlag(node, globalNotificationType);
//
//                        Iterable<Relationship> it = node.getRelationships(RelationType.FOLLOWER, RelationType.MEMBER);
//                        for (Relationship rel: it)
//                        {
//                            Node componentNode = rel.getOtherNode(node);
//                            String compRelType = globalNotificationType.name().split("_")[0];
//                            if (componentNode.getProperty(RSComponent.TYPE).equals(compRelType))
//                            {
////                                Log.log.debug(node.getProperty(RSComponent.USERNAME) + " " + compRelType + " " + componentNode.getProperty("displayName"));
////                                Relationship relLocal = ResolveGraphFactory.findRelationForType(node, componentNode, globalNotificationType);
////                                if (relLocal == null)
////                                {
////                                    if(global)
////                                    {
////                                        try
////                                        {
////                                            ServiceSocial.updateSpecificNotificationTypeFor(componentNode.getProperty("sys_id")+ "", user, globalNotificationType, true);
////                                        }
////                                        catch (Exception e)
////                                        {
////                                            Log.log.info("Issue with setting the notification of type: " + globalNotificationType.name() + ": " + e.getMessage());
////                                        }
////                                    }
////                                }
////                                    else
////                                    {
////                                        Log.log.debug((String)relLocal.getProperty("NOTIFICATIONVALUE"));
////                                    }
//                            }
//                        }
//                    }
                }
                
                tx.success();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
            finally
            {
                tx.finish();
            }
        }
    }
    
    private boolean getGlobalNotificationFlag(Node userNode, UserGlobalNotificationContainerType type)
    {
        boolean flag = false;
        
        Node notificationTypeNode = SocialGlobalNotificationFactory.getNotificationConfigTypeNode(type);
        if (notificationTypeNode != null)
        {
//            Relationship relLocal = ResolveGraphFactory.findRelationForType(notificationTypeNode, userNode, type);
//            if (relLocal != null)
//            {
//                String value = (String) relLocal.getProperty("NOTIFICATIONVALUE");
//                if (StringUtils.isNotBlank(value) && value.equalsIgnoreCase("true"))
//                {
//                    flag = true;
//                }
//            }
        }
        
        return flag;
    }
}
