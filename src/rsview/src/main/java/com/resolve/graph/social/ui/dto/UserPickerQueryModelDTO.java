/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.ui.dto;

public class UserPickerQueryModelDTO
{
    private String groups;
    
    private String usersOfTeams;
    private Boolean recurseUsersOfTeam;
    
    private String teamsOfTeams;
    private Boolean recurseTeamsOfTeam;
    
    public UserPickerQueryModelDTO() {}

    public String getGroups()
    {
        return groups;
    }

    public void setGroups(String groups)
    {
        this.groups = groups;
    }

    public String getUsersOfTeams()
    {
        return usersOfTeams;
    }

    public void setUsersOfTeams(String usersOfTeams)
    {
        this.usersOfTeams = usersOfTeams;
    }

    public Boolean getRecurseUsersOfTeam()
    {
        return recurseUsersOfTeam;
    }

    public void setRecurseUsersOfTeam(Boolean recurseUsersOfTeam)
    {
        this.recurseUsersOfTeam = recurseUsersOfTeam;
    }

    public String getTeamsOfTeams()
    {
        return teamsOfTeams;
    }

    public void setTeamsOfTeams(String teamsOfTeams)
    {
        this.teamsOfTeams = teamsOfTeams;
    }

    public Boolean getRecurseTeamsOfTeam()
    {
        return recurseTeamsOfTeam;
    }

    public void setRecurseTeamsOfTeam(Boolean recurseTeamsOfTeam)
    {
        this.recurseTeamsOfTeam = recurseTeamsOfTeam;
    }
}
