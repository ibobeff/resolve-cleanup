/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;

public class ImportProcessGraph extends ImportComponentGraph
{
    private Process process = null;
    
    public ImportProcessGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }
    
    @Override
    public void immport() throws Exception
    {
        SocialProcessDTO processDTO = ServiceHibernate.getProcessByName(rel.getSourceName(), username);
        if(processDTO != null)
        {
            Node processNode = ResolveGraphFactory.findNodeByIndexedID(processDTO.getSys_id());
            if(processNode != null)
            {
                process = new Process();
//                process.convertNodeToObject(processNode);

                switch(rel.getTargetType())
                {
                    case USER: 
                        addUserToProcess();
                        break;
                        
                    case ACTIONTASK:
                        addActiontaskToProcess();
                        break;
                        
                    case DOCUMENT:
                        addDocumentToProcess();
                        break;
                        
                    case NAMESPACE:
                        addNamespaceToProcess();
                        break;
                       
                    case FORUM:
                        addForumToProcess();
                        break;
                        
                    case RSS:
                        subscribeRssToProcess();
                        break;
                    
                    case TEAM :
                        addTeamToProcess();
                        break;
                        
                    default:
                        break;
                }
            }
            else
            {
                throw new Exception("Process " + processDTO.getU_display_name() + " is not sync with graph db." );
            } 
        }//end of if
        
    }
    
    private void addUserToProcess() throws Exception
    {
        String usernameTobeAdded = rel.getTargetName();
        User user = SocialCompConversionUtil.getSocialUser(usernameTobeAdded);
        if(user != null)
        {
//            Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            if(userNode != null)
//            {
//                //add user to this process
//                ServiceSocial.addUserToProcess(user, process);
//            }
//            else
//            {
//                throw new Exception("User " + user.getUsername() + " is not sync with graph db." );
//            } 
        }
    }
    
    private void addActiontaskToProcess() throws Exception
    {
        String actiontaskName = rel.getTargetName();
        ActionTask at = SocialCompConversionUtil.createActionTaskByName(actiontaskName);
        if(at != null)
        {
//            Node userNode = ResolveGraphFactory.findNodeByIndexedID(at.getSys_id());
//            if(userNode != null)
//            {
//                //add actiontask to this process
//                ServiceSocial.addActionTaskToProcess(at, process);
//            }
//            else
//            {
//                throw new Exception("Actiontask " + at.getDisplayName() + " is not sync with graph db." );
//            } 
        }
    }
    
    private void addDocumentToProcess() throws Exception
    {
        String documentName = rel.getTargetName();
        Document doc = SocialCompConversionUtil.createDocumentByName(documentName);
        if(doc != null)
        {
//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(doc.getSys_id());
//            if(docNode != null)
//            {
//                //add doc to this process
//                ServiceSocial.addDocumentToProcess(doc, process);
//            }
//            else
//            {
//                throw new Exception("Actiontask " + doc.getDisplayName() + " is not sync with graph db." );
//            } 
        }
    }
    
    private void addNamespaceToProcess() throws Exception
    {
        //TODO
    }
    
    private void addTeamToProcess()  throws Exception
    {
        String teamName = rel.getTargetName();
        SocialTeamDTO team = ServiceHibernate.getTeamByName(teamName, username);
        if(team != null)
        {
            Node teamNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
//            if(teamNode != null)
//            {
//                Team teamGrpahModel = new Team();
//                teamGrpahModel.convertNodeToObject(teamNode);
//                
//                //add team to this process
//                ServiceSocial.addTeamToProcess(teamGrpahModel, process);
//            }
//            else
//            {
//                throw new Exception("Team " + team.getU_display_name() + " is not sync with graph db." );
//            } 
        }
    }
    
    private void addForumToProcess()  throws Exception
    {
        String forumName = rel.getTargetName();
        SocialForumDTO forum = ServiceHibernate.getForumByName(forumName, username);
        if(forum != null)
        {
            Node forumNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
//            if(forumNode != null)
//            {
//                Forum forumGrpahModel = new Forum();
//                forumGrpahModel.convertNodeToObject(forumNode);
//                
//                //add forum to this process
//                ServiceSocial.addForumToProcess(forumGrpahModel, process);
//            }
//            else
//            {
//                throw new Exception("Forum " + forum.getU_display_name() + " is not sync with graph db." );
//            } 
        }
    }
    
    private void subscribeRssToProcess()  throws Exception
    {
        String rssName = rel.getTargetName();
        SocialRssDTO rss = ServiceHibernate.getRssByName(rssName, username);
        if(rss != null)
        {
            Node rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
//            if(rssNode != null)
//            {
//                Rss rssGrpahModel = new Rss();
//                rssGrpahModel.convertNodeToObject(rssNode);
//                
//                //add forum to this process
//                ServiceSocial.subscribeRssToProcess(rssGrpahModel, process);
//            }
//            else
//            {
//                throw new Exception("Forum " + rss.getU_display_name() + " is not sync with graph db." );
//            } 
        }
    }
    
    

}
