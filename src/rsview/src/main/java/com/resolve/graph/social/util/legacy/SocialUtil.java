/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util.legacy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.dto.SocialComponentType;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.graph.social.factory.SocialWorkSheetFactory;
import com.resolve.rsview.main.MSocial;
import com.resolve.rsview.main.Main;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.SocialUIPreferences;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.message.Post;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialUtil
{
    private final static Map<String, SocialFactory> FACTORYMAP = new ConcurrentHashMap<String, SocialFactory>();
    private final static Map<String, RSComponent> TYPE_RSCOMPONENT = new ConcurrentHashMap<String, RSComponent>();
    private final static Map<String, String> TYPEMAP = new ConcurrentHashMap<String, String>();

    static
    {
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.container.Process", new SocialProcessFactory());
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.container.Team", new SocialTeamFactory());
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.container.Forum", new SocialForumFactory());
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.User", new SocialUsersFactory());
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.ActionTask", new SocialActionTaskFactory());
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.WorkSheet", new SocialWorkSheetFactory());
//        FACTORYMAP.put("com.resolve.services.graph.social.model.component.Runbook", new SocialRunbookFactory());
//        FACTORYMAP.put("com.resolve.services.graph.social.model.component.DecisionTree", new SocialDecisionTreeFactory());
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.Document", new SocialDocumentFactory());
        FACTORYMAP.put("com.resolve.services.graph.social.model.component.Rss", new SocialRssFactory());
    }

    static
    {
        TYPE_RSCOMPONENT.put("process", new Process());
        TYPE_RSCOMPONENT.put("team", new Team());
        TYPE_RSCOMPONENT.put("forum", new Forum());
        TYPE_RSCOMPONENT.put("user", new User());
        TYPE_RSCOMPONENT.put("actiontask", new ActionTask());
        TYPE_RSCOMPONENT.put("worksheet", new Worksheet());
        TYPE_RSCOMPONENT.put("runbook", new Runbook());
//        TYPE_RSCOMPONENT.put("decisiontree", new DecisionTree());
        TYPE_RSCOMPONENT.put("document", new Document());
        TYPE_RSCOMPONENT.put("rss", new Rss());
    }

    static
    {
        TYPEMAP.put("com.resolve.services.graph.social.model.component.container.Process", "Process");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.container.Team", "Team");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.container.Forum", "Forum");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.User", "User");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.ActionTask", "ActionTask");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.WorkSheet", "WorkSheet");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.Runbook", "Runbook");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.DecisionTree", "DecisionTree");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.Document", "Document");
        TYPEMAP.put("com.resolve.services.graph.social.model.component.Rss", "Rss");
        TYPEMAP.put("com.resolve.services.graph.resolvetag.model.ResolveTag", "ResolveTag");
        TYPEMAP.put("com.resolve.services.graph.catalog.model.ResolveCatalog", "ResolveCatalog");
    }

    public static SocialFactory getSocialFactory(String className)
    {
        return FACTORYMAP.get(className);
    }

    public static ResolveGraphNode getSocialNodeByType(String type)
    {
        ResolveGraphNode rgNode = null;

//        if(type.equalsIgnoreCase("resolvetag"))
//        {
//            rgNode = new ResolveTag();
//        }

        return rgNode;
    }

    public static RSComponent getSocialComponentByType(String type)
    {
        RSComponent rscomp = null;

        if(type.equalsIgnoreCase("process"))
        {
            rscomp = new Process();
        }
        else if(type.equalsIgnoreCase("team"))
        {
            rscomp = new Team();
        }
        else if(type.equalsIgnoreCase("forum"))
        {
            rscomp = new Forum();
        }
        else if(type.equalsIgnoreCase("user"))
        {
            rscomp = new User();
        }
        else if(type.equalsIgnoreCase("actiontask"))
        {
            rscomp = new ActionTask();
        }
        else if(type.equalsIgnoreCase("worksheet"))
        {
            rscomp = new Worksheet();
        }
        else if(type.equalsIgnoreCase("runbook"))
        {
            rscomp = new Runbook();
        }
        else if(type.equalsIgnoreCase("decisiontree"))
        {
//            rscomp = new DecisionTree();
        }
        else if(type.equalsIgnoreCase("document"))
        {
            rscomp = new Document();
        }
        else if(type.equalsIgnoreCase("rss"))
        {
            rscomp = new Rss();
        }

        return rscomp;
    }

    public static String getType(String className)
    {
        return TYPEMAP.get(className);
    }

    
    public static Process createProcess(String sysId)
    {
        return SocialCompConversionUtil.createProcess(sysId);

    }// createProcess

    public static Process createProcessByName(String processName)
    {
        return SocialCompConversionUtil.createProcessByName(processName);

    }// createProcess

    public static Team createTeam(String sysId)
    {
        return SocialCompConversionUtil.createTeam(sysId);

    }// createTeam

    public static Team createTeamByName(String name)
    {
        return SocialCompConversionUtil.createTeamByName(name);

    }// createTeam

    public static Forum createForumByName(String name)
    {
        return SocialCompConversionUtil.createForumByName(name);

    }// createForum

    public static Rss createRssByName(String name)
    {
        return SocialCompConversionUtil.createRssByName(name);

    }// createRss

    public static Document createDocument(String sysId)
    {
        return SocialCompConversionUtil.createDocument(sysId);
    }// createDocument

    public static ActionTask createActionTask(String sysId)
    {
        return SocialCompConversionUtil.createActionTask(sysId);
    }// createActionTask

    public static Rss createRss(String sysId)
    {
        return SocialCompConversionUtil.createRss(sysId);
    }// createRss

    public static Forum createForum(String sysId)
    {
        return SocialCompConversionUtil.createForum(sysId);

    }// createForum

    public static void insertOrUpdateCustomTable(String username, String tableName, Map<String, Object> data) throws Throwable
    {
        ServiceHibernate.insertOrUpdateCustomTable(username, tableName, data);
    }// insertOrUpdateCustomTable

    public static void deleteCustomTable(String username, String tableName, String sysId) throws Throwable
    {
        ServiceHibernate.deleteDBRow(username, tableName, sysId, true);
    }// insertOrUpdateCustomTable

    public static void deleteCustomTable(String username, String tableName, List<String> ids) throws Throwable
    {
        ServiceHibernate.deleteDBRow(username, tableName, ids, true);
    }// insertOrUpdateCustomTable

    public static List<Map<String, Object>> getAllData(String tableName)
    {
        return ServiceHibernate.getAllData(tableName);
    }

    @Deprecated
    public static List<Map<String, Object>> getResolveContainer(String tableName, String whereClause)
    {
        return ServiceHibernate.queryCustomTable(tableName, whereClause, null);
    }

    public static List<Map<String, Object>> getResolveContainer(String tableName, String whereClause, Map<String, Object> whereClauseParams)
    {
        return ServiceHibernate.queryCustomTable(tableName, whereClause, whereClauseParams);
    }
    
    public static List<Map<String, Object>> getAllData(String tableName, int limit, int offset)
    {
        return ServiceHibernate.getAllData(tableName, limit, offset);
    }

    public static Map<String, Object> getCustomTableRecord(String tableName, String sysId)
    {
       return ServiceHibernate.findById(tableName, sysId);
    }



    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Rss
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    public static List<Map<String, Object>> getAllFollowRss(String username)
//    {
//        List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        RssTree rssTree = ServiceSocial.getRssTree(user);
//        if (rssTree != null && rssTree.getRsss() != null && rssTree.getRsss().size() > 0)
//        {
//            for (Rss rss : rssTree.getRsss())
//            {
//                String sysId = rss.getSys_id();
//                if (StringUtils.isNotEmpty(sysId))
//                {
//                    Map<String, Object> rssEntry = getCustomTableRecord(Constants.RSS_SUBSCRIPTION, sysId);
//                    if (rssEntry != null)
//                    {
//                        results.add(rssEntry);
//                    }
//                }// end of if
//            }// end of for
//        }// end of if
//        return results;
//    }// getAllFollowRss

//    public static void addUserToRss(String userId, List<RssSubscriptionBaseModel> rss) throws Exception
//    {
////        UsersVO user = getUser(userId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userId);//convertUsersToSocialUser(user);
////        ServiceSocial.createUser(socialUser);
//        for (RssSubscriptionBaseModel rssItem : rss)
//        {
//            Rss socialRss = convertRssToSocialRss(rssItem);
////            ServiceSocial.createRss(socialRss);
//            ServiceSocial.subscribeRssToUser(socialUser, socialRss);
//        }
//
//    } // addUserToRss

    public static void subscribeRssToUser (User socialUser, Rss socialRss) throws Exception
    {
//        ServiceSocial.subscribeRssToUser(socialUser, socialRss);
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Users
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    public static List<User> getFollowUsers(String username)
//    {
//        List<User> results = new ArrayList<User>();
//
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        UserTree userTree = ServiceSocial.getUserTree(user);
//        if (userTree != null && userTree.getUsers() != null && userTree.getUsers().size() > 0)
//        {
//            results.addAll(userTree.getUsers());
//        }
//
//        return results;
//    }// getFollowUsers

//    private static User convertUsersToSocialUser(UserBaseModel user)
//    {
//        User socialUser = null;
//        if (user != null)
//        {
//            String displayName = StringUtils.isNotEmpty(user.getUFirstName()) && StringUtils.isNotEmpty(user.getULastname()) ? (user.getUFirstName() + " " + user.getULastname()) : user.getUUsername();
//
//            socialUser = new User();
//            socialUser.setId(user.getSys_Id());
//            socialUser.setSys_id(user.getSys_Id());
//            socialUser.setDisplayName(displayName);
//            socialUser.setUsername(user.getUUsername());
//        }
//
//        return socialUser;
//    }// convertUsersToSocialUser

//    public static UsersModel getUserProfile(String userName)
//    {
//        UsersModel userProfile = null;
//        UsersVO user = ServiceHibernate.getUser(userName);
//        userProfile = (new UserManagementService()).getUsersModelByID(user.getSys_id());
//        // userProfile = convertUserProfile(userProfile, user);
//        return userProfile;
//    }
//
//    public static List<UserRolesModel> getAvailableRoles(String selectedRoles)
//    {
//        List<UserRolesModel> availableRoles = new ArrayList<UserRolesModel>();
//        availableRoles = (new UserManagementService()).getUserRoles();
//        if (selectedRoles != null)
//        {
//            String rolesArray[] = selectedRoles.split(",");
//            for (String role : rolesArray)
//            {
//                for (UserRolesModel roleModel : availableRoles)
//                {
//                    if (roleModel.getName().equalsIgnoreCase(role.trim()))
//                    {
//                        availableRoles.remove(roleModel);
//                        break;
//                    }
//                }
//            }
//        }
//
//        return availableRoles;
//    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Runbooks
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    public static List<Runbook> getFollowRunbooks(String username)
//    {
//        List<Runbook> results = new ArrayList<Runbook>();
//
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        RunbookTree atTree = ServiceSocial.getRunbookTree(user, false);
//        if (atTree != null && atTree.getRunbooks() != null && atTree.getRunbooks().size() > 0)
//        {
//            results.addAll(atTree.getRunbooks());
//        }
//
//        return results;
//    }// getFollowRunbook

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Documents
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    public static List<Document> getFollowDocuments(String username)
//    {
//        List<Document> results = new ArrayList<Document>();
//
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        DocumentTree atTree = ServiceSocial.getDocumentTree(user, false);
//        if (atTree != null && atTree.getDocuments() != null && atTree.getDocuments().size() > 0)
//        {
//            results.addAll(atTree.getDocuments());
//        }
//
//        return results;
//    }// getFollowDocument
//
//    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    // ActionTask
//    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    public static List<ActionTask> getFollowActionTasks(String username)
//    {
//        List<ActionTask> results = new ArrayList<ActionTask>();
//
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        ActionTaskTree atTree = ServiceSocial.getActionTaskTree(user, false);
//        if (atTree != null && atTree.getActiontasks() != null && atTree.getActiontasks().size() > 0)
//        {
//            results.addAll(atTree.getActiontasks());
//        }
//
//        return results;
//    }// getFollowActionTasks

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Teams
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    public static Team convertTeamToSocialTeam(TeamBaseModel model)
//    {
//        Team socialTeam = null;
//
//        if (model != null)
//        {
//            socialTeam = createTeam(model.getSys_Id());
//        }
//
//        return socialTeam;
//    }

//    public static void addUsersToTeam(List<UserBaseModel> users, String teamSysId)
//    {
//        Team socialTeam = new Team();
//        Map<String, Object> rsTeamMap = null;
//        try
//        {
//            rsTeamMap = ServiceHibernate.findById(TeamBaseModel.TABLE_NAME, teamSysId);
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        if (rsTeamMap != null)
//        {
//            socialTeam.setId((String) rsTeamMap.get(TeamBaseModel.SYS_ID));
//            socialTeam.setSys_id((String) rsTeamMap.get(TeamBaseModel.SYS_ID));
//            socialTeam.setDisplayName((String) rsTeamMap.get(TeamBaseModel.U_DISPLAY_NAME));
//
//        }
//
//        for (UserBaseModel user : users)
//        {
//            User socialUser = convertUsersToSocialUser(user);
////            ServiceSocial.createTeam(socialTeam);
////            ServiceSocial.createUser(socialUser);
//            try
//            {
//                addUserToTeam(socialUser, socialTeam);
//            }
//            catch (Exception e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//    }

//    public static void addTeamsToTeam(List<ResolveBaseModel> teams, String teamSysId) throws Exception
//    {
//        Team socialTeam = new Team();
//        Map<String, Object> rsTeamMap = null;
//
//        try
//        {
//            rsTeamMap = ServiceHibernate.findById(TeamBaseModel.TABLE_NAME, teamSysId);
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        if (rsTeamMap != null)
//        {
//            socialTeam.setId((String) rsTeamMap.get(TeamBaseModel.SYS_ID));
//            socialTeam.setSys_id((String) rsTeamMap.get(TeamBaseModel.SYS_ID));
//            socialTeam.setDisplayName((String) rsTeamMap.get(TeamBaseModel.U_DISPLAY_NAME));
//        }
//
//        for (ResolveBaseModel team : teams)
//        {
//            Team sTeam = convertTeamToSocialTeam((TeamBaseModel) team);
////            ServiceSocial.createTeam(sTeam);
//            ServiceSocial.addTeamToTeam(sTeam, socialTeam);
//        }
//    }

//    public static Set<String> getParentsOfTeam(String teamsID)
//    {
//        return ServiceSocial.getParentsOfTeam(teamsID);
//    }

//    public static void addUserToTeams(String userId, List<TeamBaseModel> teams)
//    {
////        UsersVO user = getUser(userId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userId);//convertUsersToSocialUser(user);
////        ServiceSocial.createUser(socialUser);
//        for (TeamBaseModel team : teams)
//        {
//            Team socialTeam = convertTeamToSocialTeam(team);
////            ServiceSocial.createTeam(socialTeam);
//            try
//            {
//                addUserToTeam(socialUser, socialTeam);
//            }
//            catch (Exception e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//    }

//    public static void addUserToTeam(User socialUser, Team socialTeam) throws Exception
//    {
//        ServiceSocial.addUserToTeam(socialUser, socialTeam);
//    }

//    public static void removeUserFromTeams(String userId, List<TeamBaseModel> teams)  throws Exception
//    {
////        UsersVO user = getUser(userId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userId);//convertUsersToSocialUser(user);
//        for (TeamBaseModel team : teams)
//        {
//            Team socialTeam = convertTeamToSocialTeam(team);
//            removeUserFromTeam(socialUser, socialTeam);
//        }
//    }

//    public static void removeUsersFromTeam(List<UserBaseModel> users, String teamSysId)  throws Exception
//    {
//        Team socialTeam = new Team();
//        socialTeam.setId(teamSysId);
//        socialTeam.setSys_id(teamSysId);
//        // SocialStore.createTeam(socialTeam);
//
//        for (UserBaseModel user : users)
//        {
//            User socialUser = convertUsersToSocialUser(user);
//            // SocialStore.createUser(socialUser);
//            removeUserFromTeam(socialUser, socialTeam);
//        }
//    }
//
//    public static void removeUserFromTeam(User socialUser, Team socialTeam)  throws Exception
//    {
//        ServiceSocial.removeUserFromTeam(socialUser, socialTeam);
//    }
//
//    public static void removeTeamsFromTeam(List<ResolveBaseModel> teams, String teamSysId)  throws Exception
//    {
//        Team socialTeam = new Team();
//        socialTeam.setId(teamSysId);
//        socialTeam.setSys_id(teamSysId);
//        // SocialStore.createTeam(socialTeam);
//
//        for (ResolveBaseModel resolveBaseModel : teams)
//        {
//            Team sTeam = createTeam(((TeamBaseModel) resolveBaseModel).getSys_Id());
//            // SocialStore.createUser(socialUser);
//            ServiceSocial.removeTeamFromTeam(sTeam, socialTeam);
//        }
//    }

//    public static Team getTeam(String teamId, boolean children)
//    {
//        Team team = null;
//        team = ServiceGraph.getTeam(teamId, children);
//        return team;
//    }

//    public static void deteteTeam(TeamBaseModel team) throws Exception
//    {
//        Team socialTeam = convertTeamToSocialTeam(team);
//        ServiceSocial.deleteComponent(socialTeam.getSys_id(), "system");
//    }

//    public static List<BaseModel> getModuleList(String tableName)
//    {
//        List<? extends Object> data = null;
//        String sql = "SELECT DISTINCT u_module FROM " + tableName + " WHERE u_module IS NOT NULL ORDER BY u_module";
//        List<BaseModel> baseModelList = new ArrayList<BaseModel>();
//
//        try
//        {
//            data = ServiceHibernate.executeHQLSelect(sql);
//        }
//        catch (Throwable e)
//        {
//            Log.log.error("Error with sql :" + sql, e);
//        }
//
//        if (data != null)
//        {
//            for (Object o : data)
//            {
//                BaseModel btm = new BaseTreeModel();
//                btm.set("u_module", o);
//                baseModelList.add(btm);
//            }
//        }
//
//        return baseModelList;
//    }

    /*
     * Process Start
     */
//
//    public static void deleteProcess(ProcessBaseModel process) throws Exception
//    {
//        Process socialProcess = convertProcessToSocialProcess(process);
//        ServiceSocial.deleteComponent(socialProcess.getSys_id(), "system");
//    }
//
//    public static void addUserToProcesses(String userId, List<ProcessBaseModel> processes)
//    {
////        UsersVO user = getUser(userId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userId);//convertUsersToSocialUser(user);
////        ServiceSocial.createUser(socialUser);
//        for (ProcessBaseModel process : processes)
//        {
//            Process socialProcess = convertProcessToSocialProcess(process);
////            ServiceSocial.createProcess(socialProcess);
//            try
//            {
//                addUserToProcess(socialUser, socialProcess);
//            }
//            catch (Exception e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//    }

//    public static Process getProcess(String processId, boolean children)
//    {
//        Process process = null;
//        process = ServiceGraph.getProcess(processId, children);
//        return process;
//    }

//    public static void addTeamsToProcess(List<TeamBaseModel> teams, String processId) throws Exception
//    {
//        Process socialProcess = new Process();
//        Map<String, Object> rsTeamMap = null;
//
//        try
//        {
//            rsTeamMap = ServiceHibernate.findById(ProcessBaseModel.TABLE_NAME, processId);
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        if (rsTeamMap != null)
//        {
//            socialProcess.setId((String) rsTeamMap.get(ProcessBaseModel.SYS_ID));
//            socialProcess.setSys_id((String) rsTeamMap.get(ProcessBaseModel.SYS_ID));
//            socialProcess.setDisplayName((String) rsTeamMap.get(ProcessBaseModel.U_DISPLAY_NAME));
//        }
//
////        ServiceSocial.createProcess(socialProcess);
//
//        for (TeamBaseModel team : teams)
//        {
//            Team sTeam = convertTeamToSocialTeam(team);
////            ServiceSocial.createTeam(sTeam);
//            ServiceSocial.addTeamToProcess(sTeam, socialProcess);
//        }
//    }

//    public static void removeTeamsFromProcess(List<ResolveBaseModel> teams, String processId)  throws Exception
//    {
//        Process socialProcess = new Process();
//        socialProcess.setSys_id(processId);
//
//        for (ResolveBaseModel resolveBaseModel : teams)
//        {
//            Team socialTeam = createTeam(((TeamBaseModel) resolveBaseModel).getSys_Id());
//            ServiceSocial.removeTeamFromProcess(socialTeam, socialProcess);
//        }
//    }

//    public static void addUsersToProcess(List<UserBaseModel> users, String processId)
//    {
//        Process socialProcess = new Process();
//        Map<String, Object> rsTeamMap = null;
//
//        try
//        {
//            rsTeamMap = ServiceHibernate.findById(ProcessBaseModel.TABLE_NAME, processId);
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        if (rsTeamMap != null)
//        {
//            socialProcess.setId((String) rsTeamMap.get(TeamBaseModel.SYS_ID));
//            socialProcess.setSys_id((String) rsTeamMap.get(TeamBaseModel.SYS_ID));
//            socialProcess.setDisplayName((String) rsTeamMap.get(TeamBaseModel.U_DISPLAY_NAME));
//
//        }
//
//        for (UserBaseModel user : users)
//        {
//            User socialUser = convertUsersToSocialUser(user);
////            ServiceSocial.createProcess(socialProcess);
////            ServiceSocial.createUser(socialUser);
//            try
//            {
//                addUserToProcess(socialUser, socialProcess);
//            }
//            catch (Exception e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//    }

    public static void addUserToProcess(User socialUser, Process socialProcess) throws Exception
    {
//        ServiceSocial.addUserToProcess(socialUser, socialProcess);
    }

//    public static void removeUsersFromProcess(List<UserBaseModel> users, String processId)  throws Exception
//    {
//        Process socialProcess = new Process();
//        socialProcess.setId(processId);
//        socialProcess.setSys_id(processId);
//        // SocialStore.createTeam(socialTeam);
//
//        for (UserBaseModel user : users)
//        {
//            User socialUser = convertUsersToSocialUser(user);
//            // SocialStore.createUser(socialUser);
//            removeUserFromProcesses(socialUser, socialProcess);
//        }
//    }

//    public static void addDocumentsToProcess(List<DocumentInfoModel> docs, String processId, Boolean isRunbook, String username) throws Exception
//    {
//        Process socialProcess = createProcess(processId);
//
//        for (DocumentInfoModel doc : docs)
//        {
//                Document socialDoc = createDocument(doc.getSys_Id());// convertDocsToSocialDocs(doc);
////                ServiceSocial.createDocument(socialDoc);
//                ServiceSocial.addDocumentToProcess(socialDoc, socialProcess);
//                NotificationHelper.getDocumentNotification(socialDoc, socialProcess, UserGlobalNotificationContainerType.DOCUMENT_ADDED_TO_PROCESS, username, true, true);
//                NotificationHelper.getProcessNotification(socialProcess, socialDoc, UserGlobalNotificationContainerType.PROCESS_DOCUMENT_ADDED, username, true, true);
//        }
//    }
//
//    public static void removeDocumentsFromProcess(List<DocumentInfoModel> docs, String processId, Boolean isRunbook, String username)  throws Exception
//    {
//        Process socialProcess = createProcess(processId);
//
//        for (DocumentInfoModel documentBaseModel : docs)
//        {
//                Document socialDocument = createDocument(documentBaseModel.getSys_Id());
//                NotificationHelper.getDocumentNotification(socialDocument, socialProcess, UserGlobalNotificationContainerType.DOCUMENT_REMOVED_FROM_PROCESS, username, true, false);
//                NotificationHelper.getProcessNotification(socialProcess, socialDocument, UserGlobalNotificationContainerType.PROCESS_DOCUMENT_REMOVED, username, true, false);
//                ServiceSocial.removeDocumentFromProcess(socialDocument, socialProcess);
//        }
//    }

//    public static Process convertProcessToSocialProcess(ProcessBaseModel model)
//    {
//        Process socialProcess = null;
//
//        if (model != null)
//        {
//            socialProcess = new Process();
//            socialProcess.setId(model.getSys_Id());
//            socialProcess.setSys_id(model.getSys_Id());
//            socialProcess.setDisplayName(model.getUDisplayName());
//        }
//
//        return socialProcess;
//    }
//
//    public static void removeUserFromProcesses(String userId, List<ProcessBaseModel> processList)  throws Exception
//    {
////        UsersVO user = getUser(userId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userId);//convertUsersToSocialUser(user);
//        for (ProcessBaseModel process : processList)
//        {
//            Process socialProcess = convertProcessToSocialProcess(process);
//            removeUserFromProcesses(socialUser, socialProcess);
//        }
//    }

    public static void removeUserFromProcesses(User user, Process process)  throws Exception
    {
//        ServiceSocial.removeUserFromProcess(user, process);
    }

    /*
     * Process End
     */



//    public static Map<String, Object> getSocialTeamMap(String userName, Team team)
//    {
//        Map<String, Object> map = null;
//        String teamId = team.getSys_id();
//
//        try
//        {
//            map = ServiceHibernate.findById(TeamBaseModel.TABLE_NAME, teamId);
//            if (map == null)
//            {
//                Log.log.error("Team '" + team.getDisplayName() + "' with Sys_Id '" + teamId + "' is not present in Database.");
//            }
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        return map;
//    }

//    public static Map<String, Object> getSocialProcessMap(String userName, Process process)
//    {
//        Map<String, Object> map = null;
//        String processId = process.getSys_id();
//
//
//        try
//        {
//            map = ServiceHibernate.findById(ProcessBaseModel.TABLE_NAME, processId);
//            if (map == null)
//            {
//                Log.log.error("Process '" + process.getDisplayName() + "' with Sys_Id '" + processId +"' is not present in Database.");
//            }
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//
//        return map;
//    }

//    /**
//     * based on the file extension it creates the html element
//     *
//     * jpg,png,gif - <img src='smiley.gif' alt='Smiley face' height='42'
//     * width='42'/> any other file - <a
//     * href='/post/download?fileid=filesysid123'>Dislay name</a> mov,mp4 -
//     * <video>
//     *
//     * @param filename
//     * @return
//     */
//    public static String createHtmlElement(String fileid, String filename, String displayName)
//    {
//        String element = PostProcessEngine.HTML_BEGIN;
//        String url = AjaxSocial.SOCIAL_DOWNLOAD_URL + "?" + AjaxSocial.FILEID_PARAM + "=" + fileid;
//
//        filename = filename.toLowerCase();
//
//        // for images
//        if (filename.indexOf("png") > -1 || filename.indexOf("jpg") > -1 || filename.indexOf("gif") > -1 || filename.indexOf("bmp") > -1 || filename.indexOf("tif") > -1)
//        {
//            element += "<a href=\"#\" onclick=\"javascript:displayLargeImage(\\'/resolve/service" + url + "\\', \\'" + displayName + "\\')\">" + "<img src=/resolve/service" + url + " title=" + displayName + " height=42 width=42></img>" + "</a>";
//        }/*
//          * else if( filename.indexOf("mov") > -1) //for videos {
//          *
//          * }
//          */
//        else
//        // rest of it are files
//        {
//            element += "<a href=/resolve/service" + url + " title=" + displayName + ">" + displayName + "</a>";
//        }
//
//        element += PostProcessEngine.HTML_END;
//
//        return element;
//    }

//    public static List<Map<String, Object>> getAllFollowProcesses(String userName)
//    {
//        List<Map<String, Object>> resolveBaseModel = new ArrayList<Map<String, Object>>();
//        List<Process> socialTeamList = null;
//        User socialUser = SocialCompConversionUtil.getSocialUser(userName);
//        ProcessTree processTree = ServiceSocial.getProcessTree(socialUser);
//        if (processTree != null && processTree.getProcesses() != null)
//        {
//            socialTeamList = new ArrayList<Process>(processTree.getProcesses());
//        }
//
//        if (socialTeamList != null && !socialTeamList.isEmpty())
//        {
//            for (Process process : socialTeamList)
//            {
//                Map<String, Object> rec = getSocialProcessMap(userName, process);
//                if (rec != null)
//                {
//                    resolveBaseModel.add(rec);
//                }
//            }
//        }
//        return resolveBaseModel;
//    }

//    public static void addActionTasksToProcess(List<ActionTaskModel> actionTasks, String processId, String username) throws Exception
//    {
//        Process socialProcess = createProcess(processId);
//
//        for (ActionTaskModel actionTask : actionTasks)
//        {
//            ActionTask socialActionTask = createActionTask(actionTask.getSys_ID());
////            ServiceSocial.createActionTask(socialActionTask);
//            ServiceSocial.addActionTaskToProcess(socialActionTask, socialProcess);
//            NotificationHelper.getActionTaskNotification(socialActionTask, socialProcess, UserGlobalNotificationContainerType.ACTIONTASK_ADDED_TO_PROCESS, username, true, true);
//            NotificationHelper.getProcessNotification(socialProcess, socialActionTask, UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_ADDED, username, true, true);
//        }
//    }
//
//    public static void removeActionTasksFromProcess(List<ActionTaskModel> actionTasks, String processId, String username)  throws Exception
//    {
//        Process socialProcess = createProcess(processId);
//        // SocialStore.createTeam(socialTeam);
//
//        for (ActionTaskModel model : actionTasks)
//        {
//            ActionTask socialActionTask = createActionTask(model.getSys_ID());
//            NotificationHelper.getActionTaskNotification(socialActionTask, socialProcess, UserGlobalNotificationContainerType.ACTIONTASK_REMOVED_FROM_PROCESS, username, true, false);
//            NotificationHelper.getProcessNotification(socialProcess, socialActionTask, UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_REMOVED, username, true, false);
//            ServiceSocial.removeActionTaskFromProcess(socialActionTask, socialProcess);
//        }
//    }
//
//    public static void addRssToProcess(List<RssSubscriptionBaseModel> rssList, String processId, String username) throws Exception
//    {
//        Process socialProcess = createProcess(processId);
//
////        ServiceSocial.createProcess(socialProcess);
//
//        for (RssSubscriptionBaseModel rss : rssList)
//        {
//            Rss socialRss = createRss(rss.getSys_Id());
////            ServiceSocial.createRss(socialRss);
//            ServiceSocial.subscribeRssToProcess(socialRss, socialProcess);
//            NotificationHelper.getRssNotification(socialRss, socialProcess, UserGlobalNotificationContainerType.RSS_ADDED_TO_PROCESS, username, true, true);
//            NotificationHelper.getProcessNotification(socialProcess, socialRss, UserGlobalNotificationContainerType.PROCESS_RSS_ADDED, username, true, true);
//        }
//    }
//
//    private static Rss convertRssToSocialRss(RssSubscriptionBaseModel rssModel)
//    {
//        Rss socialRss = new Rss();
//        socialRss.setId(rssModel.getSys_Id());
//        socialRss.setSys_id(rssModel.getSys_Id());
//        socialRss.setDisplayName(rssModel.getUDisplayName());
//        return socialRss;
//    }
//
//    public static void removeRssFromProcess(List<RssSubscriptionBaseModel> rssList, String processId, String username)  throws Exception
//    {
//        Process socialProcess = createProcess(processId);
//        // SocialStore.createTeam(socialTeam);
//
//        for (RssSubscriptionBaseModel rssModel : rssList)
//        {
//            Rss socialRss = createRss(rssModel.getSys_Id());
//            NotificationHelper.getRssNotification(socialRss, socialProcess, UserGlobalNotificationContainerType.RSS_REMOVED_FROM_PROCESS, username, true, false);
//            NotificationHelper.getProcessNotification(socialProcess, socialRss, UserGlobalNotificationContainerType.PROCESS_RSS_REMOVED, username, true, false);
//            ServiceSocial.unsubscribeRssFromProcess(socialRss, socialProcess);
//        }
//    }
//
//    public static void addForumsToProcess(List<ForumBaseModel> forums, String processId) throws Exception
//    {
//        Process socialProcess = new Process();
//        Map<String, Object> rsProcessMap = null;
//
//        try
//        {
//            rsProcessMap = ServiceHibernate.findById(ProcessBaseModel.TABLE_NAME, processId);
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        if (rsProcessMap != null)
//        {
//            socialProcess.setId((String) rsProcessMap.get(ProcessBaseModel.SYS_ID));
//            socialProcess.setSys_id((String) rsProcessMap.get(ProcessBaseModel.SYS_ID));
//            socialProcess.setDisplayName((String) rsProcessMap.get(ProcessBaseModel.U_DISPLAY_NAME));
//        }
//
////        ServiceSocial.createProcess(socialProcess);
//
//        for (ForumBaseModel model : forums)
//        {
//            Forum sForum = convertForumToSocialForum(model);
////            ServiceSocial.createForum(sForum);
//            ServiceSocial.addForumToProcess(sForum, socialProcess);
//        }
//    }
//
//    public static void removeForumsFromProcess(List<ForumBaseModel> forumList, String processId)  throws Exception
//    {
//        Process socialProcess = new Process();
//        socialProcess.setId(processId);
//        socialProcess.setSys_id(processId);
//
//        for (ForumBaseModel forumModel : forumList)
//        {
//            Forum socialForum = createForum(forumModel.getSys_Id());
//            ServiceSocial.removeForumFromProcess(socialForum, socialProcess);
//        }
//    }
//
//    public static Forum convertForumToSocialForum(ForumBaseModel model)
//    {
//        Forum socialForum = null;
//        if (model != null)
//        {
//            socialForum = new Forum();
//            socialForum.setSys_id(model.getSys_Id());
//            socialForum.setDisplayName(model.getUDisplayName());
//        }
//        return socialForum;
//    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Miscelleanous
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    public static void addToHome(String username, RSComponent comp)
//    {
//        User user = SocialCompConversionUtil.getSocialUser(username);
//
//        ServiceGraph.addToHome(user, comp);
//
//    }
//
//    public static void removeFromHome(String username, RSComponent comp)
//    {
//        User user = SocialCompConversionUtil.getSocialUser(username);
//
//        ServiceGraph.removeFromHome(user, comp);
//
//    }
//
//    public static void markStarToPost(String username, String postId)  throws Exception
//    {
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        Post post = createPost(postId);
//
//        ServiceGraph.markStarred(user, post);
//    }
//
//    public static void unmarkStarToPost(String username, String postId)  throws Exception
//    {
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        Post post = createPost(postId);
//
//        ServiceGraph.unmarkStarred(user, post);
//
//    }
//
//    public static Post likePost(String username, String postId)
//    {
//        User user = SocialCompConversionUtil.getSocialUser(username);
//        Post post = createPost(postId);
//
//        post = ServiceGraph.markLike(user, post);
//
//        return post;
//
//    }

    public static Post createPost(String postId)
    {
        Post post = new Post();
        post.setId(postId);
        post.setSys_id(postId);

        return post;
    }// createPost

    // //////////////////////
    // Forums
    // /////////////////////

//    public static void addUsersToForum(List<UserBaseModel> users, String forumId)  throws Exception
//    {
//        Forum socialForum = new Forum();
//        Map<String, Object> rsForumMap = null;
//
//        try
//        {
//            rsForumMap = ServiceHibernate.findById(ForumBaseModel.TABLE_NAME, forumId);
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        if (rsForumMap != null)
//        {
//            socialForum.setSys_id((String) rsForumMap.get(ForumBaseModel.SYS_ID));
//            socialForum.setDisplayName((String) rsForumMap.get(ForumBaseModel.U_DISPLAY_NAME));
//
//        }
//
//        for (UserBaseModel user : users)
//        {
//            User socialUser = convertUsersToSocialUser(user);
////            ServiceSocial.createForum(socialForum);
////            ServiceSocial.createUser(socialUser);
//            addUserToForum(socialUser, socialForum);
//        }
//
//    } // addUsersToForums
//
//    public static void addUserToForums(String userId, List<ForumBaseModel> forums)  throws Exception
//    {
////        UsersVO user = getUser(userId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userId);//convertUsersToSocialUser(user);
////        ServiceSocial.createUser(socialUser);
//        for (ForumBaseModel forum : forums)
//        {
//            Forum socialForum = convertForumToSocialForum(forum);
////            ServiceSocial.createForum(socialForum);
//            addUserToForum(socialUser, socialForum);
//        }
//
//    } // addUserToForums

    public static void addUserToForum(User socialUser, Forum socialForum)  throws Exception
    {
//        ServiceSocial.addUserToForum(socialUser, socialForum);
    }

//    public static void removeUsersFromForum(List<UserBaseModel> users, String forumId)
//    {
//        Forum socialForum = new Forum();
//        socialForum.setId(forumId);
//        socialForum.setSys_id(forumId);
//
//        for (UserBaseModel user : users)
//        {
//            User socialUser = convertUsersToSocialUser(user);
//            removeUserFromForum(socialUser, socialForum);
//        }
//
//    } // removeUsersFromForum

//    public static Forum getForum(String forumId, boolean children)
//    {
//        Forum socialForum = null;
//        socialForum = ServiceGraph.getForum(forumId, children);
//        return socialForum;
//
//    } // getForum

//    public static void deleteForum(ForumBaseModel forum) throws Exception
//    {
//        Forum socialForum = convertForumToSocialForum(forum);
//        ServiceSocial.deleteComponent(socialForum.getSys_id(), "system");
//
//    } // deleteForum

//    public static List<Map<String, Object>> getAllFollowForums(String userName)
//    {
//        List<Map<String, Object>> resolveBaseModel = new ArrayList<Map<String, Object>>();
//        User socialUser = SocialCompConversionUtil.getSocialUser(userName);
//        ForumTree forumtree = ServiceSocial.getForumsTree(socialUser);
//
//        // get Forum from forum collection as well as all forums inside process
//        List<Forum> socialForumList = new ArrayList<Forum>(forumtree.getForums());
//        // Collection<Process> processContainForums = (Collection<Process>)
//        // forumtree.getProcesses();
//        // for(Process proc : processContainForums)
//        // {
//        // socialForumList.addAll(proc.getForums());
//        // }
//
//        if (socialForumList != null && !socialForumList.isEmpty())
//        {
//            for (Forum forum : socialForumList)
//            {
//                resolveBaseModel.add(getSocialForumMap(userName, forum));
//            }
//        }
//        return resolveBaseModel;
//
//    } // getAllFollowForums

//    public static Map<String, Object> getSocialForumMap(String userName, Forum forum)
//    {
//        Map<String, Object> map = null;
//        String forumId = forum.getSys_id();
//
//        try
//        {
//            map = ServiceHibernate.findById(ForumBaseModel.TABLE_NAME, forumId);
//            if (map == null)
//            {
//                Log.log.error("Forum '" + forum.getDisplayName() + "' with Sys_ID '" + forumId + "' is not present in Database.");
//            }
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//        }
//
//        return map;
//
//    } // getSocialForumMap

//    public static void removeUserFromForums(String userId, List<ForumBaseModel> forums)
//    {
////        UsersVO user = getUser(userId);
//        User socialUser = SocialCompConversionUtil.getSocialUserById(userId);//convertUsersToSocialUser(user);
//        for (ForumBaseModel forum : forums)
//        {
//            Forum socialForum = convertForumToSocialForum(forum);
//            removeUserFromForum(socialUser, socialForum);
//        }
//
//    } // removeUserFromForums

//    public static void removeUserFromForum(User socialUser, Forum socialForum)
//    {
//        ServiceGraph.removeUserFromForum(socialUser, socialForum);
//    }

//    public static void submitESBForUpdateSocialForWiki(Map<String, Object> params)
//    {
//        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.updateSocialForWiki", params);
//    }

    public static void updateComponentESB(Map<String, Object> params)
    {
        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.updateComponent", params);
    }

    public static void deleteComponentESB(Map<String, Object> params)
    {
        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.deleteComponent", params);
    }

    public static void submitNotification(SubmitNotification submitNotification, boolean isAsync)
    {
        if (isAsync)
        {
            submitNotificationESB(submitNotification);
        }
        else
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("submitNotification", submitNotification);
            MSocial mSocial = new MSocial();
            mSocial.submitNotification(params);
        }
    }

    public static void submitNotifications(List<SubmitNotification> notifications, boolean isAsync)
    {
        if (isAsync)
        {
            submitBulkNotificationESB(notifications);
        }
        else
        {
            for (SubmitNotification notification : notifications)
            {
                submitNotification(notification, isAsync);
            }
        }
    }

    private static void submitNotificationESB(SubmitNotification submitNotification)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("submitNotification", submitNotification);

        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.submitNotification", params);
    }

    private static void submitBulkNotificationESB(List<SubmitNotification> notifications)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bulkNotifications", notifications);

        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.submitBulkNotifications", params);
    }

    /*
     * private static void submitNotificationsESB(List<SubmitNotification>
     * notifications) { Map<String, Object> params = new HashMap<String,
     * Object>(); params.put("submitNotificationList", notifications);
     *
     * Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW,
     * "MSocial.submitNotification", params); }
     */

    /**
     * MAY NOT NEED THIS ANYMORE
     */
//    public static void updateSocialForWiki(Map<String, Object> params)
//    {
//        Object oldComp = params.get(ConstantValues.SOCIAL_OLD_TYPE);
//
//        // first convert the obj from doc to runbook or vice versa
//        if (oldComp != null)
//        {
//            ServiceSocial.swapRunbookOrDocument((RSComponent) oldComp);
//        }
//
//        // update if there is any change in the attributes of the object
//        updateComponent(params);
//
//    }// updateSocialForWiki

    /**
     * MAY NOT NEED THIS ANYMORE
     */
//    public static void updateComponent(Map<String, Object> params)
//    {
//        RSComponent comp = (RSComponent) params.get(ConstantValues.SOCIAL_RS_COMPONENT);
//
//        if (comp != null)
//        {
////            try
////            {
////                ServiceSocial.insertOrUpdateRSComponent(comp);
////            }
////            catch (Exception e)
////            {
////               Log.log.error(e);
////            }
//        }
//
//    }

//    public static void deleteComponent(Map<String, Object> params) throws Exception
//    {
//        Object comp = params.get(ConstantValues.SOCIAL_DELETE_COMPONENT);
//        Object sysIds = params.get(ConstantValues.SOCIAL_DELETE_LIST_OF_DOCSYSID);
//        Object atsysIds = params.get(ConstantValues.SOCIAL_DELETE_LIST_OF_ATSYSID);
//        Object usersysIds = params.get(ConstantValues.SOCIAL_DELETE_LIST_OF_USERS_SYSID);
//
////        if (comp != null)
////        {
////            if (comp instanceof RSComponent)
////            {
////                ServiceSocial.deleteComponent(((RSComponent) comp).getSys_id(), "system");
////            }
////        }
////
////        if (sysIds != null)
////        {
////            List<String> listSysIds = (List<String>) sysIds;
////            Document doc = null;
////            for (String sysId : listSysIds)
////            {
////                doc = new Document();
////                doc.setId(sysId);
////                doc.setSys_id(sysId);
////
////                ServiceSocial.deleteComponent(doc.getSys_id(), "system");
////            }
////        }
////
////        if (atsysIds != null)
////        {
////            List<String> listSysIds = (List<String>) atsysIds;
////            ActionTask task = null;
////            for (String sysId : listSysIds)
////            {
////                task = new ActionTask();
////                task.setId(sysId);
////                task.setSys_id(sysId);
////
////                ServiceSocial.deleteComponent(task.getSys_id(), "system");
////            }
////        }
////
////        if (usersysIds != null)
////        {
////            List<String> listSysIds = (List<String>) usersysIds;
////            User user = null;
////            for (String sysId : listSysIds)
////            {
////                user = new User();
////                user.setId(sysId);
////                user.setSys_id(sysId);
////
////                ServiceSocial.deleteComponent(user.getSys_id(), "system");
////            }
////        }
//
//    }

//    public static void deleteRSS(RssSubscriptionBaseModel rss, String username) throws Exception
//    {
//        Rss socialRSS = createRss(rss.getSys_Id());
//        ServiceSocial.deleteComponent(socialRSS.getSys_id(), "system");
//
//    } // deleteRSS

    // Utility
    public static RSComponent getRSComponentBasedOnType(SocialComponentType type, String compId, String displayName)
    {
        RSComponent componentType = null;
        if (type == null)
        {
            componentType = null;
        }
        else if (type == SocialComponentType.PROCESS)
        {
            componentType = new com.resolve.services.graph.social.model.component.container.Process();
        }
        else if (type == SocialComponentType.TEAMS)
        {
            componentType = new Team();
        }
        else if (type == SocialComponentType.RUNBOOKS)
        {
            componentType = new Runbook();
        }
        else if (type == SocialComponentType.DOCUMENTS)
        {
            componentType = new Document();
        }
        else if (type == SocialComponentType.ACTIONTASKS)
        {
            componentType = new ActionTask();
        }
        else if (type == SocialComponentType.USERS)
        {
            componentType = new User();
        }
        else if (type == SocialComponentType.FOLLOWS)
        {
            componentType = new User();
        }
        else if (type == SocialComponentType.RSS)
        {
            componentType = new Rss();
        }
        else if (type == SocialComponentType.FORUMS)
        {
            componentType = new Forum();
        }

        if (componentType != null)
        {
            componentType.setId(compId);
            componentType.setSys_id(compId);
            componentType.setDisplayName(displayName);
        }

        return componentType;

    }

    public static SocialUIPreferences getUserSocialPreferences(String username)
    {
        SocialUIPreferences preferences = new SocialUIPreferences();
        Map<String, String> prefMap = new HashMap<String, String>();

        // prepare the map
        try
        {
            prefMap.putAll(ServiceHibernate.getUserSocialPreferences(username));
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        //Note: ALL THESE ARE FROM THE UI NOW AND NOT VALID
        
        // populate the pref obj
//        preferences.setPostAutoRefreshInterval(prefMap.get(SocialConstants.POST_REFRESH) != null ? prefMap.get(SocialConstants.POST_REFRESH) : "2");

        //populate the preference for menu sets
//        preferences.setMenuSets(prefMap.get(SocialConstants.MENU_SETS) != null ? prefMap.get(SocialConstants.MENU_SETS) : "");

//        if (prefMap.size() > 0)
//        {
//            preferences.setUserAutoRefreshFlag(getPrefValue(SocialConstants.AUTO_POST_REFRESH, prefMap));
//            preferences.setSocialuserAdvanceTab(getPrefValue(SocialConstants.ADVANCED_TAB, prefMap));
//            preferences.setSocialuserProcessTab(getPrefValue(SocialConstants.PROCESS_TAB, prefMap));
//            preferences.setSocialuserTeamTab(getPrefValue(SocialConstants.TEAMS_TAB, prefMap));
//            preferences.setSocialuserForumTab(getPrefValue(SocialConstants.FORUMS_TAB, prefMap));
//            preferences.setSocialuserActionTaskTab(getPrefValue(SocialConstants.ACTIONTASK_TAB, prefMap));
//            preferences.setSocialuserRunbookTab(getPrefValue(SocialConstants.RUNBOOKS_TAB, prefMap));
//            preferences.setSocialuserDocumentTab(getPrefValue(SocialConstants.DOCUMENTS_TAB, prefMap));
//            preferences.setSocialuserUserTab(getPrefValue(SocialConstants.USER_TAB, prefMap));
//            preferences.setSocialuserFeedTab(getPrefValue(SocialConstants.FEED_TAB, prefMap));
//            preferences.setSocialuserShowEastpanel(getPrefValue(SocialConstants.SHOW_EAST_PANEL, prefMap));
//            preferences.setSocialuserShowEastpanelWhatToDoWidget(getPrefValue(SocialConstants.SHOW_EAST_PANEL_WHAT_TO_DO_WIDGET, prefMap));
//        }
//        else
//        {
//            preferences.setSocialuserProcessTab(true);
//            preferences.setSocialuserAdvanceTab(true);
//        }

        return preferences;
    }

    //
    // public static boolean hasDeleteRights(User loggedInUser, String
    // postRoles)
    // {
    // boolean hasAccess = false;
    //
    //
    //
    // String roles = postRoles.replaceAll(" ", ",");
    // String csvUserRoles = loggedInUser.getRoles().replaceAll(" ",
    // ",").replaceAll("_", "&");
    // Set<String> userRoles = new HashSet<String>
    // (StringUtils.convertStringToList(csvUserRoles, ","));
    //
    // boolean hasAccess = UserUtils.hasRole(loggedInUser.getUsername(),
    // userRoles, roles);
    //
    //
    //
    // return hasAccess;
    // }

    public static boolean isAdminUser(User user)
    {
        boolean isAdmin = false;

        try
        {
            String csvUserRoles = user.getRoles().replaceAll(" ", ",").replaceAll("_", "&");
            Set<String> userRoles = new HashSet<String>(StringUtils.convertStringToList(csvUserRoles, ","));
            if (userRoles.contains(UserUtils.ADMIN))
            {
                isAdmin = true;
            }
        }
        catch (Throwable t)
        {
            Log.log.error("error validating if the social user is admin or not.", t);
        }

        return isAdmin;
    }

    /**
     *
     // %3F ==> ? // %3D --> = // %26 --> & // %27 --> ' // %20 --> space
     *
     * @param url
     * @return
     */
    public static String encodeUrlForJS(String url)
    {
        String encodeUrl = url;

        encodeUrl = encodeUrl.replaceAll("\\?", "%3F");
        encodeUrl = encodeUrl.replaceAll("=", "%3D");
        encodeUrl = encodeUrl.replaceAll("&", "%26");
        encodeUrl = encodeUrl.replaceAll("'", "%27");
        encodeUrl = encodeUrl.replaceAll(" ", "%20");

        return encodeUrl;
    }

    private static boolean getPrefValue(String key, Map<String, String> values)
    {
        boolean val = false;
        String value = values.get(key);

        if (StringUtils.isNotEmpty(value))
        {
            val = value.equalsIgnoreCase("T") ? true : false;
        }

        return val;
    }

    /**
     * This method retuns all User Names which is not locked.
     *
     * @return
     */
    public static List<User> getAllSocialUsers()
    {
        List<User> socialUsers = new ArrayList<User>();
        List<UsersVO> unlockedUsers = UserUtils.getAllUsers(false);
        try
        {
            for(UsersVO user : unlockedUsers)
            {
                socialUsers.add(SocialCompConversionUtil.convertUserToSocialUser(user, null));
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return socialUsers;
    } //getAllSocialUsers

//    public static User getSocialUser(HttpServletRequest request)
//    {
//        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
//        return SocialCompConversionUtil.getSocialUser(username);
//    }
}
