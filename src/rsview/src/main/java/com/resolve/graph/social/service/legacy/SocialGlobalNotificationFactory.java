/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialGlobalNotificationFactory extends SocialFactory
{
    
    public static SocialUsersFactory socialUsersFactory = new SocialUsersFactory();
    private static Node notificationConfigContainerNode;
    
    public static void init()
    {   
        
        init(SocialRootRelationshipTypes.NOTIFICATIONCONFIG, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.NOTIFICATIONCONFIG.name(), containerNode);
        notificationConfigContainerNode = containerNode;

        //init document
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_CREATE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_UPDATE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_ACTIVE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_INACTIVE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_LOCKED);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_UNLOCKED);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_HIDE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_UNHIDE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_DELETE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_UNDELETE);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_PURGED);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_ADDED_TO_PROCESS); 
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_REMOVED_FROM_PROCESS);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_COMMIT);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.DOCUMENT_FORWARD_EMAIL);
        
        //init Namespace
        initProperties(UserGlobalNotificationContainerType.NAMESPACE_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.NAMESPACE_FORWARD_EMAIL);
        
        //init actiontask
        initProperties(UserGlobalNotificationContainerType.ACTIONTASK_CREATE);
        initProperties(UserGlobalNotificationContainerType.ACTIONTASK_UPDATE);
        initProperties(UserGlobalNotificationContainerType.ACTIONTASK_PURGED);
        initProperties(UserGlobalNotificationContainerType.ACTIONTASK_ADDED_TO_PROCESS);
        initProperties(UserGlobalNotificationContainerType.ACTIONTASK_REMOVED_FROM_PROCESS);
        initProperties(UserGlobalNotificationContainerType.ACTIONTASK_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.ACTIONTASK_FORWARD_EMAIL);
        
        //init worksheet
        initProperties(UserGlobalNotificationContainerType.WORKSHEET_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.WORKSHEET_FORWARD_EMAIL);
        
        //init rss
        initProperties(UserGlobalNotificationContainerType.RSS_CREATE);
        initProperties(UserGlobalNotificationContainerType.RSS_UPDATE);
        initProperties(UserGlobalNotificationContainerType.RSS_PURGED);
        initProperties(UserGlobalNotificationContainerType.RSS_ADDED_TO_PROCESS);
        initProperties(UserGlobalNotificationContainerType.RSS_REMOVED_FROM_PROCESS);
        initProperties(UserGlobalNotificationContainerType.RSS_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.RSS_FORWARD_EMAIL);
        
        //init forum
        initProperties(UserGlobalNotificationContainerType.FORUM_CREATE);
        initProperties(UserGlobalNotificationContainerType.FORUM_UPDATE);
        initProperties(UserGlobalNotificationContainerType.FORUM_PURGED);
        initProperties(UserGlobalNotificationContainerType.FORUM_ADDED_TO_PROCESS);
        initProperties(UserGlobalNotificationContainerType.FORUM_REMOVED_FROM_PROCESS);
        initProperties(UserGlobalNotificationContainerType.FORUM_USER_ADDED);
        initProperties(UserGlobalNotificationContainerType.FORUM_USER_REMOVED);
        initProperties(UserGlobalNotificationContainerType.FORUM_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.FORUM_FORWARD_EMAIL);
        
        //init team
        initProperties(UserGlobalNotificationContainerType.TEAM_CREATE);
        initProperties(UserGlobalNotificationContainerType.TEAM_UPDATE);
        initProperties(UserGlobalNotificationContainerType.TEAM_PURGED);
        initProperties(UserGlobalNotificationContainerType.TEAM_ADDED_TO_PROCESS);
        initProperties(UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_PROCESS);
        initProperties(UserGlobalNotificationContainerType.TEAM_ADDED_TO_TEAM);
        initProperties(UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_TEAM);
        initProperties(UserGlobalNotificationContainerType.TEAM_USER_ADDED);
        initProperties(UserGlobalNotificationContainerType.TEAM_USER_REMOVED);
        initProperties(UserGlobalNotificationContainerType.TEAM_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.TEAM_FORWARD_EMAIL);
        
        //init process
        initProperties(UserGlobalNotificationContainerType.PROCESS_CREATE);
        initProperties(UserGlobalNotificationContainerType.PROCESS_UPDATE);
        initProperties(UserGlobalNotificationContainerType.PROCESS_PURGED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_DOCUMENT_ADDED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_DOCUMENT_REMOVED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_ADDED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_REMOVED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_RSS_ADDED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_RSS_REMOVED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_FORUM_ADDED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_FORUM_REMOVED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_TEAM_ADDED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_TEAM_REMOVED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_USER_ADDED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_USER_REMOVED);
        initProperties(UserGlobalNotificationContainerType.PROCESS_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL);
     
        //init user
        initProperties(UserGlobalNotificationContainerType.USER_ADDED_TO_PROCESS);
        initProperties(UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM);
        initProperties(UserGlobalNotificationContainerType.USER_FOLLOW_ME);
        initProperties(UserGlobalNotificationContainerType.USER_REMOVED_FROM_PROCESS);
        initProperties(UserGlobalNotificationContainerType.USER_REMOVED_FROM_TEAM);
        initProperties(UserGlobalNotificationContainerType.USER_UNFOLLOW_ME);
        initProperties(UserGlobalNotificationContainerType.USER_ADDED_TO_FORUM);
        initProperties(UserGlobalNotificationContainerType.USER_REMOVED_FROM_FORUM);
        initProperties(UserGlobalNotificationContainerType.USER_PROFILE_CHANGE);
        initProperties(UserGlobalNotificationContainerType.USER_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.USER_FORWARD_EMAIL);
        initProperties(UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL);
        initProperties(UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL);
        initProperties(UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL);
       
    }
        
    public static void initProperties(UserGlobalNotificationContainerType notifyContainerType) 
    {
        Transaction tx = null;
        Node notifyConfigTypeNode = null;
        
        try
        {           
            Relationship rel = findNotificationConfigNode(notifyContainerType);
            
            if(rel == null)
            {
//                tx = GraphDBUtil.getGraphDBService().beginTx();            
//                notifyConfigTypeNode =  GraphDBUtil.getGraphDBService().createNode();
//                notifyConfigTypeNode.setProperty(RSComponent.SYS_ID, notifyContainerType.name());
//                notifyConfigTypeNode.setProperty(NODE_TYPE, notifyContainerType.name());
                
//                if(notifyConfigTypeNode != null)
//                {
//                    ResolveGraphFactory.addRelation(notificationConfigContainerNode, notifyConfigTypeNode, notifyContainerType);
//                }
                
//                tx.success();
            }
        }
        catch(Throwable t)
        {
            Log.log.info(t.getMessage() + t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }
    
    public static Relationship findNotificationConfigNode(UserGlobalNotificationContainerType notifyContainerType)
    {
        Relationship relLocal = null;
        
        if(notificationConfigContainerNode != null)
        {
            Iterable<Relationship> rels = notificationConfigContainerNode.getRelationships();
            
            for(Relationship rel: rels)
            {
               if(rel.getType().name().equals(notifyContainerType.name()))
               {
                   relLocal = rel;
                   
                   break;
               }
            }
        }
        
        return relLocal;
    }
    
    public static void addUserNotificationConfig(User user, List<NotificationConfig> notificationConfig)
                                                 throws Exception
    {        
        Node userNode = null;
    
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//        
//            if(userNode != null && notificationConfig != null && !notificationConfig.isEmpty())
//            {                            
//                Node typeNodeLocal = null;
//                String valueString = "";
//                //add relation between user and notificationConfig node
//                for(NotificationConfig notifConfig: notificationConfig)
//                {
//                    UserGlobalNotificationContainerType type = notifConfig.getType();
//                    valueString = String.valueOf(notifConfig.getValue());
//            
//                    typeNodeLocal = getNotificationConfigTypeNode(type);
//            
//                    if(typeNodeLocal != null)
//                    {
////                        addRelationWithProperty(typeNodeLocal, userNode, type, valueString);
//                    }
//                }
//            }
//        }
    }
    
    public static void updateUserNotificationConfig(User user, List<NotificationConfig> notificationConfig)
                                                    throws Exception
    {        
        Node userNode = null;
        
//        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
//        {
//            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            
//            if(userNode != null && notificationConfig != null && !notificationConfig.isEmpty())
//            {                            
//                //remove all notificationConfig relations from the user
//                removeAllRelationsFromUser(userNode, notificationConfig);
//                
//                Node typeNodeLocal = null;
//                String valueString = "";
//                //add relation between user and notificationConfig node
//                for(NotificationConfig notifConfig: notificationConfig)
//                {
//                    UserGlobalNotificationContainerType type = notifConfig.getType();
//                    valueString = String.valueOf(notifConfig.getValue());
//                    
//                    typeNodeLocal = getNotificationConfigTypeNode(type);
//                    
//                    if(typeNodeLocal != null)
//                    {
////                        addRelationWithProperty(typeNodeLocal, userNode, type, valueString);
//                    }
//                }
//            }
//        }
    }
    
    public static Node getNotificationConfigTypeNode(UserGlobalNotificationContainerType type)
    {
        Node typeNode = null;
        
////        Iterable<Relationship> rels = notificationConfigContainerNode.getRelationships(type);
//        
//        if(rels != null)
//        {
//            for(Relationship rel: rels)
//            {
//                typeNode = rel.getEndNode();
//                break;
//            }
//        }
        
        return typeNode;
    }
    
        
    public static void removeAllRelationsFromUser(Node userNode, List<NotificationConfig> notificationConfig) 
                                                  throws Exception
    {
        
        Transaction tx = null;
        UserGlobalNotificationContainerType typeLocal = null;
        
        try
        {
            if(userNode != null && notificationConfig != null && !notificationConfig.isEmpty())
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();
                
                for(NotificationConfig notifyConfig: notificationConfig)
                {
                    typeLocal = notifyConfig.getType();
                
//                    Iterable<Relationship> rels = userNode.getRelationships(typeLocal, Direction.INCOMING);
//                    
//                    if(rels != null)
//                    {
//                        for(Relationship rel: rels)
//                        {
//                            rel.delete();
//                        }
//                    }
                }
                
                tx.success();
            }
        }
        catch(Exception t)
        {
            Log.log.info("removeAllRelationsFromUser catched exception: " + t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }
   
    public Node findNode(String id)
    {
        return ResolveGraphFactory.findNodeByIndexedID(id);
    }
    
}
