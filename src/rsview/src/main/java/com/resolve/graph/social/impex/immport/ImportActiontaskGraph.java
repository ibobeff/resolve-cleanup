/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.vo.GraphRelationshipDTO;

public class ImportActiontaskGraph  extends ImportComponentGraph
{
    private ActionTask actiontask = null;
    
    
    public ImportActiontaskGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        actiontask = SocialCompConversionUtil.createActionTaskByName(rel.getSourceName());
//        if(actiontask != null)
//        {
//            Node atNode = ResolveGraphFactory.findNodeByIndexedID(actiontask.getSys_id());
//            if(atNode != null)
//            {
//                switch(rel.getTargetType())
//                {
//                    case ResolveTag: 
//                        assignTagToActiontask();
//                        break;
//                        
//                        
//                    default:
//                        break;
//                }
//            }
//            else
//            {
//                throw new Exception("Actiointask " + actiontask.getDisplayName() + " is not sync with graph db." );
//            } 
//        }
    }
    
    private void assignTagToActiontask() throws Exception
    {
        String tagName = rel.getTargetName();
        Set<String> tagNames = new HashSet<String>();
        tagNames.add(tagName);
        
//        ServiceTag.addAdditionalTagsToComponent(actiontask.getSys_id(), tagNames);
    }
    
    

}
