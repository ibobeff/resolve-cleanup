/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.export;

import java.util.List;

import org.neo4j.graphdb.Node;

import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.util.TraversalUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.GraphRelationshipDTO;

public class ExportForumGraph extends ExportComponentGraph
{

    public ExportForumGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
       super(sysId, options, SocialRelationshipTypes.FORUM);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode, SocialImpexVO socialImpex) throws Exception
    {
      //get all the nodes that this process node is refering too
        Iterable<Node> membersOfForumsNode = TraversalUtil.findUsersBelongingToForum(compNode);
        if(membersOfForumsNode != null)
        {
            for(Node anyNode : membersOfForumsNode)
            {
                addRelationship(compNode, anyNode);
                prepareReturnNode(anyNode, socialImpex);
                
            }//end of for
        }
        
        return relationships;
    }
    
    private void prepareReturnNode(Node anyNode, SocialImpexVO socialImpex) throws Exception
    {
//        String type = (String) anyNode.getProperty(RSComponent.TYPE);
//        String displayName = (String) anyNode.getProperty(RSComponent.DISPLAYNAME);
//        String sysId = (String) anyNode.getProperty(RSComponent.SYS_ID);
//        
//        if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//        {
//            if(options != null && options.getForumUsers())
//            {
//                String userName = (String) anyNode.getProperty(RSComponent.USERNAME);
//                socialImpex.addReturnUser(new User(sysId, userName, displayName));
//            }
//        }
        
    }
}
