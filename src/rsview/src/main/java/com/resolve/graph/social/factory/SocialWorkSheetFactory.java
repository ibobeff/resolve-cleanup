/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.util.Log;

public class SocialWorkSheetFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.WORKSHEET, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.WORKSHEET.name(), containerNode);
    }

    public static Worksheet insertOrUpdateWorksheet(Worksheet team, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                team = insert(team);
//            }
//            else
//            {
//                //update
//                team = (Worksheet) updateComponent(team, docNode);
//            }
        }
        catch(Exception t)
        {
            Log.log.warn("create WorkSheet catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
         return team;
    }

    public static Set<RSComponent> findAllWorksheets()
    {
        Node containerNode = getContainerNode();
        return findAllRsComponentFor(containerNode, SocialRelationshipTypes.WORKSHEET);
    }

    protected static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.WORKSHEET.name());
        }

        return containerNodeLocal;
    }

    private static Worksheet insert(Worksheet worksheet) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

            //insert
//            tx = GraphDBUtil.getGraphDBService().beginTx();
//
//            Node node = GraphDBUtil.getGraphDBService().createNode();
//            worksheet.convertObjectToNode(node);
//            //                node.setProperty(NODE_TYPE, SocialObjectType.DOCUMENT.name());
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.WORKSHEET);
//
//            tx.success();
//
//            //update the object
//            worksheet.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create WorkSheet catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return worksheet;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }
}
