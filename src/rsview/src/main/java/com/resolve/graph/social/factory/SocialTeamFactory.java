/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.util.Log;

public class SocialTeamFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.TEAMS, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.TEAMS.name(), containerNode);
    }

    public static Team insertOrUpdateProcess(Team team, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(team.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                team = insert(team);
//            }
//            else
//            {
//                //update
//                team = (Team) updateComponent(team, docNode);
//            }
        }
        catch(Exception t)
        {
            Log.log.warn("create Team catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
         return team;
    }

    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.TEAMS.name());
        }

        return containerNodeLocal;
    }

    public static Collection<Team> getAllTeams()
    {
        Collection<Team> teams = new ArrayList<Team>();
        Team teamLocal = null;

        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.TEAMS.name());
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.TEAM);

            if(rels != null)
            {
                for(Relationship rel: rels)
                {
//                    Node endNode = rel.getEndNode();
//                    teamLocal = new Team();
//                    teamLocal.convertNodeToObject(endNode);
//                    teams.add(teamLocal);
                }
            }
        }

        return teams;
    }
    
    public static void syncGraphToDB(Set<String> dbSysIds)
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.TEAMS.name());
        }
        
        if(dbSysIds == null)
        {
            //to avoid the null pointer
            dbSysIds = new HashSet<String>();
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.TEAM);
            deleteNodes(dbSysIds, rels);
        }
    }

    private static Team insert(Team team) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

//            //insert
//            tx = GraphDBUtil.getGraphDBService().beginTx();
//
//            Node node = GraphDBUtil.getGraphDBService().createNode();
//            team.convertObjectToNode(node);
//            //                node.setProperty(NODE_TYPE, SocialObjectType.DOCUMENT.name());
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.TEAM);
//
//            tx.success();
//
//            //update the object
//            team.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create Team catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return team;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }
}
