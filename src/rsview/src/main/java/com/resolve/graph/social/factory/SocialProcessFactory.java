/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.util.Log;


public class SocialProcessFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.PROCESSES, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.PROCESSES.name(), containerNode);
    }

    public static Process insertOrUpdateProcess(Process process, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(process.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                process = insert(process);
//            }
//            else
//            {
//                //update
//                process = (Process) updateComponent(process, docNode);
//            }
        }
        catch(Exception t)
        {
            Log.log.warn("create Process catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
         return process;
    }


    public static Collection<Process> getAllProcess()
    {
        Collection<Process> processes = new ArrayList<Process>();
        Process processLocal = null;

        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.PROCESSES.name());
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.PROCESS);

            if(rels != null)
            {
                for(Relationship rel: rels)
                {
//                    Node endNode = rel.getEndNode();
//                    processLocal = new Process();
//                    processLocal.convertNodeToObject(endNode);
//                    processes.add(processLocal);
                }
            }
        }

        return processes;
    }



    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.PROCESSES.name());
        }

        return containerNodeLocal;
    }
    
    public static void syncGraphToDB(Set<String> dbSysIds)
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.PROCESSES.name());
        }
        
        if(dbSysIds == null)
        {
            //to avoid the null pointer
            dbSysIds = new HashSet<String>();
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.PROCESS);
            deleteNodes(dbSysIds, rels);
        }
    }


    private static Process insert(Process process) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

//            //insert
//            tx = GraphDBUtil.getGraphDBService().beginTx();
//
//            Node node = GraphDBUtil.getGraphDBService().createNode();
//            process.convertObjectToNode(node);
//
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.PROCESS);
//
//            tx.success();
//
//            //update the object
//            process.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create Process catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return process;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }
}

