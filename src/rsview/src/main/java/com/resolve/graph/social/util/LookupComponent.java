/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class LookupComponent
{
    public static RSComponent getRSComponentById(String sysid)
    {
        RSComponent rscomp = null;

        try
        {
            if(StringUtils.isNotEmpty(sysid))
            {
                Node node = ResolveGraphFactory.findNodeByIndexedID(sysid);

                if(node != null)
                {
                    if(!node.hasProperty(SocialFactory.NODE_TYPE))
                    {
                        throw new RuntimeException("Node type not there for : " + node.getProperty(ResolveGraphNode.DISPLAYNAME));
                    }

                    String nodeType = (String) node.getProperty(SocialFactory.NODE_TYPE);

                    if(StringUtils.isNotEmpty(nodeType))
                    {
                        if(nodeType.equals(SocialRelationshipTypes.PROCESS.name()))
                        {
                            rscomp = new Process();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.TEAM.name()))
                        {
                            rscomp = new Team();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.FORUM.name()))
                        {
                            rscomp = new Forum();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.USER.name()))
                        {
                            rscomp = new User();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.RSS.name()))
                        {
                            rscomp = new Rss();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.ACTIONTASK.name()))
                        {
                            rscomp = new ActionTask();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.RUNBOOK.name()))
                        {
                            rscomp = new Runbook();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.DOCUMENT.name()))
                        {
                            rscomp = new Document();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.DECISIONTREE.name()))
                        {
                            rscomp = new DecisionTree();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.WORKSHEET.name()))
                        {
                            rscomp = new Worksheet();
                        }
                        else if(nodeType.equals(SocialRelationshipTypes.NAMESPACE.name()))
                        {
                            rscomp = new Namespace();
                        }

//                        rscomp.convertNodeToObject(node);

                    }

                }
            }
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }

        return rscomp;
    }
}
