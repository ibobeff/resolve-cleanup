/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.HighlyAvailableGraphDatabaseFactory;

import com.resolve.graph.catalogbuilder.service.CatalogContainerType;
import com.resolve.graph.catalogbuilder.service.CatalogFactory;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialNameSpaceFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.graph.social.factory.SocialWorkSheetFactory;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.graph.social.service.legacy.SocialDeleteCompFactory;
import com.resolve.graph.social.service.legacy.SocialDeletePostFactory;
import com.resolve.graph.social.service.legacy.SocialGlobalNotificationFactory;
import com.resolve.graph.social.service.legacy.SocialNotificationFactory;
import com.resolve.graph.social.service.legacy.SocialPostFactory;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;


public class GraphManager
{

    public static GraphManager graphManager;
    public static GraphDatabaseService graphDB;
    public static Properties configNeo = null;

    boolean isTimeCanceled = false;
    boolean isInterrupted = false;
    public boolean chachedException = false;

    private GraphManager()
    {
    }

    public static GraphManager getInstance()
    {
        if(graphManager == null)
        {
            synchronized(GraphManager.class)
            {
                if(graphManager == null)
                {
                    graphManager = new GraphManager();
                }
            }
        }

        return graphManager;
    }

    @SuppressWarnings("deprecation")
    public boolean initGraphDB()//Properties configNeo)
    {
        boolean result = false;
        Properties configNeo = null;

        int retry = 3;
        while (!result && retry > 0)
        {
            try
            {
                Log.log.debug("Connecting to graphdb");
                configNeo = new Properties(RSContext.getWebHome()+"WEB-INF/neo4j.properties");
                configNeo.put( org.neo4j.kernel.configuration.Config.NODE_AUTO_INDEXING, "true" );
                configNeo.put( org.neo4j.kernel.configuration.Config.RELATIONSHIP_AUTO_INDEXING, "true" );
                configNeo.put( org.neo4j.kernel.configuration.Config.NODE_KEYS_INDEXABLE, "sys_id" + ", "
                               + SocialFactory.NODE_TYPE + ", " 
//                               + RSComponent.DISPLAYNAME + ", " //added to be able to lookup using the name
//                               + Post.TITLE_URL + ", " 
                               + Namespace.NAMESPACE_NAME + ", " 
                               + ResolveGraphNode.TAG_NAME + ", " 
//                               + ResolveCatalog.ROOT_NAME + ", " 
//                               + ResolveGraphNode.DESC + ", " 
                               + ResolveGraphNode.RESOLVE_GRAPH_ROOT);

                Log.log.debug("neo4j.properties:\n"+configNeo.toMapString());
                isTimeCanceled = false;
                isInterrupted = false;

                final Properties configNeoLocal = configNeo;

                final Timer timer = new Timer();
                timer.schedule(new TimerTask()
                {
                    int i = 0;
                    Thread threadLocal = startCreateGraphDBThread(configNeoLocal);

                    @Override
                    public void run()
                    {
                        i++;

                        if(i>=15 || graphDB != null || chachedException)
                        {
                            if(!threadLocal.isInterrupted())
                            {
                                threadLocal.interrupt();

                                if(graphDB == null && !chachedException)
                                {
                                    threadLocal.stop();
                                }

                                isInterrupted = true;
                            }

                            timer.cancel();
                            isTimeCanceled = true;
                        }
                    }
                }, 0l, 30*1000);

                while (!isTimeCanceled)
                {
                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (Exception exp)
                    {
                    }
                }

                if(graphDB != null)
                {
                    Log.log.debug("Init GraphDBUtil");
                    GraphDBUtil.init();
                    result = true;
                }
                else
                {
                    result = false;
                    throw new Exception("failed init graphDB");
                }

            }
            //catch (ZooKeeperTimedOutException e)
            catch (Exception e)
            {
                try
                {
                    Log.log.error("Failed to initialize graphdb", e);
                    Log.log.debug("Waiting to retry connection to graphdb --- " + e);
                    Thread.sleep(30000);
                }
                catch (Throwable t) { }
            }
            catch (Throwable t)
            {
                Log.log.debug("Error connecting to graphdb");
                t.printStackTrace();
                Log.log.error(t.getMessage(), t);

                try
                {
                    Thread.sleep(30000);
                }
                catch (Throwable t1) { }
            }

            retry--;
        }

        if (result)
        {
            Log.log.info("Connected to graphdb");
        }
        else
        {
            Log.log.error("Failed to connect to graphdb");
        }

        GraphManager.configNeo = configNeo;

        return result;
    }

    public void stopGraphDB()
    {
        try
        {
            if (graphDB != null)
            {
                Log.log.info("Shutdown graphdb");
                graphDB.shutdown();
                graphDB = null;
            }
        }
        catch(Throwable t)
        {
            Log.log.info(t, t);
        }
    }

    public static void initAllCatalogFactory()
    {
        CatalogFactory.init(CatalogContainerType.Catalog, Direction.OUTGOING);
    }

    public static void initAllTagFactory()
    {
        ResolveGraphFactory.init(ResolveRelationshipType.ResolveTag, Direction.OUTGOING);
//        ResolveGraphFactory.init(TagContainerType.HashTag, Direction.OUTGOING);
    }

    public static void initAllSocialFactory()
    {
        Log.log.info("--- start init all social factory.");

        Log.log.info("--- init Users Factory");
        SocialUsersFactory.init();

        Log.log.info("--- init Process Factory");
        SocialProcessFactory.init();

        Log.log.info("--- init Team Factory");
        SocialTeamFactory.init();

        Log.log.info("--- init Forum Factory");
        SocialForumFactory.init();

        Log.log.info("--- init Task Factory");
        SocialActionTaskFactory.init();

        Log.log.info("--- init WorkSheet Factory");
        SocialWorkSheetFactory.init();

        Log.log.info("--- init NameSpace Factory");
        SocialNameSpaceFactory.init();

//        Log.log.info("--- init Runbook Factory");
//        SocialRunbookFactory.init();

//        Log.log.info("--- init DecisionTree Factory");
//        SocialDecisionTreeFactory.init();

        Log.log.info("--- init Document Factory");
        SocialDocumentFactory.init();

        Log.log.info("--- init Rss Factory");
        SocialRssFactory.init();

        Log.log.info("--- init Post Factory");
        SocialPostFactory.init();

        Log.log.info("--- init Notification Factory");
        SocialNotificationFactory.init();

        Log.log.info("--- init GlobalNotification Factory");
        SocialGlobalNotificationFactory.init();

        Log.log.info("--- init DeletePost Factory");
        SocialDeletePostFactory.init();

        Log.log.info("--- init Delete Runbook/DecisionTree/Document/ActionTask Factory");
        SocialDeleteCompFactory.init();

        Log.log.info("--- end of init all social factory.");
    }

    public Thread startCreateGraphDBThread(final Properties config) throws Exception
    {
        Thread createGraphDBThread = new Thread(new Runnable()
        {
            public void run()
            {
                try
                {
                    chachedException = false;
                    //graphDB = new HighlyAvailableGraphDatabase(RSContext.getWebHome()+"WEB-INF/graphdb", config.toMapString());
                    graphDB = new HighlyAvailableGraphDatabaseFactory().
                                    newHighlyAvailableDatabaseBuilder(RSContext.getWebHome()+"WEB-INF/graphdb").
                                    setConfig(config.toMapString()).
//                                    setConfig( GraphDatabaseSettings.node_auto_indexing, "true" ).
//                                    setConfig( GraphDatabaseSettings.relationship_auto_indexing, "true" ).
//                                    setConfig( GraphDatabaseSettings.node_keys_indexable, RSComponent.SYS_ID + ", " + RSComponent.NODE_TYPE + ", " + Post.TITLE_URL).
                                    newGraphDatabase();

                    System.out.println("---- graphDB started --");

                }
                catch(Throwable t)
                {
                    Log.log.error("Failed to startCreateGraphDBThread " + t, t);
                    chachedException = true;
                }
            }

          });

        createGraphDBThread.start();

        return createGraphDBThread;
    }

    public static void deleteTempCopyDir()
    {
        File dir = null;

        try
        {
            String dirString = RSContext.getWebHome()+"WEB-INF/graphdb/temp-copy";

            Log.log.info("try to delete directory of: " + dirString);

            if(StringUtils.isNotEmpty(dirString))
            {
                dir = FileUtils.getFile(dirString);
                deleteDirectory(dir);
            }
        }
        catch(Throwable t)
        {
            Log.log.info("Delete graphdb temp-copy folder catched exception: " + t, t);
        }
    }

    public static void deleteDirectory(File path)
    {
        if(path != null && path.exists())
        {
            File[] files = path.listFiles();

            if(files != null)
            {
                for(File f: files)
                {
                    if(f.isDirectory())
                    {
                        deleteDirectory(f);
                    }
                    else
                    {
                        f.delete();
                    }
                }
            }
        }

        path.delete();
    }

//    @SuppressWarnings("deprecation")
//    public static void checkGraphSize()
//    {
//        org.neo4j.graphdb.Transaction tx = graphDB.beginTx();
//
//        try
//        {
//            int count = 0;
//            for ( org.neo4j.graphdb.Node node : graphDB.getAllNodes())
//            {
//                count++;
//            }
//            tx.success();
//
//            System.out.println("graphdb node count: "+count);
//            Log.log.trace("graphdb node count: "+count);
//        }
//        catch (Throwable t)
//        {
//            t.printStackTrace();
//        }
//        finally
//        {
//            tx.finish();
//        }
//    } // checkGraphSize

    public GraphDatabaseService getGraphDB()
    {
        return graphDB;
    }

    public static void resetAllSocialFactory()
    {
        Log.log.info("--- start reset all social factory.");

        Log.log.info("--- reset Users Factory");
        SocialUsersFactory.reset();

        Log.log.info("--- reset Process Factory");
        SocialProcessFactory.reset();

        Log.log.info("--- reset Team Factory");
        SocialTeamFactory.reset();

        Log.log.info("--- reset Forum Factory");
        SocialForumFactory.reset();

        Log.log.info("--- reset Task Factory");
        SocialActionTaskFactory.reset();

        Log.log.info("--- reset WorkSheet Factory");
        SocialWorkSheetFactory.reset();

        Log.log.info("--- reset NameSpace Factory");
        SocialNameSpaceFactory.reset();

//        Log.log.info("--- init Runbook Factory");
//        SocialRunbookFactory.init();

//        Log.log.info("--- init DecisionTree Factory");
//        SocialDecisionTreeFactory.init();

        Log.log.info("--- reset Document Factory");
        SocialDocumentFactory.reset();

        Log.log.info("--- reset Rss Factory");
        SocialRssFactory.reset();

        Log.log.info("--- reset Post Factory");
        SocialPostFactory.reset();

        Log.log.info("--- reset Notification Factory");
        SocialNotificationFactory.reset();

        Log.log.info("--- reset GlobalNotification Factory");
        SocialGlobalNotificationFactory.reset();

        Log.log.info("--- reset DeletePost Factory");
        SocialDeletePostFactory.reset();

        Log.log.info("--- reset Delete Runbook/DecisionTree/Document/ActionTask Factory");
        SocialDeleteCompFactory.reset();

        Log.log.info("--- end of init all social factory.");
    }

    public static void resetAllCatalogFactory()
    {
//        CatalogFactory.reset();
    }

    public static void resetAllTagFactory()
    {
        ResolveGraphFactory.reset();
//        ResolveGraphFactory.init(TagContainerType.HashTag, Direction.OUTGOING);
    }
}
