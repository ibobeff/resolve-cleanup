/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.immport;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.services.vo.social.SocialForumDTO;

public class ImportForumGraph  extends ImportComponentGraph
{
    private Forum forum = null;
    
    public ImportForumGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        SocialForumDTO forumDTO = ServiceHibernate.getForumByName(rel.getSourceName(), username);
        if(forumDTO != null)
        {
            Node forumNode = ResolveGraphFactory.findNodeByIndexedID(forumDTO.getSys_id());
            if(forumNode != null)
            {
                forum = new Forum();
//                forum.convertNodeToObject(forumNode);

                switch(rel.getTargetType())
                {
                    case USER: 
                        addUserToForum();
                        break;
                        
                    default:
                        break;
                }
            }
            else
            {
                throw new Exception("Forum " + forumDTO.getU_display_name() + " is not sync with graph db." );
            } 
        }
    }
    
    private void addUserToForum() throws Exception
    {
        String usernameTobeAdded = rel.getTargetName();
        User user = SocialCompConversionUtil.getSocialUser(usernameTobeAdded);
        if(user != null)
        {
//            Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            if(userNode != null)
//            {
//                //add user to this forum
//                ServiceSocial.addUserToForum(user, forum);
//            }
//            else
//            {
//                throw new Exception("User " + user.getUsername() + " is not sync with graph db." );
//            } 
        }
    }
    
    

}
