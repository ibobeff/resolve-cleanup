/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;


public class DeleteCatalog
{
//    public static void deleteCatalog(String catalogSysID) throws Exception
//    {
//        if (StringUtils.isNotEmpty(catalogSysID))
//        {
//            Node catalogRootNode = CatalogFactory.findNodeByIndexedID(catalogSysID);
//            if (catalogRootNode != null)
//            {
//                String rootSysId = (String) catalogRootNode.getProperty(ResolveCatalog.SYS_ID);
//                boolean isRoot = (Boolean) catalogRootNode.getProperty(ResolveCatalog.IS_ROOT);
//                if (isRoot)
//                {
//                    Set<String> nodeIds = new HashSet<String>();
//                    nodeIds.add(rootSysId);
//                    
//                    Iterable<Node> catalogNodes = TraversalUtil.findCatalogNodes(catalogRootNode);
//                    if (catalogNodes != null)
//                    {
//                        //first get the nodes but don't delete as once the Relationships are deleted, it throws NoSuchNodeFound exception
//                        for (Node node : catalogNodes)
//                        {
//                            if (node.hasProperty(ResolveCatalog.TYPE))
//                            {
//                                String type = (String) node.getProperty(ResolveCatalog.TYPE);
//                                String sysId = (String) node.getProperty(ResolveCatalog.SYS_ID);
//                                nodeIds.add(sysId);
//                                
//                                Log.log.trace("** Type :" + type);
//                            }
//                        }//end of for
//                        
//                        //now delete the nodes with the sysIds
//                        for(String sysId : nodeIds)
//                        {
//                            Node node = CatalogFactory.findNodeByIndexedID(sysId);
//                            if(node != null)
//                            {
//                                ResolveGraphFactory.deleteNode(node);
//                            }
//                        }
//                    }
//                }
//                else
//                {
//                    throw new Exception("Cannot delete this catalog as this is NOT A ROOT.");
//                }
//            }
//        }//end of if
//    }//end of deleteCatalog
}
