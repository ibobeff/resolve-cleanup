/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.export;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.GraphRelationshipDTO;
import com.resolve.services.vo.ImpexDTO;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportGraph
{
    public static final String IMPEX_GRAPH_EXPORT_FILENAME = "graph_relationships.json";
    
    private SocialImpexVO socialImpex = null;
    
    //collection of relationships that will be exported
    private List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
    
    
    public ExportGraph(SocialImpexVO socialImpex, String username) throws Exception
    {
        if(socialImpex == null || StringUtils.isEmpty(username))
        {
            throw new Exception("impex obj and username are mandatory");
        }
        
        if(StringUtils.isBlank(socialImpex.getFolderLocation()))
        {
            throw new Exception("Location to be exported is mandatory");
        }
        
        this.socialImpex = socialImpex;
    }
    
    public SocialImpexVO export()  throws Exception
    {
        //prepare the json
        exportProcesses();
        exportTeams();
        exportForums();
        
        exportDocuments();
        exportActiontasks();
        
        exportCatalogs();
        
        //sort the data
        Collections.sort(relationships);
        
        //persist the json
        exportRelationshipJson();
        
        return this.socialImpex;
    }
    
    //private apis
    private void exportProcesses()  throws Exception
    {
        if(this.socialImpex.getProcesses() != null)
        {
            Iterator<String> it = this.socialImpex.getProcesses().keySet().iterator();
            while(it.hasNext())
            {
                String sysId = it.next();
                ImpexOptionsDTO options = this.socialImpex.getProcesses().get(sysId);
                
                try
                {
                    ExportProcessGraph process = new ExportProcessGraph(sysId, options);
                    relationships.addAll(process.export(socialImpex));
                }
                catch (Exception e)
                {
                    Log.log.error("Error exporting process with sysId " + sysId, e);
                }
            }//end of while
        }//end of if
    }
    
    private void exportTeams() throws Exception
    {
        if(this.socialImpex.getTeams() != null)
        {
            Iterator<String> it = this.socialImpex.getTeams().keySet().iterator();
            while(it.hasNext())
            {
                String sysId = it.next();
                ImpexOptionsDTO options = this.socialImpex.getTeams().get(sysId);
                
                try
                {
                    boolean recurse = options.getTeamTeams() != null ? options.getTeamTeams() : false;
                    ExportTeamGraph team = new ExportTeamGraph(sysId, options, recurse);
                    relationships.addAll(team.export(socialImpex));
                }
                catch (Exception e)
                {
                    Log.log.error("Error exporting process with sysId " + sysId, e);
                }
            }//end of while
        }//end of if
    }
    
    private void exportForums() throws Exception
    {
        if(this.socialImpex.getForums() != null)
        {
            Iterator<String> it = this.socialImpex.getForums().keySet().iterator();
            while(it.hasNext())
            {
                String sysId = it.next();
                ImpexOptionsDTO options = this.socialImpex.getForums().get(sysId);
                
                try
                {
                    ExportForumGraph forum = new ExportForumGraph(sysId, options);
                    relationships.addAll(forum.export(socialImpex));
                }
                catch (Exception e)
                {
                    Log.log.error("Error exporting forum with sysId " + sysId, e);
                }
            }//end of while
        }//end of if
    }
    
    private void exportDocuments() throws Exception
    {
        if(this.socialImpex.getDocuments() != null)
        {
            Iterator<String> it = this.socialImpex.getDocuments().keySet().iterator();
            while(it.hasNext())
            {
                String sysId = it.next();
                ImpexOptionsDTO options = this.socialImpex.getDocuments().get(sysId);
                
                try
                {
                    ExportDocumentGraph document = new ExportDocumentGraph(sysId, options);
                    relationships.addAll(document.export(socialImpex));
                }
                catch (Exception e)
                {
                    Log.log.error("Error exporting document with sysId " + sysId, e);
                }
            }//end of while
            
        }
        
    }

    private void exportActiontasks() throws Exception
    {
        if(this.socialImpex.getActiontasks() != null)
        {
            Iterator<String> it = this.socialImpex.getActiontasks().keySet().iterator();
            while(it.hasNext())
            {
                String sysId = it.next();
                ImpexOptionsDTO options = this.socialImpex.getActiontasks().get(sysId);
                
                try
                {
                    ExportActiontaskGraph actiontask = new ExportActiontaskGraph(sysId, options);
                    relationships.addAll(actiontask.export(socialImpex));
                }
                catch (Exception e)
                {
                    Log.log.error("Error exporting actiontask with sysId " + sysId, e);
                }
            }//end of while
            
        }
    }
    
    
    private void exportCatalogs() throws Exception
    {
        if(this.socialImpex.getCatalogs() != null)
        {
            Iterator<String> it = this.socialImpex.getCatalogs().keySet().iterator();
            while(it.hasNext())
            {
                String sysId = it.next();
                ImpexOptionsDTO options = this.socialImpex.getCatalogs().get(sysId);
                
                try
                {
                    ExportCatalogGraph catalog = new ExportCatalogGraph(sysId, options);
                    relationships.addAll(catalog.export(socialImpex));
                }
                catch (Exception e)
                {
                    Log.log.error("Error exporting catalog with sysId " + sysId, e);
                }
            }//end of while
        }
    }
    
    private void exportRelationshipJson()  throws Exception
    {
        FileOutputStream fos = null;
        try
        {
            ImpexDTO obj = new ImpexDTO();
            obj.setRelationships(relationships);
            
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);

            String json = objectMapper.writer().writeValueAsString(obj);
//            JsonNode tree = objectMapper.readTree(json);
//            String formattedJson = objectMapper.writeValueAsString(tree);
            
            File dir = FileUtils.getFile(socialImpex.getFolderLocation());
            if (!dir.exists())
            {
                dir.mkdirs();
            }
            
            File file = FileUtils.getFile(dir, IMPEX_GRAPH_EXPORT_FILENAME);
            fos = new FileOutputStream(file);
            fos.write(json.getBytes());
            fos.flush();
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    fos.close();
                }
                catch (Exception e)
                {
                }
            }
        }
        
    }

    
}
