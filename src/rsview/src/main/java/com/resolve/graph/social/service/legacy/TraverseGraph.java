/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.StopEvaluator;
import org.neo4j.graphdb.Traverser;
import org.neo4j.graphdb.Traverser.Order;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;


public class TraverseGraph
{
//    public static boolean findProcess(Collection<Process> processes, Process process)
//    {
//        boolean existed = false;
//        
//        for(Process processLocal: processes)
//        {
////            if(processLocal.getSys_id().equals(process.getSys_id()))
////            {
////                existed = true;
////            }
//        }
//        
//        return existed;
//    }
//    
//    public static Process getProcessForActionTaskTab(Node processNode)
//    {
//        Process process = new Process();
//        
//        Map<String, String> actionTasksMap = new ConcurrentHashMap<String, String>();        
//        Collection<ActionTask> actionTasks = new ArrayList<ActionTask>();
//        
//        if(processNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                        .breadthFirst()
//                                                        .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                        .evaluator(Evaluators.excludeStartPosition())
//                                                        .evaluator(Evaluators.atDepth(1));
//                            
//            
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {    
//                
////                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.ACTIONTASK.name()))
////                {
////                    if(!actionTasksMap.containsKey(Long.toString(nodeLocal.getId())))
////                    {
////                        ActionTask actionTask = new ActionTask();
////                        actionTask.convertNodeToObject(nodeLocal);
////                        actionTasks.add(actionTask);
////                        actionTasksMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
////                    }
////                }
//            }
//        }
//        
////        process.convertNodeToObject(processNode);
////        process.setActiontasks(actionTasks);
//    
//        return process;
//    }
//    
//    public static Process getProcessForRunbookTab(Node processNode)
//    {
//        Process process = new Process();
//        
//        Map<String, String> runbooksMap = new ConcurrentHashMap<String, String>();        
//        Collection<Runbook> runbooks = new ArrayList<Runbook>();
//        
//        if(processNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {    
//         
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RUNBOOK.name()) )
//                {
//                    if(!runbooksMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        Runbook runbook = new Runbook();
//                        runbook.convertNodeToObject(nodeLocal);
//                        runbooks.add(runbook);
//                        runbooksMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//            }
//        }
//        
//        process.convertNodeToObject(processNode);
//        process.setRunbooks(runbooks);
//    
//        return process;
//    }
//    
//    
//    public static Process getProcessForDecisionTreeTab(Node processNode)
//    {
//        Process process = new Process();
//        
//        Map<String, String> dtMap = new ConcurrentHashMap<String, String>();        
//        Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//        
//        if(processNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {    
//         
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DECISIONTREE.name()) )
//                {
//                    if(!dtMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        DecisionTree dtLocal = new DecisionTree();
//                        dtLocal.convertNodeToObject(nodeLocal);
//                        dts.add(dtLocal);
//                        dtMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//            }
//        }
//        
//        process.convertNodeToObject(processNode);
//        process.setDecisionTrees(dts);
//    
//        return process;
//    }
//    
//   
//    public static Process getProcessForDocumentTab(Node processNode)
//    {
//        Process process = new Process();
//        
//        Map<String, String> documentsMap = new ConcurrentHashMap<String, String>();        
//        Collection<Document> documents = new ArrayList<Document>();
//        
//        if(processNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//                                    
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {    
//         
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DOCUMENT.name()) ) 
//                {
//                    if(!documentsMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        Document document = new Document();
//                        document.convertNodeToObject(nodeLocal);
//                        documents.add(document);
//                        documentsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//            }
//        }
//        
//        process.convertNodeToObject(processNode);
//        process.setDocuments(documents);
//    
//        return process;
//    }
//    
//    public static Process getProcessForRssTab(Node processNode)
//    {
//        Process process = new Process();
//        
//        Map<String, String> rssMap = new ConcurrentHashMap<String, String>();        
//        Collection<Rss> rsss = new ArrayList<Rss>();
//        
//        if(processNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {    
//       
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RSS.name()) )
//                {
//                    if(!rssMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        Rss rss = new Rss();
//                        rss.convertNodeToObject(nodeLocal);
//                        rsss.add(rss);
//                        rssMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//            }
//        }
//        
//        process.convertNodeToObject(processNode);
//        process.setRsss(rsss);
//    
//        return process;
//    }
//    
//    public static Process getProcessForForumTab(Node processNode)
//    {
//        Process process = new Process();
//        
//        Map<String, String> forumsMap = new ConcurrentHashMap<String, String>();        
//        Collection<Forum> forums = new ArrayList<Forum>();
//        
//        if(processNode != null)
//        {   
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {    
//            
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()) ) 
//                {
//                    if(!forumsMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        Forum forum = new Forum();
//                        forum.convertNodeToObject(nodeLocal);
//                        forums.add(forum);
//                        forumsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//            }
//        }
//        
//        process.convertNodeToObject(processNode);
//        process.setForums(forums);
//    
//        return process;
//    }
//    
//    public static Process getProcessForNameSpaceTab(Node processNode)
//    {
//        Process process = new Process();
//        
//        Map<String, String> resultMap = new ConcurrentHashMap<String, String>();        
//        Collection<Namespace> namespaces = new ArrayList<Namespace>();
//        
//        if(processNode != null)
//        {   
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {    
//            
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.NAMESPACE.name()) ) 
//                {
//                    if(!resultMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        Namespace namespace = new Namespace();
//                        namespace.convertNodeToObject(nodeLocal);
//                        namespaces.add(namespace);
//                        resultMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//            }
//        }
//        
//        process.convertNodeToObject(processNode);
//        process.setNameSpaces(namespaces);
//    
//        return process;
//    }
//    
////    public static Process getProcessForTeamTab(Node processNode)
////    {
////        Process process = new Process();
////        
////        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();        
////        Collection<Team> teams = new ArrayList<Team>();
////        
////        if(processNode != null)
////        {
////            Traverser trav = processNode.traverse(Order.BREADTH_FIRST, 
////                                    StopEvaluator.END_OF_GRAPH, 
////                                    ReturnableEvaluator.ALL_BUT_START_NODE,
////                                    RelationType.MEMBER,
////                                    Direction.INCOMING);
////            
////            for(Node nodeLocal: trav)
////            {    
////                int depth = trav.currentPosition().depth();
////                
////                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.TEAM.name()) && depth == 1)
////                {
////                    Collection<Node> teamRootNodes = TraverseGraph.getRootNodes(nodeLocal);
////                    
////                    if(teamRootNodes != null && !teamRootNodes.isEmpty())
////                    {
////                        for(Node teamRootNode: teamRootNodes)
////                        {
////                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.PROCESS.name()))
////                            {
////                                if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
////                                {
////                                    Team team = getTeamForTeamTab(nodeLocal);
////                                    teams.add(team);
////                                    teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
////                                }
////                            }
////                        }
////                    }
////                }
////            }
////        }
////        
////        process.convertNodeToObject(processNode);
////        process.setTeams(teams);
////    
////        return process;
////    }
//    
//    public static Process getProcess(Node processNode)
//    {
//        Process process = new Process();
//        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//        
//        Collection<Team> teams = new ArrayList<Team>();
//        Collection<User> users = new ArrayList<User>();
//        Collection<Forum> forums = new ArrayList<Forum>();
//        Collection<Namespace> namespaces = new ArrayList<Namespace>();
//        Collection<Runbook> runbooks = new ArrayList<Runbook>();
//        Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//        Collection<Document> documents = new ArrayList<Document>();
//        Collection<ActionTask> actiontasks = new ArrayList<ActionTask>();
//        Collection<Rss> rsss = new ArrayList<Rss>();
//                
//        if(processNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(processNode).nodes())
//            {   
//               
//                BuildOneDepthTree.buildOneDepthTree(nodeLocal, users, actiontasks, runbooks, dts, documents, rsss, forums, namespaces, 1); 
//                
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                {
//                    Collection<Node> teamRootNodes = new ArrayList<Node>();
//                    TraverseGraph.getRootNodes(nodeLocal, teamRootNodes);
//                    
//                    if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                    {
//                        for(Node teamRootNode: teamRootNodes)
//                        {
//                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
//                                {
//                                    Team team = getTeam(nodeLocal);
//                                    teams.add(team);
//                                    
//                                    teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        
//        process.convertNodeToObject(processNode);
//        process.setTeams(teams);
//        process.setForums(forums);
//        process.setNameSpaces(namespaces);
//        process.setUsers(users);
//        process.setRunbooks(runbooks);
//        process.setDecisionTrees(dts);
//        process.setDocuments(documents);
//        process.setActiontasks(actiontasks);
//        process.setRsss(rsss);
//       
//        return process;
//    }
//    
//    //this is build from leaf to root
//    public static Team getTeamFromTargetToRoot(Node targetNode, Team targetTeamWithChild)
//    {
//        Team team = new Team();
//        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//        Map<String, String> usersMap = new ConcurrentHashMap<String, String>();
//        
//        Collection<Team> teams = new ArrayList<Team>();
//        Collection<User> users = new ArrayList<User>();
//        
//        if(targetNode != null)
//        {            
//            
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.OUTGOING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            int count = 0;
//            
//            for(Node nodeLocal: trav.traverse(targetNode).nodes())
//            {       
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()) )
//                {
//                    if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        //add child node to the parent node
//                        Team parentTeam = new Team();                        
//                       
//                        TraversalDescription travOneDepth = Traversal.description()
//                                                                .breadthFirst()
//                                                                .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                                .evaluator(Evaluators.excludeStartPosition())
//                                                                .evaluator(Evaluators.atDepth(1));
//    
//                        for(Node nodeLocalOnDepth: travOneDepth.traverse(targetNode).nodes())
//                        {       
//                       
//                            if(nodeLocalOnDepth.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()) )
//                            {
////                                if(!usersMap.containsKey(Long.toString(nodeLocalOnDepth.getId())))
////                                {
////                                    User user = new User();
////                                    user.convertNodeToObject(nodeLocalOnDepth);
////                                    users.add(user);
////                                    usersMap.put(Long.toString(nodeLocalOnDepth.getId()), Long.toString(nodeLocalOnDepth.getId()));
////                                }
//                            }            
//                        }
//                            
//                        if(targetTeamWithChild != null)
//                        {
//                            team = targetTeamWithChild;
//                        }
//                        
//                        team.convertNodeToObject(targetNode);
//                        team.setUsers(users);
//                    
//                        if(parentTeam.getTeams() == null)
//                        {
//                            parentTeam.setTeams(teams);
//                        }
//                        
//                        parentTeam.getTeams().add(team);
//                        
//                        team = getTeamFromTargetToRoot(nodeLocal, parentTeam);
//                        
//                        teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//                else if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()) ) 
//                {
//                    if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//
//                        TraversalDescription travOneDepth = Traversal.description()
//                                                                        .breadthFirst()
//                                                                        .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                                        .evaluator(Evaluators.excludeStartPosition())
//                                                                        .evaluator(Evaluators.atDepth(1));
//    
//                        for(Node nodeLocalOnDepth: travOneDepth.traverse(targetNode).nodes())
//                        {       
//                       
//                            if(nodeLocalOnDepth.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()) ) 
//                            {
//                                if(!usersMap.containsKey(Long.toString(nodeLocalOnDepth.getId())))
//                                {
//                                    User user = new User();
//                                    user.convertNodeToObject(nodeLocalOnDepth);
//                                    users.add(user);
//                                    usersMap.put(Long.toString(nodeLocalOnDepth.getId()), Long.toString(nodeLocalOnDepth.getId()));
//                                }
//                            }            
//                        }
//                            
//                        if(targetTeamWithChild != null)
//                        {
//                            team = targetTeamWithChild;
//                        }
//                        
//                        team.convertNodeToObject(targetNode);
//                        team.setUsers(users);
//                        
//                        teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//                
//                count++;
//            }
//            
//            if(count == 0)
//            {
//
//                TraversalDescription travOneDepth = Traversal.description()
//                                                            .breadthFirst()
//                                                            .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                            .evaluator(Evaluators.excludeStartPosition())
//                                                            .evaluator(Evaluators.atDepth(1));
//                
//                
//                for(Node nodeLocalOnDepth: travOneDepth.traverse(targetNode).nodes())
//                {       
//              
//                    if(nodeLocalOnDepth.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()) ) 
//                    {
//                        if(!usersMap.containsKey(Long.toString(nodeLocalOnDepth.getId())))
//                        {
//                            User user = new User();
//                            user.convertNodeToObject(nodeLocalOnDepth);
//                            users.add(user);
//                            usersMap.put(Long.toString(nodeLocalOnDepth.getId()), Long.toString(nodeLocalOnDepth.getId()));
//                        }
//                    }            
//                }
//                
//                if(targetTeamWithChild != null)
//                {
//                    team = targetTeamWithChild;
//                }
//                
//                team.convertNodeToObject(targetNode);
//                team.setUsers(users);
//            }   
//        }
//     
//        return team;
//    }
//    
//    public static Team getTeamFromRootToTarget(final Node rootNode, final Node targetNode)
//    {
//        Team team = new Team();
//        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//        Map<String, String> usersMap = new ConcurrentHashMap<String, String>();
//        
//        Collection<Team> teams = new ArrayList<Team>();
//        Collection<User> users = new ArrayList<User>();
//        final Map<String, Node> path = getPathToRoot(rootNode, targetNode);
//        
//        if(rootNode != null && targetNode != null)
//        {            
//
//            TraversalDescription trav = Traversal.description()
//                                                .breadthFirst()
//                                                .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                .evaluator(Evaluators.excludeStartPosition())
//                                                .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(rootNode).nodes())
//            {       
//           
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()) )
//                {
//                    if(!usersMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        User user = new User();
//                        user.convertNodeToObject(nodeLocal);
//                        users.add(user);
//                        usersMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//                else if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()) )
//                {
//                    if(!path.isEmpty())
//                    {
//                        Node pathNode = path.get(Integer.toString(1));
//                        
//                        if(pathNode != null && nodeLocal.equals(pathNode))
//                        {
//                            if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
//                            {
//                                Team teamLocal = getTeamFromRootToTarget(nodeLocal, targetNode);
//                                teams.add(teamLocal);
//                                teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                            }
//                        }
//                    }
//                }
//            }
//            
//            team.convertNodeToObject(rootNode);
//            team.setUsers(users);
//            team.setTeams(teams);            
//        }
//        
//        return team;
//    }
//    
//    public static void getDepthToRootOrTailMember(Node currentNode, boolean toRoot, int currentDepth, List<Integer> endDepth)
//    {
//        boolean isRoot = true;
//        
//        if(currentNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, (toRoot)?Direction.OUTGOING:Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//
//            for(Node nodeLocal: trav.traverse(currentNode).nodes())
//            {      
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                {
//                    currentDepth++;
//                    isRoot = false;
//                    getDepthToRootOrTailMember(nodeLocal, toRoot, currentDepth, endDepth);
//                }
//            }
//            
//            if(isRoot)
//            {
//                endDepth.add(currentDepth);
//            }
//        }
//        
//    }
//    
//    public static Team getTeam(Node teamNode)
//    {
//        Team team = new Team();
//        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//        Map<String, String> usersMap = new ConcurrentHashMap<String, String>();
//        
//        Collection<Team> teams = new ArrayList<Team>();
//        Collection<User> users = new ArrayList<User>();
//        
//        if(teamNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            
//            for(Node nodeLocal: trav.traverse(teamNode).nodes())
//            {       
//         
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name())) 
//                {
//                    if(!usersMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        User user = new User();
//                        user.convertNodeToObject(nodeLocal);
//                        users.add(user);
//                        usersMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//                else if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name())) 
//                {                                        
//                    if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        Team teamLocal = getTeam(nodeLocal);
//                        teams.add(teamLocal);
//                        teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }
//            }
//            
//            team.convertNodeToObject(teamNode);
//            team.setUsers(users);
//            team.setTeams(teams);
//            
//        }
//        
//        return team;
//    }
//    
////    public static Team getTeamForTeamTab(Node teamNode)
////    {
////        Team team = new Team();
////        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
////        Collection<Team> teams = new ArrayList<Team>();
////        
////        if(teamNode != null)
////        {
////            Traverser trav = teamNode.traverse(Order.BREADTH_FIRST, 
////                                        StopEvaluator.END_OF_GRAPH, 
////                                        ReturnableEvaluator.ALL_BUT_START_NODE,
////                                        RelationType.MEMBER,
////                                        Direction.INCOMING);
////            
////            for(Node nodeLocal: trav)
////            {       
////                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.TEAM.name()))
////                {
////                    if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
////                    {
////                        Team teamLocal = getTeamForTeamTab(nodeLocal);
////                        teams.add(teamLocal);
////                        teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
////                    }
////                }
////            }
////            
////            team.convertNodeToObject(teamNode);
////            team.setTeams(teams);
////            
////        }
////        
////        return team;
////        
////    }
//    
////    public static Node getRootNode(Node startNode)
////    {
////        Node rootNode = null;
////        
////        if(startNode != null)
////        {
////            //find the start node
////            Traverser trav = startNode.traverse(Order.BREADTH_FIRST, 
////                                StopEvaluator.END_OF_GRAPH, 
////                                ReturnableEvaluator.ALL_BUT_START_NODE,
////                                RelationType.MEMBER,
////                                Direction.OUTGOING);
////            
////            for(Node nodeLocal: trav)
////            {
////                int depth = trav.currentPosition().depth();
////                
////                rootNode = nodeLocal;
////                
////                String rootNodeID = (String) rootNode.getProperty("sys_id");
////                
////                String end = "";
////                
////            }
////            
////            rootNode = (rootNode == null)?startNode:rootNode;           
////        }
////        
////        return rootNode;
////    }
//    
//    public static void getRootNodesOfForum(Node startNode, Collection<Node> rootNodes)
//    {
//        if(startNode != null)
//        {
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.OUTGOING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//           
//            for(Node nodeLocal: trav.traverse(startNode).nodes())
//            {
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                {
//                    rootNodes.add(nodeLocal);
//                }
//            }
//            
//        }
//    }
//    
//    public static void getRootNodes(Node startNode, Collection<Node> rootNodes)
//    {
//        boolean isRoot = true;
//        
//        if(startNode != null)
//        {
//
//           TraversalDescription trav = Traversal.description()
//                                       .breadthFirst()
//                                       .relationships(RelationType.MEMBER, Direction.OUTGOING)
//                                       .evaluator(Evaluators.excludeStartPosition() )
//                                       .evaluator(Evaluators.toDepth(1));
//
//           
//            for(Node nodeLocal: trav.traverse(startNode).nodes())
//            {
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name())
//                      || nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                {
//                    isRoot = false;
//                    getRootNodes(nodeLocal, rootNodes);
//                }
//            }
//            
//            if(isRoot)
//            {
//                rootNodes.add(startNode);
//            }
//        }
//    }
//
////    public static Collection<Node> getRootNodes(Node startNode)
////    {
////        Map<String, Node> rootNodeMap = new HashMap<String, Node>();
////        int rootDepth = -1;
////        
////        if(startNode != null)
////        {
////            String startNodeID = (String) startNode.getProperty("sys_id");
////            
////            Set<String> parentIDS = SocialConfig.getParentsOfTeam(startNodeID);
////            
////            rootDepth = getRootDepth(startNode);
////            
////            if(rootDepth != -1)
////            {
////                Traverser trav = startNode.traverse(Order.BREADTH_FIRST, 
////                                                    StopEvaluator.END_OF_GRAPH, 
////                                                    ReturnableEvaluator.ALL_BUT_START_NODE,
////                                                    RelationType.MEMBER,
////                                                    Direction.OUTGOING);
////                
////                for(Node nodeLocal: trav)
////                {
////                    int depth = trav.currentPosition().depth();
////                   
////                    if(depth == rootDepth)
////                    {
////                        if(!rootNodeMap.containsKey(Long.toString(nodeLocal.getId())))
////                        {
////                            rootNodeMap.put(Long.toString(nodeLocal.getId()), nodeLocal);
////                        }
////                    }
////                }  
////            }
////        }
////        
////        return rootNodeMap.values();
////        
////    }
//    
////    public static int getRootDepth(Node startNode)
////    {
////        int rootDepth = -1;
////        
////        if(startNode != null)
////        {
////            Traverser trav = startNode.traverse(Order.BREADTH_FIRST, 
////                                StopEvaluator.END_OF_GRAPH, 
////                                ReturnableEvaluator.ALL_BUT_START_NODE,
////                                RelationType.MEMBER,
////                                Direction.OUTGOING);
////            
////            for(Node nodeLocal: trav)
////            {    
////                rootDepth = trav.currentPosition().depth();
////            }
////        }
////        
////        return rootDepth;
////    }
//    
//    public static Set<String> getToRootPathTeams(Node startNode)
//    {
//        Set<String> postPathTeams = new HashSet<String>();
//        
//        if(startNode != null)
//        {
//            postPathTeams.add((String) startNode.getProperty(RSComponent.SYS_ID));
//            
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.OUTGOING)
//                                                    .evaluator(Evaluators.excludeStartPosition());
//            
//            
//            for(Node nodeLocal: trav.traverse(startNode).nodes())
//            {
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                {
//                    postPathTeams.add((String) nodeLocal.getProperty(RSComponent.SYS_ID));
//                }  
//            }
//        }
//        
//        return postPathTeams;
//    }
//    
//    public static Map<String, Node> getPathToRoot(Node rootNode, Node targetNode)
//    {
//        int k= 0;
//        Map<String, Node> pathMap = new ConcurrentHashMap<String, Node>();
//        Map<String, Node> path = new ConcurrentHashMap<String, Node>();
//        
//        if(targetNode != null)
//        {
//            //find the start node
//            Traverser trav = targetNode.traverse(Order.BREADTH_FIRST, 
//                                StopEvaluator.END_OF_GRAPH, 
//                                ReturnableEvaluator.ALL_BUT_START_NODE,
//                                RelationType.MEMBER,
//                                Direction.OUTGOING);
//            
//            for(Node nodeLocal: trav)
//            {
//                int depth = trav.currentPosition().depth();
//                
//                if(!nodeLocal.equals(rootNode))
//                {
//                    String nodeLocalName = (String) nodeLocal.getProperty(RSComponent.DISPLAYNAME);
//                    path.put(Integer.toString(depth), nodeLocal);
//                }
//                else
//                {
//                    path.put(Integer.toString(depth), nodeLocal);
//                    break;
//                }
//            }
//            
//            if(!path.isEmpty())
//            {
//                Object[] keySetArray = path.keySet().toArray();
//                k= 0;
//                
//                if(keySetArray != null && keySetArray.length > 0)
//                {
//                    for(int i = keySetArray.length; i >=1; i--)
//                    {
//                        pathMap.put(Integer.toString(k), path.get((String)keySetArray[i-1]));
//                        k++;
//                    }
//                }
//            }
//        }
//        
//        pathMap.put(Integer.toString(k), targetNode);
//        
//        return pathMap;
//    }
//    
//    public static Forum getForum(Node forumNode)
//    {
//        Forum forum = new Forum();
//        Map<String, String> usersMap = new ConcurrentHashMap<String, String>();
//        Collection<User> users = new ArrayList<User>();
//       
//        if(forumNode != null)
//        {
//            
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                    .evaluator(Evaluators.excludeStartPosition())
//                                                    .evaluator(Evaluators.atDepth(1));
//            
//            for(Node nodeLocal: trav.traverse(forumNode).nodes())
//            {         
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()))
//                {
//                    if(!usersMap.containsKey(Long.toString(nodeLocal.getId())))
//                    {
//                        User user = new User();
//                        user.convertNodeToObject(nodeLocal);
//                        users.add(user);
//                        usersMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                    }
//                }                
//            }
//            
//            forum.convertNodeToObject(forumNode);
//            forum.setUsers(users);
//        }
//        
//        return forum;
//    }
//    
//    public static void buildProcessesForTab(Iterable<Relationship> memberRels, Collection<Process> processes)
//    {
//        for(Relationship member: memberRels)
//        {
//            Node endNode = member.getEndNode();
//                     
//            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                Process process = TraverseGraph.getProcess(endNode);
//                if(!TraverseGraph.findProcess(processes, process))
//                {
//                    processes.add(process);
//                }
//            }
//        }
//    }
//    
//    public static void buildProcessesForActionTaskTab(Iterable<Relationship> memberRels, Collection<Process> processes)
//    {
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        
//        for(Relationship member: memberRels)
//        {
//            Node endNode = member.getEndNode();
//                     
//            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                {
//                    Process process = TraverseGraph.getProcessForActionTaskTab(endNode); 
//                    if(!TraverseGraph.findProcess(processes, process))
//                    {
//                        processes.add(process);
//                    }
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//            {
//                Collection<Node> teamRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodes(endNode, teamRootNodes);
//                
//                if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                {
//                    for(Node teamRootNode: teamRootNodes)
//                    {
//                        if(teamRootNode != null)
//                        {
//                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForActionTaskTab(teamRootNode);
//                                    processes.add(process);
//                                    processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//            {
//                Collection<Node> forumRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//                
//                if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                {
//                    for(Node forumRootNode: forumRootNodes)
//                    {
//                        if(forumRootNode != null)
//                        {
//                            if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForActionTaskTab(forumRootNode);
//                                    processes.add(process);
//                                    processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    public static void buildProcessesForRunbookTab(Iterable<Relationship> memberRels, Collection<Process> processes)
//    {
//        Set<String> processIDSet = new HashSet<String>();
//        
//        for(Relationship member: memberRels)
//        {
//            Node endNode = member.getEndNode();
//             
//            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                if(!processIDSet.contains(Long.toString(endNode.getId())))
//                {
//                    Process process = TraverseGraph.getProcessForRunbookTab(endNode);
//                    processes.add(process);
//                    processIDSet.add(Long.toString(endNode.getId()));
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//            {
//                Collection<Node> teamRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodes(endNode, teamRootNodes);
//                
//                if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                {
//                    for(Node teamRootNode: teamRootNodes)
//                    {
//                        if(teamRootNode != null)
//                        {
//                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processIDSet.contains(Long.toString(teamRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForRunbookTab(teamRootNode);
//                                    processes.add(process);
//                                    processIDSet.add(Long.toString(teamRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//            {
//                Collection<Node> forumRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//                
//                if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                {
//                    for(Node forumRootNode: forumRootNodes)
//                    {
//                        if(forumRootNode != null)
//                        {
//                            if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processIDSet.contains(Long.toString(forumRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForRunbookTab(forumRootNode);
//                                    processes.add(process);
//                                    processIDSet.add(Long.toString(forumRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    public static void buildProcessesForDecisionTreeTab(Iterable<Relationship> memberRels, Collection<Process> processes)
//    {
//        Set<String> processIDSet = new HashSet<String>();
//        
//        for(Relationship member: memberRels)
//        {
//            Node endNode = member.getEndNode();
//             
//            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                if(!processIDSet.contains(Long.toString(endNode.getId())))
//                {
//                    Process process = TraverseGraph.getProcessForDecisionTreeTab(endNode);
//                    processes.add(process);
//                    processIDSet.add(Long.toString(endNode.getId()));
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//            {
//                Collection<Node> teamRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodes(endNode, teamRootNodes);
//                
//                if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                {
//                    for(Node teamRootNode: teamRootNodes)
//                    {
//                        if(teamRootNode != null)
//                        {
//                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processIDSet.contains(Long.toString(teamRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForDecisionTreeTab(teamRootNode);
//                                    processes.add(process);
//                                    processIDSet.add(Long.toString(teamRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//            {
//                Collection<Node> forumRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//                
//                if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                {
//                    for(Node forumRootNode: forumRootNodes)
//                    {
//                        if(forumRootNode != null)
//                        {
//                            if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processIDSet.contains(Long.toString(forumRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForDecisionTreeTab(forumRootNode);
//                                    processes.add(process);
//                                    processIDSet.add(Long.toString(forumRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    public static void buildProcessesForDocumentTab(Iterable<Relationship> memberRels, Collection<Process> processes)
//    {
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        
//        for(Relationship member: memberRels)
//        {
//            Node endNode = member.getEndNode();
//                     
//            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                {
//                    Process process = TraverseGraph.getProcessForDocumentTab(endNode);
//                    processes.add(process);
//                    processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//            {
//                Collection<Node> teamRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodes(endNode, teamRootNodes);
//                
//                if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                {
//                    for(Node teamRootNode: teamRootNodes)
//                    {
//                        if(teamRootNode != null)
//                        {
//                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForDocumentTab(teamRootNode);
//                                    processes.add(process);
//                                    processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//            {
//                Collection<Node> forumRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//                
//                if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                {
//                    for(Node forumRootNode: forumRootNodes)
//                    {
//                        if(forumRootNode != null)
//                        {
//                            if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForDocumentTab(forumRootNode);
//                                    processes.add(process);
//                                    processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    public static void buildProcessesForRssTab(Iterable<Relationship> memberRels, Collection<Process> processes)
//    {
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        
//        if(memberRels != null)
//        {
//            for(Relationship member: memberRels)
//            {
//                Node endNode = member.getEndNode();
//               
//                if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                {
//                    if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                    {
//                        Process process = TraverseGraph.getProcessForRssTab(endNode);
//                        processes.add(process);
//                        processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                    }
//                }
//                else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                {
//                    Collection<Node> teamRootNodes = new ArrayList<Node>();
//                    TraverseGraph.getRootNodes(endNode, teamRootNodes);
//                    
//                    if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                    {
//                        for(Node teamRootNode: teamRootNodes)
//                        {
//                            if(teamRootNode != null)
//                            {
//                                if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                {
//                                    if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                    {
//                                        Process process = TraverseGraph.getProcessForRssTab(teamRootNode);
//                                        processes.add(process);
//                                        processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//                {
//                    Collection<Node> forumRootNodes = new ArrayList<Node>();
//                    TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//                    
//                    if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                    {
//                        for(Node forumRootNode: forumRootNodes)
//                        {
//                            if(forumRootNode != null)
//                            {
//                                if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                                {
//                                    if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                                    {
//                                        Process process = TraverseGraph.getProcessForRssTab(forumRootNode);
//                                        processes.add(process);
//                                        processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    public static void buildProcessesForForumTab(Iterable<Relationship> memberRels, Collection<Process> processes, 
//                                                 Collection<Forum> forums, Set<String> forumIDSet)
//    {
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        
//        for(Relationship member: memberRels)
//        {
//            Node endNode = member.getEndNode();
//           
//            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                {
//                    Process process = TraverseGraph.getProcessForForumTab(endNode);
//                    processes.add(process);
//                    processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//            {
//                Collection<Node> teamRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodes(endNode, teamRootNodes);
//                
//                if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                {
//                    for(Node teamRootNode: teamRootNodes)
//                    {
//                        if(teamRootNode != null)
//                        {
//                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForForumTab(teamRootNode);
//                                    processes.add(process);
//                                    processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//            {
//                Collection<Node> forumRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//                
//                if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                {
//                    for(Node forumRootNode: forumRootNodes)
//                    {
//                        if(forumRootNode != null)
//                        {
//                            if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                                if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                                {
//                                    Process process = TraverseGraph.getProcessForForumTab(forumRootNode);
//                                    processes.add(process);
//                                    processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                                }
//                            }
//                        }
//                    }
//                }
//                else 
//                {
//                    Forum forum = new Forum();
//                    forum.convertNodeToObject(endNode);
//                        
//                    if(!forumIDSet.contains(forum.getSys_id()))
//                    {
//                        forums.add(forum);
//                        forumIDSet.add(forum.getSys_id());
//                    }
//                }
//            }
//        }
//    }
//    
//    public static void buildProcessesForNameSpaceTab(Iterable<Relationship> memberRels, Collection<Process> processes, 
//                                                        Collection<Namespace> namespaces, Set<String> namespaceIDSet)
//    {
//        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();
//        
//        for(Relationship member: memberRels)
//        {
//            Node endNode = member.getEndNode();
//            
//            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                if(!processesMap.containsKey(Long.toString(endNode.getId())))
//                {
//                    Process process = TraverseGraph.getProcessForNameSpaceTab(endNode);
//                    processes.add(process);
//                    processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//            {
//                Collection<Node> teamRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodes(endNode, teamRootNodes);
//                
//                if(teamRootNodes != null && !teamRootNodes.isEmpty())
//                {
//                    for(Node teamRootNode: teamRootNodes)
//                    {
//                        if(teamRootNode != null)
//                        {
//                            if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                               if(!processesMap.containsKey(Long.toString(teamRootNode.getId())))
//                               {
//                                   Process process = TraverseGraph.getProcessForNameSpaceTab(teamRootNode);
//                                   processes.add(process);
//                                   processesMap.put(Long.toString(teamRootNode.getId()), Long.toString(teamRootNode.getId()));
//                               }
//                            }
//                        }
//                    }
//                }
//            }
//            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//            {
//                Collection<Node> forumRootNodes = new ArrayList<Node>();
//                TraverseGraph.getRootNodesOfForum(endNode, forumRootNodes);
//                
//                if(forumRootNodes != null && !forumRootNodes.isEmpty())
//                {
//                    for(Node forumRootNode: forumRootNodes)
//                    {
//                        if(forumRootNode != null)
//                        {
//                            if(forumRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//                            {
//                               if(!processesMap.containsKey(Long.toString(forumRootNode.getId())))
//                               {
//                                   Process process = TraverseGraph.getProcessForNameSpaceTab(forumRootNode);
//                                   processes.add(process);
//                                   processesMap.put(Long.toString(forumRootNode.getId()), Long.toString(forumRootNode.getId()));
//                               }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }    
//    
//    
//    public static List<ComponentNotification> getAllUserCustomNotificationComps(Node userNode, ComponentNotification compNotification) 
//    {
//        List<ComponentNotification> results = new ArrayList<ComponentNotification>();
//        
//        if(compNotification != null)
//        {
//            RSComponent comp = compNotification.getRSComponent();
//            List<NotificationConfig> types = compNotification.getNotifyTypes();
//            
//            if(types != null)
//            {
//                for(NotificationConfig notifyConfig: types)
//                {
//                    UserGlobalNotificationContainerType typeLocal = notifyConfig.getType();
//                    
//                    if(userNode != null && typeLocal != null)
//                    {
////                        Iterable<Relationship> rels = userNode.getRelationships(typeLocal, Direction.OUTGOING);
////                        
////                        if(rels != null)
////                        {
////                            for(Relationship rel: rels)
////                            {
////                                Node endNode = rel.getEndNode();
////                                String notifyValue = (String) rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
////                                notifyValue = (!notifyValue.equals("true") && !notifyValue.equals("false"))?"false":notifyValue;
////                                
////                                RSComponent compLocal = BuildOneDepthTree.buildComponent(endNode);
////                                
////                                if(compLocal != null)
////                                {
////                                    ComponentNotification compNotify = new ComponentNotification();
////                                    NotificationConfig notifyConfigLocal = new NotificationConfig();
////                                    notifyConfigLocal.setType(typeLocal);
////                                    notifyConfigLocal.setValue(notifyValue);
////                                    
////                                    List<NotificationConfig> typeList = new ArrayList<NotificationConfig>();
////                                    typeList.add(notifyConfigLocal);
////                                    compNotify.setNotifyTypes(typeList);
////                                    compNotify.setRSComponent(compLocal);
////                                    
////                                    results.add(compNotify);
////                                }
////                            }
////                        }
//                    }
//                }
//            }
//        }
//        
//        return results;
//    }
//    
////    public static void buildProcesses(Iterable<Relationship> memberRels, Collection<Process> processes)
////    {
////        Map<String, String> runbooksMap = new ConcurrentHashMap<String, String>();
////        
////        for(Relationship member: memberRels)
////        {
////            Node endNode = member.getEndNode();
////                     
////            if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.PROCESS.name()))
////            {
////                if(!runbooksMap.containsKey(Long.toString(endNode.getId())))
////                {             
////                    Process process = TraverseGraph.getProcessForRunbookTab(endNode);
////                    processes.add(process);
////                    runbooksMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
////                }
////            }
////            else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.TEAM.name()))
////            {
////                Node teamRootNode = TraverseGraph.getRootNode(endNode);
////                
////                if(teamRootNode != null)
////                {
////                    if(teamRootNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.PROCESS.name()))
////                    {
////                        if(!runbooksMap.containsKey(Long.toString(endNode.getId())))
////                        { 
////                            Process process = TraverseGraph.getProcessForRunbookTab(teamRootNode);
////                            processes.add(process);
////                            runbooksMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
////                        }
////                    }
////                }
////            }
////        }
////    }
    
}
