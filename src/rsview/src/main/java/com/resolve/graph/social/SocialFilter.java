/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.Metric;
import com.resolve.util.StringUtils;


public class SocialFilter implements Filter
{
    public static final String POST_SEARCH_KEY = "searchKey";
    public static final String POST_SEARCH_TYPE = "searchType";
    public static final String POST_SEARCH_TIME = "searchTime";
    public static final String POST_SEARCH_QUERY_TYPE = "queryType";
    public static final String POST_SEARCH_RESULTS_TYPE = "resultsType";

    @Override
    public void destroy()
    {
        // TODO Auto-generated method stub
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        //Keep track of Metrics
        Metric.setStartTime(System.currentTimeMillis());
        Metric.setSocialPage(false);
        String url = Authentication.getURL((HttpServletRequest)request);
        if(StringUtils.isNotEmpty(url) && url.indexOf("%2Fsocial%2F") > -1)
        {
            Metric.setSocialPage(true);
        }

        //execute the chain 
        chain.doFilter(request, response);
        
        //log the metric for social
        if(Metric.isSocialPage())
        {
            //Keep track of social page response time metric
            Metric.setEndTime(System.currentTimeMillis() - Metric.getStartTime());
            Metric.socialResponseTime.getAndAdd(Metric.getEndTime());
            Metric.socialPageViewCount.incrementAndGet();
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException
    {
        // TODO Auto-generated method stub
    }
}
