/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.io.Serializable;


public class ResolvePost implements Serializable
{
    private static final long serialVersionUID = -4944490975346166627L;
    
    //the sys columns 
    private String sys_id;
    private String sysCreatedBy;
    private long sysCreatedOn;
    private Integer sysModCount;
    private String sysUpdatedBy;
    private long sysUpdatedOn;
    private long expiration = 0l; //Long.MAX_VALUE;
    
    //the ids of source, target, and receive
    private String displayName;
    private String title;
    private String titleurl;
    private String author;
    private String content;
    private String contentDesc;
    private String hashText;
    private String roles;
    private String editRoles;
    private String likeCount;
    private String parentID;
//    private boolean starred;
    
    private boolean locked;
    private boolean solved;
    private boolean answer;
    
    private String sourceIDs;
    private String targetID;
    private String receiverIDs;
    private String starredIDs;
    private String outBoxIDs;
    
    private String targetDisplayName;
    private String targetType;
    private String namespace;
    
    private Boolean uShouldDelete;
    private Boolean uShouldIndex;
    
    private String sysCreatedOnString;
    private String sysUpdatedOnString;
    private String expirationString;
    
    private String type;
         
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
    
    public String getDisplayName()
    {
        return this.displayName;
    }
    
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    public String getTitle()
    {
        return this.title;
    }
    
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public String getTitleUrl()
    {
        return this.titleurl;
    }
    
    public void setTitleUrl(String titleUrl)
    {
        this.titleurl = titleUrl;
    }
    
    public String getAuthor()
    {
        return this.author;
    }
    
    public void setAuthor(String author)
    {
        this.author = author;
    }
    
    public String getContent()
    {
        return this.content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public void setContentDesc(String contentDesc)
    {
        this.contentDesc = contentDesc;
    }
    
    public String getContentDesc()
    {
        return contentDesc;
    }
    
    public void setHashText(String hashText)
    {
        this.hashText = hashText;
    }
    
    public String getHashText()
    {
        return this.hashText;
    }
    
    public String getRoles()
    {
       return this.roles;
    }
    
    public void setRoles(String roles)
    {
        this.roles = roles;
    }
    
    public String getEditRoles()
    {
        return editRoles;
    }
    
    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }
    
    public String getLikeCount()
    {
        return this.likeCount;
    }
    
    public void setLikeCount(String likeCount)
    {
        this.likeCount = likeCount;
    }
    
    public void setParentID(String parentID)
    {
        this.parentID = parentID;
    }
    
    public String getParentID()
    {
        return this.parentID;
    }
    
//    public void markStarred(boolean starred)
//    {
//        this.starred = starred;
//    }
//    
//    public boolean getStarred()
//    {
//        return this.starred;
//    }
    
    public String getSourceIDs()
    {
        return this.sourceIDs;
    }
    
    public void setSourceIDs(String sourceIDs)
    {
        this.sourceIDs = sourceIDs;
    }
    
    public String getTargetID()
    {
        return this.targetID;
    }
    
    public void setTargetID(String targetID)
    {
        this.targetID = targetID;
    }
    
    public String getReceiverIDs()
    {
        return receiverIDs;
    }
    
    public void setReceiverIDs(String receiverIDs)
    {
        this.receiverIDs = receiverIDs;
    }
    
    public String getStarredIDs()
    {
        return this.starredIDs;
    }
    
    public void setStarredIDs(String starredIDs)
    {
        this.starredIDs = starredIDs;
    }
    
    public String getOutBoxIDs()
    {
        return this.outBoxIDs;
    }
    
    public void setOutBoxIDs(String outBoxIDs)
    {
        this.outBoxIDs = outBoxIDs;
    }
    
    public String getSysCreatedBy()
    {
        return this.sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    public long getSysCreatedOn()
    {
        return this.sysCreatedOn;
    }

    public void setSysCreatedOn(long sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }
    
    public long getExpiration()
    {
        return this.expiration;
    }
    
    public void setExpiration(long expiration)
    {
        this.expiration = expiration;
    }

    public Integer getSysModCount()
    {
        return this.sysModCount;
    }

    public void setSysModCount(Integer sysModCount)
    {
        this.sysModCount = sysModCount;
    }

    public String getSysUpdatedBy()
    {
        return this.sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    public long getSysUpdatedOn()
    {
        return this.sysUpdatedOn;
    }
    
    public void setSysUpdatedOn(long sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }
    
    public void setTargetDisplayName(String targetDisplayName)
    {
        this.targetDisplayName = targetDisplayName;
    }
    
    public String getTargetDisplayName()
    {
        return this.targetDisplayName;
    }
    
    public void setTargetType(String targetType)
    {
        this.targetType = targetType;
    }
    
    public String getTargetType()
    {
        return this.targetType;
    }
    
    public void setNameSpace(String namespace)
    {
        this.namespace = namespace;
    }
    
    public String getNameSpace()
    {
        return namespace;
    }

    public Boolean getUShouldDelete()
    {
            return uShouldDelete;
    }
   
    public void setUShouldDelete(Boolean uShouldDelete)
    {
        this.uShouldDelete = uShouldDelete;
    }
    
    public Boolean getUShouldIndex()
    {
            return uShouldIndex;
    }
     
    public void setUShouldIndex(Boolean uShouldIndex)
    {
        this.uShouldIndex = uShouldIndex;
    }
    
    public String getExpirationString()
    {
        return this.expirationString;
    }
    
    public void setExpiration(String expirationString)
    {
        this.expirationString = expirationString;
    }
    
    public void setSysCreatedOnString(String sysCreatedOnString)
    {
        this.sysCreatedOnString = sysCreatedOnString;
    }
    
    public String getSysCreatedOnString()
    {
        return this.sysCreatedOnString;
    }
    
    public void setSysUpdatedOnString(String sysUpdatedOnString)
    {
        this.sysUpdatedOnString = sysUpdatedOnString;
    }
    
    public String getSysUpdatedOnString()
    {
        return this.sysUpdatedOnString;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    public String getType()
    {
        return type;
    }

    public boolean isLocked()
    {
        return locked;
    }

    public void setLocked(boolean locked)
    {
        this.locked = locked;
    }

    public boolean isSolved()
    {
        return solved;
    }

    public void setSolved(boolean solved)
    {
        this.solved = solved;
    }

    public boolean isAnswer()
    {
        return answer;
    }

    public void setAnswer(boolean answer)
    {
        this.answer = answer;
    }
    
    
    
}
