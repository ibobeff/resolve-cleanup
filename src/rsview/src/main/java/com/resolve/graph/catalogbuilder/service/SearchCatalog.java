/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;


public class SearchCatalog
{
//    private QueryDTO query = null;
////    private String username = null; //Not req presently but will be required when we implement MCP
//    private int start = -1;
//    private int limit = -1;
//    
//    public SearchCatalog(QueryDTO query, String username) throws Exception
//    {
//        this.query = query;
////        this.username = username;
//        
//        if(query != null)
//        {
//            this.start = query.getStart();
//            this.limit = query.getLimit();
//        }
//    }
//    
//    public ResponseDTO<ResolveCatalog> getCatalogs() throws Exception
//    {
//        List<ResolveCatalog> catalogResult = new ArrayList<ResolveCatalog>();
//        int size = 0;
//
//        //get all the catalogs
//        catalogResult.addAll(GetCatalog.getAllCatalogs());
//        size = catalogResult.size();
//
//        
//        //check if the query obj is there for filtering
//        if(query != null)
//        {
//            //filtering
//            List<QueryFilter> filters = query.getFilters();
//            if(filters != null && filters.size() > 0)
//            {
//                for(QueryFilter filter : filters)
//                {
//                    catalogResult = applyFilter(catalogResult, filter);
//                }
//            }
//            
//            //sorting
//           List<QuerySort> sortItems = query.getSortItems();
//           if(sortItems != null && sortItems.size() > 0)
//           {
//               for(QuerySort sort : sortItems)
//               {
//                   applySorting(catalogResult, sort);
//               }
//           }
//           else
//           {
//               //default sorting
//               Collections.sort(catalogResult, new ComparatorResolveCatalogNameAsc());
//           }
//        }
//        else
//        {
//            //default sorting
//            Collections.sort(catalogResult, new ComparatorResolveCatalogNameAsc());
//        }
//        
//        //pagination
//        if(start > -1 && limit > 0)
//        {
//            catalogResult = JavaUtils.pagination(catalogResult, start, limit); 
//        }
//        
//        //prepare the response
//        ResponseDTO<ResolveCatalog> response = new ResponseDTO<ResolveCatalog>();
//        response.setRecords(catalogResult);
//        response.setTotal(size);
//        
//        return response;
//    }
//    
//    //[QueryFilter [type=auto, field=name, value=new catalog1, condition=equals, paramName=null]]
//    //[QueryFilter [type=auto, field=name, value=new catalog1, condition=equals, paramName=null], QueryFilter [type=date, field=sys_created_on, value=2014-02-25t00:00:00-08:00, condition=on, paramName=null]]
//    //[QueryFilter [type=auto, field=name, value=new catalog1, condition=equals, paramName=null], QueryFilter [type=date, field=sys_created_on, value=2014-02-25t00:00:00-08:00, condition=on, paramName=null], QueryFilter [type=auto, field=catalogType, value=tile, condition=equals, paramName=null]]
//    private List<ResolveCatalog> applyFilter(List<ResolveCatalog> catalogResult, QueryFilter filter)
//    {
//        List<ResolveCatalog> result = new ArrayList<ResolveCatalog>();
//
//        String type = filter.getType();
//        String field = filter.getField();
//        String queryValue = filter.getValue();
//        String condition = filter.getCondition();
//        
//        try
//        {
//            for(ResolveCatalog catalog : catalogResult)
//            {
//                Object value = BeanUtil.getProperty(catalog, field);
//                boolean match = false;
//                
//                if(type.equalsIgnoreCase("date"))
//                {
//                    //date
//                    List<QueryParameter> params = filter.getParameters();
//                    match = evaluateMatch(new Date(Long.parseLong((String) value)), params, condition);
//                }
//                else
//                {
//                    //string, auto
//                    match = evaluateMatch((String) value, queryValue, condition);
//                }
//                
//                //if match, than add to the resultset
//                if(match)
//                {
//                    result.add(catalog);
//                }
//               
//            }//end of for
//        }
//        catch (Throwable e)
//        {
//            Log.log.error("error in filtering for catalog", e);
//            result.addAll(catalogResult);
//        }
//        
//        return result;
//    }
//
//    private boolean evaluateMatch(Date value, List<QueryParameter> params, String condition)
//    {
//        boolean match = false;
//        
//        if(value != null && params != null && params.size() > 0 && StringUtils.isNotBlank(condition))
//        {
//            if(condition.equalsIgnoreCase(QueryFilter.ON))
//            {
//                //2 params will be there
//                match = value.after((Date)params.get(0).getValue()) && value.before((Date)params.get(1).getValue());
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.BEFORE))
//            {
//                //1 param
//                match = value.before((Date)params.get(0).getValue());
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.AFTER))
//            {
//                //1 param
//                match = value.after((Date)params.get(0).getValue());
//            }
//        }
//        else
//        {
//            match = true;
//        }
//        return match;
//    }
//    
//    private boolean evaluateMatch(String value, String queryValue, String condition)
//    {
//        boolean match = false;
//        
//        if(StringUtils.isNotBlank(value) && StringUtils.isNotBlank(queryValue) && StringUtils.isNotBlank(condition))
//        {
//            value = value.toLowerCase().trim();
//            queryValue = queryValue.toLowerCase().trim();
//            
//            if(condition.equalsIgnoreCase(QueryFilter.EQUALS))
//            {
//                match = value.equalsIgnoreCase(queryValue);
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.NOT_EQUALS))
//            {
//                match = !value.equalsIgnoreCase(queryValue);
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.STARTS_WITH))
//            {
//                match = value.startsWith(queryValue);
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.NOT_STARTS_WITH))
//            {
//                match = !value.startsWith(queryValue);
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.ENDS_WITH))
//            {
//                match = value.endsWith(queryValue);
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.NOT_ENDS_WITH))
//            {
//                match = !value.endsWith(queryValue);
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.CONTAINS))
//            {
//                match = value.contains(queryValue);
//            }
//            else if(condition.equalsIgnoreCase(QueryFilter.NOT_CONTAINS))
//            {
//                match = !value.contains(queryValue);
//            }
//        }
//        else
//        {
//            match = true;
//        }
//        
//        return match;
//    }
//    
//    
//    
//    private void applySorting(List<ResolveCatalog> catalogResult, QuerySort sort)
//    {
//        boolean asc = (StringUtils.isNotEmpty(sort.getDirection()) && sort.getDirection().equalsIgnoreCase("asc")) ? true : false;
//        String column = sort.getProperty();
//        
//        if(asc)
//        {
//            if(column.equalsIgnoreCase("sys_created_on"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysCreatedOnAsc());
//            }
//            else if(column.equalsIgnoreCase("sys_updated_on"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysUpdatedOnAsc());
//            }
//            else if(column.equalsIgnoreCase("sys_created_by"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysCreatedByAsc());
//            }
//            else if(column.equalsIgnoreCase("sys_updated_by"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysUpdatedByAsc());
//            }
//            else if(column.equalsIgnoreCase("id"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysIdAsc());
//            }
//            else
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogNameAsc());
//            }
//        }
//        else
//        {
//            if(column.equalsIgnoreCase("sys_created_on"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysCreatedOnDesc());
//            }
//            else if(column.equalsIgnoreCase("sys_updated_on"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysUpdatedOnDesc());
//            }
//            else if(column.equalsIgnoreCase("sys_created_by"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysCreatedByDesc());
//            }
//            else if(column.equalsIgnoreCase("sys_updated_by"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysUpdatedByDesc());
//            }
//            else if(column.equalsIgnoreCase("id"))
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogSysIdDesc());
//            }
//            else
//            {
//                Collections.sort(catalogResult, new ComparatorResolveCatalogNameDesc());
//            }
//        }
//    }
//    
////    
////         
////    private void sortResult(List<ResolveCatalog> result)
////    {
////        boolean asc = true;
////        if(this.query.getSortItems() != null && this.query.getSortItems().size() > 0)
////        {
////            for(QuerySort sort : this.query.getSortItems())
////            {
////                if(sort.getProperty().equalsIgnoreCase("name") && sort.getDirection().equalsIgnoreCase("DESC"))
////                {
////                    asc = false;
////                    break;
////                }
////            }
////        }
////        
////        if(asc)
////        {
////            Collections.sort(result, new ComparatorRsComponentAsc());
////        }
////        else
////        {
////            Collections.sort(result, new ComparatorRsComponentDesc());
////        }
////    }
////    
//    
//    
//    
//    
//    
//    
//    
//    
//    private class ComparatorResolveCatalogNameAsc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            return a.getName().toLowerCase().compareTo(b.getName().toLowerCase());
//        } // end compare
//
//    } // ComparatorBaseModel    
//
//    private class ComparatorResolveCatalogNameDesc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            int val = a.getName().toLowerCase().compareTo(b.getName().toLowerCase());
//            return val * -1;
//        } // end compare
//
//    } // ComparatorBaseModel      
//
//    private class ComparatorResolveCatalogSysCreatedByAsc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            return a.getSys_created_by().toLowerCase().compareTo(b.getSys_created_by().toLowerCase());
//        } // end compare
//
//    } // ComparatorBaseModel    
//
//    private class ComparatorResolveCatalogSysCreatedByDesc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            int val = a.getSys_created_by().toLowerCase().compareTo(b.getSys_created_by().toLowerCase());
//            return val * -1;
//        } // end compare
//
//    } // ComparatorBaseModel 
//    
//
//    private class ComparatorResolveCatalogSysUpdatedByAsc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            return a.getSys_updated_by().toLowerCase().compareTo(b.getSys_updated_by().toLowerCase());
//        } // end compare
//
//    } // ComparatorBaseModel    
//
//    private class ComparatorResolveCatalogSysUpdatedByDesc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            int val = a.getSys_updated_by().toLowerCase().compareTo(b.getSys_updated_by().toLowerCase());
//            return val * -1;
//        } // end compare
//
//    } // ComparatorBaseModel 
//    
//    private class ComparatorResolveCatalogSysCreatedOnAsc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            return new Long(a.getSys_created_on()).compareTo(new Long(b.getSys_created_on()));
//        } // end compare
//
//    } // ComparatorBaseModel    
//
//    private class ComparatorResolveCatalogSysCreatedOnDesc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            int val = new Long(a.getSys_created_on()).compareTo(new Long(b.getSys_created_on()));
//            return val * -1;
//        } // end compare
//
//    } // ComparatorBaseModel 
//    
//    private class ComparatorResolveCatalogSysUpdatedOnAsc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            return new Long(a.getSys_updated_on()).compareTo(new Long(b.getSys_updated_on()));
//        } // end compare
//
//    } // ComparatorBaseModel    
//
//    private class ComparatorResolveCatalogSysUpdatedOnDesc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            int val = new Long(a.getSys_updated_on()).compareTo(new Long(b.getSys_updated_on()));
//            return val * -1;
//        } // end compare
//
//    } // ComparatorBaseModel
//    
//    private class ComparatorResolveCatalogSysIdAsc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            return a.getId().toLowerCase().compareTo(b.getId().toLowerCase());
//        } // end compare
//
//    } // ComparatorBaseModel    
//
//    private class ComparatorResolveCatalogSysIdDesc implements Comparator<ResolveCatalog>
//    {
//        public final int compare(ResolveCatalog a, ResolveCatalog b)
//        {
//            int val = a.getId().toLowerCase().compareTo(b.getId().toLowerCase());
//            return val * -1;
//        } // end compare
//
//    } // ComparatorBaseModel 
    
}
