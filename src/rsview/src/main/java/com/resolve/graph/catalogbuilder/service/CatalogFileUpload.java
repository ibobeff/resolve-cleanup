/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.resolve.services.ServiceCatalog;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.vo.CatalogAttachmentVO;
import com.resolve.util.StringUtils;

public class CatalogFileUpload
{
    private HttpServletRequest request = null;
    private String username = null;
    
    public CatalogFileUpload(HttpServletRequest request, String username) throws Exception
    {
        if(request == null || StringUtils.isEmpty(username))
        {
            throw new Exception("Request object and username cannot be null");
        }
        
        this.request = request;
        this.username = username;
    }
    
    public CatalogAttachmentVO upload() throws Exception
    {
        // Check that we have a file upload request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart)
        {
            //if not an upload request, then return
            throw new Exception("Request object does not have any multipart content to upload.");
        }
        
        // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();
        
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        
        //create the attachment object for the document
        CatalogAttachmentVO file = prepareAttachment(upload, request, username);
        if(file != null)
        {
            //persist it 
            file = ServiceCatalog.saveCatalogAttachment(file, username);
        }
        
        return file;
    }
    
    @SuppressWarnings("unchecked")
    private CatalogAttachmentVO prepareAttachment(ServletFileUpload upload, HttpServletRequest request, String username) throws Exception
    {
      //create the attachment object for the document
        CatalogAttachmentVO file =  new CatalogAttachmentVO();
        
        // Parse the request
        List<FileItem> items = upload.parseRequest(request);

        // Process the uploaded items
        Iterator<FileItem> iter = items.iterator();
        while (iter.hasNext())
        {
            FileItem item = iter.next();
//            String name = item.getFieldName();

            if (item.isFormField())
            {
                processFormField(item, file);
            }
            else
            {
                processUploadedFile(item, file);
            }
        }//end of while loop
        
        return file;
    }
    
    private void processFormField(FileItem item, CatalogAttachmentVO file) throws WikiException
    {
       
    }
    
    private void processUploadedFile(FileItem item, CatalogAttachmentVO file) throws Exception
    {
        String fileName = item.getName();
        fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
        long sizeInBytes = item.getSize();
        InputStream uploadedStream = item.getInputStream();
        byte[] data = new byte[(int) sizeInBytes];
        uploadedStream.read(data);

        file.setContent(data);
        file.setFilename(fileName);
        file.setU_size(data.length);
        file.setDisplayname(fileName);
    }
    
    
}
