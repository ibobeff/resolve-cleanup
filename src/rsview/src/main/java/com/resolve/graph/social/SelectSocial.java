/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Node;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.util.TraversalUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SelectSocial
{
    /**
     * get all the users that are directly following or member of this stream
     * 
     * @param streamId
     * @param username
     * @return
     */
    public static List<User> getAllUsersFollowingThisStream(String streamId, String username)
    {
        List<User> users = new ArrayList<User>();
        Node streamNode = null;
        
        try
        {
            //do we need the current user?
            if(StringUtils.isNotEmpty(streamId))
            {
                streamNode = ResolveGraphFactory.findNodeByIndexedID(streamId);
                if(streamNode != null)
                {
//                    if(streamNode.hasProperty(RSComponent.TYPE))
//                    {
//                        //if its user to user relationship, than add the current user also in the list
//                        String type = (String) streamNode.getProperty(RSComponent.TYPE);
//                        if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//                        {
//                            User followingUser = new User();
////                            followingUser.convertNodeToObject(streamNode);
//                            
//                            //if the username is the same as the Stream username, ignore it as User cannot follow himself
//                            if(!username.equalsIgnoreCase(followingUser.getUsername()))
//                            {
//                                users.add(followingUser);
//                            }
//                        }
//                    }
//                
//                    Iterable<Node> usersFollowingThisNode = TraversalUtil.findUsersDirectMembersOrFollowerOf(streamNode);
//                    if(usersFollowingThisNode != null)
//                    {
//                        for(Node userNode : usersFollowingThisNode)
//                        {
//                            User followingUser = new User();
//                            followingUser.convertNodeToObject(userNode);
//                            users.add(followingUser);
//                        }
//                    }
                }
            }

        }
        catch(Throwable t)
        {
            Log.log.info(t.getMessage(), t);
        }

        return users;
    }
    
//    public static List<User> getListOfUsersThisUserIsFollowing(User user)
//    {
//        List<User> result = new ArrayList<User>();
//        
//        try
//        {
//            //do we need the current user?
////            if(StringUtils.isNotEmpty(user.getSys_id()))
////            {
////                Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////                if(userNode != null)
////                {
////                    Iterable<Node> usersFollowingThisNode = TraversalUtil.findUsersThatThisUserIsFollowing(userNode);
////                    if(usersFollowingThisNode != null)
////                    {
////                        for(Node otherUser : usersFollowingThisNode)
////                        {
////                            User followingUser = new User();
////                            followingUser.convertNodeToObject(otherUser);
////                            result.add(followingUser);
////                        }
////                    }
////                }
////            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info(t.getMessage(), t);
//        }
//        
//        
//        return result;
//        
//    }
    
    public static Process getProcess(String processId, boolean children)
    {
        Process process = null;
        
        if (StringUtils.isNotBlank(processId)) 
        {
            Node processNode = ResolveGraphFactory.findNodeByIndexedID(processId);
            if(processNode != null)
            {
//                String sysId = (String) processNode.getProperty(RSComponent.SYS_ID);
//                String displayName = (String) processNode.getProperty(RSComponent.DISPLAYNAME);
                
                //populate the comp data
//                process = new Process(sysId, displayName);
//                process.convertNodeToObject(processNode);
//                
//              
//                //if the flag is true, than get the children
//                if (children)
//                {
//                    //get the process members
//                    Iterable<Node> nodes = TraversalUtil.findMembersBelongingToProcess(processNode);
//                    if (nodes != null)
//                    {
//                        for (Node node : nodes)
//                        {
//                            if (node.hasProperty(RSComponent.TYPE))
//                            {
//                                String type = (String) node.getProperty(RSComponent.TYPE);
//
//                                if (type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
//                                {
//                                    Team team = new Team();
//                                    team.convertNodeToObject(node);
//                                    process.getTeams().add(team);
//                                }
//                                else if (type.equalsIgnoreCase(SocialRelationshipTypes.FORUM.name()))
//                                {
//                                    Forum forum = new Forum();
//                                    forum.convertNodeToObject(node);
//                                    process.getForums().add(forum);
//                                }
//                                else if (type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//                                {
//                                    User user = new User();
//                                    user.convertNodeToObject(node);
//                                    process.getUsers().add(user);
//                                }
//                                else if (type.equalsIgnoreCase(SocialRelationshipTypes.ACTIONTASK.name()))
//                                {
//                                    ActionTask at = new ActionTask();
//                                    at.convertNodeToObject(node);
//                                    process.getActiontasks().add(at);
//                                }
//                                else if (type.equalsIgnoreCase(SocialRelationshipTypes.WORKSHEET.name()))
//                                {
//                                    Worksheet ws = new Worksheet();
//                                    ws.convertNodeToObject(node);
//                                    //TODO - Ws is not done yet
//                                    //                                process.get
//                                }
//                                else if (type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//                                {
//                                    Document doc = new Document();
//                                    doc.convertNodeToObject(node);
//                                    process.getDocuments().add(doc);
//
//                                    if (doc.isRunbook())
//                                    {
//                                        Runbook rb = new Runbook();
//                                        rb.convertNodeToObject(node);
//                                        rb.setType(SocialRelationshipTypes.RUNBOOK.name());
//                                        process.getRunbooks().add(rb);
//                                    }
//
//                                    if (doc.isDecisionTree())
//                                    {
//                                        DecisionTree dt = new DecisionTree();
//                                        dt.convertNodeToObject(node);
//                                        dt.setType(SocialRelationshipTypes.DECISIONTREE.name());
//                                        process.getDecisionTrees().add(dt);
//                                    }
//                                }
//                                else if (type.equalsIgnoreCase(SocialRelationshipTypes.RSS.name()))
//                                {
//                                    Rss rss = new Rss();
//                                    rss.convertNodeToObject(node);
//                                    process.getRss().add(rss);
//                                }
//                                else if (type.equalsIgnoreCase(SocialRelationshipTypes.NAMESPACE.name()))
//                                {
//                                    Namespace ns = new Namespace();
//                                    ns.convertNodeToObject(node);
//                                    process.getNameSpaces().add(ns);
//                                }
//                            }
//
//                        }//end of for loop
//                    }
//                }//end of if
            }
        }
        return process;
    }
    
    public static Team getTeam(String teamID, boolean recurse) throws Exception
    {
        Team team = null;
        Node teamNode = null;
        
        if(StringUtils.isNotEmpty(teamID))
        {
//            teamNode = ResolveGraphFactory.findNodeByIndexedID(teamID);
//            if(teamNode != null)
//            {
//                team = new Team();
//                team.convertNodeToObject(teamNode);
//                
//                //get the list of users for this team
//                //list of users that this team has
//                Iterable<Node> userNodes = TraversalUtil.findUsersBelongingToTeam(teamNode);
//                if (userNodes != null)
//                {
//                    List<User> users = new ArrayList<User>();
//                    
//                    for (Node userNode : userNodes)
//                    {
//                       User user = new User();
//                       user.convertNodeToObject(userNode);
//                       users.add(user);                       
//                    }
//                    
//                    team.setUsers(users);
//                }
//                
//                
//                //get the list of teams
//                //list of teams that this team has
//                Iterable<Node> childTeams = TraversalUtil.findTeamsBelongingToTeam(teamNode);
//                if (childTeams != null)
//                {
//                    List<Team> teams = new ArrayList<Team>();
//                    
//                    for (Node childTeam : childTeams)
//                    {
//                        Team t = null;
//                        if(recurse)
//                        {
//                            //** RECURSIVE
//                            String childSysId = (String) childTeam.getProperty(RSComponent.SYS_ID); 
//                            t = getTeam(childSysId, recurse);
//                        }
//                        else
//                        {
//                            t = new Team();
//                            t.convertNodeToObject(childTeam);
//                        }
//                        
//                        if(t != null)
//                        {
//                            teams.add(t);
//                        }
//                    }
//                    
//                    team.setTeams(teams);
//                }
//            }
        }        
        
        return team;
    }
    
    public static Forum getForum(String forumID) throws Exception
    {
        Forum forum = null;
        if(StringUtils.isNotEmpty(forumID))
        {
//            Node forumNode = ResolveGraphFactory.findNodeByIndexedID(forumID);
//            if(forumNode != null)
//            {
//                forum = new Forum();
//                forum.convertNodeToObject(forumNode);
//                
//                Iterable<Node> userNodes = TraversalUtil.findUsersBelongingToForum(forumNode);
//                if (userNodes != null)
//                {
//                    List<User> users = new ArrayList<User>();
//                    
//                    for (Node userNode : userNodes)
//                    {
//                       User user = new User();
//                       user.convertNodeToObject(userNode);
//                       users.add(user);                       
//                    }
//                    
//                    forum.setUsers(users);
//                }
//            }
        }
        
        
        return forum;
    }

}
