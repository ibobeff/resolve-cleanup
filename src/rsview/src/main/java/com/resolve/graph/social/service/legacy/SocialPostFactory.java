/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.index.lucene.ValueContext;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.message.Post;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialPostFactory extends SocialFactory
{
    private static Node containerNodeLocal;
    
    public static void init()
    {
        init(SocialRootRelationshipTypes.POSTS, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.POSTS.name(), containerNode);
    }
    
    public static Node create(Post post)
    {
        Node node = null;
        Transaction tx = null;
               
        try
        {
            getContainerNode();

            node = findPost(post.getSys_id(), post.getTitleUrl());
            
//            if(node == null)
//            {
//                tx = GraphDBUtil.getGraphDBService().beginTx();  
//                node = GraphDBUtil.getGraphDBService().createNode(); 
//                post.convertObjectToNode(node);  
////                node.setProperty(NODE_TYPE, SocialObjectType.POST.name()); 
//                containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.POST); 
//                tx.success(); 
//                
//                //index the node sys_update_on
//                GraphDBUtil.getPostIndex().add(node, RSComponent.SYS_UPDATED_ON, new ValueContext(post.getSys_updated_on()).indexNumeric());
//            }
//            else
//            {
//                if(StringUtils.isEmpty(post.getSys_id()))
//                {
//                    post.setId(Long.toString(node.getId()));
//                    post.setSys_id(Long.toString(node.getId()));
//                }
//            }
        }
        catch(Throwable t)
        {
            Log.log.warn("create Post caught exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
        
        return node;
    }
    
    public static void setPostIsRoot(Node postNode)
    {
        Transaction tx = null;
               
        try
        {
            getContainerNode();
            
            if(postNode != null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();   
                postNode.setProperty(Post.ISROOT, true);
                tx.success();
            }
        }
        catch(Throwable t)
        {
            Log.log.warn("create Post caught exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }
    
    public static boolean findPostWithURLForRSS(String id, String titleUrl)
    {
        boolean find = false;
        Node node = null;
        
        if(!StringUtils.isEmpty(titleUrl) && StringUtils.isEmpty(id))
        {
            node = findNodeByIndexedTitleURL(titleUrl);
            
            if(node != null)
            {
                find = true;
            }
        }
            
        return find;
    }
    
    public static Node findPost(String id, String titleUrl)
    {
        Node node = null;
        
        if(!StringUtils.isEmpty(id) && !StringUtils.isEmpty(titleUrl))
        {
            node = ResolveGraphFactory.findNodeByIndexedID(id);
        }
        else if(!StringUtils.isEmpty(id) && StringUtils.isEmpty(titleUrl))
        {
            node = ResolveGraphFactory.findNodeByIndexedID(id);
        }
        else if(!StringUtils.isEmpty(titleUrl) && StringUtils.isEmpty(id))
        {
            node = findNodeByIndexedTitleURL(titleUrl);
        }
            
        return node;
    }
    
    public Node findPostNode(String id, String titleUrl)
    {
        return findPost(id, titleUrl);
    }
    
    public Node findNode(String id)
    {
        return ResolveGraphFactory.findNodeByIndexedID(id);
    }
    
    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.POSTS.name());
        }
        
        return containerNodeLocal;
    }

}

