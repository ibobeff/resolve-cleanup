/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.catalogbuilder.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.util.Log;


public class CatalogFactory
{
    public static final String NODE_TYPE = "NODE_TYPE";
    public static final String OUTBOX = "OUTBOX";

    //user global config for notification
    public static final String AT_NOTIFY_ON_SAVE   = "AT_NOTIFY_ON_SAVE";
    public static final String AT_NOTIFY_ON_UPDATE = "AT_NOTIFY_ON_UPDATE";
    public static final String AT_NOTIFY_ON_DELETE = "AT_NOTIFY_ON_DELETE";
    public static final String AT_NOTIFY_ON_COMMIT = "AT_NOTIFY_ON_COMMIT";

    public static final String RB_NOTIFY_ON_SAVE   = "RB_NOTIFY_ON_SAVE";
    public static final String RB_NOTIFY_ON_UPDATE = "RB_NOTIFY_ON_UPDATE";
    public static final String RB_NOTIFY_ON_DELETE = "RB_NOTIFY_ON_DELETE";
    public static final String RB_NOTIFY_ON_COMMIT = "RB_NOTIFY_ON_COMMIT";

    //protected static Node containerNode;
    protected static Map<String, Node> allContainerNodes  = new ConcurrentHashMap<String, Node>();


    //create container node for the type
    @SuppressWarnings("deprecation")
    public static void init(RelationshipType type, Direction direction)
    {
        Transaction tx = null;

        try
        {
            Relationship rel = ResolveGraphFactory.findRelationship(type, direction);
            Node containerNode = null;

            if(rel == null)
            {
                tx = GraphDBUtil.getGraphDBService().beginTx();
                containerNode =  GraphDBUtil.getGraphDBService().createNode();
//                GraphDBUtil.getGraphDBService().getReferenceNode().createRelationshipTo(containerNode, type);
                GraphDBUtil.getReferenceNode().createRelationshipTo(containerNode, type);
                tx.success();
            }
            else
            {
                containerNode = rel.getEndNode();
            }

            allContainerNodes.put(type.name(), containerNode);

            Log.log.info("-- finished init catalog container -- " + type.name());
        }
        catch(Throwable t)
        {
            Log.log.warn("catalog builder create exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }

//    public static Node createUpdateCatalogNode(ResolveCatalog catalogObj, boolean isRoot) throws Exception
//    {
//        Node resultNode = null;
//        Transaction tx = null;
//        Node containerNodeLocal = null;
//
//        try
//        {
//            if(StringUtils.isNotEmpty(catalogObj.getId()))
//            {
//                //update
//                resultNode = findNodeByIndexedID(catalogObj.getId());
//            }
//
//
//            tx = GraphDBUtil.getGraphDBService().beginTx();
//
//            //create a new node if its null
//            if(resultNode == null)
//            {
//                resultNode = GraphDBUtil.getGraphDBService().createNode();
//                if (isRoot)
//                {
//                    containerNodeLocal = allContainerNodes.get(CatalogContainerType.Catalog.name());
//                    if (containerNodeLocal != null)
//                    {
//                        containerNodeLocal.createRelationshipTo(resultNode, CatalogContainerType.Catalog);
//                    }
//                }
//            }
//
//            //update rest of the attributes
//            catalogObj.convertObjectToNode(resultNode);
//
//            tx.success();

//
//
//
//            if(!StringUtils.isEmpty(catalogObj.getId()))
//            {
//                resultNode = findNodeByIndexedID(catalogObj.getId());
//            }
//
//            if(resultNode == null)
//            {
//                tx = GraphDBUtil.getGraphDBService().beginTx();
//
//                resultNode = GraphDBUtil.getGraphDBService().createNode();
//                catalogObj.convertObjectToNode(resultNode);
//
//                if(isRoot)
//                {
//                    containerNodeLocal = allContainerNodes.get(CatalogContainerType.Catalog.name());
//
//                    if(containerNodeLocal != null)
//                    {
//                        containerNodeLocal.createRelationshipTo(resultNode, CatalogContainerType.Catalog);
//                    }
//                }
//
//                tx.success();
//            }

//        }
//        catch(Exception t)
//        {
//            Log.log.warn("create/update catalog node exception: " + t, t);
//            throw t;
//        }
//        finally
//        {
//            if(tx != null)
//            {
//                tx.finish();
//            }
//        }
//
//        return resultNode;
//    }
//
//    public static Node findNodeByIndexedID(String id)
//    {
//        return ResolveGraphFactory.findNodeByIndexedID(id);
//    }
//
//
//    public static Node getContainerNode(CatalogContainerType type)
//    {
//        Node result = null;
//
//        if(type != null)
//        {
//            result = allContainerNodes.get(type.name());
//        }
//
//        return result;
//    }
//
//    public static void reset()
//    {
//        Log.log.debug("Reseting... ");
//        if(allContainerNodes != null)
//        {
//            for(String key : allContainerNodes.keySet())
//            {
//                Log.log.debug("Here is a node with key: " + key + " and the ID: " + allContainerNodes.get(key).getId());
//            }
//            allContainerNodes.clear();
//        }
//    }
}
