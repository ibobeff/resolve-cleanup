/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class GraphUtil
{
    private static Set<User> users = new HashSet<User>();
//    public static RSComponent setStreamLockUnlock(User user, String id, boolean locked) throws Exception
//    {
//        Node compNode = null;
//        Transaction tx = null;
//        RSComponent comp = null;
//
//        if (StringUtils.isNotEmpty(id))
//        {
//            compNode = ResolveGraphFactory.findNodeByIndexedID(id);
//
//            if (compNode != null)
//            {
//                try
//                {
//                    String type = (String) compNode.getProperty(RSComponent.TYPE);
//                    
//                    //User stream cannot be LOCKED
//                    if(!type.equalsIgnoreCase(SocialObjectType.USER.name()))
//                    {
//                        tx = GraphDBUtil.getGraphDBService().beginTx();
//                        compNode.setProperty(RSComponent.LOCK, locked);
//                        tx.success();
//                    }
//                    
//                    comp = new RSComponent();
//                    comp.convertNodeToObject(compNode);
//                }
//                catch (Exception t)
//                {
//                    Log.log.info(t.getMessage(), t);
//                    throw t;
//                }
//                finally
//                {
//                    if (tx != null)
//                    {
//                        tx.finish();
//                    }
//                }
//            }
//        }
//        
//        return comp;
//    }

//    public static void additionalOperationForComponents(User user, List<String> ids, boolean follow)
//    {
//        //this api does additional steps that may be needed after a follow/unfollow of comp is done
//
//        if(ids != null)
//        {
//            for(String compId : ids)
//            {
////                RSComponent comp = ServiceSocial.getComponentById(compId, user.getUsername());
////
////                //for RSS
////                if(comp != null && comp.getType().equalsIgnoreCase(HibernateConstants.SOCIAL_TYPE_RSS))
////                {
////                    if(follow)
////                    {
////                        FollowUnfollowUtil.loadRss((Rss)comp);
////                    }
////                    else
////                    {
////                        FollowUnfollowUtil.removeRss((Rss)comp);
////                    }
////                }
//            }
//        }
//
//
//
//    }

    public static Set<String> findUserIdsBelongingToProcessIncludingTeams(String processId)
    {
        Set<String> userIds = new HashSet<String>();

        Node processNode = ResolveGraphFactory.findNodeByIndexedID(processId);
        if(processNode != null)
        {
            //user that belong to the Process directly
//            Iterable<Node> userNodes = TraversalUtil.findUsersBelongingToThisProcess(processNode);
//            if(userNodes != null)
//            {
//                for(Node userNode : userNodes)
//                {
//                    String sysId = (String) userNode.getProperty(RSComponent.SYS_ID);
//                    userIds.add(sysId);
//                }
//            }
//
//            //users that belong to Team of the Process
//            //first get the list of Teams that this Process has
//            Iterable<Node> teamNodes = TraversalUtil.findTeamsBelongingToProcess(processNode);
//            if(teamNodes != null)
//            {
//                for(Node teamNode : teamNodes)
//                {
//                    userIds.addAll(findUserIdsBelongingToTeamAndItsChildTeams(teamNode, true));
//                }
//            }

        }

        return userIds;
    }


    public static Set<String> findUserIdsBelongingToTeamAndItsChildTeams(Node teamNode, boolean child)
    {
        Set<String> userIds = new HashSet<String>();

        //get the users of this team
//        Iterable<Node> usersOfTeam =  TraversalUtil.findUsersBelongingToTeam(teamNode);
//        if(usersOfTeam != null)
//        {
//            for(Node userNode : usersOfTeam)
//            {
//                String sysId = (String) userNode.getProperty(RSComponent.SYS_ID);
//                userIds.add(sysId);
//            }
//        }
//
//        if (child)
//        {
//            //list of teams that this team has
//            Iterable<Node> childTeams = TraversalUtil.findTeamsThatThisTeamIsMemberOf(teamNode);
//            if (childTeams != null)
//            {
//                for (Node childTeam : childTeams)
//                {
//                    //**RECURSIVE
//                    Set<String> userIdsOfTeam = findUserIdsBelongingToTeamAndItsChildTeams(childTeam, child);
//                    userIds.addAll(userIdsOfTeam);
//                }
//            }
//        }

        return userIds;
    }
    
    public static Set<RSComponent> findTeamsThatThisTeamIsMemberOf(String teamSysId) throws Exception
    {
        Set<RSComponent> result = new HashSet<RSComponent>();
        
//        Node node = ResolveGraphFactory.findNodeByIndexedID(teamSysId);
//        if(node != null)
//        {
//            //get the list of team nodes - this should be recursive
//            Iterable<Node> memberOfTeamNodes = TraversalUtil.findTeamsThatThisTeamIsMemberOf(node);
//            if (memberOfTeamNodes != null)
//            {
//                for (Node teamNode : memberOfTeamNodes)
//                {
//                    RSComponent comp = new RSComponent();
//                    comp.convertNodeToObject(teamNode);
//                    result.add(comp);
//                    
//                    //**RECURSIVE
//                    Set<RSComponent> teams = findTeamsThatThisTeamIsMemberOf(comp.getSys_id());
//                    result.addAll(teams);
//                }
//            }
//        }
//        else
//        {
//            throw new Exception("Team with sysId " + teamSysId + " does not exist.");
//        }
        
        return result;
        
    }

    public static Set<User> findUsersBelongingToTeamAndItsChildTeams(String teamId, boolean child)
    {
        if(StringUtils.isNotBlank(teamId))
        {
            Collection<ResolveNodeVO> nodes = null;
            try
            {
                // Users
                nodes = ServiceGraph.getNodesFollowingMe(null, teamId, null, NodeType.TEAM, "admin");
            }
            catch (Exception e)
            {
                Log.log.error("ERROR getting users followed by process. SysId: " + teamId, e);
            }
            
            if (nodes != null && nodes.size() > 0)
            {
                for (ResolveNodeVO node : nodes)
                {
                    if (node.getUType().equals(NodeType.USER))
                    {
                        try
                        {
                            User user = new User(node);
                            users.add(user);
                        }
                        catch (Exception e)
                        {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                }
            }
            
            if (child)
            {
                try
                {
                    nodes = ServiceGraph.getNodesFollowedBy(null, teamId, null, NodeType.TEAM, "admin");
                }
                catch (Exception e)
                {
                    Log.log.error("ERROR getting nodes of process. SysId: " + teamId, e);
                }
                
                if (nodes != null && nodes.size() > 0)
                {
                    for (ResolveNodeVO node : nodes)
                    {
                        if (node.getUType().equals(NodeType.TEAM))
                        {
                            findUsersBelongingToTeamAndItsChildTeams(node.getUCompSysId(), true);
                        }
                    }
                }
            }
            
//            Node teamNode = ResolveGraphFactory.findNodeByIndexedID(teamId);
//            if(teamNode != null)
//            {
//                Set<String> userIds = findUserIdsBelongingToTeamAndItsChildTeams(teamNode, child);
//                for(String userId : userIds)
//                {
//                    Node userNode = ResolveGraphFactory.findNodeByIndexedID(userId);
//                    if(userNode != null)
//                    {
//                        User user = new User();
//                        user.convertNodeToObject(userNode);
//                        users.add(user);
//                    }
//                }
//            }
        }

        return users;
    }

    public static void addToHome(User user, RSComponent rscomponent) throws Exception
    {
        Node node = null;
        Node userNode = null;

        try
        {
//            if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if (userNode != null && rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
//                {
//                    node = ResolveGraphFactory.findNodeByIndexedID(rscomponent.getSys_id());
//
//                    if (node != null)
//                    {
//                        //check if the user is following this comp first
////                        boolean isFollowing = GetSocial.isUserFollowingThisTarget(userNode, node);
////                        if (isFollowing)
////                        {
////                            ResolveGraphFactory.addRelation(userNode, node, RelationType.FAVORITE);
////                        }
//                    }
//                }
//            }
        }
        catch (Exception t)
        {
            Log.log.info("addToHome catched exception: " + t);
            throw t;
        }
    }

    public static void removeFromHome(User user, RSComponent rscomponent) throws Exception
    {
        Node userNode = null;
        Node node = null;

//        try
//        {
//            if (user != null && StringUtils.isNotEmpty(user.getSys_id()))
//            {
//                userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//                if (userNode != null && rscomponent != null && StringUtils.isNotEmpty(rscomponent.getSys_id()))
//                {
//                    node = ResolveGraphFactory.findNodeByIndexedID(rscomponent.getSys_id());
//
//                    if (node != null)
//                    {
//                        ResolveGraphFactory.deleteRelation(userNode, node, RelationType.FAVORITE);
//                    }
//                }
//            }
//        }
//        catch (Exception t)
//        {
//            Log.log.info("removeFromHome catched exception: " + t);
//            throw t;
//        }
    }

//    public static void setFollowStreams(User user, List<String> ids, boolean follow) throws Exception
//    {
//        Node userNode = null;
//        Node followNode = null;
//
////        if(user != null && StringUtils.isNotEmpty(user.getSys_id()))
////        {
////            List<SubmitNotification> submitNotifications = new ArrayList<SubmitNotification>();
//////            StringBuilder errors = new StringBuilder();
////            
////            userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
////
////            if(userNode != null && ids != null)
////            {
////                for(String followId: ids)
////                {
////                    followNode = ResolveGraphFactory.findNodeByIndexedID(followId);
////                    RSComponent comp = LookupComponent.getRSComponentById(followId);
////
////                    try
////                    {
////                        if(followNode != null)
////                        {
////                            if(follow)
////                            {
////                                //validate if the user has roles to follow this comp
//////                                validateIfUserCanFollowThisComponent(user, comp);                            
////                                
////                                if(comp instanceof RSContainer)
////                                {
////                                    ResolveGraphFactory.addRelation(userNode, followNode, RelationType.MEMBER);
////                                }
////                                else
////                                {
////                                    ResolveGraphFactory.addRelation(userNode, followNode, RelationType.FOLLOWER);
////                                }
////                            }
////                            else
////                            {
////                                //this is UNFOLLOW
////                                unfollow(userNode, followNode);
////                            }
////                            
////                            //prepare notification
////                            SubmitNotification notification = prepareNotificationForUserFollowingComp(user, comp, follow);
////                            if(notification != null)
////                            {
////                                submitNotifications.add(notification);
////                            }
////                        }
////                    }
////                    catch (Exception e)
////                    {
////                       Log.log.error(e.getMessage(), e);
//////                       errors.append(e.getMessage()).append("\n");
////                    }
////                }//end of for loop
////                
////                if(submitNotifications.size() > 0)
////                {
////                    Map<String, Object> params = new HashMap<String, Object>();
////                    params.put(ConstantValues.SOCIAL_NOTIFICATION_LIST, submitNotifications);
////                    
////                    SocialUtil.submitESBForUpdateSocialForWiki(params);
////                }
////                
////                //if there are any errors, throw it
//////                if(errors.length() > 0)
//////                {
//////                    throw new Exception(errors.toString());
//////                }
////            }
////        }
//
//    }
    

    @SuppressWarnings("unused")
    public static long getFollowerCountForComp(String compId)
    {
        long count = 0;
        Node compNode = ResolveGraphFactory.findNodeByIndexedID(compId);
        if(compNode != null)
        {
            Iterable<Node> userNodes = TraversalUtil.findUsersDirectOrIndirectMembersOrFollowerOf(compNode);
            if(userNodes != null)
            {
                for(Node userNode : userNodes)
                {
                    count++;
                }
            }
        }

        return count;
    }
    
    public static Set<String> getComponentIdsBelongingToContainer(RSComponent targetComponent) throws Exception
    {
        Set<String> result = new HashSet<String>();
        
//        if(targetComponent.getType().equalsIgnoreCase(SocialRelationshipTypes.PROCESS.name()))
//        {
//            Process process = ServiceSocial.getProcess(targetComponent.getSys_id(), true);
//            result.addAll(getAllChildComponentIdsForProcess(process));
//        }
//        else if(targetComponent.getType().equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
//        {
//            Team team = ServiceSocial.getTeam(targetComponent.getSys_id(), true);
//            result.addAll(getAllChildComponentIdsForTeam(team, false));
//        }
        
        return result;
    }

    //TODO: wait till elastic search is implemented
    public static void notifyUser(User notifyuser, UserGlobalNotificationContainerType notifyType, String subject)
    {
//        try
//        {
//            User  adminUser = SocialCompConversionUtil.getSocialUser(UserUtils.ADMIN);
//            String content = adminUser.getUsername() + subject;
//            SystemNotification notif = new SystemNotification(notifyType);
//
//            notif.setDisplayName(adminUser.getUsername());
//            notif.setContent(content);
//            notif.setAuthor(adminUser.getUsername());
//            notif.addComments(new ArrayList());
//            notif.setSys_created_on(new Date().getTime());
//            notif.setSys_updated_on(new Date().getTime());
//            notif.setSys_created_by(UserUtils.ADMIN);
//            notif.setSys_updated_by(UserUtils.ADMIN);
//            notif.setRoles(UserUtils.ADMIN +  " " + PostSocial.INTERNAL_POST_MEMBER_ROLE);
//
//            Set<String> targets = new HashSet<String>();
//            Set<String> receiver = new HashSet<String>();
//            Set<String> starredIDsSet = new HashSet<String>();
//            Set<String> outboxIDsSet = new HashSet<String>();
//            Set<String> namespaceSet = new HashSet<String>();
//
//            List<RSComponent> targetLocal = new ArrayList();
//
//            if(notif != null)
//            {
//                targetLocal.add(notifyuser);
//                notif.setTarget(targetLocal);
//
//                Node postNode = null;
//                Node targetUserNode = null;
//
//                postNode = PostSocial.createPost(notif);
//                PostSocial.userSentPost(adminUser, notif, postNode);
//
//                if(postNode != null && notifyuser != null && StringUtils.isNotEmpty(notifyuser.getSys_id()))
//                {
//                    targetUserNode = ResolveGraphFactory.findNodeByIndexedID(notifyuser.getSys_id());
//
//                    if(postNode != null && targetUserNode != null)
//                    {
//                        ResolveGraphFactory.addRelation(postNode, targetUserNode, RelationType.TARGET);
//                        //SocialFactory.updateRSComponentLastActivityOn(targetUserNode);
//                    }
//                }
//
//                receiver.add(notifyuser.getSys_id());
//                targets.add(notifyuser.getSys_id());
//
//                PostSocial.buildindexPost(notif, adminUser, targets, receiver, starredIDsSet, outboxIDsSet, namespaceSet);
//
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.error("process notification catch exception: " + exp, exp);
//        }
    }
    
    private static Set<String> getAllChildComponentIdsForProcess(Process process) throws Exception
    {
        Set<String> result = new HashSet<String>();
        
//        Collection<User> users = process.getUsers(); //no users as it will show blogs
        Collection<Team> teams = process.getTeams();
        if(teams != null && teams.size() > 0)
        {
            for(Team team : teams)
            {
                result.addAll(getAllChildComponentIdsForTeam(team, true));
            }
        }
        
//        Collection<Document> documents = process.getDocuments();
//        if(documents != null)
//        {
//            for(Document doc : documents)
//            {
//                result.add(doc.getSys_id());
//            }
//        }
//        
//        Collection<ActionTask> actiontasks = process.getActiontasks();
//        if(actiontasks != null)
//        {
//            for(ActionTask task : actiontasks)
//            {
//                result.add(task.getSys_id());
//            }
//        }
//        
//        Collection<Rss> rsss = process.getRss();
//        if(rsss != null)
//        {
//            for(Rss rss : rsss)
//            {
//                result.add(rss.getSys_id());
//            }
//        }
//        
//        Collection<Forum> forums = process.getForums();
//        if(forums != null)
//        {
//            for(Forum forum : forums)
//            {
//                result.add(forum.getSys_id());
//            }
//        }
//        
//        Collection<Namespace> namespaces = process.getNameSpaces();
//        if(namespaces != null)
//        {
//            for(Namespace ns : namespaces)
//            {
//                result.add(ns.getSys_id());
//            }
//        }
        
        
        return result;
        
    }
    
    private static Set<String> getAllChildComponentIdsForTeam(Team team, boolean queryToGraph) throws Exception
    {
        Set<String> result = new HashSet<String>();
        Team teamLocal = team;
//       
//        //query to get the team details
//        if(queryToGraph)
//        {
//            teamLocal = ServiceSocial.getTeam(team.getSys_id(), true);
//        }
//        
//        //add this team
//        result.add(teamLocal.getSys_id());
//        
//        Collection<Team> teams = teamLocal.getTeams();
//        if(teams != null)
//        {
//            for(Team childTeam : teams)
//            {
//                //**RECURSE
//                result.addAll(getAllChildComponentIdsForTeam(childTeam, false));
//            }
//        }
        
        return result;
    }
    

    private static SubmitNotification prepareNotificationForUserFollowingComp(User user, RSComponent comp, boolean follow)
    {
        SubmitNotification submitNotification = null;
        UserGlobalNotificationContainerType notificationType = null;
//        String compType = comp.getType();
//        
//        if(follow)
//        {
//            if(compType.equalsIgnoreCase(SocialRelationshipTypes.PROCESS.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.PROCESS_USER_ADDED;
//                submitNotification = NotificationHelper.getProcessNotification((Process) comp, user, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.TEAM_USER_ADDED;
//                submitNotification = NotificationHelper.getTeamNotification((Team) comp, user, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.FORUM.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.FORUM_USER_ADDED;
//                submitNotification = NotificationHelper.getForumNotification((Forum) comp, user, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.USER_FOLLOW_ME;
//                //submitNotification = NotificationHelper.getUserNotification((User) comp, user, notificationType, user.getUsername(), false, true);
////                                                        getUserFollowNotification(User user, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
//                submitNotification = NotificationHelper.getUserFollowNotification((User) comp, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.ACTIONTASK.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.AC;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.WORKSHEET.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.NAMESPACE.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.PROCESS_USER_ADDED;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.RSS.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.PROCESS_USER_ADDED;
//            }
//        }
//        else
//        {
//            if(compType.equalsIgnoreCase(SocialRelationshipTypes.PROCESS.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.PROCESS_USER_REMOVED;
//                submitNotification = NotificationHelper.getProcessNotification((Process) comp, user, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.TEAM_USER_REMOVED;
//                submitNotification = NotificationHelper.getTeamNotification((Team) comp, user, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.FORUM.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.FORUM_USER_REMOVED;
//                submitNotification = NotificationHelper.getForumNotification((Forum) comp, user, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//            {
//                notificationType = UserGlobalNotificationContainerType.USER_UNFOLLOW_ME;
//                //submitNotification = NotificationHelper.getUserNotification((User) comp, user, notificationType, user.getUsername(), false, true);
//                submitNotification = NotificationHelper.getUserFollowNotification((User) comp, notificationType, user.getUsername(), false, true);
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.ACTIONTASK.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.AC;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.WORKSHEET.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.NAMESPACE.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.PROCESS_USER_ADDED;
//            }
//            else if(compType.equalsIgnoreCase(SocialRelationshipTypes.RSS.name()))
//            {
////                notificationType = UserGlobalNotificationContainerType.PROCESS_USER_ADDED;
//            }
//        }
        
        return submitNotification;
    }
    
//    private static void validateIfUserCanFollowThisComponent(User user, RSComponent comp) throws Exception
//    {
//        String type = comp.getType();
//
//        if (type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()) 
//                        || type.equalsIgnoreCase(SocialRelationshipTypes.NAMESPACE.name()) 
//                        || type.equalsIgnoreCase(SocialRelationshipTypes.WORKSHEET.name()))
//        {
//            //ignore
//        }
//        else
//        {
//            String compRoles = SocialCompConversionUtil.convertSocialRolesStringToRoles(comp.getEditRoles() + " " + comp.getRoles());
//            String userRolesStr = SocialCompConversionUtil.convertSocialRolesStringToRoles(user.getRoles());
//            Set<String> userRoles = new HashSet<String>(StringUtils.convertStringToList(userRolesStr, ","));
//
//            boolean hasRole = UserUtils.hasRole(user.getUsername(), userRoles, compRoles);
//            if (!hasRole)
//            {
//                throw new Exception("User " + user.getUsername() + " does not have rights to follow comp " + comp.getDisplayName());
//            }
//        }
//
//    }
    
    private static void unfollow(Node thisNode, Node otherNode) throws Exception
    {

        Transaction tx = null;

//        try
//        {
//            String otherNodeSysId = (String) otherNode.getProperty(ResolveCatalog.SYS_ID);
//            if(thisNode != null && otherNode != null && StringUtils.isNotBlank(otherNodeSysId))
//            {
//                tx = GraphDBUtil.getGraphDBService().beginTx();
//
//                Iterable<Relationship> rels = thisNode.getRelationships(Direction.OUTGOING);
//                if(rels != null)
//                {
//                    for(Relationship rel: rels)
//                    {
//                        Node endNode = rel.getEndNode();
//
////                        if(endNode != null 
////                            && endNode.hasProperty(ResolveCatalog.SYS_ID)
////                            && endNode.getProperty(ResolveCatalog.SYS_ID).equals(otherNodeSysId))
////                        {
////                            rel.delete();
////                        }
//                    }
//                }
//                tx.success();
//            }
//
//
//        }
//        catch(Exception t)
//        {
//            Log.log.warn("deleteRelation catched exception: " + t, t);
//            throw t;
//        }
//        finally
//        {
//            if(tx != null)
//            {
//                tx.finish();
//            }
//        }
    }
    
    

}
