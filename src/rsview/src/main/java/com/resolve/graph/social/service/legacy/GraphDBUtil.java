/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;


import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.graphdb.index.ReadableIndex;

import com.resolve.graph.GraphManager;
import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.util.Log;


/**
 *  this class provide util methods such as get graph service, get index objects.
 *
 */
public class GraphDBUtil
{   
    private static final String DB_PATH = "C:\\project\\resolve\\dist\\tomcat\\webapps\\resolve\\WEB-INF\\neo4j-store";
    private static GraphDatabaseService graphDBService;
    private static ReadableIndex<Node> autoNodeIndex;
    private static Index<Node> postIndex;
    public static final String POST = "post";
    //private static ReadableIndex<Relationship> autoRelIndex;
    
    public static void init()
    {
        //graphDBService = new EmbeddedGraphDatabase(DB_PATH);
        getGraphDBService();
    }
    
    /**
     * for migration purpose...after migration the id of the old reference node is no longer 0 so the deprecated getReferenceNode api may cause issues.
     * @return
     */
    @Deprecated
    public static Node getReferenceNode() {
        IndexHits<Node> hit = GraphDBUtil.getAutoNodeIndex().get(ResolveGraphNode.RESOLVE_GRAPH_ROOT,true);
        Node reference = null;
        if(!hit.hasNext())
            reference = GraphManager.getInstance().getGraphDB().getReferenceNode();
        else
            reference = hit.next();
        return reference;
    }
    
    public static GraphDatabaseService getGraphDBService()
    {
        graphDBService = GraphManager.getInstance().graphDB; //Main.graphDB; 
        
        if(graphDBService == null)
        {
            graphDBService = GraphManager.getInstance().graphDB; //Main.graphDB; 
                //new EmbeddedGraphDatabase(DB_PATH);
            
//            Map<String, String> config = new HashMap<String, String>();
//            config.put( Config.NODE_KEYS_INDEXABLE, RSComponent.SYS_ID + ", " + Post.TITLE_URL);
//            //config.put( Config.RELATIONSHIP_KEYS_INDEXABLE, "xyz");
//            config.put( Config.NODE_AUTO_INDEXING, "true" );
//            config.put( Config.RELATIONSHIP_AUTO_INDEXING, "true" );
//            
//            graphDBService = new EmbeddedGraphDatabase(DB_PATH, config);
            
            if(graphDBService == null)
            {
                Log.log.error("GraphDB not initialized", new Exception());
            }
            else
            {    
                autoNodeIndex = graphDBService.index().getNodeAutoIndexer().getAutoIndex();
                postIndex = graphDBService.index().forNodes(POST);
            }
            //autoRelIndex = graphDBService.index().getRelationshipAutoIndexer().getAutoIndex();
        }
        else
        {
            autoNodeIndex = graphDBService.index().getNodeAutoIndexer().getAutoIndex();
            postIndex = graphDBService.index().forNodes(POST);
        }
        
        return graphDBService;
    }
    
    public static ReadableIndex<Node> getAutoNodeIndex()
    {
        getGraphDBService();
        return autoNodeIndex;
    }
    
    public static Index<Node> getPostIndex()
    {
        getGraphDBService();
        return postIndex;
    }
    
    public static void registerShutdownHookForNeo()
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running example before it's completed)
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                shutdown();
            }
        } );
    }
    
    public static void shutdown()
    {
        if ( graphDBService != null )
        {
            shutdownGraphDb();
        }
    }
    
    private static void deleteUserContainerNodeSpace()
    {        
        Transaction tx = graphDBService.beginTx();
        
        try
        {          
            Node userContainerNode = graphDBService.getReferenceNode().getSingleRelationship(SocialRootRelationshipTypes.USERS, Direction.OUTGOING).getEndNode();
            
            for(Relationship userContainerUserRel : userContainerNode.getRelationships(SocialRelationshipTypes.USER, Direction.OUTGOING))
            {
                Node userNode = userContainerUserRel.getEndNode();
                
                for(Relationship userRel: userNode.getRelationships(RelationType.FOLLOWER))
                {
                    userRel.delete();
                }
                
                userNode.delete();
                userContainerUserRel.delete();
            }
            
            userContainerNode.getSingleRelationship(SocialRootRelationshipTypes.USERS, Direction.OUTGOING).delete();
            userContainerNode.delete();
            
            tx.success();
        }
        catch(Throwable t)
        {
            t.printStackTrace();
        }
        finally
        {
            tx.finish();
        }
    }
    
    private static void shutdownGraphDb()
    {
        System.out.println( "Shutting down graph database ..." );
        graphDBService.shutdown();
        graphDBService = null;
    }
    
}
