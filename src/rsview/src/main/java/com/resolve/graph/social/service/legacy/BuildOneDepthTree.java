/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.graphdb.Node;

import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;


public class BuildOneDepthTree
{
    public static void buildOneDepthUserTree(Node endNode, Collection<User> users, int depth)
    {   
        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()) && depth ==1)
        {
//            User userLocal = new User();
//            userLocal.convertNodeToObject(endNode);
//            userLocal.setIsDirectFollowFlag(true);
//            users.add(userLocal);
        }
    }
    
    public static void buildOneDepthRssTree(Node endNode, Collection<Rss> rsss, int depth, Set<String> rssIDs)
    {   
        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RSS.name()) && depth ==1)
        {
//            Rss rssLocal = new Rss();
//            rssLocal.convertNodeToObject(endNode);
//            
//            if(!rssIDs.contains(rssLocal.getSys_id()))
//            {
//                rssLocal.setIsDirectFollowFlag(true);
//                rsss.add(rssLocal);
//                rssIDs.add(rssLocal.getSys_id());
//            }
        }
    }
    
    public static void buildOneDepthForumTree(Node endNode, Collection<Forum> forums, int depth, Set<String> forumIDSet)
    {   
        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()) && depth ==1)
        {
//            Forum forumLocal = new Forum();
//            forumLocal.convertNodeToObject(endNode);
//            
//            if(!forumIDSet.contains(forumLocal.getSys_id()))
//            {
//                forumLocal.setIsDirectFollowFlag(true);
//                forums.add(forumLocal);
//                forumIDSet.add(forumLocal.getSys_id());
//            }
        }
    }
    
    public static void buildOneDepthNameSpaceTree(Node endNode, Collection<Namespace> namespaces, int depth, Set<String> namespaceIDSet)
    {   
        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.NAMESPACE.name()) && depth ==1)
        {
//            Namespace namespaceLocal = new Namespace();
//            namespaceLocal.convertNodeToObject(endNode);
//            
//            if(!namespaceIDSet.contains(namespaceLocal.getSys_id()))
//            {
//                namespaceLocal.setIsDirectFollowFlag(true);
//                namespaces.add(namespaceLocal);
//                namespaceIDSet.add(namespaceLocal.getSys_id());
//            }
        }
    }
    
    public static void buildOneDepthRunbookTree(Node endNode, Collection<Runbook> runbooks, int depth, Set<String> runbookIDs)
    {   
        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RUNBOOK.name()) && depth ==1)
        {
//            Runbook runbookLocal = new Runbook();
//            runbookLocal.convertNodeToObject(endNode);
//            
//            if(!runbookIDs.contains(runbookLocal.getSys_id()))
//            {
//                runbookLocal.setIsDirectFollowFlag(true);
//                runbooks.add(runbookLocal);
//                runbookIDs.add(runbookLocal.getSys_id());
//            }
        }
    }
    
    public static void buildOneDepthDecisionTreeTree(Node endNode, Collection<DecisionTree> dts, int depth, Set<String> dtIDs)
    {   
//        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DECISIONTREE.name()) && depth ==1)
//        {
//            DecisionTree dtLocal = new DecisionTree();
//            dtLocal.convertNodeToObject(endNode);
//            
//            if(!dtIDs.contains(dtLocal.getSys_id()))
//            {
//                dtLocal.setIsDirectFollowFlag(true);
//                dts.add(dtLocal);
//                dtIDs.add(dtLocal.getSys_id());
//            }
//        }
    }
    
    public static void buildOneDepthDocumentTree(Node endNode, Collection<Document> documents, int depth, Set<String> docIDs)
    {   
//        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DOCUMENT.name()) && depth ==1)
//        {
//            Document documentLocal = new Document();
//            documentLocal.convertNodeToObject(endNode);
//            
//            if(!docIDs.contains(documentLocal.getSys_id()))
//            {
//                documentLocal.setIsDirectFollowFlag(true);
//                documents.add(documentLocal);
//                docIDs.add(documentLocal.getSys_id());
//            }
//        }
    }
    
    public static void buildOneDepthActionTaskTree(Node endNode, 
                                                   Collection<ActionTask> actiontasks, 
                                                   int depth, 
                                                   Set<String> actionTaskIDs) 
    {   
//        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.ACTIONTASK.name()) && depth ==1) 
//        {            
//            ActionTask atLocal = new ActionTask();
//            atLocal.convertNodeToObject(endNode);
//            
//            if(!actionTaskIDs.contains(atLocal.getSys_id()))
//            {
//                atLocal.setIsDirectFollowFlag(true);
//                actiontasks.add(atLocal);
//                actionTaskIDs.add(atLocal.getSys_id());
//            }
//        }
    }
    
    public static void buildOneDepthWorkSheetTree(Node endNode, 
                                                    Collection<Worksheet> worksheets, 
                                                    int depth, 
                                                    Set<String> worksheetIDs) 
    {   
//        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.WORKSHEET.name()) && depth ==1) 
//        {            
//            Worksheet worksheetLocal = new Worksheet();
//            worksheetLocal.convertNodeToObject(endNode);
//            
//            if(!worksheetIDs.contains(worksheetLocal.getSys_id()))
//            {
//                worksheetLocal.setIsDirectFollowFlag(true);
//                worksheets.add(worksheetLocal);
//                worksheetIDs.add(worksheetLocal.getSys_id());
//            }
//        }
    }
    
    public static void buildOneDepthTree(Node endNode, 
                                         Collection<User> users, 
                                         Collection<ActionTask> actiontasks, 
                                         Collection<Runbook> runbooks, 
                                         Collection<DecisionTree> dts, 
                                         Collection<Document> documents, 
                                         Collection<Rss> rsss, 
                                         Collection<Forum> forums,
                                         Collection<Namespace> namespaces,
                                         int depth)
    {           
        Map<String, String> usersMap = new ConcurrentHashMap<String, String>();
        Map<String, String> actiontasksMap = new ConcurrentHashMap<String, String>();
        Map<String, String> runbooksMap = new ConcurrentHashMap<String, String>();
        Map<String, String> dtMap = new ConcurrentHashMap<String, String>();
        Map<String, String> documentsMap = new ConcurrentHashMap<String, String>();
        Map<String, String> rsssMap = new ConcurrentHashMap<String, String>();
        Map<String, String> forumsMap = new ConcurrentHashMap<String, String>();
        Map<String, String> namespaceMap = new ConcurrentHashMap<String, String>();
        
        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()) && depth ==1)
        {
            if(!usersMap.containsKey(Long.toString(endNode.getId())))
            {
//                User userLocal = new User();
//                userLocal.convertNodeToObject(endNode);
//                userLocal.setIsFollowing(true);
//                users.add(userLocal);
//                usersMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
            }
        }
        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.ACTIONTASK.name()) && depth ==1)
        {
            if(!actiontasksMap.containsKey(Long.toString(endNode.getId())))
            {
//                ActionTask atLocal = new ActionTask();
//                atLocal.convertNodeToObject(endNode);
//                atLocal.setIsFollowing(true);
//                actiontasks.add(atLocal);
//                actiontasksMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
            }
        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.RUNBOOK.name()) && depth ==1)
//        {
//            if(!runbooksMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Runbook runbookLocal = new Runbook();
//                runbookLocal.convertNodeToObject(endNode);
//                runbookLocal.setIsFollowing(true);
//                runbooks.add(runbookLocal);
//                runbooksMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialObjectType.DECISIONTREES.name()) && depth ==1)
//        {
//            if(!dtMap.containsKey(Long.toString(endNode.getId())))
//            {
//                DecisionTree dtLocal = new DecisionTree();
//                dtLocal.convertNodeToObject(endNode);
//                dtLocal.setIsFollowing(true);
//                dts.add(dtLocal);
//                dtMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DOCUMENT.name()) && depth ==1)
        {
            if(!documentsMap.containsKey(Long.toString(endNode.getId())))
            {
//                Document documentLocal = new Document();
//                documentLocal.convertNodeToObject(endNode);
//                documentLocal.setIsFollowing(true);
//                
//                documentsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                
//                //add only if its not marked deleted
//                if(!documentLocal.isMarkDeleted())
//                {
//                    documents.add(documentLocal);
//                    
//                    //if the document is runbook
//                    if(documentLocal.isRunbook())
//                    {
//                        if(!runbooksMap.containsKey(Long.toString(endNode.getId())))
//                        {
//                            Runbook runbookLocal = new Runbook();
//                            runbookLocal.convertNodeToObject(endNode);
//                            runbookLocal.setType(SocialRelationshipTypes.RUNBOOK.name());
//                            runbookLocal.setIsFollowing(true);
//                            runbooks.add(runbookLocal);
//                            runbooksMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                        }
//                    }
//                    
//                    //if the doc is DT
//                    if(documentLocal.isDecisionTree())
//                    {
//                        if(!dtMap.containsKey(Long.toString(endNode.getId())))
//                        {
//                            DecisionTree dtLocal = new DecisionTree();
//                            dtLocal.convertNodeToObject(endNode);
//                            dtLocal.setType(SocialRelationshipTypes.DECISIONTREE.name());
//                            dtLocal.setIsFollowing(true);
//                            dts.add(dtLocal);
//                            dtMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//                        }
//                    }
//                }
            }
        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RSS.name()) && depth ==1)
//        {
//            if(!rsssMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Rss rssLocal = new Rss();
//                rssLocal.convertNodeToObject(endNode);
//                rssLocal.setIsFollowing(true);
//                rsss.add(rssLocal);
//                rsssMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }   
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()) && depth ==1)
//        {
//            if(!forumsMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Forum forum = TraverseGraph.getForum(endNode);
//                forum.setIsFollowing(true);
//                forums.add(forum);
//                forumsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.NAMESPACE.name()) && depth ==1)
//        {
//            if(!namespaceMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Namespace namespace = new Namespace();
//                namespace.convertNodeToObject(endNode);
//                namespace.setIsFollowing(true);
//                namespaces.add(namespace);
//                namespaceMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
    }
    
    
    public static RSComponent buildComponent(Node endNode)
    {                   
        RSComponent rsComp = null;
        
        if (endNode != null && endNode.hasProperty(SocialFactory.NODE_TYPE))
        {
//            if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()))
//            {
//                User userLocal = new User();
//                userLocal.convertNodeToObject(endNode);
//                rsComp = userLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()))
//            {
//                Forum forumLocal = new Forum();
//                forumLocal.convertNodeToObject(endNode);
//                rsComp = forumLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//            {
//                Team teamLocal = new Team();
//                teamLocal.convertNodeToObject(endNode);
//                rsComp = teamLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()))
//            {
//                Process processLocal = new Process();
//                processLocal.convertNodeToObject(endNode);
//                rsComp = processLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.ACTIONTASK.name()))
//            {
//                ActionTask atLocal = new ActionTask();
//                atLocal.convertNodeToObject(endNode);
//                rsComp = atLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RUNBOOK.name()))
//            {
//                Runbook runbookLocal = new Runbook();
//                runbookLocal.convertNodeToObject(endNode);
//                rsComp = runbookLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DECISIONTREE.name()))
//            {
//                DecisionTree dtLocal = new DecisionTree();
//                dtLocal.convertNodeToObject(endNode);
//                rsComp = dtLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DOCUMENT.name()))
//            {
//                Document documentLocal = new Document();
//                documentLocal.convertNodeToObject(endNode);
//                rsComp = documentLocal;
//            }
//            else if (endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RSS.name()))
//            {
//                Rss rssLocal = new Rss();
//                rssLocal.convertNodeToObject(endNode);
//                rsComp = rssLocal;
//            }
        }
        return rsComp;
    }
    
    public static void buildOneDepthFavorite(Node endNode, 
                                             Collection<User> users, 
                                             Collection<ActionTask> actiontasks, 
                                             Collection<Runbook> runbooks,
                                             Collection<DecisionTree> dts, 
                                             Collection<Document> documents, 
                                             Collection<Rss> rsss, 
                                             Collection<Forum> forums, 
                                             Collection<Team> teams,
                                             Collection<Process> processes,
                                             int depth)
  {           
        Map<String, String> usersMap = new ConcurrentHashMap<String, String>();
        Map<String, String> actiontasksMap = new ConcurrentHashMap<String, String>();
        Map<String, String> runbooksMap = new ConcurrentHashMap<String, String>();
        Map<String, String> dtMap = new ConcurrentHashMap<String, String>();
        Map<String, String> documentsMap = new ConcurrentHashMap<String, String>();
        Map<String, String> rsssMap = new ConcurrentHashMap<String, String>();
        Map<String, String> forumsMap = new ConcurrentHashMap<String, String>();
        Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
        Map<String, String> processesMap = new ConcurrentHashMap<String, String>();

//        if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()) && depth ==1)
//        {
//            if(!usersMap.containsKey(Long.toString(endNode.getId())))
//            {
//                User userLocal = new User();
//                userLocal.convertNodeToObject(endNode);
//                users.add(userLocal);
//                usersMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.ACTIONTASK.name()) && depth ==1)
//        {
//            if(!actiontasksMap.containsKey(Long.toString(endNode.getId())))
//            {
//                ActionTask atLocal = new ActionTask();
//                atLocal.convertNodeToObject(endNode);
//                actiontasks.add(atLocal);
//                actiontasksMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RUNBOOK.name()) && depth ==1)
//        {
//            if(!runbooksMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Runbook runbookLocal = new Runbook();
//                runbookLocal.convertNodeToObject(endNode);
//                runbooks.add(runbookLocal);
//                runbooksMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DECISIONTREE.name()) && depth ==1)
//        {
//            if(!dtMap.containsKey(Long.toString(endNode.getId())))
//            {
//                DecisionTree dtLocal = new DecisionTree();
//                dtLocal.convertNodeToObject(endNode);
//                dts.add(dtLocal);
//                dtMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.DOCUMENT.name()) && depth ==1)
//        {
//            if(!documentsMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Document documentLocal = new Document();
//                documentLocal.convertNodeToObject(endNode);
//                documents.add(documentLocal);
//                documentsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.RSS.name()) && depth ==1)
//        {
//            if(!rsssMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Rss rssLocal = new Rss();
//                rssLocal.convertNodeToObject(endNode);
//                rsss.add(rssLocal);
//                rsssMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }   
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.FORUM.name()) && depth ==1)
//        {
//            if(!forumsMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Forum forum = TraverseGraph.getForum(endNode);
//                forums.add(forum);
//                forumsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()) && depth ==1)
//        {
//            if(!teamsMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Team teamLocal = new Team();
//                teamLocal.convertNodeToObject(endNode);
//                teams.add(teamLocal);
//                teamsMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
//        else if(endNode.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.PROCESS.name()) && depth ==1)
//        {
//            if(!processesMap.containsKey(Long.toString(endNode.getId())))
//            {
//                Process processLocal = new Process();
//                processLocal.convertNodeToObject(endNode);
//                processes.add(processLocal);
//                processesMap.put(Long.toString(endNode.getId()), Long.toString(endNode.getId()));
//            }
//        }
     }
}
