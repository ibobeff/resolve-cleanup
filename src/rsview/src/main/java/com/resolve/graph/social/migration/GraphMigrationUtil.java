/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.migration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.vo.GraphUserDTO;

public class GraphMigrationUtil
{

    //get info of all the users from the graph
    public static List<GraphUserDTO> getAllUsersDataFromGraph()
    {
        List<GraphUserDTO> results = new ArrayList<GraphUserDTO>();
        
        //get list of all the users
        Collection<User> users = SocialUsersFactory.getAllUsers();
        for(User user : users)
        {
//            Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//            if(userNode != null)
//            {
//                GraphUserDTO graphUserDTO = new GraphUserDTO();
//                graphUserDTO.setUser(user);
//                graphUserDTO.setLikePostIds(getLikePostIds(userNode));
//                graphUserDTO.setReadPostIds(getReadPostIds(userNode));
//                graphUserDTO.setStarredPostIds(getStarredPostIds(userNode));
//                
//                results.add(graphUserDTO);
//            }
        }
        
        return results;
    }
    
    //private apis
    private static Set<String> getReadPostIds(Node userNode)
    {
        Set<String> results = new HashSet<String>();
        
        Iterable<Relationship> rels = userNode.getRelationships(RelationType.READ);
        results.addAll(getStarredPostIds(userNode, rels));
        
        return results;
    }
    
    private static Set<String> getStarredPostIds(Node userNode)
    {
        Set<String> result = new HashSet<String>();
        
        Iterable<Relationship> rels = userNode.getRelationships(RelationType.STARRED);
        result.addAll(getStarredPostIds(userNode, rels));
        
        return result;
    }
    
    private static Set<String> getLikePostIds(Node userNode)
    {
        Set<String> result = new HashSet<String>();
        
        Iterable<Relationship> rels = userNode.getRelationships(RelationType.LIKE);
        result.addAll(getStarredPostIds(userNode, rels));
        
        return result;
    }
    
    
    private static Set<String> getStarredPostIds(Node userNode, Iterable<Relationship> rels)
    {
        Set<String> result = new HashSet<String>();
        
        for(Relationship rel : rels)
        {
//            Node postNode = rel.getOtherNode(userNode);
//            if(postNode.hasProperty(RSComponent.SYS_ID))
//            {
//                result.add((String) postNode.getProperty(RSComponent.SYS_ID));
//            }
        }
        
        return result;
    }    
    
    
    
}
