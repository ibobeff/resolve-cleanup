/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.export;

import java.util.List;

import org.neo4j.graphdb.Node;

import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.util.TraversalUtil;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.GraphRelationshipDTO;

public class ExportActiontaskGraph extends ExportComponentGraph
{
    public ExportActiontaskGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
       super(sysId, options, SocialRelationshipTypes.ACTIONTASK);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode, SocialImpexVO socialImpex) throws Exception
    {
        //get all the tags for this actiontask node
        Iterable<Node> tagsForActiontask = TraversalUtil.findTagsFor(compNode);
        if(tagsForActiontask != null)
        {
            for(Node anyNode : tagsForActiontask)
            {
                addRelationship(compNode, anyNode);
            }//end of for
        }
        
        
        return relationships;
    }
}
