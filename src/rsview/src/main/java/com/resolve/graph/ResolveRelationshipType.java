/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph;

import org.neo4j.graphdb.RelationshipType;

public enum ResolveRelationshipType  implements RelationshipType
{
    //TODO: consolidate the Relationship types to this file - all files from com.resolve.graph.social.constants
    //tags
    ResolveTag
}
