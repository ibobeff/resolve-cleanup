/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialActionTaskFactory;
import com.resolve.graph.social.factory.SocialDocumentFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.factory.SocialForumFactory;
import com.resolve.graph.social.factory.SocialProcessFactory;
import com.resolve.graph.social.factory.SocialRssFactory;
import com.resolve.graph.social.factory.SocialTeamFactory;
import com.resolve.graph.social.factory.SocialUsersFactory;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialConfig
{    
    public static SocialUsersFactory socialUsersFactory = new SocialUsersFactory();
    public static SocialForumFactory socialForumFactory = new SocialForumFactory();
    public static SocialTeamFactory socialTeamFactory = new SocialTeamFactory();
    public static SocialProcessFactory socialProcessFactory = new SocialProcessFactory();
    public static SocialActionTaskFactory socialActionTaskFactory = new SocialActionTaskFactory();
    public static SocialDocumentFactory socialDocumentFactory = new SocialDocumentFactory();
    public static SocialRssFactory socialRssFactory = new SocialRssFactory();
    public static SocialPostFactory socialPostFactory = new SocialPostFactory();
    
//    public static Collection<User> getAllUsers(boolean nochild)
//    {
//        Collection<User> users = new ArrayList<User>();
//        
//        try
//        {
//            if(nochild)
//            {
//                users = socialUsersFactory.getAllUsers();
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.warn(t.getMessage(), t);
//        }
//        
//        return users;
//    }
//    
//    public static Collection<Team> getAllTeams(boolean nochild)
//    {
//        Collection<Team> teams  = new ArrayList<Team>();
//        
//        try
//        {
//            if(nochild)
//            {
//                teams = socialTeamFactory.getAllTeams();
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.warn(t.getMessage(), t);
//        }
//        
//        return teams;
//    }
//    
//    public static Team getTeam(String teamID, boolean nochild)
//    {
//        Team team = new Team();
//        Node teamNode = null;
//        
//        if(StringUtils.isNotEmpty(teamID))
//        {
//            teamNode = ResolveGraphFactory.findNodeByIndexedID(teamID);
//            
//            if(teamNode != null)
//            {
//                team = getTeam(teamNode, nochild);
//            }
//        }        
//        
//        return team;
//    }
//    
//    public static Set<User> getUsersInThisTeam(String teamID, boolean nochild)
//    {
//        Set<User> results = new HashSet<User>();
//        Team team = null;
//        
//        try
//        {
//            team = getTeam(teamID, nochild);
//            
//            if(team != null)
//            {
//                results.addAll((team.getUsers() != null)?team.getUsers():new ArrayList());
//                
//                if(!nochild)
//                {
//                     Collection<Team> teams = team.getTeams();  
//                     
//                     if(teams != null)
//                     {
//                         for(Team teamLocal: teams)
//                         {
//                             Set<User> userOfThisTeam = new HashSet<User>();
//                             getUserInTeam(teamLocal, userOfThisTeam);
//                             results.addAll(userOfThisTeam);
//                         }
//                     }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info(t, t);
//        }
//        
//        return results;
//    }
//    
//    public static void getUserInTeam(Team team, Set<User> users)
//    {
//        try
//        {
//            if(team != null)
//            {
//                users.addAll((team.getUsers() != null)?team.getUsers():new ArrayList());
//                
//                Collection<Team> teams = team.getTeams();
//                
//                if(teams != null)
//                {
//                    for(Team teamLocal: teams)
//                    {
//                        getUserInTeam(teamLocal, users);
//                    }
//                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//    }
//   
//    public static Set<String> getParentsOfTeam(String teamID)
//    {
//        Set<String> parents = new HashSet<String>();
//        Node teamNode = null;
//        
//        if(StringUtils.isNotEmpty(teamID))
//        {
//            teamNode = ResolveGraphFactory.findNodeByIndexedID(teamID);
//            
//            TraversalDescription trav = Traversal.description()
//                                            .breadthFirst()
//                                            .relationships(RelationType.MEMBER, Direction.OUTGOING)
//                                            .evaluator(Evaluators.excludeStartPosition());
//            
//            for(Node nodeLocal: trav.traverse(teamNode).nodes())
//            {
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                {
//                    parents.add((String) nodeLocal.getProperty(RSComponent.SYS_ID));
//                }
//            }            
//        }
//        
//        return parents;
//    }
//    
//    public static Set<String> getParentsOfTeamNode(Node teamNode)
//    {
//        Set<String> parents = new HashSet<String>();
//        
//        if(teamNode != null)
//        {
//
//            TraversalDescription trav = Traversal.description()
//                                                    .breadthFirst()
//                                                    .relationships(RelationType.MEMBER, Direction.OUTGOING)
//                                                    .evaluator(Evaluators.excludeStartPosition());
//        
//            for(Node nodeLocal: trav.traverse(teamNode).nodes())
//            {
//                if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                {
//                    parents.add((String) nodeLocal.getProperty(RSComponent.SYS_ID));
//                }
//            }            
//        }
//        
//        return parents;
//    }
//    
//    public static Process getProcess(String processID, boolean nochild)
//    {
//        Process process = new Process();
//        Node processNode = null;
//       
//        if(StringUtils.isNotEmpty(processID))
//        {
//            processNode = ResolveGraphFactory.findNodeByIndexedID(processID);
//            
//            if(processNode != null)
//            {
//                process = getProcess(processNode, nochild);
//            }
//        }
//               
//        return process;
//    }
//    
//    public static Set<User> getUsersInThisProcess(String processID, boolean nochild)
//    {
//        Set<User> results = new HashSet<User>();
//        
//        try
//        {
//            Process process = getProcess(processID, nochild);
//            
//            if(process != null)
//            {
//                
//                results.addAll((process.getUsers() != null)?process.getUsers(): new ArrayList());
//                
//                if(!nochild)
//                {
//                    Collection<Team> teams = process.getTeams();
//                    
//                    if(teams != null)
//                    {
//                        for(Team teamLocal: teams)
//                        {
//                            String teamsLocalID = teamLocal.getSys_id();
//                            
//                            if(StringUtils.isNotEmpty(teamsLocalID))
//                            {
//                                Set<User> thisTeamUsers = getUsersInThisTeam(teamsLocalID, nochild);
//                                
//                                if(thisTeamUsers != null)
//                                {
//                                    results.addAll(thisTeamUsers);
//                                }
//                            }
//                        }
//                    }
//                }
//                
//                Log.log.info("--- the total users in this process is: " + results.size());
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.info(t, t);
//        }
//        
//        return results;
//    }
//    
//    
//    public static Forum getForum(String forumID, boolean nochild)
//    {
//        Forum forum = new Forum();
//        Node forumNode = null;
//        Collection<User> users = new ArrayList<User>();
//        User user = new User();
//        
//        if(StringUtils.isNotEmpty(forumID))
//        {
//            forumNode = ResolveGraphFactory.findNodeByIndexedID(forumID);
//            
//            if(forumNode != null)
//            {
//
//                forum.convertNodeToObject(forumNode);
//                TraversalDescription trav = Traversal.description()
//                                                        .breadthFirst()
//                                                        .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                        .evaluator(Evaluators.excludeStartPosition());
//                
//                for(Node userNodeLocal: trav.traverse(forumNode).nodes())
//                {
//                    if(userNodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()))
//                    {
//                        user = new User();
//                        user.convertNodeToObject(userNodeLocal);
//                        users.add(user);
//                    }
//                }
//                
//            }
//        }
//        
//        forum.setUsers(users);
//        return forum;
//        
//    }
//    
//    public static Set<User> getUsersInThisForum(String processID)
//    {
//        Set<User> results = new HashSet<User>();
//        
//        try
//        {
//            Forum forum = getForum(processID, false);
//            
//            if(forum != null && forum.getUsers() != null && !forum.getUsers().isEmpty())
//            {
//                results.addAll(forum.getUsers());
//            }
//            
//            Log.log.info("--- total user number in process is : " + results.size());
//        }
//        catch(Throwable t)
//        {
//            Log.log.info(t, t);
//        }
//        
//        return results;
//    }
//    
//    public static Process getProcess(Node processNode, boolean nochild)
//    {
//        Process process = new Process();
//        
//        if(!nochild)
//        {
//            process = TraverseGraph.getProcess(processNode);
//        }
//        else
//        {
//            Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//            
//            Collection<Team> teams = new ArrayList<Team>();
//            Collection<User> users = new ArrayList<User>();
//            Collection<Forum> forums = new ArrayList<Forum>();
//            Collection<Namespace> namespaces = new ArrayList<Namespace>();
//            Collection<Runbook> runbooks = new ArrayList<Runbook>();
//            Collection<DecisionTree> dts = new ArrayList<DecisionTree>();
//            Collection<Document> documents = new ArrayList<Document>();
//            Collection<ActionTask> actiontasks = new ArrayList<ActionTask>();
//            Collection<Rss> rsss = new ArrayList<Rss>();
//            
//            if(processNode != null)
//            {
//
//                TraversalDescription trav = Traversal.description()
//                                                        .breadthFirst()
//                                                        .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                        .evaluator(Evaluators.excludeStartPosition())
//                                                        .evaluator(Evaluators.atDepth(1));
//                
//                
//                for(Node nodeLocal: trav.traverse(processNode).nodes())
//                {    
//                    
//                    BuildOneDepthTree.buildOneDepthTree(nodeLocal, users, actiontasks, runbooks, dts, documents, rsss, forums, namespaces, 1);
//                    
//                    if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name()))
//                    {
//                        if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
//                        {
//                            Team team = new Team();
//                            team.convertNodeToObject(nodeLocal);
//                            teams.add(team);
//                            teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                        }
//                    }
//                }
//            }
//            
//            process.convertNodeToObject(processNode);
//            process.setTeams(teams);
//            process.setForums(forums);
//            process.setNameSpaces(namespaces);
//            process.setUsers(users);
//            process.setRunbooks(runbooks);
//            process.setDecisionTrees(dts);
//            process.setDocuments(documents);
//            process.setActiontasks(actiontasks);
//            process.setRsss(rsss);
//        }
//        
//        return process;
//    }
//    
//    public static Team getTeam(Node teamNode, boolean nochild)
//    {
//        Team team = new Team();
//        
//        if(!nochild)
//        {
//            team = TraverseGraph.getTeam(teamNode);
//        }
//        else
//        {
//            Map<String, String> teamsMap = new ConcurrentHashMap<String, String>();
//            Map<String, String> usersMap = new ConcurrentHashMap<String, String>();
//            
//            Collection<Team> teams = new ArrayList<Team>();
//            Collection<User> users = new ArrayList<User>();
//            
//            if(teamNode != null)
//            {
//                
//                TraversalDescription trav = Traversal.description()
//                                                        .breadthFirst()
//                                                        .relationships(RelationType.MEMBER, Direction.INCOMING)
//                                                        .evaluator(Evaluators.excludeStartPosition())
//                                                        .evaluator(Evaluators.atDepth(1));
//                
//                
//                for(Node nodeLocal: trav.traverse(teamNode).nodes())
//                {       
//                    
//                    if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name())) 
//                    {
//                        if(!usersMap.containsKey(Long.toString(nodeLocal.getId())))
//                        {
//                            User user = new User();
//                            user.convertNodeToObject(nodeLocal);
//                            users.add(user);
//                            usersMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                        }
//                    }
//                    else if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.TEAM.name())) 
//                    {
//                        if(!teamsMap.containsKey(Long.toString(nodeLocal.getId())))
//                        {
//                            Team teamLocal = new Team();
//                            teamLocal.convertNodeToObject(nodeLocal);
//                            teams.add(teamLocal);
//                            teamsMap.put(Long.toString(nodeLocal.getId()), Long.toString(nodeLocal.getId()));
//                        }
//                    }
//                }
//                
//                team.convertNodeToObject(teamNode);
//                team.setUsers(users);
//                team.setTeams(teams);
//            }
//        }
//        
//        return team;
//    }
//    
//   
//    public static Collection<Process> getAllProcess(boolean nochild)
//    {
//        Collection<Process> processes = new ArrayList<Process>();
//        
//        try
//        {
//            if(nochild)
//            {
//                processes = socialProcessFactory.getAllProcess();
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.warn(t.getMessage(), t);
//        }
//        
//        return processes;
//    }
//    
//    public static Collection<User> getAllFollowersOfRss(Rss rss)
//    {
//        Collection<User> users = new ArrayList<User>();
//        Set<String> userNames = new HashSet<String>();
//        
//        Node rssNode = null;
//        
//        try
//        {
//            if(rss != null && StringUtils.isNotEmpty(rss.getSys_id()))
//            {
//                rssNode = ResolveGraphFactory.findNodeByIndexedID(rss.getSys_id());
//                
//                if(rssNode != null)
//                {
//
//                    TraversalDescription trav = Traversal.description()
//                                                            .breadthFirst()
//                                                            .relationships(RelationType.FOLLOWER, Direction.INCOMING)
//                                                            .evaluator(Evaluators.excludeStartPosition());
//                                                            
//                    for(Node nodeLocal: trav.traverse(rssNode).nodes())
//                    {
//                        if(nodeLocal.getProperty(SocialFactory.NODE_TYPE).equals(SocialRelationshipTypes.USER.name()))
//                        {
//                            if(!userNames.contains(nodeLocal.getId()))
//                            {
//                                User user = new User();
//                                user.convertNodeToObject(nodeLocal);
//                                users.add(user);
//                                userNames.add(Long.toString(nodeLocal.getId()));
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        catch(Throwable t)
//        {
//            Log.log.warn(t.getMessage(), t);
//        }
//        
//        return users;
//        
//    }
//    
//    public static Rss getRss(String rssId)
//    {
//        Rss rss = new Rss();
//        Node rssNode = null;
//
//        if (StringUtils.isNotEmpty(rssId))
//        {
//            rssNode = ResolveGraphFactory.findNodeByIndexedID(rssId);
//
//            if (rssNode != null)
//            {
//                rss.convertNodeToObject(rssNode);
//            }
//        }
//
//        return rss;
//    }
}
