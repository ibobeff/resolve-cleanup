/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util.legacy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.dto.SocialComponentType;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;


public class NotificationConfUtil
{
    private SocialComponentType type;
    private RSComponent componentType = null;
    private User user = null;
    
    private Map<String,  List<NotificationConfig>> cache = new HashMap<String,  List<NotificationConfig>>(); 
    
    //for specifc comp notification update
    Map<String, ComponentNotification> userSpecficNotification = new HashMap<String, ComponentNotification>();
    Map<String, ComponentNotification> userDefaultNotification = new HashMap<String, ComponentNotification>();
    
    public NotificationConfUtil()
    {
        // No Aurgument Constructor
    }
    
    public NotificationConfUtil(String type, String userName)
    {
        setSocialComponentType(type);
        user = SocialCompConversionUtil.getSocialUser(userName);
    }
    
//    public List<BaseTreeModel> getSpecificNotificationsForComponents(List<? extends ResolveBaseModel> components)
//    {
//        List<BaseTreeModel> result = new ArrayList<BaseTreeModel>();
//        
//        if(componentType != null)
//        {
//            List<NotificationConfig> allSpecificNotifications = new ArrayList();//ServiceGraph.getCompNotificationType(componentType);
//            if(allSpecificNotifications !=null )
//            {
//                for(NotificationConfig notification : allSpecificNotifications)
//                {
//                    String displayName = notification.getDisplayName();
//                    if (displayName.equalsIgnoreCase("create"))
//                    {
//                    	continue;
//                    }
//                    String notifyKey = notification.getType().name();
//                    
//                  //create a list of children
//                    List<BaseTreeModel> children = new ArrayList<BaseTreeModel>();
//                    for(ResolveBaseModel compObj : components)
//                    {
//                        String compId = compObj.get(TeamBaseModel.SYS_ID);
//                        String compDisplayName = compObj.get(TeamBaseModel.U_DISPLAY_NAME);
//                        
//                        //reference can be reused
//                        componentType = SocialUtil.getRSComponentBasedOnType(type, compId, compDisplayName);
//                        
//                        NotifyBaseTreeModel treeModel = new NotifyBaseTreeModel(compDisplayName, null);
//                        treeModel.setRSCompType(type.getTagName());
//                        treeModel.setRSCompSysId(compId);
//                        treeModel.setNotifyEventType(notifyKey);
//
//                        //query the graph to find out if this was selected or not
//                        setNotifyFlagForComponent(notifyKey, treeModel);
//                        
//                        children.add(treeModel);
//                    }
//                    
//                    //create a notify folder and add children
//                    Folder notify = new Folder(displayName, children.toArray(new BaseTreeModel[children.size()]));
//                    notify.setDesc(notifyKey);//desc use to save the key
//                    
//                    //add it to the root
//                    result.add(notify);
//                }                
//            }          
//        }
//        
//        return result;
//        
//    }
    
//    public List<NotifyBaseTreeModel> getSpecificNotificationsForComponentsV2(List<? extends BaseModel> components)
//    {
//        List<NotifyBaseTreeModel> result = new ArrayList<NotifyBaseTreeModel>();
//        
//        String comboSelection = "False";
//        
//        if(componentType != null)
//        {
//            List<NotificationConfig> allSpecificNotifications = new ArrayList();//ServiceGraph.getCompNotificationType(componentType);
//            if(allSpecificNotifications !=null )
//            {
//                for(NotificationConfig notification : allSpecificNotifications)
//                {
//                    String displayName = notification.getDisplayName();
//                    if (displayName.equalsIgnoreCase("create"))
//                    {
//                        continue;
//                    }
//                    String notifyKey = notification.getType().name();
//                    
//                    for(BaseModel compObj : components)
//                    {
//                        ResolveBaseModel model = new ResolveBaseModel();
//                        model.setProperties(compObj.getProperties());
//                        
//                        String compId = model.get(TeamBaseModel.SYS_ID);
//                        String compDisplayName = model.get(TeamBaseModel.U_DISPLAY_NAME);
//                        
//                        //reference can be reused
//                        componentType = SocialUtil.getRSComponentBasedOnType(type, compId, compDisplayName);
//                        
//                        NotifyBaseTreeModel notifyBaseTreeModel = new NotifyBaseTreeModel(compDisplayName, null);
//                        notifyBaseTreeModel.setRSCompType(type.getTagName());
//                        notifyBaseTreeModel.setSpecificNotifyDisplayName(displayName);
//                        notifyBaseTreeModel.setRSCompSysId(compId);
//                        notifyBaseTreeModel.setNotifyEventType(notifyKey);
//                        notifyBaseTreeModel.setSpecificsComboSelection("Global Default");
//
//                        //query the graph to find out if this was selected or not
//                        NotificationConfig notificationConfig = null;//ServiceGraph.getCompSpecialNotificationTypeForUser(user, componentType, notification.getType());
//                        if (notificationConfig != null)
//                        {
//                            notifyBaseTreeModel.setSpecificsComboSelection(notificationConfig.getValue().equalsIgnoreCase("true") ? "True":"False");
//                        }
//                        setNotifyFlagForComponent(notifyKey, notifyBaseTreeModel);
//                        result.add(notifyBaseTreeModel);
//                    }
//                }                
//            }          
//        }
//        
//        return result;
//        
//    }
    
//    public List<BaseTreeModel> getGlobalNotifications(User socialUser)
//    {
//        List<BaseTreeModel> result = new ArrayList<BaseTreeModel>();
//        
//        List <ComponentNotification> notificationTypeList = ServiceGraph.getAllNotificationType();
//        
//        for (ComponentNotification compNotification : notificationTypeList)
//        {
//            String notifyKey = compNotification.getRSComponent().getDisplayName();
//            String displayName = compNotification.getRSComponent().getDisplayName();
//            
//            List<NotificationConfig> allSpecificNotifications = compNotification.getNotifyTypes();
//            if(allSpecificNotifications !=null )
//            {
//                List<BaseTreeModel> children = new ArrayList<BaseTreeModel>();
//                
//                for(NotificationConfig notification : allSpecificNotifications)
//                {
//                    if (displayName.equalsIgnoreCase("create"))
//                    {
//                        continue;
//                    }
//                    String compId = notification.getType().name();
//                    String compDisplayName = notification.getDisplayName();
//                    NotifyBaseTreeModel treeModel = new NotifyBaseTreeModel(compDisplayName, null);
//                    treeModel.setRSCompType(notifyKey);
//                    treeModel.setRSCompSysId(compId);
//                    treeModel.setNotifyEventType(notifyKey);
////                    treeModel.setDefaultSelected(true);
//                    
//                    NotificationConfig notificationConfig = null;// ServiceGraph.getUserNotificationType(socialUser, UserGlobalNotificationContainerType.valueOf(compId));
//                    if (notificationConfig != null)
//                    {
//                        treeModel.setUserSelected(Boolean.valueOf(notificationConfig.getValue()));
//                        treeModel.setDefaultSelected(false);
//                    }
//                    
//                    children.add(treeModel);
//                }
//                Folder notify = new Folder(displayName, children.toArray(new BaseTreeModel[children.size()]));
//                notify.setDesc(notifyKey);//desc use to save the key
//                
//                //add it to the root
//                result.add(notify);
//            }
//            
//        }
//        
//        return result;
//    }
    
//    public Map<String, List<NotifyBaseTreeModel>> getGlobalNotificationsV2(User socialUser)
//    {
//        Map<String, List<NotifyBaseTreeModel>> result = new HashMap<String, List<NotifyBaseTreeModel>>();
//        
//        List <ComponentNotification> notificationTypeList = ServiceGraph.getAllNotificationType();
//        
//        List<NotifyBaseTreeModel> allNotify = new ArrayList<NotifyBaseTreeModel>();
//        List<NotifyBaseTreeModel> checkedNotify = new ArrayList<NotifyBaseTreeModel>();
//        
//        for (ComponentNotification compNotification : notificationTypeList)
//        {
//            List<NotificationConfig> allSpecificNotifications = compNotification.getNotifyTypes();
//            if(allSpecificNotifications !=null )
//            {
//                for(NotificationConfig notification : allSpecificNotifications)
//                {
////                    if (notification.getDisplayName().equalsIgnoreCase("create"))
////                    {
////                        continue;
////                    }
//                    NotifyBaseTreeModel model = new NotifyBaseTreeModel();
//                    model.setRSCompDisplayName(compNotification.getRSComponent().getDisplayName());
//                    model.setRSCompType(compNotification.getRSComponent().getDisplayName());
//                    model.setRSCompSysId(notification.getType().name());
//                    model.setNotifyEventType(notification.getDisplayName());
//                    
//                    
//                    NotificationConfig notificationConfig = null;//ServiceGraph.getUserNotificationType(socialUser, UserGlobalNotificationContainerType.valueOf(notification.getType().name()));
//                    if (notificationConfig != null)
//                    {
//                        if (notificationConfig.getValue().equalsIgnoreCase("true"))
//                        {
//                            checkedNotify.add(model);
//                            model.setUserSelected(true);
//                        }
//                    }
//                    allNotify.add(model);
//                }
//            }
//        }
//        
//        result.put("CHECKED_NOTIFY_MODEL", checkedNotify);
//        result.put("ALL_NOTIFY_MODEL", allNotify);
//        
//        return result;
//    }
    
//    public void updateSpecificNotifications(List<NotifyBaseTreeModel> notifyList) throws Exception
//    {
//        //prepare the appropriate buckets
//        for(NotifyBaseTreeModel model : notifyList)
//        {
//            String userSelection = model.getSpecificsComboSelection();
//            
//            if (userSelection.equals("True"))
//            {
//                addToSpecficNotification(true, model);
//            }
//            else if (userSelection.equals("False"))
//            {
//                addToSpecficNotification(false, model);
//            }
//            else
//            {
//                addToDefaultNotification(model);
//            }
//        }//end of for loop
//
//        //exe
////        ServiceGraph.addNotificationRelation(user,  new ArrayList<ComponentNotification>(userSpecficNotification.values()));
////        ServiceGraph.removeNotificationRelation(user,  new ArrayList<ComponentNotification>(userDefaultNotification.values()));
//        
////        testData();
//    }
    
    
    
    private void setSocialComponentType(String strType)
    {
        type = SocialComponentType.getRevisionType(strType);
        componentType = SocialUtil.getRSComponentBasedOnType(type, null, null);
//        setRSComponentBasedOnType();
    }

//    private void setNotifyFlagForComponent(String notifyKey, NotifyBaseTreeModel treeModel)
//    {
//        List<NotificationConfig> compNotificationForUser = cache.get(componentType.getSys_id());
//        if(compNotificationForUser == null)
//        {
////            compNotificationForUser = ServiceGraph.getCompNotificationTypeForUser(user, componentType);
//            cache.put(componentType.getSys_id(), compNotificationForUser);
//        }
//        
//        //if there are no recs in the graph db, then it is DEFAULT 
//        boolean defaultValue = true;
//        if(compNotificationForUser != null)
//        {
//            for(NotificationConfig config : compNotificationForUser)
//            {
//                if(notifyKey.equals(config.getType().name()))
//                {
//                    treeModel.setUserSelected(Boolean.valueOf(config.getValue()));
//                    defaultValue = false;
//                    break;
//                }
//            }
//        }
//
//        treeModel.setDefaultSelected(defaultValue);
//    }

//    private void addToSpecficNotification(boolean value, NotifyBaseTreeModel model)
//    {
//        String compId = model.getRSCompSysId();
//        String compDisplayName = model.getRSCompDisplayName();
//
//        ComponentNotification compNotify = userSpecficNotification.get(compId);
//        if(compNotify == null)
//        {
//            componentType = SocialUtil.getRSComponentBasedOnType(type, compId, compDisplayName);
//
//            compNotify = new ComponentNotification();
//            compNotify.setRSComponent(componentType);
//            
//            userSpecficNotification.put(compId, compNotify);
//        }
//        
//        NotificationConfig notifyType = new NotificationConfig();
//        notifyType.setType(UserGlobalNotificationContainerType.valueOf(model.getNotifyEventType()));
//        notifyType.setValue(value+"");
//        
//        compNotify.addNotifyType(notifyType);
//      
//        
//        
//    }//addToSpecficNotification
    
//    private void testData()
//    {
//        
//        RSComponent testType = SocialUtil.getRSComponentBasedOnType(SocialComponentType.RUNBOOKS, "2c9181e52f2289b6012f22d11145000e", "Runbook.Test5");
//
//        NotificationConfig test = new NotificationConfig();
//        test.setType(UserGlobalNotificationContainerType.DOCUMENT_UPDATE);
//        test.setValue(true+"");
//        
//        ComponentNotification compNotify = new ComponentNotification();
//        compNotify.setRSComponent(testType);
//        compNotify.addNotifyType(test);
//        
//        List<ComponentNotification> list = new ArrayList<ComponentNotification>();
//        list.add(compNotify);
//        
////        ServiceGraph.addNotificationRelation(user,  list);
//        
//    }
//    
//    private void addToDefaultNotification(NotifyBaseTreeModel model)
//    {
//        String compId = model.getRSCompSysId();
//        String compDisplayName = model.getRSCompDisplayName();
//
//        ComponentNotification compNotify = userDefaultNotification.get(compId);
//        if(compNotify == null)
//        {
//            componentType = SocialUtil.getRSComponentBasedOnType(type, compId, compDisplayName);
//
//            compNotify = new ComponentNotification();;
//            compNotify.setRSComponent(componentType);
//            
//            userDefaultNotification.put(compId, compNotify);
//        }
//        
//        NotificationConfig notifyType = new NotificationConfig();
//        notifyType.setType(UserGlobalNotificationContainerType.valueOf(model.getNotifyEventType()));
//        notifyType.setValue(true+"");
//        
//        compNotify.addNotifyType(notifyType);
//        
//    }//addToDefaultNotification
    
    
}
