/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.test;



public class TestSocial
{
//    
//    private static final String DB_PATH = "neo4j-store";
//    private static final String NODE_TYPE = "User";
//    private static final String USERNAME_KEY = "username";
//    private static final String USERNAME_CONTENT = "usernameContent";
//    private static GraphDatabaseService graphDb;
//    // private static Index<Node> nodeIndex;
//    public static StringBuffer testString = new StringBuffer();
//
//    // START SNIPPET: createRelTypes
//    private static enum RelTypes implements RelationshipType
//    {
//        USERS_REFERENCE, USER
//    }
//
//    // END SNIPPET: createRelTypes
//
//    /*
//    public static void main(final String[] args)
//    {
//        testGraph();
//    }
//    */
//   
//    
//    public static void testGraph()
//    {
//        /**
//         * try { FileChannel fileChannel = new RandomAccessFile( "C:\\project\\resolve3\\dist\\rscontrol\\config\\graphdb" + "/neostore", "r" ).getChannel(); ByteBuffer buf = ByteBuffer.allocate( 4*9 );
//         * 
//         * if ( fileChannel.read( buf ) != 4*9 ) { throw new RuntimeException( "Unable to read neo store header information" ); } } catch(Exception exp) { }
//         */
//
//        try
//        {
//
//            // testGetPostById();
//
//            // testNotifyOfProfileChange();
//
//            // testIndexQuery();
//
//            // Team teamLocal = SocialStore.getTeam("8a9482e739bc518e0139bc61c1060007", false);
//
//            // System.out.println("===== ok " );
//
//            // testIndexMerge();
//            // testDigestEmail();
//            //testImpex();
//
//            // testFilterNotification();
//
//            /*
//             * createActionTask(0, 10); createRunbook(0, 10); createDocument(0, 10); createRss(0, 10); createUsers(0, 25); createProcess(0, 10); createTeam(0, 40); createForum(0, 10);
//             * 
//             * createUserReader();
//             * 
//             * int count = SocialStore.getActiveSocialUserCount();
//             * 
//             * updateComponent();
//             * 
//             * testTeams(); testTeamsInProcess();
//             * 
//             * post();
//             * 
//             * testMSocial();
//             * 
//             * //testMSocialBigAmount();
//             * 
//             * addComment();
//             */
//            
//            //post();
//             //getPost();
//            
//            //testTag();
//            testNewSocial();
//             
//             
//             /* 
//             * testSocialConfig();
//             * 
//             * testTree(); testSwap();
//             * 
//             * testGetFollowers();
//             * 
//             * testMSocial();
//             * 
//             * testMarkStarred(); testMarkLike();
//             * 
//             * testRssFollwerCount();
//             * 
//             * testUserFavorite();
//             * 
//             * testNotification();
//             * 
//             * //removeExpiredPosts();
//             * 
//             * //getPost();
//             * 
//             * //deleteSocial(); //GraphDBUtil.shutdown();
//             */
//
//        }
//        catch (Exception exp)
//        {
//            exp.printStackTrace();
//        }
//    }
//    
//    public static void testSocialNewAPI()
//    {
//        try
//        {
//            QueryDTO query = new QueryDTO();
//            query.setStart(0);
//            query.setLimit(60);
//            query.setWhereClause("");
//            
//            //List<NameSpace> ns = ServiceGraph.searchNamespace(query);
//            
//            
//            User user = new User();
//            user.setSys_id("8a9482e73d64c915013d64d2087a000c");
//            user.setRoles("admin");
//            user.setDisplayName("admin");
//         
//            Team team = new Team();
//            team.setId("1111111111111111122222222222222222");
//            team.setName("1111111111111111122222222222222");
//            ServiceGraph.createTeam(team);
//            
//            Post myPost = buildPost("Admin", "AdminAAAAAAAAA", null, "AAAAAAAAAAAAAAAAAAAAAAAAAAA", new ArrayList());
//            myPost.setUser(user);
//            
//            List<RSComponent> target1 = new ArrayList<RSComponent>();
//            target1.add(team);
//            myPost.setTarget(target1);
//            ServiceSocial.createPost(myPost);
//            
//            List<Post> posts = ServiceSocial.getPosts(user, new QueryDTO(), team.getSys_id(), false, new HashMap());
//            
//            ServiceSocial.setStreamLockUnlock(user, team.getSys_id(), true);
//            
//            Post myPostA = buildPost("Admin", "AdminAAAAAAAAAaaaaa", null, "AAAAAAAAAAAAAAAAAAAAAAAAAAA", new ArrayList());
//            myPostA.setUser(user);
//            
//            List<RSComponent> target = new ArrayList<RSComponent>();
//            target.add(team);
//            myPostA.setTarget(target);
//            ServiceSocial.createPost(myPostA);
//            
//            posts = ServiceSocial.getPosts(user, new QueryDTO(), team.getSys_id(), false, new HashMap());
//            
//            List<Post> allPost = ServiceGraph.getAllPosts(user, new HashMap());
//            
//            Post firstPost = allPost.get(0);
//            String postid = firstPost.getSys_id();
//            
//            ServiceSocial.setAnswer(user, postid, true);
//            
//            Post post = ServiceSocial.getPostById(user, postid);
//            
//            ServiceSocial.setAnswer(user, postid, false);
//            
//            Post postAfter = ServiceSocial.getPostById(user, postid);
//            
//            System.out.println("----");
//            
//        }
//        catch(Exception exp)
//        {
//            
//        }
//        
//    }
//    
//    
//    public static void testUpdateAgain()
//    {
//        try
//        {
//            boolean testSuccess = true;
//            String message = "";
//            
//            long lDateTime = new Date().getTime();
//            Random r = new Random(lDateTime);
//            int sysIdInt = Math.abs(r.nextInt());
//            String sysId = "tag!"+ String.valueOf(sysIdInt);;
//            String originalSysId = sysId;
//            
//            ResolveTag tag = new ResolveTag();
//            tag.setName(sysId);
//            tag.setName(sysId);
//            tag.setType(ResolveTag.NODE_TYPE.ResolveTag.name());
//            tag.setRoles("admin");
//            tag.setEditRoles("admin");
//            tag.setSys_created_by("admin");
//            tag.setSys_updated_by("admin");
//            tag.setSys_created_on(lDateTime);
//            tag.setSys_updated_on(lDateTime);
//            
//            ServiceTag.createResolveTag(tag, true);
//            
//            ResolveTag currentTag = ServiceTag.getResolveTag(sysId);
//            if(currentTag == null)
//            {
//                testSuccess = false;
//                List<ResolveTag> tags = ServiceTag.getAllResolveTags("");
//                message = " | original tag ("+sysId+") not found "+currentTag+" tags size is: "+tags.size()+" ";
//                ResolveTag current;
//                for(int i=0; i<tags.size(); i++)
//                {
//                    current = tags.get(i);
//                    message+= " | "+current.getName()+" | ";
//                }
//            }
//            else if((!(currentTag.getName()).equals(sysId)) || (!(currentTag.getSys_id()).equals(sysId)))
//            {
//                testSuccess = false;
//                message += " | name: '"+currentTag.getName()+"' and id: '"+currentTag.getSys_id()+"' should be = '"+sysId+"' | ";
//            }
//            
//            sysId = "differenttag"+Math.abs(r.nextInt());
//            tag.setName(sysId);
//            //tag.setId(sysId);
//            ServiceTag.updateResolveTag(tag);
//            
//            currentTag = ServiceTag.getResolveTag(originalSysId);
//            if(currentTag != null)
//            {
//                testSuccess = false;
//                message = " | original tag ("+sysId+") was found "+currentTag+" | ";
//                List<ResolveTag> tags = ServiceTag.getAllResolveTags("");
//                ResolveTag current;
//                for(int i=0; i<tags.size(); i++)
//                {
//                    current = tags.get(i);
//                    message+= " | "+current.getName()+" | ";
//                }
//            }
//            
//            currentTag = ServiceTag.getResolveTag(sysId);
//            if(currentTag == null)
//            {
//                testSuccess = false;
//                message = " | new tag ("+sysId+") not found "+currentTag+" | ";
//                List<ResolveTag> tags = ServiceTag.getAllResolveTags("");
//                ResolveTag current;
//                for(int i=0; i<tags.size(); i++)
//                {
//                    current = tags.get(i);
//                    message+= " | "+current.getName()+" | ";
//                }
//            }
//            else if((!(currentTag.getName()).equals(sysId)) || (!(currentTag.getSys_id()).equals(sysId)))
//            {
//                testSuccess = false;
//                message += " | name: '"+currentTag.getName()+"' and id: '"+currentTag.getSys_id()+"' should be = '"+sysId+"' | ";
//            }
//        }
//        catch(Exception exp) {}
//    }
//    
//    
//    public static void testUpdateTag()
//    {
//        try
//        {
//        
//            boolean testSuccess = true;
//            String message = "";
//            
//            long lDateTime = new Date().getTime();
//            Random r = new Random(lDateTime);
//            
//            int sysIdInt = Math.abs(r.nextInt());
//            String sysId = "tag"+ String.valueOf(sysIdInt);
//            String originalSysId = sysId;
//            
//            ResolveTag tag = new ResolveTag();
//            tag.setName(sysId);
//            tag.setId(sysId);
//            tag.setType(ResolveTag.NODE_TYPE.ResolveTag.name());
//            tag.setRoles("admin");
//            tag.setEditRoles("admin");
//            tag.setSys_created_by("admin");
//            tag.setSys_updated_by("admin");
//            tag.setSys_created_on(lDateTime);
//            tag.setSys_updated_on(lDateTime);
//            
//            ServiceTag.createResolveTag(tag, true);
//            
//            ResolveTag currentTag = ServiceTag.getResolveTag(sysId);
//            
//            
//            if(currentTag == null)
//            {
//                testSuccess = false;
//                message = " | original tag ("+sysId+") not found "+currentTag+" | ";
//                List<ResolveTag> tags = ServiceTag.getAllResolveTags("");
//                ResolveTag current;
//                for(int i=0; i<tags.size(); i++)
//                {
//                    current = tags.get(i);
//                    message+= " | "+current.getName()+" | ";
//                }
//            }
//            
//            String updatesysId = "differenttag"+Math.abs(r.nextInt());
//            tag.setName(updatesysId);
//            //tag.setId(sysId);
//            ServiceTag.updateResolveTag(tag);
//            
//            currentTag = ServiceTag.getResolveTag(originalSysId);
//            if(currentTag != null)
//            {
//                testSuccess = false;
//                message = " | original tag ("+sysId+") was found "+currentTag+" | ";
//                List<ResolveTag> tags = ServiceTag.getAllResolveTags("");
//                ResolveTag current;
//                for(int i=0; i<tags.size(); i++)
//                {
//                    current = tags.get(i);
//                    message+= " | "+current.getName()+" | ";
//                }
//            }
//            
//            currentTag = ServiceTag.getResolveTag(updatesysId);
//            if(currentTag == null)
//            {
//                testSuccess = false;
//                message = " | new tag ("+sysId+") not found "+currentTag+" | ";
//                List<ResolveTag> tags = ServiceTag.getAllResolveTags("");
//                ResolveTag current;
//                for(int i=0; i<tags.size(); i++)
//                {
//                    current = tags.get(i);
//                    message+= " | "+current.getName()+" | ";
//                }
//            }
//        }
//        catch(Exception exp)
//        {
//            
//        }
//    }
//    
//    public static void test7102()
//    {
//        
//        try
//        {
//            String message = "";
//            boolean testSuccess = true;
//            
//            User user = new User();
//            user.setSys_id("8a9482e73d64c915013d64d2087a000c");
//            user.setRoles("admin");
//            user.setDisplayName("admin");
//            
//            Team team_followed = new Team();
//            team_followed.setSys_id("999911111222222a");
//            team_followed.setDisplayName("teamName1");
//            team_followed.setSys_created_by("admin");
//            team_followed.setSys_updated_by("admin");
//            ServiceGraph.createTeam(team_followed);
//            ServiceGraph.addUserToTeam(user, team_followed);
//           
//            
//            Team team_not_followed = new Team();
//            team_not_followed.setSys_id("xyxyxyxyx1222a");
//            team_not_followed.setDisplayName("teamName2");
//            team_not_followed.setSys_created_by("admin");
//            team_not_followed.setSys_updated_by("admin");    
//            ServiceGraph.createTeam(team_not_followed);
//    
//            
//            Forum forum_followed = new Forum();
//            forum_followed.setSys_id("1000111111111111111111122222a");
//            forum_followed.setDisplayName("forum_followed");
//            forum_followed.setSys_created_by("admin");
//            forum_followed.setSys_updated_by("admin");
//            ServiceGraph.createForum(forum_followed);
//            ServiceGraph.addUserToForum(user, forum_followed);
//                    
//            Forum forum_not_followed = new Forum();
//            forum_not_followed.setSys_id("200011111111111111122222a");
//            forum_not_followed.setDisplayName("forum_not_followed");
//            forum_not_followed.setSys_created_by("admin");
//            forum_not_followed.setSys_updated_by("admin");
//            ServiceGraph.createForum(forum_not_followed);
//    
//     
//            Process proc = new Process();
//            proc.setSys_id("30001111111111111112222a");
//            proc.setDisplayName("proc");
//            proc.setSys_created_by("admin");
//            proc.setSys_updated_by("admin");
//            ServiceGraph.createProcess(proc);
//            ServiceGraph.addUserToProcess(user, proc);
//           
//            ServiceGraph.addForumToProcess(forum_followed, proc);
//            ServiceGraph.addForumToProcess(forum_not_followed, proc);
//            
//            ServiceGraph.addTeamToProcess(team_followed, proc);
//            ServiceGraph.addTeamToProcess(team_not_followed, proc);
//            
//            List<RSComponent> userStream = ServiceGraph.getUserStreams(user);
//            boolean forum1Found = false;
//            boolean forum2Found = false;
//            boolean team1Found = false;
//            boolean team2Found = false;
//            boolean procFound = false;
//            RSComponent current;
//            for(int i=0; i<userStream.size(); i++)
//            {
//                current = userStream.get(i);
//               
//                if((current.getDisplayName()).equals(forum_followed.getName()))
//                {
//                    forum_followed = (Forum) current;
//                    forum1Found = true;
//                }
//                else if((current.getDisplayName()).equals(forum_not_followed.getName()))
//                {
//                    forum_not_followed = (Forum) current;
//                    forum2Found = true;
//                }
//                else if((current.getDisplayName()).equals(team_followed.getName()))
//                {
//                    team_followed = (Team) current;
//                    team1Found = true;
//                }
//                else if((current.getDisplayName()).equals(team_not_followed.getName()))
//                {
//                    team_not_followed = (Team) current;
//                    team2Found = true;
//                }
//                else if((current.getDisplayName()).equals(proc.getName()))
//                {
//                    proc = (Process) current;
//                    procFound = true;
//                }
//            }
//            
//            if(!team1Found)
//            {
//                testSuccess = false;
//                message += " |  "+team_followed.getName()+" was not found in the list of all user streams | ";
//            }     
//            if(!team2Found)
//            {
//                testSuccess = false;
//                message += " |  "+team_not_followed.getName()+" was not found in the list of all user streams | ";
//            }   
//
//            
//            if(!forum1Found)
//            {
//                testSuccess = false;
//                message += " |  "+forum_followed.getName()+" was not found in the list of all user streams | ";
//            }  
//            if(!forum2Found)
//            {
//                testSuccess = false;
//                message += " |  "+forum_not_followed.getName()+" was not found in the list of all user streams | ";
//            }  
//            if(!procFound)
//            {
//                testSuccess = false;
//                message += " |  "+proc.getName()+" was not found in the list of all user streams | ";
//            }
//    
//            
//            if(forum1Found && procFound && forum2Found && team2Found && team1Found)
//            {
//                String postContent = "Posting to the new Team, Forum, and Process";
//               
//                ArrayList<RSComponent> targets = new ArrayList();
//                
//                
//                for(int i=0; i<4; i++)
//                {
//                    Post myPostLocal = buildPost("Admin", "AdminAAAAAAAAA", null, "AAAAAAAAAAAAAAAAAAAAAAAAAAA"+i*100, new ArrayList());
//                    myPostLocal.setUser(user);
//                   
//                    targets = new ArrayList();
//                    
//                    if(i == 0)
//                    {
//                        targets.add(team_followed);
//                    }
//                    else if(i == 1)
//                    {
//                        targets.add(team_not_followed);
//                    }
//                    else if(i == 2)
//                    {
//                        targets.add(forum_followed);
//                    }
//                    else if(i==3)
//                    {
//                        targets.add(forum_not_followed);
//                    }
//                    myPostLocal.setTarget(targets);
//                    ServiceSocial.createPost(myPostLocal);
//                }
//                
//                
//                userStream = ServiceGraph.getUserStreams(user);
//             
//                forum1Found = false;
//                forum2Found = false;
//                procFound = false;
//                for(int i=0; i<userStream.size(); i++)
//                {
//                    current = userStream.get(i);
//                    
//                    if((current.getDisplayName()).equals(forum_followed.getName()))
//                    {
//                        forum_followed = (Forum) current;
//                        forum1Found = true;
//                    }
//                    else if((current.getDisplayName()).equals(forum_not_followed.getName()))
//                    {
//                        forum_not_followed = (Forum) current;
//                        forum2Found = true;
//                    }
//                    else if((current.getDisplayName()).equals(team_followed.getName()))
//                    {
//                        team_followed = (Team) current;
//                        team1Found = true;
//                    }
//                    else if((current.getDisplayName()).equals(team_not_followed.getName()))
//                    {
//                        team_not_followed = (Team) current;
//                        team2Found = true;
//                    }
//                    else if((current.getDisplayName()).equals(proc.getName()))
//                    {
//                        proc = (Process) current;
//                        procFound = true;
//                    }
//                }
//                
//                if(!team1Found)
//                {
//                    
//                    testSuccess = false;
//                    message += " |  "+team_followed.getName()+" was not found in the list of all user streams | ";
//                }     
//                if(!team2Found)
//                {
//                    testSuccess = false;
//                    message += " |  "+team_not_followed.getName()+" was not found in the list of all user streams | ";
//                }   
//
//          
//                if(!forum1Found)
//                {
//                    testSuccess = false;
//                    message += " | Forum: "+forum_followed.getName()+" was not found in the list of all user streams | ";
//                }  
//                if(!forum2Found)
//                {
//                    testSuccess = false;
//                    message += " | Forum: "+forum_not_followed.getName()+" was not found in the list of all user streams | ";
//                }  
//                if(!procFound)
//                {
//                    testSuccess = false;
//                    message += " | Process: "+proc.getName()+" was not found in the list of all user streams | ";
//                }
//        
//                
////                if(forum1Found && procFound && forum2Found && forum1Found && forum2Found)
////                {
////                    
////                    int followed_team_post_count = team_followed.getPostCount();
////                    if(followed_team_post_count != 1)
////                    {
////                        testSuccess = false;
////                        message += " | the followed team should have had 1 post, but it had "+followed_team_post_count+" | ";
////                    }
////                
////                    int unfollowed_team_post_count = team_not_followed.getPostCount();
////                    if(unfollowed_team_post_count != 1)
////                    {
////                        testSuccess = false;
////                        message += " | the unfollowed team should have had 1 post, but it had "+unfollowed_team_post_count+" | ";
////                    }
////               
////                    int followed_forum_post_count = forum_followed.getPostCount();
////                    if(followed_forum_post_count != 1)
////                    {
////                        testSuccess = false;
////                        message += " | the followed forum should have had 1 post, but it had "+followed_forum_post_count+" | ";
////                    }
////                
////                    int unfollowed_forum_post_count = forum_not_followed.getPostCount();
////                    if(unfollowed_forum_post_count != 1)
////                    {
////                        testSuccess = false;
////                        message += " | the unfollowed forum should have had 1 post, but it had "+unfollowed_forum_post_count+" | ";
////                    }
////                }
//                
//            }
//        }
//        catch(Exception exp)
//        {
//            
//        }
//    }
//    
//    public static void testNotificationFollow()
//    {
//        boolean testSuccess = true;
//        String message = "";
//        
//        User user = new User();
//        user.setSys_id("11111");
//        user.setRoles("admin");
//        user.setDisplayName("userDisplayName");
//        user.setUsername("userDisplayName");
//        ServiceGraph.createUser(user);
//        
//        List<NotificationConfig> notificationConfigs = new ArrayList<NotificationConfig>();
//        NotificationConfig notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.USER_FOLLOW_ME);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig); 
//        ServiceGraph.addUserNotificationConfig(user, notificationConfigs);
//        
//        
//        User user2 = new User();
//        user2.setSys_id("22222");
//        user2.setRoles("admin");
//        user2.setDisplayName("userDisplayName2");
//        user2.setUsername("userDisplayName2");
//        ServiceGraph.createUser(user2);
//        
//        ServiceGraph.unfollowUser(user2, user);
//        ServiceGraph.followUser(user2, user);
//        
//        Map<String, Object> configMap = new ConcurrentHashMap<String, Object>();
//        configMap.put("pageSize", 1000);
//        configMap.put("offset", 0);
//        configMap.put("sortField", "u_component_updated_on");
//        configMap.put("sortDir", "sortDsc");
//        configMap.put("timezone", "GMT-08:00");
//        Collection<Post> adminNotifications = ServiceGraph.getSystemNotifications(user, configMap);
//        
//        message += adminNotifications;
//        if(adminNotifications.isEmpty())
//        {
//            testSuccess = false;
//            message += " this notifications list was empty";
//        }
//    }
//    
//    
//    public static void testDynamicHome()
//    {
//        
//        try
//        {
//            User user = new User();
//            user.setSys_id("8a9482e73d64c915013d64d2087a000c");
//            user.setRoles("admin");
//            user.setDisplayName("admin");
//            
//            
//            Log.log.info("Start testDynamicHome ");
//            
//            boolean teamFound = false;
//            boolean forumFound = false;
//            boolean procFound = false;
//            
//            Team newTeam = new Team();
//            newTeam.setId("eeeeebbb");
//            newTeam.setName("newTeam");
//            
//            Forum newForum = new Forum();
//            newForum.setId("wwwwwwbbb");
//            newForum.setName("newForum");
//            
//            Forum newForum2 = new Forum();
//            newForum2.setId("wwwwwwabbbb2");
//            newForum2.setName("newForum2");
//            
//            Process newProc = new Process();
//            newProc.setId("qqqqqqqabbbbb");
//            newProc.setName("newProc");
//           
//            ServiceGraph.createTeam(newTeam);
//            ServiceGraph.createForum(newForum);
//            ServiceGraph.createProcess(newProc);
//            
//            ServiceGraph.addUserToTeam(user, newTeam);
//            ServiceGraph.addUserToForum(user, newForum);
//            ServiceGraph.addUserToProcess(user, newProc);
//            
//            Post myPost = buildPost("Admin", "AdminAAAAAAAAA", null, "AAAAAAAAAAAAAAAAAAAAAAAAAAA", new ArrayList());
//            myPost.setUser(user);
//            
//            List<RSComponent> target1 = new ArrayList<RSComponent>();
//            target1.add(newTeam);
//            myPost.setTarget(target1);
//            ServiceSocial.createPost(myPost);
//            
//            Post myPost1 = buildPost("Admin", "AdmiBBBBB", null, "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB", new ArrayList());
//            myPost1.setUser(user);
//            
//            List<RSComponent> target2 = new ArrayList<RSComponent>();
//            target2.add(newForum);
//            target2.add(newForum2);
//            myPost1.setTarget(target2);
//            ServiceSocial.createPost(myPost1);
//            
//            Post myPost2 = buildPost("Admin", "AdmiCCCCC", null, "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", new ArrayList());
//            myPost2.setUser(user);
//            List<RSComponent> target3 = new ArrayList<RSComponent>();
//            target3.add(newProc);
//            myPost2.setTarget(target3);
//            ServiceSocial.createPost(myPost2);
//            
//            ServiceSocial.setRead(user, myPost1.getSys_id(), true);
//            
//            RSComponent current = null;
//            RSComponent team = null;
//            RSComponent forum = null;
//            RSComponent proc = null;
//            
//            boolean testSuccess = false;
//            String message = "";
//            
//            List<RSComponent> userStream = ServiceGraph.getUserStreams(user);
//            
//            for(int i=0; i<userStream.size(); i++)
//            {
//                current = userStream.get(i);
//                if((current.getDisplayName()).equals(newTeam.getName()))
//                {
//                    team = current;
//                    teamFound = true;
//                }
//                else if((current.getDisplayName()).equals(newForum.getName()))
//                {
//                    forum = current;
//                    forumFound = true;
//                }
//                else if((current.getDisplayName()).equals(newProc.getName()))
//                {
//                    proc = current;
//                    procFound = true;
//                }
//            }
//      
//            if(!teamFound)
//            {
//                testSuccess = false;
//                message += " | Team: "+newTeam+" was not found in the list of all user streams | ";
//            }
//            else
//            {
////                int  unreadTeamPosts = team.getPostCount();
////                
////               Log.log.info("==== unreadTeamPosts ===" + unreadTeamPosts);
////                
////                if(unreadTeamPosts!=1)
////                {
////                    testSuccess = false;
////                    message += " | new team "+newTeam+" has "+unreadTeamPosts+" unread posts when it should have one | ";
////                }
//            }
//            
//            QueryDTO query = new QueryDTO();
//            query.setStart(0);
//            query.setLimit(100);
//            Map emptyQuery = new HashMap();
//            
//            Post currentPost = null;
//            
//            List<Post> newStreamPosts = ServiceSocial.getPosts(user, query, newTeam.getSys_id(), false, null);      
//            Post postedPost = null;
//            List postTexts = new ArrayList();
//            for(int i=0; postedPost == null && i<newStreamPosts.size() ; i++)
//            {
//                currentPost = newStreamPosts.get(i);
//                postTexts.add("'"+currentPost.getContent()+"'");
//                if((currentPost.getSys_id()).equals(myPost.getSys_id()))
//                {
//                    postedPost = currentPost;
//                }
//            }
//            if(postedPost == null)
//            {
//                testSuccess = false;
//                message += " | This post is not in the team stream: "+newTeam+" size = "+newStreamPosts.size()+" | ";
//                for(int i=0; i<newStreamPosts.size(); i++)
//                {
//                    message += (newStreamPosts.get(i)).getContent();
//                }
//            }
//        
//            
//            if(!forumFound)
//            {
//                testSuccess = false;
//                message += " | Forum: "+newForum+" was not found in the list of all user streams | ";
//            }
//            else
//            {
//                int unreadForumPosts = forum.getUnReadCount();
//                
//                Log.log.info("==== unreadForumPosts == " + unreadForumPosts);
//                
//                if(unreadForumPosts!=1)
//                {
//                    testSuccess = false;
//                    message += " | new forum "+newForum+" has "+unreadForumPosts+" unread posts when it should have one | ";
//
//                }
//            }
//            
//            
//            
//            emptyQuery = new HashMap();
//            newStreamPosts = ServiceSocial.getPosts(user, query, newForum.getSys_id(), false, null);      
//            postedPost = null;
//            postTexts = new ArrayList();
//            for(int i=0; postedPost == null && i<newStreamPosts.size() ; i++)
//            {
//                currentPost = newStreamPosts.get(i);
//                postTexts.add("'"+currentPost.getContent()+"'");
//                if((currentPost.getSys_id()).equals(myPost1.getSys_id()))
//                {
//                    postedPost = currentPost;
//                }
//            }
//            if(postedPost == null)
//            {
//                testSuccess = false;
//                message += " | This post is not in the forum stream: "+newForum+" size = "+newStreamPosts.size()+" | ";
//                for(int i=0; i<newStreamPosts.size(); i++)
//                {
//                    message += (newStreamPosts.get(i)).getContent();
//                }
//            }
//            
//            
//            if(!procFound)
//            {
//                testSuccess = false;
//                message += " | Process: "+newProc+" was not found in the list of all user streams | ";
//            }
//            else
//            {
//                int unreadProcessPosts = proc.getUnReadCount();
//                
//                Log.log.info("==== unreadProcessPosts == " + unreadProcessPosts);
//                
//                if(unreadProcessPosts!=1)
//                {
//                    testSuccess = false;
//                    message += " | new process "+newProc+" has "+unreadProcessPosts+" unread posts when it should have one | ";
//                }
//            }
//            
//            emptyQuery = new HashMap();
//            newStreamPosts = ServiceSocial.getPosts(user, query, newProc.getSys_id(), false, null);      
//            postedPost = null;
//            postTexts = new ArrayList();
//            for(int i=0; postedPost == null && i<newStreamPosts.size() ; i++)
//            {
//                currentPost = newStreamPosts.get(i);
//                postTexts.add("'"+currentPost.getContent()+"'");
//                if((currentPost.getSys_id()).equals(myPost2.getSys_id()))
//                {
//                    postedPost = currentPost;
//                }
//            }
//            if(postedPost == null)
//            {
//                testSuccess = false;
//                message += " | This post is not in the process stream: "+newProc+" size = "+newStreamPosts.size()+" | ";
//                for(int i=0; i<newStreamPosts.size(); i++)
//                {
//                    message += (newStreamPosts.get(i)).getContent();
//                }
//            }
//            
//            Log.log.info("End testDynamicHome ");
//            
//            message += ServiceSocial.getUserStreams(user).size();
//            
//        }
//        catch(Exception exp) {}
//
//    }
//
//    public static void testGetPostById()
//    {
//        User user = getUserWithID("718eea88c611227201bb112d7966edf8");
//        String postID = "29786.1359410883085";
//
//        Post postLocal = ServiceGraph.getPostById(user, postID);
//
//        String end = "";
//
//    }
//
//    public static void testNotifyOfProfileChange()
//    {
//        SystemNotification notif = buildSystemNotifaction("notifyAdmin", "notifyAdmin", "google.notifyAdmin", "notifyAdmin notifyAdmin notifyAdmin notifyAdmin", UserGlobalNotificationContainerType.USER_PROFILE_CHANGE, new ArrayList());
//
//        ServiceGraph.notifyFromUser(getUserWithID("718eea88c611227201bb112d7966edf8"), getUserWithID("718eea88c611227201bb112d7966edf8"), notif);
//    }
//
//    public static void testIndexQuery()
//    {
//        testIndexOneQuery("test 123");
//        testIndexOneQuery("test 12345");
//        testIndexOneQuery("test 123466");
//
//        testIndexOneQuery("test hello world 123466");
//        testIndexOneQuery("test hello 123");
//        testIndexOneQuery("test hello 123 test");
//
//        testIndexOneQuery("test hello 123 test ttt");
//        testIndexOneQuery("test hello 123 test aaa ");
//        testIndexOneQuery("test hello 123 test bbb ");
//
//        System.out.println("==== test top common query");
//
//        List<ResolveSearchQuery> docList = ResolveQuerySearch.findTopCommonQuery(3);
//
//        List<ResolveSearchQuery> similarDocs = ResolveQuerySearch.findSimilarQuery("hello", 3);
//        List<ResolveSearchQuery> similarDocs1 = ResolveQuerySearch.findSimilarQuery("test", 3);
//        List<ResolveSearchQuery> similarDocs2 = ResolveQuerySearch.findSimilarQuery("123", 3);
//
//        System.out.println("==== ok ok");
//
//    }
//
//    public static void testIndexOneQuery(String queryString)
//    {
//        try
//        {
//            List<ResolveSearchQuery> queryList = new ArrayList<ResolveSearchQuery>();
//            ResolveSearchQuery query = new ResolveSearchQuery();
//            query.setQuery(queryString);
//            query.setReenterCount(1);
//            query.setSysCreatedBy("admin");
//            query.setSysUpdatedBy("admin");
//            query.setSysUpdatedOn(new Date());
//            query.setSysCreatedOn(new Date());
//
//            queryList.add(query);
//
//            // ResolveQueryWriter.indexQuery(queryList);
//            ServiceIndex.indexQuery(queryList);
//        }
//        catch (Exception exp)
//        {
//        }
//    }
//
//    public static void testFilterNotification()
//    {
//        List<RSComponent> comps = new ArrayList<RSComponent>();
//        ActionTask at1 = new ActionTask();
//        at1.setSys_id("2905b038c0a8a217003d55da3bf24403");
//        comps.add(at1);
//
//        ActionTask at2 = new ActionTask();
//        at2.setSys_id("2c9181e6310b072201310bc565f60004");
//        comps.add(at2);
//
//        long toTime = System.currentTimeMillis();
//        long fromTime = System.currentTimeMillis() - 6 * 60 * 60 * 1000;
//
//        User user = SocialCompConversionUtil.getSocialUser("admin");
//
//        Collection<Post> posts = ServiceGraph.getSystemNotificationsInTimeRange(user, comps, fromTime, toTime, getConfigMap());
//
//        String end = "";
//    }
//
//    public static void testIndexMerge()
//    {
//        try
//        {
//            IndexMerge.mergeIndex();
//        }
//        catch (Exception exp)
//        {
//            exp.printStackTrace();
//        }
//    }
//
//    public static void testDigestEmail()
//    {
//        try
//        {
//            List<ComponentNotification> allTypes = ServiceGraph.getAllNotificationType();
//
//            long fromTimeInMilliSec = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
//            long toTimeInMilliSec = System.currentTimeMillis();
//
//            Collection<Post> results = ServiceGraph.getAllPosts("admin", fromTimeInMilliSec, toTimeInMilliSec);
//
//            // System.out.println("==== end ==");
//
//        }
//        catch (Exception exp)
//        {
//            exp.printStackTrace();
//        }
//    }
//
//    public static void testImpex()
//    {
//        try
//        {
//            //testTag();
//            
//            
//            //TestGraph.createCatalogs();
//            
//            SocialImpexVO impexInfo = new SocialImpexVO();
//            Set<String> forumIDS = new HashSet<String>();
//            Set<String> teamIDS = new HashSet<String>();
//            Set<String> processIDs = new HashSet<String>();
//            Set<String> rssIDS = new HashSet<String>();
//            Set<UserImpexVO> users = new HashSet<UserImpexVO>();
//            
//            Set<String> tagIDS = new HashSet<String>();
//            tagIDS.add(RSComponent.ALL);
//            impexInfo.setTags(tagIDS);
//
//            rssIDS.add(RSComponent.ALL);
//            //impexInfo.setRsses(rssIDS);
//            
//            processIDs.add("8a9482e740a2037e0140a205fde30004");
//            //impexInfo.setProcesses(processIDs);
//
//            // forumIDS.add("8a9482e739210b890139210e651a0005");
//            // forumIDS.add("8a9482e739210b890139210e10880004");
//
//            forumIDS.add(RSComponent.ALL);
//            // impexInfo.setForums(forumIDS);
//
//            /*
//             * teamIDS.add("8a9482e739210b890139210fd1170007"); teamIDS.add("8a9482e739210b890139210ed25e0006"); impexInfo.setTeams(teamIDS);
//             * 
//             * processIDs.add("8a9482e73ae20af0013ae270d3280006"); impexInfo.setProcesses(processIDs);
//             * 
//             * UserImpexVO userVOAdmin = new UserImpexVO(); userVOAdmin.sysId = "718eea88c611227201bb112d7966edf8"; userVOAdmin.displayName = "admin"; userVOAdmin.exportPreference = true; userVOAdmin.exportNotification = true; users.add(userVOAdmin);
//             * 
//             * 
//             * UserImpexVO userVOUser20 = new UserImpexVO(); userVOUser20.sysId = "8a9482e73907326a01390733b9fb0004"; userVOUser20.displayName = "user20"; userVOUser20.exportPreference = true; userVOUser20.exportNotification = true; users.add(userVOUser20);
//             * 
//             * impexInfo.setUsers(users);
//             */
//
//            impexInfo.setFolderLocation("C:\\project\\resolve3\\dist\\rsexpert\\graph");
//
//            //ServiceGraph.exportGraph(impexInfo);
//            //ServiceGraph.uninstallGraph(impexInfo);
//
//            ServiceGraph.importGraph(impexInfo);
//
//        }
//        catch (Throwable t)
//        {
//        }
//    }
//
//    public static void testMSocialBigAmount()
//    {
//        Map<String, String> map = null;
//
//        for (int i = 0; i < 10000; i++)
//        {
//            map = new HashMap<String, String>();
//            map.put("USERNAME", "admin");
//            map.put("POSTCONTENT", "test MSocial " + i);
//            map.put("TARGET", "user20");
//            MSocial.postToUser(map);
//        }
//    }
//
//    public static void testMSocial() throws Exception
//    {
//        Map<String, String> map = new HashMap<String, String>();
//
//        map = new HashMap<String, String>();
//        map.put("USERNAME", "admin");
//        map.put("POSTCONTENT", "MSocial post to process1");
//        map.put("TARGET", "process3");
//        MSocial.postToProcess(map);
//
//        map = new HashMap<String, String>();
//        map.put("USERNAME", "admin");
//        map.put("POSTCONTENT", "MSocial post to process2");
//        map.put("TARGET", "process4");
//        MSocial.postToProcess(map);
//
//        map.put("USERNAME", "admin");
//        map.put("POSTCONTENT", "test MSocial");
//        map.put("TARGET", "user20");
//        MSocial.postToUser(map);
//
//        map = new HashMap<String, String>();
//        map.put("USERNAME", "admin");
//        map.put("POSTCONTENT", "MSocial post to forum1 11");
//        map.put("TARGET", "forum1");
//        MSocial.postToForum(map);
//
//        map = new HashMap<String, String>();
//        map.put("USERNAME", "admin");
//        map.put("POSTCONTENT", "MSocial post to team1");
//        map.put("TARGET", "team1");
//        MSocial.postToTeam(map);
//
//        map = new HashMap<String, String>();
//        map.put("USERNAME", "admin");
//        map.put("POSTCONTENT", "MSocial post to process1");
//        map.put("TARGET", "process1");
//        MSocial.postToProcess(map);
//
//        map = new HashMap<String, String>();
//        map.put("USERNAME", "admin");
//        map.put("POSTCONTENT", "MSocial post to rss1");
//        map.put("TARGET", "rss1");
//        MSocial.postToRSS(map);
//    }
//
//    public static void testSwap()
//    {
//        AdvanceTree advTree = ServiceGraph.getAdvanceTree(getUserWithID("user20"));
//
//        ServiceGraph.swapRunbookOrDocument(getRunbook("rb1"));
//        advTree = ServiceGraph.getAdvanceTree(getUserWithID("user20"));
//
//        ServiceGraph.swapRunbookOrDocument(getDocument("doc1"));
//        advTree = ServiceGraph.getAdvanceTree(getUserWithID("user20"));
//
//        String end = "";
//
//    }
//
//    public static void testGetFollowers()
//    {
//        Collection<User> users = ServiceGraph.getAllFollowersOfRss(getRss("rss1"));
//
//        Collection<User> users2 = ServiceGraph.getAllFollowersOfRss(getRss("rss2"));
//
//        System.out.println("==== the follower users ");
//    }
//
//    public static void testMarkLike()
//    {
//        try
//        {
//            Collection<Post> user20InboxPosts = ServiceGraph.getInboxPost(getUserWithID("user20"), getConfigMap());
//
//            for (Post user11P1 : user20InboxPosts)
//            {
//                Post postLocal = ServiceGraph.markLike(getUserWithID("user20"), user11P1);
//
//                Thread.sleep(500);
//
//                List<User> users = ServiceGraph.getAllUsersLikeThisPost(postLocal.getSys_id());
//
//            }
//
//            Collection<Post> postsAfter = ServiceGraph.getLikedPost(getUserWithID("user20"), getConfigMap());
//
//            String end = "";
//
//        }
//        catch (Exception exp)
//        {
//        }
//
//    }
//
//    public static void testMarkStarred()
//    {
//        Collection<Post> user20InboxPosts = ServiceGraph.getInboxPost(getUserWithID("user20"), getConfigMap());
//
//        for (Post user11P1 : user20InboxPosts)
//        {
//            // Post user11P1 = buildPost("user11P1", "user11P1", "user11P1.com", "user11P1 user11P1 user11P1 user11P1", new ArrayList());
//            ServiceGraph.markStarred(getUserWithID("user20"), user11P1);
//
//            // user20InboxPosts = SocialStore.getStarredPost(getUserWithID("user20"), getConfigMap());
//            //
//            // SocialStore.unmarkStarred(getUserWithID("user20"), user11P1);
//            //
//            // user20InboxPosts = SocialStore.getStarredPost(getUserWithID("user20"), getConfigMap());
//            //
//            // SocialStore.markStarred(getUserWithID("user20"), user11P1);
//            // break;
//        }
//
//        Collection<Post> postsLocal = ServiceGraph.getStarredPost(getUserWithID("user20"), getConfigMap());
//        Collection<Post> postsLocal1 = ServiceGraph.getStarredPost(getUserWithID("user11"), getConfigMap());
//
//        Collection<Post> posts = ServiceGraph.getAllPosts(getUserWithID("user20"), getConfigMap());
//
//        String end = "";
//    }
//
//    public static void testTeams()
//    {
//
//        ActionTaskTree actTree = ServiceGraph.getActionTaskTree(getUserWithID("user20"));
//
//        TeamTree teamTree = ServiceGraph.getTeamTree(getUserWithID("user22"));
//
//        ServiceGraph.addTeamToTeam(getTeam("team32"), getTeam("team30"));
//        ServiceGraph.addTeamToTeam(getTeam("team33"), getTeam("team32"));
//        ServiceGraph.addTeamToTeam(getTeam("team34"), getTeam("team33"));
//
//        ServiceGraph.addUserToTeam(getUserWithID("user22"), getTeam("team33"));
//
//        AdvanceTree adTree = ServiceGraph.getAdvanceTree(getUserWithID("user22"));
//
//        ServiceGraph.addTeamToTeam(getTeam("team32"), getTeam("team31"));
//        AdvanceTree adTreeLocal = ServiceGraph.getAdvanceTree(getUserWithID("user22"));
//
//    }
//
//    public static void testTeamsInProcess()
//    {
//        ServiceGraph.addUserToProcess(getUserWithID("user24"), getProcess("process6"));
//
//        ServiceGraph.addTeamToProcess(getTeam("team21"), getProcess("process6"));
//        ServiceGraph.addTeamToProcess(getTeam("team11"), getProcess("process6"));
//
//        ServiceGraph.addTeamToTeam(getTeam("team12"), getTeam("team21"));
//        ServiceGraph.addTeamToTeam(getTeam("team13"), getTeam("team12"));
//        ServiceGraph.addTeamToTeam(getTeam("team14"), getTeam("team13"));
//
//        AdvanceTree adTree = ServiceGraph.getAdvanceTree(getUserWithID("user24"));
//
//        ServiceGraph.addTeamToTeam(getTeam("team12"), getTeam("team11"));
//
//        AdvanceTree adTreeAgain = ServiceGraph.getAdvanceTree(getUserWithID("user24"));
//
//        String end = "";
//
//    }
//
//    public static void testSocialConfig()
//    {
//        List<Team> teamsLocal = ServiceGraph.getUserDirectMemberOfTeams(getUserWithID("user20"));
//
//        Collection<User> users = ServiceGraph.getAllUsers(true);
//        Collection<Team> teams = ServiceGraph.getAllTeams(true);
//        Collection<Process> procs = ServiceGraph.getAllProcess(true);
//
//        Team team1 = ServiceGraph.getTeam("team1", true);
//        Team team4 = ServiceGraph.getTeam("team4", true);
//
//        Process process = ServiceGraph.getProcess("process1", true);
//
//        ServiceGraph.addUserToForum(getUserWithID("user20"), getForum("forum3"));
//
//        ForumTree forumTree = ServiceGraph.getForumsTree(getUserWithID("user20"));
//        ForumTree forumTree1 = ServiceGraph.getForumsTree(getUserWithID("user19"));
//        Collection<Forum> forums = ServiceGraph.getAllForums(true);
//        Forum forum = ServiceGraph.getForum("forum1", true);
//
//        String end = "";
//
//    }
//
//    public static void testNotification()
//    {
//
//        List<NotificationConfig> compTypes = ServiceGraph.getCompNotificationType(getRunbook("rb1"));
//        List<ComponentNotification> allTypes = ServiceGraph.getAllNotificationType();
//
//        List<NotificationConfig> notificationConfigs = new ArrayList<NotificationConfig>();
//        NotificationConfig notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.ACTIONTASK_CREATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.RUNBOOK_DELETE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.RUNBOOK_CREATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.RSS_CREATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.RSS_UPDATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.RSS_PURGED);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.PROCESS_CREATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.PROCESS_UPDATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.PROCESS_PURGED);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.TEAM_CREATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.TEAM_UPDATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.TEAM_PURGED);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.FORUM_CREATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.FORUM_UPDATE);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        notifyConfig = new NotificationConfig();
//        notifyConfig.setType(UserGlobalNotificationContainerType.FORUM_PURGED);
//        notifyConfig.setValue(true);
//        notificationConfigs.add(notifyConfig);
//
//        ServiceGraph.addUserNotificationConfig(getUserWithID("user20"), notificationConfigs);
//        ServiceGraph.removeUserNotificationConfig(getUserWithID("user20"), notificationConfigs);
//        ServiceGraph.addUserNotificationConfig(getUserWithID("user20"), notificationConfigs);
//
//        ServiceGraph.getUserNotificationType(getUserWithID("user20"), UserGlobalNotificationContainerType.FORUM_PURGED);
//
//        // setting the list of ComponentNotification
//        List<ComponentNotification> compNotifys = new ArrayList<ComponentNotification>();
//
//        ComponentNotification compNotify = new ComponentNotification();
//        compNotify.setRSComponent(getActionTaskWithID("at1"));
//
//        List<NotificationConfig> types = new ArrayList();
//        NotificationConfig notifyType1 = new NotificationConfig();
//        notifyType1.setType(UserGlobalNotificationContainerType.ACTIONTASK_CREATE);
//        notifyType1.setValue(true);
//
//        NotificationConfig notifyType2 = new NotificationConfig();
//        notifyType2.setType(UserGlobalNotificationContainerType.ACTIONTASK_UPDATE);
//        notifyType2.setValue(true);
//
//        NotificationConfig notifyType3 = new NotificationConfig();
//        notifyType3.setType(UserGlobalNotificationContainerType.ACTIONTASK_PURGED);
//        notifyType3.setValue(true);
//
//        types.add(notifyType1);
//        types.add(notifyType2);
//        types.add(notifyType3);
//
//        compNotify.setNotifyTypes(types);
//        compNotifys.add(compNotify);
//
//        ComponentNotification compNotifyRunbook = new ComponentNotification();
//        compNotifyRunbook.setRSComponent(getActionTaskWithID("rb1"));
//
//        List<NotificationConfig> typesRunbook = new ArrayList<NotificationConfig>();
//        NotificationConfig config1 = new NotificationConfig();
//        config1.setType(UserGlobalNotificationContainerType.RUNBOOK_CREATE);
//        config1.setValue(true);
//
//        NotificationConfig config2 = new NotificationConfig();
//        config2.setType(UserGlobalNotificationContainerType.RUNBOOK_UPDATE);
//        config2.setValue(true);
//
//        NotificationConfig config3 = new NotificationConfig();
//        config3.setType(UserGlobalNotificationContainerType.RUNBOOK_DELETE);
//        config3.setValue(true);
//
//        typesRunbook.add(config1);
//        typesRunbook.add(config2);
//        typesRunbook.add(config3);
//
//        compNotifyRunbook.setNotifyTypes(typesRunbook);
//        compNotifys.add(compNotifyRunbook);
//
//        ServiceGraph.addNotificationRelation(getUserWithID("user20"), compNotifys);
//        ServiceGraph.removeNotificationRelation(getUserWithID("user20"), compNotifys);
//        ServiceGraph.addNotificationRelation(getUserWithID("user20"), compNotifys);
//
//        NotificationConfig config = ServiceGraph.getCompSpecialNotificationTypeForUser(getUserWithID("user20"), getActionTaskWithID("at1"), UserGlobalNotificationContainerType.ACTIONTASK_CREATE);
//
//        List<NotificationConfig> at1Types = ServiceGraph.getCompNotificationTypeForUser(getUserWithID("user20"), getActionTaskWithID("at1"));
//        List<ComponentNotification> allTypesLocal = ServiceGraph.getUserGlobalNotificationType(getUserWithID("user20"));
//
//        SystemNotification notif = buildSystemNotifaction("notifAt1", "notifAt1", "google.com_notifAt1", "notifAt1 notifAt1 notifAt1 notifAt1", UserGlobalNotificationContainerType.ACTIONTASK_CREATE, new ArrayList());
//
//        ServiceGraph.notifyFromActionTask(getUserWithID("user20"), getActionTaskWithID("at1"), notif);
//
//        notif = buildSystemNotifaction("notifRss1", "notifRss1", "google.com_notifRss1", "notifRss1 notifRss1 notifRss1 notifRss1", UserGlobalNotificationContainerType.RSS_CREATE, new ArrayList());
//
//        ServiceGraph.notifyFromRss(getUserWithID("user20"), getRss("rss1"), notif);
//
//        Collection<Post> allNotifLocal = ServiceGraph.getSystemNotifications(getUserWithID("user20"), getConfigMap());
//
//        notif = buildSystemNotifaction("notifAt2", "notifAt2", "google.com_notifAt2", "notifAt2 notifAt2 notifAt2 notifAt2", UserGlobalNotificationContainerType.ACTIONTASK_CREATE, new ArrayList());
//
//        ServiceGraph.notifyFromActionTask(getUserWithID("user20"), getActionTaskWithID("at2"), notif);
//
//        notif = buildSystemNotifaction("notifRt1", "notifRt1", "google.com_notifRt1", "notifRt1 notifRt1 notifRt1 notifRt1", UserGlobalNotificationContainerType.RUNBOOK_CREATE, new ArrayList());
//        ServiceGraph.notifyFromRunbook(getUserWithID("user20"), getRunbook("rb1"), notif);
//
//        notif = buildSystemNotifaction("notifRt2", "notifRt2", "google.com_notifRt2", "notifRt2 notifRt2 notifRt2 notifRt2", UserGlobalNotificationContainerType.RUNBOOK_CREATE, new ArrayList());
//
//        ServiceGraph.notifyFromRunbook(getUserWithID("user20"), getRunbook("rb2"), notif);
//
//        notif = buildSystemNotifaction("notifDoc1", "notifDoc1", "google.com_notifDoc1", "notifDoc1 notifDoc1 notifDoc1 notifDoc1", UserGlobalNotificationContainerType.DOCUMENT_CREATE, new ArrayList());
//        ServiceGraph.notifyFromDocument(getUserWithID("user20"), getDocument("doc1"), notif);
//
//        notif = buildSystemNotifaction("notifDoc2", "notifDoc2", "google.com_notifDoc2", "notifDoc2 notifDoc2 notifDoc2 notifDoc2", UserGlobalNotificationContainerType.DOCUMENT_CREATE, new ArrayList());
//        ServiceGraph.notifyFromDocument(getUserWithID("user20"), getDocument("doc2"), notif);
//
//        notif = buildSystemNotifaction("notifprocess1", "notifprocess1", "google.com_notifprocess1", "notifprocess1 notifprocess1 notifprocess1 notifprocess1", UserGlobalNotificationContainerType.PROCESS_CREATE, new ArrayList());
//        ServiceGraph.notifyFromProcess(getUserWithID("user20"), getProcess("process1"), notif);
//
//        notif = buildSystemNotifaction("notifforum1", "notifforum1", "google.com_notifforum1", "notifforum1 notifforum1 notifforum1 notifforum1", UserGlobalNotificationContainerType.FORUM_CREATE, new ArrayList());
//        ServiceGraph.notifyFromForum(getUserWithID("user20"), getForum("forum1"), notif);
//
//        notif = buildSystemNotifaction("notifteam1", "notifteam1", "google.com_notifteam1", "notifteam1 notifteam1 notifteam1 notifteam1", UserGlobalNotificationContainerType.TEAM_CREATE, new ArrayList());
//
//        ServiceGraph.notifyFromTeam(getUserWithID("user20"), getTeam("team1"), notif);
//
//        Collection<Post> allNotif = ServiceGraph.getSystemNotifications(getUserWithID("user20"), getConfigMap());
//
//        allNotif = ServiceGraph.getSystemNotifications(getUserWithID("user3"), getConfigMap());
//        allNotif = ServiceGraph.getSystemNotifications(getUserWithID("user7"), getConfigMap());
//
//        Collection<Post> allPosts = ServiceGraph.getAllPosts(getUserWithID("user20"), getConfigMap());
//
//        String end = "";
//
//    }
//
//    public static void updateComponent()
//    {
//        String userID = "user";
//        User user = null;
//
//        for (int i = 1; i < 25; i++)
//        {
//            userID += i;
//            user = getUserWithID(userID);
//            user.setDisplayName(userID + "UUUU");
//            ServiceGraph.updateComponent(user);
//        }
//    }
//
//    public static void testUserFavorite()
//    {
//        ServiceGraph.addToHome(getUserWithID("user20"), getProcess("process1"));
//        ServiceGraph.addToHome(getUserWithID("user20"), getTeam("team2"));
//        ServiceGraph.addToHome(getUserWithID("user20"), getUserWithID("user5"));
//
//        AdvanceTree userFavorite = ServiceGraph.getUserFavorite(getUserWithID("user20"));
//
//        ServiceGraph.removeFromHome(getUserWithID("user20"), getProcess("process1"));
//        ServiceGraph.removeFromHome(getUserWithID("user20"), getTeam("team2"));
//        ServiceGraph.removeFromHome(getUserWithID("user20"), getUserWithID("user5"));
//
//        AdvanceTree userFavoriteLocal = ServiceGraph.getUserFavorite(getUserWithID("user20"));
//
//        String end = "";
//    }
//
//    public static void removeExpiredPosts()
//    {
//        Map config = new HashMap();
//        config.put(SearchConstants.REMOVEALL.getTagName(), "true");
//        ServiceGraph.removeExpiredPosts(config);
//    }
//
//    public static Map<String, Object> getConfigMap()
//    {
//        Map<String, Object> configMap = new ConcurrentHashMap<String, Object>();
//        configMap.put(SearchConstants.PAGE_SIZE.getTagName(), 20);
//        configMap.put(SearchConstants.OFFSET.getTagName(), 0);
//        // configMap.put(SearchConstants.SORT_FIELD.getTagName(), SearchConstants.COMPONENT_UPDATED_ON.getTagName());
//        // configMap.put(SearchConstants.SORT_DIR.getTagName(), SearchConstants.SORT_DSC.getTagName());
//        configMap.put(SearchConstants.TIMEZONE.getTagName(), "GMT-07:00");
//
//        return configMap;
//    }
//
//    public static void addCommentsForOutboxUser20()
//    {
//        
//        Comment comment = new Comment();
//        comment.setUserName("admin");
//        comment.setSys_created_on(new Date().getTime());
//        comment.setCommentText("addComment user20Outbox addComment user20Outbox addComment user20Outbox addComment user20Outbox");
//
//        Collection<Post> posts = ServiceGraph.getOutboxPost(getUserWithID("user20"), getConfigMap());
//        List<RSComponent> mentions = new ArrayList();
//        User user = getUserWithID("user11");
//        mentions.add(user);
//
//        for (Post post : posts)
//        {
//            post.setTarget(null);
//
//            //ServiceGraph.addComment(getUserWithID("user20"), post, comment, mentions);
//        }
//
//        Collection<Post> postsLocal = ServiceGraph.getOutboxPost(getUserWithID("user11"), getConfigMap());
//
//        comment = new Comment();
//        comment.setUserName("admin");
//        comment.setSys_created_on(new Date().getTime());
//        comment.setCommentText("addComment user11P1Local addComment user11P1Local addComment user11P1Local addComment");
//
//        for (Post postLocal : postsLocal)
//        {
//            //TODO -- using new comment
//            //ServiceGraph.addComment(getUserWithID("user11"), postLocal, comment);
//        }
//    }
//    
//    
//    public static void testTag()
//    {
//        try
//        {
//            QueryDTO query = new QueryDTO();
//            query.setStart(0);
//            query.setLimit(60);
//            
//            QueryFilter filter = new QueryFilter("auto", "doc6.resolve.network", SearchConstants.HASH_TEXT.getTagName(), QueryDTO.QUERY_COMPARISION.startLeaf.name());
//            
//            List<QueryFilter> filters = new ArrayList<QueryFilter>();
//            filters.add(filter);
//            
//            QueryFilter filter1 = new QueryFilter("auto", "abc", SearchConstants.HASH_TEXT.getTagName(), QueryDTO.QUERY_COMPARISION.contains.name());
//            
//            
//            QueryFilter filter2 = new QueryFilter("auto", "abc", SearchConstants.HASH_TEXT.getTagName(), QueryDTO.QUERY_COMPARISION.notContains.name());
//            
//            QueryFilter filter3 = new QueryFilter("auto", "fen.x.1", SearchConstants.HASH_TEXT.getTagName(), QueryDTO.QUERY_COMPARISION.notStartsWith.name());
//            
//            
//            //filters.add(filter1);
//            //filters.add(filter);
//            //filters.add(filter);
//            
//            query.setFilters(filters);
//            
//            List<ResolveTag> tags = ServiceTag.searchResolveTags(query);
//            
//            System.out.println("==== tags size " + tags.size());
//            String start = "";
//            
//            ResolveTag tag = new ResolveTag();
//            tag.setName("doc6.resolve.network.abc1");
//            tag.setId("doc6.resolve.network.abc1");
//            tag.setType(TagContainerType.ResolveTag.name());
//            
//            ServiceTag.createResolveTag(tag, true);
//            
//           
//            List<ResolveTag> tags1 = ServiceTag.searchResolveTags(query);
//            
//            ResolveTag tag1 = new ResolveTag();
//            tag1.setName("fen.x.google2");
//            tag1.setId("fen.x.google2");
//            tag1.setType(TagContainerType.ResolveTag.name());
//            
//            ServiceTag.createResolveTag(tag, true);
//            
//            
//            query.setFilters(filters);
//            
//            List<ResolveTag> tags2 = ServiceTag.searchResolveTags(query);
//            
//            System.out.println("");
//            
//            System.out.println("----");
//        }
//        catch(Exception exp)
//        {
//            exp.printStackTrace();
//        }
//        
//    }
//    
//    public static void testMovePostToOtherTargest()
//    {
//        try
//        {
//            User userAdmin = getUserWithID("718eea88c611227201bb112d7966edf8");
//            userAdmin.setUsername("userAdmin");
//            ServiceGraph.createUser(userAdmin);
//            
//            User userB = getUserWithID("8a9482e73d64c915013d64d2087a000c");
//            userB.setUsername("userB");
//            ServiceGraph.createUser(userB);
//            
//            AdvanceTree treeW = ServiceGraph.getAdvanceTree(userAdmin);
//            
//            Process myProc = (Process) ((List) treeW.getProcesses()).get(0);
//            
//            //user B create post
//            Post post1 = buildPost("AdminWWWWWPPPPPPPPPPPPPP1", "AdminWWWWWPPPPPPPPPPPPPPPPPPP1 ", null, "goooooooogle", new ArrayList());
//            post1.setAuthor("userB");
//            post1.setIsRoot(true);
//            post1.setUser(userB);
//            
//            List<RSComponent> targets = new ArrayList<RSComponent>();
//            targets.add(myProc);
//            post1.setTarget(targets);
//            
//            ServiceSocial.createPost(post1);
//            
//          //check the post if existed
//            List<Post> getPost = ServiceSocial.getPostById(userB, post1.getSys_id());
//            
//            List<String> targetsIDS = new ArrayList<String>();
//            
//            targetsIDS.add(((Runbook)((List)treeW.getRunbooks()).get(0)).getSys_id());
//            ServiceSocial.movePost(userB, post1, targetsIDS);
//            
//            getPost = ServiceSocial.getPostById(userB, post1.getSys_id());
//            
//            ServiceGraph.deletePost(post1.getSys_id());
//            
//            getPost = ServiceSocial.getPostById(userB, post1.getSys_id());
//            
//            
//        }
//        catch(Exception exp)
//        {
//            
//        }
//        
//    }
//   
//    
//    public static void testUserActivityStream()
//    {
//        try
//        {
//            
//            //testTag();
//            //PostSocial.moveCommentsToGraphAndIndex();
//             
//            User userAdmin = getUserWithID("718eea88c611227201bb112d7966edf8");
//            userAdmin.setUsername("userAdmin");
//            ServiceGraph.createUser(userAdmin);
//            
//            
//            //MIndex.moveCommentsToGraphAndIndex();
//            List<Post> allPosts = ServiceGraph.getAllPosts(userAdmin, new HashMap());
//            
//            
//            
//            
//            User userB = getUserWithID("8a9482e73d64c915013d64d2087a000c");
//            userB.setUsername("userB");
//            ServiceGraph.createUser(userB);
//            
//            //admin may don't have all posts
//            List<Post> allMyPost = ServiceGraph.getAllPosts(userAdmin, new HashMap());
//            
//            
//            //user B create post
//            Post post1 = buildPost("AdminWWWWWPPPPPPPPPPPPPP1", "AdminWWWWWPPPPPPPPPPPPPPPPPPP1 ", null, "goooooooogle", new ArrayList());
//            post1.setAuthor("userB");
//            post1.setIsRoot(true);
//            post1.setUser(userB);
//            
//            List<RSComponent> targets = new ArrayList<RSComponent>();
//            Process processTest = new Process();
//            processTest.setId("my firs post from userB");
//            ServiceGraph.createProcess(processTest);
//           
//            Process processTestA = new Process();
//            
//            processTestA.setId("Resolve Process AAA");
//            ServiceGraph.createProcess(processTestA);
//            
//            targets.add(processTestA);
//            post1.setTarget(targets);
//            
//            ServiceSocial.createPost(post1);
//            
//            
//            //user Admin create post2
//            Post post2 = buildPost("AdminWWWWW2222PPPPPPPPPPPPPP2", "AdminWWWWW2222PPPPPPPPPPPPP2 ", null, "goooooooogle222222222222222", new ArrayList());
//            post2.setAuthor("userAdmin");
//            post2.setUser(userAdmin);
//            post2.setIsRoot(true);
//            
//            targets.add(processTest);
//            post2.setTarget(targets);
//            ServiceSocial.createPost(post2);
//            
//            
//            //userB add comment to post 2
//            Post comment1 = new Post();
//            comment1.setUser(userB);
//            comment1.setContent("MY COMMENT");
//            comment1.setRoles("admin");
//            //comment1.setSys_created_on(new Date().getTime());
//            //comment1.setSys_updated_on(new Date().getTime());
//            
//            List<Post> comments = new ArrayList<Post>();
//            post2.addPostComment(comment1);
//            ServiceSocial.updatePost(post2);
//           
//            //userAdmin add comment to post1
//            //userAdmin add comment to comment1
//            
//            Post comment2 = new Post();
//            comment2.setUser(userAdmin);
//            comment2.setContent("userAdmin add comment to post1");
//            comment2.setRoles("admin");
//            comments = new ArrayList<Post>();
//            comments.add(comment2);
//            post1.addPostComment(comment2);
//            ServiceSocial.updatePost(post1);
//            
//            Post comment3 = new Post();
//            comment3.setUser(userAdmin);
//            comment3.setContent("userAdmin add comment to post2");
//            comment3.setRoles("admin");
//            comments = new ArrayList<Post>();
//            comments.add(comment2);
//            comment1.addPostComment(comment3);
//            ServiceSocial.updatePost(comment1);
//            
//            List<Post> results = ServiceSocial.getUserActivityStream(userB, new HashMap<String, Object>());
//            
//            System.out.println("===before");
//            
//            List<Post> myPosts = ServiceGraph.getAllPosts(userB, new HashMap());
//            
//            
//            
//            System.out.println("===OK");
//            
//        }
//        catch(Exception exp)
//        {
//            
//        }
//        
//    }
//    
//    public static void testNotificationCenter()
//    {
//        try
//        {
//            
//            //user admin follow process
//            User userAdmin = getUserWithID("718eea88c611227201bb112d7966edf8");
//            userAdmin.setUsername("userAdmin");
//            
//            //forwarding email from social inbox
//            List<NotificationConfig> notificationConfigs = new ArrayList<NotificationConfig>();
//            NotificationConfig notifyConfig = new NotificationConfig();
//            notifyConfig.setType(UserGlobalNotificationContainerType.USER_FORWARD_EMAIL);
//            notifyConfig.setValue(true);
//            notificationConfigs.add(notifyConfig);
//            
//            ServiceGraph.addUserNotificationConfig(userAdmin, notificationConfigs);
//
//            //user admin follow process
//            notificationConfigs = new ArrayList<NotificationConfig>();
//            notifyConfig = new NotificationConfig();
//            notifyConfig.setType(UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL);
//            notifyConfig.setValue(true);
//            notificationConfigs.add(notifyConfig);
//            
//            //ServiceGraph.addUserNotificationConfig(userAdmin, notificationConfigs);
//            
//            User userB = getUserWithID("8a9482e73d64c915013d64d2087a000c");
//            userB.setUsername("userB");
//            
//            Process processTest = new Process();
//            processTest.setId("8a9482e73f532c71013f533586b80008");
//            ServiceGraph.addUserToProcess(userAdmin, processTest);
//            
//            notificationConfigs = new ArrayList<NotificationConfig>();
//            notifyConfig = new NotificationConfig();
//            notifyConfig.setType(UserGlobalNotificationContainerType.USER_FOLLOW_ME);
//            notifyConfig.setValue(true);
//            notificationConfigs.add(notifyConfig);
//            
//            notifyConfig = new NotificationConfig();
//            notifyConfig.setType(UserGlobalNotificationContainerType.USER_ADDED_TO_PROCESS);
//            notifyConfig.setValue(true);
//            notificationConfigs.add(notifyConfig);
//            
//            ServiceGraph.addUserNotificationConfig(userB, notificationConfigs);
//            
//            ServiceGraph.followUser(userAdmin, userB);
//            
//            Collection<Post> allNotify = ServiceGraph.getSystemNotifications(userB, new HashMap());
//            
//            ServiceGraph.addUserToProcess(userB, processTest);
//            allNotify = ServiceGraph.getSystemNotifications(userB, new HashMap());
//         
//            userB.setDisplayName("testFen");
//            Post post1 = buildPost("Admin", "AdminAAAAAAAAA", null, "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", new ArrayList());
//            post1.setAuthor("testFen");
//            post1.setUser(userB);
//            
//            List<RSComponent> mentions = new ArrayList<RSComponent>();
//            mentions.add(userAdmin);
//            post1.setMentions(mentions);
//            List<RSComponent> targets = new ArrayList<RSComponent>();
//            targets.add(processTest);
//            post1.setTarget(targets);
//            
//            ServiceSocial.createPost(post1);
//            
//            userAdmin.setUsername("admin");
//            ServiceSocial.setLike(userAdmin, post1.getSys_id(), false);
//            ServiceSocial.setLike(userAdmin, post1.getSys_id(), true);
//            
//            allNotify = ServiceGraph.getSystemNotifications(userB, new HashMap());
//           
//            
//            Post  commentLocal = new Post();
//            commentLocal.setSys_created_on(System.currentTimeMillis());
//            commentLocal.setSys_updated_on(System.currentTimeMillis());
//            commentLocal.setSys_created_by("adminA");
//            commentLocal.setSys_updated_by("adminA");
//            commentLocal.setDisplayName("adminA");
//            commentLocal.setType("COMMENT");
//            commentLocal.setEditRoles("admin");
//            commentLocal.setRoles("admin");
//            commentLocal.setContent("THIS IS A COMMNENT!");
//            
//            
//            
//            // ServiceGraph.addCommentToPost(user, post, comment); //It doesn't matter if this is commented out or not. 
//            post1.addPostComment(commentLocal);
//            ServiceSocial.updatePost(post1);
//            
//        }
//        catch(Exception exp)
//        {
//            System.out.println("==== ok");
//        }
//        
//    }
//    
//    public static void testMovePost()
//    {
//        try
//        {
//            Log.log.info("Start test movePost ");
//            
//            User user = new User();
//            user.setSys_id("8a9482e73d64c915013d64d2087a000c");
//            user.setRoles("admin");
//            user.setDisplayName("admin");
//           
//            Post myPost = buildPost("Admin", "AdminAAAAAAAAA", null, "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", new ArrayList());
//            myPost.setUser(user);
//            
//            Team team = new Team();
//            team.setId("12345");
//            team.setName("myTeam");
//            ServiceGraph.createTeam(team);
//            
//            ServiceGraph.addUserToTeam(user, team);
//            
//            Process pr = new Process();
//            pr.setId("111111");
//            pr.setName("myProcess");
//            ServiceGraph.createProcess(pr);
//            
//            Log.log.info("Post to target " + team.getName());
//            
//            List<RSComponent> data = ServiceSocial.getUserStreams(user);
//                                        
//            List<RSComponent> comp = new ArrayList<RSComponent>();
//            
//            System.out.println("---");
//            
//            
//            
//            comp.add(team);
//            comp.add(new Outbox());
//            myPost.setTarget(comp);
//            ServiceSocial.createPost(myPost);
//            
//            List<Post> rel = ServiceSocial.getPosts(user, new QueryDTO(), "12345", true, null);
//            
//            //get post back 
//            QueryDTO query = new QueryDTO();
//            List<Post> posts = ServiceSocial.getPostById(user, myPost.getSys_id());
//            
//            Post post1 = posts.get(0);
//            
//            Log.log.info("Post1 target name " + ((List<RSComponent>)post1.getTarget()).get(0).getName());
//            
////            Runbook rb1 = new Runbook();
////            rb1.setId("22222");
////            rb1.setName("myRunbook");
////            ServiceGraph.createRunbook(rb1);
//            
////            Log.log.info("move to new target: " + rb1.getName());
////            
////            List<String> targets = new ArrayList<String>();
////            targets.add(rb1.getId());
////            ServiceSocial.movePost(user, post1, targets);
//            
//            List<Post> postsAfter = ServiceSocial.getPostById(user, post1.getSys_id());
//            
//            Post post1After = postsAfter.get(0);
//            
//            
//            Log.log.info("Post1After target name " + ((List<RSComponent>)post1After.getTarget()).get(0).getName());
//            
//        }
//        catch(Exception exp)
//        {
//            
//        }
//        
//    }
//    
//    public static void testNewSocial()
//    {
//        try
//        {
//            
//            QueryDTO queryTest = new QueryDTO();
//            
//            AdvanceTree treeW = ServiceGraph.getAdvanceTree(getUserWithID("718eea88c611227201bb112d7966edf8"));
//           
//            System.out.println("ppp");
//            
//            Collection<Post> allPosts = ServiceGraph.getAllPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), new HashMap());
//            //718eea88c611227201bb112d7966edf8
//            List<Post> thisPosts = ServiceSocial.getPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), queryTest, "718eea88c611227201bb112d7966edf8",  true, null);
//            
//            System.out.println("ppp");
//            
//            System.out.println();
//            
//            Post user11P14 = buildPost("google", "google", "google.com", "google google google google", new ArrayList());
//            //ServiceGraph.postToProcess(getUserWithID("718eea88c611227201bb112d7966edf8"), getProcess("process1"), user11P14);
//            
//            Process processTest = new Process();
//            processTest.setId("8a9482e73f532c71013f533586b80008");
//            
//            System.out.println("----");
//           
//            ServiceGraph.postToProcess(getUserWithID("718eea88c611227201bb112d7966edf8"), processTest, user11P14);
//           
//            AdvanceTree treeWagain = ServiceGraph.getAdvanceTree(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            
//            ServiceSocial.getPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), queryTest, "2905b038c0a8a217003d55da3bf24403",  true, null);
//            
//            //test post to targets list 
//            List<RSComponent> targets = new ArrayList<RSComponent>();
//            targets.addAll(treeW.getProcesses());
//            targets.addAll(treeW.getActionTasks());
//            user11P14.setTarget(targets);
//            user11P14.setUser(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            ServiceSocial.createPost(user11P14);
//            
//            targets.clear();
//            targets.add(new Outbox());
//            ServiceSocial.createPost(user11P14);
//            Collection<Post> allPostsAgain = ServiceGraph.getAllPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), new HashMap());
//            
//            //test move post to new targets
//            List<String> targetIds = new ArrayList<String>();
//            Collection<Forum> allForum = treeW.getForums();
//            Collection<Document> allDocument = treeW.getDocuments();
//            
//            for(Forum forumLocal: allForum)
//            {
//                targetIds.add(forumLocal.getSys_id());
//            }
//            
//            String documentIDAgain = "";
//            
//            for(Document docLocal: allDocument)
//            {
//                documentIDAgain = docLocal.getSys_id();
//                targetIds.add(documentIDAgain);
//            }
//            
//            targetIds.add(treeW.getOwner().getSys_id());
//            ServiceSocial.movePost(getUserWithID("718eea88c611227201bb112d7966edf8"), user11P14, targetIds);
//            
//            Collection<Post> allPostsAgainAgain = ServiceGraph.getAllPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), new HashMap());
//            
//            QueryDTO queryLocalAgain = new QueryDTO();
//            queryLocalAgain.setStart(0);
//            queryLocalAgain.setLimit(50);
//            
//            List<Post> newStreamPosts = ServiceSocial.getPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), queryLocalAgain, documentIDAgain, false, null);      
//            
//            List<Post> postResultsAgainLcoal = ServiceSocial.getPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), queryLocalAgain, "8a9482e73f3a000f013f3a0de2e40006", true, null);
//            
//            
//            System.out.println("==== OK1");
//            System.out.println("==== OK");
//            
//            List<RSComponent> userStreamsLocal = ServiceSocial.getUserStreams(getUserWithID("718eea88c611227201bb112d7966edf8"));
//           
//            Outbox outbox = new Outbox();
//            
//            Post postObjectA = buildPost("Admin", "AdminAAAAAAAAA", null, "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", new ArrayList());
//            postObjectA.setUser(getUserWithID("8a9482e73d64c915013d64d2087a000c"));
//            
//            List<RSComponent> comp = new ArrayList<RSComponent>();
//            comp.add(outbox);
//            postObjectA.setTarget(comp);
//           
//            ServiceSocial.createPost(postObjectA);
//            
//            //ServiceGraph.postToOutbox(getUserWithID("8a9482e73d64c915013d64d2087a000c"), new Outbox(), postObjectA);
//            
//            Collection<Post> post = ServiceGraph.getOutboxPost(getUserWithID("8a9482e73d64c915013d64d2087a000c"), getConfigMap());
//            Post post1 = (Post) ((List) post).get(0);
//            
//            post1.setUser(getUserWithID("8a9482e73d64c915013d64d2087a000c"));
//            
//            Post commentLocal = new Post();
//            String commentContent = "THIS IS A COMMNENT!";
//            for(int i=0; i<2; i++)
//            {
//                commentLocal = new Post();
//                commentLocal.setSys_created_on(System.currentTimeMillis());
//                commentLocal.setSys_updated_on(System.currentTimeMillis());
//                commentLocal.setSys_created_by("adminA");
//                commentLocal.setSys_updated_by("adminA");
//                commentLocal.setDisplayName("adminA");
//                commentLocal.setType("COMMENT");
//                commentLocal.setEditRoles("admin");
//                commentLocal.setRoles("admin");
//                commentLocal.setContent("THIS IS A COMMNENT!");
//                
//                // ServiceGraph.addCommentToPost(user, post, comment); //It doesn't matter if this is commented out or not. 
//                post1.addPostComment(commentLocal);
//                ServiceSocial.updatePost(post1);
//            }
//            
//            QueryDTO queryA = new QueryDTO();
//            queryA.setStart(0);
//            queryA.setLimit(60);
//            
//            User userMe = getUserWithID("8a9482e73d64c915013d64d2087a000c");
//            String postID = post1.getSys_id();
//            List<Post> comments = ServiceSocial.getComments(userMe, queryA, postID);
//            
//            String commentID = "";
//            
//            int i = 0;
//            Post current = null;
//            for(Post commentThis : comments)
//            {
//                if((i%2) == 0)
//                {
//                    current = comments.get(i);
//                    ServiceGraph.setLike(userMe, current.getSys_id(), true);
//                    ServiceGraph.setSolved(userMe, current.getSys_id(), true);
//                    
//                    commentID = current.getSys_id();
//                }
//                
//                i++;
//            }
//            
//            //ServiceGraph.setLike(userMe, post1.getId(), true);
//            
//            List<Post> commentsAgain = ServiceSocial.getComments(userMe, queryA, postID);
//            
//            ServiceGraph.setSolved(userMe, commentID, false);
//            
//            List<Post> commentsAgainAgain = ServiceSocial.getComments(userMe, queryA, postID);
//            
//            
//            //using UI to create graph
//            AdvanceTree tree = ServiceGraph.getAdvanceTree(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            
//            System.out.println("==== ok ");
//            
//            System.out.println(";;");
//            
//            //test getUserStreams
//            List<RSComponent> userStreams = ServiceSocial.getUserStreams(getUserWithID("718eea88c611227201bb112d7966edf8"));
//        
//            Process process = ServiceGraph.getProcess("8a9482e73f3a000f013f3a0de2e40006", true);
//            Collection<Process> procs = ServiceGraph.getAllProcess(true);
//        
//            ServiceGraph.createUser(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            ServiceGraph.createUser(getUserWithID("8a9482e73d64c915013d64d2087a000c"));
//        
//            //test createPOst                    
//            Post postObject = buildPost("Admin", "AdminAAAAAAAAA", "google.com_useradminAAAAAAAAAAAAAAA", "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", new ArrayList());
//            postObject.setUser(getUserWithID("8a9482e73d64c915013d64d2087a000c"));
//            
//            List<RSComponent> targetsLocal = new ArrayList<RSComponent>();
//            targetsLocal.add(process);
//            postObject.setTarget(targetsLocal);
//            ServiceSocial.createPost(postObject);
//            
//            //ServiceGraph.postToOutbox(getUserWithID("718eea88c611227201bb112d7966edf8"), new Outbox(), postUser20);
//           
//            Collection<Post> posts = ServiceGraph.getAllPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), getConfigMap());
//            System.out.println("==== start getOutboxPost() " + posts.size());
//            
//            List<RSComponent> userStreamsA = ServiceSocial.getUserStreams(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            
//            QueryDTO query = new QueryDTO();
//            query.setStart(0);
//            query.setLimit(60);
//            List<Post> postResults = ServiceSocial.getPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), query, "8a9482e73f3a000f013f3a0de2e40006", true, null);
//            
//            System.out.println("==== start getOutboxPost() AAAAAA " + posts.size());
//            System.out.println("==== start getOutboxPost() AAAAAA " + posts.size());
//    
//              //test add comments to post
//              for (Post postLocal : posts)
//              {
//                  Post comment = new Post();
//                  comment.setSys_id("commentSysID1");
//                  comment.setName("admin");
//                  comment.setSys_created_on(new Date().getTime());
//                  comment.setContent("addComment addComment addComment addComment");
//                  ServiceGraph.addCommentToPost(getUserWithID("718eea88c611227201bb112d7966edf8"), postLocal, comment);
//                  
//                  Post commentA = new Post();
//                  commentA.setSys_id("commentSysID1AAAAAAAAAAAAAAA");
//                  commentA.setName("adminAAAAAAAAAAAA");
//                  commentA.setSys_created_on(new Date().getTime());
//                  commentA.setContent("addComment addComment addComment addCommentAAAAAAAAAAAAAAAAA");
//                  ServiceGraph.addCommentToPost(getUserWithID("718eea88c611227201bb112d7966edf8"), postLocal, commentA);
//              }
//            
//              posts = ServiceGraph.getAllPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), getConfigMap());
//              
//              
//              List<Post> postResultsAfter = ServiceSocial.getPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), query, "8a9482e73f3a000f013f3a0de2e40006", true, null);
//              
//              System.out.println("====ok");
//              
//              //test move post to new Target
//              
//              List<Post> postsLocal = new ArrayList<Post>();
//              postsLocal.addAll(posts);
//              
//              Post postAfter = postsLocal.get(1);
//                  
//              Collection<ActionTask> ats = tree.getActionTasks();
//              List<String> newTargetIDS = new ArrayList<String>();
//              
//              for(ActionTask atLocal: ats)
//              {
//                  newTargetIDS.add(atLocal.getSys_id());
//              }
//              
//              ServiceSocial.movePost(getUserWithID("718eea88c611227201bb112d7966edf8"), postAfter, newTargetIDS);
//              User currentUser = getUserWithID("718eea88c611227201bb112d7966edf8");
//              
//              ServiceSocial.setLike(currentUser, postAfter.getSys_id(), true);
//              ServiceSocial.setRead(currentUser, postAfter.getSys_id(), true);
//              ServiceSocial.setLock(currentUser, postAfter.getSys_id(), true);
//              ServiceSocial.setSolved(currentUser, postAfter.getSys_id(), true);
//              
//              List<RSComponent> results = ServiceSocial.getUserStreams(currentUser);
//              
//              List<Post> postResultsA = ServiceSocial.getPosts(getUserWithID("718eea88c611227201bb112d7966edf8"), query, "8a9482e73f3a000f013f3a0de2e40006", true, null);
//              
//              System.out.println("====ok");
//        } 
//        catch(Exception exp)
//        {
//            exp.printStackTrace();
//        }
//    }
//    
//    public static void testNamespace()
//    {
//        try
//        {
//            NameSpaceTree nsTree1 = ServiceGraph.getNameSpaceTree(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            
//            ServiceGraph.purgeAllNameSpacesChild();
//            
//            NameSpaceTree nsTreeB = ServiceGraph.getNameSpaceTree(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            
//            
//            Runbook runbook = new Runbook();
//            runbook.setDisplayName("testresolve.index1");
//            ServiceGraph.addRSComponentToNameSpace(runbook);
//            
//            Runbook runbook1 = new Runbook();
//            runbook1.setDisplayName("fen.index1");
//            ServiceGraph.addRSComponentToNameSpace(runbook);
//            
//            
//            ServiceGraph.purgeAllNameSpacesChild();
//            
//            
//            
//            Namespace namespace = new Namespace();
//            namespace.setId("testresolve");
//            namespace.setName("testresolve");
//            
//            ServiceGraph.followNameSpace(getUserWithID("718eea88c611227201bb112d7966edf8"), namespace);
//            
//            
//            
//            NameSpaceTree nsTree = ServiceGraph.getNameSpaceTree(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            
//            ServiceGraph.purgeAllNameSpacesChild();
//            
//            System.out.print("");
//            
//            NameSpaceTree nsTreeA = ServiceGraph.getNameSpaceTree(getUserWithID("718eea88c611227201bb112d7966edf8"));
//            
//            ServiceGraph.followNameSpace(getUserWithID("718eea88c611227201bb112d7966edf8"), namespace);
//        }
//        catch(Exception exp)
//        {
//            
//        }
//
//    }
//
//    public static void getPost()
//    {
//        System.out.println("====OK");
//        Set<String> hashTags = ServiceGraph.getAllHashTagsFromIndex();
//        QueryDTO temp = new QueryDTO();
//        temp.setQuery("123");
//        List<ResolveTag> hashTagsLocal = ServiceGraph.getAllTagsFromGraph(temp);
//
//        Set<String> hashTagsLocalL = ServiceGraph.getAllHashTagsFromIndex("123", getConfigMap());
//
//        System.out.println("------ end ");
//        
//        
//        ServiceGraph.createUser(getUserWithID("8a9482e73c269775013c26b7ac7c0007"));
//
//        Post postUser20 = buildPost("user20Blog1", "user20Blog1", "google.com_user20Blog1", "user20Blog1 user20Blog1 user20Blog1 user20Blog1", new ArrayList());
//        ServiceGraph.postToOutbox(getUserWithID("8a9482e73c269775013c26b7ac7c0007"), new Outbox(), postUser20);
// 
//        Collection<Post> posts = ServiceGraph.getInboxPost(getUserWithID("8a9482e73c269775013c26b7ac7c0007"), getConfigMap());
//        System.out.println("==== start getInboxPost() " + posts.size());
//
//        posts = ServiceGraph.getAllPosts(getUserWithID("8a9482e73c269775013c26b7ac7c0007"), getConfigMap());
//        System.out.println("==== start getOutboxPost() " + posts.size());
//
//        Comment comment = new Comment();
//        comment.setSys_id("commentSysID1");
//        comment.setUserName("admin");
//        comment.setSys_created_on(new Date().getTime());
//        comment.setCommentText("addComment addComment addComment addComment");
//
//        for (Post postLocal : posts)
//        {
//            //TODO -- using new comment
//            //ServiceGraph.addComment(getUserWithID("8a9482e73c269775013c26b7ac7c0007"), postLocal, comment);
//        }
//
//        posts = ServiceGraph.getOutboxPost(getUserWithID("8a9482e73c269775013c26b7ac7c0007"), getConfigMap());
//
//        System.out.println("====");
//        
//        System.out.println("'''''");
//        
//        System.out.println("------ end ");
//        
//        if (posts != null)
//        {
//            for (Post postLocal : posts)
//            {
//                ServiceGraph.deleteComment(postLocal.getSys_id(), "commentSysID1", "admin");
//                // SocialStore.deletePost(postLocal.getSys_id());
//            }
//        }
//
//        posts = ServiceGraph.getOutboxPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("==== start getOutboxPost() " + posts.size());
//
//        posts = ServiceGraph.getOutboxPost(getUserWithID("user20"), getConfigMap());
//
//        Collection<Post> postsLocal = ServiceGraph.getInboxPost(getUserWithID("user11"), getConfigMap());
//        posts = ServiceGraph.getSentPostByUser(getUserWithID("user11"), getConfigMap());
//
//        addCommentsForOutboxUser20();
//
//        posts = ServiceGraph.getOutboxPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("==== start getOutboxPost() " + posts.size());
//
//        posts = ServiceGraph.getAllUsersOutBoxPost(getUserWithID("user20"), getConfigMap());
//
//        posts = ServiceGraph.getUserOutBoxPost(getUserWithID("user20"), getUserWithID("user11"), getConfigMap());
//
//        posts = ServiceGraph.getUserOutBoxPost(getUserWithID("user20"), getUserWithID("user1"), getConfigMap());
//
//        posts = ServiceGraph.getStarredPost(getUserWithID("user11"), getConfigMap());
//        System.out.println("==================getStarredPost user11 ============" + posts.size());
//
//        posts = ServiceGraph.getStarredPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("==================getStarredPost user20 ============" + posts.size());
//
//        posts = ServiceGraph.getAllRsssPost(getUserWithID("user11"), getConfigMap());
//        System.out.println("=== start getAllRsssPost ==" + posts.size());
//
//        posts = ServiceGraph.getRssPost(getUserWithID("user20"), getRss("rss1"), getConfigMap());
//        System.out.println("=== start getRssPost rss1 ==" + posts.size());
//
//        posts = ServiceGraph.getRssPost(getUserWithID("user20"), getRss("rss2"), getConfigMap());
//
//        posts = ServiceGraph.getRssPost(getUserWithID("user11"), getRss("rss2"), getConfigMap());
//
//        System.out.println("=== start getRssPost rss1 ==" + posts.size());
//
//        posts = ServiceGraph.getAllRunbooksPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("=== start getAllRunbooksPost ==" + posts.size());
//
//        posts = ServiceGraph.getRunbookPost(getUserWithID("user20"), getRunbook("rb2"), getConfigMap());
//        System.out.println("=== start getRunbookPost rb2 ==" + posts.size());
//
//        posts = ServiceGraph.getAllDocumentsPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("=== start getAllRunbooksPost ==" + posts.size());
//
//        posts = ServiceGraph.getDocumentPost(getUserWithID("user20"), getDocument("doc1"), getConfigMap());
//        System.out.println("=== start getAllRunbooksPost doc1 ==" + posts.size());
//
//        posts = ServiceGraph.getAllActionTasksPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("=== start getAllActionTasksPost ==" + posts.size());
//
//        posts = ServiceGraph.getActionTaskPost(getUserWithID("user20"), getActionTaskWithID("at2"), getConfigMap());
//        System.out.println("=== start getActionTaskPost at2==" + posts.size());
//
//        posts = ServiceGraph.getAllForumsPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("=== start getAllForumsPost == " + posts.size());
//
//        posts = ServiceGraph.getForumPost(getUserWithID("user20"), getForum("forum1"), getConfigMap());
//        System.out.println("=== start getAllForumsPost forum1 ==" + posts.size());
//
//        posts = ServiceGraph.getAllTeamsPost(getUserWithID("user20"), getConfigMap());
//
//        System.out.println("==== start getAllTeamsPost ===" + posts.size());
//
//        System.out.println("====");
//
//        posts = ServiceGraph.getAllTeamsPost(getUserWithID("user20"), getTeam("team4"), getConfigMap());
//        System.out.println("==== getAllTeamsPost(user, rootTeam) team4 ==" + posts.size());
//
//        posts = ServiceGraph.getAllTeamsPost(getUserWithID("user20"), getTeam("team1"), getConfigMap());
//
//        System.out.println("==== getAllTeamsPost(user, rootTeam) team1  aaac==" + posts.size());
//
//        posts = ServiceGraph.getAllTeamsPost(getUserWithID("user20"), getTeam("team1"), getUserWithID("user11"), getConfigMap());
//        System.out.println("==== getAllTeamsPost(User user, Team rootTeam, User srcUser) rootTeam = team1 and srcUser= user11 " + posts.size());
//
//        posts = ServiceGraph.getAllProcessesPost(getUserWithID("user20"), getConfigMap());
//        System.out.println("==== get getAllProcessesPost ===" + posts.size());
//
//        posts = ServiceGraph.getAllProcessesPost(getUserWithID("user20"), getProcess("process1"), getConfigMap());
//        System.out.println("==== get getAllProcessesPost ===" + posts.size());
//
//        System.out.println("====");
//
//        posts = ServiceGraph.getAllProcessesPost(getUserWithID("user20"), getProcess("process1"), getUserWithID("user5"), getConfigMap());
//        System.out.println("==== getAllProcessesPost ===" + posts.size());
//
//        posts = ServiceGraph.getAllPosts(getUserWithID("user20"), getConfigMap());
//        System.out.println("==== start getAllPosts() " + posts.size());
//
//    }
//
//    public static void testTree()
//    {
//        System.out.println("==== start");
//        AdvanceTree advTree = ServiceGraph.getAdvanceTree(getUserWithID("user20"));
//        ProcessTree prcTree = ServiceGraph.getProcessTree(getUserWithID("user20"));
//        TeamTree teamTree = ServiceGraph.getTeamTree(getUserWithID("user20"));
//        UserTree userTree = ServiceGraph.getUserTree(getUserWithID("user20"));
//        ForumTree forumTree = ServiceGraph.getForumsTree(getUserWithID("user20"));
//
//        Set<String> parentSysID = ServiceGraph.getParentsOfTeam("team7");
//        Team teamLocal = ServiceGraph.getTeam("team1", true);
//
//        ActionTaskTree actionTaskTree = ServiceGraph.getActionTaskTree(getUserWithID("user20"));
//        RunbookTree runbookTree = ServiceGraph.getRunbookTree(getUserWithID("user20"));
//        DocumentTree docTree = ServiceGraph.getDocumentTree(getUserWithID("user20"));
//        RssTree rssTree = ServiceGraph.getRssTree(getUserWithID("user20"));
//
//        ServiceGraph.addUserToForum(getUserWithID("user20"), getForum("forum2"));
//
//    }
//
//    public static void testRssFollwerCount()
//    {
//        System.out.println("==== the rss1 FollwerCount is : " + ServiceGraph.getRssFollwerCount(getRss("rss1")));
//        System.out.println("==== the rss2 FollwerCount is : " + ServiceGraph.getRssFollwerCount(getRss("rss2")));
//    }
//
//    public static void addComment()
//    {
//        User user11Local = getUserWithID("user11");
//        user11Local.setRoles("admin resolve_dev");
//        Collection<Post> rssPosts = ServiceGraph.getAllRsssPost(user11Local, getConfigMap());
//
//        // Collection<Post> posts = SocialStore.getInboxPost(getUserWithID("user20"), getConfigMap());
//
//        Comment comment = new Comment();
//        comment.setSys_id("commentSysID1");
//        comment.setUserName("admin");
//        comment.setSys_created_on(new Date().getTime());
//        comment.setCommentText("addComment addComment addComment addComment");
//
//        for (Post post : rssPosts)
//        {
//            //TODO -- using new comment
//            //ServiceGraph.addComment(user11Local, post, comment);
//        }
//
//        rssPosts = ServiceGraph.getAllRsssPost(user11Local, getConfigMap());
//
//        String end = "";
//
//    }
//
//    public static void markStarred()
//    {
//        Post user11P1 = buildPost("user11P1", "user11P1", "user11P1.com", "user11P1 user11P1 user11P1 user11P1", new ArrayList());
//        ServiceGraph.markStarred(getUserWithID("user20"), user11P1);
//    }
//
//    public static void getSocial()
//    {
//        // SocialStore.getInboxPost(getUserWithID("user11"));
//        // SocialStore.getInboxPost(getUserWithID("user20"));
//        //
//        // SocialStore.getOutboxPost(getUserWithID("user11"));
//        // SocialStore.getOutboxPost(getUserWithID("user20"));
//        //
//        // SocialStore.getSentPostByUser(getUserWithID("user11"));
//        // SocialStore.getSentPostByUser(getUserWithID("user20"));
//
//        ServiceGraph.getOutboxPost(getUserWithID("user20"), new HashMap<String, Object>());
//        System.out.println("============== getStarredPost ===============");
//
//        ServiceGraph.getStarredPost(getUserWithID("user11"), new HashMap<String, Object>());
//        System.out.println("==================getStarredPost ============");
//        ServiceGraph.getStarredPost(getUserWithID("user20"), new HashMap<String, Object>());
//    }
//
//    public static void deleteSocial()
//    {
//        ServiceGraph.deleteProcess(getProcess("process1"));
//
//        ServiceGraph.deleteForum(getForum("forum1"));
//
//        ServiceGraph.deleteRunbook(getRunbook("rb1"));
//        ServiceGraph.deleteRunbook(getRunbook("rb2"));
//        ServiceGraph.deleteRunbook(getRunbook("rb3"));
//
//        ServiceGraph.deleteActionTask(getActionTaskWithID("at1"));
//        ServiceGraph.deleteActionTask(getActionTaskWithID("at2"));
//        ServiceGraph.deleteActionTask(getActionTaskWithID("at3"));
//
//        ServiceGraph.deleteRss(getRss("rss1"));
//        ServiceGraph.deleteRss(getRss("rss2"));
//        ServiceGraph.deleteRss(getRss("rss3"));
//
//        ServiceGraph.deleteTeam(getTeam("team1"));
//        ServiceGraph.deleteTeam(getTeam("team2"));
//        ServiceGraph.deleteTeam(getTeam("team3"));
//        ServiceGraph.deleteTeam(getTeam("team4"));
//        ServiceGraph.deleteTeam(getTeam("team5"));
//        ServiceGraph.deleteTeam(getTeam("team6"));
//        ServiceGraph.deleteTeam(getTeam("team7"));
//
//        ServiceGraph.deleteProcess(getProcess("process1"));
//
//        ServiceGraph.deleteUser(getUserWithID("user11"));
//        ServiceGraph.deleteUser(getUserWithID("user20"));
//
//    }
//
//    public static User getUserWithID(String id)
//    {
//        User user = new User();
//        user.setSys_id(id);
//        user.setRoles("admin");
//        user.setDisplayName(id);
//        return user;
//    }
//
//    public static ActionTask getActionTaskWithID(String actionTaskID)
//    {
//        ActionTask at = new ActionTask();
//        at.setSys_id(actionTaskID);
//        at.setDisplayName(actionTaskID);
//
//        return at;
//    }
//
//    public static Runbook getRunbook(String runBookID)
//    {
//        Runbook runbook = new Runbook();
//        runbook.setSys_id(runBookID);
//        runbook.setDisplayName(runBookID);
//
//        return runbook;
//    }
//
//    public static Document getDocument(String documentID)
//    {
//        Document document = new Document();
//        document.setSys_id(documentID);
//        document.setDisplayName(documentID);
//
//        return document;
//    }
//
//    public static Rss getRss(String rssID)
//    {
//        Rss rss = new Rss();
//        rss.setSys_id(rssID);
//        rss.setDisplayName(rssID);
//        return rss;
//    }
//
//    public static Forum getForum(String forumID)
//    {
//        Forum forum = new Forum();
//        forum.setSys_id(forumID);
//        forum.setDisplayName(forumID);
//
//        return forum;
//    }
//
//    public static Team getTeam(String teamID)
//    {
//        Team team = new Team();
//        team.setSys_id(teamID);
//        team.setDisplayName(teamID);
//        return team;
//    }
//
//    public static Process getProcess(String processID)
//    {
//        Process process = new Process();
//        process.setSys_id(processID);
//        process.setDisplayName(processID);
//        return process;
//    }
//
//    public static SystemNotification buildSystemNotifaction(String sys_id, String title, String titleUrl, String content, UserGlobalNotificationContainerType notifyType, List<Comment> comments)
//    {
//        SystemNotification notif = new SystemNotification(notifyType);
//
//        notif.setDisplayName(sys_id);
//        notif.setTitle(title);
//        notif.setTitleUrl(titleUrl);
//        notif.setContent(content);
//        notif.setAuthor("admin");
//        notif.addComments(comments);
//        notif.setSys_created_on(new Date().getTime());
//        notif.setSys_updated_on(new Date().getTime());
//        notif.setSys_created_by("admin");
//        notif.setSys_updated_by("admin");
//
//        notif.setRoles("admin");
//
//        long expiration = new Date().getTime() - 2 * 24 * 60 * 60 * 1000;
//        notif.setExpiration(expiration);
//
//        return notif;
//    }
//
//    public static Post buildPost(String sys_id, String title, String titleUrl, String content, List<Comment> comments)
//    {
//        Post post = new Post();
//        // post.setSys_id(sys_id);
//        post.setDisplayName(sys_id);
//        post.setTitle(title);
//        post.setTitleUrl(titleUrl);
//        post.setContent(content);
//        post.setAuthor("admin");
//        //post.setType(Post.POST_TYPE);
//        post.addComments(comments);
//        post.setSys_created_on(new Date().getTime());
//        post.setSys_updated_on(new Date().getTime());
//        post.setSys_created_by("admin");
//        post.setSys_updated_by("admin");
//        post.setRoles("admin");
//        post.setEditRoles("admin");
//
//        long expiration = new Date().getTime() - 2 * 24 * 60 * 60 * 1000;
//        post.setExpiration(expiration);
//
//        return post;
//    }
//    
//
//    public static void post()
//    {
//        // user20
//        User user20 = getUserWithID("admin");
//        Outbox outboxUser20 = new Outbox();
//        outboxUser20.setSys_id("user20Outbox");
//        Post postUser20 = buildPost("user20Blog1", "user20Blog1", "google.com_user20Blog1", "user20Blog1 user20Blog1 user20Blog1 user20Blog1", new ArrayList());
//        ServiceGraph.postToOutbox(user20, outboxUser20, postUser20);
//
//        List<RSComponent> mentions = new ArrayList();
//        mentions.add(user20);
//
//        User user11 = getUserWithID("user11");
//        Outbox outboxUser11 = new Outbox();
//        outboxUser11.setSys_id("user11Outbox");
//        Post user11Blog1 = buildPost("user11Blog1", "user11Blog1", "user11Blog1.com", "user11Blog1 user11Blog1 user11Blog1 user11Blog1", new ArrayList());
//        ServiceGraph.postToOutbox(user11, outboxUser11, user11Blog1, mentions);
//
//        Post user11P1 = buildPost("user11P1", "user11P1", "user11P1.com", "user11P1 user11P1 user11P1 user11P1", new ArrayList());
//        ServiceGraph.postToUser(user11, user20, user11P1);
//
//        String start = "";
//
//        Post user11P2 = buildPost("user11P2", "user11P2", "user11P2.com", "user11P2 user11P2 user11P2 user11P2", new ArrayList());
//        ServiceGraph.postToActionTask(user11, getActionTaskWithID("at1"), user11P2);
//
//        Post user11P3 = buildPost("user11P3", "user11P3", "user11P3.com", "user11P3 user11P3 user11P3 user11P3", new ArrayList());
//        ServiceGraph.postToActionTask(user11, getActionTaskWithID("at2"), user11P3);
//
//        Post user11P4 = buildPost("user11P4", "user11P4", "user11P4.com", "user11P4 user11P4 user11P4 user11P4", new ArrayList());
//        ServiceGraph.postToActionTask(user11, getActionTaskWithID("at3"), user11P4);
//
//        Post user11P5 = buildPost("user11P5", "user11P5", "user11P5.com", "user11P5 user11P5 user11P5 user11P5", new ArrayList());
//        ServiceGraph.postToRunbook(user11, getRunbook("rb2"), user11P5);
//
//        Post user11P6 = buildPost("user11P6", "user11P6", "user11P6.com", "user11P6 user11P6 user11P6 user11P6", new ArrayList());
//        ServiceGraph.postToRunbook(user11, getRunbook("rb3"), user11P6);
//
//        Post user11P7 = buildPost("user11P7", "user11P7", "user11P7.com", "user11P7 user11P7 user11P7 user11P7", new ArrayList());
//        ServiceGraph.postToRunbook(user11, getRunbook("rb1"), user11P7);
//
//        Post user11PostDoc1 = buildPost("user11PostDoc1", "user11PostDoc1", "user11PostDoc1.com", "user11PostDoc1 user11PostDoc1 user11PostDoc1 user11PostDoc1", new ArrayList());
//        ServiceGraph.postToDocument(user11, getDocument("doc5"), user11PostDoc1);
//
//        Post user11PostDoc2 = buildPost("user11PostDoc2", "user11PostDoc2", "user11PostDoc2.com", "user11PostDoc2 user11PostDoc2 user11PostDoc2 user11PostDoc2", new ArrayList());
//        ServiceGraph.postToDocument(user11, getDocument("doc2"), user11PostDoc2);
//
//        Post user11P8 = buildPost("rss1Post", "rss1Post", "rss1Post.com", "rss1Post rss1Post rss1Post rss1Post", new ArrayList());
//        user11P8.setRoles("admin resolve_dev");
//        ServiceGraph.postToRss(user11, getRss("rss1"), user11P8);
//
//        Collection<Post> posts = ServiceGraph.getAllPosts(getUserWithID("user11"), getConfigMap());
//
//        Post user11P15 = buildPost("user11P15", "user11P15", "user11P15.com", "user11P15 user11P15 user11P15 user11P15", new ArrayList());
//        user11P15.setRoles("admin resolve_dev");
//        ServiceGraph.postToRss(user11, getRss("rss2"), user11P15);
//
//        Post user11P11 = buildPost("user11P11", "user11P11", "user11P11.com", "user11P11 user11P11 user11P11 user11P11", new ArrayList());
//        ServiceGraph.postToForum(user11, getForum("forum1"), user11P11);
//
//        // post 1000 post to team1
//
//        Post user11P12 = buildPost("user11P12", "user11P12", "user11P12.com", "user11P12 user11P12 user11P12 user11P12", new ArrayList());
//        ServiceGraph.postToTeam(user11, getTeam("team1"), user11P12);
//
//        posts = ServiceGraph.getAllPosts(getUserWithID("user20"), getConfigMap());
//
//        Post user11P13 = buildPost("user11P13", "user11P13", "user11P13.com", "user11P13 user11P13 user11P13 user11P13", new ArrayList());
//        ServiceGraph.postToTeam(user11, getTeam("team2"), user11P13);
//
//        Post user11P16 = buildPost("user11P16", "user11P16", "user11P16.com", "user11P16 user11P16 user11P16 user11P16", new ArrayList());
//        ServiceGraph.postToTeam(user11, getTeam("team3"), user11P16);
//
//        Post user11P17 = buildPost("user11P17", "user11P17", "user11P17.com", "user11P17 user11P17 user11P17 user11P17", new ArrayList());
//        ServiceGraph.postToTeam(user11, getTeam("team4"), user11P17);
//
//        // post 1000 post to team4
//        Post user11P18 = buildPost("user11P18", "user11P18", "user11P18.com", "user11P18 user11P18 user11P18 user11P18", new ArrayList());
//        ServiceGraph.postToTeam(user11, getTeam("team5"), user11P18);
//
//        Post user11P19 = buildPost("user11P19", "user11P19", "user11P19.com", "user11P19 user11P19 user11P19 user11P19", new ArrayList());
//        ServiceGraph.postToTeam(user11, getTeam("team7"), user11P19);
//
//        Post user11P14 = buildPost("user11P14", "user11P14", "user11P14.com", "user11P14 user11P14 user11P14 user11P14", new ArrayList());
//        ServiceGraph.postToProcess(user11, getProcess("process1"), user11P14);
//    }
//
//    public static void createUserReader()
//    {
//        try
//        {
//            // build teams and teams in teams
//            ServiceGraph.addUserToTeam(getUserWithID("user12"), getTeam("team3"));
//            ServiceGraph.addUserToTeam(getUserWithID("user13"), getTeam("team3"));
//            ServiceGraph.addUserToTeam(getUserWithID("user20"), getTeam("team3"));
//
//            ServiceGraph.addUserToTeam(getUserWithID("user14"), getTeam("team4"));
//            ServiceGraph.addUserToTeam(getUserWithID("user15"), getTeam("team4"));
//            ServiceGraph.addUserToTeam(getUserWithID("user20"), getTeam("team4"));
//
//            ServiceGraph.addUserToTeam(getUserWithID("user16"), getTeam("team5"));
//            ServiceGraph.addUserToTeam(getUserWithID("user17"), getTeam("team5"));
//            ServiceGraph.addTeamToTeam(getTeam("team5"), getTeam("team4"));
//
//            ServiceGraph.addUserToTeam(getUserWithID("user7"), getTeam("team6"));
//            ServiceGraph.addUserToTeam(getUserWithID("user20"), getTeam("team6"));
//            ServiceGraph.addUserToTeam(getUserWithID("user16"), getTeam("team6"));
//            ServiceGraph.addUserToTeam(getUserWithID("user17"), getTeam("team6"));
//
//            ServiceGraph.addUserToTeam(getUserWithID("user20"), getTeam("team7"));
//
//            ServiceGraph.addUserToTeam(getUserWithID("user3"), getTeam("team1"));
//            ServiceGraph.addUserToTeam(getUserWithID("user4"), getTeam("team1"));
//
//            ServiceGraph.addUserToTeam(getUserWithID("user5"), getTeam("team2"));
//            ServiceGraph.addUserToTeam(getUserWithID("user6"), getTeam("team2"));
//            ServiceGraph.addUserToTeam(getUserWithID("user20"), getTeam("team2"));
//            ServiceGraph.addUserToTeam(getUserWithID("user11"), getTeam("team2"));
//
//            // build forum
//            ServiceGraph.addUserToForum(getUserWithID("user9"), getForum("forum1"));
//            ServiceGraph.addUserToForum(getUserWithID("user10"), getForum("forum1"));
//            ServiceGraph.addUserToForum(getUserWithID("user20"), getForum("forum1"));
//            // SocialStore.followForum(getUserWithID("user20"), getForum("forum1"));
//
//            // build process
//            ServiceGraph.addUserToProcess(getUserWithID("user1"), getProcess("process1"));
//            ServiceGraph.addUserToProcess(getUserWithID("user2"), getProcess("process1"));
//
//            ServiceGraph.addTeamToProcess(getTeam("team1"), getProcess("process1"));
//            ServiceGraph.addActionTaskToProcess(getActionTaskWithID("at1"), getProcess("process1"));
//            ServiceGraph.addRunbookToProcess(getRunbook("rb1"), getProcess("process1"));
//            ServiceGraph.addDocumentToProcess(getDocument("doc1"), getProcess("process1"));
//            ServiceGraph.subscribeRssToProcess(getRss("rss1"), getProcess("process1"));
//            ServiceGraph.addUserToForum(getUserWithID("user19"), getForum("forum2"));
//            ServiceGraph.addForumToProcess(getForum("forum2"), getProcess("process1"));
//
//            // build followers
//            // follower users
//            ServiceGraph.followUser(getUserWithID("user20"), getUserWithID("user7"));
//            ServiceGraph.followUser(getUserWithID("user20"), getUserWithID("user8"));
//            ServiceGraph.followUser(getUserWithID("user20"), getUserWithID("user11"));
//
//            // follower ActionTask. Runbook, Document
//            ServiceGraph.followActionTask(getUserWithID("user20"), getActionTaskWithID("at2"));
//            ServiceGraph.followActionTask(getUserWithID("user20"), getActionTaskWithID("at3"));
//            ServiceGraph.followRunbook(getUserWithID("user20"), getRunbook("rb2"));
//            ServiceGraph.followRunbook(getUserWithID("user20"), getRunbook("rb3"));
//            ServiceGraph.followDocument(getUserWithID("user20"), getDocument("doc2"));
//            ServiceGraph.followDocument(getUserWithID("user7"), getDocument("doc5"));
//
//            boolean isFollow = ServiceGraph.isUserDirstFollowTheComponent(getUserWithID("user20"), getActionTaskWithID("at2"));
//
//            // user follow rss
//            ServiceGraph.subscribeRssToUser(getRss("rss2"), getUserWithID("user20"));
//            ServiceGraph.subscribeRssToUser(getRss("rss3"), getUserWithID("user20"));
//
//            ServiceGraph.addTeamToTeam(getTeam("team6"), getTeam("team4"));
//            ServiceGraph.addTeamToTeam(getTeam("team7"), getTeam("team6"));
//            ServiceGraph.addTeamToTeam(getTeam("team2"), getTeam("team1"));
//
//            // SocialStore.addTeamToTeam(getTeam("team4"), getTeam("team2"));
//
//            List<String> teams = new ArrayList<String>();
//            teams.add("team2");
//            ServiceGraph.validateTeams(getTeam("team4"), teams, 7);
//
//        }
//        catch (Throwable exp)
//        {
//            exp.printStackTrace();
//        }
//    }
//
//    public static void removeUserFromProcess()
//    {
//        User user = new User();
//        user.setSys_id("user1");
//
//        Process process = new Process();
//        process.setSys_id("process1");
//
//        ServiceGraph.removeUserFromProcess(user, process);
//
//    }
//
//    public static void unsubscribeRssFromUser()
//    {
//        Rss rss = new Rss();
//        rss.setSys_id("rss1");
//
//        User user = new User();
//        user.setSys_id("user1");
//
//        try
//        {
//            UpdateSocial.unsubscribeRssFromUser(rss, user);
//        }
//        catch (Exception exp)
//        {
//
//        }
//    }
//
//    public static void addUserToProcess()
//    {
//        User user = new User();
//        user.setSys_id("user1");
//
//        Process process = new Process();
//        process.setSys_id("process1");
//
//        ServiceGraph.addUserToProcess(user, process);
//
//        User user2 = new User();
//        user2.setSys_id("user2");
//
//        ServiceGraph.addUserToProcess(user2, process);
//    }
//
//    public static void addUserToTeam()
//    {
//        User user3 = new User();
//        user3.setSys_id("user3");
//
//        Team team1 = new Team();
//        team1.setSys_id("team1");
//
//        User user4 = new User();
//        user4.setSys_id("user4");
//
//        ServiceGraph.addUserToTeam(user3, team1);
//        ServiceGraph.addUserToTeam(user4, team1);
//
//        User user5 = new User();
//        user5.setSys_id("user5");
//
//        User user6 = new User();
//        user6.setSys_id("user6");
//
//        Team team2 = new Team();
//        team2.setSys_id("team2");
//
//        ServiceGraph.addUserToTeam(user5, team2);
//        ServiceGraph.addUserToTeam(user6, team2);
//    }
//
//    public static void addUserToForum()
//    {
//        User user = new User();
//        user.setSys_id("user1");
//
//        Forum forum = new Forum();
//        forum.setSys_id("forum1");
//
//        ServiceGraph.addUserToForum(user, forum);
//    }
//
//    public static void addActionTaskToProcess()
//    {
//        ActionTask at = new ActionTask();
//        at.setSys_id("at1");
//
//        Process process = new Process();
//        process.setSys_id("process1");
//
//        ServiceGraph.addActionTaskToProcess(at, process);
//    }
//
//    public static void addRunbookToProcess()
//    {
//        Runbook rb = new Runbook();
//        rb.setSys_id("rb1");
//
//        Process process = new Process();
//        process.setSys_id("process1");
//
//        ServiceGraph.addRunbookToProcess(rb, process);
//    }
//
//    public static void subscribeRssToProcess()
//    {
//        Rss rss = new Rss();
//        rss.setSys_id("rss1");
//
//        Process process = new Process();
//        process.setSys_id("process1");
//
//        ServiceGraph.subscribeRssToProcess(rss, process);
//    }
//
//    public static void subscribeRssToUser()
//    {
//        Rss rss = new Rss();
//        rss.setSys_id("rss1");
//
//        User user = new User();
//        user.setSys_id("user1");
//
//        ServiceGraph.subscribeRssToUser(rss, user);
//    }
//
//    public static void followUser()
//    {
//        User user1 = new User();
//        user1.setSys_id("user1");
//
//        User user2 = new User();
//        user2.setSys_id("user2");
//
//        ServiceGraph.followUser(user1, user2);
//    }
//
//    public static void followActionTask()
//    {
//        User user1 = new User();
//        user1.setSys_id("user1");
//
//        ActionTask actionTask = new ActionTask();
//        actionTask.setSys_id("at1");
//
//        ServiceGraph.followActionTask(user1, actionTask);
//    }
//
//    public static void followRunbook()
//    {
//        User user1 = new User();
//        user1.setSys_id("user1");
//
//        Runbook runbook = new Runbook();
//        runbook.setSys_id("rb1");
//
//        ServiceGraph.followRunbook(user1, runbook);
//    }
//
//    public static void initAllSocialContainer()
//    {
//        SocialUsersFactory.init();
//        SocialProcessFactory.init();
//        SocialTeamFactory.init();
//        SocialForumFactory.init();
//        SocialActionTaskFactory.init();
//        SocialRunbookFactory.init();
//        SocialDocumentFactory.init();
//        SocialRssFactory.init();
//        SocialPostFactory.init();
//        SocialNotificationFactory.init();
//    }
//
//    /**
//     * Create 10 users
//     */
//    public static void createUsers(int offset, int totalUsers)
//    {
//        User user = null;
//
//        for (int i = offset; i < offset + totalUsers; i++)
//        {
//            user = new User();
//            user.setSys_id("user" + i);
//            user.setDisplayName("user" + i);
//            user.setSys_created_by("admin");
//            user.setSys_updated_by("admin");
//            user.setSys_created_on(new Date().getTime());
//            user.setSys_updated_on(new Date().getTime());
//
//            ServiceGraph.createUser(user);
//        }
//    }
//
//    public static void createActionTask(int offset, int totalCount)
//    {
//        ActionTask obj = null;
//
//        for (int i = offset; i < offset + totalCount; i++)
//        {
//            obj = new ActionTask();
//            obj.setSys_id("at" + i);
//            obj.setDisplayName("at" + i);
//            obj.setSys_created_by("admin");
//            obj.setSys_updated_by("admin");
//            obj.setSys_created_on(new Date().getTime());
//            obj.setSys_updated_on(new Date().getTime());
//            ServiceGraph.createActionTask(obj);
//        }
//    }
//
//    public static void createRunbook(int offset, int totalCount)
//    {
//        Runbook obj = null;
//
////        for (int i = offset; i < offset + totalCount; i++)
////        {
////            obj = new Runbook();
////            obj.setSys_id("rb" + i);
////            obj.setDisplayName("rb" + i);
////            obj.setSys_created_by("admin");
////            obj.setSys_updated_by("admin");
////            obj.setSys_created_on(new Date().getTime());
////            obj.setSys_updated_on(new Date().getTime());
////            ServiceGraph.createRunbook(obj);
////        }
//    }
//
//    public static void createDocument(int offset, int totalCount)
//    {
//        Document obj = null;
//
//        for (int i = offset; i < offset + totalCount; i++)
//        {
//            obj = new Document();
//            obj.setSys_id("doc" + i);
//            obj.setDisplayName("doc" + i);
//            obj.setSys_created_by("admin");
//            obj.setSys_updated_by("admin");
//            obj.setSys_created_on(new Date().getTime());
//            obj.setSys_updated_on(new Date().getTime());
//            ServiceGraph.createDocument(obj);
//        }
//    }
//
//    public static void createProcess(int offset, int totalCount)
//    {
//        Process obj = null;
//
//        for (int i = offset; i < offset + totalCount; i++)
//        {
//            obj = new Process();
//            obj.setSys_id("process" + i);
//            obj.setDisplayName("process" + i);
//            obj.setSys_created_by("admin");
//            obj.setSys_updated_by("admin");
//            obj.setSys_created_on(new Date().getTime());
//            obj.setSys_updated_on(new Date().getTime());
//            ServiceGraph.createProcess(obj);
//        }
//    }
//
//    public static void createTeam(int offset, int totalCount)
//    {
//        Team obj = null;
//
//        for (int i = offset; i < offset + totalCount; i++)
//        {
//            obj = new Team();
//            obj.setSys_id("team" + i);
//            obj.setDisplayName("team" + i);
//            obj.setSys_created_by("admin");
//            obj.setSys_updated_by("admin");
//            obj.setSys_created_on(new Date().getTime());
//            obj.setSys_updated_on(new Date().getTime());
//            ServiceGraph.createTeam(obj);
//        }
//    }
//
//    public static void createForum(int offset, int totalCount)
//    {
//        Forum obj = null;
//
//        for (int i = offset; i < offset + totalCount; i++)
//        {
//            obj = new Forum();
//            obj.setSys_id("forum" + i);
//            obj.setDisplayName("forum" + i);
//            obj.setSys_created_by("admin");
//            obj.setSys_updated_by("admin");
//            obj.setSys_created_on(new Date().getTime());
//            obj.setSys_updated_on(new Date().getTime());
//            ServiceGraph.createForum(obj);
//        }
//    }
//
//    public static void createRss(int offset, int totalCount)
//    {
//        Rss obj = null;
//
//        for (int i = offset; i < offset + totalCount; i++)
//        {
//            obj = new Rss();
//            obj.setSys_id("rss" + i);
//            obj.setDisplayName("rss" + i);
//            obj.setSys_created_by("admin");
//            obj.setSys_updated_by("admin");
//            obj.setSys_created_on(new Date().getTime());
//            obj.setSys_updated_on(new Date().getTime());
//            ServiceGraph.createRss(obj);
//        }
//    }
//
//    public static void testSearchWithBigQuery()
//    {
//        StringBuffer stringBuffer = new StringBuffer();
//
//        for (int id = 0; id < 20000; id++)
//        {
//            stringBuffer.append(idToUserName(id));
//        }
//
//        String testQueryString = stringBuffer.toString();
//
//        WebserviceListener webServiceListener = new WebserviceListener();
//
//        webServiceListener.search("admin", "resolve", testQueryString, "");
//
//        String end = "";
//
//    }
//
//    public static void testNeo4j()
//    {
//        // START SNIPPET: startDb
//        graphDb = new EmbeddedGraphDatabase(DB_PATH);
//        // nodeIndex = graphDb.index().forNodes( "nodes" );
//        registerShutdownHook();
//        // END SNIPPET: startDb
//
//        // index();
//
//        // START SNIPPET: addUsers
//        Transaction tx = graphDb.beginTx();
//
//        try
//        {
//            // Create users sub reference node (see design guidelines on
//            // http://wiki.neo4j.org/ )
//            Node usersReferenceNode = graphDb.createNode();
//            graphDb.getReferenceNode().createRelationshipTo(usersReferenceNode, RelTypes.USERS_REFERENCE);
//
//            List<Node> allNode = new ArrayList();
//            long startTime = System.currentTimeMillis();
//
//            int id = 0;
//            Node userNode = null;
//
//            // Create some users and index their names with the IndexService
//            for (id = 0; id < 100000; id++)
//            {
//                userNode = createAndIndexUser(id, idToUserName(id));
//                // allNode.add(userNode);
//                usersReferenceNode.createRelationshipTo(userNode, RelTypes.USER);
//            }
//
//            System.out.println("==== the total time to create node of " + id + " is " + (System.currentTimeMillis() - startTime) + " ms");
//
//            // END SNIPPET: addUsers
//            System.out.println("Users created");
//
//            // startTime = System.currentTimeMillis();
//            // int idToFind = 5;
//            // Node foundUser = nodeIndex.get(USERNAME_KEY, idToFind).getSingle();
//            //
//            // System.out.println("==== the total time to find the specifical node is  " + (System.currentTimeMillis() - startTime) + " ms"
//            // + " the foundNode name is : " + foundUser.getProperty(USERNAME_CONTENT));
//
//            // WebserviceListener searcher = new WebserviceListener();
//            // searcher.search("admin", "resolve", "neo4j", "");
//
//            // IndexHits<Node> results = nodeIndex.query(arg0);
//
//            startTime = System.currentTimeMillis();
//
//            Iterable<Relationship> results = usersReferenceNode.getRelationships(RelTypes.USER, Direction.OUTGOING);
//
//            // usersReferenceNode.traverse(traversalOrder, stopEvaluator, returnableEvaluator, relationshipTypesAndDirections)
//
//            System.out.println("==== the total time to query the results  " + (System.currentTimeMillis() - startTime) + " ms");
//
//            startTime = System.currentTimeMillis();
//
//            int count = 0;
//
//            List<Long> ids = new ArrayList();
//
//            // sorting of the timestamp
//            Map<String, Node> sortedmaps = new TreeMap();// (String.CASE_INSENSITIVE_ORDER);
//            // List<Node> unsortedNodeList = new ArrayList();
//
//            for (Relationship relationship : results)
//            {
//
//                String typeName = relationship.getType().name();
//
//                Node user = relationship.getEndNode();
//
//                // Object userId = user.getProperty(USERNAME_KEY);
//                // Object content = (Object) user.getProperty(USERNAME_CONTENT);
//
//                sortedmaps.put((String) user.getProperty(USERNAME_CONTENT), user);
//                // unsortedNodeList.add(user);
//
//                // Object userObj = user.getProperty(USERNAME_KEY);
//                // String ok = "OK";
//
//                // nodeIndex.remove( user, USERNAME_KEY,
//                // user.getProperty(USERNAME_KEY));
//                // user.delete();
//                // relationship.delete();
//
//                count++;
//            }
//
//            System.out.println("===== the total count is : " + count + " used time of " + (System.currentTimeMillis() - startTime) + " ms");
//            startTime = System.currentTimeMillis();
//
//            // List<Map.Entry<Object, Object>> srotedNodeList = srotNodeMap(unSortedmaps);
//
//            // System.out.println("===== the total time for sorting the map is : " + (System.currentTimeMillis() - startTime) + " ms");
//
//            // startTime = System.currentTimeMillis();
//
//            for (Map.Entry<String, Node> nodeMapEntity : sortedmaps.entrySet())
//            {
//                String theKey = nodeMapEntity.getKey();
//
//                // System.out.println("===== the update time is : " + theKey);
//            }
//
//            System.out.println("===== the total time to extract the sorting results of " + sortedmaps.size() + " is : " + (System.currentTimeMillis() - startTime) + " ms");
//            //
//            // List<Node> sortedNode = new ArrayList();
//            // Node nodeLocal = null;
//            //
//            // int m = 0;
//            //
//            // for(Map.Entry<String, Long> mapLocal: maps.entrySet())
//            // {
//            // nodeLocal = graphDb.getNodeById(mapLocal.getValue());
//            // sortedNode.add(nodeLocal);
//            // m++;
//            // }
//            //
//            // System.out.println("===== the total count build sorted node is : " + m + " used time of " + (System.currentTimeMillis() - startTime) + " ms");
//
//            // usersReferenceNode.getSingleRelationship( RelTypes.USERS_REFERENCE,
//            // Direction.INCOMING ).delete();
//            // usersReferenceNode.delete();
//            tx.success();
//            System.out.println("Success");
//        }
//        catch (Throwable t)
//        {
//            t.printStackTrace();
//        }
//        finally
//        {
//            tx.finish();
//            System.out.println("finished");
//        }
//        System.out.println("Shutting down database ...");
//        shutdown();
//    }
//
//    public static List srotNodeMap(Map unsortMap)
//    {
//        List list = new LinkedList(unsortMap.entrySet());
//
//        // sort list based on comparator
//        // Collections.sort(list, new Comparator()
//        // {
//        // public int compare(Object o1, Object o2) {
//        // return ((Comparable) ((Map.Entry) (o1)).getValue())
//        // .compareTo(((Map.Entry) (o2)).getValue());
//        // }
//        // });
//
//        Collections.sort(list, new Comparator()
//        {
//            public int compare(Object o1, Object o2)
//            {
//                return ((Comparable) ((Map.Entry) (o1)).getKey()).compareTo(((Map.Entry) (o2)).getKey());
//            }
//        });
//
//        // Collections.sort(list, new Comparator<Node>()
//        // {
//        // public int compare(Node o1, Node o2) {
//        // return ((Comparable) ((o1)).getProperty(USERNAME_KEY))
//        // .compareTo(o2.getProperty(USERNAME_KEY));
//        // }
//        // });
//
//        // //put sorted list into map again
//        // Map sortedMap = new LinkedHashMap();
//        //
//        // for (Iterator it = list.iterator(); it.hasNext();)
//        // {
//        // Map.Entry entry = (Map.Entry)it.next();
//        // sortedMap.put(entry.getKey(), entry.getValue());
//        // }
//
//        System.out.println("==== the sorted list size is : " + list.size());
//
//        return list;
//
//    }
//
//    private static void shutdown()
//    {
//        graphDb.shutdown();
//    }
//
//    // START SNIPPET: helperMethods
//    private static String idToUserName(final int id)
//    {
//        // System.out.println(testString.length());
//        return "user" + id + "@neo4j.org "; // + testString.toString();
//    }
//
//    private static Node createAndIndexUser(int id, final String username)
//    {
//        Node node = graphDb.createNode();
//        // node.setProperty( USERNAME_KEY, usernameKey );
//        node.setProperty(NODE_TYPE, "User");
//        node.setProperty(USERNAME_KEY, id);
//        node.setProperty(USERNAME_CONTENT, username);
//
//        // nodeIndex.add(node, USERNAME_KEY, id);
//
//        List indexList = new ArrayList();
//        ResolveIndexLog indexLog = new ResolveIndexLog();
//        // indexLog.setsys_id(String.valueOf(node.getId()));
//        indexLog.setUComponentId(String.valueOf(node.getId()));
//        indexLog.setUComponentName(username);
//        indexLog.setUComponentTitle(null);
//        indexLog.setUComponentType("WikiDocument");
//        indexLog.setUComponentDescription("testtttttttaaaaaaa of neo4j");
//        indexLog.setUComponentUpdatedBy("Fen1111");
//        indexLog.setUComponentUpdatedOn(new Date(System.nanoTime()));
//        indexLog.setUComponentParentId("123");
//        indexLog.setUComponentParentName("testNeo4j");
//        indexLog.setUShouldDelete(false);
//        indexLog.setUShouldIndex(true);
//        indexLog.setUViewRoles("dev");
//        indexLog.setUContentToIndex(username);
//        indexList.add(indexLog);
//
//        ResolveIndexWriter.index(indexList, true);
//
//        return node;
//    }
//
//    // END SNIPPET: helperMethods
//
//    public static void index()
//    {
//
//        ServiceIndex.initAllIndex();
//
//        // com.resolve.persistence.model.Properties flag = PropertiesUtil.getPropertiesWithName("index.on.startup");
//        // boolean doIndex = (flag != null && flag.getUValue() != null && flag.getUValue().equalsIgnoreCase("yes")) ? true : false;
//        //
//        // if(doIndex)
//        // {
//        // //submit request to index
//        // Map<String, Object> params = new HashMap<String, Object>();
//        // params.put(SharedConstants.INDEX_ACTIONTASK, true);
//        // params.put(SharedConstants.INDEX_WIKIDOCUMENTS, true);
//        // params.put(SharedConstants.INDEX_ATTACHMENTS, true);
//        // params.put(SharedConstants.PURGE_ALL_INDEXES, false);
//        //
//        // //getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.indexAll", params);
//        // }
//    }// index
//
//    private static void registerShutdownHook()
//    {
//        // Registers a shutdown hook for the Neo4j and index service instances
//        // so that it shuts down nicely when the VM exits (even if you
//        // "Ctrl-C" the running example before it's completed)
//        Runtime.getRuntime().addShutdownHook(new Thread()
//        {
//            @Override
//            public void run()
//            {
//                shutdown();
//            }
//        });
//    }
}
