/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.services.graph.social.model.message.Notification;
import com.resolve.util.Log;


public class SocialNotificationFactory extends SocialFactory
{
    
    private static Node containerNodeLocal;
    
    public static void init()
    {
        init(SocialRootRelationshipTypes.NOTIFICATIONS, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.NOTIFICATIONS.name(), containerNode);
    }
    
    public static void create(Notification notification)
    {
        Transaction tx = null;
               
        try
        {
            getContainerNode();
            
//            if(find(notification.getSys_id()) == null)
//            {
//                tx = GraphDBUtil.getGraphDBService().beginTx();             
//                Node node = GraphDBUtil.getGraphDBService().createNode();
//                notification.convertObjectToNode(node); 
//                node.setProperty(NODE_TYPE, SocialRelationshipTypes.NOTIFICATION.name());
//                
//                containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.NOTIFICATION);
//                    
//                tx.success();
//            }
        }
        catch(Throwable t)
        {
            Log.log.warn("create Notification exception: " + t.getMessage(), t);
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
    }
    
    public static Node find(String id)
    {
        return ResolveGraphFactory.findNodeByIndexedID(id);
    }
    
    public Node findNode(String id)
    {
        return ResolveGraphFactory.findNodeByIndexedID(id);
    }
    
    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.NOTIFICATIONS.name());
        }
        
        return containerNodeLocal;
    }

}

