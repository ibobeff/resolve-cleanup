/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.util.SpecificNotificationUtil;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.NotificationTypeEnum;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialNotification
{
    public static List<ComponentNotification> getGlobalNotificationsForUser(User user) throws Exception
    {
        List<ComponentNotification> result = new ArrayList<ComponentNotification>();
        if (user != null)
        {
            Map<UserGlobalNotificationContainerType, Boolean> userCurrentSettings = getCurrentGlobalConfForUser(user);
            
            result.add(getGlobalConfForType(NodeType.PROCESS, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.TEAM, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.FORUM, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.USER, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.ACTIONTASK, userCurrentSettings));
            //        result.add(getGlobalConfForType(SocialObjectType.NAMESPACE, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.DOCUMENT, userCurrentSettings));
            result.add(getGlobalConfForType(NodeType.RSS, userCurrentSettings));
        }
        return result;
    }
    
    public static void updateGlobalNotificationsForUser(User user, Set<String> selectedNotificationTypes) throws Exception
    {
        if(user != null)
        {
            Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
            if(userNode != null)
            {
                Iterator<UserGlobalNotificationContainerType> it = UserGlobalNotificationContainerType.allOfGlobalNotification.iterator();
                while(it.hasNext())
                {
                    UserGlobalNotificationContainerType type = it.next();

                    //skip the EMAIL and DIGEST type notifications - they are handled separately
                    if(type.name().contains(ENUM_TO_SEARCH_TYPE.DIGEST_EMAIL.name())
                                    || type.name().contains(ENUM_TO_SEARCH_TYPE.FORWARD_EMAIL.name()))
                    {
                        continue;
                    }
                    
                    String typeName = type.name();
                    boolean value =  selectedNotificationTypes.contains(typeName) ? true : false;
                    
                    //update the value
                    updateGlobalNotificationTypeForUser(user, type, value);
                    
                }//end of while loop
            }
        }
    }
    
    /**
     * this api is used to update/set a specific Global type for a user. 
     * 
     * For eg. if the user wants to get email for all the Post for all the Processes it is following, than the values will be
     * type =  "PROCESS_FORWARD_EMAIL"  <== mapped to UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL
     * value = true
     * 
     * @param user
     * @param selectedType
     * @param value
     * @throws Exception
     */
    public static void updateGlobalNotificationTypeForUser(User user, UserGlobalNotificationContainerType type, boolean value) throws Exception
    {
        if(user != null)
        {
            Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
            if(userNode != null)
            {
                Node typeNode = ResolveGraphFactory.findNodeByIndexedID(type.name());
                
                if(typeNode != null)
                {
//                    boolean hasRel = userNode.hasRelationship(type);
//                    if(!hasRel)
//                    {
//                        //create the relationship if it does not exist
//                        ResolveGraphFactory.addRelation(userNode, typeNode, type);
//                    }
//                    
//                    //get the rel
//                    Relationship rel = ResolveGraphFactory.findRelationForType(userNode, typeNode, type);
//                    if(rel != null)
//                    {
//                        ResolveGraphFactory.updatePropertyOnRelationship(ResolveGraphFactory.NOTIFICATIONVALUE, Boolean.toString(value), rel);
//                    }
                }
            }
            
        }
    }
    
    public static void updateSpecificNotificationTypeFor(String streamId, User user, UserGlobalNotificationContainerType type, boolean value) throws Exception
    {
        if(StringUtils.isNotEmpty(streamId))
        {
            //for the Inbox and System stream, the setting is in the user component node
            if(streamId.equalsIgnoreCase("inbox") || streamId.equalsIgnoreCase("system") || streamId.equalsIgnoreCase("all"))
            {
                //get the user node using the lookup 
                RSComponent userComp = ServiceSocial.getComponentById(user.getSys_id(), user.getName());
                if(type == UserGlobalNotificationContainerType.USER_FORWARD_EMAIL)
                {
                    userComp.setInboxForwardEmail(value);
                }
                else if(type == UserGlobalNotificationContainerType.USER_DIGEST_EMAIL)
                {
                    userComp.setInboxDigestEmail(value);
                }
                else if(type == UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL)
                {
                    userComp.setSystemForwardEmail(value);
                }
                else if(type == UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL)
                {
                    userComp.setSystemDigestEmail(value);
                }
                else if(type == UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL)
                {
                    userComp.setAllForwardEmail(value);
                }
                else if(type == UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL)
                {
                    userComp.setAllDigestEmail(value);
                }
                
                //update the node
                //TODO: Update the User node with the properties
//                ServiceSocial.insertOrUpdateRSComponent(userComp);
                
            }
            else
            {
                new SpecificNotificationUtil(streamId, user).updateType(type, value);
            }
            
        }
    }
    
    public static ComponentNotification getSpecificNotificationFor(String streamId, User user) throws Exception
    {
        ComponentNotification result = null;
        
        if(StringUtils.isNotEmpty(streamId))
        {
            //for the Inbox and System stream, the setting is in the user component node
            if(streamId.equalsIgnoreCase("inbox") || streamId.equalsIgnoreCase("system") || streamId.equalsIgnoreCase("all"))
            {
                //get the user node using the lookup 
                RSComponent userComp = ServiceSocial.getComponentById(user.getSys_id(), user.getName());
                List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>();
                
                EnumSet<NotificationTypeEnum> enumSetNotificationType = SocialNotification.findNotificationTypesForType(NodeType.USER, false); 
                for (NotificationTypeEnum typeEnum : enumSetNotificationType)
                {
                    NotificationConfig config = new NotificationConfig();
                    config.setType(typeEnum.getType());
                    config.setDisplayName(typeEnum.getDisplayName());
                    if(typeEnum.getType() == UserGlobalNotificationContainerType.USER_FORWARD_EMAIL)
                    {
                        config.setValue(userComp.isInboxForwardEmail()+"");
                    }
                    else if(typeEnum.getType() == UserGlobalNotificationContainerType.USER_DIGEST_EMAIL)
                    {
                        config.setValue(userComp.isInboxDigestEmail()+"");
                    }
                    else if(typeEnum.getType() == UserGlobalNotificationContainerType.USER_SYSTEM_FORWARD_EMAIL)
                    {
                        config.setValue(userComp.isSystemForwardEmail()+"");
                    }
                    else if(typeEnum.getType() == UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL)
                    {
                        config.setValue(userComp.isSystemDigestEmail()+"");
                    }
                    else if(typeEnum.getType() == UserGlobalNotificationContainerType.USER_ALL_FORWARD_EMAIL)
                    {
                        config.setValue(userComp.isAllForwardEmail()+"");
                    }
                    else if(typeEnum.getType() == UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL)
                    {
                        config.setValue(userComp.isAllDigestEmail()+"");
                    }
                    
                    //add to the list
                    notifyTypes.add(config);
                }
                
                //prepare the result
                result = new ComponentNotification();
                result.setCompType(NodeType.USER);
                result.setNotifyTypes(notifyTypes);
                result.setComponentDisplayName(userComp.getDisplayName());
            }
            else
            {
                result = new SpecificNotificationUtil(streamId, user).getSpecificNotifications();
            }
            
        }
        
        return result;
        
    }
    
    
    public static EnumSet<NotificationTypeEnum> findNotificationTypesForType(NodeType compType, boolean forNotificationType)
    {
        EnumSet<NotificationTypeEnum> enumSet = null;
        
        switch (compType)
        {
            case PROCESS:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetProcess;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetProcessEmail;
                }
                break;

            case TEAM:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetTeam;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetTeamEmail;
                }
                break;

            case FORUM:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetForum;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetForumEmail;
                }
                break;

            case USER:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetUser;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetUserEmail;
                }
                break;

            case WORKSHEET:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetWorksheet;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetWorksheetEmail;
                }
                break;

            case NAMESPACE:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetNamespace;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetNamespaceEmail;
                }
                break;

            case RUNBOOK:
//                if (forNotificationType)
//                {
//                    enumSet = NotificationTypeEnum.enumSetRunbook;
//                }
//                else
//                {
//                    enumSet = NotificationTypeEnum.enumSetRunbookEmail;
//                }
                break;

            case DOCUMENT:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetDocument;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetDocumentEmail;
                }
                break;

            case DECISIONTREE:
                break;

            case RSS:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetRss;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetRssEmail;
                }
                break;

            case ACTIONTASK:
                if (forNotificationType)
                {
                    enumSet = NotificationTypeEnum.enumSetActionTask;
                }
                else
                {
                    enumSet = NotificationTypeEnum.enumSetActionTaskEmail;
                }
                break;
            default:
                break;

        }
        
        return enumSet;
    }
    
    
    public static List<RSComponent> findUsersRegisteredToReceiveThisNotificationForComp(String compSysId, UserGlobalNotificationContainerType notificationType) throws Exception
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        //get the list of user Ids that have global relationship of this notification type set as 'True'
        Set<String> userWithGlobalNotification = userWithGlobalNotification(notificationType);
        
        //if this is a CREATE notfication, than any user who have registered for that will receive the notification
        if(notificationType.name().contains("CREATE") || notificationType.name().contains("PURGE"))
        {
            if(userWithGlobalNotification != null && userWithGlobalNotification.size() > 0)
            {
                for(String userSysId : userWithGlobalNotification)
                {
//                    Node userNode = ResolveGraphFactory.findNodeByIndexedID(userSysId);
//                    if(userNode != null)
//                    {
//                        User user = new User();
//                        user.convertNodeToObject(userNode);
//                        
//                        result.add(user);
//                    }
                }
            }            
        }
        else
        {
            //get the list of users that are following this component
            List<User> usersFollowingThisComp = ServiceSocial.getAllUsersFollowingThisStream(compSysId, "system");
            if(usersFollowingThisComp != null)
            {
                for(User user : usersFollowingThisComp)
                {
                    if(userWithGlobalNotification.contains(user.getSys_id()))
                    {
                        result.add(user);
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * prepares list of users who will receive email for the Post or notification for this specific component which can be Process, Team, Document, etc 
     * This is specific to get the settings of *FORWARD_EMAIL* for all the components 
     * 
     * @param component
     * @param username
     * @return
     */
    public static Set<User> getUsersForForwardEmail(RSComponent component, String username)
    {
        return getUsersForDigestEmailOrForwardEmail(component, ENUM_TO_SEARCH_TYPE.FORWARD_EMAIL, username);
    }
    
    public static Set<User> getUsersForDigestEmail(RSComponent component, String username)
    {
        return getUsersForDigestEmailOrForwardEmail(component, ENUM_TO_SEARCH_TYPE.DIGEST_EMAIL, username);
    }
    
    //private apis
    private static Set<User> getUsersForDigestEmailOrForwardEmail(RSComponent component, ENUM_TO_SEARCH_TYPE searchString, String username)
    {
        //get the notification type for this comp
        NodeType compType = NodeType.USER;//valueOf(component.getType());
        UserGlobalNotificationContainerType emailType = getEmailTypeForComp(compType, searchString);
        
        return getUsersForNotificationTypeOfStream(component.getSys_id(), emailType);
    }
    
    
    private static Set<User> getUsersForNotificationTypeOfStream(String streamSysId, UserGlobalNotificationContainerType notificationType)
    {
        Set<User> result = new HashSet<User>();
        Set<String> userSysidsToIgnore = new HashSet<String>();
        
      //get all the users who are following this comp
        Node compNode = ResolveGraphFactory.findNodeByIndexedID(streamSysId);
        if(compNode != null && notificationType != null)
        {
            //this is for SPECIFIC settings
//            Iterable<Relationship> rels = compNode.getRelationships(notificationType);
//            if(rels != null)
//            {
//                for(Relationship rel: rels)
//                {
//                    Node userNode = rel.getOtherNode(compNode);
//                    if(userNode != null && userNode.hasProperty(RSComponent.TYPE))
//                    {
//                        String type = (String) userNode.getProperty(RSComponent.TYPE);
//                        if(NodeType.USER.name().equalsIgnoreCase(type))
//                        {
//                            boolean notification = true;
//                            String sysId = (String) userNode.getProperty(RSComponent.SYS_ID);
//                            
//                            if(rel.hasProperty(ResolveGraphFactory.NOTIFICATIONVALUE))
//                            {
//                                String value = (String) rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
//                                notification = value.equalsIgnoreCase("true") ? true : false;
//                            }
//                            
//                            //if the flag is true, than add it to the list
//                            //if the property is not there but the relationship is there, than also act as true
//                            if(notification)
//                            {
//                                User u = new User();
//                                u.convertNodeToObject(userNode);
//                                
//                                result.add(u);
//                            }
//                            else
//                            {
//                                //here the user has explicitly mentioned that he does not want a notification for this type
//                                userSysidsToIgnore.add(sysId);
//                            }
//                        }
//                    }
//                }//end of for
//            }//end of if
            
            //this is for GLOBAL settings
            Node notificationTypeGlobalNode = ResolveGraphFactory.findNodeByIndexedID(notificationType.name());
            if(notificationTypeGlobalNode != null)
            {
//                Iterable<Relationship> globalRels = compNode.getRelationships(notificationType);
//                if(globalRels != null)
//                {
//                    for(Relationship rel: globalRels)
//                    {
//                        Node userNode = rel.getOtherNode(compNode);
//                        if(userNode != null && userNode.hasProperty(RSComponent.TYPE))
//                        {
//                            String type = (String) userNode.getProperty(RSComponent.TYPE);
//                            if(NodeType.USER.name().equalsIgnoreCase(type))
//                            {
//                                boolean notification = true;
//                                String sysId = (String) userNode.getProperty(RSComponent.SYS_ID);
//                                
//                                if(rel.hasProperty(ResolveGraphFactory.NOTIFICATIONVALUE))
//                                {
//                                    String value = (String) rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
//                                    notification = value.equalsIgnoreCase("true") ? true : false;
//                                }
//                                
//                                //if the flag is true, than add it to the list
//                                //ignore the users that have specifically conf NOT to get any notifications for this stream 
//                                if(notification && !userSysidsToIgnore.contains(sysId))
//                                {
//                                    User u = new User();
//                                    u.convertNodeToObject(userNode);
//                                    
//                                    result.add(u);
//                                }
//                            }
//                        }
//                    }//end of for
//                }//end of if
            }//end of if
        }//end of if
        
        return result;
    }
    

    private static UserGlobalNotificationContainerType getEmailTypeForComp(NodeType compType, ENUM_TO_SEARCH_TYPE emailOrDigestType)
    {
        UserGlobalNotificationContainerType result = null;
        
        EnumSet<NotificationTypeEnum> enumSet = findNotificationTypesForType(compType, false);
        if (enumSet != null)
        {
            for (NotificationTypeEnum typeEnum : enumSet)
            {
                if(typeEnum.name().contains(emailOrDigestType.name()))
                {
                    result = typeEnum.getType();
                    break;
                }
            }//end of for
        }
        
        return result;
    }
    
    private static Map<UserGlobalNotificationContainerType, Boolean> getCurrentGlobalConfForUser(User user)
    {
        Map<UserGlobalNotificationContainerType, Boolean> result = new HashMap<UserGlobalNotificationContainerType, Boolean>();
        
        Set<String> globalNotificationTypes = new HashSet<String>();
        Iterator<UserGlobalNotificationContainerType> it = UserGlobalNotificationContainerType.allOfGlobalNotification.iterator();
        while(it.hasNext())
        {
            UserGlobalNotificationContainerType t = it.next();
            globalNotificationTypes.add(t.name());
        }
        
        
        
        //traverse the graph to get the relationships and value that is set
        Node userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
        if(userNode != null)
        {
            Iterable<Relationship> rels = userNode.getRelationships();
            for(Relationship rel : rels)
            {
                boolean value = false;
                RelationshipType relType = rel.getType();
//                String relName = relType.name();
//                Log.log.debug("******    relname :" + relName);
                //make sure that this rel is of the notification type
                if(globalNotificationTypes.contains(relType.name()))
                {
                    //make sure that the other node is of the Global node type and not any Rscomponent
                    Node otherNode = rel.getOtherNode(userNode);
                    String type = (String) otherNode.getProperty(SocialFactory.NODE_TYPE);
//                    Log.log.debug("******    other node type:" + type);
                    if(type.equalsIgnoreCase(relType.name()))
                    {
                        //this is global rel
                        String valueStr = rel.hasProperty(ResolveGraphFactory.NOTIFICATIONVALUE) ? (String) rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE) : "false";
//                        Log.log.debug("******    relname :" + relName + " - value:" + valueStr);
                        if(StringUtils.isNotBlank(valueStr) && valueStr.equalsIgnoreCase("true"))
                        {
                            value = true;
                        }
                    }
                    
                    result.put(UserGlobalNotificationContainerType.valueOf(relType.name()), value);
                }
                
            }
        }                            
        
        
        return result;
    }
    
    private static ComponentNotification getGlobalConfForType(NodeType compType, Map<UserGlobalNotificationContainerType, Boolean> userCurrentSettings)
    {
        ComponentNotification compNotify = new ComponentNotification();
        compNotify.setCompType(compType);
        compNotify.setNotifyTypes(getCompNotificationType(compType, userCurrentSettings, true));
        
        switch(compType)
        {
            case PROCESS:
                compNotify.setComponentDisplayName("Process");
                break;

            case TEAM:
                compNotify.setComponentDisplayName("Team");
                break;

            case FORUM:
                compNotify.setComponentDisplayName("Forum");
                break;


            case USER:
                compNotify.setComponentDisplayName("User");
                break;


            case WORKSHEET:
                compNotify.setComponentDisplayName("Worksheet");
                break;

            case NAMESPACE:
                compNotify.setComponentDisplayName("Namespace");
                break;

            case RUNBOOK:
                compNotify.setComponentDisplayName("Runbook");
                break;


            case DOCUMENT:
                compNotify.setComponentDisplayName("Document");
                break;

            case DECISIONTREE:
                compNotify.setComponentDisplayName("Decision Tree");
                break;

            case RSS:
                compNotify.setComponentDisplayName("Rss");
                break;

            case ACTIONTASK:
                compNotify.setComponentDisplayName("Actiontask");
                break;
                
            default:
                break;
                
        }
        
        return compNotify;
    }

    private static List<NotificationConfig> getCompNotificationType(NodeType compType, Map<UserGlobalNotificationContainerType, Boolean> userCurrentSettings, boolean forNotificationType)
    {
        List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>();

        try
        {
            EnumSet<NotificationTypeEnum> enumSet = findNotificationTypesForType(compType, forNotificationType);
            if (enumSet != null)
            {
                NotificationConfig config = null;

                for (NotificationTypeEnum typeEnum : enumSet)
                {
                    UserGlobalNotificationContainerType typeOfNotification = typeEnum.getType();
                    boolean value = userCurrentSettings.containsKey(typeOfNotification) ? userCurrentSettings.get(typeOfNotification) : false;
                    
                    config = new NotificationConfig();
                    config.setType(typeOfNotification);
                    config.setDisplayName(typeEnum.getDisplayName());
                    config.setValue(value+"");
                    notifyTypes.add(config);
                }
            }

        }
        catch (Throwable t)
        {
            Log.log.info("getCompNotificationType catch exception: " + t, t);
        }

        return notifyTypes;
    }
    
    private static Set<String> userWithGlobalNotification(UserGlobalNotificationContainerType notificationType)
    {
        Set<String> userIds = new HashSet<String>();
        
        Node notificationNode = ResolveGraphFactory.findNodeByIndexedID(notificationType.name());
        if(notificationNode != null)
        {
//            Iterable<Relationship> rels = notificationNode.getRelationships(notificationType);
//            if(rels != null)
//            {
//                for(Relationship rel : rels)
//                {
//                    Node userNode = rel.getOtherNode(notificationNode);
//                    if(userNode.hasProperty(RSComponent.TYPE))
//                    {
//                        String type = (String) userNode.getProperty(RSComponent.TYPE);
//                        if(type.equalsIgnoreCase(NodeType.USER.name()))
//                        {
//                            if(rel.hasProperty(ResolveGraphFactory.NOTIFICATIONVALUE))
//                            {
//                                String value = (String) rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
//                                if(value.equalsIgnoreCase("true"))
//                                {
//                                    userIds.add((String) userNode.getProperty(RSComponent.SYS_ID));
//                                }
//                            }
//                        }
//                    }//end of if
//                }//end of for
//            }
        }
        
        return userIds;
    }
//    
//    private static Map<String, Boolean> userSpecificNotificationForComp(String compSysId, UserGlobalNotificationContainerType notificationType)
//    {
//        Map<String, Boolean> result = new HashMap<String, Boolean>();
//        
//        if(StringUtils.isNotEmpty(compSysId))
//        {
//            Node compNode = ResolveGraphFactory.findNodeByIndexedID(compSysId);
//            if(compNode != null)
//            {
//                Iterable<Relationship> rels = compNode.getRelationships(notificationType);
//                if(rels != null)
//                {
//                    for(Relationship rel : rels)
//                    {
//                        Node userNode = rel.getOtherNode(compNode);
//                        if(userNode.hasProperty(RSComponent.TYPE))
//                        {
//                            String type = (String) userNode.getProperty(RSComponent.TYPE);
//                            if(type.equalsIgnoreCase(SocialObjectType.USER.name()))
//                            {
//                                if(rel.hasProperty(ResolveGraphFactory.NOTIFICATIONVALUE))
//                                {
//                                    String userId = (String) userNode.getProperty(RSComponent.SYS_ID);
//                                    String value = (String) rel.getProperty(ResolveGraphFactory.NOTIFICATIONVALUE);
//                                    
//                                    result.put(userId, value.equalsIgnoreCase("true") ? true : false);
//                                }
//                            }
//                        }//end of if
//                    }//end of for
//                }
//            }
//        }
//        
//        return result;
//    }

}

//just string used to search the type of the digest or email
enum ENUM_TO_SEARCH_TYPE {
    DIGEST_EMAIL,
    FORWARD_EMAIL;    
}
