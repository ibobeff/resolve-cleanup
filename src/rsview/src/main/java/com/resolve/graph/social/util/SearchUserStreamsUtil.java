/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.factory.SocialFactory;
import com.resolve.graph.social.util.legacy.PostUtil;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SearchUserStreamsUtil
{
    private User user = null;
    private Node userNode = null;

    private Set<RSComponent> streams = new HashSet<RSComponent>();
    private Set<String> streamIds = new HashSet<String>();

    //switches
    private boolean recurse = true;//be default, recurse to get the comps that are followed

    public SearchUserStreamsUtil(User user)
    {
        if(user == null)
        {
            throw new RuntimeException("User is mandatory for searching the streams");
        }

        this.user = user;
    }

    public boolean isRecurse()
    {
        return recurse;
    }

    public void setRecurse(boolean recurse)
    {
        this.recurse = recurse;
    }

    /**
     * This method simply provides a Set of {@link RSComponent} that this user attached to.
     *
     * @return
     * @throws Exception
     * @throws
     */
    public Set<RSComponent> getUserStreams() throws Exception
    {
//        userNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//        if(userNode != null)
//        {
//            //add this current user
//            streams.addAll(evaluateNodeAndConvertToRSComponent(userNode, false));
//
//            //get the other comp that this user follows or member of
//            Iterable<Relationship> rels = userNode.getRelationships(Direction.OUTGOING, RelationType.FOLLOWER, RelationType.MEMBER);
//            if(rels != null)
//            {
//                for(Relationship rel: rels)
//                {
//                    Node componentNode = rel.getEndNode();
//                    if(componentNode != null)
//                    {
//                        streams.addAll(evaluateNodeAndConvertToRSComponent(componentNode, true));
//                    }
//                }//end of for
//            }//end of if
//        }//end of if
//
//        //convert nodes to comps
////        convertNodeToRsComponent();
//
//        for(RSComponent comp : this.streams)
//        {
//            comp.setUnReadCount(PostUtil.countUnreadPostByStreamId(comp.getSys_id(), comp.getType(), user));
//            comp.setComponentNotification(ServiceSocial.getSpecificNotificationFor(comp.getSys_id(), user));
//        }

        return streams;
    }

//    private Set<RSComponent> evaluateNodeAndConvertToRSComponent(Node node, boolean directFollow)
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//
//        String type = (String) node.getProperty(SocialFactory.NODE_TYPE);
//        String sysId = (String) node.getProperty(RSComponent.SYS_ID);
//        String name = sysId;
//        if(node.hasProperty(RSComponent.DISPLAYNAME))
//        {
//            name = (String) node.getProperty(RSComponent.DISPLAYNAME);
//        }
//        Log.log.trace("Type for this node :" + type + " - " + name);
//
//        if(StringUtils.isEmpty(type))
//        {
//            //ignore the nodes that DO NOT HAVE a type
//            Log.log.error("Type - which is a mandatory field - is missing for node " + (String) node.getProperty(ResolveGraphNode.DISPLAYNAME, ""));
//        }
//        else
//        {
//            //if the recurse flag is true that be recursive for all the containers
//            if (isRecurse())
//            {
//                if (type.equalsIgnoreCase(SocialRelationshipTypes.FORUM.name()))
//                {
//                    //a FORUM can be a part of PROCESS - so the user can have indirect reference
//                    //get the list of PROCESS that this belongs to Forum
//                    Iterable<Node> processNodes = TraversalUtil.findProcessThatThisForumBelongsTo(node);
//                    for (Node processNode : processNodes)
//                    {
//                        result.addAll(convertNodeToRsComponent(processNode, directFollow));
//                        //                    //**RECURSIVE - GOES INTO INFINITE LOOP
//                        //                    evaluateNodeAndAddToList(processNode);
//                    }
//                }
//                else if (type.equals(SocialRelationshipTypes.TEAM.name()))
//                {
//                    //get the list of users that this team has - WE ARE NOT SHOWING THE USERS IF THEY ARE RELATED FROM PROCESS --> TEAM --> USER
//                    //                Iterable<Node> userNodes = TraversalUtil.findUsersBelongingToTeam(node);
//                    //                for(Node userNode : userNodes)
//                    //                {
//                    //                    result.addAll(convertNodeToRsComponent(userNode));
//                    //                }
//
//                    //get the list of Process nodes that this team belongs to and add it
//                    Iterable<Node> processNodes = TraversalUtil.findProcessThatThisTeamBelongsTo(node);
//                    for (Node processNode : processNodes)
//                    {
//                        result.addAll(convertNodeToRsComponent(processNode, directFollow));
//                        //                    //**RECURSIVE - GOES INTO INFINITE LOOP
//                        //                    evaluateNodeAndAddToList(processNode);
//                    }
//
//                    //get the list of team nodes - this should be recursive
//                    Iterable<Node> memberOfTeamNodes = TraversalUtil.findTeamsThatThisTeamIsMemberOf(node);
//                    if (memberOfTeamNodes != null)
//                    {
//                        for (Node teamNode : memberOfTeamNodes)
//                        {
//                            //**RECURSIVE
//                            Set<RSComponent> teams = evaluateNodeAndConvertToRSComponent(teamNode, false);
//                            result.addAll(teams);
//                        }
//                    }
//                }
//                else if (type.equals(SocialRelationshipTypes.PROCESS.name()))
//                {
//                    Iterable<Node> membersOfProcessNode = TraversalUtil.findMembersBelongingToProcess(node);
//                    if (membersOfProcessNode != null)
//                    {
//                        //this can be DOC, AT, USER, etc
//                        for (Node anyNode : membersOfProcessNode)
//                        {
//                            String currNodeType = (String) anyNode.getProperty(SocialFactory.NODE_TYPE);
//
//                            //do not include the USERS belonging to a Process
//                            if (currNodeType.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
//                            {
//                                continue;
//                            }
//
//                            //**RECURSIVE
//                            Set<RSComponent> anyNodes = evaluateNodeAndConvertToRSComponent(anyNode, false);
//                            result.addAll(anyNodes);
//                        }
//                    }
//
//                }
//            }
//            
//            //add the main one
//            result.addAll(convertNodeToRsComponent(node, directFollow));
//        }
//
//        return result;
//    }
//
//
//    private Set<RSComponent> convertNodeToRsComponent(Node node, boolean directFollow)
//    {
//        Set<RSComponent> result = new HashSet<RSComponent>();
//
//        RSComponent comp = new RSComponent();
//        comp.convertNodeToObject(node);
//        comp.setIsFollowing(true);
//
//        String type = comp.getType();
//        if (StringUtils.isEmpty(type))
//        {
//            //ignore the nodes that DO NOT HAVE a type
//            Log.log.error("Type - which is a mandatory field - is missing for node " + comp.getDisplayName());
//        }
//        else
//        {
//            //add only if its not marked deleted
//            if (!comp.isMarkDeleted())
//            {
//                //set the flag
//                comp.setIsDirectFollowFlag(directFollow);
//
//                //                    Log.log.debug("STARTING THE COUNT FOR:" + comp.getDisplayName());
//                //get the count for this component
////                int unreadCount = ServiceSocial.findUnReadCountForComp(comp.getSys_id(), user);//getUnReadCountForComp(node);
////                comp.setUnReadCount(unreadCount);
//
//                //check if the comp is pinned
//                boolean addedToHome = ResolveGraphFactory.findRelation(userNode, node, RelationType.FAVORITE);
//                comp.setPinned(addedToHome);
//
//                //for the current user object, set the flag to true
//                if (comp.getSys_id().equalsIgnoreCase(user.getSys_id()))
//                {
//                    comp.seIsCurrentUser(true);
//                }
//
//                //add it to the list
//                result.add(comp);
//                this.streamIds.add(comp.getSys_id());
//
//
//                //if its a DOCUMENT type
//                if (type.equalsIgnoreCase(SocialRelationshipTypes.DOCUMENT.name()))
//                {
//                    //if the document is runbook
//                    if (comp.isRunbook())
//                    {
//
//                        try
//                        {
//                            RSComponent runbook = new RSComponent();
//                            BeanUtils.copyProperties(runbook, comp);
//                            runbook.setType(SocialRelationshipTypes.RUNBOOK.name());
//                            runbook.setId(runbook.getSys_id() + "-" + SocialRelationshipTypes.RUNBOOK.name());
//
//                            result.add(runbook);
//                        }
//                        catch (Exception e)
//                        {
//                            //do nothing
//                        }
//                    }
//
//                    //if the doc is DT
//                    if (comp.isDecisionTree())
//                    {
//                        try
//                        {
//                            RSComponent dt = new RSComponent();
//                            BeanUtils.copyProperties(dt, comp);
//                            dt.setType(SocialRelationshipTypes.DECISIONTREE.name());
//                            dt.setId(dt.getSys_id() + "-" + SocialRelationshipTypes.DECISIONTREE.name());
//
//                            result.add(dt);
//
//                        }
//                        catch (Exception e)
//                        {
//                            //do nothing
//                        }
//
//                    }
//                }
//            }//end of if
//        }//end of else
//
//        return result;
//
//    }

}
