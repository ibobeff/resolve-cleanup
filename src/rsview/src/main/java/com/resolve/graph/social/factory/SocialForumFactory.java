/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.util.Log;


public class SocialForumFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.FORUMS, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.FORUMS.name(), containerNode);
    }

    public static Forum insertOrUpdateForum(Forum forum, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(forum.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                forum = insert(forum);
//            }
//            else
//            {
//                //update
//                forum = (Forum) updateComponent(forum, docNode);
//            }
        }
        catch(Exception t)
        {
            Log.log.warn("create Forum catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
         return forum;
    }


    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.FORUMS.name());
        }

        return containerNodeLocal;
    }

    public static Set<RSComponent> findAllForums()
    {
        Node containerNode = getContainerNode();
        return findAllRsComponentFor(containerNode, SocialRelationshipTypes.FORUM);
    }
    
    public static void syncGraphToDB(Set<String> dbSysIds)
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.FORUMS.name());
        }
        
        if(dbSysIds == null)
        {
            //to avoid the null pointer
            dbSysIds = new HashSet<String>();
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.FORUM);
            deleteNodes(dbSysIds, rels);
        }
    }

    private static Forum insert(Forum forum) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

            //insert
//            tx = GraphDBUtil.getGraphDBService().beginTx();

//            Node node = GraphDBUtil.getGraphDBService().createNode();
//            forum.convertObjectToNode(node);
//            //                node.setProperty(NODE_TYPE, SocialObjectType.DOCUMENT.name());
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.FORUM);
//
//            tx.success();
//
//            //update the object
//            forum.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create Forum catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return forum;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }
}
