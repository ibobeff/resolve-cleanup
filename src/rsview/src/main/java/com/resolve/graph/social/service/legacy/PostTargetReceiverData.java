/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.service.legacy;

import java.util.HashSet;
import java.util.Set;

public class PostTargetReceiverData
{
    public Set<String> targets = new HashSet<String>();
    public Set<String> receiver = new HashSet<String>();
    public Set<String> starredIDsSet = new HashSet<String>();
    public Set<String> outboxIDsSet = new HashSet<String>();
    public Set<String> namespaceSet = new HashSet<String>();
}
