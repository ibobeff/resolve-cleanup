/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.util.legacy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.services.graph.social.model.RSComponent;

public class PostUtil
{
    public static final String POST_SYS_ID = "postSysId";
    public static final String INTERNAL_POST_MEMBER_ROLE = "postmrole";
    public static final String INTERNAL_POST_RSS_ROLE = "postrssrole";

    private String postid = null;
    private PostProcessEngine postEngine = null;
    private List<RSComponent> mentions = null;

    //TODO DEMO code start
    //Following is for a demo only, should be removed later
    private static final Map<String, String[]> demoDataMap = new HashMap<String, String[]>();
}