/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.util.Log;


public class SocialUsersFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.USERS, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.USERS.name(), containerNode);
    }

    public static User insertOrUpdateProcess(User user, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(user.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                user = insert(user);
//            }
//            else
//            {
//                //update
//                user = (User) updateComponent(user, docNode);
//            }
        }
        catch(Exception t)
        {
            Log.log.warn("create User catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
         return user;
    }

    @SuppressWarnings("unused")
    public int getContainerUserCount()
    {
        int count = 0;

        try
        {
            getContainerNode();

            Iterable<Relationship> rels = containerNodeLocal.getRelationships(SocialRelationshipTypes.USER);

            if(rels != null)
            {
                for(Relationship rel: rels)
                {
                    count++;
                }
            }
        }
        catch(Throwable t)
        {
            Log.log.warn(t.getMessage(), t);
        }

        return count;
    }

    public static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.USERS.name());
        }

        return containerNodeLocal;
    }

    public static Collection<User> getAllUsers()
    {
        Collection<User> users = new ArrayList<User>();

        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.USERS.name());
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.USER);

            if(rels != null)
            {
                for(Relationship rel: rels)
                {
//                    Node endNode = rel.getEndNode();
//                    User userLocal = new User();
//                    userLocal.convertNodeToObject(endNode);
//                    users.add(userLocal);
                }
            }
        }

        return users;
    }
    
    public static void syncGraphToDB(Set<String> dbSysIds)
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.USERS.name());
        }
        
        if(dbSysIds == null)
        {
            //to avoid the null pointer
            dbSysIds = new HashSet<String>();
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.USER);
            deleteNodes(dbSysIds, rels);
        }
    }

    private static User insert(User user) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

//            //insert
//            tx = GraphDBUtil.getGraphDBService().beginTx();
//
//            Node node = GraphDBUtil.getGraphDBService().createNode();
//            user.convertObjectToNode(node);
//            //                node.setProperty(NODE_TYPE, SocialObjectType.DOCUMENT.name());
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.USER);
//
//            tx.success();
//
//            //update the object
//            user.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create User catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return user;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }
}
