/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.impex.export;

import java.util.List;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.util.TraversalUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.GraphRelationshipDTO;

public class ExportDocumentGraph  extends ExportComponentGraph
{
    public ExportDocumentGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
       super(sysId, options, SocialRelationshipTypes.DOCUMENT);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode, SocialImpexVO socialImpex) throws Exception
    {
        if (options.wikiTags)
        {
            //for documents, export the tags 
            exportTagsRelationship(compNode, socialImpex);
        }
        
        if (options.wikiCatalogs)
        {
            //and catalog node relationship
            exportCatalogRelationship(compNode, socialImpex);
        }
        
        return relationships;
    }
    
    private void exportCatalogRelationship(Node compNode, SocialImpexVO socialImpex) throws Exception
    {
        Iterable<Relationship> rels = compNode.getRelationships(SocialRelationshipTypes.DOCUMENT_TO_CATALOG_REF);
        if(rels != null)
        {
//            String sourceName = (String) compNode.getProperty(RSComponent.DISPLAYNAME);
//            String sourceType = (String) compNode.getProperty(RSComponent.TYPE);
            
            for(Relationship rel : rels)
            {
                Node catalogItemNode = rel.getOtherNode(compNode);
//                if(catalogItemNode.hasProperty(ResolveCatalog.PATH))
//                {
//                    ResolveCatalog catalog = new ResolveCatalog();
//                    catalog.convertNodeToObject(catalogItemNode); 
//                    
//                    String targetName = (String) catalogItemNode.getProperty(ResolveCatalog.PATH);
//                    String targetType = (String) catalogItemNode.getProperty(ResolveCatalog.TYPE);
//                    
//                    GraphRelationshipDTO relation = new GraphRelationshipDTO();
//                    relation.setSourceName(sourceName);
//                    relation.setSourceType(SocialRelationshipTypes.valueOf(sourceType));
//                    relation.setTargetName(targetName);
//                    relation.setTargetType(SocialRelationshipTypes.valueOf(targetType));
//                    
//                    relation.setTargetObject(catalog);
//                    
//                    relationships.add(relation);
//                }
            }
        }
    }
    
    private void exportTagsRelationship(Node compNode, SocialImpexVO socialImpex) throws Exception
    {
        //get all the nodes that this process node is refering too
        Iterable<Node> tagsForDocument = TraversalUtil.findTagsFor(compNode);
        if(tagsForDocument != null)
        {
            for(Node anyNode : tagsForDocument)
            {
                addRelationship(compNode, anyNode);
            }//end of for
        }
        
    }
    
}
