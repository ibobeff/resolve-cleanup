/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.constants;

import org.neo4j.graphdb.RelationshipType;

/**
 * 
 * This class define all relationtype between nodes 
 * in social graph database.
 *
 */
public enum RelationType implements RelationshipType
{
    SENT, TARGET, RECEIVER, MENTIONS, MEMBER, NAMESPACE, FOLLOWER, //INDIRECT_FOLLOW, 
    SUBSCRIBE, STARRED, LIKE, READ, SOLVED, NOTIFICATION, FAVORITE, COMMENT 
}
