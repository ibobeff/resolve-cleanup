/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.factory;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.IndexHits;

import com.resolve.graph.ResolveGraphFactory;
import com.resolve.graph.social.constants.RelationType;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.graph.social.constants.SocialRootRelationshipTypes;
import com.resolve.graph.social.service.legacy.GraphDBUtil;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialNameSpaceFactory extends SocialFactory
{
    private static Node containerNodeLocal;

    public static void init()
    {
        init(SocialRootRelationshipTypes.NAMESPACE, Direction.OUTGOING);
        allContainerNodes.put(SocialRootRelationshipTypes.NAMESPACE.name(), containerNode);
    }

    public static Namespace insertOrUpdateNamespace(Namespace namespace, String username) throws Exception
    {
        Transaction tx = null;

        try
        {
            getContainerNode();

//            Node docNode = ResolveGraphFactory.findNodeByIndexedID(namespace.getSys_id());
//
//            if(docNode == null)
//            {
//                //insert
//                namespace = insert(namespace);
//            }
//            else
//            {
//                //update
//                namespace = (Namespace) updateComponent(namespace, docNode);
//            }
        }
        catch(Exception t)
        {
            Log.log.warn("create NameSpace catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if(tx != null)
            {
                tx.finish();
            }
        }
         return namespace;
    }


    public static Node findNameSpaceNodeByName(String namespaceName)
    {
        Node node = null;

        if(!StringUtils.isEmpty(namespaceName))
        {
            node = findNodeByIndexedNamespaceName(namespaceName);
        }

        return node;
    }

    public static Node findNameSpaceNodeFor(Node childNode)
    {
        Node result = null;

        if(childNode != null)
        {
            Iterable<Relationship> rels = childNode.getRelationships(RelationType.NAMESPACE, Direction.OUTGOING);

            if(rels != null)
            {
                for(Relationship rel: rels)
                {
                    Node endNode = rel.getEndNode();

                    if(endNode != null && StringUtils.isNotEmpty((String)endNode.getProperty(Namespace.NAMESPACE_NAME)))
                    {
                        result = endNode;
                        break;
                    }
                }
            }
        }

        return result;
    }

    private static Node getContainerNode()
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.NAMESPACE.name());
        }

        return containerNodeLocal;
    }
    
    public static void syncGraphToDB(Set<String> dbSysIds)
    {
        if(containerNodeLocal == null)
        {
            containerNodeLocal = allContainerNodes.get(SocialRootRelationshipTypes.NAMESPACE.name());
        }
        
        if(dbSysIds == null)
        {
            //to avoid the null pointer
            dbSysIds = new HashSet<String>();
        }

        if(containerNodeLocal != null)
        {
            Iterable<Relationship> rels  = containerNodeLocal.getRelationships(Direction.OUTGOING, SocialRelationshipTypes.NAMESPACE);
            deleteNodes(dbSysIds, rels);
        }
    }

    private static Namespace insert(Namespace document) throws Exception
    {
        Transaction tx = null;
        try
        {
            getContainerNode();

            //insert
//            tx = GraphDBUtil.getGraphDBService().beginTx();
//
//            Node node = GraphDBUtil.getGraphDBService().createNode();
//            document.convertObjectToNode(node);
//            //                node.setProperty(NODE_TYPE, SocialObjectType.DOCUMENT.name());
//            containerNodeLocal.createRelationshipTo(node, SocialRelationshipTypes.NAMESPACE);
//
//            tx.success();
//
//            //update the object
//            document.convertNodeToObject(node);

        }
        catch (Exception t)
        {
            Log.log.warn("create NameSpace catched exception: " + t, t);
            throw t;
        }
        finally
        {
            if (tx != null)
            {
                tx.finish();
            }
        }

        return document;
    }

    private static Node findNodeByIndexedNamespaceName(String namespaceName)
    {
        Node nodeLocal = null;
        IndexHits<Node> nodes = GraphDBUtil.getAutoNodeIndex().get(Namespace.NAMESPACE_NAME, namespaceName);

        for(Node node: nodes)
        {
            if(node.hasProperty(Namespace.NAMESPACE_NAME))
            {
                if(node.getProperty(Namespace.NAMESPACE_NAME).equals(namespaceName))
                {
                    nodeLocal = node;
                    break;
                }
            }
        }

        return nodeLocal;
    }

    public static void reset()
    {
        if(containerNodeLocal != null)
        {
            Log.log.debug("Resetting container Node with id: " + containerNodeLocal.getId());
            containerNodeLocal = null;
        }
    }

}

