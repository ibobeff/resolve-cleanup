/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * This is just a container to grab all the info for the client at one shot
 * 
 * @author jeet.marwah
 *
 */
public class GenericSectionModel implements Serializable
{
    private static final long serialVersionUID = -7579462958248624450L;
    
    public ArrayList<SectionModel> listOfSections;
	public HashMap<String, String> mapOfString;
	
}
