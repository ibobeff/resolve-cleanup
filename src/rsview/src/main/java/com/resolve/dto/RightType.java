/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.io.Serializable;

/**
 * TYpes of rights that are supported by the application for the user
 * 
 * @author jeet.marwah
 *
 */
public enum RightType implements Serializable
{
	view,//view a document
	edit,//edit a doc right
	execute,//exe a runbook doc right
	admin//admin right that can do anything
	;
	
}
