package com.resolve.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UsersVO;

public class GroupDTO {
    private String id;
    private String sys_id;
    private Date sysCreatedOn;
    private String sysCreatedBy;
    private Date sysUpdatedOn;
    private String sysUpdatedBy;
    private String sysOrganizationName;

    private String UName;
    private String UDescription;
    private Boolean UIsLinkToTeam = false;
    private Boolean UHasExternalLink = false;
    private String roles;
    private List<RolesVO> groupRoles;
    private List<UserDTO> groupUsers;
    private List<OrgsVO> groupOrgs;
	
	public void fromVO(GroupsVO vo) {
        this.setId(vo.getId());
        this.setSys_id(vo.getSys_id());
        this.sysCreatedBy = vo.getSysCreatedBy();
        this.sysCreatedOn = vo.getSysCreatedOn();
        this.sysUpdatedBy = vo.getSysUpdatedBy();
        this.sysUpdatedOn = vo.getSysUpdatedOn();
        this.sysOrganizationName = vo.getSysOrganizationName();
		
		this.setUName(vo.getUName());
		this.setUDescription(vo.getUDescription());
		this.setUIsLinkToTeam(vo.getUIsLinkToTeam());
		this.setUHasExternalLink(vo.getUHasExternalLink());
		this.setRoles(vo.getRoles());
		this.setGroupRoles(vo.getGroupRoles());
		this.setGroupOrgs(vo.getGroupOrgs());
		
		List<UserDTO> dtos = new ArrayList<>();
		if (vo.getGroupUsers()!=null)
		{
			for (UsersVO userVO : vo.getGroupUsers()) {
				UserDTO dto = new UserDTO();
				dto.fromUserVO(userVO);
				dtos.add(dto);
			}
		}
			
		this.setGroupUsers(dtos);
    }

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public String getSys_id() {
		return sys_id;
	}

	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}

	public Date getSysCreatedOn() {
		return sysCreatedOn;
	}

	public void setSysCreatedOn(Date sysCreatedOn) {
		this.sysCreatedOn = sysCreatedOn;
	}

	public String getSysCreatedBy() {
		return sysCreatedBy;
	}

	public void setSysCreatedBy(String sysCreatedBy) {
		this.sysCreatedBy = sysCreatedBy;
	}

	public Date getSysUpdatedOn() {
		return sysUpdatedOn;
	}

	public void setSysUpdatedOn(Date sysUpdatedOn) {
		this.sysUpdatedOn = sysUpdatedOn;
	}

	public String getSysUpdatedBy() {
		return sysUpdatedBy;
	}

	public void setSysUpdatedBy(String sysUpdatedBy) {
		this.sysUpdatedBy = sysUpdatedBy;
	}

	public String getSysOrganizationName() {
		return sysOrganizationName;
	}

	public void setSysOrganizationName(String sysOrganizationName) {
		this.sysOrganizationName = sysOrganizationName;
	}

	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	public String getUDescription() {
		return UDescription;
	}

	public void setUDescription(String uDescription) {
		UDescription = uDescription;
	}

	public Boolean getUIsLinkToTeam() {
		return UIsLinkToTeam;
	}

	public void setUIsLinkToTeam(Boolean uIsLinkToTeam) {
		UIsLinkToTeam = uIsLinkToTeam;
	}

	public Boolean getUHasExternalLink() {
		return UHasExternalLink;
	}

	public void setUHasExternalLink(Boolean uHasExternalLink) {
		UHasExternalLink = uHasExternalLink;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public List<RolesVO> getGroupRoles() {
		return groupRoles;
	}

	public void setGroupRoles(List<RolesVO> groupRoles) {
		this.groupRoles = groupRoles;
	}

	public List<UserDTO> getGroupUsers() {
		return groupUsers;
	}

	public void setGroupUsers(List<UserDTO> groupUsers) {
		this.groupUsers = groupUsers;
	}

	public List<OrgsVO> getGroupOrgs() {
		return groupOrgs;
	}

	public void setGroupOrgs(List<OrgsVO> groupOrgs) {
		this.groupOrgs = groupOrgs;
	}
}
