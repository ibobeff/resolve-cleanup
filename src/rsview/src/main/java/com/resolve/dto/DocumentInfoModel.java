/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.util.Date;

import com.resolve.services.vo.ResolveHashMap;

/**
 * This is a Data object used primarily for the search results 
 * 
 * @author jeet.marwah
 *
 */

public class DocumentInfoModel extends ResolveHashMap // extends BaseTreeModel implements Serializable, com.google.gwt.user.client.rpc.IsSerializable
{

    private static final long serialVersionUID = 420789072153989020L;
    
    public final static String U_FULLNAME = "fullname";
    public static final String SYS_ID = "sys_id";
    public static final String SYS_UPDATED_BY = "modifiedBy";
    public static final String SYS_UPDATED_ON = "modifiedOn";
    public static final String SUMMARY = "summary";
    public static final String IS_RUNBOOK = "isRunbook";
    public static final String IS_DECISION_TREE = "isDecisionTree";
    public static final String FLAG_DOCUMENT = "flagDocument";
    public static final String NAMESPACE = "namespace";
    public static final String DOCNAME = "documentName";
    
	public String getBrowserTimezone()
	{
		return (String) get("browserTimezone");
	}

	public void setBrowserTimezone(String BrowserTimezone)
	{
		set("browserTimezone", BrowserTimezone);
	}

	public String getSys_Id()
	{
		return (String) get(SYS_ID);
	}

	public void setSys_Id(String sys_id)
	{
		set(SYS_ID, sys_id);
	}
	
	public String getSummary()
	{
		return (String) get(SUMMARY);
	}

	public void setSummary(String Summary)
	{
		set(SUMMARY, Summary);
	}

	public String getFullname()
	{
		return (String) get(U_FULLNAME);
	}

	public void setFullname(String fullname)
	{
		set(U_FULLNAME, fullname);
		set("u_display_name", fullname);
	}
	
	public String getNamespace()
	{
		return (String) get(NAMESPACE);
	}

	public void setNamespace(String namespace)
	{
		set(NAMESPACE, namespace);
	}

	public String getDocumentName()
	{
		return (String) get(DOCNAME);
	}

	public void setDocumentName(String documentName)
	{
		set(DOCNAME, documentName);
	}

	public String getViewLink()
	{
		return (String) get("viewLink");
	}

	public void setViewLink(String viewLink)
	{
		set("viewLink", viewLink);
	}

	public String getEditLink()
	{
		return (String) get("editLink");
	}

	public void setEditLink(String editLink)
	{
		set("editLink", editLink);
	}

	public String getModifiedBy()
	{
		return (String) get(SYS_UPDATED_BY);
	}

	public void setModifiedBy(String modifiedBy)
	{
		set(SYS_UPDATED_BY, modifiedBy);
	}

	public String getModifiedOnString()
	{
		return (String) get("modifiedOnString");
	}
	
	public void setModifiedOnString(String ModifiedOnString)
	{
		set("modifiedOnString", ModifiedOnString);
	}

   public Boolean isActive()
    {
        return (Boolean) get("active");
    }
    
    public void setActive(Boolean active)
    {
        set("active", active);
    }

	public Boolean isLocked()
	{
		return (Boolean) get("locked");
	}
	
	public void setLocked(Boolean locked)
	{
		set("locked", locked);
	}
	
	public String lockedBy()
    {
        return (String) get("LOCKED_BY");
    }
    
    public void setLockedBy(String lockedBy)
    {
        set("LOCKED_BY", lockedBy);
    }
	
	public Boolean isHidden()
	{
		return (Boolean) get("hidden");
	}
	
	public void setHidden(Boolean hidden)
	{
		set("hidden", hidden);
	}
	
	public Boolean isDeleted()
	{
		return (Boolean) get("deleted");
	}
	
	public void setDeleted(Boolean deleted)
	{
		set("deleted", deleted);
	}
	
	public void setDefault(Boolean defaultRole)
	{
	    set("default", defaultRole);
	}
	
	public Boolean isDefault()
	{
	    return (Boolean) get("default");
	}

	public void setRunbook(Boolean isRunbook)
	{
	    set("isRunbook", isRunbook);
	}
	    
	public Boolean isRunbook()
	{
	    return (Boolean) get("isRunbook");
	}

    public void setDecisionTree(Boolean isDecisionTree)
    {
        set("isDecisionTree", isDecisionTree);
    }
        
    public Boolean isDecisionTree()
    {
        return (Boolean) get("isDecisionTree");
    }

    public void setFlagDocument(Boolean flagDocument)
    {
        set("flagDocument", flagDocument);
    }
        
    public Boolean isFlagDocument()
    {
        return (Boolean) get("flagDocument");
    }

    public Date getModifiedOn()
	{
		return (Date) get(SYS_UPDATED_ON);
	}

	public void setModifiedOn(Date modifiedOn)
	{
		set(SYS_UPDATED_ON, modifiedOn);
	}
	
	public Boolean isIndexed()
    {
        return (Boolean) get("indexed");
    }
    
    public void setIndexed(Boolean indexed)
    {
        set("indexed", indexed);
    }
    
    public Boolean isIndexedAttachments()
    {
        return (Boolean) get("indexedAttachments");
    }
    
    public void setIndexedAttachments(Boolean indexedAttachments)
    {
        set("indexedAttachments", indexedAttachments);
    }
    
    public Long getCountOfWikidocuments()
    {
        return (Long) get("countOfWikidocuments");
    }
    
    public void setCountOfWikidocuments(Long countOfWikidocuments)
    {
        set("countOfWikidocuments", countOfWikidocuments);
    }
    
}
