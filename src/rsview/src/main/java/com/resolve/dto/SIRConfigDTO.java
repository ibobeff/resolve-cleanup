/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.resolve.persistence.model.Orgs;
import com.resolve.services.hibernate.util.OrgsUtil;
import com.resolve.services.hibernate.vo.CustomDataFormTabVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.SIRConfigVO;
import com.resolve.util.StringUtils;

public class SIRConfigDTO
{
    private String orgId;
    private String orgName;
    private String id;
    private List<CustomDataFormTabDTO> customDataFormTabs;

    public SIRConfigDTO()
    {
        customDataFormTabs = new ArrayList<CustomDataFormTabDTO>();
    }
    
    public SIRConfigDTO(SIRConfigVO sirConfigVO)
    {
        customDataFormTabs = new ArrayList<CustomDataFormTabDTO>();
        
        if (sirConfigVO != null)
        {
            setOrgId(sirConfigVO.getSysOrg());
            
            if (StringUtils.isNotBlank(sirConfigVO.getSysOrg()))
            {
                setOrgName(OrgsUtil.findOrgById(sirConfigVO.getSysOrg()).getUName());
            }
            
            setId(sirConfigVO.getSys_id());
            
            if (sirConfigVO.getCustomDataFormTabs() != null && !sirConfigVO.getCustomDataFormTabs().isEmpty())
            {
                List<CustomDataFormTabDTO> customDataFormTabDTOs = new ArrayList<CustomDataFormTabDTO>();
                
                List<CustomDataFormTabVO> orderedCustomDataFormTabVOs = new ArrayList<CustomDataFormTabVO>(sirConfigVO.getCustomDataFormTabs());
                
                Collections.sort(orderedCustomDataFormTabVOs);
                
                for (CustomDataFormTabVO customDataFormTabVO : orderedCustomDataFormTabVOs)
                {
                    CustomDataFormTabDTO customDataFormTabDTO = new CustomDataFormTabDTO(customDataFormTabVO);
                    customDataFormTabDTOs.add(customDataFormTabDTO);
                }
                
                setCustomDataFormTabs(customDataFormTabDTOs);
            }
        }
    }
    
    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }
    
    public String getOrgName()
    {
        return orgName;
    }

    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
    
    public List<CustomDataFormTabDTO> getCustomDataFormTabs()
    {
        return customDataFormTabs;
    }
    
    public void setCustomDataFormTabs(List<CustomDataFormTabDTO> customDataFormTabObjs)
    {
        customDataFormTabs.clear();
        customDataFormTabs.addAll(customDataFormTabObjs);
    }
    
    public SIRConfigVO toSIRConfigVO(String username)
    {
        SIRConfigVO sirConfigVO = new SIRConfigVO(false);
        
        if (StringUtils.isNotBlank(id))
        {
            sirConfigVO.setSys_id(id);
        }
        
        if (StringUtils.isNotBlank(orgId))
        {
            sirConfigVO.setSysOrg(orgId);
        }
        
        if (StringUtils.isBlank(sirConfigVO.getSysOrg()) && StringUtils.isNotBlank(orgName) &&
            !orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
        {
            Orgs orgs = OrgsUtil.findOrgModelByName(orgName);
            
            if (orgs != null)
            {
                sirConfigVO.setSysOrg(orgs.getSys_id());
            }
        }
        
        if (customDataFormTabs != null && !customDataFormTabs.isEmpty())
        {
            Set<CustomDataFormTabVO> customDataFormTabVOs = new HashSet<CustomDataFormTabVO>();
            
            int order = 0;
            for (CustomDataFormTabDTO customDataFormTabDTO : customDataFormTabs)
            {
                CustomDataFormTabVO customDataFormTabVO = customDataFormTabDTO.toCustomDataFormTabVO(username);
                
                customDataFormTabVO.setSysOrg(sirConfigVO.getSysOrg());
                customDataFormTabVO.setUOrder(Integer.valueOf(++order));
                customDataFormTabVO.setSirConfig(sirConfigVO);
                customDataFormTabVOs.add(customDataFormTabVO);
            }
            
            sirConfigVO.setCustomDataFormTabs(customDataFormTabVOs);
        }
        
        return sirConfigVO;
    }
}
