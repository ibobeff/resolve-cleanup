/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

public class ClientHeaderDTO
{
    private boolean reloadMenu;
    private String redirectUrl = null;

    public boolean isReloadMenu()
    {
        return reloadMenu;
    }

    public ClientHeaderDTO setReloadMenu(boolean reloadMenu)
    {
        this.reloadMenu = reloadMenu;
        return this;
    }
    
    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }
    
    public String getRedirectUrl()
    {
        return redirectUrl;
    }
}
