/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import org.apache.commons.lang3.StringUtils;

import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.CustomDataFormTabVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;

public class CustomDataFormTabDTO
{
    private String id;
    private String name;
    private Boolean isActive;
    private String baseWikiId;
    private String baseWikiFullName;
    
    public CustomDataFormTabDTO()
    {
    }
    
    public CustomDataFormTabDTO(CustomDataFormTabVO customDataFormTabVO)
    {
        if (customDataFormTabVO != null)
        {
            setId(customDataFormTabVO.getSys_id());
            setName(customDataFormTabVO.getUName());
            setIsActive(customDataFormTabVO.getUIsActive());
            setBaseWikiId(customDataFormTabVO.getBaseWiki().getSys_id());
            setBaseWikiFullName(customDataFormTabVO.getBaseWiki().getUFullname());
        }
    }
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public Boolean getIsActive()
    {
        Boolean result = isActive;
        
        if (result == null)
        {
            result = Boolean.FALSE;
        }
        
        return result;
    }
    
    public void setIsActive(Boolean isActive)
    {
        this.isActive = isActive;
    }
    
    public String getBaseWikiId()
    {
        return baseWikiId;
    }
    
    public void setBaseWikiId(String baseWikiId)
    {
        this.baseWikiId = baseWikiId;
    }
    
    public String getBaseWikiFullName()
    {
        return baseWikiFullName;
    }
    
    public void setBaseWikiFullName(String baseWikiFullName)
    {
        this.baseWikiFullName = baseWikiFullName;
    }
    
    public CustomDataFormTabVO toCustomDataFormTabVO(String username)
    {
        CustomDataFormTabVO customDataFormTabVO = new CustomDataFormTabVO(false);
        
        if (StringUtils.isNotBlank(id))
        {
            customDataFormTabVO.setSys_id(id);
        }
        
        customDataFormTabVO.setUName(name);
        customDataFormTabVO.setUIsActive(isActive);
        
        WikiDocumentVO baseWikiDocVO = WikiUtils.getWikiDoc(baseWikiId, baseWikiFullName, username);
        
        if (baseWikiDocVO != null)
        {
            customDataFormTabVO.setBaseWiki(baseWikiDocVO);
        }
        
        return customDataFormTabVO;
    }
    
    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        
        if (otherObj instanceof CustomDataFormTabDTO)
        {
            CustomDataFormTabDTO otherCustomDataFormTabDTO = (CustomDataFormTabDTO)otherObj;
            
            if (otherCustomDataFormTabDTO != this)
            {
                if (otherCustomDataFormTabDTO.getName().equals(this.getName()))
                {
                    isEquals = true;
                }
            }
            else
                isEquals = true; 
        }
        
        return isEquals;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + (this.getName() == null ? 0 : this.getName().hashCode());
        return hash;
    }
}
