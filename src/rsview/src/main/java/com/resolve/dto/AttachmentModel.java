/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.util.Date;

import com.resolve.services.vo.ResolveHashMap;

/**
 * This is a Data object used primarily for showing list of Attachments
 * 
 * @author jeet.marwah
 * 
 */

public class AttachmentModel  extends ResolveHashMap//extends BaseTreeModel implements Serializable, com.google.gwt.user.client.rpc.IsSerializable
{
	
    private static final long serialVersionUID = 3005666739686923828L;

    public String getBrowserTimezone()
	{
		return (String) get("browserTimezone");
	}

	public void setBrowserTimezone(String BrowserTimezone)
	{
		set("browserTimezone", BrowserTimezone);
	}
	
	public String getSys_id()
	{
		return (String) get("sys_id");
	}

	public void setSys_id(String sys_id)
	{
		set("sys_id", sys_id);
	}

	public String getFilename()
	{
		return (String) get("filename");
	}

	public void setFilename(String filename)
	{
		set("filename", filename);
	}

	public String getDocumentName()
	{
		return (String) get("documentName");
	}

	public void setDocumentName(String documentName)
	{
		set("documentName", documentName);
	}

	public String getDownloadLink()
	{
		return (String) get("downloadLink");
	}

	public void setDownloadLink(String downloadLink)
	{
		set("downloadLink", downloadLink);
	}

	public String getType()
	{
		return (String) get("type");
	}

	public void setType(String type)
	{
		set("type", type);
	}

	public String getLocation()
	{
		return (String) get("location");
	}

	public void setLocation(String location)
	{
		set("location", location);
	}

	public String getIcon()
	{
		return (String) get("icon");
	}

	public void setIcon(String icon)
	{
		set("icon", icon);
	}
	
	
	public boolean isGlobal()
	{
		return get("global") != null ? (Boolean) get("global") : false ;
	}
	
	public void setGlobal(Boolean isglobal)
	{
		set("global", isglobal);
	}
	
	public int getSize()
	{
		return (Integer) get("size");
	}

	public void setSize(int size)
	{
		set("size", size);
	}

	public String getCreatedBy()
	{
		return (String) get("createdBy");
	}

	public void setCreatedBy(String createdBy)
	{
		set("createdBy", createdBy);
	}

	public String getCreatedOnString()
	{
		return (String) get("createdOnString");
	}
	
	public void setCreatedOnString(String createdOnString)
	{
		set("createdOnString", createdOnString);
	}
	
	public Date getCreatedOn()
	{
		return (Date) get("createdOn");
	}

	public void setCreatedOn(Date createdOn)
	{
		set("createdOn", createdOn);
	}
}
