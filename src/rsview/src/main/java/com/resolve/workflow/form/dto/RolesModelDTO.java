/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.form.dto;

public class RolesModelDTO
{
    private String read;
    private String edit;
    private String admin;

    public RolesModelDTO() {}
    
    public RolesModelDTO(String read, String edit, String admin)
    {
        this.read = read;
        this.edit = edit;
        this.admin = admin;
    }

    public String getRead()
    {
        return read;
    }

    public void setRead(String read)
    {
        this.read = read;
    }

    public String getEdit()
    {
        return edit;
    }

    public void setEdit(String edit)
    {
        this.edit = edit;
    }

    public String getAdmin()
    {
        return admin;
    }

    public void setAdmin(String admin)
    {
        this.admin = admin;
    }
    
    
    
    
}
