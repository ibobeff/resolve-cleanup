/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.form.dto;

public class MetaFieldDTO
{
    private boolean newField = false;

    private String sys_id;
    private String uname;
    private String utable;
    private String ucolumn;
    private String ucolumnModelName;
    private String udisplayName;
    private String utype;
    private String udbType;
    private Integer usize;

    public boolean isNewField()
    {
        return newField;
    }
    public void setNewField(boolean isNewField)
    {
        this.newField = isNewField;
    }
    public String getSys_id()
    {
        return sys_id;
    }
    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
    public String getUName()
    {
        return uname;
    }
    public void setUName(String uName)
    {
        uname = uName;
    }
    public String getUTable()
    {
        return utable;
    }
    public void setUTable(String uTable)
    {
        utable = uTable;
    }
    public String getUColumn()
    {
        return ucolumn;
    }
    public void setUColumn(String uColumn)
    {
        ucolumn = uColumn;
    }
    public String getUColumnModelName()
    {
        return ucolumnModelName;
    }
    public void setUColumnModelName(String uColumnModelName)
    {
        ucolumnModelName = uColumnModelName;
    }
    public String getUDisplayName()
    {
        return udisplayName;
    }
    public void setUDisplayName(String uDisplayName)
    {
        udisplayName = uDisplayName;
    }
    public String getUType()
    {
        return utype;
    }
    public void setUType(String uType)
    {
        utype = uType;
    }
    public String getUDbType()
    {
        return udbType;
    }
    public void setUDbType(String uDbType)
    {
        udbType = uDbType;
    }
	public Integer getUSize()
    {
        return usize;
    }
    public void setUSize(Integer uSize)
    {
        this.usize = uSize;
    }




}
