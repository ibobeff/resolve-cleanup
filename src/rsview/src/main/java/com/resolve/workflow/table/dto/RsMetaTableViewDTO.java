/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.table.dto;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.resolve.services.vo.UserPreferencesDTO;
import com.resolve.services.vo.form.RsUIComponent;
import com.resolve.services.vo.form.RsUIField;

public class RsMetaTableViewDTO  extends RsUIComponent
{

    private String type;
    private Boolean isGlobal;
    private String user;
    private String metaFormLink;
    private String metaNewLink;
    private String createFormId;
    private String editFormId;
    private String target;
    private String params;
    
    private String parentSysId;
    private String parentName;
    private String parentDisplayName;
    
    private String metaTableSysId;
    private String metaTableName;
    private String metaTableDisplayName;
    
    //roles for a form - CSVs
    private String viewRoles;
    private String editRoles;
    private String adminRoles;

    private String currentUsername;
    
    private List<RsUIField> fields;
    
    private Set<UserPreferencesDTO> userPrefs;
    
    private Date sysUpdatedOn;
    private Date sysCreatedOn;
    
    private String sysUpdatedBy;
    private String sysCreatedBy;
    private String sysOrganizationName;
    
    public RsMetaTableViewDTO() {}

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Boolean getIsGlobal()
    {
        return isGlobal;
    }

    public void setIsGlobal(Boolean isGlobal)
    {
        this.isGlobal = isGlobal;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getMetaFormLink()
    {
        return metaFormLink;
    }

    public void setMetaFormLink(String metaFormLink)
    {
        this.metaFormLink = metaFormLink;
    }

    public String getMetaNewLink()
    {
        return metaNewLink;
    }

    public void setMetaNewLink(String metaNewLink)
    {
        this.metaNewLink = metaNewLink;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }

    public String getParams()
    {
        return params;
    }

    public void setParams(String params)
    {
        this.params = params;
    }

    public String getParentSysId()
    {
        return parentSysId;
    }

    public void setParentSysId(String parentSysId)
    {
        this.parentSysId = parentSysId;
    }

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    public String getParentDisplayName()
    {
        return parentDisplayName;
    }

    public void setParentDisplayName(String parentDisplayName)
    {
        this.parentDisplayName = parentDisplayName;
    }

    public String getViewRoles()
    {
        return viewRoles;
    }

    public void setViewRoles(String viewRoles)
    {
        this.viewRoles = viewRoles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    public String getAdminRoles()
    {
        return adminRoles;
    }

    public void setAdminRoles(String adminRoles)
    {
        this.adminRoles = adminRoles;
    }

    public List<RsUIField> getFields()
    {
        return fields;
    }

    public void setFields(List<RsUIField> fields)
    {
        this.fields = fields;
    }

    public String getMetaTableSysId()
    {
        return metaTableSysId;
    }

    public void setMetaTableSysId(String metaTableSysId)
    {
        this.metaTableSysId = metaTableSysId;
    }

    public String getMetaTableName()
    {
        return metaTableName;
    }

    public void setMetaTableName(String metaTableName)
    {
        this.metaTableName = metaTableName;
    }

    public String getMetaTableDisplayName()
    {
        return metaTableDisplayName;
    }

    public void setMetaTableDisplayName(String metaTableDisplayName)
    {
        this.metaTableDisplayName = metaTableDisplayName;
    }

    public String getCurrentUsername()
    {
        return currentUsername;
    }

    public void setCurrentUsername(String currentUsername)
    {
        this.currentUsername = currentUsername;
    }

    public Set<UserPreferencesDTO> getUserPrefs()
    {
        return userPrefs;
    }

    public void setUserPrefs(Set<UserPreferencesDTO> userPrefs)
    {
        this.userPrefs = userPrefs;
    }

    public String getCreateFormId()
    {
        return createFormId;
    }

    public void setCreateFormId(String createFormId)
    {
        this.createFormId = createFormId;
    }

    public String getEditFormId()
    {
        return editFormId;
    }

    public void setEditFormId(String editFormId)
    {
        this.editFormId = editFormId;
    }

    public Date getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    public Date getSysCreatedOn()
    {
        return sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    public String getSysUpdatedBy()
    {
        return sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    public String getSysCreatedBy()
    {
        return sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    public String getSysOrganizationName()
    {
        return sysOrganizationName;
    }

    public void setSysOrganizationName(String sysOrganizationName)
    {
        this.sysOrganizationName = sysOrganizationName;
    }
    
    
    
}
