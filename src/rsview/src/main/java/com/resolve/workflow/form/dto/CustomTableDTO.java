/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.form.dto;

import java.util.List;

/**
 * phase this out and use RsCUstomTableDTO
 * 
 * @author jeet.marwah
 *
 */
public class CustomTableDTO
{
    private boolean newTable = false;
    
    private String sys_id;
    private String uname;
    private String umodelName;
    private String udisplayName;
    private String utype;
    private String usource;
    private String udestination;
    
    private List<MetaFieldDTO> fields;
    
    
    public String getSys_id()
    {
        return sys_id;
    }
    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
    public String getUName()
    {
        return uname;
    }
    public void setUName(String uName)
    {
        uname = uName;
    }
    public String getUModelName()
    {
        return umodelName;
    }
    public void setUModelName(String uModelName)
    {
        umodelName = uModelName;
    }
    public String getUDisplayName()
    {
        return udisplayName;
    }
    public void setUDisplayName(String uDisplayName)
    {
        udisplayName = uDisplayName;
    }
    public String getUType()
    {
        return utype;
    }
    public void setUType(String uType)
    {
        utype = uType;
    }
    public String getUSource()
    {
        return usource;
    }
    public void setUSource(String uSource)
    {
        usource = uSource;
    }
    public String getUDestination()
    {
        return udestination;
    }
    public void setUDestination(String uDestination)
    {
        udestination = uDestination;
    }
    public List<MetaFieldDTO> getFields()
    {
        return fields;
    }
    public void setFields(List<MetaFieldDTO> fields)
    {
        this.fields = fields;
    }
    
    public boolean isNewTable()
    {
        return newTable;
    }
    public void setNewTable(boolean isNewTable)
    {
        this.newTable = isNewTable;
    }
}
