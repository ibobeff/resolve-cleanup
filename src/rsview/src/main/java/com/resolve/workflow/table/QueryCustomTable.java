/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.table;

import java.util.List;

import com.resolve.dto.ViewLookupTableDTO;
import com.resolve.services.ServiceCustomTable;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.vo.RsMetaFilterDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class QueryCustomTable
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Meta Filter apis
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public ResponseDTO saveMetaFilter(RsMetaFilterDTO filter, String username)
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            //reload the filter
            filter = ServiceCustomTable.saveMetaFilter(filter, username);
            result.setSuccess(true).setData(filter);
        }
        catch (Throwable t)
        {
           result.setSuccess(false).setMessage("Error for the filter :" + filter.getName() + ". " + t.getMessage());
        }
        
        return result;
    }
    
    public ResponseDTO deleteFilters(String sysIds, String username)
    {
        ResponseDTO result = new ResponseDTO();

        String errorStr = ServiceHibernate.deleteFilters(sysIds, username);
        if(StringUtils.isNotEmpty(errorStr))
        {
            result.setSuccess(false).setMessage(errorStr);
        }
        else
        {
            result.setSuccess(true);
        }
        
        return result;

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Meta view lookup
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public ResponseDTO saveViewLookup(ViewLookupTableDTO viewLookup, String username)
    {
        ResponseDTO result = new ResponseDTO();
        
        try
        {
            viewLookup = ServiceCustomTable.saveViewLookup(viewLookup, username);
            result.setSuccess(true).setMessage("Successfully updated the view lookup");
        }
        catch(Throwable t)
        {
            Log.log.error("error in updating the meta form lookup", t);
            result.setSuccess(false).setMessage("error in updating the meta form lookup");
        }
        
        return result;
    }
    
    public ResponseDTO updateMetaViewLookupOrder(String sysIds, String username)
    {
        ResponseDTO result = new ResponseDTO();
        result.setSuccess(true).setMessage("Successfully updated the ordering");

        if(StringUtils.isNotBlank(sysIds))
        {
            try
            {
                List<String> sysIdsList = StringUtils.stringToList(sysIds);
                ServiceCustomTable.updateMetaViewLookupOrder(sysIdsList, username);
            }
            catch(Throwable t)
            {
                Log.log.error("error in updating the ordering for lookup", t);
                result.setSuccess(false).setMessage("error in updating the ordering for lookup");
            }
        }
        
        return result;
    }
    
    

}
