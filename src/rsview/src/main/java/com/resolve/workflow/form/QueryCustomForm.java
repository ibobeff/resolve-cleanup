/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.util.Log;

import com.resolve.dto.UploadFileDTO;
import com.resolve.graph.social.ui.dto.UserPickerQueryModelDTO;
import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.util.fileupload.FileUpload;
import com.resolve.services.ServiceCustomTable;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceJDBC;
import com.resolve.services.ServiceSocial;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.hibernate.customtable.CustomFormUtil;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.MetaFieldPropertiesVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.services.jdbc.util.GenericJDBCHandler;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.radeox.api.engine.WikiRenderEngine;
import com.resolve.wiki.radeox.engine.WikiRadeoxRenderEngine;
import com.resolve.wiki.rsradeox.filter.WikiLinkFilter;
import com.resolve.workflow.form.dto.CustomTableDTO;
import com.resolve.workflow.form.dto.MetaFieldDTO;
import com.resolve.workflow.form.dto.RolesModelDTO;

public class QueryCustomForm
{
    private final static List<String> MODULE_TABLE_MAP = new ArrayList<String>();

    static 
    {
        //rsadmin
        MODULE_TABLE_MAP.add("resolve_cron");
        MODULE_TABLE_MAP.add("sys_user");
        MODULE_TABLE_MAP.add("groups");
        
        //rsworksheet
        MODULE_TABLE_MAP.add("worksheet");
        MODULE_TABLE_MAP.add("archive_worksheet");

        //rsactiontask
        MODULE_TABLE_MAP.add("resolve_preprocess");
        MODULE_TABLE_MAP.add("resolve_parser");
        MODULE_TABLE_MAP.add("resolve_assess");
        MODULE_TABLE_MAP.add("resolve_properties");
        MODULE_TABLE_MAP.add("resolve_action_pkg");
        MODULE_TABLE_MAP.add("resolve_action_task");
        
        //rswiki
        MODULE_TABLE_MAP.add("wikidoc");
    }
    
    public List<Map<String, Object>> getRecordData(QueryDTO query) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        if (StringUtils.isNotBlank(query.getTableName()) || StringUtils.isNotBlank(query.getModelName()) || StringUtils.isNotBlank(query.getSqlQuery()) || StringUtils.isNotBlank(query.getHql()))
        {
            if (query.isUseSql())
            {
                String sql = query.getSelectSQLNonParameterizedSort();
                result = ServiceJDBC.executeSQLSelectForUI(sql, query.getStart(), query.getLimit());
            }
            else
            {
                //Check if table is not one of the internally used custom table in MODULE_TABLE_MAP
                
                if (!MODULE_TABLE_MAP.contains(query.getTableName()))
                {
                    List<QueryFilter> queryFilters = query.getFilters();
                    
                    
                    for (QueryFilter queryFilter : queryFilters)
                    {
                        /*
                         *  Currently only checking for Journal fields, for equals condition and whose value always starts with {[" and ends with "]},
                         *  If WYSIWYG (which is also of type longtext) is needed then will need to get the db column type (TO DO)
                         *  For other conidtions such as contains/not contains UI will need to add starts with and ends with for Journal entry field.
                         */
                        String type = CustomTableMappingUtil.getColumnType(query.getTableName(), queryFilter.getField());
                        if (StringUtils.isNotBlank(type) && type.equals("clob"))
//                        if (queryFilter.getField().startsWith("u_") && queryFilter.getType().equalsIgnoreCase("string") &&
//                            queryFilter.getValue().startsWith("[{\"") && queryFilter.getValue().endsWith("\"}]") &&
//                            queryFilter.getCondition().equalsIgnoreCase(QueryFilter.EQUALS))
                        {
                            queryFilter.setType(QueryFilter.CLOB_TYPE);
                        }
                    }
                }
                
                result = ServiceHibernate.executeHQLSelect(query, query.getStart(), query.getLimit());
            }
        }

        return result;
    }


    /////////////////////////////////////////////
    
    public List<CustomTableDTO> getCustomTables() throws Exception
    {
        List<CustomTableDTO> tables = new ArrayList<CustomTableDTO>();

        // prepare the qry
//        CustomTableVO query = new CustomTableVO();
//        query.setUType(WorkflowConstants.NORMAL_CUSTOM_TABLE.getTagName());

        // get the results
        List<CustomTableVO> allCustomTables = ServiceCustomTable.getAllCustomTables("system");

        for (CustomTableVO table : allCustomTables)
        {
            CustomTableDTO dto = new CustomTableDTO();
            dto.setSys_id(table.getSys_id());
            dto.setUDestination(table.getUDestination());
            dto.setUDisplayName(table.getUDisplayName());
            dto.setUModelName(table.getUModelName());
            dto.setUName(table.getUName());
            dto.setUSource(table.getUSource());
            dto.setUType(table.getUType());

            tables.add(dto);
        }// end of for loop

        //sort it
        Collections.sort(tables, CUSTOMTABLEDTO_COMPARATOR);
        
        return tables;
    }

    public List<ComboboxModelDTO> getCustomTablesForReferenceWidget()  throws Exception
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();

        //prepare the list of resolve tables
        List<String> resolveTables = new ArrayList<String>(MODULE_TABLE_MAP);//getAllModuleTableNames();

        // prepare the qry for custom table
//        CustomTableVO query = new CustomTableVO();
//        query.setUType(WorkflowConstants.NORMAL_CUSTOM_TABLE.getTagName());
        
        List<CustomTableVO> allCustomTables = ServiceCustomTable.getAllCustomTables("system");
        for (CustomTableVO table : allCustomTables)
        {
            resolveTables.add(table.getUName());
        }// end of for loop

        //prepare the result
        if (resolveTables != null)
        {
            java.util.Collections.sort(resolveTables);

            for (String tableName : resolveTables)
            {
                result.add(new ComboboxModelDTO(tableName, tableName));
            }
        }

        //sort it
        Collections.sort(result, COMBOBOXMODEL_COMPARATOR);
        
        return result;
    }


    public CustomTableDTO getCustomTable(String modelTableName)  throws Exception
    {
        // get the results
        CustomTableVO table = ServiceCustomTable.getCustomTableWithReferences(null, modelTableName, "system");
        CustomTableDTO dto = new CustomTableDTO();

        if (table != null)
        {
            dto.setSys_id(table.getSys_id());
            dto.setUDestination(table.getUDestination());
            dto.setUDisplayName(table.getUDisplayName());
            dto.setUModelName(table.getUModelName());
            dto.setUName(table.getUName());
            dto.setUSource(table.getUSource());
            dto.setUType(table.getUType());

            if (table.getMetaFields() != null)
            {
                List<MetaFieldDTO> fields = new ArrayList<MetaFieldDTO>();

                for (MetaFieldVO field : table.getMetaFields())
                {
                    MetaFieldDTO fieldDto = new MetaFieldDTO();
                    fieldDto.setSys_id(field.getSys_id());
                    fieldDto.setUName(field.getUName());
                    fieldDto.setUTable(field.getUTable());
                    fieldDto.setUColumn(field.getUColumn());
                    fieldDto.setUColumnModelName(field.getUColumnModelName());
                    fieldDto.setUDisplayName(field.getUDisplayName());
                    fieldDto.setUType(field.getUType());
                    fieldDto.setUDbType(field.getUDbType());

                    MetaFieldPropertiesVO metaFieldProperties = field.getMetaFieldProperties();
                    if(metaFieldProperties != null)
                    {
                        fieldDto.setUSize(metaFieldProperties.getUStringMaxLength());
                    }

                    fields.add(fieldDto);
                }// end of for loop

                dto.setFields(fields);
            }// end of if
        }
        else
        {
            // its a new table
            dto.setNewTable(true);
        }

        return dto;
    }

    /**
     * TODO: add access rights validation when a user is trying to look up from
     * a form but as its an 'Admin' right now, we can make this as low priority
     *
     * @param tableSysId
     * @param tableName
     * @return
     */
    
    public List<RsUIField> getColumnsForTable(String tableModelName)  throws Exception
    {
        List<RsUIField> columns = new ArrayList<RsUIField>();

        if(StringUtils.isNotBlank(tableModelName))
        {
            boolean isResolveTable = isStaticTable(tableModelName);
            if(isResolveTable)
            {
                Collection<String> columnNames =  ServiceHibernate.getAllColumnsByTable(tableModelName);
                if(columnNames != null)
                {
                    for(String colName : columnNames)
                    {
                        String type = getCorrectTypeForUI(ServiceHibernate.getColumnTypeString(tableModelName, colName));

                        RsUIField field = new RsUIField();
                        field.setName(colName);
                        field.setDisplayName(colName);
                        field.setUiType(type);
                        columns.add(field);
                    }
                }
            }
            else
            {
                //custom table
                Collection<MetaFieldVO> columnNames = ServiceCustomTable.getColumnsForTable(null, tableModelName, "system");
                if(columnNames != null)
                {
                    for(MetaFieldVO metaField : columnNames)
                    {
                        RsUIField field = ServiceHibernate.convertMetaFieldToRsUIField(null, metaField, null, RightTypeEnum.admin, "admin", null, null);
                        columns.add(field);
                    }
                }
            }

        }

        return columns;
    }

    public List<ComboboxModelDTO> getListOfRunbooks()
    {
        return getAllWikiDocuments(null, true);
    }

    public List<ComboboxModelDTO> getListOfAllWikis(String wikiName)
    {
        return getAllWikiDocuments(wikiName, true);
    }

    public List<ComboboxModelDTO> getListOfAllCustomForms()
    {
        List<ComboboxModelDTO> allForms = new ArrayList<ComboboxModelDTO>();

        List<MetaFormViewVO> dbObjs = ServiceHibernate.getAllMetaForms();
        for (MetaFormViewVO view : dbObjs)
        {
            allForms.add(new ComboboxModelDTO(view.getUViewName(), view.getSys_id()));
        }
        
        //sort it
//        Collections.sort(allForms, CustomTableUtil.COMBOBOXMODEL_COMPARATOR);

        return allForms;
    }

    public List<ComboboxModelDTO> getListOfScripts()
    {
        List<ComboboxModelDTO> scripts = new ArrayList<ComboboxModelDTO>();
        List<String> dbObjs = ServiceHibernate.getAllSystemScriptsName(null);
        for (String dbScript : dbObjs)
        {
            scripts.add(new ComboboxModelDTO(dbScript, dbScript));
        }

        return scripts;
    }

    public List<ComboboxModelDTO> getGroups()
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();
        Set<String> groups = UserUtils.getAllGroups();
        for (String group : groups)
        {
            result.add(new ComboboxModelDTO(group, group));
        }

        //sort it
        Collections.sort(result, COMBOBOXMODEL_COMPARATOR);

        return result;
    }

    public List<ComboboxModelDTO> getRoles()
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();
        Set<String> roles = UserUtils.getAllRoles();
        for (String role : roles)
        {
            result.add(new ComboboxModelDTO(role, role));
        }

        //sort it
        Collections.sort(result, COMBOBOXMODEL_COMPARATOR);

        return result;
    }

    public List<ComboboxModelDTO> getTeams()
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();

        List<Map<String, Object>> records = ServiceHibernate.queryCustomTable("social_team", null, null);
        if (records != null && records.size() > 0)
        {
            for (Map<String, Object> rec : records)
            {
                String displayName = (String) rec.get("u_display_name");
                result.add(new ComboboxModelDTO(displayName, displayName));
            }
        }

        //sort it
        Collections.sort(result, COMBOBOXMODEL_COMPARATOR);

        return result;
    }

    public List<ComboboxModelDTO> getTeams(String teamPickerTeamsOfTeams, boolean recurseTeamsOfTeam)throws Exception
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();
        DataContainer dataTeams = getUsersAndTeams(teamPickerTeamsOfTeams, recurseTeamsOfTeam, false);

        if (dataTeams.teams != null)
        {
            for (String team : dataTeams.teams)
            {
                if (StringUtils.isNotBlank(team))
                {
                    ComboboxModelDTO dto = new ComboboxModelDTO(team, team);
                    dto.setType("team");
                    result.add(dto);
                }
            }
        }

        //sort it
        Collections.sort(result, COMBOBOXMODEL_COMPARATOR);

        return result;

    }

    public List<ComboboxModelDTO> getUsersForGroupsAndTeams(UserPickerQueryModelDTO query) throws Exception
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();

        // /////////////////////////
        // prepare users list
        // ////////////////////////
        Set<String> users = new HashSet<String>();
        // all users
        // users.addAll(UserUtils.getAllUsernames(false));

        // from groups
        if (StringUtils.isNotBlank(query.getGroups()))
        {
            String[] groupArray = query.getGroups().split("\\|");
            users.addAll(UserUtils.getUsersForGroup(groupArray));
        }

        // ////////////////////////
        // prepare list of Teams
        // ///////////////////////
        DataContainer dataTeams = getUsersAndTeams(query.getTeamsOfTeams(), query.getRecurseTeamsOfTeam(), false);
        DataContainer dataUsers = getUsersAndTeams(query.getUsersOfTeams(), query.getRecurseUsersOfTeam(), true);

        // ///////////////////////////
        // prepare the return result
        // ///////////////////////////
        users.addAll(dataUsers.users);
        for (String user : users)
        {
            ComboboxModelDTO dto = new ComboboxModelDTO(user, user);
            dto.setType("user");

            result.add(dto);
        }

        for (String team : dataTeams.teams)
        {
            SocialTeamDTO teamModel = ServiceHibernate.getTeamByName(team, "admin");

            if (teamModel != null)
            {
                ComboboxModelDTO dto = new ComboboxModelDTO(teamModel.getU_display_name(), teamModel.getU_owner());
                dto.setType("team");
                result.add(dto);
            }
        }

        if (StringUtils.isBlank(query.getGroups()) && StringUtils.isBlank(query.getTeamsOfTeams()) && StringUtils.isBlank(query.getUsersOfTeams()))
        {
            users.addAll(UserUtils.getAllUsernames(false));
        }

        //sort it
        Collections.sort(result, COMBOBOXMODEL_COMPARATOR);

        return result;
    }

    public RolesModelDTO getDefaultAccessRightsForCustomForms()
    {
        RolesModelDTO roles = new RolesModelDTO();

        // prepare the list of props to get
        List<String> propNames = new ArrayList<String>();
        propNames.add("customform.rights.admin");
        propNames.add("customform.rights.edit");
        propNames.add("customform.rights.read");

        // get the values
        Map<String, String> values = PropertiesUtil.getProperties(propNames);

        // set it
        roles.setAdmin(values.get("customform.rights.admin"));
        roles.setEdit(values.get("customform.rights.edit"));
        roles.setRead(values.get("customform.rights.read"));

        return roles;

    }

//    public static void main(String[] args)
//    {
//        // System.out.println(new
//        // QueryCustomForm().getRenderedWikiLink("[Test.test1]"));
//        // System.out.println(new
//        // QueryCustomForm().getRenderedWikiLink("[somthing here>Test.test1]"));
//
//    }

    public String getRenderedWikiLink(String wikilink, String target)
    {
        StringBuffer result = new StringBuffer();

        if (StringUtils.isNotBlank(wikilink))
        {
            wikilink = wikilink.trim();

            if (wikilink.indexOf("[") > -1)
            {
                wikilink = wikilink.substring(wikilink.indexOf("[") + 1);
            }

            if (wikilink.indexOf("]") > -1)
            {
                wikilink = wikilink.substring(0, wikilink.indexOf("]"));
            }

            // get a handle to Wiki
            Wiki wiki = (Wiki) RSContext.appContext.getBean("wiki");
            WikiRenderEngine renderEngine = (WikiRadeoxRenderEngine) RSContext.appContext.getBean("wikiRadeoxRenderEngine");

            result.append((new WikiLinkFilter()).getGeneratedWikiLink(wiki, renderEngine, wikilink));
        }

        return result.toString();
    }

    public int getTotalCount(QueryDTO query) throws Exception
    {
        if (query.isUseSql())
//            return ServiceJDBC.getTotalCount(query.getCountQuery());
            return ServiceJDBC.getTotalCountSafe(query.getTableName(),query.getSelectColumns(),
            		query.getWhereClause(), ((Main) MainBase.main).getConfigSQL());
        else
            return ServiceHibernate.getTotalHqlCount(query);
    }

    public void deleteRecordData(QueryDTO query) throws Exception
    {

        if (StringUtils.isNotBlank(query.getTableName()) || StringUtils.isNotBlank(query.getModelName()) || StringUtils.isNotBlank(query.getSqlQuery()) || StringUtils.isNotBlank(query.getHql()))
        {
            if (StringUtils.isEmpty(query.getTableName()))
            {
                throw new Exception("Table name can't be empty or null: " + query.getTableName());
            }
            /*
            else
            {
                if (!ServiceJDBC.hasTable(query.getTableName()))
                {
                    throw new Exception("Query table doesn't exist: " + query.getTableName());
                }
            }*/
            
            String sql = query.getDeleteQuery();

            ServiceJDBC.executeSQLUpdate(sql, "system");
        }

    }

    public Map<String, String> getMetaData(QueryDTO query) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();

        if (StringUtils.isNotBlank(query.getTableName()) || StringUtils.isNotBlank(query.getModelName()) || StringUtils.isNotBlank(query.getSqlQuery()) || StringUtils.isNotBlank(query.getHql()))
        {
            String sql = query.getSelectSQLNonParameterizedSort();
            if (query.isUseSql())
            {
                result = ServiceJDBC.getSQLMetaData(query.getTableName());
            }
            else
            {
                result = ServiceHibernate.getHQLMetaData(sql);
            }
        }

        return result;
    }

    public List<ComboboxModelDTO> getReferenceTables(String tableName)  throws Exception
    {
        // SELECT u_name, u_table FROM meta_field_properties WHERE u_ui_type =
        // 'Reference' AND u_reference_table = 'cust_table_1';
        List<ComboboxModelDTO> data = new ArrayList<ComboboxModelDTO>();
        List<MetaFieldPropertiesVO> fields = ServiceCustomTable.getReferenceTables(tableName, "system");

        for (MetaFieldPropertiesVO prop : fields)
        {
            if (prop.getUTable() != null)
            {
                data.add(new ComboboxModelDTO(prop.getUName(), prop.getUTable()));
            }
        }
        
        //sort it
        Collections.sort(data, COMBOBOXMODEL_COMPARATOR);

        return data;
    }

    public List<ComboboxModelDTO> getListOfActionTask()
    {
        List<ComboboxModelDTO> actionTasks = new ArrayList<ComboboxModelDTO>();

        List<String> actiontaskNames = ServiceHibernate.getAllActionTaskNames(/*null*/);
        for (String actiontask : actiontaskNames)
        {
            actionTasks.add(new ComboboxModelDTO(actiontask, actiontask));
        }

        return actionTasks;
    }

    public UploadFileDTO upload(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        return new FileUpload(request, response).upload();
    }

    public void downloadFile(String sys_id, String tableName, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        new FileUpload(request, response).download(sys_id, tableName);
    }
    
    /**
     * API to execute select query on a given table with the 
     * @param tableName
     * @param params
     */
    @SuppressWarnings("unchecked")
	public ResponseDTO selectRecordData(String tableName, Map<String, String> params, List<String> selectColumns)
    {
    	ResponseDTO result = new ResponseDTO();
    	String sqlQuery = "";
    	List<Object> columns = null;
    	List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
    	
    	SQLConnection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        ResultSetMetaData rsmd = null;
    	
    	tableName = HibernateUtil.table2Class(tableName);
    	
    	StringBuilder sql = new StringBuilder("select ");
    	if (selectColumns != null && selectColumns.size() > 0)
    	{
    		for (String col : selectColumns)
    		{
    			if (sql.length() > 7)
    			{
    				sql.append(" , ");
    			}
    			sql.append(col);
    		}
    	}
    	else
    	{
    		sql.append(" * ");
    	}
    	
    	sql.append(" from ").append(tableName);
    	if (params != null && params.size() > 0)
    	{
    		columns = Arrays.asList(params.keySet().toArray());
    		sql.append(" where ");
    		
    		for (Object column : columns)
    		{
    			sql.append(column + " = ? and ");
    		}
    		
    		sqlQuery = sql.substring(0, sql.length() - 5);
    	}
    	
    	if (StringUtils.isNotBlank(sqlQuery))
    	{
    		try
    		{   			
    			
    			connection = SQL.getConnection();
                statement = connection.prepareStatement(SQLUtils.getSafeSQL(sqlQuery));
	    		
	    		if (columns != null)
	    		{
	    			for (int i=0; i<columns.size(); i++)
	    			{
	    				statement.setString(i+1, params.get(columns.get(i)));
	    			}
	    		}
	    		
	    		rs = statement.executeQuery();
	            rsmd = rs.getMetaData();
	            int numberOfColumns = rsmd.getColumnCount();
	            String[] dbColumnNames = new String[numberOfColumns];
	            for (int count = 0; count < numberOfColumns; count++)
	            {
	                // columns start with 1 , so count+1
	                dbColumnNames[count] = rsmd.getColumnLabel(count + 1);
	            }
	            
	            while (rs.next())
	            {
	                Map<String, Object> row = GenericJDBCHandler.getRowData(rs, dbColumnNames, false);
	                data.add(row);
	            }
	    		
//	    		int count = ServiceJDBC.getTotalCountSafe(tableName, null, null);
//	    		result.setTotal(count);
	    		result.setRecords(data);
    		}
    		catch(Exception e)
    		{
    			Log.log.error(e.getMessage(), e);
    		}
    		finally
            {

                try
                {
                    if (rs != null)
                    {
                        rs.close();
                    }

                    if (statement != null)
                    {
                        statement.close();
                    }

                    if (connection != null)
                    {
                        connection.close();
                    }
                }
                catch (Throwable t)
                {
                }
            }
    	}
    	
    	return result;
    }

    // private apis
    private DataContainer getUsersAndTeams(String teams, boolean recurse, boolean processUsers) throws Exception
    {
        Set<String> allTeams = new HashSet<String>();
        Set<String> allUsers = new HashSet<String>();

        if (StringUtils.isNotBlank(teams))
        {
            String[] teamsArray = teams.split("\\|");

            for (String teamName : teamsArray)
            {
                // add this team first and then find the rest of the info
                //allTeams.add(teamName);

                Team team = SocialCompConversionUtil.createTeamByName(teamName);
                if (team != null)
                {
                    allTeams.add(teamName);
                    
                    // true is no child
                    // false is with all the childs, so this is recurse
                    team = ServiceSocial.getTeam(team.getSys_id(), recurse);

                    // for teams in a team
                    Collection<Team> localTeams = team.getTeams();
                    allTeams.add(team.getDisplayName());
                    if (recurse)
                    {
                        getAllTeamsFor(localTeams, allTeams);
                    }

                    // for users
                    if (processUsers)
                    {
                        Collection<User> localUsers = team.getUsers();
                        if (localUsers != null)
                        {
                            for (User user : localUsers)
                            {
                                allUsers.add(user.getName());
                            }
                        }
                        getAllUsersForTeam(localTeams, allUsers);
                    }

                }
            }

        }

        DataContainer data = new DataContainer();
        data.teams = allTeams;
        data.users = allUsers;

        return data;

    }

    private void getAllTeamsFor(Collection<Team> localTeams, Set<String> teams)
    {
        if (localTeams != null)
        {
            for (Team team : localTeams)
            {
                teams.add(team.getDisplayName());

                // recurse
                if (team.getTeams() != null && team.getTeams().size() > 0)
                {
                    getAllTeamsFor(team.getTeams(), teams);
                }
            }
        }
    }

    private void getAllUsersForTeam(Collection<Team> localTeams, Set<String> users)
    {
        if (localTeams != null)
        {
            for (Team team : localTeams)
            {
                Collection<User> localUsers = team.getUsers();
                if (localUsers != null)
                {
                    for (User user : localUsers)
                    {
                        users.add(user.getName());
                    }
                }

                // recurse
                if (team.getTeams() != null && team.getTeams().size() > 0)
                {
                    getAllUsersForTeam(team.getTeams(), users);
                }
            }
        }
    }
    
    private String getCorrectTypeForUI(String type)
    {
        String uitype = type;
        
        if(StringUtils.isNotBlank(uitype))
        {
            if(uitype.equalsIgnoreCase("yes_no"))
            {
                uitype = "boolean";
            }
            else if(uitype.indexOf("Clob") > -1 || uitype.startsWith("com."))
            {
                uitype = "string";
            }
        }
        
        return uitype;
    }
    
    private List<ComboboxModelDTO> getAllWikiDocuments(String wikiname, boolean isRunbook)
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();
        List<String> docs = ServiceWiki.getAllWikiDocuments(wikiname, isRunbook);
        if (docs != null)
        {
            for (String doc : docs)
            {
                result.add(new ComboboxModelDTO(doc, doc));
            }
        }

        return result;
    }
    
    private static Comparator<ComboboxModelDTO> COMBOBOXMODEL_COMPARATOR = new Comparator<ComboboxModelDTO>()
    {

        @Override
        public int compare(ComboboxModelDTO o1, ComboboxModelDTO o2)
        {
            String name1 = o1.getName().toLowerCase();
            String name2 = o2.getName().toLowerCase();

            return name1.compareTo(name2);

        }
    };

    private static Comparator<CustomTableDTO> CUSTOMTABLEDTO_COMPARATOR = new Comparator<CustomTableDTO>()
    {

        @Override
        public int compare(CustomTableDTO o1, CustomTableDTO o2)
        {
            String name1 = o1.getUName().toLowerCase();
            String name2 = o2.getUName().toLowerCase();

            return name1.compareTo(name2);

        }
    };
    
    private static boolean isStaticTable(String table)
    {
        return MODULE_TABLE_MAP.contains(table);
    }
    
    public List<ComboboxModelDTO> getListOfAllCustomFormNamesWithIds() throws Exception
    {
        return CustomFormUtil.getAllMetaFormViewNamesWithIds();
    }
}

class DataContainer
{
    public Set<String> teams;
    public Set<String> users;
}
