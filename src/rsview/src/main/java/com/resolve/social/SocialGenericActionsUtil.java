/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.social;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsview.main.RSContext;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.Post;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.constants.HibernateConstantsEnum;

public class SocialGenericActionsUtil
{
    public static void sendJoinConfirmPostForUser(NodeType type, UsersVO sender, User targetUser, SocialDTO socialDTO)
    {
        String userInfo = targetUser.getDisplayName() + "(" + targetUser.getName()+ ")";

        String postContent = "";
        if (type == NodeType.PROCESS)
        {
            postContent = "User '" + userInfo + "' has been successfully added to Process '" + socialDTO.getU_display_name() + "'";
        }
        else if(type == NodeType.TEAM)
        {
            postContent = "User '" + userInfo + "' has been successfully added to Team '" + socialDTO.getU_display_name() + "'";
        }
        else if(type == NodeType.FORUM)
        {
            postContent = "User '" + userInfo + "' has been successfully added to Forum '" + socialDTO.getU_display_name() + "'";
        }
        
        sendJoinConfirmPostForUser(sender, targetUser, socialDTO, postContent);
    }
    
    public static void sendRejectConfirmPostForUser(NodeType type, UsersVO sender, User targetUser, SocialDTO socialDTO)
    {
        String userInfo = targetUser.getDisplayName() + "(" + targetUser.getName()+ ")";

        String postContent = "";
        if (type == NodeType.PROCESS)
        {
            postContent = "User '" + userInfo + "' is not allowed to be added to Process '" + socialDTO.getU_display_name() + "'";
        }
        else if(type == NodeType.TEAM)
        {
            postContent = "User '" + userInfo + "' is not allowed to be added to Team '" + socialDTO.getU_display_name() + "'";
        }
        else if(type == NodeType.FORUM)
        {
            postContent = "User '" + userInfo + "' is not allowed to be added to Forum '" + socialDTO.getU_display_name() + "'";
        }

        sendJoinConfirmPostForUser(sender, targetUser, socialDTO, postContent);
    }
    
    private static void sendJoinConfirmPostForUser(UsersVO sender, User targetUser, SocialDTO container, String postContent)
    {
        if(targetUser != null)
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(HibernateConstantsEnum.TIMEZONE.getTagName(), Post.TIMEZONE_DEFAULT);
            params.put(HibernateConstantsEnum.TIMEZONE_OFFSET.getTagName(), Post.TIMEZONE_OFFSET_DEFAULT);

            String username = sender.getUUserName();
            String targetSysId = targetUser.getSys_id();
            String targetCompType = NodeType.USER.name();//AdvanceTree.USERS;
            
            new PostUtil(params).post(postContent, username, targetSysId, targetCompType);
        }
        else
        {
            throw new RuntimeException("No Owner is assigned to '" + container.getU_display_name() + "'");
        }
    }
}