/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.social.impex;

import java.text.SimpleDateFormat;


public class ImportSocialGraph
{
    public static SimpleDateFormat SQLTimestampFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public static SimpleDateFormat SQLDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    public static String FOLLOW = "follow";
    public static String MEMBER = "member";
    
//    public static void importSocialGraph(String folderLocation) throws Exception
//    {
//        
//        Log.log.info("start importing graph");
//        
//        if(StringUtils.isNotEmpty(folderLocation))
//        {
//            String installDir = folderLocation + File.separator + "install";
//            
//            File installFolder = FileUtils.getFile(installDir);
//            
//            if(!installFolder.isDirectory())
//            {
//                //throw new WikiException("Folder with '" + installDir + "' name does not exist.");
//                Log.log.info("Folder with '" + installDir + "' name does not exist.");
//            }
//            else
//            {
//                //create filter that reads only xml files
//                FilenameFilter filter = new FilenameFilter()
//                {
//                    @Override
//                    public boolean accept(File dir, String name)
//                    {
//                        if (name != null && name.endsWith(".xml"))
//                            return true;
//                        else
//                            return false;
//                    }
//                };
//                
//                //install folder
//                File[] installfiles = installFolder.listFiles(filter);
//                List<RSComponent> installRSComponentList = new ArrayList<RSComponent>();
//                List<ResolveGraphNode> installNodeList = new ArrayList<ResolveGraphNode>();
//                List<ResolveCatalog> installCatalogList = new ArrayList<ResolveCatalog>();
//                List<XDoc> installEdge = new ArrayList<XDoc>();
//                
//                for(File installF: installfiles)
//                {
//                    if(installF.isFile())
//                    {
//                        XDoc xDocLocal = com.resolve.services.hibernate.util.ImpexUtil.convertFileToXDoc(installF);
//                        String rootElementName = xDocLocal.getRoot().getName();
//                        
//                        if(rootElementName.equals("graph_edge"))
//                        {
//                            installEdge.add(xDocLocal);
//                        }
//                        else
//                        {
////                            RSComponent comp = convertXDocToRSComponent(xDocLocal);
////                            
////                            if(comp != null)
////                            {
////                                installRSComponentList.add(comp);
////                            }
////                            
////                            ResolveGraphNode rgNode = convertXDocToNode(xDocLocal);
////                            if(rgNode != null)
////                            {
////                                installNodeList.add(rgNode);
////                            }
//                            
//                            ResolveCatalog catalog = convertXDocToCatalog(xDocLocal);
//                            if(catalog != null)
//                            {
//                                installCatalogList.add(catalog);
//                            }
//                        }
//                    }
//                }
//                
//                List<GraphEdge> graphEdges = convertXDocEdgesToObjects(installEdge);
//                
//                if(installRSComponentList != null && !installRSComponentList.isEmpty())
//                {
//                    Log.log.info("Start importing social node");
//                    updateRSComponent(installRSComponentList);
//                }
//               
//                if(installCatalogList != null && !installCatalogList.isEmpty())
//                {
//                    Log.log.info("Start importing catalogs");
//                    updateCatalogs(installCatalogList);
//                }
//                
//                if(installNodeList != null && !installNodeList.isEmpty())
//                {
//                    Log.log.info("Start importing tags");
//                    updateNodes(installNodeList);
//                }
//                
//                Log.log.info("Start importing relations");
//                updateEdges(graphEdges);
//                
//                Log.log.info("finished importing graph");
//            }   
//        }
//    }
//    
//    public static void uninstallSocialGraph(String folderLocation) throws Exception
//    {
//        if(StringUtils.isNotEmpty(folderLocation))
//        {
//            String uninstallDir = folderLocation + File.separator + "uninstall";
//            File uninstallFolder = FileUtils.getFile(uninstallDir);
//            
//            if(!uninstallFolder.isDirectory())
//            {
//                throw new WikiException("Folder with '" + uninstallDir + "' name does not exist.");
//            }
//            
//            FilenameFilter filter = new FilenameFilter()
//            {
//                @Override
//                public boolean accept(File dir, String name)
//                {
//                    if (name != null && name.endsWith(".xml"))
//                        return true;
//                    else
//                        return false;
//                }
//            };
//            
//           
//            File[] uninstallfiles = uninstallFolder.listFiles(filter);
//            List<RSComponent> uninstallRSComponentList = new ArrayList<RSComponent>();
//            List<ResolveGraphNode> uninstallNodeList = new ArrayList<ResolveGraphNode>();
//            List<ResolveCatalog> uninstallCatalogList = new ArrayList<ResolveCatalog>();
//            List<XDoc> uninstallEdge = new ArrayList<XDoc>();
//            
//            for(File uninstallF: uninstallfiles)
//            {
//                if(uninstallF.isFile())
//                {
//                    XDoc xDocLocal = ImpexUtil.convertFileToXDoc(uninstallF);
//                    String rootElementName = xDocLocal.getRoot().getName();
//                    
//                    if(rootElementName.equals("graph_edge"))
//                    {
//                        uninstallEdge.add(xDocLocal);
//                    }
//                    else
//                    {
//                        RSComponent rsComp = convertXDocToRSComponent(xDocLocal);
//                        if(rsComp != null)
//                        {
//                            uninstallRSComponentList.add(rsComp);
//                        }
//                        
//                        ResolveGraphNode rgNode = convertXDocToNode(xDocLocal);
//                        if(rgNode != null)
//                        {
//                            uninstallNodeList.add(rgNode);
//                        }
//                        
//                        ResolveCatalog catalog = convertXDocToCatalog(xDocLocal);                        
//                        if(catalog != null)
//                        {
//                            uninstallCatalogList.add(catalog);
//                        }
//                        
//                    }
//                }
//            }
//            
//            List<GraphEdge> graphEdges = convertXDocEdgesToObjects(uninstallEdge);
//            
//            updateEdges(graphEdges);
//            updateRSComponent(uninstallRSComponentList);
//           
//            if(uninstallCatalogList != null && !uninstallCatalogList.isEmpty())
//            {
//                updateCatalogs(uninstallCatalogList);
//            }
//            
//            if(uninstallNodeList != null && !uninstallNodeList.isEmpty())
//            {
//                updateNodes(uninstallNodeList);
//            }
//            
//        }
//    }
//    
//    @SuppressWarnings("unchecked")
//    public static List<GraphEdge> convertXDocEdgesToObjects(List<XDoc> edges) throws Exception
//    {
//        List<GraphEdge> graphEdges = new ArrayList<GraphEdge>();
//        
//        GraphEdge graphEdge = new GraphEdge();
//        
//        if(edges != null)
//        {
//            for(XDoc xDocLocal: edges)
//            {
//                graphEdge = new GraphEdge();
//                
//                String rootElmName = xDocLocal.getRoot().getName();
//                List<Element> columns  = xDocLocal.getNodes("/"+ rootElmName +"/*");
//                
//                for(Element elem: columns)
//                {
//                    String edgetype = elem.getName();
//                   
//                    Attribute attrFrom = elem.attribute(RSComponent.FROM);
//                    String fromSysID = attrFrom.getValue();
//                   
//                    Attribute attrTo = elem.attribute(RSComponent.TO);
//                    String toSysID = attrTo.getValue();
//                   
//                    Attribute attrAction = elem.attribute(RSComponent.ACTION);
//                    String valueAction = attrAction.getValue();
//                    
//                    Attribute notificationvalueAttr = elem.attribute(ResolveGraphFactory.NOTIFICATIONVALUE.toLowerCase());
//                    String notificationValue = (notificationvalueAttr != null)?notificationvalueAttr.getValue():"";
//                    
//                    graphEdge = new GraphEdge();
//                    graphEdge.setRelationType(edgetype);
//                    graphEdge.setFromSysID(fromSysID);
//                    graphEdge.setToSysID(toSysID);
//                    graphEdge.setOperation(valueAction);
//                    
//                    if(StringUtils.isNotEmpty(notificationValue))
//                    {
//                        graphEdge.setNotificationValue(notificationValue);
//                    }
//                    
//                    graphEdges.add(graphEdge);
//                    
//                }
//               
//            }
//        }
//        
//        return graphEdges;
//    }
//    
//    public static void updateEdges(List<GraphEdge> graphEdges) throws Exception
//    {
//        if(graphEdges != null)
//        {
//            for(GraphEdge edge: graphEdges)
//            {
//                String edgetype = (edge.getRelationType().toUpperCase().equalsIgnoreCase(FOLLOW))?(RelationType.FOLLOWER.name()):(edge.getRelationType().toUpperCase());
//                String fromSysID = edge.getFromSysID();
//                String toSysID = edge.getToSysID();
//                String operation = edge.getOperation();
//                String notificationvalue = edge.getNotificationValue();
//                
//                if(edgetype.equalsIgnoreCase(MEMBER) || edgetype.equalsIgnoreCase(RelationType.FOLLOWER.name()))
//                {
//                    if(operation.equals(RSComponent.INSERT_OR_UPDATE))
//                    {
//                        UpdateSocial.addRelation(fromSysID, toSysID, edgetype);
//                        Log.log.info("importing fromSysID -->" + fromSysID + " --- toSysID -- " + toSysID);
//                    }
//                    else if(operation.equals(RSComponent.DELETE))
//                    {
//                        UpdateSocial.deleteRelation(fromSysID, toSysID, edgetype);
//                    }
//                }
//                else if(edgetype.equalsIgnoreCase(SocialRelationshipTypes.CATALOG_REF.name())) 
//                {
//                    updateCatalogEdges(fromSysID, toSysID, operation, SocialRelationshipTypes.CATALOG_REF);
//                }
//                else if(edgetype.equalsIgnoreCase(SocialRelationshipTypes.CHILD_OF_CATALOG.name()) )
//                {
//                    updateCatalogEdges(fromSysID, toSysID, operation, SocialRelationshipTypes.CHILD_OF_CATALOG);
//                }
//                else if(edgetype.equalsIgnoreCase(SocialRelationshipTypes.TAG_OF_CAT.name().toLowerCase())) 
//                {
//                    updateCatalogEdges(fromSysID, toSysID, operation, SocialRelationshipTypes.TAG_OF_CAT);
//                }
//                else
//                {
//                    if(operation.equals(RSComponent.INSERT_OR_UPDATE))
//                    {
//                        UpdateSocial.addNotificationConfig(fromSysID, toSysID, edgetype, notificationvalue);
//                    }
//                    else if(operation.equals(RSComponent.DELETE))
//                    {
//                        UpdateSocial.removeRelation(fromSysID, toSysID, edgetype);
//                    }
//                   
//                }
//            }
//        }
//    }
//    
//    public static void updateCatalogEdges(String fromSysID, String toSysID, String operation, SocialRelationshipTypes type)
//    {
//        if(operation.equals(RSComponent.INSERT_OR_UPDATE))
//        {
//            CreateUpdateCatalog.addRelation(fromSysID, toSysID, type);
//        }
//        else if(operation.equals(RSComponent.DELETE))
//        {
//            CreateUpdateCatalog.deleteRelation(fromSysID, toSysID,  type);
//        }
//    }
//    
//    public static void updateRSComponent(List<RSComponent> compList) throws Exception
//    {
//        if(compList != null)
//        {
//            for(RSComponent compLocal: compList)
//            {
//                if(compLocal.getOperation().equalsIgnoreCase("insert_or_update"))
//                {
//                    ServiceSocial.insertOrUpdateRSComponent(compLocal);
//                }
//                else if(compLocal.getOperation().equalsIgnoreCase("delete"))
//                {
//                    DeleteSocial.deleteRSComponent(compLocal);
//                }
//            }
//        }
//    }
//    
//    public static void updateCatalogs(List<Catalog> catalogList) throws Exception
//    {
//        List<Catalog> createOrUpdateListIsRoot = new ArrayList<Catalog>();
//        List<Catalog> createOrUpdateListNotRoot = new ArrayList<Catalog>();
//        
//        List<Catalog> deleteList = new ArrayList<Catalog>();
//        
//        try
//        {
//            if(catalogList != null)
//            {
//                for(Catalog catalogLocal: catalogList)
//                {
//                    if(catalogLocal.getOperation().equalsIgnoreCase("insert_or_update"))
//                    {
//                        if(CatalogFactory.findNodeByIndexedID(catalogLocal.getId()) == null)
//                        {
//                            if(catalogLocal.getClass().getName().equals("com.resolve.services.graph.catalog.model.ResolveCatalog"))
//                            {
//                                if(catalogLocal.isRoot() || catalogLocal.isRootRef())
//                                {
//                                    createOrUpdateListIsRoot.add(catalogLocal);
//                                }
//                                else
//                                {
//                                    createOrUpdateListNotRoot.add(catalogLocal);
//                                }
//                            }
//                        }
//                    }
//                    else if(catalogLocal.getOperation().equalsIgnoreCase("delete"))
//                    {
//                        if (CatalogFactory.findNodeByIndexedID(catalogLocal.getId()) == null)
//                        {
//                            if (catalogLocal.getClass().getName().equals("com.resolve.services.graph.catalog.model.ResolveCatalog"))
//                            {
//                                deleteList.add(catalogLocal);
//                            }
//                        }
//                    }
//                }
//                
//                
////                for(Catalog catLocal : createOrUpdateListIsRoot)
////                {
////                    CatalogFactory.createUpdateCatalogNode(catLocal, true);
////                }
////                
////                for(Catalog catLocal : createOrUpdateListNotRoot)
////                {
////                    CatalogFactory.createUpdateCatalogNode(catLocal, false);
////                }
//                
//                for(Catalog catLocal: deleteList)
//                {
//                    ServiceCatalog.deleteCatalog(catLocal.getId(), null, "admin");
//                }
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//    }
//    
//    
//    //TODO: Review the import of the Tags/Catalog and all the Graph related components
//    private static void updateNodes(List<ResolveGraphNode> nodeList) throws Exception
//    {
//        List<ResolveTag> createOrUpdateList = new ArrayList<ResolveTag>();
//        List<ResolveTag> deleteList = new ArrayList<ResolveTag>();
//        
//        try
//        {
//            if(nodeList != null)
//            {
//                for(ResolveGraphNode nodeLocal: nodeList)
//                {
//                    if(nodeLocal.getOperation().equalsIgnoreCase("insert_or_update"))
//                    {
//                        if(SocialFactory.findNodeByIndexedID(nodeLocal.getSys_id()) == null)
//                        {
//                            if(nodeLocal.getClass().getName().equals("com.resolve.services.graph.resolvetag.model.ResolveTag"))
//                            {
//                                createOrUpdateList.add((ResolveTag)nodeLocal);
//                            }
//                        }
//                    }
//                    else if(nodeLocal.getOperation().equalsIgnoreCase("delete"))
//                    {
//                        if (SocialFactory.findNodeByIndexedID(nodeLocal.getSys_id()) == null)
//                        {
//                            if (nodeLocal.getClass().getName().equals("com.resolve.services.graph.resolvetag.model.ResolveTag"))
//                            {
//                                deleteList.add((ResolveTag)nodeLocal);
//                            }
//                        }
//                    }
//                }
//                
////                TagFactory.createResolveTags(createOrUpdateList, true); 
////                ServiceTag.deleteResolveTags(deleteList, "admin");
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//    }
//    
//    
//    public static RSComponent convertXDocToRSComponent(XDoc installXDoc) throws Exception
//    {
//        
//        RSComponent rsComp = null;
//        
//        String rootElmName = installXDoc.getRoot().getName();                    
//        String type = installXDoc.getStringValue("/"+rootElmName+"/@type");
//        Map<String, String> values = prepareRowData(installXDoc, rootElmName);
//                
//        rsComp = SocialUtil.getSocialComponentByType(type);
//        
//        if(rsComp != null)
//        {
//            rsComp.setId(values.get("sys_id"));
//            rsComp.setSys_id(values.get("sys_id"));
//            rsComp.setDisplayName(values.get("displayName"));
//
//            rsComp.setOperation(values.get("operation"));
//            rsComp.setSys_created_by(values.get("sys_created_by"));
//            rsComp.setSys_updated_by(values.get("sys_updated_by"));
//            
//            String sysCreateOnString = values.get("sys_created_on");
//            String sysUpdateOnString = values.get("sys_updated_on");
//            
//            long sysCreateOnLong = SQLTimestampFormatter.parse(sysCreateOnString).getTime();
//            long sysUpdateOnLong = SQLTimestampFormatter.parse(sysUpdateOnString).getTime();
//            
//            rsComp.setSys_created_on(sysCreateOnLong);
//            rsComp.setSys_updated_on(sysUpdateOnLong);
//        }
//        
//        return rsComp;
//       
//    }
//    
//    public static ResolveGraphNode convertXDocToNode(XDoc installXDoc) throws Exception
//    {
//        ResolveGraphNode rgNode = null;
//        
//        String rootElmName = installXDoc.getRoot().getName();                    
//        String type = installXDoc.getStringValue("/"+rootElmName+"/@type");
//        Map<String, String> values = prepareRowData(installXDoc, rootElmName);
//                
//        rgNode = SocialUtil.getSocialNodeByType(type);
//        
//        if(rgNode != null)
//        {
//            rgNode.setId(values.get("sys_id"));
//            rgNode.setSys_id(values.get("sys_id"));
//            rgNode.setName(values.get("displayName"));
//            rgNode.setOperation(values.get("operation"));
//            rgNode.setSys_created_by(values.get("sys_created_by"));
//            rgNode.setSys_updated_by(values.get("sys_updated_by"));
//            
//            String sysCreateOnString = values.get("sys_created_on");
//            String sysUpdateOnString = values.get("sys_updated_on");
//            
//            long sysCreateOnLong = SQLTimestampFormatter.parse(sysCreateOnString).getTime();
//            long sysUpdateOnLong = SQLTimestampFormatter.parse(sysUpdateOnString).getTime();
//            
//            rgNode.setSys_created_on(sysCreateOnLong);
//            rgNode.setSys_updated_on(sysUpdateOnLong);
//        }
//        
//        return rgNode;
//       
//    }
//    
//    public static ResolveCatalog convertXDocToCatalog(XDoc installXDoc) throws Exception
//    {
//        ResolveCatalog catalog = null; 
//        
//        try
//        {
//            String rootElmName = installXDoc.getRoot().getName();
//            String type = installXDoc.getStringValue("/"+rootElmName+"/@type");
//            if (StringUtils.isNotBlank(type) && type.equals("resolvecatalog"))
//            {
//                catalog = new ResolveCatalog();
//                Map<String, String> values = prepareRowData(installXDoc, rootElmName);
//               
//                catalog.setId(values.get("sys_id"));
//                catalog.setName(values.get("displayName"));
//                catalog.setOperation(values.get("operation"));
//                catalog.setSys_created_by(values.get("sys_created_by"));
//                catalog.setSys_updated_by(values.get("sys_updated_by"));
//                    
//                String sysCreateOnString = values.get("sys_created_on");
//                String sysUpdateOnString = values.get("sys_updated_on");
//                    
//                long sysCreateOnLong = SQLTimestampFormatter.parse(sysCreateOnString).getTime();
//                long sysUpdateOnLong = SQLTimestampFormatter.parse(sysUpdateOnString).getTime();
//                    
//                catalog.setSys_created_on(sysCreateOnLong);
//                catalog.setSys_updated_on(sysUpdateOnLong);
//                
//                catalog.setType(values.get(ResolveCatalog.TYPE));
//                catalog.setOrder( (values.get(ResolveCatalog.ORDER) == null)?0:(Integer.valueOf(values.get(ResolveCatalog.ORDER))));
//                catalog.setRoles(values.get(ResolveCatalog.ROLES));
//                catalog.setEditRoles(values.get(ResolveCatalog.EDIT_ROLES));
//                catalog.setTitle(values.get(ResolveCatalog.TITLE));
//                catalog.setDescription(values.get(ResolveCatalog.DESC));
//                catalog.setImage(values.get(ResolveCatalog.IMAGE));
//                catalog.setImageName(values.get(ResolveCatalog.IMAGE_NAME));
//                catalog.setTooltip(values.get(ResolveCatalog.TOOLTIP));
//                catalog.setWiki(values.get(ResolveCatalog.WIKI));
//                catalog.setLink(values.get(ResolveCatalog.LINK));
//                catalog.setDisplayType(values.get(ResolveCatalog.DISPLAYTYPE));
//                catalog.setWikidocSysID(values.get(ResolveCatalog.WIKISYSID));
//                catalog.setIsRoot(Boolean.valueOf(values.get(ResolveCatalog.IS_ROOT)));
//                catalog.setIsRootRef(Boolean.valueOf(values.get(ResolveCatalog.IS_ROOT_REF)));
//                catalog.setCatalogType(values.get(ResolveCatalog.CATALOG_TYPE_STRING));
//                catalog.setPath(values.get(ResolveCatalog.PATH));
//                catalog.setOpenInNewTab(Boolean.valueOf(values.get(ResolveCatalog.OPEN_IN_NEW_TAB)));
//            }
//        }
//        catch(Exception exp)
//        {
//            Log.log.info(exp, exp);
//        }
//        
//        return catalog;
//       
//    }
//    
//    @SuppressWarnings("unchecked")
//    public static Map<String, String> prepareRowData(XDoc doc, String rootElementName)
//    {
//        String type = doc.getStringValue("/" + rootElementName + "/@type");
//
//        HashMap<String, String> row = new HashMap<String, String>();
//        
//        String updateOrDelete = doc.getStringValue("/" + rootElementName + "/" + type + "/@action");
//        row.put("operation", updateOrDelete);
//        
//        List<Element> columns = doc.getNodes("/graph_node/" + type + "/*");
//        
//        for(Element e : columns)
//        {
//            String name = e.getName();
//            String value = e.getText();
//            row.put(name, value);
//        }
//        
//        return row;
//        
//    }//prepareRowData
    
}