/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.social;

import javax.servlet.http.HttpServletRequest;

import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.util.Constants;

public class SocialUtil
{
    public static User getSocialUser(HttpServletRequest request)
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        return SocialCompConversionUtil.getSocialUser(username);
    }

}
