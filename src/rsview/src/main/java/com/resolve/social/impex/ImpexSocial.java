/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.social.impex;

import com.resolve.services.graph.social.vo.SocialImpexVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class ImpexSocial
{   
    public static void importGraph(SocialImpexVO socialImpex) throws Throwable
    {
        String folderLocation = ""; 
        
        try
        {
            folderLocation = socialImpex.getFolderLocation();
            
            if(StringUtils.isNotEmpty(folderLocation))
            {
//                ImportSocialGraph.importSocialGraph(folderLocation);
            }
        }
        catch(Throwable t)
        {
            Log.log.info("importGraph catched exception: ", t);
            throw t;
        }
    }
    
    public static void uninstallGraph(SocialImpexVO socialImpex) throws Throwable
    {
        String folderLocation = ""; 
        
        try
        {
            folderLocation = socialImpex.getFolderLocation();
            
            if(StringUtils.isNotEmpty(folderLocation))
            {
//                ImportSocialGraph.uninstallSocialGraph(folderLocation);
            }
        }
        catch(Throwable t)
        {
            Log.log.info("importGraph catched exception: ", t);
            throw t;
        }
    }
}