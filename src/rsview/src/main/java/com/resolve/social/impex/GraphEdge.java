/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.social.impex;

import java.util.Map;

public class GraphEdge
{   
    private String fromSysID;
    private String toSysID;
    private String relationType;
    private String operation;
    private String notificationValue;
    private Map<String, String> relProperty;
    
    public String getFromSysID()
    {
        return fromSysID;
    }
    
    public void setFromSysID(String fromSysID)
    {
        this.fromSysID = fromSysID;
    }
    
    public String getToSysID()
    {
        return toSysID;
    }
    
    public void setToSysID(String toSysID)
    {
        this.toSysID = toSysID;
    }
    
    public String getRelationType()
    {
        return relationType;
    }
    
    public void setRelationType(String relationType)
    {
        this.relationType = relationType;
    }
    
    public void setOperation(String operation)
    {
        this.operation = operation;
    }
    
    public String getOperation()
    {
        return this.operation;
    }
    
    public String getNotificationValue()
    {
        return this.notificationValue;
    }
    
    public void setNotificationValue(String notificationValue)
    {
        this.notificationValue = notificationValue;
    }
    
    public Map<String, String> getRelProperty()
    {
        return relProperty;
    }
    
    public void setRelProperty(Map<String, String> relProperty)
    {
        this.relProperty = relProperty;
    }
}