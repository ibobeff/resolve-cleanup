/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.Element;
import org.hibernate.query.Query;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import com.resolve.persistence.model.ResolveImpexGlide;
import com.resolve.persistence.model.ResolveImpexModule;
import com.resolve.persistence.model.ResolveImpexWiki;
import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.util.ExportUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.MAction;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.service.License;
import com.resolve.service.LicenseService;
import com.resolve.services.ImpexService;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.customtable.CustomFormUtil;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.ResolveImpexManifestVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.hibernate.vo.ResolveImpexWikiVO;
import com.resolve.services.impex.importmodule.ImportGlideCompsUtil;
import com.resolve.services.impex.importmodule.ImportValidationUtil;
import com.resolve.services.impex.importmodule.ImportWikiDocUtils;
import com.resolve.services.impex.importmodule.ModuleValidationUtil;
import com.resolve.services.impex.importmodule.TagValidationUtil;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexDefinitionDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.rr.util.ResolutionRoutingUtil;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;

public class ImportUtils
{
    protected String moduleName;
    protected String userName;
    protected ResolveImpexModuleVO impexModuleVO;
    protected ImportWikiDocUtils wikiDocUtils;
    protected ImportGlideCompsUtil glideCompUtil;
    protected List<String> excludeList;
    protected int totalImportCount;
    protected String moduleFileName;
    protected String manifestGraphJson;
    
    public static final String NEW = "New";
    public static final String EXIST_SAME = "Exist, Same";
    public static final String EXIST_DIFFERENT = "Exist, Different";
    private static String xmlModuleSysId;
    private static String dbModuleSysId;
    
    /**
     * Map key is module name and value is list of impexComponentDTO
     */
    private static Map<String, List<ImpexComponentDTO>> impexComponentMap = new HashMap<String, List<ImpexComponentDTO>>();
    
    /*
    protected Map<String, Boolean> fileProcessMap;
    protected StringBuffer validationBuffer;
    protected StringBuffer accessRightsTableBuffer;
    protected Set <String> customTableNames;
    protected Set<String> processedTables;
    */
    protected boolean validateImpex;
    protected boolean blockIndex;
    protected boolean hasReportsToImport;
    
    protected String zipFileName;
    
    protected String TEMP_FOLDER;
    protected String DESTINATION_FOLDER;
    protected String ZIP_FILE;
    protected String MODULE_FOLDER_LOCATION;
    protected String WIKI_FOLDER_LOCATION;
    protected String REPORT_FOLDER_LOCATION;
    protected String GLIDE_INSTALL_FOLDER_LOCATION;
    protected String GLIDE_UNINSTALL_FOLDER_LOCATION;
    
    public ImportUtils ()
    {
        xmlModuleSysId = null;
        dbModuleSysId = null;
        DESTINATION_FOLDER = RSContext.getResolveHome() + "rsexpert/";
        TEMP_FOLDER = DESTINATION_FOLDER + "temp/";
        
        String flag = PropertiesUtil.getPropertyString("impex.validation");
        validateImpex = false;
        if (StringUtils.isNotBlank(flag))
        {
            if (flag.equalsIgnoreCase("true"))
            {
                validateImpex = true;
            }
        }
    }
    
    public void importModule (String moduleName, List<String> excludeList, String userName, boolean blockIndex) throws Exception
    {
        this.moduleName = moduleName;
        this.excludeList = excludeList;
        
        this.userName = userName;
        this.blockIndex = blockIndex;
        
        try
        {
            validateModuleName();
            initImport();
            startImport();
           
            // Check for user count violations after import
            License license = MainBase.main.getLicenseService().getLicense();
            boolean userCountChekedOut = LicenseService.checkUsers(license, UserUtils.getNamedUserCount());
            MainBase.main.getLicenseService().setIsUserCountViolated(!userCountChekedOut);
        }
        catch(Throwable e)
        {
            ImpexUtil.logImpexMessage(String.format("Error while importing %s module.", moduleName), true);
            logImportOperation();
            if (e instanceof Error) {
                logErrorEvent(e.getMessage());
                ImpexUtil.cleanup(TEMP_FOLDER, null);
                if (MODULE_FOLDER_LOCATION != null)
                {
                    ImpexUtil.cleanup(MODULE_FOLDER_LOCATION, ZIP_FILE);
                }
                cleanManifestList();
            } else {
                try {
                    storeZipFileToDB(true);
                } catch (Throwable e1) {
                    Log.log.error(e1.getMessage(), e1);
                }
                Log.log.error(e.getMessage(), e);
                logErrorEvent(e.getMessage());
                                            
                ImpexManifestUtil.deleteManifest(moduleName, null, userName);
                cleanup(" ");
            }
        }
    }
    
    private void logErrorEvent(String message) throws Exception {
        if (Log.log.isTraceEnabled()) {
            Log.log.trace(String.format("ImportUtil.importModule(%s,%s,%s,%b) calling " +
                                        "ServiceHibernate.getLatestImpexEventByValue...", 
                                        moduleName, StringUtils.listToString(excludeList, ", "), userName, blockIndex));
        }
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        if (Log.log.isTraceEnabled()) {
            Log.log.trace(String.format("ImportUtil.importModule(%s,%s,%s,%b) " +
                                        "ServiceHibernate.getLatestImpexEventByValue returned Resolve Event [%s]", 
                                        moduleName, StringUtils.listToString(excludeList, ", "), userName, blockIndex,
                                        (eventVO != null ? eventVO : "null")));
        }
        
        if (eventVO == null) {
            eventVO = new ResolveEventVO();
        }
        eventVO.setUValue("IMPEX:" + this.moduleName + ",-1," + "Error importing: " + this.moduleName + ". Please refer to rsview.log for more details. Message: " + message);
        ServiceHibernate.insertResolveEvent(eventVO);
        if (Log.log.isTraceEnabled()) {
            Log.log.debug(String.format("ImportUtil.importModule(%s,%s,%s,%b) inserted Resolve Event [%s]",
                                        moduleName, StringUtils.listToString(excludeList, ", "), userName, blockIndex, 
                                        eventVO));
        }
    }
    
    private void validateModuleName() throws Exception
    {
        Log.log.debug("CHECK: ModuleName: " + moduleName);
        if (StringUtils.isBlank(moduleName))
        {
            moduleName = " ";
            throw new Exception("Module name cannot be empty.");
        }
    }
    
    private void initializeGlobalAttributes()
    {
        // MODULE_FOLDER_LOCATION = DESTINATION_FOLDER + moduleName + "/";
        WIKI_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.WIKI_FOLDER;
        REPORT_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.REPORT_FOLDER;
        GLIDE_INSTALL_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.GLIDE_FOLDER + "install/";
        GLIDE_UNINSTALL_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.GLIDE_FOLDER + "uninstall/";
        
    }//initializeGlobalAttributes
    
    private void initImport() throws Throwable
    {
//        setImportProgressInfo(0);
        
        if (moduleName.contains(".zip"))
        {
            zipFileName = moduleName;
        }
        else
        {
            zipFileName = getZipFileName(moduleName);
            
            if (StringUtils.isBlank(zipFileName))
            {
                zipFileName = moduleName + ".zip";
            }
        }
        
        Log.log.debug("CHECK: zipFileName before readFileFromDB(true): " + zipFileName );
        
        ImpexUtil.initImpexOperationStage("IMPORT", "(1/51) Read File from Database and Delete", 1);
        readFileFromDB(true);
        ImpexUtil.updateImpexCount("IMPORT");
        
        Log.log.debug("CHECK: zipFileName after readFileFromDB(true): " + zipFileName );
        
        if(StringUtils.isNotEmpty(this.zipFileName))
        {
            ZIP_FILE = RSContext.getResolveHome() + "rsexpert/" + this.zipFileName;
            
            Log.log.debug("CHECK: ResolveHome: " + RSContext.getResolveHome() + ", ZIP_FILE: " + ZIP_FILE);
            
            if(this.zipFileName.indexOf(".zip") > -1)
            {
                String localModuleName = moduleName;
                ImpexUtil.initImpexOperationStage("IMPORT", "(2/51) Evaluate Module Name", 1);
                evaluateModuleName(null, true);
                
                if (!localModuleName.equals(moduleName))
                {
                	ResolveEventUtil.updateResolveEvent("IMPORT", localModuleName, moduleName);
                    moduleFileName = localModuleName;
                }
            }
            
            //make sure that the module name is not null if the zip file name exist
            if(StringUtils.isEmpty(this.moduleName))
            {
                this.moduleName = this.zipFileName.substring(0, this.zipFileName.indexOf(".zip"));
            }
        }
        
        ImpexUtil.logImpexMessage("zipFileLocation : " + ZIP_FILE, false);
        
        setModuleFolderLocation(ZIP_FILE) ;
        
        initializeGlobalAttributes();
        
        wikiDocUtils = new ImportWikiDocUtils(moduleName, excludeList, WIKI_FOLDER_LOCATION, userName);
        glideCompUtil = new ImportGlideCompsUtil(moduleName, excludeList, MODULE_FOLDER_LOCATION, userName);
        glideCompUtil.setImpexValidationFlag(validateImpex);
    }
    
    public List<ImpexComponentDTO> populateImportManifest(String moduleName, String userName, boolean fromUI) throws Throwable
    {
        this.moduleName = moduleName;
        this.userName = userName;
        
        validateModuleName();
        
        if (moduleName.contains(".zip"))
        {
            zipFileName = moduleName;
            this.moduleName = this.moduleName.substring(0, moduleName.indexOf(".zip"));
        }
        else
        { 
            zipFileName = moduleName + ".zip";
        }
        readFileFromDB(false);
        
        ZIP_FILE = RSContext.getResolveHome() + "rsexpert/" + zipFileName;
        
        unzip(MODULE_FOLDER_LOCATION, DESTINATION_FOLDER + "/manifest", ZIP_FILE);
        
        this.moduleName = evaluateModuleName(zipFileName, false);
        
        String rootDir = setModuleFolderLocation(ZIP_FILE) ;
        
        initializeGlobalAttributes();
        
        List<ImpexComponentDTO> impexManifestList = new ArrayList<ImpexComponentDTO>();
        
        List<ImpexComponentDTO> glideList = new ArrayList<ImpexComponentDTO>();
        if (wikiDocUtils == null)
        {
            String wikiFolderLocation = RSContext.getResolveHome() + "rsexpert/manifest/" + rootDir + "wiki/";
            wikiDocUtils = new ImportWikiDocUtils(this.moduleName, excludeList, wikiFolderLocation, userName);
        }
        if (glideCompUtil == null)
        {
            String glideFolderLocation = ZIP_FILE = RSContext.getResolveHome() + "rsexpert/manifest/" + rootDir;
            glideCompUtil = new ImportGlideCompsUtil(this.moduleName, excludeList, glideFolderLocation, userName);
            glideCompUtil.setImpexValidationFlag(validateImpex);
        }
        
        glideList.addAll(prepareGlideCompList(fromUI));
        
        List<ImpexComponentDTO> wikiList = new ArrayList<ImpexComponentDTO>();
        wikiList.addAll(prepareWikiCompList(fromUI));
        
        impexManifestList.addAll(glideList);
        impexManifestList.addAll(wikiList);
        
        ImpexUtil.cleanup(DESTINATION_FOLDER + "manifest/", DESTINATION_FOLDER + zipFileName);
        cleanManifestList();
        
        return impexManifestList;
    }
    
    /*
     * This is synchronous operation from 6.3 onwards
     */
    public static boolean prepareManifest(String moduleName, String userName, boolean fromUI) throws Throwable
    {
        boolean result = false;
        
        int count = ImpexManifestUtil.getCount(moduleName, "IMPORT", userName);
        if (count <= 0)
        {
            /*
             * The manifest is not prepared yet.
             */
            Map<String, Object> params = new HashMap<String,Object>();
            params.put("USER", userName);
            params.put("MODULE_NAME", moduleName);
            params.put("OPERATION", "IMPORT");
            ImpexService.buildImportManifestSync(params, fromUI);
            
            count = ImpexManifestUtil.getCount(moduleName, "IMPORT", userName);
            
            Log.log.debug(String.format("Import Manifest Count After building Import Manifest is %d", count));
            
            if (count > 0) {
            	result = true;
            }
        }
        else
        {
            // The manifest records are there. Send the response 'true' to UI so that it could call an API to pull it.
            result = true;
        }
        ImpexUtil.finishOperation("MANIFEST", "Import", moduleName);
        return result;
    }
    
    public void buildImportManifest(String moduleName, String userName, boolean fromUI) throws Throwable
    {
        this.moduleName = moduleName;
        
        cleanManifestList();
        
        List<ImpexComponentDTO> compDTOList = populateImportManifest(moduleName, userName, fromUI);
        ImpexManifestUtil.deleteManifest(moduleName, null, userName);
        ImpexUtil.populateResolveManifestModel(compDTOList, moduleName, "IMPORT", userName);
    }
    
    public static Object manifestStatus(String moduleName, String userName) throws Exception 
    {
        boolean result = false;
        Object node = null;
        
        try
        {
            String json = "{\"module\": \""+ moduleName + "\",\"finished\": " + result + "}";
            
            ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("IMPEX-MANIFEST");
            if (eventVO != null)
            {
                String value = eventVO.getUValue();
                if (StringUtils.isNotBlank(value))
                {
                    String module = value.split(",")[0].split(":")[1];
                    if (StringUtils.isNotBlank(module) && moduleName.equals(module))
                    {
                        if (value.split(",").length == 2)
                        {
                            if (value.split(",")[1].equals("-2"))
                            {
                                result = true;
                                //ServiceHibernate.clearAllResolveEventByValue("IMPORT-MANIFEST");
                            }
                        }
                    }
                }
                json = "{\"module\": \""+ moduleName + "\",\"finished\": " + result + "}";
                node = new ObjectMapper().readValue(json, Map.class);
            }
            else
            {
                throw new Exception("Error while generation manifest. Please refer to rsview logs for more detail.");
            }
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
        
        return node;
    }
    
    public List<ResolveImpexManifestVO> getManifest(QueryDTO query, String moduleName, String userName) throws Exception
    {
        List<ResolveImpexManifestVO> manifestVoList = ImpexUtil.readManifest(query, moduleName, "IMPORT");
//        if (manifestVoList.isEmpty())
//        {
//            this.moduleName = moduleName;
//            readFileFromDB(false);
//            String zipFile = RSContext.getResolveHome() + "rsexpert/" + zipFileName;
//            
//            unzip(null, DESTINATION_FOLDER, zipFile);
//            
//            cleanManifestList();
//          
//            List<ImpexComponentDTO> compDTOList = populateImportManifest(moduleName, userName);
//            ImpexUtil.populateResolveManifestModel(compDTOList, moduleName, "IMPORT", userName);
//            manifestVoList = ImpexUtil.readManifest(query, moduleName, "IMPORT");
//        }
        
        return manifestVoList;
    }
    
    
    public void validateModule(String moduleName, String userName) throws Exception
    {
//        List<ImpexComponentDTO> componentDTOList = populateImportManifest(moduleName, userName);
        
        new ModuleValidationUtil(moduleName, userName).startValidation();
    }
    
    private void startImport() throws Throwable
    {
    	ImpexUtil.clearLogs();
        ImpexUtil.logImpexMessage("******** START IMPORT  ********", false);
        ImpexUtil.logImpexMessage("Module Name: " + moduleName, false);
        ImpexUtil.logImpexMessage("Product home : " + RSContext.getResolveHome(), false);
        ImpexUtil.logImpexMessage("destinationFolder : " + DESTINATION_FOLDER, false);
        
        ImpexUtil.initImpexOperationStage("IMPORT", "READ FILE FROM DB", 1);
        readFileFromDB(false);
        ImpexUtil.updateImpexCount("IMPORT");
        
        ImpexUtil.initImpexOperationStage("IMPORT", "(3/51) UNZIP", 1);
        unzip(MODULE_FOLDER_LOCATION, DESTINATION_FOLDER, ZIP_FILE);
        ImpexUtil.updateImpexCount("IMPORT");
        
        processDeleteXMLFiles();
        
        ImportValidationUtil validationUtil = new ImportValidationUtil(MODULE_FOLDER_LOCATION, GLIDE_INSTALL_FOLDER_LOCATION, userName);
        validationUtil.checkOlderVersionOfActionTask();
        // ActionTask validation.
        validationUtil.checkForSameSysIdButDiffName(excludeList, true);
        // Check whether all the referenced tables are present in the module.
        validationUtil.checkRefTables();
        // Check same named AB. If found, delete the metadata.
        validationUtil.checkForSameNamedAB();
        // Check for same named User. If found, delete user-group rel and user-role rel.
        Map<String, Collection<UserGroupRel>> delUsrGrpRelsByUsr = 
                                                    new HashMap<String, Collection<UserGroupRel>>();
        Map<String, Collection<UserRoleRel>> delUsrRoleRelsByUsr = 
                                                    new HashMap<String, Collection<UserRoleRel>>();
        Set<String> newUserImportFileNames = validationUtil.checkForSameNamedUser(delUsrGrpRelsByUsr, delUsrRoleRelsByUsr);
        //Validate social component names. If not valid, don't import and log the message.
        validationUtil.validateSocialCompsName();
        
        //check if we need to import reports
        setFlagToImportReports();
        
        List<ImpexComponentDTO> definitionList = prepareImportDefinitionList();
//        setImportProgressInfo(definitionList.size() + reportCount);
        impexComponentMap.put(moduleName, definitionList);

        processAlterTableXMLs(true);
        
        if (validateImpex)
        {
            checkForTheSameModuleName(FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION));
            
            validationUtil.validateImportModule();
            
            TagValidationUtil tagValidation = new TagValidationUtil(MODULE_FOLDER_LOCATION);
            tagValidation.validateTags();
        }
        
        importWikiDocs();
        
        importGlideComps(delUsrGrpRelsByUsr, delUsrRoleRelsByUsr, newUserImportFileNames);
        
        importCatalogs();
        
        ImpexUtil.initImpexOperationStage("IMPORT", "(43/51) Social Glide Components", 1);
        importSocialComponents();
        ImpexUtil.updateImpexCount("IMPORT");
        
        processAlterTableXMLs(false);
        
        postImportOperations(blockIndex);
        
        storeZipFileToDB(true);
        
        logImportOperation();
        
        String docName = checkWikiDocToBeShown(); // this API also checks for the System Script to be executed.
        
        importReports();
        
        cleanup(docName);
    }
    
    // Utility API
    
    private List<ImpexComponentDTO> prepareImportDefinitionList() throws Exception
    {
        List<ImpexComponentDTO> resultList = new ArrayList<ImpexComponentDTO>();
        
        resultList.addAll(prepareWikiCompList(false));
        
        resultList.addAll(prepareGlideCompList(false));
        
        return resultList;
    }
    
    private List<ImpexComponentDTO> prepareWikiCompList(boolean fromUI) throws Exception
    {
        return wikiDocUtils.getWikiDocsToImport(fromUI);
    }
    
    @Deprecated
    private int setFlagToImportReports()
    {
        int count = 0;
        File reportsFolderLocation = FileUtils.getFile(REPORT_FOLDER_LOCATION);
        if(reportsFolderLocation.exists() && reportsFolderLocation.isDirectory())
        {
            count = reportsFolderLocation.list().length;
            hasReportsToImport = true;
            try
            {
                List<? extends Object> list = ImpexUtil.executeQuery("ResolveRegistration", "UType='RSVIEW' and UStatus='ACTIVE'", "sys_id");
                if (list != null)
                {
                    count = count * list.size();
                }
            }
            catch(Exception e)
            {
                // Do nothing here
            }
        }
        
        return count;
    }
    
    private void importGlideComps(Map<String, Collection<UserGroupRel>> delUsrGrpRelsByUsr,
                                  Map<String, Collection<UserRoleRel>> delUsrRoleRelsByUsr,
                                  Set<String> newUserImportFileNames) throws Exception
    {
        glideCompUtil.importGlideComps(delUsrGrpRelsByUsr, delUsrRoleRelsByUsr, newUserImportFileNames);
    }
    
    private void importSocialComponents()
    {
        glideCompUtil.importSocialComponents(MODULE_FOLDER_LOCATION);
    }
    
    private void importWikiDocs() throws Exception
    {
        wikiDocUtils.importWikiDocs();
    }
    
    private void importCatalogs()
    {
        glideCompUtil.importCatalogs(MODULE_FOLDER_LOCATION);
    }
    
    private List<ImpexComponentDTO> prepareGlideCompList(boolean fromUI) throws Exception
    {
        return glideCompUtil.getGlideCompsToImport(fromUI);
    }
    
    private String getZipFileName(String moduleName)
    {
        String zipFileName = null;
        QueryDTO query = new QueryDTO();
        query.setSelectColumns("UZipFileName");
        query.setModelName("ResolveImpexModule");
        
        try
        {
            String whereClause = " UName = '" + moduleName + "' and UDirty = true order by sysUpdatedOn desc";
            query.setWhereClause(whereClause);
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
            if (list != null && list.size() == 0)
            {
                whereClause = " UName = '" + moduleName + "' and (UDirty = false OR UDirty is null) order by sysUpdatedOn desc";
                query.setWhereClause(whereClause);
                list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
            }
            
            if (list != null && list.size() > 0)
            {
                zipFileName = (String)list.get(0);
            }
        }
        catch (Exception e)
        {
            // Need not do anything here.
        }
        
        if (StringUtils.isBlank(zipFileName))
        {
            zipFileName = moduleName + ".zip";
        }
        
        return zipFileName;
    }//getZipFileName
    
    public String evaluateModuleName(String fileName, boolean validateModuleName) throws Throwable
    {
        if (fileName != null)
        {
            this.zipFileName = fileName;
            TEMP_FOLDER = DESTINATION_FOLDER + "temp/";
            ZIP_FILE = DESTINATION_FOLDER + fileName;
        }
        String moduleName = null;
        String foldername = zipFileName.substring(0, zipFileName.indexOf(".zip"));
       
        // extract resolve_impex_module file
        ImpexUtil.extractFile(TEMP_FOLDER, TEMP_FOLDER, ZIP_FILE, "resolve_impex_");
        
        //get the name
        File tempFile = FileUtils.getFile(TEMP_FOLDER);
        if (tempFile.list() == null)
        {
            File zipFile = FileUtils.getFile(ZIP_FILE);
            zipFile.delete();
            ImpexUtil.logImpexMessage("Content error: Module name could not be evaluated. Please verify zip file and try again.", true);
            throw new Exception("Content error: Module name could not be evaluated. Please verify zip file and try again.");
        }
        else
        {
            foldername = tempFile.list()[0];
            this.moduleName = moduleName = readModuleNameFromTemp(TEMP_FOLDER + foldername + "/", validateModuleName);
        }
        
        //storeZipFileToDB(false);
        
        File tempFolder = FileUtils.getFile(TEMP_FOLDER);
        if (tempFolder != null && tempFolder.isDirectory())
        {
            FileUtils.deleteDirectory(tempFolder);
        }
        
        return moduleName;
    }//evaluateModuleName
    
    private String readModuleNameFromTemp(String fileLocation, boolean validateModuleName)  throws Throwable 
    {
        String moduleName = null;
        
//        String folderLocationForPackageXML = fileLocation + ImpexUtil.WIKI_FOLDER;
        String folderLocationForPackageXML = fileLocation + ImpexUtil.GLIDE_FOLDER + "install/";
        
        File dir = new File (folderLocationForPackageXML);
        String moduleFileName = dir.list(new PrefixFileFilter(ImpexUtil.DefaultPackageFileName))[0];
        
        //make sure that the file exist
        File packageXMLFile = FileUtils.getFile(folderLocationForPackageXML, moduleFileName);
        if (!packageXMLFile.exists())
        {
            ImpexUtil.logImpexMessage("package.xml does not exist in " + folderLocationForPackageXML, true);
            throw new Exception("package.xml does not exist in " + folderLocationForPackageXML + " folder.");
        }
        
      //read and get the document object
        Document documentPackageXML = null;
        try
        {
            documentPackageXML = ImpexUtil.fromXml(FileUtils.readFileToString(packageXMLFile, ControllerUtil.ENCODING));
            if (documentPackageXML == null)
            {
                throw new Exception("Cannot read package.xml file");
            }
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }

        Element docFiles = documentPackageXML.getRootElement();
        Element moduleElement = docFiles.element("resolve_impex_module");
        Element childElem = moduleElement.element("u_name");
        if(childElem != null)
        {
            moduleName = childElem.getText();
        }
        
        childElem = moduleElement.element("manifest_graph");
        if (childElem != null)
        {
            manifestGraphJson = childElem.getText();
        }
        
        if (validateModuleName)
        {
            List<String> ids = new ArrayList<String>();
            xmlModuleSysId = moduleElement.element("sys_id").getText();
            checkForTheSameModuleName(dir);
            ids.add(xmlModuleSysId);
            try
            {
                //ResolveImpexModule rim = ImpexUtil.getImpexModel(null, moduleName, false);
                String sysId = ImpexUtil.getModuleSysId(moduleName);
                if (StringUtils.isNotBlank(sysId))
                {
                    ids.add(sysId);
                }
                
                // Delete module with the same sysId or moduleName, if already present.
                ImpexUtil.deleteImpexModelByIDS(ids, userName);
                
                File[] files = dir.listFiles();
                for (File file : files)
                {
                    // import only the definition.
                    ExportUtil.importFile(file);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return moduleName;
        
    }//readModuleNameFromTemp
    
    public void checkForTheSameModuleName(File dir)
    {
        /*
         * If there's already a module present with the same name,
         * check if the sys_id is the same, otehrwise replace xml module sys_id by system sys_id.
         */
        Log.log.debug("Checking module: " + moduleName + " if already exist.");
        Log.log.debug("XML sys_id: " + xmlModuleSysId);
        
        if (StringUtils.isNotBlank(dbModuleSysId))
        {
            Log.log.debug("dbModuleSysId: " + dbModuleSysId);
            if (!dbModuleSysId.equals(xmlModuleSysId))
            {
                WildcardFileFilter fileFilter = new WildcardFileFilter("resolve_impex_*");
                Collection<File> fileCollection = FileUtils.listFiles(dir, fileFilter, TrueFileFilter.INSTANCE);
                
                if (CollectionUtils.isNotEmpty(fileCollection)) {
	                Log.log.debug("Replacing sys_id's for module: " + moduleName + " from: " + xmlModuleSysId + " to: " + dbModuleSysId);
	                try {
	                	ImpexUtil.initImpexOperationStage("IMPORT", "(18/51) Replace Sys Ids in Module to Import", 
	                									   fileCollection.size());
						Log.log.debug("Replacing sys_id's for module: " + moduleName + " from: " + xmlModuleSysId + " to: " + dbModuleSysId);
		                for (File file : fileCollection)
		                {
		                	ImpexUtil.updateImpexCount("IMPORT");
		                    String content;
		                    try
		                    {
		                        content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
		                        if (content.contains(xmlModuleSysId))
		                        {
		                            content = content.replace(xmlModuleSysId , dbModuleSysId);
		                            FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
		                        }
		                    }
		                    catch (IOException e)
		                    {
		                        Log.log.error("Error in checkForTheSameModuleName", e);
		                    }
		                }
	                } catch (Exception e) {
	                	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
	        					  	  e.getMessage() : "Failed to Replace Sys Ids in Module to Import.", e);
	                	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
	  								  		  	  e.getMessage() : 
	  								  		  	  "Failed to Replace Sys Ids in Module to Import. Please check RSView " +
	  								  			  "logs for details",
	  								  			  true);
	                }
                }
            }
        }
        else
        {
            Log.log.debug("Module: " + moduleName + " does not exist.");
        }
    }
    
    private void readFileFromDB(boolean deleteFile)
    {
        ResolveImpexModule rim = ImpexUtil.getImpexModel(null, moduleName, true);
        if (rim == null)
        {
            rim = ImpexUtil.getImpexModel(null, moduleName, false);
            if (rim != null) dbModuleSysId = rim.getSys_id();
        }
        else
        {
            ResolveImpexModule tmpRim = ImpexUtil.getImpexModel(null, moduleName, false);
            if (tmpRim != null) dbModuleSysId = tmpRim.getSys_id();
        }
        
        if (rim != null)
        {
            // Log.log.debug("CHECK: rim.getUZipFileName(): " + rim.getUZipFileName());
//            if (StringUtils.isNotBlank(rim.getUZipFileName()))
//            {
//                zipFileName = rim.getUZipFileName();
//            }
            
            File zipFileContent = FileUtils.getFile(DESTINATION_FOLDER + zipFileName);
            ResolveImpexModuleVO moduleVO = ImpexUtil.getImpexModelZipContent(rim.getSys_id(), null, userName);
            if (moduleVO.getUZipFileContent() != null && !zipFileContent.exists())
            {
                try
                {
                    FileUtils.writeByteArrayToFile(zipFileContent, moduleVO.getUZipFileContent());
                }
                catch (IOException e)
                {
                    Log.log.error("Could not read content of the zip file:" + zipFileName, e);
                }
            }
            
            
            if (deleteFile)
            {
                ImpexUtil.deleteImpexDefinitionByName(moduleName, true, userName);
                
                // ImpexUtil.deleteImpexModelByIDS(Arrays.asList(rim.getSys_id()), userName);
                // HibernateUtil.beginTransaction();
                // ResolveImpexModuleDAO dao = HibernateUtil.getDAOFactory().getResolveImpexModuleDAO();
                // dao.delete(rim);
            }
            else
            {
                impexModuleVO = rim.doGetVO();
            }
        }
        
        // Log.log.debug("CHECK: In readFileFromDB. zipFileName: " + zipFileName );
    }
    
    public Map<String, String> storeZipFileToDB(String zipFileName, boolean isNew, String userName, boolean fromUI) throws Throwable
    {
        String moduleName = null;
        try {
            moduleName = evaluateModuleName(zipFileName, false);
        } catch (Exception e) {
            ImpexUtil.updateImpexCount("UPLOAD");
            throw e;
        }
        Map<String, String> messageMap = new HashMap<>();
        if (isNew && isModulePresent(moduleName))
        {
            /*
             *Same named module already present on the system.
             * 1. Save the current uploaded zip content with '<moduleName> (1)' as module name with dirty flag on.
             * 2. Return and error message.
             */
            int nextNumber = getNextNumberForTempModule(moduleName.toLowerCase());
            String newModuleName = moduleName + ImpexEnum.DASH.getValue() + nextNumber;
            saveTempModule(zipFileName, newModuleName);
            messageMap.put(ImpexEnum.MESSAGE.getValue(), String.format(ERR.IMPEX100.getMessage(), moduleName, newModuleName + ImpexEnum.DASH.getValue() + nextNumber));
            messageMap.put(ImpexEnum.ERROR_CODE.getValue(), ERR.IMPEX100.getCode());
            messageMap.put(ImpexEnum.NAME.getValue(), moduleName);
        }
        else
        {
           /*
             * Save the current uploaded zip content with the module name with dirty flag on
             * Return manifestGraphJson
             
             * Before storing the module zip file to DB for the first time,
             * clean any manifest and any previously stored 'dirty' module.
             */
            ImpexManifestUtil.deleteManifest(moduleName, null, userName);
            ImpexUtil.deleteImpexDefinitionByName(moduleName, true, userName);
           
            saveTempModule(zipFileName, moduleName);
        }
        if (isNew) {
        if (StringUtils.isBlank(manifestGraphJson))
        {
            /*
             * An older zip file is uploaded to import. Start the old manifest generation API and inform
             * UI by a boolean flag whether to start polling or pull already generated manifest records. 
             */
                populateImportManifest(moduleName, userName, fromUI);
                messageMap.put(ImpexEnum.READ_MANIFEST_RECORDS.getValue(), "true");
            messageMap.put(ImpexEnum.ERROR_CODE.getValue(), ERR.IMPEX102.getCode());
            messageMap.put(ImpexEnum.MESSAGE.getValue(), String.format(ERR.IMPEX102.getMessage(),zipFileName));
        }
        else
        {
            manifestGraphJson = verifyImport(manifestGraphJson, userName);
        }
        }
        messageMap.put(ImpexEnum.NAME.getValue(), moduleName);
        messageMap.put(ImpexEnum.MANIFEST_GRAPH.getValue(), manifestGraphJson);
        
        return messageMap;
    }
    
    private int getNextNumberForTempModule(String moduleName)
    {
        int nextNumber = 0;
        String sql = "select CONVERT(REPLACE (lower(u_name), '"+ moduleName + ImpexEnum.DASH.getValue() + "', ''),UNSIGNED INTEGER) as num  from resolve_impex_module where lower(u_name) REGEXP ? order by num desc";
        if (HibernateUtil.isOracle())
        {
            sql = "select replace(lower(U_NAME), '" + moduleName + ImpexEnum.DASH.getValue() + "', '') as num from resolve_impex_module where  REGEXP_LIKE (lower(U_NAME), ?) order by num desc";
        }
        ResultSet rs = null;
        SQLConnection connection = null;
        try
        {
            connection = SQL.getConnection();
            PreparedStatement ps  = connection.prepareStatement(sql);
            ps.setString(1, "^" + moduleName + ImpexEnum.DASH.getValue() + "[0-9]+$");
            rs = ps.executeQuery();
            while (rs.next())
            {
                nextNumber = rs.getInt(1);
                break;
            }
        }
        catch(Exception e)
        {
            Log.log.error("Error while getting next number for temporary module.", e);
        }
        finally
        {
            SQL.close(connection);
        }
       
        return ++nextNumber;
    }
    
    private void saveTempModule(String zipFileName, String moduleName) throws Throwable
    {
        ResolveImpexModuleVO rimVO = new ResolveImpexModuleVO();
        rimVO.setUName(moduleName);
        rimVO.setUDirty(true);
       
        rimVO.setUZipFileName(zipFileName);
        
        //update the zipfile name and save
        File zipFileContent = FileUtils.getFile(DESTINATION_FOLDER + zipFileName);
        if(zipFileContent.isFile())
        {
            try
            {
                rimVO.setUZipFileContent(FileUtils.readFileToByteArray(zipFileContent));
                rimVO.setSysUpdatedBy("system");
                rimVO.setManifestGraph(manifestGraphJson);
                ImpexUtil.saveResolveImpexModule(rimVO, userName, false, "IMPORT");
            }
            catch (IOException e)
            {
                Log.log.error("Could not read content of the zip file:" + zipFileName, e);
            }
        }
        
        zipFileContent.delete();
    }
    
    @SuppressWarnings("rawtypes")
    private boolean isModulePresent(String moduleName)
    {
        boolean result = false;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (boolean) HibernateProxy.execute(() -> {
	            Query query = HibernateUtil.getCurrentSession().             
	                createQuery("select 1 from ResolveImpexModule where lower(UName) = :name and (UDirty = false OR UDirty is null)" );
	            query.setParameter("name", moduleName.toLowerCase() );
	            return (query.uniqueResult() != null);
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error while checking if module is present", e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return result;
    }
    
    public void storeZipFileToDBKeepZip(String zipFileName, String userName) throws Throwable
    {
        String moduleName = zipFileName.substring(0, zipFileName.indexOf(".zip"));
        
        ResolveImpexModuleVO rimVO = new ResolveImpexModuleVO(); 
        rimVO.setUName(moduleName);
        rimVO.setUDirty(true);
        
        rimVO.setUZipFileName(zipFileName);
        
        //update the zipfile name and save
        File zipFileContent = FileUtils.getFile(DESTINATION_FOLDER + zipFileName);
        if(zipFileContent.isFile())
        {
            try
            {
                rimVO.setUZipFileContent(FileUtils.readFileToByteArray(zipFileContent));
                rimVO.setSysUpdatedBy("system");
                ImpexUtil.saveResolveImpexModule(rimVO, userName, false, "IMPORT");
            }
            catch (IOException e)
            {
                Log.log.error("Could not read content of the zip file:" + zipFileName, e);
            }
        }
    }
    
    public static void unzip(String folderLocation, String destinationFolder, String zipFileName) throws Exception 
    {
        try
        {
            //delete the destination folder if it exist
            if (folderLocation != null)
            {
                File moduleFolder = FileUtils.getFile(folderLocation);
                if(moduleFolder != null && moduleFolder.isDirectory() && moduleFolder.exists())
                {
                    Log.log.info("Deleting  existing folder -->" + moduleFolder.getName());
                    FileUtils.deleteDirectory(moduleFolder);
                }
            }
            
            File file = FileUtils.getFile(zipFileName);
            FileUtils.unzip(file, FileUtils.getFile(destinationFolder));
        }
        catch(Exception e)
        {
            Log.log.error("Error: While unzipping module : "+ zipFileName, e);
            throw new Exception(e);
        }
    }//unzip
    
    protected String setModuleFolderLocation(String zipFilePath) throws IOException
    {
        ZipFile zipFile = new ZipFile(FileUtils.getFile(zipFilePath)); 
        Enumeration<? extends ZipEntry> e = zipFile.entries();
        String rootDir = "";
        if (e.hasMoreElements())
        {
            ZipEntry zipEntry = e.nextElement();
            rootDir = zipEntry.getName().split("/")[0] + "/";
        }
        zipFile.close();
        
        MODULE_FOLDER_LOCATION = DESTINATION_FOLDER + rootDir;
        
        return rootDir;
    }
    
    protected void processAlterTableXMLs(boolean before) throws Exception
    {
        List<File> files = new ArrayList<File>();
        File glideFoler = new File (GLIDE_INSTALL_FOLDER_LOCATION);
        if (glideFoler.isDirectory())
        {
            WildcardFileFilter filter = null;
            if (before)
            {
                filter = new WildcardFileFilter("*_before_*");
            }
            else
            {
                filter = new WildcardFileFilter("*_after_*");
            }
            
            files.addAll(FileUtils.listFiles(glideFoler,filter, null));
            
            String stageIndex = before ? "(17/51)" : "(44/51)";
            
            processAlterCommand(files, stageIndex);
        }                   
    }
    
    protected void postImportOperations(boolean blockIndex)
    {
        if (!blockIndex)
        {
            // indexWikiDocuments();
            
            //as we don;t have the handle to ActionTask, we will index all action task if there was any
            // indexAllActionTask();
        }
        
        checkForOlderModuleAndConvert();
        
        try {
			ImpexUtil.initImpexOperationStage("IMPORT", "(47/51) Replace Wiki SysIds in Resolution Routing", 1);
			replaceWikiSysIdInRR();
			ImpexUtil.updateImpexCount("IMPORT");
		} catch (Exception e) {
			Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
												e.getMessage() : "Failed to Replace Wiki SysIds in Resolution Routing.", e);
			ImpexUtil.logImpexMessage(
				StringUtils.isNotBlank(
					e.getMessage()) ? 
					e.getMessage() : 
					"Failed to Replace Wiki SysIds in Resolution Routing. Please check RSView logs for details", true);
		}
        
        checkImportForRRSchema();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected void processAlterCommand(List<File> files, String stageIndex) throws Exception
    {
        if (!files.isEmpty())
        {
            Document docPackageXML = null;
            try {
            	ImpexUtil.initImpexOperationStage("IMPORT", String.format("%s Process Alter Table Commands", stageIndex), 
            															   files.size());
	            for (File file : files)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                docPackageXML = new XDoc(file).getDocument();
	                Element docFiles = docPackageXML.getRootElement();
	                String tableName = docFiles.attribute("table").getText();
	
	                Element rootFieldElem = docFiles.element(tableName);
	                if(rootFieldElem != null)
	                {
	                    try
	                    {
	                        List<Element> fieldElems = rootFieldElem.elements();
	                       
	                        HibernateProxy.execute(() -> {
	                        	for (Element elem : fieldElems)
	 	                        {
	 	                            String columnName = elem.getQName().getName();
	 	                            String value = elem.attribute("size").getValue();
	 	                            String defaultValue = elem.getText();
	 	                            
	 	                            StringBuilder alterBuilder = new StringBuilder();
	 	                            Map<Integer, Object> stmtParams = new HashMap<Integer, Object>();
	 	                            
	 	                            //alterBuilder.append("ALTER TABLE ").append(tableName).append(" MODIFY ").append(columnName).append(" VARCHAR(").append(value).append(") ");
	 	                            alterBuilder.append("ALTER TABLE ").append("?").append(" MODIFY ").append("?").append(" VARCHAR(").append("?").append(") ");
	 	                            
	 	                            stmtParams.put(Integer.valueOf(1), tableName);
	 	                            stmtParams.put(Integer.valueOf(2), columnName);
	 	                            stmtParams.put(Integer.valueOf(3), Integer.parseInt(value));
	 	                            
	 	                            if (StringUtils.isNotBlank(defaultValue))
	 	                            {
	 	                                //alterBuilder.append("DEFAULT '").append(defaultValue).append("'");
	 	                                alterBuilder.append("DEFAULT ?");
	 	                                stmtParams.put(Integer.valueOf(4), defaultValue);
	 	                            }
	 	                            
	 	                            //HibernateUtil.createSQLQuery(alterBuilder.toString()).executeUpdate();
	 	                            
	 	                            Query sqlQuery = HibernateUtil.createSQLQuery(alterBuilder.toString());
	 	                            
	 	                            sqlQuery.setParameter(1, (String)stmtParams.get(Integer.valueOf(1)));
	 	                            sqlQuery.setParameter(2, (String)stmtParams.get(Integer.valueOf(2)));
	 	                            sqlQuery.setParameter(3, (Integer)stmtParams.get(Integer.valueOf(3)), IntegerType.INSTANCE);
	 	                            
	 	                            if (stmtParams.containsKey(Integer.valueOf(4)))
	 	                            {
	 	                                sqlQuery.setParameter(4, (String)stmtParams.get(Integer.valueOf(4)), StringType.INSTANCE);
	 	                            }
	 	                            
	 	                            int result = sqlQuery.executeUpdate();
	 	                            
	 	                            Log.log.info("Executed: " + sqlQuery.getQueryString() + ", returned " + result);
	 	                        }
	                        });
	                    }
	                    catch (Exception e)
	                    {
	                        Log.log.debug("ERROR IN ALTER EXECUTION - FILE: " + file.getName(), e);
	                        throw new Exception("ERROR IN ALTER EXECUTION - FILE: " + file.getName());
	                    }
	                }
	            }
            } catch (Exception e) {
            	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
            				  e.getMessage() : "Failed to Process Alter Table Commands.", e);
            	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
  								  		  e.getMessage() : 
  								  		  "Failed to Process Alter Table Commands. Please check RSView logs for details", true);
            }
        }
    }
    
    private void storeZipFileToDB(boolean rezipIt) throws Throwable
    {
        ResolveImpexModule rim = ImpexUtil.getImpexModel(null, moduleName, false);
        if (rim == null)
        {
            rim = new ResolveImpexModule();
            rim.setUName(moduleName);
        }
        
        rim.setUZipFileName(zipFileName);
        
        //update the zipfile name and save
        File zipFileContent = null;
//        if (rezipIt)
//        {
//            Compress compress = new Compress();
//            compress.zip(FileUtils.getFile(MODULE_FOLDER_LOCATION), FileUtils.getFile(DESTINATION_FOLDER + zipFileName));
//            zipFileContent = FileUtils.getFile(DESTINATION_FOLDER + zipFileName);
//        }
//        else
        {
            zipFileContent = FileUtils.getFile(DESTINATION_FOLDER + zipFileName);
        }
        
        if(zipFileContent.isFile())
        {
            try
            {
                ResolveImpexModuleVO rimVO = rim.doGetVO();
                rimVO.setUZipFileContent(FileUtils.readFileToByteArray(zipFileContent));
                rimVO.setSysUpdatedBy(userName);
                impexModuleVO = ImpexUtil.saveResolveImpexModule(rimVO, userName, true, "IMPORT");
            }
            catch (IOException e)
            {
                Log.log.error("Could not read content of the zip file:" + zipFileName, e);
            }
        }
    }
    
    @Deprecated
    private void importReports()
    {
        if(hasReportsToImport)
        {
            //send a jms message to import reports
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.IMPEX_PARAM_MODULENAMES, moduleName);
            params.put(Constants.HTTP_REQUEST_USERNAME, userName);
            
            // report imports jar files, so all rsviews needs to be updated.
//            Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "MAction.importReports", params);
            MAction.importReports(params);
        }
    }
    
    private void logImportOperation()
    {
        ImpexUtil.logImpexMessage("******** END IMPORT ********", false);
        ImpexUtil.logImpexOperation(impexModuleVO, "IMPORT", userName);
    }
    
    private String checkWikiDocToBeShown()
    {
        String docName = " ";
        ResolveImpexModule rim = ImpexUtil.getImpexModel(null, moduleName, false);
        if (rim != null)
        {
            CheckScriptToBeExecuted(rim);
            
            String name = rim.getUForwardWikiDocument();
            if (StringUtils.isNotBlank(name))
            {
                docName = name;
            }
        }
        
        return docName;
    }
    
    private void CheckScriptToBeExecuted(ResolveImpexModule rim)
    {
        String scriptName = rim.getUScriptName();
        if (StringUtils.isNotBlank(scriptName))
        {
            Log.log.debug("Executing system script: " + scriptName);
            Map<String, Object> params = new HashMap<String, Object>();
            ServiceHibernate.executeScript(scriptName, params);
        }
    }
    
    private void checkForOlderModuleAndConvert()
    {
        /*
         * Check if the package got imported is from old resolve. If yes, make it compatible with 3.5.x
         */
        
        ResolveImpexModuleVO impexModuleVO = ImpexUtil.getImpexModelVOWithReferences(null, moduleName, userName);
        if (impexModuleVO != null)
        {
            Collection<ResolveImpexGlideVO> impexGlideColl = impexModuleVO.getResolveImpexGlides();
            if (CollectionUtils.isNotEmpty(impexGlideColl))
            {
            	try {
            		ImpexUtil.initImpexOperationStage("IMPORT", "(45/51) Converting Pre 3.5.x Packages to Post 3.5.x", 
            										   impexGlideColl.size());
	                for (ResolveImpexGlideVO glideVO : impexGlideColl)
	                {
	                	ImpexUtil.updateImpexCount("IMPORT");
	                    boolean doFormat = false;
	                    String description = glideVO.getUDescription();
	                    if (StringUtils.isBlank(description))
	                    {
	                        doFormat = true;
	                    }
	                    else
	                    {
	                        /*
	                         * check if description contains json starting char "{". If it doesn't,
	                         * it's an old format, so convert it.
	                         */
	                        if (!description.contains("{"))
	                        {
	                            doFormat = true;
	                        }
	                    }
	                    
	                    if (doFormat)
	                    {
	                        StringBuilder desc = new StringBuilder();
	                        ImpexDefinitionDTO definitionDTO = new ImpexDefinitionDTO();
	                        
	                        if (glideVO.getUType().equalsIgnoreCase("ACTIONTASK_BY_TAG"))
	                        {
	                            // we don't support this category from Resolve 5 onwards.
	                            continue;
	                        }
	                        if (glideVO.getUType().toLowerCase().contains("actiontask"))
	                        {
	                            if (StringUtils.isBlank(glideVO.getUName()))
	                            {
	                                definitionDTO.setType("actionTaskModule");
	                                String namespace = glideVO.getUModule();
	                                if (StringUtils.isBlank(namespace))
	                                {
	                                    namespace = glideVO.getUActiontaskModule();
	                                }
	                                desc.append("{\"unamespace\":\"").append(namespace).append("\"");
	                                desc.append(",\"usummary\":\"\"} ");
	                            }
	                            else
	                            {
	                                definitionDTO.setType("actionTask");
	                                desc.append("{\"unamespace\":\"").append(glideVO.getUModule()).append("\"");
	                                desc.append(",\"uname\":\"").append(glideVO.getUName()).append("\"} ");
	                            }
	                        }
	                        
	                        else if (glideVO.getUType().equalsIgnoreCase("SOCIAL"))
	                        {
	                            definitionDTO.setType(glideVO.getUName().toLowerCase());
	                            glideVO.setUName(null);
	                            desc.append("{\"u_display_name\":\"").append(glideVO.getUModule()).append("\"");
	                            desc.append(",\"u_description\":\"\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("METAFORMVIEW"))
	                        {
	                            definitionDTO.setType("form");
	                            desc.append("{\"uformName\":\"").append(glideVO.getUModule()).append("\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("DB"))
	                        {
	                            definitionDTO.setType("customDB");
	                            desc.append("{\"utableName\":\"").append(glideVO.getUModule()).append("\"");
	                            String query = StringUtils.isBlank(glideVO.getUName()) ? "" : glideVO.getUName();
	                            desc.append(",\"uquery\":\"").append(query).append("\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("CUSTOMTABLE"))
	                        {
	                            definitionDTO.setType("customTable");
	                            desc.append("{\"uname\":\"").append(glideVO.getUModule()).append("\"} ");
	                            
	                            if (glideVO.getUName().contains("DATA"))
	                            {
	                                ImpexOptionsDTO impexOptions = new ImpexOptionsDTO();
	                                impexOptions.setTableData(true);
	                                try
	                                {
	                                    String impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
	                                    glideVO.setUOptions(impexOptionJson);
	                                }
	                                catch(Exception e)
	                                {
	                                    Log.log.error("Could not parse default ImpexOption for ResolveImpexGlide", e);
	                                }
	                            }
	                            glideVO.setUName(null);
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("TAG"))
	                        {
	                            String tagName = StringUtils.isBlank(glideVO.getUName()) ? "-Old Definition-" : glideVO.getUName();
	                            desc.append("{\"name\":\"").append(tagName).append("\"} ");
	                            definitionDTO.setType("tag");
	                            if (desc.toString().contains("-Old Definition-"))
	                            {
	                                ImpexUtil.logImpexMessage("WARNING: Old Tag glide definition. SysId: "
	                                                + glideVO.getSys_id() + ". Please recreate this definition.", true);
	                            }
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("MENUSET"))
	                        {
	                            desc.append("{\"name\":\"").append(glideVO.getUModule()).append("\"} ");
	                            definitionDTO.setType("menuSet");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("MENUSECTION"))
	                        {
	                            definitionDTO.setType("menuDefinition");
	                            SysAppApplication app = ImpexUtil.findMenuDefinition(glideVO.getUModule(), userName);
	                            if (app != null)
	                            {
	                                glideVO.setUName(app.getName());
	                                glideVO.setUModule(app.getTitle());
	                            }
	                            else
	                            {
	                                glideVO.setUName(glideVO.getUModule());
	                                glideVO.setUModule("MISSING");
	                            }
	                            desc.append("{\"title\":\"").append(glideVO.getUModule()).append("\"");
	                            desc.append(",\"name\":\"").append(glideVO.getUName()).append("\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("MENUITEM"))
	                        {
	                            definitionDTO.setType("menuItem");
	                            String name = glideVO.getUName();
	                            if (StringUtils.isBlank(name) || name.equals("&nbsp"))
	                            {
	                                glideVO.setUName(null);
	                            }
	                            
	                            String menuItemTitle = glideVO.getUModule();
	                            if (menuItemTitle.contains("::"))
	                            {
	                                menuItemTitle = menuItemTitle.split("::")[1];
	                            }
	                            desc.append("{\"title\":\"").append(menuItemTitle).append("\"");
	                            String menuDefTitle = ImpexUtil.formatMenuItemDef(glideVO.getUModule());
	                            if (menuDefTitle != null)
	                            {
	                                String g = StringUtils.isBlank(glideVO.getUName()) ? "" : glideVO.getUName() + "::";
	                                glideVO.setUName(g + menuDefTitle);
	                            }
	                            
	                            desc.append(",\"name\":\"").append(glideVO.getUName()).append("\"} ");
	                            glideVO.setUModule(menuItemTitle);
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("BUSINESS_RULE"))
	                        {
	                            desc.append("{\"uname\":\"").append(glideVO.getUModule()).append("\"");
	                            definitionDTO.setType("businessRule");
	                            desc.append(",\"udescription\":\"\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("SYSTEM_SCRIPT"))
	                        {
	                            desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
	                            definitionDTO.setType("systemScript");
	                            desc.append(",\"udescription\":\"\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("CRONJOB") || glideVO.getUType().equalsIgnoreCase("scheduler"))
	                        {
	                            desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
	                            desc.append(",\"umodule\":\"").append(glideVO.getUModule()).append("\"} ");
	                            
	                            definitionDTO.setType("scheduler");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("PROPERTIES"))
	                        {
	                            definitionDTO.setType("properties");
	                            if (StringUtils.isNotBlank(glideVO.getUModule()))
	                            {
	                                desc.append("{\"umodule\":\"").append(glideVO.getUModule()).append("\"");
	                            }
	                            else if (StringUtils.isNotBlank(glideVO.getUPropertyModule()))
	                            {
	                                desc.append("{\"umodule\":\"").append(glideVO.getUPropertyModule()).append("\"");
	                            }
	                            else
	                            {
	                                desc.append("{\"umodule\":\"").append("-UNKNOWN-").append("\"");
	                            }
	                            String name = StringUtils.isBlank(glideVO.getUName()) ? "" : glideVO.getUName();
	                            desc.append(",\"uname\":\"").append(name).append("\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("WIKILOOKUP"))
	                        {
	                            definitionDTO.setType("wikiLookup");
	                            desc.append("{\"uwiki\":\"").append("-Old Definition-").append("\"");
	                            String name = StringUtils.isBlank(glideVO.getUName()) ? "-Old Definition-" : glideVO.getUName();
	                            desc.append(",\"uregex\":\"").append(name).append("\"} ");
	                            ImpexUtil.logImpexMessage("WARNING: Old WikiLookup glide definition. SysId: " + glideVO.getSys_id() + ". Please recreate this definition.", true);
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("USER"))
	                        {
	                            definitionDTO.setType("user");
	                            desc.append("{\"uuserName\":\"").append(glideVO.getUName()).append("\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("GROUP"))
	                        {
	                            definitionDTO.setType("group");
	                            desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
	                            desc.append(",\"udescription\":\"\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("ROLE"))
	                        {
	                            definitionDTO.setType("role");
	                            desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
	                            desc.append(",\"udescription\":\"\"} ");
	                        }
	                        else if (glideVO.getUType().equalsIgnoreCase("catalog"))
	                        {
	                            definitionDTO.setType("catalog");
	                            desc.append("{\"name\":\"").append(glideVO.getUModule()).append("\"");
	                            if (StringUtils.isNotBlank(glideVO.getUName()))
	                            {
	                                desc.append(",\"id\":\"" + glideVO.getUName() + "\"} ");
	                            }
	                            else
	                            {
	                                desc.append(",\"id\":\"" + glideVO.getUModule() + "\"} ");
	                            }
	                        }
	                        
	                        String impexOptionJson = glideVO.getUOptions();
	                        if (StringUtils.isBlank(impexOptionJson))
	                        {
	                            // It seems to be an old definition. Populate the default impexOption and save the glude.
	                            ImpexOptionsDTO impexOptions = new ImpexOptionsDTO();
	                            try
	                            {
	                                impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
	                                glideVO.setUOptions(impexOptionJson);
	                            }
	                            catch(Exception e)
	                            {
	                                Log.log.error("Could not parse default ImpexOption for ResolveImpexGlide", e);
	                            }
	                        }
	                        
	                        glideVO.setUDescription(desc.toString());
	                        if (definitionDTO.getType() != null)
	                        {
	                            glideVO.setUType(definitionDTO.getType());
	                        }
	                        ResolveImpexGlide localGlide = new ResolveImpexGlide(glideVO);
	                        localGlide.setResolveImpexModule(new ResolveImpexModule(impexModuleVO));
	                        ImpexUtil.saveResolveImpexGlide(localGlide, userName);
	                    }
	                }
            	} catch (Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
            					  e.getMessage() : "Failed Converting Pre 3.5.x Packages to Post 3.5.x", e);
            		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
            								  e.getMessage() : 
            								  "Failed Converting Pre 3.5.x Packages to Post 3.5.x. Please check RSView logs " +
            								  "for details", true);
            	}
            }
            
            Collection<ResolveImpexWikiVO> impexWikiColl = impexModuleVO.getResolveImpexWikis();
            if (CollectionUtils.isNotEmpty(impexWikiColl))
            {
            	try {
            		ImpexUtil.initImpexOperationStage("IMPORT", "(46/51) Process Resolve impex Wikis", 
            		                impexWikiColl.size());
	                for (ResolveImpexWikiVO impexWiki : impexWikiColl)
	                {
	                	ImpexUtil.updateImpexCount("IMPORT");
	                    StringBuilder desc = new StringBuilder();
	                    ImpexDefinitionDTO definitionDTO = new ImpexDefinitionDTO();
	                    if (StringUtils.isBlank(impexWiki.getUDescription()))
	                    {
	                        if (impexWiki.getUType().equalsIgnoreCase("tag"))
	                        {
	                            // we don't support Runbooks with tag category from Resolve 5 onwards.
	                            continue;
	                        }
	                        if (impexWiki.getUType().equals("DOCUMENT"))
	                        {  
	                            definitionDTO.setType("wiki");
	                            desc.append("{\"ufullname\":\"").append(impexWiki.getUValue()).append("\"} ");
	                        }
	                        if (impexWiki.getUType().contains("NAMESPACE"))
	                        {
	                            definitionDTO.setType("wikiNamespace");
	                            desc.append("{\"unamespace\":\"").append(impexWiki.getUValue()).append("\"");
	                            desc.append(",\"usummary\":\"\"} ");
	                        }
	                        
	                        String impexOptionJson = impexWiki.getUOptions();
	                        if (impexOptionJson == null)
	                        {
	                            // It seems to be an old definition. Populate the default impexOption and save the glude.
	                            ImpexOptionsDTO impexOptions = new ImpexOptionsDTO();
	                            if (impexWiki.getUScan() == null || impexWiki.getUScan() == false)
	                            {
	                                impexOptions.setWikiForms(false);
	                                impexOptions.setWikiSubRB(false);
	                                impexOptions.setWikiSubDT(false);
	                                impexOptions.setWikiRefATs(false);
	                            }
	                            try
	                            {
	                                impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
	                                impexWiki.setUOptions(impexOptionJson);
	                            }
	                            catch(Exception e)
	                            {
	                                Log.log.error("Could not parse default ImpexOption for ResolveImpexWiki", e);
	                            }
	                        }
	                        
	                        definitionDTO.setDescription(desc.toString());
	                        
	                        impexWiki.setUDescription(desc.toString());
	                        impexWiki.setUType(definitionDTO.getType());
	                        ResolveImpexWiki localWiki = new ResolveImpexWiki(impexWiki);
	                        localWiki.setResolveImpexModule(new ResolveImpexModule(impexModuleVO));
	                        ImpexUtil.saveResolveImpexWiki(localWiki, userName);
	                    }
	                }
            	} catch (Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
      					  		  e.getMessage() : "Failed to Process Resolve Impex Wikis", e);
            		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
      								  		  e.getMessage() : 
      								  		  "Failed to Process Resolve Impex Wikis for details", true);
            	}
            }
        }
    }
    
    private void processDeleteXMLFiles()
    {
        /*
         * Process all xml files which has DELETE action and delete it
         * so that it won't be processed again.
         */
        File dir = new File (GLIDE_INSTALL_FOLDER_LOCATION);
        WildcardFileFilter filter = new WildcardFileFilter("*.xml");
        Collection<File> fileCollection = FileUtils.listFiles(dir, filter, TrueFileFilter.INSTANCE);

        if (CollectionUtils.isNotEmpty(fileCollection)) {
	        try {
	        	ImpexUtil.initImpexOperationStage("IMPORT", "(4/51) Process Delete Glide Components", fileCollection.size());
		        for (File file : fileCollection)
		        {
		        	ImpexUtil.updateImpexCount("IMPORT");
		            try
		            {
		                String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
		                if (StringUtils.isNotBlank(content) && content.contains("action=\"DELETE\""))
		                {
		                    ExportUtil.importDelete(file);
		                    file.delete();
		                }
		            }
		            catch (Exception e)
		            {
		                Log.log.error("File: " + file.getName() + " could not be read. Message: " + e.getMessage());
		            }
	        	}
	    	} catch (Exception e) {
	    		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Failed Processing Delete Glide Components.", e);
	    		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
	    								  e.getMessage() : 
	    								  "Failed Processing Delete Glide Components. Please check RSView logs for details", true);
	    	}
        }
    }
    
    private void cleanup(String docName) throws Exception
    {
        try
        {
            //delete the unzipped folder
            ImpexUtil.cleanup(TEMP_FOLDER, null);
            if (MODULE_FOLDER_LOCATION != null)
            {
                ImpexUtil.cleanup(MODULE_FOLDER_LOCATION, ZIP_FILE);
            }
            cleanManifestList();
        }
        catch (Exception e)
        {
            Log.log.error("Error while Import folder cleanup", e);
        }
        
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImportUtil.cleanup(%s) calling 2nd ServiceHibernate.getLatestImpexEventByValue...", 
										docName));
        }
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImportUtil.cleanup(%s) 2nd ServiceHibernate.getLatestImpexEventByValue returned " +
										"Resolve Event [%s]", docName, (eventVO != null ? eventVO : "null")));
        }
        
        if (eventVO != null)
        {
//            eventVO.setUValue("IMPEX:" + (moduleFileName == null ? moduleName : moduleFileName ) + "," + -2 + "," + docName);
        	Pair<String, Boolean> moduleNameAndHasStage = eventVO.getModuleNameAndHasStage("IMPORT");
        	String eventModuleName = moduleNameAndHasStage != null ? 
        							 moduleNameAndHasStage.getLeft() : 
        							 (moduleFileName != null ? moduleFileName : moduleName);
            eventVO.setUValue(String.format("IMPEX-IMPORT:%s,-2,%s", eventModuleName, docName));
            ServiceHibernate.insertResolveEvent(eventVO);
            
            if (Log.log.isTraceEnabled()) {
            	Log.log.trace(String.format("ImportUtil.cleanup(%s) inserted/updated Resolve Event [%s]", docName, eventVO));
            }
        }
        
    }//cleanup
    
    private void cleanManifestList()
    {
        ImportWikiDocUtils.clearListOfWikiDocs();
        ImportGlideCompsUtil.clearGlideCompList();
    }
    
    public String uploadFileForImport(FileItem fileItem) throws Exception
    {
    	String fileName = fileItem.getName();
		//remove any path information from file name (IE issue)
    	File tmpFile = new File(fileName);
		fileName = tmpFile.getName();
		
        File distFolder = new File(DESTINATION_FOLDER);
        if (!distFolder.exists())
        {
            distFolder.mkdir();
        }
        File file = FileUtils.getFile(DESTINATION_FOLDER, fileName);
        fileItem.write(file);
        
        String contentType = Files.probeContentType(file.toPath());
        if (StringUtils.isBlank(contentType) || !contentType.toLowerCase().contains("zip")) {
            ImpexUtil.updateImpexCount("UPLOAD");
            throw new Exception("Content error: " + ERR.IMPEX106.getMessage());
        }
        
        return fileName;
    }
    
    public String uploadFileForImport(File srcFile) throws Exception
    {
        String fileName = srcFile.getName();
        File distFolder = new File(DESTINATION_FOLDER);
        if (!distFolder.exists())
        {
            distFolder.mkdir();
        }
        File destFile = FileUtils.getFile(DESTINATION_FOLDER, fileName);
        FileUtils.copyFile(srcFile, destFile);
        
        return fileName;
    }
    
    public static boolean checkForCustomTables(String moduleName)
    {
        boolean result = false;
        ResolveImpexModule rim = ImpexUtil.getImpexModel(null, moduleName, true);
        if (rim == null)
        {
            rim = ImpexUtil.getImpexModel(null, moduleName, false);
        }
        
        if (rim != null)
        {
            String zipFileName = moduleName + "_" + new Date().getTime() + ".zip";
            File zipFileContent = FileUtils.getFile(RSContext.getResolveHome() + "rsexpert/" + zipFileName);
            if (rim.getUZipFileContent() != null && !zipFileContent.exists())
            {
                try
                {
                    FileUtils.writeByteArrayToFile(zipFileContent, rim.getUZipFileContent());
                    if (zipFileContent.isFile())
                    {
                        ZipFile zipFile = new ZipFile(zipFileContent);
                        Enumeration<? extends ZipEntry> e = zipFile.entries();
                        while (e.hasMoreElements())
                        {
                            ZipEntry entry = e.nextElement();
                            if (entry.getName().contains("/custom_table_"))
                            {
                                result = true;
                                break;
                            }
                        }
                        zipFile.close();
                    }
                }
                catch (IOException e)
                {
                    Log.log.error("Could not read content of the zip file:" + zipFileName, e);
                }
            }
            
            if (zipFileContent.exists())
            {
                zipFileContent.delete();
            }
        }
        
        return result;
    }
    
    protected void replaceWikiSysIdInRR()
    {
    	Map<String, String> map = wikiDocUtils.getWikiReplaceSysIdsMap();
    	if (map.size() > 0)
    	{
    		PreparedStatement preparedStatement = null;
            SQLConnection connection = null;
            List<String> sqls = new ArrayList<String>();
            sqls.add("update rr_rule set u_wiki_id = ? where u_wiki_id = ?");
            sqls.add("update rr_rule set u_runbook_id = ? where u_runbook_id = ?");
            sqls.add("update rr_rule set u_automation_id = ? where u_automation_id = ?");
            
            try
            {
                connection = SQL.getConnection();
                connection.getConnection().setAutoCommit(false);
                for (String sql : sqls)
                {
                	Log.log.trace("Executing SQL :\n" + sql);
	                preparedStatement = connection.prepareStatement(sql);
	                Set<String> originalSysIds = map.keySet();
	                for (String originalSysId : originalSysIds)
	                {
	                	preparedStatement.setString(1, map.get(originalSysId));
	                	preparedStatement.setString(2, originalSysId);
	                	preparedStatement.addBatch();
	                }
	                
	                preparedStatement.executeBatch();
	                connection.getConnection().commit();
                }
            }
            catch(Exception e)
            {
            	Log.log.error("Could not update RRRule table.", e);
            }
            finally
            {
            	try
                {
                    if (preparedStatement != null)
                    {
                        preparedStatement.close();
                    }

                    if (connection != null)
                    {
                        connection.close();
                    }
                }
                catch (Throwable t)
                {
                }
            }
    	}
    }
    
    private void checkImportForRRSchema()
    {
        File dir = new File (GLIDE_INSTALL_FOLDER_LOCATION);
        WildcardFileFilter filter = new WildcardFileFilter("rr_schema*.xml");
        Collection<File> fileCollection = FileUtils.listFiles(dir, filter, null);
        
        if (CollectionUtils.isNotEmpty(fileCollection))
        {
            try
            {
            	ImpexUtil.initImpexOperationStage("IMPORT", "(48/51) Re-Order Resolution Routing Schemes", 1);
                ResolutionRoutingUtil.ReOrderSchemes(userName, ((Main) MainBase.main).getConfigSQL());
                ImpexUtil.updateImpexCount("IMPORT");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                ImpexUtil.logImpexMessage(e.getMessage(), true);
            }
        }
    }
    
    public static Map<String, String> getManifestGraph(String moduleId, boolean isDirty, String username) throws Exception
    {
        Map<String, String> manifestGrapMap = new HashMap<>();
        ResolveImpexModule model = ImpexUtil.getImpexModel(moduleId, null, isDirty);
        if (model != null)
        {
            manifestGrapMap.put(ImpexEnum.NAME.getValue(), model.getUName());
            manifestGrapMap.put(ImpexEnum.MANIFEST_GRAPH.getValue(), verifyImport(model.getManifestGraph(), username));
        }
        return manifestGrapMap;
    }
    
    @SuppressWarnings("unchecked")
    private static String verifyImport(String manifestJson, String username) throws Exception
    {
        if (StringUtils.isBlank(manifestJson))
        {
            throw new Exception("Manifest JSON is blank.");
        }
        JSONArray jsonArray = new ObjectMapper().readValue(manifestJson, JSONArray.class);
        jsonArray.stream().forEach(obj -> {
            verifyJsonObject((JSONObject)obj, username);
        });
        return jsonArray.toString();
    }
    
    @SuppressWarnings("unchecked")
    private static void verifyJsonObject(JSONObject object, String username)
    {
        String name = object.getString(ImpexEnum.NAME.getValue().toLowerCase());
        String type = object.getString(ImpexEnum.type.getValue());
        String sysId = object.getString(ImpexEnum.id.getValue());
        String filterModel = null;
        if (object.containsKey((ImpexEnum.filterModel.getValue()))) {
            filterModel = object.getString(ImpexEnum.filterModel.getValue());
        }
        String checksumStr = object.getString(ImpexEnum.checksum.getValue());
        String status = "new";
        if (object.getBoolean("exported") && StringUtils.isNotBlank(checksumStr)) {
            Integer checksum = Integer.parseInt(checksumStr);
            Integer localModelChecksum = getCompChecksum(name, type, filterModel, sysId, username);
            if (localModelChecksum != null)
            {
                if (checksum.compareTo(localModelChecksum) == 0)
                {
                    status = "same";
                }
                else
                {
                    status = "different";
                }
            }
            object.put("status", status);
        } else {
            object.put("status", "Not Exported");
        }
        
        Object children = object.get("children");
        if (children != null)
        {
            JSONArray childrenArray = (JSONArray) children;
            childrenArray.stream().filter(child -> child != null && !(child instanceof JSONNull)).forEach(child -> {
                verifyJsonObject((JSONObject)child, username);
            });
        }
    }
    
    private static Integer getCompChecksum(String name, String type, String filterModel, String sysId, String username)
    {
        Integer checksum = null;
        switch(type.toLowerCase())
        {
            case ExportUtils.ACTIONTASK :
            {
                checksum = ActionTaskUtil.getTaskChecksum(name, username);
                break;
            }
            case ExportUtils.WIKI :
            
            case ExportUtils.DECISIONTREE :
            case ExportUtils.SECURITY_TEMPLATE :
            case ExportUtils.RUNBOOK :
            {
                checksum = WikiUtils.getWikichecksum(name, username);
                break;
            }
            case ExportUtils.WIKI_TEMPLATE :
            {
                checksum = WikiTemplateUtil.getTemplateChecksum(name, username);
                break;
            }
            case "properties" :
            {
                checksum = ActionTaskPropertiesUtil.getTaskPropertyChecksum(name, username);
                break;
            }
            case ExportUtils.FORM :
            {
                checksum = CustomFormUtil.getFormChecksum(name, username);
                break;
            }
            case "table" :
            {
                checksum = CustomTableUtil.getCustomTableChecksum(name, username);
                break;
            }
            case ExportUtils.SYSTEM_SCRIPT :
            {
                checksum = SystemScriptUtil.getSystemScriptChecksum(name, username);
                break;
            }
            case "wikilookup" :
            {
                checksum = WikiLookupUtil.getLookupChecksum(name, username);
                break;
            }
            case ExportUtils.TAG :
            {
                checksum = TagUtil.getTagChecksum(name, username);
                break;
            }
            case ExportUtils.SCHEDULER :
            {
                checksum = CronUtil.getCronChecksum(name, username);
                break;
            }
            case ExportUtils.SYSTEM_PROPERTY :
            {
                checksum = com.resolve.services.hibernate.util.PropertiesUtil.getSysPropertyChecksum(name);
                break;
            }
            case "artifacttype" :
            {
                checksum = ArtifactTypeUtil.getArtifactTypeChecksom(name);
                break;
            }
            case "cefkeys" :
            {
                checksum = ArtifactTypeUtil.getCustomKeyChecksom(name, username);
                break;
            }
            case ExportUtils.ARTIFACT_ACTION :
            {
                checksum = ArtifactConfigurationUtil.getArtifactConfigChecksum(name);
                break;
            }
            case ExportUtils.FILTERS :
            case ExportUtils.COMPANION :
            {
                checksum = ServiceGateway.getFilterChecksum(name, filterModel, sysId);
                break;
            }
            default :
            {
                Log.log.info(String.format("The checksum for the component type %s is not implemented yet", type));
            }
        }
        return checksum;
    }
}
