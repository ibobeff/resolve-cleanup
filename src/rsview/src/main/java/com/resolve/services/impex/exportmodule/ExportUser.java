/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportUser extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String userNameToBeExported;
    protected List<ImpexComponentDTO> usersToBeExportedList = new ArrayList<ImpexComponentDTO>();
    
    public ExportUser(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
        this.userNameToBeExported = impexGlideVO.getUModule();
    }
    
    public void export() throws Exception
    {
        if (userNameToBeExported.equals("admin"))
        {
            if (!userName.equals("resolve.maint"))
            {
                ImpexUtil.logImpexMessage("Only resolve.maint will be able to export admin user.", false);
                return;
            }
        }
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for User " + userNameToBeExported);
        }
        
        Users user = getUser(userNameToBeExported);
        if (user != null)
        {
            populateUser(user);
            
            if (impexOptions.getUserRoleRel())
            {
                Collection<UserRoleRel> userRoleRelCollection = user.getUserRoleRels();
                if (userRoleRelCollection != null)
                {
                    for (UserRoleRel userRoleRelVO : userRoleRelCollection)
                    {
                        populateUserRoleRel(userRoleRelVO);
                    }
                }
            }
            
            if (impexOptions.getUserGroupRel())
            {
                Collection<UserGroupRel> userGroupRelVOCollection = user.getUserGroupRels();
                if (userGroupRelVOCollection != null)
                {
                    for (UserGroupRel userGroupRelVO : userGroupRelVOCollection)
                    {
                        populateUserGroupRel(userGroupRelVO);
                    }
                }
            }
        }
    }
    
    private void populateUser(Users userVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.Users");
        String userFirstName = StringUtils.isBlank(userVO.getUFirstName()) ? "" : userVO.getUFirstName();
        String userLastName = StringUtils.isBlank(userVO.getULastName()) ? "" : userVO.getULastName();
        
        String fullName = userFirstName + " " + userLastName;
        
        if (StringUtils.isBlank(fullName))
        {
            fullName = userVO.getUUserName();
        }
        dto.setType(USER);
        dto.setSys_id(userVO.getSys_id());
        dto.setFullName(fullName);
        dto.setModule(fullName);
        dto.setName(userVO.getUName());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        dto.setGroup(USER + " " + dto.getFullName());
        
        if (impexOptions.getUserInsert())
        {
            dto.setOperation("INSERT");
        }
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateUserRoleRel (UserRoleRel userRoleRelVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.UserRoleRel");
        
        dto.setType(DB + " " + USERROLEREL);
        dto.setSys_id(userRoleRelVO.getSys_id());
        dto.setTablename(tableName);
        dto.setFullName(userRoleRelVO.getSys_id());
        dto.setDisplayType(dto.getType());
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateUserGroupRel (UserGroupRel userGroupRelVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.UserGroupRel");
        
        dto.setType(DB + " " + USERGROUPREL);
        dto.setSys_id(userGroupRelVO.getSys_id());
        dto.setTablename(tableName);
        dto.setFullName(userGroupRelVO.getSys_id());
        dto.setDisplayType(dto.getType());
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private Users getUser(String username)
    {
        Users userResult = null;

        try
        {
            userResult = (Users) HibernateProxy.execute(() -> {
            	Users res = null;
            	
            	if (StringUtils.isNotBlank(username))
                {
                    //to make this case-insensitive
                    //String sql = "from Users where LOWER(UUserName) = '" + username.trim().toLowerCase() + "'";
                    String sql = "from Users where LOWER(UUserName) = :UUserName";
                    
                    Map<String, Object> queryParams = new HashMap<String, Object>();
                    
                    queryParams.put("UUserName", username.trim().toLowerCase());
                    
                    try
                    {
                        List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                        if(result != null && result.size() > 0)
                        {
                        	res = (Users) result.get(0);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error while executing sql:" + sql, e);
                    }
                }

                if(res != null)
                {                
                    Collection<UserRoleRel> userRoleRels = res.getUserRoleRels();
                    if(userRoleRels != null)
                    {
                        userRoleRels.size(); //load it
                    }

                    Collection<UserGroupRel> userGroupRels = res.getUserGroupRels();
                    if(userGroupRels != null)
                    {
                        userGroupRels.size(); //load it
                    }
                }
                
                return res;
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return userResult;
    }
    
    public List<ImpexComponentDTO> getUsersToBeExported()
    {
        return usersToBeExportedList;
    }
}
