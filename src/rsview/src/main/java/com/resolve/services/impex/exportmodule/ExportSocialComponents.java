/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.services.ServiceCatalog;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.SocialAdminUtil;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.social.impex.SocialImpexUtil;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportSocialComponents extends ExportComponent
{
    protected String socialFolderLocation;
    protected ResolveImpexGlideVO impexGlideVO;
    protected Set<String> wikiNames = new HashSet<String>();
    protected Set<String> tagSet = new HashSet<String>();
    protected Set<Catalog> catalogList = new HashSet<Catalog>();

    protected SocialImpexVO socialImpexVO = new SocialImpexVO();

    public void setExportLocation(String socialFolderLocation)
    {
        this.socialFolderLocation = socialFolderLocation;
    }

    public void setImpexGlideVO(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }

    public void addTagsToExport(String tagId)
    {
//        if (socialImpexVO.getTags() == null)
//        {
//            socialImpexVO.setTags(new HashMap<String, ImpexOptionsDTO>());
//        }
//        socialImpexVO.getTags().put(tagId, null);
    }

//    public void addCatalogsToExport(String catalogId)
//    {
//        try
//        {
//            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Could not parse Impex Options for Catalog ID: " + catalogId, e);
//            ImpexUtil.logImpexMessage("Could not parse Impex Options for Catalog ID: " + catalogId, true);
//            return;
//        }
//
//        if (socialImpexVO.getCatalogs() == null)
//        {
//            socialImpexVO.setCatalogs(new HashMap<String, ImpexOptionsDTO>());
//        }
//        
//        //socialImpexVO.getCatalogs().put(catalogId, impexOptions);
//        
//        try
//        {
//            Catalog catalog = ServiceCatalog.getCatalog(catalogId);
//            catalogIdList.add(catalogId);
//            
//            ImpexComponentDTO dto = new ImpexComponentDTO();
//            dto.setSys_id(catalog.getId());
//            dto.setName(catalog.getName()); 
//            dto.setFullName(catalog.getName());
//            dto.setDisplayType("CATALOG");
//            
//            ExportUtils.impexComponentList.add(dto);
//            
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Could not retrieve catalog id: " + catalogId, e);
//        }
//    }
    
    public void exportCatalogs()
    {
        
    }
    
    public void populateWikidocId(String wikidocId, ImpexOptionsDTO optionsDTO)
    {
        /*
         * Populate wiki doc id to export the graph relationship
         */
        
        if (socialImpexVO.getDocuments() == null)
        {
            socialImpexVO.setDocuments(new HashMap<String, ImpexOptionsDTO>());
        }
        socialImpexVO.getDocuments().put(wikidocId, optionsDTO);
    }
    
    public void populateActionTaskId(String actionTaskId, ImpexOptionsDTO optionsDTO)
    {
        /*
         * Populate Action Task id to export the graph relationship
         */
        
        if (socialImpexVO.getActiontasks() == null)
        {
            socialImpexVO.setActiontasks(new HashMap<String, ImpexOptionsDTO>());
        }
        socialImpexVO.getActiontasks().put(actionTaskId, optionsDTO);
    }
    
    public void exportCatalog(String catalogId, ImpexOptionsDTO impexOptions)
    {
        this.impexOptions = impexOptions;
        
        try
        {
            Catalog catalog = ServiceCatalog.getCatalog(catalogId, null, userName);
            getAllCatalogs(catalog);
            catalogList.add(catalog);//this is the main root catalog
            exportCatalogImages(catalog);
            writeCatalogToJson();
            
            if (socialImpexVO.getCatalogs() == null)
            {
                socialImpexVO.setCatalogs(new HashMap<String, ImpexOptionsDTO>());
            }
            socialImpexVO.getCatalogs().put(catalog.getId(), impexOptions);
        }
        catch (Throwable e)
        {
            Log.log.error("Catalog could not be exported", e);
            ImpexUtil.logImpexMessage("Catalog could not be exported", true);
        }
    }
    
    private void getAllCatalogs(Catalog resolveCatalog) throws Exception
    {
        if (resolveCatalog != null)
        {   
            List<Catalog> localCatalogList = resolveCatalog.getChildren();
            //if this is a Root Ref node and the type is 'CatalogReference'
            if (resolveCatalog.isRootRef() && resolveCatalog.getType().equals("CatalogReference"))
            {
            	if (impexOptions.getCatalogRefCatalogs())
                {
            		if (resolveCatalog.getChildren() != null && !resolveCatalog.getChildren().isEmpty())
            		{
		                Catalog refCatalog = resolveCatalog.getChildren().get(0);
		                resolveCatalog.setChildren(null);
	                    catalogList.add(refCatalog);
	                    exportCatalogImages(refCatalog);
            		}
                }
            }
            
            if (localCatalogList != null && localCatalogList.size() > 0)
            {
                for (Catalog catalog : localCatalogList)
                {
                    getAllCatalogs(catalog);
                }
            }
        }
    }
    
    private void exportCatalogImages(Catalog resolveCatalog)
    {
        if (StringUtils.isNotBlank(resolveCatalog.getIcon()))
        {
            // Export the image
            String iconLink = resolveCatalog.getIcon();
            String tableName = "catalog_attachment";
            String sysId = iconLink.split("=")[1];
            ExportUtils.impexComponentList.add( ExportUtils.createDBComponent(sysId, tableName));
        }
        
        List<Catalog> localCatalogList = resolveCatalog.getChildren();
        if (localCatalogList != null && localCatalogList.size() > 0)
        {
            for (Catalog catalog : localCatalogList)
            {
                exportCatalogImages(catalog);
            }
        }
    }
    
    private void writeCatalogToJson()
    {
        for (Catalog catalog : catalogList)
        {
            FileOutputStream fos= null;
            try
            {
                String catalogJson = new ObjectMapper().writer().writeValueAsString(catalog);
                String fileName = "catalog-"+catalog.getName()+".json";
                File dir = FileUtils.getFile(socialFolderLocation);
                if (!dir.exists())
                {
                    dir.mkdirs();
                }
                File file = FileUtils.getFile(dir, fileName);
                //file.createNewFile();
                
                fos = new FileOutputStream(file);
                fos.write(catalogJson.getBytes());
                fos.flush();
                
                ImpexUtil.logImpexMessage("CATALOG: " + catalog.getName() + " exported.", false);
                
                if (impexOptions.getCatalogWikis())
                {
                    wikiNames.addAll(catalog.findAllDocumentNames());
                }
                if (impexOptions.getCatalogTags())
                {
                    tagSet.addAll(catalog.findAllTagIds());
                    if (tagSet.size() > 0)
                    {
                        for (String tagId : tagSet)
                        {
                            addTagsToExport(tagId);
                        }
                    }
                }
            }
            catch (Throwable e)
            {
                Log.log.error("Catalog could not be exported", e);
                ImpexUtil.logImpexMessage("Catalog could not be exported", true);
            }
            finally
            {
                if (fos != null)
                {
                    try
                    {
                        fos.close();
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }
    }

    public void addProcessToExport(String processName, ImpexOptionsDTO optionsDTO)
    {
        setImpexOptions(optionsDTO);
        SocialProcessDTO process = SocialAdminUtil.getProcessByName(processName, userName);
        if (process != null)
        {
            if (socialImpexVO.getProcesses() == null)
            {
                socialImpexVO.setProcesses(new HashMap<String, ImpexOptionsDTO>());
            }
            //optionsDTO.setTeamTeams(optionsDTO.getProcessTeams());
            socialImpexVO.getProcesses().put(process.getSys_id(), optionsDTO);
            // populateSocialComponent(process);
            // compOptionsMap.put(process , optionsDTO);
        }
        else
        {
            Log.log.error("Process '" + processName + "' could not be found.");
        }
    }
    
//    private void populateSocialComponent(SocialDTO socialDTO)
//    {
//        
//        ImpexComponentDTO dto = new ImpexComponentDTO();
//        dto.setSys_id(socialDTO.getSys_id());
//        dto.setName(socialDTO.getU_display_name()); 
//        dto.setFullName(socialDTO.getU_display_name());
//        
//        if (socialDTO instanceof SocialProcessDTO)
//        {
//            dto.setTablename(SocialProcessDTO.TABLE_NAME);
//            dto.setDisplayType("SOCIAL PROCESS");
//        }
//        else if (socialDTO instanceof SocialTeamDTO)
//        {
//            dto.setTablename(SocialTeamDTO.TABLE_NAME);
//            dto.setDisplayType("SOCIAL TEAM");
//        }
//        else if (socialDTO instanceof SocialForumDTO)
//        {
//            dto.setTablename(SocialForumDTO.TABLE_NAME);
//            dto.setDisplayType("SOCIAL FORUM");
//        }
//        else if (socialDTO instanceof SocialRssDTO)
//        {
//            dto.setTablename(SocialRssDTO.TABLE_NAME);
//            dto.setDisplayType("RSS");
//        }
//        
//        ExportUtils.impexComponentList.add(dto);
//    }

    public void addTeamToExport(String teamName, ImpexOptionsDTO optionsDTO)
    {
        setImpexOptions(optionsDTO);
        SocialTeamDTO team = SocialAdminUtil.getTeamByName(teamName, userName);
        if (team != null)
        {
            if (socialImpexVO.getTeams() == null)
            {
                socialImpexVO.setTeams(new HashMap<String, ImpexOptionsDTO>());
            }
            socialImpexVO.getTeams().put(team.getSys_id(), optionsDTO);
            
            // populateSocialComponent(team);
        }
        else
        {
            Log.log.error("Team '" + teamName + "' could not be found.");
        }
    }

    public void addForumToExport(String forumName, ImpexOptionsDTO optionsDTO)
    {
        setImpexOptions(optionsDTO);
        SocialForumDTO forum = SocialAdminUtil.getForumByName(forumName, userName);
        if (forum != null)
        {
            if (socialImpexVO.getForums() == null)
            {
                socialImpexVO.setForums(new HashMap<String, ImpexOptionsDTO>());
            }
            socialImpexVO.getForums().put(forum.getSys_id(), optionsDTO);
            
            // populateSocialComponent(forum);
        }
        else
        {
            Log.log.error("Forum '" + forumName + "' could not be found.");
        }
    }

    public void addRSSToExport(String rssName, ImpexOptionsDTO optionsDTO)
    {
        setImpexOptions(optionsDTO);
        SocialRssDTO rss = SocialAdminUtil.getRssByName(rssName, userName);
        if (rss != null)
        {
//            if (socialImpexVO.getRsses() == null)
//            {
//                socialImpexVO.setRsses(new HashMap<String, ImpexOptionsDTO>());
//            }
//            socialImpexVO.getRsses().put(rss.getSys_id(), optionsDTO);
            
            // populateSocialComponent(rss);
        }
        else
        {
            Log.log.error("RSS '" + rssName + "' could not be found.");
        }
    }

    public Set<String> getWikiDocNamesToExport()
    {
        return wikiNames;
    }

    public void export() throws Exception
    {
        try
        {
            socialImpexVO.setFolderLocation(socialFolderLocation);
            SocialImpexUtil.exportGraph(socialImpexVO);

            Set<? extends RSComponent> processSet = socialImpexVO.getReturnProcesses();
            if (CollectionUtils.isNotEmpty(processSet))
            {
            	ImpexUtil.initImpexOperationStage("EXPORT", "(9/24) Build List of Social Process Components to Export", processSet.size());
                for (RSComponent process : processSet)
                {
                	ImpexUtil.updateImpexCount("EXPORT");
                    ImpexComponentDTO comp = new ImpexComponentDTO();
                    comp.setSys_id(process.getSys_id());
                    comp.setTablename("social_process");
                    comp.setType("Process");
                    comp.setFullName("social_process");
                    comp.setDisplayType(comp.getType());
                    ExportUtils.impexComponentList.add(comp);
                }
            }

            Set<? extends RSComponent> teamSet = socialImpexVO.getReturnTeams();
            if (CollectionUtils.isNotEmpty(teamSet))
            {
            	ImpexUtil.initImpexOperationStage("EXPORT", "(10/24) Build List of Social Team Components to Export", teamSet.size());
                for (RSComponent team : teamSet)
                {
                	ImpexUtil.updateImpexCount("EXPORT");
                    ImpexComponentDTO comp = new ImpexComponentDTO();
                    comp.setSys_id(team.getSys_id());
                    comp.setTablename("social_team");
                    comp.setType("TEAM");
                    comp.setFullName(team.getDisplayName());
                    comp.setDisplayType(comp.getType());
                    ExportUtils.impexComponentList.add(comp);
                }
            }

            Set<? extends RSComponent> forumSet = socialImpexVO.getReturnForums();
            if (CollectionUtils.isNotEmpty(forumSet))
            {
            	ImpexUtil.initImpexOperationStage("EXPORT", "(11/24) Build List of Social Forum Components to Export", forumSet.size());
                for (RSComponent forum : forumSet)
                {
                	ImpexUtil.updateImpexCount("EXPORT");
                    ImpexComponentDTO comp = new ImpexComponentDTO();
                    comp.setSys_id(forum.getSys_id());
                    comp.setTablename("social_forum");
                    comp.setType("FORUM");
                    comp.setFullName(forum.getDisplayName());
                    comp.setDisplayType(comp.getType());
                    ExportUtils.impexComponentList.add(comp);
                }
            }
            
            Set<? extends RSComponent> rssSet = socialImpexVO.getReturnRss();
            if (CollectionUtils.isNotEmpty(rssSet))
            {
            	ImpexUtil.initImpexOperationStage("EXPORT", "(12/24) Build List of Social RSS Components to Export", 
            									  rssSet.size());
                for (RSComponent rss : rssSet)
                {
                	ImpexUtil.updateImpexCount("EXPORT");
                    ImpexComponentDTO comp = new ImpexComponentDTO();
                    comp.setSys_id(rss.getSys_id());
                    comp.setTablename("rss_subscription");
                    comp.setType("RSS");
                    comp.setFullName(rss.getDisplayName());
                    comp.setDisplayType(comp.getType());
                    ExportUtils.impexComponentList.add(comp);
                }
            }

            Set<? extends RSComponent> userSet = socialImpexVO.getReturnUsers();
            if (CollectionUtils.isNotEmpty(userSet))
            {
            	ImpexUtil.initImpexOperationStage("EXPORT", "(13/24) Export Social User Components", userSet.size());
            	
                for (RSComponent user : userSet)
                {
                    UsersVO userVO = UserUtils.findUserWithRolesById(user.getId());
                    if (userVO != null)
                    {
                    	ImpexUtil.updateImpexCount("EXPORT");
                        ResolveImpexGlideVO glideVO = new ResolveImpexGlideVO();
                        glideVO.setUModule(userVO.getUUserName());
                        
                        String impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
                        glideVO.setUOptions(impexOptionJson);
                        
                        ExportUser exportUser = new ExportUser(glideVO);
                        exportUser.setUserName(userName);
                        exportUser.export();
                    }
                }
            }

            Set<? extends RSComponent> docSet = socialImpexVO.getReturnDocument();
            if (CollectionUtils.isNotEmpty(docSet))
            {
                for (RSComponent doc : docSet)
                {
//                    ImpexComponentDTO comp = new ImpexComponentDTO();
//                    comp.setSys_id(doc.getSys_id());
//                    comp.setFullName(doc.getDisplayName());
//                    comp.setTablename(doc.getDisplayName());
//                    comp.setType("Wiki");
//                    ExportUtils.wikiComponentList.add(comp);
                    wikiNames.add(doc.getDisplayName());
                }
            }

//            Set<Runbook> runbookSet = socialImpexVO.getReturnRunbook();
//            if (runbookSet != null && runbookSet.size() > 0)
//            {
//                for (Runbook runbook : runbookSet)
//                {
//                    ImpexComponentDTO comp = new ImpexComponentDTO();
//                    comp.setSys_id(runbook.getSys_id());
//                    comp.setTablename(runbook.getDisplayName());
//                    comp.setType("Wiki");
//                    ExportUtils.wikiComponentList.add(comp);
//                }
//            }

            Set<? extends RSComponent> actiontaskSet = socialImpexVO.getReturnActiontask();
            if (CollectionUtils.isNotEmpty(actiontaskSet))
            {
            	ImpexUtil.initImpexOperationStage("EXPORT", "(14/24) Export Social Action Task Components", userSet.size());
            	
                for (RSComponent actiontask : actiontaskSet)
                {
                	ImpexUtil.updateImpexCount("EXPORT");
                    ResolveActionTaskVO taskVO = ActionTaskUtil.getResolveActionTask(actiontask.getId(), null, userName);
                    
                    if (taskVO != null)
                    {
                        ResolveImpexGlideVO glideVO = new ResolveImpexGlideVO();
                        glideVO.setUName(taskVO.getUName());
                        glideVO.setUModule(taskVO.getUNamespace());
                        String impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
                        glideVO.setUOptions(impexOptionJson);
                                            
                        ExportActionTask atExport = ExportActionTask.getInstance();
                        atExport.init(glideVO);
                        atExport.setUserName(userName);
                        atExport.setExportSocialComponent(this);
                        atExport.exportActionTask(null);
                    }
                    else
                    {
                        ImpexUtil.logImpexMessage("Could not find ActionTask: " + actiontask.getName() + " to export.", true);
                    }
                }
            }
        }
        catch (Throwable e)
        {
            throw new Exception(e);
            //Log.log.error("Error exporting social graph.", e);
        }
    }
}
