/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.NIOFSDirectory;

import com.resolve.graph.social.migration.GraphMigrationUtil;
import com.resolve.persistence.model.Users;
import com.resolve.rsbase.MainBase;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.SocialComment;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.SocialTarget;
import com.resolve.search.social.SocialIndexAPI;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.vo.GraphUserDTO;
import com.resolve.social.PostUtil;
import com.resolve.util.FileUtils;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class migrates social posts/comments from the old Lucens index and put
 * into ElastiSearch
 *
 */
public class ServiceSocialPostMigration
{
    @SuppressWarnings("serial")
    static class TmpSocialPost extends SocialPost
    {
        private static final long serialVersionUID = -621538679490433225L;
		private String legacySysId;
        private String legacyContent;

        public String getLegacySysId()
        {
            return legacySysId;
        }

        public void setLegacySysId(String legacySysId)
        {
            this.legacySysId = legacySysId;
        }

        public String getLegacyContent()
        {
            return legacyContent;
        }

        public void setLegacyContent(String legacyContent)
        {
            this.legacyContent = legacyContent;
        }
    }

    // migrate posts one user at a time(?)
    public static void migratePost() throws SearchException
    {
        // C:\Project\resolve3\dist\tomcat\webapps\resolve\
        String indexDir = MainBase.main.getProductHome() + "/WEB-INF/index/posts";
        // get the users
        List<GraphUserDTO> users = GraphMigrationUtil.getAllUsersDataFromGraph();

        if (users != null)
        {
            // prepare some convenient Maps
            Map<String, String> likedUsernames = new HashMap<String, String>();
            Map<String, String> readUsernames = new HashMap<String, String>();
            Map<String, String> starredUsernames = new HashMap<String, String>();
            for (GraphUserDTO dto : users)
            {
                System.out.println("This user's name: " + dto.getUser().getName());
                Set<String> likedPosts = dto.getLikePostIds();
                for (String likedPostId : likedPosts)
                {
                    String usernames = likedUsernames.get(likedPostId);
                    if(StringUtils.isBlank(usernames))
                    {
                        usernames = ""; 
                    }
                    usernames = usernames + dto.getUser().getName() + StringUtils.FIELDSEPARATOR;
                    likedUsernames.put(likedPostId, usernames);
                    //System.out.println("Like username : " + usernames);
                }

                Set<String> readPosts = dto.getReadPostIds();
                for (String readPostId : readPosts)
                {
                    String usernames = readUsernames.get(readPostId);
                    if(StringUtils.isBlank(usernames))
                    {
                        usernames = "";
                    }
                    usernames = usernames + dto.getUser().getName() + StringUtils.FIELDSEPARATOR;
                    readUsernames.put(readPostId, usernames);
                    //System.out.println("Read username : " + usernames);
                }

                Set<String> starredPosts = dto.getStarredPostIds();
                for (String starredPostId : starredPosts)
                {
                    String usernames = starredUsernames.get(starredPostId);
                    if(StringUtils.isBlank(usernames))
                    {
                        usernames = ""; 
                    }
                    usernames = usernames + dto.getUser().getName() + StringUtils.FIELDSEPARATOR;
                    starredUsernames.put(starredPostId, usernames);
                    //System.out.println("Starred username : " + usernames);
                }
            }

            for (GraphUserDTO dto : users)
            {
                String username = dto.getUser().getName();
                if(StringUtils.isNotBlank(username))
                {
                    Log.log.debug("Migrating social posts for USER: " + username);

                    Collection<TmpSocialPost> socialPosts = new ArrayList<TmpSocialPost>();
                    Collection<SocialComment> socialComments = new ArrayList<SocialComment>();

                    Collection<TmpSocialPost> posts = searchPost(indexDir, dto.getUser());
                    for (TmpSocialPost post : posts)
                    {
                        collect(socialPosts, socialComments, post);
                    }

                    // Index posts
                    for (TmpSocialPost socialPost : socialPosts)
                    {
                        try
                        {
                            Log.log.debug("#################### POST BEING MIGRATED ##################");
                            Log.log.debug("Indexing post with new ID : " + socialPost.getSysId() + ", Old ID: " + socialPost.getLegacySysId());
                            Log.log.debug("Legacy Content : " + socialPost.getLegacyContent());
                            for(SocialTarget target : socialPost.getTargets())
                            {
                                Log.log.debug("Target : " + target.toString());
                            }
                            SocialIndexAPI.indexSocialPost((SocialPost) socialPost, username, SearchConstants.SOCIALPOST_TYPE_POST, true);

                            // some people may have read this post
                            if (readUsernames.containsKey(socialPost.getLegacySysId()))
                            {
                                String[] usernames = readUsernames.get(socialPost.getLegacySysId()).split(StringUtils.FIELDSEPARATOR_REGEX);
                                for (String readUsername1 : usernames)
                                {
                                    if(StringUtils.isNotBlank(readUsername1))
                                    {
                                        SocialIndexAPI.setRead(socialPost.getSysId(), readUsername1, true);
                                    }
                                }
                            }

                            // some people may have starred this post
                            if (starredUsernames.containsKey(socialPost.getLegacySysId()))
                            {
                                String[] usernames = starredUsernames.get(socialPost.getLegacySysId()).split(StringUtils.FIELDSEPARATOR_REGEX);
                                for (String starredUsername1 : usernames)
                                {
                                    if(StringUtils.isNotBlank(starredUsername1))
                                    {
                                        SocialIndexAPI.setStarred(socialPost.getSysId(), starredUsername1, true);
                                    }
                                }
                            }

                            // some people may have liked the post
                            if (likedUsernames.containsKey(socialPost.getLegacySysId()))
                            {
                                String[] usernames = likedUsernames.get(socialPost.getLegacySysId()).split(StringUtils.FIELDSEPARATOR_REGEX);
                                for (String likedUsername1 : usernames)
                                {
                                    if(StringUtils.isNotBlank(likedUsername1))
                                    {
                                        SocialIndexAPI.setLikedPost(socialPost.getSysId(), likedUsername1, true);
                                    }
                                }
                            }
                        }
                        catch (SearchException e)
                        {
                            Log.log.warn(e.getMessage(), e);
                        }
                    }

                    for (SocialComment socialComment : socialComments)
                    {
                        try
                        {
                            Log.log.debug("Indexing social comment with ID : " + socialComment.getSysId() + ", in post ID: " + socialComment.getPostId());
                            SocialIndexAPI.indexSocialComment(socialComment, username, SearchConstants.SOCIALPOST_TYPE_POST);
                        }
                        catch (SearchException e)
                        {
                            Log.log.warn(e.getMessage(), e);
                        }
                    }
                }
            }
            
            //all the posts/comments are migrated, now generate the
            //search query alongwith their ratings
            SocialIndexAPI.updateAllQueryRating("system");
        }
    }

    private static void collect(Collection<TmpSocialPost> socialPosts, Collection<SocialComment> socialComments, TmpSocialPost post)
    {
        if (post != null && StringUtils.isNotBlank(post.getLegacyContent()))
        {
            String content = post.getLegacyContent();
            String[] oldContents = content.split(StringUtils.LINESEPARATOR_REGEX);
            // the first item is always the main post
            post.setContent(replaceString(oldContents[0]));
            System.out.printf("Post content %s \n", post.getContent());
            socialPosts.add(post);

            // second item onward it is all comments
            if (oldContents.length > 1)
            {
                for(int i=1; i<oldContents.length; i++)
                {
                    String oldContent = oldContents[i];
                    if (StringUtils.isNotBlank(oldContent))
                    {
                        String[] oldComment = oldContent.split(StringUtils.FIELDSEPARATOR_REGEX);
                        String commentAuthorUsername = oldComment[0];
                        User user = SocialCompConversionUtil.getSocialUser(commentAuthorUsername);
                        String commentContent = oldComment[1];
                        Long commentCreatedOn = post.getSysCreatedOn();
                        Long commentUpdatedOn = post.getSysUpdatedOn();
                        try
                        {
                            if(!commentCreatedOn.equals(Long.parseLong(oldComment[2])))
                            {
                                Log.log.debug("Found a comment with different date");
                            }
                            commentCreatedOn = Long.parseLong(oldComment[2]);
                            commentUpdatedOn = Long.parseLong(oldComment[2]);
                            
                        }
                        catch (NumberFormatException e)
                        {
                            // too bad, ignore and set the post's time
                        }
                        Log.log.debug("Comment author's username: " + commentAuthorUsername);
                        SocialComment socialComment = SearchUtils.prepareSocialComment(null, post.getSysId(), post.getAuthorUsername(), commentContent, user);
                        socialComment.setSysCreatedOn(commentCreatedOn);
                        socialComment.setSysUpdatedOn(commentUpdatedOn);
                        socialComments.add(socialComment);
                    }
                }
            }
        }
    }

    /**
     * This method replaces old URLs with the new format of URLs for various links
     * 
     * @param seed
     * @return
     */
    private static String replaceString(String seed)
    {
        String result = seed;
        /*
        //Mentions
        String result = seed.replace("/resolve/service/social/showentity?type=user&value=&username", "#RS.user.User/username");

        //File download
        result = result.replace("href=/resolve/service/post/download?fileid=", "href=\"\" dataType=\"\" fullData=\"/resolve/service/social/download?id=");
        result = result.replace(" title=", "\" title=");
        
        //Link to anything
        */
        
        //
     // Find all the html blocks in the result and put
     // them into the htmlBlocks map for future restoration
     //
     // Example: <!-- HTML_BEGIN -->test<!-- HTML_END -->
     //

         Map<String,String> htmlBlocks = new HashMap<String,String>();
    
         Pattern p = Pattern.compile("<!-- HTML_BEGIN -->(.*?)<!-- HTML_END -->", Pattern.DOTALL);
         Matcher m = p.matcher(result);
         int counter = 0;
         while (m.find())
         {
             String fullText = m.group();
             String text = m.group(1);
    
             String replacement = "{hmtlBlock" + counter + "}";
             htmlBlocks.put(replacement, text);
             result = result.replace(fullText, replacement);
             counter++;
         }
    
         // HTML Escape the remaining text
         result = StringUtils.escapeHtml(result);
    
         // Replace the original values between html tags
          for (String key : htmlBlocks.keySet())
         {
             result = result.replace(key, htmlBlocks.get(key));
         }
          
        return result;
    }
    
    public static Collection<TmpSocialPost> searchPost(String indexDir, User user)
    {
        String author = user.getName();
        String userSysId = user.getSys_id();

        Collection<TmpSocialPost> returnList = new ArrayList<TmpSocialPost>();

        //try
        {
            Analyzer analyzer = new KeywordAnalyzer();

            Directory directory;
            IndexSearcher isearcher;
            try
            {
                directory = new NIOFSDirectory(FileUtils.getFile(indexDir).toPath());
                DirectoryReader ireader = DirectoryReader.open(directory);
                isearcher = new IndexSearcher(ireader);
            }
            catch (IOException e)
            {
                Log.log.error("Something went terribly wrong opening " + indexDir + " directory. Actual error: " + e.getMessage(), e);
                throw new RuntimeException(e.getMessage(), e);
            }


            QueryParser queryParser = new QueryParser("author", analyzer);
            queryParser.setDefaultOperator(QueryParser.Operator.AND);

            Query queryLocal;
            try
            {
                queryLocal = queryParser.parse(author);
            }
            catch (ParseException e)
            {
                Log.log.error("Somthing went terribly wrong parsing author " + author + ". Actual error: " + e.getMessage(), e);
                throw new RuntimeException(e.getMessage(), e);
            }
            
            BooleanQuery finalQuery = new BooleanQuery.Builder().add(new BooleanClause(queryLocal, BooleanClause.Occur.MUST)).build();
            //finalQuery.add(queryLocal, BooleanClause.Occur.MUST);

            QueryParser queryParser1 = new QueryParser( "postType", analyzer);
            queryParser1.setDefaultOperator(QueryParser.Operator.AND);
            try
            {
                Query queryLocal1 = queryParser.parse("POST");
            }
            catch (ParseException e)
            {
                Log.log.error("Somthing went terribly wrong parsing the postType=POST. Actual error: " + e.getMessage(), e);
                throw new RuntimeException(e.getMessage(), e);
            }
            // finalQuery.add(queryLocal1, BooleanClause.Occur.MUST);

            TopDocs topDocs;
            try
            {
                topDocs = isearcher.search(finalQuery, 10000);
            }
            catch (IOException e)
            {
                Log.log.error("Somthing went terribly wrong searching with finalQuery: " + finalQuery.toString() + ". Actual error: " + e.getMessage(), e);
                throw new RuntimeException(e.getMessage(), e);
            }

            if (topDocs != null)
            {
                ScoreDoc[] hits = topDocs.scoreDocs;
                for (int i = 0; i < hits.length; i++)
                {
                    ScoreDoc scoreDoc = hits[i];

                    Document document = null;
                    try
                    {
                        document = isearcher.doc(scoreDoc.doc);
                    }
                    catch (IOException e)
                    {
                        Log.log.error("Somthing went terribly wrong with isearcher.doc(scoreDoc.doc): " + scoreDoc.doc + ". Actual error: " + e.getMessage(), e);
                        throw new RuntimeException(e.getMessage(), e);
                    }
                    if (document != null)
                    {
                        String type = document.get("postType");
                        if ("post".equalsIgnoreCase(type))
                        {
                            if(document.get("u_component_description") != null && document.get("u_component_description").trim().length() > 0)
                            {
                                TmpSocialPost post = new TmpSocialPost();
    
                                post.setSysId(Guid.getGuid());
                                post.setLegacySysId(document.get("post_id"));
                                post.setTitle(document.get("u_component_name"));
                                post.setReadRoles(SearchUtils.parseRoles(document.get("post_roles")));
                                post.setEditRoles(SearchUtils.parseRoles(document.get("post_edit_roles")));
                                post.setAuthorUsername(document.get("author"));
                                if(StringUtils.isNotBlank(post.getAuthorUsername()))
                                {
                                    if(author.equals(post.getAuthorUsername()))
                                    {
                                        Log.log.debug("This is a BLOG for " + post.getAuthorUsername());
                                        //by default author reads her own post
                                        post.addReadUsername(post.getAuthorUsername());
                                        User authorUser = ServiceSocial.findUserByUsername(post.getAuthorUsername());
                                        if(authorUser == null)
                                        {
                                            Log.log.debug("This user might have been deleted from the system but keeping the posts " + author);
                                            post.setAuthorSysId(author);
                                            post.setAuthorUsername(author);
                                            post.setAuthorDisplayName(author);
                                        }
                                        else
                                        {
                                            post.setAuthorSysId(userSysId);
                                            post.setAuthorUsername(authorUser.getName());
                                            post.setAuthorDisplayName(authorUser.getDisplayName());
                                        }
                                    }
                                    else
                                    {
                                        User authorUser = ServiceSocial.findUserByUsername(post.getAuthorUsername());
                                        if(authorUser == null)
                                        {
                                            //check by splitting name into first and last name
                                            Users tmpUser = UserUtils.findUserByDisplayName(post.getAuthorUsername());
                                            if(tmpUser != null)
                                            {
                                                Log.log.debug("Found user by its display name: " + post.getAuthorUsername());
                                                authorUser = SocialCompConversionUtil.convertUserToSocialUser(tmpUser);
                                                post.setAuthorSysId(authorUser.getSys_id());
                                                post.setAuthorUsername(authorUser.getName());
                                                post.setAuthorDisplayName(authorUser.getDisplayName());
                                            }
                                        }
                                        else
                                        {
                                            post.setAuthorSysId(authorUser.getSys_id());
                                            post.setAuthorUsername(authorUser.getName());
                                            post.setAuthorDisplayName(authorUser.getDisplayName());
                                        }
                                    }
                                }
                                post.setType(document.get("postType"));
                                // interesting we used to keep the comment in a |;|
                                // delimeted way in the following field
                                // alright, we'll parse it once we get the data
                                post.setLegacyContent(document.get("u_component_description"));
                                Log.log.debug("Found social post, author:" + post.getAuthorUsername());
                                Log.log.debug("legacy id: " + document.get("post_id"));
                                Log.log.debug("post content: " + document.get("u_component_description"));
                                
                                String createdOn = document.get("u_component_created_on");
                                String updatedOn = document.get("u_component_updated_on");
    
                                //String hashtags = document.get(SearchConstants.HASH_TEXT.getTagName());
                                try
                                {
                                    Date created = DateTools.stringToDate(createdOn);
                                    post.setSysCreatedOn(created.getTime());
                                    post.setSysCreatedBy(post.getAuthorUsername());
                                    
                                    Date updated = DateTools.stringToDate(updatedOn);
                                    post.setSysUpdatedOn(updated.getTime());
                                    post.setSysUpdatedBy(post.getAuthorUsername());
                                }
                                catch (java.text.ParseException e)
                                {
                                    Log.log.error("Somthing went terribly wrong parsing date, createdOn: " + createdOn + " and or updatedOn: " + updatedOn + ". Actual error: " + e.getMessage(), e);
                                    throw new RuntimeException(e.getMessage(), e);
                                }
    
                                String targetId = document.get("targetID");
                                String targetDisplayName = document.get("targetDisplayName");
                                List<String> targetIds = new LinkedList<String>();
                                if(StringUtils.isBlank(targetId) || "Blog".equalsIgnoreCase(targetDisplayName))
                                {
                                    //this is a Blog post
                                    Log.log.debug("This is a blog for : " + userSysId);
                                    targetIds.add(userSysId);
                                }
                                //sometime targetId could have multiple sys ids of the target
                                targetIds.addAll(parseTargetIds(targetId));
                                TreeSet<SocialTarget> targets = PostUtil.getSocialTargetByIds(targetIds);
                                if(targets == null || targets.size() == 0)
                                {
                                    Log.log.debug("Target might have been deleted: " + targetIds);
                                }
                                else
                                {
                                    for(SocialTarget target : targets)
                                    {
                                        Log.log.debug("Target info: " + target.toString());
                                    }
                                    post.setTargets(targets);
                                }
                                returnList.add(post);
                            }
                        }
                    }
                }
            }
        }

        return returnList;
    }

    private static List<String> parseTargetIds(String targetId)
    {
        List<String> targetIds = new LinkedList<String>();
        if(StringUtils.isNotBlank(targetId))
        {
            //The \s matches any white space char, and {1,} will match it 1 or more times.
            String[] parts = targetId.split("\\s{1,}");
            for(String p : parts)
            {
                targetIds.add(p);
            }
        }
        return targetIds;
    }
}
