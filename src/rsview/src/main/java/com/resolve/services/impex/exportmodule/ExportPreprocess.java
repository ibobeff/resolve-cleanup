/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.List;
import java.util.Set;

import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportPreprocess extends ExportComponent
{
    protected ResolvePreprocess resolvePreprocess;
    protected ExportActionTask exportActionTask;
    
    public ExportPreprocess(ResolvePreprocess vo, ExportActionTask exportActionTask)
    {
        this.resolvePreprocess = vo;
        this.exportActionTask = exportActionTask;
    }
    
    public List<ManifestGraphDTO> export()
    {
        List<ManifestGraphDTO> list = null;
        populateAssessor();
        if (exportActionTask.impexOptions.getAtProperties())
        {
            list = processPreprocessorProperties();
        }
        return list;
    }
    
    public void populateAssessor()
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolvePreprocess");
        dto.setSys_id(resolvePreprocess.getSys_id());
        dto.setFullName(resolvePreprocess.getUName());
        dto.setName(resolvePreprocess.getUName());
        dto.setGroup(exportActionTask.ACTIONTASK + " " + resolvePreprocess.getUName());
        dto.setType(exportActionTask.PREPROCESSOR);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        exportActionTask.getParentGraphDTO().getIds().add(resolvePreprocess.getSys_id());
    }
    
    public List<ManifestGraphDTO> processPreprocessorProperties()
    {
        List<ManifestGraphDTO> list = null;
    	if (StringUtils.isNotBlank(resolvePreprocess.getUScript()))
    	{
    		list = processProperty(resolvePreprocess.getUScript(), resolvePreprocess.getUName());
    	}
    	return list;
    }
    
}