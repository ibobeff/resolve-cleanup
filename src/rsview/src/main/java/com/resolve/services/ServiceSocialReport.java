/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import com.resolve.rsview.main.Metric;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.util.Log;

public class ServiceSocialReport
{
    
    /**
     * Each time when user post, we update the  
     * Metric userPostCount map
     * 
     * @param user
     */
    public static void updateUserPostCount(User user)
    {
        try
        {
            Metric.updateUserPostCount(user.getSys_id());
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }
    }
    
    /**
     * Each time when user add comment, 
     * we update the Metric userCommentCount map
     * 
     * @param user
     */
    public static void updateUserCommentCount(User user)
    {
        try
        {
            Metric.updateUserCommentCount(user.getSys_id());
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }
    }
    
    /**
     * Each time when user mark like of post or comment
     * we update the Metric userLikeCount map
     * 
     * @param user
     */
    public static void updateUserLikeCount(User user)
    {
        try
        {
            Metric.updateUserLikeCount(user.getSys_id());
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }
    }
    
    /**
     * merge all three maps to one map, and clean
     * each map
     */
    public static void refreshSocialReportMaps()
    {
        try
        {
            Metric.mergeAllMaps();
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }
    }
    
}
