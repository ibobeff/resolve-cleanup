/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;

import com.resolve.persistence.model.DatabaseConnectionPool;
import com.resolve.persistence.model.EWSAddress;
import com.resolve.persistence.model.EmailAddress;
import com.resolve.persistence.model.RemedyxForm;
import com.resolve.persistence.model.SSHPool;
import com.resolve.persistence.model.TelnetPool;
import com.resolve.persistence.model.XMPPAddress;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceGateway;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.MSGGatewayFilterUtil;
import com.resolve.services.hibernate.util.PullGatewayFilterUtil;
import com.resolve.services.hibernate.util.PushGatewayFilterUtil;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.util.WikiUtils;
import com.resolve.util.StringUtils;

/**
 * Gateway Filter Export handles requests coming from new UI.
 * 
 * @author mangesh.shimpi
 *
 */

public class ExportFilterV2 extends ExportComponent {
    private static String gatewayName;
    private static String modelName;
    private static String type;
    private static ManifestGraphDTO parent;
    
    public ExportFilterV2(ResolveImpexGlideVO impexGlideVO) throws Exception {
        gatewayName = impexGlideVO.getUModule();
    }

    public ManifestGraphDTO export() throws Exception {
        parent = new ManifestGraphDTO();
        parent.setName(gatewayName);
        parent.setId(gatewayName);
        parent.setChecksum(0);
        parent.setType("gateway");
        
        modelName = ServiceGateway.getModelName(gatewayName);
        if (StringUtils.isBlank(modelName))
            modelName = HibernateUtil.getGatewayModelFilter(gatewayName);
        if (!modelName.startsWith("com")) {
            modelName = String.format("com.resolve.persistence.model.%s", modelName);
        }
        exportFilters(parent);
        return parent;
    }
    
    private void exportFilters(ManifestGraphDTO parent) throws Exception {
        List<GatewayVO> voList = null;
        type = ServiceGateway.getGatewayType(gatewayName);
        if (StringUtils.isBlank(type) || type.equalsIgnoreCase(ExportUtils.NONE)) {
            if (modelName.endsWith(ImpexEnum.SSHPool.getValue())) {
                exportSSHPoolFilters();
            } else if (modelName.endsWith(ImpexEnum.TelnetPool.getValue())) {
                exportTelnetPoolFilters();
            } else {
            
                voList = ServiceGateway.getAllFilters(modelName, ImpexEnum.default_queue.getValue(), userName);
            }
        } else {
            voList = ServiceGateway.getAllSDK2Filters(modelName, gatewayName, ImpexEnum.default_queue.getValue());
        }
        if (CollectionUtils.isNotEmpty(voList)) {
            voList.stream().map(vo -> (GatewayFilterVO)vo)
                           .forEach(ExportFilterV2::exportFilter);
        }
        populateCompanionFilter();
    }
    
    private static void exportFilter(GatewayFilterVO filterVO) {
        ManifestGraphDTO filterGraph = new ManifestGraphDTO();
        filterGraph.setChecksum(filterVO.getChecksum());
        filterGraph.setName(filterVO.getUName());
        filterGraph.setType("filters");
        filterGraph.setId(filterVO.getSys_id());
        filterGraph.setFilterModel(modelName);
        if (excludeList.contains(filterVO.getSys_id())) {
            filterGraph.setExported(false);
        } else {
            filterGraph.setExported(true);
            populateFilter(filterVO, filterGraph);
        }
        parent.getChildren().add(filterGraph);
    }
    
    private static void populateFilter(GatewayFilterVO filterVO, ManifestGraphDTO filterGraph) {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table(modelName);
        dto.setSys_id(filterVO.getSys_id());
        dto.setFullName(filterVO.getUName());
        dto.setName(filterVO.getUName());
        dto.setType(gatewayName);
        dto.setDisplayType(gatewayName);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
        
        if (StringUtils.isNotBlank(filterVO.getURunbook())) {
            String wikiId = WikiUtils.getWikidocSysId(filterVO.getURunbook());
            ManifestGraphDTO wikiGraph = new ManifestGraphDTO();
            wikiGraph.setType(ExportUtils.RUNBOOK);
            wikiGraph.setName(filterVO.getURunbook());
            if (StringUtils.isNotBlank(wikiId) && !excludeList.contains(wikiId)) {
                wikiGraph.setId(wikiId);
                wikiGraph.setExported(true);
            } else {
                wikiGraph.setExported(false);
            }
            filterGraph.getChildren().add(wikiGraph);
        }
        
        if (StringUtils.isNotBlank(type) && !type.equalsIgnoreCase(ExportUtils.NONE)) {
            populateFilterArrtib(filterVO, filterGraph); // It's SDK2 filter
        }
        
    }
    
    private void exportSSHPoolFilters() {
        List<SSHPool> pools = ServiceGateway.getAllSSHPoolFilters();
        if (CollectionUtils.isNotEmpty(pools)) {
            pools.stream().forEach(ExportFilterV2::populateSSHPool);
        }
    }
    
    private static void populateSSHPool(SSHPool sshPool)
    {
        ManifestGraphDTO filterGraph = new ManifestGraphDTO ();
        filterGraph.setChecksum(sshPool.getChecksum());
        filterGraph.setName(sshPool.getUSubnetMask());
        filterGraph.setType(ExportUtils.FILTERS);
        filterGraph.setId(sshPool.getSys_id());
        filterGraph.setFilterModel(ImpexEnum.SSHPool.getValue());
        if (excludeList.contains(sshPool.getSys_id())) {
            filterGraph.setExported(false);
        } else {
            filterGraph.setExported(true);
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = HibernateUtil.class2Table(modelName);
            
            dto.setSys_id(sshPool.getSys_id());
            dto.setFullName(sshPool.getUSubnetMask());
            dto.setName(sshPool.getUSubnetMask());
            dto.setType(gatewayName);
            dto.setDisplayType(gatewayName);
            dto.setTablename(tableName);
            ExportUtils.impexComponentList.add(dto);
        }
        parent.getChildren().add(filterGraph);
    }
    
    private void exportTelnetPoolFilters() {
        List<TelnetPool> pools = ServiceGateway.getAllTelnetPoolFilters();
        if (CollectionUtils.isNotEmpty(pools)) {
            pools.stream().forEach(ExportFilterV2::populateTelnetPool);
        }
    }
    
    private static void populateTelnetPool(TelnetPool telnetPool)
    {
        ManifestGraphDTO filterGraph = new ManifestGraphDTO ();
        filterGraph.setChecksum(telnetPool.getChecksum());
        filterGraph.setName(telnetPool.getUSubnetMask());
        filterGraph.setType(ExportUtils.FILTERS);
        filterGraph.setId(telnetPool.getSys_id());
        filterGraph.setFilterModel(ImpexEnum.TelnetPool.getValue());
        if (excludeList.contains(telnetPool.getSys_id())) {
            filterGraph.setExported(false);
        } else {
            filterGraph.setExported(true);
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = HibernateUtil.class2Table(modelName);
            
            dto.setSys_id(telnetPool.getSys_id());
            dto.setFullName(telnetPool.getUSubnetMask());
            dto.setName(telnetPool.getUSubnetMask());
            dto.setType(gatewayName);
            dto.setDisplayType(gatewayName);
            dto.setTablename(tableName);
            ExportUtils.impexComponentList.add(dto);
        }
        parent.getChildren().add(filterGraph);
    }
    
    private static void populateCompanionFilter() {
        List<ManifestGraphDTO> graphList = null;
        String model = modelName.replace("com.resolve.persistence.model.", "");
        String companionModel = ServiceGateway.primaryModelName2FCMName.get(model);
        if (EnumUtils.isValidEnum(ImpexEnum.class, companionModel)) {
            ImpexEnum modelEnum = ImpexEnum.valueOf(companionModel);
            switch (modelEnum) {
                case DatabaseConnectionPool : {
                    graphList = exportConnectionPool();
                    break;
                }
                case EmailAddress : {
                    graphList = exportEmailAddress();
                    break;
                }
                case EWSAddress : {
                    graphList = exportEWSAddress();
                    break;
                }
                case RemedyxForm : {
                    graphList = exportRemedyxForm();
                    break;
                }
                case XMPPAddress : {
                    graphList = exportXMPPAddress();
                    break;
                }
                default : {
                    // Do nothing here
                }
            }
        }
        if (CollectionUtils.isNotEmpty(graphList))
            parent.getChildren().addAll(graphList);
    }
    
    private static List<ManifestGraphDTO> exportConnectionPool() {
        List<ManifestGraphDTO> graphList = new ArrayList<>();
        List<DatabaseConnectionPool> poolList = ServiceGateway.getAllDBPools();
        if (CollectionUtils.isNotEmpty(poolList)) {
            poolList.stream().forEach(pool -> {
                graphList.add(populateConnectionPool(pool));
            });
        }
        return graphList;
    }
    
    private static ManifestGraphDTO populateConnectionPool(DatabaseConnectionPool pool) {
        ManifestGraphDTO graph = new ManifestGraphDTO();
        graph.setId(pool.getSys_id());
        graph.setName(pool.getUPoolName());
        graph.setType(ExportUtils.COMPANION);
        graph.setChecksum(pool.getChecksum());
        graph.setFilterModel(ImpexEnum.DatabaseConnectionPool.getValue());
        if (excludeList.contains(pool.getSys_id())) {
            graph.setExported(false);
            return graph;
        } else {
            graph.setExported(true);
        }
        
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.DatabaseConnectionPool");
        dto.setSys_id(pool.getSys_id());
        dto.setFullName(pool.getUPoolName());
        dto.setName(pool.getUPoolName());
        dto.setType(ExportUtils.DB_POOL);
        dto.setDisplayType(ExportUtils.DB_POOL);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
        
        return graph;
    }
    
    private static  List<ManifestGraphDTO> exportEmailAddress() {
        List<ManifestGraphDTO> graphList = new ArrayList<>();
        List<EmailAddress> poolList = ServiceGateway.getAllEmailAddresses();
        if (CollectionUtils.isNotEmpty(poolList)) {
            poolList.stream().forEach(pool -> {
                graphList.add(populateEmailAddress(pool));
            });
        }
        return graphList;
    }
    
    private static ManifestGraphDTO populateEmailAddress(EmailAddress address) {
        ManifestGraphDTO graph = new ManifestGraphDTO();
        graph.setId(address.getSys_id());
        graph.setName(address.getUEmailAddress());
        graph.setType(ExportUtils.COMPANION);
        graph.setChecksum(address.getChecksum());
        graph.setFilterModel(ImpexEnum.EmailAddress.getValue());
        if (excludeList.contains(address.getSys_id())) {
            graph.setExported(false);
            return graph;
        } else {
            graph.setExported(true);
        }
        
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.EmailAddress");
        dto.setSys_id(address.getSys_id());
        dto.setFullName(address.getUEmailAddress());
        dto.setName(address.getUEmailAddress());
        dto.setType(ExportUtils.EMAIL_ADDRESS);
        dto.setDisplayType(ExportUtils.EMAIL_ADDRESS);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
        
        return graph;
    }
    
    private static  List<ManifestGraphDTO> exportEWSAddress() {
        List<ManifestGraphDTO> graphList = new ArrayList<>();
        List<EWSAddress> poolList = ServiceGateway.getAllEWSAddresses();
        if (CollectionUtils.isNotEmpty(poolList)) {
            poolList.stream().forEach(address -> {
                graphList.add(populateEWSAddress(address));
            });
        }
        return graphList;
    }
    
    private static ManifestGraphDTO populateEWSAddress(EWSAddress address) {
        ManifestGraphDTO graph = new ManifestGraphDTO();
        graph.setId(address.getSys_id());
        graph.setName(address.getUEWSAddress());
        graph.setType(ExportUtils.COMPANION);
        graph.setChecksum(address.getChecksum());
        graph.setFilterModel(ImpexEnum.EWSAddress.getValue());
        if (excludeList.contains(address.getSys_id())) {
            graph.setExported(false);
            return graph;
        } else {
            graph.setExported(true);
        }
        
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.EWSAddress");
        dto.setSys_id(address.getSys_id());
        dto.setFullName(address.getUEWSAddress());
        dto.setName(address.getUEWSAddress());
        dto.setType(ExportUtils.EWS_ADDRESS);
        dto.setDisplayType(ExportUtils.EWS_ADDRESS);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
        
        return graph;
    }
    
    private static  List<ManifestGraphDTO> exportRemedyxForm() {
        List<ManifestGraphDTO> graphList = new ArrayList<>();
        List<RemedyxForm> formList = ServiceGateway.getAllRemedyxForms();
        if (CollectionUtils.isNotEmpty(formList)) {
            formList.stream().forEach(form -> {
                graphList.add(populateRemedyxForm(form));
            });
        }
        return graphList;
    }
    
    private static ManifestGraphDTO populateRemedyxForm(RemedyxForm form) {
        ManifestGraphDTO graph = new ManifestGraphDTO();
        graph.setId(form.getSys_id());
        graph.setName(form.getUName());
        graph.setType(ExportUtils.COMPANION);
        graph.setChecksum(form.getChecksum());
        graph.setFilterModel(ImpexEnum.RemedyxForm.getValue());
        if (excludeList.contains(form.getSys_id())) {
            graph.setExported(false);
            return graph;
        } else {
            graph.setExported(true);
        }
        
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RemedyxForm");
        dto.setSys_id(form.getSys_id());
        dto.setFullName(form.getUName());
        dto.setName(form.getUName());
        dto.setType(ExportUtils.REMEDYX_FORM);
        dto.setDisplayType(ExportUtils.REMEDYX_FORM);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
        
        return graph;
    }
    
    private static  List<ManifestGraphDTO> exportXMPPAddress() {
        List<ManifestGraphDTO> graphList = new ArrayList<>();
        List<XMPPAddress> addressList = ServiceGateway.getAllXMPPAddresses();
        if (CollectionUtils.isNotEmpty(addressList)) {
            addressList.stream().forEach(form -> {
                graphList.add(populateXMPPAddress(form));
            });
        }
        return graphList;
    }
    
    private static ManifestGraphDTO populateXMPPAddress(XMPPAddress address) {
        ManifestGraphDTO graph = new ManifestGraphDTO();
        graph.setId(address.getSys_id());
        graph.setName(address.getUXMPPAddress());
        graph.setType(ExportUtils.COMPANION);
        graph.setChecksum(address.getChecksum());
        graph.setFilterModel(ImpexEnum.XMPPAddress.getValue());
        if (excludeList.contains(address.getSys_id())) {
            graph.setExported(false);
            return graph;
        } else {
            graph.setExported(true);
        }
        
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.XMPPAddress");
        dto.setSys_id(address.getSys_id());
        dto.setFullName(address.getUXMPPAddress());
        dto.setName(address.getUXMPPAddress());
        dto.setType(ExportUtils.XMPP_ADDRESS);
        dto.setDisplayType(ExportUtils.XMPP_ADDRESS);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
        
        return graph;
    }
    
    private static void populateFilterArrtib(GatewayFilterVO filterVO, ManifestGraphDTO filterGraph) {
     // Push, Pull or MSG type gateway. Export the filter attributes as well.
        List<String> attrSysIds = null;
        switch (type) {
            case ExportUtils.PUSH : {
                attrSysIds = PushGatewayFilterUtil.getPushGatewayFilterAttributeIds(filterVO.getSys_id());
                break;
            }
            case ExportUtils.PULL : {
                attrSysIds = PullGatewayFilterUtil.getPullGatewayFilterAttributeIds(filterVO.getSys_id());
                break;
            }
            case ExportUtils.MSG : {
                attrSysIds = MSGGatewayFilterUtil.getMSGGatewayFilterAttributeIds(filterVO.getSys_id());
                break;
            }
        }
        filterGraph.getIds().addAll(attrSysIds);
        attrSysIds.stream().forEach ( attrSysId -> {
            ImpexComponentDTO arrtDto = new ImpexComponentDTO();
            String attrTableName = HibernateUtil.class2Table(String.format("%sAttr", modelName));
            arrtDto.setSys_id(attrSysId);
            arrtDto.setFullName(filterVO.getUName());
            arrtDto.setName(filterVO.getUName());
            String type = String.format("%s Attributes", filterVO.getUName());
            arrtDto.setType(type);
            arrtDto.setDisplayType(type);
            arrtDto.setTablename(attrTableName);
            ExportUtils.impexComponentList.add(arrtDto);
        });
    }
}
