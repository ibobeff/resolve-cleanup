/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.resolve.persistence.model.SysAppModule;
import com.resolve.persistence.model.SysAppModuleroles;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;

public class ExportMenuItem extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String menuItemTitle;
    protected String menuSectionName;
    protected String sysAppAppColumn;
    protected String sql;
    protected Map<String, Object> queryParams = new HashMap<String, Object>();
    
    public ExportMenuItem(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    public void export()
    {
        String menuItemTitle = impexGlideVO.getUModule();
        String menuItemGroup = impexGlideVO.getUName();
        String menuDefTitle = null;
        String menuDefName = null;
        
        if (menuItemTitle.contains("::") || StringUtils.isBlank(menuItemGroup) || menuItemGroup.contains("&nbsp"))
        {
            // Old definition
            if (menuItemTitle.contains("::"))
            {
                menuDefName = menuItemTitle.split("::")[0];
                menuItemTitle = menuItemTitle.split("::")[1];
                menuItemGroup = impexGlideVO.getUName();
                
                //sql = "from SysAppModule where title = '" + menuItemTitle + "' and sysAppApplication.name = '" + menuDefName + "'";
                sql = "from SysAppModule where title = :menuItemTitle and sysAppApplication.name = :menuDefName";
                
                queryParams.put("menuItemTitle", menuItemTitle);
                queryParams.put("menuDefName", menuDefName);
                
                if (StringUtils.isNotBlank(menuItemGroup))
                {
                    //sql = sql + " and group = '" + menuItemGroup + "'";
                    sql = sql + " and appModuleGroup = :menuItemGroup";
                    queryParams.put("menuItemGroup", menuItemGroup);
                }
            }
            else if (StringUtils.isBlank(menuItemGroup) || menuItemGroup.contains("&nbsp"))
            {
                menuItemTitle = impexGlideVO.getUModule();
                //sql = "from SysAppModule as SM where SM.title = '" + menuItemTitle + "' and (SM.group is null OR SM.group = '&nbsp')";
                sql = "from SysAppModule as SM where SM.title = :menuItemTitle and (SM.appModuleGroup is null OR SM.appModuleGroup = '&nbsp')";
                queryParams.put("menuItemTitle", menuItemTitle);
            }
        }
        else
        {
            sysAppAppColumn = "sysAppApplication.title";
            if (impexGlideVO.getUName().contains("::"))
            {
                menuItemGroup = impexGlideVO.getUName().split("::")[0];
                menuDefTitle = impexGlideVO.getUName().split("::")[1];
                //sql = "from SysAppModule where title = '" + menuItemTitle + "' and sysAppApplication.title = '" + menuDefTitle + "' and group = '" + menuItemGroup + "'";
                sql = "from SysAppModule where title = :menuItemTitle and sysAppApplication.title = :menuDefTitle and appModuleGroup = :menuItemGroup";
                queryParams.put("menuItemTitle", menuItemTitle);
                queryParams.put("menuDefTitle", menuDefTitle);
                queryParams.put("menuItemGroup", menuItemGroup);
            }
            else
            {
                menuDefTitle = impexGlideVO.getUName();
                //sql = "from SysAppModule where title = '" + menuItemTitle + "' and sysAppApplication.title = '" + menuDefTitle + "'";
                sql = "from SysAppModule where title = :menuItemTitle and sysAppApplication.title = :menuDefTitle";
                queryParams.put("menuItemTitle", menuItemTitle);
                queryParams.put("menuDefTitle", menuDefTitle);
            }
        }
        
        
//        if (menuItemAndSection.contains("("))
//        {
//            sysAppAppColumn = "sysAppApplication.title";
//            menuItemTitle = menuItemAndSection.substring(0, menuItemAndSection.indexOf('(') - 1).trim();
//            menuSectionName = menuItemAndSection.substring(menuItemAndSection.indexOf('(') + 1, menuItemAndSection.length() - 1);
//        }
//        else if (menuItemAndSection.contains("::"))
//        {
//            // backward compatibility
//            sysAppAppColumn = "sysAppApplication.name";
//            String[] arr = menuItemAndSection.split("::");
//            menuSectionName = arr[0];
//            menuItemTitle = arr[1];
//        }
//        else
//        {
//            menuItemTitle = menuItemAndSection;
//        }     
        
        SysAppModule appModule = getMenuItem();
        if (appModule != null)
        {
            populateAppModule(appModule);
            
            if (appModule.getSysAppModuleroles() != null)
            {
                populateMenuItemRoles(appModule.getSysAppModuleroles());
            }
        }
    }
    
    private void populateAppModule(SysAppModule appModule)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysAppModule");
        String fullName = impexGlideVO.getUModule();
        dto.setSys_id(appModule.getSys_id());
        dto.setFullName(fullName);
        dto.setModule("");
        dto.setName(fullName);
        dto.setType(MENU_ITEM);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private SysAppModule getMenuItem()
    {       
        try
        {
            return (SysAppModule) HibernateProxy.execute(() -> {
            	SysAppModule appModule = null;
            	 List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                 if(result != null && result.size() > 0)
                 {
                     appModule = (SysAppModule)result.get(0);
                     
                     if (appModule != null)
                     {
                         Collection<SysAppModuleroles> appModuleRoleColl = appModule.getSysAppModuleroles();
                         if (appModuleRoleColl != null)
                         {
                             appModuleRoleColl.size();
                         }
                     }
                 }
                 
                 return appModule;
            });
            
           
        }
        catch(Exception e)
        {
            Log.log.error("Error while executing sql:" + sql, e);
                      HibernateUtil.rethrowNestedTransaction(e);
            return null;
        }        
        
    }
}
