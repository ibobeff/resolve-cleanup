/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.RBGeneral;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.RBConditionVO;
import com.resolve.services.hibernate.vo.RBConnectionParamVO;
import com.resolve.services.hibernate.vo.RBConnectionVO;
import com.resolve.services.hibernate.vo.RBCriterionVO;
import com.resolve.services.hibernate.vo.RBIfVO;
import com.resolve.services.hibernate.vo.RBLoopVO;
import com.resolve.services.hibernate.vo.RBTaskConditionVO;
import com.resolve.services.hibernate.vo.RBTaskSeverityVO;
import com.resolve.services.hibernate.vo.RBTaskVO;
import com.resolve.services.hibernate.vo.RBTaskVariableVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.rb.util.RBTree;
import com.resolve.services.rb.util.ResolutionBuilderUtils;
import com.resolve.util.Log;

public class ExportAutomationBuilder extends ExportComponent
{
    protected String name;
    protected String namespace;
    protected String wikiSysId;
    protected Set<String> wikiDocNames = new HashSet<String>();
    
    public ExportAutomationBuilder(String wikiSysId) throws Exception
    {
        this.wikiSysId = wikiSysId;
    }
    
    public void export()
    {
        StringBuilder hql = new StringBuilder();
        hql.append("from RBGeneral ");
        //hql.append(" where wikiId = '").append(wikiSysId).append("'");
        hql.append(" where wikiId = :wikiSysId");
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("wikiSysId", wikiSysId);
        
        List<? extends Object> list = null;
        try
        {
            list = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
        }
        catch (Throwable t)
        {
            String message = String.format("Error while reading RB General for wiki sysId: %s", wikiSysId);
            Log.log.error(message, t);
            ImpexUtil.logImpexMessage(message, true);
        }
        
        if (list != null && list.size() > 0)
        {
            RBGeneral rbGeneral = (RBGeneral)list.get(0);
            name = rbGeneral.getNamespace() + "." + rbGeneral.getName();
            populateRBGeneral(rbGeneral);
            
            RBTree tree = ResolutionBuilderUtils.getRBTree(rbGeneral.getSys_id(), userName);
            if (tree != null)
            {
                List<Object> chidren = tree.getChildren();
                findChildAndExport(chidren);
            }
        }
    }
    
    public Set<String> getWikiDocNames()
    {
        return wikiDocNames;
    }
    
    private void populateRBGeneral(RBGeneral rbGeneral)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBGeneral");
        
        dto.setSys_id(rbGeneral.getSys_id());
        dto.setFullName("AB: " + name);
        dto.setName(rbGeneral.getName());
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void findChildAndExport(List<Object> chidren)
    {
        if (chidren != null && chidren.size() > 0)
        {
            for (Object obj : chidren)
            {
                if (obj instanceof RBTree)
                {
                    RBTree localTree = (RBTree)obj;
                    
                    findChildAndExport(localTree.getChildren());
                    
                    if (localTree.getData() != null)
                    {
                        checkInstanceAndExport(localTree.getData());
                    }
                }
                else
                {
                    checkInstanceAndExport(obj);
                }
            }
        }
    }
    
    private void checkInstanceAndExport(Object object)
    {
        System.out.println(object.getClass().getName());
        if (object instanceof RBTaskVO)
        {
            RBTaskVO taskVO = (RBTaskVO)object;
            populateRBTask(taskVO);
            if (taskVO.getVariables() != null && taskVO.getVariables().size() > 0)
            {
                for (RBTaskVariableVO rbTaskVariableVO : taskVO.getVariables())
                {
                    populateRBTaskVariableVO(rbTaskVariableVO);
                    System.out.println(rbTaskVariableVO.getClass().getName());
                }
            }
            
            if (taskVO.getSeverities() != null && taskVO.getSeverities().size() > 0)
            {
                for (RBTaskSeverityVO rbTaskSeverityVO : taskVO.getSeverities())
                {
                    populateRBTaskSeverityVO(rbTaskSeverityVO, taskVO.getName());
                    System.out.println(rbTaskSeverityVO.getClass().getName());
                }
            }
            
            if (taskVO.getConditions() != null && taskVO.getConditions().size() > 0)
            {
                for (RBTaskConditionVO rbTaskConditionVO : taskVO.getConditions())
                {
                    populateRBTaskConditionVO(rbTaskConditionVO, taskVO.getName());
                    System.out.println(rbTaskConditionVO.getClass().getName());
                }
            }
            
            if (taskVO.getRefRunbook() != null)
            {
                String wikiFullname = taskVO.getName();
                wikiDocNames.add(wikiFullname);
            }
        }
        
        if (object instanceof RBConnectionVO)
        {
            RBConnectionVO rbConnectionVO = (RBConnectionVO)object;
            populateRBConnectionVO(rbConnectionVO);
            if (rbConnectionVO.getParams() != null && rbConnectionVO.getParams().size() > 0)
            {
                for (RBConnectionParamVO connParamVO : rbConnectionVO.getParams())
                {
                    populateRBConnectionParamVO(connParamVO);
                    System.out.println(connParamVO.getClass().getName());
                }
            }
        }
        
        if (object instanceof RBIfVO)
        {
            RBIfVO rbIfVO = (RBIfVO)object;
            populateRBIf(rbIfVO);
        }
        
        if (object instanceof RBConditionVO)
        {
            RBConditionVO rbConditionVO = (RBConditionVO)object;
            populateRBCondition(rbConditionVO);
            
            if (rbConditionVO.getCriteria() != null && rbConditionVO.getCriteria().size() > 0)
            {
                for (RBCriterionVO rbCriteriaVO : rbConditionVO.getCriteria())
                {
                    populateRBCriterion(rbCriteriaVO);
                    System.out.println(rbCriteriaVO.getClass().getName());
                }
            }
        }
        
        if (object instanceof RBLoopVO)
        {
            RBLoopVO loopVO = (RBLoopVO)object;
            populateRBLoop(loopVO);
        }
    }
    
    private void populateRBTask(RBTaskVO taskVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBTask");
        
        dto.setSys_id(taskVO.getSys_id());
        dto.setFullName("AB TASK: " + taskVO.getName());
        dto.setName(taskVO.getName());
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBTaskVariableVO(RBTaskVariableVO rbTaskVariableVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBTaskVariable");
        
        dto.setSys_id(rbTaskVariableVO.getSys_id());
        dto.setFullName("AB TASK VARIABLE: " + rbTaskVariableVO.getName());
        dto.setName(rbTaskVariableVO.getName());
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBTaskSeverityVO(RBTaskSeverityVO rbTaskSeverityVO, String taskName)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBTaskSeverity");
        
        dto.setSys_id(rbTaskSeverityVO.getSys_id());
        dto.setFullName("SEVERITY FOR TASK: " + taskName);
        dto.setName("SEVERITY FOR TASK: " + taskName);
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBTaskConditionVO(RBTaskConditionVO rbTaskConditionVO, String taskName)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBTaskCondition");
        
        dto.setSys_id(rbTaskConditionVO.getSys_id());
        dto.setFullName("CONDITION FOR TASK: " + taskName);
        dto.setName("CONDITION FOR TASK: " + taskName);
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBConnectionVO (RBConnectionVO rbConnectionVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBConnection");
        
        dto.setSys_id(rbConnectionVO.getSys_id());
        dto.setFullName("CONNECTION FOR AB: " + name);
        dto.setName(rbConnectionVO.getName());
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBConnectionParamVO(RBConnectionParamVO connParamVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBConnectionParam");
        
        dto.setSys_id(connParamVO.getSys_id());
        dto.setFullName("CONNECTION PARAM FOR AB " + name + ": " + connParamVO.getName());
        dto.setName(connParamVO.getName());
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBIf(RBIfVO rbIfVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBIf");
        
        dto.setSys_id(rbIfVO.getSys_id());
        dto.setFullName("IF CONDITION FOR AB: " + name);
        dto.setName("IF CONDITION");
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBCondition(RBConditionVO rbConditionVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBCondition");
        
        dto.setSys_id(rbConditionVO.getSys_id());
        dto.setFullName("CONDITION FOR AB: " + name);
        dto.setName("CONDITION");
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBCriterion(RBCriterionVO rbCriteriaVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBCriterion");
        
        dto.setSys_id(rbCriteriaVO.getSys_id());
        dto.setFullName("CRITERIA FOR AB: " + name);
        dto.setName("CRITERIA");
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateRBLoop(RBLoopVO loopVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RBLoop");
        
        dto.setSys_id(loopVO.getSys_id());
        dto.setFullName("LOOP FOR AB: " + name);
        dto.setName("LOOP");
        dto.setType("AB");
        dto.setDisplayType("AB");
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
}
