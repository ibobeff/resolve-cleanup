/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.persistence.model.ResolveCatalog;
import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceCatalog;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.catalog.CatalogUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ImportCatalogs
{
    private String moduleFolderLocation;
    private String username;
    private File[] catalogFilesToImport = null;
    private Set<String> catalogs = new HashSet<String>();

    public ImportCatalogs(String moduleFolderLocation, String username)
    {
        this.moduleFolderLocation = moduleFolderLocation;
        this.username = username;
    }
    
    public void importCatalogs()
    {
        try 
        {
            prepareImport();
            
            if(catalogFilesToImport != null && catalogFilesToImport.length > 0)
            {
            	try {
            		ImpexUtil.initImpexOperationStage("IMPORT", "(41/51) Import Catalogs", catalogFilesToImport.length);
	                //process each catalog first
	                for(File catalogFile : catalogFilesToImport)
	                {
	                	ImpexUtil.updateImpexCount("IMPORT");
	                    try
	                    {
	                        importCatalog(catalogFile);
	                    }
	                    catch (Exception e)
	                    {
	                        Log.log.error("Error while importing catalog: " + catalogFile.getName(), e);
	                        ImpexUtil.logImpexMessage("Error while importing catalog: " + e.getMessage(), true);
	                    }
	                }
            	} catch (Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Failed to Import Catalogs.", e);
            		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
							  		  	  	  e.getMessage() : "Failed to Import Catalogs. Please check RSView logs for details", 
							  		  	  	  true);
            	}
                
                //update the relationships for catalog to catalog reference
                
                Map<String, Object> whereClauseParams = new HashMap<String, Object>();
                
                String whereClause = prepareUPathSqlString(whereClauseParams) + " and rcn.UType = 'CatalogReference' and rcn.UIsRootRef = true";
                Set<String> sysIdsToUpdateReferences = GeneralHibernateUtil.getSysIdsFor("ResolveCatalogNode", "rcn", whereClause , whereClauseParams, "system");
                if(CollectionUtils.isNotEmpty(sysIdsToUpdateReferences))
                {
                	try {
                		ImpexUtil.initImpexOperationStage("IMPORT", "(42/51) Update Catalog Sys Ids", sysIdsToUpdateReferences.size());
	                    for(String sysId : sysIdsToUpdateReferences)
	                    {
	                    	ImpexUtil.updateImpexCount("IMPORT");
	                        try
	                        {
	
                           HibernateProxy.setCurrentUser("system");
	                        	HibernateProxy.execute( () -> {
	                        		ResolveCatalogNode model = HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().findById(sysId);
		                            if(model != null)
		                            {
		                                String path = model.getUPath();
		                                String refCatalogName = path.substring(path.lastIndexOf('/')+1);
		                                
		                                ResolveCatalog catalog = CatalogUtil.findCatalogModel(null, refCatalogName, "system");
		                                if(catalog != null)
		                                {
		                                    model.setUReferenceCatalogSysId(catalog.getSys_id());
		                                }
		                            }
	                        	});
	                        }
	                        catch (Exception e)
	                        {
	                            Log.log.warn(e.getMessage(), e);
                                                        HibernateUtil.rethrowNestedTransaction(e);
	                        }
	                    }//end of for loop
                	} catch (Exception e) {
                		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
                					  e.getMessage() : "Failed to Update Catalog Sys Ids", e);
                		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
								  		  	  	  e.getMessage() : 
								  		  	  	  "Failed to Update Catalog Sys Ids. Please check RSView logs for details", true);
                	}
                }//end of if
            }
        }
        catch(Throwable t)
        {
            Log.log.error("Error in Importing Catalog from location:" + moduleFolderLocation, t);
        }
    }
    
    private void prepareImport() throws Exception
    {
        String socialFolder = moduleFolderLocation + "social/";
        File socialFolderLocation = FileUtils.getFile(socialFolder);
        if (socialFolderLocation.isDirectory())
        {
            FileFilter fileFilter = new WildcardFileFilter("catalog"+ "-*.json");
            catalogFilesToImport = socialFolderLocation.listFiles(fileFilter);
        }
    }
    
    
    private void importCatalog(File catalogFile) throws Exception
    {
        Catalog resolveCatalog = new ObjectMapper().readValue(FileUtils.readFileToString(catalogFile), Catalog.class);
        catalogs.add(resolveCatalog.getName());
        
        //save the catalog
        Catalog newCatalog = ServiceCatalog.saveCatalog(resolveCatalog, username);
        
        String oldCatalogSysId = resolveCatalog.getParentSysId();
        if (StringUtils.isBlank(oldCatalogSysId))
        {
            oldCatalogSysId = resolveCatalog.getId();
        }
        if (!newCatalog.getParentSysId().equals(oldCatalogSysId))
        {
            /*
             * If the imported catalog is not prsent on the system, Resolve creates a brand new catalog
             * with the same name and different sys_id. If this imported catalog is referenced in a WikiDoc,
             * the catalogId would not match. So, update the catalogId in WikiDocument table with the newly
             * created catalogId. 
             */
            WikiUtils.updateWikiDocCatalogRef(newCatalog.getParentSysId(), oldCatalogSysId);
        }
        
        ImpexUtil.logImpexMessage("Catalog: " + resolveCatalog.getName() + " imported.", false);
    }
    
    //set of catalog names to ==> (UPath like '/ddd%' OR UPath like '/www%')
    private String prepareUPathSqlString(Map<String, Object> whereClauseParams)
    {
        StringBuilder sb = new StringBuilder("(");
        
        Iterator<String> catalogsIt = catalogs.iterator();
        while(catalogsIt.hasNext())
        {
            String catalogName = catalogsIt.next();
            
            /*
             * Prefix catalog name with "cpn_" as Hibernate query parameter names have to be alpha numeric 
             * and catalog names can be numeric only.
             */
            String catalogParamName = "cpn_" + (catalogName.replaceAll("\\s+","")).replaceAll("-", "_");
            sb.append("rcn.UPath like :").append(catalogParamName);
            
            whereClauseParams.put(catalogParamName, catalogName + "%");
            
            if(catalogsIt.hasNext())
            {
                sb.append(" OR ");
            }
        }
        sb.append(")");
        
        return sb.toString();
    }
    
    
}
