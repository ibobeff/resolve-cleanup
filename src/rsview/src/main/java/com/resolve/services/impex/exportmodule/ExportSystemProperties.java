/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.List;

import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;

public class ExportSystemProperties extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    
    public ExportSystemProperties(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    public ManifestGraphDTO export()
    {
        ManifestGraphDTO parent = null;
        List<PropertiesVO> propertyList = PropertiesUtil.getPropertiesUsingWhereClause("UName = '" + impexGlideVO.getUModule() + "'" , userName);
        if (propertyList != null && propertyList.size() > 0)
        {
            PropertiesVO propertiesVO = propertyList.get(0);
            parent = new ManifestGraphDTO();
            parent.setId(propertiesVO.getSys_id());
            parent.setType(ExportUtils.SYSTEM_PROPERTY);
            parent.setName(propertiesVO.getUName());
            parent.setExported(true);
            parent.setChecksum(propertiesVO.getChecksum());
            populateProperty(propertiesVO);
        }
        return parent;
    }
    
    private void populateProperty(PropertiesVO propertiesVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.Properties");
        
        dto.setSys_id(propertiesVO.getSys_id());
        dto.setFullName(propertiesVO.getUName());
        dto.setModule("");
        dto.setName(propertiesVO.getUName());
        dto.setType(SYSTEM_PROPERTIES);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
}
