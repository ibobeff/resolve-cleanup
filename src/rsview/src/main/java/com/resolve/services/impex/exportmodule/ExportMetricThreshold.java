/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.MetricThreshold;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;

public class ExportMetricThreshold extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String ruleName;
    protected String ruleModule;
    
    public ExportMetricThreshold(ResolveImpexGlideVO impexGlideVO) throws Exception
    {
        this.impexGlideVO = impexGlideVO;
        this.ruleName = impexGlideVO.getUName();
        this.ruleModule = impexGlideVO.getUModule();
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for Metric Threshold " + ruleName);
        }
    }
    
	@SuppressWarnings("unchecked")
	public void export() {
		List<MetricThreshold> results = null;
		try {
			results = (List<MetricThreshold>) HibernateProxy.execute(() -> {
				String hql = " from MetricThreshold mt where lower(mt.URuleModule) = :namespace ";
				if (StringUtils.isNotBlank(ruleName)) {
					hql = hql + "and lower(mt.URuleName) = :rulename";
				}

				if (!impexOptions.isIncludeAllVersions()) {
					hql = hql + " and (UVersion is null OR UVersion = 0) ";
				}
				Query query = HibernateUtil.createQuery(hql);
				query.setParameter("namespace", ruleModule.toLowerCase());
				if (StringUtils.isNotBlank(ruleName)) {
					query.setParameter("rulename", ruleName.toLowerCase());
				}
				return query.list();
			});
		} catch (Exception e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (results != null) {
			for (MetricThreshold metricThreshold : results) {
				populateMetricThreshold(metricThreshold);
			}
		}
	}
    
    private void populateMetricThreshold(MetricThreshold metricThreshold)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.MetricThreshold");
        dto.setSys_id(metricThreshold.getSys_id());
        if (metricThreshold.getUVersion() == null || metricThreshold.getUVersion() == 0)
        {
        	dto.setFullName(metricThreshold.getURuleName() + " - Latest Revision");
        }
        else
        {
        	dto.setFullName(metricThreshold.getURuleName() + " - Version " + metricThreshold.getUVersion());
        }
        dto.setName(metricThreshold.getURuleName());
        dto.setDisplayType(METRIC_THRESHOLD);
        dto.setType(METRIC_THRESHOLD);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
}
