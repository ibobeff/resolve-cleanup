/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import com.resolve.execute.ExecuteOS;
import com.resolve.execute.ExecuteResult;
import com.resolve.persistence.model.ResolveImpexModule;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ImportReports
{
    private String moduleName = null;
    
    private String ZIP_FILE = null;
    private String DESTINATION_FOLDER = null;
    private String MODULE_FOLDER_LOCATION = null;
    private String REPORT_FOLDER_LOCATION = null;
    
    private File reportsFolderLocation = null;
    private ResolveImpexModule resolveImpexModule = null;

    public ImportReports(String moduleName)
    {
        this.moduleName = moduleName;
        
        DESTINATION_FOLDER = RSContext.getResolveHome() + "rsexpert/";
        
    }//ImportReports
    
    /**
     * 
     * @param csvModuleNames - module1,module2
     */
    @Deprecated
    public static void importReportsOfModules(String csvModuleNames)
    {
        if(StringUtils.isNotEmpty(csvModuleNames))
        {
            String[] moduleNames = csvModuleNames.split(",");
            for(String moduleName : moduleNames)
            {        
                Log.log.debug("*******  MODULE TO BE IMPORTED :" + moduleName);

                try
                {
                    //new ImportReports(moduleName.trim()).importReports();
                }
                catch(Exception e)
                {
                    Log.log.error("Error importing reports for module:" + moduleName, e);
                }
            }
        }
    }
    
    @Deprecated
    public void importReports() throws Exception 
    {
        if(StringUtils.isNotEmpty(moduleName))
        {
            ZIP_FILE = moduleName + ".zip";
            
            Log.log.debug("*******  START TO DOWNLOAD THE FILE");
            
            // Read file from DB
            readFileFromDB();
            
            Log.log.debug("*******  START TO UNZIP THE FILE");
            
            String folder = new Date().getTime() + "";
            DESTINATION_FOLDER = DESTINATION_FOLDER + folder + "/";
            //unzip the folder
            unzip(folder);
            
            Log.log.debug("*******  START TO IMPORT THE FILE");
            
            //import the reports
            importReportsPrivate();
            
            Log.log.debug("*******  START TO CLEANUP");
            
            //clean up
            cleanup(folder);
            
        }

    }//importReports
    
    @Deprecated
    private void importReportsPrivate()
    {
        reportsFolderLocation = FileUtils.getFile(DESTINATION_FOLDER);
        if(reportsFolderLocation.exists() && reportsFolderLocation.isDirectory())
        {
            //read only jar files for report
            WildcardFileFilter filter = new WildcardFileFilter("*.jar");
            
            Collection<File> files = FileUtils.listFiles(reportsFolderLocation, filter, TrueFileFilter.INSTANCE);
            
            for (File f : files)
            {
                if (f.isFile())
                {
                    try
                    {
                        //System.out.println(f.getAbsolutePath());
                        startReportImport(f.getAbsolutePath());
                    }
                    catch (Throwable e)
                    {
                        // ignore failures
                        Log.log.error("SKIPPING file: " + f.getName(), e);
                    }
                }
            }//end of for loop
        }//importReports
    }
    
    @Deprecated
    public static void startReportImport(String reportName)
    {
        final int TIMEOUT = 180;
        
        if (StringUtils.isNotEmpty(reportName))
        {
            // remove lock file - /opt/resolve/tomcat/webapps/sree/WEB-INF/classes/schedule.xml 
            File lockfile = FileUtils.getFile(RSContext.getResolveHome()+"/tomcat/webapps/sree/WEB-INF/classes/schedule.xml");
            if (lockfile.exists())
            {
                lockfile.delete();
            }
            
            String osname = System.getProperty("os.name").toLowerCase();
            if (osname.equals("linux") || osname.equals("sunos") || osname.equals("aix"))
            {
                String filename;
                if (reportName.endsWith(".jar"))
                {
                    filename = reportName;
                }
                else
                {
                    filename = RSContext.getResolveHome()+"/rsexpert/"+reportName+".jar"; 
                }
                
                // execute dashboard.sh IMPORTASSET -DREPORT=$DIST/rsexpert/reportname.jar
                String command = "/bin/bash "+RSContext.getResolveHome()+"/bin/dashboard.sh IMPORTASSET -DREPORT="+filename;
                ExecuteOS os = new ExecuteOS(RSContext.getResolveHome()+"/bin");
                ExecuteResult result = os.execute(command, null, true, TIMEOUT, "IMPORTASSETS: "+reportName);
                String out = result.getResultString();
                if (out != null)
                {
                    Log.log.info(out);
                }
            }
            
            // windows
            else
            {
                String filename;
                if (reportName.endsWith(".jar"))
                {
                    filename = reportName.replace('/', '\\');
                }
                else
                {
                    filename = RSContext.getResolveHome()+"\\rsexpert\\"+reportName+".jar"; 
                }
                
                // execute dashboard.bat IMPORTASSET -DREPORT=$DIST\rsexpert\reportname.jar
                String command = "cmd.exe /c \\\"\\\""+RSContext.getResolveHome()+"bin\\dashboard.bat\\\" IMPORTASSET \\\"-DREPORT="+filename + "\\\"\\\"";
                ExecuteOS os = new ExecuteOS(RSContext.getResolveHome()+"\\bin");
                ExecuteResult result = os.execute(command, null, true, TIMEOUT, "IMPORTASSETS: "+reportName);
                String out = result.getResultString();
                if (out != null)
                {
                    Log.log.info(out);
                }
            }
        }
    }
    
    private void readFileFromDB() throws Exception 
    {
        resolveImpexModule = ImpexUtil.getImpexModel(null, moduleName, false);
        if (resolveImpexModule != null && resolveImpexModule.getUZipFileContent() != null)
        {
            ZIP_FILE = DESTINATION_FOLDER + resolveImpexModule.getUZipFileName();
            File zipFileContent = FileUtils.getFile(ZIP_FILE);
            try 
            {
                FileUtils.writeByteArrayToFile(zipFileContent, resolveImpexModule.getUZipFileContent());
            }
            catch (IOException e)
            {
                String error = "Could not read content of the zip file:" + resolveImpexModule.getUZipFileName();
                Log.log.error(error, e);
                throw new Exception(error);
            }
            
            MODULE_FOLDER_LOCATION = DESTINATION_FOLDER + resolveImpexModule.getUName() + "/";
            REPORT_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.REPORT_FOLDER;
        }
    }
    
    private void unzip(String folder) throws Exception
    {
        ImpexUtil.extractFile("", DESTINATION_FOLDER, RSContext.getResolveHome() + "rsexpert/" + ZIP_FILE, ".jar");
        
    }//unzip
    
    private void cleanup(String folder) 
    {
        //delete the unzipped folder
        ImpexUtil.cleanup(DESTINATION_FOLDER, ZIP_FILE);
        
    }//cleanup

}
