/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.CronUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.StringUtils;

public class ExportCronJob extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String jobName;
    protected String runbookName;
    
    public ExportCronJob(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
        this.jobName = impexGlideVO.getUName();
    }
    
    public ManifestGraphDTO export()
    {
        ManifestGraphDTO parent = null;
        ResolveCronVO cronVO = CronUtil.findCronJob(null, jobName, userName);
        if (cronVO == null)
        {
            ImpexUtil.logImpexMessage("Cron Job: " + jobName + " could not be found.", true);
        }
        else
        {
            parent = new ManifestGraphDTO();
            parent.setId(cronVO.getSys_id());
            parent.setType(ExportUtils.SCHEDULER);
            parent.setName(cronVO.getUName());
            parent.setExported(true);
            parent.setChecksum(cronVO.getChecksum());
            populateCronJob(cronVO);
            String runbook = cronVO.getURunbook();
            if (StringUtils.isNotBlank(runbook) && !runbook.contains("#")) {
                this.runbookName = runbook;
            }
        }
        return parent;
    }
    
    private void populateCronJob(ResolveCronVO cronVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.ResolveCron");
        dto.setSys_id(cronVO.getSys_id());
        dto.setFullName(cronVO.getUName());
        dto.setName(cronVO.getUName());
        dto.setDisplayType(SCHEDULER_JOB);
        dto.setType(SCHEDULER_JOB);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    public String getRunbookName() {
        return this.runbookName;
    }
}
