/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import com.resolve.services.ServiceTag;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;

public class ExportTagUtil extends ExportComponent
{
    private String tagName;
    
    public ExportTagUtil (String tagName, String userName)
    {
        this.tagName = tagName;
        this.userName = userName;
    }
    
    public ManifestGraphDTO export()
    {
        ResolveTagVO resolveTagVO = null;
        ManifestGraphDTO parent = null;
        try
        {
            resolveTagVO = ServiceTag.getTag(null, tagName, userName);
        }
        catch(Exception e)
        {
            Log.log.error(String.format("Could not get tag '%s' to export", tagName));
        }
        
        if (resolveTagVO != null)
        {
            parent = new ManifestGraphDTO();
            parent.setId(resolveTagVO.getSys_id());
            parent.setType(ExportUtils.TAG);
            parent.setName(resolveTagVO.getUName());
            parent.setExported(true);
            parent.setChecksum(resolveTagVO.getChecksum());
            exportTag(resolveTagVO);
        }
        return parent;
    }
    
    private void exportTag(ResolveTagVO tagVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveTag");
        dto.setSys_id(tagVO.getSys_id());
        dto.setFullName(tagVO.getUName());
        dto.setName(tagVO.getUName());
        dto.setGroup(TAG);
        dto.setType(TAG);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
}
