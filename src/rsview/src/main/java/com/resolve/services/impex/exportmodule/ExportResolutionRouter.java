package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.resolve.persistence.model.RRRule;
import com.resolve.persistence.model.RRSchema;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.RRRuleFieldVO;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.rr.util.ResolutionRoutingUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;

public class ExportResolutionRouter extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String rrName;
    protected String rrType;
    protected String rrModule;
    protected Set<String> wikiNames = new HashSet<>();
    protected RRSchemaVO schemaVO = null;
    
    public ExportResolutionRouter (ResolveImpexGlideVO impexGlideVO) throws Exception
    {
        this.impexGlideVO = impexGlideVO;
        // Import fail when RR mapping value field has apostrophe (') in its value field (http://10.20.2.111/browse/RBA-12841)
        // added .replace("'", "''") to fix that issue, validated on MySQL and Oracle, it should be working on all databases
        this.rrName = impexGlideVO.getUName();
        if (this.rrName != null && this.rrName.contains("'")) {
            this.rrName = this.rrName.replace("'", "''");
        }
        this.rrModule = impexGlideVO.getUModule();
        this.rrType = impexGlideVO.getUType();
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for ResolutionRouter: " + rrName);
        }
    }
    
    public ManifestGraphDTO exportSchema() {
        ManifestGraphDTO graphDTO = new ManifestGraphDTO();
        QueryDTO query = new QueryDTO();
        if (rrType.equals("rrSchema")) {
            query.setModelName("RRSchema");
            try {
                schemaVO = ResolutionRoutingUtil.getSchemaByName(rrName, userName);
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
            
            if (schemaVO != null) {
                graphDTO.setType(ExportUtils.RR_SCHEMA);
                graphDTO.setId(schemaVO.getSys_id());
                graphDTO.setName(schemaVO.getName());
                graphDTO.setExported(excludeList.contains(schemaVO.getSys_id()));
                graphDTO.setChecksum(schemaVO.getChecksum());
                populateRRSchema(schemaVO);
            }
        }
        return graphDTO;
    }
    
    public List<RRRuleVO> getSchemaMappingsToExport() {
        List<RRRuleVO> result = new ArrayList<>();
        List<RRRule> list = new ArrayList<>();
        try {
            HibernateUtil.action(hibernateUtil -> {
                RRSchema schema = HibernateUtil.getDAOFactory().getRRSchemaDAO().findById(schemaVO.getSys_id());
                schema.getRules().size();
                list.addAll(schema.getRules());
            }, userName);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        
        if (CollectionUtils.isNotEmpty(list)) {
            result = list.parallelStream().map(rule -> rule.doGetVO()).collect(Collectors.toList());
        }
        
        return result;
    }
    
    public ManifestGraphDTO exportSchemaMapping(RRRuleVO rrRuleVO) {
        populateRRRule(rrRuleVO);
        ManifestGraphDTO childGraphDTO = new ManifestGraphDTO();
        childGraphDTO.setType(ExportUtils.RID_MAPPING);
        childGraphDTO.setId(rrRuleVO.getSys_id());
        childGraphDTO.setName(rrRuleVO.getRid());
        childGraphDTO.setExported(!excludeList.contains(rrRuleVO.getSys_id()));
        childGraphDTO.setChecksum(rrRuleVO.getChecksum());
        
        List<RRRuleFieldVO> fieldVOList = rrRuleVO.getRidFields();
        if (fieldVOList != null && fieldVOList.size() > 0) {
            for (RRRuleFieldVO fieldVO : fieldVOList) {
                populateRRField(fieldVO);
            }
        }
        checkRRWikiExport(rrRuleVO);
        return childGraphDTO;
    }
    
    public List<RRRuleVO> getMappingsToExport() {
        List<RRRule> list = new ArrayList<>();
        RRRule rrRule = new RRRule();
        
        if (StringUtils.isBlank(rrName)) {
            rrRule.setModule(rrModule);
        } else {
            rrRule.setRid(rrName);
        }
        
        try {
            HibernateUtil.action(hibernateUtil -> {
                list.addAll(HibernateUtil.getDAOFactory().getRRRuleDAO().find(rrRule));
            }, userName);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        
        List<RRRuleVO> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(list)) {
            result = list.parallelStream().map(rule -> rule.doGetVO()).collect(Collectors.toList());
        }
        return result;
    }
    
    protected ManifestGraphDTO populateRRRule(RRRuleVO rrRuleVO)
    {
        ManifestGraphDTO graphDTO = new ManifestGraphDTO();
        ImpexComponentDTO dto = new ImpexComponentDTO(); 
        
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RRRule");
        dto.setSys_id(rrRuleVO.getSys_id());
        dto.setFullName(rrRuleVO.getModule() + "." + rrRuleVO.getRid());
        dto.setName(rrRuleVO.getRid());
        dto.setDisplayType(RR_RULE);
        dto.setType(RR_RULE);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        return graphDTO;
    }
    
    protected ManifestGraphDTO populateRRField(RRRuleFieldVO fieldVO)
    {
        ManifestGraphDTO graphDTO = new ManifestGraphDTO();
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RRRuleField");
        dto.setSys_id(fieldVO.getSys_id());
        dto.setFullName(fieldVO.getName());
        dto.setName(fieldVO.getName());
        dto.setDisplayType(RR_RULE_FIELD);
        dto.setType(RR_RULE_FIELD);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        return graphDTO;
    }
    
    protected ManifestGraphDTO populateRRSchema(RRSchemaVO schemaVO)
    {
        ManifestGraphDTO graphDTO = new ManifestGraphDTO();
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RRSchema");
        dto.setSys_id(schemaVO.getSys_id());
        dto.setFullName(schemaVO.getName());
        dto.setName(schemaVO.getName());
        dto.setDisplayType(RR_SCHEMA);
        dto.setType(RR_SCHEMA);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        return graphDTO;
    }
    
    protected void checkRRWikiExport(RRRuleVO rrRuleVO)
    {
        
        if (impexOptions.getIncludeUIDisplay())
        {
            if (StringUtils.isNotBlank(rrRuleVO.getWiki()))
            {
                wikiNames.add(rrRuleVO.getWiki());
            }
        }
        
        if (impexOptions.getIncludeUIAutomation())
        {
            if (StringUtils.isNotBlank(rrRuleVO.getAutomation()))
            {
                wikiNames.add(rrRuleVO.getAutomation());
            }
        }
        
        if (impexOptions.getIncludeGWAutomation())
        {
            if (StringUtils.isNotBlank(rrRuleVO.getRunbook()))
            {
                wikiNames.add(rrRuleVO.getRunbook());
            }
        }
        
        Boolean sir = rrRuleVO.getSir();
        if (sir != null && sir)
        {
            if (impexOptions.getIncludeSirPlaybook())
            {
                if (StringUtils.isNotBlank(rrRuleVO.getSirPlaybook()))
                {
                    wikiNames.add(rrRuleVO.getSirPlaybook());
                }
            }
        }
    }
    
    public Set<String> getWikiListToExport()
    {
        return wikiNames;
    }
}
