/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.List;

import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.WikiTemplateUtil;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;

public class ExportWikiTemplate extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String wikiFullName;
    protected List<ImpexComponentDTO> metaFormCompDTOList;
    
    public ExportWikiTemplate(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    public ManifestGraphDTO export() throws Throwable
    {
        String templateName = impexGlideVO.getUName();
        ManifestGraphDTO parent = null;
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for Wiki Template: " + templateName);
        }
        
        WikiDocumentMetaFormRelVO templateVO = WikiTemplateUtil.findWikiTemplateByIdOrByName(null, templateName, userName);
        if (templateVO != null)
        {
            parent = new ManifestGraphDTO();
            parent.setId(templateVO.getSys_id());
            parent.setType(ExportUtils.WIKI_TEMPLATE);
            parent.setName(templateVO.getUName());
            parent.setExported(true);
            
            populateWikiDocumentMetaFormRel(templateVO);
            populateWikiTemplateAccessRights(templateVO.getAccessRights());
            if (impexOptions.getTemplateWiki())
            {
                wikiFullName = templateVO.getUWikiDocName();
            }
            
            if (impexOptions.getTemplateForm())
            {
                ManifestGraphDTO formManifestDTO = new ManifestGraphDTO();
                formManifestDTO.setId(templateVO.getSys_id());
                formManifestDTO.setType(ImpexEnum.FORM.getValue());
                formManifestDTO.setName(templateVO.getUFormName());
                
                String formName = templateVO.getUFormName();
                ExportMetaFormViewUtil metaFormViewExportUtil = new ExportMetaFormViewUtil(formName, impexOptions, formManifestDTO);
                metaFormCompDTOList = metaFormViewExportUtil.exportMetaFormView(true);
                ExportUtils.populateFormChildIds(metaFormCompDTOList, formManifestDTO);
                parent.getChildren().add(formManifestDTO);
                //metaFormCompDTOList = ExportMetaFormViewUtil.exportForm(formName, impexOptions);
            }
        }
        else
        {
            ImpexUtil.logImpexMessage(String.format("Could not find Wiki Template %s",templateName), true);
        }
        
        return parent;
    }
    
    public String getWikiFullName()
    {
        return wikiFullName;
    }
    
    public List<ImpexComponentDTO> getMetaFormCompList()
    {
        return metaFormCompDTOList;
    }
    
    private void populateWikiDocumentMetaFormRel(WikiDocumentMetaFormRelVO templateVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.WikiDocumentMetaFormRel");
        
        dto.setType(WIKI_TEMPLATE);
        dto.setSys_id(templateVO.getSys_id());
        dto.setFullName(templateVO.getUName());
        dto.setModule("");
        dto.setName(templateVO.getUName());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateWikiTemplateAccessRights(AccessRightsVO accessRightsVO )
    {
        if (accessRightsVO != null)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.AccessRights");
            dto.setType(WIKI_TEMPLATE_ACCESSRIGHTS);
            dto.setFullName(accessRightsVO.getUResourceName());
            dto.setSys_id(accessRightsVO.getSys_id());
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
        }
    }
}
