/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaFormTab;
import com.resolve.persistence.model.MetaFormTabField;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.MetaSource;
import com.resolve.persistence.model.MetaxFieldDependency;
import com.resolve.persistence.model.MetaxFormViewPanel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * export prep for MetaFormView tables
 * 
 * @author jeet.marwah
 *
 */
public class ExportMetaFormViewUtil extends ExportComponent
{
    private List<MetaFormView> metaFormViews = new ArrayList<MetaFormView>();
    private ManifestGraphDTO manifestDTO;

    public ExportMetaFormViewUtil(String formName, ImpexOptionsDTO impexOptions, ManifestGraphDTO manifestDTO) throws Exception
    {
        this.impexOptions = impexOptions;
        this.manifestDTO = manifestDTO;
        findMetaFormViews(formName, null);

        if(metaFormViews.size() == 0)
        {
            //throw new Exception("Form View (" + formName + ") does not exist.");
            ImpexUtil.logImpexMessage("WARNING: Form View (" + formName + ") does not exist.", false);
        }
    }
    
    public ExportMetaFormViewUtil(CustomTable customTable, ImpexOptionsDTO impexOptions, ManifestGraphDTO manifestDTO) throws Throwable
    {
        this.impexOptions = impexOptions;
        this.manifestDTO = manifestDTO;
        
        findMetaFormViews(null, customTable.getUModelName());

        if(metaFormViews.size() == 0)
        {
            // throw new Exception("No Form Views for custom table (" + customTable.getUModelName() + ") exist.");
            ImpexUtil.logImpexMessage("No Form Views for custom table (" + customTable.getUModelName() + ") exist.", false);
        }
    }
    
	public List<ImpexComponentDTO> exportMetaFormView(boolean fromForm) throws Throwable {
		List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();

		try {
			HibernateProxy.execute(() -> {

				ExportCustomTableUtil exportCustomTable = null;
				for (MetaFormView metaFormView : metaFormViews) {
					list.addAll(exportMetaFormView(metaFormView, fromForm));
					if (impexOptions.getFormTables()) {
						String tableName = metaFormView.getUTableName();
						if (StringUtils.isNotBlank(tableName)) {
							if (exportCustomTable == null) {
								exportCustomTable = new ExportCustomTableUtil();
							}
							if (ExportUtils.tableNames == null) {
								ExportUtils.tableNames = new ArrayList<String>();
								ExportUtils.tableNames.add(tableName);
							} else {
								if (ExportUtils.tableNames.contains(tableName)) {
									continue;
								} else {
									ExportUtils.tableNames.add(tableName);
								}
							}
							CustomTable table = getCustomTableByName(tableName);

							if (table == null) {
								Log.log.warn(String.format("CustomTable, %s, belonging to Form '%s' does not exist.",
										tableName, metaFormView.getUFormName()));
								continue;
							}

							exportCustomTable.setImpexOptions(impexOptions);

							/*
							 * if the table is exported, then only create the manifest graph and attach it
							 * to it's parent, in this case the form.
							 */
							if (!excludeList.contains(table.getSys_id())) {
								if (fromForm) {
									/*
									 * If the request is initiated as form export, then only create a child (table
									 * node) and attach it to the parent (form). Otherwise, the parent graph (table)
									 * is already populated.
									 */
									ManifestGraphDTO graphDTO = new ManifestGraphDTO();
									graphDTO.setId(table.getSys_id());
									graphDTO.setType("table");
									graphDTO.setName(table.getUName());
									manifestDTO.getChildren().add(graphDTO);

									ExportUtils.customTabledefList.addAll(
											exportCustomTable.getMetaDataForCustomTable(table, graphDTO, false));
									ImpexComponentDTO compDTO = exportCustomTable.populateCustomTable();
									ExportUtils.customTabledefList.add(compDTO);
									exportRefTables(metaFormView.getUFormName(), exportCustomTable, graphDTO);
								} else {
									exportRefTables(metaFormView.getUFormName(), exportCustomTable, manifestDTO);
								}
							}
						}
					}
				} // end of for

			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
			if (t instanceof Error) {
				throw t;
			}
		}
        
        return list;
    }//exportMetaFormView
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //private methods
    private void exportRefTables(String formName, ExportCustomTableUtil exportCustomTable, ManifestGraphDTO graphDTO) {
        for (String tableName : ExportUtils.refTableNames) {
            if (ExportUtils.tableNames != null && !ExportUtils.tableNames.contains(tableName)) {
                ExportUtils.tableNames.add(tableName);
                CustomTable table = getCustomTableByName(tableName);
                if (table == null) {
                    Log.log.warn(String.format("CustomTable, %s, belonging to Form '%s' does not exist.", tableName, formName));
                    continue;
                }
                graphDTO.getIds().add(table.getSys_id());
                ExportUtils.customTabledefList.addAll(exportCustomTable.getMetaDataForCustomTable(table, graphDTO, false));
                ImpexComponentDTO compDTO = exportCustomTable.populateCustomTable();
                ExportUtils.customTabledefList.add(compDTO);
            }
        }
    }
    
    private CustomTable getCustomTableByName(String tableName)
    {
        CustomTable table = null;
        
        //String sql = "from CustomTable where LOWER(UName) = '" + tableName.trim().toLowerCase() + "'" ;
        String sql = "from CustomTable where LOWER(UName) = :UName" ;
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UName", tableName.trim().toLowerCase());
        
        try
        {
          HibernateProxy.setCurrentUser("admin");
            @SuppressWarnings("unchecked")
			List<? extends Object> list = (List<? extends Object>) HibernateProxy.execute(() -> {
            	return GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
            });
            if(list != null && list.size() > 0)
            {
                table = (CustomTable) list.get(0);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while executing sql:" + sql, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return table;
    }
    
    private void findMetaFormViews(String formName, String customTableName) throws Exception
    {
        // MetaFormView metaFormView = new MetaFormView();
        // metaFormView.setUFormName(formName);
        // metaFormView.setUTableName(customTableName);
        // metaFormViews = ImpexDAO.find(metaFormView);
        QueryDTO query = new QueryDTO();
        query.setModelName("MetaFormView");
        List<QueryFilter> filters = new ArrayList<>();
        
        if (StringUtils.isNotBlank(formName))
        {
            QueryFilter filter = new QueryFilter();
            filter.setType("auto");
            filter.setCondition(QueryFilter.EQUALS);
            filter.setField("UFormName");
            filter.setValue(formName);
            filters.add(filter);
        }
        else if (StringUtils.isNotBlank(customTableName))
        {
            QueryFilter filter = new QueryFilter();
            filter.setType("auto");
            filter.setCondition(QueryFilter.EQUALS);
            filter.setField("UTableName");
            filter.setValue(customTableName);
            filters.add(filter);
        } else {
            ImpexUtil.logImpexMessage("Both, form name and custom table name cannot be blank while fetching form to export.", true);
            return;
        }
        
        query.setFilterItems(filters);
        List<? extends Object> list = null;
        try {
            list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        } catch (Throwable t) {
            String message = String.format("Error while reading form for given " +
                            (StringUtils.isBlank(formName) ? "Custom Table, %s" : "Custom Form, %s"),
                            (StringUtils.isBlank(formName) ? customTableName : formName));
            Log.log.error(message, t);
            ImpexUtil.logImpexMessage(message, true);
        }
        
        if (CollectionUtils.isNotEmpty(list))
        {
            for (Object obj : list) {
                MetaFormView form = (MetaFormView)obj;
                /*
                 * form can be exported individually or it might get exported as part of a wiki.
                 * In the later case, if the wiki is exclude, form need not be exported. 
                 */
                if (manifestDTO.isExported() == false || excludeList.contains(form.getSys_id()))
                {
                    manifestDTO.setExported(false);
                }
                else
                {
                    metaFormViews.add(form);
                }
                if (manifestDTO.getName().equals(form.getUFormName())) {
                    manifestDTO.setChecksum(form.getChecksum());
                    manifestDTO.setId(form.getSys_id());
                }
            }
        }
        
    }//findMetaFormView
    
    private List<ImpexComponentDTO> exportMetaFormView(MetaFormView metaFormView, boolean fromForm) throws Exception
    {
    	List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        if (ExportUtils.formNames == null)
        {
            ExportUtils.formNames = new ArrayList<String>();
            ExportUtils.formNames.add(metaFormView.getUFormName());
        }
        else
        {
            if (ExportUtils.formNames.contains(metaFormView.getUFormName()))
            {
                return list;
            }
            else
            {
                ExportUtils.formNames.add(metaFormView.getUFormName());
            }
        }
        
        //need this for attaching the object back into this transaction
        metaFormView = HibernateUtil.getDAOFactory().getMetaFormViewDAO().findById(metaFormView.getSys_id());
        
        list.add(convertMetaFormViewToModel(metaFormView));
        if (fromForm) {
            ExportCustomTableUtil.setParentGraphDTO(manifestDTO);
        }
        //access rights
        list.addAll(ExportCustomTableUtil.convertMetaAccessRightsToModel(metaFormView.getMetaAccessRights()));
        
        //properties
//        list.addAll(ExportCustomTableUtil.convertMetaFormViewPropertiesToModel(metaFormView.getMetaFormViewProperties()));

        //panels and tabs
        Collection<MetaFormTab> metaFormTabs = metaFormView.getMetaFormTabs();
        if(metaFormTabs != null && metaFormTabs.size() > 0)
        {
            for(MetaFormTab metaFormTab : metaFormTabs)
            {
                list.addAll(exportMetaFormTab(metaFormTab));
            }
        }//end of if
        else if(metaFormView.getMetaxFormViewPanels() != null && metaFormView.getMetaxFormViewPanels().size() > 0)
        {
            for(MetaxFormViewPanel metaxFormViewPanel : metaFormView.getMetaxFormViewPanels())
            {
                list.addAll(exportMetaxFormViewPanel(metaxFormViewPanel));
            }
        }
        
        //meta source
        Collection<MetaSource> metaSources = metaFormView.getMetaSources();
        if(metaSources != null && metaSources.size() > 0)
        {
            for(MetaSource metaSource : metaSources)
            {
                list.addAll(exportMetaSource(metaSource));
            }//end of for
        }//end if
        
        //control
        list.addAll(ExportCustomTableUtil.convertMetaControlToModel(metaFormView.getMetaControl(), impexOptions));
        
//        if (impexOptions.getFormSS())
//        {
//            exportFormSystemScript(metaFormView.getMetaControl());
//        }
        
        return list;
    }//exportMetaFormView
    
    private ImpexComponentDTO convertMetaFormViewToModel(MetaFormView metaFormView)
    {
        String tableName = HibernateUtil.class2Table(metaFormView.getClass().getName());

        ImpexComponentDTO dto = new ImpexComponentDTO();
        dto.setSys_id(metaFormView.getSys_id());
        dto.setFullName(metaFormView.getUFormName());
        dto.setModule("");
        dto.setName(metaFormView.getUFormName());
        dto.setType(METAFORMVIEW);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        return dto;
        
    }//convertMetaFormViewToModel
    
    private List<ImpexComponentDTO> exportMetaFormTab(MetaFormTab metaFormTab) throws Exception
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        String tableName = HibernateUtil.class2Table(metaFormTab.getClass().getName());
        list.add(ExportUtils.createDBComponent(metaFormTab.getSys_id(), tableName));
        
        //tab field
        Collection<MetaFormTabField> metaFormTabFields = metaFormTab.getMetaFormTabFields();
        if(metaFormTabFields != null && metaFormTabFields.size() > 0)
        {
            for(MetaFormTabField metaFormTabField : metaFormTabFields)
            {
                list.addAll(exportMetaFormTabField(metaFormTabField));
            }
        }
        
        //dependencies
        Collection<MetaxFieldDependency> metaxFieldDependencys = metaFormTab.getMetaxFieldDependencys();
        list.addAll(ExportCustomTableUtil.exportMetaxFieldDependencies(metaxFieldDependencys));
        
        return list;
    }//exportMetaFormTab
    

    
    private List<ImpexComponentDTO> exportMetaFormTabField(MetaFormTabField metaFormTabField) throws Exception
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        String tableName = HibernateUtil.class2Table(metaFormTabField.getClass().getName());
        list.add(ExportUtils.createDBComponent(metaFormTabField.getSys_id(), tableName));
        
        //metaProperties 
        list.addAll(new ExportCustomTableUtil().convertMetaFieldPropertiesToModel(metaFormTabField.getMetaFieldFormProperties(), false));
        
        
        return list;
    }//exportMetaFormTabField

    private List<ImpexComponentDTO> exportMetaSource(MetaSource metaSource) throws Exception
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        String tableName = HibernateUtil.class2Table(metaSource.getClass().getName());
        list.add(ExportUtils.createDBComponent(metaSource.getSys_id(), tableName));
        
        //metaProperties 
        ExportCustomTableUtil exportCustomTableUtil = new ExportCustomTableUtil();
        exportCustomTableUtil.setImpexOptions(impexOptions);
        list.addAll(exportCustomTableUtil.convertMetaFieldPropertiesToModel(metaSource.getMetaFieldProperties(), false));
        
        return list;
    }//exportMetaSource
    
    private List<ImpexComponentDTO> exportMetaxFormViewPanel(MetaxFormViewPanel metaxFormViewPanel) throws Exception
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        String tableName = HibernateUtil.class2Table(metaxFormViewPanel.getClass().getName());
        list.add(ExportUtils.createDBComponent(metaxFormViewPanel.getSys_id(), tableName));
        
        //tabs
        Collection<MetaFormTab> metaFormTabs = metaxFormViewPanel.getMetaFormTabs();
        if(metaFormTabs != null && metaFormTabs.size() > 0)
        {
            for(MetaFormTab metaFormTab : metaFormTabs)
            {
                list.addAll(exportMetaFormTab(metaFormTab));
            }
        }//end of if        
        
        return list;
        
    }
    
//    private void exportFormSystemScript(MetaControl metaControl)
//    {
//        if(metaControl != null)
//        {
//            Collection<MetaControlItem> metaControlItems = metaControl.getMetaControlItems();
//            if (metaControlItems != null && metaControlItems.size() > 0)
//            {
//                for (MetaControlItem metaControlItem : metaControlItems)
//                {
//                    if (metaControlItem.getMetaFormActions() != null)
//                    {
//                        Collection<MetaFormAction> formActionColl = metaControlItem.getMetaFormActions();
//                        for (MetaFormAction formAction : formActionColl)
//                        {
//                            if (formAction.getUOperation().equalsIgnoreCase("script"))
//                            {
//                                ResolveImpexGlideVO glideVO = new ResolveImpexGlideVO();
//                                glideVO.setUModule(formAction.getUScriptName());
//                                
//                                ExportSystemScript exportSystemScript = new ExportSystemScript(glideVO);
//                                exportSystemScript.setUserName(userName);
//                                exportSystemScript.export();
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
}
