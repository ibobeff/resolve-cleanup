/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.persistence.model.ArtifactConfiguration;
import com.resolve.persistence.model.CustomDictionaryItem;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ArtifactConfigurationUtil;
import com.resolve.services.hibernate.util.ArtifactTypeUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.vo.ArtifactTypeVO;
import com.resolve.services.hibernate.vo.CEFDictionaryItemVO;
import com.resolve.services.hibernate.vo.CustomDictionaryItemVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.util.Log;

public class ExportCefType extends ExportComponent {
    private ResolveImpexGlideVO impexGlideVO;
    private String cefType;
    private Set<String> automationNames = new HashSet<>();
    private ArtifactTypeVO typeVO;
    
    public ExportCefType(ResolveImpexGlideVO impexGlideVO) {
        this.impexGlideVO = impexGlideVO;
        this.cefType = impexGlideVO.getUName();
    }
    
    public ManifestGraphDTO exportCefType() {
        ManifestGraphDTO parent = new ManifestGraphDTO();
        
        typeVO = ArtifactTypeUtil.getArtifactTypeByName(cefType);
        if (typeVO != null) {
            parent.setName(cefType);
            parent.setChecksum(typeVO.getChecksum());
            parent.setId(typeVO.getSys_id());
            parent.setType(ExportUtils.ARTIFACT_TYPE);
            if (typeVO.getUStandard()) { // need not export standard type.
                parent.setExported(false);
                excludeList.add(typeVO.getSys_id());
            } else {
                parent.setExported(true);
                populateCefType(typeVO);
            }
        }
        return parent;
    }
    
    public List<ManifestGraphDTO> processKeys() {
        List<ManifestGraphDTO> keyGraphList = processKeys(typeVO);
        return keyGraphList;
    }
    
    private void populateCefType(ArtifactTypeVO typeVO) {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.ArtifactType");
        dto.setSys_id(typeVO.getSys_id());
        dto.setFullName(typeVO.getUName());
        dto.setName(typeVO.getUName());
        dto.setDisplayType(ExportUtils.ARTIFACT_TYPE);
        dto.setType(ExportUtils.ARTIFACT_TYPE);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
    }
    
    private List<ManifestGraphDTO> processKeys(ArtifactTypeVO typeVO) {
        List<ManifestGraphDTO> keyGraphList = new ArrayList<>();
        Set<CEFDictionaryItemVO> cefItemSet = typeVO.getCefItems();
        if (CollectionUtils.isNotEmpty(cefItemSet)) {
            cefItemSet.stream().forEach(key -> processCefTypeKeys(key, keyGraphList));
        }
        Set<CustomDictionaryItemVO> customItemSet = null;
        if (impexOptions.isIncludeCustomKeys()) { // include all custom cef keys
            customItemSet = new HashSet<>();
            customItemSet.addAll(getAllCustomCefKeys());
        } else {
            customItemSet = typeVO.getCustomItems();
        }
        
        if (CollectionUtils.isNotEmpty(customItemSet)) {
            customItemSet.stream().forEach(key -> processCustomTypeKeys(key, keyGraphList));
        }
        return keyGraphList;
    }
    
    private List<CustomDictionaryItemVO> getAllCustomCefKeys() {
        List<CustomDictionaryItem> customKeys = new ArrayList<>();
        try {
            HibernateUtil.action(util -> {
                customKeys.addAll(HibernateUtil.getDAOFactory().getCustomDictionaryDAO().findAll());
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        List<CustomDictionaryItemVO> customKeysVO = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(customKeys)) {
            customKeysVO.addAll(customKeys.stream().map(key -> key.doGetVO()).collect(Collectors.toList()));
        }
        return customKeysVO;
    }
    
    private void processCefTypeKeys(CEFDictionaryItemVO keyVO, List<ManifestGraphDTO> keyGraphList) {
        ManifestGraphDTO keyGraph = new ManifestGraphDTO();
        keyGraph.setName(keyVO.getUShortName());
        keyGraph.setChecksum(keyVO.getChecksum());
        keyGraph.setId(keyVO.getSys_id());
        keyGraph.setType(ExportUtils.ARTIFACT_CEF_KEY);
        excludeList.add(keyVO.getSys_id()); // adding the cef key ids in exclude list 
        keyGraph.setExported(false);
        keyGraphList.add(keyGraph);
    }
    
    private void processCustomTypeKeys(CustomDictionaryItemVO keyVO, List<ManifestGraphDTO> keyGraphList) {
        ManifestGraphDTO keyGraph = new ManifestGraphDTO();
        keyGraph.setName(keyVO.getUShortName());
        keyGraph.setChecksum(keyVO.getChecksum());
        keyGraph.setId(keyVO.getSys_id());
        keyGraph.setType(ExportUtils.ARTIFACT_CEF_KEY);
        keyGraph.setExported(true);
        if (excludeList.contains(keyVO.getSys_id())) {
            keyGraph.setExported(false); // if the key is not exported, populate the graph and return.
            keyGraphList.add(keyGraph);
            return;
        }
        populateCustomKey(keyVO);
        keyGraphList.add(keyGraph);
    }
    
    private void populateCustomKey(CustomDictionaryItemVO customKey) {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.CustomDictionaryItem");
        dto.setSys_id(customKey.getSys_id());
        dto.setFullName(customKey.getUShortName());
        dto.setName(customKey.getUShortName());
        dto.setDisplayType(ExportUtils.ARTIFACT_CEF_KEY);
        dto.setType(ExportUtils.ARTIFACT_CEF_KEY);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
    }
    
    public List<ArtifactConfiguration> getCefActionsToExport() {
        return ArtifactConfigurationUtil.getConfigurationsByTypeId(typeVO.getSys_id(), "admin");
    }
    
    public ManifestGraphDTO processActifactAction(ArtifactConfiguration action) throws Exception {
        ManifestGraphDTO parent = new ManifestGraphDTO();
        parent.setType(ExportUtils.ARTIFACT_ACTION);
        parent.setId(action.getSys_id());
        parent.setChecksum(action.getChecksum());
        parent.setName(action.getUName());
        if (excludeList.contains(action.getSys_id())) {
            parent.setExported(false);
            return parent;
        } else {
            parent.setExported(true);
            populateArtifactAction(action);
            return parent;
        }
    }
    
    private void populateArtifactAction(ArtifactConfiguration action) {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.ArtifactConfiguration");
        dto.setSys_id(action.getSys_id());
        dto.setFullName(action.getUName());
        dto.setName(action.getUName());
        dto.setDisplayType(ExportUtils.ARTIFACT_ACTION);
        dto.setType(ExportUtils.ARTIFACT_ACTION);
        dto.setTablename(tableName);
        ExportUtils.impexComponentList.add(dto);
    }
    
    public ManifestGraphDTO exportTask(ResolveActionTask taskVO) throws Exception
    {
        ResolveImpexGlideVO impexGlideVO = new ResolveImpexGlideVO();
        impexGlideVO.setUName(taskVO.getUName());
        impexGlideVO.setUModule(taskVO.getUNamespace());
        impexGlideVO.setUOptions(this.impexGlideVO.getUOptions());
        
        ImpexOptionsDTO optionsDTO = new ObjectMapper().readValue(impexGlideVO.getUOptions(), ImpexOptionsDTO.class);
        optionsDTO.copyAtNsOptionsToAt();
        
        String impexOptionJson = new ObjectMapper().writer().writeValueAsString(optionsDTO);
        impexGlideVO.setUOptions(impexOptionJson);
        
        ExportActionTask atExport = ExportActionTask.getInstance();
        atExport.init(impexGlideVO);
        atExport.setUserName("admin");
        return atExport.exportActionTask(null);
    }
    
    public Set<String> getAutomationNames() {
        return automationNames;
    }
}
