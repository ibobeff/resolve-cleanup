/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/
package com.resolve.services.util;

import java.io.ByteArrayInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.AuthenticationCredentials;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.SSLConnectionContext;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.connectionpool.impl.SimpleAuthenticationCredentials;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.ConsistencyLevel;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;
import com.netflix.astyanax.query.IndexQuery;
import com.netflix.astyanax.retry.ConstantBackoff;
import com.netflix.astyanax.serializers.StringSerializer;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;
import com.resolve.rsbase.MainBase;
import com.resolve.rsbase.ConfigCAS;
import com.resolve.rsview.main.Main;
import com.resolve.search.APIFactory;
import com.resolve.search.ScrollCallback;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchException;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.Worksheet;
import com.resolve.search.worksheet.WorksheetIndexAPI;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class provides methods for migrations of data for any given release. In the beginning of a release
 * this class may have nothing in it. As we identify the source and target of migration we add the
 * corresponding methods here 
 */
public class MigrationUtil
{
    private static SearchAPI<Worksheet> worksheetSearchApi = APIFactory.getWorksheetSearchAPI(Worksheet.class);
    private static SearchAPI<ProcessRequest> processRequestSearchApi = APIFactory.getProcessRequestSearchAPI(ProcessRequest.class);

    private static Keyspace keyspace = null;
    
    public static void migrateWsdata()
    {
        if(keyspace == null)
        {
            keyspace = getResolveKeyspace();
        }
        
        ScrollCallback callback = new WsdataMigrator();
        try
        {
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            worksheetSearchApi.processDataByScrolling(queryBuilder, 1000, 60000, callback);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void migrateExecuteState()
    {
        if(keyspace == null)
        {
            keyspace = getResolveKeyspace();
        }
        
        ScrollCallback callback = new ExecuteStateMigrator();
        try
        {
            QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
            processRequestSearchApi.processDataByScrolling(queryBuilder, 1000, 60000, callback);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    private static class WsdataMigrator implements ScrollCallback
    {
        public <T> void process(T data)
        {
            try
            {
                Worksheet worksheet = (Worksheet) data;
                Map<String, Object> worksheetData = getWorksheetData(worksheet.getSysId());
                if(worksheetData != null && !worksheetData.isEmpty())
                {
                    Log.log.debug("Migrating WSDATA for worksheet: " + worksheet.getNumber());
                    WorksheetIndexAPI.indexWorksheetData(worksheet.getSysId(), worksheetData, "system");
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        public <T> void process(Collection<T> records)
        {
            throw new RuntimeException("This method is not yet implemented.");
        }
    }

    private static class ExecuteStateMigrator implements ScrollCallback
    {
        public <T> void process(T data)
        {
            try
            {
                if(data != null)
                {
                    ProcessRequest processRequest = (ProcessRequest) data;
                    
                    List<ExecuteState> executeStates = getExecuteState(processRequest.getSysId(), processRequest.getProblem());
                    for(ExecuteState executeState : executeStates)
                    {
                        Log.log.debug("Migrating ExecuteState data: " + executeState.toString());
                        WorksheetIndexAPI.indexExecuteState(executeState, "system");
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        public <T> void process(Collection<T> records)
        {
            throw new RuntimeException("This method is not yet implemented.");
        }
    }

    /**
     * Get worksheet WSDATA map from cassandra by worksheet's sysId (row key)
     *
     * @param problemid
     *            row key (sysId) of requested worksheet row
     * @return WSDATA Map of requested worksheet. Map is keyed by property names.
     *
     */
    private static Map<String, Object> getWorksheetData(String problemid)
    {
        Map<String, Object> result = new HashMap<String, Object>();

        if (StringUtils.isNotBlank(problemid))
        {
            if (findRowByKey(keyspace, problemid, getWorkSheetCF(), "sys_created_on") == null)
            {
                Log.log.trace("WSDATA get: worksheet doesn't exist: " + problemid);
                return null;
            }

            Map<String,Object> resultMap = findWorksheetData(keyspace, problemid);

            byte[] wsBytes = null;

            if (resultMap!=null)
            {
                wsBytes=(byte[])resultMap.get("u_wsdata");
            }

            if (wsBytes!=null)
            {
                ByteArrayInputStream bis = new ByteArrayInputStream(wsBytes);
                ObjectInput in = null;
                try {
                  in = new ObjectInputStream(bis);
                  result = (Map<String,Object>)in.readObject();
                }
                catch (Exception ex)
                {
                    Log.log.error("Failed to deserialize WSDATA from database", ex);
                    throw new RuntimeException(ex);
                }
                finally {
                    try
                    {
                        bis.close();
                        in.close();
                    }
                    catch (Exception ex)
                    {
                        //ignore
                    }
                }
            }
        }
        return result;
    }
    
    private static List<ExecuteState> getExecuteState(String processId, String problemId)
    {
        List<ExecuteState> result = new ArrayList<ExecuteState>();
        
        List<Row<String, String>> rows = getExecuteStateFromCassandra(keyspace, processId, problemId);
        if (rows!=null && !rows.isEmpty())
        {
            for(Row<String,String> row : rows)
            {
                String UParams = row.getColumns().getColumnByName("u_params").getStringValue();
    
                String wiki = null;
                String reference = null;
                String timeout = null;
                String processTimeout = null;
                String metricId = null;
                Map persistedParams = null;
                String targetAddr = null;
                String userid = null;
                String eventId = null;
                String eventReference = null;
                
                // params
                if (UParams != null)
                {
                    persistedParams = (Map) StringUtils.stringToObj(UParams);
                    if (persistedParams != null)
                    {
                        wiki = (String) persistedParams.get(Constants.EXECUTE_WIKI);
                        reference = (String) persistedParams.get(Constants.EXECUTE_REFERENCE);
                        timeout = (String) persistedParams.get(Constants.EXECUTE_TIMEOUT);
                        processTimeout = "10";
                        if(persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
                            processTimeout = (String) persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT); // optional
                        } else if (persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
                            processTimeout = "" + persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT);
                        }
                        metricId = (String) persistedParams.get(Constants.EXECUTE_METRICID);
    
                        eventId =  (String) persistedParams.get(Constants.EXECUTE_EVENTID);
                        eventReference = (String) persistedParams.get(Constants.EVENT_REFERENCE);
                        targetAddr = (String) persistedParams.get(Constants.EXECUTE_ADDRESS);
                        userid = (String) persistedParams.get(Constants.EXECUTE_USERID);
                    }
                    else
                    {
                        Log.log.debug("No parameter found for execute state process id: " + processId);
                    }
                }
                
                String tmpEventReference = row.getColumns().getColumnByName("u_reference") != null ? row.getColumns().getColumnByName("u_reference").getStringValue() : null;
                if (StringUtils.isNotBlank(tmpEventReference))
                {
                    reference = tmpEventReference;
                }
                String tmpTargetAddress = row.getColumns().getColumnByName("u_targetaddr") != null ? row.getColumns().getColumnByName("u_targetaddr").getStringValue() : null;
                if (StringUtils.isNotBlank(tmpTargetAddress))
                {
                    targetAddr = tmpTargetAddress;
                }
    
                String UFlows = row.getColumns().getColumnByName("u_flows").getStringValue(); 
                String UInputs = row.getColumns().getColumnByName("u_inputs").getStringValue();
                String debugStr = row.getColumns().getColumnByName("u_debug").getStringValue(); 
                
                //String sysId = row.getColumns().getColumnByName("sys_id").getStringValue();
                String sysId = row.getKey(); 
                String sysCreatedBy = row.getColumns().getColumnByName("sys_created_by").getStringValue(); 
                Long sysCreatedOn =  row.getColumns().getColumnByName("sys_created_on").getLongValue();
                String sysUpdatedBy = row.getColumns().getColumnByName("sys_updated_by").getStringValue(); 
                Long sysUpdatedOn =  row.getColumns().getColumnByName("sys_updated_on").getLongValue();
                
                ExecuteState executeState = new ExecuteState(sysId, null, "system");
                executeState.setSysCreatedBy(sysCreatedBy);
                executeState.setSysCreatedOn(sysCreatedOn);
                executeState.setSysUpdatedBy(sysUpdatedBy);
                executeState.setSysUpdatedOn(sysUpdatedOn);
                
                executeState.setDebug(debugStr);
                //executeState.setExecuteId(executeId);
                executeState.setFlows(UFlows);
                executeState.setInputs(UInputs);
                executeState.setParams(UParams);
                executeState.setProblemId(problemId);
                executeState.setProcessId(processId);
                if(StringUtils.isBlank(reference))
                {
                    executeState.setReference(eventReference.concat(eventId));
                }
                else
                {
                    executeState.setReference(reference);
                }
                executeState.setTargetAddress(targetAddr);
                // starttime
                long processStartTime = row.getColumns().getColumnByName("u_process_start_time").getLongValue();
                executeState.setProcessStartTime(processStartTime);
                String processState = "";
                if(row.getColumns().getColumnByName("u_process_state") != null)
                {
                    processState = row.getColumns().getColumnByName("u_process_state").getStringValue();
                }
                executeState.setProcessState(processState);
                
                result.add(executeState);
            }
        }        
        return result;
    }
    
    //Cassandra code
    
    public static String CL_ONE = "ONE";
    public static String CL_QUORUM = "QUORUM";
    public static String CL_LOCAL_QUORUM = "LOCAL_QUORUM";
    public static String CL_EACH_QUORUM = "EACH_QUORUM";
    public static String CL_ALL = "ALL";
    public static String CL_ANY = "ANY";
    public static String CL_TWO = "TWO";
    public static String CL_THREE = "THREE";

    private volatile static Keyspace ksp;
    private static ColumnFamily<String, String> worksheet_cf;
    private static ColumnFamily<String, String> executeState_cf;
    private static String clusterName = "resolve";
    private static String keyspaceName = "resolve";
    private static int maxConnsPerHost = 1;
    private static String seeds = "127.0.0.1:9160";
    private static int maxExhaustedTimeout = 10000; // ms
    private static int maxBlockedThreads = 200;
    private static String username = "admin";
    private static String p_assword;
    private static ConsistencyLevel readConsistency = ConsistencyLevel.CL_QUORUM;
    private static ConsistencyLevel writeConsistency = ConsistencyLevel.CL_QUORUM;
    private static boolean secure = false;
    private static String truststore = "cassandra/conf/.truststore";
    private static String truststoreP_assword;
    private static int retrySleepTimeMs = 500;
    private static int retryMaxAttempts = 20;

    public static Keyspace getResolveKeyspace()
    {
        Main main = (Main) MainBase.main;
        ConfigCAS configCAS = main.getConfigCAS();
        clusterName = configCAS.getCluster();
        keyspaceName = configCAS.getKeyspace();
        maxConnsPerHost = configCAS.getMaxConnsPerHost();
        seeds = configCAS.getSeeds();
        maxExhaustedTimeout = configCAS.getMaxExhaustedTimeout();
        maxBlockedThreads = configCAS.getMaxBlockedThreads();
        username = configCAS.getUsername();
        p_assword = configCAS.getP_assword();
        setReadConsistency(configCAS.getReadConsistency());
        secure = configCAS.getSecure();
        truststore = configCAS.getTruststore();
        truststoreP_assword = configCAS.getTruststoreP_assword();
        retrySleepTimeMs = configCAS.getRetrySleepTimeMs();
        retryMaxAttempts = configCAS.getRetryMaxAttempts();
        
        if (ksp != null) return ksp;
        
        synchronized (MigrationUtil.class)
        {
            if (ksp != null) return ksp;

            Log.log.info("New keyspace object: clustername=" + clusterName + ", keyspace name=" + keyspaceName + ", Max connections per host=" + maxConnsPerHost + ", Max blocked threads=" + maxBlockedThreads + ", Max exhausted timeout=" + maxExhaustedTimeout + ", seeds=" + seeds);
            AuthenticationCredentials authentication = new SimpleAuthenticationCredentials(username, p_assword);
            AstyanaxContext<Keyspace> context;
            if (secure)
            {
                context = new AstyanaxContext.Builder().forCluster(clusterName).forKeyspace(keyspaceName).withAstyanaxConfiguration(new AstyanaxConfigurationImpl().setDiscoveryType(NodeDiscoveryType.NONE).setDefaultReadConsistencyLevel(readConsistency).setDefaultWriteConsistencyLevel(writeConsistency).setCqlVersion("3.0.0").setTargetCassandraVersion("1.2"))
                                .withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("ResolveCASConnectionPool").setAuthenticationCredentials(authentication).setMaxConnsPerHost(maxConnsPerHost).setSeeds(seeds).setMaxTimeoutWhenExhausted(maxExhaustedTimeout).setMaxBlockedThreadsPerHost(maxBlockedThreads).setSSLConnectionContext(new SSLConnectionContext(truststore, truststoreP_assword))).withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
                                .buildKeyspace(ThriftFamilyFactory.getInstance());
            }
            else
            {
                context = new AstyanaxContext.Builder().forCluster(clusterName).forKeyspace(keyspaceName).withAstyanaxConfiguration(new AstyanaxConfigurationImpl().setDiscoveryType(NodeDiscoveryType.NONE).setDefaultReadConsistencyLevel(readConsistency).setDefaultWriteConsistencyLevel(writeConsistency).setCqlVersion("3.0.0").setTargetCassandraVersion("1.2"))
                                .withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("ResolveCASConnectionPool").setAuthenticationCredentials(authentication).setMaxConnsPerHost(maxConnsPerHost).setSeeds(seeds).setMaxTimeoutWhenExhausted(maxExhaustedTimeout).setMaxBlockedThreadsPerHost(maxBlockedThreads)).withConnectionPoolMonitor(new CountingConnectionPoolMonitor()).buildKeyspace(ThriftFamilyFactory.getInstance());
            }
            context.start();

            Keyspace tmpKsp = context.getEntity();
            for (int i = 0; i < 20; ++i)
            {
                Log.log.debug("Testing Cassandra Connection. Count: " + i);
                try
                {
                    // CassandraUtil.findWorksheetByKey("" + i * 391371 + "Abc"
                    // + i * 93279);
                    ColumnList<String> result = null;
                    try
                    {
                        result = tmpKsp.prepareQuery(getWorkSheetCF()).getKey("" + i * 391371 + "Abc" + i * 93279).execute().getResult();
                    }
                    catch (Exception ex)
                    {
                        Log.log.error("Cassandra Test Failed, Waiting to Try Again", ex);
                        try
                        {
                            if (i != 19)
                            {
                                Thread.sleep(10000);
                            }
                        }
                        catch (InterruptedException ie)
                        {
                            // do nothing
                        }
                        throw new RuntimeException(ex);
                    }
                }
                catch (Throwable t)
                {
                    if (i == 19)
                    {
                        ksp = null;
                        try
                        {
                            context.shutdown();
                        }
                        catch (Throwable ex)
                        {
                        }
                        finally
                        {
                            context = null;
                        }
                        throw new RuntimeException(t);
                    }
                    try
                    {
                        Thread.currentThread().sleep(1000);
                    }
                    catch (Exception ext)
                    {
                    }
                }
            }
            Log.log.debug("Cassandra connection is good.");

            if (context != null)
            {
                context.shutdown();
            }
            Log.log.debug("Creating Full connection");

            if (secure)
            {
                context = new AstyanaxContext.Builder().forCluster(clusterName).forKeyspace(keyspaceName).withAstyanaxConfiguration(new AstyanaxConfigurationImpl().setDiscoveryType(NodeDiscoveryType.NONE).setDefaultReadConsistencyLevel(readConsistency).setDefaultWriteConsistencyLevel(writeConsistency).setCqlVersion("3.0.0").setTargetCassandraVersion("1.2").setRetryPolicy(new ConstantBackoff(retrySleepTimeMs, retryMaxAttempts)))
                                .withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("ResolveCASConnectionPool").setAuthenticationCredentials(authentication).setMaxConnsPerHost(maxConnsPerHost).setSeeds(seeds).setMaxTimeoutWhenExhausted(maxExhaustedTimeout).setMaxBlockedThreadsPerHost(maxBlockedThreads).setSocketTimeout(120000).setConnectTimeout(120000).setSSLConnectionContext(new SSLConnectionContext(truststore, truststoreP_assword)))
                                .withConnectionPoolMonitor(new CountingConnectionPoolMonitor()).buildKeyspace(ThriftFamilyFactory.getInstance());
            }
            else
            {
                context = new AstyanaxContext.Builder().forCluster(clusterName).forKeyspace(keyspaceName).withAstyanaxConfiguration(new AstyanaxConfigurationImpl().setDiscoveryType(NodeDiscoveryType.NONE).setDefaultReadConsistencyLevel(readConsistency).setDefaultWriteConsistencyLevel(writeConsistency).setCqlVersion("3.0.0").setTargetCassandraVersion("1.2").setRetryPolicy(new ConstantBackoff(retrySleepTimeMs, retryMaxAttempts)))
                                .withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("ResolveCASConnectionPool").setAuthenticationCredentials(authentication).setMaxConnsPerHost(maxConnsPerHost).setSeeds(seeds).setMaxTimeoutWhenExhausted(maxExhaustedTimeout).setMaxBlockedThreadsPerHost(maxBlockedThreads).setSocketTimeout(120000).setConnectTimeout(120000)).withConnectionPoolMonitor(new CountingConnectionPoolMonitor()).buildKeyspace(ThriftFamilyFactory.getInstance());
            }
            context.start();
            ksp = context.getEntity();
        }
        return ksp;
    }
    
    public static void setReadConsistency(String c)
    {
        if (CL_ONE.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_ONE;
        }
        else if (CL_QUORUM.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_QUORUM;
        }
        else if (CL_LOCAL_QUORUM.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_LOCAL_QUORUM;
        }
        else if (CL_EACH_QUORUM.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_EACH_QUORUM;
        }
        else if (CL_ALL.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_ALL;
        }
        else if (CL_ANY.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_ANY;
        }
        else if (CL_TWO.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_TWO;
        }
        else if (CL_THREE.equals(c))
        {
            readConsistency=ConsistencyLevel.CL_THREE;
        }
        else
        {
            throw new RuntimeException("Invalid read consistency level: " +c);
        }
    }

    public static ColumnFamily<String, String> getWorkSheetCF()
    {
        if (worksheet_cf != null) return worksheet_cf;

        worksheet_cf = new ColumnFamily<String, String>("worksheet", // Column
                                                                     // Family
                                                                     // Name
                        StringSerializer.get(), // Key Serializer
                        StringSerializer.get()); // Column Serializer
        return worksheet_cf;
    }

    public static ColumnFamily<String, String> getExecuteStateCF()
    {
        if (executeState_cf != null) return executeState_cf;

        executeState_cf = new ColumnFamily<String, String>("resolve_execute_state",
                        StringSerializer.get(), // Key Serializer
                        StringSerializer.get()); // Column Serializer
        return executeState_cf;
    }

   public static ColumnList<String> findRowByKey(Keyspace keyspace, String rowKey, ColumnFamily<String, String> cf, String testColumn)
    {
        ColumnList<String> result = null;
        try
        {
            result = keyspace.prepareQuery(cf).getKey(rowKey).withColumnSlice(testColumn).execute().getResult();
        }
        catch (Exception ex)
        {
            Log.log.error(ex, ex);
        }
        return result;
    }

    public static boolean isResolveRowExpired(ColumnList<String> ws)
    {
        if (ws==null || ws.isEmpty())
        {
            return true; 
        }
        boolean result = false;
        
        if (ws.getColumnByName("sys_created_by")==null)
        {
            result = true;
        }
        else if (ws.getColumnByName("sys_created_on")==null)
        {
            result = true;
        }
        
        return result;
    }

    public static ColumnList<String> findCFRowByKey(Keyspace keyspace, String rowKey, ColumnFamily<String, String> cf)
    {
        ColumnList<String> result = null;
        try
        {

            result = keyspace.prepareQuery(cf).getKey(rowKey).execute().getResult();
        }
        catch (Exception ex)
        {
            Log.log.error(ex, ex);
            throw new RuntimeException(ex);
        }
        return result;
    }

    // These for table row query and search
    public static ColumnList<String> findWorksheetByKey(Keyspace keyspace, String rowKey)
    {
        long start = 0;
        ColumnList<String> result = null;
        try
        {
            result = keyspace.prepareQuery(getWorkSheetCF()).getKey(rowKey).execute().getResult();
            if (isResolveRowExpired(result))
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            Log.log.error(ex, ex);
            throw new RuntimeException(ex);
        }

        return result;
    }

    // Column family must already have index created
    public static List<Row<String, String>> searchByIndex(IndexQuery<String, String> query)
    {
        int pageCount = 0, rowCount = 0;
        List<Row<String, String>> list = new ArrayList<Row<String, String>>();
        OperationResult<Rows<String, String>> result;
        long start = 0;

        try
        {
            while (!(result = query.execute()).getResult().isEmpty())
            {
                pageCount++;
                rowCount += result.getResult().size();
                for (Row<String, String> row : result.getResult())
                {
                    list.add(row);
                }
            }
        }
        catch (Exception ex)
        {
            Log.log.error(ex, ex);
            throw new RuntimeException(ex);
        }

        return list;
    }

    public static Map<String, Object> findWorksheetData(Keyspace keyspace, String worksheetId)
    {
        Map result = new HashMap<String, Object>();
        
        String[] columns = new String[] {"u_wsdata"};
        ColumnList<String> list = null;
        try
        {
            list = keyspace.prepareQuery(getWorkSheetCF()).getKey(worksheetId).withColumnSlice(columns).execute().getResult();
        }
        catch (Exception ex)
        {
            Log.log.error(ex, ex);
            throw new RuntimeException(ex);
        }

        if (list == null) return null;

        for (Column<String> col : list)
        {
            if ("u_wsdata".equals(col.getName()))
            {
                result.put(col.getName(), col.getByteArrayValue());
            }
        }
        return result;
    }

    public static List<Row<String, String>> getExecuteStateFromCassandra(Keyspace keyspace, String processid, String problemid)
    {
        List<Row<String, String>> rows = null;

        if (StringUtils.isNotEmpty(problemid) && StringUtils.isNotEmpty(processid))
        {
            try
            {
                // try with eventid / targetaddr first
                IndexQuery<String, String> query = keyspace.prepareQuery(getExecuteStateCF())
                                .searchWithIndex().setRowLimit(10).autoPaginateRows(true)
                                .addExpression().whereColumn("u_process_id").equals().value(processid)
                                .addExpression().whereColumn("u_problem_id").equals().value(problemid);
                rows = searchByIndex(query);
            }
            catch (Exception e)
            {
                Log.log.error("Error getting rows for processid: " + processid + " && problemid:" + problemid, e);
            }
        }

        return rows;
    }

}
