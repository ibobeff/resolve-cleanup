/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.resolve.persistence.model.ResolveProperties;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Log;

public class ExportResolveProperties extends ExportComponent
{
protected ResolveImpexGlideVO impexGlideVO;
    
    public ExportResolveProperties(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    public ManifestGraphDTO export()
    {
        ManifestGraphDTO graphDTO = null;
        String module = impexGlideVO.getUModule();
        String name = impexGlideVO.getUName();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveProperties");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UModule");
        filter.setValue(module);
        filters.add(filter);
        
        if (StringUtils.isNotBlank(name))
        {
            filter = new QueryFilter();
            filter.setType("auto");
            filter.setCondition(QueryFilter.EQUALS);
            filter.setField("UName");
            filter.setValue(name);
            filters.add(filter);
        }
        
        query.setFilterItems(filters);

        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
            if (list != null && list.size() > 0)
            {
                ResolveProperties resolveProperties = (ResolveProperties) list.get(0);
                if (resolveProperties != null)
                {
                    graphDTO = new ManifestGraphDTO();
                    if (excludeList.contains(resolveProperties.getSys_id())) {
                        graphDTO.setExported(false);
                    }
                    populateResolveProperties(resolveProperties, graphDTO);
                }
            }
        }
        catch (Throwable t)
        {
            String message = String.format("Could not read Resolve Property name: %s", name);
            Log.log.error(message, t);
            ImpexUtil.logImpexMessage(message, true);
        }
        
        return graphDTO;
    }
    
    private void populateResolveProperties(ResolveProperties resolveProperties, ManifestGraphDTO graphDTO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveProperties");
        
        dto.setSys_id(resolveProperties.getSys_id());
        dto.setFullName(resolveProperties.getUModule() + "." + resolveProperties.getUName());
        dto.setModule(resolveProperties.getUModule());
        dto.setName(resolveProperties.getUName());
        dto.setType(RESOLVE_PROPERTIES);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        graphDTO.setId(resolveProperties.getSys_id());
        graphDTO.setName(resolveProperties.getUName());
        graphDTO.setType(ImpexEnum.PROPERTIES.getValue());
        
        ExportUtils.impexComponentList.add(dto);
    }
}
