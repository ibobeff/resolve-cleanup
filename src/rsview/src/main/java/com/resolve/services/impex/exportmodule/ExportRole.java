/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.GroupRoleRel;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportRole extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String roleName;
    
    public ExportRole(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
        this.roleName = impexGlideVO.getUModule();
    }
    
    public void export() throws Exception
    {
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for Role " + roleName);
        }
        
        Roles role = getRole();
        if (role != null)
        {
            populateRole(role);
            
            if (impexOptions.getRoleUsers())
            {
                Collection<UserRoleRel> userRoleRelList = role.getUserRoleRels();
                if (userRoleRelList != null)
                {
                    for (UserRoleRel userRoleRel : userRoleRelList)
                    {
                        populateUserRoleRel(userRoleRel);
                    }
                }
            }
            
            if (impexOptions.getRoleGroups())
            {
                Collection<GroupRoleRel> groupRoleRelList = role.getGroupRoleRels();
                if (groupRoleRelList != null)
                {
                    for (GroupRoleRel groupRoleRel : groupRoleRelList)
                    {
                        populateGroupRoleRel(groupRoleRel);
                    }
                }
            }
        }
        
    }
    
    private void populateRole(Roles role)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.Roles");
        
        dto.setType(ROLE);
        dto.setSys_id(role.getSys_id());
        dto.setFullName(role.getUName());
        dto.setModule(role.getUName());
        dto.setName(role.getUName());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        dto.setGroup(ROLE + " " + dto.getFullName());
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateUserRoleRel (UserRoleRel userRoleRel)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.UserRoleRel");
        
        dto.setType(USERROLEREL);
        dto.setSys_id(userRoleRel.getSys_id());
        dto.setTablename(tableName);
        dto.setDisplayType(dto.getType());
        dto.setFullName(userRoleRel.getSys_id());
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateGroupRoleRel(GroupRoleRel groupRoleRel)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.GroupRoleRel");
        
        dto.setType(GROUPROLEREL);
        dto.setSys_id(groupRoleRel.getSys_id());
        dto.setTablename(tableName);
        dto.setDisplayType(dto.getType());
        dto.setFullName(groupRoleRel.getSys_id());
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private Roles getRole()
    {
        Roles roleModel = null;
        
        if(StringUtils.isNotBlank(roleName))
        {
            //to make this case-insensitive
            //String sql = "from Roles where LOWER(UName) = '" + roleName.trim().toLowerCase() + "'";
            String sql = "from Roles where LOWER(UName) = :UName";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", roleName.trim().toLowerCase());
            
            try
            {
                roleModel = (Roles) HibernateProxy.execute(() -> {
                	
                	Roles model = null;
                	 List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                     if(result != null && result.size() > 0)
                     {
                    	 model = (Roles) result.get(0);
                     }
                     
                     if (model != null)
                     {
                         Collection<UserRoleRel> userRoleRelCollection = model.getUserRoleRels();
                         if (userRoleRelCollection != null)
                         {
                             userRoleRelCollection.size();
                         }
                         
                         Collection<GroupRoleRel> groupRoleRelCollection = model.getGroupRoleRels();
                         if (groupRoleRelCollection != null)
                         {
                             groupRoleRelCollection.size();
                         }
                     }
                     
                     return model;
                });            
               
                
                
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return roleModel;
    }
}
