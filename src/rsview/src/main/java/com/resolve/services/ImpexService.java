package com.resolve.services;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;

import com.resolve.rsbase.MInfo;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.ImportUtils;
import com.resolve.services.hibernate.vo.ResolveImpexManifestVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ImpexService {
	public static void impexSync(Map<String, Object> params) throws Throwable {
		String operation = (String) params.get("OPERATION");
		String task = (String) params.get("TASK");

		if (StringUtils.isNotBlank(task)) {
			if (operation.equals("IMPORT")) {
				buildImportManifestSync(params, true);

			} else if (operation.equals("EXPORT")) {
				buildExportManifestSync(params, true);
			}
		} else if (StringUtils.isNotBlank(operation)) {
			if (operation.equals("IMPORT")) {
				importSync(params);
			} else if (operation.equals("EXPORT")) {
				exportSync(params);
			}
		}
	}

	public static synchronized void buildImportManifestSync(Map<String, Object> params, boolean fromUI) throws Throwable {
		String moduleName = (String) params.get("MODULE_NAME");
		String userName = (String) params.get("USER");

		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("ImpexService.buildImportManifestSync(%s) B4 DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
		
//		ImpexUtil.checkCurrentImpexManifestStatus(moduleName);

		try {
			new ImportUtils().buildImportManifest(moduleName, userName, fromUI);
		} catch (Exception e) {
			Log.log.error("Error while building import manifest: " + moduleName, e);
			ServiceHibernate.clearAllResolveEventByValue("IMPEX-MANIFEST:");
		}
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("ImpexService.buildImportManifestSync(%s) After DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
											new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
	}

	public static synchronized void buildExportManifestSync(Map<String, Object> params, boolean fromUI) throws Throwable {
		String moduleName = (String) params.get("MODULE_NAME");
		String userName = (String) params.get("USER");
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("ImpexService.buildExportManifestSync(%s) B4 DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
		
//		ImpexUtil.checkCurrentImpexManifestStatus(moduleName);

		try {
			new ExportUtils().buildExportManifest(moduleName, userName, fromUI);
		} catch (Exception e) {
			Log.log.error("Error while building export manifest: " + moduleName, e);
		}
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("ImpexService.buildImportManifestSync(%s) After DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
											new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
	}

	@SuppressWarnings("unchecked")
	public static void exportSync(Map<String, Object> params) {
		String moduleName = (String) params.get("MODULE_NAME");
		List<String> excludeList = (List<String>) params.get("EXCLUDE_LIST");
		String userName = (String) params.get("USER");
		boolean cleanModule = true;
		if (params.get("CLEAN_MODULE") != null) {
			cleanModule = (Boolean) params.get("CLEAN_MODULE");
		}
		
		if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("ImpexService.exportSync(%s) B4 DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
		boolean isNew = (Boolean)params.get("IS_NEW");
		try {
			new ExportUtils().export(moduleName, excludeList, userName, isNew, cleanModule);
		} catch (Exception e) {
			Log.log.error("Error while exporting a module: " + moduleName, e);
		}
		
		if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("ImpexService.exportSync(%s) After DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
											new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
	}

	@SuppressWarnings("unchecked")
	public static void importSync(Map<String, Object> params) {
		String moduleName = (String) params.get("MODULE_NAME");
		List<String> includeList = (List<String>) params.get("INCLUDE_LIST");
		String userName = (String) params.get("USER");
		Boolean blockIndex = (Boolean) params.get("BLOCK_INDEX");

		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("ImpexService.importSync(%s) B4 DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
		
//		ImpexUtil.checkCurrentImpexStatus(moduleName, "IMPORT");

		try {
			new ImportUtils().importModule(moduleName, includeList, userName, blockIndex);
		} catch (Exception e) {
			Log.log.error("Error while importing a module: " + moduleName, e);
		}
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("ImpexService.importSync(%s) After DBPoolUsage [%s]", 
										StringUtils.mapToString(params, "=", ", "),
										StringUtils.mapToString(
											new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
	}

	public static String importModule(String moduleName, List<String> includeList, String userName, boolean blockIndex)
			throws Exception {
		return ImpexUtil.importModule(moduleName, includeList, userName, blockIndex);
	}

	public static boolean checkForCustomTables(String moduleName) {
		return ImportUtils.checkForCustomTables(moduleName);
	}

	public static Map<String, String> uploadFileForImport(FileItem fileItem, boolean isNew, String userName) throws Throwable {
		ImportUtils importUtils = new ImportUtils();
		String fileName = importUtils.uploadFileForImport(fileItem);
		return importUtils.storeZipFileToDB(fileName, isNew, userName, true);
	}

	public static String uploadFileForImport(File file, String userName, Boolean keepZip) throws Throwable {
		ImportUtils importUtils = new ImportUtils();
		String fileName = importUtils.uploadFileForImport(file);
		if (keepZip) {
			importUtils.storeZipFileToDBKeepZip(fileName, userName);
		} else {
			importUtils.storeZipFileToDB(fileName, false, userName, true);
		}
		return fileName;
	}
	
	public static boolean prepareManifest(String moduleName, String operation, String userName, boolean fromUI) throws Throwable
    {
        boolean result = false;
        
        if (StringUtils.isNotBlank(operation))
        {
            if (operation.equals("IMPORT"))
            {
                result = ImportUtils.prepareManifest(moduleName, userName, fromUI);
            }
            else if (operation.equals("EXPORT"))
            {
                result = ExportUtils.prepareManifest(moduleName, userName, fromUI);
            }
        }
        
        return result;
    }
	
	public static Object manifestStatus(String moduleName, String operation, String userName) throws Exception
    {
        Object result = null;
        
        if (StringUtils.isNotBlank(operation))
        {
            if (operation.equals("IMPORT"))
            {
                result = ImportUtils.manifestStatus(moduleName, userName);
            }
            else if (operation.equals("EXPORT"))
            {
                result = ExportUtils.manifestStatus(moduleName, userName);
            }
        }
        
        return result;
    }
	
	public static List<ResolveImpexManifestVO> getManifest(QueryDTO query, String moduleName, String operation, String userName) throws Exception
    {
        List<ResolveImpexManifestVO> result = null;
        
        if (operation.equalsIgnoreCase("import"))
        {
            ImportUtils importUtils = new ImportUtils();
            result = importUtils.getManifest(query, moduleName, userName);
        }
        else if (operation.equalsIgnoreCase("export"))
        {
            ExportUtils exportUtil = new ExportUtils();
            result = exportUtil.getExportManifest(query, moduleName, userName);
        }
        
        return result;
    }
	
	public static ResolveImpexModuleVO saveResolveImpexModule(ResolveImpexModuleVO vo, String username, 
															  boolean validate, String operation) throws Throwable
    {
        return ImpexUtil.saveResolveImpexModule(vo, username, validate, operation);
    }
	
	public static void saveResolveVersion(ResolveImpexModuleVO vo, String userName) throws Exception
    {
        ImpexUtil.saveResolveVersion(vo, userName);
    }
	
	public static void deleteImpexModelByIDS(List<String> ids, String username) throws Throwable
    {
        ImpexUtil.deleteImpexModelByIDS(ids, username);
    }
	
	public static List<ResolveImpexModuleVO> getResolveImpexModules(QueryDTO query, String username) throws Exception
    {
        return ImpexUtil.getResolveImpexModules(query, username);
    }
	
	public static List<ResolveImpexModuleVO> getResolveImpexModulesV2(QueryDTO query, String username) throws Exception
    {
        return ImpexUtil.getResolveImpexModulesV2(query, username);
    }
}