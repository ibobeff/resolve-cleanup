/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.tree.DefaultAttribute;
import org.hibernate.query.Query;

import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskMockData;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionBuilder;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.ImportValidationRuleVO;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.FileUtils;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.NumberUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ImportValidationUtil
{
    protected String GLIDE_INSTALL_FOLDER_LOCATION;
    protected String MODULE_FOLDER_LOCATION;
    
    protected Map<String, ImportValidationRuleVO> validationMap;
    protected Map<String, Boolean> fileProcessMap = new HashMap<String, Boolean>();
//    protected StringBuffer validationBuffer;
//    protected StringBuffer accessRightsTableBuffer;
//    protected Set <String> customTableNames;
    protected Set<String> processedTables;
    protected boolean validateImpex;
    protected Map<String, String> replacableSysIdMap = new HashMap<String, String>();
    protected Map<String, String> actionInvocDescMap = new HashMap<String, String>();
    protected String userName;
    protected String operation;
//    protected List<String> filesToBeDeleted = new ArrayList<String>();
    
    public ImportValidationUtil(String moduleFolder, String glideFolder, String userName)
    {
    	this.MODULE_FOLDER_LOCATION = moduleFolder;
        this.GLIDE_INSTALL_FOLDER_LOCATION = glideFolder;
        this.userName = userName;
        this.operation = "IMPORT";
    }

    public ImportValidationUtil(String moduleFolder, String glideFolder, String userName, String operation)
    {
        this.MODULE_FOLDER_LOCATION = moduleFolder;
        this.GLIDE_INSTALL_FOLDER_LOCATION = glideFolder;
        this.userName = userName;
        this.operation = operation;
    }
    
    public void validateImportModule() throws Exception
    {
        validationMap = ImportValidationRules.getImportValidationRules();
//        validationBuffer = new StringBuffer();
//        accessRightsTableBuffer = new StringBuffer();
//        customTableNames = new HashSet<String>();
        processedTables = new HashSet<String>();
        
        File dir = new File (GLIDE_INSTALL_FOLDER_LOCATION);
        WildcardFileFilter filter = new WildcardFileFilter("*.xml");
        Collection<File> fileCollection = FileUtils.listFiles(dir, filter, TrueFileFilter.INSTANCE);
        updateImportStatusCount(fileCollection.size());
        
        for (File file : fileCollection)
        {
            fileProcessMap.put(file.getName(), new Boolean(false));
        }
        
        // Add the validation rule at runtime as we don't know the model ahead of time.
        checkCustomGateways();
        
        try {
        	ImpexUtil.initImpexOperationStage(operation, "(19/51) Validate Glide Components to Import", fileCollection.size());
	        for (File importFile : fileCollection)
	        {
	            ImpexUtil.updateImpexCount(operation);
	            String fileName = importFile.getName();
	            
	            if (fileName.contains("resolve") && fileName.contains("pkg"))
	            {
	                /*
	                 * From resolve 3.5 onwards, resolve tags are not stored in DB, but in graph.
	                 * These tags will be validated during actual import.
	                 */
	                continue;
	            }
	            
	            if (!fileProcessMap.get(fileName))
	            {
	                XDoc doc;
	                try
	                {
	                    doc = ImpexUtil.convertFileToXDoc(importFile);
	                    String table = doc.getStringValue("/record_update/@table");
	                    if (!table.contains("access_rights"))
	                    {
	                        checkDependencies(table);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error(e.getMessage(), e);
	                }
	            }
	        }
        } catch (Exception e) {
        	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
					  	  e.getMessage() : "Failed to Validate List of Glide Components to Import.", e);
        	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
						  		  	  e.getMessage() : 
						  		  	  "Failed to Validate List of Glide Components to Import. Please check RSView " +
						  		  	  "logs for details", 
						  		  	  true);
        }
        
        validateXmlFile("access_rights");
        validateXmlFile("meta_access_rights");
        
        replaceAllSysIds();
        
        // Clean all the resources here.
        processedTables.clear();
        replacableSysIdMap.clear();
        actionInvocDescMap.clear();
        fileProcessMap.clear();
        replacableSysIdMap.clear();
        actionInvocDescMap.clear();
    }
    
    private void checkCustomGateways()
    {
        File dir = new File (GLIDE_INSTALL_FOLDER_LOCATION);
        WildcardFileFilter filter = new WildcardFileFilter("*_sdk.xml");
        Collection<File> fileCollection = FileUtils.listFiles(dir, filter, TrueFileFilter.INSTANCE);
        
        if (CollectionUtils.isNotEmpty(fileCollection))
        {
            List<String> tableNameSet = new ArrayList<String>();
            
            try {
            	ImpexUtil.initImpexOperationStage(operation, "(18/51) Validate Rules for SDK Developed Gateways", fileCollection.size());
	            for (File file : fileCollection)
	            {
	            	ImpexUtil.updateImpexCount(operation);
	                try
	                {
	                    XDoc doc = ImpexUtil.convertFileToXDoc(file);
	                    String tableName = doc.getStringValue("/record_update/@table");
	                    if (!tableNameSet.contains(tableName))
	                    {
	                        ImportValidationRuleVO vo = new ImportValidationRuleVO(tableName, null, "u_name,u_queue", null, null);
	                        validationMap.put(tableName, vo);
	                    }
	                }
	                catch(Exception e)
	                {
	                    Log.log.error("Error while updating validation rules for sdk developed gateway: " + file.getName(), e);
	                }
	            }
            } catch (Exception e) {
            	Log.log.error(
                		StringUtils.isNotBlank(
                			e.getMessage()) ? e.getMessage() : "Failed Validating Rules for SDK Developed Gateways.", e);
            	ImpexUtil.logImpexMessage(
            		StringUtils.isNotBlank(
            			e.getMessage()) ? 
            			e.getMessage() : 
            			"Failed Validating Rules for SDK Developed Gateways. Please check RSView logs for details", true);
            }
        }
    }
    
    private void updateImportStatusCount(int count) throws Exception
    {
    	if (Log.log.isTraceEnabled()) {
	    	Log.log.trace(String.format("ImpexUtil.updateImportStatusCount(%d) calling " +
										"ServiceHibernate.updateImportStatusCount...", 
										count));
    	}
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.updateImportStatusCount(%d) " +
										"ServiceHibernate.getLatestImpexEventByValue returned Resolve Event [%s]", 
										count, (eventVO != null ? eventVO : "null")));
        }
        
        if (eventVO != null)
        {
            String[] progress = eventVO.getUValue().split(",");
            if (progress.length == 3)
            {
                int c = Integer.parseInt(progress[1]);
                c = c + count;
                String value = progress[0] + "," + c + "," + progress[2];
                eventVO.setUValue(value);
                ServiceHibernate.insertResolveEvent(eventVO);
                if (Log.log.isTraceEnabled()) {
	                Log.log.trace(String.format("ImpexUtil.updateImportStatusCount(%d) inserted Resolve Event [%s]",
	                							count, eventVO));
                }
            }
        }
    }
    
    public void checkForSameSysIdButDiffName(List<String> excludedCompList, boolean cleanup)
    {
        FileFilter fileFilter = new WildcardFileFilter("resolve_action_task_*");
        File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        File[] files = dir.listFiles(fileFilter);
        
        // Capture current processing file name and put it in the logs during exception.
        String fileName = null;
        
        XDoc doc = null;
        
        if (files.length == 0) {
        	return;
        }
        
        try
        {
        	ImpexUtil.initImpexOperationStage(operation, "(6/51) Checking Same SysId Different Name For Glide Components", 
        									   files.length);
            for (File file : files)
            {
            	ImpexUtil.updateImpexCount(operation);
                fileName = file.getName();
                doc = ImpexUtil.convertFileToXDoc(file);
                if (doc.getStringValue("/record_update/@table").equals("resolve_action_task"))
                {
                    Map<String, String> xmlATValues = prepareMapFromXML(doc, "resolve_action_task");
                    
                    String namespace = xmlATValues.get("u_namespace");
                    if (StringUtils.isNotBlank(namespace) && namespace.equals("resolve")
                                    && !userName.equals("resolve.maint"))
                    {
                        // This is a system action task. Don't validate it.
                        continue;
                    }
                    /*
                     * A Map containing ActionInvoc SysId (key) and ActionTask full name (value).
                     * It will be used for older modules where actionInvoc does not have description field.
                     * During validation, description field will be used from this map if not present in the XML.
                     */
                    String actionInvocSysId = xmlATValues.get("u_invocation");
                    actionInvocDescMap.put(actionInvocSysId, xmlATValues.get("u_fullname"));
                    
                    String xmlFullName = "";
                    if (xmlATValues.get("u_fullname") != null)
                    {
                        xmlFullName = xmlATValues.get("u_fullname");
                    }
                    else
                    {
                        if (xmlATValues.get("u_name") != null && xmlATValues.get("u_namespace") != null)
                        {
                            xmlFullName = xmlATValues.get("u_name") + "#" + xmlATValues.get("u_namespace");
                        }
                    }
                    
                    if (xmlFullName == null)
                    {
                        Log.log.error("Fullname of an ActionTask could not be evaludated from: " + file.getName());
                        continue;
                    }
                    
                    String xmlSysId = xmlATValues.get("sys_id");
                    
                    
                    
                    /*
                     * Let's check Actiontask with the xml full name.
                     */
                    ResolveActionTaskVO actionTaskVO = ServiceHibernate.getResolveActionTask(null, xmlFullName, "system");
                    if (actionTaskVO != null)
                    {
                        /*
                         * Ahaa, we found an Action task (dbTask) in local DB with the same name as that of XML full name.
                         * Check the sys_id of dbTask with the XML sys_id. If both are same, do nothing.
                         * If different, then it's 'Same name different sys_id' scenario. So, handle it.
                         */
                        
                        String dbSysId = actionTaskVO.getSys_id();
                        
                        /*
                         * check if variables need to be cleaned up or if this is just done as part of the manifest verification
                         * There's an action task present in the local system whoes name is save as that of xmlActionTask
                         * Check if local action task has any local references, if present, delete it.
                         */
                        if(cleanup)
                        	cleanActionTask(dbSysId, actionInvocSysId, excludedCompList);
                        
                        if (!dbSysId.equals(xmlSysId))
                        {
                            /*
                             * Same name, different sys_id.
                             * Overwrite dbTask with XML action task along with all the references.
                             */
                            replacableSysIdMap.put(xmlSysId, dbSysId);
                            // getNewIdentity(dir, xmlATValues, xmlFullName);
                        }
                        
                        continue;
                    }
                    
                    actionTaskVO = ServiceHibernate.getResolveActionTask(xmlSysId, null, "system");
                    if (actionTaskVO != null)
                    {
                        /*
                         * Oh, we found an Actiontask (dbTask) in current system that has
                         * same sys_id as that of XML sys_id. Let's compare the fullnames.
                         * If XML fullname is same as that of dbTask fullname, it's the same ActionTask, don't do anything.
                         * Otherwise, it's 'Same Sys_id and different name' scenario. So, handle it.
                         */
                        String dbFullName = actionTaskVO.getUFullName();
                        if (!xmlFullName.equals(dbFullName))
                        {
                            /*
                             * Same Sys_id and different name.
                             * In this case, create new Ids for Action task and all it's references, update the XML, and import it.
                             */
                            String newSysId = Guid.getGuid(true);
                            replacableSysIdMap.put(xmlSysId, newSysId);
                            
                            getNewIdentity(dir, xmlATValues, xmlFullName);
                            
                            continue;
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
        	Log.log.error(
        		StringUtils.isNotBlank(
        			e.getMessage()) ? e.getMessage() : "Failed Checking Same SysId Different Name For Glide Components.", e);
    		ImpexUtil.logImpexMessage(
    			StringUtils.isNotBlank(
    				e.getMessage()) ? 
    				e.getMessage() : 
    				"Failed Checking Same SysId Different Name For Glide Components. Please check RSView logs for details", 
    			true);
        }
    }
    
    public void checkOlderVersionOfActionTask()
    {
        List<File> actionTaskFiles = getFileList("resolve_action_task");
        
        if (CollectionUtils.isEmpty(actionTaskFiles)) {
        	return;
        }
        
        try {
        	ImpexUtil.initImpexOperationStage(operation, "(5/51) Checking Old Versions of Action Tasks", actionTaskFiles.size());
	        for (File actionTaskFile : actionTaskFiles)
	        {
	        	ImpexUtil.updateImpexCount(operation);
	            try
	            {
	                XDoc actionTaskDoc = ImpexUtil.convertFileToXDoc(actionTaskFile);
	                Map<String, String> xmlATValues = prepareMapFromXML(actionTaskDoc, "resolve_action_task");
	                String actionTaskId = xmlATValues.get("sys_id");
	                String fullName = xmlATValues.get("u_name") + "#" + xmlATValues.get("u_namespace");
	                
	                if (!isOldTaskValid(xmlATValues, fullName))
	                {
	                    // Old version of ActionTasks did not have access rights reference.
	                    ImpexUtil.logImpexMessage("ActionTask '" + fullName + "' is old formatted or name has consecutive white spaces, so not imported.", true);
	                    actionTaskFile.delete();
	                    List<File> actionInvocFiles = getFileList("resolve_action_invoc");
	                    for (File actionInvocFile : actionInvocFiles)
	                    {
	                        String content = FileUtils.readFileToString(actionInvocFile);
	                        if (content.contains(actionTaskId) || content.contains(fullName))
	                        {
	                            XDoc actionInvocDoc = ImpexUtil.convertFileToXDoc(actionInvocFile);
	                            actionInvocFile.delete();
	                            
	                            Map<String, String> xmlAIValues = prepareMapFromXML(actionInvocDoc, "resolve_action_invoc");
	                            String actionInvocSysId = xmlAIValues.get("sys_id");
	                            deleteFile(GLIDE_INSTALL_FOLDER_LOCATION + "resolve_action_invoc_" + actionInvocSysId + ".xml");
	                            
	                            if (StringUtils.isNotBlank(xmlAIValues.get("u_parser")))
	                            {
	                                deleteFile(GLIDE_INSTALL_FOLDER_LOCATION + "resolve_parser_" + xmlAIValues.get("u_parser") + ".xml");
	                            }
	                            if (StringUtils.isNotBlank(xmlAIValues.get("u_preprocess")))
	                            {
	                                deleteFile(GLIDE_INSTALL_FOLDER_LOCATION + "resolve_preprocess_" + xmlAIValues.get("u_preprocess") + ".xml");
	                            }
	                            if (StringUtils.isNotBlank(xmlAIValues.get("u_assess")))
	                            {
	                                deleteFile(GLIDE_INSTALL_FOLDER_LOCATION + "resolve_assess_" + xmlAIValues.get("u_assess") + ".xml");
	                                // Add this file for deletion list
	                            }
	                            
	                            List<File> actionInvocOptionsFiles = getFileList("resolve_action_invoc_options");
	                            for (File actionInvocOptionsFile : actionInvocOptionsFiles)
	                            {
	                                content = FileUtils.readFileToString(actionInvocOptionsFile);
	                                if (content.contains(actionInvocSysId))
	                                {
	                                    //filesToBeDeleted.add(actionInvocOptionsFile.getName());
	                                    actionInvocOptionsFile.delete();
	                                    break;
	                                }
	                            }
	                        
	                            List<File> resolveParameterFiles = getFileList("resolve_action_parameter");
	                            for (File resolveParameterFile : resolveParameterFiles)
	                            {
	                                content = FileUtils.readFileToString(resolveParameterFile);
	                                if (content.contains(actionInvocSysId))
	                                {
	                                    //filesToBeDeleted.add(resolveParameterFile.getName());
	                                    resolveParameterFile.delete();
	                                    break;
	                                }
	                            }
	                            continue;
	                        }
	                    }
	                }
	                else
	                {
	                    /*
	                     * resolve_action_task.xml looks good. Let's check whether we have resolve_action_invoc.
	                     * If not, we cannot import this action task.
	                     */
	                    String actionInvocId = xmlATValues.get("u_invocation");
	                    File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
	                    FileFilter fileter = new NameFileFilter("resolve_action_invoc_" + actionInvocId + ".xml");
	                    if (dir.listFiles(fileter).length == 0)
	                    {
	                        /*
	                         * Corresponding resolve_action_invoc does not exist.
	                         * This task cannot be imported. Delete the XML file.
	                         */
	                        ImpexUtil.logImpexMessage("Incomplete ActionTask: " + xmlATValues.get("u_fullname") + " not imported.", true);
	                        Log.log.error("Incomplete ActionTask: " + xmlATValues.get("u_fullname") + " not imported.");
	                        actionTaskFile.delete();
	                    }
	                    else
	                    {
	                        // Make sure action invoc description is same as that of action task full name.
	                        File actionInvocFile = dir.listFiles(fileter)[0];
	                        String content = FileUtils.readFileToString(actionInvocFile);
	                        String actionInvocDesc = StringUtils.substringBetween(content, "<u_description>", "</u_description>");
	                        if (StringUtils.isNotBlank(actionInvocDesc) && StringUtils.isNotBlank(fullName))
	                        {
	                            if (!fullName.equals(actionInvocDesc))
	                            {
	                                content = content.replace("<u_description>" + actionInvocDesc + "</u_description>", "<u_description>" + fullName + "</u_description>");
	                                FileUtils.writeStringToFile(actionInvocFile, content);
	                            }
	                        }
	                    }
	                }
	            }
	            catch(Exception e)
	            {
	                // nothing is to be done here.
	            }
	        }
        } catch (Exception e) {
        	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Failed Checking Old Versions of Action Tasks.", e);
    		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
    								  e.getMessage() : 
    								  "Failed Checking Old Versions of Action Tasks. Please check RSView logs for details", true);
        }
    }
    
    private boolean isOldTaskValid(Map<String, String> xmlATValues, String fullName)
    {
    	boolean result = true;
    	
    	if ((StringUtils.isBlank(xmlATValues.get("u_accessrights")) && StringUtils.isBlank(xmlATValues.get("u_assigned_to"))) ||
    			(fullName.matches("^.*\\s{2,}.*$")))
    	{
    		result = false;
    	}
    	
    	return result;
    }
    
    private void deleteFile(String fileName)
    {
        File file = new File(fileName);
        file.delete();
    }
    
    private List<File> getFileList(String partialFileName)
    {
        File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        FileFilter fileFilter = new WildcardFileFilter(partialFileName + "_*");
        File[] files = dir.listFiles(fileFilter);
        
        List<File> fileList = new ArrayList<File>();
        Collections.addAll(fileList, files);
        Iterator<File> fileIterator = fileList.iterator();
        
        while (fileIterator.hasNext())
        {
            File file = fileIterator.next();
            try
            {
                XDoc doc = ImpexUtil.convertFileToXDoc(file);
                if (!doc.getStringValue("/record_update/@table").equals(partialFileName))
                {
                    fileIterator.remove();
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return fileList;
    }
    
    private void getNewIdentity(File dir, Map<String, String> xmlATValues, String xmlFullName) throws Exception
    {
        String xmlAccessRightsId = xmlATValues.get("u_accessrights");
        String newAccessRightsId = Guid.getGuid(true);
        if (StringUtils.isNotBlank(xmlAccessRightsId))
        {
            replacableSysIdMap.put(xmlAccessRightsId, newAccessRightsId);
            grepFiles(xmlAccessRightsId);
        }
        else
        {
            ImpexUtil.logImpexMessage("ActionTask: " + xmlATValues.get("u_fullname") +  " does not have an AccessRights. Please verify." , true);
        }
        
        String xmlInvocId = xmlATValues.get("u_invocation");
        String newInvocId = Guid.getGuid(true);
        replacableSysIdMap.put(xmlInvocId, newInvocId);
        // This will take care of ActionTask, ActionInvoc, InvocOptions, Parameters and Mock
        grepFiles(xmlInvocId);
        
        XDoc actionInvocXdox = getXDocById(xmlInvocId, dir);
        Map<String, String> xmlActionInvocValues = prepareMapFromXML(actionInvocXdox, "resolve_action_invoc");
        
        String xmlParserId = xmlActionInvocValues.get("u_parser");
        if (StringUtils.isNotBlank(xmlParserId))
        {
            if (isLocalReference(xmlFullName, "resolve_parser_"+ xmlParserId + ".xml", dir))
            {
                String newParserId = Guid.getGuid(true);
                replacableSysIdMap.put(xmlParserId, newParserId);
                grepFiles(xmlParserId);
            }
        }
        
        String xmlPreprocessId = xmlActionInvocValues.get("u_preprocess");
        if (StringUtils.isNotBlank(xmlPreprocessId))
        {
            if (isLocalReference(xmlFullName, "resolve_preprocess_"+ xmlPreprocessId + ".xml", dir))
            {
                String newPreprocessId = Guid.getGuid(true);
                replacableSysIdMap.put(xmlPreprocessId, newPreprocessId);
                grepFiles(xmlPreprocessId);
            }
        }
        
        String xmlAssessId = xmlActionInvocValues.get("u_assess");
        if (StringUtils.isNotBlank(xmlAssessId))
        {
            if (isLocalReference(xmlFullName, "resolve_assess_"+ xmlAssessId + ".xml", dir))
            {
                String newAssessId = Guid.getGuid(true);
                replacableSysIdMap.put(xmlAssessId, newAssessId);
                grepFiles(xmlAssessId);
            }
        }
        
        List<File> filesToGiveNewSysId = findFilesContainingString(xmlInvocId, "resolve_action_*");
        if (filesToGiveNewSysId.size() > 0)
        {
            for (File f : filesToGiveNewSysId)
            {
                String tableName = f.getName();
                tableName = tableName.substring(0, tableName.lastIndexOf('_'));
                
                XDoc localDoc = ImpexUtil.convertFileToXDoc(f);
                Map<String, String> localMapValues = prepareMapFromXML(localDoc, tableName);
                
                String localSysId = localMapValues.get("sys_id");
                if (StringUtils.isNotBlank(localSysId))
                {
                    String localNewId = Guid.getGuid(true);
                    replacableSysIdMap.put(localSysId, localNewId);
                    grepFiles(localSysId);
                }
            }
        }
    }
    
    /**
     * 
     * @param id 
     *  Search all files containing this id.
     * @param fileNameToSearch
     *  To narrow down the search.
     */
    private List<File> findFilesContainingString(String id, String fileNameToSearch)
    {
        List<File> result = new ArrayList<File>();
        File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        FileFilter fileFilter = new WildcardFileFilter(fileNameToSearch);
        File[] files = dir.listFiles(fileFilter);
        
        if (files != null && files.length > 0)
        {
            for (File f : files)
            {
                String name = f.getName();
                if (name.contains("mock") || name.contains("options") || name.contains("parameter"))
                {
                    try
                    {
                        String content = FileUtils.readFileToString(f);
                        if (content.contains(id))
                        {
                            result.add(f);
                        }
                    }
                    catch (IOException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }
        }
        
        return result;
    }
    
    private boolean isLocalReference(String actionTaskName, String compFileName, File dir)
    {
        boolean result = false;
        
        FileFilter nameFileFilter = new NameFileFilter(compFileName);
        File[] fileArray = dir.listFiles(nameFileFilter);
        
        if (fileArray != null && fileArray.length == 1)
        {
            // Check if it's local parser.
            try
            {
                if (FileUtils.readFileToString(fileArray[0]).contains(actionTaskName))
                {
                    result = true;
                }
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    }
    
    /**
     * 
     * @param actionTaskId
     *      sys_id of an ActionTask which needs to be cleaned
     * @param xmlActionInvocSysId
     *      sys_id of ActionInvoc present in the import module. We will insert only new options from import module to DB Action Task.
     */
    private void cleanActionTask(String actionTaskId, String xmlActionInvocSysId, List<String> excludedCompList)
    {
        /*
         * While importing, if we find same named action task (with different sys_id) on the local system,
         * we are updating local task, by retaining it's sys_id.
         * To void any orphan params, options or mock data records, we are cleaning it before import starts. 
         */
        
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	HibernateProxy.execute(() -> {
            
	            ResolveActionTask resolveActionTask = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(actionTaskId);
	            
	            ResolveActionInvoc actionInvoc = resolveActionTask.getResolveActionInvoc();
	            
	            if (actionInvoc != null)
	            {
	                // Delete Parameters
	                Collection<ResolveActionParameter> parameterColl = actionInvoc.getResolveActionParameters();
	                if (parameterColl != null && parameterColl.size() > 0)
	                {
	                    for (ResolveActionParameter parameter : parameterColl)
	                    {
	                    	if (excludedCompList == null)
	                    	{
	                    		HibernateUtil.getDAOFactory().getResolveActionParameterDAO().delete(parameter);
	                    	}
	                    	else
	                    	{
	                    		if (!excludedCompList.contains(parameter.getSys_id()))
	                    		{
	                    			HibernateUtil.getDAOFactory().getResolveActionParameterDAO().delete(parameter);
	                    		}
	                    	}
	                    }
	                }
	//                
	                // Delete mock data
	                Collection<ResolveActionTaskMockData> mockDataColl = actionInvoc.getMockDataCollection();
	                if (mockDataColl != null && mockDataColl.size() > 0)
	                {
	                    for (ResolveActionTaskMockData mockData : mockDataColl)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveActionTaskMockDataDAO().delete(mockData);
	                    }
	                }
	                
	//                HibernateUtil.getDAOFactory().getResolveActionInvocDAO().delete(actionInvoc);
	            }
	            else
	            {
	                Log.log.error("Action Invo is null for ActionTask: " + resolveActionTask.getUFullName());
	            }

        	});
            
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    public XDoc getXDocById(String sys_id, File dir) throws Exception
    {
        FileFilter localFileFilter = new WildcardFileFilter("*" + sys_id + "*");
        File[] refFiles = dir.listFiles(localFileFilter);
        return ImpexUtil.convertFileToXDoc(refFiles[0]);
    }
    
    public void grepFiles(String regex)
    {
        if (StringUtils.isBlank(regex))
        {
            return;
        }
//        List<String> fileList = new ArrayList<String>();
        Pattern p = Pattern.compile(regex, Pattern.DOTALL | Pattern.MULTILINE);
        
        File dir = new File (GLIDE_INSTALL_FOLDER_LOCATION);
        WildcardFileFilter filter = new WildcardFileFilter("*.xml");
        Collection<File> fileCollection = FileUtils.listFiles(dir, filter, TrueFileFilter.INSTANCE);
        Iterator<File> iterator = fileCollection.iterator();
        while (iterator.hasNext())
        {
            File f = iterator.next();
            try
            {
                String content = FileUtils.readFileToString(f);
                Matcher m = p.matcher(content);
                
                if (m.find())
                {
                    fileProcessMap.put(f.getName(), true);
                }
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    public void checkDependencies(String tableName) throws Exception
    {
//        System.out.println("checkDependencies: " + tableName);
        ImportValidationRuleVO validationVO = validationMap.get(tableName);
        if (validationVO != null)
        {
            if (StringUtils.isBlank(validationVO.getUDependentTables()) && !processedTables.contains(tableName))
            {
//                writeToValidationFile(tableName);
                validateXmlFile(tableName);
                if (StringUtils.isNotBlank(validationVO.getChidren()))
                {
                    String children[] = validationVO.getChidren().split(",");
                    for (String child : children)
                    {
                        checkDependencies(child);
                    }
                }
            }
            else
            {
                if (StringUtils.isNotBlank(validationVO.getUDependentTables()))
                {
                    String[] dependentTables = validationVO.getUDependentTables().split(",");
                    for (String dependentTableName: dependentTables)
                    {
                        if (!processedTables.contains(dependentTableName))
                        {
                            checkDependencies(dependentTableName.trim());
                        }
                    }
                }
//                writeToValidationFile(tableName);
                validateXmlFile(tableName);
                if (StringUtils.isNotBlank(validationVO.getChidren()))
                {
                    String children[] = validationVO.getChidren().split(",");
                    for (String child : children)
                    {
                        checkDependencies(child);
                    }
                }
            }
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void validateXmlFile(String tableName)
    {
        File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        WildcardFileFilter fileFilter = new WildcardFileFilter(tableName+ "_*");
        Collection<File> fileCollection = FileUtils.listFiles(dir, fileFilter, TrueFileFilter.INSTANCE);
        
        processedTables.add(tableName);
        
        if (CollectionUtils.isEmpty(fileCollection)) {
        	return;
        }
        
        try {
        	boolean isNewStage = "access_rights".equalsIgnoreCase(tableName) || 
        						 "meta_access_rights".equalsIgnoreCase(tableName);
        	
        	if (isNewStage) {
        		String stageIndex = "access_rights".equalsIgnoreCase(tableName) ? "(20/39)" : "(21/39)";
        		ImpexUtil.initImpexOperationStage(operation, String.format("%s Validate %s Glide Components", stageIndex, 
        																   tableName), 
        										   fileCollection.size());
        	}
        	
	        for (File xmlFile : fileCollection)
	        {
	        	if (isNewStage) {
	        		ImpexUtil.updateImpexCount(operation);
	        	}
	        	
	            if (!fileProcessMap.get(xmlFile.getName()))
	            {
	                XDoc doc = null;
	                try
	                {
	                    doc = ImpexUtil.convertFileToXDoc(xmlFile);
	                }
	                catch (Exception e1)
	                {
	                    Log.log.error(e1.getMessage(), e1);
	                }
	                String localTableName = doc.getStringValue("/record_update/@table");
	                
	                if (!tableName.equals(localTableName))
	                {
	                    continue;
	                }
	                fileProcessMap.put(xmlFile.getName(), new Boolean(true));
	                
	                Map<String, String> values = prepareMapFromXML(doc, localTableName);
	                
	                if (tableName.equals("wikidoc_metaform_rel"))
	                {
	                    String docName = values.get("u_wikidoc_fullname");
	                    WikiDocumentVO docVO = WikiUtils.getWikiDoc(docName);
	                    if (docVO != null)
	                    {
	                        String xmlDocSysId = values.get("u_wikidoc_sys_id");
	                        if (StringUtils.isNotBlank(xmlDocSysId) && !xmlDocSysId.equals(docVO.getSys_id()))
	                        {
	                            try
	                            {
	                                String xmlContent = FileUtils.readFileToString(xmlFile);
	                                xmlContent = xmlContent.replace(xmlDocSysId, docVO.getSys_id());
	                                FileUtils.writeStringToFile(xmlFile, xmlContent);
	                            }
	                            catch (IOException e)
	                            {
	                                Log.log.error("Could not read " + xmlFile.getName() + ". Message: " + e.getMessage());
	                            }
	                        }
	                    }
	                }
	                
	                ImportValidationRuleVO vo = validationMap.get(localTableName);
	                List<String> selectList = new ArrayList<String>();
	                Map<String, String> dbMap = new HashMap<String, String>();
	                if (vo != null)
	                {
	                    if (StringUtils.isNotBlank(vo.getUUniqueTableColumnNames()))
	                    {
	                        StringBuilder selectSql = new StringBuilder();
	                        selectSql.append("select sys_id ");
	                        selectList.add("sys_id");
	                        if (StringUtils.isNotBlank(vo.getForeignRefs()))
	                        {
	                            String refs[] = vo.getForeignRefs().split(",");
	                            for (String ref : refs)
	                            {
	                                selectSql.append(", ").append(ref);
	                                selectList.add(ref);
	                            }
	                        }
	                        selectSql.append(" from ").append(localTableName);
	                        
	//                        if (StringUtils.isNotBlank(vo.getUUniqueTableColumnNames()))
	//                        {
	                        selectSql.append(" where ");
	                        String uniqueCols[] = vo.getUUniqueTableColumnNames().split(",");
	//                        boolean gotUniqueCols = false;
	                        
	                        Map<Integer, Object> queryParams = new HashMap<Integer, Object>();
	                        int parameter = 0;
	                        for (String uniqueCol : uniqueCols)
	                        {
	                            parameter++;
	                            if (values.get(uniqueCol) != null)
	                            {
	//                                gotUniqueCols = true;
	                                String value = values.get(uniqueCol);
	                                if (NumberUtils.isNumber(value) && (uniqueCol.equals("u_order") || uniqueCol.equals("u_sequence")))
	                                {
	                                    /*
	                                     * If the value is number, write a query considering it as a number.
	                                     * Well, it's not an ideal solution, as the exported XML file does not contain column data type.
	                                     * Perhaps in the future release we will include that.
	                                     */
	                                    selectSql.append(uniqueCol).append(" = ").append(value).append(" and ");
	                                }
	                                else
	                                {
	                                    value = checkMapForReplacement(value).replaceAll("'", "''");
	                                    //selectSql.append("LOWER(" + uniqueCol + ")").append(" = '").append(value.toLowerCase()).append("' and ");
	                                    selectSql.append("LOWER(" + uniqueCol + ")").append(" = :a" + parameter + " and ");
	                                    queryParams.put(parameter, value.toLowerCase());
	                                }
	                            }
	                            else
	                            {
	                                if (localTableName.equals("resolve_action_invoc") && uniqueCol.equals("u_description"))
	                                {
	                                    String description = actionInvocDescMap.get(values.get("sys_id"));
	                                    //selectSql.append(uniqueCol).append(" = '").append(description).append("' and ");
	                                    selectSql.append(uniqueCol).append(" = :a" + parameter + " and ");
	                                    queryParams.put(parameter, description);
	                                }
	                                else
	                                {
	                                    selectSql.append(" (").append(uniqueCol).append(" is null OR ").append(uniqueCol).append(" = '' ) and ");
	                                }
	                            }
	                        }
	//                        if (gotUniqueCols)
	                        {
	                            selectSql.replace(selectSql.length() - 4, selectSql.length(), "");
	                        }
	                        
	                        if (selectSql.toString().endsWith("where "))
	                        {
	                            break;
	                        }
	//                        }
	                        
	                        Log.log.info(selectSql.toString() + ", params: " + queryParams);
	                        // System.out.println(selectSql.toString());
	                        List<Object> result = null;
	                        try
	                        {
	                            result = (List<Object>) HibernateProxy.execute( () -> {
	                            	  Query sqlQuery = HibernateUtil.createSQLQuery(selectSql.toString());
	  	                            
	  	                            if (queryParams != null && !queryParams.isEmpty())
	  	                            {
	  	                                for (Integer paramIndx : queryParams.keySet())
	  	                                {
	  	                                    sqlQuery.setParameter("a" + paramIndx, (String)queryParams.get(paramIndx));
	  	                                }
	  	                            }
	  	                            
	  	                            return sqlQuery.list();
	                            });                          
	                            
	                           
	                        }
	                        catch (Exception e)
	                        {
	                            Log.log.error("Impex validateXmlFile. SQL Query: " + selectSql.toString(), e);
                                                        HibernateUtil.rethrowNestedTransaction(e);
	                            
	                        }
	                        
	                        if (result != null && result.size() > 0)
	                        {
	                            if (result.size() > 1)
	                            {
	                                Log.log.error("** ERROR **. Found multiple records during import validation for query: " + selectSql.toString() + ", params: " + queryParams);
	                            }
	                            
	                            int i = 0;
	                            
	                            if (selectList.size() == 1)
	                            {
	                                dbMap.put(selectList.get(0), (String)result.get(0));
	                            }
	                            else
	                            {
	                                Object objArray[] = (Object[])result.get(0);
	                                
	                                for (String select : selectList)
	                                {
	                                    dbMap.put(select, (String)objArray[i++]);
	                                }
	                            }
	                            
	                            String xmlSysId = values.get("sys_id");
	                            xmlSysId = checkMapForReplacement(xmlSysId);
	                            
	                            if (!xmlSysId.equals(dbMap.get("sys_id")))
	                            {
	                                String previousValue = replacableSysIdMap.put(xmlSysId, dbMap.get("sys_id"));
	                                if (previousValue != null)
	                                {
	                                    if (!previousValue.equalsIgnoreCase(dbMap.get("sys_id")))
	                                    {
	                                        Log.log.error("####################### Key collision found while validation. Key: " + xmlSysId + ", Previous Value: " + previousValue + ", new Value: " + dbMap.get("sys_id") + "  ####################### ", new Exception("Collision Validation Error"));
	                                    }
	                                }
	                            }
	                            
	                            if (selectList.size() > 1)
	                            {
	                                for (i = 1; i<selectList.size(); i++)
	                                {
	                                    String source = values.get(selectList.get(i));
	                                    source = checkMapForReplacement(source);
	                                    
	                                    String dest = dbMap.get(selectList.get(i));
	                                    
	                                    if (StringUtils.isNotBlank(source) && StringUtils.isNotBlank(dest))
	                                    {
	                                        if (!source.equals(dest))
	                                        {
	                                            {
	                                                if (replacableSysIdMap.put(source, dest) != null)
	                                                {
	                                                    Log.log.error("\n#######################\nKey Collision found while validation. Source: " + source + "\n#######################\n", new Exception("Collision Validation Error"));
	                                                }
	                                            }
	                                        }
	                                    }
	                                }
	                            }
	                        }
	                    }
	                }
	            }
	        }
        } catch (Exception e) {
        	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
						  e.getMessage() : String.format("Failed Validating %s Glide Components.", tableName ), e);
        	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
        							  e.getMessage() : 
        							  String.format("Failed Validating %s Glide Components.", tableName), true);
        }
    }
    
    /**
     * Check if the replacableSysIdMap contains id to be replaced by dbId.
     * If present, return that otherwise return the same xmlSysId.
     * @param xmlSysId
     *      sys_id to to be checked for.
     * @return
     *      If replacableSysIdMap contains a value for xmlSysId, return it,
     *      otherwise return the same xmlSysId.
     */
    
    private String checkMapForReplacement(String xmlSysId)
    {
        String result = xmlSysId;
        
        if (replacableSysIdMap.get(xmlSysId) != null)
        {
            result = replacableSysIdMap.get(xmlSysId);
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, String> prepareMapFromXML(XDoc doc, String table)
    {
        HashMap<String, String> row = new HashMap<String, String>();
        
        List<Element> columns = doc.getNodes("/record_update/" + table + "/*");
        for(Element e : columns)
        {
            String name = e.getName();
            String value = e.getText();
            row.put(name, value);
        }//end of for loop
        
        return row;
    }
    
    public void replaceAllSysIds(/*String sourceSysId, String destSysId*/)
    {
        Set<String> sourceIdSet = replacableSysIdMap.keySet();

        if (sourceIdSet.size() > 0)
        {
            Collection<File> fileCollection = FileUtils.listFiles(FileUtils.getFile(MODULE_FOLDER_LOCATION),  null, true);
            Map<String, String> modifiedFileMap = new HashMap<String, String>();
            
            if (CollectionUtils.isEmpty(fileCollection)) {
            	return;
            }
            
            try {
            	ImpexUtil.initImpexOperationStage(operation, "(22/51) Replace SysIds in Glide Components", fileCollection.size());
	            for (File file : fileCollection)
	            {
	            	ImpexUtil.updateImpexCount(operation);
	                String content;
	                try
	                {
	                    content = FileUtils.readFileToString(file);
	                    for (String sourceSysId : sourceIdSet)
	                    {
	                        if (content.contains(sourceSysId))
	                        {
	                            String destSysId = replacableSysIdMap.get(sourceSysId);
	                            String tmpFileName = file.getName();
	                        	if (modifiedFileMap.containsKey(tmpFileName) && modifiedFileMap.get(tmpFileName).equals(sourceSysId))
	                        	{
	                        		continue;
	                        	}
	                            Log.log.info("Replacing source: " + sourceSysId + " to: " + destSysId + " in file: " + file.getName());
	                            
	                            content = content.replaceAll(sourceSysId , destSysId);
	                            FileUtils.writeStringToFile(file, content);
	                            if (file.getName().contains(sourceSysId))
	                            {
	                            	// This map contains fileName (record) whose sys_id got modified with the destSysId.
	                            	modifiedFileMap.put(file.getName(), destSysId);
	                            }
	                        }
	                    }
	                }
	                catch (IOException e)
	                {
	                    Log.log.error(e.getMessage(), e);
	                }
	            }
            } catch (Exception e) {
            	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
            										e.getMessage() : "Failed Replacing SysIds in Glide Components.", e);
                ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
                												 e.getMessage() : 
                												 "Failed Replacing SysIds in Glide Components.", 
                						  true);
            }
        }
        else
        {
        	Log.log.info("No sysId to replace.");
        }
    }
    
    @SuppressWarnings("unchecked")
    public void checkRefTables() throws Exception
    {
        File dir = new File (GLIDE_INSTALL_FOLDER_LOCATION + "tabledef/");
        
        if (dir.isDirectory())
        {
            WildcardFileFilter filter = new WildcardFileFilter("custom_table_*.xml");
            Collection<File> customTableFiles = FileUtils.listFiles(dir, filter, TrueFileFilter.INSTANCE);
            
            List<String> cTables = new ArrayList<String>();
            Set<String> refTables = new HashSet<String>();
            
            if (CollectionUtils.isEmpty(customTableFiles)) {
            	return;
            }
            
            try {
            	ImpexUtil.initImpexOperationStage(operation, "(7/51) Check Referenced Custom Tables", customTableFiles.size());
	            for (File file : customTableFiles)
	            {
	            	ImpexUtil.updateImpexCount(operation);
	                try
	                {
	                    XDoc ctDoc = new XDoc(file);
	                    cTables.add(ctDoc.getValue("/record_update/custom_table/u_name"));
	                    
	                    String schema = ctDoc.getValue("/record_update/custom_table/u_schema_definition");
	                    ctDoc = new XDoc(schema);
	                    List<DefaultAttribute> defaultAttributes = ctDoc.getNodes("//class/many-to-one/@entity-name");
	                    
	                    if (defaultAttributes != null && defaultAttributes.size() > 0)
	                    {
	                        for (DefaultAttribute attrib : defaultAttributes)
	                        {
	                            if (StringUtils.isNotBlank(attrib.getValue()))
	                            {
	                                refTables.add(attrib.getValue());
	                            }
	                        }
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error(e.getMessage(), e);
	                }
	            }
            } catch (Exception e) {
            	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
            										e.getMessage() : "Failed Checking Referenced Custom Tables.", e);
        		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
        								  e.getMessage() : 
        								  "Failed Checking Referenced Custom Tables. Please check RSView logs for details",
        								  true);
            }
            
            Set<String> missingTables = new HashSet<String>();
            
            if (CollectionUtils.isNotEmpty(refTables)) {
            	try {
            		ImpexUtil.initImpexOperationStage(operation, "Create Missing Referenced Custom Tables List", 
            										   refTables.size());
		            for (String refTable: refTables)
		            {
		            	ImpexUtil.updateImpexCount(operation);
		                if (!cTables.contains(refTable))
		                {
		                    String table = HibernateUtil.class2Table(refTable);
		                    
		                    if (table == null)
		                    {
		                        missingTables.add(refTable);
		                    }
		                }
		            }
            	} catch(Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
								  e.getMessage() : "Failed Creating Missing Referenced Custom Tables List.", e);
            		ImpexUtil.logImpexMessage(
            			StringUtils.isNotBlank(
            				e.getMessage()) ? 
            				e.getMessage() : 
            				"Failed Creating Missing Referenced Custom Tables List. Please check RSView logs for details",
            				true);
            	}
            }
            
            if (missingTables.size() > 0)
            {
                ImpexUtil.logImpexMessage("Not all referred custom tables are present in this exported module. Please fix it by re-exporting it with all referred custom tables, " + missingTables + ", and try re-importing.", true);
                throw new Exception("Not all referred custom tables are present in this exported module. Please fix it by re-exporting it with all referred custom tables, " + missingTables + ", and try re-importing.");
            }
        }
    }
    
    public void checkForSameNamedAB()
    {
        FileFilter fileFilter = new WildcardFileFilter("rb_general_*");
        File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        File[] files = dir.listFiles(fileFilter);
        XDoc doc = null;
        
        if (files.length == 0) {
        	return;
        }
        
        try {
        	ImpexUtil.initImpexOperationStage(operation, 
        									   "(8/51) Checking For Dupilcate Action Tasks Built By Action Task Builder in " +
        									   "Glide Components", files.length);
	        for (File file : files)
	        {
	        	ImpexUtil.updateImpexCount(operation);
	            try
	            {
	                doc = ImpexUtil.convertFileToXDoc(file);
	                if (doc != null && doc.getStringValue("/record_update/@table").equals("rb_general"))
	                {
	                    Map<String, String> xmlABValues = prepareMapFromXML(doc, "rb_general");
	                    String abName = xmlABValues.get("u_name");
	                    String abNamespace = xmlABValues.get("u_namespace");
	                    
	                    QueryDTO queryDTO = new QueryDTO();
	                    queryDTO.setWhereClause(" namespace = '" + abNamespace + "' and name = '" + abName + "' ");
	                    List<RBGeneralVO> generalList = ServiceResolutionBuilder.listGeneral(queryDTO, "admin");
	                    
	                    if (generalList != null && !generalList.isEmpty())
	                    {
	                        // OK, same named AB is found. Delete just the metadata.
	                        for (RBGeneralVO rbGeneralVO : generalList)
	                        {
	                            ServiceResolutionBuilder.deleteGeneral(rbGeneralVO.getSys_id(), false, "admin");
	                        }
	                    }
	                }
	                
	            }
	            catch(Exception e)
	            {
	                Log.log.error(e.getMessage());
	            }
	        }
        } catch (Exception e) {
        	Log.log.error(
            	StringUtils.isNotBlank(
            		e.getMessage()) ? 
            		e.getMessage() : 
            		"Failed Checking For Dupilcate Action Tasks Built By Action Task Builder in Glide Components.", e);
        	ImpexUtil.logImpexMessage(
        		StringUtils.isNotBlank(
        			e.getMessage()) ? 
        			e.getMessage() : 
        			"Failed Checking For Dupilcate Action Tasks Built By Action Task Builder in Glide Components. " +
        			"Please check RSView logs for details", 
        		true);
        }
    }
    
    public void validateSocialCompsName()
    {
    	List<String> fileWildCardList = new ArrayList<String>();
    	fileWildCardList.add("social_process_*");
    	fileWildCardList.add("social_team_*");
    	fileWildCardList.add("social_forum_*");
    	fileWildCardList.add("rss_subscription_*");
    	
    	FileFilter fileFilter = new WildcardFileFilter(fileWildCardList);
        File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        
        File[] files = dir.listFiles(fileFilter);
        if (files.length > 0)
        {
        	try {
		        XDoc doc = null;
		        ImpexUtil.initImpexOperationStage(operation, "(10/51) Validate Social Glide Component Names", files.length);
		        for (File file : files)
		        {
		        	ImpexUtil.updateImpexCount(operation);
		        	String tableName = null;
		        	String compName = null;
		        	try
		        	{
		        		doc = ImpexUtil.convertFileToXDoc(file);
		        		tableName = doc.getSecureStringValue("/record_update/@table");
		        		Node node = doc.getNode("/record_update/"+ tableName + "/u_display_name");
		        		
		        		if (node != null)
		        		{
		        		    compName = node.getText();
	    	        		if (!compName.matches("^[a-zA-Z0-9 ]*$"))
	    	                {
	    	                    ImpexUtil.logImpexMessage("WARNING: " + tableName.toUpperCase() + ", " + compName + ", should contain only alphanumeric characters and spaces.", true);
	    	                    file.delete();
	    	                }
		        	    }
		        		else
		        		{
		        		    ImpexUtil.logImpexMessage("WARNING: There's no name assocoated with this component: " +  file.getName() + ". Please check the export log for any exception(s).", true);
		        		    file.delete();
		        		}
		        	}
		        	catch(Exception e)
		        	{
		        		Log.log.error("Could not validate name for " + tableName.toUpperCase() + ", " + compName, e);
		        	}
		        }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
        											e.getMessage() : "Failed Validating Social Glide Component Names.", e);
            	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
            							  e.getMessage() : "Failed Validating Social Glide Component Names.", true);
        	}
        }
    }
    
    public Set<String> checkForSameNamedUser(
                    		Map<String, Collection<UserGroupRel>> delUsrGrpRelsByUsr,
                    		Map<String, Collection<UserRoleRel>> delUsrRoleRelsByUsr)
    {
    	Set<String> newUserImportFileNames = new HashSet<String>();
        FileFilter fileFilter = new WildcardFileFilter("sys_user_*");
        File dir = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        File[] files = dir.listFiles(fileFilter);
        XDoc doc = null;
        
        if (files.length == 0) {
        	return newUserImportFileNames;
        }
        
        try {
        	ImpexUtil.initImpexOperationStage(operation, "(9/51) Check For Duplicate Users", files.length);
	        for (File file : files)
	        {
	        	ImpexUtil.updateImpexCount(operation);
	            try
	            {
	                doc = ImpexUtil.convertFileToXDoc(file);
	                if (doc != null && doc.getStringValue("/record_update/@table").equals("sys_user"))
	                {
	                    Map<String, String> xmlABValues = prepareMapFromXML(doc, "sys_user");
	                    String userName = xmlABValues.get("user_name");
	                    
	                    try
	                    {
                       HibernateProxy.setCurrentUser(userName);
	                    	HibernateProxy.execute(() -> {
	                        
		                        Users user = UserUtils.findUser(null, userName);
		                        if (user != null)
		                        {
		                        	//delete user group relations
		                        	Collection<UserGroupRel> usrGrpRels = user.getUserGroupRels();
		                        	
		                        	if (CollectionUtils.isNotEmpty(usrGrpRels)) {
		                        		if (!delUsrGrpRelsByUsr.containsKey(userName)) {
		                        			for (UserGroupRel rel : usrGrpRels) {
		                        				HibernateUtil.getDAOFactory().getUserGroupRelDAO().delete(rel);
		                        			}
		                        			delUsrGrpRelsByUsr.put(userName, usrGrpRels);
		                        		}
		                        	}
		                            
		                            //delete user role relations
			                        Collection<UserRoleRel> userRoleRels = user.getUserRoleRels();
			                        
			                        if (CollectionUtils.isNotEmpty(userRoleRels)) {
			                            if (!delUsrRoleRelsByUsr.containsKey(userName)) {
			                            	for (UserRoleRel rel : userRoleRels) {
			                            		HibernateUtil.getDAOFactory().getUserRoleRelDAO().delete(rel);
			                            	}
			                            	delUsrRoleRelsByUsr.put(userName, userRoleRels);
		                                }
			                        }
		                        } else {
		                        	newUserImportFileNames.add(file.getCanonicalPath());
		                        }
	                        
	                    	});
                        } catch (Throwable t) {
                        	Log.log.error(t.getMessage(), t);
                                                                         HibernateUtil.rethrowNestedTransaction(t);
                        }
	                }
                } catch(Exception e) {
                	Log.log.error(e.getMessage());
                }
	        }
        } catch (Exception e) {
        	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Failed Checking For Duplicate Users.", e);
        	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
        							  e.getMessage() : "Failed Checking For Duplicate Users.", true);
        }
        
        return newUserImportFileNames;
    }
}
