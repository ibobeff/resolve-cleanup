/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.services.hibernate.vo.ImportValidationRuleVO;

public class ImportValidationRules
{
    static Map<String, ImportValidationRuleVO> resultMap;
    static List<String> modelHirarchyList = new ArrayList<String>();
    static List<String> ctModelHirarchyList = new ArrayList<String>();
    
    public static Map<String, ImportValidationRuleVO> getImportValidationRules()
    {
        if (resultMap == null)
        {
            resultMap = new HashMap<String, ImportValidationRuleVO>();
            initImportValidationRulesMap();
        }
        
        return resultMap;
    }
    
    private static void initImportValidationRulesMap()
    {
        // ActionTask Group and order
        populateVO("resolve_action_task", null,"u_fullname", "u_invocation", "resolve_action_invoc");
        populateVO("resolve_action_invoc", "resolve_action_task", "u_description", null, null);
        populateVO("resolve_assess", "resolve_action_invoc", "u_name", null, null);
        populateVO("resolve_preprocess", "resolve_action_invoc", "u_name", null, null);
        populateVO("resolve_parser", "resolve_action_invoc", "u_name", null, null);
        populateVO("resolve_action_invoc_options","resolve_action_invoc","u_name,u_action_invocation", null, null);
        populateVO("resolve_action_task_pkg_rel", "resolve_action_task", "u_resolve_action_task_name,u_tag_fullname", null, null);
        populateVO("resolve_action_parameter","resolve_action_invoc","u_type,u_name,u_invocation", null, null);
        populateVO("resolve_action_task_mock_data", "resolve_action_invoc", "u_name,u_invocation", null, null);
        populateVO("resolve_task_expression", "resolve_action_invoc", "u_invocation,u_criteria,u_left_operand_type,u_left_operand_name,u_operator,u_right_operand_type,u_right_operand_name,u_order,u_expression_type,u_result_level", null, null);
        populateVO("resolve_task_output_mapping", "resolve_action_invoc", "u_invocation,u_output_type,u_output_name,u_source_type,u_source_name", null, null);

        // Wiki
        populateVO("wikidoc", null,"u_fullname", null, "wikidoc_attachment_rel,wikidoc_resolve_action_pkg_rel,wikidoc_statistics,role_wikidoc_homepage_rel,wikidoc_rating_resolution,wikidoc_rating_quality");
        populateVO("wikiattachment", null, "u_fullname", null, "wikiattachmentcontent");
        populateVO("wikiattachmentcontent", "wikiattachment", "u_wikiattch_sys_id", null, null);
        populateVO("wikidoc_attachment_rel", "wikidoc,wikiattachment", "u_parent_wikidoc_sys_id,u_attachment_sys_id,u_wikidoc_sys_id", null, null);
        populateVO("wikidoc_resolve_action_pkg_rel", "wikidoc", "u_tag_fullname", "u_wikidoc_sys_id", null);
        populateVO("wikidoc_statistics", "wikidoc", "u_wikidoc_sys_id", null, null);
        populateVO("role_wikidoc_homepage_rel", "wikidoc,sys_user_role", "u_wikidoc_sys_id,u_role_sys_id", null, null);
        populateVO("wikidoc_rating_resolution", "wikidoc", "u_wikidoc_sys_id", null, null);
        populateVO("wikidoc_rating_quality", "wikidoc", "u_wikidoc_sys_id", null, null);
        populateVO("content_wikidoc_task_rel", "wikidoc", "u_wikidoc_fullname,u_content_wikidoc_sys_id", null, null);
        populateVO("content_wikidoc_wikidoc_rel","wikidoc","u_wikidoc_fullname,u_content_wikidoc_sys_id", null, null);
        populateVO("wikiarchive","wikidoc","u_table_sys_id,u_version,u_table_column", null, null);
        populateVO("wikidoc_metaform_rel","meta_form_view","u_name", "u_metaform_sys_id", null);

        // Users, Role, Group
        populateVO("sys_user", null,"user_name", null, "user_role_rel,user_group_rel");
        populateVO("sys_user_role", null,"name", null, "user_role_rel");
        populateVO("groups", null,"u_name", null, "user_group_rel,group_role_rel");
        populateVO("user_role_rel", "sys_user,sys_user_role", "u_role_sys_id,u_user_sys_id", null, null);
        populateVO("user_group_rel", "sys_user,groups", "u_group_sys_id,u_user_sys_id", null, null);
        populateVO("group_role_rel", "groups,sys_user_role", "u_group_sys_id,u_role_sys_id", null, null);
        populateVO("user_preferences","sys_user", "u_pref_group,u_pref_key,u_pref_value,u_user_sys_id", null, null);

        // Custom tables
        populateVO("custom_table", null,"u_name", "u_meta_table_sys_id", "meta_table,meta_field");
        populateVO("meta_control", null, "u_name", null, null);
        populateVO("meta_table", "custom_table", "u_name", null, null);
        populateVO("meta_table_view","meta_table,meta_control", "u_meta_control_sys_id,u_meta_table_sys_id,u_name", "u_meta_control_sys_id,u_meta_table_sys_id,u_parent_sys_id", null);
        populateVO("meta_field", "custom_table", "u_table,u_name", "u_custom_table_sys_id,u_meta_field_properties_sys_id", null);
        populateVO("meta_table_view_field", "meta_table_view,meta_field,meta_field_properties",
                        "u_meta_field_sys_id,u_meta_field_properties_sys_id,u_meta_table_view_sys_id", "u_meta_view_field_sys_id", null);
        populateVO("meta_control_item", "meta_control", "u_meta_control_sys_id,u_sequence", null, "meta_form_action,metax_field_dependency");
        populateVO("meta_form_action", "meta_control_item", "u_meta_control_item_sys_id,u_operation", null, null);
        populateVO("meta_form_tab", "meta_form_view,metax_form_view_panel", "u_meta_form_view_sys_id,u_tab_name,u_order,u_metax_form_view_pnl_sys_id", null, "meta_form_tab_field,metax_field_dependency");
        populateVO("metax_field_dependency", "meta_field_properties,meta_control_item,meta_form_tab", "u_meta_field_prop_sys_id,u_meta_control_item_sys_id,u_meta_form_tab_sys_id", null, null);
        //populateVO("meta_field_properties", "meta_field", null, null, null);
        
        populateVO("meta_form_view", "meta_control", "u_meta_control_sys_id", "u_meta_control_sys_id", "meta_form_tab,meta_source");
        populateVO("meta_source", "meta_form_view", "u_name,u_type,u_meta_form_view_sys_id", "u_meta_field_properties_sys_id", null);
        populateVO("meta_form_view_properties", "meta_form_view", null, null, null);
        populateVO("metax_form_view_panel", "meta_form_view", "u_meta_form_view_sys_id", null, null);
        populateVO("metax_field_dependency", "meta_control,meta_field_properties,meta_form_tab", "u_meta_control_item_sys_id,u_meta_field_prop_sys_id,u_meta_form_tab_sys_id", null, null);
        populateVO("meta_filter", "meta_table", "u_name,u_meta_table_sys_id", null, null);
        populateVO("meta_form_tab_field", "meta_form_tab,meta_field,meta_source,meta_field_properties",
                        "u_meta_field_sys_id,u_meta_field_properties_sys_id,u_meta_form_tab_sys_id,u_meta_source_sys_id", null, null);
        
        populateVO("meta_style", "meta_field_properties,meta_form_tab_field,meta_table_view_field", null, null, null);
        populateVO("meta_view_field", "meta_table_view_field", null, null, null);

        // Menus
        populateVO("sys_app_application", null, "title", null, null);
        populateVO("sys_app_applicationroles", "sys_app_application", "value,parent_id", null, null);
        populateVO("sys_app_module", "sys_app_application", "title,application", null, null);
        populateVO("sys_app_moduleroles", "sys_app_module", "value,parent_id", null, null);
        
        populateVO("sys_perspective", null, "name", null, null);
        populateVO("sys_perspectiveapplications", "sys_perspective", "value,parent_id", null, null);
        populateVO("sys_perspectiveroles", "sys_perspective", "value,parent_id", null, null);
        
        // Miscellaneous
        populateVO("resolve_blueprint", null,"u_guid", null, null);
        populateVO("resolve_registration", null,"u_guid", null, null);
        populateVO("resolve_registration_property", "resolve_registration","u_name,u_registration", null, null);
        populateVO("properties", null,"u_name", null, null);
        populateVO("resolve_business_rule", null,"u_name", null, null);
        populateVO("resolve_cron", null,"u_name", null, null);
        populateVO("resolve_event_handler", null,"u_name", null, null);
        /*
         * resolve_event does not have any unique column. It's table on it's own and may never get exported.
         */
        populateVO("resolve_properties", null,"u_name,sys_org", null, null);
        populateVO("resolve_sys_script", null,"u_name", null, null);
        
        populateVO("database_connectionpool", null,"u_name,u_queue", null, null);
        populateVO("database_filter", null,"u_name,u_queue", null, null);
        populateVO("emailserver_filter", null,"u_name,u_queue", null, null);
        populateVO("ews_filter", null,"u_name,u_queue", null, null);
        populateVO("exchangeserver_filter", null,"u_name,u_queue", null, null);
        populateVO("hpom_filter", null,"u_name,u_queue", null, null);
        populateVO("remedyx_filter", null,"u_name,u_queue", null, null);
        populateVO("remedyx_form", null,"u_name,u_queue", null, null);
        populateVO("salesforce_filter", null,"u_name,u_queue", null, null);
        populateVO("servicenow_filter", null,"u_name,u_queue", null, null);
        populateVO("snmp_filter", null,"u_name,u_queue", null, null);
        populateVO("tsrm_filter", null,"u_name,u_queue", null, null);
        populateVO("xmpp_filter", null,"u_name,u_queue", null, null);
        populateVO("resolve_wiki_lookup", null,"u_order,u_regex", null, null);
        populateVO("social_post_attachment", null,"u_post_id", null, null);
        populateVO("ssh_pool", null,"u_queue,u_subnetmask", null, null);
        populateVO("telnet_pool", null,"u_queue,u_subnetmask", null, null);
        populateVO("resolve_archive", null,"u_table_sys_id,u_table_name", null, null);
        populateVO("amqp_filter", null,"u_name,u_queue", null, null);
        populateVO("ema_filters", null,"u_name,u_queue", null, null);
        populateVO("hpsm_filter", null,"u_name,u_queue", null, null);
        populateVO("netcool_filter", null,"u_name,u_queue", null, null);
        populateVO("tcp_filter", null,"u_name,u_queue", null, null);
        populateVO("caspectrum_filter", null,"u_name,u_queue", null, null);
        populateVO("http_filter", null,"u_name,u_queue", null, null);
        populateVO("pull_gateway_filter", null,"u_name,u_queue,u_gateway_name", null, null);
        populateVO("pull_gateway_filter_attr", "pull_gateway_filter","u_name,pull_filter_id", null, null);
        populateVO("push_gateway_filter", null,"u_name,u_queue,u_gateway_name", null, null);
        populateVO("push_gateway_filter_attr", "push_gateway_filter","u_name,push_filter_id", null, null);
        populateVO("msg_gateway_filter", null,"u_name,u_queue,u_gateway_name", null, null);
        populateVO("msg_gateway_filter_attr", "msg_gateway_filter","u_name,msg_filter_id", null, null);
        
        // Social Admin
        populateVO("social_process", "sys_user","u_display_name", null, null);
        populateVO("social_team", "sys_user","u_display_name", null, null);
        populateVO("social_forum", "sys_user","u_display_name", null, null);
        
        // Automation Builder
        populateVO("rb_general", null, "u_name,u_namespace", null, null);
        populateVO("rb_loop", "rb_general", "u_parent_id,u_parent_type,u_order", null, null);
        populateVO("rb_connection", "rb_general", "u_parent_id,u_order", null, null);
        populateVO("rb_task", "rb_connection", "u_parent_id,u_name,u_order", null, null);
        populateVO("rb_if", "rb_connection", "u_parent_id,u_parent_type,u_order", null, null);
        populateVO("rb_condition", "rb_if", "u_parent_id,u_parent_type,u_order", null, null);
        populateVO("rb_connection_param", "rb_connection", "u_name,u_connection_id", null, null);
        populateVO("rb_criterion", "rb_condition", "u_parent_id,u_parent_type", null, null);
        populateVO("rb_task_condition", "rb_task", "u_task_id,u_comparison,u_criteria,u_order,u_result,u_source,u_source_name,u_variable,u_variable_source", null, null);
        populateVO("rb_task_severity", "rb_task", "u_task_id,u_comparison,u_criteria,u_order,u_severity,u_source,u_source_name,u_variable,u_variable_source", null, null);
        populateVO("rb_task_variable", "rb_task", "u_task_id,u_name", null, null);
        
        // Resolution Routing
        populateVO("rr_schema", null, "u_name", null, null);
        populateVO("rr_rule", "rr_schema", "u_rid,u_module,u_schema_id", null, null);
        populateVO("rr_rule_field", "rr_rule", "u_name,u_value,u_rule", null, null);
        
        // App Security
        populateVO("sys_user_role", null,"name", null, null);
        populateVO("resolve_apps", null, "u_app_name", null, null);
        populateVO("resolve_controller", null, "u_controller_name", null, null);
        populateVO("resolve_app_roles_rel", "sys_user_role,resolve_apps", "u_resolve_app_sys_id,u_resolve_roles_sys_id", null, null);
        populateVO("resolve_app_controller_rel", "resolve_apps,resolve_controller", "u_resolve_app_sys_id,u_resolve_cntlr_sys_id", null, null);
        populateVO("resolve_app_org_rel", "resolve_apps", "u_resolve_app_sys_id,u_org_sys_id", null, null);
        
        // Metric Threshold
        populateVO("metric_threshold", null, "u_rulename,u_rulemodule,u_version", null, null);
        
        // Access Rights. These files will be processed at the end.
        populateVO("access_rights", null, "u_resource_sys_id,u_resource_name", null, null);
        populateVO("meta_access_rights", null, "u_resource_sys_id,u_resource_name", null, null);
        
        // CEF Artifacts
        populateVO("artifact_type", null, "u_type_name", null, null);
        populateVO("custom_artifact_key_dictionary", "artifact_configuration", "u_short_name,artifact_type_sys_id", null, null);
        populateVO("artifact_configuration", "artifact_type", "u_name,u_type", null, null);
    }
    
    private static void populateVO(String tableName, String dependentTableNames, String uniqueTableColumnNames, String foreignRefs, String children)
    {
        ImportValidationRuleVO vo = new ImportValidationRuleVO(tableName, dependentTableNames, uniqueTableColumnNames, foreignRefs, children);
        resultMap.put(tableName, vo);
        if (tableName.equals("custom_table") || tableName.startsWith("meta"))
        {
            ctModelHirarchyList.add(tableName);
        }
        else
        {
            modelHirarchyList.add(tableName);
        }
    }
    
    public static List<String> getModelHirarchyList()
    {
        if (modelHirarchyList.isEmpty())
        {
            getImportValidationRules();
        }
        
        return modelHirarchyList;
    }
    
    public static List<String> getCtModelHirarchyList()
    {
        if (ctModelHirarchyList.isEmpty())
        {
            getImportValidationRules();
        }
        
        return ctModelHirarchyList;
    }
}
