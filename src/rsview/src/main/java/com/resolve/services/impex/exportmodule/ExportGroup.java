/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.Collection;
import java.util.List;

import com.resolve.persistence.model.GroupRoleRel;
import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportGroup extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String groupName;
    
    public ExportGroup(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
        this.groupName = impexGlideVO.getUModule();
    }
    
    public void export() throws Exception
    {
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for Group " + groupName);
        }
        
        Groups group = getGroup();
        if (group != null)
        {
            populateGroup(group);
            if (impexOptions.getGroupRoleRel())
            {
                populateGroupRoleRel(group);
            }
            
            if (impexOptions.getGroupUsers())
            {
                Collection<UserGroupRel> userGroupRelCollection = group.getUserGroupRels();
                if (userGroupRelCollection != null)
                {
                    for (UserGroupRel userGroupRel : userGroupRelCollection)
                    {
                        populateUserGroupRel(userGroupRel);
                    }
                }
            }
        }
    }
    
    private void populateGroup(Groups group)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.Groups");
        
        dto.setType(GROUP);
        dto.setSys_id(group.getSys_id());
        dto.setFullName(group.getUName());
        dto.setModule(group.getUName());
        dto.setName(group.getUName());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        dto.setGroup(GROUP + " " + dto.getFullName());
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateUserGroupRel (UserGroupRel userGroupRel)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.UserGroupRel");
        
        dto.setType(USERGROUPREL);
        dto.setSys_id(userGroupRel.getSys_id());
        dto.setTablename(tableName);
        dto.setDisplayType(dto.getType());
        dto.setFullName(userGroupRel.getSys_id());
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateGroupRoleRel(Groups group)
    {
        Collection<GroupRoleRel> groupRoleRelColl = group.getGroupRoleRels();
        if (groupRoleRelColl != null)
        {
            for (GroupRoleRel groupRoleRel : groupRoleRelColl)
            {
                ImpexComponentDTO dto = new ImpexComponentDTO();
                String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.GroupRoleRel");
                
                dto.setType(GROUPROLEREL);
                dto.setSys_id(groupRoleRel.getSys_id());
                dto.setTablename(tableName);
                dto.setDisplayType(dto.getType());
                dto.setFullName(groupRoleRel.getSys_id());
                
                ExportUtils.impexComponentList.add(dto);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private Groups getGroup()
    { 

        if(StringUtils.isNotBlank(groupName))
        {
            String sql = "from Groups where LOWER(UName) = '" + groupName.trim().toLowerCase() + "'";
            try
            {
                return (Groups) HibernateProxy.execute(() -> {
                	Groups groupModel = null;
                	List<Groups> list = HibernateUtil.createQuery(sql).list();
                    if(list != null && list.size() > 0)
                    {
                        groupModel = list.get(0);
                    }
                    
                    if (groupModel != null)
                    {
                        Collection<UserGroupRel> userGroupRelCollection = groupModel.getUserGroupRels();
                        if (userGroupRelCollection != null)
                        {
                            userGroupRelCollection.size();
                        }
                        
                        Collection<GroupRoleRel> groupRoleRelCollection = groupModel.getGroupRoleRels();
                        if (groupRoleRelCollection != null)
                        {
                            groupRoleRelCollection.size();
                        }
                    }
                    
                    return groupModel;
                });
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
                              HibernateUtil.rethrowNestedTransaction(e);
               
            }
        }
        
        return null;       
        
    }
}
