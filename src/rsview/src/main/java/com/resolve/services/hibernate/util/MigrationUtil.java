/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MigrationUtil
{
    
    public static void migrate()
    {
//        migrateWikidocTagRelationship();
//        migrateATTagRelationship();

        //migrate the user info
        migrateUserColumnData();

        //update the {/pre} to {pre} 
        updatePreTagInWikidocContent();
        
        //update the wikidoc title where it is null
        updateWikidocTitle();
        
        //enable Impex Validation for 3.5
        enableImpexValidation();
    }
    
    /**
     * This api is called during the new Installation after all the imports are done. This is last api 
     * to be called before the system is just ready to be used. 
     */
    public static void postInstallationOperations(Map<String, Object> params)
    {
        //clean up the tables and insert the recs again as Roles will be available at this moment
//        reloadControllers();
    }
    
    //Address 1, 2 ==> street, phone ==> home_phone for all the users
    private static void migrateUserColumnData()
    {
        String sql = "select sys_id, UAddress1, UAddress2, UPhone from Users where UAddress1 is NOT NUll OR UAddress2 is NOT NULL or UPhone is NOT NULL";
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(sql, new HashMap<String, Object>());
            if (data != null && data.size() > 0)
            {
                //this will be the array of objects
                for (Object o : data)
                {
                    Object[] record = (Object[]) o;
                    String sysId = (String) record[0];
                    String address1 = (String) record[1];
                    String address2 = (String) record[2];
                    String phone = (String) record[3];
                    
                    String street = StringUtils.isNotBlank(address1) ? address1 : "";
                    street = StringUtils.isNotBlank(address2) ? (street + "," + address2) : street;
                    
                    StringBuilder updateSql = new StringBuilder("update Users set ");
                    updateSql.append("UAddress1 = null, ");
                    updateSql.append("UAddress2 = null, ");
                    updateSql.append("UPhone = null, ");
                    updateSql.append("UStreet = '").append(street).append("', ");
                    updateSql.append("UHomePhone = '").append(phone).append("' ");
                    updateSql.append(" where sys_id = '").append(sysId).append("' ");
                    
                    try
                    {
                        GeneralHibernateUtil.executeHQLUpdate(updateSql.toString(), "system");
                    }
                    catch (Throwable e)
                    {
                        Log.log.error("error while executing the update for User with sql :" + updateSql, e);
                    }
                    

                }//end of for loop
            }//end of if

            //            Log.log.debug("Processed " + count + " Documents");
        }
        catch (Exception e)
        {
            Log.log.error("Error while updating the Users table with sql: " + sql, e);
        }

    }
    
    //update the {/pre} to {pre} 
    private static void updatePreTagInWikidocContent()
    {
        Log.log.debug("*** START UPDATE PRE TAG OF DOC CONTENT   ***");
        long startTime = System.currentTimeMillis();
        
        //get list of sysids
        String whereClause = "UContent like '%{/pre}%'";
        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("WikiDocument", whereClause, "system"));
        if(sysIds != null && sysIds.size() > 0)
        {
            Log.log.debug("Total # of recs : " + sysIds.size());
            int count = 1;
            for(String sysId : sysIds)
            {
                Log.log.debug("Processed " + count++ + " Documents");
                
                //there is no need to use the save api for the doc as its just fixing the wiki content.
                WikiDocument document = null;
                try
                {
                  HibernateProxy.setCurrentUser("system");
                	document = (WikiDocument) HibernateProxy.execute(() -> {                    
                		WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(sysId);
	                    if(doc != null)
	                    {
	                        String content = doc.getUContent();
	                        if(StringUtils.isNotBlank(content))
	                        {
	                            content = content.replace("{/pre}", "{pre}");
	                            doc.setUContent(content);
	                        }
	                    }
	                    return doc;
                    });
                }
                catch (Exception e)
                {                    
                    Log.log.error("Error saving the doc during the migration script. Please ignore this error as this will not affect the migration process. The document that may still have the {/pre} tag is " + document.getUFullname(), e);
                                      HibernateUtil.rethrowNestedTransaction(e);
                }
            }//end of for loop
        
        }
        
        Log.log.debug("*** END OF UPDATE PRE TAG OF DOC CONTENT  *** "  + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }
    
    //update Title of wikidoc if its null
    private static void updateWikidocTitle()
    {
        Log.log.debug("*** START UPDATE TITLE OF DOC  ***");
        long startTime = System.currentTimeMillis();
        
        //update hql, the qry fails if the alias is removed.
        String updateHql = "update WikiDocument as a set a.UTitle = a.UFullname where (a.UTitle is null OR a.UTitle = '')";
        try
        {
//          String whereClause = "SELECT a.sys_id FROM WikiDocument as a where a.UTitle is null OR a.UTitle = ''";
//            List sysIds = ServiceHibernate.executeHQLSelect(whereClause);
            
            ServiceHibernate.executeHQLUpdate(updateHql, "admin");
        }
        catch (Throwable t)
        {
            Log.log.error("Error in updating the title", t);
        }
        
        Log.log.debug("*** END OF UPDATE TITLE OF DOC  *** "  + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
    }
    
    private static void enableImpexValidation()
    {
        String updateHql = "update Properties as p set p.UValue = 'true' where p.UName = 'impex.validation'";
        
        try
        {
            Log.log.debug("Updating impex validation to true");
            ServiceHibernate.executeHQLUpdate(updateHql, "admin");
            Log.log.debug("Updated impex validation to true");
        }
        catch(Exception e)
        {
            Log.log.error("Error while enabling impex validation flag.",e);
        }
    }
    
    /**
     * Delete all the recs from this table and reload the data
     * 
     * RESOLVE_CONTROLLER
     * RESOLVE_APPS
     * RESOLVE_APP_CONTROLLER_REL
     * RESOLVE_APP_ROLES_REL
     * RESOLVE_APP_ORG_REL
     * 
     */
    private static void reloadControllers()
    {
        Log.log.debug("*** START OF RELOADING CONTROLLERS  ***");
        long startTime = System.currentTimeMillis();

        try
        {
            //ServiceHibernate.deleteQuery("ResolveControllers", "1=1", "system");
            ServiceHibernate.deleteAllQuery("ResolveControllers", "sys_id", "id_sys", "system");
            
            //ServiceHibernate.deleteQuery("ResolveApps", "1=1", "system");
            ServiceHibernate.deleteAllQuery("ResolveApps", "sys_id", "id_sys", "system");
            
            //ServiceHibernate.deleteQuery("ResolveAppControllerRel", "1=1", "system");
            ServiceHibernate.deleteAllQuery("ResolveAppControllerRel", "sys_id", "id_sys", "system");
            
            //ServiceHibernate.deleteQuery("ResolveAppRoleRel", "1=1", "system");
            ServiceHibernate.deleteAllQuery("ResolveAppRoleRel", "sys_id", "id_sys", "system");
            
            //ServiceHibernate.deleteQuery("ResolveAppOrganizationRel", "1=1", "system");
            ServiceHibernate.deleteAllQuery("ResolveAppOrganizationRel", "sys_id", "id_sys", "system");
            
            //this will insert all the recs again
            String filename = RSContext.getWebHome() + "WEB-INF/controllers.properties";
            ServiceHibernate.initControllerProperties(filename);
        }
        catch (Exception e)
        {
            Log.log.error("Error while reloading the controllers again", e);
        }
        
        Log.log.debug("*** END OF RELOADING CONTROLLERS  *** "  + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");

        
    }
    
    

}
