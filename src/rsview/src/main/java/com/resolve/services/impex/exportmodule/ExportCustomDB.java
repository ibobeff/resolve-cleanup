/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;

public class ExportCustomDB extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    
    public ExportCustomDB(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ManifestGraphDTO export()
    {
        ManifestGraphDTO graphDTO = new ManifestGraphDTO();
        String tableName = impexGlideVO.getUModule();
        String whereClause = impexGlideVO.getUName();
        String sqlQuery = null;
        
        if (StringUtils.isNotBlank(tableName))
        {
            sqlQuery = "select sys_id from " + tableName;
            if (StringUtils.isNotBlank(whereClause))
            {
                sqlQuery = sqlQuery + " where " + whereClause ;
            }
        }
        List<String> resultList = null;
        try
        {
            if (StringUtils.isNotBlank(sqlQuery))
            {
            	String finalSqlQuery = sqlQuery;
              HibernateProxy.setCurrentUser(userName);
            	resultList = (List<String>) HibernateProxy.execute(() -> {
	                Query query = HibernateUtil.createSQLQuery(finalSqlQuery);
                    return query.list();
            	});
            }
        }
        catch(Exception e)
        {
            Log.log.error("Could not export Custom DB: " + tableName, e);
            ImpexUtil.logImpexMessage("Could not export Custom DB: " + tableName, true);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        graphDTO.setName(tableName);
        graphDTO.setType(ExportUtils.CUSTOM_DB);
        graphDTO.setExported(true);
        
        if (CollectionUtils.isNotEmpty(resultList))
        {
            populateCustomDB(resultList, tableName);
            graphDTO.setIds(resultList);
    }
    
        return graphDTO;
    }
    
    private void populateCustomDB(List<String> resultList, String tableName)
    {
        String name = String.format("%s (Custom DB)", tableName);
        
        for (String sysId : resultList)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            
            dto.setSys_id(sysId);
            dto.setFullName(name);
            dto.setType(DB);
            dto.setDisplayType(dto.getType());
            
            String table = HibernateUtil.table2Class(tableName.toLowerCase().trim());

            if (StringUtils.isNotBlank(table) && table.contains("."))
            {
            	table = HibernateUtil.class2TableTrueCase(table);
            }
            
            if (StringUtils.isBlank(table))
            {
            	table = CustomTableMappingUtil.table2Class(tableName);
            	table = CustomTableMappingUtil.class2TableTrueCase(table);
            }
            
            if (StringUtils.isBlank(table))
            {
            	ImpexUtil.logImpexMessage("WARNING: Cound not find table: '" + tableName + "' to export.", true);
            }
            else
            {
	            dto.setTablename(table.toLowerCase());
	            ExportUtils.impexComponentList.add(dto);
            }
        }
    }
}
