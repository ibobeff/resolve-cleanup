/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaTable;
import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.MetaTableService;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.table.dto.RsMetaTableViewDTO;

public class ExtSaveMetaTableView
{
    private RsMetaTableViewDTO tableView;
    private String username;

    private MetaTable metaTable = null;
    private MetaTableView metaTableView = null;

    public ExtSaveMetaTableView(RsMetaTableViewDTO tableView, String username) throws Exception
    {
        this.tableView = tableView;
        this.username = username;

        if (StringUtils.isEmpty(this.tableView.getMetaTableSysId()))
        {
            throw new RuntimeException("Table view does not have custom table info");
        }
    }
    
    public RsMetaTableViewDTO save() throws Exception
    {
        validateDTO();
        saveInternal();
        return tableView;
    }
    
    private void validateDTO() throws Exception 
    {
        //if the view is a new one and it does not have 'fields', get that from the 'Default' view.
        if (StringUtils.isBlank(this.tableView.getId()))
        {
            RsMetaTableViewDTO defaultView = new MetaTableService().getMetaTableView(this.tableView.getMetaTableName(), "Default", username, RightTypeEnum.view);
            if(defaultView == null)
            {
                throw new Exception("Default view for table " + this.tableView.getMetaTableName() + " does not exist. So cannot create this view as columns are missing.");
            }

            //add the fields if they are not available 
            if(this.tableView.getFields() == null || this.tableView.getFields().size() == 0)
            {
                List<RsUIField> fields = defaultView.getFields();
                
                if(fields != null && fields.size() > 0)
                {
                    List<RsUIField> nonNullFields = new ArrayList<RsUIField>();
                    
                    for (RsUIField rsUIFld : fields)
                    {
                        if (rsUIFld != null && StringUtils.isNotBlank(rsUIFld.getId()))
                        {
                            nonNullFields.add(rsUIFld);
                        }
                    }
                    
                    if (!nonNullFields.isEmpty())
                    {
                        this.tableView.setFields(nonNullFields);
                    }
                }
            }
            
            //update the roles if they are not there.
            if(StringUtils.isBlank(this.tableView.getViewRoles()) 
                            && StringUtils.isBlank(this.tableView.getEditRoles())
                            && StringUtils.isBlank(this.tableView.getAdminRoles()))
            {
                this.tableView.setViewRoles(defaultView.getViewRoles());
                this.tableView.setEditRoles(defaultView.getEditRoles());
                this.tableView.setAdminRoles(defaultView.getAdminRoles());
            }
        }
    }

    private void saveInternal() throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
	            metaTable = HibernateUtil.getDAOFactory().getMetaTableDAO().findById(this.tableView.getMetaTableSysId());
	            if (metaTable == null)
	            {
	                throw new Exception("MetaTable does not exist for view :" + this.tableView.getDisplayName());
	            }
	
	            if (StringUtils.isNotBlank(this.tableView.getId()))
	            {
	                //update
	                metaTableView = HibernateUtil.getDAOFactory().getMetaTableViewDAO().findById(this.tableView.getId());
	
	                //make sure that if the name of the view is 'Default', user is not able to rename it
	                String currentViewName = metaTableView.getUName();
	                if (currentViewName.equalsIgnoreCase("Default"))
	                {
	                    String newViewName = tableView.getName();
	                    if (!newViewName.equalsIgnoreCase(currentViewName))
	                    {
	                        throw new Exception("Default view cannot be renamed.");
	                    }
	                }
	
	            }
	            else
	            {
	                //insert
	                metaTableView = new MetaTableView();
	            }
	
	            if (metaTableView == null)
	            {
	                throw new Exception("MetaTableView does not exist for view :" + this.tableView.getDisplayName());
	            }
	
	            metaTableView.setUName(tableView.getName());
	            metaTableView.setUDisplayName(tableView.getDisplayName());
	            metaTableView.setUType(tableView.getType());
	            metaTableView.setUIsGlobal(tableView.getIsGlobal());
	            metaTableView.setUUser(StringUtils.isNotBlank(tableView.getUser()) ? tableView.getUser() : username);
	            metaTableView.setUMetaFormLink(tableView.getMetaFormLink());
	            metaTableView.setUMetaNewLink(tableView.getMetaNewLink());
	            metaTableView.setUCreateFormId(tableView.getCreateFormId());
	            metaTableView.setUEditFormId(tableView.getEditFormId());
	            metaTableView.setUTarget(tableView.getTarget());
	            metaTableView.setUParams(tableView.getParams());
	
	            //save immediately - to get the sysid
	            HibernateUtil.getDAOFactory().getMetaTableViewDAO().persist(metaTableView);
	
	            //add the table view fields
	            metaTableView.setMetaTableViewFields(createMetaTableViewFields());
	
	            //add the metatable reference
	            metaTableView.setMetaTable(metaTable);
	
	            //access rights
	            MetaAccessRights metaAccessRights = metaTableView.getMetaAccessRights();
	            if (metaAccessRights == null)
	            {
	                metaAccessRights = new MetaAccessRights();
	            }
	            metaAccessRights.setUReadAccess(this.tableView.getViewRoles());
	            metaAccessRights.setUWriteAccess(this.tableView.getEditRoles());
	            metaAccessRights.setUAdminAccess(this.tableView.getAdminRoles());
	            metaAccessRights.setUResourceId(this.metaTableView.getSys_id());
	            metaAccessRights.setUResourceName(this.metaTableView.getUName());
	            metaAccessRights.setUResourceType(MetaTableView.RESOURCE_TYPE);
	
	            //persist it
	            HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(metaAccessRights);
	            metaTableView.setMetaAccessRights(metaAccessRights);
	
	            //get/refresh the view from db
	            tableView = new ExtMetaTableLookup(metaTable.getUName(), metaTableView.getUName(), username, RightTypeEnum.admin).getMetaTableView();
            });

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);            
            throw new Exception(t);
        }
    }

    private Collection<MetaTableViewField> createMetaTableViewFields()
    {
        List<MetaTableViewField> newFields = new ArrayList<MetaTableViewField>();
        if (this.tableView.getFields() != null)
        {
            //first delete the existing ones
            deleteMetaTableViewField(metaTableView.getMetaTableViewFields());

            int order = 1;
            for (RsUIField field : this.tableView.getFields())
            {
                if (field == null || StringUtils.isBlank(field.getId()))
                {
                    throw new RuntimeException(field == null ? "Table view field is null" : "sysid of " + field.getName() + " is empty.");
                }

                MetaField metaField = HibernateUtil.getDAOFactory().getMetaFieldDAO().findById(field.getId());
                if (metaField == null)
                {
                    throw new RuntimeException(field.getName() + " does not exist.");
                }

                //prepare new props and persist it
                MetaFieldProperties tableProperties = ExtSaveMetaFormView.convertINPUTUIToDbModelProperties(field, null, null, false);
                HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().persist(tableProperties);

                //prepare new rec                            
                MetaTableViewField metaTableViewField = new MetaTableViewField();
                metaTableViewField.setMetaTableView(this.metaTableView);
                metaTableViewField.setMetaField(metaField);
                metaTableViewField.setUOrder(new Integer(order));
                metaTableViewField.setMetaFieldTableProperties(tableProperties);

                //persist it
                HibernateUtil.getDAOFactory().getMetaTableViewFieldDAO().persist(metaTableViewField);

                newFields.add(metaTableViewField);
                order++;
            }
        }
        else
        {
            //Add all fields by default by leaving newFields blank
        }

        return newFields;
    }

    private static void deleteMetaTableViewField(Collection<MetaTableViewField> metaTableViewFields)
    {
        if (metaTableViewFields != null && metaTableViewFields.size() > 0)
        {
            try
            {
                //NEED THIS FOR ORACLE as the DELETES are not COMMITTED immediately
              HibernateProxy.setCurrentUser("admin");
                HibernateProxy.execute(() -> {

	                for (MetaTableViewField metaTableViewField : metaTableViewFields)
	                {
	                    if (metaTableViewField.getMetaFieldTableProperties() != null)
	                    {
	                        HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().delete(metaTableViewField.getMetaFieldTableProperties());
	                    }
	                    HibernateUtil.getDAOFactory().getMetaTableViewFieldDAO().delete(metaTableViewField);
	                }
	
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }//deleteMetaFormTabField

}
