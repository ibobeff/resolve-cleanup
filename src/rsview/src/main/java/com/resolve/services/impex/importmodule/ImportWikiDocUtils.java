/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.owasp.esapi.ESAPI;
import org.xml.sax.InputSource;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.hibernate.vo.ResolveImpexWikiVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ImportWikiDocUtils
{
    protected final String SKIP = "1";
    protected final String OVERWRITE = "0";
    protected String moduleName;
    protected String WIKI_FOLDER_LOCATION;
    protected String userName;
    protected File folderLocation;
    protected int dummySysId=1;
    protected boolean isResolveMaintUser;
    protected static List <ImpexComponentDTO> listWikiDocument;
    protected WikiDocument globalDoc;
    protected List<String> excludeList;
    protected int importedDocsCount;
    protected List<String> archiveDocIdList = new ArrayList<String>();
    protected boolean overwrite;
    protected static Map<String, ImpexOptionsDTO> wikiOptionsMap = new HashMap<String, ImpexOptionsDTO>();
    protected Map<String, String> replaceIdsMap = new HashMap<String, String>();
    protected List<String> wikiFullPath = new ArrayList<String>();
    protected String updatedBy;
    
    protected String xmlSysId = null;
    protected String xmlName = null;
    protected String xmlNamespace = null;
    protected String xmlFullName = null;
    protected int retryCount;
    // Map<OrginalSysId, NewSysId> needed to change RRRule table
    protected Map<String, String> wikiReplaceSysIdsMap = new HashMap<String, String>();
    protected List<WikiDocument> persistLast = new ArrayList<WikiDocument>();
    
    public ImportWikiDocUtils(String moduleName, List<String> excludeList, String wikiFolderLocation, String userName)
    {
        this.moduleName = moduleName;
        this.excludeList = excludeList;
        this.WIKI_FOLDER_LOCATION = wikiFolderLocation;
        this.userName = userName;
        isResolveMaintUser = userName.equalsIgnoreCase("resolve.maint") ? true : false;
        
        folderLocation = FileUtils.getFile(WIKI_FOLDER_LOCATION);
    }
    
    @SuppressWarnings("unchecked")
    public List <ImpexComponentDTO> getWikiDocsToImport(boolean fromUI) throws Exception
    {
        if (listWikiDocument != null)
        {
            return listWikiDocument;
        }
        
        listWikiDocument = new ArrayList<ImpexComponentDTO>();
        populateWikiFullPath();
        List<Map<String, String>> docNames = readPackageXMLToGetDocumentNames();
        ResolveImpexModuleVO impexModuleVO = ImpexUtil.getImpexModelVOWithReferences(null, moduleName, userName);
        
        if (CollectionUtils.isEmpty(docNames)) {
        	return listWikiDocument;
        }
        
        try {
        	if (!fromUI) {
        		ImpexUtil.initImpexOperationStage("IMPORT", "(11/51) Prepare List of Wikis to Import", docNames.size());
        	}
        	
	        for (Map<String, String> docNameMap : docNames)
	        {
	        	if (!fromUI) {
	        		ImpexUtil.updateImpexCount("IMPORT");
	        	}
	        	
	            String docName = docNameMap.get("DOCNAME");
	            ImpexOptionsDTO optionsDTO = null;
	            ResolveImpexWikiVO impexWikiVO = null;
	            
	            if (impexModuleVO != null)
	            {
	                Collection<ResolveImpexWikiVO> impexWikiCollection = impexModuleVO.getResolveImpexWikis();
	                if (impexWikiCollection != null)
	                {
	                    Iterator<ResolveImpexWikiVO> i = impexWikiCollection.iterator();
	                    
	                    while (i.hasNext())
	                    {
	                        impexWikiVO = i.next();
	                        
	                        if (impexWikiVO.getUValue().equals(docName))
	                        {
	                            if (impexWikiVO.getUOptions() != null)
	                            {
	                                optionsDTO = new ObjectMapper().readValue(impexWikiVO.getUOptions(), ImpexOptionsDTO.class);
	                                break;
	                            }
	                        }
	                        else
	                        {
	                            impexWikiVO = null;
	                        }
	                    }
	                }
	            }
	            else
	            {
	                /*
	                 * If an export has indirectly exported components, there won't be any glide entry.
	                 * So, create a default ImpexOptionsDTO.
	                 */
	                optionsDTO = new ImpexOptionsDTO();
	            }
	            
	            String UDefaultAction = docNameMap.get("DEFAULT_ACTION");
	            if (optionsDTO != null)
	            {
	                if (optionsDTO.getWikiOverride())
	                {
	                    UDefaultAction = OVERWRITE;
	                }
	                else
	                {
	                    UDefaultAction = SKIP;
	                }
	            }
	            
	            if (optionsDTO == null)
	            {
	                wikiOptionsMap.put(docName, new ImpexOptionsDTO());
	            }
	            else
	            {
	                wikiOptionsMap.put(docName, optionsDTO);
	            }
	            
	            String[] arrFullDocName = docName.split("\\.");
	            
	            String UNamespace = arrFullDocName[0];
	            String UName = arrFullDocName[1];
	            String fileName = WIKI_FOLDER_LOCATION + UNamespace + "/" + UName;
	            String docSysId = null;
	            
	            if (impexWikiVO != null)
	            {
	                if (impexWikiVO.getUType().equalsIgnoreCase("namespace") || // For older modules
	                                impexWikiVO.getUType().equalsIgnoreCase("wikinamespace"))
	                {
	                    fileName = WIKI_FOLDER_LOCATION + impexWikiVO.getUValue() + "/" + docName.split("\\.")[1];
	                }
	                
	                if (StringUtils.isNotBlank(impexWikiVO.getUDescription())) {
	                    // if descript present, it's always going to be a json. So safe to construct a Map.
    	                Map<String, Object> descriptionMap = new ObjectMapper().readValue(impexWikiVO.getUDescription(), Map.class);
    	                if (descriptionMap != null) {
    	                    if (descriptionMap.containsKey("sys_id")) {
    	                        docSysId = (String)descriptionMap.get("sys_id");
    	                    }else {
    	                        docSysId = getWikiSysId(fileName);
    	                    }
    	                } else {
    	                    docSysId = getWikiSysId(fileName);
    	                }
	                } else {
	                    docSysId = getWikiSysId(fileName);
	                }
	            }
	            
	            //if its a regular user and the namespace is to be filtered, then skip that document
	            if(!isResolveMaintUser  && UNamespace.equals("System"))
	            {
	                continue;
	            }
	            
	            ImpexComponentDTO doc = new ImpexComponentDTO();
	            doc.setSys_id(docSysId);
	            doc.setModule(docName); // This is fullName of a document.
	            doc.setName(docName); // This is fullName of a document.
	            doc.setFullName(docName);
	            doc.setType("DOCUMENT");
	            doc.setDisplayType("DOCUMENT");
	            if (StringUtils.isNotBlank(docNameMap.get("IS_TEMPLATE")))
	            {
	            	doc.setType("SECURITY TEMPLATE");
	                doc.setDisplayType("SECURITY TEMPLATE");
	            }
	            doc.setFilename(fileName);
	            doc.setDefaultAction(UDefaultAction);
	            doc.setOperation("0".equals(UDefaultAction)?"OVERWRITE":"SKIP");
	
	            listWikiDocument.add(doc);
	        }
        } catch (Exception e) {
        	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Failed to Prepare List of Wikis to Import.", 
        				  e);
    		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
    								  e.getMessage() : 
    								  "Failed to Prepare List of Wikis to Import. Please check RSView logs for details", true);
        }
        
        return listWikiDocument;
    }
    
    private String getWikiSysId(String fileName) {
        String wikiId = null;
        if (StringUtils.isNotBlank(fileName)) {
            File file = FileUtils.getFile(fileName);
            if (file != null) {
                try {
                    String docString = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
                    wikiId = StringUtils.substringBetween(docString, "<sys_id>", "</sys_id>");
                } catch (IOException e) {
                    Log.log.error("Could not read exported wiki xml.", e);
                }
            }
        }
        return wikiId;
    }

    private void populateWikiFullPath()
    {
        if (folderLocation.exists())
        {
            wikiFullPath.clear();
            WildcardFileFilter filter = new WildcardFileFilter("*");
            Collection<File> fileCollection = FileUtils.listFiles(folderLocation, filter, TrueFileFilter.INSTANCE);
            
            for (File file : fileCollection)
            {
                wikiFullPath.add(file.getPath().replace("\\", "/"));
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> readPackageXMLToGetDocumentNames()  throws Exception
    {
        List<Map<String, String>> documentNames = new ArrayList<Map<String, String>>();
        List<Map<String, String>> templateDocumentNames = new ArrayList<Map<String, String>>();
        Map<String, String> docMap = null;
        
        //make sure that the file exist 
        File packageXMLFile = FileUtils.getFile(folderLocation, "package.xml");
        if (packageXMLFile.exists())
        {
            //throw new Exception("package.xml does not exist in " + folderLocation.getPath() + " folder.");
            
            //read and get the document object
            Document documentPackageXML = null;
            try
            {
                documentPackageXML = ImpexUtil.fromXml(FileUtils.readFileToString(packageXMLFile, ControllerUtil.ENCODING));
                if (documentPackageXML == null)
                {
                    throw new Exception("Cannot read package.xml file");
                }
            }
            catch(Exception e)
            {
                throw new Exception(e.getMessage());
            }
    
            Element docFiles = documentPackageXML.getRootElement();
            Element infosFiles = docFiles.element("files");
    
            List<Element> docNameElements = infosFiles.elements("file");
            Log.log.info("Package declares -->" + docNameElements.size() + " documents.");
            
            for(Element docName : docNameElements)
            {
            	boolean isTemplate = false;
            	if (docName.attributeValue("isTemplate") != null)
            	{
            		isTemplate = docName.attributeValue("isTemplate").equals("true") ? true : false;
            	}
                String docFullName = docName.getStringValue();
                String defaultAction = docName.attributeValue("defaultAction");
                if (defaultAction == null)
                {
                    defaultAction = OVERWRITE;
                }
                
                docMap = new HashMap<String, String>();
                docMap.put("DOCNAME",docFullName);
                docMap.put("DEFAULT_ACTION", defaultAction);
                if (isTemplate)
                {
                	docMap.put("IS_TEMPLATE", "true");
                	templateDocumentNames.add(docMap);
                }
                else
                {
                	documentNames.add(docMap);
                }
            }//end of for loop
        }
        else
        {
            ImpexUtil.logImpexMessage("No Wiki Document(s) to import.", false);
        }
        
        if (templateDocumentNames.size() > 0)
        {
        	for (Map<String, String> map : templateDocumentNames)
        	{
        		documentNames.add(map);
        	}
        }
        
        return documentNames;

    }//readPackageXMLToGetDocumentNames
    
    public void importWikiDocs() throws Exception
    {
        if (!listWikiDocument.isEmpty())
        {
        	ImpexUtil.initImpexOperationStage("IMPORT", "(24/51) Wiki Documents", listWikiDocument.size());
        	
            globalDoc = WikiUtils.getWikiDocumentModel(null, "System.Attachment", userName, false);
            
            for (ImpexComponentDTO compDTO : listWikiDocument)
            {
                ImpexUtil.updateImpexCount("IMPORT");
                
                if (excludeList != null && excludeList.contains(compDTO.getSys_id()))
                {
                    ImpexUtil.logImpexMessage("Excluded " + compDTO.getFullName(), false);
                    //updateImportCount();
                    continue;
                }
                if(!isResolveMaintUser  && compDTO.getModule().equals("System"))
                {
                    importedDocsCount++;
                    //updateImportCount();
                    continue;
                }
                
                try
                {
                    String message = compDTO.getType().equals("SECURITY TEMPLATE") ? "Importing Security Template: " : "Importing Wiki Document: ";
                    ImpexUtil.logImpexMessage(message + compDTO.getFullName(), false);
                    importWikiDoc(compDTO);
                    importedDocsCount++;
                    
                }
                catch(Exception e)
                {
                    Log.log.error("Failed to Import Wiki Document: " + compDTO.getFullName(), e);
                    ImpexUtil.logImpexMessage("Failed to Import Wiki Document: " + compDTO.getFullName(), true);
                    //throw new Exception("Failed to Import Wiki Document: " + compDTO.getFullName());
                }
                
            }
            
            // Only for old imports with social.
            if (replaceIdsMap.size() > 0)
            {
                String socialFolderName = WIKI_FOLDER_LOCATION + "../social/";
                File socialFolder = FileUtils.getFile(socialFolderName);
                if (socialFolder != null && socialFolder.isDirectory())
                {
                    NameFileFilter fileter = new NameFileFilter("edge_edge.xml");
                    Collection<File> files = FileUtils.listFiles(socialFolder, fileter, TrueFileFilter.INSTANCE);
                    if (files != null && files.size() > 0)
                    {
                        Set<String> sourceIdSet = replaceIdsMap.keySet();
                        for (File file : files)
                        {
                            try
                            {
                                String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
                                for (String sourceId : sourceIdSet)
                                {
                                    if (content.contains(sourceId))
                                    {
                                        String destSysId = replaceIdsMap.get(sourceId);
                                        content = content.replace(sourceId, destSysId);
                                        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
                                    }
                                }
                            }
                            catch (IOException e)
                            {
                                Log.log.error(e.getMessage());
                            }
                        }
                    }
                }
            }
            
            if (persistLast.size() > 0)
            {
                for (WikiDocument doc : persistLast )
                {
                    try
                    {
                        persistWikiDocument(doc);
                    }
                    catch (Exception e)
                    {
                        archiveDocIdList.remove(doc.getSys_id());
                        Log.log.error("Could not save Wiki: " + doc.getUFullname() + " during import persist last.", e);
                        ImpexUtil.logImpexMessage("Could not save Wiki: " + doc.getUFullname() + " during import persist last. Message: " + e, true);
                    }
                }
            }
            
            if (!archiveDocIdList.isEmpty())
            {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put(ConstantValues.WIKI_DOCUMENT_SYS_IDS, archiveDocIdList);
                param.put("UPDATED_BY", updatedBy);
                Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MWiki.archiveDocuments", param);
            }
            
            wikiOptionsMap.clear();
            ImpexUtil.logImpexMessage("Completed. In total " + importedDocsCount + " documents processed.", false);
        }
    }
    
//    public void updateImportCount()
//    {
//        ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("IMPORT");
//        if (eventVO != null)
//        {
//            String[] progress = eventVO.getUValue().split(",");
//            if (progress.length == 3)
//            {
//                
//                int c = Integer.parseInt(progress[2]);
//                String value = progress[0] + "," + progress[1] + "," + ++c;
//                eventVO.setUValue(value);
//                ServiceHibernate.insertResolveEvent(eventVO);
//            }
//        }
//    }
    
    public void importWikiDoc(ImpexComponentDTO compDTO) throws Exception
    {
        String fullName = compDTO.getModule();
        String name = fullName.split("\\.")[1];
        String namespace = fullName.split("\\.")[0];
        String defaultAction = compDTO.getDefaultAction();
        ImpexOptionsDTO impexOptions = wikiOptionsMap.get(compDTO.getModule());
        File docfile = FileUtils.getFile(compDTO.getFilename());
        
        if (docfile == null || !docfile.exists())
        {
            /*
             * In older resolve, we allowed creating namespaces 'Test' and 'test'. During import, the extracted wiki folder
             * contains namespace folder and all wiki's under that namespace would go under that folder.
             * On Windows, folder 'Test' and 'test' refers to the same folder, whereas on Unix flavored environments,
             * these are two distinct folders.
             * This small check is to take care of this specific scenario.  
             */
            docfile = null;
            String wikiPath = compDTO.getFilename().replaceAll("\\\\", "/");
            for (String path : wikiFullPath)
            {
                if (wikiPath.equalsIgnoreCase(path))
                {
                    docfile = FileUtils.getFile(path);
                    break;
                }
            }
            
            if (docfile == null)
            {
                ImpexUtil.logImpexMessage("Document file: '" + compDTO.getFilename() + "' does not exist. Please verify." , true);
                return;
            }
        }
        
        // File namespaceFolder = FileUtils.getFile(folderLocation, namespace);
        // docfile = FileUtils.getFile(namespaceFolder, name);
        int count = 0;
        
        while (count < 2)
        {
            InputStream inputStream= new FileInputStream(docfile);
            Reader reader = new InputStreamReader(inputStream,"UTF-8");
             
            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");
            
            SAXReader saxReader = new SAXReader();
            saxReader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            saxReader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            saxReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            Document domdoc = null;
            try
            {
                domdoc = saxReader.read(is);
            }
            catch (DocumentException e)
            {
                if (count == 1)
                {
                    Log.log.error("Error in parsing document file: " + docfile.getAbsolutePath(), e);
                    ImpexUtil.logImpexMessage("Error in parsing document file: " + docfile.getAbsolutePath(), true);
                    break;
                }
                Log.log.warn("Document parsing fased an issue for file: " + docfile.getAbsolutePath() + ". Rectify it and retry...");
                
                ImpexUtil.removeCdataFromXML(docfile);
                
                count++;
                continue;   
            }
            
            Element docel = domdoc.getRootElement();
            xmlSysId = getElement(docel, "sys_id");
            
            /*
             * If exported Wiki is sub component, there's no resolve_impex_wiki
             * record. So no way to figure out wiki sysId unless we read the xml file.
             */
            if (CollectionUtils.isNotEmpty(excludeList) && excludeList.contains(xmlSysId)) {
                ImpexUtil.logImpexMessage("Excluded " + compDTO.getFullName(), false);
                break;
            }
            
            xmlName = getElement(docel, "name");
            xmlNamespace = getElement(docel, "web");
            xmlFullName = xmlNamespace + "." + xmlName;
            
            WikiDocument importDoc = WikiUtils.getWikiDocumentModel(xmlSysId, null, userName, false);
            
            if (importDoc != null)
            {
                if (!importDoc.getUFullname().equalsIgnoreCase(xmlFullName))
                {
                    /*
                     * OK, till this point we know there's a wiki present in the current system whoes sysId maches with the xml SysId.
                     * Let's check whether there's a wiki present whoes names matches with the XML Name.
                     */
                    
                    WikiDocument importDocNameCheck = WikiUtils.getWikiDocumentModel(null, xmlFullName, userName, false);
                    
                    if (importDocNameCheck != null)
                    {
                        /*
                         * Wow, this is interesting. System has 2 wiki's, one has same sysId as XML SysId and other has same name as XML Name
                         */
                        
                        importDoc = importDocNameCheck;
                        
                        if (defaultAction.equals(OVERWRITE))
                        {
                            if (impexOptions.getWikiDeleteIfSameName())
                            {
                                overwrite = true;
                                importDoc.setUImpexSysId(xmlSysId);
                            }
                            else
                            {
                                replaceIdsMap.put(xmlSysId, importDoc.getSys_id());
                                overwrite = false;
                            }
                        }
                        else if (defaultAction.equals(SKIP))
                        {
                            /*
                             * Same Fullname doc already present in the system.
                             * As the default action is set to SKIP (skip import if doc already present), doc import is skipped. 
                             */
                            Log.log.info("Document: " + importDoc.getUFullname() + " already present. As defaultAction is SKIP, skipping the import.");
                            return;
                        }
                    }
                    else
                    {
                        /*
                         * Different Name and Same Sys_Id
                         */
                        importDoc = sameSysIdDiffName(importDoc, impexOptions);
                    }
                }
                else
                {
                    // Same Name and Same Sys_Id
                    if (defaultAction.equals(OVERWRITE))
                    {
                        overwrite = false;
                    }
                    else
                    {
                        Log.log.info("Document: " + importDoc.getUFullname() + " already present. As defaultAction is SKIP, skipping the import.");
                        return;
                    }
                }
            }
            else
            {
                importDoc = WikiUtils.getWikiDocumentModel(null, xmlFullName, userName, false);
                if (importDoc != null)
                {
                    if (defaultAction.equals(OVERWRITE))
                    {
                        if (impexOptions.getWikiDeleteIfSameName())
                        {
                            overwrite = true;
                            importDoc.setUImpexSysId(xmlSysId);
                        }
                        else
                        {
                            replaceIdsMap.put(xmlSysId, importDoc.getSys_id());
                            overwrite = false;
                        }
                    }
                    else if (defaultAction.equals(SKIP))
                    {
                        /*
                         * Same Fullname doc already present in the system.
                         * As the default action is set to SKIP (skip import if doc already present), doc import is skipped. 
                         */
                        Log.log.info("Document: " + importDoc.getUFullname() + " already present. As defaultAction is SKIP, skipping the import.");
                        return;
                    }
                }
                else
                {
                    // This is an insert operation. Need not do anything here.
                }
            }
            
            if (importDoc == null)
            {
                importDoc = new WikiDocument();
                importDoc.setUFullname(namespace + "." + name);
                importDoc.setUNamespace(namespace);
                importDoc.setUName(name);
                
                importDoc.setUIsActive(true);
                importDoc.setUIsDeleted(false);
                importDoc.setUIsHidden(false);
                importDoc.setUIsLocked(false);
                importDoc.setUIsRoot(false);
                importDoc.setUSIRRefCount(0L);
                importDoc.setUVersion(0);
            }
            importDoc.setUImpexSysId(xmlSysId);
            if (StringUtils.isNotBlank(importDoc.getUDisplayMode()) && importDoc.getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK) && 
                importDoc.ugetUIsTemplate())
            {
            	if (importDoc.getUVersion() != null)
            	{
            		importDoc.setUVersion(importDoc.getUVersion() + 1);
            	}
            }
            
            parseDocXML(importDoc, docel);
            
            //save the document as soon as it is read so that no attachments are in the memory
            if (importDoc.getSys_id() != null)
            {
                archiveDocIdList.add(importDoc.getSys_id());
            }
            
            if (StringUtils.isBlank(importDoc.getUDecisionTree()) && StringUtils.isBlank(importDoc.getUTag()))
            {
                persistWikiDocument(importDoc);
            }
            else
            {
                /*
                 * if doc has DT model or a tag associated with it, save it to be persisted later.
                 * This way, while populating DT relationship in ResolveCompRel table, all the sub wiki/runbooks will be availbale.
                 */
                persistLast.add(importDoc);
            }
            
            break;
        }
    }
    
    private WikiDocument sameSysIdDiffName(WikiDocument importDoc, ImpexOptionsDTO impexOptions)
    {
        if (impexOptions.getWikiDeleteIfSameSysId())
        {
            importDoc.setUImpexSysId(xmlSysId);
            importDoc.setUFullname(xmlFullName);
            importDoc.setUNamespace(xmlNamespace);
            importDoc.setUName(xmlName);
            overwrite = true;
        }
        else
        {
            importDoc = new WikiDocument();
            overwrite = false;
            importDoc.setUFullname(xmlFullName);
            importDoc.setUNamespace(xmlNamespace);
            importDoc.setUName(xmlName);
            importDoc.setUIsActive(true);
            importDoc.setUIsDeleted(false);
            importDoc.setUIsHidden(false);
            importDoc.setUIsLocked(false);
            importDoc.setUIsRoot(false);
        }
        
        return importDoc;
    }
    
    @SuppressWarnings("unchecked")
    protected void parseDocXML(WikiDocument doc, Element docel) throws Exception
    {
        doc.setSysCreatedBy(getElement(docel, "author"));
        String content = getElement(docel, "content");
        if (StringUtils.isNotBlank(content))
        {
            content = content.replace("{/pre}", "{pre}");
        }
        doc.setUContent(content);
        doc.setUModelProcess(getElement(docel, "model"));
        doc.setUHasActiveModel(StringUtils.isNotEmpty(doc.getUModelProcess()));
        
        doc.setUModelException(getElement(docel, "abort"));
        doc.setUModelFinal(getElement(docel, "final"));
        doc.setUSummary(getElement(docel, "summary"));
        doc.setUTitle(getElement(docel, "title"));
        try
        {
            doc.setURatingBoost(Double.parseDouble(getElement(docel, "weight")));    
        }
        catch(Exception e)
        {
            doc.setURatingBoost(0.0);
        }
        
        String defaultRole = getElement(docel, "defaultRole"); 
        doc.setUIsDefaultRole(defaultRole.trim().length() > 0 && defaultRole.trim().equals("false") ?  false : true);
        
        AccessRights accessRights = doc.getAccessRights();
        if (accessRights == null)
        {
            accessRights = new AccessRights();
        }
        accessRights.setUReadAccess(getElement(docel, "readRole"));
        accessRights.setUWriteAccess(getElement(docel, "writeRole"));
        accessRights.setUAdminAccess(getElement(docel, "adminRole"));
        accessRights.setUExecuteAccess(getElement(docel, "executeRole"));
        
        doc.setAccessRights(accessRights);
        
//        doc.setUReadRoles(getElement(docel, "readRole"));
//        doc.setUWriteRoles(getElement(docel, "writeRole"));
//        doc.setUAdminRoles(getElement(docel, "adminRole"));
//        doc.setUExecuteRoles(getElement(docel, "executeRole"));
        String decisionTree = getElement(docel, "decisionTree");
        if (StringUtils.isNotBlank(decisionTree))
        {
        	if (decisionTree.startsWith("&lt;"))
        	{
        		// decode only if the DT model is encoded.
        		decisionTree = ESAPI.encoder().decodeForHTML(decisionTree);
        	}
        }
        
        doc.setUDecisionTree(decisionTree);
        doc.setUIsRoot(StringUtils.isNotBlank(doc.getUDecisionTree()));
        
        try
        {
            String createdDateStr = getElement(docel, "creationDate");
            doc.setSysCreatedOn(new Date(Long.parseLong(createdDateStr)));
        }
        catch(Exception t)
        {
            doc.setSysCreatedOn(GMTDate.getDate());
        }
        
        try
        {
            String updatedDateStr = getElement(docel, "contentUpdateDate");
            doc.setSysUpdatedOn(new Date(Long.parseLong(updatedDateStr)));
        }
        catch(Exception t)
        {
            doc.setSysUpdatedOn(GMTDate.getDate());
        }
        
        String tags = getElement(docel, "tags");
        if(StringUtils.isNotBlank(tags))
        {
            tags = tags.replaceAll(";", ",");
            doc.setUTag(tags);
        }
        else
        {
            doc.setUTag(tags);
        }
        
        String catalogs = getElement(docel, "catalogs");
        if(StringUtils.isNotBlank(catalogs))
        {
            catalogs = catalogs.replaceAll(";", ",");
            doc.setUCatalog(catalogs);
        }
        else
        {
            doc.setUCatalog(catalogs);
        }
        
        //set the summary
        doc.setUSummary(getElement(docel, "contentAuthor"));//this is for resolve 2
        if (doc.getUSummary() == null || doc.getUSummary().trim().length() == 0)
        {
            doc.setUSummary(getElement(docel, "summary"));
        }
        
        // Attachments
        List<Element> attachmentElements = docel.elements("attachment");
        List<WikidocAttachmentRel> attachments = new ArrayList<WikidocAttachmentRel>();
        for (Element element : attachmentElements)
        {
            boolean isGlobal = false;
            if(element.element("is_global") != null)
            {
                isGlobal = element.element("is_global").getText().equalsIgnoreCase("true") ? true : false;
            }

            WikiAttachment wikiAttachment = new WikiAttachment();
            wikiAttachment.setUType("DB");

            parseAttachment(doc.getUFullname(), wikiAttachment, element);

            WikidocAttachmentRel rel = new WikidocAttachmentRel();
            rel.setWikiAttachment(wikiAttachment);
            rel.setWikidoc(doc);
            if(isGlobal)
            {
                rel.setParentWikidoc(globalDoc);
            }
            else
            {
                rel.setParentWikidoc(doc);
            }
            
            rel.setSysCreatedBy(wikiAttachment.getSysCreatedBy());
            rel.setSysCreatedOn(wikiAttachment.getSysCreatedOn());

            attachments.add(rel);
        }
        
        doc.setWikidocAttachmentRels(attachments);
        
        doc.setUDisplayMode(getElement(docel, "displayMode"));
        doc.setUCatalogId(getElement(docel, "catalogId"));
        doc.setUWikiParameters(getElement(docel, "wikiParameters"));
        
        String isRequestSubmission = getElement(docel, "isRequestSubmission");
        if (StringUtils.isNotBlank(isRequestSubmission))
        {
            doc.setUIsRequestSubmission(isRequestSubmission.equalsIgnoreCase("true") ? true : false);
        }
        
        String reqestSubmissionOn = getElement(docel, "reqestSubmissionOn");
        if (StringUtils.isNotBlank(reqestSubmissionOn))
        {
            doc.setUReqestSubmissionOn(new Date(Long.parseLong(reqestSubmissionOn)));
        }
        
        String lastReviewedOn = getElement(docel, "lastReviewedOn");
        if (StringUtils.isNotBlank(lastReviewedOn))
        {
            doc.setULastReviewedOn(new Date(Long.parseLong(lastReviewedOn)));
        }
        
        doc.setULastReviewedBy(getElement(docel, "lastReviewedBy"));
        
        String expireOn = getElement(docel, "expireOn");
        if (StringUtils.isNotBlank(expireOn))
        {
            doc.setUExpireOn(new Date(Long.parseLong(expireOn)));
        }
        
        String dtAbortTime = getElement(docel, "dtAbortTime");
        if (StringUtils.isNotBlank(dtAbortTime))
        {
            doc.setUDTAbortTime(new Integer(dtAbortTime));
        }
        else
        {
            doc.setUDTAbortTime(new Integer(0));
        }
        
        updatedBy = getElement(docel, "updatedBy");
        if (StringUtils.isNotBlank(updatedBy))
        {
            doc.setSysUpdatedBy(updatedBy);
        }
        String updatedOn = getElement(docel, "updatedOn");
        if (StringUtils.isNotBlank(updatedOn))
        {
            doc.setSysUpdatedOn(new Date(Long.parseLong(updatedOn)));
        }
        
        String resolutuinBuilderId = getElement(docel, "resolutionBuilderId");
        if (StringUtils.isNotBlank(resolutuinBuilderId))
        {
            doc.setUResolutionBuilderId(resolutuinBuilderId);
            doc.setUHasResolutionBuilder(true);
        }
        
        String isDeleted = getElement(docel, "isDeleted");
        if (StringUtils.isNotBlank(isDeleted))
        {
            doc.setUIsDeleted(new Boolean(isDeleted));
        }
        
        String dtOptions = getElement(docel, "decisionTreeOptions");
        if (StringUtils.isNotBlank(dtOptions))
        {
        	doc.setUDTOptions(dtOptions);
        }
        
        String isTemplate = getElement(docel, "isTemplate");
        if (StringUtils.isNotBlank(isDeleted))
        {
            doc.setUIsTemplate(new Boolean(isTemplate));
        }
        
        String checksum = getElement(docel, "checksum");
        if (StringUtils.isNotBlank(checksum))
        {
            doc.setChecksum(new Integer(checksum));
        }
    }

    private void parseAttachment(String wikiFullName, WikiAttachment wikiAttachment, Element attachElement)
    {
        WikiAttachmentContent wac = new WikiAttachmentContent();
        String wikiNamespace = wikiFullName.split("\\.")[0];
        String wikiFileName = wikiFullName.split("\\.")[1];
        String attachmentFileName = attachElement.element("filename").getText();
        Log.log.debug("Wiki import attachment location (for Resolve 5+): " + WIKI_FOLDER_LOCATION + wikiNamespace + "/" + wikiFileName + "#" + attachmentFileName);
        String fileFullPath = WIKI_FOLDER_LOCATION + wikiNamespace + "/" + wikiFileName + "#" + attachmentFileName;
        File attachment = FileUtils.getFile(fileFullPath);
        
        if (!attachment.isFile())
        {
            /*
             * In older resolve, we allowed creating namespaces 'Test' and 'test'. During import, the extracted wiki folder
             * contains namespace folder and all wiki's under that namespace would go under that folder.
             * On Windows, folder 'Test' and 'test' refers to the same folder, whereas on Unix flavored environments,
             * these are two distinct folders.
             * This small check is to take care of this specific scenario.  
             */
            fileFullPath = fileFullPath.replaceAll("\\\\", "/");
            for (String path : wikiFullPath)
            {
                if (fileFullPath.equalsIgnoreCase(path))
                {
                    attachment = FileUtils.getFile(path);
                    break;
                }
            }
        }
        
        if (attachment.isFile())
        {
            try
            {
                wac.setUContent(FileUtils.readFileToByteArray(attachment));
                wikiAttachment.setUSize(wac.getUContent().length);
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        else
        {
            Element contentel = attachElement.element("content");
            if (contentel != null)
            {
                String base64content = contentel.getText();
                byte[] content = Base64.decodeBase64(base64content.getBytes());
                wac.setUContent(content);
                wikiAttachment.setUSize(wac.getUContent().length);
            }
            else
            {
                ImpexUtil.logImpexMessage("Attachment: " + attachmentFileName + " for Wiki: " + wikiFullName + " not found." , true);
            }
        }
        
        wikiAttachment.setSysCreatedOn(GMTDate.getDate());
        wikiAttachment.setUFilename(attachElement.element("filename").getText());
        wikiAttachment.setSysCreatedBy(attachElement.element("author").getText());
        wikiAttachment.usetWikiAttachmentContent(wac);
    }//parseAttachment
    
    private String getElement(Element docel, String name)
    {
        Element el = docel.element(name);
        if (el == null)
            return "";
        else
            return el.getText().trim();
    }//getElement
    
    private void persistWikiDocument(WikiDocument wikiDoc) throws Exception
    {
        try
        {
            WikiUtils.validateWikiDocName(wikiDoc.getUFullname());
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage());
            ImpexUtil.logImpexMessage("Wiki name: " + wikiDoc.getUFullname() + " is invalid. Please rename it after import.", true);
        }
        
        updatedBy = StringUtils.isNotBlank(wikiDoc.getSysUpdatedBy()) ? wikiDoc.getSysUpdatedBy() : userName;
        Set<String> userRoles = UserUtils.getUserRoles(updatedBy);
        String editRoles = wikiDoc.getAccessRights().getUWriteAccess() + ","+  wikiDoc.getAccessRights().getUAdminAccess();
        boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(updatedBy, userRoles, editRoles);
        if(!userCanEdit)
        {
            updatedBy = userName;
        }
        // Remove any unrecorgnizable chars before importing.
        wikiDoc.setUSummary(wikiDoc.getUSummary().replace("\uFFFD", ""));
        wikiDoc.setUContent(wikiDoc.getUContent().replace("\uFFFD", "")
        // Also fix for RBA-11473: Wiki page looks truncated, when height=300 is enforced
                                                .replace("height=300", "height=-1"));
        
        SaveHelper docSaveHelper = new SaveHelper(wikiDoc, true, updatedBy);
        docSaveHelper.setImportExportOperation(true);
        docSaveHelper.setOverwrite(overwrite);
        try
        {
            WikiDocumentVO docVO = docSaveHelper.save();
            WikiUtils.updateResultMacro(docVO.getUFullname());
            WikiUtils.updateCarriageReturn(docVO.getUFullname());
            if (!xmlSysId.equals(docVO.getSys_id()))
            {
            	/*
            	 * if xml sysId is different than the saved sysId,
            	 * we need to replace it in RRRule table.
            	 */
            	wikiReplaceSysIdsMap.put(xmlSysId, docVO.getSys_id());
            }
        }
        catch(Throwable e)
        {
            archiveDocIdList.remove(wikiDoc.getSys_id());
            
            if (StringUtils.isBlank(e.getMessage()) || 
                !(e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
                  (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
                   e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX)))) {
            	Log.log.error("Could not save Wiki: " + wikiDoc.getUFullname() + " during import.", e);
            }
            
            ImpexUtil.logImpexMessage("Could not save Wiki: " + wikiDoc.getUFullname() + " during import. Message: " + 
            						  e.getMessage(), true);
        }
    }

    public static List<ImpexComponentDTO> getListWikiDocument()
    {
        return listWikiDocument;
    }
    
    public static void clearListOfWikiDocs()
    {
        if (listWikiDocument != null)
        {
            listWikiDocument.clear();
            listWikiDocument = null;
        }
    }

	public Map<String, String> getWikiReplaceSysIdsMap()
	{
		return wikiReplaceSysIdsMap;
	}
}