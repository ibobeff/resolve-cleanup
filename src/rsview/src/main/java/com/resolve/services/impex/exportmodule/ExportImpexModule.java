/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;

public class ExportImpexModule extends ExportComponent
{
    protected ResolveImpexModuleVO impexModuleVO;
    
    public ExportImpexModule(ResolveImpexModuleVO vo)
    {
        this.impexModuleVO = vo;
    }
    
    public void export()
    {
        populateImpexModule();
    }
    
    public void populateImpexModule()
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        dto.setSys_id(impexModuleVO.getSys_id());
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveImpexModule");
        dto.setTablename(tableName);
        dto.setFullName("RESOLVE_IMPEX_MODULE: " + impexModuleVO.getUName());
        dto.setType("DB");
        dto.setDisplayType("DB");
        
        ExportUtils.impexComponentList.add(dto);
    }
}
