/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services;

import java.util.Map;

import com.resolve.services.hibernate.actiontask.ActiontaskCleanupUtil;
import com.resolve.services.hibernate.util.MigrationUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.migration.MigrateActionTask;
import com.resolve.services.migration.gateways.MigrateLegacyGateways;
import com.resolve.services.migration.MigrateNeo4jToSql;

public class ServiceMigration
{
    
    public static void migrateNeo4jToSQL()
    {
        MigrateNeo4jToSql.migrate();
    }
    
    public static void migrateActionTask(int batchLoadSize)
    {
        MigrateActionTask.migrate(batchLoadSize);
    }
    
    public static void migrateLegacyGateways(Map params)
    {
        if(params.size() == 0)
            return;
        
        MigrateLegacyGateways.migrate(params);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Cleanup apis
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * sets the external flag to true in the groups table during the startup of RSVIEW
     */
    public static void updateExternalFlags()
    {
        UserUtils.updateExternalFlags();
    }

    /**
     * syncs DB with the GraphDB
     */
//    public static void syncDBToGraph()
//    {
//        SyncDBToGraph.syncDBToGraph();
//    }

    public static void cleanupActiontaskTable()
    {
        ActiontaskCleanupUtil.cleanup();
    }

    public static void migrate()
    {
        MigrationUtil.migrate();
    }

    public static void postInstalledOperations(Map<String, Object> params)
    {
        MigrationUtil.postInstallationOperations(params);
    }

} // ServiceMigration
