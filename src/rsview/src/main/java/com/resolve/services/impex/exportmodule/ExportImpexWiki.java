/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.hibernate.vo.ResolveImpexWikiVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;

public class ExportImpexWiki extends ExportComponent
{
    protected ResolveImpexModuleVO impexModuleVO;
    
    public ExportImpexWiki(ResolveImpexModuleVO vo)
    {
        this.impexModuleVO = vo;
    }
    
    public void export()
    {
        populateAssessor();
    }
    
    public void populateAssessor()
    {
    	/*
    	 * Adding all WIKI components to the manifest list. That way both, import and export, manifest list
    	 * would show the same number of components.
    	 */
        if (impexModuleVO.getResolveImpexWikis() != null && impexModuleVO.getResolveImpexWikis().size() > 0)
        {
        	List<ResolveImpexWikiVO> wikiList = new ArrayList<ResolveImpexWikiVO>();
            wikiList.addAll(impexModuleVO.getResolveImpexWikis());
            for (ResolveImpexWikiVO vo : wikiList)
            {
	            ImpexComponentDTO dto = new ImpexComponentDTO();
	            dto.setSys_id(vo.getSys_id());
	            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveImpexWiki");
	            dto.setTablename(tableName);
	            dto.setFullName("RESOLVE_IMPEX_WIKI: " + vo.getUValue());
	            dto.setType("DB");
	            dto.setDisplayType("DB");
	            
	            ExportUtils.impexComponentList.add(dto);
            }
        }
    }
}
