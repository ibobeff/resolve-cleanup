/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.dom.DOMDocument;
import org.dom4j.dom.DOMElement;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.services.ServiceCatalog;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.hibernate.customtable.CustomFormUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.WikiAttachmentUtil;
import com.resolve.services.hibernate.util.WikiTemplateUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.ResolveImpexWikiVO;
import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.DecisionTreeHelper;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportWikiDocs extends ExportComponent
{
    protected ResolveImpexWikiVO impexWikiVO;
    protected String wikiFolderLocation;
    protected SortedSet<String> namesOfWikidocExported = new TreeSet<String>();
    protected Map<String, Boolean> WikiNameTemplateMap = new HashMap<String, Boolean>();
    protected Map<String, Boolean> componentNames = new HashMap<String, Boolean>();
    protected ExportSocialComponents exportSocialComps;
    protected Set<String> actionTakNameSet = new HashSet<String>();
    protected Set<String> wikiDocSysId = new HashSet<String>();
    protected String wikiDocFullName;
    protected ManifestGraphDTO parent;
    protected String impexOptionsJson;
    
    protected final static String MSG_WIKI_EXCLUDED = "WARNING: The Wiki/Runbook/DT '%s' is excluded from the export list\n";
    
    public void init(ResolveImpexWikiVO impexWikiVO, String wikiFolderLOcation)
    {
        this.impexWikiVO = impexWikiVO;
        this.wikiFolderLocation = wikiFolderLOcation;
    }
    
    public ManifestGraphDTO populateWikiDocsToExport(ManifestGraphDTO parentGraphDTO) throws Throwable
    {
        String fullName = impexWikiVO.getUValue();
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexWikiVO.getUOptions());
            boolean exportProperties = StringUtils.isTrue(PropertiesUtil.getPropertyString(Constants.IMPEX_EXPORT_PROPERTIES));
            impexOptions.setAtProperties(exportProperties);
            impexOptionsJson = new ObjectMapper().writeValueAsString(impexOptions);
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for Wikidoc: " + fullName);
        }
        boolean overwrite = impexOptions.getWikiOverride();
        //QueryDTO query = new QueryDTO();
        //query.setSelectColumns("sys_id,UName,UNamespace,UFullname,UHasActiveModel");
        //query.setWhereClause("wd.UFullname='" + fullName + "'");
        
        WikiDocument wikiDocLocal = WikiUtils.getWikiDocumentModel(null, fullName, userName, true);
        if (wikiDocLocal == null)
        {
            Log.log.error("Could not find Wiki document: '" + fullName + "' to export.");
            ImpexUtil.logImpexMessage("Could not find Wiki document: '" + fullName + "' to export.", true);
            return null;
        }
        else
        {
            parent = new ManifestGraphDTO();
            setGraphType(wikiDocLocal, parent);
            if (parentGraphDTO != null)
                parentGraphDTO.getChildren().add(parent);
            if (excludeList != null)
            {
                if (excludeList.contains(wikiDocLocal.getSys_id()))
                {
                    ImpexUtil.logImpexMessage(String.format(MSG_WIKI_EXCLUDED, fullName), true);
                    parent.setExported(false);
                    return parent;
                }
            }
        }
        wikiDocLocal.setUCatalog(wikiDocLocal.doGetVO().getUCatalog());
        //WikiDocumentVO wikiDocVO = wikiDocLocal.doGetVO();
        
        //if (namesOfWikidocExported.add(wikiDocLocal.getUFullname()))
        if (!WikiNameTemplateMap.containsKey(wikiDocLocal.getUFullname()))
        {
        	boolean isTemplate = ((StringUtils.isNotBlank(wikiDocLocal.getUDisplayMode()) && 
        	                       wikiDocLocal.getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK)) &&
        	                      wikiDocLocal.ugetUIsTemplate().booleanValue());
        	WikiNameTemplateMap.put(wikiDocLocal.getUFullname(), isTemplate);
            populateWikiDoc(wikiDocLocal, overwrite);
            
            exportSocialComps.populateWikidocId(wikiDocLocal.getSys_id(), impexOptions);
            componentNames.put(wikiDocLocal.getUFullname(), overwrite);
            if (StringUtils.isNotBlank(wikiDocLocal.getUDisplayMode()) && wikiDocLocal.getUDisplayMode().equals("playbook"))
            {
            	// export security template. Check template activities and export their corresponding Wiki/Runbook/DT as well.
            	List<Map<String, String>> activityMapList = ParseUtil.parseListOfActivityMaps(wikiDocLocal.getUContent());
            	if (CollectionUtils.isNotEmpty(activityMapList))
            	{
            		for (Map<String, String> activityMap : activityMapList)
            		{
            			String wikiName = activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY);
            			if (StringUtils.isNotBlank(wikiName))
            			{
            				WikiDocument localWiki = WikiUtils.getWikiDocumentModel(null, wikiName, userName, true);
            				if (localWiki != null)
            				{
            				    ManifestGraphDTO localParent = new ManifestGraphDTO();
            				    setGraphType(localWiki, localParent);
            				    if (excludeList.contains(localWiki.getSys_id())) {
            				        localParent.setExported(false);
            				    } else if (parent.isExported() == false) {
            				        localParent.setExported(false);
            				    }
            					exportReferencedComps(localWiki, overwrite, localParent);
            				}
            				else
            				{
            				    ImpexUtil.logImpexMessage(String.format("WARNING: Wiki document '%s' could not be found on the system." , wikiName), true);
            				}
            			}
            		}
            	}
            }
            else
            {
	            exportReferencedComps(wikiDocLocal, overwrite, parent);
            }
        }
        if (parentGraphDTO != null)
            return parentGraphDTO;
        else
            return parent;
    }
    
    private void setGraphType(WikiDocument wikiDoc, ManifestGraphDTO graphDTO) {
        String displayMode = "wiki";
        if (StringUtils.isNotBlank(wikiDoc.getUDisplayMode())) {
            displayMode = wikiDoc.getUDisplayMode();
        }
        if (StringUtils.isNotBlank(displayMode)) {
            if (displayMode.equals("decisiontree")) {
                graphDTO.setType(ImpexEnum.DECISIONTREE.getValue());
            } else if (displayMode.equals("playbook")) {
                graphDTO.setType(ExportUtils.SECURITY_TEMPLATE);
            } else if (wikiDoc.getUHasActiveModel()) {
                graphDTO.setType(ExportUtils.RUNBOOK);
            } else {
                graphDTO.setType(ExportUtils.WIKI);
            }
            graphDTO.setChecksum(wikiDoc.getChecksum());
            graphDTO.setName(wikiDoc.getUFullname());
            graphDTO.setId(wikiDoc.getSys_id());
        }
    }
    
    private void exportReferencedComps(WikiDocument wikiDocLocal, boolean overwrite, ManifestGraphDTO localParent) throws Throwable
    {
    	if (impexOptions.getWikiRefATs())
        {
            collectReferredActionTasks(wikiDocLocal, localParent);
        }
        
        if (impexOptions.getWikiSubRB())
        {
            collectLinkedSubRunbooks(wikiDocLocal, overwrite, localParent);
            
            // if wikiDocLocal is a DT, export all corresponding Wiki's, Runbooks and AT's
            if (StringUtils.isNotBlank(wikiDocLocal.getUDecisionTree()))
            {
            	exportDecisionTree(wikiDocLocal, overwrite, localParent);
            }
        }
        
        exportReferencedWikiComps(wikiDocLocal, localParent, overwrite);
        
        if (!parent.getName().equals(localParent.getName())) {
            parent.getChildren().add(localParent);
        }
    }
    
 	// recurssively export all DT's and Runbooks. Also export Wiki's and AT's.
    private void exportDecisionTree(WikiDocument wikiDocLocal, boolean overwrite, ManifestGraphDTO parent) throws Throwable
    {
        Set<String> wikiNames = DecisionTreeHelper.getAllDTComponents(wikiDocLocal, "Document");
        wikiNames.addAll(DecisionTreeHelper.getAllDTComponents(wikiDocLocal, "Subprocess"));
        
        if (wikiNames.size() > 0)
        {
            for (String docFullName : wikiNames)
            {
                WikiDocument document = WikiUtils.getWikiDocumentModel(null, docFullName, userName, false);
                if (document != null)
                {
                    ManifestGraphDTO localParent = new ManifestGraphDTO();
                    setGraphType(document, localParent);
                    if (parent.isExported() == false) {
                        localParent.setExported(false);
                    } else if (excludeList.contains(document.getSys_id())) {
                        localParent.setExported(false);
                    }
                    if (localParent.isExported() && !componentNames.containsKey(docFullName))
                    {
                        if (StringUtils.isNotBlank(document.getUDecisionTree()))
                        {
                            exportReferencedComps(document, overwrite, localParent);
                            exportDecisionTree(document, overwrite, localParent);
                        }
                    }
                }
                else
                {
                    Log.log.error(String.format("Document %s referred by %s does not exist on the system.", docFullName, wikiDocLocal.getUFullname()));
                }
            }
        }
    }
    
    private ManifestGraphDTO collectLinkedSubRunbooks(WikiDocument wikiDoc, boolean overwrite, ManifestGraphDTO parent) throws Throwable
    {
    	if (wikiDoc != null)
    	{
    	    componentNames.put(wikiDoc.getUFullname(), overwrite);
	        
	        Set<WikiDocument> wikiDocs = WikiUtils.getWikiDocRefs(wikiDoc.getSys_id());
	        wikiDocs.addAll(WikiUtils.getWikiDocDTRefs(wikiDoc.getSys_id()));
	        
	        if (wikiDocs != null && wikiDocs.size() > 0)
	        {
	        	Log.log.info("Wiki '" + wikiDoc.getUFullname() + "' has following referred Wiki's: " + wikiDocs);
	            for (WikiDocument localWikiDoc : wikiDocs)
	            {
	                ManifestGraphDTO localParent = new ManifestGraphDTO();
                    if (parent.isExported() == false) {
                        localParent.setExported(false);
                    } else if (excludeList.contains(localWikiDoc.getSys_id())) {
                        localParent.setExported(false);
                    }
                    setGraphType(localWikiDoc, localParent);
                    parent.getChildren().add(localParent);
	                if (localParent.isExported() && !componentNames.containsKey(localWikiDoc.getUFullname()))
	                {
	                	componentNames.put(localWikiDoc.getUFullname(), overwrite);
	                	collectReferredActionTasks(localWikiDoc, localParent);
	                    exportReferencedWikiComps(localWikiDoc, localParent, overwrite);
	                    collectLinkedSubRunbooks(localWikiDoc, overwrite, localParent);
	                }
	            }
	        }
    	}
    	return 	parent;
    }
    
    private void exportReferencedWikiComps(WikiDocument wikiDocLocal, ManifestGraphDTO localParent, boolean overwrite) throws Throwable
    {
        boolean exportAB = wikiDocLocal.getUHasResolutionBuilder()==null ? false : wikiDocLocal.getUHasResolutionBuilder();
        if (exportAB)
        {
            // Export Automation Builder tables
            if (localParent.isExported()) {
                ExportAutomationBuilder exportAutomation = new ExportAutomationBuilder(wikiDocLocal.getSys_id());
                exportAutomation.export();
                
                for (String docName : exportAutomation.getWikiDocNames())
                {
                	componentNames.put(docName, overwrite);
                }
            }
            if (impexOptions.getWikiForms())
            {
                String formName = ("ABF_" + wikiDocLocal.getUNamespace().replace(" ", "_").replace("-", "_")
                                + "_" + wikiDocLocal.getUName().replace(" ", "_").replace("-", "_")).toUpperCase();
                if (CustomFormUtil.isFormPresent(null, formName, "admin")) {
                    expportForms(formName, localParent);
                }
            }
        }
        
        if (exportAB || impexOptions.getWikiForms())
        {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.setModelName("WikiDocumentMetaFormRel");
            queryDTO.setWhereClause("wd.UWikiDocName = '" + wikiDocLocal.getUFullname() + "'");
            
            List<? extends Object> list = WikiTemplateUtil.getWikiTemplate(queryDTO, userName);
            
            if (list != null && list.size() > 0)
            {
                WikiDocumentMetaFormRelVO wikiMetaFormRelVO = (WikiDocumentMetaFormRelVO)list.get(0);
                populateWikiMetaFormRel(wikiMetaFormRelVO);
                
                expportForms(wikiMetaFormRelVO.getUFormName(), localParent);
                
                if (wikiMetaFormRelVO.getAccessRights() != null)
                {
                    populateAccessRights(wikiMetaFormRelVO.getAccessRights());
                }
            }
            
            Set<String> formNames = ParseUtil.getListOfReferredForms(wikiDocLocal.getUContent());
            
            for (String formName : formNames)
            {
                expportForms(formName, localParent == null ? parent : localParent);
            }
        }
        
        if (impexOptions.getWikiCatalogs())
        {
            String catalogs = wikiDocLocal.getUCatalog();
            if (StringUtils.isNotBlank(catalogs))
            {
                String[] catalogArr = catalogs.split(",");
                for (String catalog : catalogArr)
                {
                    catalog = catalog.substring(1);
                    catalog = catalog.substring(0, catalog.indexOf("/"));
                    Catalog resolveCatalog = ServiceCatalog.getCatalog(null, catalog, userName);
                    exportSocialComps.exportCatalog(resolveCatalog.getParentSysId(), impexOptions);
                    for (String docName : exportSocialComps.getWikiDocNamesToExport())
                    {
                    	componentNames.put(docName, overwrite);
                    }
                }
            }
            
            String displayMode = wikiDocLocal.getUDisplayMode();
            if (StringUtils.isNotBlank(displayMode))
            {
                if (displayMode.equalsIgnoreCase("catalog"))
                {
                    if (StringUtils.isNotBlank(wikiDocLocal.getUCatalogId()))
                    {
                        exportSocialComps.exportCatalog(wikiDocLocal.getUCatalogId(), impexOptions);
                    }
                }
            }
        }
    }
    
    private void expportForms(String formName, ManifestGraphDTO manifestDTO) throws Throwable 
    {
        ManifestGraphDTO localFormGraphDTO = new ManifestGraphDTO();
        localFormGraphDTO.setName(formName);
        localFormGraphDTO.setType(ImpexEnum.FORM.getValue());
        if (manifestDTO != null && manifestDTO.isExported() == false) {
            localFormGraphDTO.setExported(false);
        }
        ExportMetaFormViewUtil exportMetaFormViewUtil = new ExportMetaFormViewUtil(formName, impexOptions, localFormGraphDTO);
        if (localFormGraphDTO.isExported() == false)
        {
            manifestDTO.getChildren().add(localFormGraphDTO);
            return;
        }
        List<ImpexComponentDTO> formImpexCompDTOList = exportMetaFormViewUtil.exportMetaFormView(true);
        if (formImpexCompDTOList.isEmpty())
        {
            return;
        }
        manifestDTO.getChildren().add(localFormGraphDTO);
        ExportUtils.populateFormChildIds(formImpexCompDTOList, localFormGraphDTO);
        ExportUtils.customTabledefList.addAll(formImpexCompDTOList);
        
        for (String wikiName : ExportCustomTableUtil.getWikiNames())
        {
            WikiDocument wikiDocLocal = WikiUtils.getWikiDocumentModel(null, wikiName, userName, true);
            if (wikiDocLocal != null) {
				ManifestGraphDTO localWikiGraphDTO = new ManifestGraphDTO();
				localWikiGraphDTO.setExported(localFormGraphDTO.isExported());
				setGraphType(wikiDocLocal, localWikiGraphDTO);

				if (localFormGraphDTO.isExported() && !componentNames.containsKey(wikiDocLocal.getUFullname())) {
					// manifestDTO.getChildren().add(localFormGraphDTO);
					if (impexOptions.getWikiSubRB()) {
						collectLinkedSubRunbooks(wikiDocLocal, impexOptions.getWikiOverride(), localFormGraphDTO);
						// if wikiDocLocal is a DT, export all corresponding Wiki's, Runbooks and AT's
						if (StringUtils.isNotBlank(wikiDocLocal.getUDecisionTree())) {
							exportDecisionTree(wikiDocLocal, impexOptions.getWikiOverride(), localWikiGraphDTO);
						}
					}
					exportReferencedWikiComps(wikiDocLocal, localFormGraphDTO, impexOptions.getWikiOverride());
				}
            } else {
                ImpexUtil.logImpexMessage(String.format("WARNING: Wiki document '%s' could not be found on the system." , wikiName), true);
            }
            
        }
        ExportCustomTableUtil.getWikiNames().clear();
        
        // export the actiontask's present as form action
        for (String taskName : ExportCustomTableUtil.getActionTaskNames())
        {
            localFormGraphDTO.getChildren().add(populateActionTask(taskName, localFormGraphDTO));
        }
        ExportCustomTableUtil.getActionTaskNames().clear();
        
        // ExportUtils.customTabledefList.addAll(ExportMetaFormViewUtil.exportForm(wikiMetaFormRelVO.getUFormName(), impexOptions));
    }
    
    private ManifestGraphDTO collectReferredActionTasks(WikiDocument document, ManifestGraphDTO localParent) throws Exception
    { 
        if (parent.getName().equals(document.getUFullname()))
            localParent = parent;
        
        setGraphType(document, localParent);
        if (excludeList.contains(document.getSys_id())) {
            localParent.setExported(false);
        }   
    	Set<String> taskSet = ImpexUtil.getTaskNamesReferedFromWiki(document.getSys_id());
    	taskSet.addAll(DecisionTreeHelper.getAllDTComponents(document, "Task"));
    	if (taskSet.size() > 0)
    	{
    		Log.log.info("Wiki '" + document.getUFullname() + "' has following referred tasks: " + taskSet);
    		
    		for (String taskFullName : taskSet)
    		{
    		    ManifestGraphDTO graphDTO = populateActionTask(taskFullName, localParent);
    		    if (graphDTO != null)
    		    {
		            localParent.getChildren().add(graphDTO);
    		    }
    		}
    	}
    	
    	return localParent;
    }
    
    private void populateWikiMetaFormRel(WikiDocumentMetaFormRelVO wikiMetaFormRelVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.WikiDocumentMetaFormRel");
        dto.setType(WIKI_META_FORM_REL);
        dto.setFullName(wikiMetaFormRelVO.getUWikiDocName());
        dto.setSys_id(wikiMetaFormRelVO.getSys_id());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.customTabledefList.add(dto);
    }
    
    private void populateAccessRights(AccessRightsVO  accessRightsVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.AccessRights");
        dto.setType(ACTIONTASK_ACCESSRIGHTS);
        dto.setFullName(accessRightsVO.getUResourceName());
        dto.setSys_id(accessRightsVO.getSys_id());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    public void populateWikiDocs(boolean direct) throws Exception
    {
    	Set<String> docFullNames = componentNames.keySet();
    	
    	if (direct && CollectionUtils.isNotEmpty(docFullNames)) {
    		ImpexUtil.initImpexOperationStage("EXPORT", "(5/24) Add Related Wikis to Manifest", docFullNames.size());
    	}
    	
        for (String docFullName : docFullNames)
        {
        	if (direct) {
        		ImpexUtil.updateImpexCount("EXPORT");
        	}
        	
        	boolean overwrite = componentNames.get(docFullName);
            WikiDocument document = WikiUtils.getWikiDocumentModel(null, docFullName, userName, false);
            if (document != null) {
                if (!WikiNameTemplateMap.containsKey(document.getUFullname()))
                {
                	boolean isTemplate = document.ugetUIsTemplate().booleanValue();
                	
                	WikiNameTemplateMap.put(document.getUFullname(), isTemplate);
                	populateWikiDoc(document, overwrite);
                }
            } else {
                ImpexUtil.logImpexMessage(String.format("WARNING: Wiki document '%s' could not be found on the system." , docFullName), true);
            }
        }
    }
    
    public void populateActionTaskToExport(boolean direct) throws Exception
    {
    	// actionTakNameSet will be populated only during runbook export.
    	if (!actionTakNameSet.isEmpty())
    	{
    		if (direct) {
    			ImpexUtil.initImpexOperationStage("EXPORT", "(6/24) Add Related Action Tasks to Manifest", 
    											  actionTakNameSet.size());
    		}
    		
	        for (String taskFullName : actionTakNameSet)
	        {
	        	if (direct) {
	        		ImpexUtil.updateImpexCount("EXPORT");
	        	}
	        }
    	}
    }
    
    public ManifestGraphDTO populateActionTask(String taskFullName, ManifestGraphDTO parent)
    {
        ManifestGraphDTO graphDTO = null;
        if (taskFullName.contains("?"))
        {
            taskFullName = StringUtils.substringBefore(taskFullName, "?");
        }
        
        String[] taskFullNameArray = taskFullName.split("#");
        if (taskFullNameArray.length != 2)
        {
            Log.log.error("ActionTask full name is not correct: " + taskFullName);
            return graphDTO;
        }
        String name = taskFullNameArray[0];
        String nameSpace = taskFullNameArray[1];
        ResolveImpexGlideVO impexGlideVO = new ResolveImpexGlideVO();
        impexGlideVO.setUName(name);
        impexGlideVO.setUModule(nameSpace);
        impexGlideVO.setUOptions(impexOptionsJson);
        
        if (parent.isExported() == false) {
            // if parent is exlude, no child needs to be exported.
            graphDTO = new ManifestGraphDTO();
            graphDTO.setName(taskFullName);
            graphDTO.setType(ImpexEnum.TASK.getValue());
            graphDTO.setExported(false);
        } else {
            ExportActionTask atExport = ExportActionTask.getInstance();
            atExport.init(impexGlideVO);
            atExport.setExportSocialComponent(exportSocialComps);
            atExport.setUserName(userName);
            try
            {
                String parentWikiName = parent.getName();
                if (StringUtils.isNotBlank(parentWikiName) & !parentWikiName.contains(".")) {
                    parentWikiName = null;
                }
                graphDTO = atExport.exportActionTask(parentWikiName);
            }
            catch(Exception e)
            {
                Log.log.error("Error while exporting linked ActionTask: '" + taskFullName + "' from wiki document: '" + impexWikiVO.getUValue() + "'", e);
                ImpexUtil.logImpexMessage("Error while exporting linked ActionTask: '" + taskFullName + "'", true);
            }
        }
        
        return graphDTO;
    }
    
    public void populateWikiDoc(WikiDocument wiki, boolean overwrite)
    {
        if (wikiDocSysId.add(wiki.getSys_id()))
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            
            dto.setType("DOCUMENT");
            if (StringUtils.isNotBlank(wiki.getUDisplayMode()) && 
                wiki.getUDisplayMode().equalsIgnoreCase(WikiDocumentVO.PLAYBOOK) &&
            	wiki.ugetUIsTemplate())
            {
            	dto.setType("SECURITY TEMPLATE");
            }
            dto.setSys_id(wiki.getSys_id());
            dto.setFullName(wiki.getUFullname());
            dto.setModule(wiki.getUNamespace());
            dto.setName(wiki.getUName());
            dto.setDisplayType(dto.getType());
            if (overwrite)
            {
            	dto.setOperation("OVERWRITE");
            }
            else
            {
            	dto.setOperation("SKIP");
            }
            ExportUtils.wikiComponentList.add(dto);
        }
    }
    
    public void exportWikiDocuments(Set<ImpexComponentDTO>  wikiCompDTOList) throws Exception
    {
    	ImpexUtil.initImpexOperationStage("EXPORT", "(18/24) Export Wikis", wikiCompDTOList.size());
        for (ImpexComponentDTO wikiCompDTO : wikiCompDTOList)
        {
            ImpexUtil.updateImpexCount("EXPORT");
            if (excludeList != null && excludeList.contains(wikiCompDTO.getSys_id()))
            {
                ImpexUtil.logImpexMessage("Excluding Wiki: " + wikiCompDTO.getFullName(), false);
                continue;
            }
            
            File wikiNamespaceFolder = FileUtils.getFile(wikiFolderLocation, wikiCompDTO.getModule());
            if (!wikiNamespaceFolder.exists())
            {
                if (!wikiNamespaceFolder.mkdirs())
                {
                    throw new Exception("Error creating directory -->" + wikiNamespaceFolder.toString());
                }
            }
            
            try
            {
                String filename = wikiCompDTO.getName();
                wikiDocFullName = wikiCompDTO.getFullName();
                namesOfWikidocExported.add(wikiDocFullName);
                WikiDocument wikiDocLocal = WikiUtils.getWikiDocumentModel(null, wikiCompDTO.getFullName(), userName, true);
                
                File file = FileUtils.getFile(wikiNamespaceFolder, filename);
                toXML(file, wikiDocLocal);
      
                // FileUtils.writeStringToFile(file, xml, ImpexUtil.UTF8);
                String message = wikiCompDTO.getType().equals("SECURITY TEMPLATE") ? "Exported Security Template: " : "Exported Wiki Document: ";
                ImpexUtil.logImpexMessage(message + wikiCompDTO.getFullName(), false);
            }
            catch (Exception e)
            {
                ImpexUtil.logImpexMessage("Error exporting wikidoc: " + wikiCompDTO.getFullName(), true);
                Log.log.error("Error exporting wikidoc: " + wikiCompDTO.getFullName(), e);
            }
        }
    }
    
    private void toXML(File file, WikiDocument wikiDoc) throws Exception
    {
        //create XML Document object
        Document doc = toXMLDocument(wikiDoc);
        toXML(file, doc);
    }//toXML

    private void toXML(File file, Document doc) throws Exception
    {
        OutputFormat outputFormat = new OutputFormat("", true);
        outputFormat.setEncoding(ImpexUtil.UTF8);

        // StringWriter out = new StringWriter();
        OutputStream out = new BufferedOutputStream (new FileOutputStream(file));
        XMLWriter writer = new XMLWriter(out, outputFormat);
        try
        {
            writer.write(doc);
            // return out.toString();
        }
        catch (IOException e)
        {
            throw new Exception(new Exception(e));
        }
    }//toXML

    private Document toXMLDocument(WikiDocument wikiDoc) throws Exception
    {
        Document doc = new DOMDocument();
        Element docel = new DOMElement("xwikidoc");
        doc.setRootElement(docel);

        Element el = new DOMElement("web");
        el.addText(wikiDoc.getUNamespace());
        docel.add(el);

        el = new DOMElement("sys_id");
        el.addText(wikiDoc.getSys_id());
        docel.add(el);

        el = new DOMElement("name");
        el.addText(wikiDoc.getUName());
        docel.add(el);
        
        el = new DOMElement("summary");
        el.addText(wikiDoc.getUSummary() != null ?  wikiDoc.getUSummary() : "");
        docel.add(el);
        
        el = new DOMElement("title");
        el.addText(wikiDoc.getUTitle() != null ?  wikiDoc.getUTitle() : "");
        docel.add(el);

        el = new DOMElement("language");
        el.addText("");
        docel.add(el);

        el = new DOMElement("defaultLanguage");
        el.addText("");
        docel.add(el);

        el = new DOMElement("translation");
        el.addText("");
        docel.add(el);

        el = new DOMElement("parent");
        el.addText("");
        docel.add(el);

        el = new DOMElement("creator");
        el.addText("");
        docel.add(el);

        el = new DOMElement("author");
        el.addText(wikiDoc.getSysCreatedBy() == null ? "" : wikiDoc.getSysCreatedBy());
        docel.add(el);

        el = new DOMElement("contentAuthor");
        el.addText("");
        docel.add(el);

        if (wikiDoc.getSysCreatedOn() != null)
        {
	        el = new DOMElement("creationDate");
	        el.addText("" + wikiDoc.getSysCreatedOn().getTime());
	        docel.add(el);
        }

//        el = new DOMElement("date");
//        el.addText("" + GMTDate.getTime());
//        docel.add(el);

        el = new DOMElement("contentUpdateDate");
        el.addText("" + wikiDoc.getSysUpdatedOn().getTime());
        docel.add(el);

//        el = new DOMElement("version");
//        el.addText("" + wikiDoc.getUVersion());
//        docel.add(el);

        if (impexOptions.getWikiTags())
        {
            el = new DOMElement("tags");
            el.addText(wikiDoc.getUTag() != null ? wikiDoc.getUTag() : "");
            docel.add(el);
        }
//        
//        el = new DOMElement("catalogs");
//        el.addText(wikiDoc.getUCatalog() != null ? wikiDoc.getUCatalog() : "");
//        docel.add(el);

        el = new DOMElement("template");
        el.addText("");
        docel.add(el);

        //List<AttachmentDTO>  attachmentDUP = WikiAttachmentUtil.getAllAttachmentsFor(wikiDoc.getSys_id(), wikiDoc.getUFullname(), this.userName);
        //attachments
        Collection<WikidocAttachmentRel> alist = wikiDoc.getWikidocAttachmentRels();
        if (alist != null && alist.size() > 0)
        {
            for (WikidocAttachmentRel rel : alist)
            {
                if (rel.getWikiAttachment() != null)
                {
                    docel.add(toXML(rel, rel.getWikiAttachment()));
                }
            }
        }

        // Add Model
        el = new DOMElement("model");
        el.addCDATA(wikiDoc.getUModelProcess() != null ? wikiDoc.getUModelProcess() : "");
        docel.add(el);

        // Add Abort/Exception
        el = new DOMElement("abort");
        el.addCDATA(wikiDoc.getUModelException() != null ? wikiDoc.getUModelException() : "");
        docel.add(el);

        // Add Final 
        el = new DOMElement("final");
        el.addCDATA(wikiDoc.getUModelFinal() != null ? wikiDoc.getUModelFinal() : "");
        docel.add(el);

        // Add Content
        el = new DOMElement("content");
        el.addCDATA(wikiDoc.getUContent() != null ? wikiDoc.getUContent() : "");
        docel.add(el);

        // Add Default Role Flag
        el = new DOMElement("defaultRole");
        el.addText(wikiDoc.ugetUIsDefaultRole() != null ? wikiDoc.ugetUIsDefaultRole().toString() : "true");
        docel.add(el);
        
        // Add Read Role
        el = new DOMElement("readRole");
        el.addText(wikiDoc.getUReadRoles() != null ? wikiDoc.getUReadRoles() : "");
        docel.add(el);

        // Add Write Role
        el = new DOMElement("writeRole");
        el.addText(wikiDoc.getUWriteRoles() != null ? wikiDoc.getUWriteRoles() : "");
        docel.add(el);

        // Add Admin Role
        el = new DOMElement("adminRole");
        el.addText(wikiDoc.getUAdminRoles() != null ? wikiDoc.getUAdminRoles() : "");
        docel.add(el);

        // Add Execute Role
        el = new DOMElement("executeRole");
        el.addText(wikiDoc.getUExecuteRoles() != null ? wikiDoc.getUExecuteRoles() : "");
        docel.add(el);

        // Add Decision Tree
        el = new DOMElement("decisionTree");
        el.addCDATA(wikiDoc.getUDecisionTree() != null ? wikiDoc.getUDecisionTree() : "");
        docel.add(el);
        
        //add weight 
        el = new DOMElement("weight");
        el.addText(wikiDoc.getURatingBoost() != null ? wikiDoc.getURatingBoost()+"" : "0.0");
        docel.add(el);
        
        el = new DOMElement("displayMode");
        el.addText(StringUtils.isNotBlank(wikiDoc.getUDisplayMode()) ? wikiDoc.getUDisplayMode() : "");
        docel.add(el);
        
        el = new DOMElement("catalogId");
        el.addText(StringUtils.isNotBlank(wikiDoc.getUCatalogId()) ? wikiDoc.getUCatalogId() : "");
        docel.add(el);
        
        el = new DOMElement("wikiParameters");
        el.addText(StringUtils.isNotBlank(wikiDoc.getUWikiParameters()) ? wikiDoc.getUWikiParameters() : "");
        docel.add(el);
        
        el = new DOMElement("isRequestSubmission");
        el.addText(wikiDoc.getUIsRequestSubmission() != null ? wikiDoc.getUIsRequestSubmission() + "" : "");
        docel.add(el);
        
        el = new DOMElement("reqestSubmissionOn");
        el.addText(wikiDoc.getUReqestSubmissionOn() != null ? wikiDoc.getUReqestSubmissionOn().getTime() + "" : "");
        docel.add(el);
        
        el = new DOMElement("lastReviewedOn");
        el.addText(wikiDoc.getULastReviewedOn() != null ? wikiDoc.getULastReviewedOn().getTime() + "" : "");
        docel.add(el);
        
        el = new DOMElement("lastReviewedBy");
        el.addText(StringUtils.isNotBlank(wikiDoc.getULastReviewedBy()) ? wikiDoc.getULastReviewedBy() : "");
        docel.add(el);
        
        el = new DOMElement("expireOn");
        el.addText(wikiDoc.getUExpireOn() != null ? wikiDoc.getUExpireOn().getTime() + "" : "");
        docel.add(el);
        
        el = new DOMElement("dtAbortTime");
        el.addText(wikiDoc.getUDTAbortTime() != null ? wikiDoc.getUDTAbortTime().toString() : "");
        docel.add(el);
        
        el = new DOMElement("updatedBy");
        el.addText(wikiDoc.getSysUpdatedBy() != null ? wikiDoc.getSysUpdatedBy() : "");
        docel.add(el);
        
        el = new DOMElement("updatedOn");
        el.addText(wikiDoc.getSysUpdatedOn() != null ? wikiDoc.getSysUpdatedOn().getTime() + "" : "");
        docel.add(el);
        
        el = new DOMElement("resolutionBuilderId");
        el.addText(wikiDoc.getUResolutionBuilderId() != null ? wikiDoc.getUResolutionBuilderId() : "");
        docel.add(el);
        
        el = new DOMElement("isDeleted");
        el.addText(wikiDoc.getUIsDeleted() != null ? wikiDoc.getUIsDeleted() == true ? "true" : "false" : "");
        docel.add(el);

        // Add Content
        el = new DOMElement("decisionTreeOptions");
        el.addCDATA(wikiDoc.getUDTOptions() != null ? wikiDoc.getUDTOptions() : "");
        docel.add(el);
        
        el = new DOMElement("isTemplate");
        el.addText(wikiDoc.getUIsTemplate() != null ? wikiDoc.getUIsTemplate().booleanValue() ? "true" : "false" : "");
        docel.add(el);
        
        el = new DOMElement("checksum");
        el.addText(wikiDoc.getChecksum() != null ? wikiDoc.getChecksum().toString() : "");
        docel.add(el);
        
        return doc;
    }//toXMLDocument
    
    private Element toXML(WikidocAttachmentRel rel, WikiAttachment att) throws Exception
    {
        WikiAttachmentUtil.populateAttachmentContent(att);
        
        Element docel = new DOMElement("attachment");
        Element el = new DOMElement("filename");
        el.addText(att.getUFilename());
        docel.add(el);

        el = new DOMElement("filesize");
        el.addText("" + att.getUSize());
        docel.add(el);

        el = new DOMElement("author");
        el.addText(att.getSysCreatedBy());
        docel.add(el);

        el = new DOMElement("date");
        el.addText("" + GMTDate.getTime());
        docel.add(el);

        el = new DOMElement("version");
        el.addText("");
        docel.add(el);

        el = new DOMElement("comment");
        el.addText("");
        docel.add(el);
        
        //flag to indicate if this a global attachment or local
        boolean isGlobal = rel.getParentWikidoc().getSys_id().equalsIgnoreCase(rel.getWikidoc().getSys_id()) ? false : true;
        el = new DOMElement("is_global");
        el.addText(""+ isGlobal);
        docel.add(el);

        //el = new DOMElement("content");
        if (att.ugetWikiAttachmentContent() != null)
        {
            WikiAttachmentContent wac = att.ugetWikiAttachmentContent();
            String wikiDocNameArray[] = wikiDocFullName.split("\\.");
            String wikiDocNamespace = wikiDocNameArray[0];
            
            File wikiNamespaceFolder = FileUtils.getFile(wikiFolderLocation + "/" + wikiDocNamespace, wikiDocNameArray[1] + "#" + att.getUFilename());
            try
            {
                FileUtils.writeByteArrayToFile(wikiNamespaceFolder, wac.getUContent());
            }
            catch (IOException e)
            {
                Log.log.error("Could not read content of attachment for wikiDoc: " + impexWikiVO.getUValue(), e);
            }
        }

        return docel;
    }
    
    public void createPackageXML(File dir, String moduleName)
    {
        FileOutputStream fos = null;
        try
        {
            String filename = "package.xml";
            File file = FileUtils.getFile(dir, filename);
            fos = new FileOutputStream(file);
            fos.write(toXml(moduleName).getBytes());
            fos.flush();
        }
        catch (Exception e)
        {
            Log.log.error("Error while creating package.xml file.", e);
            ImpexUtil.logImpexMessage("Error exporting Package.xml to: " + dir.getName() + ", Message: " + e.getMessage(), true);
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    fos.close();
                }
                catch (Exception e)
                {
                }
            }
        }
    }
    
    private String toXml(String moduleName)
    {
        OutputFormat outputFormat = new OutputFormat("", true);
        outputFormat.setEncoding(ImpexUtil.UTF8);
        StringWriter out = new StringWriter();
        XMLWriter writer = new XMLWriter(out, outputFormat);
        try
        {
            writer.write(toXmlDocument(moduleName));
            return out.toString();
        }
        catch (IOException e)
        {
            ImpexUtil.logImpexMessage(e.getMessage(), true);
            return "";
        }
    }
    
    private Document toXmlDocument(String moduleName)
    {
        Document doc = new DOMDocument();
        Element docel = new DOMElement("package");
        doc.setRootElement(docel);
        Element elInfos = new DOMElement("infos");
        docel.add(elInfos);

        Element el = new DOMElement("name");
        el.addText(moduleName);
        elInfos.add(el);

        el = new DOMElement("module");
        el.addText(moduleName);
        elInfos.add(el);

        el = new DOMElement("description");
        el.addText("wiki module exported on " + GMTDate.getDate());
        elInfos.add(el);

        el = new DOMElement("licence");
        el.addText("proprietry");
        elInfos.add(el);

        el = new DOMElement("author");
        el.addText("Resolve");
        elInfos.add(el);

        el = new DOMElement("version");
        el.addText("3.x");
        elInfos.add(el);

        el = new DOMElement("backupPack");
        el.addText(new Boolean(false).toString());
        elInfos.add(el);

        Element elfiles = new DOMElement("files");
        docel.add(elfiles);

        
        for (String wikiDocName : namesOfWikidocExported)
        {
            Element elfile = new DOMElement("file");
            elfile.addAttribute("defaultAction", componentNames.get(wikiDocName) ? "0" : "1");
            elfile.addAttribute("language", "");
            elfile.addText(wikiDocName);
            elfile.addAttribute("isTemplate", WikiNameTemplateMap.get(wikiDocName)+"");
            elfiles.add(elfile);
        }
        return doc;
    }
    
    public boolean isWikiDocsExported()
    {
        boolean flag = false;
        
        if (namesOfWikidocExported.size() > 0 )
        {
            flag = true;
        }
        
        return flag;
    }
    
    public void setExportSocialComp(ExportSocialComponents exportSocialComps)
    {
        this.exportSocialComps = exportSocialComps;
    }
    
    public void clearCollections()
    {
        namesOfWikidocExported.clear();
        componentNames.clear();
        actionTakNameSet.clear();
        wikiDocSysId.clear();
    }
    
    public ManifestGraphDTO getParentManifestGraph()
    {
        return parent;
    }
}