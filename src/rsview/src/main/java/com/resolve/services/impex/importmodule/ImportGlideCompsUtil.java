/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Element;
import org.hibernate.query.Query;

import com.resolve.rsbase.MainBase;
import com.resolve.service.LicenseService;
import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.ExportUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceMigration;
import com.resolve.services.ServiceTag;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.menu.MenuCache;
import com.resolve.services.hibernate.util.ActionTaskPropertiesUtil;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.GatewayUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.SocialAdminUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.social.impex.SocialImpexUtil;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ImportGlideCompsUtil
{
    private static final String OPERATION = "operation";//DELETE OR INSERTORUPDATE , used for Import
    
    protected String moduleName;
    protected String userName;
    protected String MODULE_FOLDER_LOCATION;
    protected String GLIDE_INSTALL_FOLDER_LOCATION;
    protected File glideFolder;
    protected Set<String> actionTaskNames = new HashSet<>();
    protected Set<String> userNames = new HashSet<>();
    protected static List<ImpexComponentDTO> glideComponents;
    protected List<ImpexComponentDTO> tableDefComponents;
    protected boolean isResolveMaintUser = false;
    protected boolean isCustomTablePresent;
    protected boolean isMenuPresent;
    protected int processedImportCount;
    protected boolean validateImpex;
    protected List<String> excludeList;
    
    protected Set<String> processNames = new HashSet<String>();
    protected Set<String> forumNames = new HashSet<String>();
    protected Set<String> teamNames = new HashSet<String>();
    protected Set<String> rssNames = new HashSet<String>();
    protected Set<String> taskProperties = new HashSet<>();
    protected Set<String> customForms = new HashSet<>();
    
    protected Set<String> cronJobIds = new HashSet<String>();
    protected List<Map<String, String>> filterMaps;
    protected Map<String, Set<String>> taskTagRel;
    
    public ImportGlideCompsUtil(String moduleName, List<String> excludeList, String moduleFolder, String userName)
    {
        this.moduleName = moduleName;
        this.excludeList = excludeList;
        this.MODULE_FOLDER_LOCATION = moduleFolder;
        this.GLIDE_INSTALL_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + "update/install/";
        this.userName = userName;
        
        this.glideFolder = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        this.isResolveMaintUser = userName.equalsIgnoreCase("resolve.maint") ? true : false;
    }
    
    public List<ImpexComponentDTO> getGlideCompsToImport(boolean fromUI) throws Exception
    {
        List<ImpexComponentDTO> localList = new ArrayList<ImpexComponentDTO>();
        if (!glideFolder.isDirectory())
        {
            ImpexUtil.logImpexMessage(GLIDE_INSTALL_FOLDER_LOCATION + " does not exist.", true);
            throw new Exception(GLIDE_INSTALL_FOLDER_LOCATION + " does not exist.");
        }
        
        glideComponents = new ArrayList<ImpexComponentDTO>();
        
        Set<File> files = getAllGlideXmlFiles();
        
        glideComponents.addAll(populateGlideList(files, "(12/51) Prepare List of ", fromUI));
        
        glideComponents.addAll(populateTagCompList(fromUI));
        
        glideComponents.addAll(populateCatalogCompList(fromUI));
        
        localList.addAll(glideComponents);
        // Check for tabledef folder and populate glide list
        File custoTableDefFolder = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION + "tabledef/");
        if (custoTableDefFolder.isDirectory())
        {
            files = getAllCustomDefFiles();
            tableDefComponents = populateGlideList(files, "(16/51) Prepare List of Custom Table ", fromUI);
            localList.addAll(tableDefComponents);
        }
        
        Log.log.info("getGlideCompsToImport Done. " + glideComponents.size() + " components to import" );
        return localList;
    }
    
    private List<ImpexComponentDTO> populateGlideList (Set<File> files, String stagePrefix, boolean fromUI)
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        
        if (CollectionUtils.isNotEmpty(files)) {
        	try {
        		if (!fromUI) {
        			ImpexUtil.initImpexOperationStage("IMPORT", String.format("%s Glide Components to Import", stagePrefix), 
        										   	  files.size());
        		}
        		
		        for (File file : files)
		        {
		        	if (!fromUI) {
		        		ImpexUtil.updateImpexCount("IMPEX");
		        	}
		        	
		            ImpexComponentDTO componentDTO = null;
		            try
		            {
		                componentDTO = convertFiletToDto(file);
		                if (componentDTO != null)
		                {
		                    list.add(componentDTO);
		                }
		            }
		            catch(Exception e)
		            {
		                ImpexUtil.logImpexMessage("Skipping file: " + file.getName(), true);
		                Log.log.error("Skipping file: " + file.getName(), e);
		            }
		        }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
        					  e.getMessage() : String.format("Failed to %sGlide Components to Import.", 
        							  						 StringUtils.substringAfter(stagePrefix, ") ")), e);
        		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
  								  		  e.getMessage() : 
  								  		  String.format("Failed to %sGlide Components to Import. Please check RSView " +
  								  				  	    "logs for details",
  								  				  	    StringUtils.substringAfter(stagePrefix, ") ")),
  								  		  true);
        	}
        }
        
        return list;
    }
    
    @SuppressWarnings("unchecked")
    private List<ImpexComponentDTO> populateTagCompList(boolean fromUI)
    {
        List<ImpexComponentDTO> result = new ArrayList<ImpexComponentDTO>();
        
        List<File> tagFiles = new ArrayList<File>();
        File socialFolder = FileUtils.getFile(MODULE_FOLDER_LOCATION + "social/install/");
        
        if (socialFolder.isDirectory())
        {
            tagFiles.addAll(FileUtils.listFiles(socialFolder, new WildcardFileFilter("resolvetag_*.xml"), null));
            
            try {
            	if (CollectionUtils.isNotEmpty(tagFiles)) {
            		if (!fromUI) {
            			ImpexUtil.initImpexOperationStage("IMPORT", "(13/51) Prepare List of Glide Component DTOs to Import", 
            										   	  tagFiles.size());
            		}
            		
		            for (File tagFile : tagFiles)
		            {
		            	if (!fromUI) {
		            		ImpexUtil.updateImpexCount("IMPORT");
		            	}
		            	
		                try
		                {
		                    String xml = FileUtils.readFileToString(tagFile, ImpexUtil.UTF8);
		                    XDoc doc = new XDoc(xml);
		                    
		                    HashMap<String, String> row = new HashMap<String, String>();
		                    List<Element> columns = doc.getNodes("/graph_node/resolvetag/*");
		                    for(Element e : columns)
		                    {
		                        String name = e.getName();
		                        String value = e.getText();
		                        row.put(name, value);
		                    }//end of for loop
		                    
		                    ImpexComponentDTO dto = new ImpexComponentDTO();
		                    dto.setName(row.get("displayName"));
		                    dto.setFullName(row.get("displayName"));
		                    dto.setSys_id(row.get("sys_id"));
		                    dto.setDisplayType("TAG");
		                    dto.setType("TAG");
		                    dto.setOperation("INSERT_OR_UPDATE");
		                    result.add(dto);
		                }
		                catch(Exception e)
		                {
		                    Log.log.error("Error processing " + tagFile + " file.", e);
		                }
		            }
            	}
            } catch (Exception e) {
            	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
            				  e.getMessage() : "Failed to Prepare List of Tags to Import.", e);
            	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
							  		  	  e.getMessage() : 
							  		  	  "Failed to Prepare List of Tags to Import. Please check RSView logs for details", 
							  		  	  true);
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private List<ImpexComponentDTO> populateCatalogCompList(boolean fromUI)
    {
        List<ImpexComponentDTO> result = new ArrayList<ImpexComponentDTO>();
        
        List<File> catalogFiles = new ArrayList<File>();
        File socialFolder = FileUtils.getFile(MODULE_FOLDER_LOCATION + "social/");
        
        if (socialFolder.isDirectory())
        {
            catalogFiles.addAll(FileUtils.listFiles(socialFolder, new WildcardFileFilter("catalog-*.json"), null));
            
            if (CollectionUtils.isNotEmpty(catalogFiles)) {
            	try {
            		if (!fromUI) {
            			ImpexUtil.initImpexOperationStage("IMPORT", "(14/51) Prepare List of Catalog Components to Import", 
            										   	  catalogFiles.size());
            		}
            		
		            for (File catalogFile : catalogFiles)
		            {
		            	if (!fromUI) {
		            		ImpexUtil.updateImpexCount("IMPORT");
		            	}
		            	
		                Catalog resolveCatalog = null;;
		                try
		                {
		                    resolveCatalog = new ObjectMapper().readValue(
		                    										FileUtils.readFileToString(catalogFile, 
		                    																   StandardCharsets.UTF_8.name()), 
		                    										Catalog.class);
		                    if (resolveCatalog != null)
		                    {
		                        ImpexComponentDTO dto = new ImpexComponentDTO();
		                        dto.setName(resolveCatalog.getName());
		                        dto.setFullName(resolveCatalog.getName());
		                        dto.setSys_id(resolveCatalog.getId());
		                        dto.setDisplayType("CATALOG");
		                        dto.setType("CATALOG");
		                        dto.setOperation("INSERT_OR_UPDATE");
		                        result.add(dto);
		                    }
		                }
		                catch(Exception e)
		                {
		                    Log.log.error("Error processing Catalog: " + catalogFile, e);
		                }
		            }
            	} catch (Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
      					  		  e.getMessage() : "Failed to Prepare List of Catalog Components to Import.", e);
            		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
								  		  	  e.getMessage() : 
								  		  	  "Failed to Prepare List of Catalog Components to Import. Please check RSView " +
								  		  	  "logs for details", 
								  		  	  true);
            	}
            }
        }
        
        catalogFiles.clear();
        /*
         * For backward compatibility
         */
        File socialInstallFolder = FileUtils.getFile(MODULE_FOLDER_LOCATION + "social/install/");
        
        if (socialInstallFolder.isDirectory())
        {
            catalogFiles.addAll(FileUtils.listFiles(socialInstallFolder, new WildcardFileFilter("resolvecatalog_*.xml"), null));
            
            if (CollectionUtils.isNotEmpty(catalogFiles)) {
            	try {
            		if (!fromUI) {
            			ImpexUtil.initImpexOperationStage("IMPORT", "(15/51) Prepare List of Catalog Components to Import", 
							   						   	  catalogFiles.size());
            		}
            		
		            for (File catalogFile : catalogFiles)
		            {
		            	if (!fromUI) {
		            		ImpexUtil.updateImpexCount("IMPORT");
		            	}
		            	
		                try
		                {
		                    String xml = FileUtils.readFileToString(catalogFile, ImpexUtil.UTF8);
		                    XDoc doc = new XDoc(xml);
		                    
		                    HashMap<String, String> row = new HashMap<String, String>();
		                    List<Element> columns = doc.getNodes("/graph_node/resolvecatalog/*");
		                    for(Element e : columns)
		                    {
		                        String name = e.getName();
		                        String value = e.getText();
		                        row.put(name, value);
		                    }//end of for loop
		                    
		                    ImpexComponentDTO dto = new ImpexComponentDTO();
		                    dto.setName(row.get("displayName"));
		                    dto.setFullName(row.get("displayName"));
		                    dto.setSys_id(row.get("sys_id"));
		                    dto.setDisplayType("CATALOG");
		                    dto.setType("CATALOG");
		                    dto.setOperation("INSERT_OR_UPDATE");
		                    result.add(dto);
		                }
		                catch(Exception e)
		                {
		                    Log.log.error("Error processing Catalog: " + catalogFile, e);
		                }
		            }
            	} catch (Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
					  		  	  e.getMessage() : "Failed to Prepare List of Legacy Catalog Components to Import.", e);
            		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
							  		  	  	  e.getMessage() : 
							  		  	  	  "Failed to Prepare List of Legacy Catalog Components to Import. Please check " +
							  		  	  	  "RSView logs for details", 
							  		  	  	  true);
            	}
            }
        }
        
        return result;
    }
    
    protected Set<File> getAllGlideXmlFiles()
    {
        Set<File> files = new LinkedHashSet<File>();
        List<File> localFiles = new ArrayList<File>();
        localFiles.addAll(FileUtils.listFiles(glideFolder, new WildcardFileFilter("*.xml"), null));
        
        List<String> modelHierarchyList = ImportValidationRules.getModelHirarchyList();
        for (String model : modelHierarchyList)
        {
            files.addAll(FileUtils.listFiles(glideFolder, new WildcardFileFilter(model + "*.xml"), null));
        }
        
        for (File file : files)
        {
            if (localFiles.contains(file))
            {
                // remove the files which are already read using hierarchy list.
                localFiles.remove(file);
            }
        }
        
        // add all the remaining files at the end of the list.
        files.addAll(localFiles);
        
        return files;
    }
    
    protected Set<File> getAllCustomDefFiles()
    {
        Set<File> files = new HashSet<File>();
        
        List<String> ctModelHierarchyList = ImportValidationRules.getCtModelHirarchyList();
        for (String model : ctModelHierarchyList)
        {
            files.addAll(FileUtils.listFiles(FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION + "tabledef/")
                        , new WildcardFileFilter(model + "*.xml"), null));
        }
        
        return files;
    }
    
    protected ImpexComponentDTO convertFiletToDto(File file) throws Exception
    {
        String xml = FileUtils.readFileToString(file, ImpexUtil.UTF8);
        
        xml = ImpexUtil.clearCData(xml);
        
        XDoc doc = new XDoc(xml);
        doc.removeResolveNamespaces();
        
        //XDoc doc = ImpexUtil.convertFileToXDoc(file);
        String table = doc.getStringValue("/record_update/@table");
        String type = ImpexUtil.MAP_TABLENAME_TYPE.get(table);
        
        ImpexComponentDTO componentDTO = new ImpexComponentDTO();
        Map<String, String> values = prepareRowData(doc);
        
        if (type != null)
        {
			switch (type.toLowerCase())
            {
                case "properties": 
                {
                componentDTO.setFullName(values.get("u_namespace") + "." + values.get("u_name"));
                componentDTO.setModule(values.get("u_namespace"));
                    componentDTO.setName(values.get("u_name"));
                    break;
            }
                case "wikilookup": {
                componentDTO.setFullName(values.get("u_module") + "(" + values.get("u_regex") + ")");
                componentDTO.setModule(values.get("u_module"));
                    componentDTO.setName(values.get("u_regex"));
                    break;
            }
                case "tag": {
                componentDTO.setFullName(values.get("u_name"));
                componentDTO.setName(values.get("u_name"));
                String desc = values.get("u_description");
                if (StringUtils.isNotBlank(desc))
                {
                    componentDTO.setModule(desc);
                }
                    break;
            }
                case "actiontask": {
                String namespace = values.get("u_namespace");
                String fullName = values.get("u_name") + "#" + namespace;
                if(!isResolveMaintUser && namespace.equals("resolve"))
                {
                    ImpexUtil.logImpexMessage("WARNING: Cannot IMPORT the ActionTask '" + fullName + "' " +
                    		"as this user has no rights. If you are importing the #resolve namespace, please use the resolve.maint user.", true);
                    doc = null; componentDTO = null; values = null;
                    return null;
                }
                componentDTO.setFullName(fullName);
                componentDTO.setModule(values.get("u_namespace"));
                componentDTO.setName(values.get("u_name"));
                    break;
                }            
                case "actioninvoc" : {
                String desc = values.get("u_description");
                if (StringUtils.isNotBlank(desc) && desc.contains("#"))
                {
                    String namespace = desc.substring(desc.indexOf('#')+1);
                    if (!isResolveMaintUser && namespace.equals("resolve"))
                    {
                        ImpexUtil.logImpexMessage("WARNING: Cannot IMPORT the Action Invoc '" + desc + "' " +
                                        "as this user has no rights. If you are importing the #resolve namespace, please use the resolve.maint user.", true);
                        doc = null; componentDTO = null; values = null;
                        return null;
                    }
                }
                componentDTO.setFullName(values.get("u_description"));
                componentDTO.setModule("");
                    componentDTO.setName(values.get("u_description"));
                    break;
            }
                case "preprocessor":
                case "parser":
                case "assessor": {
                String fullName = values.get("u_name");
                String namespace = fullName.substring(fullName.indexOf('#')+1);
                if(!isResolveMaintUser && namespace.equals("resolve"))
                {
                    ImpexUtil.logImpexMessage("WARNING: Cannot IMPORT " + type +" '" + fullName + "' " +
                            "as this user has no rights. If you are importing the #resolve namespace, please use the resolve.maint user.", true);
                    doc = null; componentDTO = null; values = null;
                    return null;
                }
                componentDTO.setFullName(fullName);
                componentDTO.setModule("");
                componentDTO.setName(values.get("u_name"));
                    break;
            }
                case "expression": {
            	componentDTO.setFullName("ActionTask Expression");
                	break;
            }
                case "output_mapping": {
            	componentDTO.setFullName("ActionTask Output Mapping");
                	break;
            }
                case "options": {
                String name = values.get("u_name");
                String description = values.get("u_description");
                
                if(name.equals("INPUTFILE"))
                {
                    if(StringUtils.isNotBlank(description) && description.indexOf('#') > -1)
                    {
                        String namespace = description.substring(description.indexOf('#')+1);
                        if(!isResolveMaintUser && namespace.equals("resolve"))
                        {
                            ImpexUtil.logImpexMessage("WARNING: Cannot IMPORT OPTION '" + name + "' " +
                                    "as this user has no rights. If you are importing the #resolve namespace, please use the resolve.maint user.", true);
                            doc = null; componentDTO = null; values = null;
                            return null;
                        }
                        else
                        {
                            componentDTO.setFullName(description);
                            componentDTO.setName(name);
                        }
                    }
                    else
                    {
                        componentDTO.setFullName(name);
                        componentDTO.setName(name);
                    }
                }
                else
                {
                    componentDTO.setFullName(name);
                    componentDTO.setName(name);
                }
                    break;
            }
                case "params":
                case "scheduler": {
                String fullName = values.get("u_name");
                    if (type.equalsIgnoreCase("scheduler"))
                {
                    if (StringUtils.isNotBlank(values.get("u_module")))
                    {
                        fullName = values.get("u_module") + "." + values.get("u_name");
                    }
                }
                componentDTO.setFullName(fullName);
                componentDTO.setModule("");
                    componentDTO.setName(values.get("u_name"));
                    break;
            }
                case "menuset": {
                componentDTO.setFullName(values.get("name"));
                componentDTO.setModule("");
                    componentDTO.setName(values.get("name"));
                    break;
            }
                case "menusection": {
                componentDTO.setFullName(values.get("title"));
                componentDTO.setModule("");
                    componentDTO.setName(values.get("name"));
                    break;
            }
                case "menuitem": {
                componentDTO.setFullName(values.get("title"));
                componentDTO.setModule(values.get("application"));
                String name = values.get("resolve_group");
                if (StringUtils.isNotBlank(name) && !name.equals("&nbsp"))
                    componentDTO.setName(values.get("resolve_group"));
                    break;
            }
                case "customtable": {
                componentDTO.setFullName(values.get("u_model_name"));
                componentDTO.setModule(values.get("u_model_name"));
                    componentDTO.setName(values.get("u_model_name"));
                    break;
            }
                case "metaformview": {
                componentDTO.setFullName(values.get("u_view_name"));
                componentDTO.setModule("");
                componentDTO.setName(values.get("u_view_name"));
                    break;
            }
                case "process": case "team": case "forum":
                case "rss": {
                componentDTO.setFullName(values.get("u_display_name"));
                componentDTO.setModule(values.get("u_display_name"));
                    componentDTO.setName(values.get("u_display_name"));
                    break;
            }
                case "user": {
                String firstName = values.get("first_name");
                String lastName = values.get("last_name");
                String fullName = "";
                if (StringUtils.isNotBlank(firstName))
                {
                    fullName = firstName + " "; 
                }
                if (StringUtils.isNotBlank(lastName))
                {
                    fullName = fullName + firstName; 
                }
                if (StringUtils.isBlank(fullName))
                {
                    fullName = values.get("user_name");
                }
                
                componentDTO.setFullName(fullName);
                componentDTO.setName(values.get("user_name"));
                    break;
            }
                case "automation builder": {
                if (table.equals("rb_general"))
                {
                    String fullName = values.get("u_namespace") + "." + values.get("u_name");
                    componentDTO.setFullName(fullName);
                }
                else
                {
                    componentDTO.setFullName(table + ": " + values.get("sys_id"));
                }
                    break;
            }
                case "gateway_filter": {
                componentDTO.setFullName(values.get("u_name"));
                componentDTO.setModule(values.get("u_name"));
                componentDTO.setName(values.get("u_name"));
                    break;
            }
                case "rr_rule": {
                componentDTO.setFullName(values.get("u_module") + "." + values.get("u_rid"));
                componentDTO.setName( values.get("u_rid"));
                componentDTO.setModule(values.get("u_module"));
                    break;
            }
                case "rr_rule_field": {
                componentDTO.setFullName("Name: " + values.get("u_name") + ", Value: " + values.get("u_value"));
                componentDTO.setName( values.get("u_name"));
                componentDTO.setModule(values.get("u_value"));
                    break;
            }
                case "rr_schema": {
                componentDTO.setFullName(values.get("u_name"));
                componentDTO.setName( values.get("u_name"));
                componentDTO.setModule(values.get("u_name"));
                    break;
            }
                case "task_tag_rel": {
                // For backward compatibility only
                componentDTO.setFullName(values.get("u_resolve_action_task_name"));
                componentDTO.setName( values.get("u_resolve_action_task_name"));
                componentDTO.setModule(values.get("u_tag_fullname"));
                    break;
            }
                case "metric_threshold": {
            	String version = values.get("u_version");
            	if (version == null || version.equals("0"))
            	{
            		componentDTO.setFullName(values.get("u_rulename") + " - Latest Revision");
            	}
            	else
            	{
            		componentDTO.setFullName(values.get("u_rulename") + " - Version " + version);
            	}
            	componentDTO.setName(values.get("u_rulename"));
            	componentDTO.setModule(values.get("u_rulemodule"));
                	break;
            }
                case "systemscript":
                case "systemproperty": {
                    componentDTO.setFullName(values.get("u_name"));
                    componentDTO.setModule("");
                    componentDTO.setName(values.get("u_name"));
                    break;
                }
            }
            componentDTO.setSys_id(values.get("sys_id"));
            componentDTO.setTablename(table);            
            componentDTO.setType(type);
            componentDTO.setDisplayType(type);
            componentDTO.setFilename(file.toString());
            componentDTO.setOperation(values.get(OPERATION));
        }
        else
        {
            //VERIFIED WITH MANGESH - THIS IS DEAD CODE, NEEDS VERIFICATION IF WE WANT TO SUPPORT IMPORT FROM 3.4 to 3.5
//            if (StringUtils.isBlank(table))
//            {
//                String rootElementName = doc.getRoot().getName();
//                if (!rootElementName.equals("graph_edge"))
//                {
//                    Catalog catalog = ImportSocialGraph.convertXDocToCatalog(doc);
//                    
//                    componentDTO = new ImpexComponentDTO();
//                    componentDTO.setSys_id(catalog.getId());
//                    componentDTO.setFullName(catalog.getName() + " : " + catalog.getType());
//                    componentDTO.setType("CATALOG");
//                    componentDTO.setDisplayType(componentDTO.getType());
//                    componentDTO.setOperation(catalog.getOperation());
//                }
//            }
//            else
//            {
                componentDTO = new ImpexComponentDTO();
                componentDTO.setSys_id(values.get("sys_id"));
                componentDTO.setName(table);
                componentDTO.setFullName(table.toUpperCase());
                switch(table)
                {
                	case "resolve_impex_glide" : case "resolve_impex_module" :
                		String name = values.get("u_module_name");
                		if (StringUtils.isNotBlank(name))
                		{
                			if (StringUtils.isNotBlank(values.get("u_name")))
                			{
                				name = name + "." + values.get("u_name");
                			}
                		}
                		else
                		{
                			name = StringUtils.isBlank(values.get("u_name")) ? "" : values.get("u_name");
                		}
                		componentDTO.setFullName(table.toUpperCase() + ": " + name);
                		break;
                	case "resolve_impex_wiki" :
                		componentDTO.setFullName(table.toUpperCase() + ": " + values.get("u_value"));
                		break;
                }
                componentDTO.setTablename(table);
                componentDTO.setType("DB");
                componentDTO.setDisplayType(componentDTO.getType());
                componentDTO.setFilename(file.toString());
                componentDTO.setOperation(values.get("operation"));
//            }
        }
        
        return componentDTO;
    }
    
    public void importGlideComps(Map<String, Collection<UserGroupRel>> delUsrGrpRelsByUsr,
                                 Map<String, Collection<UserRoleRel>> delUsrRoleRelsByUsr,
                                 Set<String> newUserImportFileNames) throws Exception
    {
        if (!glideFolder.isDirectory())
        {
            ImpexUtil.logImpexMessage(GLIDE_INSTALL_FOLDER_LOCATION + " does not exist.", true);
            throw new Exception(GLIDE_INSTALL_FOLDER_LOCATION + " does not exist.");
        }
        
        if (!glideComponents.isEmpty())
        {
            preImportOperation();
            
            if (tableDefComponents != null)
            {
                processGlideList(tableDefComponents, "(26/51) Table Definition", newUserImportFileNames);
                postOperationsForCustomTable("(27/51) Pre Import");
            }
            
            processGlideList(glideComponents, "(28/51) Glide", newUserImportFileNames);
            
            ImpexUtil.logImpexMessage("Completed. " + processedImportCount + " components imported.", false);
            
            Log.log.info("Starting Post Import Operations...");
            postImportOperation(delUsrGrpRelsByUsr, delUsrRoleRelsByUsr);
            Log.log.info("Completed Post Import Operations");
        }
    }
    
    private void processGlideList(List<ImpexComponentDTO> glideList, String stageIndex, 
    							  Set<String> newUserImportFileNames) throws Exception
    {
    	ImpexUtil.initImpexOperationStage("IMPORT", String.format("%s Components", stageIndex), glideList.size());
    	
        for (ImpexComponentDTO componentDTO : glideList)
        {
            ImpexUtil.updateImpexCount("IMPORT");
            
            if(excludeList != null && excludeList.contains(componentDTO.getSys_id()) )
            {
                ImpexUtil.logImpexMessage("Excluded: " + componentDTO.getType() + " type: " + componentDTO.getFullName(), false);
                continue;
            }
            String tableName = componentDTO.getTablename();
            if (StringUtils.isNotBlank(tableName))
            {
                if (tableName.equals("resolve_impex_module"))
                {
                    processedImportCount++;
                    continue;
                }
                
                if (tableName.equals("custom_table"))
                {
                    isCustomTablePresent = true;
                }
                if (!isMenuPresent && (tableName.equals("sys_perspective") || tableName.equals("sys_app_module")
                                || tableName.equals("sys_app_application")))
                {
                    isMenuPresent = true;
                }
            }
            else
            {
                // For Catalogs and Tags, there won't be a tablename. This import is part of Social Import.
                continue;
            }
            
            try
            {
                importComponent(componentDTO, newUserImportFileNames);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
//    public void updateImportCount()
//    {
//        ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("IMPORT");
//        if (eventVO != null)
//        {
//            String[] progress = eventVO.getUValue().split(",");
//            if (progress.length == 3)
//            {
//                try
//                {
//                    int c = Integer.parseInt(progress[2]);
//                    String value = progress[0] + "," + progress[1] + "," + ++c;
//                    eventVO.setUValue(value);
//                    ServiceHibernate.insertResolveEvent(eventVO);
//                }
//                catch(Exception e)
//                {
//                    // Do nothing
//                }
//            }
//        }
//    }
    
    public int getGlideImportCount()
    {
        return processedImportCount;
    }
    
    private void preImportOperation()
    {
        ImpexUtil.logImpexMessage("Doing the preoperations before import.", false);
        
        List<ImpexComponentDTO>  localList = new ArrayList<ImpexComponentDTO>();
        localList.addAll(glideComponents);
        
        if (tableDefComponents != null)
        {
            localList.addAll(tableDefComponents);
        }
        
        if (CollectionUtils.isEmpty(localList)) {
        	ImpexUtil.logImpexMessage("Preoperations completed.", false);
        	return;
        }
        
        try {
        	ImpexUtil.initImpexOperationStage("IMPORT", "(25/51) Pre-processing Glide and Table Definition Components to Import", 
        									   localList.size());
	        for(ImpexComponentDTO compModel : localList)
	        {
	        	ImpexUtil.updateImpexCount("IMPORT");
	            //if the user has asked to excude a file, don't import it
	            if(compModel.getExclude())
	            {
	                continue;
	            }
	            
	            //if there is a custom form that is getting imported, delete the existing one
	            if(compModel.getType().equals("METAFORMVIEW"))
	            {
	                String viewName = compModel.getFullName();
	                try
	                {
	                    ServiceHibernate.deleteMetaFormByName(viewName);
	                }
	                catch(Throwable t)
	                {
	                    Log.log.error("Error while deleting the custom form:" + viewName, t);
	                }
	            }
	
	        }//end of for loop
        } catch (Exception e) {
        	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
        				  e.getMessage() : "Failed Pre-processing Glide and Table Definition Components to Import", e);
    		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
    								  e.getMessage() : 
    								  "Failed Pre-processing Glide and Table Definition Components to Import", true);
        }
        
        ImpexUtil.logImpexMessage("Preoperations completed.", false);
    }
    
    private void importComponent(ImpexComponentDTO componentDTO, Set<String> newUserImportFileNames) throws Exception
    {
        File fileToImport = FileUtils.getFile(componentDTO.getFilename());
        try
        {
            ImpexUtil.logImpexMessage("Importing: " + componentDTO.getType() + " type: " + componentDTO.getFullName(), false);
            String type = componentDTO.getType();
            
            if(type.equalsIgnoreCase("tag"))
            {
                String name = componentDTO.getName();
                String desc = componentDTO.getModule();
                
                if(StringUtils.isNotBlank(name))
                {
                    ResolveTagVO resolveTag = new ResolveTagVO();
                    resolveTag.setUName(name);
                    resolveTag.setUDescription(desc);
                    try
                    {
                        ServiceTag.saveTag(resolveTag, userName);
                        return;
                    }
                    catch (Exception e)
                    {
                        Log.log.info("Could not import tag. Message: " + e.getMessage());
                        return;
                    }
                }
            }
            
            if (type.equalsIgnoreCase("task_tag_rel"))
            {
                if (taskTagRel == null)
                {
                    taskTagRel = new HashMap<String, Set<String>>();
                }
                
                Set<String> tags = taskTagRel.get(componentDTO.getName());
                if (tags == null)
                {
                    tags = new HashSet<String>();
                }
                tags.add(componentDTO.getModule());
                taskTagRel.put(componentDTO.getName(), tags);
                
                return;
            }
            
            // Check component specific license entitlements (if any)

            boolean glideCompImportAllowed = true;
            
            // Check named user count license entitlement
            
            if (type.equalsIgnoreCase("user") && CollectionUtils.isNotEmpty(newUserImportFileNames) &&
            	newUserImportFileNames.contains(fileToImport.getCanonicalPath())) {
            	// Check user license entitlement (V1 & V2) before adding any new user
            	glideCompImportAllowed = LicenseService.canAddNewUser(MainBase.main.getLicenseService().getLicense(),
            													   	  UserUtils.getNamedUserCount());
            	
            	if (!glideCompImportAllowed) {
            		ImpexUtil.logImpexMessage(String.format("New user glide component file %s ignored as " +
            												"adding new user will breach current user licnese entitlement.",
            												fileToImport.getAbsolutePath()), true);
            	}
            }
            
            if (glideCompImportAllowed) {
	            ExportUtil.importFile(fileToImport);
	            
	            String compName = componentDTO.getName();
	            if (type.equalsIgnoreCase("user"))
	            {
	                if (StringUtils.isNotBlank(compName))
	                    userNames.add(compName);
	            }
	            if (type.equalsIgnoreCase("actiontask"))
	            {
	               actionTaskNames.add(componentDTO.getFullName());
	            }
	            else if (type.equalsIgnoreCase("process"))
	            {
	                if (StringUtils.isNotBlank(compName))
	                    processNames.add(compName);
	            }
	            else if (type.equalsIgnoreCase("team"))
	            {
	                if (StringUtils.isNotBlank(compName))
	                    teamNames.add(compName);
	            }
	            else if (type.equalsIgnoreCase("forum"))
	            {
	                if (StringUtils.isNotBlank(compName))
	                    forumNames.add(compName);
	            }
	            else if (type.equalsIgnoreCase("rss"))
	            {
	                if (StringUtils.isNotBlank(compName))
	                    rssNames.add(compName);
	            }
	            else if (type.equalsIgnoreCase("cronjob") || type.equalsIgnoreCase("scheduler"))
	            {
	                cronJobIds.add(componentDTO.getSys_id());
	            }
	            else if (type.equalsIgnoreCase("gateway_filter"))
	            {
	                if (filterMaps == null)
	                {
	                    filterMaps = new ArrayList<Map<String, String>>();
	                }
	                Map<String, String> filterMap = getFilterMap(componentDTO);
	                filterMaps.add(filterMap);
	            }
	            else if ("metaformview".equalsIgnoreCase(type))
	            {
	                customForms.add(componentDTO.getName());
	            }
	            else if ("properties".equalsIgnoreCase(type))
                {
	                taskProperties.add(componentDTO.getName());
                }
            }
        }
        catch (Exception e)
        {
            ImpexUtil.logImpexMessage("Error Importing: " + componentDTO.getType() + ", type: " + componentDTO.getFullName() + ", File: " + componentDTO.getFilename(), true);
            throw new Exception(e);
        }
    }//importComponent
    
    protected Map<String, String> getFilterMap(ImpexComponentDTO componentDTO)
    {
        Map<String, String> map = new HashMap<String, String>();
        
        String modelName = HibernateUtil.table2Class(componentDTO.getTablename());
        modelName = modelName.substring(modelName.lastIndexOf('.')+1, modelName.length());
        map.put("MODEL", modelName);
        map.put("FILTER_NAME", componentDTO.getFullName());
        
        return map;
    }
    
    public void importSocialComponents(String moduleFolderLocation)
    {
        String socialFolder = moduleFolderLocation + "social/";
        File socialFolderLocation = FileUtils.getFile(socialFolder);
        if (socialFolderLocation.isDirectory())
        {
            boolean oldSocial = false;
            NameFileFilter filter = new NameFileFilter("edge_edge.xml");
            if (FileUtils.listFiles(socialFolderLocation, filter, TrueFileFilter.INSTANCE).size() > 0)
            {
                oldSocial = true;
            }
            SocialImpexVO socialImpex = new SocialImpexVO();
            socialImpex.setFolderLocation(socialFolder);
            try
            {
                //NOTE**** The oldSocial is NOT AT ALL compatible with this 5.2 social as the ids would have been lost from migration
                //so SOCIAL imports of version 3.4 zip file to 5.2 Resolve will not work 
                if (oldSocial)
                {
//                    ImportSocialGraph.importSocialGraph(socialFolder);
                }
                else
                {
                    SocialImpexUtil.importGraph(socialImpex);
                }
            }
            catch (Throwable t)
            {
                Log.log.error("Error while importing social components.", t);
                ImpexUtil.logImpexMessage("Error while importing social components: " + t.getMessage(), true);
            }
        }
    }
    
    public void importCatalogs(String moduleFolderLocation)
    {
        new ImportCatalogs(moduleFolderLocation, userName).importCatalogs();
        
//        String socialFolder = moduleFolderLocation + "social/";
//        File socialFolderLocation = FileUtils.getFile(socialFolder);
//        if (socialFolderLocation.isDirectory())
//        {
//            FileFilter fileFilter = new WildcardFileFilter("catalog"+ "-*.json");
//            File[] files = socialFolderLocation.listFiles(fileFilter);
//            
//            if (files != null && files.length > 0)
//            {
//                for (File file : files)
//                {
//                    Catalog resolveCatalog;
//                    try
//                    {
//                        resolveCatalog = new ObjectMapper().readValue(FileUtils.readFileToString(file), Catalog.class);
//                        
//                        //save the catalog
//                        ServiceCatalog.saveCatalog(resolveCatalog, userName);
//                        
//                        ImpexUtil.logImpexMessage("Catalog: " + resolveCatalog.getName() + " imported.", false);
//                    }
//                    catch (Exception e)
//                    {
//                        Log.log.error("Error while importing catalog: " + file.getName(), e);
//                        ImpexUtil.logImpexMessage("Error while importing catalog: " + e.getMessage(), true);
//                    }
//                }
//            }
//        }
    }
    
    protected void postImportOperation(Map<String, Collection<UserGroupRel>> delUsrGrpRelsByUsr,
                                       Map<String, Collection<UserRoleRel>> delUsrRoleRelsByUsr)
    {
        if (MapUtils.isNotEmpty(delUsrGrpRelsByUsr))
        {
        	Log.log.debug(String.format("Post Import - Processing %d Delete User Group Relations...", 
        								delUsrGrpRelsByUsr.size()));
            postOperationsForUserGroupRelTable(delUsrGrpRelsByUsr);
            Log.log.debug(String.format("Post Import - Processed %d Delete User Group Relations", 
										delUsrGrpRelsByUsr.size()));
        }
        
        if (MapUtils.isNotEmpty(delUsrRoleRelsByUsr))
        {
        	Log.log.debug(String.format("Post Import - Processing %d Delete User Role Relations...", 
        								delUsrRoleRelsByUsr.size()));
            postOperationsForUserRoleRelTable(delUsrRoleRelsByUsr);
            Log.log.debug(String.format("Post Import - Processed %d Delete User Role Relations", 
            							delUsrRoleRelsByUsr.size()));
        }
        
        postOperationsForCustomTable("(31/51) Post Import");
        
        importClobData();
        
        postOperationsForActionTasksAndSocialComps();
    }
    
    private void postOperationsForUserGroupRelTable(Map<String, Collection<UserGroupRel>> delUsrGrpRelsByUsr)
    {
    	try
    	{
	    	ImpexUtil.initImpexOperationStage("IMPORT", "(29/51) User Group Relations", delUsrGrpRelsByUsr.size());
	    	
	        for (String usrName : delUsrGrpRelsByUsr.keySet())
	        {
	        	ImpexUtil.updateImpexCount("IMPORT");
	            Users user = UserUtils.findUserWithAllReferencesNoCache(null, usrName);
	            
	            if (user != null)
	            {
	                Set<String> groupSysIds = new HashSet<String>();
	                
	                for (UserGroupRel usrGrpRel : delUsrGrpRelsByUsr.get(usrName))
	                {
	                    Groups grp = UserUtils.findGroupBy(usrGrpRel.getGroup().getSys_id(), null);
	                    
	                    // It is assumed that Resolve group names and group ids are cardinal
	                    
	                    groupSysIds.add(grp.getSys_id());
	                }
	                
	                if (!groupSysIds.isEmpty())
	                {
	                    UserUtils.addGroupToUser(user.getSys_id(), groupSysIds, userName, false);
	                }
	            }
	        }
    	} catch (Exception e) {
    		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Failed Adding User Group Relations.", e);
    		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
    								  e.getMessage() : 
    								  "Failed Adding User Group Relations. Please check RSView logs for details", true);
    	}
    }
    
    private void postOperationsForUserRoleRelTable(Map<String, Collection<UserRoleRel>> delUsrRoleRelsByUsr)
    {
    	try
    	{
    		ImpexUtil.initImpexOperationStage("IMPORT", "(30/51) User Role Relations", delUsrRoleRelsByUsr.size());
    		
	        for (String usrName : delUsrRoleRelsByUsr.keySet())
	        {
	        	ImpexUtil.updateImpexCount("IMPORT");
	            Users user = UserUtils.findUserWithAllReferencesNoCache(null, usrName);
	            
	            if (user != null)
	            {
	                Set<String> roleSysIds = new HashSet<String>();
	                
	                for (UserRoleRel usrRoleRel : delUsrRoleRelsByUsr.get(usrName))
	                {
	                    Roles role = UserUtils.findRoleBy(usrRoleRel.getRole().getSys_id(), null);
	                    
	                    // It is assumed that Resolve role names and role ids are cardinal
	                    
	                    roleSysIds.add(role.getSys_id());
	                }
	                
	                if (!roleSysIds.isEmpty())
	                {
	                    UserUtils.addRolesToUser(user.getSys_id(), roleSysIds, userName, false);
	                }
	            }
	        }
    	} catch (Exception e) {
    		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Failed Adding User Role Relations.", e);
    		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
    								  e.getMessage() : 
    								  "Failed Adding User Role Relations. Please check RSView logs for details", true);
    	}
    }
    
    protected void postOperationsForCustomTable(String stageIndex)
    {
        try
        {
            if (isCustomTablePresent)
            {
                isCustomTablePresent = false; // This flag will prevent Hibernate reinit twice.
                
                /*
                 * After custom table import, make sure that sys_perm column is not present
                 * in mata_field and meta_field_properties. Remove meta_access_rights
                 * associated with meta_field table.
                 * Also make sure that custom_table does not have schema definition for sys_perm. 
                 */
                cleanSysPermFieldFromCT();
                
                if (validateImpex)
                {
                    CustomTableMappingUtil.syncDBCustomTableSchema();
                    ImpexUtil.initImpexOperationStage("IMPORT", String.format("%s Post Custom Table Processing", 
                    														   stageIndex), 1);
                    HibernateUtil.reinit();
                    ImpexUtil.updateImpexCount("IMPORT");
                }
                else
                {
                    CustomTableMappingUtil.updateCutomTableSchema();
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error exporting custom table: ", e);
            ImpexUtil.logImpexMessage("Error exporting custom table: " + e.getMessage(), true);
        }
    }
    
    private void cleanSysPermFieldFromCT()
    {
        String cleanCTSchemaSQL = "UPDATE custom_table SET u_schema_definition = REPLACE(u_schema_definition,'<property name=\"sys_perm\" type=\"string\" column=\"sys_perm\"/>','')";
        String deleteMARSQL = "DELETE FROM meta_access_rights WHERE sys_id in (SELECT u_meta_access_rights_sys_id FROM meta_field WHERE u_name = 'sys_perm')";
        String deleteMFPSQL = "DELETE FROM meta_field_properties where u_name = 'sys_perm'";
        String deleteMFSQL = "DELETE FROM meta_field WHERE u_name = 'sys_perm'";
        
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	HibernateProxy.execute(() -> {
            
	            HibernateUtil.createSQLQuery(cleanCTSchemaSQL).executeUpdate();
	            HibernateUtil.createSQLQuery(deleteMARSQL).executeUpdate();
	            HibernateUtil.createSQLQuery(deleteMFPSQL).executeUpdate();
	            HibernateUtil.createSQLQuery(deleteMFSQL).executeUpdate();
            
        	});
        }
        catch(Exception ex)
        {
        	Log.log.warn(ex.getMessage(), ex);
                   HibernateUtil.rethrowNestedTransaction(ex);
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected void importClobData()
    {
        String dataFolder = GLIDE_INSTALL_FOLDER_LOCATION + "/data";
        File dir = FileUtils.getFile(dataFolder);
        
        //if the data folder exist 
        if (dir.exists())
        {
            try
            {
                //ExportUtil.importDirectory(dir);
                WildcardFileFilter filter = new WildcardFileFilter("*");
                Collection<File> files = FileUtils.listFiles(dir, filter, null);
                if (files != null && files.size() > 0)
                {
                	ImpexUtil.initImpexOperationStage("IMPORT", "(32/51) CUSTOM TABLE DATA FILES", files.size());
                	
                    for (File file : files)
                    {
                    	ImpexUtil.updateImpexCount("IMPORT");
                        String name = file.getName();
                        String[] fileInfoArray = name.split(",");
                        if (fileInfoArray.length == 1)
                        {
                            ExportUtil.importFile(file);
                            continue;
                        }
                        String tableName = fileInfoArray[0];
                        String tableSysId = fileInfoArray[1];
                        String tableField = fileInfoArray[2];
                        
                        
                        String hql = "from " + tableName + " where sys_id = :SYS_ID";
                        try
                        {
                            HibernateProxy.execute(() -> {
                            	 Query query = HibernateUtil.createQuery(hql);
                                 query.setParameter("SYS_ID", tableSysId);
                                 List<Object> list = query.list();
                                 
                                 if (list != null && list.size() > 0)
                                 {
                                     Map<String, Object> data;
                                     for (Object object : list)
                                     {
                                         data = (Map<String, Object>) object;
                                         Blob blob = ServiceHibernate.getBlobFromBytes(FileUtils.readFileToByteArray(file));
                                         data.put(tableField, blob);
                                         
                                         ServiceHibernate.insertOrUpdateCustomTable("admin", tableName, data);
                                     }
                                 }
                            });
                        }
                        catch(Exception e)
                        {
                            Log.log.error(e, e);
                                                      HibernateUtil.rethrowNestedTransaction(e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error importing data from folder '" + dataFolder + "': " + e.getMessage(), e);
                ImpexUtil.logImpexMessage("Error importing data from folder '" + dataFolder + "': " + e.getMessage(), true);
            }
        }// end of if
    }
    
    //@SuppressWarnings("static-access")
    protected void postOperationsForActionTasksAndSocialComps()
    {
        if (userNames.size() > 0)
        {
        	try {
	        	ImpexUtil.initImpexOperationStage("IMPORT", "(33/51) SOCIAL USER COMPONENTS", userNames.size());
	            for (String localUserName : userNames)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                UsersVO user = ServiceHibernate.getUser(localUserName);
	                if (StringUtils.isNotBlank(user.getUSummary()) && user.getUSummary().length() > 4000)
	                {
	                	String summary = StringUtils.substring(user.getUSummary(), 0, 3999);
	                	ImpexUtil.logImpexMessage("WARNING: User : " + user.getUUserName()  + " summary truncated to 4000 characters.", true);
	                	user.setUSummary(summary);
	                }
	                
	                try
	                {
	                    ServiceHibernate.saveUser(user, null, userName);
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Could not save user : " + user.getUUserName(), e);
	                    ImpexUtil.logImpexMessage("Could not save user : " + user.getUUserName()  + ". Message: " + e.getMessage(), true);
	                }
	            }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
        					  e.getMessage() : "Failed Post Processing Social User Components.", e);
        		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
        								  e.getMessage() : 
        								  "Failed Post Processing Social User Components. Please check RSView logs for details", 
        								  true);
        	}
        }
        
        if (actionTaskNames.size() > 0)
        {
        	try {
	        	ImpexUtil.initImpexOperationStage("IMPORT", "(34/51) ACTION TASKS", actionTaskNames.size());
	            for (String actionTaskName : actionTaskNames)
	            {
	            		ImpexUtil.updateImpexCount("IMPORT");
		                ResolveActionTaskVO actionTaskVO = ServiceHibernate.getActionTaskFromIdWithReferences(null, actionTaskName, userName);
		                if (actionTaskVO != null)
		                {
		                    if (taskTagRel != null && taskTagRel.get(actionTaskName) != null)
		                    {
		                        actionTaskVO.setTags(taskTagRel.get(actionTaskName));
		                    }
		                    try
		                    {
		                        ActionTaskUtil.saveResolveActionTask(actionTaskVO, userName);
		                    }
		                    catch (Exception e)
		                    {
		                    	if (StringUtils.isBlank(e.getMessage()) || 
		                            !(e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
		                              (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
		                               e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX)))) {
		                            Log.log.error("Could not save ActionTask: " + actionTaskVO.getUFullName(), e);
		                        }
		                    	
		                        ImpexUtil.logImpexMessage("Could not save ActionTask: " + actionTaskVO.getUFullName() + 
		                        						  ". Message: " + e.getMessage(), true);
		                    }
		                    
		                    Map<String, String> content = new HashMap<String, String>();
		                    content.put(Constants.ESB_PARAM_ACTIONTASKID, actionTaskVO.getSys_id()); 
		                    MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.invalidateActionTask", content);
		                }
	            	
	            }
	            // Clean all 'bad' actiontasks
	            ServiceMigration.cleanupActiontaskTable();
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
  					  		  e.getMessage() : "Failed Post Processing Action Task Components.", e);
        		ImpexUtil.logImpexMessage(
        			StringUtils.isNotBlank(e.getMessage()) ? 
  										   e.getMessage() : 
  										   "Failed Post Processing Action Task Components. Please check RSView logs for details",
  										   true);
        	}
            
            if (taskTagRel != null)
            {
                taskTagRel.clear();
                taskTagRel = null;
            }
        }
        
        if (processNames.size() > 0)
        {
        	try {
        		ImpexUtil.initImpexOperationStage("IMPORT", "(35/51) SOCIAL PROCESS COMPONENTS", processNames.size());
	            for (String processName : processNames)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                SocialProcessDTO processDTO = null;
	                try
	                {
	                    processDTO = SocialAdminUtil.getProcessByName(processName, userName);
	                    if (processDTO != null)
	                    {
	                        SocialAdminUtil.saveProcess(processDTO, userName, false);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Could not save Process: " + processName, e);
	                    ImpexUtil.logImpexMessage("Could not save Process: " + processName + ". Message: " + e.getMessage(), true);
	                }
	            }
        	} catch(Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
        					  e.getMessage() : "Failed Post Processing Social Process Components.", e);
        		ImpexUtil.logImpexMessage(
        			StringUtils.isNotBlank(
        				e.getMessage()) ? 
						e.getMessage() : 
						"Failed Post Processing Social Process Components. Please check RSView logs for details",
						true);
        	}
        }
        
        if (teamNames.size() > 0)
        {
        	try {
        		ImpexUtil.initImpexOperationStage("IMPORT", "(36/51) SOCIAL TEAM COMPONENTS", teamNames.size());
	            for (String teamName : teamNames)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                SocialTeamDTO teamDTO = null;
	                try
	                {
	                    teamDTO = SocialAdminUtil.getTeamByName(teamName, userName);
	                    if (teamDTO != null)
	                    {
	                        SocialAdminUtil.saveTeam(teamDTO, userName, false);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Could not save Team: " + teamName, e);
	                    ImpexUtil.logImpexMessage("Could not save Team: " + teamName + ". Message: " + e.getMessage(), true);
	                }
	            }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
				  		  	  e.getMessage() : "Failed Post Processing Social Team Components.", e);
        		ImpexUtil.logImpexMessage(
        			StringUtils.isNotBlank(
        				e.getMessage()) ? 
						e.getMessage() : 
						"Failed Post Processing Social Team Components. Please check RSView logs for details",
						true);
        	}
        }
        
        if (forumNames.size() > 0)
        {
        	try {
        		ImpexUtil.initImpexOperationStage("IMPORT", "(37/51) SOCIAL FORUM COMPONENTS", forumNames.size());
	            for (String forumName : forumNames)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                SocialForumDTO forumDTO = null;
	                try
	                {
	                    forumDTO = SocialAdminUtil.getForumByName(forumName, userName);
	                    if (forumDTO != null)
	                    {
	                        SocialAdminUtil.saveForum(forumDTO, userName, false);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Could not save Forum: " + forumName , e);
	                    ImpexUtil.logImpexMessage("Could not save Forum: " + forumName + ". Message: " + e.getMessage(), true);
	                }
	            }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
			  		  	  e.getMessage() : "Failed Post Processing Social Forum Components.", e);
        		ImpexUtil.logImpexMessage(
        			StringUtils.isNotBlank(
        				e.getMessage()) ? 
        				e.getMessage() : 
        				"Failed Post Processing Social Forum Components. Please check RSView logs for details",
        				true);
        	}
        }
        
        if (rssNames.size() > 0)
        {
        	try {
        		ImpexUtil.initImpexOperationStage("IMPORT", "(38/51) SOCIAL RSS COMPONENTS", rssNames.size());
	            for (String rssName : rssNames)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                SocialRssDTO rssDTO = null;
	                try
	                {
	                    rssDTO = SocialAdminUtil.getRssByName(rssName, userName);
	                    if (rssDTO != null)
	                    {
	                        SocialAdminUtil.saveRss(rssDTO, userName, false);
	                    }
	                }
	                catch (Exception e)
	                {
	                    Log.log.error("Could not save RSS: " + rssName, e);
	                    ImpexUtil.logImpexMessage("Could not save RSS: " + rssName + ". Message: " + e.getMessage(), true);
	                }
	            }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
			  		  	  	  e.getMessage() : "Failed Post Processing Social RSS Components.", e);
        		ImpexUtil.logImpexMessage(
        			StringUtils.isNotBlank(
        				e.getMessage()) ? 
        				e.getMessage() : 
        				"Failed Post Processing Social RSS Components. Please check RSView logs for details",
        				true);
        	}
        }
        
        if (cronJobIds.size() > 0)
        {
        	try {
        		ImpexUtil.initImpexOperationStage("IMPORT", "(39/51) CRON JOBS", cronJobIds.size());
	            for (String cronJobId : cronJobIds)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                ResolveCronVO vo = ServiceHibernate.findCronJob(cronJobId, null, userName);
	                if (vo != null)
	                {
	                    try
	                    {
	                        vo = ServiceHibernate.saveResolveCron(vo, userName);
	                    }
	                    catch (Exception e)
	                    {
	                       Log.log.error("Could not save Cron task:" + vo.getUName(), e);
	                       ImpexUtil.logImpexMessage("Could not save Cron task:" + vo.getUName() + ". Message: " + e.getMessage(), true);
	                    }
	                }
	            }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
		  		  	  	  	  e.getMessage() : "Failed Post Processing CRON Jobs.", e);
        		ImpexUtil.logImpexMessage(
        			StringUtils.isNotBlank(
        				e.getMessage()) ? 
        				e.getMessage() : 
        				"Failed Post Processing CRON jobs. Please check RSView logs for details",
        				true);
        	}
        }
        
        if (CollectionUtils.isNotEmpty(taskProperties)) {
            taskProperties.stream().forEach(property -> {
                try {
                    ResolvePropertiesVO taskPropertyVO = ActionTaskPropertiesUtil.findResolveProperties(null, null, property, userName);
                    if (taskPropertyVO != null) {
                        ActionTaskPropertiesUtil.saveResolveProperties(taskPropertyVO, userName);
                    }
                } catch (Exception e) {
                    Log.log.error(String.format("Could not read/save task property '%s' as part of post import operation", property), e);
                }
            });
        }
        
        if (CollectionUtils.isNotEmpty(customForms)) {
            customForms.stream().forEach(formName -> {
                try {
                    RsFormDTO form = ServiceHibernate.getMetaFormView(null, formName, null, userName, RightTypeEnum.admin, null, false);
                    if (form != null) {
                        ServiceHibernate.saveMetaFormView(form, userName);
                    }
                } catch (Exception e) {
                    Log.log.error(String.format("Could not read/save custom form '%s' as part of post import operation", formName), e);
                }
            });
        }
        
        if (isMenuPresent)
        {
            MenuCache.getInstance().invalidate();
            isMenuPresent = false;
        }
        
        if (filterMaps != null)
        {
        	try {
        		ImpexUtil.initImpexOperationStage("IMPORT", "(40/51) FILTER MAPS", filterMaps.size());
	            for (Map<String, String> map : filterMaps)
	            {
	            	ImpexUtil.updateImpexCount("IMPORT");
	                String model = map.get("MODEL");
	                String name = map.get("FILTER_NAME");
	
	                try
	                {
	                    GatewayVO filterModel = GatewayUtil.getInstance().getFilterByName(model, "<|Default|Queue|>", name);
	                    GatewayUtil.getInstance().saveFilter(model, filterModel, userName);
	                }
	                catch (Exception e)
	                {
	                    Log.log.error(e.getMessage(), e);
	                }
	            }
        	} catch (Exception e) {
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
	  		  	  	  	  	  e.getMessage() : "Failed Post Processing Filter Maps.", e);
        		ImpexUtil.logImpexMessage(
        			StringUtils.isNotBlank(
        				e.getMessage()) ? 
        				e.getMessage() : 
        				"Failed Post Processing Filter Maps. Please check RSView logs for details",
        				true);        		
        	}
        }
    } //postOperationsForActionTasks
    
    
    @SuppressWarnings("unchecked")
    protected Map<String, String> prepareRowData(XDoc doc)
    {
        String table = doc.getStringValue("/record_update/@table");

        String updateOrDelete = doc.getStringValue("/record_update/" + table + "/@action");
        HashMap<String, String> row = new HashMap<String, String>();
        row.put(OPERATION, updateOrDelete);
        
        List<Element> columns = doc.getNodes("/record_update/" + table + "/*");
        for(Element e : columns)
        {
            String name = e.getName();
            String value = e.getText();
            row.put(name, value);
        }//end of for loop
        
        return row;
    }//prepareRowData

    public static List<ImpexComponentDTO> getListComponents()
    {
        return glideComponents;
    }
    
    public static void clearGlideCompList()
    {
        if (glideComponents != null)
        {
            glideComponents.clear();
            glideComponents = null;
        }
    }
    
    public void setImpexValidationFlag(boolean validateImpex)
    {
        this.validateImpex = validateImpex;
    }
}
