/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.hibernate.query.Query;

import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.ResolveActionInvocOptions;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveCron;
import com.resolve.persistence.model.ResolveImpexManifest;
import com.resolve.persistence.model.ResolveImpexModule;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.model.ResolveProperties;
import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysAppModule;
import com.resolve.persistence.model.SysPerspective;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.ServiceCatalog;
import com.resolve.services.ServiceTag;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexManifestUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.ImportUtils;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ModuleValidationUtil
{
    protected List<ImpexComponentDTO> impexManifestList = new ArrayList<ImpexComponentDTO>();
    protected String operation;
    protected String moduleName;
    protected String userName;
    
    private final static String CONTENT = "content";
    private final static String MAIN_MODEL = "mainModel";
    private final static String ABORT_MODEL = "abortModel";
    
    public ModuleValidationUtil(String moduleName, String userName)
    {
        this.moduleName = moduleName;
        this.userName = userName;
    }
    
    @SuppressWarnings({"unchecked" })
    public List<ImpexComponentDTO> startValidation() throws Exception
    {
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveImpexManifest");
        query.setWhereClause("UModuleName = '" + moduleName + "' and lower(UOperationType) = 'import'");
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        
        String DESTINATION_FOLDER = RSContext.getResolveHome() + "rsexpert/";
        String zilFileName = DESTINATION_FOLDER + moduleName + ".zip";
        
        readZipFileFromDB(DESTINATION_FOLDER, moduleName);
        
        ImportUtils.unzip(null, DESTINATION_FOLDER + "manifest/", zilFileName);
        
        moduleName = evaluateModuleName(DESTINATION_FOLDER + "manifest/");
        if (StringUtils.isBlank(moduleName)) {
            throw new Exception("Could not find moduleName after evaluation.");
        }
        
        if (list != null && list.size() > 0)
        {
            List<ResolveImpexManifest> impexManifestList = (List<ResolveImpexManifest>) list;
            validateXMLS(DESTINATION_FOLDER+"manifest/"+moduleName+"/");
            for (ResolveImpexManifest impexManifest : impexManifestList)
            {
                try
                {
                    validate(impexManifest);
                }
                catch (Throwable t)
                {
                	String errMsg = String.format("Error%swhile validating impex defition: Type [%s], File [%s].",
 						   						  (StringUtils.isNotBlank(t.getMessage()) ? " [" + t.getMessage() + "] " : " "),
 						   						  impexManifest.getUDisplayType(), impexManifest.getUFileName());
                	
                    Log.log.warn(errMsg, t);
                    impexManifest.setUStatus(errMsg);
                }
                
                ImpexManifestUtil.saveManifest(impexManifest.doGetVO(), "admin");
            }
        }
        
        ImpexUtil.cleanup(DESTINATION_FOLDER + "/manifest", zilFileName);
        ImpexUtil.finishOperation("MANIFEST", "Import", moduleName);
        return impexManifestList;
    }
    
    private void validateXMLS(String manifestFolder) throws Exception
    {
    	String glideInstallFolder = manifestFolder + ImpexUtil.GLIDE_FOLDER + "install/";
    	ImportValidationUtil validationUtil = new ImportValidationUtil(manifestFolder,glideInstallFolder , userName, "MANIFEST");
    	validationUtil.checkOlderVersionOfActionTask();
    	// ActionTask validation.
    	validationUtil.checkForSameSysIdButDiffName(null, false);
    	// Check whether all the referenced tables are present in the module.
    	validationUtil.checkRefTables();
        //Validate social component names. If not valid, don't import and log the message.
        validationUtil.validateSocialCompsName();
        ImportUtils importUtils = new ImportUtils();
        importUtils.checkForTheSameModuleName(FileUtils.getFile(glideInstallFolder));
        validationUtil.validateImportModule();
        TagValidationUtil tagValidation = new TagValidationUtil(manifestFolder);
        tagValidation.validateTags();
             
    }
    
    private void validate(ResolveImpexManifest dto) throws Exception
    {
        String type = dto.getUDisplayType();
        Map<String, String> values = null;
        
        XDoc doc = null;
        
        if (!type.equalsIgnoreCase("document") && !type.equalsIgnoreCase("tag") 
                        && !type.equalsIgnoreCase("catalog") && !type.equalsIgnoreCase("security template"))
        {
            String fileName = dto.getUFileName();
            fileName = fileName.replace("\\", "/").substring(fileName.lastIndexOf("rsexpert"), fileName.length());
            String xml = FileUtils.readFileToString(FileUtils.getFile(RSContext.getResolveHome() + fileName), "UTF-8");
            
            Map<String, String> cDataTextMap = new HashMap<String, String>();
            
            for (String column : ImpexUtil.cDataColumnList)
            {
                // If column is <u_script>, closingElem will be </u_script>
                String closingElem = new StringBuilder(column).insert(1, "/").toString();
                
                String columnText = StringUtils.substringBetween(xml, column, closingElem);
                if (columnText != null)
                {
                    if (columnText.contains("CDATA"))
                    {
                        int startIndex = xml.indexOf(column);
                        int lastIndex = xml.lastIndexOf(closingElem);
                        columnText = xml.substring(startIndex+column.length(), lastIndex).trim();
                        StringBuilder sb = new StringBuilder(xml);
                        xml = sb.replace(startIndex+column.length(), lastIndex, "").toString();
                        cDataTextMap.put(column, columnText);
                    }
                }
            }
            
            doc = new XDoc(xml);
            values = prepareRowData(doc);
            
            for (String key : cDataTextMap.keySet())
            {
                String value = cDataTextMap.get(key);
                if (value.startsWith("<![CDATA["))
                {
                    value = value.substring(9, value.length() - 3).trim();
                    key = key.replace("<", "").replace(">", "");
                    values.put(key, value);
                }
            }
        }
        
        if(type.equalsIgnoreCase("PROPERTIES"))
        {
            validateProperties(dto, values);
        }
        else if(type.equalsIgnoreCase("TAG"))
        {
            validateTag(dto);
        }
        else if(type.equalsIgnoreCase("catalog"))
        {
            validateCatalog(dto);
        }
        else if(type.equalsIgnoreCase("document") || type.equalsIgnoreCase("security template"))
        {
            validateDocument(dto);
        }
        else if(type.equalsIgnoreCase("WIKILOOKUP"))
        {
            validateWikiLookup(dto, values);
        }
        else if(type.equalsIgnoreCase("ACTIONTASK"))
        {
            validateActionTask(dto, values);
        }
        else if(type.equalsIgnoreCase("ACTIONINVOC"))
        {
            validateActionInvoc(dto, values);
        }
        else if(type.equalsIgnoreCase("PREPROCESSOR"))
        {
            validatePreprocessor(dto, values);
        }
        else if(type.equalsIgnoreCase("PARSER"))
        {
            validateParser(dto, values);
        }
        else if(type.equalsIgnoreCase("ASSESSOR"))
        {
            validateAssessor(dto, values);
        }
        else if(type.equalsIgnoreCase("OPTIONS"))
        {
        	validateOptions(dto, values);
        }
        else if(type.equalsIgnoreCase("CRONJOB"))
        {
            validateCronJob(dto, values);
        }
        else if(type.equalsIgnoreCase("MENUSET"))
        {
            validateMenuSet(dto, values);
        }
        else if(type.equalsIgnoreCase("MENUSECTION"))
        {
            validateMenuSection(dto, values);
        }
        else if(type.equalsIgnoreCase("MENUITEM"))
        {
            validateMenuItem(dto, values);
        }
        else if(type.equalsIgnoreCase("CUSTOMTABLE"))
        {
            validateCustomTable(dto, values);
        }
        else if(type.equalsIgnoreCase("process") || type.equalsIgnoreCase("team")
                        || type.equalsIgnoreCase("forum") || type.equalsIgnoreCase("rss"))
        {
            validateSocialTable(dto, values);
        }
        else if(type.equalsIgnoreCase("PARAMS"))
        {
            validateParapeter(dto, values);
        }
        else if(type.equalsIgnoreCase("GATEWAY_FILTER"))
        {
            validateGatewayFilter(dto, values);
        }
        else
        {
            //Type --> DB, PARAMS
            validateDB(dto, values);
        }
    }
    
    private void validateProperties(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveProperties");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UModule");
        filter.setValue(dto.getUModule());
        filters.add(filter);
        
        filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UName");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
//        
//        ResolveProperties example = new ResolveProperties();
//        example.setUModule(dto.getModule());
//        example.setUName(dto.getName());
//
//        List<ResolveProperties> list = ImpexDAO.find(example);
        if(list != null && list.size() > 0)
        {
            ResolveProperties obj = (ResolveProperties)list.get(0);
            String dbValue = obj.getUValue() == null ? "" : obj.getUValue();
            String xmlValue = values.get("u_value") == null ? "" : values.get("u_value");
            if(!dbValue.equals(xmlValue)
                || !obj.getUType().equals(values.get("u_type"))
            )
            {
                error.append(ImpexComponentDTO.EXIST_DIFFERENT);
            }
            else
            {
                error.append(ImpexComponentDTO.EXIST_SAME);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());

    }//validateProperties
    
    private void validateTag(ResolveImpexManifest dto)
    {
        String tagName = dto.getUFullName();
        String status = "NEW";
        try
        {
            ResolveTagVO tag = ServiceTag.getTag(null, tagName, "admin");
            if (tag != null)
            {
                status = "Exist, Same";
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        dto.setUStatus(status);
    }//validateTag
    
    private void validateCatalog(ResolveImpexManifest dto)
    {
        String catalogId = dto.getUCompSysId();
        String status = "NEW";
        try
        {
            Catalog catalog = ServiceCatalog.getCatalog(catalogId, null, userName);
            if (catalog != null)
            {
                status = "Exist, Same";
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        dto.setUStatus(status);
    }//validateTag
    
    private void validateDocument (ResolveImpexManifest dto) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("WikiDocument");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UFullname");
        filter.setValue(dto.getUFullName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
        if (list != null && list.size() > 0)
        {
            WikiDocument wikidoc = (WikiDocument)list.get(0); // this.wiki.getDocument(dto.getFullName());//StoreUtility.find(docModel.getFullName());
            if(wikidoc != null)
            {
                Map<String, String> fileData = getDocValuesFromFile(dto.getUFileName());
                if(StringUtils.isNotBlank(fileData.get(CONTENT)))
                	fileData.put(CONTENT, fileData.get(CONTENT).replaceAll("\\\\n", "</br>"));
                if(!fileData.get(CONTENT).equals(wikidoc.getUContent()==null?"":wikidoc.getUContent())
                    || !fileData.get(MAIN_MODEL).equals(wikidoc.getUModelProcess()==null?"":wikidoc.getUModelProcess())
                    || !fileData.get(ABORT_MODEL).equals(wikidoc.getUModelException()==null?"":wikidoc.getUModelException())
                 )
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
            }//end of if
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        
        //set the error message
        dto.setUStatus(error.toString());
    }
    
    private void validateWikiLookup(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveWikiLookup");
        
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UModule");
        filter.setValue(dto.getUModule());
        filters.add(filter);
        
        filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("URegex");
        filter.setValue(dto.getUName());
        filters.add(filter);
        
        filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UWiki");
        filter.setValue(values.get("u_wiki"));
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
        if(list != null && list.size() > 0)
        {
            error.append(ImpexComponentDTO.EXIST_SAME);
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());

    }//validateWikiLookup

    private void validateActionTask(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveActionTask");
        
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UNamespace");
        filter.setValue(dto.getUModule());
        filters.add(filter);
        
        filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UName");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);

        if(data != null && data.size() > 0)
        {
            error.append(ImpexComponentDTO.EXIST_SAME);
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validateActionTask
    
    private void validateActionInvoc(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveActionInvoc");
        
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UDescription");
        filter.setValue(dto.getUFullName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
//        ResolveActionInvoc obj = ImpexDAO.findResolveActionInvoc(dto.getSys_id());
        if(data == null || data.size() == 0)
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        else
        {
            error.append(ImpexComponentDTO.EXIST_SAME);
        }
        dto.setUStatus(error.toString());
    }//validateActionInvoc

    private void validatePreprocessor(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolvePreprocess");
        
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UName");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
//        ResolvePreprocess example = new ResolvePreprocess();
//        example.setUName(dto.getName());
//
//        List<ResolvePreprocess> list = ImpexDAO.find(example);
        if(list != null && list.size() > 0)
        {
            ResolvePreprocess obj = (ResolvePreprocess)list.get(0);
            String scriptDB = obj.getUScript() != null ? obj.getUScript() : "";
            String fileDB = values.get("u_script") != null ? values.get("u_script") : "";
            fileDB = fileDB.replace("&#13;", "");
            scriptDB = scriptDB.replace("\r", "");
            if (fileDB.contains("CDATA"))
            {
                if (fileDB.contains(scriptDB))
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
            }
            else if(!scriptDB.equals(fileDB))
            {
                error.append(ImpexComponentDTO.EXIST_DIFFERENT);
            }
            else
            {
                error.append(ImpexComponentDTO.EXIST_SAME);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validatePreprocessor
    
    private void validateParser(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveParser");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UName");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
//        
//        ResolveParser example = new ResolveParser();
//        example.setUName(dto.getName());
//
//        List<ResolveParser> list = ImpexDAO.find(example);
        if(list != null && list.size() > 0)
        {
            ResolveParser obj = (ResolveParser)list.get(0);
            String scriptDB = obj.getUScript() != null ? obj.getUScript() : "";
            String fileDB = values.get("u_script") != null ? values.get("u_script") : "";
            fileDB = fileDB.replace("&#13;", "");
            scriptDB = scriptDB.replace("\r", "");
            if (fileDB.contains("CDATA"))
            {
                if (fileDB.contains(scriptDB))
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
            }
            else if(!scriptDB.equals(fileDB))
            {
                error.append(ImpexComponentDTO.EXIST_DIFFERENT);
            }
            else
            {
                error.append(ImpexComponentDTO.EXIST_SAME);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());

    }//validateParser
    
    private void validateAssessor(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveAssess");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UName");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
//        
//        ResolveAssess example = new ResolveAssess();
//        example.setUName(dto.getName());
//
//        List<ResolveAssess> list = ImpexDAO.find(example);
        if(list != null && list.size() > 0)
        {
            ResolveAssess obj = (ResolveAssess)list.get(0);
            String scriptDB = obj.getUScript() != null ? obj.getUScript() : "";
            String fileDB = values.get("u_script") != null ? values.get("u_script") : "";
            fileDB = fileDB.replace("&#13;", "");
            scriptDB = scriptDB.replace("\r", "");
            if (fileDB.contains("CDATA"))
            {
                if (fileDB.contains(scriptDB))
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
            }
            else if(!scriptDB.equals(fileDB))
            {
                error.append(ImpexComponentDTO.EXIST_DIFFERENT);
            }
            else
            {
                error.append(ImpexComponentDTO.EXIST_SAME);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validateAssessor
    
    private void validateOptions(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveActionInvocOptions");
        
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("sys_id");
        filter.setValue(values.get("sys_id"));
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
//        
//        ResolveActionInvocOptions example = new ResolveActionInvocOptions();
//        example.setUName(dto.getName());
//        example.setUDescription(values.get("u_description"));
//
//        List<ResolveActionInvocOptions> list = ImpexDAO.find(example);
        if(list != null && list.size() > 0)
        {
            ResolveActionInvocOptions obj = (ResolveActionInvocOptions)list.get(0);
            String valueDB = obj.getUValue();
            String valueFile = values.get("u_value");
            
            if (StringUtils.isNotBlank(valueDB))
            {
            	valueDB = valueDB.replaceAll("\r", "");
            }
            if (StringUtils.isNotBlank(valueFile))
            {
            	valueFile = valueFile.replaceAll("&#13;", "");   
            }
            if (StringUtils.isBlank(valueDB))
            {
                if (StringUtils.isBlank(valueFile))
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
            }
            else if (valueFile.contains("CDATA"))
            {
                if (valueFile.contains(valueDB))
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
            }
            else if(!valueDB.equals(valueFile))
            {
                error.append(ImpexComponentDTO.EXIST_DIFFERENT);
            }
            else
            {
                error.append(ImpexComponentDTO.EXIST_SAME);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validateOPtions
    
    
    private void validateCronJob(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveCron");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UName");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
//        
//        ResolveCron example = new ResolveCron();
//        example.setUName(dto.getName());
//        example.setUModule(dto.getModule());
//
//        List<ResolveCron> list = ImpexDAO.find(example);
        if(list != null && list.size() > 0)
        {
            ResolveCron obj = (ResolveCron)list.get(0);
            if(!(obj.getUExpression().equals(values.get("u_expression")) || !(obj.getURunbook().equals(values.get("u_runbook")))))
            {
                error.append(ImpexComponentDTO.EXIST_DIFFERENT);
            }
            else
            {
                error.append(ImpexComponentDTO.EXIST_SAME);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validateCronJob
    
    
    private void validateMenuSet(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("SysPerspective");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("name");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
        if (CollectionUtils.isNotEmpty(data)) {
            SysPerspective menuSet = (SysPerspective)data.get(0);
            if(menuSet != null)
            {
                if(!(menuSet.getSys_id().equals(values.get("sys_id"))))
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
            }
            else
            {
                error.append(ImpexComponentDTO.NEW_STATUS);
            }
        } else {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        
        dto.setUStatus(error.toString());
    }//validateMenuSet
    
    private void validateMenuSection(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("SysAppApplication");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("name");
        filter.setValue(dto.getUName());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
        if(CollectionUtils.isNotEmpty(data))
        {
            SysAppApplication menuSection = (SysAppApplication) data.get(0);
            if (menuSection != null) {
                if(!(menuSection.getSys_id().equals(values.get("sys_id"))) || !(menuSection.getTitle().equals(values.get("title")))) {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                } else {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
            }
            else {
                error.append(ImpexComponentDTO.NEW_STATUS);
            }
        } else {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validateMenuSection
    
    private void validateMenuItem(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();

//        String appId = dto.getUModule();
//        String group = dto.getUName();
//        String title = dto.getUFullName();
        
        String name = values.get("name");
        String title = values.get("title");
        String appModuleGroup = values.get("resolve_group");
        
        if (StringUtils.isBlank(title)) {
    		String errMsg = String.format("Invalid System Application Module Title [%s].", 
    									  (StringUtils.isNotBlank(title) ? title : "null"));
    		throw new IllegalArgumentException(errMsg);
    	}
        
        List<QueryFilter> filters = new ArrayList<>();
        /*
         * For Sys App Module validation along with Sys App it is associated with, 
         * the export data for Sys App Module should contain the name and 
         * of the Sys App and not sys_id. 
         * 
         * The sys_ids between exporting and importing systems are not same
         * unless they are cardinal sys_ids.
         * 
         * Sys App Name should be unique.
         * 
         * Sys App Module Name/Title + Sys App Name should also be unique,
         * 
         * Exported data of Sys App Module should contain name of the 
         * referenced Sys App in the "application" and not the sys_id.
         * 
         */
//        QueryFilter filter = new QueryFilter();
//        filter.setType("auto");
//        filter.setCondition(QueryFilter.EQUALS);
//        filter.setField("sysAppApplication.sys_id");
//        filter.setValue(appId);
//        filters.add(filter);
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("title");
        filter.setValue(title);
        filters.add(filter);
        
        if (StringUtils.isNotBlank(name))
        {
            filter = new QueryFilter();
            filter.setType("auto");
            filter.setCondition(QueryFilter.EQUALS);
            filter.setField("name");
            filter.setValue(name);
            filters.add(filter);
        }
        
        if (StringUtils.isNotBlank(appModuleGroup))
        {
            filter = new QueryFilter();
            filter.setType("auto");
            filter.setCondition(QueryFilter.EQUALS);
            filter.setField("appModuleGroup");
            filter.setValue(appModuleGroup);
            filters.add(filter);
        }
        
        QueryDTO query = new QueryDTO();
        query.setModelName("SysAppModule");
        query.setFilterItems(filters);
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
//        
//        SysAppModule example = new SysAppModule();
//        example.setTitle(dto.getName());
//        example.setGroup(dto.getModule());
        SysAppModule menuItem = null;
        if (data != null && data.size() > 0)
        {
            menuItem = (SysAppModule) data.get(0);
        }
//        SysAppModule menuItem = ImpexDAO.findMenuItem(example, null, null);
        if(menuItem != null)
        {
            String dbQuery = menuItem.getQuery() != null ? menuItem.getQuery().trim() : "";
            String fileQuery = values.get("query") != null ? values.get("query") : "";
            if(!(menuItem.getSys_id().equals(values.get("sys_id")))
                            || !(dbQuery.equals(fileQuery))
             )
            {
                error.append(ImpexComponentDTO.EXIST_DIFFERENT);
            }
            else
            {
                error.append(ImpexComponentDTO.EXIST_SAME);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validateMenuSection
    
    private void validateCustomTable(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        StringBuffer error = new StringBuffer();

        QueryDTO query = new QueryDTO();
        query.setModelName("CustomTable");
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("UName");
        filter.setValue(dto.getUModule());
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
//        CustomTable example = new CustomTable();
//        example.setUModelName(dto.getModule());
        if (data != null && data.size() > 0)
        {
            CustomTable customTable = (CustomTable) data.get(0);
    //        CustomTable customTable = ImpexDAO.find(example, null);
            if(customTable != null)
            {
                String dbSchemaDef = customTable.getUSchemaDefinition() != null ? customTable.getUSchemaDefinition() : "";
                String fileSchemaDef = values.get("u_schema_definition") != null ? values.get("u_schema_definition") : "";
                if(!(customTable.getSys_id().equals(values.get("sys_id")))
                                || !(dbSchemaDef.equals(fileSchemaDef))
                 )
                {
                    error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                }
                else
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
            }
            else
            {
                error.append(ImpexComponentDTO.NEW_STATUS);
            }
        }
        else
        {
            error.append(ImpexComponentDTO.NEW_STATUS);
        }
        dto.setUStatus(error.toString());
    }//validateCustomTable
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void validateSocialTable(ResolveImpexManifest dto, Map<String, String> values)
    {
        String status = "NEW";

        String hql = "select * from " + dto.getUTableName() + " where u_display_name = '" + dto.getUModule() + "'";
        
        List<? extends Object> result = null;

        try
        {
            result = (List<? extends Object>) HibernateProxy.execute(() -> {
            	Query query = HibernateUtil.createSQLQuery(hql);
                return query.list();
            });

        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing SQL: " + hql, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if (result != null && result.size() > 0)
        {
            status = "Exist, Same";
        }
        dto.setUStatus(status);
    }//validateCustomTable
    
    private void validateParapeter(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        String status = "NEW";
        
        QueryDTO query = new QueryDTO();
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("sys_id");
        filter.setValue(values.get("sys_id"));
        filters.add(filter);
        query.setFilterItems(filters);
        
        query.setModelName("ResolveActionParameter");
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
        if (data != null && data.size() > 0)
        {
            ResolveActionParameter parameter = (ResolveActionParameter)data.get(0);
            String xmlName = values.get("u_name") == null ? "" : values.get("u_name");
            String xmlInvocId = values.get("u_invocation") == null ? "" : values.get("u_invocation");
            
            String dbName = parameter.getUName() == null ? "" : parameter.getUName();
            String dbInvocId = parameter.getInvocation() == null ? "" : parameter.getInvocation().getSys_id();
            dto.setUFullName(xmlName +  (parameter.getInvocation() == null ? "" : " - " +  parameter.getInvocation().getUDescription()));
            
            if (!xmlName.equals(dbName) || !xmlInvocId.equals(dbInvocId))
            {
                status = "Exist, Different";
            }
            else
            {
                status = "Exist, Same";
            }
        }

        dto.setUStatus(status);
    }//validateCustomTable
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void validateGatewayFilter(ResolveImpexManifest dto, Map<String, String> values) throws Exception
    {
        String status = "NEW";
        
        QueryDTO query = new QueryDTO();
        String modelName = HibernateUtil.table2Class(dto.getUTableName());
        query.setModelName(modelName);
        List<QueryFilter> filters = new ArrayList<>();
        QueryFilter filter = new QueryFilter();
        filter.setType("auto");
        filter.setCondition(QueryFilter.EQUALS);
        filter.setField("sys_id");
        filter.setValue(values.get("sys_id"));
        filters.add(filter);
        query.setFilterItems(filters);
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
        
        if (data != null && data.size() > 0)
        {
            Object objArray[] = null;
            Class noparams[] = {};
            Object o = data.get(0);
            Class clazz = o.getClass();
            Method method = clazz.getMethod("getUName", noparams);
            String dbName = (String)method.invoke(o, objArray);
            
            method = clazz.getMethod("getUScript", noparams);
            String dbScript = (String)method.invoke(o, objArray);
            if(StringUtils.isBlank(dbScript))
            {
                dbScript = "";
            }
            method = clazz.getMethod("getURunbook", noparams);
            String dbRunbook = (String)method.invoke(o, objArray);
            if(StringUtils.isBlank(dbRunbook))
            {
                dbRunbook = "";
            }
            
            method = clazz.getMethod("getSys_id", noparams);
            String dbSysId = (String)method.invoke(o, objArray);
            
            String xmlName = values.get("u_name");
            String xmlScript = values.get("u_script") == null ? "" : values.get("u_script");
            String xmlRunbook = values.get("u_runbook") == null ? "" : values.get("u_runbook");
            String xmlSysId = values.get("sys_id");
            
            if (!dbSysId.equals(xmlSysId) || !dbRunbook.equals(xmlRunbook) || !dbScript.equals(xmlScript) || !dbName.equals(xmlName))
            {
                status = "Exist, Different";
            }
            else
            {
                status = "Exist, Same";
            }
        }

        dto.setUStatus(status);
    }//validateCustomTable

    
    @SuppressWarnings("unused")
    private void validateDB(ResolveImpexManifest dto, Map<String, String> values)
    {
        //Type --> DB
        StringBuffer error = new StringBuffer();
        String modelName = HibernateUtil.table2Class(dto.getUTableName());
        if (modelName == null)
        {
            Log.log.error("Model for the table " + dto.getUTableName() + " could not be found.");
            error.append("Model Does Not Exist");
        }
        else
        {
            Map<String, Object> dbRowData =  GeneralHibernateUtil.findByIdForModel(modelName, values.get("sys_id"));//ServiceHibernate.findById(modelName, dto.getUCompSysId());//StoreUtility.getRowData(modelName, dto.getUCompSysId(), null);
            String id = dbRowData != null ? (String) dbRowData.get("sys_id") : null; 
            if(StringUtils.isNotEmpty(id))
            {
                Iterator<String> it = dbRowData.keySet().iterator();
                while(it.hasNext())
                {
                    String key = it.next();//id
                    String keyFile = HibernateUtil.getColumnName(modelName, key);//sys_id
                    
                    //don't compare the sys columns
                    if(ImpexUtil.isSysField(keyFile))
                    {
                        continue;
                    }
                    Object dbValue = dbRowData.get(key);
                    //only compare the strings
                    if(dbValue != null && dbValue instanceof String)
                    {
                        String fileValue = values.get(keyFile) != null ? values.get(keyFile) : "";
                        fileValue = fileValue.replace("&#13;", "\r");
                        if(!dbValue.equals(fileValue))
                        {
                            error.append(ImpexComponentDTO.EXIST_DIFFERENT);
                            break;
                        }
                    }
                }//end of while
                if(error.length() == 0 )
                {
                    error.append(ImpexComponentDTO.EXIST_SAME);
                }
            }
            else
            {
                error.append(ImpexComponentDTO.NEW_STATUS);
            }
        }
        
        dto.setUStatus(error.toString());
    }//validateDB
    
    @SuppressWarnings("unchecked")
    private Map<String, String> prepareRowData(XDoc doc)
    {
        String table = doc.getStringValue("/record_update/@table");

        HashMap<String, String> row = new HashMap<String, String>();
        String updateOrDelete = doc.getStringValue("/record_update/" + table + "/@action");
        row.put("operation", updateOrDelete);
        
        List<Element> columns = doc.getNodes("/record_update/" + table + "/*");
        for(Element e : columns)
        {
            String name = e.getName();
            String value = e.getText();
            row.put(name, value);
        }//end of for loop
        
        return row;
    }//prepareRowData
    
    private  Map<String, String> getDocValuesFromFile(String fileName) throws Exception
    {
        Map<String, String> fileData = new HashMap<String, String>();
        
        //read the file and get content, model, abort in hashmap
        File importFile = FileUtils.getFile(fileName);
        XDoc xdoc = null;
        
        try {
            xdoc = ImpexUtil.convertFileToXDoc(importFile);
        } catch (DocumentException de) {
            ImpexUtil.removeCdataFromXML(importFile);
            xdoc = ImpexUtil.convertFileToXDoc(importFile);
        }
        if (xdoc!= null) {
            Document domdoc = xdoc.getDocument();
            Element docel = domdoc.getRootElement();
            String content = ImpexUtil.getElement(docel, "content");
            String mainModel = ImpexUtil.getElement(docel, "model");
            String abortModel = ImpexUtil.getElement(docel, "abort");
            
            fileData.put(CONTENT, content);
            fileData.put(MAIN_MODEL, mainModel);
            fileData.put(ABORT_MODEL, abortModel);
        }
        
        return fileData;
    }//getValuesFromFile
    
    private void readZipFileFromDB(String destination, String moduleName)
    {
        ResolveImpexModule rim = ImpexUtil.getResolveImpexModule(moduleName, true, userName);
        if (rim == null)
        {
            rim = ImpexUtil.getResolveImpexModule(moduleName, false, userName);
        }
        
        if (rim != null)
        {
            String zipFileName = destination + moduleName + ".zip";
            File zipFileContent = FileUtils.getFile(destination + moduleName + ".zip");
            
            if (rim.getUZipFileContent() != null && !zipFileContent.exists())
            {
                try
                {
                    FileUtils.writeByteArrayToFile(zipFileContent, rim.getUZipFileContent());
                }
                catch (IOException e)
                {
                    Log.log.error("Could not read content of the zip file:" + zipFileName, e);
                }
            }
        }
    }
    
    private String evaluateModuleName(String path) throws Exception{
        String moduleName = null;
        File dir = FileUtils.getFile(path);
        Collection<File> moduleFileList = FileUtils.listFiles(dir, new PrefixFileFilter(ImpexUtil.DefaultPackageFileName), DirectoryFileFilter.DIRECTORY);
        if (CollectionUtils.isNotEmpty(moduleFileList)) {
            String fileContent = FileUtils.readFileToString(moduleFileList.iterator().next(), StandardCharsets.UTF_8);
            Pattern pattern = Pattern.compile("<u_name>(.*)<\\/u_name>", Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(fileContent);
            if (matcher.find()) {
                moduleName = matcher.group(1);
            }
        }
        return moduleName;
    }
}
