/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import static com.resolve.services.vo.QueryFilter.BETWEEN;
import static com.resolve.services.vo.QueryFilter.DATE_TYPE;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.query.Query;
import org.hibernate.type.BooleanType;
import org.hibernate.type.StringType;
import org.owasp.esapi.ESAPI;
import org.xml.sax.SAXException;

import com.resolve.dto.AboutDTO;
import com.resolve.persistence.dao.ResolveImpexGlideDAO;
import com.resolve.persistence.dao.ResolveImpexWikiDAO;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.ResolveImpexGlide;
import com.resolve.persistence.model.ResolveImpexLog;
import com.resolve.persistence.model.ResolveImpexManifest;
import com.resolve.persistence.model.ResolveImpexModule;
import com.resolve.persistence.model.ResolveImpexWiki;
import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysAppModule;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.main.MImpex;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.RSContext;
import com.resolve.search.saas.SaaSReport;
import com.resolve.services.ServiceCatalog;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.ServiceTag;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.customtable.CustomFormUtil;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.services.hibernate.customtable.ExtMetaFormLookup;
import com.resolve.services.hibernate.menu.MenuDefinitionUtil;
import com.resolve.services.hibernate.menu.MenuSetUtil;
import com.resolve.services.hibernate.vo.ArtifactTypeVO;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveBusinessRuleVO;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.ResolveImpexManifestVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.hibernate.vo.ResolveImpexWikiVO;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveSysScriptVO;
import com.resolve.services.hibernate.vo.ResolveWikiLookupVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.SysAppApplicationVO;
import com.resolve.services.hibernate.vo.SysAppModuleVO;
import com.resolve.services.hibernate.vo.SysPerspectiveVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexDefinitionDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.QuerySort.SortOrder;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.constants.HibernateConstantsEnum;

public class ImpexUtil
{
    private static StringBuilder exportLog = new StringBuilder();
    
    public static final String UTF8 = "UTF-8";
    public static final String WIKI_FOLDER = "wiki/";
    public static final String GLIDE_FOLDER = "update/";
    public static final String REPORT_FOLDER = "report/";
    public static final String DefaultPackageFileName = "resolve_impex_module";
    public static List<String> cDataColumnList = new ArrayList<String>();
    public static final String MODULE_ALREADY_EXISTS_MARKER = "already exists";
    
    public static final Map<String, String> MAP_TABLENAME_TYPE = new HashMap<String, String>();
    static
    {
        MAP_TABLENAME_TYPE.put("resolve_action_task", "ACTIONTASK");
        MAP_TABLENAME_TYPE.put("resolve_action_invoc", "ACTIONINVOC");
        MAP_TABLENAME_TYPE.put("resolve_preprocess", "PREPROCESSOR");
        MAP_TABLENAME_TYPE.put("resolve_parser", "PARSER");
        MAP_TABLENAME_TYPE.put("resolve_assess", "ASSESSOR");
        MAP_TABLENAME_TYPE.put("resolve_task_expression", "EXPRESSION");
        MAP_TABLENAME_TYPE.put("resolve_task_output_mapping", "OUTPUT_MAPPING");
        MAP_TABLENAME_TYPE.put("resolve_action_invoc_options", "OPTIONS");
        MAP_TABLENAME_TYPE.put("resolve_action_parameter", "PARAMS");
        MAP_TABLENAME_TYPE.put("resolve_properties", "PROPERTIES");
        MAP_TABLENAME_TYPE.put("resolve_action_pkg", "TAG");
        MAP_TABLENAME_TYPE.put("resolve_tag", "TAG");
        MAP_TABLENAME_TYPE.put("properties", "systemProperty");
        MAP_TABLENAME_TYPE.put("resolve_sys_script", "systemScript");
        // For backward Compatibility
        MAP_TABLENAME_TYPE.put("resolve_action_task_pkg_rel", "TASK_TAG_REL");
        MAP_TABLENAME_TYPE.put("resolve_wiki_lookup", "WIKILOOKUP");
        MAP_TABLENAME_TYPE.put("resolve_cron", "scheduler");
        MAP_TABLENAME_TYPE.put("sys_perspective", "MENUSET");
        MAP_TABLENAME_TYPE.put("sys_app_application", "MENUSECTION");
        MAP_TABLENAME_TYPE.put("sys_app_module", "MENUITEM");
        MAP_TABLENAME_TYPE.put("custom_table", "CUSTOMTABLE");
        MAP_TABLENAME_TYPE.put("meta_form_view", "METAFORMVIEW");
        MAP_TABLENAME_TYPE.put("resolve_decision_tree", "DECISIONTREE");
        MAP_TABLENAME_TYPE.put("social_process", "PROCESS");
        MAP_TABLENAME_TYPE.put("social_team", "TEAM");
        MAP_TABLENAME_TYPE.put("social_forum", "FORUM");
        MAP_TABLENAME_TYPE.put("rss_subscription", "RSS");
        MAP_TABLENAME_TYPE.put("sys_user", "USER");
        
        MAP_TABLENAME_TYPE.put("database_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("emailserver_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("ews_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("exchangeserver_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("hpom_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("hpsm_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("netcool_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("remedyx_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("salesforce_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("servicenow_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("snmp_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("tcp_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("tsrm_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("xmpp_filter", "GATEWAY_FILTER");
        MAP_TABLENAME_TYPE.put("http_filter", "GATEWAY_FILTER");
        
        MAP_TABLENAME_TYPE.put("rb_condition", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_connection", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_connection_param", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_criterion", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_general", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_if", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_loop", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_task", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_task_condition", "AUTOMATION BUILDER");
        MAP_TABLENAME_TYPE.put("rb_task_variable", "AUTOMATION BUILDER");
        
        MAP_TABLENAME_TYPE.put("rr_rule", "RR_RULE");
        MAP_TABLENAME_TYPE.put("rr_rule_field", "RR_RULE_FIELD");
        MAP_TABLENAME_TYPE.put("rr_schema", "RR_SCHEMA");
        MAP_TABLENAME_TYPE.put("metric_threshold", "METRIC_THRESHOLD");
        
        cDataColumnList.add("<u_script>");
        cDataColumnList.add("<u_value>");
        cDataColumnList.add("<u_sample>");
    }

    /*
     * New implementation start
     */

    public static List<ResolveImpexModuleVO> getResolveImpexModules(QueryDTO query, String username) throws Exception
    {
        List<ResolveImpexModuleVO> result = new ArrayList<ResolveImpexModuleVO>();
        int limit = query.getLimit();
        int offset = query.getStart();
        
        if (CollectionUtils.isEmpty(query.getFilterItems())) {
        	SimpleDateFormat sdf = new SimpleDateFormat(SaaSReport.BUCKET_DATE_TIME_FORMAT);
        	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
        	query.addFilterItem(new QueryFilter(DATE_TYPE, "sysUpdatedOn", sdf.format(new Date(0l)), 
    				  			   				sdf.format(new Date()), BETWEEN));
        }
        
        if (CollectionUtils.isEmpty(query.getSortItems())) {
        	QuerySort sortItem = new QuerySort("sysUpdatedOn", SortOrder.DESC);
        	query.addSortItem(sortItem);
        }
        
        // In case this is invoked directly bypassing Ajax controller
        if (StringUtils.isBlank(query.getSelectColumns()) || query.getSelectColumns().contains("UZipFileContent") ||
            query.getSelectColumns().contains("manifestGraph") || query.getSelectColumns().contains("impexDefinition") ||
            query.getSelectColumns().contains("resolveImpexWikis") || query.getSelectColumns().contains("resolveImpexGlides")) {
        	query.setSelectColumns(ImpexEnum.DEFINITION_LIST_COLUMNS.getValue());
        }
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
        
        if (CollectionUtils.isNotEmpty(data))
        {
            populateResultList(data, result, query.getSelectColumns());
        }

        return result;
    }
    
    /**
     * API to be called from new Impex UI ONLY to access Impex definition list as it has
     * specific processing. E.g.
     * 1. It expects QueryFilter for free text search only.
     * 2. Advanced search, present in legacy API, is not supported.
     * 
     * TODO : If React UI gets modified to support advanced search, this API would need modification.
     * 
     * @param query
     *            {@link QueryDTO}, represents the query, pagination, sorting,
     *            grouping for the UI
     * @param username : String representing username of a logged in user.
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	public static List<ResolveImpexModuleVO> getResolveImpexModulesV2(QueryDTO query, String username) throws Exception
    {
        List<ResolveImpexModuleVO> result = new ArrayList<ResolveImpexModuleVO>();
        List<? extends Object> data = null;
        int limit = query.getLimit();
        int offset = query.getStart();
        if (StringUtils.isBlank(query.getSort()))
        {
            query.setSort("[{\"property\":\"sysUpdatedOn\",\"direction\":\"DESC\"}]");
        }
        try
        {
          HibernateProxy.setCurrentUser(username);
        	data = (List<? extends Object>) HibernateProxy.execute(() -> {
                CriteriaBuilder cb = HibernateUtil.getCurrentSession().getCriteriaBuilder();
                CriteriaQuery<Object[]> criteriaQuery = cb.createQuery(Object[].class);
                Root<ResolveImpexModule> resolveModule = criteriaQuery.from(ResolveImpexModule.class);
                criteriaQuery.multiselect(
                                resolveModule.get("sys_id"),
                                resolveModule.get("UName"),
                                resolveModule.get("UZipFileName"),
                                resolveModule.get("UDescription"),
                                resolveModule.get("UDirty"),
                                resolveModule.get("UVersion"),
                                resolveModule.get("sysCreatedOn"),
                                resolveModule.get("sysCreatedBy"),
                                resolveModule.get("sysUpdatedOn"),
                                resolveModule.get("sysUpdatedBy"),
                                cb.selectCase()
                                .when(cb.isNull(resolveModule.get("manifestGraph")), false)
                                .when(cb.equal(cb.length(resolveModule.get("manifestGraph")), 0), false)
                                .otherwise(true)
                                );
                if (StringUtils.isBlank(query.getSort()))
                    criteriaQuery.orderBy(cb.desc(resolveModule.get("sysUpdatedOn")));
                else
                {
                    String field = query.getSortItems().get(0).getProperty();
                    
                    if (query.getSortItems().get(0).getDirection() == SortOrder.DESC)
                    {
                        criteriaQuery.orderBy(cb.desc(resolveModule.get(field)));
                    }
                    else
                    {
                        criteriaQuery.orderBy(cb.asc(resolveModule.get(field)));
                    }
                }
                
                if (StringUtils.isNotBlank(query.getFilter()))
                {
                    query.getFilterItems().stream().forEach(qf -> { criteriaQuery.where(cb.like(cb.lower(resolveModule.get(qf.getField())), "%" + qf.getValue().toLowerCase() + "%"));});
                }

                return  HibernateUtil.getCurrentSession().createQuery(criteriaQuery).setFirstResult(offset).setMaxResults(limit).getResultList();
             
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error while getting impex list", e);
            throw new Exception(e);
        }
        
        if (CollectionUtils.isNotEmpty(data))
        {
            populateResultList(data, result, query.getSelectColumns());
        }
        
        return result;
    }
    
    private static void populateResultList(List<? extends Object> data, List<ResolveImpexModuleVO> result, String selectedColumns) throws Exception
    {
        Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
        if (StringUtils.isNotBlank(selectedColumns))
        {
            String[] columns = selectedColumns.split(",");

            //this will be the array of objects
            for (Object o : data)
            {
                if (columns.length == 1)
                {
                    ResolveImpexModule module = new ResolveImpexModule();
                    PropertyUtils.setProperty(module, columns[0], o);
                    result.add(populateVoOrgName(module, mapOfOrganization));
                }
                else
                {
                    Object[] record = (Object[]) o;
                    ResolveImpexModuleVO instance = new ResolveImpexModuleVO();

                    //set the attributes in the object
                    for (int x = 0; x < columns.length; x++)
                    {
                        String column = columns[x].trim();
                        Object value = record[x];

                        PropertyUtils.setProperty(instance, column, value);
                    }
                    
                    String url = ImpexEnum.IMPEX_DOWNLOAD_URL.getValue() + ImpexEnum.QUESTION.getValue() + ImpexEnum.FILENAME.getValue() + ImpexEnum.EQUAL.getValue() + instance.getUZipFileName() +
                                    ImpexEnum.AMPERCENT.getValue() + ImpexEnum.MODULEID.getValue() + ImpexEnum.EQUAL.getValue() + instance.getSys_id();

                    instance.setULocation(url);

                    result.add(instance);
                }
            }
        }
        else
        {
            // Grab everything
            for (Object o : data)
            {
                ResolveImpexModule instance = (ResolveImpexModule) o;
                if (instance != null)
                {
                    result.add(populateVoOrgName(instance, mapOfOrganization));
                }
            }
        }
    }


    private static ResolveImpexModuleVO populateVoOrgName(ResolveImpexModule model, Map<String, String> mapOfOrganization)
    {
        ResolveImpexModuleVO vo = null;

        if(model != null)
        {
            vo = model.doGetVO();

//            if (mapOfOrganization.size() > 0)
//            {
//
//                if (StringUtils.isNotBlank(vo.getSysOrg()))
//                {
//                    vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
//                }
//                else
//                {
//                    vo.setSysOrganizationName("");
//                }
//            }
        }

        return vo;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO getExportCompListToBeAdded (QueryDTO query, String compType, String userName) throws Exception
    {
        ResponseDTO result = new ResponseDTO();
        if (compType.equalsIgnoreCase("empty"))
        {
        	return result;
        }
        if (compType.equalsIgnoreCase("actionTask") || compType.equalsIgnoreCase(ImpexEnum.TASK.getValue()))
        {
            query.setModelName("ResolveActionTask");
            query.setSelectColumns("sys_id,UName,UNamespace,USummary,sysUpdatedOn");
            query.setPage(1);
            /*
             * In actiontask the model has a property named UFullName, whereas when we construct QueryFilter, the 'N' stays small
             * causing query to fail.
             */
            if (!query.getFilterItems().isEmpty()) {
                Optional<QueryFilter> filter = query.getFilterItems().stream().filter(f -> f.getField().equals("UFullname")).findFirst();
                if (filter.isPresent()) {
                    filter.get().setField("UFullName");
                }
            }
            Map<String, Object> resultMap = ExportUtils.getAllActionTasksForImpex(query);
            int total = ServiceHibernate.getTotalHqlCount(query);
            List<ResolveActionTaskVO> data = (List<ResolveActionTaskVO>)resultMap.get("ACTION_TASK_VO_LIST");
//            Integer total = (Integer)resultMap.get("TOTAL_COUNT");
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("actionTaskModule"))
        {
            query.setModelName("ResolveActionTask");

            Map<String, Object> resultMap = ServiceHibernate.findNamespaceForImpex(query, userName);
            List<ResolveActionTaskVO> data = (List<ResolveActionTaskVO>)resultMap.get("ACTION_TASK_VO_LIST");
            Integer total = (Integer)resultMap.get("TOTAL_COUNT");
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase(ImpexEnum.WIKI.getValue()) || compType.equalsIgnoreCase("runbook") ||
        		compType.equalsIgnoreCase("securityTemplate") || compType.equalsIgnoreCase(ImpexEnum.DECISIONTREE.getValue()))
        {
            query.setModelName("WikiDocument");
            if (compType.equalsIgnoreCase("runbook"))
            {
                query.setWhereClause("UHasActiveModel = true");
            }
            else if (compType.equalsIgnoreCase("securityTemplate"))
            {
                query.setWhereClause(" (UDisplayMode = 'playbook' and UIsTemplate is true) ");
            }
            else if (compType.equalsIgnoreCase(ImpexEnum.DECISIONTREE.getValue()))
            {
                query.setWhereClause(" (UIsRoot is true and UDisplayMode = 'decisiontree') ");
            }
            else if (compType.equalsIgnoreCase("wiki"))
            {
                query.setWhereClause(" (UDisplayMode = 'wiki') ");
            }
            query.setPage(1);
            query.setCustomOperator(QueryDTO.AND_OPERATOR);
            query.setSelectColumns("sys_id,UFullname,UName,UNamespace,USummary,sysUpdatedOn");
            String[] columns = {"sys_id","UFullname","UName","UNamespace","USummary","sysUpdatedOn"};
            List<WikiDocumentVO> list = new ArrayList<>();
            List rawList = GeneralHibernateUtil.executeHQLSelectModel(query, query.getStart(), query.getLimit(), false);
            
            rawList.parallelStream().forEach(o -> {
                Object[] record = (Object[]) o;
                WikiDocumentVO instance = new WikiDocumentVO();
                boolean hasChildren = ExportUtils.doesCompHasAChild(compType, (String)record[0], userName);
                instance.setHasChildren(hasChildren);
                //set the attributes in the object
                for (int x = 0; x < columns.length; x++)
                {
                    String column = columns[x].trim();
                    Object value = record[x];
                    try
                    {
                        PropertyUtils.setProperty(instance, column, value);
                    } 
                    catch (Exception e)
                    {
                        // there should never be an exception. But just in case...
                        Log.log.error(e.getMessage(), e);
                    }
                }
                list.add(instance);
            });
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(list);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("wikiNamespace"))
        {
            query.setModelName("WikiDocument");

            List<? extends Object> resultList = null;
            List<WikiDocumentVO> WikiDocumentVOList = new ArrayList<WikiDocumentVO>();
            int pageSize = query.getLimit();
            int start = query.getStart();

            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT DISTINCT (UNamespace) from WikiDocument ");

            List<QueryFilter> qFilterList = QueryFilter.decodeJson(query.getFilter());
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            if (qFilterList.size() > 0)
            {
                sql.append(" where ");
                for (QueryFilter filter : qFilterList)
                {
                    QueryParameter l = filter.getParameters().get(0);
                    String params = filter.getField();

                    //sql.append("LOWER (").append(params).append(") ").append(filter.getComp()).append(" '").append(l.getValue()).append("' and ");
                    sql.append("LOWER (").append(params).append(") ").append(filter.getComp()).append(" :").append(params).append(" and ");
                    queryParams.put(params, l.getValue());
                    
                    filter.getParameters();
                    Log.log.trace(sql);
                }
                sql.replace(sql.length() - 4, sql.length(), "");
                start = -1;
                pageSize = 0;
            }

            sql.append(" order by UNamespace ");
            if (StringUtils.isNotBlank(query.getSort()))
            {
                try
                {
                    String sort = query.getSort().substring(1, query.getSort().length() - 1);
                    Map<String,String> descMap = new ObjectMapper().readValue(sort, HashMap.class);
                    
                    List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
                    
                    // Validate direction against order by functions black list
                    
                    if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
                        dbOrderbyFunctionsBlackList.contains(descMap.get("direction").toLowerCase()))
                    {
                        throw new RuntimeException("Unsupported function in order by clause.");
                    }
                    
                    sql.append(descMap.get("direction"));
                }
                catch (Exception e)
                {
                    Log.log.warn("Error " + e.getLocalizedMessage() + " occurred in  parsing Sort direction, setting it to DESC.");
                    sql.append(" DESC ");
                }
            }
            else
            {
                sql.append(" DESC ");
            }
            
            try
            {
                //resultList = GeneralHibernateUtil.executeHQLSelect(sql.toString(), start, pageSize);
                resultList = GeneralHibernateUtil.executeHQLSelect(sql.toString(), queryParams);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }

            if (resultList != null)
            {
                int i = (start-1) * pageSize ;
                for (Object obj : resultList)
                {
                    WikiDocumentVO actionTaskVO = new WikiDocumentVO();
                    actionTaskVO.setId(i++ + "");
                    actionTaskVO.setUNamespace((String)obj);
                    WikiDocumentVOList.add(actionTaskVO);
                }
            }

            result.setSuccess(true).setRecords(WikiDocumentVOList);
            query.setSelectColumns("sys_id");
            String countSql = "SELECT DISTINCT(UNamespace) AS COUNT from WikiDocument";
            List<? extends Object> countList = GeneralHibernateUtil.executeHQLSelect(countSql, -1, 0);
            result.setTotal(countList.size());
        }
        else if (compType.equalsIgnoreCase("properties"))
        {
            query.setModelName("ResolveProperties");

            if (query.getSortItems() != null && query.getSortItems().size() > 0)
            {
                List<QuerySort> sortItems = query.getSortItems();
                SortOrder sortDirection = sortItems.get(0).getDirection();
                query.addSortItem(new QuerySort("sys_id", sortDirection));
            }
            
            query.setSelectColumns("sys_id,UName,UModule,UValue,sysUpdatedOn");

            List<ResolvePropertiesVO> list = ServiceHibernate.getResolveProperties(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(list);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("tag"))
        {
            //will be of type Tag
        	if (StringUtils.isNotBlank(query.getSort()))
        	{
        		String sort = query.getSort().replace("name", "UName");
        		query.setSort(sort);
        	}
        	if (StringUtils.isNotBlank(query.getFilter()))
            {
                query.setFilter(query.getFilter().replaceAll("name", "UName"));
            }
            result = ServiceTag.listTags(query, userName);
        }
        else if (compType.equalsIgnoreCase("catalog"))
        {
            //will be of type Tag
        	if (StringUtils.isNotBlank(query.getSort()))
            {
                query.setSort(query.getSort().replaceAll("name", "UName"));
            }
        	if (StringUtils.isNotBlank(query.getFilter()))
            {
                query.setFilter(query.getFilter().replaceAll("name", "UName"));
            }
            List<? extends Object> list = ServiceCatalog.getAllCatalogs(query, userName);
//            for (Object o : list)
//            {
//                ResolveCatalog cat = (ResolveCatalog)o;
//                cat.findAllDocumentNames();
//            }
//            if(list != null)
//            {
//                result.setTotal(list.size());
//            }
            result.setRecords(list);
            result.setSuccess(true);

        }
        else if (compType.equalsIgnoreCase("wikiLookup"))
        {
            query.setPage(1);
            query.setModelName("ResolveWikiLookup");

            List<ResolveWikiLookupVO> list = ServiceHibernate.getResolveWikiLookup(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("scheduler"))
        {
            query.setModelName("ResolveCron");

            List<ResolveCronVO> list = ServiceHibernate.getResolveCronJobs(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("metricThreshold") || compType.equalsIgnoreCase("mtnamespace"))
        {
        	if (compType.equalsIgnoreCase("mtnamespace"))
        	{
        		query.setSelectColumns(" distinct (URuleModule) ");
        	}
        	query.setModelName("MetricThreshold");
        	query.setWhereClause(" URuleName is not null ");
            Map<String, Object> resultMap = ServiceHibernate.getUIEditableMetricThresholds(query);
            List<MetricThresholdVO> data = (List<MetricThresholdVO>)resultMap.get("UNIQUE_THRESHOLDS");
            
            int total = data==null ? 0 : (Integer)(resultMap.get("UNIQUE_THRESHOLDS_TOTAL"));
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("menuSet"))
        {
            query.setModelName("SysPerspective");
            query.setSelectColumns("sys_id,name");

            List<SysPerspectiveVO> menuSetList = MenuSetUtil.getMenuSets(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(menuSetList);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("menuDefinition"))
        {
            query.setModelName("SysAppApplication");
            query.setSelectColumns("sys_id,name,title");

            List<SysAppApplicationVO> menuDefList = ServiceHibernate.getMenuDefinitions(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(menuDefList);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("menuItem"))
        {
            query.setSelectColumns("sys_id");
            int limit = query.getLimit();
            String sort = query.getSort();
            
            query.setLimit(-1);
            query.setSort("");
            List<SysAppModuleVO> countList = ServiceHibernate.getMenuItems(query, userName);
            
            query.setLimit(limit);
            query.setSort(sort);
            query.setSelectColumns("sys_id,name,title,appModuleGroup");
            List<SysAppModuleVO> menuItemList = ServiceHibernate.getMenuItems(query, userName);
            
            int total = countList.size();
            
            //long total = ServiceHibernate.getTotalRecordsIn("SysAppModule am, SysAppApplication saa", " am.sysAppApplication = saa.sys_id ");
            result.setRecords(menuItemList);
            result.setSuccess(true);
            result.setTotal((int)total);
        }
        else if (compType.equalsIgnoreCase("process") || compType.equalsIgnoreCase("team")
                        || compType.equalsIgnoreCase("rss") || compType.equalsIgnoreCase("forum"))
        {
//            query.setPage(1);
            if (compType.equalsIgnoreCase("process"))
            {
                query.setModelName("social_process");
            }
            else if (compType.equalsIgnoreCase("team"))
            {
                query.setModelName("social_team");
            }
            else if (compType.equalsIgnoreCase("forum"))
            {
                query.setModelName("social_forum");
            }
            else if (compType.equalsIgnoreCase("rss"))
            {
                query.setModelName("rss_subscription");
            }

            Map<String, Object> socialMap= ServiceHibernate.getSocialComponents(query, userName);
            List<SocialDTO> resultList = (List<SocialDTO>)socialMap.get("DATA");
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(resultList);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("user"))
        {
            query.setModelName("Users");
            query.setSelectColumns("sys_id,UUserName,UFirstName,ULastName");

            List<UsersVO> list = ServiceHibernate.getUsers(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("role"))
        {
            query.setModelName("Roles");
            query.setSelectColumns("sys_id,UName,UDescription");

            List<RolesVO> list = ServiceHibernate.getRoles(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("group"))
        {
            query.setModelName("Groups");
            query.setSelectColumns("sys_id,UName,UDescription");

            List<GroupsVO> list = ServiceHibernate.getGroups(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("customTable"))
        {
            query.setModelName("CustomTable");
            query.setSelectColumns("sys_id,UName,UModelName,sysUpdatedOn");
            int limit = query.getLimit();
            int offset = query.getStart();
            if (StringUtils.isBlank(query.getSort()))
            {
                query.setSort("[{\"property\":\"UName\",\"direction\":\"DESC\"}]");
            }
            QueryFilter filter = new QueryFilter();
            filter.setField("UName");
            filter.setType("auto");
            filter.setCondition(QueryFilter.NOT_ENDS_WITH);
            filter.setValue("_fu");
            query.getFilters().add(filter);
            query.setOperator(" AND ");
            
            List<CustomTableVO> list = new ArrayList<CustomTableVO>(); 
            try
            {
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
                if (data != null && data.size() > 0)
                {
                    for (Object o : data)
                    {
                        Object[] objArray = (Object[])o;
                        CustomTableVO customTableVO = new CustomTableVO();
                        customTableVO.setSys_id((String)objArray[0]);
                        customTableVO.setId((String)objArray[0]);
                        customTableVO.setUName((String)objArray[1]);
                        customTableVO.setUModelName((String)objArray[2]);
                        customTableVO.setSysUpdatedOn((Date)objArray[3]);
                        list.add(customTableVO);
                    }
                }
            }
            catch(Exception e)
            {
                Log.log.error("Could not retrieve Custom Tables", e);
            }
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setTotal(total);
            result.setRecords(list);
            result.setSuccess(true);
        }
        else if (compType.equalsIgnoreCase("form"))
        {
            query.setModelName("MetaFormView");
            query.setSelectColumns("sys_id,UFormName,UDisplayName,sysUpdatedOn");
            int limit = query.getLimit();
            int offset = query.getStart();
            if (StringUtils.isBlank(query.getSort()))
            {
                query.setSort("[{\"property\":\"UFormName\",\"direction\":\"DESC\"}]");
            }
            
            List<MetaFormViewVO> list = new ArrayList<MetaFormViewVO>();
            try
            {
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
                if (CollectionUtils.isNotEmpty(data))
                {
                    data.parallelStream().forEach(o -> {
                        Object[] objArray = (Object[])o;
                        MetaFormViewVO metaFormViewVO = new MetaFormViewVO();
                        metaFormViewVO.setSys_id((String)objArray[0]);
                        metaFormViewVO.setId((String)objArray[0]);
                        metaFormViewVO.setUFormName((String)objArray[1]);
                        metaFormViewVO.setUDisplayName((String)objArray[2]);
                        metaFormViewVO.setSysUpdatedOn((Date)objArray[3]);
                        try {
                            ExtMetaFormLookup lookup = new ExtMetaFormLookup(metaFormViewVO.getSys_id(), null, null, userName, RightTypeEnum.admin, null);
                            lookup.setFormDefinitionOnly(false);
                            boolean hasChildren = lookup.doesFormHasChildren();
                            metaFormViewVO.setHasChildren(hasChildren);
                        } catch(Exception e) {
                            
                        }
                        list.add(metaFormViewVO);
                    });
                }
            }
            catch(Exception e)
            {
                Log.log.error("Could not retrieve Custom Forms", e);
            }
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setTotal(total);
            result.setRecords(list);
            result.setSuccess(true);
        }
        else if (compType.equalsIgnoreCase("businessRule"))
        {
            query.setModelName("ResolveBusinessRule");
            query.setSelectColumns("sys_id,UName");

            List<ResolveBusinessRuleVO> list = ServiceHibernate.getResolveBusinessRules(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("systemScript"))
        {
            query.setModelName("ResolveSysScript");
            query.setSelectColumns("sys_id,UName,UDescription");

            List<ResolveSysScriptVO> list = ServiceHibernate.getResolveSysScripts(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("systemProperty"))
        {
            query.setModelName("Properties");
            query.setSelectColumns("sys_id,UName,UDescription");

            List<PropertiesVO> list = ServiceHibernate.getProperties(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("wikitemplate"))
        {
            query.setModelName("WikiDocumentMetaFormRel");
            query.setSelectColumns("sys_id,UName,UWikiDocName,UDescription,sysUpdatedOn");
            
            List<WikiDocumentMetaFormRelVO> list = ServiceHibernate.getWikiTemplate(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setRecords(list);
            result.setSuccess(true);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("worksheet"))
        {
            /**
             * TODO
             * Pending Implementation! This will be implemented in 3.6
             */
        }
        else if (compType.equalsIgnoreCase("automationbuilder"))
        {
            query.setModelName("WikiDocument");
            
            //query.setPage(1);
            //query.setSelectColumns("sys_id,UFullname,USummary");
            query.setWhereClause("UHasResolutionBuilder = true");
            List<? extends Object> list = WikiUtils.getWikiDocuments(query, null, userName);
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(list);
            result.setTotal(total);
        }
        else if (compType.endsWith("Filter") || compType.equals("SSHPool"))
        {
            try
            {
                String className = "com.resolve.persistence.model." + compType;
                Class.forName(ESAPI.validator().getValidInput("Resolve Model", className, "ResolveModel", 512, false));
                
                query.setModelName(compType);
                query.setWhereClause("UQueue = '<|Default|Queue|>'");
                List<GatewayVO> list = ServiceGateway.getAllFilter(query, userName);

                result.setRecords(list);
                result.setSuccess(true);
                result.setTotal(list.size());
            } catch(ClassNotFoundException e) {
                int index = compType.indexOf("Filter");
                
                if(index != -1) {
                    List<GatewayVO> records = ServiceGateway.listFilters(compType.substring(0, index), "<|Default|Queue|>", null);

                    result.setRecords(records);
                    result.setTotal(records.size());
                    result.setSuccess(true);
                }
                
                else {
                    Log.log.warn("compType does not contain 'Filter'.");
                    result.setSuccess(false);
                    return result;
                }
            }
        }
        else if (compType.equalsIgnoreCase("ridmapping"))
        {
            query.setModelName("RRRule");
            query.setOperator("AND");
            List<RRRuleVO> records = ServiceResolutionRouting.listRules(query,userName);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(records);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("rrmodule"))
        {
            query.setModelName("RRRule");
            query.setOperator("AND");
            List<String> modules = ServiceResolutionRouting.listRuleModules(query,userName);
            List<Map<String, String>> records = new ArrayList<Map<String, String>>();
            int total = 0;
            if (modules != null && modules.size() > 0)
            {
                for (String module: modules)
                {
                    Map<String, String> record = new HashMap<String,String>();
                    record.put("module", module);
                    records.add(record);
                }
                query.setHqlDistinct(true);
                query.setSelectColumns("module");
                total = ServiceHibernate.getTotalHqlCount(query);
            }
                result.setSuccess(true).setRecords(records);
                result.setTotal(total);
            }
        else if (compType.equalsIgnoreCase("rrschema"))
        {
            query.setModelName("RRSchema");
            query.setOperator("AND");
            List<RRSchemaVO> records = ServiceResolutionRouting.listSchema(query, userName);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(records);
            result.setTotal(total);
        }
        else if (compType.equalsIgnoreCase("artifacttype"))
        {
            List<ArtifactTypeVO> data = ArtifactTypeUtil.getAllArtifactTypeNoRef(query, userName);
            result.setSuccess(true).setRecords(data);
            result.setTotal(data.size());
        }
        else if (compType.equalsIgnoreCase("gateway"))
        {
            List<Map<String, String>> gateways = ServiceGateway.getGatewayListForExport(query, userName);
            result.setSuccess(true).setRecords(gateways);
            result.setTotal(gateways.size());
        }
        return result;
    }

    public static ResolveImpexModule getResolveImpexModule(String moduleName, Boolean isDirty, String userName)
    {
        ResolveImpexModule impexModel = null;

        impexModel = new ResolveImpexModule();
        impexModel.setUName(moduleName);
        if (isDirty != null && isDirty)
        {
            impexModel.setUDirty(isDirty);
        }
        
        ResolveImpexModule impexModelFinal = impexModel;
        
        try
        {
          HibernateProxy.setCurrentUser(userName);
            impexModel = (ResolveImpexModule) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().findFirst(impexModelFinal);
            });
        }
        catch (Exception e)
        {
        	HibernateUtil.rethrowNestedTransaction(e);
        }
        return impexModel;
    }


    public static void extractFile (String folderLocation, String destinationFolder, String zipFileName, String partialFileName) throws Exception
    {
        try
        {
            //delete the destination folder if it exist
            File moduleFolder = FileUtils.getFile(folderLocation);
            if(moduleFolder != null && moduleFolder.isDirectory() && moduleFolder.exists())
            {
                Log.log.info("Deleting  existing folder -->" + moduleFolder.getName());
                FileUtils.deleteDirectory(moduleFolder);
            }
            FileUtils.extractFile(FileUtils.getFile(zipFileName), FileUtils.getFile(destinationFolder), partialFileName);
        }
        catch(Exception e)
        {
            Log.log.error("Error: Extracting file :"+ partialFileName, e);
            ImpexUtil.logImpexMessage(e.getMessage(), true);
            throw new Exception(e);
        }
    }

    public static Document fromXml(String xml) throws DocumentException, SAXException
    {
        StringReader in = new StringReader(xml);
        SAXReader reader = new SAXReader();
        reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        Document domdoc = reader.read(in);

        return domdoc;
    }//fromXml
    
    public static String getElement(Element docel, String name)
    {
        Element el = docel.element(name);
        if (el == null)
            return "";
        else
            return el.getText().trim();
    }//getElement

    
    public static XDoc convertFileToXDoc(File importFile) throws Exception
    {
        String xml = FileUtils.readFileToString(importFile, ImpexUtil.UTF8);
        xml = clearCData(xml);
        
        XDoc doc = new XDoc(xml);
        doc.removeResolveNamespaces();

        return doc;
    }//convertFileToXDoc
    
    public static String clearCData(String xml)
    {
        /*
         * This piece of code would remove any u_value and u_script text from XML.
         * This text was causing XML parser issue.
         */
        for (String column : ImpexUtil.cDataColumnList)
        {
            // If column is <u_script>, closingElem will be </u_script>
            String closingElem = new StringBuilder(column).insert(1, "/").toString();
            
            String columnText = StringUtils.substringBetween(xml, column, closingElem);
            if (columnText != null)
            {
                if (columnText.contains("CDATA"))
                {
                    int startIndex = xml.indexOf(column);
                    int lastIndex = xml.lastIndexOf(closingElem);
                    columnText = xml.substring(startIndex+column.length(), lastIndex).trim();
                    StringBuilder sb = new StringBuilder(xml);
                    xml = sb.replace(startIndex+column.length(), lastIndex, "").toString();
                }
            }
        }
        
        return xml;
    }
    
    public static Set<String> getTaskNamesReferedFromWiki(String docSysId) throws Exception
    {
        Set<String> taskFullNameSet = new HashSet<String>();
        
        String tableName = "ContentWikidocActiontaskRel";
        String whereClause = "contentWikidoc = '" + docSysId + "'";
        String selectColumns = "UActiontaskFullname";
        
        List <? extends Object> data = executeQuery(tableName, whereClause, selectColumns );
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                taskFullNameSet.add((String)o);
            }
        }
        
        tableName = "MainWikidocActiontaskRel";
        whereClause = "mainWikidoc = '" + docSysId + "'";
        selectColumns = "UActiontaskFullname";
        
        data = executeQuery(tableName, whereClause, selectColumns );
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                taskFullNameSet.add((String)o);
            }
        }
        
        tableName = "ExceptionWikidocActiontaskRel";
        whereClause = "exceptionWikidoc = '" + docSysId + "'";
        selectColumns = "UActiontaskFullname";
        
        data = executeQuery(tableName, whereClause, selectColumns );
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                taskFullNameSet.add((String)o);
            }
        }
        
        return taskFullNameSet;
    }
    
    public static List<? extends Object> executeQuery(String tableName, String whereClause, String selectColumns) throws Exception
    {
        QueryDTO query = new QueryDTO();
        
        query.setModelName(tableName);
        query.setWhereClause(whereClause);
        query.setSelectColumns(selectColumns);
        
        return GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
    }
    
    @SuppressWarnings("unchecked")
    public static List<ResolveImpexManifestVO> readManifest(QueryDTO query, String moduleName, String operation) throws Exception
    {
        List<ResolveImpexManifestVO> manifestVoList = new ArrayList<ResolveImpexManifestVO>();
        query.setModelName("ResolveImpexManifest");
        query.setWhereClause("UOperationType = '" + operation + "' and UModuleName = '" + moduleName + "'");
        
        query.addSortItem(new QuerySort("UDisplayType", QuerySort.SortOrder.ASC));
        query.addSortItem(new QuerySort("sys_id", QuerySort.SortOrder.ASC));
        
        List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectModel(query, query.getStart(), query.getLimit(), true);
        if (list != null)
        {
            List<ResolveImpexManifest> localManifestList = (List<ResolveImpexManifest>) list;
            for (ResolveImpexManifest manifest : localManifestList)
            {
                manifestVoList.add(manifest.doGetVO());
            }
        }
        
        return manifestVoList;
    }
    
    public static void populateResolveManifestModel(List<ImpexComponentDTO> compDTOList, String moduleName, String operation, String userName)
    {
        if (compDTOList != null)
        {
            for (ImpexComponentDTO dto : compDTOList)
            {
                ResolveImpexManifestVO vo = new ResolveImpexManifestVO();
                vo.setUCompSysId(dto.getSys_id());
                vo.setUTableName(dto.getTablename());
                vo.setUDisplayType(dto.getDisplayType());
                vo.setUModule(dto.getModule());
                vo.setUFullName(dto.getFullName());
                vo.setUFileName(dto.getFilename());
                vo.setUStatus(dto.getStatus());
                vo.setUName(dto.getName());
                vo.setUOperationType(operation);
                vo.setUOperation(dto.getOperation());
                vo.setUModuleName(moduleName);
                
                ImpexManifestUtil.saveManifest(vo, userName);
            }
        }
    }

    /*
     * New implementation end
     */
    public static boolean createExport(String modulename, String description)
    {
        boolean result = false;
        if ("ALL".equalsIgnoreCase(modulename))
        {
            Log.log.error("Impex Module name 'ALL' is a reserved impex name, cannot be created");
        }
        else if (StringUtils.isNotEmpty(modulename))
        {
            Log.log.debug("Creating Impex Module " + modulename);

            ResolveImpexModuleVO rim = new ResolveImpexModuleVO();
            rim.setUName(modulename);
            if (StringUtils.isNotEmpty(description))
            {
                rim.setUDescription(description);
            }
            rim.setSysCreatedBy("system");
            rim.setSysUpdatedBy("system");

            try
            {
                saveResolveImpexModule(rim, "system", true, "EXPORT");
                result = true;
            }
            catch (Throwable t)
            {
                result = false;
                Log.log.error("Failed to Create Export Module", t);
            }
        }
        else
        {
            Log.log.error("No Impex Module Name Given");
        }

        return result;
    } // createExport

    @SuppressWarnings("rawtypes")
    public static boolean addRunbooksToExport(String modulename, List items, String type, boolean scan)
    {
        boolean result = false;
        Log.log.debug("Adding " + type + ": " + items + " to module " + modulename);
        if (StringUtils.isNotEmpty(modulename) && items != null && StringUtils.isNotEmpty(type))
        {
            try
            {
                HibernateProxy.execute(() -> {
                	ResolveImpexModule rim = new ResolveImpexModule();
                	rim.setUName(modulename);
	                rim = HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().findFirst(rim);
	                if (rim != null)
	                {
	                    // for each docname passed, add a resolve_impex_wiki entry
	                    // to the resolve_impex_module
	                    if (items != null)
	                    {
	                        for (Object item : items)
	                        {
	                            ResolveImpexWiki riw = createImpexWikiDefinition(item.toString(), type, scan);
	                            riw.setResolveImpexModule(rim);
	
	                            //save it
	                            SaveUtil.saveResolveImpexWiki(riw, null);
	                        }
	                    }
	
	                    //save it
	                    SaveUtil.saveResolveImpexModule(rim, null);
	                    return true;
	                }
	                else
	                {
	                    Log.log.error("Unable to Find Module " + modulename + " To add Documents To");
	                    return false;
	                }
                });
            }
            catch (Throwable t)
            {
                Log.log.error("Failed to Add Runbook Type To Export Module", t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        return result;
    }// addRunbooksToExport

    @SuppressWarnings("rawtypes")
    public static boolean addMenuSectionsToExport(String modulename, String menuSection, List menuItems)
    {
        boolean result = false;
        if (StringUtils.isEmpty(modulename))
        {
            Log.log.warn("No Export Module Name Passed to Add Menu Sections to");
        }
        else if (StringUtils.isNotEmpty(menuSection))
        {
            try
            {
                result = (boolean) HibernateProxy.execute(() -> {
	            	boolean internalResult = false;
	
	            	Log.log.debug("Attempting to Add Menu Section: " + menuSection);
	            	ResolveImpexModule rim = new ResolveImpexModule();
	            	rim.setUName(modulename);
	                rim = HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().findFirst(rim);
	                if (rim != null)
	                {
	                    Log.log.debug("Looking Up Section App");
	                    // look up menu section title passed, wiki glide impex will
	                    // require the name, which is not visible from the UI
	                    SysAppApplication sectionApp = new SysAppApplication();
	                    sectionApp.setTitle(menuSection);
	                    sectionApp = HibernateUtil.getDAOFactory().getSysAppApplicationDAO().findFirst(sectionApp);
	
	                    if (sectionApp != null)
	                    {
	                        // add the menu section
	                        ResolveImpexGlide rig = createImpexGlideDefinition(sectionApp.getName(), Constants.IMPEX_PARAM_MENUSECTION);
	                        rig.setResolveImpexModule(rim);
	                        rig.setUScan(false);
	
	                        //save it
	                        SaveUtil.saveResolveImpexGlide(rig, null);
	
	                        internalResult = true;
	
	                        // get all menu items for the menu section
	                        Log.log.debug("Get Menu Section Items");
	                        Collection<SysAppModule> sectionItems = sectionApp.getSysAppModules();
	
	                        // if specific names were passed, compare to the
	                        // existing items, and add validated items
	                        if (menuItems != null && menuItems.size() > 0)
	                        {
	                            // add each item to the map with key=title, and
	                            // value=title#group
	                            Map<String, String> menuItemMap = new HashMap<String, String>();
	                            for (SysAppModule sectionItem : sectionItems)
	                            {
	                                String item = sectionItem.getTitle();
	                                if (StringUtils.isNotEmpty(sectionItem.getAppModuleGroup()))
	                                {
	                                    item = sectionItem.getAppModuleGroup() + "#" + item;
	                                }
	                                menuItemMap.put(sectionItem.getTitle(), item);
	                            }
	
	                            // for each passed menu item, find if it exists
	                            // based on the title, then add (module=title,
	                            // name=group) impex definition
	                            for (Object menuItem : menuItems)
	                            {
	                                Log.log.debug("Attempting to add specific Menu Item: " + menuItem);
	                                if (menuItemMap.containsKey(menuItem))
	                                {
	                                    Log.log.debug("Specific Menu Item Found and Added: " + menuItem);
	                                    ResolveImpexGlide rigItem = createImpexGlideDefinition(menuItemMap.get(menuItem), Constants.IMPEX_PARAM_MENUITEM);
	                                    rigItem.setResolveImpexModule(rim);
	
	                                    //save it
	                                    SaveUtil.saveResolveImpexGlide(rigItem, null);
	                                }
	                                else
	                                {
	                                    Log.log.debug("Unable to Add Menu Item " + menuItem + ", as It cannot Be found in Menu Section " + menuSection);
	                                    internalResult = false;
	                                }
	                            }
	                        }
	                        else
	                        {
	                            // if no specific items are passed, add all menu
	                            // items: module=title, name=group
	                            for (SysAppModule sectionItem : sectionItems)
	                            {
	                                Log.log.debug("Adding Menu Item: " + sectionItem.getTitle());
	                                String item = sectionItem.getTitle();
	                                if (StringUtils.isNotEmpty(sectionItem.getAppModuleGroup()))
	                                {
	                                    item = sectionItem.getAppModuleGroup() + "#" + item;
	                                }
	                                ResolveImpexGlide rigItem = createImpexGlideDefinition(item, Constants.IMPEX_PARAM_MENUITEM);
	                                rigItem.setResolveImpexModule(rim);
	
	                                //save it
	                                SaveUtil.saveResolveImpexGlide(rigItem, null);
	                            }
	                        }
	                    }
	                    else
	                    {
	                        Log.log.warn("Section " + menuSection + " Not Found, Unable to add definition to module");
	                    }
	                }
	                else
	                {
	                    // no export
	                    Log.log.error("Unable to Find Module " + modulename + " To Add Components To");
	                }
	                
	                return internalResult;
                });
            }
            catch (Exception t)
            {
                Log.log.error("Failed to Create Export Module", t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        else
        {
            Log.log.warn("No Menu Sections Passed to Add");
        }

        return result;
    }// addMenuSectionsToExport

    @SuppressWarnings("rawtypes")
    public static boolean addComponentsToExport(String modulename, List items, String type)
    {
        boolean result = false;
        Log.log.debug("Adding " + type + ": " + items + " to module " + modulename);
        if (StringUtils.isNotEmpty(modulename) && items != null && StringUtils.isNotEmpty(type))
        {
            
            try
            {
				result = (boolean) HibernateProxy.execute(() -> {
					ResolveImpexModule rim = new ResolveImpexModule();
		            rim.setUName(modulename);
					rim = HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().findFirst(rim);
					if (rim != null) {
						// for each docname passed, add a resolve_impex_glide entry
						// to the resolve_impex_module
						if (items != null && items.size() > 0) {
							for (Object item : items) {
								ResolveImpexGlide rig = createImpexGlideDefinition(item.toString(), type);
								if (Constants.IMPEX_PARAM_MENUSET.equals(type)
										|| Constants.IMPEX_PARAM_METAFORMVIEW.equals(type)) {
									rig.setUModule(item.toString());
									rig.setUName(null);
								}
								rig.setResolveImpexModule(rim);

								// save it
								SaveUtil.saveResolveImpexGlide(rig, null);
							}
						}

						// save it
						SaveUtil.saveResolveImpexModule(rim, null);
						return true;
					} else {
						Log.log.error("Unable to Find Module " + modulename + " To add Components To");
						return false;
					}
				});
            }
            catch (Throwable t)
            {
                result = false;
                Log.log.error("Failed to Add Component Type To Export Module", t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        return result;
    }// addComponentsoExport

	public static ResolveImpexModuleVO addComponentsToModule(String moduleSysId, String moduleName,
			List<ResolveImpexWikiVO> wikiItems, List<ResolveImpexGlideVO> compItems, String username) {
		ResolveImpexModuleVO moduleVO = null;
		ResolveImpexModule model = null;
		try {
        HibernateProxy.setCurrentUser(username);
			model = (ResolveImpexModule) HibernateProxy.execute(() -> {
				List<ResolveImpexWiki> wikiItemsModels = null;
				if (wikiItems != null) {
					wikiItemsModels = new ArrayList<ResolveImpexWiki>();
					for (ResolveImpexWikiVO vo : wikiItems) {
						wikiItemsModels.add(new ResolveImpexWiki(vo));
					}
				}

				List<ResolveImpexGlide> compItemsModels = null;
				if (compItems != null) {
					compItemsModels = new ArrayList<ResolveImpexGlide>();
					for (ResolveImpexGlideVO vo : compItems) {
						compItemsModels.add(new ResolveImpexGlide(vo));
					}
				}

				ResolveImpexModule searchedModel = getImpexModel(moduleSysId, moduleName, false);
				if (searchedModel != null) {
					if (wikiItemsModels != null) {
						for (ResolveImpexWiki wiki : wikiItemsModels) {
							wiki.setResolveImpexModule(searchedModel);
							SaveUtil.saveResolveImpexWiki(wiki, username);
						}
					}

					if (compItemsModels != null) {
						for (ResolveImpexGlide glide : compItemsModels) {
							glide.setResolveImpexModule(searchedModel);
							SaveUtil.saveResolveImpexGlide(glide, username);
						}
					}
				} // end of if
				return searchedModel;

			});
		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (model != null) {
			moduleVO = model.doGetVO();
		}

		return moduleVO;
	}

    public static String getAllModuleNames()
    {
        try
        {
            return (String) HibernateProxy.execute(() -> {
            	String result = "";
            	List<ResolveImpexModule> modules = HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().find();
                for (ResolveImpexModule module : modules)
                {
                    result += module.getUName() + ",";
                }
                
                return result;
            });          

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
            return "";
        }
    } // getAllModuleNames

    public static List<ResolveImpexModuleVO> getImpexRecords(int offset, int limit, String sortField, String sortDir)
    {
        List<ResolveImpexModuleVO> results = new ArrayList<ResolveImpexModuleVO>();
        String orderByName = getOrderByColumnName(sortField);
        String sql = "select sys_id, UName, UZipFileName, sysUpdatedBy, sysUpdatedOn from ResolveImpexModule where (UDirty = false OR UDirty is null) ";
        if (orderByName != null && orderByName.trim().length() > 0)
        {
            sql = sql + " order by " + orderByName;

            if (sortDir.equals("DESC"))
            {
                sql = sql + " " + sortDir;
            }
        }
        else
        {
            sql = sql + " order by UName ";
        }

        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, offset, limit);
            if (list != null)
            {
                for (Object obj : list)
                {
                    Object[] values = (Object[]) obj;

                    ResolveImpexModuleVO model = new ResolveImpexModuleVO();
                    model.setSys_id((String) values[0]);
                    model.setUName((String) values[1]);
                    model.setUZipFileName((String) values[2]);
                    model.setSysUpdatedBy((String) values[3]);
                    model.setSysUpdatedOn((Date) values[4]);

                    results.add(model);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return results;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResolveImpexModule getImpexModelWithReferences(String sysId, String moduleName, String userName)
    {     

        try
        {
          HibernateProxy.setCurrentUser(userName);
        	return (ResolveImpexModule) HibernateProxy.executeNoCache(() -> {
        		ResolveImpexModule model = null;
                List<Object> objList = null;
                model = getImpexModel(sysId, moduleName, false);
                if (model != null)
                {
                    String hql = ImpexEnum.IMPEX_GLIDE_SELECT_SQL.getValue();
                    Query query = HibernateUtil.createQuery(hql);
                    query.setParameter("sysId", model.getSys_id());
                    objList = query.list();
                    
                    if (objList != null && objList.size() > 0)
                    {
                        List<ResolveImpexGlide> glideList = new ArrayList<ResolveImpexGlide>();
                        for (Object o : objList)
                        {
                            Object[] objArray = (Object[])o;
                            
                            ResolveImpexGlide glide = new ResolveImpexGlide();
                            glide.setSys_id((String)objArray[0]);
                            glide.setUType((String)objArray[1]);
                            glide.setUModule((String)objArray[2]);
                            glide.setUName((String)objArray[3]);
                            glide.setUDescription((String)objArray[4]);
                            glide.setUOptions((String)objArray[5]);
                            glide.setUScan((Boolean)objArray[6]);
                            
                            glide.setSysCreatedOn((Date)objArray[7]);
                            glide.setSysCreatedBy((String)objArray[8]);
                            glide.setSysUpdatedOn((Date)objArray[9]);
                            glide.setSysUpdatedBy((String)objArray[10]);
                            
                            glideList.add(glide);
                        }
                        
                        model.setResolveImpexGlides(glideList);
                    }
                    
                    hql = ImpexEnum.IMPEX_WIKI_SELECT_SQL.getValue();
                    query = HibernateUtil.createQuery(hql);
                    query.setParameter("sysId", model.getSys_id());
                    objList = query.list();
                    
                    if (objList != null && objList.size() > 0)
                    {
                        List<ResolveImpexWiki> wikiList = new ArrayList<ResolveImpexWiki>();
                        for (Object o : objList)
                        {
                            Object[] objArray = (Object[])o;
                            
                            ResolveImpexWiki wiki = new ResolveImpexWiki();
                            wiki.setSys_id((String)objArray[0]);
                            wiki.setUType((String)objArray[1]);
                            wiki.setUValue((String)objArray[2]);
                            wiki.setUScan((Boolean)objArray[3]);
                            wiki.setUOptions((String)objArray[4]);
                            wiki.setUDescription((String)objArray[5]);
                            
                            wiki.setSysCreatedOn((Date)objArray[6]);
                            wiki.setSysCreatedBy((String)objArray[7]);
                            wiki.setSysUpdatedOn((Date)objArray[8]);
                            wiki.setSysUpdatedBy((String)objArray[9]);
                            
                            wikiList.add(wiki);
                        }
                        
                        model.setResolveImpexWikis(wikiList);
                    }
                }
                return model;
        	});
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
            return null;
        }
    }

    public static ResolveImpexModuleVO getImpexModelVOWithReferences(String sysId, String moduleName, String userName)
    {
        ResolveImpexModuleVO result = null;
        ResolveImpexModule model = getImpexModelWithReferences(sysId, moduleName, userName);
        if (model != null)
        {
            result = model.doGetVO();
        }

        return result;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResolveImpexModuleVO getImpexModelZipContent(String sysId, String moduleName, String userName)
    {
        ResolveImpexModuleVO model = null;
        
        String hql = "select sys_id, UName, UZipFileContent from ResolveImpexModule ";
        List<Object> temp = null;
        
        if (StringUtils.isNotBlank(sysId))
        {
            hql = hql + " where sys_id = '" + sysId + "'";
        }
        else if (StringUtils.isNotBlank(moduleName))
        {
            hql = hql + "where lower(UName) = '" + moduleName.toLowerCase() + "' ";
        }
        
        String hqlFinal = hql;
        try
        {
            temp = (List<Object>) HibernateProxy.execute(() -> {
            	Query query = HibernateUtil.createQuery(hqlFinal);
                return query.list();
            });
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        if (temp != null && temp.size() > 0)
        {
            Object[] objArray = (Object[])temp.get(0);
            model = new ResolveImpexModuleVO();
            model.setSys_id((String) objArray[0]);
            model.setUName((String) objArray[1]);
            model.setUZipFileContent((byte[])objArray[2]);
        }
        
        return model;
    }

    public static ResolveImpexModuleVO getImpexModelWithDefinitions(String sysId, String userName) throws Exception
    {
        ResolveImpexModuleVO impexModelVO = getImpexModelVOWithReferences(sysId, null, userName);
        List<ImpexDefinitionDTO> definitionList = new ArrayList<ImpexDefinitionDTO>();

        if (impexModelVO.getResolveImpexGlides() != null)
        {
            for (ResolveImpexGlideVO glideVO : impexModelVO.getResolveImpexGlides())
            {
                ImpexDefinitionDTO definitionDTO = new ImpexDefinitionDTO();
                definitionDTO.setType(glideVO.getUType());
                definitionDTO.setSys_id(glideVO.getSys_id());
                
                if (StringUtils.isNotBlank(glideVO.getUModule()))
                {
                    definitionDTO.setNamespace(glideVO.getUModule());
                }
                else
                {
                    definitionDTO.setNamespace(glideVO.getUName());
                }
                definitionDTO.setName(glideVO.getUName());
                if (glideVO.getUOptions() != null)
                {
                    try
                    {
                        ImpexOptionsDTO optionsDTO = new ObjectMapper().readValue(glideVO.getUOptions(), ImpexOptionsDTO.class);
                        definitionDTO.setOptions(optionsDTO);
                    }
                    catch(Exception e)
                    {
                        Log.log.error("Could not parser options. Message: " + e.getMessage() + " So, applying default options.");
                        definitionDTO.setOptions(new ImpexOptionsDTO());
                    }
                }
                
                String description = glideVO.getUDescription();
                
                if (StringUtils.isNotBlank(description) && description.contains("{"))
                {
                    definitionDTO.setDescription(glideVO.getUDescription());
                }
                else
                {
                    StringBuilder desc = new StringBuilder();
                    
                    if (glideVO.getUType().equalsIgnoreCase("ACTIONTASK_BY_TAG"))
                    {
                        // we don't support this category from Resolve 5 onwards.
                        continue;
                    }
                    if (glideVO.getUType().contains("ACTIONTASK"))
                    {
                        if (StringUtils.isBlank(glideVO.getUName()))
                        {
                            definitionDTO.setType("actionTaskModule");
                            String namespace = glideVO.getUModule();
                            if (StringUtils.isBlank(namespace))
                            {
                                namespace = glideVO.getUActiontaskModule();
                            }
                            desc.append("{\"unamespace\":\"").append(namespace).append("\"");
                            desc.append(",\"usummary\":\"\"} ");
                        }
                        else
                        {
                            definitionDTO.setType("actionTask");
                            desc.append("{\"unamespace\":\"").append(glideVO.getUModule()).append("\"");
                            desc.append(",\"uname\":\"").append(glideVO.getUName()).append("\"} ");
                        }
                    }
                    
                    if (glideVO.getUType().equalsIgnoreCase("SOCIAL"))
                    {
                        definitionDTO.setType(glideVO.getUName().toLowerCase());
                        glideVO.setUName(null);
                        desc.append("{\"u_display_name\":\"").append(glideVO.getUModule()).append("\"");
                        desc.append(",\"u_description\":\"\"} ");
                    }
                    if (glideVO.getUType().equalsIgnoreCase("METAFORMVIEW"))
                    {
                        definitionDTO.setType("form");
                        desc.append("{\"uformName\":\"").append(glideVO.getUModule()).append("\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("DB"))
                    {
                        definitionDTO.setType("customDB");
                        desc.append("{\"utableName\":\"").append(glideVO.getUModule()).append("\"");
                        String query = StringUtils.isBlank(glideVO.getUName()) ? "" : glideVO.getUName();
                        desc.append(",\"uquery\":\"").append(query).append("\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("CUSTOMTABLE"))
                    {
                        definitionDTO.setType("customTable");
                        desc.append("{\"uname\":\"").append(glideVO.getUModule()).append("\"} ");
                        
                        if (glideVO.getUName().contains("DATA"))
                        {
                            ImpexOptionsDTO impexOptions = new ImpexOptionsDTO();
                            impexOptions.setTableData(true);
                            try
                            {
                                String impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
                                glideVO.setUOptions(impexOptionJson);
                            }
                            catch(Exception e)
                            {
                                Log.log.error("Could not parse default ImpexOption for ResolveImpexGlide", e);
                            }
                        }
                        glideVO.setUName(null);
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("TAG"))
                    {
                        String tagName = StringUtils.isBlank(glideVO.getUName()) ? "-Old Definition-" : glideVO.getUName();
                        desc.append("{\"name\":\"").append(tagName).append("\"} ");
                        definitionDTO.setType("tag");
                        if (desc.toString().contains("-Old Definition-"))
                        {
                            ImpexUtil.logImpexMessage("WARNING: Old Tag glide definition. SysId: "
                                            + glideVO.getSys_id() + ". Please recreate this definition.", true);
                        }
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("MENUSET"))
                    {
                        desc.append("{\"name\":\"").append(glideVO.getUModule()).append("\"} ");
                        definitionDTO.setType("menuSet");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("MENUSECTION"))
                    {
                        definitionDTO.setType("menuDefinition");
                        SysAppApplication app = ImpexUtil.findMenuDefinition(glideVO.getUModule(), userName);
                        if (app != null)
                        {
                            glideVO.setUName(app.getName());
                            glideVO.setUModule(app.getTitle());
                        }
                        else
                        {
                            glideVO.setUName(glideVO.getUModule());
                            glideVO.setUModule("MISSING");
                        }
                        desc.append("{\"title\":\"").append(glideVO.getUModule()).append("\"");
                        desc.append(",\"name\":\"").append(glideVO.getUName()).append("\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("MENUITEM"))
                    {
                        definitionDTO.setType("menuItem");
                        String name = glideVO.getUName();
                        if (StringUtils.isBlank(name) || name.equals("&nbsp"))
                        {
                            glideVO.setUName(null);
                        }
                        
                        String menuItemTitle = glideVO.getUModule();
                        if (menuItemTitle.contains("::"))
                        {
                            menuItemTitle = menuItemTitle.split("::")[1];
                        }
                        desc.append("{\"title\":\"").append(menuItemTitle).append("\"");
                        String menuDefTitle = ImpexUtil.formatMenuItemDef(glideVO.getUModule());
                        if (menuDefTitle != null)
                        {
                            String g = StringUtils.isBlank(glideVO.getUName()) ? "" : glideVO.getUName() + "::";
                            glideVO.setUName(g + menuDefTitle);
                        }
                        
                        desc.append(",\"name\":\"").append(glideVO.getUName()).append("\"} ");
                        glideVO.setUModule(menuItemTitle);
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("BUSINESS_RULE"))
                    {
                        desc.append("{\"uname\":\"").append(glideVO.getUModule()).append("\"");
                        definitionDTO.setType("businessRule");
                        desc.append(",\"udescription\":\"\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("SYSTEM_SCRIPT"))
                    {
                        desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
                        definitionDTO.setType("systemScript");
                        desc.append(",\"udescription\":\"\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("CRONJOB"))
                    {
                        desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
                        desc.append(",\"umodule\":\"").append(glideVO.getUModule()).append("\"} ");
                        
                        definitionDTO.setType("scheduler");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("PROPERTIES"))
                    {
                        definitionDTO.setType("properties");
                        if (StringUtils.isNotBlank(glideVO.getUModule()))
                        {
                            desc.append("{\"umodule\":\"").append(glideVO.getUModule()).append("\"");
                        }
                        else if (StringUtils.isNotBlank(glideVO.getUPropertyModule()))
                        {
                            desc.append("{\"umodule\":\"").append(glideVO.getUPropertyModule()).append("\"");
                        }
                        else
                        {
                            desc.append("{\"umodule\":\"").append("-UNKNOWN-").append("\"");
                        }
                        String name = StringUtils.isBlank(glideVO.getUName()) ? "" : glideVO.getUName();
                        desc.append(",\"uname\":\"").append(name).append("\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("WIKILOOKUP"))
                    {
                        definitionDTO.setType("wikiLookup");
                        desc.append("{\"uwiki\":\"").append("Old Definition-").append("\"");
                        String name = StringUtils.isBlank(glideVO.getUName()) ? "-Old Definition-" : glideVO.getUName();
                        desc.append(",\"uregex\":\"").append(name).append("\"} ");
                        ImpexUtil.logImpexMessage("WARNING: Old WikiLookup glide definition. SYS_ID: " + glideVO.getSys_id() + ". Please recreate this definition.", true);
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("USER"))
                    {
                        definitionDTO.setType("user");
                        String name = glideVO.getUName();
                        desc.append("{\"uuserName\":\"").append(name).append("\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("GROUP"))
                    {
                        definitionDTO.setType("group");
                        desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
                        desc.append(",\"udescription\":\"\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("ROLE"))
                    {
                        definitionDTO.setType("role");
                        desc.append("{\"uname\":\"").append(glideVO.getUName()).append("\"");
                        desc.append(",\"udescription\":\"\"} ");
                    }
                    else if (glideVO.getUType().equalsIgnoreCase("CATALOG"))
                    {
                        definitionDTO.setType("catalog");
                        desc.append("{\"name\":\"").append(glideVO.getUModule()).append("\"");
                        if (StringUtils.isNotBlank(glideVO.getUName()))
                        {
                            desc.append(",\"id\":\"" + glideVO.getUName() + "\"} ");
                        }
                        else
                        {
                            desc.append(",\"id\":\"" + glideVO.getUModule() + "\"} ");
                        }
                    }
                    
                    String impexOptionJson = glideVO.getUOptions();
                    if (StringUtils.isBlank(impexOptionJson))
                    {
                        // It seems to be an old definition. Populate the default impexOption and save the glude.
                        ImpexOptionsDTO impexOptions = new ImpexOptionsDTO();
                        try
                        {
                            impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
                            glideVO.setUOptions(impexOptionJson);
                        }
                        catch(Exception e)
                        {
                            Log.log.error("Could not parse default ImpexOption for ResolveImpexGlide", e);
                        }
                    }
                    
                    glideVO.setUDescription(desc.toString());
                    glideVO.setUType(definitionDTO.getType());
                    ResolveImpexGlide localGlide = new ResolveImpexGlide(glideVO);
                    localGlide.setResolveImpexModule(new ResolveImpexModule(impexModelVO));
                    ImpexUtil.saveResolveImpexGlide(localGlide, userName);
                }
                
                definitionDTO.setSysUpdatedBy(glideVO.getSysUpdatedBy());
                definitionDTO.setSysUpdatedOn(glideVO.getSysUpdatedOn());
                
                definitionList.add(definitionDTO);
            }
        }

        if (impexModelVO.getResolveImpexWikis() != null)
        {
            for (ResolveImpexWikiVO impexWiki : impexModelVO.getResolveImpexWikis())
            {
                ImpexDefinitionDTO definitionDTO = new ImpexDefinitionDTO();
                definitionDTO.setSys_id(impexWiki.getSys_id());
                if (impexWiki.getUValue().indexOf(".") > -1 )
                {
                    String namespace = impexWiki.getUValue().substring(0, impexWiki.getUValue().indexOf("."));
                    String name = impexWiki.getUValue().substring(impexWiki.getUValue().indexOf(".") + 1, impexWiki.getUValue().length());
                    definitionDTO.setName(name);
                    definitionDTO.setNamespace(namespace);
                }
                else
                {
                    definitionDTO.setName(impexWiki.getUValue());
                }

                if (impexWiki.getUOptions() != null)
                {
                    try
                    {
                        ImpexOptionsDTO optionsDTO = new ObjectMapper().readValue(impexWiki.getUOptions(), ImpexOptionsDTO.class);
                        definitionDTO.setOptions(optionsDTO);
                    }
                    catch(Exception e)
                    {
                        Log.log.error("Could not parser options. Message: " + e.getMessage() + " So, applying default options.");
                        definitionDTO.setOptions(new ImpexOptionsDTO());
                    }
                }
                
                definitionDTO.setType(impexWiki.getUType());
                
                if (StringUtils.isNotBlank(impexWiki.getUDescription()))
                {
                    definitionDTO.setDescription(impexWiki.getUDescription());
                }
                else
                {
                    StringBuilder desc = new StringBuilder();
                    if (impexWiki.getUType().equalsIgnoreCase("tag"))
                    {
                        // we don't support Runbooks with tag category from Resolve 5 onwards.
                        continue;
                    }
                    if (impexWiki.getUType().equals("DOCUMENT"))
                    {  
                        definitionDTO.setType("wiki");
                        desc.append("{\"ufullname\":\"").append(impexWiki.getUValue()).append("\"} ");
                    }
                    if (impexWiki.getUType().contains("NAMESPACE"))
                    {
                        definitionDTO.setType("wikiNamespace");
                        desc.append("{\"unamespace\":\"").append(impexWiki.getUValue()).append("\"");
                        desc.append(",\"usummary\":\"\"} ");
                    }
                    
                    String impexOptionJson = impexWiki.getUOptions();
                    if (impexOptionJson == null)
                    {
                        // It seems to be an old definition. Populate the default impexOption and save the glude.
                        ImpexOptionsDTO impexOptions = new ImpexOptionsDTO();
                        if (impexWiki.getUScan() == null || impexWiki.getUScan() == false)
                        {
                            impexOptions.setWikiForms(false);
                            impexOptions.setWikiSubRB(false);
                            impexOptions.setWikiSubDT(false);
                            impexOptions.setWikiRefATs(false);
                        }
                        try
                        {
                            impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
                            impexWiki.setUOptions(impexOptionJson);
                        }
                        catch(Exception e)
                        {
                            Log.log.error("Could not parse default ImpexOption for ResolveImpexWiki", e);
                        }
                    }
                    
                    definitionDTO.setDescription(desc.toString());
                    
                    impexWiki.setUDescription(desc.toString());
                    impexWiki.setUType(definitionDTO.getType());
                    ResolveImpexWiki localWiki = new ResolveImpexWiki(impexWiki);
                    localWiki.setResolveImpexModule(new ResolveImpexModule(impexModelVO));
                    ImpexUtil.saveResolveImpexWiki(localWiki, userName);
                }
                
                definitionDTO.setSysUpdatedBy(impexWiki.getSysUpdatedBy());
                definitionDTO.setSysUpdatedOn(impexWiki.getSysUpdatedOn());
                
                definitionList.add(definitionDTO);
            }
        }

        impexModelVO.setImpexDefinition(definitionList);

        impexModelVO.setResolveImpexGlides(null);
        impexModelVO.setResolveImpexWikis(null);

        return impexModelVO;
    }

    public static ResolveImpexModuleVO saveResolveImpexModule(ResolveImpexModuleVO vo, String username, boolean validate,
    														  String operation) throws Throwable
    {
        Collection<ImpexDefinitionDTO> defDTOCollection = null;
        
        if (vo != null)
        {
            ResolveImpexModule model = null;
            if (StringUtils.isBlank(vo.getSys_id()) || vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                if (validate)
                {
                    model = getImpexModel(null, vo.getUName(), false);
                }
                if (model != null)
                {
                    throw new Exception(String.format("Error: Module Name, '%s', %s. Please use a unique name.", 
                    								  model.getUName(), MODULE_ALREADY_EXISTS_MARKER));
                }

                model = new ResolveImpexModule(vo);
            }
            else
            {
                model = getImpexModelWithReferences(vo.getSys_id(), null, username);
                if (vo.getUZipFileContent() != null)
                {
                    model.setUZipFileContent(vo.getUZipFileContent());
                    if (vo.getUZipFileName() != null)
                    {
                        if (vo.getUZipFileName().equals("UNDEFINED"))
                        {
                            model.setUZipFileName(null);
                        }
                        else
                        {
                            model.setUZipFileName(vo.getUZipFileName());
                        }
                    }
                }
                if (!model.getUName().equalsIgnoreCase(vo.getUName()))
                {
                    if (getImpexModel(null, vo.getUName(), false) == null)
                    {
                        model.setUName(vo.getUName());
                    }
                    else
                    {
                        throw new Exception(String.format("Error: Module Name %s. Please use a unique name.", 
                        								  MODULE_ALREADY_EXISTS_MARKER));
                    }
                }
                
                model.setUForwardWikiDocument(vo.getUForwardWikiDocument());
                model.setUScriptName(vo.getUScriptName());
                model.setUDescription(vo.getUDescription());
            }
            model.setExcludeList(vo.getExcludeList());
            if (StringUtils.isBlank(vo.getUVersion())) {
                model.setUVersion(new AboutDTO().getVersion());
            }
            model = SaveUtil.saveResolveImpexModule(model, username);
            populateImpexGlideAndWiki(model, vo, username, operation.toUpperCase());

            if (vo.getResolveImpexGlides() != null)
            {
                List<ResolveImpexGlideVO> glideList = new ArrayList<ResolveImpexGlideVO>();
                glideList.addAll(vo.getResolveImpexGlides());

                for (ResolveImpexGlideVO glideVO : glideList)
                {
                    ResolveImpexGlide glideModel = new ResolveImpexGlide(glideVO);
                    glideModel.setResolveImpexModule(model);
                    SaveUtil.saveResolveImpexGlide(glideModel, username);
                }
            }

            if (vo.getResolveImpexWikis() != null)
            {
                List<ResolveImpexWikiVO> wikiList = new ArrayList<ResolveImpexWikiVO>();
                wikiList.addAll(vo.getResolveImpexWikis());

                for (ResolveImpexWikiVO wikiVO : wikiList)
                {
                    ResolveImpexWiki impexWikiModel = new ResolveImpexWiki(wikiVO);
                    impexWikiModel.setResolveImpexModule(model);
                    SaveUtil.saveResolveImpexWiki(impexWikiModel, username);
                }
            }
            
            defDTOCollection = vo.getImpexDefinition();
            vo = model.doGetVO();
        }

        vo.setUZipFileContent(null);
        vo.setImpexDefinition(defDTOCollection);
        
        ImpexManifestUtil.deleteManifest(vo.getUName(), null, username);
        
        return vo;
    }
    
    public static void saveResolveVersion(ResolveImpexModuleVO vo, String userName) throws Exception
    {
        ResolveImpexModule module = new ResolveImpexModule(vo);
        module.setSysCreatedBy(vo.getSysCreatedBy());
        if (vo.getSysCreatedOn() != null)
        {
            module.setSysCreatedOn(GMTDate.getDate(vo.getSysCreatedOn()));
        }
        module.setUVersion(new AboutDTO().getVersion());
        SaveUtil.saveResolveImpexModule(module, userName);
    }

    protected static void populateImpexGlideAndWiki(ResolveImpexModule model, ResolveImpexModuleVO vo, String userName, 
    												String operation)
    {
        Collection<ResolveImpexGlide> glideList = model.getResolveImpexGlides();
        Collection<ResolveImpexWiki> wikiList = model.getResolveImpexWikis();

        String stagePrefix = null;
        String errMsg = null;
        
		if ("import".equalsIgnoreCase(operation)) {
			errMsg = "Post Import";
		} else if ("export".equalsIgnoreCase(operation)){
			errMsg = "for Export";
		} else if ("save".equalsIgnoreCase(operation)) {
			errMsg = "for Save Impex Definition";
		}
		
        Collection<ImpexDefinitionDTO> definitionDTOCollection = vo.getImpexDefinition();
        if (CollectionUtils.isNotEmpty(definitionDTOCollection))
        {
        	try {
        		if ("import".equalsIgnoreCase(operation)) {
        			stagePrefix = "(49/51) Post Import";
        		} else if ("export".equalsIgnoreCase(operation)) {
        			stagePrefix = "(1/24) Export";
        		} else {
        			stagePrefix = null;
        		}
        	
        		if (StringUtils.isNotBlank(stagePrefix)) {
        			ImpexUtil.initImpexOperationStage(operation.toUpperCase(), 
        											  String.format("%s Save Wiki And Glide Components", 
						  											stagePrefix), 
					   	  						  	  definitionDTOCollection.size());
        		}
        		
	            Iterator<ImpexDefinitionDTO> iterator = definitionDTOCollection.iterator();
	            while (iterator.hasNext())
	            {
	            	if (StringUtils.isNotBlank(stagePrefix)) {
	            		ImpexUtil.updateImpexCount(operation.toUpperCase());
	            	}
	            	
	                ImpexDefinitionDTO dto = iterator.next();
	                String type = dto.getType().toLowerCase();
	                if (type.contains("doc") || type.equalsIgnoreCase(ImpexEnum.WIKI.getValue()) || type.equalsIgnoreCase("securitytemplate") ||
	                                type.equalsIgnoreCase("wikinamespace") || type.equalsIgnoreCase(ImpexEnum.RUNBOOK.getValue()) ||
	                                type.equalsIgnoreCase(ImpexEnum.DECISIONTREE.getValue()) || type.equalsIgnoreCase("automationBuilder"))
	                {
	                    ResolveImpexWiki local = null;
	                    if (wikiList == null)
	                    {
	                        wikiList = new ArrayList<ResolveImpexWiki>();
	                    }
	
	                    Iterator<ResolveImpexWiki> i = wikiList.iterator();
	                    while (i.hasNext())
	                    {
	                        ResolveImpexWiki wiki = i.next();
	                        if (wiki.getSys_id().equals(dto.getSys_id()))
	                        {
	                            local = wiki;
	                            i.remove();
	                            break;
	                        }
	                    }
	
	                    if (local == null)
	                    {
	                        local = new ResolveImpexWiki();
	                        local.setUType(dto.getType());
	                    }
	                    else
	                    {
	                        if (local.getUOptions() != null)
	                        {
	                            // If options are null, it's old definition.
	                            try
	                            {
	                                String dtoOptionsJson = new ObjectMapper().writer().writeValueAsString(dto.getOptions());
	                                if (dtoOptionsJson.equalsIgnoreCase(local.getUOptions()))
	                                {
	                                    dto.setSysUpdatedOn(GMTDate.getLocalServerDate(local.getSysUpdatedOn()));
	                                    dto.setSysUpdatedBy(local.getSysUpdatedBy());
	                                    continue;
	                                }
	                            }
	                            catch (Exception e)
	                            {
	                                Log.log.error(e.getMessage(), e);
	                            }
	                        }
	                    }
	                    
	                    if (dto.getOptions() != null)
	                    {
	                        String impexDefinitionJSON = null;
	                        try
	                        {
	                            impexDefinitionJSON = new ObjectMapper().writer().writeValueAsString(dto.getOptions());
	                        }
	                        catch(Exception e)
	                        {
	                            ImpexUtil.logImpexMessage("Could not convert ImpexDefinitionDTO '" + dto.getName() + "' to JSON. " + e.getMessage(), true);
	                        }
	                        local.setUOptions(impexDefinitionJSON);
	                    }
	                    local.setUValue(dto.getNamespace());
	                    local.setResolveImpexModule(model);
	                    local.setUDescription(dto.getDescription());
	                    if (dto.getSysUpdatedOn() != null)
	                    {
	                        local.setSysUpdatedOn(GMTDate.getDate(dto.getSysUpdatedOn()));
	                    }
	
	                    saveResolveImpexWiki(local, userName);
	                    dto.setSys_id(local.getSys_id());
	                    
	                    if (dto.getSysUpdatedBy() == null)
	                        dto.setSysUpdatedBy(local.getSysUpdatedBy());
	                    if (dto.getSysUpdatedOn() == null)
	                    dto.setSysUpdatedOn(GMTDate.getLocalServerDate(local.getSysUpdatedOn()));
	                }
	                else
	                {
	                    ResolveImpexGlide local = null;
	                    if (glideList == null)
	                    {
	                        glideList = new ArrayList<ResolveImpexGlide>();
	                    }
	                    Iterator<ResolveImpexGlide> i = glideList.iterator();
	                    while (i.hasNext())
	                    {
	                        ResolveImpexGlide glide = i.next();
	                        if (glide.getSys_id().equals(dto.getSys_id()))
	                        {
	                            local = glide;
	                            i.remove();
	                            break;
	                        }
	                    }
	
	                    if (local == null)
	                    {
	                        local = new ResolveImpexGlide();
	                        local.setUType(dto.getType());
	                    }
	                    else
	                    {
	                        if (local.getUOptions() != null)
	                        {
	                            // If options are null, it's old definition.
	                            try
	                            {
	                                String dtoOptionsJson = new ObjectMapper().writer().writeValueAsString(dto.getOptions());
	                                if (dtoOptionsJson.equalsIgnoreCase(local.getUOptions()))
	                                {
	                                    dto.setSysUpdatedOn(GMTDate.getLocalServerDate(local.getSysUpdatedOn()));
	                                    dto.setSysUpdatedBy(local.getSysUpdatedBy());
	                                    continue;
	                                }
	                            }
	                            catch (Exception e)
	                            {
	                                Log.log.error(e.getMessage(), e);
	                            }
	                        }
	                    }
	
	                    local.setResolveImpexModule(model);
	                    local.setUName(dto.getName());
	                    local.setUModule(dto.getNamespace());
	                    local.setUDescription(dto.getDescription());
	                    local.setUType(dto.getType());
	                    if (dto.getSysUpdatedOn() != null)
	                    {
	                        local.setSysUpdatedOn(GMTDate.getDate(dto.getSysUpdatedOn()));
	                    }
	                    if (dto.getOptions() != null)
	                    {
	                        String impexDefinitionJSON = null;
	                        try
	                        {
	                            impexDefinitionJSON = new ObjectMapper().writer().writeValueAsString(dto.getOptions());
	                        }
	                        catch(Exception e)
	                        {
	                            ImpexUtil.logImpexMessage("Could not convert ImpexDefinitionDTO '" + dto.getName() + "' to JSON. " + e.getMessage(), true);
	                        }
	                        local.setUOptions(impexDefinitionJSON);
	                    }
	
	                    saveResolveImpexGlide(local, userName);
	                    dto.setSys_id(local.getSys_id());
	                    
	                    if (dto.getSysUpdatedBy() == null)
	                        dto.setSysUpdatedBy(local.getSysUpdatedBy());
	                    if (dto.getSysUpdatedOn() == null)
	                        dto.setSysUpdatedOn(GMTDate.getLocalServerDate(local.getSysUpdatedOn()));
	                }
	            }
        	} catch (Exception e) {        		
        		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
      				  		  e.getMessage() : String.format("Failed to Save Wiki And Glide Components %s.", errMsg), e);
        		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
						  		  	  	  e.getMessage() : 
						  		  	  	  String.format("Failed to Save Wiki And Glide Components %s. " +
						  		  	  			  		"Please check RSView logs for details.", errMsg), 
						  		  	  	  true);
        	}
        	
            if (CollectionUtils.isNotEmpty(wikiList))
            {
            	try {
            		if ("import".equalsIgnoreCase(operation)) {
            			stagePrefix = "(50/51) Post Import";
            		} else if ("export".equalsIgnoreCase(operation)) {
            			stagePrefix = "(2/24) Export";
            		} else {
            			stagePrefix = null;
            		}
            		
            		if (StringUtils.isNotBlank(stagePrefix)) {
            			ImpexUtil.initImpexOperationStage(operation.toUpperCase(), 
            											  String.format("%s Delete Wiki Components Saved", 
            													  		stagePrefix), 
            											  definitionDTOCollection.size());
            		}
            		
	                for (ResolveImpexWiki wiki : wikiList)
	                {
	                	if (StringUtils.isNotBlank(stagePrefix)) {
	                		ImpexUtil.updateImpexCount(operation.toUpperCase());
	                	}
	                    deleteResolveImpexWikiBySysId(wiki.getSys_id(), userName);
	                }
            	} catch (Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
    				  		  	  e.getMessage() : String.format("Failed to Delete Wiki Components Saved %s.", errMsg), e);
            		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
						  		  	  	  	  e.getMessage() : 
						  		  	  	  	  String.format("Failed to Delete Wiki Components Saved %s. Please check " +
						  		  	  	  			  		"RSView logs for details.", errMsg), 
						  		  	  	  	  true);
            	}
            }
            
            if (CollectionUtils.isNotEmpty(glideList))
            {
            	try {
            		if ("import".equalsIgnoreCase(operation)) {
            			stagePrefix = "(51/51) Post Import";
            		} else if ("export".equalsIgnoreCase(operation)) {
            			stagePrefix = "(3/24) Export";
            		} else {
            			stagePrefix = null;
            		}
            		
            		if (StringUtils.isNotBlank(stagePrefix)) {
            			ImpexUtil.initImpexOperationStage(operation.toUpperCase(), 
            										  	  String.format("%s Delete Glide Components Saved", 
            										  			  		stagePrefix),
	   						   						  	  definitionDTOCollection.size());
            		}
            		
	                for (ResolveImpexGlide glide : glideList)
	                {
	                	if (StringUtils.isNotBlank(stagePrefix)) {
	                		ImpexUtil.updateImpexCount(operation.toUpperCase());
	                	}
	                    deleteResolveImpexGlideBySysId(glide.getSys_id(), userName);
	                }
            	} catch (Exception e) {
            		Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
				  		  	  	  e.getMessage() : String.format("Failed to Delete Glide Components Saved %s.", errMsg), e);
            		ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
					  		  	  	  	  	  e.getMessage() : 
					  		  	  	  	  	  String.format("Failed to Delete Glide Components Saved %s. Please check " +
					  		  	  	  	  			  		"RSView logs for details.", errMsg), 
					  		  	  	  	  	  true);
            	}
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
    public static void deleteImpexDefinitionByName(String moduleName, boolean dirty, String userName)
    {
        if (StringUtils.isNotBlank(moduleName))
        {
            try
            {
                String hql = "update ResolveImpexModule set UZipFileContent = null where UName = :MODULE_NAME and UDirty = :DIRTY";
                
              HibernateProxy.setCurrentUser(userName);
                HibernateProxy.executeNoCache(() -> {
                    /*
                     * This updated is needed, cause if the zip file content is too big,
                     * the findById(sysId) fails by throwing java.lang.OutOfMemoryError: Java heap space. 
                     */
                    Query query = HibernateUtil.createQuery(hql);
                    query.setParameter("MODULE_NAME", moduleName, StringType.INSTANCE);
                    query.setParameter("DIRTY", dirty, BooleanType.INSTANCE);
                    query.executeUpdate();
                    
                    ResolveImpexModule model = new ResolveImpexModule();
                    model.setUName(moduleName);
                    model.setUDirty(dirty);
                    
                    List<ResolveImpexModule> list = HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().find(model);
                    
                    if (list != null && list.size() > 0)
                    {
                        for (ResolveImpexModule impexModel : list)
                        {
                            //delete glide
                            ResolveImpexGlideDAO daoGlide = HibernateUtil.getDAOFactory().getResolveImpexGlideDAO();
                            Collection<ResolveImpexGlide> glideList = impexModel.getResolveImpexGlides();
                            for (ResolveImpexGlide glide : glideList)
                            {
                                daoGlide.delete(glide);
                            }

                            //delete wiki
                            ResolveImpexWikiDAO daoWiki = HibernateUtil.getDAOFactory().getResolveImpexWikiDAO();
                            Collection<ResolveImpexWiki> wikiList = impexModel.getResolveImpexWikis();
                            for (ResolveImpexWiki wiki : wikiList)
                            {
                                daoWiki.delete(wiki);
                            }

                            //delete the rec
                            HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().delete(impexModel);
                        }
                    }                   
                    
                });
            }
            catch(Exception e)
            {
                
              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }

    public static void deleteImpexModelByIDS(List<String> ids, String username) throws Throwable
    {
        if(ids != null)
        {
            for(String sysId : ids)
            {
                deleteImpexModelBySysId(sysId, username);
            }
        }
    }

    public static void deleteResolveImpexWikiIDS(List<String> ids, String username)
    {
        if(ids != null)
        {
            for(String sysId : ids)
            {
                deleteResolveImpexWikiBySysId(sysId, username);
            }
        }
    }

    public static void deleteResolveImpexGlideIDS(List<String> ids, String username)
    {
        if(ids != null)
        {
            for(String sysId : ids)
            {
                deleteResolveImpexGlideBySysId(sysId, username);
            }
        }
    }
    
    /*
     * From 6.3 onwards this is synchronous call
     */
    public static String uploadFileByURL(String moduleURL, String userName) throws Exception
    {
        Map<String, String> params = new HashMap<String,String>();
        params.put("URL", moduleURL);
        MImpex.uploadFile(params);
        
        URL url = new URL(moduleURL);
        String query = url.getQuery();
        String moduleFileName = "";
        
        if(StringUtils.isEmpty(query))
        {
            moduleFileName = moduleURL.substring(moduleURL.lastIndexOf("/")+1);
        }
        else
        {
            String[] splitted = query.split("&");
            for (String tokens : splitted)
            {
                if (tokens.startsWith("filename=")) // http://remote_server:port/resolve/service/wiki/impex/download?filename=doc3.zip
                {
                    moduleFileName = tokens.substring(9);
                    break;
                }
            }
        }
        
        if (moduleFileName.contains(".zip"))
        {
            moduleFileName = moduleFileName.substring(0, moduleFileName.indexOf("."));
        }
        return moduleFileName;
    }

    public static String importModule(String moduleName, List<String> includeList, String userName, boolean blockIndex) throws Exception
    {
    	String message = null;
    	
    	if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.importModule(%s,%s,%s,%b) calling " +
										"ServiceHibernate.getAndSetImpexEventValue...", 
										moduleName, StringUtils.listToString(includeList, ", "), userName, blockIndex));
    	}
    	
    	Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
    	
    	try {
    		rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("IMPORT", 5, moduleName, userName);
    	} catch (Exception e) {
    		if (StringUtils.isBlank(e.getMessage()) || !(e.getMessage().startsWith("Operation timed out after waiting for"))) {
    			throw e;
    		} else {
    			if (Log.log.isTraceEnabled()) {
    				Log.log.trace(String.format("ImpexUtil.importModule(%s,%s,%s,%b) " +
								  				"ServiceHibernate.getAndSetImpexEventValue timed out after " +
								  				"waiting for 5 seconds", 
								  				moduleName, StringUtils.listToString(includeList, ", "), userName, blockIndex));
    			}
    		}
    	}
    	
        if (rsEventVOPair != null && Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.importModule(%s,%s,%s,%b) " +
										"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
										moduleName, StringUtils.listToString(includeList, ", "), userName, blockIndex,
										(rsEventVOPair.getLeft() != null ? 
										 "[" + rsEventVOPair.getLeft() + "] which is " + 
										 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
        }

        if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("MODULE_NAME", moduleName);
	        params.put("INCLUDE_LIST", includeList);
	        params.put("USER", userName);
	        params.put("BLOCK_INDEX", new Boolean(blockIndex));
	        
	        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MImpex.importModuleFromBrowser", params);
        } else {
        	if (rsEventVOPair != null) {
        		message = String.format("%s IMPEX by user %s in progress, please try later", 
        								rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
        								rsEventVOPair.getLeft().getSysCreatedBy());
        	} else {
        		message = "Another IMPEX operation is in progress, please try later";
        	}
        }
        
        //Thread.sleep(500);
        return message;
    }
    
    public static String exportModule(String moduleName, List<String> excludeList, boolean isNew, String userName) throws Exception
    {
    	String message = null;
    	
    	if (Log.log.isDebugEnabled()) {
	        Log.log.debug(String.format("ImpexUtil.exportModule(%s,%s,%s) calling " +
										"ServiceHibernate.getAndSetImpexEventValue...", 
										moduleName, StringUtils.listToString(excludeList, ", "), userName));
    	}
    	
    	Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
    	
    	try {
    		rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("EXPORT", 5, moduleName, userName);
    	} catch (Exception e) {
    		if (StringUtils.isBlank(e.getMessage()) || !(e.getMessage().startsWith("Operation timed out after waiting for"))) {
    			throw e;
    		} else {
    			if (Log.log.isDebugEnabled()) {
    				Log.log.debug(String.format("ImpexUtil.exportModule(%s,%s,%s) " +
												"ServiceHibernate.getAndSetImpexEventValue timed out after " +
												"waiting for 5 seconds", 
												moduleName, StringUtils.listToString(excludeList, ", "), userName));
    			}
    		}
    	}
    	
        if (rsEventVOPair != null && Log.log.isDebugEnabled()) {
	        Log.log.debug(String.format("ImpexUtil.exportModule(%s,%s,%s) " +
										"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
										moduleName, StringUtils.listToString(excludeList, ", "), userName,
										(rsEventVOPair.getLeft() != null ? 
										 "[" + rsEventVOPair.getLeft() + "] which is " + 
										 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
        }
        
        if (rsEventVOPair != null && 
        	(rsEventVOPair.getRight().booleanValue() ||
        	 (rsEventVOPair.getLeft().getModuleNameAndHasStage("EXPORT") != null && 
        	  rsEventVOPair.getLeft().getModuleNameAndHasStage("EXPORT").getLeft().equals(moduleName) &&
        	  rsEventVOPair.getLeft().getModuleNameAndHasStage("EXPORT").getRight().booleanValue() &&
        	  "EXPORT Init".equals(rsEventVOPair.getLeft().getStage())))) {
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("MODULE_NAME", moduleName);
	        params.put("EXCLUDE_LIST", excludeList);
	        params.put("USER", userName);
	        params.put("IS_NEW", isNew);
	        
	        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MImpex.exportModuleFromBrowser", params);
        } else {
        	if (rsEventVOPair != null) {
	        	message = String.format("%s IMPEX by user %s in progress, please try later", 
										rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
										rsEventVOPair.getLeft().getSysCreatedBy());
        	} else {
        		message = "Another IMPEX operation is in progress, please try later";
        	}
        }
        
        return message;
    }
    
    public static boolean checkRefCustomTables(String moduleName, String userName) throws Exception
    {
        boolean result = false;
        
        ResolveImpexModule module = getImpexModelWithReferences(null, moduleName, userName);
        if (module != null)
        {
            Collection<ResolveImpexGlide> glides = module.getResolveImpexGlides();
            if (glides != null)
            {
                String tableName;
                for (ResolveImpexGlide glide : glides)
                {
                    if (glide.getUType().equalsIgnoreCase("form"))
                    {
                        String formName = glide.getUModule();
                        MetaFormView formView = CustomFormUtil.findMetaFormView(null, formName, userName);
                        if (formView != null)
                        {
	                        tableName = formView.getUTableName();
	                        if (StringUtils.isBlank(tableName))
	                        {
	                            continue;
	                        }
	                        else
	                        {
	                            result = checkCustomTableForRef(tableName);
	                            if (result)
	                            {
	                                break;
	                            }
	                        }
                        }
                        else
                        {
                        	ImpexUtil.logImpexMessage("Custom Form, '" + formName + "' could nol be found.", true);
                        }
                    }
                    else if (glide.getUType().equalsIgnoreCase("customtable"))
                    {
                        tableName = glide.getUModule();
                        result = checkCustomTableForRef(tableName);
                        if (result)
                        {
                            break;
                        }
                    }
                }
            }
        }
        
        return result;
    }
    
    private static boolean checkCustomTableForRef(String tableName) throws Exception
    {
        boolean result = false;
        
        CustomTableVO customTableVO = CustomTableUtil.findCustomTableWithFields(null, tableName, "admin");
        String schemaDef = customTableVO.getUSchemaDefinition();
        if (StringUtils.countMatches(schemaDef, "entity-name") > 1)
            result = true;
        
        return result;
    }
    
    public static Map<String, String> getImportStatus(String operation) throws Exception
    {
        Map<String, String> resultMap = new HashMap<String, String>();
        
        int percentage = 0;
        
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.getImportStatus(%s) calling ServiceHibernate.getLatestImpexEventByValue(" +
	        							"wait for 5 secs)...", 
										operation));
        }
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue(5l);
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.getImportStatus(%s) ServiceHibernate.getLatestImpexEventByValue(" +
	        							"wait for 5 secs) returned Resolve Event [%s]", 
	        							operation, (eventVO != null ? eventVO : "null")));
        }
        
        if (eventVO != null)
        {
            String[] progress = eventVO.getUValue().split(",");
            String moduleName = "";
            String oldModuleName = "";
            String newModuleName = "";
            if (progress[0].split(":").length == 2)
            {
                moduleName = progress[0].split(":")[1];
                if (StringUtils.isBlank(moduleName))
                {
                    resultMap.put("ERROR", "Module name cannot be blank");
                    return resultMap;
                }
                
                if (moduleName.contains("|")) {
                	// For status use old module name. Module Name format New Module Name | Old Module Name                	
                	newModuleName = StringUtils.substringBefore(moduleName, "|");
                	oldModuleName = StringUtils.substringAfter(moduleName, "|");
                } else {
                	oldModuleName = moduleName;
                	newModuleName = moduleName;
                }
                
                String stageName = progress[0].split(":")[0].contains("-") ? progress[0].split(":")[0].split("-")[1] : null;
                
                if (StringUtils.isNotBlank(stageName)) {
                	resultMap.put("STAGE", stageName);
                }
            }
            
            resultMap.put("MODULE", oldModuleName);
            
            if (StringUtils.isNotBlank(newModuleName)) {
            	resultMap.put("NEWMODULE", newModuleName);
            }
            
            if (progress.length == 3)
            {
                int total = Integer.parseInt(progress[1]);
                if (total == -1)
                {
                    resultMap.put("ERROR", progress[2]);
                    // ServiceHibernate.clearAllResolveEventByValue(operation);
                }
                else
                {
                    if (/*total != 100 && */total != -2)
                    {
                        String prgrs = progress[2];
                        if (StringUtils.isBlank(prgrs))
                        {
                            prgrs = "0";
                        }
                        int processed = Integer.parseInt(prgrs);
                        if (total != 0)
                        {
                            if (processed > total)
                            {
//                            	resultMap.put("STAGESUCCESS", "100");
//                                resultMap.put("SUCCESS", "100");
                            	percentage = 100;
                            }
                            else
                            {
                                percentage = (processed * 100 / total) ;
//                                resultMap.put("STAGESUCCESS", percentage + "");
                            }
                            
                            resultMap.put("STAGESUCCESS", percentage + "");
                            
                            int stepNumber = -1;
                            int totalSteps = -1;
                            
                            if (resultMap.containsKey("STAGE") && 
                            	StringUtils.isNotBlank(resultMap.get("STAGE"))) {
                            	String stepPrfix = StringUtils.substringBetween(resultMap.get("STAGE"), "(", ")");
                            	
                            	if (StringUtils.isNotBlank(stepPrfix)) {
	                            	try {
	                            		stepNumber = Integer.parseInt(StringUtils.substringBefore(stepPrfix, "/"));
	                            		totalSteps = Integer.parseInt(StringUtils.substringAfter(stepPrfix, "/"));
	                            		
	                            		if (percentage != 100) {
	                            			if (stepNumber > 0) {
	                            				--stepNumber;
	                            			}
	                            		}
	                            		
	                            		resultMap.put("SUCCESS", ((int)((stepNumber * 100)/totalSteps)) + "");
	                            	} catch (NumberFormatException nfe) {
	                            		Log.log.error(String.format("Error converting step # and total # of steps from stage " +
	                            								    "step prefix %s", stepPrfix));
	                            	}
                            	}
                            }
                        }
                        
                        if (!resultMap.containsKey("SUCCESS") || StringUtils.isBlank(resultMap.get("SUCCESS"))) {
                        	resultMap.put("SUCCESS", (int)percentage+"");
                        }
                    }
                    else
                    {
                        resultMap.put("SUCCESS", /*progress[2]*/"100");
                        String forwardWiki = progress[2];
                        if (StringUtils.isBlank(forwardWiki) || !forwardWiki.contains(".")) {
                            forwardWiki = "";
                        }
                        resultMap.put("FORWARD_WIKI", forwardWiki);
                        String impexLog = exportLog.toString();
                        if (StringUtils.isBlank(impexLog)) {
                            impexLog = getImpexResult(newModuleName, "admin");
                        }
                        
                        if (StringUtils.isNotBlank(impexLog))
                        {
                            if (impexLog.contains("color=\"red\""))
                            {
                                resultMap.put("STATUS", "Error");
                            }
                            else if (impexLog.contains("WARNING: "))
                            {
                                resultMap.put("STATUS", "Warning");
                            }
                            resultMap.put("LOG", impexLog);
                        }
                        else
                        {
                            Log.log.warn("Failed to get status, Operation: " + operation + " Module: " +   resultMap.get("MODULE"));
                            resultMap.put("STATUS", "Warning : Failed to get status. Please check import/export log.");
                        }
                        impexLog = null;
                        // ServiceHibernate.clearAllResolveEventByValue(operation);
                    }
                }
            }
        } else {
        	resultMap.put("STATUS", "Busy");
        }
        
        // Log.log.info(percentage);
        
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.getImportStatus(%s) returning Result Map [%s]",
	        							operation, StringUtils.mapToString(resultMap, "=", ", ")));
        }
        
        return resultMap;
    }
    
    @SuppressWarnings({ "unchecked"})
    public static String getImpexResult(String moduleName, String userName)
    {
        String hql = "from ResolveImpexLog l where l.resolveImpexModule.UName = :name order by l.sysUpdatedOn desc";
        List<ResolveImpexLog> list1 = null;
        try
        {
            
          HibernateProxy.setCurrentUser(userName);
            list1 = (List<ResolveImpexLog>) HibernateProxy.execute(() -> {
            	return HibernateUtil.createHQLQuery(hql).setParameter("name", moduleName).list();
            });
        }
        catch (Exception e)
        {
            Log.log.error("Error while getting impex result for the module: " + moduleName, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        String result = null;
        if (CollectionUtils.isNotEmpty (list1))
        {
            result = list1.get(0).getULogHistory();
        }
        
        return result;
    }
    
    public static Map<String, String> urlUploadStatus(String userName) throws Exception
    {
        Map<String, String> resultMap = new HashMap<String, String>();
        int percent = 0;
        ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("URLUPLOAD");
        long size = 0l;
        long completed = 0;
        String fileName = null;
        
        if (eventVO != null)
        {
            String value = eventVO.getUValue();
            String[] valueArray = value.split(",");
            if (valueArray.length == 3)
            {
                if (valueArray[1].equals("-1"))
                {
                    resultMap.put("ERROR", valueArray[2]);
                }
                else
                {
                	Pair<String, Boolean> moduleNameAndHasStagePair = eventVO.getModuleNameAndHasStage("URLUPLOAD");
                	
                	if (moduleNameAndHasStagePair == null || StringUtils.isBlank(moduleNameAndHasStagePair.getLeft())) {
                		resultMap.put("ERROR", "File name cannot be blank" );
                	}
                	
                	if (!resultMap.containsKey("ERROR")) {
	                	fileName = moduleNameAndHasStagePair.getLeft();
	                	
	                	if (!valueArray[1].equals("-2")) {
		                	size = Long.parseLong(valueArray[2]);
	                
		                	String downloadFolder = RSContext.getResolveHome() + "rsexpert/";
		                	completed = FileUtils.getFile(downloadFolder + fileName).length();
	                    
		                	if (size > 0l )
		                	{
		                		if (completed > size) {
		                			percent = 100;
		                		} else {
		                			percent = (int)((completed * 100)/size);
		                		}
		                	}
	                    
		                	if (percent == 100) {
		                		resultMap.put("SUCCESS", fileName);
		                	} else {
		                		resultMap.put("SUCCESS", percent + "");
		                	}
	                	} else {
	                		resultMap.put("SUCCESS", fileName);
	                	}
                	}
                }
            }
        }
        
        return resultMap;
    }
    
    public static void updateImpexCount(String operation) throws Exception
    {
    	if (Log.log.isTraceEnabled()) {
	    	Log.log.trace(String.format("ImpexUtil.updateImpexCount(%s) calling " +
										"ServiceHibernate.getLatestImpexEventByValue...", 
										operation));
    	}
    	
        ResolveEventVO latestImpexEventVO = ServiceHibernate.getLatestImpexEventByValue();
        
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.importModule(%s) " +
										"ServiceHibernate.getLatestImpexEventByValue returned Resolve Event [%s]", 
										operation, (latestImpexEventVO != null ? latestImpexEventVO : "null")));
        }
        
        if (latestImpexEventVO == null) {
        	String message = String.format("No Impex Event found in database");
        	Log.log.error(message);
        	throw new Exception(message);
        }
        
        ResolveEventVO eventVO = latestImpexEventVO.getUValue()
        						 .startsWith(String.format("IMPEX%s", 
        								 				   (!operation.equalsIgnoreCase("IMPEX") ? 
        								 					"-" + operation.toUpperCase() : ""))) ?
				 				 latestImpexEventVO : null;

        if (eventVO == null) {
        	String message = String.format("Latest Impex Event [%s]'s operation does not match specified operation %s",
        								   latestImpexEventVO, operation);        	
        	Log.log.error(message);
        	throw new Exception(message);
        }
        
        String[] progress = eventVO.getUValue().split(",");
        
        // HP TODO Add stage parameter to the method and check the stage as well before incrementing the count
        
        if (progress.length == 3) {
            try
            {
                int c = Integer.parseInt(progress[2]);
                String value = progress[0] + "," + progress[1] + "," + ++c;
                eventVO.setUValue(value);
                ServiceHibernate.insertResolveEvent(eventVO);
                if (Log.log.isTraceEnabled()) {
                    Log.log.trace(String.format("ImpexUtil.updateImpexCount(%d) inserted Resolve Event [%s]",
												operation, eventVO));
                }
            }
            catch(Exception e)
            {
                // No action needs to be taken here.
            }
        } else {
        	String message = String.format("Found invalid Resolve Event [%s] while updating count for operation %s", 
        								   eventVO, operation);
        	
        	Log.log.error(message);
        	throw new Exception(message);
        }
    }
    
    public static void initImpexOperationStage(String operation, String stage, int startCount) throws Exception
    {
    	if (Log.log.isTraceEnabled()) {
	    	Log.log.trace(String.format("ImpexUtil.initImpexOperationStage(%s,%s,%d) calling " +
										"ServiceHibernate.getLatestImpexEventByValue...", 
										(StringUtils.isNotBlank(operation) ? operation : "null"), stage, startCount));
    	}
    	
        ResolveEventVO latestImpexEventVO = ServiceHibernate.getLatestImpexEventByValue();
        
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.initImpexOperationStage(%s,%s,%d) " +
										"ServiceHibernate.getLatestImpexEventByValue returned Resolve Event [%s]", 
										(StringUtils.isNotBlank(operation) ? operation : "null"), stage, startCount, 
										(latestImpexEventVO != null ? latestImpexEventVO : "null")));
        }
        
        if (latestImpexEventVO == null) {
        	String message = String.format("No Impex Event found in database");
        	Log.log.error(message);
        	throw new Exception(message);
        }
        
        ResolveEventVO eventVO = latestImpexEventVO.getUValue().startsWith(String.format("IMPEX-%s", operation.toUpperCase())) ?
        						 latestImpexEventVO : null;
        
        if (eventVO == null) {
        	String message = String.format("Latest Impex Event's [%s] operation does not match specified operation %s",
        								   latestImpexEventVO, operation);        	
        	Log.log.error(message);
        	throw new Exception(message);
        }
        
        Pair<String, Boolean> opModNameAndHasStage = eventVO.getModuleNameAndHasStage(operation);
        
        String updatedEventValue = null;
        
        if (opModNameAndHasStage != null) {
        	updatedEventValue = String.format("IMPEX-%s %s:%s,%d,0", operation, stage, opModNameAndHasStage.getLeft(), 
        									  startCount);
        } else {
        	opModNameAndHasStage = eventVO.getModuleNameAndHasStage(null);
        	updatedEventValue = String.format("IMPEX-%s %s:%s,%d,0", operation, stage, opModNameAndHasStage.getLeft(), 
					  						  startCount);
        }
        
        eventVO.setUValue(updatedEventValue);
        
        ResolveEventUtil.insertResolveEvent(eventVO);
        
        if (Log.log.isTraceEnabled()) {
        	Log.log.trace(String.format("ImpexUtil.initImportOperationStage(%s,%s,%d) inserted Resolve Event [%s]", 
        								(StringUtils.isNotBlank(operation) ? operation : "null"), stage, startCount,
        								eventVO));
        }
    }
    
    public static Pair<String, Boolean> getImportModuleName(String operation) throws Exception
    {
    	Pair<String, Boolean> impModNameAndHasStage = null;
    	
    	if (Log.log.isTraceEnabled()) {
	    	Log.log.trace(String.format("ImpexUtil.getImportModuleName(%s) calling " +
										"ServiceHibernate.getLatestImpexEventByValue...", 
										(StringUtils.isNotBlank(operation) ? operation : "null")));
    	}
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ImpexUtil.getImportModuleName(%s) " +
										"ServiceHibernate.getLatestImpexEventByValue returned Resolve Event [%s]", 
										(StringUtils.isNotBlank(operation) ? operation : "null"), 
										(eventVO != null ? eventVO : "null")));
        }
        
        if (eventVO != null)
        {
        	impModNameAndHasStage = eventVO.getModuleNameAndHasStage(operation);
            
            if (impModNameAndHasStage == null) {
            	impModNameAndHasStage = eventVO.getModuleNameAndHasStage(null);
            }
        }
        
        return impModNameAndHasStage;
    }
    
    /////////////
    //private apis

    // Create a Basic ResolveImpexWiki definition with the given value, type,
    // and scan
    private static ResolveImpexWiki createImpexWikiDefinition(String exportValue, String type, boolean scan)
    {
        ResolveImpexWiki riw = null;
        if (StringUtils.isNotEmpty(exportValue) && StringUtils.isNotEmpty(type))
        {
            riw = new ResolveImpexWiki();
            riw.setUValue(exportValue);
            riw.setUScan(scan);
            riw.setUType(type);
            riw.setSysUpdatedBy("system");
            riw.setSysCreatedBy("system");
        }

        return riw;
    }// createImpexWikiDefinition

    // Create a Basic ResolveImpexGlide definition with the given value, type,
    // and scan
    // if value has a # it will be split into module and name, otherwise name
    // will be null
    private static ResolveImpexGlide createImpexGlideDefinition(String exportName, String type)
    {
        ResolveImpexGlide rig = null;
        if (StringUtils.isNotEmpty(exportName) && StringUtils.isNotEmpty(type))
        {
            int hashAt = exportName.indexOf("#");
            String namespace = exportName;
            String name = null;
            if (hashAt > -1)
            {
                if (type == Constants.IMPEX_PARAM_CUSTOMTABLE || type == Constants.IMPEX_PARAM_DB)
                {
                    name = exportName.substring(hashAt + 1);
                    namespace = exportName.substring(0, hashAt);
                }
                else
                {
                    namespace = exportName.substring(hashAt + 1);
                    name = exportName.substring(0, hashAt);
                }
            }
            rig = new ResolveImpexGlide();
            rig.setUModule(namespace);
            rig.setUName(name);
            rig.setUType(type);
            rig.setSysUpdatedBy("system");
            rig.setSysCreatedBy("system");
        }

        return rig;
    }// createImpexGlideDefinition

    private static String getOrderByColumnName(String sortField)
    {
        String orderByName = "";

        if (sortField != null)
        {
            if (sortField.equals(HibernateConstantsEnum.IMPEX_MODEL_MODULE.getTagName()))
            {
                orderByName = "UName";
            }
            else if (sortField.equals(HibernateConstantsEnum.IMPEX_MODEL_MODIFIED_BY.getTagName()))
            {
                orderByName = "sysUpdatedBy";
            }
            else if (sortField.equals(HibernateConstantsEnum.IMPEX_MODEL_MODIFIED_ON.getTagName()))
            {
                orderByName = "sysUpdatedOn";
            }
        }

        return orderByName;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResolveImpexModule getImpexModel(String sysId, String moduleName, boolean dirty)
    {
        ResolveImpexModule model= null;
        List<Object> temp = null;
        String hql = ImpexEnum.DEFINITION_SELECT_SQL.getValue();
        
        if (StringUtils.isNotBlank(sysId))
        {
            hql = hql + " where sys_id = '" + sysId + "'";
        }
        else if (StringUtils.isNotBlank(moduleName))
        {
            hql = hql + "where lower(UName) = '" + moduleName.toLowerCase() + "' ";
            if (dirty)
            {
                hql = hql + " and UDirty = true order by sysUpdatedOn DESC";
            }
            else
            {
                hql = hql + " and (UDirty = false or UDirty is null)";
            }
        }
        
        String hqlFinal = hql;
        try
        {
           temp = (List<Object>) HibernateProxy.execute(() -> {
        	   Query query = HibernateUtil.createQuery(hqlFinal);
               return query.list();
           });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        if (temp != null && temp.size() > 0)
        {
            //model = (ResolveImpexModule) temp.get(0);
            model = new ResolveImpexModule();
            Object[] objArray = (Object[])temp.get(0);
            
            model.setSys_id((String)objArray[0]);
            model.setUName((String)objArray[1]);
            model.setUDescription((String)objArray[2]);
            model.setUForwardWikiDocument((String)objArray[3]);
            model.setUScriptName((String)objArray[4]);
            model.setUZipFileName((String)objArray[5]);
            model.setUVersion((String)objArray[6]);
            model.setManifestGraph((String)objArray[7]);
            model.setExcludeList((String)objArray[8]);

            model.setSysCreatedOn((Date)objArray[9]);
            model.setSysCreatedBy((String)objArray[10]);
            model.setSysUpdatedOn((Date)objArray[11]);
            model.setSysUpdatedBy((String)objArray[12]);
        }
        
        return model;
    }
    
    public static ResolveImpexModuleVO getImpexModelVO(String sysId, String moduleName, boolean dirty)
    {
        ResolveImpexModuleVO modelVO = null;
        ResolveImpexModule model = getImpexModel(sysId, moduleName, dirty);
        if (model != null)
        {
            modelVO = model.doGetVO();
        }
        return modelVO;
    }

    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static String getModuleSysId(String moduleName)
    {
        String sysId = null;
        String hql = "select sys_id from ResolveImpexModule where UName = :NAME and (UDirty = false or UDirty is null)";
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	List<String> list = (List<String>) HibernateProxy.executeNoCache(() -> {
        	   Query query = HibernateUtil.createQuery(hql);
               query.setParameter("NAME", moduleName, StringType.INSTANCE);
               return query.list();
           });
            
           
            if (list != null && list.size() > 0)
            {
                sysId = list.get(0);
            }
        }
        catch(Exception e)
        {
            Log.log.error(e, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return sysId;
    }

    @SuppressWarnings("rawtypes")
    private static void deleteImpexModelBySysId(String sysId, String username) throws Throwable
    {
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                String hql = "update ResolveImpexModule set UZipFileContent = null where sys_id = :SYS_ID";
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
	                
	                /*
	                 * This updated is needed, cause if the zip file content is too big,
	                 * the findById(sysId) fails by throwing java.lang.OutOfMemoryError: Java heap space. 
	                 */
	                Query query = HibernateUtil.createQuery(hql);
	                query.setParameter("SYS_ID", sysId);
	                query.executeUpdate();
	                
	                ResolveImpexModule model = HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().findById(sysId);
	                if (model != null)
	                {
	                    //delete glide
	                    ResolveImpexGlideDAO daoGlide = HibernateUtil.getDAOFactory().getResolveImpexGlideDAO();
	                    Collection<ResolveImpexGlide> glideList = model.getResolveImpexGlides();
	                    for (ResolveImpexGlide glide : glideList)
	                    {
	                        daoGlide.delete(glide);
	                    }
	
	                    //delete wiki
	                    ResolveImpexWikiDAO daoWiki = HibernateUtil.getDAOFactory().getResolveImpexWikiDAO();
	                    Collection<ResolveImpexWiki> wikiList = model.getResolveImpexWikis();
	                    for (ResolveImpexWiki wiki : wikiList)
	                    {
	                        daoWiki.delete(wiki);
	                    }
	
	                    //delete the rec
	                    model.setUZipFileContent(null);
	                    HibernateUtil.getDAOFactory().getResolveImpexModuleDAO().delete(model);
	                    
	                    ImpexManifestUtil.deleteManifest(model.getUName(), null, username);
	                }

                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                
                if (t instanceof Error) {
                    throw t;
                }
            }
        }//end of if
    }

    private static void deleteResolveImpexWikiBySysId(String sysId, String username)
    {
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {

	                ResolveImpexWiki wiki = HibernateUtil.getDAOFactory().getResolveImpexWikiDAO().findById(sysId);
	                if (wiki != null)
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexWikiDAO().delete(wiki);
	                }

            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

    public static void saveResolveImpexGlide(ResolveImpexGlide glide, String userName)
    {
        if (glide != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(userName);
                HibernateProxy.execute(() -> {
	                if (glide.getSys_id() == null)
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexGlideDAO().save(glide);
	                }
	                else
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexGlideDAO().update(glide);
	                }

                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

    public static void saveResolveImpexWiki(ResolveImpexWiki wiki, String userName)
    {
        if (wiki != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(userName);
                HibernateProxy.execute(() -> {
	                if (wiki.getSys_id() == null)
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexWikiDAO().save(wiki);
	                }
	                else
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexWikiDAO().update(wiki);
	                }

                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

    private static void deleteResolveImpexGlideBySysId(String sysId, String username)
    {
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {

	                ResolveImpexGlide glide = HibernateUtil.getDAOFactory().getResolveImpexGlideDAO().findById(sysId);
	                if (glide != null)
	                {
	                    HibernateUtil.getDAOFactory().getResolveImpexGlideDAO().delete(glide);
	                }

            	});
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }

    public static ImpexOptionsDTO getImpexOptions (String jsonClob) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(jsonClob, ImpexOptionsDTO.class);
    }

    public static void logImpexMessage(String log, Boolean format)
    {
        Log.log.info(log);
        if (format)
        {
            if (StringUtils.isNotBlank(log) && log.startsWith("WARNING: "))
            {
                exportLog.append("<font face=\"courier new\" color=\"#FFA500\" size=\"2\">" + log + "</font></br>");
            }
            else
            {
                exportLog.append("<font face=\"courier new\" color=\"red\" size=\"2\">" + log + "</font></br>");
            }
        }
        else
        {
            exportLog.append("<font face=\"courier new\" size=\"2\">" + log + "</font></br>");
        }
    }//logImpexMessage
    
    public static void logImpexOperation(ResolveImpexModuleVO moduleVO, String type, String userName )
    {
        LogUtil.saveImpexLog(moduleVO, exportLog, type, userName);
    }

    public static void cleanup(String folderToDelete, String zipFile)
    {
        // clear log.
        exportLog = new StringBuilder();
        
        File file = FileUtils.getFile(folderToDelete);
        if(file.isDirectory() && file.exists())
        {
            try
            {
                FileUtils.deleteDirectory(file);
            }
            catch(Exception e)
            {
                //no need to do anything
            }
        }

        if (zipFile != null)
        {
            File f = FileUtils.getFile(zipFile);
            f.delete();
        }

    }//cleanup
    
    public static boolean isSysField(String str)
    {
        boolean isSysField = false;
        if(StringUtils.isNotEmpty(str))
        {
            if(str.equals("sys_created_by") 
                            || str.equals("sys_created_on") 
                            || str.equals("sys_mod_count") 
                            || str.equals("sys_updated_by") 
                            || str.equals("sys_updated_on")
                            || str.equals("sys_org"))
            {
                isSysField  = true;
            }
        }
        
        return isSysField;
    }//isSysField
    
    @SuppressWarnings("unchecked")
    public static SysAppApplication findMenuDefinition(String appName, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		SysAppApplication result = null;

	            if (StringUtils.isNotEmpty(appName))
	            {
	                String sql = "from SysAppApplication where LOWER(name) = '" + appName.trim().toLowerCase() + "'";
	                List<SysAppApplication> list = HibernateUtil.createQuery(sql).list();
	                if (list != null && list.size() > 0)
	                {
	                    result = list.get(0);
	                }
	            }
	            
	            return result;
        	});
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return null;
    }
    
    public static String formatMenuItemDef(String oldMenuItemDef)
    {
        String result = null;
        if (oldMenuItemDef.contains("::"))
        {
            String[] arr = oldMenuItemDef.split("::");
            if (arr.length == 2)
            {
                SysAppApplicationVO sysAppAppVO = MenuDefinitionUtil.findMenuDefinition(null, arr[0], "admin");
                if (sysAppAppVO != null)
                {
                    result = sysAppAppVO.getTitle();
                    //result = arr[1] + " (" + title + ")";
                }
            }
        }
        
        return result;
    }
    
    @Deprecated
    public static void checkCurrentImpexStatus(String moduleName, String operation)
    {
        while (true)
        {
            try
            {
            	if (Log.log.isTraceEnabled()) {
	            	Log.log.trace(String.format("ImpexUtil.checkCurrentImpexStatus(%s,%s) calling " +
	            								"ServiceHibernate.getLatestImpexEventByValue()...", 
	            								moduleName, operation));
            	}
                ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
                if (Log.log.isTraceEnabled()) {
	                Log.log.trace(String.format("ImpexUtil.checkCurrentImpexStatus(%s,%s) " + 
	                							"ServiceHibernate.getLatestImpexEventByValue() returned Resolve " +
	                							"Event [%s]", 
	                							moduleName, operation, (eventVO != null ? eventVO : "null")));
                }
                
                if (eventVO == null || eventVO.getUValue().contains("-2"))
                {
                    if (eventVO == null)
                        eventVO = new ResolveEventVO();
                    eventVO.setUValue("IMPEX:" + moduleName + "," + 10 + ",0");
                    ServiceHibernate.insertResolveEvent(eventVO);
                    if (Log.log.isTraceEnabled()) {
                    	Log.log.trace(String.format("ImpexUtil.checkCurrentImpexStatus(%s,%s) inserted Resolve Event [%s]",
                    								moduleName, operation, eventVO));
                    }
                    break;
                }
                
                // check whether last update was made more than 5 minutes ago.
                if ( (new Date().getTime() - eventVO.getSysUpdatedOn().getTime() ) >= 300000)
                {
                    eventVO.setUValue("IMPEX:" + moduleName + "," + 10 + ",0");
                    ServiceHibernate.insertResolveEvent(eventVO);
                    if (Log.log.isTraceEnabled()) {
                    	Log.log.trace(String.format("ImpexUtil.checkCurrentImpexStatus(%s,%s) inserted Resolve Event [%s]",
													moduleName, operation, eventVO));
                    }
                    break;
                }
                
                Thread.sleep(500);
            }
            catch(Exception e)
            {
                Log.log.error("Error while waiting for other export to finish.", e);
            }
        }
    }
    
    @Deprecated
    public static void checkCurrentImpexManifestStatus(String moduleName)
    {
        while(true)
        {
            try
            {
                ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("IMPEX-MANIFEST");
                if (eventVO == null || eventVO.getUValue().contains("TRUE"))
                {
                    if (eventVO == null)
                        eventVO = new ResolveEventVO();
                    eventVO.setUValue("IMPEX-MANIFEST:" + moduleName + ",FALSE");
                    ServiceHibernate.insertResolveEvent(eventVO);
                    break;
                }
                
                // check whether last update was made more than 5 minutes ago.
                if ( (new Date().getTime() - eventVO.getSysUpdatedOn().getTime() ) >= 300000)
                {
                    /*
                     * The previous operation is not updating the record for a long time.
                     * It's safe to override the status and start this operation.
                     */
                    String[] parsedStatus = eventVO.getUValue().split(",");
                    eventVO.setUValue(parsedStatus[0] + ",TRUE");
                    ServiceHibernate.insertResolveEvent(eventVO);
                    
                    Thread.sleep(2000);
                    break;
                }
                
                Thread.sleep(100);
            }
            catch(Exception e)
            {
                Log.log.error("Error while waiting for other manifest to finish.", e);
            }
        }
    }
    
    public static void removeCdataFromXML(File docFile) throws IOException
    {
        String wikiDocString = FileUtils.readFileToString(docFile, "UTF-8");
        
        // take a copy of all <attachment> tags present in the document string.
        String sections[] = StringUtils.substringsBetween(wikiDocString, "<attachment>", "</attachment>");
        // remove it from the document string
        if (sections != null) {
            wikiDocString = wikiDocString.replaceAll("(?s)<attachment>.*</attachment>", "");
        }
        
        // Do the processing
        wikiDocString = cleanWikiXmlsections(wikiDocString, "<model>", "</model>");
        wikiDocString = cleanWikiXmlsections(wikiDocString, "<abort>", "</abort>");
        wikiDocString = cleanWikiXmlsections(wikiDocString, "<final>", "</final>");
        wikiDocString = cleanWikiXmlsections(wikiDocString, "<content>", "</content>");
        wikiDocString = cleanWikiXmlsections(wikiDocString, "<decisionTree>", "</decisionTree>");
        
        if (sections != null) {
            // write back the copied <attachment> tags
            int startIndex = wikiDocString.indexOf("</xwikidoc>");
            StringBuilder builder = new StringBuilder(wikiDocString);
            for (String section : sections) {
                builder.insert (startIndex, "<attachment>" + section + "</attachment>\n");
            }
            FileUtils.writeStringToFile(docFile, builder.toString(), StandardCharsets.UTF_8);
        } else {
            FileUtils.writeStringToFile(docFile, wikiDocString, StandardCharsets.UTF_8);
        }
    }
    
    public static String cleanWikiXmlsections(String wikiDocXml, String startTag, String endTag)
    {
        String section = StringUtils.substringBetween(wikiDocXml, startTag, endTag);
        if (section != null)
        {
            if (section.startsWith("<![CDATA["))
            {
                section = section.substring(9, section.length() - 3);
                section = section.replace("]]>", "]]]]><![CDATA[>");
                
                int startIndex = wikiDocXml.indexOf(startTag);
                int lastIndex = wikiDocXml.lastIndexOf(endTag);
                StringBuilder sb = new StringBuilder(wikiDocXml);
                wikiDocXml = sb.replace(startIndex+startTag.length(), lastIndex, "<![CDATA[" + section + "]]>").toString();
            }
            else
            {
            	int startIndex = wikiDocXml.indexOf(startTag);
                int lastIndex = wikiDocXml.lastIndexOf(endTag);
                StringBuilder sb = new StringBuilder(wikiDocXml);
                wikiDocXml = sb.replace(startIndex+startTag.length(), lastIndex, "<![CDATA[" + section + "]]>").toString();
            }
        }
        
        return wikiDocXml;
    }
    
    public static void clearLogs()
    {
    	exportLog.setLength(0);
    }
    
    public static void finishOperation(String operation, String task, String moduleName) throws Exception {
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        operation = String.format("IMPEX-%s", operation.toUpperCase());
        
        if (eventVO == null) {
            eventVO = new ResolveEventVO();
        }
        String value = String.format("%s for %s:%s,-2, ", operation, task, moduleName);
        eventVO.setUValue(value);
        ServiceHibernate.insertResolveEvent(eventVO);
    }
}
