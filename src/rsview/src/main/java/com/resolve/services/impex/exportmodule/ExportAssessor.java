/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.List;
import java.util.Set;

import com.resolve.persistence.model.ResolveAssess;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportAssessor extends ExportComponent
{
    protected ResolveAssess resolveAssess;
    protected ExportActionTask exportActionTask;
    
    public ExportAssessor(ResolveAssess vo, ExportActionTask exportActionTask)
    {
        this.resolveAssess = vo;
        this.exportActionTask = exportActionTask;
    }
    
    public List<ManifestGraphDTO> export()
    {
        List<ManifestGraphDTO> list = null;
        populateAssessor();

        if (exportActionTask.impexOptions.getAtProperties())
        {
            list = processAssessorProperties();
        }
        return list;
    }
    
    public void populateAssessor()
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveAssess");
        dto.setSys_id(resolveAssess.getSys_id());
        dto.setFullName(resolveAssess.getUName());
        dto.setName(resolveAssess.getUName());
        dto.setGroup(exportActionTask.ACTIONTASK + " " + resolveAssess.getUName());
        dto.setType(exportActionTask.ASSESSOR);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        exportActionTask.getParentGraphDTO().getIds().add(resolveAssess.getSys_id());
    }
    
    public List<ManifestGraphDTO> processAssessorProperties()
    {
        List<ManifestGraphDTO> list = null;
    	if (StringUtils.isNotBlank(resolveAssess.getUScript()))
    	{
    		list = processProperty(resolveAssess.getUScript(), resolveAssess.getUName());
    	}
    	return list;
    }
}