/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.dom4j.Element;

import com.resolve.services.ServiceCatalog;
import com.resolve.services.catalog.Catalog;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class CatalogValidationUtil
{
    protected String MODULE_FOLDER_LOCATION;
    Set<String> catalogNameSet = new HashSet<String>();
    protected Map<String, Catalog> catalogMap = new HashMap<String, Catalog>();
    
    public CatalogValidationUtil(String moduleFolderLocation)
    {
        this.MODULE_FOLDER_LOCATION = moduleFolderLocation;
    }
    
    public void validateCatalogs()
    {
        String socialFolderLocation = MODULE_FOLDER_LOCATION + "social/";
        File socialFolder = FileUtils.getFile(socialFolderLocation);
        
        if (socialFolder.isDirectory())
        {
            // Import has social components
            File installFolder = FileUtils.getFile(socialFolderLocation + "install");
            if (installFolder.isDirectory())
            {
                List<File> files = new ArrayList<File>();
                WildcardFileFilter wildCardFileFilter = new WildcardFileFilter("resolvecatalog_*.xml", null);
                files.addAll(FileUtils.listFiles(installFolder, wildCardFileFilter, null));
                
                if (!files.isEmpty())
                {
                    try
                    {
                        for (File file : files)
                        {
                            XDoc xDoc = new XDoc(file);
                            Map<String, String> mapValue = prepareMapFromXML(xDoc);
                            
//                            String sys_id = mapValue.get("sys_id");
                            String rootCatalogName = mapValue.get("rootName");
                            if (rootCatalogName != null)
                            {
                                catalogNameSet.add(rootCatalogName);
                            }
                        }
                        
                        if (!catalogNameSet.isEmpty())
                        {
                            List<Catalog> catalogList = ServiceCatalog.getAllCatalogs(null, "admin");
                            for (String catalogName : catalogNameSet)
                            {
                                for (Catalog catalog : catalogList)
                                {
                                    if (catalog.getName().equals(catalogName))
                                    {
                                        // Found a Catalog. Delete it. Import operation will create it again.
                                        ServiceCatalog.deleteCatalog(catalog.getId(), null, "admin");
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, String> prepareMapFromXML(XDoc doc)
    {
        HashMap<String, String> row = new HashMap<String, String>();
        
        List<Element> columns = doc.getNodes("/graph_node/resolvecatalog/*");
        for(Element e : columns)
        {
            String name = e.getName();
            String value = e.getText();
            row.put(name, value);
        }//end of for loop
        
        return row;
    }
}
