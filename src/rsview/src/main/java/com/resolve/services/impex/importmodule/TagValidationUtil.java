/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.importmodule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.dom4j.Element;

import com.resolve.services.ServiceTag;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class TagValidationUtil
{
    protected String MODULE_FOLDER_LOCATION;
    protected String socialFolderLocation;
    
    public TagValidationUtil(String moduleFolderLocation)
    {
        this.MODULE_FOLDER_LOCATION = moduleFolderLocation;
    }
    
    public void validateTags()
    {
        socialFolderLocation = MODULE_FOLDER_LOCATION + "social/";
        File socialFolder = FileUtils.getFile(socialFolderLocation);
        
        if (socialFolder.isDirectory())
        {
            // Import has social components
            File installFolder = FileUtils.getFile(socialFolderLocation + "install");
            if (installFolder.isDirectory())
            {
                List<File> files = new ArrayList<File>();
                WildcardFileFilter wildCardFileFilter = new WildcardFileFilter("resolvetag_*.xml", null);
                files.addAll(FileUtils.listFiles(installFolder, wildCardFileFilter, null));
                
                if (!files.isEmpty())
                {
                    try
                    {
                    	ImpexUtil.initImpexOperationStage("IMPORT", "(23/51) Validate Resolve Tag Glide Components", files.size());
                        for (File file : files)
                        {
                        	ImpexUtil.updateImpexCount("IMPORT");
                            XDoc xDoc = new XDoc(file);
                            Map<String, String> mapValue = prepareMapFromXML(xDoc);
                            String id = mapValue.get("sys_id");
                            String tagName = mapValue.get("displayName");
                            ResolveTagVO tag = getSystemTagByName(tagName);
                            if (tag != null)
                            {
                                // tag with the same name exist on the system.
                                replaceAllSysIds(id, tag.getSys_id());
                            }
                        }
                    }
                    catch(Exception e)
                    {
                    	Log.log.error(StringUtils.isNotBlank(e.getMessage()) ? 
          					  		  e.getMessage() : "Failed to Validate Resolve Tag Glide Components.", e);
                    	ImpexUtil.logImpexMessage(StringUtils.isNotBlank(e.getMessage()) ? 
    								  		  	  e.getMessage() : 
    								  		  	  "Failed to Validate Resolve Tag Glide Components. Please check RSView " +
    								  		  	  "logs for details", 
    								  		  	  true);
                    }
                }
            }
        }
    }
    
    protected ResolveTagVO getSystemTagByName(String tagName)
    {
        ResolveTagVO tag = null;
        
        try
        {
            tag = ServiceTag.getTag(null, tagName, "admin");
        }
        catch (Exception e)
        {
            // Nothing to do here.
        }
        
        return tag;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, String> prepareMapFromXML(XDoc doc)
    {
        HashMap<String, String> row = new HashMap<String, String>();
        
        List<Element> columns = doc.getNodes("/graph_node/resolvetag/*");
        for(Element e : columns)
        {
            String name = e.getName();
            String value = e.getText();
            row.put(name, value);
        }//end of for loop
        
        return row;
    }
    
    public void replaceAllSysIds(String sourceSysId, String destSysId)
    {
        Collection<File> fileCollection = FileUtils.listFiles(FileUtils.getFile(socialFolderLocation),  null, true);
        Iterator<File> iterator = fileCollection.iterator();
        Log.log.info("Replacing catalog source id: " + sourceSysId + " to: " + destSysId);
        // System.out.println("Replacing source: " + sourceSysId + " to: " + destSysId);
        while (iterator.hasNext())
        {
            File file = iterator.next();
            String content;
            try
            {
                content = FileUtils.readFileToString(file);
                content = content.replaceAll(sourceSysId , destSysId);
                FileUtils.writeStringToFile(file, content);
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
}
