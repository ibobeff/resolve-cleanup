/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.menu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.SysPerspective;
import com.resolve.persistence.model.SysPerspectiveapplications;
import com.resolve.persistence.model.SysPerspectiveroles;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.vo.SysPerspectiveVO;
import com.resolve.services.hibernate.vo.SysPerspectiveapplicationsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MenuSetUtil
{
    public static List<SysPerspectiveVO> getMenuSets(QueryDTO query, String username)
    {
        List<SysPerspectiveVO> result = new ArrayList<SysPerspectiveVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
//                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        SysPerspective instance = new SysPerspective();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        SysPerspective instance = (SysPerspective) o;
                        
                        //we have to reload the collections also...so doing this
                        result.add(findMenuSetByIdOrByName(instance.getSys_id(), null, username));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static SysPerspectiveVO findMenuSetByIdOrByName(String sys_id, String name, String username)
    {
        SysPerspective model = null;
        SysPerspectiveVO result = null;
        
        try
        {
            if(StringUtils.isNotBlank(sys_id))
            {
                model = findSysPerspectiveModelById(sys_id, null, username);
            }
            else if(StringUtils.isNotBlank(name))
            {
                model = findSysPerspectiveModelById(null, name, username);
            }
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        
        
        if(model != null)
        {
            result = model.doGetVO();
            populateMenuItemsDisplayNames(result);
        }
        
        return result;
    }
    
    public static void deleteMenuSets(String[] sysIds, QueryDTO query, String username) throws Exception
    {
        if (sysIds != null)
        {
            //TODO: prepare the list of sys_ids to delete based on qry
            if (sysIds == null || sysIds.length == 0)
            {
                //prepare the sysIds with the where clause of this obj
                String selectQry = query.getSelectHQL();
                String sql = "select sys_id " + selectQry.substring(selectQry.indexOf("from"));
                System.out.println("MenuSetUtil: #108: Delete qry :" + sql);
            }

            //delete one by one
            for (String sysId : sysIds)
            {
                deleteSysPerspectiveById(sysId, username);
            }
            
            //invalidate the menu cache
            MenuServiceUtil.invalidateMenuCache();
        }
    }
    
    public static SysPerspectiveVO saveSysPerspective(SysPerspectiveVO vo, String username)  throws Exception
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        if (vo != null && vo.validate())
        {
            SysPerspective model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                //update
                model = findSysPerspectiveModelById(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("Menu Set with sysId " + vo.getSys_id() + " does not exist.");
                }
                
                //check if the title is same or not, if not, than validate if the title is unique
                //this will throw exception if there are more than 1 recs with the same title
                SysPerspective duplicate = findSysPerspectiveModelById(null, vo.getName(), username);
                if(duplicate != null && !duplicate.getSys_id().equalsIgnoreCase(vo.getSys_id()))
                {
                    throw new Exception("'" + vo.getName() + "' already exist. Please try again.");
                }
                
            }
            else
            {
                //insert
                model = findSysPerspectiveModelById(null, vo.getName(), username);
                if(model != null)
                {
                    throw new Exception("'" + vo.getName() + "' already exist. Please try again.");
                }
                
                int maxSequence = getLatestSequenceNumberForMenu();

                model = new SysPerspective();
                model.setSequence(maxSequence + 1);
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveSysPerspective(model, username);
            
            //add roles and apps to menu set
            updateReferencesForMenuSet(model, vo, username);
            
            //get the updated info
            vo = findMenuSetByIdOrByName(model.getSys_id(), null, username);
            
            //invalidate the menu cache
            MenuServiceUtil.invalidateMenuCache();

        }
        
        return vo;
    }
    
    public static void setMenuSetSequence(String[] menuSetSysIds, String username)
    {
        if (menuSetSysIds != null)
        {            
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

                	int sequence = 1;
	                for(String sysId : menuSetSysIds)
	                {
	                    SysPerspective menuSet = HibernateUtil.getDAOFactory().getSysPerspectiveDAO().findById(sysId);
	                    if(menuSet != null)
	                    {
	                        menuSet.setSequence(sequence++);
	                        HibernateUtil.getDAOFactory().getSysPerspectiveDAO().persist(menuSet);
	                    }
	                }

                });
                
                //invalidate the menu cache
                MenuServiceUtil.invalidateMenuCache();

            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);                
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }
    
    //private apis
    @SuppressWarnings("unchecked")
    private static SysPerspective findSysPerspectiveModelById(String sys_id, String name, String username)   throws Exception
    {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (SysPerspective) HibernateProxy.execute(() -> {
        		SysPerspective result = null;

	            if (StringUtils.isNotEmpty(sys_id))
	            {
	                result = HibernateUtil.getDAOFactory().getSysPerspectiveDAO().findById(sys_id);
	            }
	            else if (StringUtils.isNotEmpty(name))
	            {
	                String sql = "from SysPerspective where LOWER(name) = '" + name.trim().toLowerCase() + "'";
	                List<SysPerspective> list = HibernateUtil.createQuery(sql).list();
	                if (list != null && list.size() > 0)
	                {
	                    if(list.size() == 1)
	                        result = list.get(0);
	                    else
	                        throw new Exception("There is more than one Menu Definition with Title " + name);
	                }
	            }
	            
	
	            if (result != null)
	            {
	                if (result.getSysPerspectiveapplications() != null) result.getSysPerspectiveapplications().size();
	
	                if (result.getSysPerspectiveroles() != null) result.getSysPerspectiveroles().size();
	
	            }

	            return result;
        	});

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    }
    
    private static void deleteSysPerspectiveById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
	
	            SysPerspective model = HibernateUtil.getDAOFactory().getSysPerspectiveDAO().findById(sysId);
	            if (model != null)
	            {
	                //delete the roles
	                Collection<SysPerspectiveroles> sysPerspectiveroles = model.getSysPerspectiveroles();
	                if(sysPerspectiveroles != null)
	                {
	                    for(SysPerspectiveroles role : sysPerspectiveroles)
	                    {
	                        HibernateUtil.getDAOFactory().getSysPerspectiverolesDAO().delete(role);
	                    }
	                }
	                
	                //delete the application
	                Collection<SysPerspectiveapplications> sysPerspectiveapplications = model.getSysPerspectiveapplications();
	                if(sysPerspectiveapplications != null)
	                {
	                    for(SysPerspectiveapplications app : sysPerspectiveapplications)
	                    {
	                        HibernateUtil.getDAOFactory().getSysPerspectiveapplicationsDAO().delete(app);
	                    }
	                }
	                
	                //delete the main rec
	                HibernateUtil.getDAOFactory().getSysPerspectiveDAO().delete(model);
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    private static void updateReferencesForMenuSet(SysPerspective inModel, SysPerspectiveVO vo, String username)
    {
        Set<String> roles = new HashSet<String>();
        if(vo.getRoles()!=null)
        {
            roles.addAll(vo.getRoles());
        }
        List<SysPerspectiveapplicationsVO> apps = new ArrayList<SysPerspectiveapplicationsVO>(vo.getSysPerspectiveapplications());//Applications();
        if(apps != null)
        {
            int sequence = 1;
            for(SysPerspectiveapplicationsVO appVO : apps)
            {
                appVO.setSequence(new Integer(sequence++));
            }
        }
        
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

            	SysPerspective model = HibernateUtil.getDAOFactory().getSysPerspectiveDAO().findById(inModel.getSys_id());
	            if (model != null)
	            {
	                //delete the roles
	                Collection<SysPerspectiveroles> sysPerspectiveroles = model.getSysPerspectiveroles();
	                if(sysPerspectiveroles != null)
	                {
	                    for(SysPerspectiveroles role : sysPerspectiveroles)
	                    {
	                        String existingRole = role.getValue();
	                        boolean roleExist = roles.contains(existingRole);
	                        if(roleExist)
	                        {
	                            //don't touch it...leave it alone..remove it from the UI list as it already exist
	                            roles.remove(role.getValue());                            
	                        }
	                        else
	                        {
	                            //delete it as it has been removed from UI
	                            HibernateUtil.getDAOFactory().getSysPerspectiverolesDAO().delete(role);
	                        }                       
	                    }
	                }
	
	                for (String role : roles)
	                {
	                    //create a new one
	                    SysPerspectiveroles newRec = new SysPerspectiveroles();
	                    newRec.setSequence(0);
	                    newRec.setSysPerspective(model);
	                    newRec.setValue(role);
	
	                    HibernateUtil.getDAOFactory().getSysPerspectiverolesDAO().persist(newRec);
	                }
	                
	                //application manipulation
	                Collection<SysPerspectiveapplications> sysPerspectiveapplications = model.getSysPerspectiveapplications();
	                if(sysPerspectiveapplications != null)
	                {
	                    for(SysPerspectiveapplications dbApp : sysPerspectiveapplications)
	                    {
	                        String dbAppValue = dbApp.getValue();
	                        SysPerspectiveapplicationsVO uiApp = getUIApp(dbAppValue, apps);
	                        if(uiApp != null)
	                        {
	                            //remove it from the list as it already exist
	                            dbApp.setSequence(uiApp.getSequence());
	                            HibernateUtil.getDAOFactory().getSysPerspectiveapplicationsDAO().persist(dbApp);
	                            
	                            apps.remove(uiApp);
	                        }
	                        else
	                        {
	                            //delete it as it is removed from the UI
	                            HibernateUtil.getDAOFactory().getSysPerspectiveapplicationsDAO().delete(dbApp);
	                        }                        
	                    }
	                }
	                
	                for (SysPerspectiveapplicationsVO app : apps)
	                {
	                    //create a new one only
	                    SysPerspectiveapplications newApp = new SysPerspectiveapplications();
	                    newApp.setSequence(app.getSequence());
	                    newApp.setValue(app.getValue());
	                    newApp.setSysPerspective(model);
	                    
	                    HibernateUtil.getDAOFactory().getSysPerspectiveapplicationsDAO().persist(newApp);
	                }
	                
	                //save the main rec
	                HibernateUtil.getDAOFactory().getSysPerspectiveDAO().persist(model);
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }  
        
    }
    
    private static void populateMenuItemsDisplayNames(SysPerspectiveVO result)
    {
        if(result != null)
        {
            List<SysPerspectiveapplicationsVO> apps = result.getSysPerspectiveapplications();
            if(apps != null)
            {
                Set<String> appNames = new HashSet<String>();
                for(SysPerspectiveapplicationsVO app : apps)
                {
                    String name = app.getValue();
                    if(StringUtils.isNotEmpty(name))
                        appNames.add(name);
                }
                
                Map<String, String> appNameValueMap = getApplicationNamesFor(appNames);
                List<String> applications = new ArrayList<String>();
                for(SysPerspectiveapplicationsVO app : apps)
                {
                    String name = appNameValueMap.get(app.getValue());
                    app.setDisplayName(name);
                    applications.add(name);
                }
                //update the names of applications to display on the grid
                result.setApplications(applications);
            }            
        }
    }
    
    //returns map of value-displayName
    private static Map<String, String> getApplicationNamesFor(Set<String> appNames)
    {
        Map<String, String> appNameValueMap = new HashMap<String, String>();
        
        if(appNames != null && appNames.size() > 0)
        {
            String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
            //String namesSqlStr = SQLUtils.prepareQueryString(new ArrayList<String>(appNames), dbType);
            //String hql = "select name, title from SysAppApplication where name IN (" + namesSqlStr + ")";
            String hql = "select name, title from SysAppApplication where name IN (:appName)";
            
            Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
            
            queryInParams.put("appName", new ArrayList<Object>(appNames));
            
            try
            {
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectInOnly(hql, queryInParams);
                if(data != null && data.size() > 0)
                {
                    for(Object o: data)
                    {
                        Object[] record = (Object[]) o;
                        String name = (String) record[0];
                        String value = (String) record[1];
                        
                        appNameValueMap.put(name, value);
                    }
                }
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }

        return appNameValueMap;
    }
    
    private static SysPerspectiveapplicationsVO getUIApp(String dbAppValue, List<SysPerspectiveapplicationsVO> uiApps)
    {
        SysPerspectiveapplicationsVO vo = null;
        
        if(StringUtils.isNotBlank(dbAppValue))
        {
            for(SysPerspectiveapplicationsVO uiApp : uiApps)
            {
                String uiAppValue = uiApp.getValue();
                if(dbAppValue.equalsIgnoreCase(uiAppValue))
                {
                    vo = uiApp;
                    break;
                }
            }
        }
        
        return vo;
        
    }
    
    private static int getLatestSequenceNumberForMenu()
    {
        int result = 0;
        String hql = "select MAX(a.sequence) from SysPerspective a";
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(hql, new HashMap<String, Object>());
            if(data != null && data.size() > 0)
            {
                result = (Integer) data.get(0);
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
}
