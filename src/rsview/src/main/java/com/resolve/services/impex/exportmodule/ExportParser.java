/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.List;
import java.util.Set;

import com.resolve.persistence.model.ResolveParser;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportParser extends ExportComponent
{
    protected ResolveParser resolveParser;
    protected ExportActionTask exportActionTask;
    
    public ExportParser(ResolveParser vo, ExportActionTask exportActionTask)
    {
        this.resolveParser = vo;
        this.exportActionTask = exportActionTask;
    }
    
    public List<ManifestGraphDTO> export()
    {
        List<ManifestGraphDTO> list = null;
        populateAssessor();
        if (exportActionTask.impexOptions.getAtProperties())
        {
            list = processParserProperties();
        }
        
        return list;
    }
    
    public void populateAssessor()
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveParser");
        dto.setSys_id(resolveParser.getSys_id());
        dto.setFullName(resolveParser.getUName());
        dto.setName(resolveParser.getUName());
        dto.setGroup(exportActionTask.ACTIONTASK + " " + resolveParser.getUName());
        dto.setType(exportActionTask.PARSER);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        exportActionTask.getParentGraphDTO().getIds().add(resolveParser.getSys_id());
    }
    
    public List<ManifestGraphDTO> processParserProperties()
    {
        List<ManifestGraphDTO> list = null;
    	if (StringUtils.isNotBlank(resolveParser.getUScript()))
    	{
    		list = processProperty(resolveParser.getUScript(), resolveParser.getUName());
    	}
    	return list;
    }
    
}