/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.SystemScriptUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.ResolveSysScriptVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;

public class ExportSystemScript extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String scriptName;
    
    public ExportSystemScript(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
        this.scriptName = impexGlideVO.getUModule();
    }
    
    public ManifestGraphDTO export()
    {
        ResolveSysScriptVO scriptVO = SystemScriptUtil.findResolveSysScriptByIdOrName(null, scriptName, userName);
        return populateSystemScript(scriptVO);
    }
    
    private ManifestGraphDTO populateSystemScript(ResolveSysScriptVO scriptVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.ResolveSysScript");
        
        dto.setSys_id(scriptVO.getSys_id());
        dto.setFullName(scriptVO.getUName());
        dto.setName(scriptVO.getUName());
        dto.setType(SYSTEM_SCRIPT);
        dto.setDisplayType(SYSTEM_SCRIPT);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        
        ManifestGraphDTO graphDTO = new ManifestGraphDTO();
        graphDTO.setName(scriptVO.getUName());
        graphDTO.setId(scriptVO.getSys_id());
        graphDTO.setType(ExportUtils.SYSTEM_SCRIPT);
        graphDTO.setChecksum(scriptVO.getChecksum());
        return graphDTO;
    }
}
