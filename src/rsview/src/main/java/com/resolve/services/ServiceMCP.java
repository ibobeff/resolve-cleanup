/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsbase.MainBase;
import com.resolve.persistence.model.MCPFile;
import com.resolve.persistence.model.MCPServer;
import com.resolve.persistence.model.MetricThreshold;
import com.resolve.persistence.util.DataAPI;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.Worksheet;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.hibernate.util.MCPUtils;
import com.resolve.services.hibernate.util.MetricUtil;
import com.resolve.services.hibernate.vo.MCPComponentVO;
import com.resolve.services.hibernate.vo.MCPFileContentVO;
import com.resolve.services.hibernate.vo.MCPFileVO;
import com.resolve.services.hibernate.vo.MCPServerVO;
import com.resolve.services.hibernate.vo.ResolveMCPGroupVO;
import com.resolve.services.vo.MCPFileTreeDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.MCPConstants;
import com.resolve.util.MCPLog;
import com.resolve.util.StringUtils;

public class ServiceMCP
{

    private static String PROCESS_ID = "_RESOLVE_RUNBOOK_PROCESS_ID_";
    private static String PROCESS_TIMEOUT = "_RESOLVE_RUNBOOK_PROCESS_TIMEOUT_";
    private static final String STATUS = "STATUS";
    private static final String NUMBER = "NUMBER";
    private static final String LAST_UPDATED = "LAST_UPDATED";

    
    ///////////////////////////////////////////
    // New MCP Service API's
    ///////////////////////////////////////////
    
    public static void registerInstance(String blueprint) throws IOException
    {
        MCPUtils.registerInstance(blueprint);
    }
    
    public static String  deregisterInstance(String clusterName, String hostName) throws Exception
    {
        return MCPUtils.deregisterInstance(clusterName, hostName);
    }
    
    public static List<ResolveMCPGroupVO> getRegisteredInstances(boolean withBlueprintFile)
    {
        return MCPUtils.getRegisteredInstances(withBlueprintFile);
    }
    
    public static List<MetricThreshold> findAutoDeploymentTargets()
    {
        return MetricUtil.getAutoDeploymentMetricThresholds();
    }
    ///////////////////////////////////////////
    // New MCP Service API's
    ///////////////////////////////////////////
    public static List<MCPServerVO> getAllMCPServer(QueryDTO query, String username) throws Exception
    {
        List<MCPServerVO> result = new ArrayList<MCPServerVO>();

        try
        {
            result = MCPUtils.getAllMCPServer(query, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return result;
    }

    public static MCPServerVO saveMCPServer(MCPServerVO vo, String username) throws Exception
    {
        MCPServerVO result = null;

        if (vo == null)
        {
            throw new Exception("The entire data object is null, please check your code!");
        }
        else
        {
            result = MCPUtils.saveMCPServer(vo, username);
        }

        return result;
    }

    public static MCPServerVO findServerById(String id, String username) throws Exception
    {
        MCPServerVO result = null;

        if (StringUtils.isBlank(id))
        {
            throw new Exception("Nothing to find as the id is null/empty, please check your code!");
        }
        else
        {
            MCPServer model = MCPUtils.findServerById(id, username);
            if (model != null)
            {
                result = model.doGetVO();
            }
        }

        return result;
    }

    public static MCPServerVO findServerByIP(String serverIp, String username) throws Exception
    {
        MCPServerVO result = null;

        if (StringUtils.isBlank(serverIp))
        {
            throw new Exception("Nothing to find as the IP is null/empty, please check your code!");
        }
        else
        {
            MCPServer model = MCPUtils.findServerByIP(serverIp, username);
            if (model != null)
            {
                result = model.doGetVO();
            }
        }

        return result;
    }

    public static boolean deleteMCPServer(String id, String username) throws Exception
    {
        boolean result = false;

        if (StringUtils.isBlank(id))
        {
            throw new Exception("Nothing to delete as the id is null/empty, please check your code!");
        }
        else
        {
            result = MCPUtils.deleteServerById(id, username);
        }

        return result;
    }
    
    public static boolean deleteMCPFile(String id, String username) throws Exception
    {
        boolean result = false;

        if (StringUtils.isBlank(id))
        {
            throw new Exception("Nothing to delete as the id is null/empty, please check your code!");
        }
        else
        {
            result = MCPUtils.deleteFileById(id, username);
        }

        return result;
    }

    public static boolean updateServerStatus(String id, String problemId, String status, String username, String version, String build) throws Exception
    {
        boolean result = false;

        if (StringUtils.isBlank(id))
        {
            throw new Exception("Nothing to update as the id is null/empty, please check your code!");
        }
        else
        {
            MCPServer model = MCPUtils.findServerById(id, username);
            if (model != null)
            {
                model.setStatus(status);
                Map<String, Object> worksheet = DataAPI.findWorksheetById(problemId);
                String problemNo = null;

                if (worksheet != null && worksheet.containsKey("u_number"))
                {
                    problemNo = (String) worksheet.get("u_number");
                }
                if (StringUtils.isNotBlank(version))
                {
                    model.setVersion(version);
                }
                if (StringUtils.isNotBlank(build))
                {
                    model.setBuild(build);
                }
                model.setWorksheetId(problemId);
                model.setWorksheet(problemNo);
                MCPUtils.saveMCPServer(model.doGetVO(), username);
                result = true;
            }
        }

        return result;
    }

    /**
     * This method deploys blueprint in remote server.
     *
     * @param params
     */
    public static void getProcessStatus(String processID)
    {
        try
        {
            Map<String, String> status = getProcessStatusx(processID);

            // processid
            status.remove(PROCESS_ID);
            String state = status.get(STATUS).trim();
            if (processID == null)
            {
                throw new RuntimeException("Missing PROCESSID for execution");
            }

            // process timeout
            String processTimeoutStr = status.get(PROCESS_TIMEOUT);
            int processTimeout = Integer.parseInt(processTimeoutStr) * 60;

            int maxWaitTime = 1;
            while (StringUtils.isEmpty(state) || "OPEN".equals(state))
            {
                // quit out after 20 minutes if runbook is still not finished or
                // aborted.
                if (maxWaitTime++ > processTimeout)
                {
                    throw new RuntimeException("Failed to finish runbook in " + processTimeoutStr + " minutes. Runbook processID is " + processID);
                }

                try
                {
                    // Sleep for one second to wait for runbook to finish
                    Thread.currentThread().sleep(1000);
                }
                catch (Exception ex)
                {
                    Log.log.info("Exception is thrown when waiting for runbook to finish", ex);
                    throw new RuntimeException(ex);
                }
                status = getProcessStatusx(processID);
                //System.out.println("status: " + status);
                state = status.get(STATUS).trim();
            }

            status.put(Constants.EXECUTE_PROCESSID, processID);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage());
        }
    }

    private static Map<String, String> getProcessStatusx(String processID)
    {
        HashMap<String, String> result = new HashMap<String, String>();

        try
        {
            // init status
            result.put(STATUS, "");

            // check if request entry exists
            ProcessRequest process = WorksheetSearchAPI.findProcessRequestsById(processID, "system").getData();

            if (process != null)
            {
                String status = process.getStatus();
                if (status == null)
                {
                    status = "";
                }
                result.put(STATUS, status);

                String problemID = process.getProblem();
                if (problemID != null)
                {
                    result.put(Constants.EXECUTE_PROBLEMID, problemID);

                    Worksheet wsh = WorksheetSearchAPI.findWorksheetById(problemID, "system").getData();
                    if (wsh != null)
                    {
                        result.put(NUMBER, wsh.getNumber());
                    }
                }

                result.put(LAST_UPDATED, "" + process.getSysUpdatedOn());
                result.put(Constants.EXECUTE_WIKI, process.getWiki());
            }
            else
            {
                throw new Exception("Invalid processID: " + processID);
            }
        }
        catch (Throwable e)
        {
            Log.log.warn("Failed to retrive process status for processid : " + processID, e);
            result.put("Error", e.getMessage());
        }
        return result;
    }

    public static void updateActualBlueprint(String mcpServerId, String actualBlueprint, String username) throws Exception
    {
        if (StringUtils.isBlank(mcpServerId))
        {
            throw new Exception("Nothing to update as the id is null/empty, please check your code!");
        }
        else
        {
            MCPServer model = MCPUtils.findServerById(mcpServerId, username);
            if (model != null)
            {
                model.setActualBlueprint(actualBlueprint);
                MCPUtils.saveMCPServer(model.doGetVO(), username);
            }
        }
    }

    public static void updateComponentStatus(String mcpServerId, String statusKey, String componentStatus, String problemId, String username) throws Exception
    {
        if (StringUtils.isBlank(mcpServerId))
        {
            throw new Exception("Nothing to update as the id is null/empty, please check your code!");
        }
        else
        {
            MCPServer model = MCPUtils.findServerById(mcpServerId, username);
            if (model != null)
            {
                MCPLog log = new MCPLog();
                log.load(componentStatus);
                //RSMGMT#3E9C9FECC89705319763CB46C772C39A|=|START#SUCCESS|&|
                List<Map<String, String>> data = log.getData();

                List<MCPComponentVO> components = MCPUtils.findComponentByServerIP(model.getServerIp(), username);

                Map<String, Object> worksheet = DataAPI.findWorksheetById(problemId);
                String problemNo = null;

                if (worksheet != null && worksheet.containsKey("u_number"))
                {
                    problemNo = (String) worksheet.get("u_number");
                }

                for (MCPComponentVO component : components)
                {
                    component.setWorksheetId(problemId);
                    component.setWorksheet(problemNo);
                    setComponentStatus(component, data, statusKey);
                    MCPUtils.saveMCPComponent(component);
                }
                model.setComponentStatus(componentStatus);

                MCPUtils.saveMCPServer(model.doGetVO(), username);
            }
        }
    }
    
    public static List<MCPFileVO> getAllMCPFiles(QueryDTO query, String username) throws Exception
    {
        List<MCPFileVO> result = new ArrayList<MCPFileVO>();

        try
        {
            result = MCPUtils.getAllMCPFile(query, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return result;   
    }
    
    public static List<MCPFileTreeDTO> getMCPFileTree(QueryDTO queryDTO, String field, String username) throws Exception
    {
        List <MCPFileTreeDTO> result = new ArrayList<MCPFileTreeDTO>();
        
        try
        {
            result = MCPUtils.getMCPFileTree(queryDTO, field, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return result;
    }
    
    public static MCPFileVO getMCPFileWithContent(String id, String username) throws Exception
    {
        MCPFileVO result = null;
        try
        {
            MCPFile model = MCPUtils.findFileById(id, username);
            if (model != null)
            {
                MCPUtils.populateMCPFileContent(model);
                result = model.doGetVO();
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return result;
    }
    
    public static void updateMCPFile(String fileName, String fileType, byte[] fileContent, String fileVersion, Date fileBuildDate, String username, String mcpServerId) throws Exception
    {
        MCPServerVO server = null;
        if (StringUtils.isNotBlank(mcpServerId))
        {
            server = findServerById(mcpServerId, username);
        }
        updateMCPFile(fileName, fileType, fileContent, fileVersion, fileBuildDate, username, server);
    } //updateMCPFile
    
    public static void updateMCPFile(String fileName, String fileType, byte[] fileContent, String fileVersion, Date fileBuildDate, String username, MCPServerVO server) throws Exception
    {
        if (StringUtils.isBlank(fileName))
        {
            throw new Exception("Nothing to update as the file name is null/empty, please check your code!");
        }
        else
        {
            List<MCPFileVO> files = MCPUtils.findFiles(fileName, fileVersion, fileBuildDate, username);
            MCPFileVO mcpFile;
            if (files == null || files.size() != 1)
            {
                mcpFile = new MCPFileVO();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                Log.log.warn("Unable to Find Unique MCP File for " + fileName + ", " + fileVersion + ", " + sdf.format(fileBuildDate));
            }
            else
            {
                mcpFile = files.get(0);
            }
            
            mcpFile.setName(fileName);
            
            String type = fileType != null ? fileType : mcpFile.getType() != null ? mcpFile.getType() : "UNKNOWN";
            mcpFile.setType(type);
            
            if (fileContent != null)
            {
                byte[] content = fileContent;
                MCPFileContentVO mcpFileContent = new MCPFileContentVO();
                mcpFileContent.setContent(content);
                mcpFile.usetMCPFileContent(mcpFileContent);
                mcpFile.setSize(content.length);
            }
            
            String version = fileVersion != null ? fileVersion : mcpFile.getVersion() != null ? mcpFile.getVersion() : MainBase.main.release.getVersion();
            mcpFile.setVersion(version);
            
            Date date = fileBuildDate != null ? fileBuildDate : mcpFile.getDate() != null ? mcpFile.getDate() : new Date();
            mcpFile.setDate(date);
            
            if (server != null)
            {
                mcpFile.setServerID(server.getSys_id());
                mcpFile.setServerName(server.getName());
            }
            
            MCPUtils.saveMCPFile(mcpFile, null);
        }
    } //updateMCPFile
    
    public static void uploadUpdate(String username, HttpServletRequest request) throws Exception
    {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart)
        {
            //if not an upload request, then return
            throw new Exception("Request object does not have any multipart content to upload.");
        }
            
        // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();
            
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
            
        List<FileItem> items = upload.parseRequest(request);
            
        // Process the uploaded items
        Iterator<FileItem> iter = items.iterator();
        while (iter.hasNext())
        {
            FileItem item = iter.next();

            if (item.isFormField())
            {
                //do nothing
            }
            else
            {
                String fileName = item.getName();
                fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
                Pattern p = Pattern.compile("((?:update)|(?:upgrade))-(?:rsremote-)?(.+)-(\\d{8}).zip");
                Matcher m = p.matcher(fileName);
                if (m.matches())
                {
                    String type = null;
                    if (m.group(1).equals("update"))
                    {
                        type = MCPConstants.MCP_FILE_UPDATE;
                    }
                    else if (m.group(1).equals("upgrade"))
                    {
                        type = MCPConstants.MCP_FILE_UPGRADE;
                    }
                    String version = m.group(2);
                    String date = m.group(3);
                    
                    List<MCPFileVO> files = MCPUtils.findFiles(fileName, null, null, username);
                    MCPFileVO mcpFile;
                    if (files == null || files.size() != 1)
                    {
                        mcpFile = new MCPFileVO();
                        mcpFile.setName(fileName);
                        Log.log.warn("Unable to Find Unique MCP File for " + fileName);
                    }
                    else
                    {
                        mcpFile = files.get(0);
                    }
                    mcpFile.setType(type);
                    mcpFile.setVersion(version);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    mcpFile.setDate(sdf.parse(date));
                    
                    long sizeInBytes = item.getSize();
                    InputStream uploadedStream = null;
                    ByteArrayOutputStream data = null;
                    try
                    {
                        uploadedStream = item.getInputStream();
                        data = new ByteArrayOutputStream((int) sizeInBytes);
    //                    byte[] data = new byte[(int) sizeInBytes];
                        byte[] tmpData = new byte[5242880];
                        int b = -1;
                        while ((b = uploadedStream.read(tmpData)) != -1)
                        {
                            data.write(tmpData, 0, b);
                        }
                        
                        byte[] content = data.toByteArray();
                            
                        MCPFileContentVO mcpFileContent = new MCPFileContentVO();
                        mcpFileContent.setContent(content);
                        mcpFile.usetMCPFileContent(mcpFileContent);
                        
                        mcpFile.setSize(content.length);
                    }
                    finally
                    {
                        if (uploadedStream != null)
                        {
                            uploadedStream.close();
                        }
                        if (data != null)
                        {
                            data.close();
                        }
                    }
                    
                    boolean saveFile = MCPUtils.saveMCPFile(mcpFile, username);
                    if (!saveFile)
                    {
                        throw new Exception("File Upload Failed, check RSView logs for details");
                    }
                }
                else
                {
                    throw new Exception("Invalid update/upgrade file name: " + fileName);
                }
                    
                break;
            }
        }//end of while loop   
    }

    private static void setComponentStatus(MCPComponentVO component, List<Map<String, String>> data, String statusKey)
    {
        String componentName = component.getComponentName().toUpperCase();
        if (StringUtils.isNotBlank(component.getInstanceName()))
        {
            componentName = component.getInstanceName().toUpperCase();
        }

        for (Map<String, String> map : data)
        {
            for (String key : map.keySet())
            {
                String tmpKey = key.toUpperCase();
                String tmpValue = map.get(key);
                //componentStatus:RSREMOTE#9D799F78E42C2F6C60946909B321BC59|=|START#SUCCESS|&|
                if (tmpKey.startsWith(componentName.toUpperCase() + "#") && StringUtils.isNotBlank(tmpValue) && tmpValue.toUpperCase().startsWith(statusKey.toUpperCase()))
                {
                    String status = MCPLog.logConfig.get(map.get(key).toUpperCase());
                    component.setStatus(status);
                    component.setLastStatusUpdatedOn(new Date());
                    break;
                }
            }
        }
    }

    public static List<MCPComponentVO> getAllMCPComponent(QueryDTO query, String username) throws Exception
    {
        List<MCPComponentVO> result = new ArrayList<MCPComponentVO>();

        try
        {
            result = MCPUtils.getAllMCPComponent(query, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return result;
    }

    public static List<MCPComponentVO> findComponentsByIds(String[] ids, String username, 
    		ConfigSQL configSQL) throws Exception
    {
        List<MCPComponentVO> result = new ArrayList<MCPComponentVO>();

        try
        {
            result = MCPUtils.findComponentsByIds(ids, username, configSQL);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return result;
    }
}
