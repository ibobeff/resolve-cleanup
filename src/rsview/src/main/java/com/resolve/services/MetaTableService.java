package com.resolve.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaTable;
import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.customtable.ExtMetaTableLookup;
import com.resolve.services.hibernate.customtable.ExtSaveMetaFormView;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.table.dto.RsMetaTableViewDTO;

/**
 * Provides metatable services.
 */
@Service
public class MetaTableService {
	public RsMetaTableViewDTO saveMetaTableView(final RsMetaTableViewDTO tableViewDTO, final String username)
			throws Exception {
		if (StringUtils.isEmpty(tableViewDTO.getMetaTableSysId())) {
			throw new RuntimeException("Table view does not have custom table info");
		}
		
		validateDTO(tableViewDTO, username);
		return saveInternal(tableViewDTO, username);
	}

	public List<RsMetaTableViewDTO> getMetaTableViews(String tableName, String username, RightTypeEnum rightType)
			throws Exception {
		return new ExtMetaTableLookup(tableName, username, rightType).getMetaTableViews();
	}

	public RsMetaTableViewDTO getMetaTableView(String tableName, String viewName, String username,
			RightTypeEnum rightType) throws Exception {
		return new ExtMetaTableLookup(tableName, viewName, username, rightType).getMetaTableView();
	}

	private void validateDTO(final RsMetaTableViewDTO tableViewDTO, final String username) throws Exception {
		if (StringUtils.isNotBlank(tableViewDTO.getId())) {
			return;
		}
		
		// the view is a new one and it does not have 'fields', get that from the 'Default' view.
		RsMetaTableViewDTO defaultView = getMetaTableView(tableViewDTO.getMetaTableName(), "Default", username,
				RightTypeEnum.view);
		if (defaultView == null) {
			throw new Exception("Default view for table " + tableViewDTO.getMetaTableName()
					+ " does not exist. So cannot create this view as columns are missing.");
		}

		// add the fields if they are not available
		if (CollectionUtils.isEmpty(tableViewDTO.getFields())) {
			List<RsUIField> fields = defaultView.getFields();

			if (CollectionUtils.isNotEmpty(fields)) {
				List<RsUIField> nonNullFields = new ArrayList<>();

				for (RsUIField rsUIFld : fields) {
					if (rsUIFld != null && StringUtils.isNotBlank(rsUIFld.getId())) {
						nonNullFields.add(rsUIFld);
					}
				}

				if (!nonNullFields.isEmpty()) {
					tableViewDTO.setFields(nonNullFields);
				}
			}
		}

		// update the roles if they are not there.
		if (StringUtils.isBlank(tableViewDTO.getViewRoles()) && StringUtils.isBlank(tableViewDTO.getEditRoles())
				&& StringUtils.isBlank(tableViewDTO.getAdminRoles())) {
			tableViewDTO.setViewRoles(defaultView.getViewRoles());
			tableViewDTO.setEditRoles(defaultView.getEditRoles());
			tableViewDTO.setAdminRoles(defaultView.getAdminRoles());
		}
	}
	
	private RsMetaTableViewDTO saveInternal(final RsMetaTableViewDTO tableViewDTO, final String username) throws Exception {
		try {
			
			// get/refresh the view from db
        HibernateProxy.setCurrentUser(username);
			return (RsMetaTableViewDTO) HibernateProxy.execute(() -> {

				MetaTable metaTable = HibernateUtil.getDAOFactory().getMetaTableDAO().findById(tableViewDTO.getMetaTableSysId());
				if (metaTable == null) {
					throw new Exception("MetaTable does not exist for view :" + tableViewDTO.getDisplayName());
				}
	
				MetaTableView metaTableView = getMetaTableView(tableViewDTO);
	
				metaTableView.setUName(tableViewDTO.getName());
				metaTableView.setUDisplayName(tableViewDTO.getDisplayName());
				metaTableView.setUType(tableViewDTO.getType());
				metaTableView.setUIsGlobal(tableViewDTO.getIsGlobal());
				metaTableView.setUUser(StringUtils.isNotBlank(tableViewDTO.getUser()) ? tableViewDTO.getUser() : username);
				metaTableView.setUMetaFormLink(tableViewDTO.getMetaFormLink());
				metaTableView.setUMetaNewLink(tableViewDTO.getMetaNewLink());
				metaTableView.setUCreateFormId(tableViewDTO.getCreateFormId());
				metaTableView.setUEditFormId(tableViewDTO.getEditFormId());
				metaTableView.setUTarget(tableViewDTO.getTarget());
				metaTableView.setUParams(tableViewDTO.getParams());
	
				// save immediately - to get the sysid
				HibernateUtil.getDAOFactory().getMetaTableViewDAO().persist(metaTableView);
	
				// add the table view fields
				metaTableView.setMetaTableViewFields(createMetaTableViewFields(tableViewDTO, metaTableView));
	
				// add the metatable reference
				metaTableView.setMetaTable(metaTable);
	
				// access rights
				MetaAccessRights metaAccessRights = metaTableView.getMetaAccessRights();
				if (metaAccessRights == null) {
					metaAccessRights = new MetaAccessRights();
				}
				metaAccessRights.setUReadAccess(tableViewDTO.getViewRoles());
				metaAccessRights.setUWriteAccess(tableViewDTO.getEditRoles());
				metaAccessRights.setUAdminAccess(tableViewDTO.getAdminRoles());
				metaAccessRights.setUResourceId(metaTableView.getSys_id());
				metaAccessRights.setUResourceName(metaTableView.getUName());
				metaAccessRights.setUResourceType(MetaTableView.RESOURCE_TYPE);
	
				// persist it
				HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(metaAccessRights);
				metaTableView.setMetaAccessRights(metaAccessRights);
	
				HibernateUtil.getCurrentSession().flush();
				return new ExtMetaTableLookup(metaTable.getUName(), metaTableView.getUName(), username,
						RightTypeEnum.admin).getMetaTableView();
			});
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);			
			throw new Exception(t);
		}
	}

	private MetaTableView getMetaTableView(final RsMetaTableViewDTO tableViewDTO) throws Exception {
		if (StringUtils.isBlank(tableViewDTO.getId())) {
			return new MetaTableView();
		}
		
		MetaTableView metaTableView = Optional.ofNullable(HibernateUtil.getDAOFactory().getMetaTableViewDAO()
				.findById(tableViewDTO.getId()))
				.orElseThrow(() -> new Exception("MetaTableView does not exist for view :" 
								+ tableViewDTO.getDisplayName()));

		// make sure that if the name of the view is 'Default', user is not able to rename it
		String currentViewName = metaTableView.getUName();
		if (currentViewName.equalsIgnoreCase("Default")) {
			String newViewName = tableViewDTO.getName();
			if (!newViewName.equalsIgnoreCase(currentViewName)) {
				throw new Exception("Default view cannot be renamed.");
			}
		}

		return metaTableView;
	}
	
	private Collection<MetaTableViewField> createMetaTableViewFields(final RsMetaTableViewDTO tableViewDTO,
			final MetaTableView metaTableView) {
		List<MetaTableViewField> newFields = new ArrayList<>();
		if (tableViewDTO.getFields() == null) {
			return newFields;
		}
		
		// first delete the existing ones
		deleteMetaTableViewField(metaTableView.getMetaTableViewFields());

		int order = 1;
		for (RsUIField field : tableViewDTO.getFields()) {
			if (field == null || StringUtils.isBlank(field.getId())) {
				throw new RuntimeException(
						field == null ? "Table view field is null" : "sysid of " + field.getName() + " is empty.");
			}

			MetaField metaField = HibernateUtil.getDAOFactory().getMetaFieldDAO().findById(field.getId());
			if (metaField == null) {
				throw new RuntimeException(field.getName() + " does not exist.");
			}

			// prepare new props and persist it
			MetaFieldProperties tableProperties = ExtSaveMetaFormView.convertINPUTUIToDbModelProperties(field, null,
					null, false);
			HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().persist(tableProperties);

			// prepare new rec
			MetaTableViewField metaTableViewField = new MetaTableViewField();
			metaTableViewField.setMetaTableView(metaTableView);
			metaTableViewField.setMetaField(metaField);
			metaTableViewField.setUOrder(new Integer(order));
			metaTableViewField.setMetaFieldTableProperties(tableProperties);

			// persist it
			HibernateUtil.getDAOFactory().getMetaTableViewFieldDAO().persist(metaTableViewField);

			newFields.add(metaTableViewField);
			order++;
		}

		return newFields;
	}
	
	private static void deleteMetaTableViewField(Collection<MetaTableViewField> metaTableViewFields) {
		if (CollectionUtils.isEmpty(metaTableViewFields)) {
			return;
		}
		
		try {
			// NEED THIS FOR ORACLE as the DELETES are not COMMITTED immediately
        HibernateProxy.setCurrentUser("admin");
            HibernateProxy.execute(() -> {

				for (MetaTableViewField metaTableViewField : metaTableViewFields) {
					if (metaTableViewField.getMetaFieldTableProperties() != null) {
						HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO()
								.delete(metaTableViewField.getMetaFieldTableProperties());
					}
					HibernateUtil.getDAOFactory().getMetaTableViewFieldDAO().delete(metaTableViewField);
				}

            });
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
	}
}
