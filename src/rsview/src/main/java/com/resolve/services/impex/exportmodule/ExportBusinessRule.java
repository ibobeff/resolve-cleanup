/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.BusinessRuleUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.vo.ResolveBusinessRuleVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;

public class ExportBusinessRule extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String ruleName;
    
    public ExportBusinessRule(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
        this.ruleName = impexGlideVO.getUModule();
    }
    
    public void export()
    {
        ResolveBusinessRuleVO  ruleVO = BusinessRuleUtil.findBusinessRuleByIdOrName(null, ruleName, userName);
        populateBusinessRule(ruleVO);
    }
    
    private void populateBusinessRule(ResolveBusinessRuleVO  ruleVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.ResolveBusinessRule");
        dto.setSys_id(ruleVO.getSys_id());
        dto.setFullName(ruleVO.getUName());
        dto.setName(ruleVO.getUName());
        dto.setType(BUSINESS_RULE);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
}
