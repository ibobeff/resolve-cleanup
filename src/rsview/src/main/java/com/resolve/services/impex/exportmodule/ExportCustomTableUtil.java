/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaControl;
import com.resolve.persistence.model.MetaControlItem;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaFilter;
import com.resolve.persistence.model.MetaFormAction;
import com.resolve.persistence.model.MetaTable;
import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.persistence.model.MetaViewField;
import com.resolve.persistence.model.MetaxFieldDependency;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.services.constants.Operation;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportCustomTableUtil extends ExportComponent
{
    private CustomTable customTable = null;
    private boolean exportData = false;
    private static Set<String> wikiNames = ConcurrentHashMap.newKeySet();
    private static Set<String> actionTaskNames = ConcurrentHashMap.newKeySet();
    private static ManifestGraphDTO parentGraphDTO;
    
//    public ExportCustomTableUtil(CustomTable customTable)
//    {
//        this.customTable = customTable;
//        
//    }//ExportCustomTableUtil
    
    public void setExportData(boolean exportData)
    {
        this.exportData = exportData;
    }
    
    public boolean getExportData()
    {
        return this.exportData;
    }

    public List<ImpexComponentDTO> getMetaDataForCustomTable(CustomTable customTable, ManifestGraphDTO graphDTO, boolean parent)
    {
        this.customTable = customTable;
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        /*
         * If the table is file upload table, just grab it's components and populate it.
         * No need to export corresponding forms.
         */
        if (!customTable.getUName().endsWith("_fu")) {
            if (parent)
            {
                parentGraphDTO = graphDTO;
            }
        }
        if (impexOptions.getTableData())
        {
            setExportData(true);
        }
        
        list.addAll(exportMetaFields());
        list.addAll(exportMetaTable());
        
        ExportUtils.populateFormChildIds(list, graphDTO);
        
        if (!customTable.getUName().endsWith("_fu")) {
            if (impexOptions.getTableForms())
            {
                list.addAll(exportMetaForm());
            }
        }
        
        return list;
    }//getMetaDataForCustomTable
    
    public ImpexComponentDTO populateCustomTable()
    {
        ImpexComponentDTO model = new ImpexComponentDTO();
        model.setSys_id(customTable.getSys_id());
        model.setFullName(customTable.getUModelName());
        model.setModule(customTable.getUModelName());
        if(exportData)
        {
            model.setName(SCHEMA + "," + DATA);//schema and data both 
        }
        else
        {
            model.setName(SCHEMA);//schema only
        }
        model.setType(CUSTOMTABLE);
        model.setDisplayType(model.getType());
        String tableName = HibernateUtil.class2Table(customTable.getClass().getName());
        model.setTablename(tableName);
        
        return model;
    }
    
    public List<ImpexComponentDTO> exportMetaFields()
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        
        try
        {
			HibernateProxy.execute(() -> {
				// get the obj
				customTable = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(customTable.getSys_id());
			});
            Collection<MetaField> metaFields = customTable.getMetaFields();
            if(metaFields != null && metaFields.size() > 0)
            {
                for(MetaField metaField : metaFields)
                {
                    String tableName = HibernateUtil.class2Table(metaField.getClass().getName());
                    list.add(ExportUtils.createDBComponent(metaField.getSys_id(), tableName));
                    
                    //export metaProperties 
                    list.addAll(convertMetaFieldPropertiesToModel(metaField.getMetaFieldProperties(), exportData));
                    
                    //access rights
                    list.addAll(convertMetaAccessRightsToModel(metaField.getMetaAccessRights()));

                }//end of for loop
            }//end of if
        }
        catch (Throwable t)
        {
            String message = String.format("Error while gathering meta fields of a custom table %s", customTable.getUName());
            Log.log.error(message, t);
            ImpexUtil.logImpexMessage(message, true);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return list;
    }//exportMetaFields

	public List<ImpexComponentDTO> exportMetaTable() {
		List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
		try {
			HibernateProxy.execute(() -> {

				// get the obj
				customTable = HibernateUtil.getDAOFactory().getCustomTableDAO().findById(customTable.getSys_id());
				MetaTable metaTable = customTable.getMetaTable();

				if (metaTable != null) {
					String tableName = HibernateUtil.class2Table(metaTable.getClass().getName());
					list.add(ExportUtils.createDBComponent(metaTable.getSys_id(), tableName));

					// views
					Collection<MetaTableView> metaTableViews = metaTable.getMetaTableViews();
					if (metaTableViews != null && metaTableViews.size() > 0) {
						for (MetaTableView metaTableView : metaTableViews) {
							// export the parent views - child views will be exported by parent views
							if (metaTableView.getParentMetaTableView() != null && !(metaTableView.getSys_id()
									.equals(metaTableView.getParentMetaTableView().getSys_id()))) {
								continue;
							}

							list.addAll(exportMetaTableView(metaTableView));
						} // end of for loop
					}

					// metafilters
					Collection<MetaFilter> metaFilters = metaTable.getMetaFilters();
					if (metaFilters != null && metaFilters.size() > 0) {
						for (MetaFilter metaFilter : metaFilters) {
							list.addAll(exportMetaFilter(metaFilter));
						} // end of for
					} // end of if
				}

			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		return list;
	}// exportMetaTable
    
    public List<ImpexComponentDTO> exportMetaForm()
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        
        try
        {
            list.addAll(new ExportMetaFormViewUtil(customTable, impexOptions, parentGraphDTO).exportMetaFormView(false));
        }
        catch(Throwable t)
        {
            Log.log.error(t);
        }
        
        return list;
    }//exportMetaForm
    

    public static List<ImpexComponentDTO> convertMetaAccessRightsToModel(MetaAccessRights metaAccessRights)
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        if(metaAccessRights != null)
        {
            String tableName = HibernateUtil.class2Table(metaAccessRights.getClass().getName());
            list.add(ExportUtils.createDBComponent(metaAccessRights.getSys_id(), tableName));
        }
        
        return list;
    }//convertMetaAccessRightsToModel
    
    public List<ImpexComponentDTO> convertMetaFieldPropertiesToModel(MetaFieldProperties metaFieldProperties, boolean exportDataAlso) throws Exception
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        if(metaFieldProperties != null)
        {
            //if this field is of type FileUpload, then export the custom table also
            if(metaFieldProperties.getUUIType() != null && metaFieldProperties.getUUIType().equalsIgnoreCase(CustomFormUIType.FileUploadField.name()))
            {
                if (impexOptions.getFormTables())
                {
                    //prepare this as a custom table export
                    String referenceTableName = metaFieldProperties.getUFileUploadTableName();
                    if (StringUtils.isNotBlank(referenceTableName) && !ExportUtils.refTableNames.contains(referenceTableName)) {
                        ExportUtils.refTableNames.add(referenceTableName);
                    }
                }
            }
            
            String tableName = HibernateUtil.class2Table(metaFieldProperties.getClass().getName());
            list.add(ExportUtils.createDBComponent(metaFieldProperties.getSys_id(), tableName));
            
            //dependencies
            Collection<MetaxFieldDependency> metaxFieldDependencys = metaFieldProperties.getMetaxFieldDependencys();
            list.addAll(exportMetaxFieldDependencies(metaxFieldDependencys));

        }//end of if
        
        return list;
    }//convertMetaAccessRightsToModel
    
    public static List<ImpexComponentDTO> exportMetaxFieldDependencies(Collection<MetaxFieldDependency> dependencies)
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        
        if(dependencies != null && dependencies.size() > 0)
        {
            for(MetaxFieldDependency dependency : dependencies)
            {
                String tableName = HibernateUtil.class2Table(dependency.getClass().getName());
                list.add(ExportUtils.createDBComponent(dependency.getSys_id(), tableName));
            }
        }
        
        return list;
    }
    
    public static List<ImpexComponentDTO> convertMetaControlToModel(MetaControl metaControl, ImpexOptionsDTO impexOptions)
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        
        if(metaControl != null)
        {
            String tableName = HibernateUtil.class2Table(metaControl.getClass().getName());
            list.add(ExportUtils.createDBComponent(metaControl.getSys_id(), tableName));

            //control items
            Collection<MetaControlItem> metaControlItems = metaControl.getMetaControlItems();
            if (metaControlItems != null && metaControlItems.size() > 0)
            {
                for (MetaControlItem metaControlItem : metaControlItems)
                {
                    list.addAll(convertMetaControlItem(metaControlItem, impexOptions));
                    
                    tableName = HibernateUtil.class2Table(metaControlItem.getClass().getName());
                    list.add(ExportUtils.createDBComponent(metaControlItem.getSys_id(), tableName));

                }// end of for
            }// end of if
        }
        
        return list;
    }//convertMetaControlToModel
    
    public static List<ImpexComponentDTO> convertMetaControlItem(MetaControlItem metaControlItem, ImpexOptionsDTO impexOptions)
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        if(metaControlItem != null)
        {
            String tableName = HibernateUtil.class2Table(metaControlItem.getClass().getName());
            list.add(ExportUtils.createDBComponent(metaControlItem.getSys_id(), tableName));
            
            //form action items
            Collection<MetaFormAction> metaMetaFormActions = metaControlItem.getMetaFormActions();
            if(metaMetaFormActions != null)
            {
                for(MetaFormAction metaFormAction : metaMetaFormActions)
                {
                    tableName = HibernateUtil.class2Table(metaFormAction.getClass().getName());
                    list.add(ExportUtils.createDBComponent(metaFormAction.getSys_id(), tableName));
                    if (impexOptions.getFormSS())
                    {
                        if(metaFormAction.getUOperation().equalsIgnoreCase(Operation.SCRIPT.getTagName()))
                        {
                            parentGraphDTO.getChildren().add(convertSystemScript(metaFormAction.getUScriptName()));
                        }
                    }
                    if (impexOptions.getFormRBs())
                    {
                        if(metaFormAction.getUOperation().equalsIgnoreCase(Operation.RUNBOOK.getTagName()))
                        {
                        	String wikiName = metaFormAction.getURunbookName();
                        	if (StringUtils.isNotBlank(wikiName) && !wikiName.startsWith("$"))
                        	{
                        		wikiNames.add(wikiName);
                        	}
                        }
                    }
                    if (impexOptions.getFormATs())
                    {
                        if(metaFormAction.getUOperation().equalsIgnoreCase(Operation.ACTIONTASK.getTagName()))
                        {
                            actionTaskNames.add(metaFormAction.getUActionTaskName());
                        }
                    }
                    
                }//end of for
            }//end of if
            
            //dependencies
            list.addAll(exportMetaxFieldDependencies(metaControlItem.getMetaxFieldDependencys()));
        }//end of if

        return list;
    }//convertMetaControlItem
    
    public static ManifestGraphDTO convertSystemScript(String scriptName)
    {
        ManifestGraphDTO graphDTO = null;
        if(StringUtils.isNotEmpty(scriptName))
        {
//            ResolveSysScriptVO script = ServiceHibernate.findResolveSysScriptByIdOrName(null, scriptName, "system");
//            if(script != null)
//            {
//                String tableName = HibernateUtil.class2Table(ResolveSysScript.class.getName());
//                list.add(ExportUtils.createDBComponent(script.getSys_id(), tableName));d
//            }//end of if
            
            ResolveImpexGlideVO glideVO = new ResolveImpexGlideVO();
            glideVO.setUModule(scriptName);
            
            ExportSystemScript exportSystemScript = new ExportSystemScript(glideVO);
            exportSystemScript.setUserName("system");
            graphDTO = exportSystemScript.export();
        }
        
        return graphDTO;
    }
    
    public static Set<String> getWikiNames()
    {
        return wikiNames;
    }
    
    public static Set<String> getActionTaskNames()
    {
        return actionTaskNames;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //private methods
    
    private List<ImpexComponentDTO> exportMetaTableView(MetaTableView metaTableView) throws Exception
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        String tableName = HibernateUtil.class2Table(metaTableView.getClass().getName());
        list.add(ExportUtils.createDBComponent(metaTableView.getSys_id(), tableName));
        
        //control
        list.addAll(convertMetaControlToModel(metaTableView.getMetaControl(), impexOptions));
        
        //rights
        list.addAll(convertMetaAccessRightsToModel(metaTableView.getMetaAccessRights()));
        
        //view fields
        Collection<MetaTableViewField> metaTableViewFields = metaTableView.getMetaTableViewFields();
        if(metaTableViewFields != null && metaTableViewFields.size() > 0)
        {
            for(MetaTableViewField metaTableViewField : metaTableViewFields)
            {
                tableName = HibernateUtil.class2Table(metaTableViewField.getClass().getName());
                list.add(ExportUtils.createDBComponent(metaTableViewField.getSys_id(), tableName));
                
                list.addAll(convertMetaFieldPropertiesToModel(metaTableViewField.getMetaFieldTableProperties(), exportData));
                
                MetaViewField metaViewField = metaTableViewField.getMetaViewField();
                if(metaViewField != null)
                {
                    tableName = HibernateUtil.class2Table(metaViewField.getClass().getName());
                    list.add(ExportUtils.createDBComponent(metaViewField.getSys_id(), tableName));
                }
            }//end of for 
        }//end of if

        //export the child view also
        Collection<MetaTableView> childMetaTableViews = metaTableView.getMetaTableViews();
        if(childMetaTableViews != null && childMetaTableViews.size() > 0)
        {
            for(MetaTableView childView : childMetaTableViews)
            {
                //recursive
                if (!childView.getSys_id().equals(metaTableView.getSys_id()))
                    list.addAll(exportMetaTableView(childView));
            }
        }
        
        return list;
    }//exportMetaTableView
    
    private List<ImpexComponentDTO> exportMetaFilter(MetaFilter metaFilter)
    {
        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
        String tableName = HibernateUtil.class2Table(metaFilter.getClass().getName());
        list.add(ExportUtils.createDBComponent(metaFilter.getSys_id(), tableName));
        
        //access rights
        list.addAll(convertMetaAccessRightsToModel(metaFilter.getMetaAccessRights()));
        
        return list;
    }//exportMetaFilter
    
//    private  static List<ImpexComponentDTO> convertMetaStyleToModel(MetaStyle metaStyle)
//    {
//        List<ImpexComponentDTO> list = new ArrayList<ImpexComponentDTO>();
//        if(metaStyle != null)
//        {
//            String tableName = HibernateUtil.class2Table(metaStyle.getClass().getName());
//            list.add(ExportUtils.createDBComponent(metaStyle.getSys_id(), tableName));
//        }
//        
//        return list;
//    }//convertMetaStyleToModel
    
    public static void setParentGraphDTO(ManifestGraphDTO graphDTO)
    {
        parentGraphDTO = graphDTO;
    }

}//ExportCustomTableUtil
