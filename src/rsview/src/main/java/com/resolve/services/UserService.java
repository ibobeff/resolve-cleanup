package com.resolve.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import com.resolve.persistence.model.RoleWikidocHomepageRel;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.wiki.web.vo.DocumentInfoVO;

/**
 * Provides user-related services.
 */
@Service
public class UserService {
	private final static String RESOLVE_MAINT = "resolve.maint";

	public String getUserHomepage(final String username) {
		if (StringUtils.isBlank(username)) {
			throw new IllegalArgumentException("Username cannot be null or empty");
		}

		UsersVO userVO = ServiceHibernate.getUserByNameNoReference(username); //.getUser(username);
		if (userVO == null) {
			throw new IllegalStateException("User " + username + " does not exist.");
		}

		String homepage = ConstantValues.WIKI_NAMESPACE_HOME + "." + ConstantValues.WIKI_WEB_HOME;
		if (StringUtils.isNotBlank(userVO.getUHomePage())) {
			return userVO.getUHomePage();
		}

		String propertyFileWikiDoc = PropertiesUtil.getPropertyString(ConstantValues.PROP_WIKI_PROPERTY_TABLE_HOMEPAGE);
		String wikiTableRoles = PropertiesUtil.getPropertyString(ConstantValues.WIKI_PROPERTY_TABLE_ROLES);
		boolean setWikiPropertyHomepage = false;

		if (wikiTableRoles != null) {
			if (wikiTableRoles.equalsIgnoreCase("true")) {
				// if the user has only 1 homepage, go to that homepage else list all the
				// homepages assigned to that user
				List<DocumentInfoVO> listOfHomePages = getUserListOfHomepages(username);
				if (listOfHomePages != null) {
					if (listOfHomePages.size() == 1) {
						homepage = listOfHomePages.get(0).getDocumentFullName();
					} else if (listOfHomePages.size() > 1) {
						boolean wikiExist = ServiceWiki.isWikidocExist(ConstantValues.WIKI_SYSTEM_HOMEPAGE, false);
						if (wikiExist) {
							homepage = ConstantValues.WIKI_SYSTEM_HOMEPAGE;
						}
					} else {
						setWikiPropertyHomepage = true;
					}
				}
			} else {
				setWikiPropertyHomepage = true;
			}
		} else {
			setWikiPropertyHomepage = true;
		}

		if (setWikiPropertyHomepage) {
			boolean wikiExist = ServiceWiki.isWikidocExist(propertyFileWikiDoc, false);
			if (wikiExist) {
				homepage = propertyFileWikiDoc;
			}
		}

		return homepage;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Set<String> getUserRoles(String userName) {
		if (StringUtils.isBlank(userName)) {
			return Collections.<String>emptySet();
		}

		Object cachedRoles = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.USER_ROLES_CACHE_REGION, userName);
		if (cachedRoles != null) {
			return (Set<String>) cachedRoles;
		}

		Set<String> userRoles = new TreeSet<>();

		if (userName.equalsIgnoreCase(RESOLVE_MAINT)) {
			userRoles.add("admin");
			return userRoles;
		}

		userName = userName.toLowerCase();
		try {
			String usernameFinal = userName;
			HibernateProxy.execute(() ->{
				// UNION ALL is not supported by HIBERNATE. So have to prepare 2
				// qrys and assemble it.
				String sql = "select r.UName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r "
						+ " where u.UUserName = :username";
				Query q = HibernateUtil.createQuery(sql);
				q.setParameter("username", usernameFinal);
				List<String> roles = q.list();
				userRoles.addAll(roles);

				// query 2
				sql = "select r.UName from Users as u" + " join u.userGroupRels as ugr " + " join ugr.group as g "
						+ " join g.groupRoleRels as grr " + " join grr.role as r " + " where u.UUserName = :username";
				q = HibernateUtil.createQuery(sql);
				q.setParameter("username", usernameFinal);

				roles = q.list();
				userRoles.addAll(roles);
			});		

			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		// trim the string in the list
		StringUtils.trimStringCollection(userRoles);
		HibernateUtil.cacheDBObject(DBCacheRegionConstants.USER_ROLES_CACHE_REGION,
				new String[] { "com.resolve.persistence.model.Users", "com.resolve.persistence.model.UserRoleRel",
						"com.resolve.persistence.model.Roles", "com.resolve.persistence.model.UserGroupRel",
						"com.resolve.persistence.model.Groups", "com.resolve.persistence.model.GroupRoleRel" },
				userName, userRoles);

		return userRoles;
	}

	/**
	 * Used on the system Homepage to list all the docs that this user can look at.
	 * List is prepared based on roles the user has
	 * 
	 * @param username
	 *            The username
	 * @return a list of {@link DocumentInfoVO}
	 */
	public List<DocumentInfoVO> getUserListOfHomepages(final String username) {
		if (StringUtils.isBlank(username)) {
			return Collections.<DocumentInfoVO>emptyList();
		}

		Set<String> roles = getUserRoles(username);
		return getListOfHomepage(roles);
	}

	@SuppressWarnings("unchecked")
	private List<DocumentInfoVO> getListOfHomepage(final Set<String> roles) {
		if (CollectionUtils.isEmpty(roles)) {
			return Collections.<DocumentInfoVO>emptyList();
		}

		List<DocumentInfoVO> listDocumentVO = new ArrayList<>();
		
		List<String> roleids = getRoleidsFor(roles);
		String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
		String queryString = SQLUtils.prepareQueryString(roleids, dbType);
		String sql = "from RoleWikidocHomepageRel where role in (" + queryString + ")";
		List<RoleWikidocHomepageRel> roleDocs = null;
		
		try {
			
			roleDocs = (List<RoleWikidocHomepageRel>) HibernateProxy.execute(() -> {
				Query q = HibernateUtil.createQuery(sql);
				return q.list();
			});


		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (CollectionUtils.isEmpty(roleDocs)) {
			return Collections.<DocumentInfoVO>emptyList();
		}
			
		List<String> temp = new ArrayList<>();
		for (RoleWikidocHomepageRel roleDoc : roleDocs) {
			WikiDocument doc = roleDoc.getWikidoc();
			Roles role = roleDoc.getRole();

			if (temp.contains(doc.getUFullname())) {
				continue;
			} else {
				temp.add(doc.getUFullname());
			}

			DocumentInfoVO obj = new DocumentInfoVO();
			obj.setDocumentFullName(doc.getUFullname());
			obj.setDocumentName(doc.getUName());
			obj.setNamespace(doc.getUNamespace());
			obj.setRoleName(role.getUName());

			listDocumentVO.add(obj);
		}

		return listDocumentVO;
	}

	@SuppressWarnings("unchecked")
	private List<String> getRoleidsFor(Set<String> roles) {
		List<String> roleids = new ArrayList<>();
		String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
		String queryString = SQLUtils.prepareQueryString(new ArrayList<String>(roles), dbType);
		String sql = " from Roles where UName in (" + queryString + ")";
		List<Roles> listRoles = null;
		try {
			listRoles = (List<Roles>) HibernateProxy.execute(() -> {
				Query q = HibernateUtil.createQuery(sql);
				return q.list();
			});

			
		} catch (Throwable e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (listRoles != null && listRoles.size() > 0) {
			for (Roles role : listRoles) {
				roleids.add(role.getSys_id());
			}
		}

		return roleids;
	}
}
