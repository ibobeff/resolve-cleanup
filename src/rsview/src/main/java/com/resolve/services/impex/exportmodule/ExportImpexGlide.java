/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.StringUtils;

public class ExportImpexGlide extends ExportComponent
{
    protected ResolveImpexModuleVO impexModuleVO;
    
    public ExportImpexGlide(ResolveImpexModuleVO vo)
    {
        this.impexModuleVO = vo;
    }
    
    public void export()
    {
        populateImpexGlide();
    }
    
    public void populateImpexGlide()
    {
    	/*
    	 * Adding all GLIDE components to the manifest list. That way both, import and export, manifest list
    	 * would show the same number of components.
    	 */
        if (impexModuleVO.getResolveImpexGlides() != null && impexModuleVO.getResolveImpexGlides().size() > 0)
        {
        	List<ResolveImpexGlideVO> glideList = new ArrayList<ResolveImpexGlideVO>();
            glideList.addAll(impexModuleVO.getResolveImpexGlides());
            for (ResolveImpexGlideVO vo : glideList)
            {
	            ImpexComponentDTO dto = new ImpexComponentDTO();
	            dto.setSys_id(vo.getSys_id());
	            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveImpexGlide");
	            dto.setTablename(tableName);
	            dto.setFullName("RESOLVE_IMPEX_GLIDE: " + (StringUtils.isBlank(vo.getUName())?vo.getUModule():vo.getUName()));
	            dto.setType("DB");
	            dto.setDisplayType("DB");
	            
	            ExportUtils.impexComponentList.add(dto);
            }
        }
    }
}
