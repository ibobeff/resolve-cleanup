/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import com.resolve.persistence.model.DatabaseConnectionPool;
import com.resolve.persistence.model.DatabaseFilter;
import com.resolve.persistence.model.EWSAddress;
import com.resolve.persistence.model.EmailAddress;
import com.resolve.persistence.model.RemedyxFilter;
import com.resolve.persistence.model.RemedyxForm;
import com.resolve.persistence.model.SSHPool;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceGateway;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.MSGGatewayFilterUtil;
import com.resolve.services.hibernate.util.PullGatewayFilterUtil;
import com.resolve.services.hibernate.util.PushGatewayFilterUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * All gateway filter related exports happen here.
 * @author mangesh.shimpi
 *
 */

public class ExportFilter extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String filterName;
    protected String modelName;
    
    public ExportFilter(ResolveImpexGlideVO impexGlideVO) throws Exception
    {
        this.impexGlideVO = impexGlideVO;
        this.filterName = impexGlideVO.getUModule();
        this.modelName = impexGlideVO.getUType();
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for Filter: " + filterName);
        }
    }
    
    public void export()
    {
        String fieldName = "UName";
        if ("SSHPool".equals(modelName))
        {
            fieldName = "USubnetMask";
        }
        
        StringBuilder hql = new StringBuilder();
        
        try {                   
            String className = "com.resolve.persistence.model." + modelName;
            Class.forName(ESAPI.validator().getValidInput("Resolve Model", className, "ResolveModel", 512, false));
        } catch(ClassNotFoundException | ValidationException | IntrusionException e) {
            int index = modelName.indexOf("Filter");
            if(index != -1) {
                String type = ServiceGateway.getGatewayType(modelName.substring(0, index));
                if(StringUtils.isNotBlank(type) && !type.equals("None"))
                    modelName = type + "GatewayFilter";
                
                else {
                    Log.log.warn("Cannot find gateway type.");
                    return;
                }
            }
            
            else {
                Log.log.warn("modelName does not contain 'Filter'.");
                return;
            }
        }
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        hql.append("select sys_id from ").append(modelName);
        hql.append(" where UQueue = '").append(Constants.DEFAULT_GATEWAY_QUEUE_NAME).append("'");
        //hql.append(" and ").append(fieldName).append(" = '").append(filterName).append("'");
        hql.append(" and ").append(fieldName).append(" = :UName");
        
        queryParams.put("UName", filterName);
        
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
            
            if (list != null && list.size() > 0)
            {
                if ("SSHPool".equals(modelName))
                {
                    /*
                     * SSHPool is not really a filter, but a ssh connection pool used only in filters.
                     * It's little different than rest of the filters and that's why this special treatment.
                     */
                	Object object = getModel((String)list.get(0));
                	if (object != null)
                	{
	                    SSHPool sshPool = (SSHPool)object;
	                    populateSSHPool(sshPool);
                	}
                	
                }
                else
                {
                	String sys_id = (String)list.get(0);
                    populateFilter(sys_id);
                    if(modelName.contains("GatewayFilter"))
                        populateFilterAttributes(sys_id);
                    
                    else if (modelName.equals("DatabaseFilter"))
                    {
                        if (impexOptions.exportDBPool)
                        {
                            // Export database connection pool records
                        	Object object = getModel(sys_id);
                        	if (object != null)
                        	{
	                            DatabaseFilter dbFilter = (DatabaseFilter)object;
	                            if (StringUtils.isNotBlank(dbFilter.getUPool()))
	                            {
	                                exportDBPool(dbFilter.getUPool());
	                            }
                        	}
                        }
                    }
                    
                    else if (modelName.equals("RemedyxFilter"))
                    {
                        if (impexOptions.exportRemedyxForm)
                        {
                            // Export Remedyx Filter Form
                        	Object object = getModel(sys_id);
                        	if (object != null)
                        	{
                        		RemedyxFilter remedyxFilter = (RemedyxFilter)object;
                                if (StringUtils.isNotBlank(remedyxFilter.getUFormName()))
                                {
                                    exportRemedyxForm(remedyxFilter.getUFormName());
                                }
                        	}
                        }
                    }
                    else if (modelName.equals("EmailFilter") || modelName.equals("EWSFilter"))
                    {
                        if (modelName.equals("EmailFilter") && impexOptions.isEmailAddress())
                        {
                            exportAddressModel("EmailAddress");
                        }
                        if (modelName.equals("EWSFilter") && impexOptions.isEwsAddress())
                        {
                            exportAddressModel("EWSAddress");
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
    }
    
    private Object getModel(String sysId) throws Exception
    {
    	Map<String, Object> queryParams = new HashMap<String, Object>();
    	Object result = null;
    	
    	String localHql = "from " + modelName + " where sys_id = :sysId";
    	queryParams.put("sysId", sysId);
    	List<? extends Object> localList = GeneralHibernateUtil.executeHQLSelect(localHql.toString(), queryParams);
    	
    	if (localList != null && localList.size() > 0)
    	{
    		result = localList.get(0);
    	}
    	
    	return result;
    }
    
    private void populateSSHPool(SSHPool sshPool)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.SSHPool");
        
        dto.setSys_id(sshPool.getSys_id());
        dto.setFullName(sshPool.getUSubnetMask());
        dto.setName(sshPool.getUSubnetMask());
        dto.setType(SSH_POOL);
        dto.setDisplayType(SSH_POOL);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }

    private void populateFilter(String filterSysId)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model." + modelName);
        
        dto.setSys_id(filterSysId);
        dto.setFullName(filterName);
        dto.setName(filterName);
        dto.setType(GATEWAY_FILTER);
        dto.setDisplayType(GATEWAY_FILTER);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateFilterAttributes(String filterSysId) {
        
        List<String> attrSysIds = new ArrayList<String>();
        
        if(modelName.contains("Push"))
            attrSysIds = PushGatewayFilterUtil.getPushGatewayFilterAttributeIds(filterSysId);
        else if(modelName.contains("Pull"))
            attrSysIds = PullGatewayFilterUtil.getPullGatewayFilterAttributeIds(filterSysId);
        else if(modelName.contains("MSG"))
            attrSysIds = MSGGatewayFilterUtil.getMSGGatewayFilterAttributeIds(filterSysId);
        
        for(String attrSysId:attrSysIds) {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = HibernateUtil.class2Table("com.resolve.persistence.model." + modelName + "Attr");
            
            dto.setSys_id(attrSysId);
            dto.setFullName(filterName);
            dto.setName(filterName);
            dto.setType(GATEWAY_FILTER);
            dto.setDisplayType(GATEWAY_FILTER);
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
        }
    }
    
    public void exportDBPool(String poolName)
    {
        StringBuilder hql = new StringBuilder();
        hql.append("from DatabaseConnectionPool ");
        hql.append("where UQueue = '").append(Constants.DEFAULT_GATEWAY_QUEUE_NAME).append("' ");
        //hql.append(" and UPoolName = '").append(poolName).append("'");
        hql.append(" and UPoolName = :UPoolName");
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UPoolName", poolName);
        
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
            if (list != null && list.size() > 0)
            {
                for (Object obj : list)
                {
                    DatabaseConnectionPool dbPool = (DatabaseConnectionPool)obj;
                    populateDBPool(dbPool);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    private void populateDBPool(DatabaseConnectionPool dbPool)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.DatabaseConnectionPool");
        
        dto.setSys_id(dbPool.getSys_id());
        dto.setFullName(dbPool.getUPoolName());
        dto.setName(dbPool.getUPoolName());
        dto.setType(DB_POOL);
        dto.setDisplayType(DB_POOL);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    public void exportRemedyxForm(String formName)
    {
        StringBuilder hql = new StringBuilder();
        hql.append("from RemedyxForm ");
        hql.append("where UQueue = '").append(Constants.DEFAULT_GATEWAY_QUEUE_NAME).append("' ");
        //hql.append(" and UName = '").append(formName).append("'");
        hql.append(" and UName = :UName");
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UName", formName);
        
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
            if (list != null && list.size() > 0)
            {
                for (Object obj : list)
                {
                    RemedyxForm remedyxForm = (RemedyxForm)obj;
                    populateRemedyxForm(remedyxForm);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    private void populateRemedyxForm(RemedyxForm remedyxForm)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.RemedyxForm");
        
        dto.setSys_id(remedyxForm.getSys_id());
        dto.setFullName(remedyxForm.getUName());
        dto.setName(remedyxForm.getUName());
        dto.setType(REMEDYX_FORM);
        dto.setDisplayType(REMEDYX_FORM);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void exportAddressModel(String model)
    {
        StringBuilder hql = new StringBuilder();
        hql.append("from " + model);
        hql.append(" where UQueue = :queue");
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("queue", Constants.DEFAULT_GATEWAY_QUEUE_NAME);
         
        List<? extends Object> list = null;
        try
        {
            list = GeneralHibernateUtil.executeHQLSelect(hql.toString(), queryParams);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (list != null && list.size() > 0)
        {
            for (Object obj : list)
            {
                if (model.equals("EmailAddress"))
                {
                    EmailAddress address = (EmailAddress)obj;
                    populateAddress(address.getSys_id(), address.getUEmailAddress(), "EMAILSERVER_ADDRESS");
                }
                else
                {
                    EWSAddress address = (EWSAddress)obj;
                    populateAddress(address.getSys_id(), address.getUEWSAddress(), "EWS_ADDRESS");
                }
            }
        }
    }
    
    private void populateAddress(String sysId, String address, String tableName)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String name = tableName.contains("EMAIL") ? "Email Address" : "EWS Address";
        dto.setSys_id(sysId);
        dto.setFullName(address);
        dto.setName(address);
        dto.setType(name);
        dto.setDisplayType(name);
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
}