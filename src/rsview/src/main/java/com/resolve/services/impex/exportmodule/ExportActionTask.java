/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionInvocOptions;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskMockData;
import com.resolve.persistence.model.ResolveTaskExpression;
import com.resolve.persistence.model.ResolveTaskOutputMapping;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportActionTask extends ExportComponent
{
    private final static ExportActionTask exportActionTask = new ExportActionTask();
    
    protected ResolveImpexGlideVO impexGlideVO;
    protected String fullName;
    protected Boolean isResolveMaintUser;
    protected ResolveActionTask actionTask;
    protected AccessRights accessRights;
    protected ResolveActionInvoc actionInvoc;
    protected Set<String> properties = new HashSet<String>();
    protected ExportSocialComponents exportSocialComps;
    protected ManifestGraphDTO parent;
    
    private final static String MSG_TASK_EXCLUDED = "WARNING: The task '%s' is excluded from the export list\n";
  
    //USER CANNOT EXPORT COMPONENTS BELONG TO THIS LIST. ONLY RESOLVE.MAINT IS ALLOWED TO DO IT
    public final static List<String> NAMESPACE_FILTER = new ArrayList<String>();
    static
    {
        NAMESPACE_FILTER.add("resolve");
    }
    
    
    public static ExportActionTask getInstance()
    {
        return exportActionTask;
    }
    
    public void init (ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    public void setExportSocialComponent(ExportSocialComponents exportSocialComponent)
    {
        this.exportSocialComps = exportSocialComponent;
    }
    
    public ManifestGraphDTO exportActionTask(String parentWikidoc) throws Exception
    {
        parent = new ManifestGraphDTO();
        String name = impexGlideVO.getUName();
        String namespace = impexGlideVO.getUModule();
        fullName =  name + "#" + namespace;
        isResolveMaintUser = userName.equalsIgnoreCase("resolve.maint") ? true : false;
        
        if (NAMESPACE_FILTER.contains(namespace) && !isResolveMaintUser)
        {
            
            // RS-23590 - SEVERE ISSUE NEEDS URGENT ATTENTION: Resolve export not functioning (http://10.20.2.111/browse/RBA-13268)
            // ImpexUtil.logImpexMessage("WARNING: Cannot EXPORT the ActionTask '" + fullName + "' as this user has no rights. If you are exporting the #resolve namespace, please use the resolve.maint user.", true);
            // return;
            ImpexUtil.logImpexMessage("WARNING: EXPORTING the ActionTask '" + fullName + "' which belongs to the #resolve namespace. It can only be imported by the resolve.maint user.", true);
        }
        
        try
        {
            impexOptions = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch(Exception e)
        {
            throw new Exception("Could not parse Impex Options for ActionTask " + fullName);
        }
        
        try {
            actionTask = ActionTaskUtil.validateUserForActionTask(null, fullName, RightTypeEnum.edit, userName);
            parent.setType(ImpexEnum.TASK.getValue());
            parent.setName(actionTask.getUFullName());
            parent.setId(actionTask.getSys_id());
            parent.setChecksum(actionTask.getChecksum());
            if (excludeList.contains(actionTask.getSys_id()))
            {
                ImpexUtil.logImpexMessage(String.format(MSG_TASK_EXCLUDED, fullName), true);
                parent.setExported(false);
                return parent;
            }  
        } catch (Exception e) { // ActionTask does not exist (deleted/renamed
            ImpexUtil.logImpexMessage("**** Error exporting ActionTask '" + fullName + "': ActionTask could not be found.", true);
            // return null manifest graph
            return null;
        }
        
        try
        {
        	HibernateProxy.execute(() -> {
            
	            if (actionTask != null)
	            {
	                if (actionTask.getSys_id() != null) {
	                    actionTask = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(actionTask.getSys_id());
	                }
	                
	                populateActionTask();
                //exportSocialComps.populateActionTaskId(actionTask.getSys_id(), impexOptions);
	                
	                accessRights = actionTask.getAccessRights();
	                if (accessRights != null)
	                {
	                    populateAccessRights();
	                }
	                
	                actionInvoc = actionTask.getResolveActionInvoc();
	                if (actionInvoc != null)
	                {
	                    populateActionInvoc();
	                    
	                    processActionInvocOptions();
	                    
	                    processActionTaskMockdata();
	                    
	                    processActionTaskParameters();
	                    
	                    processTaskExpressions();
	                    
	                    processTaskOutputMapping();
	                    
                    if (StringUtils.isNotBlank(parentWikidoc)) { // invoke only if parent wiki is not null
                        processAutomationTaskProperties(parentWikidoc);
                    }
                    
	                    ResolveActionInvoc resolveActionInvoc = actionTask.getResolveActionInvoc();
	                    List<ManifestGraphDTO> propertyGraphDTOList = new ArrayList<>();
	                    if (resolveActionInvoc != null)
	                    {
	                        if (resolveActionInvoc.getAssess() != null)
	                        {
	                            if (fullName.equals(resolveActionInvoc.getAssess().getUName()) || !impexOptions.getExcludeRefAssessor())
	                            {
	                                List<ManifestGraphDTO> list = new ExportAssessor(resolveActionInvoc.getAssess(), this).export();
	                                if (CollectionUtils.isNotEmpty(list))
	                                {
	                                    propertyGraphDTOList.addAll(list);
	                                }
	                            }
	                        }
	                        
	                        if (resolveActionInvoc.getPreprocess() != null)
	                        {
	                            if (fullName.equals(resolveActionInvoc.getPreprocess().getUName()) || !impexOptions.getExcludeRefPreprocessor())
	                            {
	                                List<ManifestGraphDTO> list = new ExportPreprocess(resolveActionInvoc.getPreprocess(), this).export();
	                                if (CollectionUtils.isNotEmpty(list))
	                                {
	                                    propertyGraphDTOList.addAll(list);
	                                }
	                            }
	                        }
	                        
	                        if (resolveActionInvoc.getParser() != null)
	                        {
	                            if (fullName.equals(resolveActionInvoc.getParser().getUName()) || !impexOptions.getExcludeRefParser())
	                            {
	                                List<ManifestGraphDTO> list = new ExportParser(resolveActionInvoc.getParser(), this).export();
	                                if (CollectionUtils.isNotEmpty(list))
	                                {
	                                    propertyGraphDTOList.addAll(list);
	                                }
	                            }
	                        }
	                        if (CollectionUtils.isNotEmpty(propertyGraphDTOList))
	                        {
	                            parent.getChildren().addAll(propertyGraphDTOList);
	                        }
	                    }
	                    else
	                    {
	                        ImpexUtil.logImpexMessage("**** Error exporting ActionTask '" + actionTask.getUFullName() + "': ActionInvoc for this ActionTask could not be found.", true);
	                        //throw new Exception("**** Error exporting ActionTask '" + actionTaskVO.getUFullName() + "' --: No ActionInvoc for this actiontask.");
	                    }
	                }
	                else
	                {
	                    ImpexUtil.logImpexMessage("**** Error exporting ActionTask '" + actionTask.getUFullName() + "' : No ActionInvoc for this actiontask.", true);
	                }
	            }
	            else
	            {
	                ImpexUtil.logImpexMessage("**** Error exporting ActionTask '" + fullName + "': ActionTask could not be found.", true);
	                //throw new Exception("**** Error exporting ActionTask '" + actionTaskVO.getUFullName() + "' --: No ActionInvoc for this actiontask.");
	            }
	            
	        	});
        }
        catch (Throwable e)
        {
            Log.log.error(String.format("Error while exporting task '%s'", fullName), e);
            throw new Exception(e); 
        }
        
        return parent;
    }
    
    public void populateActionTask()
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveActionTask");
        dto.setType(ACTIONTASK);
        dto.setSys_id(actionTask.getSys_id());
        dto.setFullName(actionTask.getUFullName());
        dto.setModule(actionTask.getUNamespace());
        dto.setName(actionTask.getUName());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        dto.setGroup(ACTIONTASK + " " + dto.getFullName());
        ExportUtils.impexComponentList.add(dto);
    }
    
    public void populateAccessRights()
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.AccessRights");
        dto.setGroup(ACTIONTASK + " " + actionTask.getUFullName());
        dto.setType(ACTIONTASK_ACCESSRIGHTS);
        dto.setFullName(actionTask.getUFullName());
        dto.setSys_id(accessRights.getSys_id());
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
        parent.getIds().add(accessRights.getSys_id());
    }
    
    private void populateActionInvoc() throws Exception
    {
        if (actionInvoc != null)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveActionInvoc");
            dto.setGroup(ACTIONTASK + " " + actionTask.getUFullName());
            dto.setType(ACTIONINVOC);
            dto.setFullName(actionTask.getUFullName());
            dto.setSys_id(actionInvoc.getSys_id());
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
            parent.getIds().add(actionInvoc.getSys_id());
        }
        else
        {
            ImpexUtil.logImpexMessage("**** Error exporting ActionTask '" + actionTask.getUFullName() + "' : No ActionInvoc for this actiontask.", true);
            //throw new Exception("**** Error exporting ActionTask '" + actionTaskVO.getUFullName() + "' : No ActionInvoc for this actiontask.");
        }
    }
    
    private void processActionInvocOptions()
    {
        if (actionInvoc.getResolveActionInvocOptions() != null)
        {
            List<ResolveActionInvocOptions> optionsList = new ArrayList<ResolveActionInvocOptions>();
            optionsList.addAll(actionInvoc.getResolveActionInvocOptions());
            for (ResolveActionInvocOptions localOptions : optionsList)
            {
                populateActionInvocOptions(localOptions);
                if (impexOptions.getAtProperties())
                {
                    processActionInvocOptionsProperties(localOptions);
                }
            }
        }
    }
    
    private void processActionTaskMockdata()
    {
        if (actionInvoc.getMockDataCollection() != null)
        {
            Collection<ResolveActionTaskMockData> mockDataColl = actionInvoc.getMockDataCollection();
            for (ResolveActionTaskMockData mockData : mockDataColl)
            {
                populateActionTaskMockData(mockData);
            }
        }
    }
    
    private void processActionTaskParameters()
    {
        if (actionInvoc.getResolveActionParameters() != null)
        {
            List<ResolveActionParameter> parameterList = new ArrayList<ResolveActionParameter>();
            parameterList.addAll(actionInvoc.getResolveActionParameters());
            for (ResolveActionParameter parameter : parameterList)
            {
                populateActionTaskParameter(parameter);
            }
        }
    }
    
    private void processTaskExpressions()
    {
    	if (actionInvoc.getExpressions() != null)
        {
    		Collection<ResolveTaskExpression> expressions = actionInvoc.getExpressions();
    		for (ResolveTaskExpression expression : expressions)
    		{
    			populateExpression(expression);
    		}
        }
    }
    
    private void processTaskOutputMapping()
    {
    	if (actionInvoc.getOutputMappings() != null)
        {
    		Collection<ResolveTaskOutputMapping> outputMappings = actionInvoc.getOutputMappings();
    		for (ResolveTaskOutputMapping outputMapping : outputMappings)
    		{
    			populateOutputMapping(outputMapping);
    		}
        }
    }
    
    private void processAutomationTaskProperties(String parentWiki) throws Exception {
        String taskAutomationInputs = ExportUtils.getAutomationTaskInputs(null,actionTask.getSys_id(),
                        parentWiki, null, userName);
        List<ManifestGraphDTO> graphDTOList = processProperty(taskAutomationInputs, actionTask.getUFullName());
        if (graphDTOList != null)
        {
            parent.getChildren().addAll(graphDTOList);
        }
    }
    
    private void populateActionInvocOptions(ResolveActionInvocOptions vo)
    {
        if (vo != null)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveActionInvocOptions");
            dto.setGroup(ACTIONTASK + " " + actionTask.getUFullName());
            dto.setType(OPTIONS);
            dto.setName(vo.getUName());
            dto.setFullName(actionTask.getUFullName());
            dto.setSys_id(vo.getSys_id());
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
            parent.getIds().add(vo.getSys_id());
        }
    }
    
    private void populateActionTaskParameter(ResolveActionParameter vo)
    {
        if (vo != null)
        {
            if (StringUtils.isNotBlank(vo.getUName())) {
                ImpexComponentDTO dto = new ImpexComponentDTO();
                String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveActionParameter");
                dto.setGroup(ACTIONTASK + " " + actionTask.getUFullName());
                dto.setType(PARAMETER);
                dto.setFullName(vo.getUName());
                dto.setSys_id(vo.getSys_id());
                dto.setDisplayType(dto.getType());
                dto.setTablename(tableName);
                
                ExportUtils.impexComponentList.add(dto);
                parent.getIds().add(vo.getSys_id());
                
                if (impexOptions.getAtProperties())
                {
                    if (vo.getUDefaultValue() != null)
                    {
                        List<ManifestGraphDTO> propertiesGraphList = processProperty(vo.getUDefaultValue(), actionTask.getUFullName());
                        if (propertiesGraphList != null)
                            parent.getChildren().addAll(propertiesGraphList);
                    }
                }
            } else {
                ImpexUtil.logImpexMessage(String.format("The name of the resolve parameter id, %s, is blank and cannot be exported", vo.getSys_id()), true);
            }
        }
    }
    
    public void processActionInvocOptionsProperties(ResolveActionInvocOptions vo)
    {
        List<ManifestGraphDTO> graphDTOList = processProperty(vo.getUValue(), actionTask.getUFullName());
        if (graphDTOList != null)
        {
            parent.getChildren().addAll(graphDTOList);
        }
    }
    
    private void populateActionTaskMockData(ResolveActionTaskMockData mockData)
    {
        if (mockData != null)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveActionTaskMockData");
            dto.setGroup(ACTIONTASK + " " + actionTask.getUFullName());
            dto.setType(MOCK_DATA);
            dto.setFullName(mockData.getUName());
            dto.setSys_id(mockData.getSys_id());
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
            parent.getIds().add(mockData.getSys_id());
        }
    }
    
    public void populateExpression(ResolveTaskExpression expression)
    {
    	ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveTaskExpression");
        dto.setTablename(tableName);
        dto.setGroup(ACTIONTASK + " " + actionTask.getUFullName());
        dto.setSys_id(expression.getSys_id());
        dto.setType(EXPRESSION);
        dto.setFullName(expression.getExpressionType() + " for Task: " + actionTask.getUFullName());
        dto.setDisplayType(dto.getType());
        ExportUtils.impexComponentList.add(dto);
        parent.getIds().add(expression.getSys_id());
    }
    
    public void populateOutputMapping(ResolveTaskOutputMapping outputMapping)
    {
    	ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveTaskOutputMapping");
        dto.setTablename(tableName);
        dto.setGroup(ACTIONTASK + " " + actionTask.getUFullName());
        dto.setSys_id(outputMapping.getSys_id());
        dto.setType(OUTPUT_MAPPING);
        dto.setFullName("Output mapping for Task: " + actionTask.getUFullName());
        dto.setDisplayType(dto.getType());
        ExportUtils.impexComponentList.add(dto);
        parent.getIds().add(outputMapping.getSys_id());
    }
    
    public ManifestGraphDTO getParentGraphDTO()
    {
        return parent;
    }
}
