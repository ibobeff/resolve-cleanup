/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.Collection;

import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysAppModule;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.util.Log;

public class ExportMenuDefinition extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    
    public ExportMenuDefinition(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    public void export()
    {
        ImpexOptionsDTO optionsDTO = null;
        try
        {
            optionsDTO = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch (Exception e)
        {
            Log.log.error("Could not parse ImpexOptionsDTO for Menu Section: " + impexGlideVO.getUModule(), e);
        }
        
        String title = impexGlideVO.getUModule();

        SysAppApplication menuDefinition = findMenuDefinition(title, null, userName);
        
        if (menuDefinition != null)
        {
            populateMenuSecttion(menuDefinition);
            
            if (menuDefinition.getSysAppApplicationroles() != null)
            {
                populateMenuSectionRoles(menuDefinition.getSysAppApplicationroles());
            }
            
            if (optionsDTO.getmSetMenuItem())
            {
                Collection<SysAppModule> moduleColl = menuDefinition.getSysAppModules();
                populateMenuItems(moduleColl, menuDefinition);
            }
        }
    }
}
