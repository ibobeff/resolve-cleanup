/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex;

/**
 * LIst of Glide type supported by export/import
 * 
 * @author jeet.marwah
 *
 */
public enum GlideType
{
    ACTIONTASK("ACTIONTASK"),
    ACTIONTASK_BY_TAG("ACTIONTASK_BY_TAG"),
    ACTIONINVOC("ACTIONINVOC"),
    PREPROCESS("PREPROCESS"),
    PARSER("PARSER"),
    ASSESSOR("ASSESSOR"),
    OPTIONS("OPTIONS"),
    PARAMS("PARAMS"),
    DECISIONTREE("DECISIONTREE"),

    PROPERTIES("PROPERTIES"), 
    TAG("TAG"),
    WIKILOOKUP("WIKILOOKUP"),
    CRONJOB("CRONJOB"),
    MENUSET("MENUSET"),
    MENUSECTION("MENUSECTION"),
    MENUITEM("MENUITEM"),
    CUSTOMTABLE("CUSTOMTABLE"),
    METAFORMVIEW("METAFORMVIEW"),
    SOCIAL("SOCIAL"),
    MOCK("MOCK"),
    DB("DB"),
    CATALOG("CATALOG")
    ;
    
    //definition of the
    private final String name;
    
    private GlideType(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }
}
