/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.hibernate.query.Query;

import com.resolve.persistence.model.ArtifactConfiguration;
import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.ResolveImpexGlide;
import com.resolve.persistence.model.ResolveImpexModule;
import com.resolve.persistence.model.ResolveImpexWiki;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikiDocumentMetaFormRel;
import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.ExportUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.ImpexService;
import com.resolve.services.ServiceCatalog;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.customtable.ExtMetaFormLookup;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.ResolveImpexManifestVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.hibernate.vo.ResolveImpexWikiVO;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.DecisionTreeHelper;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.GlideType;
import com.resolve.services.impex.exportmodule.ExportActionTask;
import com.resolve.services.impex.exportmodule.ExportActionTaskNS;
import com.resolve.services.impex.exportmodule.ExportBusinessRule;
import com.resolve.services.impex.exportmodule.ExportCefType;
import com.resolve.services.impex.exportmodule.ExportCronJob;
import com.resolve.services.impex.exportmodule.ExportCustomDB;
import com.resolve.services.impex.exportmodule.ExportCustomTableUtil;
import com.resolve.services.impex.exportmodule.ExportFilter;
import com.resolve.services.impex.exportmodule.ExportFilterV2;
import com.resolve.services.impex.exportmodule.ExportGroup;
import com.resolve.services.impex.exportmodule.ExportImpexGlide;
import com.resolve.services.impex.exportmodule.ExportImpexModule;
import com.resolve.services.impex.exportmodule.ExportImpexWiki;
import com.resolve.services.impex.exportmodule.ExportMenuDefinition;
import com.resolve.services.impex.exportmodule.ExportMenuItem;
import com.resolve.services.impex.exportmodule.ExportMenuSet;
import com.resolve.services.impex.exportmodule.ExportMetaFormViewUtil;
import com.resolve.services.impex.exportmodule.ExportMetricThreshold;
import com.resolve.services.impex.exportmodule.ExportResolutionRouter;
import com.resolve.services.impex.exportmodule.ExportResolveProperties;
import com.resolve.services.impex.exportmodule.ExportRole;
import com.resolve.services.impex.exportmodule.ExportSocialComponents;
import com.resolve.services.impex.exportmodule.ExportSystemProperties;
import com.resolve.services.impex.exportmodule.ExportSystemScript;
import com.resolve.services.impex.exportmodule.ExportTagUtil;
import com.resolve.services.impex.exportmodule.ExportUser;
import com.resolve.services.impex.exportmodule.ExportWikiDocs;
import com.resolve.services.impex.exportmodule.ExportWikiLookup;
import com.resolve.services.impex.exportmodule.ExportWikiTemplate;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.rr.util.ResolutionRoutingUtil;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Compress;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ExportUtils
{
    private static final String ERROR_COMP_NAME_OR_ID_CANNOT_BE_BLANK = "Component name or component sysId cannot be blank.";
    private static final String MSG_NAMESPACE_EXCLUDED = "WARNING: The %s namespace, %s, is excluded from the export.";
    
    protected String moduleName;
    protected String userName;
    protected ResolveImpexModuleVO impexModuleVO;
    protected ExportWikiDocs wikiDocsExport = new ExportWikiDocs();
    protected ExportSocialComponents exportSocialComps = new ExportSocialComponents();
    protected Map<String, ImpexOptionsDTO> compOptionsMap = new HashMap<String, ImpexOptionsDTO>();

    public static Set<ImpexComponentDTO> impexComponentList = new HashSet<ImpexComponentDTO>();
    public static Set<ImpexComponentDTO> wikiComponentList = new HashSet<ImpexComponentDTO>();
    public static Set<ImpexComponentDTO> customTabledefList = new HashSet<ImpexComponentDTO>();
    public static List<String> tableNames;
    public static List<String> formNames;
    public static List<String> refTableNames = new ArrayList<>();

    protected String DESTINATION_FOLDER = null;
    protected String ZIP_FILE = null;
    protected String MODULE_FOLDER_LOCATION = null;
    protected String WIKI_FOLDER_LOCATION = null;
    protected String GLIDE_INSTALL_FOLDER_LOCATION = null;
    protected String GLIDE_UNINSTALL_FOLDER_LOCATION = null;
    protected String SOCIAL_FOLDER_LOCATION;
    protected String MANIFEST_FILE;
    protected List<ManifestGraphDTO> manifestGraphList = new ArrayList<>();
    protected boolean isNewExport = false;

    public void export(String moduleName, List<String> excludeList, String userName, boolean isNew, boolean cleanModule) throws Exception
    {
        this.moduleName = moduleName;
        this.userName = userName;
        isNewExport = isNew;

        initGlobalAttributes();
        try
        {
            initExport();
            createExportManifest("EXPORT");
            packageModuleAndExport();

            createUninstallPackage();
            changeActionToDelete();
            zip();
            storeFileToDB();
            logImportOperation();

            /*
             * A flag to keep or clean the exported module zip under rsexport folder.
             * It's true by default. Made false in MAction's exportModule API to support the old feature.
             */
            cleanup(cleanModule, false);
        }
        catch (Throwable t)
        {
            Log.log.error(String.format("Error while exporting %s module.", moduleName), t);
            ImpexUtil.logImpexMessage(String.format("Error while exporting %s module.", moduleName), true);
            completeExceptionActivities("Please refer to rsview.log for more details.");
            cleanup(true, true);
        }
    }

    private void completeExceptionActivities(String message) throws Exception
    {
        logImportOperation();
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ExportUtil.completeExceptionActivities(%s) calling " +
										"ServiceHibernate.getLatestImpexEventByValue...", 
										message));
        }
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ExportUtil.completeExceptionActivities(%s) " +
										"ServiceHibernate.getLatestImpexEventByValue returned Resolve Event [%s]", 
										message, (eventVO != null ? eventVO : "null")));
        }
        
        if (eventVO == null)
        {
            eventVO = new ResolveEventVO();
        }
        eventVO.setUValue("IMPEX:" + moduleName + ",-1," + "Error exporting: " + moduleName + ". " + message);
        ServiceHibernate.insertResolveEvent(eventVO);
        Log.log.debug(String.format("ExportUtil.completeExceptionActivities(%s) inserted Resolve Event [%s]",
        							message, eventVO));
        ImpexManifestUtil.deleteManifest(moduleName, null, userName);
    }

    private void initGlobalAttributes()
    {
        DESTINATION_FOLDER = RSContext.getResolveHome() + "rsexpert/";
        ZIP_FILE = RSContext.getResolveHome() + "rsexpert/" + moduleName + ".zip";
        MODULE_FOLDER_LOCATION = DESTINATION_FOLDER + moduleName + "/";
        MANIFEST_FILE = MODULE_FOLDER_LOCATION + "MANIFEST";
        WIKI_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.WIKI_FOLDER;
        GLIDE_INSTALL_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.GLIDE_FOLDER + "install/";
        GLIDE_UNINSTALL_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + ImpexUtil.GLIDE_FOLDER + "uninstall/";
        SOCIAL_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + "social/";
    }

    private void initExport() throws Throwable
    {
        //ServiceHibernate.clearAllResolveEventByValue("EXPORT");
        ImpexUtil.clearLogs();

        impexModuleVO = ImpexUtil.getImpexModelVOWithReferences(null, moduleName, userName);

        if (impexModuleVO == null)
        {
            throw new Exception("No module definition '" + moduleName + "' found in the DB.");
        }

        String fileName = moduleName + ".zip";
        String impexFileName = impexModuleVO.getUZipFileName();

        if (StringUtils.isEmpty(impexFileName) || !fileName.trim().equals(impexFileName))
        {
            impexModuleVO.setUZipFileName(fileName);
            ImpexUtil.saveResolveImpexModule(impexModuleVO, userName, true, "EXPORT");
        }
        
        /*
         * Resolve 6.3 onwards, from React, exclude list is populated in this column as an array of Strings.
         * If not blank, use it as exclude list.
         */
        ExportComponent.excludeList.clear();
        if (StringUtils.isNotBlank(impexModuleVO.getExcludeList()))
        {
            String excludeList = impexModuleVO.getExcludeList();
            ExportComponent.excludeList.addAll(Arrays.asList(StringUtils.split(excludeList.trim().replaceAll("\\s*,\\s*", ","), ",")));
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void createExportManifest(String operation) throws Throwable
    {
        // Clear all lists before populating.
        impexComponentList.clear();
        wikiComponentList.clear();
        customTabledefList.clear();

        List<ResolveImpexGlideVO> glideVoList = new ArrayList<ResolveImpexGlideVO>();
        List<ResolveImpexWikiVO> wikiVoList = new ArrayList<ResolveImpexWikiVO>();

        exportSocialComps.setExportLocation(MODULE_FOLDER_LOCATION + "social/");
        exportSocialComps.setUserName(userName);
        wikiDocsExport.setExportSocialComp(exportSocialComps);

        if (impexModuleVO.getResolveImpexGlides() != null)
        {
            glideVoList.addAll(impexModuleVO.getResolveImpexGlides());
        }
        
        if (CollectionUtils.isNotEmpty(glideVoList)) {
        	ImpexUtil.initImpexOperationStage(operation, "(4/24) Create Export Manifest", glideVoList.size());
        }
        
        for (ResolveImpexGlideVO glideVO : glideVoList)
        {
        	ImpexUtil.updateImpexCount(operation);
            String impexOptionJson = glideVO.getUOptions();
            ImpexOptionsDTO impexOptions = null;
            if (impexOptionJson == null)
            {
                /*
                 * Import operation should take care of this activity. But in case...
                 * It seems to be an old definition. Populate the default impexOption and save the glude.
                 */
                impexOptions = new ImpexOptionsDTO();
                impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
                glideVO.setUOptions(impexOptionJson);

                ResolveImpexGlide localGlide = new ResolveImpexGlide(glideVO);
                localGlide.setResolveImpexModule(new ResolveImpexModule(impexModuleVO));
                ImpexUtil.saveResolveImpexGlide(localGlide, userName);
            }
            else
            {
                impexOptions = ImpexUtil.getImpexOptions(glideVO.getUOptions());
            }

            String type = glideVO.getUType().replaceAll("_", "");
            // Export glide components
            if (type.equalsIgnoreCase("actiontask") || type.equalsIgnoreCase(ImpexEnum.TASK.getValue()))
            {
                ExportActionTask atExport = ExportActionTask.getInstance();
                atExport.init(glideVO);
                atExport.setUserName(userName);
                atExport.setExportSocialComponent(exportSocialComps);
                ManifestGraphDTO manifestGraph =  atExport.exportActionTask(null);
                if (manifestGraph != null) {
                    manifestGraphList.add(manifestGraph);
                }
            }
            else if (type.equalsIgnoreCase("actionTaskModule"))
            {
                String namespace = glideVO.getUModule();
                if (ExportComponent.excludeList != null && ExportComponent.excludeList.contains(namespace))
                {
                    ImpexUtil.logImpexMessage(String.format(MSG_NAMESPACE_EXCLUDED, ImpexEnum.TASK.getValue(), namespace), true);
                    return;
                }
                ExportActionTaskNS atNsExport = new ExportActionTaskNS(glideVO);
                atNsExport.setUserName(userName);
                atNsExport.setExportSocialComponent(exportSocialComps);
                List<ManifestGraphDTO> localManifestGraphList = atNsExport.exportActionTaskNS();
                manifestGraphList.addAll(localManifestGraphList);
            }
            else if (type.equalsIgnoreCase("customTable"))
            {
                String customTableName = glideVO.getUName();
                if (customTableName == null || customTableName.equals("SCHEMA"))
                {
                    // Its old definition. Grab the name from correct field.
                    customTableName = glideVO.getUModule();
                }
                ExportCustomTableUtil.setParentGraphDTO(null);
                ManifestGraphDTO manifestDTO = new ManifestGraphDTO();
                manifestDTO.setName(customTableName);
                manifestDTO.setType("table");
                populateCustomTable(customTableName, impexOptions, manifestDTO);
                manifestGraphList.add(manifestDTO);
                
                Set<String> wikiFullNames = new HashSet<>();
                wikiFullNames.addAll(ExportCustomTableUtil.getWikiNames());
                ExportCustomTableUtil.getWikiNames().clear();
                Set<String> actionTaskFullNames = new HashSet<>();
                actionTaskFullNames.addAll(ExportCustomTableUtil.getActionTaskNames());
                ExportCustomTableUtil.getActionTaskNames().clear();
                
                if (wikiFullNames.size() > 0) {
                    for (String wikiFullName : wikiFullNames){
                        populateWikiToExport(glideVO.getUOptions(), wikiFullName, manifestDTO);
                    }
                }
                
                if (actionTaskFullNames.size() > 0) {
                    for (String actionTaskFullName : actionTaskFullNames) {
                        populateActionTask(glideVO.getUOptions(), actionTaskFullName, manifestDTO);
                    }
                }
            }
            else if (type.equalsIgnoreCase("form") || type.equalsIgnoreCase("metaformview"))
            {
                String formName = glideVO.getUModule();
                ExportCustomTableUtil.setParentGraphDTO(null);
                ManifestGraphDTO manifestDTO = new ManifestGraphDTO();
                manifestDTO.setName(formName);
                manifestDTO.setType(ImpexEnum.FORM.getValue());
                ExportMetaFormViewUtil exportMetaFormViewUtil = new ExportMetaFormViewUtil(formName, impexOptions, manifestDTO);
                manifestGraphList.add(manifestDTO);
                List<ImpexComponentDTO> formImpexCompDTOList = exportMetaFormViewUtil.exportMetaFormView(true);
                customTabledefList.addAll(formImpexCompDTOList);
                populateFormChildIds(formImpexCompDTOList, manifestDTO);
                
                // read wikis and tasks of a form locally and clean the static collection maintained by form export util.
                Set<String> wikiFullNames = new HashSet<>();
                wikiFullNames.addAll(ExportCustomTableUtil.getWikiNames());
                ExportCustomTableUtil.getWikiNames().clear();
                Set<String> actionTaskFullNames = new HashSet<>();
                actionTaskFullNames.addAll(ExportCustomTableUtil.getActionTaskNames());
                ExportCustomTableUtil.getActionTaskNames().clear();
                
                if (impexOptions.getFormRBs())
                {
                    String tableName = "WikiDocumentMetaFormRel";
                    String whereClause = "UFormName = '" + formName + "'";
                    List<? extends Object> data = ImpexUtil.executeQuery(tableName, whereClause, null);
                    if (data != null && data.size() > 0)
                    {
                        List<WikiDocumentMetaFormRel> wikiMetaFormRelList = (List<WikiDocumentMetaFormRel>) data;
                        for (WikiDocumentMetaFormRel wikiMetaFormRel : wikiMetaFormRelList)
                        {
                            populateWikiToExport(glideVO.getUOptions(), wikiMetaFormRel.getUWikiDocName(), manifestDTO);
                        }
                    }
                    
                    if (wikiFullNames.size() > 0)
                    {
                    	Log.log.debug("Wiki's to be exported from form '" + formName + "': " + wikiFullNames);
                        for (String wikiFullName : wikiFullNames)
                        {
                            populateWikiToExport(glideVO.getUOptions(), wikiFullName, manifestDTO);
                        }
                    }
                }

                if (impexOptions.getFormATs())
                {
                    if (actionTaskFullNames.size() > 0)
                    {
                    	Log.log.debug("ActionTasks to be exported from form '" + formName + "': " + actionTaskFullNames);
                        for (String actionTaskFullName : actionTaskFullNames)
                        {
                            populateActionTask(glideVO.getUOptions(), actionTaskFullName, manifestDTO);
                        }
                    }
                }
            }
            else if (type.equalsIgnoreCase("tag"))
            {
                String tagName = glideVO.getUModule();
                ExportTagUtil exportTagUtil = new ExportTagUtil(tagName, userName);
                manifestGraphList.add(exportTagUtil.export());
            }
            else if (type.equalsIgnoreCase("catalog"))
            {
                String jsonDesc = glideVO.getUDescription();
                Map<String, String> descMap = new ObjectMapper().readValue(jsonDesc, HashMap.class);
                // exportSocialComps.setImpexGlideVO(glideVO);
                // exportSocialComps.addCatalogsToExport(descMap.get("id"));
                String catalogName = descMap.get("name");

                Catalog catalog = ServiceCatalog.getCatalog(null, catalogName, "system");
                if (catalog != null)
                {
                    compOptionsMap.put(catalog.getParentSysId(), impexOptions);
                    ImpexComponentDTO dto = new ImpexComponentDTO();
                    dto.setSys_id(catalog.getParentSysId());
                    dto.setName(catalog.getName());
                    dto.setFullName(catalog.getName());
                    dto.setDisplayType("CATALOG");

                    ExportUtils.impexComponentList.add(dto);
                }
                else
                {
                    ImpexUtil.logImpexMessage("Could not locate Catalog: " + descMap.get("name") + " to export.", true);
                }

            }
            else if (type.equalsIgnoreCase("process") || (type.equalsIgnoreCase("social") && glideVO.getUName().equalsIgnoreCase("process")))
            {
                String processName = glideVO.getUModule();
                try
                {
                    SocialProcessDTO process = SocialAdminUtil.getProcessByName(processName, userName);
                    compOptionsMap.put(process.getSys_id(), impexOptions);
                    populateSocialComponent(process);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage());
                }
                //exportSocialComps.addProcessToExport(processName, impexOptions);

            }
            else if (type.equalsIgnoreCase("team") || (type.equalsIgnoreCase("social") && glideVO.getUName().equalsIgnoreCase("team")))
            {
                String teamName = glideVO.getUModule();
                try
                {
                    SocialTeamDTO team = SocialAdminUtil.getTeamByName(teamName, userName);
                    compOptionsMap.put(team.getSys_id(), impexOptions);
                    populateSocialComponent(team);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage());
                }

                //exportSocialComps.addTeamToExport(teamName, impexOptions);

            }
            else if (type.equalsIgnoreCase("forum") || (type.equalsIgnoreCase("social") && glideVO.getUName().equalsIgnoreCase("forum")))
            {
                String forumName = glideVO.getUModule();
                try
                {
                    SocialForumDTO forum = SocialAdminUtil.getForumByName(forumName, userName);
                    compOptionsMap.put(forum.getSys_id(), impexOptions);
                    populateSocialComponent(forum);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage());
                }

                //exportSocialComps.addForumToExport(forumName, impexOptions);
            }
            else if (type.equalsIgnoreCase("rss") || (type.equalsIgnoreCase("social") && glideVO.getUName().equalsIgnoreCase("rss")))
            {
                String rssName = glideVO.getUModule();
                try
                {
                    SocialRssDTO rss = SocialAdminUtil.getRssByName(rssName, userName);
                    compOptionsMap.put(rss.getSys_id(), impexOptions);
                    populateSocialComponent(rss);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage());
                }

                //exportSocialComps.addRSSToExport(rssName, impexOptions);
            }
            else if (type.equalsIgnoreCase("user"))
            {
                ExportUser exportUser = new ExportUser(glideVO);
                exportUser.setUserName(userName);
                exportUser.export();

                //                List<ImpexComponentDTO> userList = exportUser.getUsersToBeExported();
                //                if (!userList.isEmpty())
                //                {
                //                    for (ImpexComponentDTO dto : userList)
                //                    {
                //                        List<String> listOfIds = new ArrayList<String>();
                //                        listOfIds.add(dto.getSys_id());
                //                        String tableName = dto.getTablename();
                //                        ExportUtil.exportTable(tableName, listOfIds, GLIDE_INSTALL_FOLDER_LOCATION, true);
                //                    }
                //                }
            }
            else if (type.equalsIgnoreCase("role"))
            {
                ExportRole exportRole = new ExportRole(glideVO);
                exportRole.setUserName(userName);
                exportRole.export();
            }
            else if (type.equalsIgnoreCase("group"))
            {
                ExportGroup exportGroup = new ExportGroup(glideVO);
                exportGroup.setUserName(userName);
                exportGroup.export();
            }
            else if (type.equalsIgnoreCase("businessrule"))
            {
                ExportBusinessRule exportBusinessRule = new ExportBusinessRule(glideVO);
                exportBusinessRule.setUserName(userName);
                exportBusinessRule.export();
            }
            else if (type.equalsIgnoreCase("systemscript"))
            {
                ExportSystemScript exportSystemScript = new ExportSystemScript(glideVO);
                exportSystemScript.setUserName(userName);
                manifestGraphList.add(exportSystemScript.export());
            }
            else if (type.equalsIgnoreCase("wikilookup"))
            {
                ExportWikiLookup exportWikiLookup = new ExportWikiLookup(glideVO);
                exportWikiLookup.setUserName(userName);
                ManifestGraphDTO parent = exportWikiLookup.export();
                
                String wikiDocFullName = exportWikiLookup.getWikiName();
                if (wikiDocFullName != null)
                {
                    populateWikiToExport(glideVO.getUOptions(), wikiDocFullName, parent);
                }
                manifestGraphList.add(parent);
            }
            else if (type.equalsIgnoreCase("scheduler") || type.equalsIgnoreCase("cronjob"))
            {
                ExportCronJob exportCronJob = new ExportCronJob(glideVO);
                exportCronJob.setUserName(userName);
                ManifestGraphDTO parent = exportCronJob.export();
                String runbookName = exportCronJob.getRunbookName();
                if (runbookName != null)
                {
                    populateWikiToExport(glideVO.getUOptions(), runbookName, parent);
            }
                manifestGraphList.add(parent);
            }
            else if (type.equalsIgnoreCase("metricThreshold") || type.equalsIgnoreCase("mtnamespace"))
            {
            	ExportMetricThreshold exportMetricThreshold = new ExportMetricThreshold(glideVO);
            	exportMetricThreshold.setUserName(userName);
            	exportMetricThreshold.export();
            }
            else if (type.equalsIgnoreCase("mtnamespace"))
            {
            	ExportMetricThreshold exportMetricThreshold = new ExportMetricThreshold(glideVO);
            	exportMetricThreshold.setUserName(userName);
            	exportMetricThreshold.export();
            }
            else if (type.equalsIgnoreCase("menuset"))
            {
                ExportMenuSet exportMenuSet = new ExportMenuSet(glideVO);
                exportMenuSet.setUserName(userName);
                exportMenuSet.export();
            }
            else if (type.equalsIgnoreCase("menudefinition") || type.equalsIgnoreCase("menusection"))
            {
                ExportMenuDefinition exportMenuDefinition = new ExportMenuDefinition(glideVO);
                exportMenuDefinition.setUserName(userName);
                exportMenuDefinition.export();
            }
            else if (type.equalsIgnoreCase("menuitem"))
            {
                ExportMenuItem exportMenuItem = new ExportMenuItem(glideVO);
                exportMenuItem.setUserName(userName);
                exportMenuItem.export();
            }
            else if (type.equalsIgnoreCase("systemproperty"))
            {
                ExportSystemProperties exportSystemProperties = new ExportSystemProperties(glideVO);
                exportSystemProperties.setUserName(userName);
                manifestGraphList.add(exportSystemProperties.export());
            }
            else if (type.equalsIgnoreCase("properties"))
            {
                ExportResolveProperties exportResolveProperties = new ExportResolveProperties(glideVO);
                exportResolveProperties.setUserName(userName);
                manifestGraphList.add(exportResolveProperties.export());
            }
            else if (type.equalsIgnoreCase("customdb"))
            {
                ExportCustomDB exportCustomDB = new ExportCustomDB(glideVO);
                exportCustomDB.setUserName(userName);
                manifestGraphList.add(exportCustomDB.export());
            }
            else if (type.equalsIgnoreCase("wikitemplate"))
            {
                ExportWikiTemplate exportWikiTemplate = new ExportWikiTemplate(glideVO);
                exportWikiTemplate.setUserName(userName);
                ManifestGraphDTO parent = exportWikiTemplate.export();

                String wikiDocFullName = exportWikiTemplate.getWikiFullName();
                if (wikiDocFullName != null)
                {
                    populateWikiToExport(glideVO.getUOptions(), wikiDocFullName, parent);
                }

                if (exportWikiTemplate.getMetaFormCompList() != null)
                {
                    customTabledefList.addAll(exportWikiTemplate.getMetaFormCompList());
                }
                manifestGraphList.add(parent);
            }
            else if (type.endsWith("Filter") || type.equals("SSHPool"))
            {
                ExportFilter exportFilter = new ExportFilter(glideVO);
                exportFilter.setUserName(userName);
                exportFilter.export();
            }
            else if (type.equalsIgnoreCase("ridmapping") ||
                            type.equalsIgnoreCase("rrmodule") ||
                            type.equalsIgnoreCase("rrschema"))
            {
                ExportResolutionRouter exportRR = new ExportResolutionRouter(glideVO);
                exportRR.setUserName(userName);
                ManifestGraphDTO parent = null;
                if (type.equalsIgnoreCase("rrschema")) {
                    parent = exportRR.exportSchema();
                    if (ImpexUtil.getImpexOptions(glideVO.getUOptions()).getIncludeMapping())
                {
                        List<RRRuleVO> mappingsList = exportRR.getSchemaMappingsToExport();
                        if (CollectionUtils.isNotEmpty(mappingsList)) {
                            List<ManifestGraphDTO> ruleChidren = new ArrayList<>();
                            mappingsList.stream().forEach(rule -> {
                                exportRR.getWikiListToExport().clear();
                                ManifestGraphDTO ruleChild = exportRR.exportSchemaMapping(rule);
                                if (CollectionUtils.isNotEmpty(exportRR.getWikiListToExport())) {
                                    exportRR.getWikiListToExport().stream().forEach(wiki -> {
                                        try {
                                            populateWikiToExport(glideVO.getUOptions(), wiki, ruleChild);
                                        }
                                        catch (Throwable e) {
                                            Log.log.error(e.getMessage(), e);
                                        }
                                    });
                                }
                                ruleChidren.add(ruleChild);
                            });
                            parent.getChildren().addAll(ruleChidren);
                            ruleChidren.clear();
                        }
                    }
                    manifestGraphList.add(parent);
                } else {
                    List<RRRuleVO> mappingsList = exportRR.getMappingsToExport();
                    if (CollectionUtils.isNotEmpty(mappingsList)) {
                        mappingsList.stream().forEach(rule -> {
                            exportRR.getWikiListToExport().clear();
                            ManifestGraphDTO ruleChild = exportRR.exportSchemaMapping(rule);
                            if (CollectionUtils.isNotEmpty(exportRR.getWikiListToExport())) {
                                exportRR.getWikiListToExport().stream().forEach(wiki -> {
                                    try {
                                        populateWikiToExport(glideVO.getUOptions(), wiki, ruleChild);
                                    }
                                    catch (Throwable e) {
                                        Log.log.error(e.getMessage(), e);
                                    }
                                });
                            }
                            manifestGraphList.add(ruleChild);
                        });
                    }
                }
            }
            else if (type.equalsIgnoreCase(ARTIFACT_TYPE))
                    {
                ExportCefType exportCef = new ExportCefType(glideVO);
                exportCef.setImpexOptions(impexOptions);
                ManifestGraphDTO parent = exportCef.exportCefType();
                parent.getChildren().addAll(exportCef.processKeys());
                List<ArtifactConfiguration> configList = exportCef.getCefActionsToExport();
                if (CollectionUtils.isNotEmpty(configList)) {
                    configList.stream().forEach(action -> {
                        try {
                            ManifestGraphDTO actionGraph = exportCef.processActifactAction(action);
                            if (actionGraph.isExported()) { // if action is not exported, skip exporting children
                            if (action.getAutomation() != null) {
                                populateWikiToExport(glideVO.getUOptions(), action.getAutomation().getUFullname(), actionGraph);
                                }
                            if (action.getTask() != null) {
                                actionGraph.getChildren().add(exportCef.exportTask(action.getTask()));
                                }
                            }

                            parent.getChildren().add(actionGraph);
            }
                        catch (Throwable e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    });
                }
                manifestGraphList.add(parent);
            }
            else if (type.equalsIgnoreCase(GATEWAY))
            {
                ExportFilterV2 exportFilter = new ExportFilterV2(glideVO);
                exportFilter.setUserName(userName);
                ManifestGraphDTO parentgraph = exportFilter.export();
                parentgraph.getChildren().forEach(childGraph -> { // Iterate through all filter nodes
                    if (CollectionUtils.isNotEmpty(childGraph.getChildren())) {
                        ManifestGraphDTO child = childGraph.getChildren().get(0); // filter will always have 1 child, a runbook.
                        childGraph.getChildren().clear(); // clear the child list. It will get populated while populating runbook graph.
                        try {
                            populateWikiToExport(glideVO.getUOptions(), child.getName(), childGraph);
                        }
                        catch (Throwable e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                });
                manifestGraphList.add(parentgraph);
            }
            rewriteStatusInfo();
        }
        
        /*
         * while exporting Resolve components (other than Wiki/RB's), it might also export related RBs (e.g. RR) and those RBs
         * will have associated tasks. So, export them as well.
         */
        wikiDocsExport.populateWikiDocs(true);
        wikiDocsExport.populateActionTaskToExport(true);

        if (impexModuleVO.getResolveImpexWikis() != null)
        {
            wikiVoList.addAll(impexModuleVO.getResolveImpexWikis());
        }

        if (wikiVoList.size() > 0)
        {
        	ImpexUtil.initImpexOperationStage(operation, "(7/24) Process WIkis to Export", wikiVoList.size());
            for (ResolveImpexWikiVO wikiVo : wikiVoList)
            {
            	ImpexUtil.updateImpexCount(operation);
                String impexOptionJson = wikiVo.getUOptions();
                String type = wikiVo.getUType();
                if (impexOptionJson == null)
                {
                    /*
                     * Import operation should take care of this activity. But in case...
                     * It seems to be an old definition. Populate the default impexOption and save the impexWiki.
                     */
                    ImpexOptionsDTO impexOptions = new ImpexOptionsDTO();
                    if (wikiVo.getUScan() == false)
                    {
                        impexOptions.setWikiForms(false);
                        impexOptions.setWikiSubRB(false);
                        impexOptions.setWikiSubDT(false);
                        impexOptions.setWikiRefATs(false);
                    }
                    impexOptionJson = new ObjectMapper().writer().writeValueAsString(impexOptions);
                    wikiVo.setUOptions(impexOptionJson);

                    ResolveImpexWiki localGlide = new ResolveImpexWiki(wikiVo);
                    localGlide.setResolveImpexModule(new ResolveImpexModule(impexModuleVO));
                    ImpexUtil.saveResolveImpexWiki(localGlide, userName);
                }

                if (type.equalsIgnoreCase(ImpexEnum.WIKI.getValue()) || type.equalsIgnoreCase("document") || type.equalsIgnoreCase(SECURITY_TEMPLATE)
                                || type.equalsIgnoreCase(RUNBOOK) || type.equalsIgnoreCase(AUTOMATION_BUILDER)
                                || type.equalsIgnoreCase(ImpexEnum.DECISIONTREE.getValue()))
                {
                    wikiDocsExport.init(wikiVo, WIKI_FOLDER_LOCATION);
                    wikiDocsExport.setUserName(userName);
                    ManifestGraphDTO manifestGraph = wikiDocsExport.populateWikiDocsToExport(null);
                    if (manifestGraph != null) {
                        manifestGraphList.add(manifestGraph);
                    }
                    // wikiDocsExport.populateActionTaskToExport();
                }
                else if (type.toLowerCase().contains("namespace"))
                {
                    String namespace = wikiVo.getUValue();
                    if (ExportComponent.excludeList != null && ExportComponent.excludeList.contains(namespace))
                    {
                        ImpexUtil.logImpexMessage(String.format(MSG_NAMESPACE_EXCLUDED, ImpexEnum.WIKI.getValue(), namespace), true);
                        return;
                    }
                    
                    StringBuilder hql = new StringBuilder();
                    hql.append("select UFullname from WikiDocument wd where wd.UNamespace = :namespace");
                    Date startDate = null;
                    Date endDate = null;

                    if (wikiVo.getUOptions() != null)
                    {
                        ImpexOptionsDTO optionsDTO = new ObjectMapper().readValue(wikiVo.getUOptions(), ImpexOptionsDTO.class);
                        optionsDTO.copyWikiNsOptionsToWiki();

                        impexOptionJson = new ObjectMapper().writer().writeValueAsString(optionsDTO);
                        wikiVo.setUOptions(impexOptionJson);

                        startDate = optionsDTO.getStartDate();
                        endDate = optionsDTO.getEndDate();

                        if (startDate != null && endDate != null)
                        {
                            hql.append(" and wd.sysUpdatedOn BETWEEN :startDate and :endDate ");
                        }
                        else if (startDate != null)
                        {
                            hql.append(" and wd.sysUpdatedOn >= :startDate ");
                        }
                        else if (endDate != null)
                        {
                            hql.append(" and wd.sysUpdatedOn <= :endDate ");
                        }
                    }

                    try
                    {
                    	final Date finalStartDate = startDate;
                    	final Date finalEndDate = endDate;
                      HibernateProxy.setCurrentUser(userName);
                    	List<String> list = (List<String>) HibernateProxy.execute(() -> {

	                        Query q = HibernateUtil.createQuery(hql.toString());
	                        
	                        q.setParameter("namespace", namespace);
	                        
	                        if (finalStartDate != null)
	                        {
	                            q.setParameter("startDate", finalStartDate);
	                        }
	                        
	                        if (finalEndDate != null)
	                        {
	                            q.setParameter("endDate", finalEndDate);
	                        }
	
	                        return q.list();

                    	});

                        if (list != null && list.size() > 0)
                        {
                            for (String fullName : list)
                            {
                                populateWikiToExport(wikiVo.getUOptions(), fullName, null);
                                rewriteStatusInfo();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error in executing HQL for VO: " + hql, e);
                                              HibernateUtil.rethrowNestedTransaction(e);
                    }
                }
                rewriteStatusInfo();
                wikiDocsExport.populateWikiDocs(false);
                wikiDocsExport.populateActionTaskToExport(false);
            }
        }
        
        if (isNewExport) {
            impexModuleVO.setManifestGraph(new ObjectMapper().writeValueAsString(manifestGraphList));
            ResolveImpexModule tempMpdule = new ResolveImpexModule(impexModuleVO);
            SaveUtil.saveResolveImpexModule(tempMpdule, userName);
        }

        new ExportImpexModule(impexModuleVO).export();
        new ExportImpexGlide(impexModuleVO).export();
        new ExportImpexWiki(impexModuleVO).export();
    }
    
    public static void populateFormChildIds(Collection<ImpexComponentDTO> formImpexCompDTOList, ManifestGraphDTO manifestDTO)
    {
        // Collect sys_ids of all the components got exported as part of form export.
        if (formImpexCompDTOList != null)
            manifestDTO.getIds().addAll(formImpexCompDTOList.stream().map(ImpexComponentDTO::getSys_id).collect(Collectors.toList()));
    }

    private void populateWikiToExport(String options, String wikiFullName, ManifestGraphDTO parentGraphDTO) throws Throwable
    {
        ResolveImpexWikiVO impexWikiVO = new ResolveImpexWikiVO();
        impexWikiVO.setUOptions(options);
        impexWikiVO.setUValue(wikiFullName);

        wikiDocsExport.init(impexWikiVO, WIKI_FOLDER_LOCATION);
        wikiDocsExport.setUserName(userName);
        ManifestGraphDTO wikiGraphDTO = wikiDocsExport.populateWikiDocsToExport(parentGraphDTO);
        if (parentGraphDTO == null && wikiGraphDTO != null) {
                manifestGraphList.add(wikiGraphDTO);
        }
        // wikiDocsExport.populateActionTaskToExport();
    }

    private void populateActionTask(String options, String taskFullName, ManifestGraphDTO parentManifestDTO) throws Exception
    {
        String[] taskFullNameArray = taskFullName.split("#");
        if (taskFullNameArray.length != 2)
        {
            Log.log.error("ActionTask full name is not correct: " + taskFullName);
            return;
        }
        String name = taskFullNameArray[0];
        String nameSpace = taskFullNameArray[1];
        ResolveImpexGlideVO glideVO = new ResolveImpexGlideVO();
        glideVO.setUName(name);
        glideVO.setUModule(nameSpace);
        glideVO.setUOptions(options);

        ExportActionTask atExport = ExportActionTask.getInstance();
        atExport.init(glideVO);
        atExport.setUserName(userName);
        atExport.setExportSocialComponent(exportSocialComps);
        parentManifestDTO.getChildren().add(atExport.exportActionTask(null));
    }

    @Deprecated
    private void rewriteStatusInfo() throws Exception
    {
    	Log.log.trace("ExportUtil.rewriteStatusInfo() calling ServiceHibernate.getLatestImpexEventByValue..."); 
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        Log.log.trace(String.format("ExportUtil.rewriteStatusInfo() ServiceHibernate.getLatestImpexEventByValue " +
									"returned Resolve Event [%s]", (eventVO != null ? eventVO : "null")));
        
        if (eventVO != null)
        {
            ServiceHibernate.insertResolveEvent(eventVO);
            Log.log.debug(String.format("ExportUtil.rewriteStatusInfo() re-inserted received Resolve Event [%s]!!!", eventVO));
        }
    }

    private void populateCustomTable(String customTableName, ImpexOptionsDTO impexOptions, ManifestGraphDTO manifestDTO) throws Exception
    {
        QueryDTO queryDTO = new QueryDTO();
        queryDTO.setModelName("CustomTable");
        queryDTO.setWhereClause("UName = '" + customTableName + "'");

        List<? extends Object> list = null;
        try
        {
            list = GeneralHibernateUtil.executeHQLSelectModel(queryDTO, 0, 1, false);
        }
        catch (Exception e)
        {
            Log.log.error("Could not read custom table: " + customTableName, e);
            return;
        }

        if (list != null && list.size() > 0)
        {
            CustomTable customTable = (CustomTable) list.get(0);
            manifestDTO.setId(customTable.getSys_id());
            manifestDTO.setChecksum(customTable.getChecksum());
            ExportCustomTableUtil customTableUtil = new ExportCustomTableUtil();
            customTableUtil.setImpexOptions(impexOptions);
            customTabledefList.addAll(customTableUtil.getMetaDataForCustomTable(customTable, manifestDTO, true));
            ImpexComponentDTO compDTO = customTableUtil.populateCustomTable();
            customTabledefList.add(compDTO);
            populateFormChildIds(customTabledefList, manifestDTO);

            if (customTableUtil.getExportData())
            {
            	String tableName = CustomTableMappingUtil.table2Class(customTableName);
            	tableName = CustomTableMappingUtil.class2TableTrueCase(tableName);
                ExportUtil.exportTable(tableName, GLIDE_INSTALL_FOLDER_LOCATION);
            }
        }
    }

    private void populateSocialComponent(SocialDTO socialDTO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        dto.setSys_id(socialDTO.getSys_id());
        dto.setName(socialDTO.getU_display_name());
        dto.setFullName(socialDTO.getU_display_name());

        if (socialDTO instanceof SocialProcessDTO)
        {
            dto.setTablename(SocialProcessDTO.TABLE_NAME);
            dto.setDisplayType("SOCIAL PROCESS");
            dto.setType("PROCESS");
        }
        else if (socialDTO instanceof SocialTeamDTO)
        {
            dto.setTablename(SocialTeamDTO.TABLE_NAME);
            dto.setDisplayType("SOCIAL TEAM");
            dto.setType("TEAM");
        }
        else if (socialDTO instanceof SocialForumDTO)
        {
            dto.setTablename(SocialForumDTO.TABLE_NAME);
            dto.setDisplayType("SOCIAL FORUM");
            dto.setType("FORUM");
        }
        else if (socialDTO instanceof SocialRssDTO)
        {
            dto.setTablename(SocialRssDTO.TABLE_NAME);
            dto.setDisplayType("RSS");
            dto.setType("RSS");
        }

        ExportUtils.impexComponentList.add(dto);
    }

    public static boolean prepareManifest(String moduleName, String userName, boolean fromUI) throws Throwable
    {
        boolean result = false;

        int count = ImpexManifestUtil.getCount(moduleName, "EXPORT", userName);
        if (count <= 0)
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("USER", userName);
            params.put("MODULE_NAME", moduleName);
            params.put("OPERATION", "EXPORT");
            ImpexService.buildExportManifestSync(params, fromUI);
            
            count = ImpexManifestUtil.getCount(moduleName, "EXPORT", userName);
            
            Log.log.debug(String.format("Export Manifest Count After building Export Manifest is %d", count));
            
            if (count > 0) {
            	result = true;
            }
        }
        else
        {
            result = true;
    	        }
        ImpexUtil.finishOperation("MANIFEST", "Export", moduleName);
        return result;
    }

    public void buildExportManifest(String moduleName, String userName, boolean fromUI) throws Throwable
    {
        List<ImpexComponentDTO> manifestList = populateExportManifest(moduleName, userName);
        ImpexManifestUtil.deleteManifest(moduleName, null, userName);
        ImpexUtil.populateResolveManifestModel(manifestList, moduleName, "EXPORT", userName);
    }

    public static Object manifestStatus(String moduleName, String userName)
    {
        boolean result = false;
        Object node = null;

        try
        {
            String json = "{\"module\": \"" + moduleName + "\",\"finished\": " + result + "}";

            ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("IMPEX-MANIFEST");
            if (eventVO != null)
            {
                String value = eventVO.getUValue();
                if (StringUtils.isNotBlank(value))
                {
                    String module = value.split(",")[0].split(":")[1];
                    if (StringUtils.isNotBlank(module) && moduleName.equals(module))
                    {
                        if (value.split(",").length == 2)
                        {
                            if (value.split(",")[1].equals("-2"))
                            {
                                result = true;
                                // ServiceHibernate.clearAllResolveEventByValue("EXPORT-MANIFEST");
                            }
                        }
                    }
                }
                json = "{\"module\": \"" + moduleName + "\",\"finished\": " + result + "}";
                node = new ObjectMapper().readValue(json, Map.class);
            }
        }
        catch (Exception e)
        {
            // Do nothing.
        }

        return node;
    }

    public List<ResolveImpexManifestVO> getExportManifest(QueryDTO query, String moduleName, String userName) throws Exception
    {
        cleanComponentList();
        List<ResolveImpexManifestVO> manifestVoList = ImpexUtil.readManifest(query, moduleName, "EXPORT");
        //        if (manifestVoList.isEmpty())
        //        {
        //            List<ImpexComponentDTO> manifestList = populateExportManifest(moduleName, userName);
        //            ImpexUtil.populateResolveManifestModel(manifestList, moduleName, "EXPORT", userName);
        //            manifestVoList = ImpexUtil.readManifest(query, moduleName, "EXPORT");
        //        }

        return manifestVoList;
    }

    private List<ImpexComponentDTO> populateExportManifest(String moduleName, String userName) throws Throwable
    {
        this.moduleName = moduleName;
        this.userName = userName;

        initGlobalAttributes();
        initExport();
        createExportManifest("MANIFEST");

        List<ImpexComponentDTO> manifestList = new ArrayList<ImpexComponentDTO>();

        if (impexComponentList != null)
        {
            manifestList.addAll(impexComponentList);
        }
        if (wikiComponentList != null)
        {
            manifestList.addAll(wikiComponentList);
        }

        manifestList.addAll(customTabledefList);

        return manifestList;
    }

    private void packageModuleAndExport() throws Throwable
    {
        ImpexUtil.logImpexMessage("******** START EXPORT  ********", false);
        ImpexUtil.logImpexMessage("Module Name: " + moduleName, false);
        ImpexUtil.logImpexMessage("Product home : " + RSContext.getResolveHome(), false);
        ImpexUtil.logImpexMessage("destinationFolder : " + DESTINATION_FOLDER, false);
        ImpexUtil.logImpexMessage("zipFileLocation : " + ZIP_FILE, false);

        /*
         * Export social comps first. This process might add more components to
         * impexComponentList to export.
         */

        List<ImpexComponentDTO> localList = new ArrayList<ImpexComponentDTO>();
        localList.addAll(impexComponentList);

        if (CollectionUtils.isNotEmpty(localList)) {
        	ImpexUtil.initImpexOperationStage("EXPORT", "(8/24) Export Components", localList.size());
        }
        
        for (ImpexComponentDTO compDTO : localList)
        {
        	ImpexUtil.updateImpexCount("EXPORT");
            String displayType = compDTO.getDisplayType();
            if (displayType.equals("CATALOG"))
            {
                String catalogId = compDTO.getSys_id();
                ImpexOptionsDTO impexOptions = compOptionsMap.get(catalogId);

                exportSocialComps.exportCatalog(catalogId, impexOptions);
            }
            else if (displayType.equals("SOCIAL PROCESS"))
            {
                String processId = compDTO.getSys_id();
                String processName = compDTO.getName();
                ImpexOptionsDTO impexOptions = compOptionsMap.get(processId);

                exportSocialComps.addProcessToExport(processName, impexOptions);
            }
            else if (displayType.equals("SOCIAL TEAM"))
            {
                String teamId = compDTO.getSys_id();
                String teamName = compDTO.getName();
                ImpexOptionsDTO impexOptions = compOptionsMap.get(teamId);

                exportSocialComps.addTeamToExport(teamName, impexOptions);
            }
            else if (displayType.equals("SOCIAL FORUM"))
            {
                String forumId = compDTO.getSys_id();
                String forumName = compDTO.getName();
                ImpexOptionsDTO impexOptions = compOptionsMap.get(forumId);

                exportSocialComps.addForumToExport(forumName, impexOptions);
            }
            else if (displayType.equals("RSS"))
            {
                String rssId = compDTO.getSys_id();
                String rssName = compDTO.getName();
                ImpexOptionsDTO impexOptions = compOptionsMap.get(rssId);

                exportSocialComps.addRSSToExport(rssName, impexOptions);
            }
        }

        exportSocialComps.export();

        Set<String> wikiNames = exportSocialComps.getWikiDocNamesToExport();
        if (CollectionUtils.isNotEmpty(wikiNames))
        {
        	Log.log.info("Wiki's to be exported from social: " + wikiNames);
        	ImpexUtil.initImpexOperationStage("EXPORT", "(15/24) Build List of Social Wiki Components to Export", 
        									  wikiNames.size());
            for (String wikiName : wikiNames)
            {
            	ImpexUtil.updateImpexCount("EXPORT");
                ResolveImpexWikiVO impexWikiVO = new ResolveImpexWikiVO();
                String options = new ObjectMapper().writer().writeValueAsString(new ImpexOptionsDTO());
                impexWikiVO.setUOptions(options);
                impexWikiVO.setUValue(wikiName);

                wikiDocsExport.init(impexWikiVO, WIKI_FOLDER_LOCATION);
                wikiDocsExport.setUserName(userName);
                ManifestGraphDTO manifestGraph = wikiDocsExport.populateWikiDocsToExport(null);
                if (manifestGraph != null) {
                    manifestGraphList.add(manifestGraph);
                }
            }
        }

//        setExportProgressInfo();

        if (CollectionUtils.isNotEmpty(impexComponentList))
        {
            exportToXml(impexComponentList, GLIDE_INSTALL_FOLDER_LOCATION);
        }
        else
        {
            ImpexUtil.logImpexMessage("No Glide Components to Export.", false);
        }

        if (CollectionUtils.isNotEmpty(customTabledefList))
        {
            exportToXml(customTabledefList, GLIDE_INSTALL_FOLDER_LOCATION + "tabledef/");
        }

        if (CollectionUtils.isNotEmpty(wikiComponentList))
        {
            wikiDocsExport.exportWikiDocuments(wikiComponentList);
            ImpexUtil.logImpexMessage("Completed. " + wikiComponentList.size() + " documents processed.", false);
            ImpexUtil.initImpexOperationStage("EXPORT", "(19/24) Create Package XML", 1);
            wikiDocsExport.createPackageXML(FileUtils.getFile(WIKI_FOLDER_LOCATION), moduleName);
            ImpexUtil.updateImpexCount("EXPORT");
        }
        wikiDocsExport.clearCollections();

        //        ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("EXPORT");
        //        if (eventVO != null)
        //        {
        //            eventVO.setUValue("EXPORT:" + moduleName + ",-2, ");
        //            ServiceHibernate.insertResolveEvent(eventVO);
        //        }
    }

    @Deprecated
    public void setExportProgressInfo() throws Exception
    {
        //ServiceHibernate.clearAllResolveEventByValue("EXPORT");
        int count = 0;
        if (impexComponentList != null)
        {
            count += impexComponentList.size();
        }
        if (wikiComponentList != null)
        {
            count += wikiComponentList.size();
        }
        if (customTabledefList != null)
        {
            count += customTabledefList.size();
        }

        Log.log.trace("ExportUtil.setExportProgressInfo() calling ServiceHibernate.getLatestImpexEventByValue..."); 
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        Log.log.trace(String.format("ExportUtil.setExportProgressInfo() ServiceHibernate.getLatestImpexEventByValue " +
									"returned Resolve Event [%s]", (eventVO != null ? eventVO : "null")));
        
        if (eventVO == null)
        {
            eventVO = new ResolveEventVO();
        }
        eventVO.setUValue("IMPEX:" + moduleName + "," + count + ",0");
        ServiceHibernate.insertResolveEvent(eventVO);
        Log.log.debug(String.format("ExportUtil.setExportProgressInfo()inserted Resolve Event [%s]", eventVO));
    }

    private void exportToXml(Set<ImpexComponentDTO> compList, String exportLocation) throws Exception
    {
        File dir = FileUtils.getFile(exportLocation);
        if (!dir.exists())
        {
            if (!dir.mkdirs())
            {
                throw new Exception(new Exception("Error creating directory -->" + dir.getName()));
            }
        }
        
        String stagePrefix = null;
        
        if (!exportLocation.toLowerCase().endsWith("tabledef/")) {
        	stagePrefix = "(16/24) Export";
        } else {
        	stagePrefix = "(17/24) Export Custom Table ";
        }
        
        if (CollectionUtils.isNotEmpty(compList)) {
        	ImpexUtil.initImpexOperationStage("EXPORT", String.format("%s Components", stagePrefix), compList.size());
        }
        
        for (ImpexComponentDTO compDTO : compList)
        {
            ImpexUtil.updateImpexCount("EXPORT");
            if (compDTO.getTablename() == null)
            {
                /*
                 * For Social components, table name is set to null and their export will happen from ExportSocialComponents.
                 * So, we need not process these entries.
                 */

                continue;
            }
            if (ExportComponent.excludeList != null && ExportComponent.excludeList.contains(compDTO.getSys_id()))
            {
                continue;
            }

            ImpexUtil.logImpexMessage("Exporting : " + compDTO.getType() + " : " + compDTO.getFullName(), false);
            List<String> listOfIds = new ArrayList<String>();
            if (compDTO.getSys_ids() != null)
            {
                listOfIds.addAll(compDTO.getSys_ids());
            }
            else
            {
                listOfIds.add(compDTO.getSys_id());
            }
            String tableName = compDTO.getTablename();

            try
            {
                if (StringUtils.isBlank(compDTO.getOperation()) || compDTO.getOperation().equals("INSERT_OR_UPDATE"))
                {
                    ExportUtil.exportTable(tableName, listOfIds, exportLocation, false);
                }
                else
                {
                    ExportUtil.exportTable(tableName, listOfIds, exportLocation, true);
                }
            }
            catch (Exception e)
            {
                ImpexUtil.logImpexMessage("Error exporting table '" + tableName + "' --:" + e, true);
                throw new Exception("Error exporting table: " + tableName + "\n" + e);
            }
        }
    }

    //    public static void updateExportCount()
    //    {
    //        ResolveEventVO eventVO = ServiceHibernate.getLatestEventByValue("EXPORT");
    //        if (eventVO != null)
    //        {
    //            String[] progress = eventVO.getUValue().split(",");
    //            if (progress.length == 3)
    //            {
    //                try
    //                {
    //                    int c = Integer.parseInt(progress[2]);
    //                    String value = progress[0] + "," + progress[1] + "," + ++c;
    //                    eventVO.setUValue(value);
    //                    ServiceHibernate.insertResolveEvent(eventVO);
    //                }
    //                catch(NumberFormatException nfe)
    //                {
    //                    // No action needs to be taken here.
    //                }
    //            }
    //        }
    //    }

    private void createUninstallPackage() throws Exception
    {
        File sourceFolter = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION);
        File destFolder = FileUtils.getFile(GLIDE_UNINSTALL_FOLDER_LOCATION);
        ImpexUtil.initImpexOperationStage("EXPORT", "(20/24) Create Uninstall Package", 1);
        FileUtils.copyDirectory(sourceFolter, destFolder);
        ImpexUtil.updateImpexCount("EXPORT");
    }

    private void changeActionToDelete() throws Exception
    {
        // Change action to UNINSTALL of all xml records present in uninstall folder
        Collection<File> fileCollection = FileUtils.listFiles(FileUtils.getFile(GLIDE_UNINSTALL_FOLDER_LOCATION), null, false);
        Iterator<File> iterator = fileCollection.iterator();
        
        if (CollectionUtils.isNotEmpty(fileCollection)) {
        	ImpexUtil.initImpexOperationStage("EXPORT", "(21/24) Change Action to DELETE for all UnInstall XML Records", 
        									   fileCollection.size());
        }
        while (iterator.hasNext())
        {
        	ImpexUtil.updateImpexCount("EXPORT");
            File file = iterator.next();
            String content;
            try
            {
                content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
                content = content.replaceAll("INSERT_OR_UPDATE", "DELETE");
                FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    private void zip() throws Exception
    {
        Compress compress = new Compress();
        ImpexUtil.initImpexOperationStage("EXPORT", "(22/24) Compress Package", 1);
        compress.zip(FileUtils.getFile(MODULE_FOLDER_LOCATION), FileUtils.getFile(ZIP_FILE));
        ImpexUtil.updateImpexCount("EXPORT");
    }

    private void storeFileToDB() throws Throwable
    {
        File zipFile = FileUtils.getFile(ZIP_FILE);

        impexModuleVO.setUZipFileName(zipFile.getName());
        impexModuleVO.setUZipFileContent(FileUtils.readFileToByteArray(zipFile));

        ImpexUtil.initImpexOperationStage("EXPORT", "(23/24) Save ZIP File to DB", 1);
        ImpexUtil.saveResolveImpexModule(impexModuleVO, userName, true, "EXPORT");
        ImpexUtil.saveResolveVersion(impexModuleVO, userName);
        ImpexUtil.updateImpexCount("EXPORT");

        ImpexUtil.logImpexMessage("******** END EXPORT  ********", false);
    }

    public static ImpexComponentDTO createDBComponent(String id, String tableName)
    {
        ImpexComponentDTO modelDTO = new ImpexComponentDTO();
        modelDTO.setSys_id(id);
        modelDTO.setFullName(tableName);
        modelDTO.setName(tableName);
        modelDTO.setType(GlideType.DB.getName());
        modelDTO.setDisplayType(modelDTO.getType());
        modelDTO.setTablename(tableName);

        return modelDTO;

    }//createDBComponent

    protected void logImportOperation()
    {
        ImpexUtil.logImpexOperation(impexModuleVO, "EXPORT", userName);
    }

    protected void cleanup(boolean cleanModule, boolean error) throws Exception
    {
        ExportComponent.excludeList.clear();
        manifestGraphList.clear();
        if (!cleanModule)
        {
            ZIP_FILE = null;
        }
        
        ImpexUtil.cleanup(MODULE_FOLDER_LOCATION, ZIP_FILE);
        cleanComponentList();
        ImpexUtil.initImpexOperationStage("EXPORT", "(24/24) Cleanup", 1);
        ImpexUtil.updateImpexCount("EXPORT");

        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ExportUtil.cleanup(%b) calling ServiceHibernate.getLatestImpexEventByValue...",
	        							cleanModule));
        }
        if (error == false) {
        ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
        if (Log.log.isTraceEnabled()) {
	        Log.log.trace(String.format("ExportUtil.cleanup(%b) calling ServiceHibernate.getLatestImpexEventByValue returned " +
	        							"Resolve Event [%s]", cleanModule, (eventVO != null ? eventVO : "null")));
        }
        
        if (eventVO != null)
        {
            eventVO.setUValue("IMPEX-EXPORT:" + moduleName + "," + -2 + ", ");
            ServiceHibernate.insertResolveEvent(eventVO);
            
            if (Log.log.isTraceEnabled()) {
            	Log.log.trace(String.format("ExportUtil.cleanup(%b) inserted/updated Resolve Event [%s]", cleanModule, eventVO));
            }
        }
        }
    }

    protected void cleanComponentList()
    {
        impexComponentList.clear();
        wikiComponentList.clear();
        customTabledefList.clear();
        
        if (tableNames != null)
        {
            tableNames.clear();
        }
        if (formNames != null)
        {
            formNames.clear();
        }
        refTableNames.clear();
    }
    
    /*
     * API to get the component details. component and compSysId are mandatory fields.
     * It returns a Map<String, Object>.
     * 
     * Sample returned map:
     * {task=[wait#resolve], wiki=[Mesh.Test102:8a9482f04de3aa96014de8d1f38b02af]}
     * This means, the runbook, supplied as an input has one task and one Wiki associated with it.
     */
    public static final String WIKI = "wiki";
    public static final String DECISIONTREE = "decisiontree";
    public static final String RUNBOOK = "runbook";
    public static final String ACTIONTASK = "task";
    public static final String FORM = "form";
    public static final String SECURITY_TEMPLATE = "securityTemplate";
    public static final String AUTOMATION_BUILDER = "automationbuilder";
    public static final String WIKI_TEMPLATE = "wikitemplate";
    public static final String WIKI_LOOKUP = "wikilookup";
    public static final String SCHEDULER = "scheduler";
    public static final String TAG = "tag";
    public static final String SYSTEM_SCRIPT = "systemScript";
    public static final String SYSTEM_PROPERTY = "systemProperty";
    public static final String CUSTOM_DB = "customDB";
    public static final String RID_MAPPING = "ridMapping";
    public static final String RR_SCHEMA = "rrSchema";
    public static final String ARTIFACT_TYPE = "artifactType";
    public static final String ARTIFACT_ACTION = "artifactaction";
    public static final String ARTIFACT_CEF_KEY = "cefKeys";
    public static final String GATEWAY_FILTER = "GATEWAY_FILTER";
    public static final String FILTERS = "filters";
    public static final String GATEWAY = "gateway";
    public static final String COMPANION = "companion";
    public static final String EWS_ADDRESS = "EWS_ADDRESS";
    public static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    public static final String XMPP_ADDRESS = "XMPP_ADDRESS";
    public static final String DB_POOL = "DB_POOL";
    public static final String REMEDYX_FORM = "REMEDYX_FORM";
    public static final String NONE = "none";
    public static final String PUSH = "Push";
    public static final String PULL = "Pull";
    public static final String MSG = "MSG";
    
    public static Map<String, Object> getCompDetails(String component, String compSysId, 
                    String parent, String subType, String username) throws Exception
    {
        Map<String, Object> resultMap = new HashMap<>();
        
        if (StringUtils.isBlank(component) || StringUtils.isBlank(compSysId))
        {
            Log.log.warn(ERROR_COMP_NAME_OR_ID_CANNOT_BE_BLANK);
            resultMap.put(ImpexEnum.ERROR.getValue(), ERROR_COMP_NAME_OR_ID_CANNOT_BE_BLANK);
        }
        
        switch(component)
        {
            case WIKI :
            case DECISIONTREE :
            case RUNBOOK :
            {
                Set<String> taskNames = new HashSet<>();
                WikiDocument wikiDoc = WikiUtils.getWikiDocumentModel(compSysId, null, username, false);
                
                if (wikiDoc != null) {
                    Set<String> taskNameSet = ImpexUtil.getTaskNamesReferedFromWiki(compSysId);
                    if (component.equals(DECISIONTREE)) {
                        taskNameSet.addAll(DecisionTreeHelper.getAllDTComponents(wikiDoc, "Task"));
                    }
                    taskNameSet.stream().forEach(s -> {
                        if (StringUtils.isNotBlank(s))
                        {
                            String taskId = ActionTaskUtil.getActionIdFromFullname(s, username);
                            if (StringUtils.isNotBlank(taskId)) {
                                taskNames.add(s + ImpexEnum.COLON.getValue() + taskId);
                            } else {
                                Log.log.error(String.format("Task name, '%s', does not exist on the system as part of wiki/runbook/DT, '%s'", s, wikiDoc.getUFullname()));
                            }
                        }
                    });
                    resultMap.put(ImpexEnum.TASK.getValue(), taskNames);
                    
                    Set<String> wikiNames = new HashSet<>();
                    Set<String> runbooks = new HashSet<>();
                    Set<String> dts = new HashSet<>();
                    WikiUtils.getWikiDocRefs(compSysId).stream().forEach(w -> {
                            if (DECISIONTREE.equals(w.getUDisplayMode())) {
                                dts.add(w.getUFullname()+ ImpexEnum.COLON.getValue() + w.getSys_id());
                            } else if (w.getUHasActiveModel()) {
                                runbooks.add(w.getUFullname()+ ImpexEnum.COLON.getValue() + w.getSys_id());
                            } else {
                                wikiNames.add(w.getUFullname()+ ImpexEnum.COLON.getValue() + w.getSys_id());
                            }
                        });
                    WikiUtils.getWikiDocDTRefs(compSysId).stream().forEach(w -> {
                        if (DECISIONTREE.equals(w.getUDisplayMode())) {
                            dts.add(w.getUFullname()+ ImpexEnum.COLON.getValue() + w.getSys_id());
                        } else if (w.getUHasActiveModel()) {
                            runbooks.add(w.getUFullname()+ ImpexEnum.COLON.getValue() + w.getSys_id());
                        } else {
                            wikiNames.add(w.getUFullname()+ ImpexEnum.COLON.getValue() + w.getSys_id());
                        }
                    });
                    if (CollectionUtils.isNotEmpty(wikiNames)) {
                        resultMap.put(ImpexEnum.WIKI.getValue(), wikiNames);
                    }
                    if (CollectionUtils.isNotEmpty(runbooks)) {
                        resultMap.put(ImpexEnum.RUNBOOK.getValue(), runbooks);
                    }
                    if (CollectionUtils.isNotEmpty(dts)) {
                        resultMap.put(ImpexEnum.DECISIONTREE.getValue(), dts);
                    }
                    
                    List<String> formNames = WikiUtils.getReferredForms(compSysId);
                    if (CollectionUtils.isNotEmpty(formNames))
                    {
                        resultMap.put(ImpexEnum.FORM.getValue(), formNames);
                    }
                }
                break;
            }
            case ACTIONTASK :
            {
                Set<String> taskProperties = getTaskProperties(compSysId, username);
                
                if (StringUtils.isNotBlank(parent)) {
                    String automationTaskInputs = getAutomationTaskInputs(null,compSysId, parent, null, username);
                    taskProperties.addAll(ParseUtil.getListOfProperties(automationTaskInputs));
                }
                if (CollectionUtils.isNotEmpty(taskProperties)) {
                    resultMap.put(ImpexEnum.PROPERTIES.getValue(), taskProperties);
                }
                break;
            }
            case FORM:
            {
                ExtMetaFormLookup lookup = new ExtMetaFormLookup(compSysId, null, null, username, RightTypeEnum.admin, null);
                lookup.setFormDefinitionOnly(false);
                resultMap.putAll(lookup.getFormComponents());
                break;
            }
            case SECURITY_TEMPLATE :
            {
                resultMap.put(ImpexEnum.WIKI.getValue(), getTemplateActivityWikiList(compSysId, username));
                break;
            }
            case WIKI_TEMPLATE :
            {
                resultMap.putAll(WikiTemplateUtil.getTemplateComponents(compSysId, username));
                break;
            }
            case WIKI_LOOKUP :
            {
                resultMap.putAll(WikiLookupUtil.getWikiLookupComponents(compSysId, username));
                break;
            }
            case SCHEDULER :
            {
                resultMap.putAll(CronUtil.getSchedulerComponents(compSysId, username));
                break;
            }
            case RID_MAPPING :
            {
                resultMap.putAll(ResolutionRoutingUtil.getMappingComponents(compSysId, username));
                break;
            }
            case RR_SCHEMA :
            {
                resultMap.putAll(ResolutionRoutingUtil.getSchemaMappings(compSysId, username));
                break;
            }
            case ARTIFACT_TYPE :
            {
                resultMap.putAll(ArtifactTypeUtil.getTypeComponents(compSysId, username));
                break;
            }
            case ARTIFACT_ACTION :
            {
                resultMap.putAll(ArtifactConfigurationUtil.getActionComponents(compSysId, username));
                break;
            }
            case GATEWAY :
            {
                resultMap.putAll(ServiceGateway.getFiltersToExport(compSysId, subType, username));
                break;
            }
            case FILTERS :
            {
                resultMap.putAll(ServiceGateway.getFilterDetails(subType, compSysId, username));
                break;
            }
            default : 
            {
                Log.log.debug(String.format("No information for component %s with sys_id %s", component, compSysId));
                break;
            }
        }
        
        return resultMap;
    }
    
    @SuppressWarnings("unchecked")
    public static String getAutomationTaskInputs(String taskName, String taskId,
                                                 String wikiName, String wikiId,
                                                 String username) throws Exception {
        if (StringUtils.isBlank(taskName) && StringUtils.isBlank(taskId)) {
            throw new Exception("Either of task name or task id is needed to read a Task");
        }
        if (StringUtils.isBlank(wikiName) && StringUtils.isBlank(wikiId)) {
            throw new Exception("Either of wiki name or wiki id is needed to read a Wiki");
        }
        
        WikiDocument parentDoc = WikiUtils.getWikiDocumentModel(wikiId, wikiName, username, false);
        ResolveActionTaskVO task = ActionTaskUtil.getActionTaskWithReferences(taskId, taskName, username);
        final StringBuilder result = new StringBuilder();
        
        if (parentDoc != null && task != null) {
            List<String> models = new ArrayList<>();
            // grab all the models from automation / DT
            if (StringUtils.isNotBlank(parentDoc.getUModelProcess()))
                models.add(parentDoc.getUModelProcess());
            if (StringUtils.isNotBlank(parentDoc.getUModelException()))
                models.add(parentDoc.getUModelException());
            if (StringUtils.isNotBlank(parentDoc.getUDecisionTree()))
                models.add(parentDoc.getUDecisionTree());
            
            if (CollectionUtils.isNotEmpty(models)) {
                models.parallelStream().forEach(model -> {
                    XDoc doc = null;
                    try {
                        doc = new XDoc(model);
                    }catch (Exception e) {
                        Log.log.error("Error while parsing the automation.");
                    }
                    if (doc != null) {
                        List<Node> nodes = doc.getRoot().selectNodes("/mxGraphModel/root/Task"); // grab all Task nodes. Same task could be found multiple times.
                        if (CollectionUtils.isNotEmpty(nodes)) {
                            nodes.parallelStream()
                                 .filter(node -> ((Element)node).attributeValue("href").trim().equals(task.getUFullName())) // filter tasks with the name
                                 .forEach(node -> { // get task input params and collect properties if it has any.
                                     if (node.selectSingleNode("params/inputs") != null) {
                                         result.append(node.selectSingleNode("params/inputs").getText().trim());
                                     }
                                 });
                        }
                    }
                });
            }
            
        } else {
            Log.log.warn(String.format("Wiki (Runbook) Or Task could not be found to read Task inputs from an automation."
                            + " Params: taskName: %s, taskId: %s, wikiName: %s, wikiId: %s", taskName, taskId, wikiName, wikiId));
        }
        
        return result.toString();
    }

    private static Set<String> getTaskProperties(String taskId, String username)
    {
        Set<String> result = new HashSet<>();
        Set<String> properties = new HashSet<>();
        ResolveActionTaskVO taskVO = ActionTaskUtil.getActionTaskWithReferences(taskId, null, username);
        
        if (taskVO != null && taskVO.getResolveActionInvoc() != null)
        {
            Collection<ResolveActionParameterVO> parameters = taskVO.getResolveActionInvoc().getResolveActionParameters();
            if (parameters != null)
            {
                parameters.stream().forEach(p -> properties.addAll(ParseUtil.getListOfProperties(p.getUDefaultValue())));
            }
            
            if (taskVO.getResolveActionInvoc().getAssess() != null)
            {
                properties.addAll(ParseUtil.getListOfProperties(taskVO.getResolveActionInvoc().getAssess().getUScript()));
            }
            
            if (taskVO.getResolveActionInvoc().getPreprocess() != null)
            {
                properties.addAll(ParseUtil.getListOfProperties(taskVO.getResolveActionInvoc().getPreprocess().getUScript()));
            }
            
            if (taskVO.getResolveActionInvoc().getParser() != null)
            {
                properties.addAll(ParseUtil.getListOfProperties(taskVO.getResolveActionInvoc().getParser().getUScript()));
            }
            
            if (!properties.isEmpty()) {
                properties.stream().forEach(name -> {
                    ResolvePropertiesVO propertiesVO = null;
                    try {
                        propertiesVO = ActionTaskPropertiesUtil.findResolveProperties(null, null, name, username);
                    } catch(Exception e) {
                        Log.log.error(String.format("Could not find Resolve Property: %s", name),  e);
                    }
                    if (propertiesVO != null) {
                        result.add(propertiesVO.getUName() + ImpexEnum.COLON.getValue() + propertiesVO.getSys_id());
                    }
                });
            }
        }
        return result;
    }
    
    private static Set<String> getTemplateActivityWikiList(String id, String username)
    {
        Set<String> result = new HashSet<>();
        String wikiFullName = WikiUtils.getWikidocFullName(id);
        if (StringUtils.isNotBlank(wikiFullName))
        {
            List<Object> contentList = WikiUtils.getWikiContentList(wikiFullName);
            if(CollectionUtils.isNotEmpty(contentList))
            {
                Object[] objArray = (Object[])contentList.get(0);
                String content = (String)objArray[1];
                
                List<Map<String, String>> activityMapList = ParseUtil.parseListOfActivityMaps(content);
                if (CollectionUtils.isNotEmpty(activityMapList))
                {
                    for (Map<String, String> activityMap : activityMapList)
                    {
                        String wikiName = activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY);
                        if (StringUtils.isNotBlank(wikiName))
                        {
                            WikiDocument localWiki = WikiUtils.getWikiDocumentModel(null, wikiName, username, false);
                            if (localWiki != null)
                            {
                                result.add(localWiki.getUFullname()+ ImpexEnum.COLON.getValue() + localWiki.getSys_id());
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
    
    public static boolean doesCompHasAChild(String component, String compSysId, String username) {
        boolean result = false;
        
        switch(component)
        {
            case WIKI :
            case DECISIONTREE :
            case RUNBOOK :
            {
                WikiDocument wikiDoc = WikiUtils.getWikiDocumentModel(compSysId, null, username, false);
                
                if (wikiDoc != null) {
                    if (doesWikiHasTasks(compSysId)) {
                        return true;
                    }
                    
                    if (doesWikiHasWikiRef(compSysId))
                        return true;
                    
                    // Forms
                    if (StringUtils.isNotBlank(wikiDoc.getUContent())) {
                        if (wikiDoc.getUContent().contains("{form:name=")) {
                            return true;
                        }
                    }
                    
                    String formName = "ABF_" + wikiDoc.getUFullname().replace(" ", "_").replace("-", "_").replace(".", "_").toUpperCase();
                    String formHql = "select count(sys_id) from meta_form_view where u_form_name = ?";
                    if (hasRecords(formHql, Arrays.asList(new Object[]{formName}))) {
                        return true;
                    }
                    
                    // DT tasks
                    if (StringUtils.isNotBlank(wikiDoc.getUDecisionTree())) {
                        if (wikiDoc.getUDecisionTree().contains("<Task label=")) {
                            return true;
                        }
                    }
                }
                break;
            }
            case ACTIONTASK :
            {
                result = doesTaskHasProperties(compSysId, username);
                break;
            }
            case SCHEDULER :
            {
                result = CronUtil.doesSchedulerHasWiki(compSysId, username);
                break;
            }
            case SECURITY_TEMPLATE :
            {
                result = doesTemplateHasWikis(compSysId);
                break;
            }
        }
        
        return result;
    }
    
    public static boolean doesWikiHasTasks(String docSysId)
    {
        boolean taskFullNameSet = false;
        
        String sql = "select count(sys_id) from content_wikidoc_task_rel where u_content_wikidoc_sys_id = ?";
        List<Object> params = new ArrayList<>();
        params.add(docSysId);
        
        if (hasRecords (sql, params)) {
            return true;
        }
        
        sql = "select count(sys_id) from main_wikidoc_task_rel where u_main_wikidoc_sys_id = ?";
        if (hasRecords (sql, params)) {
            return true;
        }
        
        sql = "select count(sys_id) from exception_wikidoc_task_rel where u_exception_wikidoc_sys_id = ?";
        if (hasRecords (sql, params)) {
            return true;
        }
        
        return taskFullNameSet;
    }
    
    public static boolean doesWikiHasWikiRef(String docId)
    {
        if (StringUtils.isBlank(docId)) {
            return false;
        }
        docId = docId.trim();
        List<Object> params = new ArrayList<>();
        params.add(docId);
        
        // WikiDoc refs from content
        String sql = "select count(sys_id) from content_wikidoc_wikidoc_rel where u_content_wikidoc_sys_id = ?";
        if (hasRecords (sql, params)) {
            return true;
        }

        // WikiDoc refs from Main
        sql = "select count(sys_id) from main_wikidoc_wikidoc_rel where u_main_wikidoc_sys_id = ?";
        if (hasRecords (sql, params)) {
            return true;
        }

        // Wikidocs refs from Exception
        sql = "select count(sys_id) from exception_wikidoc_wikidoc_rel where u_exception_wikidoc_sys_id = ?";
        if (hasRecords (sql, params)) {
            return true;
        }

        sql = "select count(sys_id) from dt_wikidoc_wikidoc_rel dtwwr where u_dt_wikidoc_sys_id = ?";
        if (hasRecords (sql, params)) {
            return true;
        }
        
        return false;
    }
    
    public static boolean hasRecords(String sql, List<Object> paramValueList)
    {
        boolean hasRecords = false;
        SQLConnection conn = null;
        try {
            conn = SQL.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, (String)paramValueList.get(0));
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                hasRecords = rs.getInt(1) > 0;
            }
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        finally {
            if (conn != null) {
                SQL.close(conn);
            }
        }

        return hasRecords;
    }
    
    private static boolean doesTaskHasProperties(String taskId, String username)
    {
        boolean result = false;
        ResolveActionTaskVO taskVO = ActionTaskUtil.getActionTaskWithReferences(taskId, null, username);
        
        if (taskVO != null && taskVO.getResolveActionInvoc() != null)
        {
            Collection<ResolveActionParameterVO> parameters = taskVO.getResolveActionInvoc().getResolveActionParameters();
            if (CollectionUtils.isNotEmpty(parameters))
            {
                result = parameters.parallelStream().filter(p -> ParseUtil.getListOfProperties(p.getUDefaultValue()).size() > 0).findAny().isPresent();
                if (result) {
                    return result;
                }
            }
            
            if (taskVO.getResolveActionInvoc().getAssess() != null)
            {
                result = ParseUtil.getListOfProperties(taskVO.getResolveActionInvoc().getAssess().getUScript()).size() > 0;
                if (result) {
                    return result;
                }
            }
            
            if (taskVO.getResolveActionInvoc().getPreprocess() != null)
            {
                result = ParseUtil.getListOfProperties(taskVO.getResolveActionInvoc().getPreprocess().getUScript()).size() > 0;
                if (result) {
                    return result;
                }
            }
            
            if (taskVO.getResolveActionInvoc().getResolveActionInvocOptions() != null)
            {
                result = taskVO.getResolveActionInvoc().getResolveActionInvocOptions().stream()
                                .filter(options -> (options.getUName().equals("INPUTFILE") && ParseUtil.getListOfProperties(options.getUValue()).size() > 0))
                                .findAny().isPresent();
                if (result) {
                    return result;
                }
            }
            
            if (taskVO.getResolveActionInvoc().getParser() != null)
            {
                result = ParseUtil.getListOfProperties(taskVO.getResolveActionInvoc().getParser().getUScript()).size() > 0;
            }
        }
        return result;
    }
    
    public static Map<String, Object> getAllActionTasksForImpex (QueryDTO queryDTO) 
    {
        List<? extends Object> resultList = null;
        Integer count = null;
        
        try
        {
            int pageSize = queryDTO.getLimit();
            int start = queryDTO.getStart();
            
            resultList = GeneralHibernateUtil.executeHQLSelectModel(queryDTO, start, pageSize, true);
            count = GeneralHibernateUtil.getTotalHqlCount(queryDTO);
             
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        List<ResolveActionTaskVO> result = ActionTaskUtil.populateResolveActionTaskVO(resultList);
        if (CollectionUtils.isNotEmpty(result)) {
            result.parallelStream().forEach(taskVO -> {
                boolean hasChildren = ExportUtils.doesCompHasAChild(ImpexEnum.TASK.getValue(), taskVO.getSys_id(), "system");
                taskVO.setHasChildren(hasChildren);
            });
        }
        
        Map<String, Object> resultMap = new HashMap<String, Object> ();
        resultMap.put("ACTION_TASK_VO_LIST", result);
        resultMap.put("TOTAL_COUNT", count);
        
        return resultMap;
    }
    
    public static boolean doesTemplateHasWikis(String docId) {
        if (StringUtils.isBlank(docId)) {
            return false;
        } 
        docId = docId.trim();
        List<Object> params = new ArrayList<>();
        params.add(docId);
        
        // WikiDoc refs from content
        String sql = "select count(sys_id) from wikidoc where u_display_mode = 'playbook' and u_content like '%\"wikiName\":%' and sys_id = ?";
        if (hasRecords (sql, params)) {
            return true;
        }
        
        return false;
    }
}
