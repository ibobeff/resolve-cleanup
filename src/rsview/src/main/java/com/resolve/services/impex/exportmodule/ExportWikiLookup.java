/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.persistence.model.ResolveWikiLookup;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Log;

public class ExportWikiLookup extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String wikiFullName;
    
    public ExportWikiLookup(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    @SuppressWarnings("unchecked")
    public ManifestGraphDTO export()
    {
        String description = impexGlideVO.getUDescription();
        ResolveWikiLookup wikiLookup = null;
        ManifestGraphDTO parent = null;
        if (description != null)
        {
            try
            {
                Map<String, String> descMap = new ObjectMapper().readValue(description, HashMap.class);
                
                String regex = descMap.get("uregex");
                
                QueryDTO query = new QueryDTO();
                query.setModelName("ResolveWikiLookup");
                List<QueryFilter> filters = new ArrayList<>();
                QueryFilter filter = new QueryFilter();
                filter.setType("auto");
                filter.setCondition(QueryFilter.EQUALS);
                filter.setValue(regex);
                filter.setField("URegex");
                filters.add(filter);
                query.setFilterItems(filters);
                
                List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
                
                if (CollectionUtils.isNotEmpty(data))
                {
                    wikiLookup = (ResolveWikiLookup)data.get(0);
                    if (wikiLookup != null) {
                        parent = new ManifestGraphDTO();
                        parent.setId(wikiLookup.getSys_id());
                        parent.setType(ExportUtils.WIKI_LOOKUP);
                        parent.setName(wikiLookup.getURegex());
                        parent.setExported(true);
                        wikiFullName = wikiLookup.getUWiki();
                    }
                }
                else
                {
                    Log.log.debug(String.format("Could not find Wiki Lookup with Regex '%s' to export.", regex));
                }
            }
            catch (Exception e)
            {
                String message = "Could not find Wiki Lookup to export.";
                Log.log.error(message, e);
                ImpexUtil.logImpexMessage(message, true);
            }
        }
        
        populateWikiLookup(wikiLookup);
        return parent;
    }
    
    private void populateWikiLookup(ResolveWikiLookup wikiLookup)
    {
        if (wikiLookup != null)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = HibernateUtil.class2Table("com.resolve.persistence.model.ResolveWikiLookup");
            
            dto.setSys_id(wikiLookup.getSys_id());
            dto.setFullName(wikiLookup.getURegex());
            dto.setModule(wikiLookup.getUModule());
            dto.setName(wikiLookup.getURegex());
            dto.setType(WIKI_LOOKUP);
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
        }
    }
    
    public String getWikiName()
    {
        return wikiFullName;
    }
}
