/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysAppModule;
import com.resolve.services.hibernate.menu.MenuSetUtil;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.hibernate.vo.SysPerspectiveVO;
import com.resolve.services.hibernate.vo.SysPerspectiveapplicationsVO;
import com.resolve.services.hibernate.vo.SysPerspectiverolesVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.util.Log;

public class ExportMenuSet extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    
    public ExportMenuSet(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
    }
    
    public void export()
    {
        ImpexOptionsDTO optionsDTO = null;
        try
        {
            optionsDTO = ImpexUtil.getImpexOptions(impexGlideVO.getUOptions());
        }
        catch (Exception e)
        {
            Log.log.error("Could not parse ImpexOptionsDTO for Menu Set: " + impexGlideVO.getUModule(), e);
        }
        
        SysPerspectiveVO perspectiveVO = MenuSetUtil.findMenuSetByIdOrByName(null, impexGlideVO.getUModule(), userName);
        populateMenuSet(perspectiveVO);
        
        if (perspectiveVO.getSysPerspectiveroles() != null)
        {
            populateMenuSetRoles(perspectiveVO.getSysPerspectiveroles());
        }
        
        List<SysPerspectiveapplicationsVO> sysPerspectiveAppVOList = perspectiveVO.getSysPerspectiveapplications();
        
        if (sysPerspectiveAppVOList != null && sysPerspectiveAppVOList.size() > 0)
        {
            List<SysAppApplication> sysAppApplicationList = new ArrayList<SysAppApplication>();
            
            for (SysPerspectiveapplicationsVO sysPerspectiveApp : sysPerspectiveAppVOList)
            {
                populateSysPerspectiveApp(sysPerspectiveApp);
                
                if (optionsDTO.getmSetMenuSection())
                {
                    String name = sysPerspectiveApp.getValue();
                    SysAppApplication menuSection = findMenuDefinition(null, name, userName);
                    if (menuSection != null)
                    {
                        sysAppApplicationList.add(menuSection);
                        populateMenuSecttion(menuSection);
                        
                        if (menuSection.getSysAppApplicationroles() != null)
                        {
                            populateMenuSectionRoles(menuSection.getSysAppApplicationroles());
                        }
                    }
                }
            }
            
            if (optionsDTO.getmSetMenuItem())
            {
                if (sysAppApplicationList.isEmpty())
                {
                    for (SysPerspectiveapplicationsVO menuSectionVO : sysPerspectiveAppVOList)
                    {
                        String name = menuSectionVO.getValue();
                        SysAppApplication menuSection = findMenuDefinition(null, name, userName);
                        if (menuSection != null)
                        {
                            Collection<SysAppModule> moduleColl = menuSection.getSysAppModules();
                            populateMenuItems(moduleColl, menuSection);
                        }
                    }
                }
                else
                {
                    for (SysAppApplication sysApp : sysAppApplicationList)
                    {
                        Collection<SysAppModule> moduleColl = sysApp.getSysAppModules();
                        populateMenuItems(moduleColl, sysApp);
                    }
                }
            }
        }
    }
    
    private void populateMenuSet(SysPerspectiveVO perspectiveVO)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysPerspective");
        
        dto.setSys_id(perspectiveVO.getSys_id());
        dto.setFullName(perspectiveVO.getName());
        dto.setModule("");
        dto.setName(perspectiveVO.getName());
        dto.setType(MENUSET);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    private void populateMenuSetRoles(List<SysPerspectiverolesVO> menuSetRoles)
    {
        for (SysPerspectiverolesVO menuSetRole : menuSetRoles)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysPerspectiveroles");
            
            dto.setSys_id(menuSetRole.getSys_id());
            dto.setFullName(menuSetRole.getValue());
            dto.setModule("");
            dto.setName(menuSetRole.getValue());
            dto.setType(MENU_SET_ROLE);
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
        }
    }
    
    private void populateSysPerspectiveApp(SysPerspectiveapplicationsVO sysPerspectiveApp)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysPerspectiveapplications");
        
        dto.setSys_id(sysPerspectiveApp.getSys_id());
        dto.setFullName(sysPerspectiveApp.getValue());
        dto.setModule("");
        dto.setName(sysPerspectiveApp.getValue());
        dto.setType(SYS_PERSPECTIVE_APP);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
}
