/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.resolve.persistence.model.SysAppApplication;
import com.resolve.persistence.model.SysAppApplicationroles;
import com.resolve.persistence.model.SysAppModule;
import com.resolve.persistence.model.SysAppModuleroles;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportComponent
{
    public final String DB = "DB";
    public final String ACTIONTASK = "ACTIONTASK";
    public final String ACTIONTASK_ACCESSRIGHTS = "ACTIONTASK_ACCESSRIGHTS";
    public final String PREPROCESS_ACCESSRIGHTS = "PREPROCESS_ACCESSRIGHTS";
    public final String ASSESS_ACCESSRIGHTS = "ASSESS_ACCESSRIGHTS";
    public final String PARSER_ACCESSRIGHTS = "PARSER_ACCESSRIGHTS";
    public final String ASSESSOR = "ASSESSOR";
    public final String PARSER = "PARSER";
    public final String PREPROCESSOR = "PREPROCESSOR";
    public final String ACTIONINVOC = "ACTIONINVOC";
    public final String OPTIONS = "OPTIONS";
    public final String PROPERTIES = "PROPERTIES";
    public final String PARAMETER = "PARAMETER";
    public final String MOCK_DATA = "MOCK_DATA";
    public final String EXPRESSION = "EXPRESSION";
    public final String OUTPUT_MAPPING = "OUTPUT_MAPPING";
    public final String USER = "USER";
    public final String TAG = "TAG";
    public final String USERROLEREL = "USER_ROLE_REL";
    public final String USERGROUPREL = "USER_GROUP_REL";
    public final String GROUPROLEREL = "GROUP_ROLE_REL";
    public final String ROLE = "ROLE";
    public final String GROUP = "GROUP";
    public final String BUSINESS_RULE = "BUSINESS_RULE";
    public final String SYSTEM_SCRIPT = "SYSTEM_SCRIPT";
    public final String WIKI_LOOKUP = "WIKI_LOOKUP";
    public final String SCHEDULER_JOB = "SCHEDULER_JOB";
    public final String MENUSET = "MENUSET";
    public final String MENU_SET_ROLE = "MENU_SET_ROLE";
    public final String MENU_DEFINITION = "MENU_DEFINITION";
    public final String MENU_DEFINITION_ROLE = "MENU_DEFINITION_ROLE";
    public final String MENU_ITEM = "MENU_ITEM";
    public final String MENU_ITEM_ROLE = "MENU_ITEM_ROLE";
    public final String SYS_PERSPECTIVE_APP = "SYS_PERSPECTIVE_APP";
    public final String RESOLVE_PROPERTIES = "RESOLVE_PROPERTIES";
    public final String SYSTEM_PROPERTIES = "SYSTEM_PROPERTIES";
    public final String WIKI_META_FORM_REL = "WIKI_META_FORM_REL";
    public final String WIKI_TEMPLATE = "WIKI_TEMPLATE";
    public final String WIKI_TEMPLATE_ACCESSRIGHTS = "WIKI_TEMPLATE_ACCESSRIGHTS";
    public final String GATEWAY_FILTER = "GATEWAY_FILTER";
    public final String DB_POOL = "DB_POOL";
    public final String SSH_POOL = "SSH_POOL";
    public final String REMEDYX_FORM = "REMEDYX_FORM";
    public final String RR_RULE_FIELD = "RR_RULE_FIELD";
    public final String RR_SCHEMA = "RR_SCHEMA";
    public final String RR_RULE = "RR_RULE";
    public final String METRIC_THRESHOLD = "METRIC_THRESHOLD";
    
    public final static String CUSTOMTABLE = "CUSTOMTABLE";
    public final static String METAFORMVIEW = "METAFORMVIEW";
    public final static String SCHEMA = "SCHEMA";
    public final static String DATA = "DATA";
    public static List<String> excludeList = new ArrayList<>();
    
    public ImpexOptionsDTO impexOptions;
    protected String userName;

    public ImpexOptionsDTO getImpexOptions()
    {
        return impexOptions;
    }

    public void setImpexOptions(ImpexOptionsDTO impexOptions)
    {
        this.impexOptions = impexOptions;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    // Common API for Menu export.
    @SuppressWarnings("unchecked")
    protected SysAppApplication findMenuDefinition(String appTitle, String appName, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (SysAppApplication) HibernateProxy.execute(() -> {
        		SysAppApplication result = null;
        		List<SysAppApplication> list = null;

	            StringBuilder sql = new StringBuilder();
	            sql.append("from SysAppApplication where "); //LOWER(title) = '" + appTitle.trim().toLowerCase() + "'";
	            boolean foundTitle = false;
	            
	            if (StringUtils.isNotBlank(appTitle))
	            {
	                foundTitle = true;
	                sql.append(" LOWER(title) = '" + appTitle.trim().toLowerCase() + "' ");
	            }
	            
	            if (StringUtils.isNotBlank(appName))
	            {
	                if(foundTitle)
	                {
	                    sql.append(" and ");
	                }
	                sql.append("LOWER(name) = '" + appName.trim().toLowerCase() + "'");
	            }
	            
	            list = HibernateUtil.createQuery(sql.toString()).list();
	        
	            if (list != null && list.size() > 0)
	            {
	                result = list.get(0);
	            }
	    
	            if (result != null)
	            {
	                if (result.getSysAppApplicationroles() != null)
	                {
	                    result.getSysAppApplicationroles().size();
	                }
	    
	                if (result.getSysAppModules() != null)
	                {
	                    for (SysAppModule module : result.getSysAppModules())
	                    {
	                        if (module.getSysAppModuleroles() != null)
	                        {
	                            module.getSysAppModuleroles().size();
	                        }
	                    }
	                }
	            }
	            
	            return result;
        	});
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return null;
    }
    
    protected void populateMenuSectionRoles (Collection<SysAppApplicationroles> menuSectionRoles)
    {
        for (SysAppApplicationroles menuSectionRole : menuSectionRoles)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysAppApplicationroles");
            
            dto.setSys_id(menuSectionRole.getSys_id());
            dto.setFullName(menuSectionRole.getValue());
            dto.setModule("");
            dto.setName(menuSectionRole.getValue());
            dto.setType(MENU_DEFINITION_ROLE);
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
        }
    }
    
    protected void populateMenuItemRoles(Collection<SysAppModuleroles> appModuleRoleColl)
    {
        for (SysAppModuleroles appModuleRole : appModuleRoleColl)
        {
            ImpexComponentDTO dto = new ImpexComponentDTO();
            
            String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysAppModuleroles");
            dto.setSys_id(appModuleRole.getSys_id());
            dto.setFullName(appModuleRole.getValue());
            dto.setModule("");
            dto.setName(appModuleRole.getValue());
            dto.setType(MENU_ITEM_ROLE);
            dto.setDisplayType(dto.getType());
            dto.setTablename(tableName);
            
            ExportUtils.impexComponentList.add(dto);
        }
    }
    
    protected void populateMenuSecttion(SysAppApplication menuSection)
    {
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysAppApplication");
        
        dto.setSys_id(menuSection.getSys_id());
        dto.setFullName(menuSection.getName());
        dto.setModule("");
        dto.setName(menuSection.getName());
        dto.setType(MENU_DEFINITION);
        dto.setDisplayType(dto.getType());
        dto.setTablename(tableName);
        
        ExportUtils.impexComponentList.add(dto);
    }
    
    protected void populateMenuItems(Collection<SysAppModule> moduleColl, SysAppApplication sysApp)
    {
        if (moduleColl != null)
        {
            for (SysAppModule module : moduleColl)
            {
                ImpexComponentDTO dto = new ImpexComponentDTO();
                String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.SysAppModule");
                String fullName = sysApp.getTitle() + ":" +module.getTitle();
                dto.setSys_id(module.getSys_id());
                dto.setFullName(fullName);
                dto.setModule("");
                dto.setName(fullName);
                dto.setType(MENU_ITEM);
                dto.setDisplayType(dto.getType());
                dto.setTablename(tableName);
                
                ExportUtils.impexComponentList.add(dto);
                
                if (module.getSysAppModuleroles() != null)
                {
                    Collection<SysAppModuleroles> appModuleRoleColl = module.getSysAppModuleroles();
                    populateMenuItemRoles(appModuleRoleColl);
                }
            }
        }
    }
    
    protected List<ManifestGraphDTO> processProperty(String content, String actionTaskFullName)
    {
        List<ManifestGraphDTO> graphDTOList = null;
        Set<String> propertyStrings = ParseUtil.getListOfProperties(content);
        if (propertyStrings.size() > 0)
        {
            graphDTOList = new ArrayList<>();
            for (String resolveProp : propertyStrings)
            {
                QueryDTO dto = new QueryDTO();
                dto.setModelName("ResolveProperties");
                dto.setSelectColumns("sys_id,UName,UModule");
                dto.setWhereClause(" UName = '" + resolveProp + "'");
                List<ResolvePropertiesVO> propertyList;
                try
                {
                    propertyList = ServiceHibernate.getResolveProperties(dto, this.userName);
                    for (ResolvePropertiesVO propertiesVO : propertyList)
                    {
                        graphDTOList.add(populateProperties(propertiesVO, actionTaskFullName, excludeList));
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("Error in getting the Resolve properties.", e);
                }
            }
        }
        return graphDTOList;
    }
    
    public ManifestGraphDTO populateProperties(ResolvePropertiesVO propertiesVO, String actionTaskFullName, List<String> excludeList)
    {
        ManifestGraphDTO graphDTO = new ManifestGraphDTO();
        graphDTO.setName(propertiesVO.getUName());
        graphDTO.setType(ImpexEnum.PROPERTIES.getValue());
        graphDTO.setId(propertiesVO.getSys_id());
        if (excludeList.contains(propertiesVO.getUName()))
        {
            graphDTO.setExported(false);
            return graphDTO;
        }
        
        ImpexComponentDTO dto = new ImpexComponentDTO();
        String tableName = GeneralHibernateUtil.getTableName("com.resolve.persistence.model.ResolveProperties");
        dto.setTablename(tableName);
        dto.setGroup(ACTIONTASK + " " + actionTaskFullName);
        String fullname = StringUtils.isBlank(propertiesVO.getUModule()) ? propertiesVO.getUName() : propertiesVO.getUModule() + "." + propertiesVO.getUName();
        dto.setFullName(fullname);
        dto.setSys_id(propertiesVO.getSys_id());
        dto.setType(PROPERTIES);
        dto.setDisplayType(dto.getType());
        ExportUtils.impexComponentList.add(dto);
        
        return graphDTO;
    }
}
