/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaTableView;
import com.resolve.persistence.model.MetaTableViewField;
import com.resolve.services.ServiceCustomTable;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.StoreUtility;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.UserPreferencesVO;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.UserPreferencesDTO;
import com.resolve.services.vo.form.DataSource;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.table.dto.RsMetaTableViewDTO;

public class ExtMetaTableLookup
{
    private String tableName;
    private String viewName;
    private String username;
    
    //TODO: rights to be implemented for the views
    private RightTypeEnum rightType = null;
    
    private Set<String> userRoles = null;
    private MetaTableView metaTableView = null;

    public ExtMetaTableLookup(String tableName, String viewName, String username, RightTypeEnum rightType) throws Exception 
    {
        if (StringUtils.isEmpty(tableName) || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Tablename and Username are mandatory for table view lookup");
        }

        this.tableName = tableName.trim();
        this.viewName = StringUtils.isNotBlank(viewName) ? viewName.trim() : "Default";
        this.username = username.trim();
        this.rightType = rightType != null ? rightType : RightTypeEnum.admin;

        init();
        
    }
    
    public ExtMetaTableLookup(String tableName, String username, RightTypeEnum rightType) throws Exception 
    {
        if (StringUtils.isEmpty(tableName) || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Tablename and Username are mandatory for table view lookup");
        }

        this.tableName = tableName.trim();
        this.username = username.trim();
        this.rightType = rightType != null ? rightType : RightTypeEnum.admin;

        init();
        
    }
    
    public List<RsMetaTableViewDTO> getMetaTableViews()  throws Exception
    {
        List<RsMetaTableViewDTO> results = new ArrayList<RsMetaTableViewDTO>();
        List<MetaTableView> views = CustomTableUtil.findMetaTableViews(tableName, username, rightType);
        
        for(MetaTableView view : views)
        {
            results.add(convertMetaTableViewToDTO(view));
        }
        
        return results;
    }
    
    public RsMetaTableViewDTO getMetaTableView()  throws Exception
    {
        RsMetaTableViewDTO view = convertMetaTableViewToDTO(this.metaTableView);
        view.setCurrentUsername(username);
        view.setUserPrefs(getUserPreferences());
        
        //fields
        view.setFields(getMetaFields(this.metaTableView.getMetaTableViewFields()));

	    view.setParentSysId(this.metaTableView.getSys_id());
	    view.setParentDisplayName(this.metaTableView.getUDisplayName());
	    view.setParentName(this.metaTableView.getUName());
	    
	    //access rights
	    view.setViewRoles(this.metaTableView.getMetaAccessRights().getUReadAccess());
	    view.setEditRoles(this.metaTableView.getMetaAccessRights().getUWriteAccess());
	    view.setAdminRoles(this.metaTableView.getMetaAccessRights().getUAdminAccess());
            
        return view;
    }
    
    private RsMetaTableViewDTO convertMetaTableViewToDTO(MetaTableView dbView)  throws Exception
    {
        RsMetaTableViewDTO view = new RsMetaTableViewDTO();
        
        view.setId(dbView.getSys_id());
        view.setName(dbView.getUName());
        view.setType(dbView.getUType());
        view.setIsGlobal(dbView.getUIsGlobal());
        view.setUser(dbView.getUUser());
        view.setMetaFormLink(dbView.getUMetaFormLink());
        view.setMetaNewLink(dbView.getUMetaNewLink());
        view.setTarget(dbView.getUTarget());
        view.setParams(dbView.getUParams());
        view.setDisplayName(dbView.getUDisplayName());
        
        view.setCreateFormId(dbView.getUCreateFormId());
        view.setEditFormId(dbView.getUEditFormId());
        
        view.setParentSysId(dbView.getSys_id());
        view.setParentDisplayName(dbView.getUDisplayName());
        view.setParentName(dbView.getUName());
            
        //get the meta table info
        CustomTableVO customTable = ServiceCustomTable.getCustomTableWithReferences(null, dbView.getMetaTable().getUName(), "system");
        view.setMetaTableSysId(dbView.getMetaTable().getSys_id());
        view.setMetaTableName(dbView.getMetaTable().getUName());
        view.setMetaTableDisplayName(customTable.getUDisplayName());
        
        view.setSysCreatedBy(dbView.getSysCreatedBy());
        view.setSysUpdatedBy(dbView.getSysUpdatedBy());
        view.setSysCreatedOn(dbView.getSysCreatedOn());
        view.setSysUpdatedOn(dbView.getSysUpdatedOn());
        
        return view;
    }
    
    private void init() throws Exception 
    {
        loadUserRoles();
        loadMetaTableView();
    }
    
    private void loadUserRoles()
    {
        // get User roles
        try
        {
            userRoles = UserUtils.getUserRoles(username);
        }
        catch (Throwable e)
        {
            userRoles = new HashSet<String>();// making sure it is not null
            Log.log.error("Error : no roles of user " + username, e);
        }
        
    }
    
    private void loadMetaTableView() throws Exception 
    {
        if(StringUtils.isNotBlank(viewName))
        {
            this.metaTableView = CustomTableUtil.findMetaTableViewWithReferences(tableName, viewName, rightType, username);
            if(this.metaTableView == null)
            {
                throw new RuntimeException("MetaTable View " + this.viewName + " for custom table " + this.tableName + " does not exist.");
            }
        }
    }
    
 // comparator for metafield sorting
    private class ComparatorMetaTableViewField implements Comparator<MetaTableViewField>
    {
        public final int compare(MetaTableViewField a, MetaTableViewField b)
        {
            // order by column
            int value = a.getUOrder() - b.getUOrder();
            if (value == 0)
            {
                // order by
                value = a.getUOrder() - b.getUOrder();
            }
            return value;
        } // end compare

    } // ComparatorBaseModel
    
    private List<RsUIField> getMetaFields(Collection<MetaTableViewField> metaTableViewFields)
    {
        ArrayList<RsUIField> fields = new ArrayList<RsUIField>();

        if(metaTableViewFields != null)
        {
            //sort it first
            ArrayList<MetaTableViewField> sortedList = new ArrayList<MetaTableViewField>(metaTableViewFields);
            Collections.sort(sortedList, new ComparatorMetaTableViewField());
            
            for(MetaTableViewField metaTableViewField : sortedList)
            {
                MetaField metaField = metaTableViewField.getMetaField();
                fields.add(convertMetaFieldToRsUIField(metaField, metaTableViewField.getMetaFieldTableProperties(), RightTypeEnum.admin, username, userRoles));
            }
        }
        
        return fields;
    }
    
    //Have to duplicate this code from ExtMetaFormLookup as there is no need of dependencies for fields in meta grid
    private static RsUIField convertMetaFieldToRsUIField(MetaField metaField, MetaFieldProperties metaFieldProperties, RightTypeEnum rightType, String userId, Set<String> userRoles)
    {
        RsUIField field = null;
        
        if(metaField != null)
        {
            if(metaFieldProperties == null)
            {
                metaFieldProperties = metaField.getMetaFieldProperties();
            }
            /**
             * if the form is in View mode, then all fields in Text/Read Only 
             * 
             * if the form is in Edit mode, then for each field
             * check if the user has Edit rights 
             * If yes, then no issue and render the field
             * If no, then check if the user has Read rights on the field
             *      If yes, then make this field as Text/RO
             *      If no, then make it hidden
             * 
             */            
            
            field = new RsUIField();
            field.setSourceType(DataSource.DB.name());
            field.setId(metaField.getSys_id());
            field.setName(metaField.getUName());
            field.setDbtable(metaField.getUTable());
            field.setDbcolumn(metaField.getUColumn());
            field.setDbcolumnId(metaField.getSys_id());
            field.setColumnModelName(metaField.getUColumnModelName());
            field.setDisplayName(metaField.getUDisplayName());
//            field.setRstype(metaField.getUType());
            field.setDbtype(metaField.getUDbType());
            
            //set additional values
            setFieldProperties(field, metaFieldProperties, metaField.getMetaAccessRights(), rightType, userId, userRoles);
        }
        
        return field;
    }
    
    private static void setFieldProperties(RsUIField field, MetaFieldProperties metaFieldProperties, MetaAccessRights metaAccessRights, RightTypeEnum rightType, String userId, Set<String> userRoles)
    {
        if(metaFieldProperties != null)
        {
            String uitype = metaFieldProperties.getUUIType();
            
            //validation of access rights for DB type
            if(field.getSourceType().equals(DataSource.DB.name()))
            {
                //for the FB, type is admin, so the original value
                if(rightType == RightTypeEnum.admin)
                {
                    field.setUiType(uitype);
                }
                else
                {
                    //this if for the form render now
                    //if the form needs to be VIEWED, then all the fields will be TEXTONLY
                    if(rightType == RightTypeEnum.view)
                    {
                        field.setUiType(HibernateConstantsEnum.UI_READONLYTEXTFIELD.getTagName());
                    }
                    else
                    {
                        /*this is righttype = Edit
                        * check if the user has Edit rights 
                        * If yes, then no issue and render the field
                        * If no, then check if the user has Read rights on the field
                        *      If yes, then make this field as Text/RO
                        *      If no, then make it hidden
                        */
                        boolean hasFieldAccessRights = StoreUtility.hasUserAccessRight(userId, userRoles, metaAccessRights.getUWriteAccess());
                        if(hasFieldAccessRights)
                        {
                            //this means user has EDIT rights on this field, so render as it
                            field.setUiType(uitype);
                        }
                        else
                        {
                            //check if the user has VIEW rights on this field
                            hasFieldAccessRights = StoreUtility.hasUserAccessRight(userId, userRoles, metaAccessRights.getUReadAccess());
                            if(hasFieldAccessRights)
                            {
                                //if the user has VIEW rights than make the field read only text
                                field.setUiType(HibernateConstantsEnum.UI_READONLYTEXTFIELD.getTagName());
                            }
                            else
                            {
                                //make this field as HIDDEN
                                field.setUiType(HibernateConstantsEnum.UI_HIDDEN.getTagName());
                            }
                        }
                    }                    
                }
            }
            else
            {
                //for INPUT types not validation required
                field.setUiType(uitype);
            }
            
            
            field.setJs(metaFieldProperties.getUJavascript());
            field.setSize(metaFieldProperties.getUSize());
            field.setEncrypted(metaFieldProperties.getUIsCrypt() != null ? metaFieldProperties.getUIsCrypt() : false);
            field.setMandatory(metaFieldProperties.getUIsMandatory() != null ? metaFieldProperties.getUIsMandatory() : false);
            
         // for sequence, generate the # that will be used as default
         // same as FormUtil.java -> UI_SEQUENCE
            if (metaFieldProperties.getUUIType().equalsIgnoreCase(HibernateConstantsEnum.UI_SEQUENCE.getTagName()))
            {
                //if it is not in form builder, then get the value
                if (rightType != RightTypeEnum.admin)
                {
                    if (StringUtils.isNotEmpty(metaFieldProperties.getUSequencePrefix()))
                    {
                        // String seq = HibernateUtil.getNextSequenceString(properties.getUSequencePrefix());
                        String seq = SequenceUtils.getNextSequence(metaFieldProperties.getUSequencePrefix());
                        field.setDefaultValue(seq);
                    }
                }
                else
                {
                    field.setDefaultValue(null);
                }
            }
            else
            {
                field.setDefaultValue(metaFieldProperties.getUDefaultValue());
            }
            
            
            field.setWidth(metaFieldProperties.getUWidth());
            field.setHeight(metaFieldProperties.getUHeight());
            field.setLabelAlign(metaFieldProperties.getULabelAlign());
            field.setBooleanMaxLength(metaFieldProperties.getUBooleanMaxLength());
            field.setStringMinLength(metaFieldProperties.getUStringMinLength());
            field.setStringMaxLength(metaFieldProperties.getUStringMaxLength());
            field.setUiStringMaxLength(metaFieldProperties.getUUIStringMaxLength());
            field.setIsMultiSelect(metaFieldProperties.ugetUSelectIsMultiSelect());
            field.setSelectUserInput(metaFieldProperties.ugetUSelectUserInput());
            field.setSelectMaxDisplay(metaFieldProperties.getUSelectMaxDisplay());
            field.setSelectValues(metaFieldProperties.getUSelectValues());
            field.setChoiceValues(metaFieldProperties.getUChoiceValues());
            field.setCheckboxValues(metaFieldProperties.getUCheckboxValues());
            field.setDateTimeHasCalendar(metaFieldProperties.getUDateTimeHasCalendar());
            field.setIntegerMinValue(metaFieldProperties.getUIntegerMinValue());
            field.setIntegerMaxValue(metaFieldProperties.getUIntegerMaxValue());
            field.setDecimalMinValue(metaFieldProperties.getUDecimalMinValue());
            field.setDecimalMaxValue(metaFieldProperties.getUDecimalMaxValue());
            field.setJournalRows(metaFieldProperties.getUJournalRows());
            field.setJournalColumns(metaFieldProperties.getUJournalColumns());
            field.setJournalMinValue(metaFieldProperties.getUJournalMinValue());
            field.setJournalMaxValue(metaFieldProperties.getUJournalMinValue());
            field.setListRows(metaFieldProperties.getUListRows());
            field.setListColumns(metaFieldProperties.getUListColumns());
            field.setListMaxDisplay(metaFieldProperties.getUListMaxDisplay());
            field.setListValues(metaFieldProperties.getUListValues());
            field.setReferenceTable(metaFieldProperties.getUReferenceTable());
            field.setReferenceDisplayColumn(metaFieldProperties.getUReferenceDisplayColumn());
            field.setReferenceDisplayColumnList(metaFieldProperties.getUReferenceDisplayColumnList());
            field.setReferenceTarget(metaFieldProperties.getUReferenceTarget());
            field.setReferenceParams(metaFieldProperties.getUReferenceParams());
            field.setLinkTarget(metaFieldProperties.getULinkTarget());
            field.setLinkParams(metaFieldProperties.getULinkParams());
            field.setLinkTemplate(metaFieldProperties.getULinkTemplate());
            field.setSequencePrefix(metaFieldProperties.getUSequencePrefix());
            field.setHiddenValue(metaFieldProperties.getUHiddenValue());
            
            field.setGroups(metaFieldProperties.getUGroups());
            field.setUsersOfTeams(metaFieldProperties.getUUsersOfTeams());
            field.setRecurseUsersOfTeam(metaFieldProperties.ugetURecurseUsersOfTeam());
            field.setTeamsOfTeams(metaFieldProperties.getUTeamsOfTeams());
            field.setRecurseTeamsOfTeam(metaFieldProperties.ugetURecurseTeamsOfTeam());
            field.setAllowAssignToMe(metaFieldProperties.ugetUAllowAssignToMe());
            field.setAutoAssignToMe(metaFieldProperties.ugetUAutoAssignToMe());
            
            field.setTeamPickerTeamsOfTeams(metaFieldProperties.getUTeamPickerTeamsOfTeams());
            
            field.setHidden(metaFieldProperties.ugetUIsHidden());
            field.setReadOnly(metaFieldProperties.ugetUIsReadOnly());
            
            //file upload properties
            field.setFileUploadTableName(metaFieldProperties.getUFileUploadTableName());
            field.setReferenceColumnName(metaFieldProperties.getUFileUploadReferenceColumnName());
            field.setAllowUpload(metaFieldProperties.getUAllowUpload());
            field.setAllowDownload(metaFieldProperties.getUAllowDownload());
            field.setAllowRemove(metaFieldProperties.getUAllowRemove());
            field.setAllowedFileTypes(metaFieldProperties.getUAllowFileTypes());
            
            //for grid ref widget
            field.setRefGridReferenceByTable(metaFieldProperties.getURefGridReferenceByTable());
            field.setRefGridSelectedReferenceColumns(metaFieldProperties.getURefGridSelectedReferenceColumns());
            field.setRefGridReferenceColumnName(metaFieldProperties.getURefGridReferenceColumnName());
            field.setAllowReferenceTableAdd(metaFieldProperties.getUAllowReferenceTableAdd());
            field.setAllowReferenceTableRemove(metaFieldProperties.getUAllowReferenceTableRemove());
            field.setAllowReferenceTableView(metaFieldProperties.getUAllowReferenceTableView());
            field.setReferenceTableUrl(metaFieldProperties.getUReferenceTableUrl());
            field.setRefGridViewPopup(metaFieldProperties.getURefGridViewPopup());
            field.setRefGridViewPopupWidth(metaFieldProperties.getURefGridViewPopupWidth());
            field.setRefGridViewPopupHeight(metaFieldProperties.getURefGridViewPopupHeight());
            field.setRefGridViewTitle(metaFieldProperties.getURefGridViewPopupTitle());
            
            field.setWidgetColumns(metaFieldProperties.getUWidgetColumns());
        }
    }
    
    private Set<UserPreferencesDTO> getUserPreferences()
    {
        Set<String> prefKeys = new HashSet<String>();
        prefKeys.add(HibernateConstants.AUTO_WIDTH_GRID_COLUMNS);
        
        Set<UserPreferencesDTO> prefs = new HashSet<UserPreferencesDTO>();
        Set<UserPreferencesVO> dbPrefs = UserUtils.getUserPreferences(username, prefKeys);
        
        for(UserPreferencesVO dbPref : dbPrefs)
        {
            UserPreferencesDTO dto = new UserPreferencesDTO();
            dto.setPrefGroup(dbPref.getUPrefGroup());
            dto.setPrefKey(dbPref.getUPrefKey());
            dto.setPrefValue(dbPref.getUPrefValue());
            
            prefs.add(dto);
        }
        
        return prefs;
    }
}