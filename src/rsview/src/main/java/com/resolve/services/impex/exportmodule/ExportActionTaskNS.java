/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.impex.exportmodule;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveImpexGlideVO;
import com.resolve.services.impex.ExportComponent;
import com.resolve.services.impex.vo.ManifestGraphDTO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.vo.QueryDTO;

public class ExportActionTaskNS extends ExportComponent
{
    protected ResolveImpexGlideVO impexGlideVO;
    protected String namespace;
    protected ExportSocialComponents exportSocialComps;
    
    public ExportActionTaskNS(ResolveImpexGlideVO impexGlideVO)
    {
        this.impexGlideVO = impexGlideVO;
        this.namespace = this.impexGlideVO.getUModule();
    }
    
    public List<ManifestGraphDTO> exportActionTaskNS () throws Exception
    {
        List<ManifestGraphDTO> manifestGraphList = new ArrayList<>();
        QueryDTO query = new QueryDTO();
        String filter = "[{\"comp\":\"=\",\"condition\":\"equals\",\"field\":\"UNamespace\",\"paramName\":\"\",\"parameters\":[{\"name\":\"UNamespace_0\",\"value\":\""+namespace+"\"}],\"type\":\"auto\",\"value\":\""+namespace+"\"}]";
        query.setFilter(filter);
        query.setModelName("ResolveActionTask");
//        query.setWhereClause(namespace + ",.");
        List<ResolveActionTaskVO> actionTaskList = ActionTaskUtil.getResolveActionTasks(query, userName).getRecords();
        if (!actionTaskList.isEmpty())
        {
            ExportActionTask atExport = null;
            for (ResolveActionTaskVO taskVO : actionTaskList)
            {
                ResolveImpexGlideVO impexGlideVO = new ResolveImpexGlideVO();
                impexGlideVO.setUName(taskVO.getUName());
                impexGlideVO.setUModule(taskVO.getUNamespace());
                impexGlideVO.setUOptions(this.impexGlideVO.getUOptions());
                
                ImpexOptionsDTO optionsDTO = new ObjectMapper().readValue(impexGlideVO.getUOptions(), ImpexOptionsDTO.class);
                optionsDTO.copyAtNsOptionsToAt();
                
                String impexOptionJson = new ObjectMapper().writer().writeValueAsString(optionsDTO);
                impexGlideVO.setUOptions(impexOptionJson);
                
                atExport = ExportActionTask.getInstance();
                atExport.init(impexGlideVO);
                atExport.setUserName(userName);
                atExport.setExportSocialComponent(exportSocialComps);
                ManifestGraphDTO manifestGraph = atExport.exportActionTask(null);
                if (manifestGraph != null) {
                    manifestGraphList.add(manifestGraph);
                }
            }
        }
        return manifestGraphList;
    }
    
    public void setExportSocialComponent(ExportSocialComponents exportSocialComponent)
    {
        this.exportSocialComps = exportSocialComponent;
    }
}
