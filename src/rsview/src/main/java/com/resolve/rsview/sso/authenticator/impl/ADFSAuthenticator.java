package com.resolve.rsview.sso.authenticator.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.saml2.core.Response;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;

import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.sso.authenticator.SSOAuthenticationDTO;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.rsview.sso.saml.SAMLManager;
import com.resolve.rsview.sso.util.SAMLEnum;
import com.resolve.rsview.sso.util.SSOUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.UserUtils;
import com.resolve.util.ERR;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;

public class ADFSAuthenticator implements SSOAuthenticator {

  private static final String ADFS_CLAIM_TYPE_NAME = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
  private static final String ADFS_CLAIM_TYPE_GROUP = "http://schemas.xmlsoap.org/claims/Group";

  private static final String ADFS_SSO_DESCRIPTION = "ADFS SSO";
  private static final String ADFS_SSO_AUTH_FAILURE_MESSAGE_PREFIX = "ADFS SSO authentication failed : ";
  private static final String ADFS_SSO_AUTH_SUCCESS_MESSAGE_PREFIX = "ADFS SSO successfully authenticated : ";

  @Override
  public String getSSOType() {
    return "ADFS";
  }

  @Override
  public SSOAuthenticationDTO authenticate(HttpServletRequest request, HttpServletResponse response) {
    SSOAuthenticationDTO result = null;

    SAMLManager samlManager = SAMLManager.INSTANCE;
    Response samlResponse;
    try {
      samlResponse = SAMLManager.INSTANCE.consumeSAMLResponse(request);
      Map<String, List<String>> attributeStatementMap = samlManager.getAttributeStatement(samlResponse);
      if (attributeStatementMap != null && attributeStatementMap.size() > 0) {
        String username = attributeStatementMap.get(ADFS_CLAIM_TYPE_NAME).get(0);
        List<String> memberOfList = attributeStatementMap.get(ADFS_CLAIM_TYPE_GROUP);
        int allExternalGroupsSize = SSOUtil.INSTANCE.getAllExternalGroups().size();
        if (allExternalGroupsSize > 0) {
          boolean externalGroupRequirementsMet = SSOUtil.INSTANCE.externalGroupRequirementsMet(username, memberOfList);
          if (externalGroupRequirementsMet) {
            UsersVO userVO = UserUtils.getUser(username);
            boolean clonedSuccessfully = false;
            if (userVO == null) {
              try {
                clonedSuccessfully = SSOUtil.INSTANCE.createUserAndAssociateGroup(username, memberOfList, getSSOType());
              } catch (Exception e) {
            	  String unauthorizedMsg = PropertiesUtil.getPropertyString(SAMLEnum.UNAUTHORIZED_MESSAGE_KEY.getValue());
            	  
            	  if (StringUtils.isNotBlank(e.getMessage()) &&
        		      (e.getMessage().equalsIgnoreCase(ERR.E10022.getMessage()) ||
        		       e.getMessage().equalsIgnoreCase(ERR.E10023.getMessage()))) {
            		  unauthorizedMsg = e.getMessage();
        		  }
            	  
                  if (StringUtils.isBlank(unauthorizedMsg))
                  {
                      unauthorizedMsg = "Please contact Resolve System Administrator.";
                  }
                  
                  String[] csrfPageTokenDetails = JspUtils.getCSRFTokenForPage(request, SAMLEnum.ERROR_JSP.getValue());
                  StringBuilder builder = new StringBuilder();
                  builder.append(SAMLEnum.ERROR_JSP.getValue()).append("?").append(csrfPageTokenDetails[0]).append("=").append(csrfPageTokenDetails[1]);
                  builder.append("&Referer=").append(request.getRequestURL());
                  request.getSession().setAttribute(SAMLEnum.UNAUTHORIZED_MESSAGE.getValue(), 
                		  							unauthorizedMsg + (StringUtils.isNotBlank(e.getMessage()) ? 
                		  											   " " + e.getMessage() : ""));
                  try
                  {
                      ESAPI.httpUtilities().sendRedirect(response, builder.toString());
                  }
                  catch (AccessControlException ae)
                  {
                      Log.log.error(SSOUtil.JSP_REDIRECT_ERROR_MSG, ae);
                      throw new ServletException(SSOUtil.JSP_REDIRECT_ERROR_MSG, ae);
                  }
              }
              if (clonedSuccessfully) {
                String logMessage = "Cloned " + ADFS_SSO_DESCRIPTION + " user " + username + " into Resolve";
                Log.auth.debug(logMessage);
                Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
              } else {
                String logMessage = ADFS_SSO_AUTH_FAILURE_MESSAGE_PREFIX + "Failed to clone user " + username + " into Resolve";
                Log.auth.warn(logMessage);
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
              }

            } else {
              boolean isLockedOut = UserUtils.isLockedOut(username);
              if (isLockedOut) {
                String logMessage = ADFS_SSO_AUTH_FAILURE_MESSAGE_PREFIX + "User " + username + " is locked";
                Log.auth.warn(logMessage);
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
                return null;
              } else {
                Log.auth.debug("ADFS user matching with Resolve found, proceed with login: " + username);
              }
            }
            result = new SSOAuthenticationDTO(username, attributeStatementMap.get(ADFS_CLAIM_TYPE_GROUP));
            String logMessage = ADFS_SSO_AUTH_SUCCESS_MESSAGE_PREFIX + "user " + username + ", referrer: " + request.getHeader("Referer");
            Log.auth.info(logMessage);
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
          } else {
            String logMessage = ADFS_SSO_AUTH_FAILURE_MESSAGE_PREFIX + "No matching external groups found";
            Log.auth.warn(logMessage);
            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
            request.getSession().setAttribute(SAMLEnum.AUTHORIZED.getValue(), "false");
            request.getSession().setAttribute(SAMLEnum.USERNAME.getValue(), username);
          }
        } else {
          String logMessage = ADFS_SSO_AUTH_FAILURE_MESSAGE_PREFIX + "No external groups defined";
          Log.auth.warn(logMessage);
          Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
        }
      } else {
        String logMessage = ADFS_SSO_AUTH_FAILURE_MESSAGE_PREFIX + "Can not authenticate due to empty 'attributeStatementMap' from the SAML assertion";
        Log.auth.warn(logMessage);
        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), "[user unknown]", request.getLocalAddr(), logMessage));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

}
