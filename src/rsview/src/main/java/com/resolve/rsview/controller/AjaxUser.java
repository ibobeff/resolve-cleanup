/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.dto.GroupDTO;
import com.resolve.dto.UploadFileDTO;
import com.resolve.dto.UserDTO;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.auth.Authentication.AuthenticateResult;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.UserChangePasswordDTO;
import com.resolve.social.SocialUtil;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

/**
 * 
 * This controller serves CRUD operation for Resolve User, Role and User. It also provide API's to manage the relationships between them.
 *
 */
@Controller
@Service
public class AjaxUser extends GenericController
{
	private static final String ERROR_ASSIGNING_GROUPS_TO_USERS = "Error assigning groups to users";
	private static final String ERROR_ASSIGNING_ROLES_TO_USERS = "Error assigning roles to users";
	private static final String ERROR_CHANGING_USER_PASSWORD = "Error changing user password";
	private static final String ERROR_CROPPING_IMAGE = "Error cropping image";
	private static final String ERROR_DELETING_GROUPS = "Error deleting Groups";
	private static final String ERROR_DELETING_ROLES = "Error deleting Roles";
	private static final String ERROR_DELETING_USERS = "Error deleting users";
	private static final String ERROR_REMOVING_ROLES_FROM_USERS = "Error removing roles from users";
	private static final String ERROR_RETRIEVING_ALL_ORGS_IN_HIERARCHY = "Error retrieving all Orgs in hierarchy for root Org named %s";
	private static final String ERROR_RETRIEVING_ALL_ORGS_IN_HIERARCHY_BY_ID = "Error retrieving all Orgs in hierarchy for root Org with Id %s";
	private static final String ERROR_RETRIEVING_GROUPS = "Error retrieving Groups";
	private static final String ERROR_RETRIEVING_ORG_BY_ID = "Error retrieving Org by id %s";
	private static final String ERROR_RETRIEVING_ORGANIZATION = "Error retrieving Organization";
	private static final String ERROR_RETRIEVING_ORGS = "Error retrieving Orgs";
	private static final String ERROR_RETRIEVING_PARENT_ORGS_FOR_CHILD = "Error retrieving valid parent Orgs for child Org named %s";
	private static final String ERROR_RETRIEVING_PARENT_ORGS_FOR_CHILD_BY_ID = "Error retrieving valid parent Orgs for child Org with Id %s";
	private static final String ERROR_RETRIEVING_PARENT_ORGS_FOR_CHILD_BY_NAME = "Error retrieving parent Orgs in hierarchy for child Org named %s";
	private static final String ERROR_RETRIEVING_ROLES = "Error retrieving Roles";
	private static final String ERROR_RETRIEVING_USERS = "Error retrieving Users";
	private static final String ERROR_SAVING_GROUP = "Error saving Group";
	private static final String ERROR_SAVING_ORG = "Error saving Org";
	private static final String ERROR_SAVING_ROLES = "Error saving Roles";
	private static final String ERROR_SAVING_USER = "Error saving user";
	private static final String ERROR_UPLOADING_IMAGE = "Error uploading image";	
	private static final String PASSWORD_CHANGE_NO_PERMISSIONS = "Error: You don't have permissions to change other user password";
	private static final String PASSWORD_CHANGE_WRONG_CREDENTIALS = "Error: Incorrect username or password";
	private static final String PAS_SWORD_CHANGE_SUCCESS = "Password updated successfully.";
	private static final String ERROR_GETTING_NAMED_USER_COUNT = "Error getttign named user count.";
	
	private static User getSocialUser(HttpServletRequest request)
    {
        return SocialUtil.getSocialUser(request);
    }

    /**
     * Downloads user profile image.
     * 
     * @param username : String representing user name.
     * @param type : String representing what type of the image needs to be downloaded.
     * If it's profile, the image size is 100 (width) by 100 (height).<br>
     * If it's post, the image size is 46 by 46 and if it's comment, the image size will be 28 by 28.  
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @param response : {@link HttpServletResponse}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/downloadProfile", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void downloadProfile(@RequestParam(defaultValue = "") String username, @RequestParam(defaultValue = "profile") String type, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            if( StringUtils.isBlank(username)) {
                username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            }

            UsersVO profileUser = ServiceHibernate.getUserByNameNoReference(username);
            if (profileUser != null)
            {
                response.addHeader("Content-Disposition", "attachment; filename=\"" + profileUser.getUName() + "profile" + "\"");
                String contentType = "image/png";
                response.setContentType(contentType + ";  name=\"" + profileUser.getUName() + "profile" + "\"");

                if (profileUser.getUImage() == null || profileUser.getUImage().length == 0)
                {
                    String filePath = request.getSession().getServletContext().getRealPath("/images/social/user.png");
                    File tempFile = FileUtils.getFile(filePath);
                    FileInputStream fis = new FileInputStream(filePath);
                    byte[] output = new byte[(int) tempFile.length()];
                    fis.read(output);
                    response.getOutputStream().write(output);
                }
                else
                {

                    BufferedImage buffer = ImageIO.read(new ByteArrayInputStream(profileUser.getUImage()));

                    int width = -1;
                    int height = -1;

                    if ("profile".equalsIgnoreCase(type))
                    {
                        width = 100;
                        height = 100;
                    }
                    if ("post".equalsIgnoreCase(type))
                    {
                        width = 46;
                        height = 46;
                    }
                    if ("comment".equalsIgnoreCase(type))
                    {
                        width = 28;
                        height = 28;
                    }
                    // Create a Graphics object to do the "drawing"
                    Image img = buffer.getScaledInstance(width, height, Image.SCALE_DEFAULT);
                    BufferedImage bufferResize = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
                    Graphics2D g2d = (Graphics2D) bufferResize.getGraphics();
                    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                    // Draw the resized image
                    g2d.drawImage(img, 0, 0, img.getWidth(null), img.getHeight(null), null);
                    g2d.dispose();
                    // Now our buffered image is ready
                    // Encode it as a png
                    ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
                    ImageIO.write(bufferResize, "png", encoderOutputStream);
                    byte[] resizedImageByteArray = encoderOutputStream.toByteArray();
                    response.setContentLength(resizedImageByteArray.length);
                    response.getOutputStream().write(resizedImageByteArray);
                }
            }
            else {
                String filePath = request.getSession().getServletContext().getRealPath("/images/social/user.png");
                File tempFile = FileUtils.getFile(filePath);
                FileInputStream fis = new FileInputStream(filePath);
                byte[] output = new byte[(int) tempFile.length()];
                fis.read(output);
                response.getOutputStream().write(output);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error downloading image", e);
        }
    }

    /**
     * Upload user image.
     * 
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve. It also contain the payload of the image file to be uploaded.
     * @return {@link ResponseDTO} with success true and data containing {@link UploadFileDTO} when operation succseeds. The Success will be set to false if there's no file payload is found
     *          or under any server side error along with the message. 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/upload", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO upload(HttpServletRequest request) throws ServletException, IOException
    {
        String username = "";
        User user = getSocialUser(request);
        if (user != null)
        {
            username = user.getName();
        }
        UsersVO profileUser = ServiceHibernate.getUser(username);
        ResponseDTO result = new ResponseDTO();
        try
        {
            FileItem file = processUpload(request);
            if (file != null)
            {
                String fileName = file.getName();
                fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
                long sizeInBytes = file.getSize();
                InputStream uploadedStream = file.getInputStream();
                byte[] data = new byte[(int) sizeInBytes];
                uploadedStream.read(data);
                profileUser.setUTempImage(data);
                ServiceHibernate.saveUser(profileUser, null, username);
                UploadFileDTO upload = new UploadFileDTO("", "", fileName);
                result.setSuccess(true).setData(upload);
            }
            else
            {
                result.setSuccess(false).setMessage("No uploaded file found");
            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_UPLOADING_IMAGE, e);
            result.setSuccess(false).setMessage(ERROR_UPLOADING_IMAGE);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    private FileItem processUpload(HttpServletRequest request) throws Exception
    {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart)
        {
            // if is an upload request, then proceed
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            // Parse the request
            List<FileItem> items = upload.parseRequest(request);

            // Process the uploaded items
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext())
            {
                FileItem item = iter.next();
                if (!item.isFormField())
                {
                    return item;
                }
            }
        }
        return null;
    }

    /**
     * Previews the user profile image after upload. UI allows to crop the image.
     * @param id : String representing user name.
     * @param height : int representing the height of the image.
     * @param width : int representing the width of the image.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @param response : {@link HttpServletResponse}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/previewProfile", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void previewProfile(@RequestParam(defaultValue = "") String id, @RequestParam int height, @RequestParam int width, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            String username = "";
            User user = getSocialUser(request);
            if (user != null)
            {
                username = user.getName();
            }
            else if (StringUtils.isNotEmpty(id))
            {
                username = id;
            }
            UsersVO profileUser = ServiceHibernate.getUser(username);
            if (profileUser != null)
            {
                response.addHeader("Content-Disposition", "attachment; filename=\"" + profileUser.getUName() + "profile" + "\"");
                String contentType = "image/png";
                response.setContentType(contentType + ";  name=\"" + profileUser.getUName() + "profile" + "\"");

                if (profileUser.getUTempImage() == null || profileUser.getUTempImage().length == 0)
                {
                    String filePath = request.getSession().getServletContext().getRealPath("/images/social/user.png");
                    File tempFile = FileUtils.getFile(filePath);
                    FileInputStream fis = new FileInputStream(filePath);
                    byte[] output = new byte[(int) tempFile.length()];
                    fis.read(output);
                    response.getOutputStream().write(output);
                }
                else
                {
                    BufferedImage buffer = ImageIO.read(new ByteArrayInputStream(profileUser.getUTempImage()));

                    if( width > height ) width = -1;
                    else if( height > width ) height = -1;
                    // Create a Graphics object to do the "drawing"
                    Image img = buffer.getScaledInstance(width, height, Image.SCALE_DEFAULT);
                    BufferedImage bufferResize = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
                    Graphics2D g2d = (Graphics2D) bufferResize.getGraphics();
                    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                    // Draw the resized image
                    g2d.drawImage(img, 0, 0, img.getWidth(null), img.getHeight(null), null);
                    g2d.dispose();
                    // Now our buffered image is ready
                    // Encode it as a png
                    ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
                    ImageIO.write(bufferResize, "png", encoderOutputStream);
                    byte[] resizedImageByteArray = encoderOutputStream.toByteArray();
                    response.setContentLength(resizedImageByteArray.length);
                    response.getOutputStream().write(resizedImageByteArray);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error downloading image", e);
        }
    }

    /**
     * Crops the image as per given width and hight and it's start location (the x and y coordinates) in the original picture and set's it as user profile image.
     * <p>
     * @param width : int representing width of the image.
     * @param height : int representing height of the image.
     * @param x : int representing x coordinate from where to crop the image.
     * @param y : int representing y coordinate from where to crop the image.
     * @param scaledHeight : int representing heigh of the original image.
     * @param scaledWidth : int representing width of the original image.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otehrwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/crop", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO cropImage(@RequestParam int width, @RequestParam int height, @RequestParam int x, @RequestParam int y, @RequestParam int scaledHeight, @RequestParam int scaledWidth, HttpServletRequest request) throws ServletException, IOException
    {
        String username = "";
        User user = getSocialUser(request);
        if (user != null)
        {
            username = user.getName();
        }

        ResponseDTO result = new ResponseDTO();
        try
        {
            //get the user object
            UsersVO profileUser = ServiceHibernate.getUser(username);
            if(profileUser == null)
            {
                throw new Exception("there is no user with Username " + username);
            }

            //prepare the image
            BufferedImage buffer = ImageIO.read(new ByteArrayInputStream(profileUser.getUTempImage()));
            if( scaledWidth > scaledHeight )
                scaledWidth = -1;
            else if( scaledHeight > scaledWidth )
                scaledHeight = -1;

            Image img = buffer.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_DEFAULT);
            BufferedImage bufferResize = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);

            // Draw the resized image
            Graphics2D g2d = (Graphics2D) bufferResize.getGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g2d.drawImage(img, 0, 0, img.getWidth(null), img.getHeight(null), x, y, x+width, y+height, null);
            g2d.dispose();

            // Now our buffered image is ready, Encode it as a png
            ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
            ImageIO.write(bufferResize, "png", encoderOutputStream);
            byte[] resizedImageByteArray = encoderOutputStream.toByteArray();

            //update the userobject
            profileUser.setUImage(resizedImageByteArray);
            profileUser.setUUserP_assword(VO.STRING_DEFAULT);//setting the password to be default string as there is no intention of changing that

            //persist it
            ServiceHibernate.saveUser(profileUser, null, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_CROPPING_IMAGE, e);
            result.setSuccess(false).setMessage(ERROR_CROPPING_IMAGE);
        }

        return result;
    }

    /**
     * API to list all Resolve users in the UI.
     * <p>
     * @param query - {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and users list as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/user/listUsers", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
	public ResponseDTO<UserDTO> listUsers(@ModelAttribute QueryDTO query, HttpServletRequest request)
			throws ServletException, IOException {
        ResponseDTO<UserDTO> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try {
            query.setModelName("Users");
            
            Map<String, Object> resultMap = ServiceHibernate.getUsersV2(query, username);
            int total = (int)resultMap.get("COUNT");
            List<UserDTO> list = (List<UserDTO>)resultMap.get("USERS");
            
            result.setSuccess(true).setRecords(list);
            result.setTotal(total);
        } catch (Exception e) {
            Log.log.error(ERROR_RETRIEVING_USERS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_USERS);
        }

        return result;
    }

    /**
     * Get detailed information about any specific user represented by <code>username</code>
     * @param username : String representing <code>username</code> whose detailed info is needed.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and user as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getUser", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<UserDTO> getUser(@RequestParam String username, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<UserDTO> result = new ResponseDTO<>();

        try
        {
            if( StringUtils.isBlank(username)) {
                username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            }
            UsersVO vo = ServiceHibernate.getUserByNameNoCache(username);
			UserDTO userDTO = new UserDTO();
			userDTO.fromUserVO(vo);

            result.setData(userDTO);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_ORGANIZATION, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ORGANIZATION);
        }

        return result;
    }

    /**
     * Saves Resolve user represented by <code>jsonProperty</code>
     * Sample jsonProperty looks something like this:
     * <pre><code>
     * {
           "uuserName": "user",
           "ufirstName": "Resolve User",
           "ulastName": "",
           "upasswordNeedsReset": "false",
           "ulockedOut": "false",
           "utitle": "Example Account",
           "uemail": "",
           "uhomePhone": "",
           "umobilePhone": "",
           "ufax": "",
           "uim": "",
           "ustreet": "",
           "ucity": "",
           "ustate": "",
           "uzip": "",
           "ucountry": "",
           "usummary": "",
           "uquestion1": "",
           "uquestion2": "",
           "uquestion3": "",
           "uanswer1": "",
           "uanswer2": "",
           "uanswer3": "",
           "ustartPage": "",
           "uhomePage": "",
           "sysCreatedBy": "resolve",
           "sysUpdatedBy": "user",
           "sysOrg": "",
           "id": "718dc1a8c6112272007fadd3e939af1d",
           "sysCreatedOn": "2006-04-06T04:33:36",
           "sysUpdatedOn": "2015-04-16T18:17:54",
           "userRoles": [],
           "userGroups": [
              {
                 "id": "2c9181e631e2cfda0131e2e3f916006b",
                 "uname": "User Group"
              }
           ],
           // Only if it's a clone user.
           "cloneUsername":"user"
    }
    </code></pre>
     * if it's a clone user save operation, jsonProperty would have one more property, <code>"cloneUsername"</code> whose value will be the username of the user whose clone is made. 
     * @param jsonProperty : String representing user object.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link UserDTO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = Constants.USER_SAVE_USER, method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<UserDTO> saveUser(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        String backendJSON = UsersVO.getBackEndJSON(json);
        UsersVO entity = new ObjectMapper().readValue(backendJSON, UsersVO.class);
        
        ResponseDTO<UserDTO> result = new ResponseDTO<UserDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            UsersVO vo = ServiceHibernate.saveUser(entity, entity.getCloneUsername(), username);
            UserDTO userDTO = new UserDTO();
            userDTO.fromUserVO(vo);
            result.setSuccess(true).setData(userDTO).setMessage("User Created successfully.");
        }
        catch (RuntimeException re)
        {
        	Log.log.error(ERROR_SAVING_USER, re);
            String failReason = ERROR_SAVING_USER + ": " + re.getMessage();
            result.setSuccess(false).setMessage(failReason);
        }
        catch (Exception e)
        {
        	String errMsg = null;
        	
        	if (StringUtils.isNotBlank(e.getMessage())) {
	        	if (e.getMessage().equals(ERR.E10022.getMessage())) {
	        		errMsg = ERR.E10022.getMessage();
	        	} else if (e.getMessage().equals(ERR.E10023.getMessage())) {
	        		errMsg = ERR.E10023.getMessage();
	        	} else if (e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
	        			   (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
	        				e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX))) {
	        		errMsg = e.getMessage();
	        	}
        	}
        					
        	if (StringUtils.isBlank(errMsg)) {
        		errMsg = ERROR_SAVING_USER;
        		Log.log.error(errMsg, e);
        	}
        	
            result.setSuccess(false).setMessage(errMsg);            
        }

        return result;
    }

    /**
     * Delete user
     * <p>
     * @param ids : String array of sys_ids representing user in Resolve.
     * @param query : {@link QueryDTO}. Not used at this time.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/deleteUser", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO deleteUser(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Collection<String> deletedUserNames = Collections.emptyList();
        
        try
        {
            deletedUserNames = ServiceHibernate.deleteUsersByIdsAndQuery(ids, query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_USERS, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_USERS);
        }

        if (result.isSuccess())
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.collectionToString(deletedUserNames, " , "), 
                            request.getLocalAddr(), username + " successfully deleted user(s)"));
        }
        else
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.listToString(Arrays.asList(ids), " , "), 
                            request.getLocalAddr(), username + " failed to delete user(s)"));
        }
        
        return result;
    }

    /**
     * List all the roles present in Resolve.
     * <p>
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link RolesVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/listRoles", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO listRoles(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("Roles");
            //            query.setWhereClause("(sysIsDeleted is null OR sysIsDeleted = false)");

            List<RolesVO> data = ServiceHibernate.getRoles(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_ROLES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ROLES);
        }

        return result;
    }

    /**
     * Get Role represented by <code>id</code>
     * 
     * @param id : String id of the role which needs to be retrieved.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link RolesVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getRole", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO getRole(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            RolesVO vo = ServiceHibernate.getRoleById(id, username);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_ROLES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ROLES);
        }

        return result;
    }

    /**
     * Save Role
     * <p>
     * @param jsonProperty : JSON String representing Role object to be saved.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link RolesVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/saveRole", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO saveRole(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RolesVO entity = new ObjectMapper().readValue(json, RolesVO.class);

        //convert json to VO - spring does not work as the VO are not 100% java bean
        //        JsonConfig jsonConfig = new JsonConfig();
        //        jsonConfig.setRootClass(RolesVO.class);
        //        jsonConfig.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
        //        RolesVO entity = (RolesVO) JSONSerializer.toJava(jsonProperty, jsonConfig);

        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            RolesVO vo = ServiceHibernate.saveRole(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_ROLES, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_ROLES);
        }

        return result;
    }

    /**
     * Delete Role
     * <p>
     * @param ids : String array of sys_ids of Role to be deleted.
     * @param query : {@link QueryDTO}. Not used at this time.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/deleteRole", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO deleteRole(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Collection<String> deletedRolesFromUserNames = Collections.emptyList();

        try
        {
            deletedRolesFromUserNames = ServiceHibernate.deleteRolesByIdsAndQuery(ids, query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_ROLES, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_ROLES);
        }
        
        if (result.isSuccess())
        {
            Log.syslog.info(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.collectionToString(deletedRolesFromUserNames, " , "), 
                            request.getLocalAddr(), username + " successfully removed role(s) and updated user(s) having removed role(s)"));
        }
        else
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.listToString(Arrays.asList(ids), " , "), 
                            request.getLocalAddr(), username + " failed to remove role(s) and update user(s) having removed role(s)"));
        }
        
        return result;
    }

    /**
     * List Resolve groups.
     * <p>
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link GroupsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/user/listGroups", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<GroupDTO> listGroups(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<GroupDTO> result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("Groups");
            List<GroupsVO> data = ServiceHibernate.getGroups(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            List<GroupDTO> dtos = new ArrayList<>();
            for (GroupsVO vo : data) {
            	GroupDTO dto = new GroupDTO();
            	dto.fromVO(vo);
            	dtos.add(dto);
            }

            result.setSuccess(true).setRecords(dtos);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_GROUPS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_GROUPS);
        }
        
        return result;
    }

    /**
     * Get detailed Group info sys_id specified by <code>id</code>
     * <p> 
     * @param id : String sys_id of a group whoes info is needed.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link GroupsVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getGroup", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO getGroup(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            GroupsVO vo = ServiceHibernate.getGroupById(id, username);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_GROUPS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_GROUPS);
        }

        return result;
    }

    /**
     * Save Group
     * <p>
     * @param jsonProperty : JSON String representing Group to be saved.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link GroupsVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/saveGroup", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO saveGroup(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        GroupsVO entity = new ObjectMapper().readValue(json, GroupsVO.class);
        //        entity.setSys_id("UNDEFINED");

        //convert json to VO - spring does not work as the VO are not 100% java bean
        //        JsonConfig jsonConfig = new JsonConfig();
        //        jsonConfig.setRootClass(GroupsVO.class);
        ////        jsonConfig.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
        //        GroupsVO entity = (GroupsVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        //
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            GroupsVO vo = ServiceHibernate.saveGroup(entity, username);
            result.setSuccess(true).setData(vo).setMessage("Group saved successfully");
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_GROUP, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_GROUP);
        }

        return result;
    }

    /**
     * Delete Group(s)
     * <p>
     * @param ids : String array containing sys_id of Groups to be deleted.
     * @param query : {@link QueryDTO}. Not used at this time.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/deleteGroup", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO deleteGroup(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Collection<String> deletedGroupsFromUserNames = Collections.emptyList();
        
        try
        {
            deletedGroupsFromUserNames = ServiceHibernate.deleteGroupsByIdsAndQuery(ids, query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_GROUPS, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_GROUPS);
        }
        
        if (result.isSuccess())
        {
            Log.syslog.info(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.collectionToString(deletedGroupsFromUserNames, " , "), 
                            request.getLocalAddr(), username + " successfully removed group(s) and updated user(s) who were members of removed group(s)"));
        }
        else
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.listToString(Arrays.asList(ids), " , "), 
                            request.getLocalAddr(), username + " failed to remove group(s) and update user(s) who are member of group(s) being removed"));
        }
        
        return result;
    }

    /**
     * List of Groups assigned to list of Users
     *
     * @param userSysIds : String array containing sis_id of Users to whom Groups are assigned.
     * @param groupSysIds : String array containing sys_id of Groups which are assigned to a User
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/assignGroups", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO assignGroups(@RequestParam String[] userSysIds, @RequestParam String[] groupSysIds, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Collection<String> assignedUserNames = Collections.emptyList();
        
        try
        {
            Set<String> setUserSysIds = new HashSet<String>(Arrays.asList(userSysIds));
            Set<String> setGroupSysIds = new HashSet<String>(Arrays.asList(groupSysIds));

            assignedUserNames = ServiceHibernate.addGroupsToUsers(setUserSysIds, setGroupSysIds, username);

            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ASSIGNING_GROUPS_TO_USERS, e);
            result.setSuccess(false).setMessage(ERROR_ASSIGNING_GROUPS_TO_USERS);
        }
        
        if (result.isSuccess())
        {
            Log.syslog.info(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.collectionToString(assignedUserNames, " , "), 
                            request.getLocalAddr(), username + " successfully assigned user(s) groups"));
        }
        else
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.listToString(Arrays.asList(userSysIds), " , "), 
                            request.getLocalAddr(), username + " failed to assign user(s) groups"));
        }
        
        return result;
    }

    /**
     * List of Roles assigned to list of Users
     *
     * @param userSysIds : String array containing sis_id of Users to whom Roles are assigned.
     * @param rolesSysIds : String array containing sys_id of Roles which are assigned to a User
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/assignRoles", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO assignRoles(@RequestParam String[] userSysIds, @RequestParam String[] rolesSysIds, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Collection<String> assignedUserNames = Collections.emptyList();

        try
        {
            Set<String> setUserSysIds = new HashSet<String>(Arrays.asList(userSysIds));
            Set<String> setRolesSysIds = new HashSet<String>(Arrays.asList(rolesSysIds));

            assignedUserNames = ServiceHibernate.addRolesToUsers(setUserSysIds, setRolesSysIds, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ASSIGNING_ROLES_TO_USERS, e);
            result.setSuccess(false).setMessage(ERROR_ASSIGNING_ROLES_TO_USERS);
        }
        
        if (result.isSuccess())
        {
            Log.syslog.info(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.collectionToString(assignedUserNames, " , "), 
                            request.getLocalAddr(), username + " successfully assigned user(s) roles"));
        }
        else
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.listToString(Arrays.asList(userSysIds), " , "), 
                            request.getLocalAddr(), username + " failed to assign user(s) roles"));
        }
        return result;
    }

    /**
     * Remove Roles assigned to the User
     * <p>
     * @param userSysIds : String array of user sysIds
     * @param roles : String array of role names that is to be removed from the users
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/removeRoles", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO removeRoles(@RequestParam String[] userSysIds, @RequestParam String[] roles, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Collection<String> assignedUserNames = Collections.emptyList();

        try
        {
            Set<String> setUserSysIds = new HashSet<String>(Arrays.asList(userSysIds));
            Set<String> setRoles = new HashSet<String>(Arrays.asList(roles));

            assignedUserNames = ServiceHibernate.removeUsersFromRoles(setUserSysIds, setRoles, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_REMOVING_ROLES_FROM_USERS, e);
            result.setSuccess(false).setMessage(ERROR_REMOVING_ROLES_FROM_USERS);
        }
        
        if (result.isSuccess())
        {
            Log.syslog.info(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.collectionToString(assignedUserNames, " , "), 
                            request.getLocalAddr(), username + " successfully removed user(s) roles"));
        }
        else
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.listToString(Arrays.asList(userSysIds), " , "), 
                            request.getLocalAddr(), username + " failed to remove user(s) roles"));
        }
        
        return result;
    }

    /**
     * Remove Groups assigned to the User
     * <p>
     * @param userSysIds : String array of user sysIds
     * @param groups : String array of group names that is to be removed from the users
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/removeGroups", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO removeGroups(@RequestParam String[] userSysIds, @RequestParam String[] groups, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Collection<String> assignedUserNames = Collections.emptyList();
        
        try
        {
            Set<String> setUserSysIds = new HashSet<String>(Arrays.asList(userSysIds));
            Set<String> setGroups = new HashSet<String>(Arrays.asList(groups));

            assignedUserNames = ServiceHibernate.removeUsersFromGroups(setUserSysIds, setGroups, username);

            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ASSIGNING_GROUPS_TO_USERS, e);
            result.setSuccess(false).setMessage(ERROR_ASSIGNING_GROUPS_TO_USERS);
        }
        
        if (result.isSuccess())
        {
            Log.syslog.info(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.collectionToString(assignedUserNames, " , "), 
                            request.getLocalAddr(), username + " successfully removed user(s) groups"));
        }
        else
        {
            Log.syslog.warn(String.format("[Audit] %s %s %s - %s", Authentication.getRemoteAddrForSysLog(request), StringUtils.listToString(Arrays.asList(userSysIds), " , "), 
                            request.getLocalAddr(), username + " failed to remove user(s) groups"));
        }
        
        return result;
    }
    
    /**
     * List Resolve orgs.
     * <p>
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link OrgsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/user/listOrgs", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO listOrgs(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("Orgs");
            //            query.setWhereClause("(sysIsDeleted is null OR sysIsDeleted = false)");

            List<OrgsVO> data = ServiceHibernate.getOrgs(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_ORGS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ORGS);
        }

        return result;
    }
    
    /**
     * Save Org
     * <p>
     * @param jsonProperty : JSON String representing Org to be saved.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link OrgsVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/user/saveOrg", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO saveOrg(@RequestBody OrgsVO entity, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            OrgsVO vo = ServiceHibernate.saveOrg(entity, username);
            result.setSuccess(true).setData(vo).setMessage("Org saved successfully");
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_ORG, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_ORG);
        }

        return result;
    }
    /**
     * Get detailed Org info for specified sys_id <code>id</code>
     * <p> 
     * @param id : String sys_id of a org whose info is needed.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link OrgsVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/user/getOrg", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO getOrg(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        //String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            OrgsVO vo = ServiceHibernate.getOrgById(id);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_ORG_BY_ID, id);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * Get list of valid parent orgs for specified child org name.
     * <p>
     * @param childOrgName : Name of child org to get valid parent orgs
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link OrgsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getValidParentOrgsByName", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<OrgsVO> getValidParentOrgsByName(@RequestParam String childOrgName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<OrgsVO> result = new ResponseDTO<OrgsVO>();
        //String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            Set<OrgsVO> validParentOrgVOs = ServiceHibernate.getValidParentOrgs(null, childOrgName);
            
            result.setSuccess(true).setRecords(new ArrayList<OrgsVO>(validParentOrgVOs));
            result.setTotal(validParentOrgVOs.size());
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_PARENT_ORGS_FOR_CHILD, childOrgName);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * Get list of valid parent orgs for specified child org id.
     * <p>
     * @param childOrgId : Id of child org to get valid parent orgs
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link OrgsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getValidParentOrgsById", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<OrgsVO> getValidParentOrgsById(@RequestParam String childOrgId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<OrgsVO> result = new ResponseDTO<OrgsVO>();
        //String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            Set<OrgsVO> validParentOrgVOs = ServiceHibernate.getValidParentOrgs(childOrgId, null);
            
            result.setSuccess(true).setRecords(new ArrayList<OrgsVO>(validParentOrgVOs));
            result.setTotal(validParentOrgVOs.size());
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_PARENT_ORGS_FOR_CHILD_BY_ID, childOrgId);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * Get list of all orgs in hierarchy (including root org) for specified root org name.
     * <p>
     * @param rootOrgName : Name of root org to get org hierarchy for
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link OrgsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getAllOrgsInHierarchyByName", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<OrgsVO> getAllOrgsInHierarchyByName(@RequestParam String rootOrgName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<OrgsVO> result = new ResponseDTO<OrgsVO>();
        //String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            Set<OrgsVO> orgsInHierarchyVOs = ServiceHibernate.getAllOrgsInHierarchy(null, rootOrgName);
            
            result.setSuccess(true).setRecords(new ArrayList<OrgsVO>(orgsInHierarchyVOs));
            result.setTotal(orgsInHierarchyVOs.size());
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_ALL_ORGS_IN_HIERARCHY, rootOrgName);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * Get list of all orgs in hierarchy (including root org) for specified root org id.
     * <p>
     * @param rootOrgId : Id of root org to get org hierarchy for
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link OrgsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getAllOrgsInHierarchyById", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<OrgsVO> getAllOrgsInHierarchyById(@RequestParam String rootOrgId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<OrgsVO> result = new ResponseDTO<OrgsVO>();
        //String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            Set<OrgsVO> orgsInHierarchyVOs = ServiceHibernate.getAllOrgsInHierarchy(rootOrgId, null);
            
            result.setSuccess(true).setRecords(new ArrayList<OrgsVO>(orgsInHierarchyVOs));
            result.setTotal(orgsInHierarchyVOs.size());
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_ALL_ORGS_IN_HIERARCHY_BY_ID, rootOrgId);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * Changes the password for the current user.
     * <p>
     * @param entity : json object with user name, old and new passwords
     * @return {@link ResponseDTO} with operation status and message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/user/changePassword", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO changeUserPassword(@RequestBody UserChangePasswordDTO entity, HttpServletRequest request)
			throws ServletException, IOException {

		ResponseDTO result = new ResponseDTO();

		String username = entity.getUsername();
		String currentUser = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		
		try {
			if (currentUser.equalsIgnoreCase(username)) {
				AuthenticateResult authResult = Authentication.authenticate(username, entity.getOldPassword());

				if (authResult.valid) {
					ServiceHibernate.updatePassword(username, entity.getNewPassword());
					result.setSuccess(true).setMessage(PAS_SWORD_CHANGE_SUCCESS);
				} else {
					result.setSuccess(false)
					.setMessage(authResult.errCode != null ? 
								authResult.errCode.getMessage() : PASSWORD_CHANGE_WRONG_CREDENTIALS);
				}
			} else {
				if (ServiceHibernate.isAdminUser(currentUser)) {
					ServiceHibernate.updatePassword(username, entity.getNewPassword());
					result.setSuccess(true).setMessage(PAS_SWORD_CHANGE_SUCCESS);
				} else {
					result.setSuccess(false).setMessage(PASSWORD_CHANGE_NO_PERMISSIONS);
				}
			}
		} catch (Exception e) {
			Log.log.error(ERROR_CHANGING_USER_PASSWORD, e);
			result.setSuccess(false).setMessage(ERROR_CHANGING_USER_PASSWORD);
		}

		return result;
	}
    
    /**
     * Get list of all parent orgs in hierarchy for specified child org name.
     * <p>
     * @param childOrgName : Name of child org to get parent org hierarchy for
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link OrgsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getParentOrgsInHierarchyByName", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<OrgsVO> getParentOrgsInHierarchyByName(@RequestParam String childOrgName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<OrgsVO> result = new ResponseDTO<OrgsVO>();
        //String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            List<OrgsVO> parentOrgsInHierarchyVOs = ServiceHibernate.getParentOrgsInHierarchy(null, childOrgName);
            
            result.setSuccess(true).setRecords(new ArrayList<OrgsVO>(parentOrgsInHierarchyVOs));
            result.setTotal(parentOrgsInHierarchyVOs.size());
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_PARENT_ORGS_FOR_CHILD_BY_NAME, childOrgName);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * Get list of all parent orgs in hierarchy for specified child org id.
     * <p>
     * @param rootOrgId : Id of child org to get parent org hierarchy for
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, {@link OrgsVO} list as data and total record couont as total if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/getParentOrgsInHierarchyById", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<OrgsVO> getParentOrgsInHierarchyById(@RequestParam String childOrgId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<OrgsVO> result = new ResponseDTO<OrgsVO>();
        //String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            List<OrgsVO> parentOrgsInHierarchyVOs = ServiceHibernate.getParentOrgsInHierarchy(childOrgId, null);
            
            result.setSuccess(true).setRecords(new ArrayList<OrgsVO>(parentOrgsInHierarchyVOs));
            result.setTotal(parentOrgsInHierarchyVOs.size());
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_PARENT_ORGS_FOR_CHILD_BY_ID, childOrgId);
        	Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * Get named (created by customer) users count.
     * <p> 
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and {@link Integer} as count if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/user/getNamedUserCount", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Integer> getNamedUserCount(HttpServletRequest request) throws ServletException, IOException {
    	ResponseDTO<Integer> result = new ResponseDTO<Integer>();

        try
        {
            int namedUserCount = UserUtils.getNamedUserCount();
            result.setData(Integer.valueOf(namedUserCount)).setSuccess(true);
        }
        catch (Throwable e)
        {
        	String msg = ERROR_GETTING_NAMED_USER_COUNT;
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
}
