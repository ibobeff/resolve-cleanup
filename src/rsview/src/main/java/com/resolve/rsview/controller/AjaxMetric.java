/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.MetricUtil;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.hibernate.vo.ResolveMCPLoginVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

@Controller
@Service
public class AjaxMetric extends GenericController
{
	private static final String ERROR_DELETING_METRIC_THRESHOLD = "Error deleting MetricThreshold";
	private static final String ERROR_MAKING_MCP_CALL = "Error making mcp api call. Action=%s";
	private static final String ERROR_RETRIEVING_METRIC_LATEST_VERSION = "Error retrieving the latest version number for MetricThreshold %s.%s";
	private static final String ERROR_RETRIEVING_METRIC_THRESHOLD = "Error retrieving MetricThreshold";
	private static final String ERROR_RETRIEVING_UI_METRIC_THRESHOLDS = "Error retrieving UI editable MetricThresholds";
	private static final String ERROR_SAVING_METRIC_DEFINITION = "Error saving the metric definition";
	private static final String FAILED_TO_DEPLOY_THRESHOLD = "Failed to deploy threshold %s to %s";
	private static final String FAILED_TO_UNDEPLOY_THRESHOLD = "Failed to undeploy threshold %s from %s";
    private static final String ERROR_RETRIEVING_CLUSTER_LIST_FROM_GROUP = "Error retrieving cluster list from group %s";

	/**
     * Returns list of MetricThreshold based on the query from the UI grid
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            query.setModelName("MetricThreshold");
            
            Map<String, Object> resultMap = ServiceHibernate.getUIEditableMetricThresholds(query);
            List<MetricThresholdVO> data = (List<MetricThresholdVO>)resultMap.get("UNIQUE_THRESHOLDS");
            
            int total = data==null ? 0 : (Integer)(resultMap.get("UNIQUE_THRESHOLDS_TOTAL"));
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_UI_METRIC_THRESHOLDS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_UI_METRIC_THRESHOLDS);
        }
        
        return result;
    } // getAllDefinitions
    
    /**
     * Get a MetricThreshold record based on its name, module, and version
     * 
     * @param module
     * @param name
     * @param version
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/getbynameversion", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String module, @RequestParam String name, @RequestParam Integer version, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            if (module==null)
            {
                throw new Exception("Metric rule module can't be null");
            }
            else if (name == null)
            {
                throw new Exception("Metric rule name can't be null");
            }
            else if (version == null)
            {
                throw new Exception("Metric rule version can't be null");
            }

            Integer ver = version; //Integer.parseInt(version);
            MetricThresholdVO vo = ServiceHibernate.findMetricThreshold(module, name, ver, false);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_METRIC_THRESHOLD, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_METRIC_THRESHOLD);
        }

        return result;
    } //getByNameVersion
    
    /**
     * Get a MetricThreshold record based on sysId. It also gets the max version number available
     * with this revision.
     * 
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<MetricThresholdVO> get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<MetricThresholdVO> result = new ResponseDTO<MetricThresholdVO>();

        try
        {
            Map<String, Object> map = ServiceHibernate.findMetricThresholdByIdAndHighestEditableVersion(id);
            if (map.get("ERROR") == null)
            {
            	result.setData((MetricThresholdVO)map.get("METRIC_THRESHOLD"));
            	result.setTotal((Integer)map.get("VERSION"));
            	result.setSuccess(true);
            }
            else
            {
            	result.setMessage((String)map.get("ERROR"));
                result.setSuccess(false);
            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_METRIC_THRESHOLD, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_METRIC_THRESHOLD);
        }

        return result;
    } //getDefinition
    
    /**
     * Get the Revision of the rule specified by the rule module and name. Also get the count of versions associalted with the rule.
     * 
     * @param module : String representing the module of the rule.
     * @param name : String representing the name of the rule.
     * @param request
     * @return
     * 		Map with two key/value pair. METRIC_THRESHOLD key holds MetricThresholdVO object and VERSION holds the max version number.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/getRevision", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<MetricThresholdVO> getRevisionWithVersionCount(@RequestParam String module, @RequestParam String name, HttpServletRequest request) throws ServletException, IOException
    {
    	ResponseDTO<MetricThresholdVO> result = new ResponseDTO<MetricThresholdVO>();

        try
        {
            Map<String, Object> map = ServiceHibernate.getRevisionWithVersionCount(module, name);
            if (map.get("ERROR") == null)
            {
            	result.setData((MetricThresholdVO)map.get("METRIC_THRESHOLD"));
            	result.setTotal((Integer)map.get("VERSION"));
            	result.setSuccess(true);
            }
            else
            {
            	result.setMessage((String)map.get("ERROR"));
                result.setSuccess(false);
            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_METRIC_THRESHOLD, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_METRIC_THRESHOLD);
        }

        return result;
    }
    
    /**
     * Deletes list of MetricThreshold records based on sysIds
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteMetricThresholdByIdsFromUI(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_METRIC_THRESHOLD, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_METRIC_THRESHOLD);
        }

        return result;
    }
    
    /**
     * save the MetricThreshold coming from the UI form
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        MetricThresholdVO entity = new ObjectMapper().readValue(json, MetricThresholdVO.class);
        
      //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(MetricThresholdVO.class);
        MetricThresholdVO entity = (MetricThresholdVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            MetricThresholdVO vo = ServiceHibernate.saveMetricThreshold(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_METRIC_DEFINITION, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_METRIC_DEFINITION);
        }
        return result;
    } // saveDefinition

    /**
     * save a new version the MetricThreshold coming from the UI form
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/saveversion", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO saveVersion(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        MetricThresholdVO entity = new ObjectMapper().readValue(json, MetricThresholdVO.class);
        
      //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(MetricThresholdVO.class);
        MetricThresholdVO entity = (MetricThresholdVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            Log.log.debug("Saveversion on threshold " + entity.getURuleModule() + "." + entity.getURuleName() + " on version " + entity.getUVersion());
            MetricThresholdVO vo = ServiceHibernate.saveVersionMetricThreshold(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_METRIC_DEFINITION, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_METRIC_DEFINITION);
        }
        return result;
    } // saveDefinition

    
    /**
     * copy the MetricThreshold coming from the UI form
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/copy", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO copy(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        MetricThresholdVO entity = new ObjectMapper().readValue(json, MetricThresholdVO.class);
        
      //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(MetricThresholdVO.class);
        MetricThresholdVO entity = (MetricThresholdVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            MetricThresholdVO vo = ServiceHibernate.copyMetricThreshold(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_METRIC_DEFINITION, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_METRIC_DEFINITION);
        }
        return result;
    } // copy 

    
    /**
     * Get the latest version number of a MetricThreshold record based on its full name
     * 
     * @param moduleName
     * @param ruleName
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/gethighestversionnumber", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getHighestRuleVersion(@RequestParam String moduleName, @RequestParam String ruleName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            int version = ServiceHibernate.findHighestVersionNumber(moduleName, ruleName, false);
            result.setData(new Integer(version));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String errMsg = String.format(ERROR_RETRIEVING_METRIC_LATEST_VERSION, moduleName, ruleName); 
            Log.log.error(errMsg, e);
            result.setSuccess(false).setMessage(errMsg);
        }

        return result;
    } //getHighestRuleVersion
    
    /**
     *  MCP  api call  entry point
     * 
     * @param data
     * @param action
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/mcpapicall", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO mcpAPICall(@RequestParam String data, @RequestParam String action, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        Object dataObj = null;
        
        Log.log.debug("MCP api call: action = " + action);
        
        if (StringUtils.isNotBlank(action))
        {
            action = ESAPI.encoder().decodeForHTML(action);
            Log.log.debug("MCP api call: action (HTML decoded) = " + action);
        }
        
        Log.log.debug("MCP api call: data = " + data);
        
        if (StringUtils.isNotBlank(data))
        {
            data = ESAPI.encoder().decodeForHTML(data);
            Log.log.debug("MCP api call: data (HTML Decoded) = " + data);
        }
        
        try
        {
            if (StringUtils.isNotEmpty(action))
            {
                if ("login".equalsIgnoreCase(action))
                {
                    result.setSuccess(true);
                }
                else if (Constants.MCP_AUTO_DEPLOY.equalsIgnoreCase(action))
                {
                    if (data==null)
                    {
                        throw new RuntimeException("mcpapicall error: data is null. action=" + action);
                    }
                    else
                    {
                        MetricUtil.mcpAutoDeployMetricThresholds(data, action);
                    }
                    result.setSuccess(true);
                }
                else if (Constants.MCP_UNDEPLOY.equalsIgnoreCase(action) || Constants.MCP_UNDEPLOY_ALL.equalsIgnoreCase(action))
                {
                    if (data==null)
                    {
                        throw new RuntimeException("mcpapicall error: data is null. action=" + action);
                    }
                    else
                    {
                        MetricUtil.mcpManualDeployMetricThresholds(data, action);
                    }
                    result.setSuccess(true);
                } 
                else if (Constants.MCP_MANUAL_DEPLOY.equalsIgnoreCase(action))
                {
                    if (data==null)
                    {
                        throw new RuntimeException("mcpapicall error: data is null. action=" + action);
                    }
                    else
                    {
                        MetricUtil.mcpManualDeployMetricThresholds(data, action);
                    }
                    result.setSuccess(true);
                }
                else
                {
                    String err = "Unknown mcp API call action: " + action;
                    Log.log.error(err);
                    result.setSuccess(false).setMessage(err);
                }
            }
        }
        catch (Exception e)
        {
            String errMsg = String.format(ERROR_MAKING_MCP_CALL, action); 
            Log.log.error(errMsg, e);
            result.setSuccess(false).setMessage(errMsg);
        }
        
        return result;
    } //mcpAPICall
    

    /**
     *  MCP  deployment. The call will deploy a  rule to the specified clusters, hosts, and Resolve components.
     * 
     * @param deployData     JSON string value, which is equvalent to Java Map<String,Map<String, List<String>>>. The outside map is first keyed by cluster name. Inside map is keyed by host name, with the entry value of component list, which the rule will be deployed on.   
     * @param sysId          threshold rule's sysid  
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/mcpdeploy", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO mcpDeploy(@RequestParam String deployData, @RequestParam String sysId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try {
            ServiceHibernate.mcpDeployMetricThreshold(sysId, deployData);
            result.setSuccess(true);
        }
        catch(Exception ex)
        {
            Log.log.error(String.format(FAILED_TO_DEPLOY_THRESHOLD, sysId, deployData));
            result.setSuccess(false);
        }

        return result;
    }

    /**
     *  MCP  undeployment. The call will undeploy a  rule from the specified clusters, hosts, and Resolve components.
     * 
     * @param deployData     JSON string value, which is equvalent to Java Map<String,Map<String, List<String>>>. The outside map is first keyed by cluster name. Inside map is keyed by host name, with the entry value of component list, which the rule will be deployed on.   
     * @param sysId          threshold rule's sysid  
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/mcpundeploy", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO mcpUndeploy(@RequestParam String deployData, @RequestParam String sysId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try {
            ServiceHibernate.mcpUndeployMetricThreshold(sysId, deployData);
            result.setSuccess(true);
        }
        catch(Exception ex)
        {
            Log.log.error(String.format(FAILED_TO_UNDEPLOY_THRESHOLD, sysId, deployData));
            result.setSuccess(false);
        }

        return result;
    }

    
    /**
     *  MCP  undeployment. The call will undeploy a  rule from all registered clusters.
     * 
     * @param deployData     JSON string value, which is equvalent to Java Map<String,Map<String, List<String>>>. The outside map is first keyed by cluster name. Inside map is keyed by host name, with the entry value of component list, which the rule will be deployed on.   
     * @param sysId          threshold rule's sysid  
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/metric/mcpundeployall", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO mcpUndeployAll(@RequestParam String deployData, @RequestParam String sysId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try {
            ServiceHibernate.mcpAllUndeployMetricThreshold(sysId, deployData);
            result.setSuccess(true);
        }
        catch(Exception ex)
        {
            Log.log.error(String.format(FAILED_TO_UNDEPLOY_THRESHOLD, sysId, deployData));
            result.setSuccess(false);
        }

        return result;
    }
    
    /**
     * Get a MCP Login credential record based on its name, group
     * 
     * @param group
     * @param cluster
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcplogin/findbyname", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO findMCPLogin(@RequestParam String group, @RequestParam String cluster, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            if (StringUtils.isEmpty(group))
            {
                group = Constants.MCP_DEFAULT_GROUP;
            }
            
            if (StringUtils.isEmpty(cluster))
            {
                throw new Exception("Cluster name can't be null or empty");
            }
            
            ResolveMCPLoginVO vo = ServiceHibernate.findMCPLogin(group, cluster);
            result.setData(vo);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_METRIC_THRESHOLD, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_METRIC_THRESHOLD);
        }

        return result;
    } //findMCPLogin
    
    /**
     * save MCP Login data coming from  UI 
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcplogin/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO saveMCPLogin(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(ResolveMCPLoginVO.class);
        ResolveMCPLoginVO entity = (ResolveMCPLoginVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveMCPLoginVO vo = ServiceHibernate.saveMCPLogin(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_METRIC_DEFINITION, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_METRIC_DEFINITION);
        }
        return result;
    } // saveDefinition
    
    
    /**
     * Get a list of registerd cluster names for a group
     * 
     * @param group
     * @param request
     * @param response
     * @return jason object contains a list of cluster names 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcplogin/getclusterlist", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getRegisteredClusters(@RequestParam String group, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            if (StringUtils.isEmpty(group))
            {
                group = Constants.MCP_DEFAULT_GROUP;
            }
            
            List<String> l = ServiceHibernate.getRegisteredClusters(group);
            result.setData(l);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String msg = String.format(ERROR_RETRIEVING_CLUSTER_LIST_FROM_GROUP, group);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    } //getRegisteredClusters
    
} // AjaxMetric
