/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.Map;

import com.resolve.notification.NotificationAPI;
import com.resolve.services.hibernate.menu.MenuCache;
import com.resolve.util.Log;

public class MNotification
{
    /**
     * This method simply initializes the targets in the NotificationAPI. In general called
     * by the RSControl once it processes the blueprint file it received from the RSMGMT. 
     */
    public static void init(Map<String, Object> params)
    {
        Log.log.debug("Message received to initialize targets in NotificationAPI.");
        NotificationAPI.init();
    }
    
    /**
     * Invalidate menu cache
     */
    public static void invalidateMenuCache(Map<String, Object> params)
    {
        MenuCache.getInstance().invalidate();
    }
}
