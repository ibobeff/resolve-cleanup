/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

@Controller
public class ResultController extends GenericController
{
    @RequestMapping(value= ConstantValues.URL_RESULT_TASK_DETAIL, method=RequestMethod.GET)
    public ModelAndView resultTaskDetailCAS(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String,String> params = ControllerUtil.getParameterMap(request);
//        String executeid = params.get(Constants.HTTP_REQUEST_EXECUTEID);
        String taskresultid = params.get(Constants.HTTP_REQUEST_TASKRESULTID);
//        String address = params.get(Constants.HTTP_REQUEST_ADDRESS);

        ModelAndView result = null;
        String content = ServiceWorksheet.getContentForTaskDetail(taskresultid);
                        
        if (StringUtils.isNotBlank(content))
        {
            result = new ModelAndView(ConstantValues.RESULT_VIEW_JSP, ConstantValues.MODEL_CONTENT_KEY, content);
        }
        else
        {
            result = resultArchiveTaskDetail(request, response);
        }
 
        return result;
    }

    @RequestMapping(value= ConstantValues.URL_RESULT_TASK_RAW, method=RequestMethod.GET)
    public ModelAndView resultTaskRawCAS(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String,String> params = ControllerUtil.getParameterMap(request);
//        String executeid = params.get(Constants.HTTP_REQUEST_EXECUTEID);
        String executeresultid = params.get(Constants.HTTP_REQUEST_EXECUTERESULTID);
//        String address = params.get(Constants.HTTP_REQUEST_ADDRESS);
        
        ModelAndView result = null;
        String content = ServiceWorksheet.getRawContentForTaskDetail(executeresultid);
        if (StringUtils.isNotBlank(content))
        {
            result = new ModelAndView(ConstantValues.RESULT_VIEW_JSP, ConstantValues.MODEL_CONTENT_KEY, content);
        }
        else
        {
            result = resultArchiveTaskRaw(request, response);
        }
        
        return result;
    } 
    
    @RequestMapping(value= ConstantValues.URL_RESULT_ARCHIVE_TASK_DETAIL, method=RequestMethod.GET)
    public ModelAndView resultArchiveTaskDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        Map<String,String> params = ControllerUtil.getParameterMap(request);
        String executeid = params.get(Constants.HTTP_REQUEST_EXECUTEID);
        String taskresultid = params.get(Constants.HTTP_REQUEST_TASKRESULTID);
        
        String content = ServiceHibernate.getArchiveContentForTaskDetail(executeid, taskresultid); 
        if (StringUtils.isEmpty(content))
        {
            content = "Missing EXECUTEID or TASKRESULTID parameter";
        }
        
		ModelAndView result = new ModelAndView(ConstantValues.RESULT_VIEW_JSP, ConstantValues.MODEL_CONTENT_KEY, content);
        return result;
    } // resultArchiveTaskDetail

    @RequestMapping(value= ConstantValues.URL_RESULT_ARCHIVE_TASK_RAW, method=RequestMethod.GET)
    public ModelAndView resultArchiveTaskRaw(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String,String> params = ControllerUtil.getParameterMap(request);
        String executeid = params.get(Constants.HTTP_REQUEST_EXECUTEID);
        String executeresultid = params.get(Constants.HTTP_REQUEST_EXECUTERESULTID);
        String content = ServiceHibernate.getRawArchiveContentForTaskDetail(executeid, executeresultid); 
        
        if (StringUtils.isEmpty(content))
        {
            content = "Missing EXECUTEID parameter";
        }
        
		ModelAndView result = new ModelAndView(ConstantValues.RESULT_VIEW_JSP, ConstantValues.MODEL_CONTENT_KEY, content);
        return result;
    } // resultArchiveTaskRaw

} // ResultController
