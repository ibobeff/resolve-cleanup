/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.search.model.Tag;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikidocResolutionRatingVO;
import com.resolve.services.hibernate.wiki.WikiStatusEnum;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@Controller
public class AjaxWikiNamespaceAdmin extends GenericController
{
    /**
     * List WikiDoc Namespaces with the count of WikiDocs present in each one of them.
     * 
     * @param query : {@link QueryDTO} - provides start, limit, sorting, and filtering (See QueryDTO for more information)
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listNamespaces(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //get the VOs
            result = ServiceWiki.getDocumentNamespaces(query, username);

            //prepare the response
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving namespace list.", e);
            result.setSuccess(false).setMessage("Error retrieving namespace list.");
        }

        return result;
    }

    /**
     * Move/Rename/Copy WikiDocs under selected namespace(s) to a different namespace.
     * 
     * @param action : String specifying an action. It could either be Copy or Rename.
     * @param namespaces : String array of namespaces from where the WikiDoc(s) to be copied or renamed.
     * @param namespace : String specifying a napespace where WikiDoc(s) will be copied or renamed.
     * @param choice : String specifying a choice. It could either be skip or overwrite.
     * <br>
     * Skip: If selected, the WikiDoc will be skipped if the destination namespace already has a same named WikiDoc.
     * <br>
     * Overwrite : If selected, the WikiDoc will be overwritten if the destination namespace already has a same named WikiDoc.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    
    @RequestMapping(value = "/nsadmin/moveRenameCopy", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO moveRenameCopy(
                    @RequestParam("action") String action,//rename or copy
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("namespace") String namespace,
                    @RequestParam("choice") String choice,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if (namespaces != null && namespaces.length > 0 && StringUtils.isNotEmpty(action) && StringUtils.isNotEmpty(namespace))
            {
                if (action.equalsIgnoreCase("copy"))
                {
                    if (choice.equalsIgnoreCase("overwrite"))
                    {
                        ServiceWiki.copyDocumentsForNamepaces(Arrays.asList(namespaces), namespace, true, username);
                    }
                    else if (choice.equalsIgnoreCase("skip"))
                    {
                        ServiceWiki.copyDocumentsForNamepaces(Arrays.asList(namespaces), namespace, false, username);
                    }
                    else if (choice.equalsIgnoreCase("update"))
                    {
                        ServiceWiki.replaceDocumentsForNamepaces(Arrays.asList(namespaces), namespace, username);
                    }
                }
                else
                {
                    boolean overwrite = choice.equalsIgnoreCase("true") ? true : false;
                    ServiceWiki.moveRenameDocumentsForNamepaces(Arrays.asList(namespaces), namespace, overwrite, username);
                }

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error renaming/copying wiki(s).", e);
            result.setSuccess(false).setMessage("Error renaming/copying wiki(s).");
        }

        return result;
    }

    /**
     * Delete/Undelete WikiDoc(s) under selected namespace(s)
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) to be marked Deleted or Undelete
     * @param on : Boolean if true, WikiDoc(s) of the selected namespace(s) will be marked deleted. Undelete otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setDeleted", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setDeleted(@RequestParam("namespaces") String[] namespaces, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> namespaceNames = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateDeleteForNamespace(namespaceNames, on, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the namespaces deleted flag", e);
            result.setSuccess(false).setMessage("Error updating the namespaces deleted flag");
        }

        return result;
    }
    
    /**
     * Purge (delete permanently) WikiDoc(s) from selected Namespace(s)
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) to be purged. 
     * @param all : Boolean if true, all Namespaces (including all Wikis) will be purged.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/purge", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO purgeWikis(@RequestParam("namespaces") String[] namespaces, 
                    @RequestParam("all") Boolean all, //if this true, than it will purge all wikis
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> namespaceNames = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.deleteWikiForNamespaces(namespaceNames, username);
            }
            else
            {
                if(all)
                {
                    ServiceWiki.deleteAllWikiDocuments(username);
                }
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error purging namespace(s).", e);
            result.setSuccess(false).setMessage("Error purging namespace(s).");
        }

        return result;
    }

    /**
     * Activate/deactivate the WikiDoc(s) of specified Namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) to be Activate/Deactivated.
     * @param on : Boolean if true, all WikiDoc(s) present under selected Namespace(s) will be activated. Deactivated otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setActivated", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setActivated(@RequestParam("namespaces") String[] namespaces, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> namespacesSet = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateWikiStatusFlagForNamespaces(namespacesSet, WikiStatusEnum.ACTIVE, on, false, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the active flag for namespace(s).", e);
            result.setSuccess(false).setMessage("Error updating the active flag for namespace(s).");
        }

        return result;
    }
    
    /**
     * Lock/Unlock the WikiDoc(s) of specified Namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) to be Locked / Unlocked.
     * @param on : Boolean if true, all WikiDoc(s) present under selected Namespace(s) will be locked. Unlocked otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setLocked", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setLocked(@RequestParam("namespaces") String[] namespaces, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> namespacesSet = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateWikiStatusFlagForNamespaces(namespacesSet, WikiStatusEnum.LOCKED, on, false, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the lock flag for namespace(s).", e);
            result.setSuccess(false).setMessage("Error updating the lock flag for namespace(s).");
        }


        return result;
    }

    /**
     * Hide/Unhide the WikiDoc(s) of specified Namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) to be Hide / Unhide.
     * @param on : Boolean if true, all WikiDoc(s) present under selected Namespace(s) will be made hidden. Unhidden otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setHidden", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setHidden(@RequestParam("namespaces") String[] namespaces, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> namespacesSet = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateWikiStatusFlagForNamespaces(namespacesSet, WikiStatusEnum.HIDDEN, on, false, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the hidden flag for namespace(s).", e);
            result.setSuccess(false).setMessage("Error updating the hidden flag for namespace(s).");
        }


        return result;
    }

    /**
     * Rate WikiDoc(s) of specified Namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) to be rated.
     * @param u1StarCount : Integer count of 1 start rating.
     * @param u2StarCount : Integer count of 2 start rating.
     * @param u3StarCount : Integer count of 3 start rating.
     * @param u4StarCount : Integer count of 4 start rating.
     * @param u5StarCount : Integer count of 5 start rating.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/rate", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO rate(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("u1StarCount") Integer u1StarCount,
                    @RequestParam("u2StarCount") Integer u2StarCount,
                    @RequestParam("u3StarCount") Integer u3StarCount,
                    @RequestParam("u4StarCount") Integer u4StarCount,
                    @RequestParam("u5StarCount") Integer u5StarCount,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                WikidocResolutionRatingVO rating = new WikidocResolutionRatingVO();
                rating.setU1StarCount(new Long(u1StarCount));
                rating.setU2StarCount(new Long(u2StarCount));
                rating.setU3StarCount(new Long(u3StarCount));
                rating.setU4StarCount(new Long(u4StarCount));
                rating.setU5StarCount(new Long(u5StarCount));

                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateResolutionRatingCountForNamespaces(ns, rating, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the rating for namespace(s).", e);
            result.setSuccess(false).setMessage("Error updating the rating for namespace(s).");
        }

        return result;
    }

    /**
     * Change search weight to WikiDoc(s) of a specified Namespace(s)
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) whoes search weight needs to be changed.
     * @param weight : Integer search weight.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/addSearchWeight", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO addSearchWeight(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("weight") Integer weight,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateWeightForNamespaces(ns, weight.doubleValue(), username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the adding weights to namespace(s).", e);
            result.setSuccess(false).setMessage("Error updating the adding weights to namespace(s).");
        }

        return result;
    }

    /**
     * Set WikiDoc(s) as reviewed
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) to be set as reviewed.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setReviewed", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setReviewed(
                    @RequestParam("namespaces") String[] namespaces,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateLastReviewedDateForNamespace(ns, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the review flag for namespace(s).", e);
            result.setSuccess(false).setMessage("Error updating the review flag for namespace(s).");
        }

        return result;
    }

    /**
     * Set expiry date of WikiDoc(s)
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) expiry date to be set.
     * @param strDate String date of the format "2015-01-10" for Jan, 10 2015
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setExpiryDate", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setExpiryDate(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("date") String strDate,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date =  df.parse(strDate);

                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.updateExpirationDateForNamespace(ns, date, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error setting the expiry on namespace(s)", e);
            result.setSuccess(false).setMessage("Error setting the expiry on namespace(s)");
        }

        return result;
    }

    /**
     * Set access rights of all the WikiDoc(s) present under specified namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) access rights to be set.
     * @param uexecuteAccess : String of comma separated list of roles for execute access.
     * @param uadminAccess : String of comma separated list of roles for admin access.
     * @param ureadAccess : String of comma separated list of roles for read access.
     * @param uwriteAccess : String of comma separated list of roles for write access.
     * @param defaultRights : Boolean if true, sets the default system access rights to all the WikiDoc(s) present under specified namespace(s). 
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setAccessRights", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setAccessRights(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("uexecuteAccess") String uexecuteAccess,
                    @RequestParam("uadminAccess") String uadminAccess,
                    @RequestParam("ureadAccess") String ureadAccess,
                    @RequestParam("uwriteAccess") String uwriteAccess,
                    @RequestParam("defaultRights") boolean defaultRights,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                AccessRightsVO accessRights = new AccessRightsVO();
                accessRights.setUAdminAccess(uadminAccess);
                accessRights.setUReadAccess(ureadAccess);
                accessRights.setUWriteAccess(uwriteAccess);
                accessRights.setUExecuteAccess(uexecuteAccess);

                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.setRolesForNamespaces(ns, accessRights, defaultRights, username);

                result.setSuccess(true);
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error adding access rights to namespace(s).", e);
            result.setSuccess(false).setMessage("Error adding access rights to namespace(s).");
        }

        return result;
    }

    /**
     * Add specified tagss to all the WikiDoc(s) present under the given namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) needs to be tagged.
     * @param tagIds : String array of existing tag Id's to be assigned.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/addTags", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO addTags(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("tagIds") String[] tagIds,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0 && tagIds != null && tagIds.length > 0)
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                Set<String> tagSysIds = new HashSet<String>(Arrays.asList(tagIds));

                ServiceWiki.addTagsToWikiNamespaces(ns, tagSysIds, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error adding tag(s) to the namespace(s).", e);
            result.setSuccess(false).setMessage("Error adding tag(s) to the namespace(s).");
        }

        return result;
    }

    /**
     * Remove specified tagss from all the WikiDoc(s) present under the given namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) tag(s) to be removed.
     * @param tagIds : String array of existing tag Id's to be removed.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/removeTags", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO removeTags(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("tagIds") String[] tagIds,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0 && tagIds != null && tagIds.length > 0)
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                Set<String> tagSysIds = new HashSet<String>(Arrays.asList(tagIds));

                ServiceWiki.removeTagsFromWikiNamespaces(ns, tagSysIds, username);

                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error removing tasg(s) from namespace(s).", e);
            result.setSuccess(false).setMessage("Error removing tasg(s) from namespace(s).");
        }

        return result;
    }

    /**
     * Index WikiDoc(s) from specified namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) needs to be indexed.
     * @param all : Boolean if true, all WikiDocs will be indexed.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/index", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO indexWikis(
                    @RequestParam("namespaces") String[] namespaces, 
                    @RequestParam("all") Boolean all, //if this true, than it will index all
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0 )
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.indexWikiNamespaces(ns, true, username);
            }
            else
            {
                if(all)
                {
                    ServiceWiki.indexAllWikiDocuments(true, username);
                }
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error indexing wiki(s) from given namespace(s).", e);
            result.setSuccess(false).setMessage("Error indexing wiki(s) from given namespace(s).");
        }

        return result;
    }
    
    /**
     * Purge Indices of WikiDoc(s) from specified namespace(s).
     * 
     * @param namespaces : String array of namespace(s) of which WikiDoc(s) whoes indeces needs to be purged.
     * @param all : Boolean if true, all indices will be purged.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/purgeIndex", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO purgeIndex(
                    @RequestParam("namespaces") String[] namespaces, 
                    @RequestParam("all") Boolean all, //if this true, than it will purge index all
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0 )
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.purgeWikiNamespaces(ns, username);
            }
            else
            {
                if(all)
                {
                    ServiceWiki.purgeAllWikiDocumentIndexes(username);
                }
            }
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error("Error purging wiki(s) indices from given namespace(s).", e);
            result.setSuccess(false).setMessage("Error purging wiki(s) indices from given namespace(s).");
        }

        return result;
    }
    
    /**
     * Set Default access rights to all the WikiDoc(s) present under specified namespace(s)
     * 
     * @param namespaces : String array of namespace(s) whoes WikiDoc(s) needs to be set to default access rights.
     * @param uexecuteAccess : String of comma separated list of roles to give default execute access.
     * @param uadminAccess : String of comma separated list of roles to give default admin access.
     * @param ureadAccess : String of comma separated list of roles to give default read access.
     * @param uwriteAccess : String of comma separated list of roles to give default write access.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/setDefaultAccessRights", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setDefaultAccessRights(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("uexecuteAccess") String uexecuteAccess,
                    @RequestParam("uadminAccess") String uadminAccess,
                    @RequestParam("ureadAccess") String ureadAccess,
                    @RequestParam("uwriteAccess") String uwriteAccess,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
            	if (!uadminAccess.contains("admin"))
            	{
            		if (StringUtils.isBlank(uadminAccess))
            		{
            			uadminAccess = "admin";
            		}
            		else
            		{
	            		uadminAccess = "admin," + uadminAccess;
            		}
            	}
            	if (!uexecuteAccess.contains("admin"))
            	{
            		if (StringUtils.isBlank(uexecuteAccess))
            		{
            			uexecuteAccess = "admin";
            		}
            		else
            		{
	            		uexecuteAccess = "admin," + uexecuteAccess;
            		}
            	}
            	if (!uwriteAccess.contains("admin"))
            	{
            		if (StringUtils.isBlank(uwriteAccess))
            		{
            			uwriteAccess = "admin";
            		}
            		else
            		{
	            		uwriteAccess = "admin," + uwriteAccess;
            		}
            	}
            	if (!ureadAccess.contains("admin"))
            	{
            		if (StringUtils.isBlank(ureadAccess))
            		{
            			ureadAccess = "admin";
            		}
            		else
            		{
	            		ureadAccess = "admin," + ureadAccess;
            		}
            	}
                AccessRightsVO accessRights = new AccessRightsVO();
                accessRights.setUAdminAccess(uadminAccess);
                accessRights.setUReadAccess(ureadAccess);
                accessRights.setUWriteAccess(uwriteAccess);
                accessRights.setUExecuteAccess(uexecuteAccess);

                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.setNamespaceDefaultRoles(ns, accessRights, username);

                result.setSuccess(true);
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error setting default access rights to namespace(s).", e);
            result.setSuccess(false).setMessage("Error setting default access rights to namespace(s).");
        }

        return result;
    }
    
    /**
     * Commit all the WikiDoc(s) present under specified namespace(s) with the given comment.
     * 
     * @param namespaces : String array of namespace(s) whoes WikiDoc(s) needs to be commited with given comment.
     * @param comment : String comment.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/commit", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO commitRevision(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("comment") String comment,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.archiveNamespaces(ns, comment, username);

                result.setSuccess(true);
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error during commit of namespace(s).", e);
            result.setSuccess(false).setMessage("Error during commit of namespace(s).");
        }

        return result;
    }

    /**
     * Reset statistics of all the WikiDoc(s) present under specified namespace(s)
     * <br>
     * This includes setting ViewCount, EditCount, ExecuteCount, ReviewCount, UsefulNoCount and UsefulYesCount to zero.
     * 
     * @param namespaces : String array of namespace(s) whoes WikiDoc(s) statistics needs to be reseted.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/resetStats", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO resetStatistics(
                    @RequestParam("namespaces") String[] namespaces,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(namespaces));
                ServiceWiki.resetNamespaces(docSysIds, username);

                result.setSuccess(true);
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error during reset of stats for namespace(s).", e);
            result.setSuccess(false).setMessage("Error during reset of stats for namespace(s).");
        }

        return result;
    }

    /**
     * Get list of access rights for all the WikiDoc(s) of specified namespace(s).
     * 
     * @param namespaces : String array of namespace(s) whoes WikiDoc(s) access rights to be read.
     * @param defaultRights : Boolean if true, get the default access rights of all the WikiDoc(s) present under specified namespace(s)
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/nsadmin/accessRights", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<AccessRightsVO> accessRights(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("defaultRights") Boolean defaultRights,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<AccessRightsVO> result = new ResponseDTO<AccessRightsVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(namespaces != null && namespaces.length > 0)
            {
                
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                List<AccessRightsVO> rights = null;
                if(defaultRights)
                {
                    rights = ServiceWiki.getDefaultAccessRightsForNamespaces(ns, username);
                }
                else
                {
                    rights = ServiceWiki.getAccessRightsForNamespaces(ns, username);
                }
                
                //merge the access rights
                AccessRightsVO merged = AjaxWikiAdmin.mergeAccessRights(rights);
                
                result.setData(merged);
                result.setSuccess(true);
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error during getting access rights for namespace(s).", e);
            result.setSuccess(false).setMessage("Error during getting access rights for namespace(s).");
        }

        return result;
    }

    /**
     * 
     * @deprecated
     * Get the list of Tags associated with all the WikiDoc(s) present under specified Namespace(s).
     * 
     * @param namespaces : String array of namespace(s).
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Deprecated
    @RequestMapping(value = "/nsadmin/docTags", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Tag> getDocumentTags(
                    @RequestParam("namespaces") String[] namespaces,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Tag> result = new ResponseDTO<Tag>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
       
        try
        {   
            if(namespaces != null && namespaces.length > 0)
            {
                Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
                List<ResolveTagVO> tags = ServiceWiki.getTagsForNamespaces(ns, username);
                
                List<Tag> mergedTags = new ArrayList<Tag>();
                for(ResolveTagVO vo : tags)
                {
                    mergedTags.add(com.resolve.rsview.util.TagUtil.convertResolveTagToTag(vo));
                }
                
                result.setRecords(mergedTags);
                result.setSuccess(true);
            }
        
        }
        catch (Exception e)
        {
            Log.log.error("Error while getting tags for namespace(s).", e);
            result.setSuccess(false).setMessage("Error while getting tags for namespace(s).");
        }

        return result;
    }
}
