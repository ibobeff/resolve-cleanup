/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.util.WebUtils;

import com.resolve.auth.ActiveDirectory;
import com.resolve.auth.LDAP;
import com.resolve.auth.RADIUS;
import com.resolve.esb.ESB;
import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.notification.NotificationAPI;
import com.resolve.persistence.model.GroupRoleRel;
import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.ResolveCron;
import com.resolve.persistence.model.ResolveEvent;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.ExportUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigCAS;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.auth.Authentication;
import com.resolve.search.ConfigSearch;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.saas.ExecSummaryAggregation;
import com.resolve.service.License;
import com.resolve.service.LicenseService;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.menu.MenuDefinitionUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.MetricUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLDriverInterface;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.CopyFile;
import com.resolve.util.ERR;
import com.resolve.util.ExecutableObject;
import com.resolve.util.FileUtils;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class Main extends MainBase implements ApplicationContextAware, ServletContextAware
{
	public final static CountDownLatch startupLatch = new CountDownLatch(1);
	
    //Remove when neo4j is moved to its own service
    private static final String NEO_CLUSTER_MODE_PROPERTY = "neo_cluster_mode";
    final static long MAXEVALDAYS = 60;

    static AtomicBoolean active = new AtomicBoolean(false);
    static ApplicationContext appContext;
    static List<String> exportTables = new ArrayList<String>();

    ConfigSQL configSQL;
    ConfigCAS configCAS;

    ConfigLDAP configLDAP;
    ConfigRADIUS configRADIUS;
    ConfigZK configZK;
    ConfigAuth configAuth;
    ConfigActiveDirectory configActiveDirectory;
    ConfigWindows configWindows;
    ConfigSearch configSearch;
    ConfigSelfCheck configSelfCheck;
    ConfigRSLog configRSLog;
    
    LDAP ldap;
    ActiveDirectory activeDirectory;
    RADIUS radius;
    
    private Timer inactiveSessionTimer;
    private long sessionCheckDuration = 5L * 60L * 1000L; // five minutes in ms
    private boolean isPrimary;
    
    /**
     * Main startup method to initialize Resolve
     */
    public synchronized void init()
    {
        try
        {
            init(null);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    } // init

    @Override
    public synchronized void init(String serviceName) throws Exception
    {
        boolean upgradeMode = StringUtils.isNotBlank(System.getProperty(NEO_CLUSTER_MODE_PROPERTY)) ? new Boolean(System.getProperty(NEO_CLUSTER_MODE_PROPERTY)) : false;
        //CassandraUtil.setRSView(true);

        if (!getActive())
        {
            System.out.println("*** Starting RSView ***");
            System.out.println("neo_cluster_mode Flag :" + upgradeMode);
            
            setActive(true);

            try
            {
                // init main
                RSContext.main = this;

                // init release information
                initRelease(new Release(serviceName));

                // init product home
                initHome();

                // init logging
                initLog();
                
                initConfigFile();

                // init enc
                initENC();

                // init configuration
                initConfig();
                
                isPrimary = ((com.resolve.rsview.main.ConfigGeneral)configGeneral).isPrimary();

                // init startup
                initStartup();

                // init executor
                initThreadPools();

                // init timezone
                initTimezone();

                // init message bus
                initESB();

                // init SQL
                initSQL();

                // init persistence
                initPersistence();

                //Neo4j to SQL migration - Done through upgrade process and not from rsview anymore
                //ServiceMigration.migrateNeo4jToSQL();

                // init graphdb - TODO: this will be removed
//                initGraphDB();

                // init system properties
                initProperties();

                //Init resolve URL, need to be after initProperties()
                initResolveUrl();

                // init internal business rules
                initRules();

                // default exports
                initExports();

                // initMetric
                initMetric();

                // init controller properties
                if (isPrimary)
                {
                    initControllers();
                }
                // new MAction().postInstalledOperations(null);

                // start message bus - if the server is in UPGRADE MODE, than do not start the ESB
                if(upgradeMode)
                {
                   System.out.println("Will NOT initialize the ESB listener for queues as 'neo_cluster_mode' flag is set to true");
                   Log.log.info("Will NOT initialize the ESB listener for queues as 'neo_cluster_mode' flag is set to true");
                }
                else
                {
                    startESB();
                }
                

                // start ldap
                startLDAP();

                // start active directory
                startActiveDirectory();
                
                //start RADIUS
                startRADIUS();

                //init mxGraph image files
                initResolveMxGraph();

                // init sequence generator
                initSequenceNumbers();

                // init node id
                initNodeId();

                // save configuration
                initSaveConfig();

                // init cluster properties
                initClusterProperties();

                //initCassandra();

                // init cleanup
                initCleanup();

                // init serviceThreads
                startServiceThreads();

                // save configuration
                initElasticSearch(upgradeMode);

                // start the thread to cleanup the result macro data if any
                startWikiCleanupTasks();

                // initialize Notification API
                NotificationAPI.init();
                
                // init sessionChecker
                initSessionChecker();
                
                // if server is in UPGRADE/INSTALL mode then do not register the RSView or insert into resolve_event
                if (upgradeMode)
                {
                   System.out.println("Will NOT register with RSControl or insert into resolve_event as 'neo_cluster_mode' flag is set to true");
                   Log.log.info("Will NOT register with RSControl or insert into resolve_event as 'neo_cluster_mode' flag is set to true");
                }
                else
                {
                    // init completed
                    initComplete();
    
                    // init license service, must be after initComplete()
                    initLicenseService();
                    
                    // init license
                    checkLicense();
    
                    // register with RSCONTROL
                    initRegistration();
    
                    // start the default tasks
                    startDefaultTasks();
                }
                
                // Update Custom Menu's for Gateways developed by SDK
                updateSDKGatewayMenu();
                
                startSelfCheckTasks();
                
                startESLogCleanup();
                
                if (isPrimary) {
                	UserUtils.encryptUserData();
                	
                	long startTime = System.currentTimeMillis();
                	int cleandUpCount = WorksheetUtil.cleanupResolveSessions();
                	Log.log.debug(String.format("Total time taken to clean up %d Resolve Sessions: %d msec",
                								cleandUpCount, (System.currentTimeMillis() - startTime)));
                }
            }
            catch (Throwable e)
            {
                System.out.println(e.getMessage());
                e.printStackTrace();
                if (Log.log != null)
                {
                    Log.alert(ERR.E10003);
                }
                this.exit(new String[0]);
            }
        }
        
    } // init
    
    
    private void updateSDKGatewayMenu()
    {
        try
        {
            MenuDefinitionUtil.setCustomSDKMenus(RSContext.resolveHome);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    protected void exit(String[] argv)
    {
        Log.log.warn("Terminate initiated");
        ((Main) main).destroy();
        System.exit(0);
    } // exit
    
    
    /**
     * Starts a thread loop to check for inactive sessions and remove them
     */
    private void initSessionChecker() {
        if(inactiveSessionTimer == null) {
            inactiveSessionTimer = new Timer();
        }
        
        inactiveSessionTimer.scheduleAtFixedRate(new TimerTask()
        {
            
            @Override
            public void run()
            {
                Authentication.removeInactiveUserSessions();
                Log.log.info("Removing inactive sessions.");
            }
        }, 0, sessionCheckDuration); 
        Authentication.removeInactiveUserSessions();
         
    }
    
    /**
     * Stops the session check loop
     */
    private void stopSessionChecker() {
        inactiveSessionTimer.cancel();
        inactiveSessionTimer = null;
    }

    /**
     * Main shutdown method for Resolve
     */
    public synchronized void destroy()
    {
        if (getActive())
        {
            System.out.println("*** Stopping RSView ***");

            Log.log.warn("Terminating " + release.name.toUpperCase());
            try
            {
                // stop service threads
                stopServiceThreads();
                
                // stop export listeners
                Log.log.info("Stopping Export Listeners");
                ExportUtil.stopExportAllUpdates(exportTables);

                // stopping message server
                if (mServer != null)
                {
                    Log.log.info("Stopping Message Bus");
                    mServer.close();
                    mServer = null;
                }

                // stopping resolve executor
                Log.log.info("Stopping Executor Service");
                ScheduledExecutor.getInstance().stop();

                //stop index
                //Log.log.info("Stop index.");

                //ServiceIndex.closeAllIndex();
                //Log.log.info("finished stop index");

                // send shutdown alert (snmp trap)
                //Alert.info(AlertType.RSCONTROL_STOP,"RSControl terminated");

                // log event
              HibernateProxy.setCurrentUser("system");
            	HibernateProxy.execute(() -> {

	                ResolveEvent event = new ResolveEvent();
	                event.setUValue(MainBase.main.configId.guid + "|" + release.name.toUpperCase() + " terminated");
	                HibernateUtil.getDAOFactory().getResolveEventDAO().insert(event);

            	});
                
                stopSessionChecker();
            }
            catch (Throwable e)
            {
                Log.log.error("Failed to terminate RSView: " + e.getMessage(), e);
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);

            }
            finally
            {
//                ServiceGraph.stopGraphDB();

                Log.log.info("Shutdown hibernate");
                HibernateUtil.shutdown();
            }

            Log.log.warn("Terminated " + release.name.toUpperCase());

            setActive(false);
        }
    } // destroy

    /**
     * Initialize search (ElasticSearch) service
     */
    protected void initElasticSearch(final boolean upgradeMode)
    {
        WorksheetUtil.initDetailCache(configSearch.isDetailCache(), configSearch.getDetailSize(), configSearch.getDetailThreshold());
        ExecutableObject exeObj = new ExecutableObject()
        {
            public Object execute()
            {
                initElasticSearchInternal(upgradeMode);
                return null;
            }
        };
        HibernateUtil.exclusiveClusterExecution("ELESTICSEARCH_INDEX_CONFIG_LOCK", exeObj);        
    }
    
    protected void initElasticSearchInternal(boolean upgradeMode)
    {
        //search is always on from RSVIEW
        Log.log.info("Initializing Search. upgradeMode=" + upgradeMode);
        
        //add system properties that the RSSearch needs, this removes the Hibernate 
        //dependency from RSRemote when we eventually start accessing RSSearch from RSRemote.
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("search.exclude.namespace", PropertiesUtil.getPropertyString("search.exclude.namespace"));
        properties.put("search.exclude.document.pattern", PropertiesUtil.getPropertyString("search.exclude.document.pattern"));
        properties.put("search.attachment.limit", PropertiesUtil.getPropertyString("search.attachment.limit"));
        properties.put("dashboard.namespace.exclude", PropertiesUtil.getPropertyString("dashboard.namespace.exclude"));
        
        configSearch.setProperties(properties);

        SearchAdminAPI.init(configSearch);
        
        //if the indexes are not available, create them here
        if (!upgradeMode)
        {   
            //boolean isPrimary = ((com.resolve.rsview.main.ConfigGeneral)configGeneral).isPrimary();
            Log.log.info("RSview isPrimary=" + isPrimary);
            if (isPrimary)
            {
                SearchAdminAPI.configureIndex("all");
            }
        }
        else
        {
            Log.log.info("upgradeMode is true!");
        }
        Log.log.info("Search Initialized Successfully.");
        
        if (configSearch.isMigration())
        {
        	SearchAdminAPI.migrateIndex();
            Log.log.info("ES data migration completed Successfully.");
        }

    }
    
    /**
     * This method starts a cron task which periodically cleans up all old elastic search log files. 
     * Cron task parameters are pulled from ConfigSearch properties. This only occurs if valid input data is
     * given for all rssearch.log blueprint.properties.
     * <p>
     * @return      void
     */
    private void startESLogCleanup() {
        
        Integer fileDateThreshold = configSearch.getFileDateThreshold();
        Integer esLogCronInterval = configSearch.getCronTaskIntervalValue();
        String cronTaskIntervalType = configSearch.getCronTaskIntervalType();
        Boolean isRemoveOldLogFiles = configSearch.isRemoveOldLogFiles();
        
        if(isRemoveOldLogFiles != null && isRemoveOldLogFiles) {
            if(fileDateThreshold != null && esLogCronInterval != null && 
//               (cronTaskIntervalType != null || !cronTaskIntervalType.isEmpty())) This does not make any sense HP
               cronTaskIntervalType != null && !cronTaskIntervalType.isEmpty())
            {                
                Map<String, String> esLogProps = new HashMap<String, String>();        
                TimeUnit esLogTimeUnit = null;
                                
                if(cronTaskIntervalType.equalsIgnoreCase("HOURS")) {
                    esLogTimeUnit = TimeUnit.HOURS;
                } else if(cronTaskIntervalType.equalsIgnoreCase("DAYS")) {
                    esLogTimeUnit = TimeUnit.DAYS;
                } else if(cronTaskIntervalType.equalsIgnoreCase("MINUTES")) {
                    esLogTimeUnit = TimeUnit.MINUTES;
                }
                
                if(esLogTimeUnit == null) {                    
                    Log.log.error("An error occurred in starting Elastic Search log cleanup. rsview.search.cronTaskIntervalType not set to either HOURS, DAYS, or MINUTES");                    
                } else {                    
                    esLogProps.put("fileDateThreshold", Integer.toString(fileDateThreshold));
                    ScheduledExecutor.getInstance().executeRepeat(MAction.class, "broadcastRemoveOldESLogs", esLogProps, esLogCronInterval, esLogTimeUnit);                    
                }                
            }
        }
        
    }

    /**
     * Init reference to the spring application context for use throughout the
     * application
     */
    public void setApplicationContext(ApplicationContext ctx) throws BeansException
    {
        RSContext.appContext = ctx;
    } // setApplicationContext

    public void setServletContext(ServletContext ctx) throws BeansException
    {
        RSContext.servletContext = ctx;
    } // setServletContext

    @Override
    protected void initComplete()
    {
        // mserver active
        MainBase.getESB().getMServer().setActive(true);

        // ping guid queue  
        
        Map response = null;
        
        try
        {
            response = Main.esb.call(Main.main.configId.guid, "MAction.esbPing", new HashMap<String, String>(),
                                     this.configSelfCheck.pingThreshold * 20000);
        }
        catch (Exception e1)
        {
            Log.log.error(e1.getMessage(), e1);
        }
        
        if (response == null)
        {
            Log.log.fatal("Failed to get response for ping to RSView guid queue " + Main.main.configId.guid + 
                          " within " + this.configSelfCheck.pingThreshold + " seconds.");
            throw new RuntimeException();
        }
        
        // log msg
        String msg = release.name.toUpperCase() + " started ...";
        Log.log.warn(msg);

        // log event
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	ResolveEvent event = new ResolveEvent();
                event.setUValue(MainBase.main.configId.guid + "|" + msg);
                HibernateUtil.getDAOFactory().getResolveEventDAO().insert(event);
            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        try
        {
            MessageDispatcher.init();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    } // initComplete

    @Override
    protected void initLicenseService() throws ESBException
    {
        this.licenseService = new LicenseService(configId.getGuid(), getResolveHome() + "/tomcat/work/Catalina/localhost/resolve/tm");
        super.initLicenseService();
    }

    /**
     * Since RSVIEW has the timestamp file in different directory we need to override
     */
    @Override
    protected void loadLicense()
    {
        //no need to call supper.checkLicense as RSView has tm file in a diferent location
        licenseService = getLicenseService();
        licenseService.start();

        License license = licenseService.getCurrentLicense();
        if (license != null)
        {
            Log.log.debug("****** LICENSE ******");
            Log.log.debug(license.toString());
            Log.log.debug("****** LICENSE ******");
            // add more common logic here.
        }
    }

    protected void checkLicense()
    {
        getLicenseService().checkLicense();

        License license = getLicenseService().getLicense();
        
        //check for maximum heap size
        if (license != null && license.getMaxMemory() != null && 
        	license.getMaxMemory().containsKey(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) &&
        	license.getMaxMemory().get(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) != null) {
        	licenseService.checkMaximumMemory(this);
        }

        //check for user count
        int numberOfEndUsers = UserUtils.getNamedUserCount();
        //int numberOfAdminUsers = UserUtils.getNumberOfAdminUsers();
        Log.log.debug(" checking users....");
        Log.log.debug(" latest license: " + license.toString());
        Log.log.debug(" number of actual end users " + numberOfEndUsers);
        //Log.log.debug(" number of actual admin users " + numberOfAdminUsers);
        boolean userCountChekedOut = LicenseService.checkUsers(license, numberOfEndUsers);
        licenseService.setIsUserCountViolated(!userCountChekedOut);

        // set Product Name
        String banner = license.getBanner();
        Log.log.info("*** " + banner + " ***");
        String prevDesc = PropertiesUtil.getPropertyString("product.description");

        if(!prevDesc.equals(banner))
        {
            if(!(LicenseEnum.LICENSE_BANNER_COMMUNITY.getKeygenName().equals(prevDesc)
                            || LicenseEnum.LICENSE_BANNER_EVALUATION.getKeygenName().equals(prevDesc)
                            || LicenseEnum.LICENSE_BANNER_DEFAULT.getKeygenName().equals(prevDesc)))
            {
                PropertiesUtil.setPropertyString("product.description", prevDesc);
            }
            else 
            {
                PropertiesUtil.setPropertyString("product.description", banner);
            }
        }
       
        //Log.log.info("*** Product Licensed ***");

        //since this component is ok to run, send its presence.
        getLicenseService().sendLicenseRegistration();

        // schedule next license check
        ScheduledExecutor.getInstance().executeDelayed(this, "checkLicense", 30, TimeUnit.MINUTES);
    }

    void initHome()
    {
        try
        {
            // C:\project\resolve3\dist\tomcat\webapps\resolve\..\..\..
            String location = WebUtils.getRealPath(RSContext.servletContext, "/");

            // check deployed or development environment
            if (location.indexOf("dist") == -1)
            {
                // C:\Program Files\
                RSContext.resolveHome = location.substring(0, location.length() - "/tomcat/webapps/resolve".length());
            }
            else
            {
                // C:\project\resolve3\dist\
                RSContext.resolveHome = location.substring(0, location.indexOf("dist") + 5);
            }
            RSContext.webHome = location;

            // display locations
            System.out.println();
            System.out.println("productHome: " + RSContext.resolveHome);
            System.out.println("webHome: " + RSContext.webHome);
            System.out.println();
        }
        catch (FileNotFoundException e)
        {
            System.out.println("ERROR: Unable to initilize productHome directory");
        }
    } // initHome

    void initResolveUrl()
    {
        RSContext.resolveUrl = PropertiesUtil.getPropertyString("system.resolve.url");
        viewResolveUrl = RSContext.resolveUrl;
    } // initResolveUrl

    @Override
    protected void initLog() throws Exception
    {
        // init logging
        String logConfigFilename = RSContext.getWebHome() + "WEB-INF/log.cfg";

        // init RSVIEW logger
        Log.init(logConfigFilename, release.name.toLowerCase());

        // init AUTH logger
        Log.initAuth();

        Log.log.warn("*** STARTING " + release.name.toUpperCase() + " ***");
        Log.log.warn("Version: " + release.version);

        Log.auth.warn("*** STARTING " + release.name.toUpperCase() + " ***");
        Log.auth.warn("Version: " + release.version);
        
        Log.initComponent(release.serviceName);
    } // initLog

    protected void initConfigFile()  throws Exception {
        // load configuration state and initialization overrides
        String filename = RSContext.getWebHome() + "WEB-INF/config.xml";
        Log.log.info("Loading configuration file: " + filename);

        configFile = FileUtils.getFile(filename);
        configDoc = new XDoc(configFile);
    }

    protected void initENC() throws Exception
    {
        // load configuration state and initialization overrides
        Log.log.info("Loading configuration settings for enc");
        loadENCConfig();
        super.initENC();

    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    }

    @Override
    protected void initConfig() throws Exception
    {
        // load configuration state and initialization overrides
        loadConfig(configDoc);

        configSQL = new ConfigSQL(configDoc);
        configSQL.load();

        //for 5.1, do not comment this as migration needs it.
        configCAS = new ConfigCAS(configDoc);
        configCAS.load();

        configZK = new ConfigZK(configDoc);
        configZK.load();

        Log.log.info("  name: " + configId.name);
        Log.log.info("  id:   " + configId.guid);
        
        // reassign the component with guid
        Log.initComponent(release.serviceName + "/" + configId.guid);
    } // initConfig

    protected void initSQL()
    {
        Log.log.info("Starting SQL");

        // get db driver
        SQLDriverInterface driver = SQL.getDriver(configSQL.getDbtype(), configSQL.getDbname(), configSQL.getHost(), configSQL.getUsername(), configSQL.getP_assword(), configSQL.getUrl(), configSQL.isDebug());

        if (driver != null)
        {
            SQL.init(driver);
            SQL.start();
        }
        HibernateUtil.setDebugOutput(configSQL.isPerfDebug());
        HibernateUtil.setStacktraceOutput(configSQL.isDebug());
        
        //GenericJDBCHandler.setDBType(configSQL.getDbtype().toUpperCase());
    } // initSQL

    protected void initPersistence()
    {
        Log.log.info("Starting to Initialize Persistenace");
        String customMappingFileLocation = RSContext.getWebHome() + "WEB-INF";
        String hibernateCfgLocation = RSContext.getWebHome() + "WEB-INF" + File.separator + "hibernate.cfg.xml";

//        ServiceHibernate.initPersistence(configSQL, configCAS, customMappingFileLocation, hibernateCfgLocation);
        GeneralHibernateUtil.initPersistence(configSQL, configCAS, customMappingFileLocation, hibernateCfgLocation);
        HibernateUtil.initMasterLockObject();
        //new GenericJDBCHandler().refreshTableMap(null);
    } // initPersistence

//    public void initGraphDB()
//    {
//        Log.log.info("Starting to Initialize GraphDB");
//        ServiceGraph.initGraphDB();
//    } // initGraphDB

    /**
     * Reads the props file and load it in the Properties table
     */
    private void initProperties() throws Exception
    {
        String filename = RSContext.getWebHome() + "WEB-INF/rsview.properties";

        PropertiesUtil.initProperties(filename);
        
        //init the social notification properties
//        try
//        {
//            filename = RSContext.getWebHome() + "WEB-INF/social_notifications.properties";
//            SocialNotificationUtil.initSocialNotifications(filename);
//        }
//        catch(Exception e)
//        {
//            Log.log.error("Error in loading the social notification properties", e);
//        }
        
        
    } // initProperties

    public static void reloadProperties()  throws Exception {
        String filename = RSContext.getWebHome() + "WEB-INF/rsview.properties";
        com.resolve.services.hibernate.util.PropertiesUtil.initProperties(filename);
    }
    
    @Override
    protected void initESB() throws Exception
    {
        Log.log.info("Starting ESB");
        super.initESB();

        //TODO, this is an exception, we should have kept the Main.java in com.resolve.rsview package itself
        //just like other projects.
        defaultClassPackage = "com.resolve." + release.name.toLowerCase() + ".main";
    } // initESB

    @Override
    protected void startESB() throws Exception
    {
        super.startESB();

		if (((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral).isDcsEnabled()) {
			try {
				
				String username = ((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral)
						.getDcsAuthenticationUsername();
				String password = ((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral)
						.getDcsAuthenticationPassword();

				boolean success = registerDCSClient(username, password);

				Log.log.info((success ? "SUCCESSFUL" : "UNSUCCESSFUL") + " Client Registration send to RabbitMq.");

			} catch (Exception ex) {
				Log.log.info("Error while registering DCS client. " + ex.getMessage());
			}
        }
        
        // RSVIEW
        MListener listener = mServer.createListener(Constants.ESB_NAME_RSVIEW, defaultClassPackage);
        listener.init(false);

        // RSWIKI
        MListener listener1 = mServer.createListener(Constants.ESB_NAME_RSWIKI, defaultClassPackage);
        listener1.init(false);

        Log.log.info("  Initializing ESB Listener for the queue: " + MainBase.main.configId.getGuid() + " with listener class package set to " + defaultClassPackage);
        MListener mListener = mServer.createListener(MainBase.main.configId.getGuid(), defaultClassPackage);
        mListener.init(false);

        // add default subscription
        mServer.subscribePublication(Constants.ESB_NAME_BROADCAST);
        mServer.subscribePublication(Constants.ESB_NAME_RSVIEWS);
        mServer.subscribePublication(Constants.ESB_NAME_RSWIKIS);

        // HibernateUtil init esb
        HibernateUtil.setEsb(esb, configId.guid);
    }

    void initRegistration() throws Exception
    {
        MRegister.register();
    } // initRegistration

    void initRules() throws Exception
    {
        // resolve_cron
        ResolveCron.initListener(getESB());

        //ResolveTag listener
//        ResolveTag.initListener(getESB());
//
//        //ResolveActionTask listener
//        ResolveActionTask.initListener(getESB());
//
//        //ResolveAssess listener
//        ResolveAssess.initListener(getESB());
//
//        //ResolvePreprocess listener
//        ResolvePreprocess.initListener(getESB());
//
//        //ResolveParser listener
//        ResolveParser.initListener(getESB());
//
//        //ResolveActionInvocOptions listener
//        ResolveActionInvocOptions.initListener(getESB());

        // Users / Roles / Groups listeners
        Users.initListener(getESB());
        Roles.initListener(getESB());
        Groups.initListener(getESB());
        UserRoleRel.initListener(getESB());
        UserGroupRel.initListener(getESB());
        GroupRoleRel.initListener(getESB());

        //WikiDocument listener - TODO for 3.2.1/3.3.1 as this will require refactoring
        //        WikiDocument.initListener(getESB());

    } // initRules

    void initExports() throws Exception
    {
        // init exports
        Log.log.info("Initialize Export Listeners");
        ExportUtil.setDefaultOutputDirectory(RSContext.getWebHome() + "/WEB-INF/updates");

        // get list of classes from property
        String exportsProperty = PropertiesUtil.getPropertyString("export.listener.tables");
        if (exportsProperty != null)
        {
            String[] exports = exportsProperty.split(",");
            for (int i = 0; i < exports.length; i++)
            {
                exportTables.add(exports[i].trim());
            }
        }

        // init listeners
        ExportUtil.startExportAllUpdates(exportTables);
    } // initExports

    void initKerberos()
    {
        final String krbfile = RSContext.getWebHome() + "/WEB-INF/krb5.conf";
        final String loginfile = RSContext.getWebHome() + "/WEB-INF/login.conf";

        System.setProperty("java.security.krb5.conf", krbfile);
        System.setProperty("java.security.auth.login.config", loginfile);
    } // initKerberos

    void initCleanup()
    {
        //for some cleanup operations....use MAction.upgradeCleanup if possible

    } // initCleanup

    @Override
    protected void initMetric() throws Exception
    {
        metric = new Metric();
        String blueprint = MRegister.getBlueprintData();
        InputStream stream = new ByteArrayInputStream(blueprint.getBytes(StandardCharsets.UTF_8));
        Properties p = new Properties();
        p.load(stream);
        
    } // initMetric

    void startLDAP()
    {
        try
        {
            //populate the db with the default value if needed
            //            populateConfigLDAP();

            if (configLDAP.isActive())
            {
                Log.log.info("Starting LDAP authentication service");

                Log.log.debug("  mode:       " + configLDAP.getMode());
                Log.log.debug("  ipaddress:  " + configLDAP.getIpaddress());
                Log.log.debug("  port:       " + configLDAP.getPort());
                Log.log.debug("  bindDN:     " + configLDAP.getBindDN());

                File syncFile = null;
                if (configLDAP.isSync())
                {
                    syncFile = FileUtils.getFile(RSContext.getWebHome() + "/WEB-INF/ldap.properties");
                }
                ldap = new LDAP(configLDAP.getMode(), configLDAP.getIpaddress(), configLDAP.getPort(), configLDAP.getBindDN(), configLDAP.getBindP_assword(), configLDAP.getBaseDNList(), configLDAP.isSsl(), configLDAP.getCryptType(), configLDAP.getCryptPrefix(), syncFile);
                ldap.setVersion(configLDAP.getVersion());
                ldap.setDefaultDomain(configLDAP.getDefaultDomain());
                ldap.setUIDAttribute(configLDAP.getUidAttribute());
                ldap.setPasswordAttribute(configLDAP.getP_asswordAttribute());

                /*
                 * if (ldap.connect()) { ldap.disconnect(); } else {
                 * Log.log.error("Failed to start LDAP authentication service"); }
                 */
                Log.log.info("Completed LDAP startup");
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Unable to initialize LDAP. " + e.getMessage(), e);
        }
    } // startLDAP

    void startActiveDirectory()
    {
        try
        {
            //populate the db with the default value if needed
            //            populateConfigActiveDirectory();

            if (configActiveDirectory.isActive())
            {
                Log.log.info("Starting ActiveDirectory authentication service");

                Log.log.debug("  ipaddress:  " + configActiveDirectory.getIpaddress());
                Log.log.debug("  port:       " + configActiveDirectory.getPort());

                File syncFile = null;
                if (configActiveDirectory.isSync())
                {
                    syncFile = FileUtils.getFile(RSContext.getWebHome() + "/WEB-INF/ldap.properties");
                }
                activeDirectory = new ActiveDirectory(configActiveDirectory.getIpaddress(), configActiveDirectory.getPort(), configActiveDirectory.getBaseDNList(), configActiveDirectory.isSsl(), configActiveDirectory.getMode(), configActiveDirectory.getBindDN(), configActiveDirectory.getBindPassword(), syncFile);
                activeDirectory.setVersion(configActiveDirectory.getVersion());
                activeDirectory.setDefaultDomain(configActiveDirectory.getDefaultDomain());
                activeDirectory.setUIDAttribute(configActiveDirectory.getUidAttribute());

                if (activeDirectory.connect())
                {
                    activeDirectory.disconnect();
                }
                else
                {
                    Log.log.error("Failed to start ActiveDirectory authentication service");
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Unable to initialize Active Directory. " + e.getMessage(), e);
        }
    } // startActiveDirectory
    
    void startRADIUS()
    {
        try
        {
           if (configRADIUS.isActive())
            {
                Log.log.info("Starting RADIUS authentication service");
                Log.log.debug("  Primary Radius Server IPAddress:     " + configRADIUS.getPrimaryRadiusServerIpAddress());
                Log.log.debug("  Secondary Radius Server IPAddress:     " + configRADIUS.getSecondaryRadiusServerIpAddress());
                Log.log.debug("  Autheticaion Port:  " + configRADIUS.getAuthPort());
                Log.log.debug("  Accounting Port:       " + configRADIUS.getAccntPort());
                Log.log.debug("  Autheticaion Protocol:       " + configRADIUS.getAuthProtocol());
               
                radius = new RADIUS(configRADIUS.primaryRadiusServerIpAddress,configRADIUS.secondaryRadiusServerIpAddress,configRADIUS.authProtocol, configRADIUS.authPort, configRADIUS.accntPort, configRADIUS.sharedSecret);
                
                Log.log.info("Completed RADIUS startup");
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Unable to initialize RADIUS. " + e.getMessage(), e);
        }
    } // startLDAP

    @Override
    protected void loadConfig(XDoc configDoc) throws Exception
    {
        configGeneral = new ConfigGeneral(configDoc);
        configGeneral.load();
        setClusterModeProperty(configGeneral.getClusterName() + "." + Constants.CLUSTER_MODE);

        // init heartbeat
        // isActive = configGeneral.primary;
        // heartbeat = new Heartbeat(configGeneral.heartbeat,
        // configGeneral.failover);

        configLDAP = new ConfigLDAP(configDoc);
        configLDAP.load();

        configActiveDirectory = new ConfigActiveDirectory(configDoc);
        configActiveDirectory.load();
        
        configRADIUS = new ConfigRADIUS(configDoc);
        configRADIUS.load();

        configWindows = new ConfigWindows(configDoc);
        configWindows.load();

        configSearch = new ConfigSearch(configDoc);
        configSearch.load();
        
        configAuth = new ConfigAuth(configDoc);
        configAuth.load();

        configSelfCheck = new ConfigSelfCheck(configDoc);
        configSelfCheck.load();
        
        configRSLog = new ConfigRSLog(configDoc);
        configRSLog.load();
        
        super.loadConfig(configDoc);

    } // loadConfig

    @Override
    protected void saveConfig() throws Exception
    {
        configGeneral.save();

        configId.save();

        configESB.save();

        configLDAP.save();

        configActiveDirectory.save();
        
        configRADIUS.save();

        configWindows.save();

        configSQL.save();

        configCAS.save();

        configZK.save();

        configSearch.save();
        
        configSelfCheck.save();
        
        configRSLog.save();

        super.saveConfig();
    } // saveConfig

    @Override
    protected void initSaveConfig() throws Exception
    {
        Log.log.info("Saving configuration");
        saveConfig();

        // copy config.xml to config.bak
        Log.log.info("Backup configuration");
        CopyFile.setRevisions(configGeneral.getConfigRevisions());
        CopyFile.backup(configFile.getAbsolutePath());

        // save configuration to file
        configDoc.toPrettyFile(configFile);
        Log.log.info("Saved configuration");
    } // initSaveConfig

    public static boolean getActive()
    {
        return active.get();
    } // getActive

    public static void setActive(boolean active)
    {
        Main.active.set(active);
    } // setActive

    public static ESB getESB()
    {
        return esb;
    } // getESB

    private void initResolveMxGraph()
    {
        (new ResolveMxGraph()).init();
    } // initResolveMxGraph

    public LDAP getLDAP()
    {
        return ldap;
    } // getLDAP

    public ActiveDirectory getActiveDirectory()
    {
        return activeDirectory;
    } // getActiveDirectory

    public RADIUS getRADIUS()
    {
        return radius;
    }//getRADIUS
    
    @Override
    public String getProductHome()
    {
        return RSContext.getWebHome();
    } // getProductHome

    @Override
    public String getGSEHome()
    {
        return RSContext.getWebHome() + "/WEB-INF/service";
    } // getGSEHome

    static void initSequenceNumbers()
    {
        /*
        HibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_EXEC);
        HibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_PRB);
        HibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_PROC);
        HibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_INIT);
        HibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_RC);
        HibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_RV);
        */

//        SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_EXEC);
//        SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PRB);
//        SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PROC);
//        SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_INIT);
//        SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_RC);
//        SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_RV);
    } // initSequenceNumbers

    void initNodeId()
    {
        if (StringUtils.isEmpty(this.configId.getNodeid()))
        {
            //          String nodeid = HibernateUtil.getNextSequenceString(Constants.PERSISTENCE_SEQ_RV);
            String nodeid = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_RV);
            this.configId.setNodeid(nodeid);
        }
    } // initNodeId

    void startServiceThreads()
    {
        // MAction.index

    } // startServiceThreads

    void stopServiceThreads()
    {
        // MAction.index
    } // stopServiceThreads

    void startDefaultTasks()
    {
        // register
        ScheduledExecutor.getInstance().executeRepeat("REGISTER", MRegister.class, "register", 60, Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        if (Main.main.configRegistration.isLogHeartbeat())
        {
            ScheduledExecutor.getInstance().executeRepeat("REGISTER", MRegister.class, "logHeartbeat", 60, Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        }

        // send metrics
        ScheduledExecutor.getInstance().executeRepeat(metric, "sendMetrics", 5, 5, TimeUnit.MINUTES);

        // send licensed users metrics
        ScheduledExecutor.getInstance().executeRepeat(metric, "sendMetricLicensedUsers", 5, 24, TimeUnit.HOURS);
        //        ScheduledExecutor.getInstance().executeRepeat(Metric.class, "sendMetrics", 1, 1, TimeUnit.MINUTES);
        
        if(configSearch.isSyncTask())
        {
            ScheduledExecutor.getInstance().executeRepeat("SYNC_WIKI", MIndex.class, "synchronizeWiki", 60, configSearch.getSyncTaskInterval(), TimeUnit.SECONDS);
            ScheduledExecutor.getInstance().executeRepeat("SYNC_AT", MIndex.class, "synchronizeActionTask", 60, configSearch.getSyncTaskInterval(), TimeUnit.SECONDS);
        }

        ScheduledExecutor.getInstance().executeRepeat(HibernateUtil.class, "refreshPool", 10, HibernateUtil.refreshInterval, TimeUnit.SECONDS);
        
        // Delete Aggregated Execution Summary Indices
        ScheduledExecutor.getInstance().executeRepeat(ExecSummaryAggregation.class, "deleteAggregatedExecSummaryIndices", 17, 17, 
    								 TimeUnit.MINUTES);
        
        // unlock soft-locks on wikis.
        ScheduledExecutor.getInstance().executeDelayedRepeat(MWiki.class, "unlockAllSoftLocks", 12, 24, TimeUnit.HOURS);

        
        //This hack needs to be fixed after cluster login UI has been fixed.
//        if (!StringUtils.isBlank(configCAS.getPassword()))
//        {
//            MetricUtil.REMOTE_AUTO_DEPLOY_PASSWORD = configCAS.getPassword();
//        }
//        if (!StringUtils.isBlank(configCAS.getCluster()))
//        {
//            MetricUtil.REMOTE_AUTO_DEPLOY_USERNAME = configCAS.getCluster();
//        }
        if (MetricUtil.isCentralMCP())
        {
            startMCPAutoDeploy();
        }
        
        if (((com.resolve.rsview.main.ConfigGeneral)Main.main.configGeneral).isPrimary()) {
        	// Perform Execution Summary Aggregation
        	ScheduledExecutor.getInstance().executeRepeat(ExecSummaryAggregation.class, "aggregateUsage", 0, 7, TimeUnit.MINUTES);
        	
        	// Conduct Gateway Health Checks on all active gateways
        	
        	Map<String, Object> params = new HashMap<String, Object>();
    		
    		params.put(ServiceGateway.PARAM_MAINBASE_KEY, Main.main);
    		params.put(ServiceGateway.PARAM_USERNAME_KEY, UserUtils.SYSTEM);
    		
        	ScheduledExecutor.getInstance().executeRepeat(ServiceGateway.class, "conductGatewayHealthChecks", params, 0, 
        								 ((com.resolve.rsview.main.ConfigGeneral)Main.main.configGeneral).
        								 getGtwHealthChkIntrvl(), TimeUnit.MINUTES);
        }
        
    } // startDefaultTasks
    
    void startMCPAutoDeploy()
    {
        ScheduledExecutor.getInstance().executeRepeat(MetricUtil.class, "sendMCPAutoDeployRules", 5, 5, TimeUnit.MINUTES);
    }
    
    
    void startSelfCheckTasks()
    {
    	if (configSelfCheck.isDbSCActive())
    	{
    		RSViewSelfCheck.dbTimeout = configSelfCheck.getDbSCTimeout();
    		ScheduledExecutor.getInstance().executeRepeat(RSViewSelfCheck.class, "checkDBConnectivity", 10, configSelfCheck.getDbSCInterval(), TimeUnit.SECONDS);
    	}
    	
    	if (configSelfCheck.isEsSCActive())
        {
    		RSViewSelfCheck.esSCRetryCount = configSelfCheck.getEsSCRetryCount();
    		RSViewSelfCheck.esSCRetryDelay = configSelfCheck.getEsSCRetryDelay();
            ScheduledExecutor.getInstance().executeRepeat(RSViewSelfCheck.class, "checkESHealth", 10, configSelfCheck.getEsSCInterval(), TimeUnit.SECONDS);
        }
    	
    	if (configSelfCheck.isPingActive())
        {
    		RSViewSelfCheck.pingThreshold = configSelfCheck.getPingThreshold();
            ScheduledExecutor.getInstance().executeRepeat(RSViewSelfCheck.class, "pingRsControl", 10, configSelfCheck.getPingInterval(), TimeUnit.SECONDS);
            //ScheduledExecutor.getInstance().executeRepeat(MAction.class, "pingRsControlStatus", 13, configSelfCheck.getPingInterval(), TimeUnit.SECONDS);
        }
    	
    	if (configSelfCheck.isLdapAdActive())
    	{
    		if (configLDAP.isActive())
    		{
    			RSViewSelfCheck.ssl = configLDAP.isSsl();
    			RSViewSelfCheck.port = configLDAP.getPort();
    			RSViewSelfCheck.host = configLDAP.getIpaddress();
    		}
    		else if (configActiveDirectory.isActive())
    		{
    			RSViewSelfCheck.ssl = configActiveDirectory.isSsl();
    			RSViewSelfCheck.port = configActiveDirectory.getPort();
    			RSViewSelfCheck.host = configActiveDirectory.getIpaddress();
    		}
    		ScheduledExecutor.getInstance().executeRepeat(RSViewSelfCheck.class, "checkLdapAdConnection", 10, configSelfCheck.getLdapAdInterval(), TimeUnit.SECONDS);
    	}
    	
    	if (configSelfCheck.isHttp())
    	{
    		String blueprintFileName = "rsmgmt/config/blueprint.properties";
    		Properties configureProperties = new Properties();
    		try
    		{
	    		FileInputStream fis = new FileInputStream(blueprintFileName);
	            configureProperties.load(fis);
	            fis.close();
    		}
    		catch (Exception e)
    		{
    			Log.log.error(e);
    		}
    		
    		if (configureProperties.size() > 0)
    		{
	            String protocol = null;
	        	int port = 0;
	        	if (configureProperties.getProperty("rsview.tomcat.http").equals("true"))
	        	{
	        		protocol = "http";
	        		port = Integer.parseInt(configureProperties.get("rsview.tomcat.connector.http.port"));
	        	}
	        	else
	        	{
	        		protocol = "https";
	        		port = Integer.parseInt(configureProperties.get("rsview.tomcat.connector.https.port"));
	        	}
	        	
	        	RSViewSelfCheck.httpHost = "localhost";
	        	RSViewSelfCheck.httpPort = port;
	        	RSViewSelfCheck.httpProtocol = protocol;
	        	ScheduledExecutor.getInstance().executeRepeat(RSViewSelfCheck.class, "rsviewHttpSelfCheck", 10, configSelfCheck.getHttpInterval(), TimeUnit.SECONDS);
	        	
    		}
    	}
    	
    	
    }

    void startWikiCleanupTasks()
    {
        ScheduledExecutor.getInstance().executeRepeat("WikiResultMacroCleanUp", MWiki.class, "cleanupResultMacroCache", 60, 60, TimeUnit.SECONDS);
    }

    /*
     * The controller initialization will be done as part of import.
     * Content package will have all the App Security Roles and will be imported during the fresh install.
     */
    private void initControllers() throws Exception
    {
        String filename = RSContext.getWebHome() + "WEB-INF/controllers.properties";
        ServiceHibernate.initControllerProperties(filename);

    } // initControllers

    public ConfigCAS getConfigCAS()
    {
        return configCAS;
    }

    public ConfigSQL getConfigSQL()
    {
        return configSQL;
    }

    public ConfigLDAP getConfigLDAP()
    {
        return configLDAP;
    }

    public ConfigActiveDirectory getConfigActiveDirectory()
    {
        return configActiveDirectory;
    }
    
    public ConfigRADIUS getConfigRADIUS()
    {
        return configRADIUS;
    }

    public ConfigWindows getConfigWindows()
    {
        return configWindows;
    }

    public ConfigZK getConfigZK()
    {
        return configZK;
    }
    
    public ConfigAuth getConfigAuth()
    {
        return configAuth;
    }

	public ConfigRSLog getConfigRSLog()
    {
        return configRSLog;
    }

    @Override
    protected void initClusterProperties()
    {
        String clusterMode = PropertiesUtil.getPropertyString(getClusterModeProperty());
        if(StringUtils.isBlank(clusterMode))
        {
            //this is for mainly standalone system where there is no
            //system property set
            clusterMode = Constants.CLUSTER_MODE_ACTIVE;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put(getClusterModeProperty(), clusterMode);
        setClusterProperties(params);
        
        Log.log.info("Resolve cluster : " + configGeneral.getClusterName() + ", Cluster mode : " + clusterMode);
        
        //persist the property  in DB
        PropertiesUtil.setProperties(params);
    }

    public void activateCluster(Map<String, String> params)
    {
        //prevent reactivating if it's already active
        if(!isClusterModeActive())
        {
            try
            {
                params.put(getClusterModeProperty(), Constants.CLUSTER_MODE_ACTIVE);
                setClusterProperties(params);
                //persist the property in DB
                PropertiesUtil.setProperties(params);
                
                //do something like starting some process?
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    public void deactivateCluster(Map<String, String> params)
    {
        //prevent deactivating if it's already inactive
        if(isClusterModeActive())
        {
            try
            {
                params.put(getClusterModeProperty(), Constants.CLUSTER_MODE_INACTIVE);
                setClusterProperties(params);
                //persist the property in DB
                PropertiesUtil.setProperties(params);
                
                //do something like stopping some process?
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
} // Main
