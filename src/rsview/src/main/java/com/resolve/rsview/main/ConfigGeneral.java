/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import com.resolve.util.XDoc;

public class ConfigGeneral extends com.resolve.rsbase.ConfigGeneral
{
    private static final long serialVersionUID = 2714031275237877826L;
    
    // Gateway health check interval should be at or above 5 minutes
    private static final int GTW_HEALTH_CHK_INTERVAL_MIN_THRESHOLD_MINUTES = 5;
    private static final int DEFAULT_GTW_HEALTH_CHK_INTERVAL_IN_MINUTES = 10;
    
    boolean primary = true;
    int heartbeat = 20;
    int failover = 60;
    public int indexthreadcount = 5;
    String indexwikidoc = ""; 
    String indexattachment = "";
    String indexactiontask = "";
    String indexdecisiontree = "";
    String indexposts = "";
    String indexhashtag = "";
    String indexquery = "";
    String indexDictionary = "";
    String indexWorkSheet = "";
    String indexProcessRequest = "";
    String indexTaskResult = "";
    private boolean saveUserCredentials = false;
    private boolean dcsEnabled = false;
    private String reportingInstances = "";
    private String dcsAuthenticationInstance = "";
    private String dcsAuthenticationUsername = "";
    private String dcsAuthenticationPassword = "";
    private boolean sslEnabled=false;
    private boolean sslVerify;
    private String trustStore;
    private String trustStorePassword;
    // Gateway Health Check Interval in minutes, defaults to 10 minutes
    private int gtwHealthChkIntrvl = DEFAULT_GTW_HEALTH_CHK_INTERVAL_IN_MINUTES; 
    public int scheduledPool;
    
    public ConfigGeneral(XDoc config) throws Exception
    {
        super(config);
        indexwikidoc = RSContext.getWebHome() + "WEB-INF/index/wikidoc";
        indexattachment = RSContext.getWebHome() + "WEB-INF/index/attachment";
        indexactiontask = RSContext.getWebHome() + "WEB-INF/index/actiontask";
        indexdecisiontree = RSContext.getWebHome() + "WEB-INF/index/decisiontree";
        indexposts = RSContext.getWebHome() + "WEB-INF/index/posts";
        indexhashtag = RSContext.getWebHome() + "WEB-INF/index/hashtag";
        indexquery = RSContext.getWebHome() + "WEB-INF/index/query";
        indexDictionary = RSContext.getWebHome() + "WEB-INF/index/dictionary";
        indexWorkSheet = RSContext.getWebHome() + "WEB-INF/index/worksheet";
        indexProcessRequest = RSContext.getWebHome() + "WEB-INF/index/processrequest";
        indexTaskResult = RSContext.getWebHome() + "WEB-INF/index/taskresult";
        
        define("primary", BOOLEAN, "./GENERAL/@PRIMARY");
        define("heartbeat", INTEGER, "./GENERAL/@HEARTBEAT");
        define("failover", INTEGER, "./GENERAL/@FAILOVER");
        define("indexthreadcount", INTEGER, "./GENERAL/@INDEXTHREADCOUNT");
        define("indexwikidoc", STRING, "./GENERAL/@INDEXWIKIDOC");
        define("indexattachment", STRING, "./GENERAL/@INDEXATTACHMENT");
        define("indexactiontask", STRING, "./GENERAL/@INDEXACTIONTASK");
        define("indexdecisiontree", STRING, "./GENERAL/@INDEXDECISIONTREE");
        define("indexposts", STRING, "./GENERAL/@INDEXPOSTS");
        define("indexhashtag", STRING, "./GENERAL/@INDEXHASHTAG");
        define("indexquery", STRING, "./GENERAL/@INDEXQUERY");
        define("indexDictionary", STRING, "./GENERAL/@INDEXDICTIONARY");
        define("indexWorkSheet", STRING, "./GENERAL/@INDEXWORKSHEET");
        define("indexProcessRequest", STRING, "./GENERAL/@INDEXPROCESSREQUEST");
        define("indexTaskResult", STRING, "./GENERAL/@INDEXTASKRESULT");
        define("saveUserCredentials", BOOLEAN, "./GENERAL/@SAVEUSERCREDENTIALS");
        define("reportingInstances", STRING, "./GENERAL/@REPORTINGINSTANCES");
        define("sslEnabled",BOOLEAN, "./GENERAL/@SSLENABLED");
        define("sslVerify",BOOLEAN, "./GENERAL/@SSLVERIFY");
        define("trustStore",STRING, "./GENERAL/@TRUSTSTORE");
        define("trustStorePassword",STRING, "./GENERAL/@TRUSTSTOREPASSWORD");
        define("gtwHealthChkIntrvl", INTEGER, "./GENERAL/@GATEWAYHEALTHCHECKINTERVALINMINUTES");
        define("dcsAuthenticationInstance", STRING, "./GENERAL/@DCSAUTHENTICATIONINSTANCE");
        define("dcsAuthenticationUsername", STRING, "./GENERAL/@DCSAUTHENTICATIONUSERNAME");
        define("dcsAuthenticationPassword", STRING, "./GENERAL/@DCSAUTHENTICATIONPASSWORD");
		define("scheduledPool", INTEGER, "./GENERAL/@SCHEDULEDPOOL");        
		define("dcsEnabled", BOOLEAN, "./GENERAL/@ISDCSENABLED");    } // ConfigGeneral
    
    public void load()
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public boolean isPrimary()
    {
        return primary;
    }

    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    public int getHeartbeat()
    {
        return heartbeat;
    }

    public void setHeartbeat(int heartbeat)
    {
        this.heartbeat = heartbeat;
    }

    public int getFailover()
    {
        return failover;
    }

    public void setFailover(int failover)
    {
        this.failover = failover;
    }
    
    public int getIndexthreadcount()
    {
        return this.indexthreadcount;
    }
    
    public void setIndexthreadcount(int indexthreadcount)
    {
        this.indexthreadcount = indexthreadcount;
    }
    
    public void setIndexwikidoc(String indexwikidoc)
    {
        this.indexwikidoc = indexwikidoc;
    }
    
    public String getIndexwikidoc()
    {
        return this.indexwikidoc;
    }
    
    public void setIndexattachment(String indexattachment)
    {
        this.indexattachment = indexattachment;
    }
    
    public String getIndexattachment()
    {
        return this.indexattachment;
    }

    public void setIndexactiontask(String indexactiontask)
    {
        this.indexactiontask = indexactiontask;
    }
    
    public String getIndexactiontask()
    {
        return this.indexactiontask;
    }
    
    public void setIndexdesiciontree(String indexdecisiontree)
    {
        this.indexdecisiontree = indexdecisiontree;
    }
    
    public String getIndexdecisiontree()
    {
        return this.indexdecisiontree;
    }

    public void setIndexdecisiontree(String indexdecisiontree)
    {
        this.indexdecisiontree = indexdecisiontree;
    }
    
    public String getIndexposts()
    {
        return this.indexposts;
    }

    public void setIndexposts(String indexposts)
    {
        this.indexposts = indexposts;
    }
    
    public String getIndexhashtag()
    {
        return this.indexhashtag;
    }
    
    public void setIndexhashtag(String indexhashtag)
    {
        this.indexhashtag = indexhashtag;
    }
    
    public String getIndexquery()
    {
        return this.indexquery;
    }
    
    public void setIndexquery(String indexquery)
    {
        this.indexquery = indexquery;
    }
    
    public String getIndexDictionary()
    {
        return this.indexDictionary;
    }
    
    public void setIndexDictionary(String indexDictionary)
    {
        this.indexDictionary = indexDictionary;
    }
    
    public String getIndexWorkSheet()
    {
        return this.indexWorkSheet;
    }
    
    public void setIndexWorkSheet(String indexWorkSheet)
    {
        this.indexWorkSheet = indexWorkSheet;
    }
    
    public String getIndexProcessRequest()
    {
        return this.indexProcessRequest;
    }
    
    public void setIndexProcessRequest(String indexProcessRequest)
    {
        this.indexProcessRequest = indexProcessRequest;
    }
    
    public String getIndexTaskResult()
    {
        return this.indexTaskResult;
    }
    
    public void setIndexTaskResult(String indexTaskResult)
    {
        this.indexTaskResult = indexTaskResult;
    }
    
    public boolean isSaveUserCredentials()
    {
        return saveUserCredentials;
    }

    public void setSaveUserCredentials(boolean saveUserCredentials)
    {
        this.saveUserCredentials = saveUserCredentials;
    }
    
    public String getReportingInstances()
    {
        return reportingInstances;
    }
    
    public void setReportingInstances(String reportingInstances)
    {
        this.reportingInstances = reportingInstances;
    }
    
    public boolean isSslEnabled()
    {
        return sslEnabled;
    }

    public void setSslEnabled(boolean sslEnabled)
    {
        this.sslEnabled = sslEnabled;
    }

    public boolean isSslVerify()
    {
        return sslVerify;
    }

    public void setSslVerify(boolean sslVerify)
    {
        this.sslVerify = sslVerify;
    }

    public String getTrustStore()
    {
        return trustStore;
    }

    public void setTrustStore(String trustStore)
    {
        this.trustStore = trustStore;
    }

    public String getTrustStorePassword()
    {
        return trustStorePassword;
    }

    public void setTrustStorePassword(String trustStorePassword)
    {
        this.trustStorePassword = trustStorePassword;
    }
    
    public int getGtwHealthChkIntrvl()
    {
        return gtwHealthChkIntrvl;
    }

    public void setGtwHealthChkIntrvl(int gtwHealthChkIntrvl)
    {
    	int tGtwHealthChkIntrvl = gtwHealthChkIntrvl;
    	
    	if (gtwHealthChkIntrvl < GTW_HEALTH_CHK_INTERVAL_MIN_THRESHOLD_MINUTES) {
    		tGtwHealthChkIntrvl = GTW_HEALTH_CHK_INTERVAL_MIN_THRESHOLD_MINUTES;
    	}
    	
        this.gtwHealthChkIntrvl = tGtwHealthChkIntrvl;
    }

    
    public String getDcsAuthenticationInstance() {
		return dcsAuthenticationInstance;
	}
    

	public void setDcsAuthenticationInstance(String dcsAuthenticationInstance) {
		this.dcsAuthenticationInstance = dcsAuthenticationInstance;
	}

	public String getDcsAuthenticationUsername() {
		return dcsAuthenticationUsername;
	}

	public void setDcsAuthenticationUsername(String dcsAuthenticationUsername) {
		this.dcsAuthenticationUsername = dcsAuthenticationUsername;
	}

	public String getDcsAuthenticationPassword() {
		return dcsAuthenticationPassword;
	}

	public void setDcsAuthenticationPassword(String dcsAuthenticationPassword) {
		this.dcsAuthenticationPassword = dcsAuthenticationPassword;
	}

	public boolean isDcsEnabled() {
		return dcsEnabled;
	}

	public void setDcsEnabled(boolean dcsEnabled) {
		this.dcsEnabled = dcsEnabled;
	}
    
    public int getScheduledPool() {
        return scheduledPool;
    }

    public void setScheduledPool(int scheduledPool) {
        this.scheduledPool = scheduledPool;
    }
    
} // ConfigGeneral
