/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

import com.resolve.util.StringUtils;

public class ValidateTokenResult
{
    private boolean valid;
    private String user;
    private String ssoType;
    private String validToken;
    private String iFrameId;

    public ValidateTokenResult(boolean valid, String user, String ssoType, String validToken, String iFrameId)
    {
        this.valid = valid;
        this.user = user;
        this.ssoType = ssoType;
        this.validToken = validToken;
        this.iFrameId = iFrameId;
    }

    public String toString()
    {
        return "ValidateTokenResult: valid=" + valid + 
               ", user=" + (StringUtils.isNotBlank(user) ? user : "") + 
               ", ssoType=" + (StringUtils.isNotBlank(ssoType) ? ssoType : "") + 
               ", validToken=" + (StringUtils.isNotBlank(validToken) ? validToken : "") +
               ", iFrameId=" + (StringUtils.isNotBlank(iFrameId) ? iFrameId : "");
    }

    public boolean isValid()
    {
        return valid;
    }
    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public String getUser()
    {
        return user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getSsoType()
    {
        return ssoType;
    }
    public void setSsoType(String ssoType)
    {
        this.ssoType = ssoType;
    }

    public String getValidToken()
    {
        return validToken;
    }
    public void setValidToken(String validToken)
    {
        this.validToken = validToken;
    }

    public String getiFrameId()
    {
        return iFrameId;
    }
    public void setiFrameId(String iFrameId)
    {
        this.iFrameId = iFrameId;
    }
}
