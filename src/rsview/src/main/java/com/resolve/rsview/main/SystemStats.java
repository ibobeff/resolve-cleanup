/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.lang.management.ThreadMXBean;

import javax.servlet.http.HttpServletRequest;

import com.resolve.util.JavaUtils;

public class SystemStats
{
    public static String getAddress(HttpServletRequest request)
    {
        return request.getServerName() + ":" + request.getServerPort();
    } // getAddress
    
    public static String getOSArch()
    {
        return ManagementFactory.getOperatingSystemMXBean().getArch();
    } // getArch
    
    public static String getOSName()
    {
        return ManagementFactory.getOperatingSystemMXBean().getName();
    } // getOSName
    
    public static String getOSVersion()
    {
        return ManagementFactory.getOperatingSystemMXBean().getVersion();
    } // getOSVersion
    
    public static int getOSProcessors()
    {
        return Runtime.getRuntime().availableProcessors();
    } // getOSProcessors
    
    public static String getJVMVendor()
    {
        return ManagementFactory.getRuntimeMXBean().getVmVendor();
    } // getJVMVendor
    
    public static String getJVMName()
    {
        return ManagementFactory.getRuntimeMXBean().getVmName();
    } // getJVMName
    
    public static String getJVMVersion()
    {
        return ManagementFactory.getRuntimeMXBean().getVmVersion();
    } // getJVMVersion
    
    public static String getMemoryMax()
    {
        return JavaUtils.toMb(Runtime.getRuntime().maxMemory());
    } // getMemoryMax
    
    public static String getMemoryTotal()
    {
        return JavaUtils.toMb(Runtime.getRuntime().totalMemory());
    } // getMemoryTotal
    
    public static String getMemoryFree()
    {
        return JavaUtils.toMb(Runtime.getRuntime().freeMemory());
    } // getMemoryFree
    
    public static String getMemoryFreePercentage()
    {
//	    return ((int) (Runtime.getRuntime().freeMemory() * 100 / Runtime.getRuntime().totalMemory())) + "%";
        long free = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
        return ((int) (free * 100 / Runtime.getRuntime().maxMemory())) + "%";
    } // getMemoryFreePercentage
    
    public static String getHeapMax()
    {
	    MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        return JavaUtils.toMb(heap.getMax());
    } // getHeapMax
    
    public static String getHeapTotal()
    {
	    MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        return JavaUtils.toMb(heap.getCommitted());
    } // getHeapTotal
    
    public static String getHeapFree()
    {
	    MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
	    return ""+(int)(heap.getCommitted() - heap.getUsed());
    } // getHeapFree
    
    public static String getHeapFreePercentage()
    {
	    MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
	    return ((int)((heap.getCommitted() - heap.getUsed()) * 100 / heap.getCommitted()))+"%";
    } // getHeapFreePercentage
    
    public static int getThreadPeak()
    {
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
        return thread.getPeakThreadCount();
    } // getThreadPeak
    
    public static int getThreadLive()
    {
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
        return thread.getThreadCount();
    } // getThreadLive
    
    public static int getThreadDaemon()
    {
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
        return thread.getDaemonThreadCount();
    } // getThreadDaemon
    
} // SystemStats
