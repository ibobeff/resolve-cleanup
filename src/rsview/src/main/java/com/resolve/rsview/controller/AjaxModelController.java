/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.owasp.esapi.ESAPI;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

import com.resolve.rsview.main.ActionTaskParamManager;
import com.resolve.rsview.main.ResolveMxGraph;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.WikiException;
import com.resolve.services.exception.WikiExceptionCodes;
import com.resolve.services.hibernate.menu.MenuDetails;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;

import net.sf.json.JSONObject;

/**
 * This controller serves the request for Wiki/Document Model UI. There are 3 types of Model. <p> 
 * 1) Main Model - This is the Runbook Execution model <p>
 * 2) Abort Model - This model is executed if there is an error/exception in the Main Model <p>
 * 3) Decision Tree Model - This model represents the Decision Tree <p>
 * 
 * <p> 
 * For more information, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a>.
 * <p>
 * DB Table: {@code wikidoc}</br>
 * Url : {@code http://localhost:8080/resolve/service/wiki/editmodel/Test/test1}
 * 
 * @author jeet.marwah
 *
 */

@Controller
public class AjaxModelController extends GenericController
{
	private static final String INVALID_WIKI_NAME_FORMAT = "Invalid Wiki document name format %s. Use: Namespace.Document Name (e.g., network.Health Check)";
	private static final String ERROR_SAVING_ABORT_MODEL = "Error saving Abort Model";
	private static final String ERROR_SAVING_MODEL = "Error saving model";
    private static final String ERROR_SAVING_DECISION_TREE_MODEL = "Error saving Decision Tree Model";

	public static final String FILTER_REGEX = "[^(){}$a-zA-Z0-9\\s\r\n|?.;,*/_-]";
    
    /**
     * This api is to edit the Main Model of a Runbook. If there is a model xml for a Runbook, it will be shown in the Graphical UI
     * and user will be able to edit it.
     * 
     * Url : {@code http://localhost:8080/resolve/service/wiki/editmodel/<Doc Namespace>/<Doc Name>}
     * <p>
     * eg. http://localhost:8080/resolve/service/wiki/editmodel/Test/test1
     * 
     * @param namespace Document Namespace
     * @param docname Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content  
     * @throws ServletException 
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_EDITMODEL, method = {RequestMethod.GET, RequestMethod.POST} )
    public ModelAndView editmodel(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        return getModel(namespace, docname, request, response, false, "model");
    }//editmodel
    
    /**
     * This api is to view the Main Model of a Runbook.
     * <p> 
     * Url : {@code http://localhost:8080/resolve/service/wiki/viewmodel/<Doc Namespace>/<Doc Name>}
     * <p>
     * eg. http://localhost:8080/resolve/service/wiki/viewmodel/Test/test1
     * 
     * @param namespace Document Namespace
     * @param docname Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content  
     * @throws ServletException 
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_VIEWMODEL, method = {RequestMethod.GET, RequestMethod.POST} )
    public ModelAndView viewmodel(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        return getModel(namespace, docname, request, response, true, "model");
    }

    /**
     * This is another entry point to view the Main Model of a Runbook.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/viewmodel?wiki=<Wiki Namespace>.<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/viewmodel?wiki=Test.test2}
     * <p>
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
	@RequestMapping(value = ConstantValues.URL_VIEWMODEL_BASE, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView viewmodel(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String wiki = request.getParameter(ConstantValues.WIKI_NAME_KEY);
		if (wiki == null) {
			throw new Exception("Missing Wiki document");
		}

		if (wiki.indexOf(".") > 0) {
			String[] wikiPath = wiki.split("\\.");
			return getModel(wikiPath[0], wikiPath[1], request, response, true, "model");
		} else {
			throw new Exception(String.format(INVALID_WIKI_NAME_FORMAT, wiki), null);
		}
	}

    /**
     * 
     * This api is to edit the Abort Model of a Runbook. If there is a model xml for a Runbook, it will be shown in the Graphical UI
     * and user will be able to edit it.
     * 
     * Url : {@code http://localhost:8080/resolve/service/wiki/editabort/<Doc Namespace>/<Doc Name>}
     * <p>
     * eg. http://localhost:8080/resolve/service/wiki/editabort/Test/test1
     * 
     * @param namespace Document Namespace
     * @param docname Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content   
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_EDITABORT, method = {RequestMethod.GET} )
    public ModelAndView editabort(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        return getModel(namespace, docname, request, response, false, "abort");
        
    }//editabort
    
    /**
     * @deprecated
     * 
     * This is to edit the Final
     * eg. http://localhost:8080/resolve/service/wiki/editfinal/Test/test1
     * 
     * List of key-value pairs in the return type
     *      - wikiDocument = Object of type WikiDocument
     *      - saveUrl = String - used for 'save' operation
     *      - wikiEditType = String - will have one of these values - model, final, abort
     *
     * @param namespace
     * @param docname
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_EDITFINAL, method = {RequestMethod.GET} )
    public ModelAndView editfinal(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        return getModel(namespace, docname, request, response, false, "final");
        
    }//editfinal
    
    
    /**
     * This api is to edit the Decision Tree Model of a Runbook. If there is a model xml for a Runbook, it will be shown in the Graphical UI
     * and user will be able to edit it.
     * 
     * Url : {@code http://localhost:8080/resolve/service/wiki/editdtree/<Doc Namespace>/<Doc Name>}
     * <p>
     * eg. http://localhost:8080/resolve/service/wiki/editdtree/Test/test1
     * 
     * @param namespace Document Namespace
     * @param docname Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content   
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_EDITDTREE, method = {RequestMethod.GET} )
    public ModelAndView editdtree(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        return getModel(namespace, docname, request, response, false, "dtree");
        
    }//editdtree
    
    /**
     * This api saves the Main Model 'content' (in XML) that is passed from the UI into the database.
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  content - xml representing the model
     * </pre></code>
     * <p>
     * 
     * @param namespace Document Namespace
     * @param docname Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content   
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_SAVEMODEL, method = {RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8"  )
    @ResponseBody
    public ResponseDTO savemodel(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String fullname = namespace + "." + docname;        
        String content = request.getParameter("content");
        
        try
        {
            WikiDocumentVO doc = ServiceWiki.getWikiDocWithGraphRelatedData(null, fullname, username);
            if (doc != null)
            {
                doc.setUModelProcess(content);
                doc.transformModelProcessDescToCDATA();
                
                ServiceWiki.saveWikiDoc(doc, username);
                result.setSuccess(true);
            }
            else
            {
                result.setSuccess(false).setMessage("Document " + fullname + " does not exist");
            }
        }
        catch (Exception e)
        {
			Log.log.error(ERROR_SAVING_MODEL, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_MODEL);
        }
        
        return result;    
    }//savemodel
    
    /**
     * This api saves the Abort Model 'content' (in XML) that is passed from the UI into the database.
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  content - xml representing the model
     * </pre></code>
     * <p>
     * 
     * @param namespace Document Namespace
     * @param docname Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content   
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_SAVEABORT, method = {RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8"  )
    @ResponseBody
    public ResponseDTO saveabort(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String fullname = namespace + "." + docname;        
        String content = request.getParameter("content");
        
        try
        {
            WikiDocumentVO doc = ServiceWiki.getWikiDocWithGraphRelatedData(null, fullname, username);
            doc.setUModelException(content);
            doc.transformModelExceptionDescToCDATA();
            
            ServiceWiki.saveWikiDoc(doc, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
           Log.log.error(ERROR_SAVING_ABORT_MODEL, e);
           result.setSuccess(false).setMessage(ERROR_SAVING_ABORT_MODEL);
        }
        
        return result;
        
    }//saveabort
    
    /**
     * This api saves the Decision Tree Model 'content' (in XML) that is passed from the UI into the database.
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  content - xml representing the model
     * </pre></code>
     * <p>
     * 
     * @param namespace Document Namespace
     * @param docname Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content   
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_SAVEDTREE, method = {RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8" )
    @ResponseBody
    public ResponseDTO savedtree(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace,  
                                  @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, 
                                  HttpServletRequest request,
                                  HttpServletResponse response) 
                                  throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String fullname = namespace + "." + docname;        
        String content = request.getParameter("content");
        
        
        try
        {
            WikiDocumentVO doc = ServiceWiki.getWikiDocWithGraphRelatedData(null, fullname, username);
            if (doc != null)
            {
                doc.setUDecisionTree(content);
                ServiceWiki.saveWikiDoc(doc, username);
                result.setSuccess(true);
            }
            else
            {
                result.setSuccess(false).setMessage("Document " + fullname + " does not exist");
            }
            
        }
        catch (Exception e)
        {
           Log.log.error(ERROR_SAVING_DECISION_TREE_MODEL, e);
           result.setSuccess(false).setMessage(ERROR_SAVING_DECISION_TREE_MODEL);
        }
        
        return result;    
    }//savedtree

    /**
     * @deprecated
     * 
     * This api is purely for the Model UI purpose and will be refactored  
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_GET_PARAMS_GET_CHANGECOLOR, method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json; charset=utf-8" )
    @ResponseBody
    public String changeColor(HttpServletRequest request, 
                                     HttpServletResponse response) throws ServletException, IOException
    {                  
        JSONObject jsonObject = ActionTaskParamManager.getRunbooksActionTasksStatus(request, response);        
       
        String jsonObjectStr = jsonObject.toString();
        jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
        
        if (!ESAPI.validator().isValidPrintable("Detected intrusion attempt while getting AT input parameters", 
												jsonObjectStr, jsonObjectStr.length(), false)) {
            Log.log.warn(String.format("Detected intrusion attempt while getting AT input parameters [%s]", jsonObjectStr));
        	jsonObjectStr = "{\"success\":false,\"data\":\"Error getting AT input parameters\"}";
        }
        
       return  jsonObjectStr;
    }
    
    /**
     * @deprecated
     * 
     * This api is purely for the Model UI purpose and will be refactored
     *   
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_GET_PARAMS_GET_INPUT, method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8" )
    @ResponseBody
    public String getActionParametersInput(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String,String> parameterMap = ControllerUtil.getParameterMap(request);
    
        JSONObject jsonObject = ActionTaskParamManager.getActionTaskParametersInput(parameterMap);      
        
        String jsonObjectStr = jsonObject.toString();
        jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
         
        return  jsonObjectStr;     
    }
    
    /**
     * @deprecated
     * 
     * This api is purely for the Model UI purpose and will be refactored  
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.MODEL_XML_VALIDATION, method = {RequestMethod.POST} )
    @ResponseBody
    public String modelXMLValidation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), new ArrayList()); 
        jsonObject.put(HibernateConstantsEnum.OUTPUT_RUNBOOK.getTagName(), new ArrayList()); 
        String jsonObjectStr = "";
        
        Map<String,String> parameterMap = ControllerUtil.getParameterMap(request);
        //String xml = parameterMap.get(ConstantValues.CONTENT);
        String xml = parameterMap.get("content");
        
        try
        {
            if(xml != null && !xml.equals(""))
            {
                xml = xml.trim();
                jsonObject = verifySaveModelXML(xml);
                
                if(jsonObject != null)
                {
                    jsonObjectStr = jsonObject.toString();
                    //jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
                }
                else
                {
                    jsonObjectStr = jsonObject.toString();
                }
            }
            else
            {
                jsonObjectStr = jsonObject.toString();
            }
        }
        catch(Throwable t)
        {
            Log.log.info("==== modelXMLValidation catched exception " + t);
            jsonObjectStr = jsonObject.toString();
        }
        
        //jsonObjectStr = "";
        
        if (!ESAPI.validator().isValidPrintable("Detected intrusion attempt while validating model XML", 
        										jsonObjectStr, jsonObjectStr.length(), true)) {

            Log.log.warn(String.format("Detected intrusion attempt while validating model XML [%s]", jsonObjectStr));
        	jsonObjectStr = "{\"success\":false,\"data\":\"Error validating model XML\"}";
        }
        
        jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
         
        return jsonObjectStr;
    }
    
    /**
     *
     * @deprecated
     * 
     * This api is purely for the Model UI purpose and will be refactored   
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     * @throws SAXException 
     */
    @RequestMapping(value = ConstantValues.DTREE_XML_VALIDATION, method = {RequestMethod.POST} )
    @ResponseBody
    public String dtreeXMLValidation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SAXException
    {
        JSONObject jsonObject = null;
        String jsonObjectStr = null;
        
        Map<String,String> parameterMap = ControllerUtil.getParameterMap(request);
        //String xml = parameterMap.get(ConstantValues.CONTENT);
        String xml = parameterMap.get("content");
        String user = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        if(xml != null && !xml.equals(""))
        {
            xml = xml.trim();
            jsonObject = verifyDecisionTreeXML(xml, user);
            
            if(jsonObject != null)
            {
                jsonObjectStr = jsonObject.toString();
                //jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
            }
            else
            {
                jsonObjectStr = "";
            }
        }
        else
        {
            jsonObjectStr = "";
        }
        
        if (!ESAPI.validator().isValidPrintable("Detected intrusion attempt while validating decision tree XML", 
        										jsonObjectStr, jsonObjectStr.length(), true)) {
            Log.log.warn(String.format("Detected intrusion attempt while validating decision tree XML [%s]", jsonObjectStr));
        	jsonObjectStr = "{\"success\":false,\"data\":\"Error validating decision tree XML\"}";
        }
        
        jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
         
        return jsonObjectStr;
    } //dtreeXMLValidation
    
    
    /**
     *
     * @deprecated
     * 
     * This api is purely for the Model UI purpose and will be refactored  
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_GET_PARAMS_GET_OUTPUT, method = {RequestMethod.POST} )
    @ResponseBody
    public String getActionParametersOutput(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String,String> parameterMap = ControllerUtil.getParameterMap(request);
    
        JSONObject jsonObject = ActionTaskParamManager.getActionTaskParametersOutput(parameterMap);        
        
        String jsonObjectStr = jsonObject.toString();
        jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
        
        if (!ESAPI.validator().isValidPrintable("Detected intrusion attempt while getting AT output parameters", 
        										jsonObjectStr, jsonObjectStr.length(), false)) {
            Log.log.warn(String.format("Detected intrusion attempt while getting AT output parameters [%s]", jsonObjectStr));
        	jsonObjectStr = "{\"success\":false,\"data\":\"Error getting AT output parameters\"}";
        }
        
        return  jsonObjectStr;
     
    }

    /**
     * 
     * @deprecated
     * 
     * This api is purely for the Model UI purpose and will be refactored  
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_GET_PARAMS_GET_POPUPLIST, method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String getPopupList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {   
        StringBuffer modelList = new StringBuffer();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(username))
        {
            username = request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        try
        {
            String popupType = request.getParameter("name");
            //modelList.append("myPopupList = function popupList(node, e) ");
            modelList.append("myPopupList = function(node, e) ");
            modelList.append("{ ");
            //modelList.append("alert('----tttt'); ");
            if ("getdtpopup".equalsIgnoreCase(popupType))
            {
                modelList.append(MenuDetails.getInstance().getDTMenu(username));
            }
            else
            {
                modelList.append(MenuDetails.getInstance().getPopupMenu(username));
            }
            modelList.append("} ");
       }
       catch (Exception e)
       {
            Log.log.error(e.getMessage(), e);
       }
     
       return  ESAPI.encoder().encodeForHTML(modelList.toString());
    }
    
    //private apis
    private ModelAndView getModel(String namespace, String docname, HttpServletRequest request, HttpServletResponse response, boolean viewOnly, String editType) throws ServletException, IOException
    {
        String docFullName = namespace + "." + docname;
        String problemID = (String) request.getParameter(Constants.GROOVY_BINDING_PROBLEMID);
        String modelWiki = (String) request.getParameter(Constants.WIKI);
        String status = (String) request.getParameter(Constants.STATUS);
        String zoom = (String) request.getParameter(Constants.ZOOM);
        String refresh = (String) request.getParameter(Constants.REFRESH);
        String refreshInterval = (String) request.getParameter(Constants.REFRESHINTERVAL);
        String refreshCountMax = (String) request.getParameter(Constants.REFRESHCOUNTMAX);

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(username))
        {
            username = request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        
        //by default, set the model view
        ModelAndView modelAndView = new ModelAndView("/model/model");
        docname = ControllerUtil.getDocumentNameFrom(docname);
        
        Map<String, Object> result = new HashMap<String, Object>();
        try
        {
            WikiDocumentVO wikiDocument = ServiceWiki.getWikiDoc(null, docFullName, username);
            if(wikiDocument == null)
            {
                throw new Exception("Document with name " + docFullName + " does not exist. Please create the document before adding model to it.");
            }

            String xml = "";
            String saveURL = "";

            if (wikiDocument != null && StringUtils.isNotEmpty(editType))
            {
                if (editType.equals("model"))
                {
                    wikiDocument.transformModelProcessCDATAToDesc();
//                    wikiDocument.transformModelExceptionCDATAToDesc();
                    
                    xml = wikiDocument.getUModelProcess();
//                    xml = ManipulateModelXML.transformCDATAToDesc(xml);
                    saveURL = "/resolve/service/wiki/savemodel/" + namespace + "/" + docname;
                }
//                else if (editType.equals("final"))
//                {
//                    xml = wikiDocument.getUModelFinal();
//                    saveURL = "/resolve/service/wiki/savemodel/" + namespace + "/" + docname;
//                }
                else if (editType.equals("abort"))
                {
//                    wikiDocument.transformModelProcessCDATAToDesc();
                    wikiDocument.transformModelExceptionCDATAToDesc();

                    xml = wikiDocument.getUModelException();
//                    xml = ManipulateModelXML.transformCDATAToDesc(xml);
                    saveURL = "/resolve/service/wiki/saveabort/" + namespace + "/" + docname;
                }
                else if (editType.equals("dtree"))
                {
                    modelAndView = new ModelAndView("model/dtmodel");
                    xml = wikiDocument.getUDecisionTree();
                    saveURL = "/resolve/service/wiki/savedtree/" + namespace + "/" + docname;
                }

                xml = modifyXML(xml);
                xml = xml.replace("\n", "");
                if(xml.indexOf("straight;noEdgeStyle=1") == -1)
                {
                    xml = xml.replace("straight", "straight;noEdgeStyle=1");
                }
                xml = filterDetail(xml);
            }
            
            String runbookName = namespace + "." + docname;
            result.put(ConstantValues.MODEL_TITLE, runbookName);
            result.put(Constants.GROOVY_BINDING_PROBLEMID, problemID);
           
            result.put(ConstantValues.WIKI_XML, xml.trim());
            result.put(Constants.WIKI, modelWiki);
            result.put(Constants.STATUS, status);
            result.put(Constants.ZOOM, zoom);
            result.put(Constants.REFRESH, refresh);
            result.put(Constants.REFRESHINTERVAL, refreshInterval);
            result.put(Constants.REFRESHCOUNTMAX, refreshCountMax);
           
            if (!viewOnly)
            {
                //result.put(ConstantValues.POPUP_MENU, MenuDetails.getInstance().getPopupMenu(username));
                result.put(ConstantValues.WIKI_SAVE_LINK_KEY, saveURL);
                if (!editType.equals("dtree"))
                {
                    result.put(ConstantValues.IMAGES, getResolveImageInfo());
                }
            }

            result.put(ConstantValues.MODELONLY, viewOnly ? "true" : "false");
            
        }
        catch (Exception e)
        {
            if (e instanceof WikiException)
            {
                WikiException we = (WikiException)e;
                result.put(ConstantValues.ERROR_EXCEPTION_KEY, we.getGUIMessage());
                
                if (((WikiException) e).getCode() == WikiExceptionCodes.ERROR_WIKI_DOES_NOT_EXIST)
                {
                    Log.log.warn(e.getMessage());
                }
                else
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            else
            {
                result.put(ConstantValues.ERROR_EXCEPTION_KEY, e.getMessage());
                Log.log.error(e.getMessage(), e);
            }
            modelAndView = new ModelAndView(ConstantValues.ERROR_JSP);
        }

        //forward to JSP
        modelAndView.addAllObjects(result);
        return modelAndView;
        
        
    }
    
    private String modifyXML(String contents)
    {
        String startEdgeTag = "<Edge";
        String endEdgeTag = "</Edge>";
        
        StringBuffer edgeStringBuffer = new StringBuffer();
        String edgeString = "";
        String xml = "";
        
        try
        {
            if(contents != null && !contents.equals(""))
            {
                while(contents.contains(startEdgeTag))
                {
                    edgeString = contents.substring(contents.indexOf(startEdgeTag), contents.indexOf(endEdgeTag) + endEdgeTag.length());
                    edgeStringBuffer.append(edgeString);
                    contents = contents.replace(edgeString, "");
                }
                
                int lastIndex = contents.lastIndexOf("</root>");
                
                xml = contents.substring(0, lastIndex) + 
                      ((edgeStringBuffer != null)? (edgeStringBuffer.toString()):("")) + 
                      contents.substring(lastIndex, contents.length());
                
                xml = xml.replace("xwiki", "resolve/jsp");
                
                xml = refreshTooltip(xml);
            }       
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    
        return xml;
    } //modifyXML
        
    private String refreshTooltip(String xml)
    {
        String xmlLocal = "";
        StringBuffer allActionTask = new StringBuffer();
        String actionTaskModified = "";
        String startTaskTag = "<Task";
        String endTaskTag = "</Task>";
        
        if(xml != null && !xml.equals(""))
        {
            while(xml.contains(startTaskTag))
            {
                String actionTask = xml.substring(xml.indexOf(startTaskTag), xml.indexOf(endTaskTag) + endTaskTag.length());
                
                if(actionTask.contains("merge = ANY"))
                {
                    actionTaskModified = addDashedLine(actionTask);
                }
                else
                {
                    
                    actionTaskModified = removeDashedLineIfNeeded(actionTask);
                }
                
                allActionTask.append(actionTaskModified);
                xml = xml.replace(actionTask, "");
            }
        
            int lastIndex = xml.indexOf("<Edge");
            
            if(lastIndex < 0)
            {
                lastIndex = xml.lastIndexOf("</root>");
            }
            
            xmlLocal = xml.substring(0, lastIndex);
            xmlLocal += refreshDescription(allActionTask);
            xmlLocal += xml.substring(lastIndex, xml.length());         
        }
        
        return xmlLocal;
    } //refreshTooltip
    
    private String addDashedLine(String actionTask)
    {
        String actionTaskLocal = "";
        String mxCellStr = "";
        String lastPartString = "";
        
        //add dashed=1
        int startMxCellInt = actionTask.indexOf("<mxCell");
        
        if(startMxCellInt>0)
        {
            actionTaskLocal = actionTask.substring(0, startMxCellInt);              
            
            String mxCell = actionTask.substring(startMxCellInt, actionTask.length());
            int endMxCell = mxCell.indexOf(">");
            
            if(endMxCell >0)
            {
                mxCellStr = mxCell.substring(0, endMxCell);
                
                String mxCellStrLocal = "";
                String styleStrLocal = ""; 
            
                int styleInt = mxCellStr.indexOf("style="+"\"");
                
                if(styleInt > 0)
                {
                    mxCellStrLocal = mxCellStr.substring(0, styleInt + ("style="+"\"").length());                   
                    styleStrLocal = mxCellStr.substring(styleInt + ("style="+"\"").length(), mxCellStr.length());
                    String styleStringLocal = styleStrLocal.substring(0, styleStrLocal.indexOf("\""));
                    
                    if(!styleStringLocal.contains("dashed=1"))
                    {
                        styleStringLocal += ";dashed=1";
                    }
                    
                    mxCellStrLocal += styleStringLocal;
                    mxCellStrLocal +=  styleStrLocal.substring(styleStrLocal.indexOf("\""), styleStrLocal.length());    
                }
                else
                {
                    mxCellStrLocal =  mxCellStr;
                }
                
                //update mxCellStr
                lastPartString = mxCell.substring(endMxCell, mxCell.length());
                actionTaskLocal +=  mxCellStrLocal + lastPartString;
                
            }
            else
            {
                actionTaskLocal = "";
            }
        }
        else
        {
            actionTaskLocal = "";
        }
        
        return actionTaskLocal;
    } //addDashedLine
    
    private String refreshDescription(StringBuffer allActionTask)
    {   
        String refreshedDesc = "";
        String startTaskTag = "<Task";
        String endTaskTag = "</Task>";
        
        if(allActionTask != null)
        {
            String allActionTaskXml = allActionTask.toString();
            
            if(allActionTaskXml != null && !allActionTaskXml.equals(""))
            {
                while(allActionTaskXml.contains(startTaskTag))
                {
                    String descXmlLocal = "";
                    String desc = allActionTaskXml.substring(allActionTaskXml.indexOf(startTaskTag), allActionTaskXml.indexOf(endTaskTag) + endTaskTag.length());
                    
                    if(desc != null && !desc.equals("") && desc.contains("description"))
                    {
                        String description = desc.substring(desc.indexOf("description=\"")+"description=\"".length(), desc.length());
                        description = description.substring(0, description.indexOf("\""));
                        
                        if(description != null && !description.equals("") && description.contains("#"))
                        {
                            String docName = description.substring(0, description.indexOf("#"));
                            String namespace = "";
                            
                            if(description.contains("?"))
                            {
                                namespace = description.substring(description.indexOf("#") + 1, description.indexOf("?"));
                            }
                            else
                            {
                                namespace = description.substring(description.indexOf("#") + 1, description.length());
                            }
                            
                            String atName = docName;
                            if(StringUtils.isNotEmpty(namespace))
                            {
                                atName = atName + "#" + namespace;
                            }
                            ResolveActionTaskVO task = ServiceHibernate.getActionTaskFromIdWithReferences(null, atName, "system");
                            
                            //refresh tooltip
                            String tooltip = getTooltip(task);
                            tooltip = filter(tooltip); 
                            
                            if(desc.contains("tooltip="))
                            {
                                String tooltipXml = desc.substring(desc.indexOf("tooltip=\"")+"tooltip=\"".length(), desc.length());
                                tooltipXml = tooltipXml.substring(0, tooltipXml.indexOf("\""));
                                
                                descXmlLocal = desc.replace("tooltip=\""+tooltipXml+"\"", "tooltip=\""+tooltip+"\"");
                            }
                            else
                            {
                                String newTooltip = desc.substring(0, desc.indexOf("href=")) 
                                                    + " tooltip=\"" + tooltip + "\" "
                                                    + desc.substring(desc.indexOf("href="), desc.length());
            
                                descXmlLocal = newTooltip;
                            }
                    
                            //refresh details
                            String details = getDetails(task);
                            
                            if(StringUtils.isNotEmpty(details))
                            {
                                details = details.replaceAll(FILTER_REGEX, "");
                            }
                            
                            details = StringEscapeUtils.escapeHtml(details);
                            
                            if(descXmlLocal.contains("detail="))
                            {
                                String detailsXml = descXmlLocal.substring(descXmlLocal.indexOf("detail=\"")+"detail=\"".length(), descXmlLocal.length());
                                detailsXml = detailsXml.substring(0, detailsXml.indexOf("\""));
                                if (detailsXml != null && !detailsXml.equalsIgnoreCase("null"))
                                {
                                    descXmlLocal = descXmlLocal.replace("detail=\""+detailsXml+"\"", "detail=\""+details+"\"");
                                }
                            }
                            else
                            {
                                String newDetails = descXmlLocal.substring(0, descXmlLocal.lastIndexOf("href=")) 
                                                    + " detail=\"" + details + "\" "
                                                    + descXmlLocal.substring(descXmlLocal.lastIndexOf("href="), descXmlLocal.length());
            
                                descXmlLocal = newDetails;
                            }
                            
                        }
                        else
                        {
                            descXmlLocal = desc;
                        }
                        
                        refreshedDesc += descXmlLocal;
                    }
                    
                    allActionTaskXml = allActionTaskXml.replace(desc, "");
                }
            }
        }
        
        return refreshedDesc;
    } //refreshDescription
    
    private String getTooltip(ResolveActionTaskVO task)
    {
        String tooltip = "";
        
        try
        {
            if(task != null)
                tooltip = getInputsOrOutputs(task.getResolveActionInvoc());
        }
        catch(Throwable t)
        {
            Log.log.info(t.getMessage());
        }
        
        return tooltip;
    } //getTooltip    
    
    private String getInputsOrOutputs(ResolveActionInvocVO invocation)
    {
        JSONObject jsonObject = new JSONObject();
        
        List<String> inputList = new ArrayList<String>();
        List<String> outList = new ArrayList<String>();
        
        if (invocation != null)
        {
            List<ResolveActionParameterVO> actionParams = (List<ResolveActionParameterVO>)invocation.getResolveActionParameters();
            String paramName = null;
            String description = null;
            
            if(actionParams != null )
            {
                for (ResolveActionParameterVO actionParam : actionParams)
                {
                    String uType = actionParam.getUType();
                    paramName = actionParam.getUName();
                    description = actionParam.getUDescription();
                    description = filter(description);
                    description = StringEscapeUtils.escapeHtml(description);
                    
                    if(uType == null || uType.equals(HibernateConstantsEnum.INPUT.getTagName()))
                    {
                        inputList.add(paramName + "&" + description);
                    }
                    else if(uType.equals(HibernateConstantsEnum.OUTPUT.getTagName()))
                    {
                        outList.add(paramName + "&" + description);
                    }
                }
            }
        }
        
        jsonObject.put(HibernateConstantsEnum.INPUT_DESC.getTagName(), inputList);
        jsonObject.put(HibernateConstantsEnum.OUTPUT_DESC.getTagName(), outList);
        
        String jsonObjectStr = jsonObject.toString();
        jsonObjectStr = "{\"success\":true,\"data\":"+ jsonObjectStr + "}";
        
        return jsonObjectStr;
    } //getInputsOrOutputs
    
    private String filter(String source)
    {
        if(source != null)
        {
            source = source.replace("&", "&amp;");
            source = source.replace("\"", "&quot;");
        }
        
        return source;
    } //filter    

    private String getDetails(ResolveActionTaskVO task)
    {
        String details = "";

        try
        {
            if (task != null)
            {
                details = task.getUDescription();
            }
        }
        catch (Throwable t)
        {
            Log.log.info(t.getMessage(), t);
        }

        return details;
    } //getDetails
    
    private String removeDashedLineIfNeeded(String actionTask)
    {
        String modifiedTask = "";
        
        if(actionTask != null && !actionTask.equals(""))
        {
            if(actionTask.contains(";dashed=1"))
            {
                modifiedTask = actionTask.replace(";dashed=1", "");
            }
            else
            {
                modifiedTask = actionTask;
            }
        }
        
        return modifiedTask;
    } //removeDashedLineIfNeeded
    
    private String filterDetail(String source)
    {
        if(source != null)
        {
            source = source.replace("\n", "\\n");
            source = source.replace("\r", "\\r");
            source = source.replace("'", "\\\'");
            source = source.replace("\"", "\\\"");
        }
        
        return source;
    } //filterDetail
    
    private String getResolveImageInfo()
    {   
        Map<String, List<String>> imageInfo = new HashMap<String, List<String>>();
        
        String resolveImages = "";
        StringBuffer resolveImagesDirInfo = new StringBuffer("var subDirectory = new Array(); \n");
        StringBuffer resolveImagesInfo = new StringBuffer();
        
        try
        {
            imageInfo = ResolveMxGraph.getImageFiles();
            
            if(imageInfo != null && !imageInfo.isEmpty())
            {
                for(String dir: imageInfo.keySet())
                {
                    List<String> fileNames =  imageInfo.get(dir);
                    
                    if(fileNames != null && !fileNames.isEmpty())
                    {
                        resolveImagesDirInfo.append("subDirectory.push('"+dir+"'); \n");
                        
                        resolveImagesInfo.append("imagesNode = resolveImagesPanel.root.findChild('text', '"+dir+"'); \n");
                        resolveImagesInfo.append("if(imagesNode != null) \n");
                        resolveImagesInfo.append("{ \n");
                        
                        for(String imageName: fileNames)
                        {
                            String imageNameLocal = getImageName(imageName);
                            resolveImagesInfo.append("insertImageTemplateToResolveImages(resolveImagesPanel, graph, " +
                                                        "'"+imageNameLocal+"', " +
                                                        "'/resolve/jsp/model/images/resolve/"+dir+"/"+imageName+"', false, imagesNode); \n");
                            
                        }
                        
                        resolveImagesInfo.append("} \n");
                    }
                }
            }
            
            resolveImages = resolveImagesDirInfo.toString() + 
                            "\n" +
                            "resolveImagesPanel.createSection(subDirectory); " + 
                            "\n" +
                            " var imagesNode = null; \n" + 
                            resolveImagesInfo.toString() +
                            "\n";
                            
        }
        catch(Exception exp)
        {
            Log.log.error(exp.getMessage(), exp);
        }
         
        return resolveImages;
        
    } //getResolveImageInfo
    
    private String getImageName(String imageName)
    {
        String imageNameLocal = "";
        
        if(imageName != null && !imageName.equals(""))
        {
            int index= imageName.indexOf(".");
            
            if(index<0)
            {
                imageNameLocal = imageName;
            }
            else
            {
                imageNameLocal = imageName.substring(0, index);
            }
        }
        
        return imageNameLocal;
    } //getImageName
    
    /**
     * This method verify the decision tree xml, and find all 
     * runbooks not in database.
     * 
     * @param xml
     * @return
     * @throws SAXException 
     */
    private JSONObject verifyDecisionTreeXML(String xml, String user) throws SAXException
    {
        JSONObject jsonObject = new JSONObject();
        List<String> errors = new ArrayList<String>();
        boolean blank = true;
        try
        {
            SAXReader reader = new SAXReader();
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
            //Document document = DocumentHelper.parseText(xml);
            
            Map<String, String> quickModel = new HashMap<String, String>();
            Map<String, String> unlinkedNodes = new HashMap<String, String>();
            Map<String, String> endNodes = new HashMap<String, String>();
            
            List<Node> list = document.selectNodes("/mxGraphModel/root/Start");
            if (list == null || list.size() == 0)
            {
                errors.add("Model is Missing the Root Node\nSaving will Remove any Applied Decision Tree Information");
            }
            else
            {
                blank = false;
                int rootcount = list.size();
                if (rootcount > 1)
                {
                    errors.add("Model can Only have One Root Node");
                }
                for (Node root : list)
                {
                    String id = root.valueOf("@id");
                    quickModel.put(id, "Root::Root");
                    endNodes.put(id, "Root");
                    unlinkedNodes.put(id, "Root");
                }
            }
            
            List<String> runbookNameList = new ArrayList();
            list = document.selectNodes("/mxGraphModel/root/Document");
            if (list == null || list.size() == 0)
            {
                errors.add("Model Does Not Have any Documents");
            }
            else
            {
                blank = false;
                for (Node runbook : list)
                {
                    String id = runbook.valueOf("@id");
                    endNodes.put(id, "Document");
                    unlinkedNodes.put(id, "Document");
                    String wiki = runbook.valueOf("@description");
                    if (StringUtils.isEmpty(wiki))
                    {
                        wiki = runbook.valueOf("@label");
                    }
                    
                    // remove carriage returns
                    wiki = wiki.replaceAll("\\n", " ");
                    wiki = wiki.replaceAll("\\r", " ");
                    
                    if(wiki != null && !wiki.equals(""))
                    {
                        runbookNameList.add(wiki.trim());
                    }
                    quickModel.put(id, "Document::" + wiki);
                }
                List<String> notFoundRunbook = WikiUtils.runbooksNotFound(runbookNameList);
                jsonObject.put(HibernateConstantsEnum.OUTPUT_RUNBOOK.getTagName(), notFoundRunbook);
                List<String> unableToEdit = WikiUtils.runbooksEditNotAvailable(runbookNameList, user);
                for (String runbook : unableToEdit)
                {
                    errors.add("Unable to Edit Document " + runbook);
                }
            }
            
            list = document.selectNodes("/mxGraphModel/root/Question");
            if (list == null || list.size() == 0)
            {
                errors.add("Model Does Not Have any Questions");
            }
            else
            {
                blank = false;
                for (Node question : list)
                {
                    String id = question.valueOf("@id");
                    quickModel.put(id, "Question::" + question.valueOf("@label"));
                    endNodes.put(id, "Question");
                    unlinkedNodes.put(id, "Question");
                }
            }
            
            list = document.selectNodes("/mxGraphModel/root/Edge/mxCell");
            if (list == null || list.size() == 0)
            {
                errors.add("Model is Missing All Connectors");
            }
            else
            {
                blank = false;
                for (Node edge : list)
                {
                    String srcId = edge.valueOf("@source");
                    String dstId = edge.valueOf("@target");
                    
                    String srcType = quickModel.get(srcId).split("::")[0];
                    String dstType = quickModel.get(dstId).split("::")[0];
                    if ("Root".equalsIgnoreCase(srcType) || "Document".equalsIgnoreCase(srcType))
                    {
                        if (!"Question".equalsIgnoreCase(dstType))
                        {
                            String dstLabel = quickModel.get(dstId).split("::")[1];
                            errors.add(srcType + "s Can only Connect to a Question<br>(" + dstLabel + ")");
                        }
                    }
                    unlinkedNodes.remove(srcId);
                    unlinkedNodes.remove(dstId);
                    endNodes.remove(srcId);
                }
                for (String id : unlinkedNodes.keySet())
                {
                    String[] value = quickModel.get(id).split("::");
                    String type = value[0];
                    String label = value[1];
                    errors.add("Unlinked " + type + "<br>(" + label + ")");
                    endNodes.remove(id);
                }
                for (String id : endNodes.keySet())
                {
                    String[] value = quickModel.get(id).split("::");
                    String type = value[0];
                    String label = value[1];
                    if ("Question".equalsIgnoreCase(type))
                    {
                        errors.add("Decision Tree Cannot End on a Question<br>(" + label + ")");
                    }
                    if ("Root".equalsIgnoreCase(type))
                    {
                        errors.add("Decision Tree Cannot End on a Root Node");
                    }
                }
            }
        }
        catch (DocumentException de)
        {
            Log.log.error("Failed to Parse Decision Tree Model", de);
            errors.add("Decision Tree Model is corrupt, Please Revert to an older verion or clear the model");
        }
        if (!blank)
        {
            jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), errors);
        }
        else
        {
            jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), new ArrayList<String>());
        }
        
        return jsonObject;
    } //verifyDecisionTreeXml
    
    /**
     * This method verify the save model xml, and find all 
     * action task not existed in database.
     * 
     * @param xml
     * @return
     */
    private JSONObject verifySaveModelXML(String xml) throws Exception
    {
        
        String modelXML = xml;
        String startTaskTag = "<Task";
        String endTaskTag = "</Task>";
        String startRunbookTag = "<Subprocess";
        String endRunbookTag = "</Subprocess>";
        
        List<String> actionTaskNameList = new ArrayList<String>();
        String actionTaskName = "";
        
        List<String> runbookNameList = new ArrayList<String>();
        String runbookName = "";
        
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), new ArrayList<String>()); 
        jsonObject.put(HibernateConstantsEnum.OUTPUT_RUNBOOK.getTagName(), new ArrayList<String>()); 
        
        if(modelXML != null && !modelXML.equals(""))
        {
            while(modelXML.contains(startTaskTag))
            {
                String actionTask = modelXML.substring(modelXML.indexOf(startTaskTag), modelXML.indexOf(endTaskTag) + endTaskTag.length());
                
                if(actionTask != null && !actionTask.equals(""))
                {
                    String actionTaskLocal = actionTask.trim();
                    
                    if(actionTaskLocal.contains("description=\"\""))
                    {
                        actionTaskName = actionTaskLocal.substring(actionTask.indexOf("label=\"") + "label=\"".length(), actionTask.length());
                        actionTaskName = actionTaskName.substring(0, actionTaskName.indexOf("\""));
                    }
                    else
                    {
                        actionTaskName = actionTaskLocal.substring(actionTaskLocal.indexOf("description=\"") + "description=\"".length(), actionTaskLocal.length());
//                        decodeString = java.net.URLDecoder.decode(actionTaskName, "UTF-8");
                        
                        actionTaskName = actionTaskName.substring(0, actionTaskName.indexOf("#"));
                    }
    
                    if(actionTaskName != null && !actionTaskName.equals(""))
                    {
                        actionTaskNameList.add(actionTaskName);
                    }
                    
                    modelXML = modelXML.replace(actionTask, "");
                }
            }
            
            List<String> notFoundActionTask = actionTaskNotFound(actionTaskNameList);           
            jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), notFoundActionTask); 

            while(modelXML.contains(startRunbookTag))
            {
                String runbook = modelXML.substring(modelXML.indexOf(startRunbookTag), modelXML.indexOf(endRunbookTag) + endRunbookTag.length());
                
                if(runbook != null && !runbook.equals(""))
                {
                    String runbookLocal = runbook.trim();
                    runbookName = runbookLocal.substring(runbook.indexOf("label=\"") + "label=\"".length(), runbook.length());
                    runbookName = runbookName.substring(0, runbookName.indexOf("\""));
                    runbookName = runbookName.replace("&#xa;", ""); 
                    runbookName = runbookName.trim();
                    
                    if(runbookName != null && !runbookName.equals(""))
                    {
                        runbookNameList.add(runbookName);
                    }
                    
                    modelXML = modelXML.replace(runbook, "");
                }
            }
            
            List<String> notFoundRunbook = runbookNotFound(runbookNameList);            
            jsonObject.put(HibernateConstantsEnum.OUTPUT_RUNBOOK.getTagName(), notFoundRunbook); 
            
        }
        
        return jsonObject;
    } //verifySaveModelXML
    
    private List<String> actionTaskNotFound(List<String> actionTaskList)
    {
        
        List<String> actionTaskNotFound = new ArrayList<String>();
        
        if(actionTaskList != null && !actionTaskList.isEmpty())
        {
            for(String actionTaskName: actionTaskList)
            {
                ;
                if(ServiceHibernate.getResolveActionTask(null, actionTaskName, "system") == null)
                {
                    actionTaskNotFound.add(actionTaskName);
                }
            }
        }
        
        return actionTaskNotFound;
    } //actionTaskNotFound
    
    private List<String> runbookNotFound(List<String> runbookNameList)
    {
        List<String> runbookNotFound = new ArrayList<String>();
        
        if(runbookNameList != null && !runbookNameList.isEmpty())
        {
            for(String runbookName: runbookNameList)
            {
                if(ServiceWiki.getWikiDoc(null, runbookName, "system") == null)
                {
                    runbookNotFound.add(runbookName);
                }
            }
        }
        
        return runbookNotFound;
    } //runbookNotFound
}
