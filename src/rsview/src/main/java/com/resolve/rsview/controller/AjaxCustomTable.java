/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.dto.ViewLookupTableDTO;
import com.resolve.services.ServiceCustomTable;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.form.QueryCustomForm;
import com.resolve.workflow.table.QueryCustomTable;

import net.sf.json.JSONObject;


@Controller
@Service
public class AjaxCustomTable extends GenericController
{

    @RequestMapping(value = "/lookuptable/save", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO createlookuptable(@ModelAttribute ViewLookupTableDTO record, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            result = new QueryCustomTable().saveViewLookup(record, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error adding the record", e);
            result.setSuccess(false).setMessage("Error adding the record");
        }
        return result;
    }

    @RequestMapping(value = "/lookuptable/updateorder", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO updateMetaViewLookupOrder(@RequestParam String sysIds, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            result = new QueryCustomTable().updateMetaViewLookupOrder(sysIds, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error adding the record", e);
            result.setSuccess(false).setMessage("Error adding the record");
        }
        return result;
    }

    /**
     * list of custom table for the grid
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/customtable/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<CustomTableVO> list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        ResponseDTO<CustomTableVO> result = new ResponseDTO<CustomTableVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("CustomTable");
            query.setSelectColumns("sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UModelName,UDisplayName,UType,USource,UDestination");

            result = ServiceCustomTable.getCustomTables(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving CustomTable", e);
            result.setSuccess(false).setMessage("Error listing Custom Tables difinitions");
        }

        return result;
    }

    /**
     * Gets data for a specific custom table to be edited based on sysId
     * 
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/customtable/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<CustomTableVO> get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<CustomTableVO> result = new ResponseDTO<CustomTableVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            CustomTableVO vo = ServiceCustomTable.findCustomTableWithFields(id, null, username);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving CustomTable", e);
            result.setSuccess(false).setMessage("Error retrieving CustomTable");
        }

        return result;
    }

    /**
     * custom table is deleted based on sysId/s. It will delete all the related forms with it. 
     * 
     * @param ids
     * @param deleteAll
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/customtable/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @RequestParam("all") Boolean deleteAll, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ServiceCustomTable.deleteCustomTableByIds(ids, deleteAll, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting custom table", e);
            result.setSuccess(false).setMessage("Error deleting custom table");
        }

        return result;
    }
    

    /**
     * This api is used to delete data in a specific Custom Table. 
     * 
     * @param tableName Name of the custom table
     * @param ids array of sysIds to be deleted from this custom table
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/customtable/deletedata", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteCustomTableData(@RequestParam String tableName, @RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(StringUtils.isNotEmpty(tableName) && ids != null && ids.length > 0)
            {
                Set<String> sysIds = new HashSet<String>(Arrays.asList(ids));
                
                ServiceCustomTable.deleteCustomTableData(tableName, sysIds, username);
                result.setSuccess(true);
            }
            else
            {
                throw new Exception("Tablename and sysIds to delete are mandatory.");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting CustomTable data for table " + tableName, e);
            result.setSuccess(false).setMessage("Error deleting CustomTable data for table " + tableName +".");
        }

        return result;
    }
    
    /**
     * save of the custom table
     * 
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/customtable/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty,  HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
        String json = jsonProperty.toString();
        
        //NOTE: THIS IS A PATCH. UI IS NOT USING THE 'get' API AND IS NOT GETTING THE 'CustomTableVO' OBJECT AND HENCE SENDING THE DATABASE RECORD
        //PRESENTLY, WE ARE ONLY UPDATING THE CUSTOMTABLE NAME AND HENCE THIS PATCH. WE WILL MAKE THE CHANGES WHEN WE ENHANCE THE UI TO ALTER COLUMNS
        Map<String, Object> map = new ObjectMapper().readValue(json, HashMap.class);
        String sysId = (String) map.get("sys_id");
        String name = (String) map.get("u_display_name");
        
        
//        CustomTableVO entity = new ObjectMapper().readValue(json, CustomTableVO.class);
        CustomTableVO entity = new CustomTableVO();
        entity.setId(sysId);
        entity.setUDisplayName(name);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(ResolveBusinessRuleVO.class);
//        ResolveBusinessRuleVO entity = (ResolveBusinessRuleVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            CustomTableVO vo = ServiceCustomTable.saveCustomTable(entity, username);
//          result.setSuccess(true).setData(vo);
            
            //PATCH: UI is using the getRecordData api...so use that for now...
            List<QueryFilter> filters = new ArrayList<QueryFilter>();
            filters.add(new QueryFilter("auto", sysId, "sys_id", "equals"));
            
            QueryDTO query = new QueryDTO();
            query.setStart(0);
            query.setLimit(1);
            query.setModelName("CustomTable");
            query.setType("form");
            query.setFilterItems(filters);
            
            result.setSuccess(true).setData(new QueryCustomForm().getRecordData(query).get(0));
            
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Custom Table", e);
            result.setSuccess(false).setMessage("Error saving the Custom Table");
        }

        return result;
    }
    

}
