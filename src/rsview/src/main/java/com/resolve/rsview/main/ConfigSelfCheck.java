package com.resolve.rsview.main;

import com.resolve.util.XDoc;

public class ConfigSelfCheck extends ConfigGeneral
{
	private static final long serialVersionUID = 512340348243761842L;
	
	boolean dbSCActive;
	int dbSCInterval;
	int dbSCTimeout;
	// Elacticsearch Self Check
    boolean esSCActive;
    int esSCInterval;
    int esSCRetryCount;
    int esSCRetryDelay;
    // Ping RSCONTROL
    boolean pingActive;
    int pingThreshold;
    int pingInterval;
    // LDAP-AD Self Check
    boolean ldapAdActive;
    int ldapAdInterval;
    // http / https request self check
    boolean http;
    int httpInterval;
	
	public ConfigSelfCheck(XDoc config) throws Exception
	{
		super(config);
		
		define("dbSCActive", BOOLEAN, "./SELFCHECK/DB/@ACTIVE");
		define("dbSCInterval", INTEGER, "./SELFCHECK/DB/@INTERVAL");
		define("dbSCTimeout", INTEGER, "./SELFCHECK/DB/@TIMEOUT");
		
		define("esSCActive", BOOLEAN, "./SELFCHECK/ES/@ACTIVE");
        define("esSCInterval", INTEGER, "./SELFCHECK/ES/@INTERVAL");
        define("esSCRetryCount", INTEGER, "./SELFCHECK/ES/@RETRY");
        define("esSCRetryDelay", INTEGER, "./SELFCHECK/ES/@RETRYDELAY");
        
        define("pingActive", BOOLEAN, "./SELFCHECK/PING/@ACTIVE");
        define("pingInterval", INTEGER, "./SELFCHECK/PING/@INTERVAL");
        define("pingThreshold", INTEGER, "./SELFCHECK/PING/@THRESHOLD");
        
        define("ldapAdActive", BOOLEAN, "./SELFCHECK/LDAPADSC/@ACTIVE");
        define("ldapAdInterval", INTEGER, "./SELFCHECK/LDAPADSC/@INTERVAL");
        
        define("http", BOOLEAN, "./SELFCHECK/HTTP/@ACTIVE");
        define("httpInterval", INTEGER, "./SELFCHECK/HTTP/@INTERVAL");
	}

	public boolean isDbSCActive() {
		return dbSCActive;
	}

	public void setDbSCActive(boolean dbSCActive) {
		this.dbSCActive = dbSCActive;
	}

	public int getDbSCInterval() {
		return dbSCInterval;
	}

	public void setDbSCInterval(int dbSCInterval) {
		this.dbSCInterval = dbSCInterval;
	}

	public int getDbSCTimeout() {
		return dbSCTimeout;
	}

	public void setDbSCTimeout(int dbSCTimeout) {
		this.dbSCTimeout = dbSCTimeout;
	}

	public boolean isEsSCActive() {
		return esSCActive;
	}

	public void setEsSCActive(boolean esSCActive) {
		this.esSCActive = esSCActive;
	}

	public int getEsSCInterval() {
		return esSCInterval;
	}

	public void setEsSCInterval(int esSCInterval) {
		this.esSCInterval = esSCInterval;
	}

	public int getEsSCRetryCount() {
		return esSCRetryCount;
	}

	public void setEsSCRetryCount(int esSCRetryCount) {
		this.esSCRetryCount = esSCRetryCount;
	}

	public int getEsSCRetryDelay() {
		return esSCRetryDelay;
	}

	public void setEsSCRetryDelay(int esSCRetryDelay) {
		this.esSCRetryDelay = esSCRetryDelay;
	}

	public boolean isPingActive() {
		return pingActive;
	}

	public void setPingActive(boolean pingActive) {
		this.pingActive = pingActive;
	}

	public int getPingThreshold() {
		return pingThreshold;
	}

	public void setPingThreshold(int pingThreshold) {
		this.pingThreshold = pingThreshold;
	}

	public int getPingInterval() {
		return pingInterval;
	}

	public void setPingInterval(int pingInterval) {
		this.pingInterval = pingInterval;
	}

	public boolean isLdapAdActive() {
		return ldapAdActive;
	}

	public void setLdapAdActive(boolean ldapAdActive) {
		this.ldapAdActive = ldapAdActive;
	}

	public int getLdapAdInterval() {
		return ldapAdInterval;
	}

	public void setLdapAdInterval(int ldapAdInterval) {
		this.ldapAdInterval = ldapAdInterval;
	}
	
	public boolean isHttp()
	{
		return http;
	}

	public void setHttp(boolean http)
	{
		this.http = http;
	}

	public int getHttpInterval()
	{
		return httpInterval;
	}

	public void setHttpInterval(int httpInterval)
	{
		this.httpInterval = httpInterval;
	}
}
