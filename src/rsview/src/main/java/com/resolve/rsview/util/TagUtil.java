/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.util;

import java.util.Date;

import com.resolve.search.model.Tag;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.util.GMTDate;

public class TagUtil
{

    public static Tag convertResolveTagToTag(ResolveTagVO rsTag)
    {
        Tag tag = null;

        if (rsTag != null)
        {
            tag = new Tag();
            tag.setSysId(rsTag.getSys_id());
            tag.setName(rsTag.getUName());
            tag.setDescription(rsTag.getUDescription());
            tag.setSysCreatedBy(rsTag.getSysCreatedBy());
            tag.setSysUpdatedBy(rsTag.getSysUpdatedBy());

            if (rsTag.getSysCreatedOn() != null)
                tag.setSysCreatedOn(rsTag.getSysCreatedOn().getTime());
            else
                tag.setSysCreatedOn(GMTDate.getDate().getTime());

            if (rsTag.getSysUpdatedOn() != null)
                tag.setSysUpdatedOn(rsTag.getSysUpdatedOn().getTime());
            else
                tag.setSysUpdatedOn(GMTDate.getDate().getTime());
        }

        return tag;
    }

    public static ResolveTagVO convertTagToResolveTag(Tag tag)
    {
        ResolveTagVO rsTag = null;

        if (tag != null)
        {
            rsTag = new ResolveTagVO();
            rsTag.setId(tag.getSysId());
            rsTag.setSys_id(tag.getSysId());
            rsTag.setUName(tag.getName());
            rsTag.setUDescription(tag.getDescription());
            rsTag.setSysCreatedBy(tag.getSysCreatedBy());
            rsTag.setSysUpdatedBy(tag.getSysUpdatedBy());

            if (tag.getSysCreatedOn() != null)
                rsTag.setSysCreatedOn(new Date(tag.getSysCreatedOn()));
            else
                rsTag.setSysCreatedOn(GMTDate.getDate());

            if (tag.getSysUpdatedOn() != null)
                rsTag.setSysUpdatedOn(new Date(tag.getSysUpdatedOn()));
            else
                rsTag.setSysUpdatedOn(GMTDate.getDate());

        }

        return rsTag;
    }
}
