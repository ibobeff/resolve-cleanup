/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.novell.ldap.LDAPConnection;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.ConfigMap;

public class ConfigActiveDirectory extends ConfigMap
{
    private static final long serialVersionUID = 5320079853076168923L;
    
    boolean active          = false;
    int version             = LDAPConnection.LDAP_V3;
    String ipaddress        = "";
    int port                = LDAPConnection.DEFAULT_PORT;
    boolean ssl             = false;
    List<String> baseDNList     = new ArrayList<String>();
    String defaultDomain    = "";
    String uidAttribute     = "sAMAccountName";
    String mode             = "BIND";
    String bindDN           = "";
    String bindPassword     = "";
    boolean sync            = false;
    boolean fallback        = true;
    boolean grouprequired   = false;
    
    public ConfigActiveDirectory(XDoc config)
    {
        super(config);
        
        define("active", BOOLEAN, "./AUTH/ACTIVE_DIRECTORY/@ACTIVE");
        define("version", INTEGER, "./AUTH/ACTIVE_DIRECTORY/@VERSION");
        define("ipaddress", STRING, "./AUTH/ACTIVE_DIRECTORY/@IPADDRESS");
        define("port", INTEGER, "./AUTH/ACTIVE_DIRECTORY/@PORT");
        define("ssl", BOOLEAN, "./AUTH/ACTIVE_DIRECTORY/@SSL");
        define("defaultDomain", STRING, "./AUTH/ACTIVE_DIRECTORY/@DEFAULT_DOMAIN");
        define("uidAttribute", STRING, "./AUTH/ACTIVE_DIRECTORY/@UID_ATTRIBUTE");
        define("mode", STRING, "./AUTH/ACTIVE_DIRECTORY/@MODE");
        define("bindDN", STRING, "./AUTH/ACTIVE_DIRECTORY/@BIND_DN");
        define("bindPassword", ENCRYPT, "./AUTH/ACTIVE_DIRECTORY/@BIND_PASSWORD");
        define("sync", BOOLEAN, "./AUTH/ACTIVE_DIRECTORY/@SYNC");
        define("fallback", BOOLEAN, "./AUTH/ACTIVE_DIRECTORY/@FALLBACK");
        define("grouprequired", BOOLEAN, "./AUTH/ACTIVE_DIRECTORY/@GROUPREQUIRED");

    } // ConfigId
    
    @SuppressWarnings("rawtypes")
    public void load() throws Exception
    {
        loadAttributes();
        
        List baseDNs = xdoc.getListMapValue("./AUTH/ACTIVE_DIRECTORY/BASEDN");
        
        if (baseDNs.size() > 0)
        {
            for (Iterator i = baseDNs.iterator(); i.hasNext();)
            {
                Map values = (Map) i.next();

                String dn = (String) values.get("DN");
                
                if (!StringUtils.isEmpty(dn))
                {
                    if (!baseDNList.add(dn))
                    {
                        Log.log.warn("Duplicate baseDN: " + dn);
                    }
                    else
                    {
                        Log.log.info("Added a new baseDN: " + dn);
                    }
                }
            }
        }
    } // load

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void save()
    {
        List<HashMap> baseDNs = new ArrayList<HashMap>();
        if (baseDNList != null)
        {
            for (String dn : baseDNList)
            {
                HashMap entry = new HashMap();
                entry.put("DN", dn);
                baseDNs.add(entry);
            }
            xdoc.setListMapValue("./AUTH/ACTIVE_DIRECTORY/BASEDN", baseDNs);
        }
        
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    public String getIpaddress()
    {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress)
    {
        this.ipaddress = ipaddress;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    public List<String> getBaseDNList()
    {
        return baseDNList;
    }

    public void setBaseDNList(List<String> baseDN)
    {
        this.baseDNList = baseDN;
    }

    public String getDefaultDomain()
    {
        return defaultDomain;
    }

    public void setDefaultDomain(String defaultDomain)
    {
        this.defaultDomain = defaultDomain;
    }

    public String getUidAttribute()
    {
        return uidAttribute;
    }

    public void setUidAttribute(String uidAttribute)
    {
        this.uidAttribute = uidAttribute;
    }

    public String getMode()
    {
        return mode;
    }

    public void setMode(String mode)
    {
        this.mode = mode;
    }

    public String getBindDN()
    {
        return bindDN;
    }

    public void setBindDN(String bindDN)
    {
        this.bindDN = bindDN;
    }

    public String getBindPassword()
    {
        return bindPassword;
    }

    public void setBindPassword(String bindPassword)
    {
        this.bindPassword = bindPassword;
    }

    public boolean isSync()
    {
        return sync;
    }

    public void setSync(boolean sync)
    {
        this.sync = sync;
    }
    
    public boolean isFallback()
    {
        return fallback;
    }

    public void setFallback(boolean fallback)
    {
        this.fallback = fallback;
    }

    public boolean isGrouprequired()
    {
        return grouprequired;
    }

    public void setGrouprequired(boolean grouprequired)
    {
        this.grouprequired = grouprequired;
    }
    
    

} // ConfigId
