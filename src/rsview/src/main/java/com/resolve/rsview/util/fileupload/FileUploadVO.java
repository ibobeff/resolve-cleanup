/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.util.fileupload;

public class FileUploadVO
{
    public String tableName = null;
    public String listOfColumnNames = null;
    public String listOfColumnValues = null;
    
    public byte[] content = null;
    public String fileName = null;
    public Integer size = new Integer(0);
    public String refColumnName = null;
    public String refSysId = null;
}
