/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.rsview.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.rsview.main.Main;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveBlueprintVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * A utility class for resolve gateways.
 * 
 * @author bipul.dutta
 * 
 */
public class GatewayUtil
{
    //this is a map where we store some abnormality with the
    //blueprint key (e.g., rsremote.receive.db.active) not same as the
    //sys app module (e.g., database). Normally all other existing
    //gateways are fine except database.
    public static Map<String, String> GATEWAY_EXCEPTION_MAPPING = new HashMap<String, String>();
    
    static Pattern gatewayActivePattern = Pattern.compile("(.+)[.]receive[.].+[.]active");
    
    private static String PRIMARY_PROPERTY_SUFFIX = "primary";
    private static String SECONDARY_PROPERTY_SUFFIX = "secondary";
    
    static
    {
        GATEWAY_EXCEPTION_MAPPING.put("database", "db");
    }

    public static void synchronizeEmailGateway()
    {
        Map<String, Object> messageParams = new HashMap<String, Object>();
        messageParams.put("UPDATE_SOCIAL_POSTER", "TRUE");

        // send the message back to the requester who sent this message.
        Log.log.debug("SENDING message to " + Constants.ESB_NAME_GATEWAY_TOPIC + " so Email and EWS gateway could sync up.");

        if (Main.esb.sendInternalMessage(Constants.ESB_NAME_GATEWAY_TOPIC, "MEmail.sendSyncRequest", messageParams) == false)
        {
            Log.log.warn("Failed to send synchronization message to " + Constants.ESB_NAME_GATEWAY_TOPIC + " for Email gateway.");
        }
        if (Main.esb.sendInternalMessage(Constants.ESB_NAME_GATEWAY_TOPIC, "MEWS.sendSyncRequest", messageParams) == false)
        {
            Log.log.warn("Failed to send synchronization message to " + Constants.ESB_NAME_GATEWAY_TOPIC + " for XMPP gateway, ignoring it because may be there is no RSRemotes.");
        }
    }

    public static void synchronizeXMPPGateway()
    {
        Map<String, Object> messageParams = new HashMap<String, Object>();
        messageParams.put("UPDATE_SOCIAL_POSTER", "TRUE");

        // send the message back to the requester who sent this message.
        Log.log.debug("SENDING message to " + Constants.ESB_NAME_GATEWAY_TOPIC + " so XMPP gateway could sync up.");

        if (Main.esb.sendInternalMessage(Constants.ESB_NAME_GATEWAY_TOPIC, "MXMPP.sendSyncRequest", messageParams) == false)
        {
            Log.log.warn("Failed to send synchronization message to " + Constants.ESB_NAME_GATEWAY_TOPIC + " for XMPP gateway, ignoring it because may be there is no RSRemotes.");
        }
    }

    public static Set<String> findAllActiveGateways()
    {
        // LinkedHashSet is serializable
        Set<String> gateways = new TreeSet<String>();
        List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
        for (ResolveBlueprintVO blueprintVO : blueprints)
        {
            Properties properties = new Properties();
            try
            {
                properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));
            }
            catch (IOException e)
            {
                Log.log.warn(e.getMessage(), e);
            }

            // for example we want to check property like
            // rsremote.receive.email.active=true
            for (Object key : properties.keySet())
            {
                String name = key.toString();
                Matcher gatewayActiveMatcher = gatewayActivePattern.matcher(name);
                if (gatewayActiveMatcher.matches() && !name.contains(".ldap.") && !name.contains(".ad."))
                {
                    String value = properties.getProperty(name);
                    if ("true".equalsIgnoreCase(value))
                    {
	                	String rsremoteName = gatewayActiveMatcher.group(1);
                    String[] parts = name.split("\\.");
                    if (parts.length != 4) continue;
                    String gatewayName = GATEWAY_EXCEPTION_MAPPING.containsKey(parts[2]) ? GATEWAY_EXCEPTION_MAPPING.get(parts[2]) : parts[2];
	                    
                        String queueProperty = rsremoteName + ".receive." + gatewayName + ".queue";
                        String queue = properties.getProperty(queueProperty);
                        if (!StringUtils.isBlank(queue))
                        {
                            gateways.add(queue);
                        }
                    }
                }
            }
        }
        return gateways;
    }

    public static void synchronizeRoutingSchemas(List<String> gatewayQueues) throws Exception
    {
        Map<String, String> messageParams = new HashMap<String, String>();
        messageParams.put("RESET_ROUTING_SCHEMAS", "TRUE");

        //get all the active gateways, ignore the provided gatewayQueues
        //Set<String> activeGateways = findAllActiveGateways();
        TreeMap<String,Set<String>> activeQueuesGWNameMap = getActiveQueueListMap();
        
        Set<String> gateways = activeQueuesGWNameMap.keySet();
        
        if (gateways != null && !gateways.isEmpty())
        {
            for (String gateway : gateways)
            {
                Set<String> activeQueues = activeQueuesGWNameMap.get(gateway);
                //we decided that we ask every gateway to synchronize to prevent
                //the case where gateway is silently deleted from schema.
                
                for (String gatewayQueueName : activeQueues)
                {
                    // send the message to the gateway topic, let the default
                    // processor process the method.
                    Log.log.debug("SENDING message to " + gatewayQueueName.toUpperCase() + "_TOPIC so the gateway could sync up routing schemas.");
    
                    if (Main.esb.sendInternalMessage(gatewayQueueName.toUpperCase() + "_TOPIC", "DEFAULT", "synchronizeRoutingSchemas", messageParams) == false)
                    {
                        Log.log.warn("Failed to send synchronization message to " + gatewayQueueName + " for the routing schemas, ignoring it because may be there is no RSRemotes.");
                    }
                    
                    // send the message to worker, let the default
                    // processor process the method.
                    Log.log.debug("SENDING message to " + gatewayQueueName.toUpperCase() + "_WORKER so the gateway could sync up routing schemas.");
    
                    if (Main.esb.sendInternalMessage(gatewayQueueName.toUpperCase() + "_WORKER", "DEFAULT", "synchronizeRoutingSchemas", messageParams) == false)
                    {
                        Log.log.warn("Failed to send synchronization message to " + gatewayQueueName + "  workers for the routing schemas, ignoring it because may be there is no RSRemotes.");
                    }
                }
                
            }
        }
        else
        {
            Log.log.debug("No active gateway found so the gateway could sync up routing schemas.");
        }
    }
    
    public static TreeMap<String,Set<String>> getActiveQueueListMap() throws Exception
    {
        TreeMap<String,Set<String>> activeQueuesGWNameMap = new TreeMap<String,Set<String>>();
        
        List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
        for (ResolveBlueprintVO blueprintVO : blueprints)
        {
            Properties properties = new Properties();
            
            properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));
            
            // Get rsremote instance count
            
            if (properties.containsKey("rsremote.instance.count"))
            {
                int rmtInstCnt = 0;
                
                try
                {
                    rmtInstCnt = Integer.parseInt(properties.getProperty("rsremote.instance.count", "0"));
                }
                catch(NumberFormatException nfe)
                {
                    Log.log.warn("Blueprint property rsremote.instance.count value " + 
                                 properties.getProperty("rsremote.instance.count") + 
                                 " is not a number!!!");
                }
                
                if (rmtInstCnt > 0)
                {
                    for (int i = 1; i <= rmtInstCnt; i++)
                    {
                        if (properties.containsKey("rsremote.instance" + i + ".name"))
                        {
                            String rmtInstName = properties.getProperty("rsremote.instance" + i + ".name");
                            
                            String orgSuffix = "";
                            
                            if (properties.containsKey(rmtInstName + ".general.org"))
                            {
                                String orgNamePropVal = properties.getProperty(rmtInstName + ".general.org");
                                
                                if (StringUtils.isNotBlank(orgNamePropVal))
                                {
                                    orgSuffix = orgNamePropVal.replaceAll("[^0-9A-Za-z_]", "").toUpperCase();
                                }
                            }
                            
                            //Check property names like <rsremote instance name>.receive.<gateway name starting with lower case alpha>.active
                            
                            for(Object key:properties.keySet()) 
                            {
                                String name = key.toString();
                                
                                /*
                                 *  HP TO DO Need to filter out queue names for connections pools as
                                 *  connection pools are deployed to rsremotes and are shared resource 
                                 *  used by gateways in that rsremote.
                                 */
                                
                                if (name.toString().matches(rmtInstName + "[.]receive[.][a-z].*[.]active") && 
                                    !name.contains(".ldap.") && !name.contains(".ad.") && 
                                    !name.contains(".radius.") && !name.contains(".ssh.") && 
                                    !name.contains(".telnet.")) 
                                {
                                    String[] parts = name.split("\\.");
                                    if(parts.length!=4)
                                        continue;
                                    
                                    // Verify if primary/secondary is enabled
                                    
                                    /*
                                    String primaryKeyName = parts[0] + "." + parts[1] + "." + parts[2] + "." + PRIMARY_PROPERTY_SUFFIX;
                                    String secondaryKeyName = parts[0] + "." + parts[1] + "." + parts[2] + "." + SECONDARY_PROPERTY_SUFFIX;
                                    
                                    if ((Boolean.getBoolean(properties.getProperty(primaryKeyName))) ||
                                        (Boolean.getBoolean(properties.getProperty(secondaryKeyName))))
                                    {*/
                                        String gatewayName = GatewayUtil.GATEWAY_EXCEPTION_MAPPING.containsKey(parts[2])?GatewayUtil.GATEWAY_EXCEPTION_MAPPING.get(parts[2]):parts[2];
                                        
                                        String value = properties.getProperty(name);
                                        
                                        if ("true".equalsIgnoreCase(value))
                                        {
                                            if(!activeQueuesGWNameMap.containsKey(gatewayName))
                                            {
                                                activeQueuesGWNameMap.put(gatewayName, new TreeSet<String>());
                                            }
                                            
                                            String queueProperty = parts[0] + ".receive." + gatewayName + ".queue";
                                            String queue = properties.getProperty(queueProperty);
                                            if(!StringUtils.isEmpty(queue))
                                                activeQueuesGWNameMap.get(gatewayName).add(queue + orgSuffix);
                                        }
                                    /*}*/
                                }   
                            }
                        }
                        else
                        {
                            Log.log.warn("RSRemote instance name property rsremote.instance" + i + 
                                         ".name is missing from blueprint!!!");
                        }
                    }
                }
            }    
        }
        
        return activeQueuesGWNameMap;
    }
}
