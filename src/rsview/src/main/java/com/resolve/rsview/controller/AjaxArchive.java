/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveArchiveVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@Controller
@Service
public class AjaxArchive extends GenericController
{
    
    /**
     * Get list of archived records for a particular model/table
     * 
     * @param id - String sysId of the component.
     * @param modelName - String representing model name. eg. ResolveAssess, ResolvePreprocess, etc
     * @param query - used for pagination
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, records set to the list of {@link ResolveArchiveVO} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/archive/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveArchiveVO> list(@RequestParam("id") String id, @RequestParam("modelName") String modelName, @ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        ResponseDTO<ResolveArchiveVO> result = new ResponseDTO<ResolveArchiveVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            if(StringUtils.isNotEmpty(modelName) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(username))
            {
                result = ServiceHibernate.findArchivesFor(modelName, id, query.getStart(), query.getLimit(), username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Archive for " + modelName + " with sysId " + id, e);
            result.setSuccess(false).setMessage("Error getting archive list. See log for more info.");
        }
        
        return result;
    }

}
