/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.rsview.cr.ContentRequestUtil;
import com.resolve.rsview.util.ServerInformationUtil;
import com.resolve.services.ServiceCache;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.DecisionTreeHelper;
import com.resolve.services.hibernate.wiki.DocUtils;
import com.resolve.services.hibernate.wiki.WikiDocLock;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.vo.UserSessionDetailDTO;
import com.resolve.services.vo.UserSessionInfoDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.Wiki;
import com.resolve.wiki.attachment.FileToDBUtil;

/**
 * Methods that are available through JMS for wiki actions
 * 
 * @author jeet.marwah
 *
 */
public class MWiki
{
    /**
     * 
     * API to create a wiki document
     * 
     * List of keys that this API expects:
         * MANDATORY 
             * USERNAME  
             * namespace --> namespace of the wikidocument that will be created with 
             * docname --> wiki document name
             * content --> content for the wikidocument. This will get generated based on the template
         * OPTIONAL
             * summary
             * title
             * tags
             * mainXML
             * finalXML
             * abortXML
             * readRoles
             * writeRoles
             * adminRoles
             * executeRoles
     * 
     * 
     * 
     * @param params
     */
    public void create(Map<String, Object> params)
    {
        if (params != null)
        {
            String username = (String) params.get(ConstantValues.USERNAME);
            String namespace = (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY);
            String docname = (String) params.get(ConstantValues.WIKI_DOCNAME_KEY);
            if (StringUtils.isNotBlank(username))
            {
                try
                {
                    //Make sure that user is a valid resolve user
                    validateUser(username);
                    
                    ServiceWiki.createWiki(params, username);
                }
                catch (Exception e)
                {
                    Log.log.error("Error in creating doc :" + namespace + "." + docname, e);
                }
            }
        }

    }//create

    @SuppressWarnings("unchecked")
    public void archiveDocuments(Map<String, Object> params)
    {
        if (params != null)
        {
            String userName = "system";
            List<String> docSysIds = (List<String>) params.get(ConstantValues.WIKI_DOCUMENT_SYS_IDS);
            if (params.get("UPDATED_BY") != null)
            {
                userName = (String)params.get("UPDATED_BY");
            }
            if (docSysIds != null)
            {
                try
                {
                    ServiceWiki.archiveDocuments(new HashSet<String>(docSysIds), null, userName);
                }
                catch (Exception e)
                {
                    Log.log.error("error archiving the docs", e);
                }
            }
        }
    }

    /**
     * 
     * API to create a wiki document
     * 
     * List of keys that this API expects:
         * MANDATORY 
             * USERNAME  
             * namespace --> namespace of the wikidocument that will be created with 
             * docname --> wiki document name
             * content --> content for the wikidocument. This will get generated based on the template
         * OPTIONAL
             * summary
             * title
             * tags
             * mainXML
             * finalXML
             * abortXML
             * readRoles
             * writeRoles
             * adminRoles
             * executeRoles
     * 
     * 
     * 
     * @param params
     */
    public void update(Map<String, Object> params)
    {
        if (params != null)
        {
            String username = (String) params.get(ConstantValues.USERNAME);
            String namespace = (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY);
            String docname = (String) params.get(ConstantValues.WIKI_DOCNAME_KEY);
            if (StringUtils.isNotBlank(username))
            {
                try
                {
                    //Make sure that user is a valid resolve user
                    validateUser(username);
                    
                    ServiceWiki.updateWiki(params, username);
                }
                catch (Exception e)
                {
                    Log.log.error("Error in updating doc :" + namespace + "." + docname, e);
                }
            }
        }

    }//update

    /**
     * 
     * API to reapply a wiki document decision tree
     * 
     * List of keys that this API expects:
     *    USERNAME  
     *    namespace --> namespace of the wikidocument that will be reapplied
     *    docname --> wiki document name
     */
    public void reApplyDecisionTree(Map<String, Object> params)
    {
        String namespace = (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String docname = (String) params.get(ConstantValues.WIKI_DOCNAME_KEY);
        String userName = (String) params.get(ConstantValues.USERNAME);

        String fullname = namespace + "." + docname;

        if (StringUtils.isNotBlank(userName))
        {
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(userName);

                WikiDocumentVO wikiDoc = WikiUtils.getWikiDoc(fullname);
                if (wikiDoc != null && wikiDoc.getUIsRoot())
                {
                    Set<String> sysIds = new HashSet<String>();
                    sysIds.add(wikiDoc.getSys_id());
                    
                    DecisionTreeHelper.applyDecisionTreeXML(sysIds, userName);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error in reapplying Decision Tree :" + namespace + "." + docname, e);
            }
        }
    }//reApplyDecisionTree

    /**
     * 
     * API to create a wiki document with Template
     * 
     * List of keys that this API expects:
         * MANDATORY 
             * USERNAME  
             * namespace --> namespace of the wikidocument that will be created with 
             * docname --> wiki document name
             * template_namespace 
             * template_docname
         * OPTIONAL
             * summary
             * tags
             * mainXML
             * finalXML
             * abortXML
             * readRoles
             * writeRoles
             * adminRoles
             * executeRoles
             * template_values --> map of key-value pairs that will be substituted 
     * 
     * 
     * 
     * 
     * @param params
     */
    public void createWithTemplate(Map<String, Object> params)
    {
        if (params != null)
        {
            String templateNamespace = (String) params.get(ConstantValues.WIKI_TEMPLATE_NAMESPACE_KEY);
            String templateDocumentName = (String) params.get(ConstantValues.WIKI_TEMPLATE_DOCNAME_KEY);
            String username = (String) params.get(ConstantValues.USERNAME);
            String namespace = (String) params.get(ConstantValues.WIKI_NAMESPACE_KEY);
            String docname = (String) params.get(ConstantValues.WIKI_DOCNAME_KEY);

            if (StringUtils.isBlank(templateDocumentName) || StringUtils.isBlank(templateNamespace) || StringUtils.isEmpty(username) || StringUtils.isEmpty(namespace) || StringUtils.isEmpty(docname))
            {
                Log.log.error("Cannot create the document using template as mandatory fields were not available." + params);
            }
            else
            {
                try
                {
                    //Make sure that user is a valid resolve user
                    validateUser(username);

                    ServiceHibernate.createWikiUsingTemplate(params, username);
                }
                catch (Exception e)
                {
                    Log.log.error("Error in creating doc :" + namespace + "." + docname, e);
                }
            }

        }
    }//createWithTemplate

    /**
     * API to move Wikidocuments to another namespace
     * 
     * List of 'keys' that this API looks for:
         * WIKIDOC_NEW_NAMESPACE
         * WIKIDOC_FULL_NAMES
         * OVERWRITE
         * USERNAME
     * 
     * @param params
     */
    public void moveDocumentsToNamespace(Map<String, Object> params)
    {
        String newNamespace = (String) params.get(ConstantValues.WIKIDOC_NEW_NAMESPACE);
        String username = (String) params.get(ConstantValues.USERNAME);
        String documents = (String) params.get(ConstantValues.WIKIDOC_FULL_NAMES);
        String overwriteString = params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY) == null ? null : params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY).toString();
        Boolean overwrite = false;
        if (overwriteString != null && overwriteString.equalsIgnoreCase("true"))
        {
            overwrite = true;
        }
        
        //all the doc sysIds
        Set<String> docSysIds = new HashSet<String>();
        
        if (StringUtils.isNotBlank(documents) && StringUtils.isNotBlank(newNamespace) && StringUtils.isNotBlank(username))
        {
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(username);

                //prepare the list
                String[] docArr = documents.split(",");
                for (String doc : docArr)
                {
                    if (StringUtils.isNotEmpty(doc))
                    {
                        doc = doc.trim();
                        WikiDocumentVO docVO = ServiceWiki.getWikiDoc(null, doc, username);
                        if(docVO != null)
                        {
                            docSysIds.add(docVO.getSys_id());
                        }
                        else
                        {
                            Log.log.info("Cannot move - Invalid docname <" + doc + "> via ESB.");
                        }
                    }
                }//end of for loop

                if (docSysIds.size() > 0)
                {
                    try
                    {
                        //call the api to move
                        ServiceWiki.moveRenameDocuments(new ArrayList<String>(docSysIds), newNamespace, overwrite, username);
                        Log.log.info("move Documents successful via ESB.");
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error moving " + documents + " Wikidocument/s");
                        Log.log.error("Error in JMS API: ", e);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error moving " + documents + " Wikidocument/s");
                Log.log.error("Error in JMS API: ", e);
            }
        }
        else
        {
            Log.log.error(ConstantValues.WIKIDOC_FULL_NAMES + "," + ConstantValues.WIKIDOC_NEW_NAMESPACE + ","+ ConstantValues.USERNAME +" are mandatory parameters. ");
        }
    }

    /**
     * API to move/rename Multiple Wikidocuments
     * 
     * List of 'keys' that this API looks for:
     * WIKIDOC_CHANGE_MAP - map of documents to change, keys are documents and values are new
     *    names or namespaces, can also be a string formatted like key1:value1,key2:value2...
     * OVERWRITE - whether to overwrite existing documents if the move destination already exits
     * USERNAME - username the move is being done as
     * 
     * @param params
     */
    public void moveOrRenameDocuments(Map<String, Object> params)
    {
        Log.start("Move or rename multiple Documents", "DEBUG");
        //should be a map with documents as keys, and new doc names or namespaces as values
        Object documentsToMoveObj = params.get(ConstantValues.WIKIDOC_CHANGE_MAP);

        String username = (String) params.get(ConstantValues.USERNAME);
        String overwriteString = params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY) == null ? null : params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY).toString();
        Boolean overwrite = false;
        if (overwriteString != null && overwriteString.equalsIgnoreCase("true"))
        {
            overwrite = true;
        }

        try
        {
            //Make sure that user is a valid resolve user
            validateUser(username);

            Map documentsToMove = null;
            if (documentsToMoveObj != null)
            {
                if (documentsToMoveObj instanceof Map)
                {
                    documentsToMove = (Map) documentsToMoveObj;
                }
                else if (documentsToMoveObj instanceof String)
                {
                    documentsToMove = StringUtils.stringToMap((String) documentsToMoveObj, ":", ",");
                }

                if (documentsToMove != null)
                {
                    for (Object key : documentsToMove.keySet())
                    {
                        try
                        {
                            //old doc - From doc
                            String oldDoc = key.toString();
                            if ( oldDoc.indexOf(".") == -1)
                            {
                                Log.log.error("Invalid Doc Name to Move " + oldDoc + ", must be the documents Full Name");
                                break;
                            }
                            
                            //new doc - To doc
                            String newDoc = documentsToMove.get(key).toString();
                            String newNamespace = newDoc;
                            String newDocumentName = null;
                            int splitIndex = newDoc.indexOf(".");
                            if (splitIndex > -1)
                            {
                                newNamespace = newDoc.substring(0, splitIndex);
                                newDocumentName = newDoc.substring(splitIndex + 1);
                                Log.log.info("Moving Doc "+ oldDoc +" to New Document " + newDoc);
                            }
                            else
                            {
                                Log.log.info("Moving Doc to Namespace " + newNamespace);
                            }
                            
                            if(StringUtils.isNotBlank(newDocumentName))
                            {
                                ServiceWiki.moveRenameDocument(oldDoc, newDoc, overwrite, true, username);
                            }
                            else
                            {
                                WikiDocumentVO docVO = ServiceWiki.getWikiDoc(null, oldDoc, username);
                                if(docVO != null)
                                {
                                    Set<String> docSysIds = new HashSet<String>();
                                    docSysIds.add(docVO.getSys_id());                                
                                    
                                    //move the doc to namespace
                                    ServiceWiki.moveRenameDocuments(new ArrayList<String>(docSysIds), newNamespace, overwrite, username);
                                }
                            }
                            
                            
                            Log.log.info("Moved " + oldDoc + " to " + newDoc + " via ESB.");
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Error moving " + key.toString() + " Wikidocument to " + documentsToMove.get(key));
                            Log.log.error("Error in JMS API: ", e);
                        }
                    }
                }
                else
                {
                    Log.log.error("Unknown Type Passed for " + ConstantValues.WIKIDOC_CHANGE_MAP);
                }
            }
            else
            {
                Log.log.error("No Documents Passed for " + ConstantValues.WIKIDOC_CHANGE_MAP);
            }
            Log.duration("Move or rename multiple Documents", "DEBUG");
        }
        catch (Exception e)
        {
            Log.log.error("Error in moveOrRenameDocuments :", e);
        }
    }

    /**
     * API to move a Wikidocument/Namespace to another name/namespace
     * 
     * List of 'keys' that this API looks for:
         * USERNAME
         * WIKIDOC_OLD_NAMESPACE
         * WIKIDOC_NEW_NAMESPACE
         * WIKIDOC_OLD_DOCUMENT_NAME
         * WIKIDOC_NEW_DOCUMENT_NAME
     * 
     * @param params
     */
    public void moveOrRename(Map<String, Object> params)
    {
        //gather the data from map
        String username = (String) params.get(ConstantValues.USERNAME);
        String oldNamespace = (String) params.get(ConstantValues.WIKIDOC_OLD_NAMESPACE);
        String oldDocumentName = (String) params.get(ConstantValues.WIKIDOC_OLD_DOCUMENT_NAME);
        String newNamespace = (String) params.get(ConstantValues.WIKIDOC_NEW_NAMESPACE);
        String newDocumentName = (String) params.get(ConstantValues.WIKIDOC_NEW_DOCUMENT_NAME);
        String overwriteString = params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY) == null ? null : params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY).toString();
        Boolean overwrite = false;
        if (overwriteString != null && overwriteString.equalsIgnoreCase("true"))
        {
            overwrite = true;
        }
        
        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(oldDocumentName) && StringUtils.isNotBlank(oldNamespace) && StringUtils.isNotBlank(newNamespace))
        {
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(username);

                String oldDoc = oldNamespace.trim() + "." + oldDocumentName.trim();
                if (StringUtils.isNotBlank(newDocumentName))
                {
                    String newDoc = newNamespace.trim() + "." + newDocumentName.trim();
                    ServiceWiki.moveRenameDocument(oldDoc, newDoc, overwrite, true, username);
                }
                else
                {
                    WikiDocumentVO docVO = ServiceWiki.getWikiDoc(null, oldDoc, username);
                    if (docVO != null)
                    {
                        Set<String> docSysIds = new HashSet<String>();
                        docSysIds.add(docVO.getSys_id());

                        //move the doc to namespace
                        ServiceWiki.moveRenameDocuments(new ArrayList<String>(docSysIds), newNamespace, overwrite, username);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error moving " + oldNamespace + "." + oldDocumentName + " Wikidocument to " + newNamespace + "." + newDocumentName);
                Log.log.error("Error in JMS API: ", e);
            }
        }
        else
        {
            Log.log.error(ConstantValues.WIKIDOC_OLD_NAMESPACE + "," + ConstantValues.WIKIDOC_OLD_DOCUMENT_NAME + "," + ConstantValues.WIKIDOC_NEW_NAMESPACE + ","+ ConstantValues.USERNAME +" are mandatory parameters. ");
        }
    }//moveOrRename

    //    public static void main(String[] args)
    //    {
    //
    //        Map docs = new HashMap();
    //        docs.put("CR.Workflow", "CR_COPY.Workflow");
    //        
    //        Map params = new HashMap();
    //        params.put(ConstantValues.USERNAME, "admin");
    //        params.put(ConstantValues.WIKIDOCS_REPLACE_FLAG_KEY, true);
    //        params.put(ConstantValues.WIKIDOC_CHANGE_MAP, docs);
    //        
    //        new MWiki().copyDocuments(params);
    //        
    //        
    //    }

    /**
     * API to copy Multiple Wikidocuments
     * 
     * List of 'keys' that this API looks for:
     * WIKIDOC_CHANGE_MAP - map of documents to change, keys are documents and values are new
     *    names or namespaces, can also be a string formatted like key1:value1,key2:value2...
     * OVERWRITE - whether to overwrite existing documents if the copy destination already exits
     * REPLACE - flag to just replace the content, xmls, attachment and do a commit
     * USERNAME - username the copy is being done as
     * 
     * @param params
     */
    public void copyDocuments(Map<String, Object> params)
    {
        Log.start("Copy multiple Documents", "DEBUG");
        //should be a map with documents as keys, and new doc names or namespaces as values
        Object documentsToCopyObj = params.get(ConstantValues.WIKIDOC_CHANGE_MAP);

        String username = (String) params.get(ConstantValues.USERNAME);
        String overwriteString = params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY) == null ? null : params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY).toString();
        Boolean overwrite = false;
        if (overwriteString != null && overwriteString.equalsIgnoreCase("true"))
        {
            overwrite = true;
        }

        //replace flag
        String replaceString = params.get(ConstantValues.WIKIDOCS_REPLACE_FLAG_KEY) == null ? null : params.get(ConstantValues.WIKIDOCS_REPLACE_FLAG_KEY).toString();
        Boolean replace = false;
        if (replaceString != null && replaceString.equalsIgnoreCase("true"))
        {
            replace = true;
        }

        try
        {
            //Make sure that user is a valid resolve user
            validateUser(username);

            Map documentsToCopy = null;
            if (documentsToCopyObj != null)
            {
                if (documentsToCopyObj instanceof Map)
                {
                    documentsToCopy = (Map) documentsToCopyObj;
                }
                else if (documentsToCopyObj instanceof String)
                {
                    documentsToCopy = StringUtils.stringToMap((String) documentsToCopyObj, ":", ",");
                }

                if (documentsToCopy != null)
                {
                    for (Object key : documentsToCopy.keySet())
                    {
                        try
                        {
                            //old doc - From doc
                            String oldDoc = key.toString();
                            if (oldDoc.indexOf(".") == -1)
                            {
                                Log.log.error("Invalid Doc Name to Copy " + oldDoc + ", must be the documents Full Name");
                                break;
                            }
                            
                            
                            //new doc - To doc
                            String newDoc = documentsToCopy.get(key).toString();
                            String newNamespace = newDoc;
                            String newDocumentName = null;
                            int splitIndex = newDoc.indexOf(".");
                            if (splitIndex > -1)
                            {
                                newNamespace = newDoc.substring(0, splitIndex);
                                newDocumentName = newDoc.substring(splitIndex + 1);
                                Log.log.info("Copying Doc "+ oldDoc +" to New Document " + newDoc);
                            }
                            else
                            {
                                Log.log.info("Copying Doc to Namespace " + newNamespace);
                            }
                            
                            if(StringUtils.isNotBlank(newDocumentName))
                            {
                                if(replace)
                                {
                                    ServiceWiki.replaceDocument(oldDoc, newDoc, true, username);
                                }
                                else
                                {
                                    ServiceWiki.copyDocument(oldDoc, newDoc, overwrite, true, username);
                                }
                            }
                            else
                            {
                                WikiDocumentVO docVO = ServiceWiki.getWikiDoc(null, oldDoc, username);
                                if(docVO != null)
                                {
                                    Set<String> docSysIds = new HashSet<String>();
                                    docSysIds.add(docVO.getSys_id());                                
                                    
                                    if(replace)
                                    {
                                        ServiceWiki.replaceDocuments(new ArrayList<String>(docSysIds), newNamespace, username);
                                    }
                                    else
                                    {
                                        //move the doc to namespace
                                        ServiceWiki.copyDocuments(new ArrayList<String>(docSysIds), newNamespace, overwrite, username);
                                    }
                                }
                            }

                            Log.log.info("Copied " + oldDoc + " to " + newDoc + " via ESB.");
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Error copying " + key.toString() + " Wikidocument to " + documentsToCopy.get(key));
                            Log.log.error("Error in JMS API: ", e);
                        }
                    }
                }
                else
                {
                    Log.log.error("Unknown Type Passed for " + ConstantValues.WIKIDOC_CHANGE_MAP);
                }
            }
            else
            {
                Log.log.error("No Documents Passed for " + ConstantValues.WIKIDOC_CHANGE_MAP);
            }
            Log.duration("Copy multiple Documents", "DEBUG");
        }
        catch (Exception e)
        {
            Log.log.error("Error in copyDocuments: ", e);
        }
    }

    /**
     * API to copy Multiple Wikidocuments then delete the originals
     * 
     * List of 'keys' that this API looks for:
     * WIKIDOC_CHANGE_MAP - map of documents to change, keys are documents and values are new
     *    names or namespaces, can also be a string formatted like key1:value1,key2:value2...
     * OVERWRITE - whether to overwrite existing documents if the copy destination already exits
     * REPLACE - flag to just replace the content, xmls, attachment and do a commit
     * USERNAME - username the copy is being done as
     * 
     * @param params
     */
    public void copyAndDeleteDocuments(Map<String, Object> params)
    {
        Log.start("Copy multiple Documents", "DEBUG");
        //should be a map with documents as keys, and new doc names or namespaces as values
        Object documentsToCopyObj = params.get(ConstantValues.WIKIDOC_CHANGE_MAP);

        String username = (String) params.get(ConstantValues.USERNAME);
        String overwriteString = params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY) == null ? null : params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY).toString();
        Boolean overwrite = false;
        if (overwriteString != null && overwriteString.equalsIgnoreCase("true"))
        {
            overwrite = true;
        }

        String purgeString = params.get(ConstantValues.PURGE_WIKIDOCUMENT) == null ? null : params.get(ConstantValues.PURGE_WIKIDOCUMENT).toString();
        boolean purge = false;
        if (purgeString != null && purgeString.equalsIgnoreCase("true"))
        {
            purge = true;
        }

        //replace flag
        String replaceString = params.get(ConstantValues.WIKIDOCS_REPLACE_FLAG_KEY) == null ? null : params.get(ConstantValues.WIKIDOCS_REPLACE_FLAG_KEY).toString();
        Boolean replace = false;
        if (replaceString != null && replaceString.equalsIgnoreCase("true"))
        {
            replace = true;
        }

        try
        {
            //Make sure that user is a valid resolve user
            validateUser(username);

            Map documentsToCopy = null;
            if (documentsToCopyObj != null)
            {
                if (documentsToCopyObj instanceof Map)
                {
                    documentsToCopy = (Map) documentsToCopyObj;
                }
                else if (documentsToCopyObj instanceof String)
                {
                    documentsToCopy = StringUtils.stringToMap((String) documentsToCopyObj, ":", ",");
                }

                if (documentsToCopy != null)
                {
                    for (Object key : documentsToCopy.keySet())
                    {
                        WikiDocumentVO docVO = null;
                        try
                        {
                            //old doc - From doc
                            String oldDoc = key.toString();
                            if (oldDoc.indexOf(".") == -1)
                            {
                                Log.log.error("Invalid Doc Name to Copy " + oldDoc + ", must be the documents Full Name");
                                break;
                            }
                            
                            
                            //new doc - To doc
                            String newDoc = documentsToCopy.get(key).toString();
                            String newNamespace = newDoc;
                            String newDocumentName = null;
                            int splitIndex = newDoc.indexOf(".");
                            if (splitIndex > -1)
                            {
                                newNamespace = newDoc.substring(0, splitIndex);
                                newDocumentName = newDoc.substring(splitIndex + 1);
                                Log.log.info("Copying Doc "+ oldDoc +" to New Document " + newDoc);
                            }
                            else
                            {
                                Log.log.info("Copying Doc to Namespace " + newNamespace);
                            }
                            
                            docVO = ServiceWiki.getWikiDoc(null, oldDoc, username);
                            if(docVO != null)
                            {
                              //do the copy/replace operation
                                if(StringUtils.isNotBlank(newDocumentName))
                                {
                                    if(replace)
                                    {
                                        ServiceWiki.replaceDocument(oldDoc, newDoc, true, username);
                                    }
                                    else
                                    {
                                        ServiceWiki.copyDocument(oldDoc, newDoc, overwrite, true, username);
                                    }
                                }
                                else
                                {
                                    Set<String> docSysIds = new HashSet<String>();
                                    docSysIds.add(docVO.getSys_id());

                                    if (replace)
                                    {
                                        ServiceWiki.replaceDocuments(new ArrayList<String>(docSysIds), newNamespace, username);
                                    }
                                    else
                                    {
                                        //move the doc to namespace
                                        ServiceWiki.copyDocuments(new ArrayList<String>(docSysIds), newNamespace, overwrite, username);
                                    }
                                }

                                Log.log.info("Copied " + oldDoc + " to " + newDoc + " via ESB.");
                                
                                //delete the original doc
                                Set<String> docSysIds = new HashSet<String>();
                                docSysIds.add(docVO.getSys_id());  
                                
                                if(purge)
                                {
                                    //purge the original 
                                    ServiceWiki.deleteWikiDocuments(docSysIds, username); 
                                }
                                else
                                {
                                    //mark the original as deleted
                                    ServiceWiki.updateDeleteForWiki(docSysIds, true, username); 
                                }
                                Log.log.info("Deleted " + oldDoc + " via ESB.");
                                
                            }//end of if
                            
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Error copying and deleting " + key.toString() + " Wikidocument to " + documentsToCopy.get(key));
                            Log.log.error("Error in JMS API: ", e);
                        }
                    }
                }
                else
                {
                    Log.log.error("Unknown Type Passed for " + ConstantValues.WIKIDOC_CHANGE_MAP);
                }
            }
            else
            {
                Log.log.error("No Documents Passed for " + ConstantValues.WIKIDOC_CHANGE_MAP);
            }
            Log.duration("Copied And Deleted multiple Documents", "DEBUG");
        }
        catch (Exception e)
        {
            Log.log.error("Error in copyAndDeleteDocuments: ", e);
        }
    } //copyAndDeleteDocuments

    /**
     * API to copy a Wikidocument/Namespace to another name/namespace
     * 
     * List of 'keys' that this API looks for:
         * USERNAME
         * WIKIDOC_OLD_NAMESPACE
         * WIKIDOC_NEW_NAMESPACE
         * WIKIDOC_OLD_DOCUMENT_NAME
         * WIKIDOC_NEW_DOCUMENT_NAME
     * 
     * 
     * @param params
     */
    public void copy(Map<String, Object> params)
    {
        //gather the data from map
        String username = (String) params.get(ConstantValues.USERNAME);
        String oldNamespace = (String) params.get(ConstantValues.WIKIDOC_OLD_NAMESPACE);
        String oldDocumentName = (String) params.get(ConstantValues.WIKIDOC_OLD_DOCUMENT_NAME);
        String newNamespace = (String) params.get(ConstantValues.WIKIDOC_NEW_NAMESPACE);
        String newDocumentName = (String) params.get(ConstantValues.WIKIDOC_NEW_DOCUMENT_NAME);
        String overwriteString = (String) params.get(ConstantValues.WIKIDOCS_OVERWRITE_FLAG_KEY);
        Boolean overwrite = false;
        if (overwriteString != null && overwriteString.equalsIgnoreCase("true"))
        {
            overwrite = true;
        }

        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(oldDocumentName) && StringUtils.isNotBlank(oldNamespace) && StringUtils.isNotBlank(newNamespace))
        {
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(username);

                String oldDoc = oldNamespace.trim() + "." + oldDocumentName.trim();
                if (StringUtils.isNotBlank(newDocumentName))
                {
                    String newDoc = newNamespace.trim() + "." + newDocumentName.trim();
                    ServiceWiki.copyDocument(oldDoc, newDoc, overwrite, true, username);
                }
                else
                {
                    WikiDocumentVO docVO = ServiceWiki.getWikiDoc(null, oldDoc, username);
                    if (docVO != null)
                    {
                        Set<String> docSysIds = new HashSet<String>();
                        docSysIds.add(docVO.getSys_id());

                        //move the doc to namespace
                        ServiceWiki.copyDocuments(new ArrayList<String>(docSysIds), newNamespace, overwrite, username);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error copying " + oldNamespace + "." + oldDocumentName + " Wikidocument to " + newNamespace + "." + newDocumentName);
                Log.log.error("Error in JMS API: ", e);
            }
        }
        else
        {
            Log.log.error(ConstantValues.WIKIDOC_OLD_NAMESPACE + "," + ConstantValues.WIKIDOC_OLD_DOCUMENT_NAME + "," + ConstantValues.WIKIDOC_NEW_NAMESPACE + ","+ ConstantValues.USERNAME +" are mandatory parameters. ");
        }

    }//copy

    /**
     * 
     * API to delete a Wikidocument/Namespace to another name/namespace
     * 
     * List of 'keys' that this API looks for:
         * USERNAME
         * WIKIDOC_NAMESPACE
         * WIKIDOC_DOCUMENT_NAME
         * PURGE_WIKIDOCUMENT
     * 
     * @param params
     */
    public void delete(Map<String, Object> params)
    {
        String username = (String) params.get(ConstantValues.USERNAME);
        String namespace = (String) params.get(ConstantValues.WIKIDOC_NAMESPACE);
        String documentName = (String) params.get(ConstantValues.WIKIDOC_DOCUMENT_NAME);
        boolean purge = (params.get(ConstantValues.PURGE_WIKIDOCUMENT) != null) ? (Boolean) params.get(ConstantValues.PURGE_WIKIDOCUMENT) : false;
        
        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(namespace))
        {
            
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(username);

                if(StringUtils.isNotBlank(documentName))
                {
                    String oldDoc = namespace.trim() + "." + documentName.trim();
                    WikiDocumentVO docVO = ServiceWiki.getWikiDoc(null, oldDoc, username);
                    if (docVO != null)
                    {
                        //delete the original doc
                        Set<String> docSysIds = new HashSet<String>();
                        docSysIds.add(docVO.getSys_id());
    
                        if (purge)
                        {
                            //purge the original 
                            ServiceWiki.deleteWikiDocuments(docSysIds, username);
                        }
                        else
                        {
                            //mark the original as deleted
                            ServiceWiki.updateDeleteForWiki(docSysIds, true, username);
                        }
                    }
                }
                else
                {
                    Set<String> namespaceNames = new HashSet<String>();
                    namespaceNames.add(namespace);
                    
                    //delete all the doc in this namespace
                    if (purge)
                    {
                        //purge the original 
                        ServiceWiki.deleteWikiForNamespaces(namespaceNames, username);
                    }
                    else
                    {
                        //mark the original as deleted
                        ServiceWiki.updateDeleteForNamespace(namespaceNames, true, username);
                    }
                    
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error deleting " + namespace + "." + documentName + " Wikidocument");
                Log.log.error("Error in JMS API: ", e);
            }
        }

    }//delete

    /**
    * List of 'keys' that this API looks for:
         * USERNAME
         * WIKIDOC_FULL_NAMES
         * PURGE_WIKIDOCUMENT
     * 
     * @param params
     */
    public void deleteDocuments(Map<String, Object> params)
    {
        String username = (String) params.get(ConstantValues.USERNAME);
        String docs = (String) params.get(ConstantValues.WIKIDOC_DOCUMENT_NAMES);
        if (docs == null)
        {
            // backward compatibility
            docs = (String) params.get(ConstantValues.WIKIDOC_FULL_NAMES);
        }

        boolean purge = (params.get(ConstantValues.PURGE_WIKIDOCUMENT) != null) ? (Boolean) params.get(ConstantValues.PURGE_WIKIDOCUMENT) : false;
        if (StringUtils.isNotEmpty(docs) && StringUtils.isNotBlank(username))
        {
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(username);

                String[] docArr = docs.split(",");
                Set<String> docSysIds = new HashSet<String>();
                
                //prepare the list
                for (String doc : docArr)
                {
                    if (StringUtils.isNotEmpty(doc))
                    {
                        doc = doc.trim();
                        if (doc.indexOf('.') == -1)
                        {
                            Log.log.info("Cannot delete - Invalid docname <" + doc + "> via ESB.");
                            continue;
                        }

                        //look up the doc and add it to the list
                        WikiDocumentVO docVO = ServiceWiki.getWikiDoc(null, doc, username);
                        if (docVO != null)
                        {
                            docSysIds.add(docVO.getSys_id());
                        }
                    }
                }//end of for loop
                
                //delete the docs
                if(docSysIds.size() > 0)
                {
                    try
                    {
                        if (purge)
                        {
                            //purge the original 
                            ServiceWiki.deleteWikiDocuments(docSysIds, username);
                        }
                        else
                        {
                            //mark the original as deleted
                            ServiceWiki.updateDeleteForWiki(docSysIds, true, username);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error deleting " + docs);
                        Log.log.error("Error in JMS API: ", e);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error in deleteDocuments: ", e);
            }
        }
        else
        {
            Log.log.error(ConstantValues.WIKIDOC_FULL_NAMES + ","+ ConstantValues.USERNAME +", is a mandatory parameter. ");
        }

    }//delete

    /**
     * 
     * API to delete a Wikidocument/Namespace to another name/namespace
     * 
     * List of 'keys' that this API looks for:
         * USERNAME
         * WIKIDOC_NAMESPACE
         * PURGE_WIKIDOCUMENT
     * 
     * @param params
     */
    public void deleteNamespaces(Map<String, Object> params)
    {
        String username = (String) params.get(ConstantValues.USERNAME);
        String namespaces = (String) params.get(ConstantValues.WIKIDOC_NAMESPACE);
        boolean purge = (params.get(ConstantValues.PURGE_WIKIDOCUMENT) != null) ? (Boolean) params.get(ConstantValues.PURGE_WIKIDOCUMENT) : false;
        
        if (StringUtils.isNotEmpty(namespaces) && StringUtils.isNotBlank(username))
        {
            try
            {
                //Make sure that user is a valid resolve user
                validateUser(username);

                //prepare the list
                String[] nsArr = namespaces.split(",");
                Set<String> namespaceNames = new HashSet<String>();
                for (String ns : nsArr)
                {
                    if (StringUtils.isNotEmpty(ns))
                    {
                        namespaceNames.add(ns.trim());
                    }
                }//end of for loop

                if (namespaceNames.size() > 0)
                {
                    try
                    {
                        if(purge)
                        {
                            ServiceWiki.deleteWikiForNamespaces(namespaceNames, username);
                        }
                        else
                        {
                            ServiceWiki.updateDeleteForNamespace(namespaceNames, true, username);
                        }
                        
                        Log.log.info("Deleted successful via ESB.");
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error deleting " + namespaces + " Wikidocument/s");
                        Log.log.error("Error in JMS API: ", e);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error in deleteNamespaces: ", e);
            }
        }
        else
        {
            Log.log.error(ConstantValues.WIKIDOC_NAMESPACE + " is a mandatory parameter. ");
        }

    }//delete

    /**
     * if the avg you want is 4.5, the value of 'total' will be 45 and 'count' will be 10 to make the avg as 4.5. So the formula is avg = total/count < 5 and > 0.
     * 
     * List of params expected:
     * 
     * WIKIDOC_DOCUMENT_NAME - full document name (<namespace>.<docname>)
     * INIT_TOTAL = a +ve # of type Integer
     * INIT_COUNT = a +ve # of type Integer
     * 
     * 
     * @param params
     */
    public void initRating(Map<String, Object> params)
    {
        String docFullName = params.get(ConstantValues.WIKIDOC_DOCUMENT_NAME) != null ? (String) params.get(ConstantValues.WIKIDOC_DOCUMENT_NAME) : null;
        int total = params.get("INIT_TOTAL") != null ? (Integer) params.get("INIT_TOTAL") : -1;
        int count = params.get("INIT_COUNT") != null ? (Integer) params.get("INIT_COUNT") : 1;

        if (StringUtils.isNotBlank(docFullName) && total > 0)
        {
            String username = "resolve.maint";

            try
            {
                ServiceWiki.initRating(null, docFullName, total, count, username);
            }
            catch (Exception e)
            {
                Log.log.error("Error in initRating from ESB: docFullName :" + docFullName + " , total:" + total + ", count:" + count, e);
            }
        }
    }

    /**
     * 
     * 
     * List of params expected:
     * 
     * WIKIDOC_DOCUMENT_NAME - full document name (<namespace>.<docname>)
     * RATING =  1-5 range of type Integer
     * MULTIPLIER = a +ve # of type Integer
     * 
     * 
     * @param params
     */
    public void incrementRating(Map<String, Object> params)
    {
        String docFullName = params.get(ConstantValues.WIKIDOC_DOCUMENT_NAME) != null ? (String) params.get(ConstantValues.WIKIDOC_DOCUMENT_NAME) : null;
        int rating = params.get("RATING") != null ? (Integer) params.get("RATING") : -1;
        int multiplier = params.get("MULTIPLIER") != null ? (Integer) params.get("MULTIPLIER") : 1;

        String username = "resolve.maint";
        try
        {
            ServiceWiki.incrementRating(null, docFullName, rating, multiplier, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error in initRating from ESB: docFullName :" + docFullName + " , rating:" + rating + ", multiplier:" + multiplier, e);
        }
        
    }

    /**
     * API to flush the view.vm file from the memeory which will force to reload it.
     * 
     */
    public void flushTemplateViewVM()
    {
        Wiki.TEMPLATE_VIEW_VM = null;
    }

    /**
     * Accepts the following params
     *  
     * LOCATION - full path where the files to be uploaded to the document, eg - C:/project/resolve3/dist/tmp
     * DOCUMENT_NAME - Fullname of the document , if this value is not provided, it will upload it to Global document i.e. System.Attachment
     * 
     * 
     * @param params
     */
    public void importBulkAttachments(Map<String, Object> params)
    {
        try
        {
            new FileToDBUtil(params).importAllAttachments();
        }
        catch (Throwable t)
        {
            Log.log.error("Error while importing the Bulk attachments:", t);
        }
    }

    public void cleanupResultMacroCache()
    {
        ServiceCache.cleanupWorksheetDataCache();
    }
//
//    private String replaceTemplateValues(String templateContent, Map<String, String> templateValues)
//    {
//        String content = templateContent;
//
//        if (templateValues != null && templateValues.size() > 0)
//        {
//            Iterator<String> it = templateValues.keySet().iterator();
//            while (it.hasNext())
//            {
//                String key = it.next();
//                String value = templateValues.get(key);
//                key = key.replace("[", "\\[");
//                key = key.replace("]", "\\]");
//
//                value = Matcher.quoteReplacement(value);
//                content = content.replaceAll(key, value);
//            }
//
//        }//if clause
//
//        return content;
//    }//replaceTemplateValues

    public void submitReviewRequest(Map params)
    {
        String doc = (String)params.get(ConstantValues.WIKIDOC_DOCUMENT_NAME);
        String docs = (String)params.get(ConstantValues.WIKIDOC_DOCUMENT_NAMES);
        String username = StringUtils.getParam(params, Constants.EXECUTE_USERID, "admin");
        
        try
        {
            //Make sure that user is a valid resolve user
            validateUser(username);

            if (StringUtils.isNotEmpty(doc))
            {
//            wikiservice.submitRequest(doc, GMTDate.getDefaultTimezone(), true, Constants.CR_TYPE_REVIEW, username, true);
                try
                {
                    ServiceWiki.submitRequest(null, doc, Constants.CR_TYPE_REVIEW, true, true, username);
                }
                catch (Exception e)
                {
                    Log.log.error("Error submitting request for doc " + doc, e);
                }
            }
            else if (StringUtils.isNotEmpty(docs))
            {
                String[] docArr = docs.split(",");
                for(String document : docArr)
                {
                    if(StringUtils.isNotEmpty(document))
                    {
                        document = document.trim(); 
//                    wikiservice.submitRequest(document, GMTDate.getDefaultTimezone(), true, Constants.CR_TYPE_REVIEW, username, true);
                        try
                        {
                            ServiceWiki.submitRequest(null, doc, Constants.CR_TYPE_REVIEW, true, true, username);
                        }
                        catch (Exception e)
                        {
                           Log.log.error("Error submitting request for doc " + doc, e);
                        }
                    }
                }
            }
            else
            {
                Log.log.error("Missing WIKI parameter");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in submitReviewRequest ", e);
        }
        
    } // submitReviewRequest
    
    public void submitRecertifyRequest(Map params)
    {
        params.put(Constants.CR_TYPE, Constants.CR_TYPE_RECERTIFY);
        submitContentRequest(params);
    } // submetRecertifyRequest
    
    public void submitContentRequest(Map params)
    {
        String doc = (String)params.get(ConstantValues.WIKIDOC_DOCUMENT_NAME);
        String docs = (String)params.get(ConstantValues.WIKIDOC_DOCUMENT_NAMES);
        String user = StringUtils.getParam(params, Constants.EXECUTE_USERID, "admin");
        String type = StringUtils.getParam(params, Constants.CR_TYPE);
        
        boolean groupDocuments = StringUtils.getBoolean((String)params.get(ConstantValues.GROUP_DOCUMENTS));
        
        try
        {
            //Make sure that user is a valid resolve user
            validateUser(user);

            if (StringUtils.isNotEmpty(doc))
            {
                try
                {
                    ServiceWiki.submitRequest(null, doc, Constants.CR_TYPE_REVIEW, true, true, user);
                }
                catch (Exception e)
                {
                    Log.log.error("Error submitting request for doc " + doc, e);
                }

            }
            else if (StringUtils.isNotEmpty(docs))
            {
                String[] docArr = docs.split(",");
                    
                if (groupDocuments)
                {
                    ContentRequestUtil.submitGroupRequest(docArr, type, user);
                }
            }
            else
            {
                Log.log.error("Missing WIKI parameter");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in submitContentRequest ", e);
        }
        
    } // submitRecertifyRequest
    
    /**
     * key - fullname of the document
     * value - date of type java.util.Date that will be set as expiration date, if NULL, it will be set to NULL as it indicates the doc does not expire at all
     * 
     * 
     * @param params
     */
    public void setExpirationForDocuments(Map params)
    {
        if (params != null)
        {
            Map<String, Date> expireDocs = new HashMap<String, Date>();

            //to make sure that we get date objects - rest of the datatypes will be ignored
            Iterator<String> it = params.keySet().iterator();
            while (it.hasNext())
            {
                String docFullName = it.next();
                Object date = params.get(docFullName);
                if (date instanceof Date)
                {
                    expireDocs.put(docFullName, (Date) date);
                }
                else if (date == null)
                {
                    expireDocs.put(docFullName, null);
                }
            }//end of while

            //call the api to set the expiration date
            try
            {
                ServiceWiki.updateExpirationDateForWikiDocs(expireDocs, "system");
            }
            catch (Exception e)
            {
                Log.log.error("Some error happened in update the expire date. Please check the logs.", e);
            }
        }
    }

    /**
     * key - is the namespaces for the wikidocs
     * value - date of type java.util.Date that will be set as expiration date, if NULL, it will be set to NULL as it indicates the doc does not expire at all
     * 
     * @param params
     */
    public void setExpirationForNamespaces(Map params)
    {
        if (params != null)
        {
            Map<String, Date> expireNS = new HashMap<String, Date>();

            //to make sure that we get date objects - rest of the datatypes will be ignored
            Iterator<String> it = params.keySet().iterator();
            while (it.hasNext())
            {
                String ns = it.next();
                Object date = params.get(ns);
                if (date instanceof Date)
                {
                    expireNS.put(ns, (Date) date);
                }
                else if (date == null)
                {
                    expireNS.put(ns, null);
                }
            }//end of while

            //call the api to set the expiration date
            try
            {
                ServiceWiki.updateExpirationDateForNamespace(expireNS, "system");
            }
            catch (Exception e)
            {
                Log.log.error("Some error happened in update the expire date. Please check the logs.", e);
            }
        }
    }

    /**
     * params.put(ConstantValues.WIKI_DOCUMENT_SYS_ID, uiDoc.getSys_id());
        params.put(ConstantValues.WIKIDOC_STATS_COUNTER_NAME_KEY, WikiDocStatsCounterName.EDITED);
        params.put(ConstantValues.WIKI_DOCUMENT_CONTENT, updateContent);
        params.put(ConstantValues.WIKI_DOCUMENT_MODEL, updateMain);
        params.put(ConstantValues.WIKI_DOCUMENT_EXCEPTION, updateException);
        params.put(ConstantValues.WIKI_DOCUMENT_DT, updateDTree);
        params.put(ConstantValues.USERNAME, username);
        
     * @param params
     */
    public void updateWikiRelationships(Map<String, Object> params)
    {
        try {
            ServiceWiki.updateWikiRelationships(params);
        } catch (Throwable t) {
            Log.log.error("Error while updating wiki relationships.", t);
        }
    }
    
    private void validateUser(String username) throws Exception
    {
        //Make sure that user is a valid resolve user
        if(!username.equalsIgnoreCase("resolve.maint"))
        {
            boolean isValidUser = ServiceHibernate.isValidUser(username);
            if(!isValidUser)
            {
                throw new Exception("User " + username + " does not exist");
            }
        }
    }
    
    /**
     * Removes all soft locks on all wikis provided that the user with the lock has an expired sessions
     */
    public void unlockAllSoftLocks() {
        
        //System.out.println("*************************************************");
        //System.out.println("*        TURNING OFF ALL SOFTLOCKS!             *");
        //System.out.println("*************************************************");
        
        Log.log.info("Soft-lock expired session sweep has started. Unlocking all wikis whose users have expired sessions.");
        
        // get all wikis
        Set<String> allWikis =  com.resolve.services.hibernate.util.WikiUtils.getAllWikidocSysIds();
        
        // Get all user session data
        UserSessionInfoDTO dto = ServerInformationUtil.getUserSessionInfo();
        List<UserSessionDetailDTO> userSessions = dto.getSessionDetails();
        
        // iterate through wikis and remove lock
        for(String wikiSysID : allWikis) 
        {
            String docName = com.resolve.services.hibernate.util.WikiUtils.getWikidocFullName(wikiSysID);
            WikiDocLock docLock = DocUtils.getWikiDocLockNonBlocking(docName);
            
            //System.out.println("Checking doc for docLock: " + docName + ": " + docLock);
            
            // if the wiki doc is locked...
            
            if(docLock != null) {
            
                
                String userName = docLock.getUsername();
                boolean userSessionActive = false;
                
                // check user sessions if the doc lock user has an active session
                
                for(UserSessionDetailDTO user : userSessions) {
                    //System.out.println("Checking userName: " + userName + " with: " + user.getUsername());
                    if(userName.equalsIgnoreCase(user.getUsername())) {
                        userSessionActive = true;
                        //System.out.println("Active session found for " + userName + ", ignoring...");
                        break;
                    }
                }
                
                // if the user session is inactive, unlock the wiki
                if(!userSessionActive) {
                    Log.log.info(userName + "'s session is inactive. Removing their lock on the following doc: " + docName);
                    //System.out.println(docName + "'s session is inactive. Unlocking the following docName: " + docName);
                    DocUtils.unlockByAdmin(docName, "admin");
                }
            }
            
        }
    }
}//MWiki
