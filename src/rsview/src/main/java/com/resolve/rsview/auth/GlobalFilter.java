package com.resolve.rsview.auth;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHeaders;

public class GlobalFilter implements Filter
{

    static final String X_Frame_Options = "X-Frame-Options";
    static final String Content_Security_Policy = "Content-Security-Policy";
    static final String X_FRAME_OPTIONS_ALLOW_FROM = "ALLOW-FROM";
    static final String X_XSS_PROTECTTION = "X-XSS-Protection";
    static final String X_XSS_PROTECTTION_ENABLED_MODE_BLOCK = "1; mode=block ";
    static final String X_CONTENT_TYPE_OPTIONS = "X-Content-Type-Options";
    static final String X_CONTENT_TYPE_OPTIONS_NOSNIFF = "nosniff";
    static final String HTTP_REQUEST_IFRAMEID = "IFRAMEID";
    static final String HTTP_REQUEST_SSOTYPE = "SSOTYPE";
    static final String RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX = "RESOLVE_PARAM_AS_HEADER_";
    static final String COMMA_DELIMITER = ",";
    static final String QUERY_PARAMETER_SEPARATOR = "&";
    static final String QUERY_PARAMETER_EQUAL_TO = "=";
    static final String QUERY_QUESTION_MARK = "?";
    static final String MULTIPLE_PARAM_VALUES_DELIMITER = "|";
    
    private Properties properties = new Properties();

    @Override
    public void destroy()
    {
        // TODO Auto-generated method stub
    }
        
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest)req; 
        HttpServletResponse response = (HttpServletResponse) resp; 
        
        String uri = request.getRequestURI();
        
//        System.out.println("uri: " + uri);
        
        String iframeId = request.getParameter(HTTP_REQUEST_IFRAMEID);
        
        if (iframeId == null || iframeId.isEmpty())
        {
            iframeId = request.getParameter(HTTP_REQUEST_IFRAMEID.toLowerCase());
        }
        
        String ssoType = request.getParameter(HTTP_REQUEST_SSOTYPE);
        
        if (ssoType == null || ssoType.isEmpty())
        {
            ssoType = request.getParameter(HTTP_REQUEST_SSOTYPE.toLowerCase());
        }
        
//        System.out.println("iframeId: " + iframeId);
//        System.out.println("ssoType: " + ssoType);
        
        /*
         *  Set security headers for URIs which are NOT "/" (root), 
         *  nor "/resolve" and which are of length > 0 and response 
         *  for which has not been committed yet. 
         */
        
        if (!uri.equals("/") && !uri.startsWith("/resolve") && 
            uri.length() > 0 &&
            (iframeId == null || iframeId.trim().length() == 0) &&
            (ssoType == null || ssoType.trim().length() == 0) &&
            !response.isCommitted())
        {
//            System.out.println("uri: " + uri + 
//                               " is not / or does not starts with /resolve and iframeId and ssoType are not set.");
//            
            /*
             * X-Frame-Options set to value from globafilter.properties property 
             * with same name and is set to SAMEORIGIN
             */
            
            String xOpt = properties.getProperty(X_Frame_Options);
            if (xOpt != null && xOpt.length() > 0)
            {
                response.setHeader(X_Frame_Options, xOpt);
            }
            
            /*
             *  Content-Security-Policy set to value from globafilter.properties property 
             *  and is set to frame-ancestors 'self'
             */
            
            xOpt = properties.getProperty(Content_Security_Policy);
            if (xOpt != null && xOpt.length() > 0)
            {
                if (request.isSecure())
                {
                    xOpt = xOpt + "; upgrade-insecure-requests";
                }
                //response.setHeader(Content_Security_Policy, xOpt);
            }
            else
            {
                if (request.isSecure())
                {
                    xOpt = "upgrade-insecure-requests";
                }
            }
            
            if (xOpt != null && xOpt.length() > 0)
            {
                response.setHeader(Content_Security_Policy, xOpt);
            }
            
            // X-XSS-Protection enabled and mode is set to block
            
            response.setHeader(X_XSS_PROTECTTION, X_XSS_PROTECTTION_ENABLED_MODE_BLOCK);
            
            // X-Content-Type-Options is set to nosniff
            
            response.setHeader(X_CONTENT_TYPE_OPTIONS, X_CONTENT_TYPE_OPTIONS_NOSNIFF);
        }
        
        if (uri.equals("/") && ((iframeId != null && iframeId.trim().length() > 0) || 
                                (ssoType != null && ssoType.trim().length() > 0)))
        {
//            System.out.println("uri: " + uri + 
//                               " is / and one of the iframeId or ssoType is set. Redirectng to /resolve/service/login with iframeId or ssoType");
            
            String redirectURIStr = "/resolve/service/login?" + 
                                    ((iframeId != null && iframeId.trim().length() > 0) ? 
                                     "IFRAMEID=" + iframeId.toLowerCase() : 
                                     "SSOTYPE=" + ssoType.toLowerCase());
            
            String redirectURIWithHdrsAsParamsStr = addHeadersAsParams(redirectURIStr, request);
            
//            System.out.println("Redirect URI With Headers As Params: " + redirectURIWithHdrsAsParamsStr);
            
            response.sendRedirect(redirectURIWithHdrsAsParamsStr);
        }
        else
        {
//            System.out.println("uri: " + uri + 
//                               " is not / or is / but iframeId and ssoType are not set.");
            
            HttpServletRequestWrapper requestWrapper = new FixRefererHttpServletRequest(request);
                        
            if (uri.equals("/") && !response.isCommitted())
            {
                /*
                 * X-Frame-Options set to value from globafilter.properties property 
                 * with same name and is set to SAMEORIGIN
                 */
                
                String xOpt = properties.getProperty(X_Frame_Options);
                if (xOpt != null && xOpt.length() > 0)
                {
                    response.addHeader(X_Frame_Options, xOpt);
                }
                
                /*
                 *  Content-Security-Policy set to value from globafilter.properties property 
                 *  and is set to frame-ancestors 'self'
                 */
                
                xOpt = properties.getProperty(Content_Security_Policy);
                if (xOpt != null && xOpt.length() > 0)
                {
                    if (request.isSecure())
                    {
                        xOpt = xOpt + "; upgrade-insecure-requests";
                    }
                    //response.setHeader(Content_Security_Policy, xOpt);
                }
                else
                {
                    if (request.isSecure())
                    {
                        xOpt = "upgrade-insecure-requests";
                    }
                }
                
                if (xOpt != null && xOpt.length() > 0)
                {
                    response.addHeader(Content_Security_Policy, xOpt);
                }
                
                // X-XSS-Protection enabled and mode is set to block
                
                response.setHeader(X_XSS_PROTECTTION, X_XSS_PROTECTTION_ENABLED_MODE_BLOCK);
                
                // X-Content-Type-Options is set to nosniff
                
                response.setHeader(X_CONTENT_TYPE_OPTIONS, X_CONTENT_TYPE_OPTIONS_NOSNIFF);
            }
            
            chain.doFilter(requestWrapper, response);
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("globalfilter.properties");
        try
        {
            properties.load(input);
        }    
        catch (Exception ex)
        {
            //ignore
        }
    }
    
    public String addHeadersAsParams(String urlStr, HttpServletRequest request)
    {
        String urlStrWithHdrsAsParams = urlStr;
        
        if (urlStr != null && !urlStr.isEmpty())
        {
            Enumeration<String> headerNames = request.getHeaderNames();
            Map<String, String[]> requestParamMap = request.getParameterMap();
            
            if (headerNames.hasMoreElements() || (requestParamMap != null && !requestParamMap.isEmpty()))
            {
                String query = "" + urlStr;
                StringBuilder queryStringBldr = new StringBuilder(query);
                
                Map<String, String[]> requestHeadersMap = new HashMap<String, String[]>();
                
                if (headerNames.hasMoreElements())
                {
                    while (headerNames.hasMoreElements())
                    {
                        String headerName = headerNames.nextElement();
                        Enumeration<String> headerValues = request.getHeaders(headerName);
                        
                        if (headerValues.hasMoreElements())
                        {
                            List<String> headerNameVals = new ArrayList<String>();
                            
                            while (headerValues.hasMoreElements())
                            {
                                String headerValue = headerValues.nextElement();
                                
                                headerNameVals.add(headerValue);
                                
                                if (queryStringBldr.indexOf(QUERY_PARAMETER_EQUAL_TO) == -1)
                                {
                                    queryStringBldr.append(QUERY_QUESTION_MARK);
                                }
                                else
                                {
                                    queryStringBldr.append(QUERY_PARAMETER_SEPARATOR);
                                }
                                
                                try
                                {
                                    queryStringBldr.append(headerName).append(QUERY_PARAMETER_EQUAL_TO).append(URLEncoder.encode(headerValue, StandardCharsets.UTF_8.name()));
                                }
                                catch (UnsupportedEncodingException e)
                                {
                                    System.out.println("Warning: Error " + e.getLocalizedMessage() + " occurred while encoding request header value for header named " + 
                                                       headerName + ", adding un-encoded name=value parameter entry in the URL!!!");
                                    queryStringBldr.append(headerName).append(QUERY_PARAMETER_EQUAL_TO).append(headerValue);
                                }
                            }
                            
                            String[] headerNameValsArray = new String[headerNameVals.size()];
                            requestHeadersMap.put(headerName, headerNameVals.toArray(headerNameValsArray));
                        }
                    }
                }
                
                /*
                 *  Add original URL params except HTTP_REQUEST_IFRAMEID (upper or lower case) and  
                 *  HTTP_REQUEST_SSOTYPE (upper or lower case)
                 */
                
                if (requestParamMap != null && !requestParamMap.isEmpty())
                {
                    for (String requestParamName : requestParamMap.keySet())
                    {
                        String normlizedReqParamName = requestParamName.startsWith(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX) ?
                                                       requestParamName.substring(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.length()) :
                                                       requestParamName;
                        
                        if (!requestHeadersMap.containsKey(normlizedReqParamName))
                        {
                            String[] paramValues = requestParamMap.containsKey(normlizedReqParamName) ? 
                                                   requestParamMap.get(normlizedReqParamName) : requestParamMap.get(requestParamName);
                                                   
                            for (int i = 0; i < paramValues.length; i++)
                            {
                                String paramValue = paramValues[i];
                                                
                                try
                                {
                                    if (queryStringBldr.indexOf(QUERY_PARAMETER_EQUAL_TO) == -1)
                                    {
                                        queryStringBldr.append(QUERY_QUESTION_MARK);
                                    }
                                    else
                                    {
                                        queryStringBldr.append(QUERY_PARAMETER_SEPARATOR);
                                    }
                                    
                                    queryStringBldr.append(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX + normlizedReqParamName)
                                                   .append(QUERY_PARAMETER_EQUAL_TO).append(URLEncoder.encode(paramValue, StandardCharsets.UTF_8.name()));
                                }
                                catch (UnsupportedEncodingException e)
                                {
                                    System.out.println("Warning: Error " + e.getLocalizedMessage() + " occurred while encoding request parameter value for parameter named " + 
                                                       requestParamName + 
                                                       (requestParamName.startsWith(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX) ?
                                                        "(" + normlizedReqParamName + ")" : "") + 
                                                       ", adding un-encoded name=value parameter entry in the URL!!!");
                                }
                            }
                        }
                    }
                }
                
                urlStrWithHdrsAsParams = queryStringBldr.toString();
            }
        }
        
        return urlStrWithHdrsAsParams;
    }
    
    public class FixRefererHttpServletRequest extends HttpServletRequestWrapper
    {
        Map<String, String[]> requestParamMap = new HashMap<String, String[]>();
        
        public FixRefererHttpServletRequest(HttpServletRequest request)
        {
            super(request);
            requestParamMap = request.getParameterMap();
        }
        
        @Override
        public String getHeader(String name) 
        {
            if (name.equals(HttpHeaders.REFERER) && 
                (super.getHeader(name) == null || super.getHeader(name).trim().length() == 0))
            {
                String iframeId = null;
                String ssoType = null;
                
                if (requestParamMap != null && !requestParamMap.isEmpty())
                {
                    if (requestParamMap.containsKey(HTTP_REQUEST_IFRAMEID))
                    {
                        iframeId = requestParamMap.get(HTTP_REQUEST_IFRAMEID)[0];
                    }
                    
                    if (iframeId == null || iframeId.isEmpty())
                    {
                        if (requestParamMap.containsKey(HTTP_REQUEST_IFRAMEID.toLowerCase()))
                        {
                            iframeId = requestParamMap.get(HTTP_REQUEST_IFRAMEID.toLowerCase())[0];
                        }
                    }
                    
                    if (requestParamMap.containsKey(HTTP_REQUEST_IFRAMEID))
                    {
                        ssoType = requestParamMap.get(HTTP_REQUEST_SSOTYPE)[0];
                    }
                    
                    if (ssoType == null || ssoType.isEmpty())
                    {
                        if (requestParamMap.containsKey(HTTP_REQUEST_SSOTYPE.toLowerCase()))
                        {
                            ssoType = requestParamMap.get(HTTP_REQUEST_SSOTYPE.toLowerCase())[0];
                        }
                    }
                }
                
                if ((iframeId != null && iframeId.trim().length() > 0) || 
                    (ssoType != null && ssoType.trim().length() > 0))
                {
                    return super.getHeader(HttpHeaders.HOST);
                }
                else
                {
                    return super.getRequestURL().toString();
                }
            }
            else
            {
                String header = super.getHeader(name);
                
                if (header == null || header.isEmpty())
                {
                    if (requestParamMap.containsKey(name))
                    {
                        try
                        {
                            header = URLDecoder.decode(requestParamMap.get(name)[0], StandardCharsets.UTF_8.name());
                        }
                        catch (UnsupportedEncodingException e)
                        {
                            System.out.println("Warning: Error " + e.getLocalizedMessage() + " occurred while decoding request parameter named " + 
                                               name + ", returning enocoded value for getHeader(" + name + ")!!!");
                            header = requestParamMap.get(name)[0];
                        }
                    }
                    
                    if ((header == null || header.isEmpty()) &&
                        name.startsWith(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX))
                    {
                        String nameWithoutPrefix = name.substring(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.length());
                        
                        header = super.getHeader(nameWithoutPrefix);
                         
                        if (header == null || header.isEmpty())
                        {
                            if (requestParamMap.containsKey(nameWithoutPrefix))
                            {
                                try
                                {
                                    header = URLDecoder.decode(requestParamMap.get(nameWithoutPrefix)[0], StandardCharsets.UTF_8.name());
                                }
                                catch (UnsupportedEncodingException e)
                                {
                                    System.out.println("Warning: Error " + e.getLocalizedMessage() + " occurred while decoding request parameter named " + 
                                                       nameWithoutPrefix + ", returning enocoded value for getHeader(" + name + ")!!!");
                                    header = requestParamMap.get(nameWithoutPrefix)[0];
                                }
                            }
                        }
                    }
                }
                                
                return header;
            }
        }
        
        @SuppressWarnings({ "unchecked", "rawtypes" })
        @Override 
        public Enumeration getHeaders(String name)
        {
            Enumeration headers = super.getHeaders(name);
            
            headers = super.getHeaders(name);
            
            if (headers == null || !headers.hasMoreElements())
            {
                if (requestParamMap.containsKey(name))
                {
                    StringBuilder sb = new StringBuilder();
                    
                    for (int i = 0; i < (requestParamMap.get(name)).length; i++)
                    {
                        if (sb.length() > 0)
                        {
                            sb.append(MULTIPLE_PARAM_VALUES_DELIMITER);
                        }
                        
                        sb.append(requestParamMap.get(name)[i]);
                    }
                    
                    headers = new StringTokenizer(sb.toString(), MULTIPLE_PARAM_VALUES_DELIMITER);
                }
                
                if ((headers == null || !headers.hasMoreElements()) &&
                    name.startsWith(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX))
                {
                    String nameWithoutPrefix = name.substring(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.length());
                    
                    headers = super.getHeaders(nameWithoutPrefix);
                    
                    if (headers == null || !headers.hasMoreElements())
                    {
                        if (requestParamMap.containsKey(nameWithoutPrefix))
                        {
                            if (requestParamMap.get(nameWithoutPrefix) != null && (requestParamMap.get(nameWithoutPrefix)).length >= 1)
                            {
                                StringBuilder sb = new StringBuilder();
                                
                                for (int i = 0; i < (requestParamMap.get(nameWithoutPrefix)).length; i++)
                                {
                                    if (sb.length() > 0)
                                    {
                                        sb.append(MULTIPLE_PARAM_VALUES_DELIMITER);
                                    }
                                    
                                    sb.append(requestParamMap.get(nameWithoutPrefix)[i]);
                                }
                                
                                headers = new StringTokenizer(sb.toString(), MULTIPLE_PARAM_VALUES_DELIMITER);
                            }                            
                        }
                    }
                }
            }
            
            return headers;
        }
        
        @SuppressWarnings({ "unchecked", "rawtypes" })
        @Override
        public Enumeration getHeaderNames()
        {
            StringBuilder sb = new StringBuilder();
            
            Enumeration<String> baseHeaderNames = super.getHeaderNames();
            
            while (baseHeaderNames.hasMoreElements())
            {
                if (sb.length() > 0)
                {
                    sb.append(COMMA_DELIMITER);
                }
                
                sb.append(baseHeaderNames.nextElement());
            }
            
            if (requestParamMap != null && !requestParamMap.isEmpty())
            {
                for (String requestParamName : requestParamMap.keySet())
                {
                    if (sb.length() > 0)
                    {
                        sb.append(COMMA_DELIMITER);
                    }
                    
                    try
                    {
                        sb.append(URLEncoder.encode(requestParamName.startsWith(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX) ? 
                                                    requestParamName : 
                                                    RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX + requestParamName, 
                                                    StandardCharsets.UTF_8.name()));
                    }
                    catch (UnsupportedEncodingException e)
                    {
                        System.out.println("Warning: Error " + e.getLocalizedMessage() + " occurred while encoding request parameter name " + 
                                           RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX + requestParamName + ", adding decoded value to list of header names returned!!!");
                        sb.append(RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX + requestParamName);
                    }
                }
            }
            
            return new StringTokenizer(sb.toString(), COMMA_DELIMITER);
        }
    }
}
