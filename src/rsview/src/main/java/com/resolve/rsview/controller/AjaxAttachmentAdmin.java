/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.attachment.FileUploadUtil;
import com.resolve.wiki.attachment.WikiFileUpload;
import com.resolve.wiki.dto.AttachmentDTO;

/**
 * A controller to handle CRUD operation of Wiki Attachments. It also provides an API to index an attachment.
 * 
 */
@Controller
public class AjaxAttachmentAdmin extends GenericController
{
    /**
     * Upload one or more attachments to a WikiDoc. This API is called multiple times in case of more than one attachment.
     * 
     * @param request {@link HttpServletRequest}. A payload for the attachment.
     * <br>
     * Either 'docSysId' or 'docFullName' must be available as a part of submit form for a successful upload 
     * @return Returns WikiAttachmentVO
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/attachment/upload", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO upload(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try 
        {
                WikiAttachmentVO attachment = new WikiFileUpload(request, username).upload();
                result.setSuccess(true).setData(attachment);
        }
        catch (Exception e)
        {
            Log.log.error("Error uploading the attachment to document ", e);
            result.setSuccess(false).setMessage("Error uploading attachment. See log for more info.");
        }

        return result;
    }
    
    /**
     * Downloan a WikiDoc attachment.
     * 
     * @deprecated
     * Cannot find anywhere in the UI.
     * 
     * <br>
     * Ajax example:
     * <pre> 
     *    Ext.Ajax.request({
     *        "url":"/resolve/service/wiki/attachment/download", 
     *        "params":{
     *           "id":"8a9482e541e1c4610141e1c651f80004"
     *           },
     *           success: function(r) {
     *            console.log("success");
     *           }
     *     })
     * </pre>
     * URL example:
     * <pre>
     *  http://localhost:8080/resolve/service/wiki/attachment/download?id=[attachment_sys_id]
     * </pre>
     * 
     * 
     * @param id - attachment sysId
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/attachment/download", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void download(@RequestParam("id") String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            if(StringUtils.isNotEmpty(id))
            {
                FileUploadUtil.downloadFile(request, response, id, username);
            }            
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ServletException(e);
        }
    } // download
    
    

    /**
     * Get the list of attachment for WikiDoc specified either by <code>docSysId</code> or <code>docFullName</code>
     * <br>
     * To get a global WikiDoc attachments, specify <code>docFullName</code> as System.Attachment
     * 
     * @param docSysId : String WikiDoc SysId
     * @param docFullName : String WikiDoc Fullname. In case of getting a list of global attachments, it should be 'System.Attachment' 
     * @param query : {@link QueryDTO} provides start, limit, sorting, and filtering
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/attachment/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<AttachmentDTO> list(@RequestParam("docSysId") String docSysId, @RequestParam("docFullName") String docFullName, @ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<AttachmentDTO> result = new ResponseDTO<AttachmentDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try 
        {
            if(StringUtils.isNotEmpty(docSysId) || StringUtils.isNotEmpty(docFullName))
            {
                result = ServiceWiki.getAttachmentsFor(query, docSysId, docFullName, username);
//                result = ServiceWiki.getAttachmentsFor(query, "43ac4a410a14026e742f6ada8d35e795", null, username);//Test
                //result.setSuccess(true);
            }
            else
            {
                Log.log.error("Doc sysId or name must be there");
                result.setSuccess(false).setMessage("Error:Doc sysId or name must be there");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving attachments", e);
            result.setSuccess(false).setMessage("Error getting attachment list. See log for more info.");
        }

        result.setRecords(ServiceWiki.tokenizeAttachmentUrls(request, result.getRecords()));
        return result;
    }
    
    /**
     * Rename selected attachment
     * <br>
     * @param docSysId : String WikiDoc SysId
     * @param docFullName : String WikiDoc Fullname. In case of renaming global attachment, it should be 'System.Attachment'
     * @param id : String Attachment SysId
     * @param newName : String New name of the attachment
     * @param choice : String represending a choice. It could either be skip or overwrite.
     * <br> If skip is selected, the rename operation will not do anything if the attachment with the new name already present in the system. Overwrite otherwise.
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/attachment/rename", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO rename(@RequestParam("docSysId") String docSysId, @RequestParam("docFullName") String docFullName, @RequestParam("id") String id, @RequestParam("filename") String newName, @RequestParam("choice") String choice, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try 
        {
            if((StringUtils.isNotEmpty(docSysId) || StringUtils.isNotEmpty(docFullName)) && StringUtils.isNotBlank(id) && StringUtils.isNotBlank(newName) && StringUtils.isNotEmpty(choice))
            {
                boolean overwrite = choice.equalsIgnoreCase("skip") ? false : true;
                ServiceWiki.renameAttachment(docSysId, docFullName, id, newName, overwrite, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error renaming the attachments of document " + docFullName, e);
            result.setSuccess(false).setMessage("Error rename attachement. See log for more info.");
        }

        return result;
    }
    
    
    /**
     * Delete an attachment.
     * 
     * @param docSysId : String WikiDoc SysId
     * @param docFullName : String WikiDoc Fullname
     * @param ids : String array of attachment sysIds that will be deleted.
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/wikiadmin/attachment/delete", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam("docSysId") String docSysId, @RequestParam("docFullName") String docFullName, @RequestParam("ids") String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try 
        {
            if((StringUtils.isNotEmpty(docSysId) || StringUtils.isNotEmpty(docFullName)) && ids != null && ids.length > 0)
            {
                Set<String> attachSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.deleteAttachments(docSysId, docFullName, attachSysIds, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting the attachments of document " + docFullName, e);
            result.setSuccess(false).setMessage("Error deleting attachment. See log for more info.");
        }

        return result;
    }
    
    /**
     * Index given list of attachments.
     * 
     * @param docSysId : String WikiDoc SysId
     * @param docFullName : String WikiDoc Fullname
     * @param ids : String array of attachment sysIds that will be indexed
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/attachment/index", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO index(@RequestParam("docSysId") String docSysId, @RequestParam("docFullName") String docFullName,  @RequestParam("ids") String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try 
        {
            if((StringUtils.isNotEmpty(docSysId) || StringUtils.isNotEmpty(docFullName)) && ids != null && ids.length > 0)
            {
                Set<String> attachSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.indexWikiAttachments(docSysId, docFullName, attachSysIds, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error indexing the attachments of document " + docFullName, e);
            result.setSuccess(false).setMessage("Error indexing attachment. See log for more info.");
        }

        return result;
    }
}


