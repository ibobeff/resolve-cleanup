/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.AppsUtil;
import com.resolve.services.hibernate.vo.ResolveAppsVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

/**
 * In Resolve, every controller API can be controlled by giving a specific Role(s). So, user with that Role will able to see the content in UI.
 * This Controller provides CRUD operation for the same.
 *
 */
@Controller
@Service
public class AjaxApps extends GenericController
{
    /**
     * List App Security definitions.
     * <p>
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with the records populated with the <code>List<</code><code>ResolveAppsVO></code>. If no db record found, en empty <code>List</code> will be returned.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/apps/list", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            query.setModelName("ResolveApps");
            query.setWhereClause("(sysIsDeleted is null OR sysIsDeleted = false)");
            
            List<ResolveAppsVO> data = ServiceHibernate.getResolveApps(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ResolveApps", e);
            result.setSuccess(false).setMessage("Error getting apps list. See log for more info.");
        }
        
        return result;
    }

    /**
     * Save App Security definition
     * <p>
     * @param jsonProperty : JSON String representing {@link ResolveAppsVO} object.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with the record populated with the newly created <code><</code>ResolveAppsVO></code> object.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/apps/save", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty,  HttpServletRequest request) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        ResolveAppsVO entity = new ObjectMapper().readValue(json, ResolveAppsVO.class);

        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(ResolveAppsVO.class);
//        jsonConfig.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
//        ResolveAppsVO entity = (ResolveAppsVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveAppsVO vo = ServiceHibernate.saveResolveApps(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the resolve apps", e);
            result.setSuccess(false).setMessage("Error saving apps. See log for more info.");
        }
        
        return result;
    }

    /**
     * Get App Security definition specified by <code>id</code>
     * <p>
     * @param id : String representing sys_id of the definition which needs to be read.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with the record populated with the requested <code><</code>ResolveAppsVO></code> object, null otherwise.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/apps/get", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveAppsVO vo = ServiceHibernate.findResolveAppsById(id, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ResolveApps ", e);
            result.setSuccess(false).setMessage("Error getting apps. See log for more info.");
        }

        return result;
    }

    /**
     * Delete App Security definitions
     * <p>
     * @param ids : Array of String containing sys_ids of the definition user want to delete.
     * @param query : {@link QueryDTO} Not used at this time.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with the success true if operation succeeds, false otherwise.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/apps/delete", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO delete(@RequestParam("ids") String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteResolveAppsByIds(ids, query, username, false);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting ResolveApps", e);
            result.setSuccess(false).setMessage("Error deleting apps. See log for more info.");
        }

        return result;
    }

    /**
     * Tells whether the specified user has permission accessing the specified application controller
     * <p>
     * @param userName : String optional user name, if omitted, current logged in user will be used.
     * @param controller : String the controller to test. For example "/user".
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} true if the user has permission to access the controller; false otherwise
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/apps/hasPermission", method = { RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<HashMap<String, Object>> hasPermission(
        @RequestParam(value = "userName", required = false, defaultValue = "") String userName,
        @RequestParam(value = "controller") String controller,
        HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<HashMap<String, Object>> result = new ResponseDTO<HashMap<String, Object>>();
        String uName = userName;
        if (StringUtils.isEmpty(uName)) {
        	uName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        }
        try
        {
            HashMap<String, Object> map = new HashMap<String, Object>();
            boolean permission = false;
            if (!StringUtils.isEmpty(uName)) {
            	permission = AppsUtil.doesUserHaveRightsForController(controller, uName);
            }
            map.put("hasPermission", permission);
            result.setSuccess(true).setData(map);
        }
        catch (Exception e)
        {
            String msg = String.format("Error checking application security permission [%s] for user [%s]", controller, userName);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }


}
