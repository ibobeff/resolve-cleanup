package com.resolve.rsview.sso.saml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.rsview.sso.authenticator.impl.ADFSAuthenticator;
import com.resolve.rsview.sso.authenticator.impl.GenericSAMLAuthenticator;
import com.resolve.rsview.sso.authenticator.impl.NullAuthenticator;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public enum SAMLAuthenticatorFactory {

  INSTANCE;

  private static final String REFERRER_PARAM_NAME = "Referer";
  private static final String TRUSTED_REFERRER_VALIDATOR = "sso.saml.trustedIssuerHostnamesValidator";
  private static final String TRUSTED_GENERIC_SAML_ISSUER_HOSTNAMES_KEY = "sso.saml.genericSAML.trustedIssuerHostnames";
  private static final String TRUSTED_ADFS_ISSUER_HOSTNAMES_KEY = "sso.saml.trustedADFSIssuerHostnames";
  
  private enum TrustValidator {
    GENERIC,
    GENERIC_NONE,
    ADFS,
    ADFS_NONE,
    ALL;

    private TrustValidator() {}
  } 

  private final Map<SAMLAuthenticatorType, Callable<SSOAuthenticator>> authenticators = new HashMap<SAMLAuthenticatorType, Callable<SSOAuthenticator>>();
  private SAMLAuthenticatorFactory() {

    this.authenticators.put(SAMLAuthenticatorType.GENERIC_SAML, new Callable<SSOAuthenticator>() {
      public SSOAuthenticator call() {
        return new GenericSAMLAuthenticator();
      }
    });

    this.authenticators.put(SAMLAuthenticatorType.ADFS, new Callable<SSOAuthenticator>() {
      public SSOAuthenticator call() {
        return new ADFSAuthenticator();
      }
    });

  }

  private List<String> loadCommaSeparatedListOfValues(String systemPropertyKey) {

    List<String> list = new ArrayList<String>();
    if (StringUtils.isNotBlank(PropertiesUtil.getPropertyString(systemPropertyKey))) {
      String listOfValues[] = PropertiesUtil.getPropertyString(systemPropertyKey).split(",");
      for (String value : listOfValues) {
        list.add(value.trim());
      }
    }
    return list;
  }
  
  private boolean isInTrustedList(String remoteHost, String referrer, List<String> list) {
    
    boolean result = false;
    for (String recognizedIssuerHostname : list) {
      if ((remoteHost != null && remoteHost.contains(recognizedIssuerHostname)) || ((referrer != null && referrer.contains(recognizedIssuerHostname)))) {
        result = true;
        break;
      }
    }
    return result;
  }

  public SSOAuthenticator build(HttpServletRequest request, HttpServletResponse response) {

    SAMLAuthenticatorType samlAuthenticatorType = SAMLAuthenticatorType.NULL;
    
    TrustValidator validator = TrustValidator.ALL;
    try
    {
    	validator = TrustValidator.valueOf(PropertiesUtil.getPropertyString(TRUSTED_REFERRER_VALIDATOR).toUpperCase());
    }
    catch (IllegalArgumentException iae)
    {
    	Log.log.error("Failed to validate referer type from " + TRUSTED_REFERRER_VALIDATOR + " system property, defaulting to all");
    }
    
    String remoteHost = request.getRemoteHost() != null ? request.getRemoteHost() : null;
    String referrer = request.getHeader(REFERRER_PARAM_NAME) != null ? request.getHeader(REFERRER_PARAM_NAME) : null;
    List<String> trustedGenericSAMLIssuerHostnames = new ArrayList<String>();
    List<String> trustedADFSIssuerHostnames = new ArrayList<String>();

    // deciding which SAML authenticator to select based on the request or/and header parameters
    // If All checking first if is in the list of Generic SAML SSO trusted issuers, then if is the list of ADFS trusted issuers
    switch (validator)
    {
	    case GENERIC_NONE:
	    	samlAuthenticatorType = SAMLAuthenticatorType.GENERIC_SAML;
	    	break;
	    case ADFS_NONE:
	    	samlAuthenticatorType = SAMLAuthenticatorType.ADFS;
	    	break;
	    case ALL:
		    trustedGenericSAMLIssuerHostnames = loadCommaSeparatedListOfValues(TRUSTED_GENERIC_SAML_ISSUER_HOSTNAMES_KEY);
	    case ADFS:
		    trustedADFSIssuerHostnames = loadCommaSeparatedListOfValues(TRUSTED_ADFS_ISSUER_HOSTNAMES_KEY);
		    break;
	    case GENERIC:
		    trustedGenericSAMLIssuerHostnames = loadCommaSeparatedListOfValues(TRUSTED_GENERIC_SAML_ISSUER_HOSTNAMES_KEY);
			break;
	    default:
	    	Log.log.error("Unknown Validator Type: " + validator.toString());
    }
    if (isInTrustedList(remoteHost, referrer, trustedGenericSAMLIssuerHostnames)) {
		samlAuthenticatorType = SAMLAuthenticatorType.GENERIC_SAML;
    } else if (isInTrustedList(remoteHost, referrer, trustedADFSIssuerHostnames)) {
		samlAuthenticatorType = SAMLAuthenticatorType.ADFS;
	}

    Callable<SSOAuthenticator> samlAuthenticatorCallable = this.authenticators.get(samlAuthenticatorType);

    if (samlAuthenticatorCallable == null) {
      return new NullAuthenticator();
    } else {
      try {
        return samlAuthenticatorCallable.call();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

}
