/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

/**
 * Indicates single signed on user configuration is missing or incmplete in Resolve.
 * <p>
 * In a Single Sign On (SSO) integration, users login into host system and access
 * Resolve system through it. In SSO integration users has to login in only once 
 * into host system and allows user to access host and Resolve system simultaneously.
 *  
 * @author Hemant Phanasgaonkar
 * @since  5.5
 */
public class SSOConfigMissingOrIncompleteInResolveException extends Exception
{
    private static final long serialVersionUID = -6996311963513705875L;
	private String externalSystem;
    private String url;
    
    public SSOConfigMissingOrIncompleteInResolveException(String externalSystem, String url)
    {
        this.externalSystem = externalSystem;
        this.url = url;
    } // SSOConfigMissingOrIncompleteInResolveException
    
    public String getExternalSystem()
    {
        return externalSystem;
    }
    
    public String getUrl()
    {
        return url;
    }
    
    @Override
    public String toString()
    {
        return "SSOConfigMissingOrIncompleteInResolveException: externalSystem=" + externalSystem + ", url=" + url;
    }
} // SSOConfigMissingOrIncompleteInResolveException
