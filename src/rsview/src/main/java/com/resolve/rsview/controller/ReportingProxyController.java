package com.resolve.rsview.controller;

import java.net.URI;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.collect.Iterators;
import com.resolve.rsview.main.RSContext;
import com.resolve.util.StringUtils;

@Controller
@Service
public class ReportingProxyController extends GenericController
{

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String AUTHORIZATION_TYPE = "Bearer";
    private String bearerToken;
    
    private static Iterator<String> reportingInstanceAddressess;

    @RequestMapping("/reporting/**")
    @ResponseBody
    public ResponseEntity<?> getIncidetnsStatus(@RequestBody(required = false) String body, HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String requestUrl = request.getRequestURI().replace("/resolve/service/reporting", "");
        String dcsAuthUrl = ((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral).getDcsAuthenticationInstance();
        
        String reportingInstance = getReportinginstanceAddressess().next();
        URI uri = new URI(reportingInstance);

        String queryString = URLDecoder.decode(request.getQueryString(), "UTF-8");
        uri = UriComponentsBuilder.fromUri(uri).path(requestUrl).query(queryString).build(false).toUri();

        HttpHeaders headers = new HttpHeaders();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements())
        {
            String headerName = headerNames.nextElement();
            headers.set(headerName, request.getHeader(headerName));
        }

        headers.set(AUTHORIZATION_HEADER_NAME, AUTHORIZATION_TYPE + " " + requestAccessToken(dcsAuthUrl));

        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        try
        {
            ResponseEntity<?> responseEntity = restTemplate.exchange(uri, method, httpEntity, String.class);
            return new ResponseEntity<String>(responseEntity.getBody().toString(), responseEntity.getStatusCode());
        }
        catch (HttpClientErrorException e)
        {
            return new ResponseEntity<String>(e.getResponseBodyAsString(), e.getStatusCode());
        }

    }
    
    private Iterator<String> getReportinginstanceAddressess()
    {
        if(reportingInstanceAddressess == null) {
            synchronized (ReportingProxyController.class) {
                if(reportingInstanceAddressess == null){
                    reportingInstanceAddressess = Iterators.cycle(((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral).getReportingInstances().split(","));
                }
            }
        }
        return reportingInstanceAddressess;
    }

    private String requestAccessToken(String dcsAuthUrl) throws JSONException
    {
    	if(StringUtils.isNotEmpty(bearerToken) && StringUtils.isNotBlank(bearerToken)) {
    		return bearerToken;
    	}
    	
        RestTemplate restTemplate = new RestTemplate();
        String authUsername = ((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral).getDcsAuthenticationUsername();
        String authPassword = ((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral).getDcsAuthenticationPassword();
        String authRequest = "{\"username\":\"" + authUsername + "\", \"password\":\"" + authPassword + "\"}";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        HttpEntity<String> request = new HttpEntity<String>(authRequest,headers);
        
        HttpHeaders response = restTemplate.exchange(dcsAuthUrl, HttpMethod.POST, request, String.class).getHeaders();
        bearerToken = response.get("Authorization").get(0);
        return bearerToken;
    }

}
