/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsbase.MainBase;
import com.resolve.search.ReportAPI;
import com.resolve.search.model.UserInfo;
import com.resolve.search.saas.Customer;
import com.resolve.search.saas.SaaSReport;
import com.resolve.services.ServiceWiki;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;


/**
 * Controller for Resolve Reports 
 * 
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxReports extends GenericController
{
    private static final String ERROR_RETRIEVING_EXECUTION_DATA = "Error retrieving resolve execution data";
	private static final String ERROR_RETRIEVING_SAAS_MONTHLY_REPORT_DATA = "Error retrieving Saas monthly report data";
	private static final String ERROR_RETRIEVING_ADMIN_REPORT_DATA = "Error retrieving %s admin report data";
	private static final String ERROR_RETRIEVING_BUSINESS_REPORT_DATA = "Error retrieving %s business report data";
	private static final String ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA = "Error retrieving runbook analysis data";
	private static final String ERROR_RETRIEVING_WIKI_DATE_ANALYSIS_DATA = "Error retrieving wiki date analysis data";
	private static final String ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA = "Error retrieving wiki top analysis data";
	private static final String ERROR_RETRIEVING_WIKI_COUNT_ANALYSIS_DATA = "Error retrieving wiki count analysis data";
	private static final String ERROR_RETRIEVING_WIKI_AGE_ANALYSIS_DATA = "Error retrieving wiki age analysis data";
	private static final String ERROR_RETRIEVING_DT_PATH_DETAIL_REPORT = "Error retrieving DT path detail report";
	private static final String ERROR_RETRIEVING_DT_PATH_TREND_REPORT = "Error retrieving DT path trend report";
	private static final String ERROR_RETRIEVING_DT_REPORT_DATA = "Error retrieving DT report data";
	
	/**
     * 
     * Api for 
     *      Top and Bottom Executed DT reports. 
     *      Bottom Executed DT reports
     *           
     * Here are the list of 'filters' available for this api
     * 
     * top = true | false  ==> for the Top Executed report set this to true, for Bottom Executed reports set this to false
     * sysCreatedOn ==> date range 
     * namespaces ==> comma separted values of namespaces
     * authors ==> comma seperated values of authors
     * noOfRecs ==> top 5 recs or bottom 5 recs, so # of recs for the selection
     * 
     * Refer to TestReports.java for the test case.
     * 
     * @param query
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/report/dtexecuted", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> dtExecuted(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            result = ServiceWiki.getDTExecutionReportData(query, username);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_DT_REPORT_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_DT_REPORT_DATA);
        }

        return result;
    }
    
    /**
     * 
     * api for 
     *      Total Path Count Trend Reports
     *      Abort Path Count Trend Reports
     *      Path Duration Trend Reports
     * 
     * Here are the list of 'filters' available for this api
     * 
     * namespaces ==> comma separted values of namespaces
     * authors ==> comma seperated values of authors
     * status ==> valid values are COMPLETED, ABORTED
     * interval ==> valid values are day, week, month, year
     * dtFullNames ==> comma separated dt fullnames
     * sysCreatedOn ==> date range 
     * 
     * 
     * @param query
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/report/dtpathtrend", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> dtPathCountTrend(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            result = ServiceWiki.getDTPathTrendReport(query, username);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_DT_PATH_TREND_REPORT, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_DT_PATH_TREND_REPORT);
        }

        return result;
    }
    

    /**
     * 
     *  API for 
     *      Path Completed Detail Reports
     *      Path Aborted Detail Reports
     * 
     * Here are the list of 'filters' available for this api
     * 
     * namespaces ==> comma separted values of namespaces
     * authors ==> comma seperated values of authors
     * status ==> valid values are COMPLETED, ABORTED
     * interval ==> valid values are day, week, month, year
     * dtFullNames ==> comma separated dt fullnames
     * sysCreatedOn ==> date range 
     * 
     * @param query
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/report/dtpathdetail", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> dtPathDetailReport(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            result = ServiceWiki.getDTPathDetailReport(query, username);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_DT_PATH_DETAIL_REPORT, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_DT_PATH_DETAIL_REPORT);
        }

        return result;
    }
    

    @RequestMapping(value = "/report/getWikiAgeAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<Object, Object>> getWikiAgeAnalysis(@RequestParam("dateField") String dateField, @RequestParam("userField") String userField,QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.wikiAgeAnalysis(queryDTO, userField, dateField, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_WIKI_AGE_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_WIKI_AGE_ANALYSIS_DATA);
        }
        return result;
    }

    @RequestMapping(value = "/report/wikiCountAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<Object, Object>> wikiCountAnalysis(@RequestParam("aggregationField") String aggregationField, @RequestParam("subAggregationField") String subAggregationField, @RequestParam("userField") String userField, QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.wikiCountAnalysis(queryDTO, aggregationField, subAggregationField, userField, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_WIKI_COUNT_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_WIKI_COUNT_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/wikiTopAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> wikiTopAnalysis(QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.wikiTopAnalysis(queryDTO, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/taskName", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> taskName(QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.taskName(queryDTO, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/wikiTopSummaryAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> wikiTopSummaryAnalysis( QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.wikiTopSummaryAnalysis(queryDTO, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA);
        }
         return result;
    }
    
    @RequestMapping(value = "/report/wikiSummaryAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> wikiSummaryAnalysis(@RequestParam("aggregationType") String aggregationType, QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.wikiSummaryAnalysis(queryDTO, aggregationType, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_WIKI_TOP_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/wikiDateAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<Object, Object>> wikiDateAnalysis(@RequestParam("dateField") String dateField, @RequestParam("userField") String userField, @RequestParam("lowerBound") String lowerBound, @RequestParam("upperBound") String upperBound, QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.wikiDateAnalysis(queryDTO, userField, dateField, lowerBound, upperBound, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_WIKI_DATE_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_WIKI_DATE_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/runbookAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> runbookAnalysis(@RequestParam("aggregationType") String aggregationType, @RequestParam("interval") String interval, @RequestParam("timezone") String timezone, @RequestParam("lowerBound") String lowerBound, @RequestParam("upperBound") String upperBound, QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.runbookAnalysis(queryDTO,aggregationType, interval, timezone, lowerBound, upperBound, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/runbookExecutionAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> runbookExecutionAnalysis(@RequestParam("aggregationType") String aggregationType, @RequestParam("interval") String interval, @RequestParam("timezone") String timezone, @RequestParam("lowerBound") String lowerBound, @RequestParam("upperBound") String upperBound, QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.runbookExecutionAnalysis(queryDTO,aggregationType, interval, timezone, lowerBound, upperBound, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/timerAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> timerAnalysis(@RequestParam("interval") String interval, @RequestParam("timezone") String timezone, @RequestParam("lowerBound") String lowerBound, @RequestParam("upperBound") String upperBound, QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.timerAnalysis(queryDTO, interval, timezone, lowerBound, upperBound, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/taskAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> taskAnalysis(@RequestParam("interval") String interval, @RequestParam("timezone") String timezone, @RequestParam("lowerBound") String lowerBound, @RequestParam("upperBound") String upperBound, QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.taskAnalysis(queryDTO, interval, timezone, lowerBound, upperBound, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/topTaskAnalysis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> topTaskAnalysis( @RequestParam("type") String type,  QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.topTaskAnalysis(queryDTO, type, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/wikiFilter", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> wikiFilter( QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.wikiFilter(queryDTO, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA);
        }
        return result;
    }
    
    @RequestMapping(value = "/report/runbookName", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> runbookName( QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = ReportAPI.runbookName(queryDTO, new UserInfo(null, null, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RUNBOOK_ANALYSIS_DATA);
        }
        return result;
    }
    
    /**
     * API to get JVM report (avarage memory or thread count over the period of time).
     * 
     * @param queryDTO : QueryDTO containing date range for which the report is needed.
     * @param interval : String specifying the interval, which determines which table to query. It can have either 1m (minute report) or 1h (hourly report) or 1d for (24 hour report).
     * 		  Any other value will result defaulting to 24 hour report. (SQL tables will be ignored after 2 years from the Resolve 5.4 release)
     * @param type : String specifying what is the intended report. For JVM report, it can have either 'thread' or 'memory'. Any other value will default to 'memory'. For Load report,
     * 		  it can either have '1m' (1 monute load), '5m' (5 minutes load) or '15m' (15 minutes load). Will default to 15m for any other type. For Database report, it can either have
     * 		  'dbSize', 'resTime' or 'waitTime' and will default to 'waitTime' for any other type. For Page report, it can either have 'wiki' or 'wikiResTime' and will default
     * 		  to 'wikiResTime' for any other type.
     * @param reportType : String specifying the type of the report needed. It can have either 'jvm', 'cpu', 'database' or 'page'. Any other value will throw an exception.
     * 
     * @param request
     * @return
     * @throws ServletException
     */
    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/report/admin", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO adminReport(@ModelAttribute QueryDTO queryDTO, @RequestParam String interval, @RequestParam String type, String reportType, HttpServletRequest request) throws ServletException
    {
    	ResponseDTO result = new ResponseDTO();
    	try
        {
            result = ReportAPI.adminReports(queryDTO, interval, type, reportType);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_ADMIN_REPORT_DATA, reportType);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }
    	
    	return result;
    }
    
    @RequestMapping(value = "/saas/monthlyusage", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getMonthlyUsage(@RequestParam String timeZone, @RequestParam(value = "previousLicYear", required = false) Integer previousLicYear, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        Customer customer = null;
        
        try {
            customer = new Customer();
            if(!customer.isSaaSCustomer()) {
                result.setMessage("Current customer is not a SaaS customer.");
                result.setSuccess(false);
                return result;
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            return result;
        }

        try
        {
            result = SaaSReport.getMonthlyUsage(customer, timeZone, previousLicYear);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_SAAS_MONTHLY_REPORT_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SAAS_MONTHLY_REPORT_DATA);
        }
        
        return result;
    } // getMonthlyUsage()
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/saas/usagesummary", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getUsageSummary(@RequestParam String timeZone, @RequestParam String usageType, @RequestParam(value = "previousLicYear", required = false) Integer previousLicYear, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        Customer customer = null;
        
        try {
            customer = new Customer();
            if(!customer.isSaaSCustomer()) {
                result.setMessage("Current customer is not a SaaS customer.");
                result.setSuccess(false);
                return result;
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            return result;
        }

        try
        {
            result = SaaSReport.getVolumeBasedUsageReport(customer, usageType, timeZone, previousLicYear);
//            switch(usageType) {
//                case "event":
//                    //result = SaaSReport.getEventUsage(customer, timeZone);
//                	result = SaaSReport.getVolumeBasedUsageReport(customer, "Event", timeZone);
//                    break;
//                case "ticket":
//                    //result = SaaSReport.getTicketUsage(customer, timeZone);
//                	result = SaaSReport.getVolumeBasedUsageReport(customer, "Ticket", timeZone);
//                    break;
//                default:
//                    Log.log.error("Unknown usage type.");
//                    result.setSuccess(false).setMessage("Unknown usage type.");
//                    break;
//            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_SAAS_MONTHLY_REPORT_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SAAS_MONTHLY_REPORT_DATA);
        }
        
        return result;
    } // getUsageSummary()
    
    @RequestMapping(value = "/saas/licenseinfo", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getLicenseInfo(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        Customer customer = null;
        
        try {
            customer = new Customer();
            if(!customer.isSaaSCustomer()) {
                result.setMessage("Current customer is not a SaaS customer.");
                result.setSuccess(false);
                return result;
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            return result;
        }

        try
        {
            result = SaaSReport.getLicenseInfo(customer);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_SAAS_MONTHLY_REPORT_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SAAS_MONTHLY_REPORT_DATA);
        }
        
        return result;
    } // getLicenseInfo()
    
    @RequestMapping(value = "/report/executionstatus", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getExecutionStatus(QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();

        try
        {
            result = ReportAPI.getExecutionStatus(queryDTO);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_EXECUTION_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_EXECUTION_DATA);
        }
        return result;
    } // getExecutionStatus()

    /**
     * 
     * This endpoint is for getting report data for Decision Tree's Execution Count, Execution Time,
     * Execution Path and Node Duration.
     * 
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://localhost:8080/resolve/service/reports/getDTReportData
     *     payload: {
     *         "filterName":"\"'Demo.DecisionTree6','dt.dt', 'javier.reports', 'SecOps.Phishing'\"",
     *         "reportType":"Execution Count",
     *         "from":1439153845682,
     *         "to":1502225845682,
     *         "page": 1,
     *         "start": 0,
     *         "limit": 50
     *     }
     * </code></pre>
     * 
     * @param request  HttpServletRequest
     * @RequestBody    JSONObject queryParamsPayload,
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/reports/getDTReportData", method = { RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getDTReportData(
                    HttpServletRequest request,
                    @RequestBody JSONObject payload,
                    HttpServletResponse response
                    ) throws ServletException, IOException
    {
        return ReportAPI.getDTReport(request, payload, response, "data");
    }

    /**
     * 
     * This endpoint is for getting filters for Decision Tree's Execution Count, Execution Time,
     * Execution Path and Node Duration.
     * 
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://localhost:8080/resolve/service/reports/getDTReportFilters
     *     payload: {
     *         "reportType":"Execution Count",
     *         "from":1439153845682,
     *         "to":1502225845682,
     *         "page": 1,
     *         "start": 0,
     *         "limit": 50
     *     }
     * </code></pre>
     * 
     * @param request  HttpServletRequest
     * @RequestBody    JSONObject queryParamsPayload,
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/reports/getDTReportFilters", method = { RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getDTReportFilters(
                    HttpServletRequest request,
                    @RequestBody JSONObject payload,
                    HttpServletResponse response
                    ) throws ServletException, IOException
    {
        return ReportAPI.getDTReport(request, payload, response, "filter");
    }

    
    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/report/businessmetadata", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO businessReportsMetaData(@ModelAttribute QueryDTO queryDTO, @RequestParam String reportType, HttpServletRequest request) throws ServletException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            result = ReportAPI.businessReportsMetaData(queryDTO, reportType);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String msg = String.format(ERROR_RETRIEVING_BUSINESS_REPORT_DATA, reportType);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/saas/usagesummaryv2", method = { RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Map<String, Map<String, Long>>>> getUsageSummaryV2(
    											@RequestParam Long startDate, 
    											@RequestParam Long endDate, 
    											@RequestParam(required = false) String sysOrg, 
    											HttpServletRequest request) throws ServletException, IOException {
    	ResponseDTO<Map<String, Map<String, Map<String, Long>>>> result = 
    			SaaSReport.getVolumeBasedUsageReportV2(
    					startDate, 
    					endDate,
    					MainBase.main.getLicenseService().getCurrentLicense().getEventSources(),
    					true);
    	
    	return result;
    }
}
