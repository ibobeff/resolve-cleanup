
/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

public class Release extends com.resolve.rsbase.Release
{
    public Release(String service)
    {
        super();

        name = "RSView";
        type = "RSVIEW";
        if (service == null || service.equals(""))
        {
            service = "rsview";
        }
        serviceName = service;
        version = "trunk";
        year = "2018";
        copyright = "(C) Copyright 2018 Resolve Systems, LLC";

    } // Release

} // Release
        